package com.cycle30.audit;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate.encryptor.HibernatePBEEncryptorRegistry;

import com.cycle30.audit.exception.AuditException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
 

/** 
 * Generic connection manager singeleton for database and web service end point URLs.
 * 
 */
public class ConnectionManager {
    
    private static final Logger log = Logger.getLogger(ConnectionManager.class);
    
    // System environment variable (set in script).
    private static final String ENCRYPTION_KEY_ENV_VAR = "C30APPENCKEY";
    
    private static Connection connection;
    

    // Instantiate singleton when class is loaded by JVM
    private static final ConnectionManager self = new ConnectionManager();
    
    
    // --- Spring-injected properties
    private static String dbUrl;   
    private static String dbUser;
    private static String dbDriver;
    private static String password;
    // --- End of Spring-injected properties
    
    /**
     * @return the dbUrl
     */
    public String getDbUrl() {
        return dbUrl;
    }
    /**
     * @param dbUrl the dbUrl to set
     */
    public void setDbUrl(String dbUrl) {
        ConnectionManager.dbUrl = dbUrl;
    }
    /**
     * @return the dbUser
     */
    public String getDbUser() {
        return dbUser;
    }
    /**
     * @param dbUser the dbUser to set
     */
    public void setDbUser(String dbUser) {
        ConnectionManager.dbUser = dbUser;
    }
    /**
     * @return the dbDriver
     */
    public String getDbDriver() {
        return dbDriver;
    }
    /**
     * @param dbDriver the dbDriver to set
     */
    public void setDbDriver(String dbDriver) {
        ConnectionManager.dbDriver = dbDriver;
    }
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        ConnectionManager.password = password;
    }




    
    //-------------------------------------------------------------------------
    /** Private singleton constructor. 
     * */
    private ConnectionManager() {
        super();
    }
    
   
    //-------------------------------------------------------------------------
    /**
     * Return instance of singleton class
     * 
     * @return ConnectionManager instance.
     */
    public synchronized static ConnectionManager getInstance() {
        return self;
    }
    
    
    
    /**
     * 
     * @return Connection
     * @throws AuditException
     */
    public Connection getDbConnection() throws AuditException  {
        
        Connection conn = null;
        
        // See if existing db connection already exists.
        if (connection != null) {
            return connection;
        }      
        
        conn = getNewDbConnection();
        
        return conn;
        
    }
    
    
    
    /**
     * 
     * Get fresh db connection.
     * 
     * @return Connection
     * @throws AuditException
     */
    public Connection getNewDbConnection() throws AuditException  {
        
        Connection conn = null;
        
     
        try {
            
            Class.forName(dbDriver);
            String password = decryptPassword();
            conn = DriverManager.getConnection(dbUrl, dbUser, password);
            
            conn.setAutoCommit(true);
            
            // Set connection variable for later reuse
            connection = conn;
            
            
        } catch (SQLException e) {            
            throw new AuditException(e);
        } catch (ClassNotFoundException cnfe) {
            throw new AuditException(cnfe);
        } catch (Exception e) {
            throw new AuditException(e);
        }
        
        return conn;
    }
    
    


    
    /**
     * 
     * @param conn
     */
    private void closeDbConnection(Connection conn) {
        
        try {
            conn.close();
        }
        catch (Exception e)  {}  // shouldn't happen, but nothing we do if so
    }
    
    
    
    /**
     * 
     * @return decrypted password
     */
    private String decryptPassword() throws AuditException {
        
        // decrypt the password.
        
        String decryptedPwd = null;
        
        try {
                      
            String decryptionPassword = System.getenv(ENCRYPTION_KEY_ENV_VAR);
            if (decryptionPassword == null) {
                throw new AuditException(ENCRYPTION_KEY_ENV_VAR + " env variable not found!");
            }

            StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
            strongEncryptor.setAlgorithm("PBEWithMD5AndDES");
            strongEncryptor.setPassword(decryptionPassword);
            HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
            registry.registerPBEStringEncryptor("configurationHibernateEncryptor", strongEncryptor);
            decryptedPwd = strongEncryptor.decrypt(password);

        } 
        catch (Exception e) {
            log.error(e);
            throw new AuditException("Error decrypting db password.", e);
        }
        
        return decryptedPwd;
        
    }

}
