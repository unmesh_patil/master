package com.cycle30.audit.exception;


import org.apache.log4j.Logger;


/** 
 * General purpose exception handler for Cycle30 Audit logging
 * 
 *
 */
public class AuditException extends Exception {
    
    
    private static final Logger log = Logger.getLogger(AuditException.class);


    /**
     * Constructor
     * 
     * @param str String exception message.
     */
    public AuditException(String str, Exception e) {
        super(str);
        log.error(str, e);
        e.printStackTrace();      
    }



    /**
     * Constructor
     * 
     * @param str String exception message.
     */
    public AuditException(String str) {
        super(str);
        log.error(str);
        this.printStackTrace();
    }
    

    /**
     * Constructor
     */
    public AuditException(Exception e) {
        super(e);
        log.error(e);
        e.printStackTrace();  
    }




}