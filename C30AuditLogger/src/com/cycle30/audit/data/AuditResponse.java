package com.cycle30.audit.data;


/**
 * Class representing a general purpose response audit instance
 *
 */
public class AuditResponse {
    
    private String applicationId;
    private long messageId;
    private String reponseCode;
    private String responseValue;
    private String transactionId;
    private String clientTransId;
    
    

    /**
     * @return the clientTransId
     */
    public String getClientTransId() {
        return clientTransId;
    }
    /**
     * @param clientTransId the clientTransId to set
     */
    public void setClientTransId(String clientTransId) {
        this.clientTransId = clientTransId;
    }
    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }
    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    /**
     * @return the applicationId
     */
    public String getApplicationId() {
        return applicationId;
    }
    /**
     * @param applicationId the applicationId to set
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }
    /**
     * @return the messageId
     */
    public long getMessageId() {
        return messageId;
    }
    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }
    /**
     * @return the reponseCode
     */
    public String getReponseCode() {
        return reponseCode;
    }
    /**
     * @param reponseCode the reponseCode to set
     */
    public void setReponseCode(String reponseCode) {
        this.reponseCode = reponseCode;
    }
    /**
     * @return the responseValue
     */
    public String getResponseValue() {
        return responseValue;
    }
    /**
     * @param responseValue the responseValue to set
     */
    public void setResponseValue(String responseValue) {
        this.responseValue = responseValue;
    }

}
