package com.cycle30.audit.data;


/**
 *  Class representing a general purpose request audit instance
 *
 */
public class AuditRequest {

    private String applicationId;
    private String clientId;
    private String userId;
    private String resourceRequested;
    private String resourceMethod;
    private String clientTransId;
    
    
    /**
     * @return the clientTransId
     */
    public String getClientTransId() {
        return clientTransId;
    }
    /**
     * @param clientTransId the clientTransId to set
     */
    public void setClientTransId(String clientTransId) {
        this.clientTransId = clientTransId;
    }
    /**
     * @return the resourceMethod
     */
    public String getResourceMethod() {
        return resourceMethod;
    }
    /**
     * @param resourceMethod the resourceMethod to set
     */
    public void setResourceMethod(String resourceMethod) {
        this.resourceMethod = resourceMethod;
    }
    private String payloadReceived;
    private String reponseFormat;
    
    
    /**
     * @return the applicationId
     */
    public String getApplicationId() {
        return applicationId;
    }
    /**
     * @param applicationId the applicationId to set
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }
    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }
    /**
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    /**
     * @return the resourceRequested
     */
    public String getResourceRequested() {
        return resourceRequested;
    }
    /**
     * @param resourceRequested the resourceRequested to set
     */
    public void setResourceRequested(String resourceRequested) {
        this.resourceRequested = resourceRequested;
    }
    /**
     * @return the payloadReceived
     */
    public String getPayloadReceived() {
        return payloadReceived;
    }
    /**
     * @param payloadReceived the payloadReceived to set
     */
    public void setPayloadReceived(String payloadReceived) {
        this.payloadReceived = payloadReceived;
    }
    /**
     * @return the reponseFormat
     */
    public String getReponseFormat() {
        return reponseFormat;
    }
    /**
     * @param reponseFormat the reponseFormat to set
     */
    public void setReponseFormat(String reponseFormat) {
        this.reponseFormat = reponseFormat;
    }
    
    
}
