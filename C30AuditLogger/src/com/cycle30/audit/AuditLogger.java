package com.cycle30.audit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.cycle30.audit.data.AuditRequest;
import com.cycle30.audit.data.AuditResponse;
import com.cycle30.audit.exception.AuditException;


/**
 * 
 * Driver class for general purpose Cycle30 audit logging.
 * 
 * Note that the database connection properties are injected via
 * Spring IOC in the ConnectionManager.java class.  This alleviates
 * the need for any embedded .properties files, and also removes the
 * need for environment-specific versions of the resulting
 * C30AuditLogger.jar file 
 *
 */
public class AuditLogger {
    
    
    private static final Logger log = Logger.getLogger(AuditLogger.class);
    
    private static final String GET_NEXTVAL_SQL =
        "select AUDIT_SEQUENCE.nextval from DUAL";
    
    
    // Order of request params to substitute:
    // 1. Application id
    // 2. Message id (unique from sequence table)
    // 3. Client id 
    // 4. User id
    // 5. Resource requested
    // 6. Resource method (GET, PUT, etc)
    // 7. Payload received
    // 8. Response format
    // 9. Client transaction id
    private static final String REQUEST_INSERT_SQL =
        "insert into REQUEST_AUDIT values (?, ?, SYSDATE, ?, ?, ?, ?, ?, ?, ?)";
    
    
    // Order of params to substitute:
    // 1. Application id
    // 2. Message id
    // 3. Response code
    // 4. Response value
    // 5. Transaction id
    // 6. Client transaction id.
    private static final String RESPONSE_INSERT_SQL =
        "insert into RESPONSE_AUDIT values (?, ?, SYSDATE, ?, ?, ?, ?)";
    

    /**
     * Insert audit log request row
     * 
     * @param auditRequest
     */
    public long logRequest(AuditRequest auditRequest) throws AuditException {
        
        Connection conn = null;

        
        try {
            
             // Get db connection.
             conn = ConnectionManager.getInstance().getDbConnection();
             
             long messageId = getSequenceNextValue(conn);

             PreparedStatement ps = null;
             ps = conn.prepareStatement(REQUEST_INSERT_SQL);
             
             ps.setString(1, auditRequest.getApplicationId());
             ps.setLong  (2, messageId);  
             ps.setString(3, auditRequest.getClientId());  
             ps.setString(4, auditRequest.getUserId());
             ps.setString(5, auditRequest.getResourceRequested());
             ps.setString(6, auditRequest.getResourceMethod());
             ps.setString(7, auditRequest.getPayloadReceived());
             ps.setString(8, auditRequest.getReponseFormat());
             ps.setString(9, auditRequest.getClientTransId());
             
             ps.executeQuery();

             ps.close();
             
             return messageId;
                
        }
        catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
            throw new AuditException("Error inserting into REQUEST_AUDIT table", e);   
        } 
        
        
    }
   
    
    /**
     * Get next value from AUDIT_SEQUENCE table
     * 
     */
    public long getSequenceNextValue(Connection conn) throws AuditException {
        
        long messageId = 0;

        try {
            
            PreparedStatement ps = null;
            ps = conn.prepareStatement(GET_NEXTVAL_SQL);
            
            ResultSet resultSet  = ps.executeQuery();
            resultSet.next();
            messageId = resultSet.getLong(1);
            
            resultSet.close();
            ps.close();
        
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
            throw new AuditException("Error getting sequence value AUDIT_SEQUENCE table", e);   
        } 
        return messageId;
        
        
    }
    
  
    
    /**
     * Insert new audit response row
     * 
     * @param auditResponse
     * @throws AuditException
     */
    public void logResponse(AuditResponse auditResponse) throws AuditException {
        
        Connection conn = null;

        
        try {
             // Get db connection.
             conn = ConnectionManager.getInstance().getDbConnection();

             PreparedStatement ps = null;
             ps = conn.prepareStatement(RESPONSE_INSERT_SQL);
             
             ps.setString(1, auditResponse.getApplicationId());
             ps.setLong  (2, auditResponse.getMessageId());  
             ps.setString(3, auditResponse.getReponseCode());
             ps.setString(4, auditResponse.getResponseValue());
             ps.setString(5, auditResponse.getTransactionId());
             ps.setString(6, auditResponse.getClientTransId());
             ps.executeQuery();

             ps.close();
             
                
        }
        catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
            throw new AuditException("Error inserting into RESPONSE_AUDIT table", e);   
        } 
        
    }


}
