#!/bin/ksh


f_debug()
{
  if [ x$DEBUG = x"1" ]; then
    echo " [DEBUG] $*" | tee -a ${logFile}
  fi
}

f_info()
{
  echo " [INFO] $*" | tee -a ${logFile}
}

f_error()
{
  ERROR=1
  echo "" | tee -a ${logFile}
  echo " [ERROR] $*" | tee -a ${logFile} 1>&2
  echo "" | tee -a ${logFile}
}

#------------------------------------------------------------------------------+
# cleanup function - clean up work files
#------------------------------------------------------------------------------+
cleanup()
{
  rm -f ${tempLogFile}
}


#------------------------------------------------------------------------------+
# installexit function - exit unless CONTONIUEONINSTALLERROR
#------------------------------------------------------------------------------+
installexit()
{
  if [ "${contOnError}" != "Y" ]; then
    cleanup
    f_error "INSTALL FAILED!"
    exit 1
  fi
}


#------------------------------------------------------------------------------+
# executeandlog function - execute a command and make sure its output goes to
# both the console and the log, plus retain the status code.
#------------------------------------------------------------------------------+
executeandlog()
{
  $@ > ${tempLogFile} 2>&1
  status=$?
  cat ${tempLogFile} | tee -a ${logFile}
}


#------------------------------------------------------------------------------+
# derivepropertyvalue function - derive an actual property value by processing
# %OTHER_PROP% references.
#------------------------------------------------------------------------------+
derivepropertyvalue()
{
  derivedPropVal=`sed -n -e "/^ *$1 *=/s/.*= *\(.*\)/\1/p" < $2`
  tmp_lookup=""
  tmp_v=`echo "${derivedPropVal}" | sed -n -e "s/[^%]*%\([^%][^%]*\)%.*/\1/p"`
  while [ "$tmp_v" != "" ]; do
    tval=`echo "${tmp_lookup}" | sed -n -e "/:${tmp_v}:/p"`
    if [ "${tval}" != "" ]; then
      cleanup
      stderr "Error: Referenced variable is recursively mapped (%${tmp_v}%) in $2 while resolving $1"
      stderr "INSTALL FAILED!"
      exit 1
    fi
    tval=`sed -n -e "/^ *${tmp_v} *=/s/.*= *\(.*\)/\1/p" < $2`
    if [ "${tval}" = "" ]; then
      cleanup
      stderr "Error: Could not find referenced variable (%${tmp_v}%) in $2 while resolving $1"
      stderr "INSTALL FAILED!"
      exit 1
    fi
    derivedPropVal=`echo "${derivedPropVal}" | sed -n -e "s:%${tmp_v}%:${tval}:p"`
    tmp_lookup="${tmp_lookup}:${tmp_v}:"
    tmp_v=`echo "${derivedPropVal}" | sed -n -e "s/[^%]*%\([^%][^%]*\)%.*/\1/p"`
  done
}

f_check_install_database()
{
  _file="./install.database"
  f_info "Start to check '$_file'"

  if [ ! -f $_file ]; then
    f_error "'$_file' not exist."
    f_exit 1
  fi
  grep -v "^#" $_file | grep WPDS | while read dsname dbhost dbport dbname dbuser dbpwd initcap maxcap testonreserve testfreq retryfreq shrinkfreq trusttime jndinames
  do
    __dsname=$dsname
    f_info " dsname = $__dsname"
    f_debug " dsname        = $dsname"
    f_debug " dbhost        = $dbhost"
    f_debug " dbport        = $dbport"
    f_debug " dbname        = $dbname"
    f_debug " dbuser        = $dbuser"
    f_debug " dbpwd         = $dbpwd"
    f_debug " initcap       = $initcap"
    f_debug " maxcap        = $maxcap"
    f_debug " testonreserve = $testonreserve"
    f_debug " testfreq      = $testfreq"
    f_debug " retryfreq     = $retryfreq"
    f_debug " shrinkfreq    = $shrinkfreq"
    f_debug " trusttime     = $trusttime"
    f_debug " jndinames     = $jndinames"
    f_debug " "

    if [ x$__dsname = 'x' ]; then
      f_error "Can not find data source in $_file. Please update '$_file'"
    fi
  done

  if [ $ERROR = 1 ]; then
    f_exit 1
  else
    f_info "Database information is set in '$_file'"
  fi
}

