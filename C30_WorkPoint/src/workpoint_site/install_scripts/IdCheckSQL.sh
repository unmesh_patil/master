#!/bin/sh

. $ARBORDIR/scripts/utility/decrypt_pw.sh

sqlplus -S $ARBORDBU/$ARBOR_DB_PWD\@$DB << THEEND

set serveroutput off

DECLARE

BEGIN

	c30arbor.workpoint_id_check();

END;
/
EXIT;

THEEND