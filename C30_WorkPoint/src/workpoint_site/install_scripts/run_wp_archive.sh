#!/bin/sh

printusage() {
   echo "usage: $0 <mode> <older_than> "
   echo "mode is (ARCHIVE, DELETE) "
   echo "mode ARCHIVE will move old data to the archive table"
   echo "mode DELETE will delete old data from the archive table"
   echo "older_than must be a number"
   echo " older_than days the record needs to be"
   echo "Example: "
   echo " run_wp_archive ARCHIVE 90 - archive data older than 90 days"
   echo " run_wp_archive DELETE 180 - delete archived data older than 180 days"
}

printinvalidnumber() {
	echo "$DAYS is not a valid number"
	echo ""
	printusage
}

if [[ ! $# = 2 ]] ; then
   printusage
   exit 1
fi

MODE=$1
DAYS=$2

if ! [[ "$DAYS" =~ ^[0-9]+$ ]] ; then
	printinvalidnumber
	exit 1
fi

if [[ $MODE = "ARCHIVE" ]]; then

	SCRIPT='c30arbor.wp_data_archive('$DAYS');'
	
elif [[ $MODE = "DELETE" ]]; then

	SCRIPT='c30arbor.wp_archive_clean('$DAYS');'
	
else 
	printusage
	exit 1
fi

. $ARBORDIR/scripts/utility/decrypt_pw.sh

echo "Script Start:" `date`

RSP=`sqlplus -S $ARBORDBU/$ARBOR_DB_PWD\@$DB << THEEND

set serveroutput on
set feedback off

DECLARE

BEGIN

	$SCRIPT

END;
/
EXIT;

THEEND`

echo "Script End:" `date`

if [[ -z "$RSP" ]]; then

	exit 0

else

	echo $RSP
	exit 1
	
fi