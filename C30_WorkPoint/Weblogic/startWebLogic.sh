#!/bin/sh

# Run the IdCheckSQL.sh file to ensure the IDs are up to date before starting the server
cd $WORKPOINT_SITE/install_scripts
./IdCheckSQL.sh

addon=`date +%Y%m%d%H%M%S`

DOMAIN_HOME="/opt/app/kenanfx2/arbor/bea/Weblogic/user_projects/domains/WorkPoint"
cd ${DOMAIN_HOME}
mv nohup.out ./logs/nohup.out.$addon
nohup ${DOMAIN_HOME}/bin/startWebLogic.sh &


