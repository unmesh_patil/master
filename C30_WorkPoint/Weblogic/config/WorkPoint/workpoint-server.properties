#   WorkPoint Server Configuration Variables
#         WorkPoint version 3.4
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# This file, which conforms to the java.util.Properties file
# definition, configures your WorkPoint Server with instance specific
# data values. You cannot run the WorkPoint Server without setting
# the required configuration properties in this file.
#
# -------------------------------------------------

#############################################################################
# WorkPoint Cache  settings

# Determines the maximum limit on the number of objects to have within the
# cache. Defaults to 512 objects
default.cache.limit = 512

# Determines the maximum amount of time that an object will be kept in
# the cache after it has been used. This time is specified in milliseconds.
# The default value is 3600000 or 1 hour.
default.cache.refresh = 3600000

alert.cache.limit = 512
alert.cache.refresh = 3600000

job.cache.limit = 512
job.cache.refresh = 3600000

mail.cache.limit = 512
mail.cache.refresh = 3600000

process.cache.limit = 512
process.cache.refresh = 3600000

script.cache.limit = 512
script.cache.refresh = 3600000

# Specifies how often the server should go to the database to obtain unique IDs for
# use as a key for the WorkPoint tables. The number dictates how many IDs to retrieve at
# a time. The higher the number, the lower the frequency of database hits to retrieve the
# IDs, but the larger the gaps will be in ID usage when the server is stopped and restarted.
# The default is 25, and numbers smaller than the default are ignored. The default value
# will be sufficient for most WorkPoint implementations.
#
# Note - Unique IDs in WorkPoint are stored in integer fields in the database, and will
# never automatically wrap to zero if the ID approaches the maximum integer value for the
# database. Although that is a very large number, it is conceivable that a very high volume
# system running for a very long time could approach the maximum integer value, especially
# if this limit is adjusted to a sufficiently high number and the system is frequently stopped
# and restarted. In this unlikely case the solution is to change the Database ID in the WP_INI
# table and restart the system. The Database ID is also part of the unique key, so the ID
# numbers would then start at zero again.
#unique.id.cache.limit = 25

#############################################################################
# WorkPoint Database settings

# Contains a comma separated list of data source names for the WorkPoint
# databases this server can connect to. (Required)
workpoint.database.list = WPDS

# This value tells WorkPoint if the database can support stored procedures
# or not. Using stored procedures where possible improves performance. Defaults
# to 'true' if not specified.
workpoint.stored.procedure = true

# If specified, this parameter sets the isolation level used for the
# WorkPoint database connection. The isolation level controls database
# locking. If not specified, the default isolation level in the database
# driver is used.
#
# 1 = READ_COMMITTED   - Non-repeatable reads and phantom reads can occur.
#                        Dirty reads are prevented.
# 2 = READ_UNCOMMITTED - Dirty reads, non-repeatable reads and phantom reads
#                        can occur.
# 4 = REPEATABLE_READ  - Phantom reads can occur. Dirty reads and
#                        non-repeatable reads are prevented.
# 8 = SERIALIZABLE     - Dirty reads, non-repeatable reads and phantom reads
#                        are prevented.
# database.isolation.level = 2


#############################################################################
# WorkPoint JAAS settings

# If specified and set to true, the server perform user authentication
# using JAAS. If this property is set to true, ClientContext.open will
# fail if the resourceID and password specified do not pass authentication.
# workpoint.user.authentication = false

# If specified, the login.authentication.module tells WorkPoint to search for
# the specified class in the application classpath, instantiate it and use
# it for JAAS authentication. NOTE that this property is intended as an alternative
# to configuring a JAAS Login Module from within the Application Server. As such,
# when specified it supercedes any JAAS Login Modules configured from within
# the Application Server for WorkPoint user authentication.
# *** NOTE *** The location of this class or the JAR that contains it should be added to the
# WorkPoint classpath via the workpoint.classpath.url entry in this properties file.
# See the WorkPoint installation documentation for details.
# *** NOTE *** This class must implement the javax.security.auth.spi.LoginModule interface.
# A code sample is provided at:
# [WORKPOINT_HOME]/src/com/workpoint/sample/security/WpLoginModule.java
#
# login.authentication.module = WpLoginModule

# If specified, the login.authorization.module tells WorkPoint to search for
# the specified class in the application classpath, instantiate it and use
# it for authorization of specific WorkPoint operations.
# *** NOTE *** The location of this class or the JAR that contains it should be added to the
# WorkPoint classpath via the workpoint.classpath.url entry in this properties file.
# See the WorkPoint installation documentation for details.
# *** NOTE *** This class must implement the com.workpoint.server.security.IwpAuthorize interface.
# A code sample is provided at:
# [WORKPOINT_HOME]/src/com/workpoint/sample/security/WpAuthorizationModule.java
#
# login.authorization.module = WpAuthorizationModule

# Defines whether or not the server should decrypt the password received from
# the client.
# workpoint.decrypt.password = false

# This property is only used if the property workpoint.encrypt.password = true.
# It specifies the key to used when decrypting the password received from the client.
# workpoint.encrypted.key = ff70fe2af430cc84d6c1b63e151f5e3f

# This property is only used if the property workpoint.encrypt.password = true.
# It defines whether or not the server should programmatically add the Sun JCE
# Provider. The Sun JCE provider contains the algorithms to encrypt and
# decrypt the password used when logging in to WorkPoint. The Sun JCE Provider
# should normally be specified in the java.security configuration file under
# JAVA_HOME. However, in cases where this is not possible, you can set this
# parameter to true and the Sun JCE Provider will be added programmatically.
# This parameter should only be set to true if client.EncryptPassword is also
# set to true and the Sun JCE provider can not be specified in the
# java.security configuration file.
workpoint.add.sun.provider = false

# Set this to the security role configured in the ejb-jar.xml. This value
# is used by the server to verify that the current user is part of this
# role when executing WorkPoint scripts.
workpoint.security.role = WorkPoint

# Set this property to true to view and change server properties and to view
# statistics in real time via JMX, if your application server supports JMX.
workpoint.server.enable.jmx = true

# The port to be used for generic JMX access to the WorkPoint Server.
# The console may then be accessed via the URL http://[HOST_NAME]:[PORT_NUM]
# For example, http://localhost:9092
# Comment out this property or set to -1 if no HTML Adaptor is desired.
# workpoint.server.jmx.html.adaptor.port = 9092

# Set this property to true to allow users to start and stop the monitors via the
# client API. Set to false to prevent this functionality from the client API.
# Note that this does not affect the Management Console operations, it only
# affects the com.workpoint.client.Monitor API calls. It is true by default.
# allow.monitor.control.from.client = false

# This is used by the server automated activity message-driven bean.
workpoint.serverAutoActivity.resourceID = WorkPoint

# This is used by Split Nodes when creating the child jobs. The creation
# of the child jobs is done in chunks to prevent transaction timeouts.
# This value tells the server how many child jobs to create per transaction.
# If you have complex jobs with long running scripts, you might have to
# decrease this number to prevent transaction timeouts. If you have simple jobs,
# you can try increasing this number to improve performance.
split.jobs.create.per.transaction = 25

# Specify any URLs here that the WorkPoint Server should add to its classpath.
# WorkPoint will search using this extended classpath for the following items:
# 1. Properties files such as GeneralMonitor.properties
# 2. Java programs invoked as WorkPoint Scripts.
# 3. Login and Authentication Modules specified by the login.authentication.module and
#    login.authorization.module entries in this properties file.
# Note: Locations specified here will be searched after any Application Server specified
# classpath locations. The url must start with 1 and increment by 1 (i.e.workpoint.classpath.url1)
# A directory must have a '/' at the end.
# Some examples might look like:
#workpoint.classpath.url1=file:///C:/WorkPoint/wp33/myPropertiesFileFolder/
#workpoint.classpath.url2=file:///C:/WorkPoint/wp33/mySrcDir/
#workpoint.classpath.url3=http://localhost/workPointScripts.jar
#workpoint.classpath.url4=file:///C:/WorkPoint/wp33/mySrcDir/myJavaApps.jar

# This URL tells the WorkPoint Server where the WorkPoint Gateway is located.
workpoint.gateway.url=http://localhost:8080/wpGateway/

# This URL tells the WorkPoint Managment Console where the WorkPoint documentation resides.
# The Management console contains a Documentation tab which will display the WorkPoint
# Documentation within the browser if this property is uncommented and set to a valid URL.
# For best results it is recommended that you deploy the wpDocs.war file and set this
# property to ../wpDocs
# workpoint.documentation.url = ../wpDocs

# Object versioning specifies whether WorkPoint should store previous versions of certain library
# objects that may be reverted to at a later date. This applies to the following libraries:
# Process, Script, Alert, Mail, Business Calendar and Holiday Calendar.
object.versioning=true

#############################################################################
# WorkPoint License settings

# License configuration file. If a filename or a file with a relative path is specified,
# the location will be assumed to be relative to the location of this file.
# The file may alternately be specified as a URL.
license.xml.filename = License.xml

# Specify the time of day in which WorkPoint will run a background task that
# performs a brief License check in the WorkPoint database.
license.check.time = 1:30 am

#############################################################################
# Logging / Tracing

# Determines the output file (or files) for logging information.
# log4j.category.com.workpoint=[FATAL|ERROR|WARN|INFO|DEBUG], appenderName, appenderName, ...

log4j.category.com.workpoint=WARN, WSSysOut, WSFile1
log4j.category.com.workpoint.comverse=DEBUG, WSSysOut, WSFile1

#log4j.category.com.workpoint.comverse.SetNoCancel =DEBUG, WSSysOut, WSFile1
#log4j.category.com.workpoint.comverse.GetKenanServiceDetails =DEBUG, WSSysOut, WSFile1
#log4j.category.com.workpoint.comverse.GetKenanAccountDetails =DEBUG, WSSysOut, WSFile1
#log4j.category.com.workpoint.comverse.GetKenanAddress =DEBUG, WSSysOut, WSFile1
#log4j.category.com.workpoint.comverse.GetKenanInventories =DEBUG, WSSysOut, WSFile1
#log4j.category.com.workpoint.comverse.GetKenanProducts =DEBUG, WSSysOut, WSFile1

log4j.additivity.com.workpoint=false
log4j.additivity.com.workpoint.comverse= false


# WSSysOut is set to be a FileAppender which outputs to System.out.
#
log4j.appender.WSSysOut=org.apache.log4j.ConsoleAppender
log4j.appender.WSSysOut.Target=System.out

log4j.appender.WSSysOut.layout=org.apache.log4j.PatternLayout
log4j.appender.WSSysOut.layout.ConversionPattern=%d [%t] %-5p %c %x - %m%n

# WSFile1 is set to be a RollingFileAppender which outputs to WorkPoint.log
# It will save the last 5 files and create a new file after the file size
# hits 1 Meg.
log4j.appender.WSFile1=org.apache.log4j.RollingFileAppender
log4j.appender.WSFile1.File=./servers/WorkPointServer/logs/WorkPointProcess.log
log4j.appender.WSFile1.MaxFileSize=1MB
log4j.appender.WSFile1.MaxBackupIndex=5

log4j.appender.WSFile1.layout=org.apache.log4j.PatternLayout
log4j.appender.WSFile1.layout.ConversionPattern=%d [%t] %-5p %c %x - %m%n


