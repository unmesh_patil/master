package com.cycle30.monitor.k3.businessObjects;
 
import com.cycle30.monitor.k3.util.monitorUtils;
import com.cycle30.monitor.k3.util.workpointContextObject;
import com.workpoint.client.ClientContext;
import com.workpoint.client.Job;
import com.workpoint.common.data.JobHistoryData;
import com.workpoint.common.data.JobNodeData;
import com.workpoint.common.data.TableDataList;
import com.workpoint.common.util.TableID;
import java.io.RandomAccessFile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Iterator;
import java.util.List;
import java.util.Date;

import com.workpoint.common.data.ActivityInstData;
import com.workpoint.common.data.table.JobNodeHistoryTable;
import java.util.TreeMap;


public class PollWorkPointJobStatus extends BusinessObject
{
    private class aNode {
        /*
         *                                     monitorUtils.writeFile(auditStream," Node ID : " + node.getJobNodeID()+", Node STATE ID : " + node.getNodeStateID()+", Node Type ID : " + node.getNodeTypeID()+", Node Name : " + node.getName() );
                                    System.out.println(" Node ID : " + node.getJobNodeID()+", Node STATE ID : " + node.getNodeStateID()+", Node Type ID : " + node.getNodeTypeID()+", Node Name : " + node.getName() +", lastUpdateDate: "+node.getLastUpdateDate()+", lastUpdateID: "+node.getLastUpdateID() );
                                    Date nodeUpdateDate =  node.getLastUpdateDate();
                                    long lastUpdate = nodeUpdateDate.getTime();
         */
        public JobNodeData  theNode;
        public long lastUpdate;
        public  String  iterationKey;
        public TableID theNodeId;
        public boolean isProcess = false;
        public short nodeIdType;
        public short nodeIdState;
        public String nodeName = "";
        public aNode parentNode;
        
        
        public aNode(){}
        
        public aNode(JobNodeData  aNode)    
        {
            this.theNode = aNode;
            this.lastUpdate  = aNode.getLastUpdateDate().getTime(); 
            iterationKey = ""+this.lastUpdate;
            theNodeId = aNode.getJobNodeID();
            nodeIdType = aNode.getNodeTypeID();
            nodeIdState = aNode.getNodeStateID();
            if (nodeIdType == 3)
                isProcess = true;
            if (aNode.getName() != null)
                nodeName = aNode.getName().toUpperCase();
        }
       
        
    }
    
    ClientContext context;

    java.util.List  processList;
    RandomAccessFile inputDataStream;
    RandomAccessFile auditStream;
    RandomAccessFile reportStream;
    String reportDir = "";
    String reportFileName = "";
    String processType = "";
    String errText = "";
    int totalListingsCount = 0;

    Properties appProperties;
    Properties monitorProperties;
    boolean debugOn = false;
    
    int wpScriptCountThreshold = 2;
    boolean isScriptCountAlertOn = false;
    boolean errorsEncountered = false;
    boolean donePolling = false;
    Connection arborConn = null;
    Connection c30API = null;
    String childProcNodeName = "";
    boolean updateChildProcNode = false;
    
    static final int SERVER_ID= 3;
    static String DELIMITER = ";";
    static char DELIMITER_CHAR = ';';
    static String ARG_SEPARATOR = ",";
    static final int ACTIVE_STATUS_ID = 2;
    static final int startPollingHour = 8;  //8 am
    static final int endPollingHour = 21;  // 9 pm
    static String crossDBJoinLablePlaceHolder = "^^";
    
  //  static String wpServiceOrder = "select oso.wp_job_id, oso.order_status_id, oo.order_status_id  from ord_service_order oso, ord_order oo  where oso.service_order_id = ?  and oso.order_id = oo.order_id";

    static String wpGetJobFailureNode = "SELECT FROM_PROCI_NODE_ID FROM (SELECT FROM_PROCI_NODE_ID, " + 
                "(SELECT NAME from WP_PROCI_NODE WHERE PROCI_NODE_ID = a.TO_PROCI_NODE_ID) TO_NODE " + 
                "FROM WP_TRANI a where PROCI_ID = ? " + 
                "AND tran_state_id <> 3) M " + 
                "WHERE TO_NODE = 'Failure'";
    
    static String singleWpJobSearch =  "select wp_job_id from c30_sdk_sub_order where wp_job_id = ? ";
    
  
    /**
    *  pollWorkPointJobs constructor method
    */
    public PollWorkPointJobStatus() throws Exception
    {      
      
            try{
            // establish the context connection to WP on the database of interest
            //System.out.println("Before getting the context.");
            context = workpointContextObject.getContext();
           // System.out.println("After getting the context.");
            }
            catch (Exception x)
            {
               x.printStackTrace();
               errorsEncountered = true;
               errText = x.getMessage();
               throw x;
            }
        
        }
 
 
 
    /**
    *  handleAction processes the request to delete inventory
    * @param actionDesc
    * @param accountObj
    * @param fxObj
    * @param param1
    * @param param2
    * @param param3
    * @param param4
    * @return error indicator - an empty string
    * @throws java.lang.Exception on an error in the FX action
    */
    public String handleAction(Object properties,Object processDesc, Object actionDesc, Object monitorProps,Object blockEmail, Object emailDest, Object param2, Object param3, Object param4, Object param5, Object param6) throws Exception
    {
     String retval = "";
    
     processType = (String)processDesc;
     appProperties = (Properties)properties;
     monitorProperties = (Properties)monitorProps;
     Boolean noEmail = Boolean.valueOf((String)blockEmail);
     String alertEmailAddr = (String)emailDest;  
      auditStream = (RandomAccessFile)param2;
      int nodeId = 0;  
       
      
      String debugInd = (String)appProperties.getProperty("monitor.debug");
      if (debugInd != null && debugInd.equalsIgnoreCase("ON"))
          debugOn = true;
        // get the wpjobid input
        String wpJobStr = (String)param3;
        String prociStrVal = wpJobStr.substring(0,wpJobStr.indexOf(":"));
        int prociVal = monitorUtils.StringToInt(prociStrVal);
        // get the childProcessNodeName input if sent
        childProcNodeName = (String)param4;
        if (childProcNodeName != null && childProcNodeName.trim().length() > 0)
           updateChildProcNode = true;
        
       
      try { 
       monitorUtils.writeFile(auditStream,"Opening wpMonitor db connections.");  
         
       c30API  =  monitorUtils.openDbConnection("C30API");
       c30API.setAutoCommit(true);
          
       // identify the node that caused the failure
       PreparedStatement ps = c30API.prepareStatement(wpGetJobFailureNode);
       ps.setInt(1, prociVal);
        
       monitorUtils.writeFile(auditStream,"Starting query to find node failure at "+(new java.util.Date().toString()));   
       ResultSet resultSet0  = ps.executeQuery();
       
        while(resultSet0.next()){
                 nodeId = resultSet0.getInt(1);
              monitorUtils.writeFile(auditStream,"fetched nodeId: "+nodeId); 
        }
        resultSet0.close();
        ps.close();   
        
        String targetWpJobId = "";  
        PreparedStatement ps1 = c30API.prepareStatement(singleWpJobSearch);
        // looking for only a single wp job then set parameters
        
        ps1.setString(1, wpJobStr);
            
        if (debugOn)
           monitorUtils.writeFile(auditStream,"Starting query to find wpjobId target. ");   
        ResultSet resultSet1  = ps1.executeQuery();
        // monitorUtils.writeFile(auditStream,"Returned from this query.");
          while (resultSet1.next()){
                  targetWpJobId = resultSet1.getString(1);
              monitorUtils.writeFile(auditStream,"fetched targetWpJobId: "+targetWpJobId); 
          }    
        resultSet1.close();
        ps1.close();      
    
        monitorUtils.writeFile(auditStream,"wpJobId: "+wpJobStr);
        monitorUtils.writeFile(auditStream,"queried targetWpJobId: "+targetWpJobId);    

        TableID atId = new TableID(wpJobStr );

        CheckJobStatus(atId,context,nodeId  );

    }
    catch (Exception e) {
            // save the transactions     
          if (auditStream != null)
              monitorUtils.writeFile(auditStream,"Error handling pollWorkPointJobs listings : "+e.getMessage() +"\n"); 
          
            errorsEncountered = true;
            errText = e.getMessage();
            throw e;
        }  
       finally {    
       
        if (c30API != null)  {
            try  { 
               c30API.close();
            } catch (SQLException se) {
                if (auditStream != null)
                   monitorUtils.writeFile(auditStream,"SQL error closing arbor connection: "+ se.getMessage() );                          
               
            }
        }
        if (arborConn != null)  {
            try  { 
               arborConn.close();
            } catch (SQLException se) {
                if (auditStream != null)
                   monitorUtils.writeFile(auditStream,"SQL error closing arbor connection: "+ se.getMessage() );                          
                throw new Exception ("SQL error closing arbor connection: "+ se.getMessage());
            }
        }
       
        if (context != null){
            
            try{
                context.clearCache();
                context.logout();
                context.close();
                
             } catch (Exception nex) {
                if (auditStream != null)
                   monitorUtils.writeFile(auditStream,"Workpoint error closing context: "+ nex.getMessage() );                          
                throw new Exception ("Workpoint error closing context: "+ nex.getMessage());
            }
         }
        
       
       }
        if (errorsEncountered) 
            if (errText != null && errText.trim().length() > 0)
             retval = errText;
        return retval;
    }
    
 
    /*
     * this method seeks out the node that caused the failure and resets it.
     */
    public int CheckJobStatus(TableID Jobid,ClientContext context,int objectId) 
    {
        boolean failureFound = false;
        TreeMap nodeHash = new TreeMap();
        String failureKey = "";
        TableID targetTableId;
        TableID thisTableId;

            try 
            {
                 if (debugOn)
                   monitorUtils.writeFile(auditStream,"Looking for the WP_JOB Trail");
                 targetTableId = new TableID(""+objectId+":WPDS");
                 // get the current job from the JobId
                 Job currentJob = Job.queryByID(context, Jobid , true)  ;
                
                 //get Node list
                 List nodeList = currentJob.getNodeList();

                 for (Iterator it = nodeList.iterator(); it.hasNext(); ) 
                    {
                        JobNodeData node =  (JobNodeData)it.next();
                        if (debugOn){  
                           System.out.println(" Node ID : " + node.getJobNodeID()+", Node STATE ID : " + node.getNodeStateID()+", Node Type ID : " + node.getNodeTypeID()+", Node Name : " + node.getName() );
                           monitorUtils.writeFile(auditStream," Node ID : " + node.getJobNodeID()+", Node STATE ID : " + node.getNodeStateID()+", Node Type ID : " + node.getNodeTypeID()+", Node Name : " + node.getName() );
                        }
                       thisTableId = node.getJobNodeID();
                        if (targetTableId.equals(thisTableId)){
                            monitorUtils.writeFile(auditStream,"Target Node ID : " + node.getJobNodeID()+", Node STATE ID : " + node.getNodeStateID()+", Node Type ID : " + node.getNodeTypeID()+", Node Name : " + node.getName() );
                           if (!updateChildProcNode){
                              currentJob.resetFromNode(node);
                              return 0;
                           }   
                        }
                        short nodeStateId = node.getNodeStateID();
                        short nodeTypeId = node.getNodeTypeID();
                        
                        if ( (nodeStateId == 2 || nodeStateId == 3 || nodeStateId == 5 ) &&
                             (nodeTypeId == 2 || nodeTypeId == 3 || nodeTypeId == 7 )  )
                        {    
                                    ActivityInstData actI = node.getActivity();
                                    if (debugOn){
                                       monitorUtils.writeFile(auditStream," Node ID : " + node.getJobNodeID()+", Node STATE ID : " + node.getNodeStateID()+", Node Type ID : " + node.getNodeTypeID()+", Node Name : " + node.getName() );
                                       System.out.println(" Node ID : " + node.getJobNodeID()+", Node STATE ID : " + node.getNodeStateID()+", Node Type ID : " + node.getNodeTypeID()+", Node Name : " + node.getName() +", lastUpdateDate: "+node.getLastUpdateDate()+", lastUpdateID: "+node.getLastUpdateID() );
                                    }
                                    Date nodeUpdateDate =  node.getLastUpdateDate();
                                    // establish the time of last update
                                    long lastUpdate = nodeUpdateDate.getTime();
                                    // save node data for later use
                                    aNode thisNode = new aNode(node);
                                    // insert into sorted collection by last update
                                     nodeHash.put(""+lastUpdate,thisNode);
                                     if ( node.getName() != null &&  node.getName().toUpperCase().equals("FAILURE")){
                                        failureFound = true;
                                        failureKey = ""+lastUpdate;
                                     }
                                     node.getHistoryList();
                                    // try to start from the childjob rather than the currentJob
                                    TableDataList nodeHistory = node.getHistoryList();
                                    int nodeHistSize = nodeHistory.size();
                                    JobNodeHistoryTable jnhLast = (JobNodeHistoryTable)nodeHistory.get(nodeHistSize - 1);
                                    String nodeComments = jnhLast.getOptionalComments();
                                    if (debugOn && nodeComments != null)
                                         System.out.println(nodeComments);

                                    if ( node.getChildJobID() != null)
                                    {
                                            Job childjob = Job.queryByID(context, node.getChildJobID(), true)  ;
                                            Date subProcUpdateDate = childjob.getLastUpdateDate();
                                            //get child Node list
                                            if (debugOn){
                                               monitorUtils.writeFile(auditStream,"  CHILD JOB ID : " + childjob.getJobID() );
                                               System.out.println("  CHILD JOB ID : " + childjob.getJobID() +", lastUpdateDate: "+childjob.getLastUpdateDate()+", lastUpdateId: "+childjob.getLastUpdateID());
                                            }
                                            // Let's see if we have to re-start a subprocess child node
                                            List childnodeList = childjob.getNodeList();
                                            
                                            for ( Iterator it1= childnodeList.iterator() ; it1.hasNext();)
                                            {
                                                    JobNodeData childnode =  (JobNodeData)it1.next();
                                                    thisTableId = childnode.getJobNodeID();
                                                    if (debugOn){
                                                       if (targetTableId.equals(thisTableId))
                                                                System.out.println("We found our target in the mix, a childnode.");
                                                       monitorUtils.writeFile(auditStream,"   childnode NodeStateId : "+ childnode.getNodeStateID()+", nodeTypeID = "+childnode.getNodeTypeID()+", name: "+childnode.getName() ) ;     
                                                       System.out.println("    childnode Id: "+childnode.getJobNodeID()+" ,childnode NodeStateId : "+ childnode.getNodeStateID()+", nodeTypeID = "+childnode.getNodeTypeID()+", name: "+childnode.getName() ) ;  
                                                    }
                                                    if (updateChildProcNode && childProcNodeName.trim().equals(childnode.getName().trim()) ) 
                                                    {
                                                         if (debugOn)
                                                          System.out.println("We found our SubProcess target in the mix, a childnode.");
                                                         monitorUtils.writeFile(auditStream,"resetting   childnode NodeStateId : "+ childnode.getNodeStateID()+", nodeTypeID = "+childnode.getNodeTypeID()+", name: "+childnode.getName() ) ;     
                                                         childjob.resetFromNode(childnode);
                                                         return 0;
                                                    }
                                                         if (debugOn){
                                                            // try to start from the childjob rather than the currentJob
                                                            TableDataList jobHistory = childjob.getHistoryList();
                                                            int histSize = jobHistory.size();
                                                            JobHistoryData jhLast = (JobHistoryData)jobHistory.get(histSize - 1);
                                                            String comments = jhLast.getOptionalComments();
                                                           
                                                            System.out.println(comments);
                                                          }
                                              } // end for                  
                                    }
                                        
                         } // node state of interest
                                
                    }
                
               return 0;
             }
            catch(Exception e )
            {
               
                      e.printStackTrace();
                      try {
                      monitorUtils.writeFile(auditStream,"exception : "+ e.getMessage() ) ;     
                      } catch(Exception ex){}
                  
                    return 1;
            }

    }
    
  
    
  
    
    /**
     * method sends an email alert message 
     * @param emailDest
     * @param filename
     * @throws Exception
     */
    public void sendAlertEmail(String emailDest, String filename) throws Exception
    {
        String EMAIL_SOURCE =  (String) monitorProperties.get("monitor.email_source");
        String EMAIL_USER_ID = (String) monitorProperties.get("monitor.email_userId");
        String EMAIL_DOMAIN = (String) monitorProperties.get("monitor.email_domain");
        String EMAIL_PASSWORD = (String) monitorProperties.get("monitor.email_password");
        String attachments[] = filename.split(ARG_SEPARATOR);
        String sendTo[] =  emailDest.split(ARG_SEPARATOR);
        String EMAIL_SALUTATION = "WorkPoint Polling Event "; 
        String announcement = "Some unforseen event has happened.  See possible details.";
        if (errText.trim().length() > 0 )
            announcement = errText;
                                          
        if (isScriptCountAlertOn){
            EMAIL_SALUTATION = "WorkPoint Script Count Alert ";
            announcement = "ALERT WP_SCRIPT_MONITOR Count > "+wpScriptCountThreshold;
            if (errText.trim().length() > 0 )
                announcement = announcement + ". "+errText;
        }
       monitorUtils.sendEmailNotification( EMAIL_DOMAIN,
                                           EMAIL_SOURCE,
                                           sendTo,
                                           EMAIL_SALUTATION,
                                           announcement,
                                           attachments );
     
    }// end sendAlertEmail
    
}