package com.cycle30.monitor.k3;

import com.cycle30.monitor.k3.businessObjects.BusinessObject;
import com.cycle30.monitor.k3.util.monitorUtils;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.*;

/**
 * this is the driver for polling for WorkPoint jobs in trouble
 *
 * @author  
 */
public class MON_WP extends Driverclass{
 
    public  static Properties monitorProperties;
    private static String logDir = "";
    private static String dirSeperator = "\\";
    private static RandomAccessFile auditStream = null;
    private static String MONITOR_PROPERTIES_FILE_NAME = "supportmonitor";
    private static boolean standalone = false;
    private static String returnVal = "";
    private static String emailDestination = "";
    private static String auditFileName = "";
    private static boolean windowsOS = false;
    private static String prodEnvInd = "";
    private static final String wpPollTempFile = "wpLock";
    // the non-stand alone app should not reach 6 errors during the day
    // if so then an email alert should be sent.
    public static int errorCount = 0;
    public static final int errorThreshold = 6;
 
    /**
     * @param args the command line arguments
     */
    public  void main(Object obj)  throws Exception {
        
        standalone = false;
        String[] arg = (String[])obj;
        main(arg);
    
    }
    
    public static void main(String[] args) throws Exception {
        BusinessObject wpPoll = null;
        String wplockDir = "";
        String wpJobIdStr = "";
        String wpChildNodeName = "";
        boolean haveTheBaton = false;
       // System.out.println("Here we are.");
        try{
              // System.out.println("executing wpMonitorDriver ");
            
            if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") == -1) 
                       dirSeperator = "/";
               else {
                       dirSeperator = "\\\\";
                       windowsOS = true;
            }
            
               String configDir = System.getProperty("configdir");
               if (configDir == null || configDir.trim().length() == 0)
               {
                     System.out.println("EA9-Problem accessing batch system configuration file ");
                     System.exit(1);
               }
               String homeDir = System.getProperty("homedir");
               if (homeDir == null || homeDir.trim().length() == 0)
               {
                     System.out.println("EA9-Problem accessing batch system enviroment variable homedir ");
                     System.exit(1);
               }
               //System.out.println("loading the monitorProperties file.");
               monitorProperties =  monitorUtils.getResourceProperties(configDir+dirSeperator+MONITOR_PROPERTIES_FILE_NAME+".properties"); 
               if (monitorProperties.isEmpty()){
                   Exception nex = new Exception(MONITOR_PROPERTIES_FILE_NAME+".properties file is missing.");
                   System.out.println(MONITOR_PROPERTIES_FILE_NAME+".properties file is missing.");
                   throw nex;
               }
               prodEnvInd = monitorProperties.getProperty("monitor.prod_env");
              // if (!prodEnvInd.equalsIgnoreCase("YES"))
              //     return;
               logDir = args[0];
               // lets make the assumption that if there is more than one argument that it is 
               // a service order id
               if (args.length > 1)
               {
                   wpJobIdStr = args[1];
                   // this should be a wpJobId
                   if (wpJobIdStr.toUpperCase().indexOf(":WPDS") < 0){
                        System.out.println("A badly formatted parameter was found. Expecting a WorkPoint Job Id.");
                       System.exit(1);
                   }
                   if (args.length > 2)
                   {
                      wpChildNodeName = args[2];
                   }
                
                   standalone = true;
               }
               monitorProperties.setProperty("monitor.output.logdir",logDir);
               monitorProperties.setProperty("monitor.output.dirSeperator",dirSeperator);
               monitorProperties.setProperty("monitor.homedir",homeDir);
               emailDestination = (String)monitorProperties.getProperty("monitor.workpoint.emailDestination");
               // test to see if another process has the baton by trying to create a temp directory
               wplockDir = logDir+dirSeperator+wpPollTempFile;
         
               errorCount = 0; 
               
               monitorUtils.initializeMonitorUtils(monitorProperties);     
               auditFileName   = logDir+dirSeperator+"C30_WP_MONITOR"+monitorUtils.todaysDateYYYYMMDD();   
               File f = new File(auditFileName);
               long fileLength = f.length();
               auditStream = new RandomAccessFile(f, "rw");
               auditStream.seek(fileLength);
               Date today = new Date();
                if (!standalone)
                  monitorUtils.writeFile(auditStream,"WORKPOINT POLLING started: "+today.toString());
               else
                  monitorUtils.writeFile(auditStream,"WORKPOINT POLLING started: "+today.toString()+" for WpJobId "+wpJobIdStr);
             
               wpPoll = (BusinessObject)Class.forName("com.cycle30.monitor.k3.businessObjects.PollWorkPointJobStatus").newInstance();
               
               // open the log file for today and append to the end 
               if (wpPoll == null) {
                   System.out.println("Unable to instantiate the pollWorkPointJobs class. Exiting.");
                   Exception ex = new Exception("Unable to instantiate the pollWorkPointJobs class. Exiting.");
                   throw ex;
                
               }
               String emailFlag = new Boolean(windowsOS).toString();
               returnVal =  wpPoll. handleAction(monitorProperties,"WP_POLL", "MAINTENANCE",monitorProperties,emailFlag,emailDestination, auditStream, wpJobIdStr, wpChildNodeName, null, null);
               monitorUtils.writeFile(auditStream,"WORKPOINT POLLING done.\n");
       
           }
        catch( Exception ex)
        { 
           
           System.out.println("Exception caught by MON_WP.");
           System.out.println(ex.getMessage());
            if ( auditStream != null)
               auditStream.close();
            if (standalone)
            {
                System.out.println(ex.getMessage());
                System.exit(1);
            }
        }
        finally {  
               System.out.println("MON_WP done. Exiting.");
             
             try  { 
                 
                 if ( auditStream != null)
                    auditStream.close();
                 // send an email alert for errors occurring in non-Windows environments
                 if (returnVal.trim().length() > 0 && !windowsOS && wpPoll != null)
                     wpPoll.sendAlertEmail(emailDestination, auditFileName);   
                     
             } catch (Exception se) {}
                 
         }
        if (standalone){
           System.out.println("Exiting WorkPoint Monitor: Check your WorkPoint Job.");
            System.exit(0);
        }
    }
    
}
