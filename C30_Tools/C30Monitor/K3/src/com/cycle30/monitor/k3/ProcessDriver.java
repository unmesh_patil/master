package com.cycle30.monitor.k3;

import java.util.Date;
import java.util.Properties;
import com.cycle30.monitor.k3.util.monitorUtils;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * this is the driver for calling other driver programs for the monitoring applications
 *  assumption is that there are 3 command-line arguments:  logdir  processType  inputFilename
 *  inputFilename is just the file name, not the path.  The path is defined by the properties file so that
 *  a the input file can be moved to an done directory, also specified in the properties file.
 *  The properties file is the defined properties file for the CollectionsBatch application.
 * @author
 */
public class ProcessDriver {
    
    
    private static boolean sentinelOn = false;
     private static long sleepInterval = 0;
     private static int endHour = 0;
     private static String startDOM = "";
     private static String dirSeperator;
     final private static String PROCESS_DELIMITER = ";";
     final private static String ARG_SEPARATOR = ",";
     private static Properties monitorProperties  = new Properties();
     private static String PROPERTIES_FILE_NAME = "supportmonitor";
     private static boolean windowsOS = false;

    

     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Driverclass dc = null;
         Object odc = null; // = (Object)c.newInstance();
        // to accommodate the old modules while new modules being developed
        Class c = null;
        
        try{
               String os = System.getProperty("os.name");
               if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") == -1) 
                       dirSeperator = "/";
               else {
                      dirSeperator = "\\\\";
                      windowsOS = true;
               }
          
               String configDir = System.getProperty("configdir");   
                  if (configDir == null || configDir.trim().length() == 0)
                  {
                         System.out.println("EA9-Problem accessing Process Monitor system configuration file ");
                         System.exit(1);
                  }
                  String homeDir = System.getProperty("homedir");  
                  if (homeDir == null || homeDir.trim().length() == 0)
                  {
                         System.out.println("EA9-Problem accessing Process Monitor system enviroment variable homedir ");
                         System.exit(1);
                  }
                  String sentinelValue = System.getProperty("sentinel");  
                  if (sentinelValue != null && sentinelValue.trim().length() > 0 && sentinelValue.trim().equals("1"))
                     sentinelOn = true;
                  String sleepTime = System.getProperty("sleep");   
                  System.out.println("sleep parameter sent: "+sleepTime);
                  //calculate sleep time if specified
                  if (sleepTime != null && sleepTime.trim().length() > 0 )
                      // is numeric, then assume time specified is in minutes
                     if (monitorUtils.isInteger( sleepTime ))
                       sleepInterval =  60000 * Long.parseLong(sleepTime);
                  
                  // establish start day of Month     
                  Date startDate = new Date();
                  String[] startDateArray = startDate.toString().split(" ");
                  startDOM = startDateArray[2];
                  // see if end hour (24 hour) specified: 
                  String doneHour = System.getProperty("endHH"); 
                  if (doneHour != null && doneHour.trim().length() > 0)
                      // is numeric, then assume time specified is in minutes
                     if (monitorUtils.isInteger( doneHour )){
                        endHour = Integer.parseInt(doneHour);
                        if (endHour < 0 || endHour > 23)
                            endHour = 0;
                     }
                     

           
               if (args.length < 1)
               {
                  System.out.println("Not enough command line arguments specified.");
                  System.out.println("Usage: logDirectory <processName> <inputFileName>");
                  System.exit(1);
               }
            // check to see if we are running an individual process or in sentinel mode
            
                if (args.length > 1){
                    String processType =  args[1];
                    String newArgs [] = new String [args.length-1];
                    for (int j = 0; j < args.length; j++){
                        if (j == 0)
                            newArgs[j] = args[j];
                        else
                            if (j > 1)
                                newArgs[j-1] = args[j];
                        
                    }
                  
                   processType = processType.toUpperCase().trim();                   
                    try{
                       c = Class.forName("com.c30.monitor.k2.MON_"+processType);
                         odc = c.newInstance();  
                     } 
                    catch (NoClassDefFoundError de ){
                          dc = (Driverclass)Class.forName("com.cycle30.monitor.k2.MON_"+processType).newInstance();
                    }  
                    catch (ClassNotFoundException  clnfex)  {
                                // this gets invoked incase the old module or old ProcessMonitor jar is no longer there.
                                dc = (Driverclass)Class.forName("com.cycle30.monitor.k2.MON_"+processType).newInstance();
                  }
                    
                
                    while (true){
                        if (odc != null){
                            try {   
                            // get access to the main method of the old version of the module
                          /*      Sadly this works with a later version of java
                            Class[] Str = new Class[] {String.class};
                            Method mainMethod = c.getMethod("main",String[].class); 
                            String[] arguments = new String[args.length-1];
                            for (int j = 0; j < newArgs.length; j++){
                                //arguments[j] = newArgs[j];
                                if (j == 0)
                                    arguments[j] = newArgs[j];
                                else
                                    if (j > 1)
                                        arguments[j-1] = newArgs[j];
                                
                            }
                                                 
                            mainMethod.invoke(odc,(Object)arguments) ;  
                            */
                           /////////  trying with for older version compatibility /////////////
                           Method targetMethod = null;     
                           Method[] allMethods = c.getDeclaredMethods();
                               
                          // for (Method m : allMethods) {
                            for (int i = 0; i < allMethods.length; i++)  {  
                                  Method m = (Method)allMethods[i];
                                  String mname = m.getName();
                                  if (mname.toUpperCase().equals("MAIN")){
                                    Type[] pType = m.getParameterTypes();
                                     if (pType.length == 1){
                                      String className =  pType[0].toString();
                                       if (className.indexOf("java.lang.Object") > 0 )
                                            targetMethod = m;
                                       }  
                               }
                            
                                         
                            }  
                            
                             //   Method mainMethod = c.getMethod("main",Object.class); 
                                String[] arguments = new String[args.length-1];
                                for (int j = 0; j < newArgs.length; j++){
                                    arguments[j] = newArgs[j];
                                }
                                Object [] objlist = new Object[] {arguments};                     
                                targetMethod.invoke(odc,objlist) ;  
                                
               
                            } catch (NoClassDefFoundError  iex)  {
                                // this gets invoked incase the old module or old ProcessMonitor jar is no longer there.
                                dc = (Driverclass)Class.forName("com.cycle30.monitor.k2.MON_"+processType).newInstance();
                                dc.main(newArgs);
                            }
                             catch (Exception  clnfex)  {
                                // this gets invoked incase the old module or old ProcessMonitor jar is no longer there.
                                dc = (Driverclass)Class.forName("com.cycle30.monitor.k2.MON_"+processType).newInstance();
                                dc.main(newArgs);
                            }
                        }
                       else
                         dc.main(newArgs);
                        // check to see if we run these processes again.
                         if (sentinelOn && readyToRun(startDOM,endHour)){
                           System.out.println("going to sleep");
                           Thread.sleep(sleepInterval);
                         }
                        else
                             break;

                    }// end of while sentinel still running
                    
                    System.exit(0); 
                }// end of individual process run - in sentinal or run once mode
                
            // Here is where we start the sentinel looping processes
            // first find the monitor properties file.
            monitorProperties = monitorUtils.getResourceProperties(configDir+dirSeperator+PROPERTIES_FILE_NAME+".properties"); 
            if (monitorProperties.isEmpty()){
                Exception nex = new Exception("Process Monitor properties file is missing.");
                throw nex;
            }
            
            // now we figure out the proceses
            System.out.println("sleepInterval in milliseconds: "+sleepInterval+" (1 second = 1000 milliseconds)");
            String processList = monitorProperties.getProperty("monitor.process_list");
            String[] processArray = processList.split(PROCESS_DELIMITER);
            int processCount = 0;
            if (processArray.length > 0)
                while (true){
                    processCount++;
                    Date processStartTime = new Date();
                    System.out.println("Process start time: "+processStartTime.toString());
                    for    (int i = 0; i < processArray.length; i++){
                        String processType = processArray[i];
                       
                          c = Class.forName(processType);
                          odc = c.newInstance();  
                        
                         
                          /////////  trying with for older version compatibility /////////////
                          Method targetMethod = null;     
                          Method[] allMethods = c.getDeclaredMethods();
                              
                         // for (Method m : allMethods) {
                           for (int j = 0; j < allMethods.length; j++)  {  
                                 Method m = (Method)allMethods[j];
                                 String mname = m.getName();
                                 if (mname.toUpperCase().equals("MAIN")){
                                   Type[] pType = m.getParameterTypes();
                                    if (pType.length == 1){
                                     String className =  pType[0].toString();
                                      if (className.indexOf("java.lang.Object") > 0 )
                                           targetMethod = m;
                                      }  
                              }
                           
                            System.out.println("Method name: "+mname);  
                                        
                           }  

                               Object [] objlist = new Object[] {args};                     
                               targetMethod.invoke(odc,objlist) ;  
                   }
        
                   // check to see if we run these processes again.
                   if (sentinelOn && readyToRun(startDOM,endHour))
                      Thread.sleep(sleepInterval);
                   else
                        break;

               }// end of while sentinel still running
        
           }
        catch( Exception ex)
         {  System.out.println("Exception caught in ProcessDriver: "+ex.getMessage());
              
                try  { 
                    System.out.println("Trying to send email alert from ProcessDriver");
                    if (!windowsOS){
                        String EMAIL_SOURCE =  (String) monitorProperties.get("monitor.email_source");
                        String EMAIL_DESTINATION = (String) monitorProperties.get("monitor.email_alert_destination"); 
                        String DOMAIN_NAME =   (String) monitorProperties.getProperty("monitor.email_domain");
                        String EMAIL_SALUTATION = "K1 ProcessMonitor Exception ";   
                        String attachments[] = new String[0];
                        String sendTo[] =  EMAIL_DESTINATION.split(ARG_SEPARATOR);
                        String msgText = ex.getMessage().trim();
                    monitorUtils.sendEmailNotification( DOMAIN_NAME,
                                                          EMAIL_SOURCE,
                                                          sendTo,
                                                          EMAIL_SALUTATION,
                                                          msgText,
                                                         attachments);
                        
                    }
                    
                } catch (Exception se) {
                        System.out.println("Exception caught trying to send email alert in ProcessDriver: "+se.getMessage());
                        
                 }
           System.exit(1);
        }
        System.exit(0);
    }
    
    
    
    

    public static boolean readyToRun(String startDOM,int endPollingHour)
    {
        boolean retval = false;
        
        // get elements of today-now date
         Date today = new Date();
         String[] mydateArray = today.toString().split(" ");
         String DOW = mydateArray[0];
         String MOY = mydateArray[1];
         String DOM = mydateArray[2];
         String TOD = mydateArray[3];
         String TZONE = mydateArray[4];
         String YEAR = mydateArray[5];
         String [] TodArray = TOD.split(":");
         String HH = TodArray[0];
         String mm =  TodArray[1];
         String ss = TodArray[2];
         
         // immeditately determine if this is the time of day that we can process
         int intHH = monitorUtils.StringToInt(HH);
     
          // if using this method to check general processing availability,
         // then the record will be null
         if (!startDOM.equals(DOM))
           return false;
         if (intHH > endPollingHour)
              return false;
          
          return true;
        
    }
    
}