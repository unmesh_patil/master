package com.cycle30.monitor.k3.businessObjects;

import java.io.RandomAccessFile;
import java.util.Properties;
import com.cycle30.monitor.k3.util.monitorUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * messageAccount class creates a CMF message record for the Account using the 
 * KenanFX object connection and parameters passed as stored in the BatchFlow instance 
 */
public class execRuntimeCmd extends BusinessObject
{

    static RandomAccessFile auditStream;
    static String processType = "";
    static Properties appProperties;
   
  /**
   * constructor method
   */
  public execRuntimeCmd(){}
   
  /**
   *  handleAction executes runtime commands by starting a shell process
   *  HUGE ASSUMPTION: This process is executed from the directory in which the 
   *  process is started so all commands will be executed from that directory and 
   *  all references to scripts must either assume the starting directory or be fully qualified
   *  The commands are sent to the module via param1, stored in the BatchFlow entry.
   *  
   * @param actionDesc
   * @param accountObj
   * @param fxObj
   * @param param1
   * @param param2
   * @param param3
   * @param param4
   * @return error indicator - an empty string
   * @throws java.lang.Exception on an error in the FX action
   */
  public String handleAction(Object properties,Object processDesc, Object actionDesc, Object accountObj,Object fxObj, Object param1, Object param2, Object param3, Object param4, Object param5, Object param6) throws Exception
  {
    String retval = "";
    String dirSeperator = "";
   
    // retrieve the relevant value pairs
    String scriptToRun = (String)param1;
    
    
     if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") == -1) 
              dirSeperator = "/";
      else 
              dirSeperator = "\\\\";
              
    String userHome = System.getProperty("user.home");
    
    System.out.println("Entering execRuntimeCommand.handleAction");
    if (scriptToRun == null || scriptToRun.trim().length() == 0)
    {
        System.out.println("No script specified. Exiting execRuntimeCommand");
        return retval;
    }         
    
    if (userHome == null || userHome.trim().length() == 0)
    {
         System.out.println("No user.home System property found to build path for script. Exiting execRuntimeCommand");
        return retval;
    } 

      try
     {
         System.out.println("executing this script: " +userHome+dirSeperator+scriptToRun);
         String cmd = userHome+dirSeperator+scriptToRun;  // this is the command to execute in the Unix shell
         ProcessBuilder pb = new ProcessBuilder(cmd);
         pb.redirectErrorStream(true); // use this to capture messages sent to stderr
         Process shell = pb.start();
         InputStream shellIn = shell.getInputStream(); // this captures the output from the command
         int shellExitStatus = shell.waitFor(); // wait for the shell to finish and get the return code
         System.out.println("exit status from the shell: " +shellExitStatus);
          if (shellExitStatus == 1){
              Exception nex = new Exception("execRuntimeCmd: Exit status from the shell command is " +shellExitStatus);
              throw nex;
              
          }
         // at this point you can process the output issued by the command
         // for instance, this reads the output and writes it to System.out:
         
 
     }
    catch (Exception e)
    {
      
     
        
         System.out.println("Error execRuntimeCommand: "+e.getMessage() );                          
       throw e;
    }
    
    
   System.out.println("execRuntimeCommand: leaving handleAction method");
     return retval;
     
 }
 

}