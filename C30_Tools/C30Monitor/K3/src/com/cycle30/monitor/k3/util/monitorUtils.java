package com.cycle30.monitor.k3.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.net.URI;
import java.util.*;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import javax.mail.MessagingException;

import javax.security.auth.Subject;

public class monitorUtils 
{

 static String addrUsedStr = "";

  private static final String DELIMITER = ","; //regexp for tokenizing
  private static String CONNECT_DELIMITER = ",";
  private final static byte[] key = {124, 97, -124, -76, 35, -22, -127, -42, -97, -42, 40, 121, 84, 25, 79, 38};

 
  public static String ArborDbUser;
  public static String ArborDbPassword;
  public static String ArborDbURL;

  
  
  public static String c30APIDbUser;
  public static String c30APIDbPassword;
  public static String c30APIDbURL;
  
   
 

  public static void initializeMonitorUtils(Properties appProperties) throws Exception
  {
    String arborConnectString = (String)appProperties.getProperty("monitor.arbor_connect_string");
    
    String C30APIConnectString = (String)appProperties.getProperty("monitor.bill_connect_string");

    
    String[] arborConnectArray = arborConnectString.split(DELIMITER);
      
     ArborDbURL = arborConnectArray[0];
     ArborDbUser = arborConnectArray[1];
     ArborDbPassword = arborConnectArray[2]; 
     ArborDbPassword = decryptPassword(ArborDbPassword);
     
     
      String[] C30APIConnectArray = C30APIConnectString.split(DELIMITER);
        
       c30APIDbURL = C30APIConnectArray[0];
       c30APIDbUser = C30APIConnectArray[1];
       c30APIDbPassword = C30APIConnectArray[2];   // need to decrypt
        c30APIDbPassword = decryptPassword(c30APIDbPassword);
       
      

 
  }



	
   public static String strReplace(String targetData,String searchStr,String replaceStr){
		String retval = targetData.trim();
		String temp1 = "";
		String temp2 = "";
		int replaceLen = replaceStr.length();
		int strIndx = 0;
		
		if (retval.equals(searchStr.trim()))
			return replaceStr;
		
		strIndx = retval.indexOf(searchStr);
		while (strIndx >= 0){
			temp1  = retval.substring(0,strIndx);
			temp2 = retval.substring(strIndx + searchStr.length(),retval.length());
			if (replaceLen == 0)
				retval = temp1 + temp2;
			else
				retval = temp1+replaceStr+temp2;
			
			strIndx = retval.indexOf(searchStr);
		}
		return retval;
	}

 

    public static Connection openDbConnection(String dbName) throws Exception, SQLException
 {
    //  System.out.println("Request to connect to database: "+dbName) ;
      Connection conn = null;     
      if (dbName.equals("C30API")){
          conn = jdbcConnect (c30APIDbUser,c30APIDbPassword,c30APIDbURL);
          return conn;

      } 
          
      if (dbName.equals("ARBOR")){
          conn = jdbcConnect (ArborDbUser,ArborDbPassword,ArborDbURL);
          return conn;
      }
     
      return conn;   
 }
  /**
   * this method creates a connection to the database
   * @return java.sql.Connection
   * @throws java.lang.Exception
   * @throws java.sql.SQLException
   */
  public static java.sql.Connection jdbcConnect (String dbUser,String dbPassword,String dbURL) throws Exception,SQLException
 {
   
            Class.forName("oracle.jdbc.driver.OracleDriver");
            // Establish the connection to the database.  
            return ( DriverManager.getConnection(dbURL,dbUser,dbPassword));   
 }


 public static String generateNextReqKey()
 {
     
     SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMddHHmmss0SSSS");
		String ss = formatter.format(new java.util.Date());
		while (ss.equals(formatter.format(new java.util.Date()))) {}
		return formatter.format(new java.util.Date());
   
 }


   public static String todaysDate()
 {
     
     SimpleDateFormat formatter = new SimpleDateFormat ("MM/dd/yyyy");
		return formatter.format(new java.util.Date());
   
 }   
 

	public static String parseStr = "";
	
	public static String parseit(String source, char delim){
		String target = "";
		int startpos = parseStr.length();
		
		for (int i = startpos; i< source.length(); i++){
			parseStr = parseStr + source.substring(i,i+1);
			if (source.charAt(i) != delim )
				target = target + source.substring(i,i+1);
			else
				break;
		}
			return target;
		
	}
	
  
	public static String delimitData(String theData,String theChar){
		
		String replacementText = "";
		if (theData.lastIndexOf(theChar) == -1)
			return theData;
		// replace all spaces with underscores
		for (int ii = 0; ii < theData.length(); ii++){
			if (theData.charAt(ii) == theChar.charAt(0))
				replacementText = replacementText + theChar+theChar;
			else
				replacementText = replacementText + theData.charAt(ii);
						
		}
		return replacementText;
	}
	
	public static String padString(String aStrValue,int padToLen,String fillChar){
		String strVal = aStrValue;
		int fillerWidth = fillChar.length();
		int fillLen = padToLen - strVal.length();
		if (fillLen <= 0)
			return strVal;
		if (fillerWidth + strVal.length() > padToLen)
			return strVal;
		while (strVal.length() < padToLen)
			strVal = fillChar + strVal;
		return strVal;
		
	}
        
    public static String replace(String str, String pattern, String replace) {
          int s = 0;
          int e = 0;
          StringBuffer result = new StringBuffer();
      
          while ((e = str.indexOf(pattern, s)) >= 0) {
              result.append(str.substring(s, e));
              result.append(replace);
              s = e+pattern.length();
          }
          result.append(str.substring(s));
          return result.toString();
      }



    /**
     * This method assumes that a connection string is a comma seperated list 
     * of values, the last of which is the connection password to a database.
     * This password is assumed to be encrypted and in need of decryption.
     * The method will parse out the passward, decrypt it and put the re-construct the
     * property value returning the new connection string.
     * @param key
     * @param value
     */
    private static String decryptConnectionPasswords (String connecStr) throws Exception 
    {
      String retval = connecStr;
        String[] ConnectArray = ((String)connecStr).split(CONNECT_DELIMITER);
        String url = ConnectArray[0];
        String dbUser = ConnectArray[1];
        String dbPassword = ConnectArray[2];
        String decipheredPwd = decryptPassword(dbPassword);
        //log.debug("Decrypting db password for db: "+url);
        retval = url+CONNECT_DELIMITER+dbUser+CONNECT_DELIMITER+decipheredPwd;
      return retval;  
    }
    
    
    //----------------------------------------------------------------------------
    /* Decrypt db password stored in Advanced Encryption Standard (AES) format.
     * 
     */
    private static String decryptPassword(String pwd) throws Exception {
        

        String decryptedPwd = "";
        byte[] decrypted = null;
        
        // decrypt the password.
        try {
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(pwd);
     
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
     
            // Instantiate the cipher and decrypt
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
     
            decrypted = cipher.doFinal(dec);     
        } 
        catch (Exception e) {
            throw new Exception ("Error decrypting password: ", e);
        }
            
        // Convert to string
        StringBuffer sb = new StringBuffer();
        
        for (int i=0; i<decrypted.length; i++) {
           sb.append((char)decrypted[i]);
        }

        decryptedPwd = sb.toString();
        
        return decryptedPwd;

    }
    
    public static String generateNextId()
    {
        
       SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMddHHmmss0SSSS");
                   String ss = formatter.format(new java.util.Date());
                   while (ss.equals(formatter.format(new java.util.Date()))) {}
                   return formatter.format(new java.util.Date());
      
    }
    
    public static String todaysDateYYYYMMDD()
     {
         
         SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
                    return formatter.format(new java.util.Date());
       
     }   


    public static int StringToInt (String aStr){
            int result = 0;
            int integer_value = 0;
            boolean negative = false;
            if (aStr != null)
                    for (int i = 0; i < aStr.length() 
                                                    && (aStr.charAt(i) == '-' || (aStr.charAt(i) >= '0' && aStr.charAt(i) <= '9'))
                                                    && aStr.charAt(i) != ' '; i++){
                            if (aStr.charAt(i) == '-')
                                    if (i == 0)
                                            negative = true;
                                    else
                                            return 0;
                            else
                            {
                                    integer_value = aStr.charAt(i) - '0';
                                    result = result * 10 + integer_value;
                            }
                    }
            if (negative && result > 0)
                    result = result * -1;
            
            return result;
    }
    
    public static double StringToDouble (String aStr){
            double results;
            double translated_double = 0.0;
            try{
                    
                    results = Double.valueOf(aStr).doubleValue();

                    
            }
            catch (Exception ex){
                    results = -0.00009;
            }
            return results;
    }
                    
    public static long StringTolong (String aStr){
            long result = 0;
      long integer_value = 0;
            boolean negative = false;
            if (aStr != null)
                    for (int i = 0; i < aStr.length() 
                                                    && (aStr.charAt(i) == '-' || (aStr.charAt(i) >= '0' && aStr.charAt(i) <= '9'))
                                                    && aStr.charAt(i) != ' '; i++){
                            if (aStr.charAt(i) == '-')
                                    if (i == 0)
                                            negative = true;
                                    else
                                            return 0;
                            else
                            {
                                    integer_value = aStr.charAt(i) - '0';
                                    result = result * 10 + integer_value;
                            }
                    }
            if (negative && result > 0)
                    result = result * -1;
            
            return result;
    }
    
    /**
       * this method checks to see if the string is an integer
       * @param input
       * @return boolean 
       */
       public static boolean isInteger( String input )
       {   
          try
          {      
            Integer.parseInt( input );      
            return true;   
          }  catch( Exception ex) 
            {      return false;   }
        }
            
    
    /**
       * this mehtod opens a file for reading
       * @param strPath
       * @return handle to file open for reading
       * @throws java.io.IOException
       */
            public static RandomAccessFile openReadFile(String strPath) throws IOException{ 
                            return (new RandomAccessFile(strPath,"r"));
            }
            
      /**
       * this method opens a file for writing
       * @param strPath
       * @return handle to a file open for writing
       * @throws java.io.IOException
       */
            public static RandomAccessFile openWriteFile(String strPath) throws IOException{        
                            return ( new RandomAccessFile(strPath,"rw"));
            }

            /**
       * This method reads the next line in a file open for reading
       * @param inStream
       * @return 
       * @throws java.io.IOException
       */
            public static String readFile(RandomAccessFile inStream) throws IOException {
                     String line = inStream.readLine();
                     if (line.length() == 0){
                             IOException ex = new IOException();
                             throw ex;
                     }
                     else
                             
                            return line;
                    
            }
      


            /**
       * This writes to a file opened for output
       * @param outStream
       * @param rawText
       * @throws java.io.IOException
       */
            public static void writeFile(RandomAccessFile outStream,String rawText) throws IOException {
                    // assume the output file has been open
                    if (rawText == null){
                            IOException newex = new IOException();
                            throw newex;
                    }
                    rawText = rawText + "\n";
                    outStream.writeBytes(rawText);
        
            }
      
      public static boolean isDir(String dirName) throws IOException
      {
        boolean retval = false;
        File dir = new File(dirName);
        if (dir.isDirectory())
          return true;
        
        return retval;
      }

    public static boolean mkDir(String dirName) throws IOException
    {
      File dir = new File(dirName);
      return dir.mkdir();
      
    }
    
    public static boolean rmDir(String dirName) throws IOException
    {
      File dir = new File(dirName);
      return deleteDir(dir);
      
    }
   
    /**
     *Deletes all files and subdirectories under dir.
     * Returns true if all deletions were successful.
     * If a deletion fails, the method stops attempting to delete and returns false.
     * @param dir
     * @return
     */
    public static boolean deleteDir(File dir) {
        
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
    
    /**
    *  this method removes a file from a directory path
    * @param filename
    * @throws java.lang.Exception
    */
       public static void removeFile(String filename) throws Exception
    {
    boolean exists = (new File(filename)).exists();
    if (exists) {
        boolean success = (new File(filename)).delete();
       if (!success) {
         // create an Exception stating that the old work files could not be removed
         Exception nex = new Exception ("Could not remove existing file: "+filename);
         throw nex;
       }
    }

    }// end removeFile
    
    
    /**
    * this method copies a file from one place to another by opening input and output streams
    * @param source
    * @param destination
    * @throws java.lang.Exception
    */
    public static void copyFile(String source, String destination) throws Exception
    {
    
    File sourceFile = new File(source);
    File dest = new File (destination);
    
    if(!dest.exists()){
    
    dest.createNewFile();
    
    }
    
    InputStream in = null;
    
    OutputStream out = null;
    
    try{
    
    in = new FileInputStream(sourceFile);

       out = new FileOutputStream(dest);
    
     byte[] buf = new byte[1024];

       int len;
    
     while((len = in.read(buf)) > 0){
    
       out.write(buf, 0, len);

           }
    
    }
    
    finally{
    
          in.close();
    
          out.close();
    
       }


    }  // end copyFile

    public static String findFile(String dirName, String fileNamePortion) throws IOException
     {
       File dir = new File(dirName);
       
       String[] children = dir.list();
       if (children == null) {
           IOException nex = new IOException("ERROR: "+ dirName+" as Directory not found.");
           throw nex;
       } else {
           for (int i=0; i<children.length; i++) {
               // Get filename of file or directory
               String filename = children[i];
               if (filename.indexOf(fileNamePortion) > -1)
                 return filename;
           }
       }
       return "";
       
     }
    
    
    public static Properties getResourceProperties(String filename) 
       {
           Properties props = new Properties();
           try{
           File f = new File(filename);
           URI fUri = f.toURI();    
           props.load( fUri.toURL().openStream() );    
           }
           catch (Exception ex){
               ex.printStackTrace();
           }
           // if an exeception then return 
           return props;

       }      

    public static void sendEmailNotification( String domainName,
                                             String email_source,
                                             String sendTo[],
                                             String email_subjectLine,
                                             String email_msgtext,
                                             String attachments []) throws Exception
    {
       
         int returnCode = 0;
                try
                {
                   returnCode = EmailUtility.sendEmailMessage(domainName, email_source,  sendTo, email_subjectLine, email_msgtext, attachments);
                }
                catch(Exception e)
                {
                    throw e;
                }
                
    }// end sendEmailNotification


}