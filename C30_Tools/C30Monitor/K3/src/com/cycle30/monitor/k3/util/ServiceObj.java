package com.cycle30.monitor.k3.util;

public class ServiceObj 
{
  public ServiceObj()
  {  }
   
  String accountExtId = "";
  String accountExtIdType = "4";
  String varExternalId = "";
  int varExternalIdType = 0;
	String varGnAddressesKey = "";
	String varGnAssignmentKey = "";
	String varGnSubAddressesKey = "";
  String fromNodeId = "";
  String toNodeId = "";
  String macId = "";
  String mtaMacAddr = "";
  String deviceCapacity = "";
  int line_number = 0;
	String provInd = "";
  String orderId = "";
  String serviceOrderId = "";
  String errMsg = "";
  String kenanStatus = "";
  String accountInternalId = "";
  String dlpsOE = "";
  String oePrimaryCode = "";
  String oeSecondaryCode = "";
  String oeServiceNumber = "";
  String subscrNo = "";
  String subscrNoResets = "";



	public void setExternalId(String aExternalId) {
		varExternalId = aExternalId;
	}
	public String getExternalId() {
		return varExternalId;
	}
	public void setExternalIdType(int aExternalIdType) {
		varExternalIdType = aExternalIdType;
	}
	public int getExternalIdType() {
		return varExternalIdType;
	}
	public void setAddressesKey(String aGnAddressesKey) {
		varGnAddressesKey = aGnAddressesKey;
	}
	public String getAddressesKey() {
		return varGnAddressesKey;
	}

	public void setAssignmentKey(String aGnAssignmentKey) {
		varGnAssignmentKey = aGnAssignmentKey;
	}
	public String getAssignmentKey() {
		return varGnAssignmentKey;
	}
	public void setLine(int aRtLine) {
		line_number = aRtLine;
	}
	public int getLine() {
		return line_number;
	}
	public void setSubAddressesKey(String aGnSubAddressesKey) {
		varGnSubAddressesKey = aGnSubAddressesKey;
	}
	public String getSubAddressesKey() {
		return varGnSubAddressesKey;
	}
	
	public void setModemMacAddr(String s) {
		macId = s;
	}
	public String getModemMacAddr() {
		return macId;
	}
  
  	public void setMtaMacAddr(String s) {
		mtaMacAddr = s;
	}
	public String getMtaMacAddr() {
		return mtaMacAddr;
	}
  	public void setFromNode(String s) {
		fromNodeId = s;
	}
	public String getFromNode() {
		return fromNodeId;
	}
  public void setToNode(String s) {
		toNodeId = s;
	}
	public String getToNode() {
		return toNodeId;
	}
  public String getProvInd()
  {
    return provInd;
  }
  public void setProvInd(String s)
  {
    provInd = s;
  }
    public void setOrderId(String s)
  {
    orderId = s;
  }
  public String getOrderId()
  {
    return orderId;
  }
    public void setServiceOrderId(String s)
  {
    serviceOrderId = s;
  }
  public String getServiceOrderId()
  {
    return serviceOrderId;
  }
  public void setErrMsg(String s)
  {
    errMsg = s;
  }
  public String getErrMsg()
  {
    return errMsg;
  }
  public void setAccountInternalId(String s)
  {
    accountInternalId = s;
  }
  public String getAccountInternalId()
  {
    return accountInternalId;
  }
  public void setKenanStatus(String s)
  {
    kenanStatus = s;
  }
  public String getKenanStatus()
  {
    return kenanStatus;
  }
  public void setAccountExtId(String s)
  {
    accountExtId = s;
  }
  public String getAccountExtId()
  {
    return accountExtId;
  }
  public void setAccountExtIdType(String s)
  {
    accountExtIdType = s;
  }
  public String getAccountExtIdType()
  {
    return accountExtIdType;
  }
  public String getDeviceCapacity()
  {
    return deviceCapacity;
  }
  public void setDeviceCapacity(String s)
  {
    deviceCapacity = s;
  }
  /*
   * String dlpsOE = "";
  String oePrimaryCode = "";
  String oeSecondaryCode = "";
  String oeServiceNumber = "";
   */
  public void setDlpsOE(String s)
  {
    dlpsOE = s;
  }
  public String getDlpsOE()
  {
    return dlpsOE;
  }
  public void setOePrimaryCode(String s)
  {
    oePrimaryCode = s;
  }
  public String getOePrimaryCode()
  {
    return oePrimaryCode;
  }
  public void setOeSecondaryCode(String s)
  {
    oeSecondaryCode = s;
  }
  public String getOeSecondaryCode()
  {
    return oeSecondaryCode;
  }
  public void setOeServiceNumber(String s)
  {
    oeServiceNumber = s;
  }
  public String getOeServiceNumber()
  {
    return oeServiceNumber;
  }
  public void setSubscrNo(String s)
  {
    subscrNo = s;
  }
  public String getSubscrNo()
  {
    return subscrNo;
  }
  public void setSubscrNoResets(String s)
  {
    subscrNoResets = s;
  }
  public String getSubscrNoResets()
  {
    return subscrNoResets;
  }
}