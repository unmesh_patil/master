package com.cycle30.monitor.k3.webservices;


import javax.xml.parsers.*;
import java.util.*;
import org.w3c.dom.*;
import org.w3c.dom.Document;


public class wpPhoneNumberDataSet 
                                                
{
      private static String uri; 
      
     public static void initialize(Properties propertiesfile)
     {
     //nodeMigrate.MtaLookupUri=http://inquiry.operations.gci.com/Dlps/ajaxPhpServer/cmEndPoint.php?addressMacCm
     // #nodeMigrate.MtaLookupUri=http://inquiry.operations.gci.com/Dlps/ajaxPhpServer/mtaEndPoint.php?mtaMacAddress
      // http://inquiry.operations.gci.com/Dlps/ajaxPhpServer/phoneNumberInfo.php?phoneNumber=9073499980   !!!!!!!!!  this works.
      //http://inquiry.operations.gci.com/Dlps/ajaxPhpServer/mtaDataServiceLocation.php?mtaMacAddress=0000CAADC3F8  !!!!!!!!!!!!  this works give service location of by MTA MAC ADDRESS
      
     //  http://inquiry.operations.gci.com/Dlps/DlpsMonitoring/searchMtaInventory.php?mtaMacAddress=0000CAADC3F8
      // uri = (String)propertiesfile.getProperty("webservices.MtaLookupUri");
        uri =  "http://inquiry.operations.gci.com/Dlps/ajaxPhpServer/phoneNumberInfo.php?phoneNumber";
     }
        //-----------------------------------------------------------------------
        /**
         *   Get port data items from DOM document
         *   
         *   @param phoneNumber 
         *   @return Hashtable containing a HashMap of each data element
         */
        public static Hashtable getPhoneNumberData(String phoneNumber) throws Exception
        {
        
            System.out.println("Entering getPhoneNumberData");
            Hashtable portsCollection = new Hashtable();
            Hashtable portData =  new Hashtable();
            String numberEndPoint= "";
            String numberTelephone= "";
                        
           try{ 
           
             DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
             DocumentBuilder  docBuilder = docFactory.newDocumentBuilder();
             Document doc = docBuilder.parse(uri+"="+phoneNumber);
          
             String errCode = doc.getElementsByTagName("errCode").item(0).getFirstChild().getNodeValue();
             if (errCode == null)
             {
               System.out.println("wpMtaDataSet.getMtaPorts: WebPage for "+uri+" not functional.");
               Exception nex = new Exception("WebPage for "+uri+" not functional.");
               throw nex;
             }
             if (!errCode.trim().equals("0"))
               return portsCollection;
             // Get a list of all port elements in the document
            Element ports = doc.getDocumentElement();
            NodeList portlist = ports.getElementsByTagName("E911Info");

            for( int i=0; i<portlist.getLength(); i++ ) {
                 portData =  new Hashtable();
                 numberTelephone = DomUtil.getSimpleElementText((Element)portlist.item(i), "Telephone");
                 portData.put("FirstName", DomUtil.getSimpleElementText((Element)portlist.item(i), "FirstName" ));
                 portData.put("LastName", DomUtil.getSimpleElementText( (Element)portlist.item(i), "LastName" ));
                 portData.put("ActiveDate", DomUtil.getSimpleElementText( (Element)portlist.item(i), "ActiveDate" ));
                 portData.put("Company", DomUtil.getSimpleElementText((Element)portlist.item(i), "Company"));
                 portData.put("ContactPhone", DomUtil.getSimpleElementText((Element)portlist.item(i), "ContactPhone"));
                 portData.put("Areacode", DomUtil.getSimpleElementText((Element)portlist.item(i), "Areacode" ));
                 portData.put("Telephone", DomUtil.getSimpleElementText((Element)portlist.item(i), "Telephone"));
                 portData.put("ErrorNumber", DomUtil.getSimpleElementText((Element)portlist.item(i), "ErrorNumber" ));
                 portData.put("ErrorString", DomUtil.getSimpleElementText((Element)portlist.item(i), "ErrorString"));
                 portData.put("FullAddress", DomUtil.getSimpleElementText((Element)portlist.item(i), "FullAddress"));
                 portData.put("ServiceType", DomUtil.getSimpleElementText((Element)portlist.item(i), "ServiceType" ));
                 portData.put("Oe", DomUtil.getSimpleElementText((Element)portlist.item(i), "Oe" ));
          
                 
                 portsCollection.put(numberTelephone,portData);
                 
      }
 
           } catch (Exception ex)
           {
            // portData.put("ERROR","XML Parsing error for MTA Mac Address"+mtaMacAddr);
             portsCollection.put("ERROR",portData);
            // cat.debug( "getMtaPorts:  ERROR XML Parsing error for MTA Mac Address"+mtaMacAddr );
                
           }
 
          
  
            return portsCollection;
            
        }
        
        
        //---------------------------------------------------------------------------
        /**
         *   Get individual DOM element value
         *   
         *   @param responseDOM DOM document containing Health Check response
         *   @param elementName  Health Check item to retrieve
         *   @return String value of health check item value
         */
        private static String getDOMElementValue(Document responseDOM, String elementName) 
        {
            String docElement = "N/A";
            
            if (responseDOM==null || elementName==null)
               return docElement;
            
            NodeList nodelist = responseDOM.getElementsByTagName(elementName);
            for (int i=0; i<nodelist.getLength(); i++) {
                Node node = nodelist.item(i).getFirstChild();
                if (node != null)  {
                    docElement = node.getNodeValue();
                }  
            }
            
            return docElement;
        }  
        
    }