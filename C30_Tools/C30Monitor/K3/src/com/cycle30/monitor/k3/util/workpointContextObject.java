package com.cycle30.monitor.k3.util;

import com.workpoint.client.ClientContext;

public class workpointContextObject {
    
    private static ClientContext context = ClientContext.createContext();
    final private static String wpDB = "WPDS";
    final private static String wpUser = "arborsv";
    private static boolean loggedOn = false;
    
    public static ClientContext getContext() throws Exception{
        logon();
        return context;
    }
    
    public static void logout(){
        if (context != null && context.isOpen()){
            context.clearCache();
            context.logout();
            context.close();
        }
    }
    
    public static void logon() throws Exception {
        if (context == null)
            context = ClientContext.createContext();
        if (!context.isOpen())
        {
            //System.out.println("Before opening the context.");
            context.open(wpDB, wpUser,"");
        }
    }
    
    public static ClientContext renewContext() throws Exception {
            context = null;
            context = ClientContext.createContext();
            System.out.println("Before opening the context.");
            context.open(wpDB, wpUser,"");
            
            return context;
    }
    
    
}
