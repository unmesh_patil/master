package com.cycle30.monitor.k3.util;

import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

/**
 * Class EmailUtility provides basic functionality to send email. It uses the SMTP
 * protocol without authentication.
 */
public class EmailUtility 
{
  public static final Logger logger = Logger.getLogger(EmailUtility.class);
  
	public EmailUtility()
	{
	}

  
  /**
   * The method sends out an email message with the parameters supplied.
   * @throws javax.mail.MessagingException
   * @return 0 for success, -1 for failure.
   * @param attachments a list of file names to be attached with this email.
   * @param msgText the text of the email message.
   * @param subject the subject of the email message.
   * @param to a list of the email addresses to send to.
   * @param from the email address frm which the email is being sent.
   * @param host the name of the SMTP host.
   */
  public static int sendEmailMessage(
    String host,
    String from,
    String[] to,
    String subject,
    String msgText,
    String[] attachments)
    throws MessagingException
  {
    if (from == null || to == null || to.length == 0)
    {
      logger.error("EmailUtility:: Either of the from or to addresses is invalid or empty.");
      return -1;
    }
    Properties props = new Properties();
    props.put("mail.smtp.host", host);

    Session session = Session.getInstance(props, null);
    
    Message msg = new MimeMessage(session);
    msg.setFrom(new InternetAddress(from));
    
    InternetAddress[] address = new InternetAddress[to.length];
    for (int i = 0; i < to.length; i++)
    {
      address[i] = new InternetAddress(to[i]);
    }
    msg.setRecipients(Message.RecipientType.TO, address);

    Multipart mp = new MimeMultipart();

    MimeBodyPart msgBody = new MimeBodyPart();
    msgBody.setText(msgText);
    
    mp.addBodyPart(msgBody);
    
    for (int j = 0; j < attachments.length; j++)
    {
      MimeBodyPart msgAttachment = new MimeBodyPart();
      FileDataSource fds = new FileDataSource(attachments[j]);
	    msgAttachment.setDataHandler(new DataHandler(fds));
	    msgAttachment.setFileName(fds.getName());
      mp.addBodyPart(msgAttachment);
    }
    
    msg.setContent(mp);
    msg.setSubject(subject);
    msg.setSentDate(new Date());
    
    Transport.send(msg);

    return 0;
  }
}