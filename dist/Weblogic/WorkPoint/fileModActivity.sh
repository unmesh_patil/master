#! /bin/sh

cd /opt/app/kenanfx2/arbor/bea/Weblogic/user_projects/domains/WorkPoint

if [ $# -lt 2 ] 
then
    echo "Usage : $0 <FILE NAME> <seconds>"
    exit 1
fi


IN_FILE=$1

SEC_MOD=$2


# check that input file exists



if [ -z "${IN_FILE}" ]
then
    echo "ERROR: FILE NAME parameter not passed.\n" >&2
     exit 1
fi

if [ -z "${SEC_MOD}" ]
then
    echo "ERROR: SECONDS parameter not passed.  Supply numeric value representing number of seconds since file modification.\n" >&2
     exit 1
fi

if [ ! -f $IN_FILE ]
then
	echo "ERROR: file $IN_FILE not found" >&2
	exit 1
fi


if expr $SEC_MOD + 0 > /dev/null 2>&1
then
    echo "$SEC_MOD is a number" >&2
else
    echo "$SEC_MOD isn't greater than 0 or is not a number" >&2
    exit 1
fi




last=$(date -r $IN_FILE '+%s')

echo last file modification of $1 is  $last

curr=$(date '+%s')

echo current timestamp is  $curr

diff=$(($curr - $last))

echo difference  is  $diff

if [ $diff -gt $SEC_MOD ]; then
   echo OLDER
   exit 0
fi

exit 0