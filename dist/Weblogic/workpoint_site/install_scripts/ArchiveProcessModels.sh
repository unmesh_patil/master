#!/bin/sh

export=N
import=N
file=
dsname=
user=
pass=

# To be invoked from any directory, find the same directory where this file is located
currDir="`pwd`"
cd "`dirname $0`"
fileDir="`pwd`"
cd "$currDir"

. $fileDir/utility.sh


f_usage()
{
  echo ""
  echo "This script exports and imports process models ."
  echo "usage: `basename $0` -e|-i -f file [-d dsname] [-u user] [-p password]"
  echo ""
  echo " -e       - Export mode"
  echo " -i       - Import mode"
  echo " file     - name of file to use"
  echo " dsname   - Optional parameter for with dsname to load into"
  echo "            vales include WPDS, WPDS2 or WPDS3"
  echo "            default to WPDS"
  echo " user     - if security is on, user can be provided either on the prompt or using -u option"
  echo " password - if security is on, password can be provided either on the prompt or using -p option"
  exit 1
}

f_check_security_on()
{
  f_debug
  f_debug "Start f_check_security_on"
  wp_classpath=`echo $WP_CLASSPATH | sed -e 's/:/ /g'`

  for i in $wp_classpath
  do
      f_debug "  WP_CLASSPATH entry: $i"
      if [ -d $i ]; then
          f_debug "  Found dir: $i"
          server_prop_file=$i/workpoint-server.properties
          if [ -f $server_prop_file ]; then
              f_debug "Reading $server_prop_file"
              auth_str=`grep "workpoint.user.authentication" $server_prop_file | grep -v "^#"`
              f_debug "  authentication string: $auth_str"
              auth_val=`echo $auth_str | sed -e 's/ //g' | cut -f2 -d=`
              f_debug "  workpoint.user.authentication = $auth_val"

              if [ x$auth_val = x'true' ]; then
                  f_info " Security is turned on"
                  if [ x$user = x ]; then
                    printf " Please enter workflow admin user's name: "
                    read user
                  fi

                  if [ x$pass = x ]; then
                    stty -echo
                    printf " Please enter workflow admin user's password: "
                    read pass
                    stty echo
                  fi
              fi
              break;
          fi
      fi
  done
}


# ===========================================================
# Start MAIN
# ===========================================================
#f_info "Begin Process"

set -- `getopt eif:d:u:p: $*`
if [ $? -ne 0 ]; then
  f_usage
fi

#f_info "Check usage options"
while [ $# -gt 0 ]; do
 #f_info "looking at $1"
  case $1 in
    -e) export=Y
        shift
        ;;
    -i) import=Y
        shift
        ;;
    -f) file=$2
        shift 2
        ;;
    -d) dsname=$2
        shift 2
        ;;
    -u) user=$2
        shift 2
        ;;
    -p) pass=$2
        shift 2
        ;;
    --) shift
        break
        ;;
  esac
done

#f_info "Check import/export usage options"
if [ $export = "N" -a $import = "N" ]; then
  echo "Export or Import mode must be provided"
  echo ""
  f_usage
fi
if [ $export = "Y" -a $import = "Y" ]; then
  echo "Only One : Export or Import mode can be provided"
  echo ""
  f_usage
fi

#f_info "Check valid file"
if [ x$file = x ]; then
  f_usage
else
  if [ ! -f $file ]; then
    f_error "$file doesn't exist or is not a regular file."
    f_usage
  fi
fi

#f_info "Set DSname if not set"
if [ x$dsname = "x" ]; then
  dsname="WPDS"
fi

# Not sure what this is for
#
#if [ x${APPLICATION_PATH} = x ]; then
#  . ./../../bin/workflow.sh
#else
#  . "${APPLICATION_PATH}/bin/workflow.sh"
#fi

JAVA_OPTIONS=""

#if [ ${APPLICATION_SERVER_TYPE} = "jboss" ]; then
#  domainDir=${APPSERVERHOME}
#  EJB_CLASSPATH=${APPSERVERHOME}/client/jbossall-client.jar:${EJB_CLASSPATH}
#  EJB_CLASSPATH=${APPSERVERHOME}/client/jnp-client.jar:${EJB_CLASSPATH}
#  JAVA_OPTIONS="${JAVA_OPTIONS}"
#elif [ ${APPLICATION_SERVER_TYPE} = "weblogic" ]; then
  if [ x$WL_HOME = x ]; then
    f_error " ++ WL_HOME is null."
    f_error "      Set WL_HOME to WebLogic home directory. (ex: /usr/bea/weblogic_10.0/wlserver_10.0)"
    f_error "      You can set WL_HOME either in user's profile or on the prompt.  "
  fi
  #domainDir=${WORKFLOW_DOMAINDIR}
  domainDir=${BEA_HOME}/user_projects/domains/WorkPoint
  EJB_CLASSPATH=${WL_HOME}/server/lib/wlclient.jar:${EJB_CLASSPATH}
  JAVA_OPTIONS="${JAVA_OPTIONS}-Djava.security.policy=${domainDir}/config/weblogic.policy"
#elif [ ${APPLICATION_SERVER_TYPE} = "websphere" ]; then
#  domainDir=${WORKFLOW_PROFILEPATH}
#  EJB_CLASSPATH=${WAS_APPSERVERHOME}/runtimes/com.ibm.ws.orb_7.0.0.jar:${EJB_CLASSPATH}
#  EJB_CLASSPATH=${WAS_APPSERVERHOME}/runtimes/com.ibm.ws.ejb.thinclient_7.0.0.jar:${EJB_CLASSPATH}
#  JAVA_OPTIONS="${JAVA_OPTIONS} -Djava.security.policy=${domainDir}/cbs_workflow/websphere.policy"
#fi

f_info "Starting Archive Command Line Utility"

#JAR_DIR=${domainDir}/cbs_workflow/lib
JAR_DIR=${WORKPOINTSITE}/java
# files don't exist
#WP_CLASSPATH=${WP_CLASSPATH}:${domainDir}/cbs_workflow
#WP_CLASSPATH=${WP_CLASSPATH}:${domainDir}/cbs_workflow/bin
WP_CLASSPATH=${WP_CLASSPATH}:${JAR_DIR}/wpClient.jar
WP_CLASSPATH=${WP_CLASSPATH}:${JAR_DIR}/wpCommon.jar
WP_CLASSPATH=${WP_CLASSPATH}:${JAR_DIR}/wpGUI.jar
# file doesn't exist
#WP_CLASSPATH=${WP_CLASSPATH}:${JAR_DIR}/workflowapp.jar
WP_CLASSPATH=${WP_CLASSPATH}:${JAR_DIR}/log4j.jar
WP_CLASSPATH=${WP_CLASSPATH}:${JAR_DIR}/xercesImpl.jar
WP_CLASSPATH=${WP_CLASSPATH}:${JAR_DIR}/xml-apis.jar
WP_CLASSPATH=${WP_CLASSPATH}:${EJB_CLASSPATH}
#WP_CLASSPATH=${WP_CLASSPATH}:${APPLICATION_PATH}/cbs_workflow/lib/ojdbc.jar

if [ x$user = x  -o  x$pass = x ]; then
  f_check_security_on
fi

f_info "Using "
f_info "  APPLICATION_SERVER_TYPE  - ${APPLICATION_SERVER_TYPE}"
f_info "  APPSERVERHOME            - ${domainDir}"
f_info "  file                     - $file"
f_info "  dsname                   - $dsname"
f_info "  user                     - $user"
if [ x$pass = x ]; then
  f_info "  password                 - "
else
  f_info "  password                 - "`printf $pass | tr -c "" "[\052*]"`
fi
f_info "  WP_CLASSPATH             - ${WP_CLASSPATH}"
f_info "  JAVA_HOME                - ${JAVA_HOME}"
f_info "  JAVA_OPTIONS             - ${JAVA_OPTIONS}"

if [ $export = Y ]; then
  f_info "Start to run Export..."
  java -cp ${WP_CLASSPATH} ${JAVA_OPTIONS} com.workpoint.archive.Export ./script.properties $file $dsname $user $pass
elif [ $import = Y ]; then
  f_info "Start to run Import..."
  java -cp ${WP_CLASSPATH} ${JAVA_OPTIONS} com.workpoint.archive.Import ./script.properties $file $dsname $user $pass
  f_info "Finished import."
fi
