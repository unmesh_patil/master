#!/bin/sh

# Check for the right number of arguments.
if [ "$#" -lt "3" ]; then
  echo "Usage: $0 <revision> <userid> <password> [<branchtag>]"
  exit 1
else
  if [ $# = 3 ]; then
    REVISION=$1;
    SVN_USER=$2;
    SVN_PASSWORD=$3;
    BRANCH="trunk";
  else
    if [ $# = 4 ]; then
      REVISION=$1;
      SVN_USER=$2;
      SVN_PASSWORD=$3;
      BRANCH=branches/$4;
    else
      echo "Usage: $0 <revision> <userid> <password> [<branchtag>]"
    fi
  fi
fi

SHORTREV=`expr $REVISION - 1`;
BASE_DEST=dist
BASE_TRUNK=dist/$BRANCH
PROJECT=API
SUBPROJECT=muleapi
DEST=$BASE_DEST/$PROJECT/$SUBPROJECT;
RELEASELIST=tmp.$REVISION.file
DLIST=tmpd.$REVISION.file
ULIST=tmpu.$REVISION.file

DEPLOY_DIR=$HOME/deployc30

REVISION_RANGE=$SHORTREV:$1

#echo REVISION_RANGE=$REVISION_RANGE

BASE_URL=https://subversion.assembla.com/svn/c30billing/awn/integration

svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $REVISION_RANGE $BASE_URL/$BASE_TRUNK/$PROJECT/$SUBPROJECT > $RELEASELIST
cat $RELEASELIST | sed -e "s/^MM*[ \t]*//" | sed -e "s/^ [AM][ \t]*//" | sed -e "s/^A[ \t]*//" | sed -e "/^D[ \t]*/ d" | sed -e "s|$BASE_URL/$BASE_TRUNK/||" > $ULIST
cat $RELEASELIST |  sed -e "/^MM*[ \t]*/ d" | sed -e "/^ [AM][ \t]*/ d" | sed -e "/^A[ \t]*/ d" | sed -e "s/^D[ \t]*//" | sed -e "s|$BASE_URL/$BASE_TRUNK/$PROJECT/$SUBPROJECT/||" > $DLIST
rm -f $RELEASELIST

# echo done with diff
# echo DEPLOY=[$DEPLOY_DIR/$DEST]
if [ -e $DEPLOY_DIR/$DEST ]
   then
     rm -rf $DEPLOY_DIR/$DEST
fi

ENVIRONMENT=`echo $ENVIRONMENT | tr [:upper:] [:lower:]`
export ENVIRONMENT

#delete files that have been removed
while read line
do
#  echo line=$line
#  echo dirname=`dirname $line`

  echo Deleting $line from $HOME
  rm -f $HOME/$line
done < $DLIST
rm -f $DLIST
echo Done reading delete list
              
#export files
while read line
do
  HAS_REVS=1
#  echo line=$line
#  echo dirname=`dirname $line`

#  echo Checking out $line to $DEPLOY_DIR/$BASE_DEST/$line
  mkdir -p $DEPLOY_DIR/$BASE_DEST/`dirname $line`
  svn export --username=$SVN_USER --password=$SVN_PASSWORD -r $REVISION $BASE_URL/$BASE_TRUNK/$line $DEPLOY_DIR/$BASE_DEST/$line
done < $ULIST
rm -f $ULIST
#echo Done exporting files

#BUILD MODULE
#echo JAVA_HOME=$JAVA_HOME

#check for branch-based build
if [ "$BRANCH" != "trunk" ] ; then
  HAS_REVS=1
  svn export --username=$SVN_USER --password=$SVN_PASSWORD -r $REVISION $BASE_URL/$BASE_TRUNK/$PROJECT@$REVISION $DEPLOY_DIR/$BASE_DEST/$PROJECT
fi

#handle standard trunk-based build
if [ -n "$HAS_REVS" ] ; then
  svn export --force --username=$SVN_USER --password=$SVN_PASSWORD $BASE_URL/$BASE_TRUNK/buildscripts/$PROJECT $DEPLOY_DIR/$DEST
                 
  tolower() { echo $1 | tr "[:upper:]" "[:lower:]"; }
  DEPLOY_ENVIRONMENT=$(tolower ${SYSTEM_NAME}${SYSTEM_TYPE}${ENVIRONMENT})

  cd $DEPLOY_DIR/$DEST
  ant -DENVIRONMENT=$DEPLOY_ENVIRONMENT
else
  echo NO FILES FOUND TO BE DEPLOYED
fi

rm -rf $DEPLOY_DIR
