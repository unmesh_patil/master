#!/bin/bash

#--------------------------------------------------------------------------------
#  Set up runtime environment variables for FX Web Enablement app.
#--------------------------------------------------------------------------------

# Set Mule related environment
MULE_HOME=/opt/app/kenanfx2/muleapi

export MULE_HOME

# Environment identifier (to match X-Env-Id HTTP header)
export C30ENVIRONID=C3B3DEV

# This variable is used to set the path in mule-config.xml via ${BASE_PATH} 
export BASE_PATH=C3B3/DEV


$MULE_HOME/bin/mule $1 -app $MULE_APP $DEBUG