#!/bin/bash


# Set JDK related environment
JAVA_HOME=/opt/jdk1.6.0_18
PATH=$JAVA_HOME/bin:$PATH

# Mule params
MULE_HOME=/opt/app/kenanfx2/muleapi
MULE_APP=WebEnablement

# Export core environment variables
export JAVA_HOME MULE_HOME MULE_APP PATH

# archive log files
option=$1
if [ "$option" = "start" ] ; then

	# archive existing log files before restarting Mule
	dt=$(date +%Y%m%d.%H%M%S)

	cd $MULE_HOME/logs
	gzip WebEnablement.log

	mv WebEnablement.log.gz WebEnablement.log.$dt.gz

	cd $MULE_HOME
	
    # backup the C30SDK.log 
    gzip C30SDK.log
    mv C30SDK.log.gz $MULE_HOME/logs/C30SDK.log.$dt.gz
fi

# Tuxedo/FX Variables
ARBORDIR=/opt/app/kenanfx2/arbor
BSDSITE=/opt/app/kenanfx2/arbor/bsdm_site
BINDIR=/opt/app/kenanfx2/arbor/bin
FXBASE=/opt/app/kenanfx2/arbor/appdir
TUXDIR=/opt/app/kenanfx2/arbor/bea/tuxedo9.1
TUXPATH=${TUXDIR}/bin
TUXLIBS=${TUXDIR}/lib
export ARBORDIR TUXDIR TUXPATH TUXLIBS FXBASE BINDIR BSDSITE
 

FLDTBLDIR="${FXBASE}/config:${TUXDIR}/udataobj"
FIELDTBLS="bali.fml,ShieldWare.fml,tpadm,Usysflds"
FLDTBLDIR32="${FXBASE}/config:${TUXDIR}/udataobj"
FIELDTBLS32="bali.fml,ShieldWare.fml,tpadm,Usysflds"
export FLDTBLDIR FIELDTBLS
export FLDTBLDIR32 FIELDTBLS32


# Build the LD_LIBRARY_PATH
_TEST_PATH="/opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/configurator_fx_site/lib /opt/app/kenanfx2/arbor/cit_site/lib /opt/app/kenanfx2/arbor/content_catalog_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/inventory_site/lib /opt/app/kenanfx2/arbor/ordering_site/lib /opt/app/kenanfx2/arbor/content_catalog_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/serviceability_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/bsdm_site/lib /opt/app/kenanfx2/arbor/workflow_site/lib /opt/app/kenanfx2/arbor/workpoint_site/lib"
if [ -n "${_TEST_PATH}" ]; then
  for DIR in ${_TEST_PATH}; do
    echo ${LD_LIBRARY_PATH} | fgrep -si $DIR > /dev/null
    if [ ${?} -ne 0 ]; then
      LD_LIBRARY_PATH="${DIR}:${LD_LIBRARY_PATH}"
    fi
  done
fi

LD_LIBRARY_PATH="${TUXLIBS}:${LD_LIBRARY_PATH}"

# Unix PATH
PATH="${BINDIR}:${PATH}"
PATH="${TUXPATH}:${PATH}"
PATH=$PATH:$MULE_HOME


# Setup Oracle
ORACLE_HOME=/opt/app/oracle/product/11.2.0/client_1
export ORACLE_HOME
ORACLE_LIBDIR=${ORACLE_HOME}/lib; export ORACLE_LIBDIR
LD_LIBRARY_PATH="${ORACLE_LIBDIR}:${LD_LIBRARY_PATH}"
PATH="${ORACLE_HOME}/bin:${PATH}"


# Export PATH and LD_LIBRARY_PATH now that everything is established
export PATH
export LD_LIBRARY_PATH


# Job status output directory for temp Workpoint .jpg
export JOB_STATUS_IMG_OUTPUT=/tmp/
export INPUT_AQ_QUEUE_NAME=ORDER_STATUS_QUEUE

export TRANS_ID_REQUIRED=yes
export VALIDATE_DIGEST=yes

export DEBUG="-debug"
$MULE_HOME/bin/fxweb.sh $1 
