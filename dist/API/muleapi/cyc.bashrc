alias l="ls -alt"

ANT_HOME=/opt/apache-ant-1.8.2
export ANT_HOME

JAVA_HOME=/opt/jdk1.6.0_18

PATH=$PATH:$ANT_HOME/bin:$HOME/cycbin

export PATH JAVA_HOME

#Workpoint Variables
BEA_HOME=${ARBORDIR}/bea/Weblogic ; export BEA_HOME
WEBLOGIC_HOME=${BEA_HOME}/wlserver_10.3 ; export WEBLOGIC_HOME
WL_HOME=${BEA_HOME}/wlserver_10.3 ; export WL_HOME
WL_USER=weblogic ; export WL_USER
WL_PW=weblogic ; export WL_PW
