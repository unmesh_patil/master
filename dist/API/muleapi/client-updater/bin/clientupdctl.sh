#!/bin/bash

#-------------------------------------------------------------------------------------------
# This Script will Stop and Restart the Client-Updater Daemon Thread
# Usage /opt/app/kenanfx2/muleapi/bin/clientupdctl.sh command[stop][start]
#  
#------------------------------------------------------------------------------------------

# Set JDK related environment
#JAVA_HOME=/opt/jdk1.6.0_18
JAVA_HOME=/opt/jdk1.8.0_92
PATH=$JAVA_HOME/bin:$PATH

# Export core environment variables
export JAVA_HOME

option=$1
export INPUT_AQ_QUEUE_NAME=ORDER_STATUS_QUEUE

export DEBUG="-debug"

#-----------------------------------------------
# stop the client order status updater daemon.
#-----------------------------------------------
if [ "$option" = "stop" ] ; then
   ps -ef | grep 'client-updater' | grep -v grep | awk '{print $2}' | xargs kill 2>/dev/null
   echo "Stopped client-updater process."
fi

#-----------------------------------------------
#  start the client order status updater daemon.
#-----------------------------------------------
if [ "$option" = "start" ] ; then

   # - First, try to stop existing processes
   ps -ef | grep 'client-updater' | grep -v grep | awk '{print $2}' | xargs kill 2>/dev/null
   /opt/app/kenanfx2/muleapi/client-updater/bin/client-updater.sh &
   echo "Restarted client-updater processes..."
fi

