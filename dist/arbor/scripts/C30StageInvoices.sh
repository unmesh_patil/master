#!/bin/sh
echo $ARBORDIR
.  $ARBORDIR/scripts/utility/decrypt_pw.sh

#
# Check to see if ORACLE_SID is set
if [ -z "${ORACLE_SID}" ]; then
   echo "ERROR: \$ORACLE_SID environment variable is not set\n"
   echo "This script requires that the \$ORACLE_SID environment variable be set.\n\n"
   exit 1
fi
#
# Execute the Cycle30 Stage Invoice process
sqlplus arbor/${ARBOR_DB_PWD}@$ORACLE_SID << THEEND
spool $ARBORLOG/bip_c30stageInvoices.log
declare
x number;
begin
x := C30ARBOR.c30stageInvoices();
c30writeTransactionLog('Function c30stageInvoices completed '||case when x = 0 then 'successfully' else 'UNSUCCESSFULLY' end,case when x = 0 then 'info' else 'error' end);
end;
/
spool off
quit
THEEND
STATUS=$?
#
# Check for bad return code
if [ $STATUS -ne 0 ]; then
	echo "ERROR: SQLPLUS bad return code [$STATUS]"
  exit $STATUS
fi
#
# Check for Oracle SQL Errors in the log file
if grep ORA- "$ARBORLOG/bip_c30stageInvoices.log"; then
  echo "ERROR: There was an Oracle error found in the log file."
  return 1
else
  echo "INFO: Log file contained no issues."
  #
  # Display information to use with UC4
  echo "LOGFILE: $ARBORLOG/bip_c30stageInvoices.log"
  echo "COMPLETE: NO detected issues in process."
fi