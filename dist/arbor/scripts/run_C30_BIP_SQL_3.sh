#! /bin/sh
##################################################################
# R353312 12/07/2013 Migrated script run_GCI_BIP_SQL_3.sh to K3
##################################################################

.  $ARBORDIR/scripts/utility/decrypt_pw.sh

#
# Check to see if ORACLE_SID is set
if [ -z "${ORACLE_SID}" ]; then
   echo "ERROR: \$ORACLE_SID environment variable is not set\n"
   echo "This script requires that the \$ORACLE_SID environment variable be set.\n\n"
   exit 1
fi
#
# Check to see if ARBORDBU is set
if [ -z "${ARBORDBU}" ]; then
   echo "ERROR: \$ARBORDBU environment variable is not set\n"
   echo "This script requires that the \$ARBORDBU environment variable be set.\n\n"
   exit 1
fi
#
#  Verify that all 5 entries exist
if [ $# -ne 5 ]; then
    echo 'USAGE: run_C30_BIP_SQL_3.sh <Spool File Name> <Bip Num> <Group No> <Cycle Date> <Cycle Type>'
    echo 'EXAMPLE: run_C30_BIP_SQL_2.sh setup_bip_20130101.log 01 1 20130101 cycle'
    exit 1
fi
#
# Load values
ACCT_DB=${ORACLE_SID}
ARBORDBU=${ARBORDBU}
PROCESS_SCHED_SPOOL=$1
BIP_NO=$2
GRP_NO=$3
CYCLE=$4
CTYPE=$5
#
# Insert records into the PROCESS_SCHED table for this BIP instance
sqlplus -s $ARBORDBU/$ARBOR_DB_PWD\@$ACCT_DB <<- SQLPLUSEND >> /dev/null
  whenever sqlerror exit 1
  whenever oserror exit 2

 spool $PROCESS_SCHED_SPOOL

 delete from PROCESS_SCHED where process_name = 'bip$BIP_NO';

 insert into PROCESS_SCHED
 (process_name,task_name,task_cycle,task_mode,sched_start,task_intrvl,
  task_status,task_priority,slide_time,db_name,sql_query,debug_level,
  plat_id,usg_crt_hour,usg_plat_id, usg_version)
  values('bip$BIP_NO','bip$BIP_NO','N',0, SYSDATE, 86400,0,0,0,'$ACCT_DB', 
         'CMF.account_no in (select account_no from bg where grp=$GRP_NO)'
         ,0,'$CYCLE',NULL,'$CTYPE',NULL);
 commit;

  spool off

exit

SQLPLUSEND
STATUS=$?
#
# Check for bad return code
if [ $STATUS -ne 0 ]; then
	echo "ERROR: SQLPLUS bad return code [$STATUS]"
  exit $STATUS
fi
#
# Check for Oracle SQL Errors in the log file
if grep ORA- $PROCESS_SCHED_SPOOL; then
  echo "ERROR: There was an Oracle error found in the log file."
  return 1
else
  echo "INFO: Log file contained no issues."
  #
  # Display information to use with UC4
  echo "LOGFILE: $PROCESS_SCHED_SPOOL"
  echo "COMPLETE: NO detected issues in process."
fi