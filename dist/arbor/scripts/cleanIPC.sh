#!/bin/sh
# $Id: cleanIPC,v 2.1 2002/04/25 20:58:24 wcox Exp $
#
# Check arguments
if [ ${#} -ne 1 ]; then
  #echo "Usage:" `basename ${0}` "<user name>"
  #exit 1
  uName=$USER
else
  uName=${1}
fi

#
# do the ditty...
ipcs -m | egrep ${uName} | while read line; do
  key=`echo "${line}" | cut -c12-22`
  echo "ipcrm -m ${key}"
  ipcrm -m ${key}
done

ipcs -q | egrep ${uName} | while read line; do
  key=`echo "${line}" | cut -c12-22`
  echo "ipcrm -q ${key}"
  ipcrm -q ${key}
done

ipcs -s | egrep ${uName} | while read line; do
  key=`echo "${line}" | cut -c12-22`
  echo "ipcrm -s ${key}"
  ipcrm -s ${key}
done

exit 0
