#!/bin/sh
echo $ARBORDIR
.  $ARBORDIR/scripts/utility/decrypt_pw.sh

CYCLE=$1
CTYPE=$2
SQL=$3

if [ $# -ne 3 ]
then
    echo 'USAGE: bip.sh <cycle date> <cycle type> <sql query>'
    echo '(e.g.) bip.sh yyyymmdd cycle "CMF.account_no IN(111,222)" or bip.sh yyyymmdd audit CMF.account_no=123'
    exit 1
fi

sqlplus -S arbor/$ARBOR_DB_PWD\@$ORACLE_SID << THEEND
spool $ARBORLOG/bip_sh.log
prompt Logged on to "$ORACLE_SID"
delete from PROCESS_SCHED where process_name = 'bip01';
insert into PROCESS_SCHED values('bip01', 'bip01', 'N', 0, SYSDATE, 86400, 0, 2, 0, '$ORACLE_SID', '$SQL', 1, '$CYCLE', NULL, '$CTYPE', NULL);
commit;
spool off
quit
THEEND

$ARBORDIR/bin/BIP bip01 3

sqlplus arbor/${ARBOR_DB_PWD}@$ORACLE_SID << THEEND
spool $ARBORLOG/bip_c30stageInvoices.log
declare
x number;
begin
x := C30ARBOR.c30stageInvoices();
c30writeTransactionLog('Function c30stageInvoices completed '||case when x = 0 then 'successfully' else 'UNSUCCESSFULLY' end,case when x = 0 then 'info' else 'error' end);
end;
/
spool off
quit
THEEND
