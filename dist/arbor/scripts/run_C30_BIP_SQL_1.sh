#! /bin/sh
##################################################################
# R353312 12/07/2013 Migrated script run_GCI_BIP_SQL_1.sh to K3
##################################################################

.  $ARBORDIR/scripts/utility/decrypt_pw.sh

#
# Check to see if ORACLE_SID is set
if [ -z "${ORACLE_SID}" ]; then
   echo "ERROR: \$ORACLE_SID environment variable is not set\n"
   echo "This script requires that the \$ORACLE_SID environment variable be set.\n\n"
   exit 1
fi
#
# Check to see if ARBORDBU is set
if [ -z "${ARBORDBU}" ]; then
   echo "ERROR: \$ARBORDBU environment variable is not set\n"
   echo "This script requires that the \$ARBORDBU environment variable be set.\n\n"
   exit 1
fi
#
#  Verify that at least 1 entries exist
if [ $# -ne 1 ]; then
    echo 'USAGE: run_C30_BIP_SQL_1.sh <Spool File Name>'
    echo 'EXAMPLE: run_C30_BIP_SQL_1.sh get_server_id_20130101.log'
    exit 1
fi
ACCT_DB=$ORACLE_SID
ARBORDBU=$ARBORDBU
SID_SPOOL_FILE=$1
#
# Get the SERVER_ID from the SERVER_DEFINITION table
sqlplus -s $ARBORDBU/$ARBOR_DB_PWD\@$ACCT_DB <<- SQLPLUSEND >> /dev/null

set pages 0 head off feedback off trimspool on
  whenever sqlerror exit 1
  whenever oserror exit 2
  
spool $SID_SPOOL_FILE

select ltrim(server_id) from server_definition
where (lower(dsquery) = '$ACCT_DB' or upper(dsquery) = '$ACCT_DB') 
and server_id > 2;

spool off

exit

SQLPLUSEND
STATUS=$?
#
# Check for bad return code
if [ $STATUS -ne 0 ]; then
	echo "ERROR: SQLPLUS bad return code [$STATUS]"
  exit $STATUS
fi
#
# Check for Oracle SQL Errors in the log file
if grep ORA- $SID_SPOOL_FILE; then
  echo "ERROR: There was an Oracle error found in the log file."
  return 1
else
  echo "INFO: Log file contained no issues."
  #
  # Display information to use with UC4
  echo "LOGFILE: $SID_SPOOL_FILE"
  echo "COMPLETE: NO detected issues in process."
fi