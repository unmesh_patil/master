#! /bin/sh
##################################################################
# 
##################################################################

.  $ARBORDIR/scripts/utility/decrypt_pw.sh

#
# Check to see if ORACLE_SID is set
if [ -z "${ORACLE_SID}" ]; then
   echo "ERROR: \$ORACLE_SID environment variable is not set\n"
   echo "This script requires that the \$ORACLE_SID environment variable be set.\n\n"
   exit 1
fi
#
# Check to see if ARBORDBU is set
if [ -z "${ARBORDBU}" ]; then
   echo "ERROR: \$ARBORDBU environment variable is not set\n"
   echo "This script requires that the \$ARBORDBU environment variable be set.\n\n"
   exit 1
fi
#
#  Verify that at least 1 entries exist
if [ $# -ne 2 ]; then
    echo 'USAGE: run_C30_BIP_SQL_1.sh <Spool File Name>'
    echo 'EXAMPLE: run_C30_BIP_SQL_1.sh get_server_id_20130101.log'
    exit 1
fi
ACCT_DB=$ORACLE_SID
ARBORDBU=$ARBORDBU
SERVICEID=$1
USERID=$2

#> $ARBORDIR/data/log/jw_service_data.log

# Get the SERVICE data
sqlplus -s $ARBORDBU/$ARBOR_DB_PWD\@$ACCT_DB << THEEND  


set verify off
set heading off
set feedback off
set termout off
set serveroutput on
set pagesize 50000
set linesize 220
declare
  d_service_id varchar2(144) := '$SERVICEID';
  d_user_id varchar2(144) := '$USERID';
  d_external_id varchar2(144);
  d_host_external_id varchar2(144);
  d_subscr_no number(10);
  d_subscr_no_resets number(6);
  d_status_id number(6);
  d_status_descr varchar2(240);
  d_active_dt date;
  d_inactive_dt date;
  d_identifier_col c30arbor.c30identifiercol;
  d_is_host number(1);
  d_partner_count number(10);
  d_plan_col c30arbor.c30catalogcol;
  d_comp_col c30arbor.c30catalogcol;
  dcur sys_refcursor;
begin


dbms_output.enable(1000000);
dbms_output.put(CHR(10)); 
    
 c30arbor.c30getServiceInfo(
  v_transaction_id=>d_user_id||to_char(systimestamp,'YYYYMMDDHH24MISSFF'),
  v_acct_seg_id=>3,
  v_service_id=>d_service_id,
  c30getServiceInfo_cv=>dcur);
  loop
    fetch dcur into d_external_id, d_subscr_no, d_subscr_no_resets, d_status_id, d_status_descr, d_active_dt, d_inactive_dt, d_host_external_id, d_is_host, d_partner_count,
    d_identifier_col, d_plan_col, d_comp_col;
    exit when dcur%NOTFOUND;
    dbms_output.put_line('Service: '||d_external_id||'; '||d_subscr_no||'/'||d_subscr_no_resets||'; '||d_status_id||' - '||d_status_descr||'; '||d_active_dt||'; '||d_inactive_dt||'; Host:'||d_host_external_id||
      '; Host: '||d_is_host||'; Partners: '||d_partner_count);
  end loop;
  close dcur;
  if d_identifier_col.exists(1) then
    for i in 1 .. d_identifier_col.count loop
      dbms_output.put_line('Identifier: '||d_identifier_col(i).external_id||'; '||d_identifier_col(i).external_id_type||' - '||d_identifier_col(i).external_id_type_descr);
    end loop;
  end if;
  if d_plan_col.exists(1) then
    for i in 1 .. d_plan_col.count loop
      dbms_output.put_line('Plan: '||d_plan_col(i).catalog_id||'; '||d_plan_col(i).external_inst_id||'; '||d_plan_col(i).component_id||' - '||d_plan_col(i).component_descr);
    end loop;
  end if;
  if d_comp_col.exists(1) then
    for i in 1 .. d_comp_col.count loop
      dbms_output.put_line('Feature: '||d_comp_col(i).catalog_id||'; '||d_comp_col(i).external_inst_id||'; '||d_comp_col(i).component_id||' - '||d_comp_col(i).component_descr);
    end loop;
  end if;
end;

/ 
EXIT;


THEEND

