#!/bin/sh

. $ARBORDIR/scripts/utility/decrypt_pw.sh

rtn=0

EXT_ID=$1

if [ $# -ne 1 ]
then
    echo 'USAGE: mcap.sh <ext_contact_id>'
    exit 1
fi

PIDS=`pgrep -d "," MCAP`
SYSDATE=`date "+%Y%m%d%H%M%S"`
numProcs=0
procName="mcap11"

if [ -z "$PIDS" ] ; then
  `sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND > /dev/null 2>$1
  delete from PROCESS_SCHED where lower(process_name) like 'mcap%';
  delete from PROCESS_STATUS where lower(process_name) like 'mcap%';
  insert into PROCESS_SCHED values('mcap11', 'mcap11', 'N', 0, to_date('$SYSDATE','YYYYMMDDHH24MISS'), 86400, 0, 2, 0, '$ORACLE_SID', 'EXT_CONTACTS.ext_contact_id=$EXT_ID', 1, null, null, null, null);
  commit;

  quit
  THEEND`
else
  mcapProcess=`sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND
  set heading off feedback off verify off pages 0 tab off
  select 'mcap1'||to_char(substr(trim(max(process_name)),-1,1)+1)||':'||count(*) 
  from process_status
  where process_pid in ($PIDS);
  quit
  THEEND`

  numProcs=`echo "$mcapProcess" | sed -e "s/^.*://g"`
  procName=`echo "$mcapProcess" | sed -e "s/:.*$//g"`
fi


if [ $numProcs -ge 3 ]; then
  echo "  3 MCAP processes already running"
  exit 1
else
  `sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND > /dev/null 2>$1
  delete from PROCESS_SCHED where lower(process_name) = '$procName';
  delete from PROCESS_STATUS where lower(process_name) = '$procName';
  insert into PROCESS_SCHED values('$procName', '$procName', 'N', 0, to_date('$SYSDATE','YYYYMMDDHH24MISS'), 86400, 0, 2, 0, '$ORACLE_SID', 'EXT_CONTACTS.ext_contact_id=$EXT_ID', 1, null, null, null, null);
  commit;

  quit
  THEEND`
fi


nohup $ARBORDIR/bin/MCAP $procName 2 > /dev/null 2>&1 &
echo "  MCAP process $procName spawned pid: $!"
wait $!
rtn=$?
echo "  MCAP process $procName completed with an exit status: $rtn"
echo
if [ $rtn -gt 0 ] ; then
  echo "  MCAP ERROR...please investigate $procName log file"
  echo "  ERROR:MCAP"
  exit $rtn
fi

mcapStats=`sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND
  set heading off feedback off verify off pages 0 tab off
  select '  '||stats
  from c30arbor.c30_get_mps_stats
  where process_pid = $!
  and run_date = to_date('$SYSDATE','YYYYMMDDHH24MISS')
  order by process_id, cardinality;
  quit
  THEEND`

echo "$mcapStats"

