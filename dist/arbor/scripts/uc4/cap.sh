#!/bin/sh

. $ARBORDIR/scripts/utility/decrypt_pw.sh

rtn=0

EXT_ID=$1

if [ $# -ne 1 ]
then
    echo 'USAGE: cap.sh <ext_contact_id>'
    exit 1
fi

PIDS=`pgrep -d "," CAP`
SYSDATE=`date "+%Y%m%d%H%M%S"`
numProcs=0
procName="cap11"

if [ -z "$PIDS" ] ; then
  `sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND > /dev/null 2>$1
  delete from PROCESS_SCHED where lower(process_name) like 'cap%';
  delete from PROCESS_STATUS where lower(process_name) like 'cap%';
  insert into PROCESS_SCHED values('cap11', 'cap11', 'N', 0, to_date('$SYSDATE','YYYYMMDDHH24MISS'), 86400, 0, 2, 0, '$ORACLE_SID', 'EXT_CONTACTS.ext_contact_id=$EXT_ID', 1, null, null, null, null);
  commit;

  quit
  THEEND`
else
  capProcess=`sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND
  set heading off feedback off verify off pages 0 tab off
  select 'cap1'||to_char(substr(trim(max(process_name)),-1,1)+1)||':'||count(*) 
  from process_status
  where process_pid in ($PIDS);
  quit
  THEEND`

  numProcs=`echo "$capProcess" | sed -e "s/^.*://g"`
  procName=`echo "$capProcess" | sed -e "s/:.*$//g"`
fi


if [ $numProcs -ge 3 ]; then
  echo "  3 CAP processes already running"
  exit 1
else
  `sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND > /dev/null 2>$1
  delete from PROCESS_SCHED where lower(process_name) = '$procName';
  delete from PROCESS_STATUS where lower(process_name) = '$procName';
  insert into PROCESS_SCHED values('$procName', '$procName', 'N', 0, to_date('$SYSDATE','YYYYMMDDHH24MISS'), 86400, 0, 2, 0, '$ORACLE_SID', 'EXT_CONTACTS.ext_contact_id=$EXT_ID', 1, null, null, null, null);
  commit;

  quit
  THEEND`
fi


nohup $ARBORDIR/bin/CAP $procName 3 > /dev/null 2>&1 &
echo "  CAP process $procName spawned pid: $!"
wait $!
rtn=$?
echo "  CAP process $procName completed with an exit status: $rtn"
echo
if [ $rtn -gt 0 ] ; then
  echo "  CAP ERROR...please investigate $procName log file"
  echo "  ERROR:CAP"
  exit $rtn
fi

capStats=`sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND
  set heading off feedback off verify off pages 0 tab off
  select '  '||stats
  from c30arbor.c30_get_mps_stats
  where process_pid = $!
  and run_date = to_date('$SYSDATE','YYYYMMDDHH24MISS')
  order by process_id, cardinality;
  quit
  THEEND`

echo "$capStats"

