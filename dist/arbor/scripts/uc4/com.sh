#!/bin/sh

. $ARBORDIR/scripts/utility/decrypt_pw.sh

rtn=0

EXT_ID=$1

if [ $# -ne 1 ]
then
    echo 'USAGE: com.sh <ext_contact_id>'
    exit 1
fi

SYSDATE=`date "+%Y%m%d%H%M%S"`

sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND > /dev/null 2>$1
delete from PROCESS_SCHED where lower(process_name) = 'com11';
delete from PROCESS_STATUS where lower(process_name) = 'com11';
insert into PROCESS_SCHED values('com11', 'com11', 'N', 0, to_date('$SYSDATE','YYYYMMDDHH24MISS'), 86400, 0, 2, 0, '$ORACLE_SID', 'EXT_CONTACTS.ext_contact_id=$EXT_ID', 1, null, null, null, null);
commit;

quit
THEEND

nohup $ARBORDIR/bin/COM com11 2 > /dev/null 2>&1 &
echo "  COM process com11 spawned pid: $!"
wait $!
rtn=$?
echo "  COM process com11 completed with an exit status: $rtn"
echo
if [ $rtn -gt 0 ] ; then
  echo "  COM ERROR...please investigate com11 log file"
  echo "  ERROR:COM"
  exit $rtn
fi

comStats=`sqlplus -s arbor/${ARBOR_DB_PWD} << THEEND
  set heading off feedback off verify off pages 0 tab off
  select '  '||stats 
  from c30arbor.c30_get_mps_stats
  where process_pid = $!
  and run_date = to_date('$SYSDATE','YYYYMMDDHH24MISS')
  order by process_id, cardinality;
  quit
  THEEND`

echo "$comStats"


