#!/bin/sh
echo ${ARBORDIR}
.  ${ARBORDIR}/scripts/utility/decrypt_pw.sh

PROCESS=$1
SERVER=$2

rtn=0

if [ $# -ne 2 ]
then
 echo 'USAGE: uc4BIP.sh <process_name> <server_id>'
 exit 1;
fi

perl ${ARBORDIR}/scripts/utility/uc4BIP.pl ${ARBOR_DB_PWD} $PROCESS $SERVER

wait $!
rtn=$?

if [ $rtn -gt 0 ] ; then
  echo "UC4_PROCESS_ERROR BIP"
  exit $rtn
fi

exit $rtn