#! /bin/sh
##################################################################
# R241308 5/3/2011 updated select stmt to include sales_code PF
# I253224 09/13/2011 Removed the change made in R241308
# R353312 12/07/2013 Migrated script run_GCI_BIP_SQL_2.sh to K3
##################################################################

.  $ARBORDIR/scripts/utility/decrypt_pw.sh

#
# Check to see if ORACLE_SID is set
if [ -z "${ORACLE_SID}" ]; then
   echo "ERROR: \$ORACLE_SID environment variable is not set\n"
   echo "This script requires that the \$ORACLE_SID environment variable be set.\n\n"
   exit 1
fi
#
# Check to see if ARBORDBU is set
if [ -z "${ARBORDBU}" ]; then
   echo "ERROR: \$ARBORDBU environment variable is not set\n"
   echo "This script requires that the \$ARBORDBU environment variable be set.\n\n"
   exit 1
fi
#
#  Verify that at least 2 entries exist
if [ $# -ne 2 ]; then
    echo 'USAGE: run_C30_BIP_SQL_2.sh <Spool File Name> <Num of Bips>'
    echo 'EXAMPLE: run_C30_BIP_SQL_2.sh bip_load_bal_20130101.log 10'
    exit 1
fi
#
# Load values
ACCT_DB=$ORACLE_SID
ARBORDBU=$ARBORDBU
BIP_LOAD_BAL_LOG=$1
NUM_BIPS=$2
#
#  Drop and recreate the ARBOR.BG table, filling  it with accounts 
#   divided up into <Num of Bips>
sqlplus -s $ARBORDBU/$ARBOR_DB_PWD\@$ACCT_DB <<- SQLPLUSEND >> /dev/null

set echo on termout on timing on feedback on serveroutput on
  whenever sqlerror exit 1
  whenever oserror exit 2
  
spool $BIP_LOAD_BAL_LOG

declare
   c int;
begin
   select count(*) into c 
   from all_tables 
   where table_name = 'BG' and all_tables.owner = 'ARBOR';
   if c = 1 then
     execute immediate 'drop table ARBOR.BG';
     dbms_output.put_line('Table dropped.');
   else
     dbms_output.put_line('Table drop not needed.');
   end if;
end;
/

create table ARBOR.BG
as select hierarchy_id, account_no, mod(rownum,$NUM_BIPS)+1 GRP from cmf
/

create index ARBOR.bip_acct_grp_x on ARBOR.BG (grp)
/
spool off

exit

SQLPLUSEND
STATUS=$?
#
# Check for bad return code
if [ $STATUS -ne 0 ]; then
	echo "ERROR: SQLPLUS bad return code [$STATUS]"
  exit $STATUS
fi
#
# Check for Oracle SQL Errors in the log file
if grep ORA- $BIP_LOAD_BAL_LOG; then
  echo "ERROR: There was an Oracle error found in the log file."
  return 1
else
  echo "INFO: Log file contained no issues."
  #
  # Display information to use with UC4
  echo "LOGFILE: $BIP_LOAD_BAL_LOG"
  echo "COMPLETE: NO detected issues in process."
fi