#!/bin/sh
#
# $Author: Chip H.F. LaFurney         $
# $Date: 09/12/2013                   $
# $Revision: 1.0                      $
# $Author: clafurney                  $
# $Id: c30_settlement_feed_gen_csv.sh $
# $CA Ticket R325955                  $
#
.  ${ARBORDIR}/scripts/utility/decrypt_pw.sh
#
# Check to see if C30ARBOR_DB_PWD is set
if [ -z "${C30ARBOR_DB_PWD}" ] ; then
   echo "ERROR: \$C30ARBOR_DB_PWD environment variable is not set\n"
   echo "This script requires that the \$C30ARBOR_DB_PWD environment variable be set.\n\n"
   exit 1
fi
#
# Check to see if ORACLE_SID is set
if [ -z "${ORACLE_SID}" ] ; then
   echo "ERROR: \$ORACLE_SID environment variable is not set\n"
   echo "This script requires that the \$ORACLE_SID environment variable be set.\n\n"
   exit 1
fi
#
# The Oracle procedure has 2 optional parameters
if [[ $# -eq 1 && $1 = -h ]]
then
  echo 'USAGE: c30_settlement_feed_gen_csv.sh [<acct_seg_id>] [<put_line>]'
  echo 'If not specified, the acct_seg_id defaults to 3'
  echo 'Final put_line options: 0 (default) = output CSV file only; 1 = output to CSV and log file; 2 = output to log file only'
  exit 1;
fi
# start with defaults
ACCTSEGID=3
PUTLINE="null"
#
if [ $# -ge 1 ]
then
  ACCTSEGID=$1
fi
#
if [ $# -ge 2 ]
then
  PUTLINE=$2
fi
#
if [ $# -gt 2 ]
then
  echo 'For USAGE instructions enter "c30_settlement_feed_gen_csv.sh -h"'
  exit 1;
fi
#
# Fire up SQL Plus
sqlplus -S c30arbor/$C30ARBOR_DB_PWD\@$ORACLE_SID << THEEND
set echo off termout off feedback on trimspool on serveroutput on size 1000000 linesize 1000 pagesize 50000 tab off
whenever sqlerror exit 1
whenever oserror exit 2
prompt ...
prompt exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_cdr_data',$PUTLINE);;
exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_cdr_data',$PUTLINE);
prompt ...
prompt exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_acct_no',$PUTLINE);;
exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_acct_no',$PUTLINE);
prompt ...
prompt exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_unit_usage',$PUTLINE);;
exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_unit_usage',$PUTLINE);
prompt ...
prompt exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_svc_plan',$PUTLINE);;
exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'load_svc_plan',$PUTLINE);
prompt ...
prompt exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'generate_csv_files',$PUTLINE);;
exec c30_stm_feed.c30_sf_driver($ACCTSEGID,'generate_csv_files',$PUTLINE);
prompt output can be found in $ARBORDIR/cycbin/deployconfigs
prompt
exit rollback
THEEND