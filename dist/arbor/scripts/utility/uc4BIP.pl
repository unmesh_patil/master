#!/usr/bin/perl

use strict;
use DBI;
use DBD::Oracle qw(:ora_types);

if (! defined $ARGV[2]) {
 print "syntax: $0 <passwd> <process> <server>\n";
 exit 1;
}

my $db = $ENV{'DS_DATABASE'};
my $username = $ENV{'ARBORDBU'};
my $password = $ARGV[0];
my $process = $ARGV[1];
my $server = $ARGV[2];
my $rundate = `date "+%Y%m%d%H%M%S"`;
chomp ($rundate);
my %bipout;
$bipout{'UC4_BIP_QUEUED'} = 0;
$bipout{'UC4_BIP_SUCCESS'} = 0;
$bipout{'UC4_BIP_NO_ACTIVITY'} = 0;
$bipout{'UC4_BIP_LOW_BAL_SUPP'} = 0;
$bipout{'UC4_BIP_ERROR'} = 0;

my $bipstatus = 0;

my $dbstring = "dbi:Oracle:$db";
my $dbh = DBI->connect($dbstring,$username,$password,{AutoCommit => 0}) or die "couldn't connect to database: " . DBI->errstr;
my $sql = "select lower(trim(plat_id))||lower(trim(usg_plat_id)) from arbor.process_sched where lower(trim(process_name)) = lower(trim(?))";
my $sth = $dbh->prepare($sql) or die "couldn't prepare statement: " . $dbh->errstr;
my @data1 = @{execute_sql($sth, $process)};

if (my $subpid = fork()) {
 my $bippid = wait();
 $bipstatus = $?;
 #print "BIP pid $bippid status $?\n";
 #print "BIP pid $subpid\n";
 
 my $stats;
 $sql = "select process_id from c30arbor.c30_pid_ref where cycle_id = ? and process_pid = ?";
 $sth = $dbh->prepare($sql) or die "couldn't prepare statement: " . $dbh->errstr;
 my @data2 = @{execute_sql($sth, $data1[0][0], $bippid)};
 $sql = "begin c30arbor.c30_get_bip_stats(vCycleID=>:cycle, vProcessID=>:procid, vStatsCV=>:stats); end;";
 $sth = $dbh->prepare($sql) or die "couldn't prepare statement: " . $dbh->errstr;
 $sth->bind_param(":cycle", $data1[0][0]);
 $sth->bind_param(":procid", $data2[0][0]);
 $sth->bind_param_inout(":stats", \$stats, 0, {ora_type=>ORA_RSET});        
 $sth->execute() or $dbh->rollback and die "couldn't execute statement: " . $sth->errstr;
 
 while ((my $statuscode, my $count) = $stats->fetchrow_array()) {
  if ($statuscode == 9) {
   $bipout{'UC4_BIP_QUEUED'} = $count;
  } elsif ($statuscode == 1) {
   $bipout{'UC4_BIP_SUCCESS'} = $count;
  } elsif ($statuscode == 2) { 
   $bipout{'UC4_BIP_NO_ACTIVITY'} = $count;
  } elsif ($statuscode == 5) {
   $bipout{'UC4_BIP_LOW_BAL_SUPP'} = $count;
  } elsif ($statuscode == -1) {
   $bipout{'UC4_BIP_ERROR'} = $count;
  }
 }
 $sth->finish();
 $dbh->disconnect();
 print "UC4_BIP_CYCLEID $data1[0][0]\n";
 print "UC4_BIP_PROCESS $process $data2[0][0]\n";
 foreach my $key (keys %bipout) {
  print "$key $bipout{$key}\n";
 } 
} elsif (defined $subpid) {
 #print $ENV{'ARBORDIR'} . '/bin/BIP', $process, $server . "\n";
 close(STDOUT);
 close(STDERR);
 exec ($ENV{'ARBORDIR'} . '/bin/BIP', $process, $server);
} else {
 die;
}

exit $bipstatus;
 
sub execute_sql {
 my ($sth,@bind_params)  = @_;
 $sth->execute(@bind_params) or die "couldn't execute statement: " . $sth->errstr;
 my $data = $sth->fetchall_arrayref();
 return $data;
}
