#!/bin/bash
#
# uncomment DEPLOYNOMAIL to send summary email only to deployer
#DEPLOYNOMAIL="true"

# FORCE_DEPLOY=true will turn off pre-processing of what components need to be deployed
# and attempt to deploy all types of component
#FORCE_DEPLOY="true"

DATE=`date +%Y%m%d`
LOGFILE=/tmp/deploy.$DATE.$$.log
MAILFILE=/tmp/deploy.$DATE.$$.mail
REPOSLOG=/tmp/deploy.$DATE.$$.repos
MAIL_SUBJECT_PREFIX="[RELEASE] "

CACHECOREFILE=.deploycache
CACHEDIR=~/cycbin/tmp
CACHEFILE=$CACHEDIR/$CACHECOREFILE.$PPID
CACHEENCFILE=$CACHEFILE.gpg
CACHEWILDCARD=$CACHECOREFILE*

if [ $# -lt 1 ]; then
    echo "Usage: $0 <REVISION> [<BRANCHTAG>]"
    exit 1
else
  if [ $# -eq 2 ]; then
# Revision and branch tag is given
    REVISION=$1
    RELEASE_TAG=$2
    SVN_BASE=branches/$2
  else
# Only a revision is given
    REVISION=$1
    SVN_BASE=trunk
  fi
fi

RELEASELISTFILE=/tmp/contents.$REVISION.$RELEASE_TAG.$DATE.$$

ARBORDIR=/opt/app/kenanfx2/`whoami`
. $ARBORDIR/cycbin/env.sh

#create cache dir
if [ ! -d "$CACHEDIR" ]; then
    mkdir $CACHEDIR
fi

#delete the cache if older than 10 minutes
find $CACHEDIR -name "$CACHEWILDCARD" -type f -mmin +10 -delete

#attempt to use cached credentials
ACTIVECACHE=false
if [ -f $CACHEENCFILE ]; then
    echo "Using cached credentials"
    old_IFS=$IFS
    IFS=$'\n'
    lines=($(echo '$C30APPENCKEY$PPID' | gpg -q --passphrase-fd 0 --no-tty --force-mdc -d $CACHEENCFILE))
    IFS=$old_IFS

    SVN_USER=${lines[0]}
    PASSWD=${lines[1]}
    EMAILADDR=${lines[2]} 
    ACTIVECACHE=true
fi


echo "============================================================"
echo "Generating list of files for the distribution."
echo "============================================================"
OLDPWD=`pwd`

echo "Enter SVN User ID:"
if [ "$ACTIVECACHE" != "true" ]; then
  read SVN_USER
else
  echo $SVN_USER
fi

if [ "$ACTIVECACHE" != "true" ]; then
  echo "Enter password for SVN user:"
#trap "stty echo; exit" 1 2 15

  stty_orig=`stty -g`
  stty -echo
  read PASSWD
  stty $stty_orig
#trap "" 1 2 15
fi
export SVN_PASSWORD=$PASSWD

export ROOT=https://subversion.assembla.com/svn/c30billing/awn
ORIGDIR=`pwd`
DISTDIR=/tmp/dist.$$ 
mkdir -p $DISTDIR
cd $DISTDIR
SHORTREV=`expr $1 - 1`;

echo "Extracting build artifacts"

# use "| sed -e "/^D[ \t]*.*/d" | " to remove deleted files from the list
svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $SHORTREV:$REVISION $ROOT/kenan/all/$SVN_BASE/core | sed -e "s/^[AM][AM]*[ \t]*//" | sed -e "s/^ [AM][ \t]*//" | sed -e "s|$ROOT/kenan/all/$SVN_BASE/core/||" > $RELEASELISTFILE
svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $SHORTREV:$REVISION $ROOT/kenan/m2m/$SVN_BASE/core | sed -e "s/^[AM][AM]*[ \t]*//" | sed -e "s/^ [AM][ \t]*//" | sed -e "s|$ROOT/kenan/m2m/$SVN_BASE/core/||" >> $RELEASELISTFILE
svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $SHORTREV:$REVISION $ROOT/integration/dist/$SVN_BASE | sed -e "s/^[AM][AM]*[ \t]*//" | sed -e "s/^ [AM][ \t]*//" | sed -e "s|$ROOT/integration/dist/$SVN_BASE/||" >> $RELEASELISTFILE
cd $OLDPWD
rm -rf $DISTDIR
sort -o $RELEASELISTFILE $RELEASELISTFILE

echo "We are deploying the following:"
cat $RELEASELISTFILE

echo "Hit enter to continue or ^C to exit."
read CONTINUE

echo "Enter 'Y' to use default email [`logname`@gci.com] or enter your email address to continue :"
if [ "$ACTIVECACHE" != "true" ]; then
    EMAILADDR=""
    while [ -z $EMAILADDR ]; do
        read EMAILADDR
    done

    if [ "$EMAILADDR" == "Y" -o "$EMAILADDR" == "y" ]; then
      EMAILADDR=`logname`@gci.com
    fi
fi

if [ "X$DEPLOYNOMAIL" = "X" ]; then
    MAIL_TO="Cycle30-C3BEnvironmentTeam@gci.com"
#    MAIL_CC="ovsysmon@gci.com"
else
    MAIL_TO="$EMAILADDR"
fi

START_TIME=`date`

echo " " | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
echo "Executing Arbor script deploys on $ARBOR_SERVER."             | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
if [ $(grep -c "^arbor" $RELEASELISTFILE) -gt 0 -o "$FORCE_DEPLOY" = "true" ]; then
    deployscripts.sh $REVISION $SVN_USER $SVN_PASSWORD $RELEASE_TAG 2>&1 | tee -a $LOGFILE
else
    echo "Nothing to deploy for Arbor script" | tee -a $LOGFILE
fi

echo " " | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
echo "Executing WorkPoint deployment on $ARBOR_SERVER."             | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
if [ $(grep -c "^Weblogic" $RELEASELISTFILE) -gt 0 -o "$FORCE_DEPLOY" = "true" ]; then
    deployworkpoint.sh $REVISION $SVN_USER $SVN_PASSWORD $RELEASE_TAG 2>&1 | tee -a $LOGFILE
else
    echo "Nothing to deploy for WorkPoint" | tee -a $LOGFILE
fi

echo " " | tee -a $LOGFILE
echo "============================================================"  | tee -a $LOGFILE
echo "Executing database deployment as $ARBOR_USER on $ARBOR_SERVER." | tee -a $LOGFILE
echo "============================================================"  | tee -a $LOGFILE
if [ $(grep -c "^database" $RELEASELISTFILE) -gt 0 -o "$FORCE_DEPLOY" = "true" ]; then
    if [ -z $RELEASE_TAG ]; then
       deploydatabase.pl -r $REVISION -l $LOGFILE -u $SVN_USER -p $SVN_PASSWORD | tee -a $LOGFILE
    else
       deploydatabase.pl -r $REVISION -t $RELEASE_TAG -l $LOGFILE -u $SVN_USER -p $SVN_PASSWORD | tee -a $LOGFILE
    fi
else
    echo "Nothing to deploy for database" | tee -a $LOGFILE
fi

echo " " | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
echo "Executing batch deployment as $BATCH_USER on $BATCH_SERVER."   | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
echo " " | tee -a $LOGFILE
if [ $(grep -c "^batch" $RELEASELISTFILE) -gt 0 -o "$FORCE_DEPLOY" = "true" ]; then
    ssh $BATCH_USER@$BATCH_SERVER "cycbin/deploybatch.sh $REVISION $SVN_USER $SVN_PASSWORD $RELEASE_TAG" 2>&1 | tee -a $LOGFILE
else
    echo "Nothing to deploy for batch" | tee -a $LOGFILE
fi

echo " " | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
echo "Executing API deployment as $MULEAPI_USER on $MULEAPI_SERVER."   | tee -a $LOGFILE
echo "============================================================" | tee -a $LOGFILE
echo " " | tee -a $LOGFILE
if [ $(grep -c "^API" $RELEASELISTFILE) -gt 0 -o "$FORCE_DEPLOY" = "true" ]; then
    ssh $MULEAPI_USER@$MULEAPI_SERVER "cycbin/deployapi.sh $REVISION $SVN_USER $SVN_PASSWORD $RELEASE_TAG" 2>&1 | tee -a $LOGFILE
else
    echo "Nothing to deploy for Mule/API" | tee -a $LOGFILE
fi

END_TIME=`date`

#
# Generate email with the results of the deployment.  We need to ensure that none of the
# output includes a password or it will be mailed plaintext.
# 

echo "============================================================"
echo "Generating summary email."
echo "============================================================"
# Create header
cat << EOF > $MAILFILE
To: $MAIL_TO
CC: $MAIL_CC
From: $EMAILADDR
Reply-To: $EMAILADDR
Subject: ${MAIL_SUBJECT_PREFIX}Release of $REVISION to $SYSTEM_NAME $SYSTEM_TYPE$ENVIRONMENT

The release of $REVISION to the $SYSTEM_NAME $SYSTEM_TYPE$ENVIRONMENT environment is complete.

Deployer: $EMAILADDR

Start time: $START_TIME

End time: $END_TIME

Release revision: $REVISION

EOF

# append RELEASE_TAG
if [ $# -eq 2 ]; then
   echo "Release tag: $RELEASE_TAG" >> $MAILFILE
fi

# append release contents
echo "Release contents:" >> $MAILFILE
echo "" >> $MAILFILE
cat $RELEASELISTFILE >> $MAILFILE
rm -f $RELEASELISTFILE

# Append results
echo "Output results:" >> $MAILFILE
echo "" >> $MAILFILE

cat $LOGFILE >> $MAILFILE

# Verify revision info information exists (for CFG) 
if [ -f $HOME/cycbin/load_rev.pl ]; then
  if [ -z $RELEASE_TAG ]; then
    TRUNK_BRANCH_FLAG=""
  else
    TRUNK_BRANCH_FLAG="-t $RELEASE_TAG"
  fi
  $HOME/cycbin/load_rev.pl $REVISION $TRUNK_BRANCH_FLAG -u $SVN_USER -p $SVN_PASSWORD
fi

# Update the CM/RM repository with the log deploy details
echo "" >> $REPOSLOG
ENV=$ENVIRONMENT
java -jar $HOME/cycbin/rmrepository/RmRepository.jar $REVISION $SYSTEM_NAME $SYSTEM_TYPE$ENV ARBOR $MAILFILE | tee -a $REPOSLOG

# Send mail
cat $REPOSLOG >> $MAILFILE
cat $MAILFILE | /usr/lib/sendmail -oi -t  

#store user state temporarily
echo $SVN_USER > $CACHEFILE
echo $SVN_PASSWORD >> $CACHEFILE
echo $EMAILADDR >> $CACHEFILE

#remove existing credentials 
if [ -f $CACHEENCFILE ]; then
    rm $CACHEENCFILE
fi

#encrypt credentials
`echo '$C30APPENCKEY$PPID' | gpg --batch --passphrase-fd 0 --symmetric --force-mdc $CACHEFILE`

#clean up files
rm -f $CACHEFILE
rm -f $MAILFILE
#rm -f $LOGFILE
rm -f $REPOSLOG

