#!/bin/sh





#Default outcome value. Used for the subject line of the email
OUTCOME="is complete"

#enable/disable mail notification (set to false to disable)
SENDEMAIL=true

CONFIRM=true

START_TIME=`date`

USERDIR=/opt/app/kenanfx2/`whoami`

. $USERDIR/cycbin/envconf.sh

echo "top of script" 
echo "mode: $MODE"

if [ "X${MODE}" != "Xstatus" -a "X${CONFIRM}" = "Xtrue" ]; then
  echo -n "Are you sure you want to take this action (Y/N)? "
  read VERACTION
  if [[ $VERACTION != [Yy] ]]; then
    echo "Action not confirmed.  Exiting."
    exit 0
  fi
fi

echo Begin ${MODE} of \'${ENVIRONMENT}\' at $START_TIME

ARBORDIR=/opt/app/kenanfx2/${ARBOR_USER}
cd $ARBORDIR/appdir/log







#Setup baton directory
BATONDIR=/opt/app/kenanfx2/arbor/cycbin/envctl_batons

#If it baton directory doesn't exist, create it.
if [ ! -d "${BATONDIR}" ]; then
	mkdir "${BATONDIR}"
fi

#####################
#   functions()   ###
#####################


createBaton()
{
	touch "${BATONDIR}/baton.$1"
	
}

removeBaton()
{
	if [ -f "${BATONDIR}/baton.$1" ]; then
       rm "${BATONDIR}/baton.$1"
	   
	fi
}


#checkStop "Error Message" "subsystem" "STOP/START"
checkStop()
{
ret=$?
if [ "$ret" -ne 0 ]; then

	echo "ERROR: $1; Exit Code: $ret"
	outcomeVal="ERROR: $1; Exit Code: $ret"
	OUTCOME="has failed"

else
	#checks for stop vs start
	case $3 in
		"0")
			#on successful STOP, create baton.
			createBaton "$2"
			outcomeVal="stop initiated"
			
		;;
		"1")
			#on successful START, remove baton.
			removeBaton "$2"
			outcomeVal="start initiated"
		;;
		*)
			#some other invalid value.
			echo "not a valid value for the STARTUP variable"
		;;
	esac

fi 
}







################
# SHUTDOWN     #
################



if [ "X${MODE}" = "Xstop" -o "X${MODE}" = "Xrestart" ]; then
STARTUP=0




    #shutdown @@@@@
    #if [ "X${START_@@@@@}" = "Xtrue" ]; then
    # shutdown @@@@@
    #    echo Shutting down @@@@@ on ${@@@@@}
    #    ~~ stop command here ~~
    #	checkStop "stop failed: @@@@@ on ${@@@@@}" "@@@@@" $STARTUP
    #	OUTCOME_@@@@@=$outcomeVal
    #fi






    #Stop Batch Agent 
    if [ "X${START_BATCHAGENT}" = "Xtrue" ]; then
	
        echo Shutting down BatchAgent on ${BATCH_SERVER}
		GETPID=`ps -ef | grep batch | grep -v grep | grep BatchFlowAgent | cut -c10-14`
        CHECK=`echo $GETPID`
        if [ "X${CHECK}" != "X" ] ; then
            ssh ${BATCH_USER}@${BATCH_SERVER} "ps -ef | grep batch | grep -v grep | grep BatchFlowAgent | cut -c10-14 | xargs kill -9"
        fi			
		checkStop "stop failed: Batch Server on ${BATCH_SERVER}" "ba" $STARTUP
		OUTCOME_BATCHAGENT=$outcomeVal
	
    fi

	
	
	
	
	
	
	
	
    #Stop Mule API Client-Updater
    if [ "X${START_MULE_CU}" = "Xtrue" ]; then

        echo Shutting down API client-updater on ${MULEAPI_SERVER}
        ssh ${MULEAPI_USER}@${MULEAPI_SERVER} "cd client-updater/bin ; ./clientupdctl.sh stop"
		echo $?
        checkStop "stop failed: API Client-Updater on ${MULEAPI_SERVER}" "cu" $STARTUP
        OUTCOME_MULE_CU=$outcomeVal

    fi
	
	
	
	
	
	
	
    #Stop Mule API 
    if [ "X${START_MULE_API}" = "Xtrue" ]; then

        # shutdown Mule API Server
        echo Shutting down API on ${MULEAPI_SERVER}
        ssh ${MULEAPI_USER}@${MULEAPI_SERVER} "./bin/fxwebctl.sh stop"
        checkStop "stop failed: Mule API Server on ${MULEAPI_SERVER}" "api" $STARTUP
        OUTCOME_MULE_API=$outcomeVal

    fi

	
	
	
	
	
	
    #Stop Security UI 
    if [ "X${START_SECUI}" = "Xtrue" ]; then

          if [ -d $SEC_HOME/ui ] ; then
            # shut down security server UI
            echo Shutting down security server UI on ${ARBOR_SERVER}
            (cd $SEC_HOME/ui/jakarta-tomcat-5.5.9/bin; ./shutdown.sh)
            sleep 15
          else
            echo *** Security Server UI not installed ***
          fi
          checkStop "stop failed: Security UI on ${ARBOR_SERVER}" "secui" $STARTUP
          OUTCOME_SECUI=$outcomeVal

    else
        echo Skipping security server shutdown UI on ${ARBOR_SERVER}.
    fi

	
	
	
	
	
	
	
	
    #Stop Security Server	
    if [ "X${START_SECSVR}" = "Xtrue" ]; then

          #shut down security server
          echo Shutting down security server on ${ARBOR_SERVER}
          (cd $SEC_HOME/server; ./s.stop)
          checkStop "stop failed: Security Server on ${ARBOR_SERVER}" "secsvr" $STARTUP
          OUTCOME_SECSVR=$outcomeVal

    else
      echo Skipping security server shutdown on ${ARBOR_SERVER}.
    fi

	
	
	
	
	
	
    #Stop Middleware 
    if [ "X${START_MW}" = "Xtrue" ]; then


          # shutdown middleware
          echo Shutting down middleware on ${ARBOR_SERVER}
          FX_Control stop ALL
          GETPID=`ps -ef | grep BBL | grep -v grep | grep kenanfx2 | cut -c10-14`
          CHECK=`echo $GETPID`
          if [ "X${CHECK}" != "X" ] ; then
            echo Killing Middleware services
# exclude LTP which is not currently controlled by envctl.sh (djw, 5/23/2011) ~leave commented
            ps -ef | grep arbor | grep -v grep | grep kenanfx2 | grep -v LTP | cut -c10-14 | xargs kill -9
            ps -ef | grep arbor | grep -v grep | grep WSH | cut -c10-14 | xargs kill -9
# give kill the time to finish the job before running cleanup ~ leave commented
            sleep 3
              echo Running cleanup script
            (cd $HOME/scripts; cleanIPC.sh)
			fi

		checkStop "stop failed: Middleware on ${ARBOR_SERVER}" "mw" $STARTUP
		OUTCOME_MW=$outcomeVal


    else
        echo Skipping middleware shutdown on ${ARBOR_SERVER}.
    fi

	
	
	
	
	
	
	
	
	
    #Stop Weblogic	
    if [ "X${START_WL}" = "Xtrue" ]; then

          # shut down WebLogic
          echo Shutting down WebLogic on ${ARBOR_SERVER}
          (cd $HOME/bea/Weblogic/user_projects/domains/WorkPoint; ./stopWebLogic.sh)
          GETDONE=`grep "Disconnected from weblogic server: WorkPointServer" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
          ERRORCONNECT=`grep "There is no server running" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
          TimeoutIncrement=10
          TimeoutMax=24
		   TimeoutCounter=$TimeoutMax
          while [ "X$GETDONE" == "X" -a "X$ERRORCONNECT" == "X" -a "$TimeoutCounter" -gt "0" ]
          do
              echo Waiting for WebLogic to shutdown ${TimeoutCounter}...
              sleep $TimeoutIncrement
          TimeoutCounter=$(($TimeoutCounter - 1))
              GETDONE=`grep "Disconnected from weblogic server: WorkPointServer" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
              ERRORCONNECT=`grep "There is no server running" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
          done
          if [ "X$GETDONE" = "X" -a "X$ERRORCONNECT" = "X" ]; then
              GETPID=`ps -ef | grep Weblogic | grep java | grep -v shutdown | grep -v grep | awk '{ print $2 ; }'`
              GETDONE="Tired of waiting for Weblogic to shutdown..."
              ERRORCONNECT="Killing Weblogic"
              kill -9 $GETPID
          fi
          echo $GETDONE
          echo $ERRORCONNECT
		  
		checkStop "stop failed: Weblogic on ${ARBOR_SERVER}" "wl" $STARTUP
        OUTCOME_WL=$outcomeVal  
		
    else
          echo Skipping WebLogic on ${ARBOR_SERVER}.
    fi
	
	
	
	
	
	
	#Clean Logs
    if [ "X${CLEAN_LOGS}" = "Xtrue" ]; then
      echo Cleaning up log files on ${ARBOR_SERVER}
      find /opt/app/kenanfx2/${ARBOR_USER}/appdir/log -type f -print -exec rm -f {} \;
    else
      echo Skipping cleaning up log files on ${ARBOR_SERVER}.
    fi
fi







################
# STARTUP      #
################


	
	if [ "X${MODE}" = "Xstart" -o "X${MODE}" = "Xrestart" -o "X${MODE}" = "Xforcestart" ]; then
	STARTUP=1

		##START @@@@@
		   
		# if [ "X${START_@@@@@}" = "Xtrue" ]; then

			# check for baton. If it doesn't exist, skip
			#if [ ! -f "${BATONDIR}/baton.@@@@@" ]; then


				#Forcestart 
				# if [ "X${MODE}" = "Xforcestart" ]; then

					#read -p "Are you sure you want to force-start @@@@@? Yes(Y/y) or No(N/n): " answer

					#if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						#echo "@@@@@ has been force-started!"
						#~~ forcestart procedure here ~~
						#checkStop "stop failed: @@@@@ on ${@@@@@}" "@@@@@" $STARTUP
						#OUTCOME_@@@@@="forcestart of @@@@@ initiated"

					#else

						#cancel forcestart
						#echo "forcestart for @@@@@ has been canceled."
						#OUTCOME_@@@@@="no forcestart of @@@@@ initiated, canceled by operator."
						#OUTCOME="has been canceled"

					#fi


				#else

					#skip subsystem
					#echo "@@@@@ on ${@@@@@} is already running. Skipping"
					#OUTCOME_@@@@@="skipped: system already on"
					#OUTCOME="has failed"

				#fi

			#else

				# normal startup @@@@@
				#~~ normal start procedure here ~~ 
				#checkStop "stop failed: @@@@@ on ${@@@@@}" "@@@@@" $STARTUP
				#OUTCOME_@@@@@=$outcomeVal
			#fi
		#else
			#echo Skipping @@@@@ startup on ${@@@@@}.
		#fi




		
		#Start weblogic
		if [ "X${START_WL}" = "Xtrue" ]; then

			# check for baton. If it doesn't exist, skip
			if [ ! -f "${BATONDIR}/baton.wl" ]; then


				#Forcestart 
				if [ "X${MODE}" = "Xforcestart" ]; then

					read -p "Are you sure you want to force-start WebLogic? Yes(Y/y) or No(N/n): " answer

					if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						echo force-starting up WebLogic on ${ARBOR_SERVER}
						  (cd $HOME/bea/Weblogic/user_projects/domains/WorkPoint; ./startWebLogic.sh)
						  echo Waiting for Weblogic to complete starting
						  export GETSTARTED=`grep "Server state changed to RUNNING" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
						  export GETFAILED=`grep "Server state changed to FAILED" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
						  while [ "X$GETSTARTED" == "X" -a "X$GETFAILED" == "X" ]
						  do
							echo Waiting for WebLogic to complete startup...
							sleep 15
							export GETSTARTED=`grep "Server state changed to RUNNING" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
						export GETFAILED=`grep "Server state changed to FAILED" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
						  done
						  echo $GETFAILED
						  echo $GETSTARTED

						  if [ "X$GETFAILED" != "X" ]; then
							echo ************************************
							echo * WorkPoint Server Failed to Start *
							echo ************************************
						  else
							TimeoutInc=5
							TimeoutMax=12
							TimeoutCounter=$TimeoutMax
							export GETSTARTED=`grep "AlertMonitor: Connect attempt" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
							export ISDONE=`grep "AlertMonitor: Becoming master" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
							while [ "X$GETSTARTED" == "X" -a "X$ISDONE" == "X" -a "$TimeoutCounter" -gt "0" ]
							do
							  echo Waiting for WebLogic to complete phase 2 startup ${TimeoutCounter}...
							  sleep $TimeoutInc
							  TimeoutCounter=$(($TimeoutCounter - 1))
							  export GETSTARTED=`grep "AlertMonitor: Connect attempt" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
							  export ISDONE=`grep "AlertMonitor: Becoming master" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
							done
						
							if [ "X$ISDONE" != "X" ]; then
							  echo "**************************************************"
							  echo "* SUCCESS: WorkPoint Server started successfully *"
							  echo "**************************************************"
							elif [ "X$GETSTARTED" != "X" ]; then
							  echo "****************************************************"
							  echo "* SUCCESS: Weblogic is attempting to become master *"
							  echo "****************************************************"
							else
							  echo "**************************************************************"
							  echo "* FAIL: WorkPoint Server Failed to start correctly (phase 2) *"
							  echo "**************************************************************"
							fi
						  fi
						checkStop "stop failed: Weblogic on ${ARBOR_SERVER}" "wl" $STARTUP
						OUTCOME_WL="forcestart of Weblogic initiated"

					else

						#cancel forcestart
						echo "forcestart for Weblogic has been canceled."
						OUTCOME_WL="no forcestart of Weblogic initiated, canceled by operator."
						OUTCOME="has been canceled"

					fi


				else

					#skip subsystem
					echo "Weblogic on ${ARBOR_SERVER} is already running. Skipping"
					OUTCOME_WL="skipped: system already on"
					OUTCOME="has failed"

				fi

			else

				# normal startup Weblogic
				# start up WebLogic
				  echo Starting up WebLogic on ${ARBOR_SERVER}
				  (cd $HOME/bea/Weblogic/user_projects/domains/WorkPoint; ./startWebLogic.sh)
				  echo Waiting for Weblogic to complete starting
				  export GETSTARTED=`grep "Server state changed to RUNNING" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
				  export GETFAILED=`grep "Server state changed to FAILED" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
				  while [ "X$GETSTARTED" == "X" -a "X$GETFAILED" == "X" ]
				  do
					echo Waiting for WebLogic to complete startup...
					sleep 15
					export GETSTARTED=`grep "Server state changed to RUNNING" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
					export GETFAILED=`grep "Server state changed to FAILED" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
				  done
				  echo $GETFAILED
				  echo $GETSTARTED
				
				  if [ "X$GETFAILED" != "X" ]; then
					echo ************************************
					echo * WorkPoint Server Failed to Start *
					echo ************************************
				  else
					TimeoutInc=5
					TimeoutMax=12
					TimeoutCounter=$TimeoutMax
					export GETSTARTED=`grep "AlertMonitor: Connect attempt" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
					export ISDONE=`grep "AlertMonitor: Becoming master" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
					while [ "X$GETSTARTED" == "X" -a "X$ISDONE" == "X" -a "$TimeoutCounter" -gt "0" ]
					do
					  echo Waiting for WebLogic to complete phase 2 startup ${TimeoutCounter}...
					  sleep $TimeoutInc
					  TimeoutCounter=$(($TimeoutCounter - 1))
					  export GETSTARTED=`grep "AlertMonitor: Connect attempt" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
					  export ISDONE=`grep "AlertMonitor: Becoming master" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
					done
				
					if [ "X$ISDONE" != "X" ]; then
					  echo "**************************************************"
					  echo "* SUCCESS: WorkPoint Server started successfully *"
					  echo "**************************************************"
					elif [ "X$GETSTARTED" != "X" ]; then
					  echo "****************************************************"
					  echo "* SUCCESS: Weblogic is attempting to become master *"
					  echo "****************************************************"
					else
					  echo "**************************************************************"
					  echo "* FAIL: WorkPoint Server Failed to start correctly (phase 2) *"
					  echo "**************************************************************"
					fi
				  fi

				checkStop "stop failed: Weblogic on ${ARBOR_SERVER}" "wl" $STARTUP
				OUTCOME_WL=$outcomeVal

			fi

		else
		  echo Skipping WebLogic startup on ${ARBOR_SERVER}.
		fi
		
		
		
		
		
	
		# start middleware
		if [ "X${START_MW}" = "Xtrue" ]; then

			# check for baton. If it doesn't exist, skip
			if [ ! -f "${BATONDIR}/baton.mw" ]; then


				#Forcestart 
				if [ "X${MODE}" = "Xforcestart" ]; then

					read -p "Are you sure you want to force-start Middleware? Yes(Y/y) or No(N/n): " answer

					if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						echo "force-starting middleware"	
						#        echo Rebuilding middleware tuxconfig on ${ARBOR_SERVER}	~leave commented
						#               cat /opt/app/kenanfx2/${ARBOR_USER}/appdir/KenanFX.ubb | \	~leave commented
						#           sed s/@HOST@/`uname -n`/g  | tmloadcf -y -	~leave commented
						#
						#               rm -f /opt/app/kenanfx/${ARBOR_USER}/appdir/TLOG	~leave commented
						#               tmadmin << EOTMADMINCMD	~leave commented
						#crdl -z /opt/app/kenanfx/${ARBOR_USER}/appdir/TLOG	~leave commented
						#crlog -m FX	~leave commented
						#quit	~leave commented
						#EOTMADMINCMD	~leave commented
					  echo force-starting up middleware on ${ARBOR_SERVER}
					  FX_Control start ALL
					  #verify WorkPoint middleware is running ~leave commented
					  GETPID=`ps -ef | grep wkp_ | grep -v grep | awk '{ print $2 ; }'`
					  CHECK=`echo $GETPID`
					  if [ "X${CHECK}" = "X" ] ; then
							echo Starting WorkPoint middleware services
							FX_Control start WKP
					  fi
					  # Pause a few seconds to allow middleware to start completely to avoid potential errors ~leave commented
					  # with BatchRefresh startup	~leave commented
					  sleep 3
					  checkStop "stop failed: Middleware on ${ARBOR_SERVER}" "mw" $STARTUP
					  OUTCOME_Middleware="forcestart of Middlware initiated"

					else

						#cancel forcestart
						echo "forcestart for Middlware has been canceled."
						OUTCOME_MW="no forcestart of Middlware initiated, canceled by operator."
						OUTCOME="has been canceled"

					fi


				else

					#skip subsystem
					echo "Middleware on ${ARBOR_SERVER} is already running. Skipping"
					OUTCOME_MW="skipped: system already on"
					OUTCOME="has failed"

				fi

			else

				# normal startup @@@@@
				echo "starting middleware"
				#        echo Rebuilding middleware tuxconfig on ${ARBOR_SERVER}	~leave commented
				#               cat /opt/app/kenanfx2/${ARBOR_USER}/appdir/KenanFX.ubb | \	~leave commented
				#           sed s/@HOST@/`uname -n`/g  | tmloadcf -y -	~leave commented
				#
				#               rm -f /opt/app/kenanfx/${ARBOR_USER}/appdir/TLOG	~leave commented
				#               tmadmin << EOTMADMINCMD	~leave commented
				#crdl -z /opt/app/kenanfx/${ARBOR_USER}/appdir/TLOG	~leave commented
				#crlog -m FX	~leave commented
				#quit	~leave commented
				#EOTMADMINCMD	~leave commented
				  echo Starting up middleware on ${ARBOR_SERVER}
				  FX_Control start ALL
				  #verify WorkPoint middleware is running ~leave commented
				  GETPID=`ps -ef | grep wkp_ | grep -v grep | awk '{ print $2 ; }'`
				  CHECK=`echo $GETPID`
				  if [ "X${CHECK}" = "X" ] ; then
						echo Starting WorkPoint middleware services
						FX_Control start WKP
				  fi
				  # Pause a few seconds to allow middleware to start completely to avoid potential errors ~leave commented
				  # with BatchRefresh startup ~leave commented
				  sleep 3
					checkStop "stop failed: Middleware on ${ARBOR_SERVER}" "mw" $STARTUP
					OUTCOME_MW=$outcomeVal
					
					
			fi
		
	
		else
		  echo Skipping middleware startup on ${ARBOR_SERVER}.
		fi

		
		
		
		
		
		
		
		
		
		
		
		#Start Security Server
		if [ "X${START_SECSVR}" = "Xtrue" ]; then

			# check for baton. If it doesn't exist, skip
			if [ ! -f "${BATONDIR}/baton.secsvr" ]; then


				#Forcestart 
				if [ "X${MODE}" = "Xforcestart" ]; then

					read -p "Are you sure you want to force-start Security Server? Yes(Y/y) or No(N/n): " answer

					if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						echo "Security Server has been force-started!"
						echo Starting up security server in $SEC_HOME on ${ARBOR_SERVER}
						(cd $SEC_HOME/server; ./s.start)
						checkStop "start failed: Security Server on ${ARBOR_SERVER}" "secsvr" $STARTUP
						OUTCOME_SECSVR="forcestart of Security Server initiated"

					else

						#cancel forcestart
						echo "forcestart for Security Server has been canceled."
						OUTCOME_SECSVR="no forcestart of Security Server initiated, canceled by operator."
						OUTCOME="has been canceled"

					fi


				else

					#skip subsystem
					echo "Security Server on ${ARBOR_SERVER} is already running. Skipping"
					OUTCOME_SECSVR="skipped: system already on"
					OUTCOME="has failed"

				fi

			else

				# normal startup Security Server
				echo Starting up security server in $SEC_HOME on ${ARBOR_SERVER}
				(cd $SEC_HOME/server; ./s.start)
				checkStop "start failed: Security Server on ${ARBOR_SERVER}" "secsvr" $STARTUP
				OUTCOME_SECSVR=$outcomeVal
			fi

		else
			echo Skipping security server startup on ${ARBOR_SERVER}.
		fi
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		#Start Security UI
		if [ "X${START_SECUI}" = "Xtrue" ]; then

			# check for baton. If it doesn't exist, skip
			if [ ! -f "${BATONDIR}/baton.secui" ]; then


				#Forcestart 
				if [ "X${MODE}" = "Xforcestart" ]; then

					read -p "Are you sure you want to force-start Security UI? Yes(Y/y) or No(N/n): " answer

					if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						echo "force-starting Security UI"
						if [ -d $SEC_HOME/ui ] ; then
						echo Starting up security server UI on ${ARBOR_SERVER}
						(cd $SEC_HOME/ui/jakarta-tomcat-5.5.9/bin; ./startup.sh)
						else
							echo *** Security Server UI not installed ***
						fi
						checkStop "start failed: Security Server UI on ${ARBOR_SERVER}" "secui" $STARTUP
						OUTCOME_SECUI="forcestart of Security Server UI initiated"

					else

						#cancel forcestart
						echo "forcestart for Security Server UI has been canceled."
						OUTCOME_SECUI="no forcestart of Security Server UI initiated, canceled by operator."
						OUTCOME="has been canceled"

					fi


				else

					#skip subsystem
					echo "Security Server UI on ${ARBOR_SERVER} is already running. Skipping"
					OUTCOME_SECUI="skipped: system already on"
					OUTCOME="has failed"

				fi

			else

				# normal startup Security Server UI
				# shut down security server UI
				if [ -d $SEC_HOME/ui ] ; then
					echo Starting up security server UI on ${ARBOR_SERVER}
				   (cd $SEC_HOME/ui/jakarta-tomcat-5.5.9/bin; ./startup.sh)
				else
					echo *** Security Server UI not installed ***
				fi 
				checkStop "start failed: Security Server UI on ${ARBOR_SERVER}" "secui" $STARTUP
				OUTCOME_SECUI=$outcomeVal
			fi
			
		else
			echo Skipping security server UI startup on ${ARBOR_SERVER}.
		fi

		
		
		
		
		
		
		
		#Start Mule API
		if [ "X${START_MULE_API}" = "Xtrue" ]; then

			# check for baton. If it doesn't exist, skip
			if [ ! -f "${BATONDIR}/baton.api" ]; then


				#Forcestart 
				if [ "X${MODE}" = "Xforcestart" ]; then

					read -p "Are you sure you want to force-start Mule API? Yes(Y/y) or No(N/n): " answer

					if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						echo force-starting API on ${MULEAPI_SERVER}
						ssh ${MULEAPI_USER}@${MULEAPI_SERVER} "./bin/fxwebctl.sh start"
						TimeoutIncrement=5
						TimeoutMax=12
						TimeoutCounter=$TimeoutMax
						export GETSTARTED=`grep "Started app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
						export GETFAILED=`grep "Failed to deploy app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
						while [ "X$GETSTARTED" == "X" -a "X$GETFAILED" == "X" -a "$TimeoutCounter" -gt "0" ]
						do
						  echo Waiting for Api to complete startup ${TimeoutCounter}...
						  sleep $TimeoutIncrement
						  TimeoutCounter=$(($TimeoutCounter - 1))
						  export GETSTARTED=`grep "Started app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
						  export GETFAILED=`grep "Failed to deploy app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
						done
						if [ "X$GETSTARTED" != "X" ]; then
						  echo $GETSTARTED
						elif [ "X$GETFAILED" != "X" ]; then
						  echo "**************************************"
						  echo "* Mule API failed to start correctly *"
						  echo "**************************************"
						else
						  echo "Unable to determine Mule API startup success within the alloted time."
						  echo "Continuing envctl...."
						fi
						checkStop "stop failed: Mule API on ${MULEAPI_SERVER}" "api" $STARTUP
						OUTCOME_MULE_API="forcestart of Mule API initiated"

					else

						#cancel forcestart
						echo "forcestart for Mule API has been canceled."
						OUTCOME_MULE_API="no forcestart of Mule API initiated, canceled by operator."
						OUTCOME="has been canceled"

					fi


				else

					#skip subsystem
					echo "Mule API on ${MULEAPI_SERVER} is already running. Skipping"
					OUTCOME_MULE_API="skipped: system already on"
					OUTCOME="has failed"

				fi

			else

				# normal startup Mule API
				echo Starting API on ${MULEAPI_SERVER}
				ssh ${MULEAPI_USER}@${MULEAPI_SERVER} "./bin/fxwebctl.sh start"
				TimeoutIncrement=5
				TimeoutMax=12
				TimeoutCounter=$TimeoutMax
				export GETSTARTED=`grep "Started app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
				export GETFAILED=`grep "Failed to deploy app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
				while [ "X$GETSTARTED" == "X" -a "X$GETFAILED" == "X" -a "$TimeoutCounter" -gt "0" ]
				do
				  echo Waiting for Api to complete startup ${TimeoutCounter}...
				  sleep $TimeoutIncrement
				  TimeoutCounter=$(($TimeoutCounter - 1))
				  export GETSTARTED=`grep "Started app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
				  export GETFAILED=`grep "Failed to deploy app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
				done
				if [ "X$GETSTARTED" != "X" ]; then
				  echo $GETSTARTED
				elif [ "X$GETFAILED" != "X" ]; then
				  echo "**************************************"
				  echo "* Mule API failed to start correctly *"
				  echo "**************************************"
				else
				  echo "Unable to determine Mule API startup success within the alloted time."
				  echo "Continuing envctl...."
				fi
				checkStop "start failed: Mule API on ${MULEAPI_SERVER}" "api" $STARTUP
				OUTCOME_MULE_API=$outcomeVal
			fi

		else
			echo Skipping Mule API startup on ${MULEAPI_SERVER}.
		fi

		
		
		
		
		
		
		
		
		
		
		
		
		##START Mule Client-Updater
		if [ "X${START_MULE_CU}" = "Xtrue" ]; then
		

			# check for baton. If it doesn't exist, skip
			if [ ! -f "${BATONDIR}/baton.cu" ]; then


				#Forcestart 
				if [ "X${MODE}" = "Xforcestart" ]; then

					read -p "Are you sure you want to force-start Mule Client-Updater? Yes(Y/y) or No(N/n): " answer

					if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						echo Force-starting Client Updater on ${MULEAPI_SERVER}
						#use ssh -f -n to force ssh to the background so that client-updater process won't hang the
						#startup script while using ssh.  Only required because client-updater doesn't use a java shell
						#to fork the real process.
						#Also, two-part command required because the clientupdctl.sh currently kills any process containing
						#"client-updater" and so it kills itself if run as "./client-updater/bin/clientupdctl.sh start"
						ssh ${MULEAPI_USER}@${MULEAPI_SERVER} -f -n "cd client-updater; ./bin/clientupdctl.sh start"
						#ToDo:  monitor log to determine start success/failure
						#may need to sleep 5 to allow logs to be created because of the ssh -f
						checkStop "start failed: API Client-Updater on ${MULEAPI_SERVER}" "cu" $STARTUP
						OUTCOME_CU="forcestart of Mule Client-Updater initiated"

					else

						#cancel forcestart
						echo "forcestart for Mule Client-Updater has been canceled."
						OUTCOME_MULE_CU="no forcestart of Mule Client-Updater initiated, canceled by operator."
						OUTCOME="has been canceled"

					fi


				else

					#skip subsystem
					echo "Mule Client-Updater on ${MULEAPI_SERVER} is already running. Skipping"
					OUTCOME_MULE_CU="skipped: system already on"
					OUTCOME="has failed"

				fi

			else

				# normal startup Mule Client-Updater
				echo Starting Client Updater on ${MULEAPI_SERVER}
				#use ssh -f -n to force ssh to the background so that client-updater process won't hang the
				#startup script while using ssh.  Only required because client-updater doesn't use a java shell
				#to fork the real process.
				#Also, two-part command required because the clientupdctl.sh currently kills any process containing
				#"client-updater" and so it kills itself if run as "./client-updater/bin/clientupdctl.sh start"
				ssh ${MULEAPI_USER}@${MULEAPI_SERVER} -f -n "cd client-updater; ./bin/clientupdctl.sh start"
				#ToDo:  monitor log to determine start success/failure
				#may need to sleep 5 to allow logs to be created because of the ssh -f
				checkStop "start failed: API Client-Updater on ${MULEAPI_SERVER}" "cu" $STARTUP
				OUTCOME_MULE_CU=$outcomeVal
			fi
			
		else
			echo Skipping Client Updater startup on ${MULEAPI_SERVER}.
		fi
		
		
		

	  
	 

		
		
		#Start Batch Agent
		if [ "X${START_BATCHAGENT}" = "Xtrue" ]; then

			# check for baton. If it doesn't exist, skip
			if [ ! -f "${BATONDIR}/baton.ba" ]; then


				#Forcestart 
				if [ "X${MODE}" = "Xforcestart" ]; then

					read -p "Are you sure you want to force-start Batch Agent? Yes(Y/y) or No(N/n): " answer

					if [ $answer = "Y" -o $answer = "y" ]; then

						#forcestart
						echo "Batch Agent has been force-started!"
						echo Starting BatchAgent on ${BATCH_SERVER}
						ssh ${BATCH_USER}@${BATCH_SERVER} -f -n "bin/batchAgent_check.sh"
						checkStop "start failed: Batch Agent on ${BATCH_SERVER}" "ba" $STARTUP
						OUTCOME_BATCHAGENT="forcestart of Batch Agent initiated"

					else

						#cancel forcestart
						echo "forcestart for Batch Agent has been canceled."
						OUTCOME_BATCHAGENT="no forcestart of Batch Agent initiated, canceled by operator."
						OUTCOME="has been canceled"

					fi


				else

					#skip subsystem
					echo "Batch Agent on ${BATCH_SERVER} is already running. Skipping"
					OUTCOME_BATCHAGENT="skipped: system already on"
					OUTCOME="has failed"

				fi

			else

				# normal startup Batch Agent
				echo Starting BatchAgent on ${BATCH_SERVER}
				ssh ${BATCH_USER}@${BATCH_SERVER} -f -n "bin/batchAgent_check.sh"
				checkStop "start failed: Batch Agent on ${BATCH_SERVER}" "ba" $STARTUP
				OUTCOME_BATCHAGENT=$outcomeVal
			fi
		else
			echo Skipping Batch Agent startup on ${BATCH_SERVER}.
		fi


		
		
	fi




################
# STATUS       #
################

if [ "X${MODE}" = "Xstatus" ]; then
    SENDEMAIL=false

# set all status to unknown
    STATUS_MW=-
    STATUS_WL=-
    STATUS_SECSVR=-
    STATUS_SECUI=-
    STATUS_MULE_API=-
    STATUS_MULE_CU=-
    STATUS_BATCHAGENT=-

#get mw statusi
# This only checks one of many processes started by "FX_Control start ALL"
    GETPID=`ps -ef | grep bp_Catalog | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_MW=up
    else
        STATUS_MW=down
    fi

#get weblogic status
    GETPID=`ps -ef | grep Weblogic | grep java | grep -v shutdown | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_WL=up
    else
        STATUS_WL=down
    fi
    GETPID=`ps -ef | grep Weblogic | grep java | grep shutdown | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_WL="shutting down?"
    fi

#get SECSVR status
    GETPID=`ps -ef | grep SecurityServer | grep java | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_SECSVR=up
    else
        STATUS_SECSVR=down
    fi

#get SECUI status
    GETPID=`ps -ef | grep FXSecServer-5.0.20080305/ui | grep java | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_SECUI=up
    else
        STATUS_SECUI=down
    fi

#get Mule API status
    GETPID=`ps -ef | grep muleapi | grep java | grep MuleContainerBootstrap | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_MULE_API=up
    else
        STATUS_MULE_API=down
    fi

#get Client Updater status
    GETPID=`ps -ef | grep java | grep client-updater | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_MULE_CU=up
    else
        STATUS_MULE_CU=down
    fi

#get BatchAgent status
    GETPID=`ps -ef | grep batch | grep -v grep | grep BatchFlowAgent | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_BATCHAGENT=up
    else
        STATUS_BATCHAGENT=down
    fi

#
# uses spaces for the environment settings because they are displayed in
# the unix console and tabs won't look good.
#

cat << DONE
Module status:
Abbr        Module                  $MODE
mw          Middleware              $STATUS_MW
wl          WebLogic Server         $STATUS_WL
secsvr      Security Server         $STATUS_SECSVR
secui       Security UI             $STATUS_SECUI
api         Mule API                $STATUS_MULE_API
cu          Mule Client Updater     $STATUS_MULE_CU
ba          BatchAgent              $STATUS_BATCHAGENT

DONE

fi

FINISH_TIME=`date`
echo Finished ${MODE} of ${ENVIRONMENT} at $FINISH_TIME

#
# uses tabs for the environment settings because they are displayed in
# email and spaces won't work as well.  The irregular tabs are required for
# the email to look correct.
#





if [ "X$SENDEMAIL" = "Xtrue" ]; then
/usr/sbin/sendmail -oi -t << EOMAIL
From: ${ARBOR_USER}@${ARBOR_SERVER}
To: Cycle30-C3BEnvironmentTeam@gci.com
Subject: A ${MODE} of $SYSTEM_NAME "${SYSTEM_TYPE}${ENVIRONMENT}" environment ${OUTCOME}


A ${MODE} of the $SYSTEM_NAME "${SYSTEM_TYPE}${ENVIRONMENT}" environment ${OUTCOME}.

Timing:
    Start time:  $START_TIME
    Finish time: $FINISH_TIME

Current Settings:
        mode            	Mode                            		$MODE		Outcome
        mw              		Middleware                      		$START_MW	       	$OUTCOME_MW
        wl              		WebLogic Server         		$START_WL		$OUTCOME_WL
        secsvr          		Security Server                 		$START_SECSVR		$OUTCOME_SECSVR
        secui           		Security UI                     		$START_SECUI		$OUTCOME_SECUI
        api             		Mule API                        		$START_MULE_API		$OUTCOME_MULE_API
        cu              		Mule Client Updater             	$START_MULE_CU		$OUTCOME_MULE_CU
        ba              		BatchAgent                      		$START_BATCHAGENT		$OUTCOME_BATCHAGENT
        logs            		Clean Log Files                 		$CLEAN_LOGS		

Called by:
    $0 $*

Command options:  start, stop, restart, forcestart, status

Requested By:
	`logname`

EOMAIL
fi

