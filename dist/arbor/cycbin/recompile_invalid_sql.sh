#!/bin/bash

if [ $# -ne 3 ]; then
    echo "Usage: `basename $0` <db_user> <db_pwd> <db_name>"
    exit 1;
fi


DB_USER=$1
DB_PWD=$2
DB_NAME=$3
DB_USER="`echo $DB_USER | tr '[:lower:]' '[:upper:]'`"

RUNFILE=/tmp/run_invalid.$$.sql

echo Recompiling any invalid DB objects for $DB_USER\@$DB_NAME
sqlplus -S $DB_USER/$DB_PWD\@$DB_NAME << EOF
Set heading off;
set feedback off;
set echo off;
Set lines 999;
 
Spool $RUNFILE 
 
select 
   'ALTER ' || OBJECT_TYPE || ' ' ||
   OWNER || '.' || OBJECT_NAME || ' COMPILE;'
from
   all_objects
where
   status = 'INVALID' and owner = '$DB_USER'
and
   object_type in ('PACKAGE','FUNCTION','PROCEDURE','TRIGGER')
;
 
select 
   'ALTER ' || substr(OBJECT_TYPE, 1, instr(OBJECT_TYPE, ' ')) || ' ' ||
   OWNER || '.' || OBJECT_NAME || ' COMPILE;'
from
   all_objects
where
   status = 'INVALID' and owner = '$DB_USER'
and
   object_type in ('PACKAGE BODY')
;
 
spool off;
 
set heading on;
set feedback on;
set echo on;
 
@$RUNFILE
EOF
#echo "Attempted to recompile the following:"
#cat $RUNFILE | sed 's/^ / d'

rm $RUNFILE

