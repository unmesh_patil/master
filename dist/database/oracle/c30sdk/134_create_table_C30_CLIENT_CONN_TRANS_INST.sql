set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 16-05-2013
prompt Created By: Mohamed Arif
prompt Defect Resolved: C30_Client_Connector
prompt Script file Name: 134_create_table_C30SDK.C30_CLIENT_CONN_TRANS_INST.sql
prompt Version: 1.0 Initial Creation for C30_Client_Connector
prompt DETAILED EXPLANATION:
prompt Create C30SDK.C30_CLIENT_CONN_TRANS_INST table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
DROP TABLE C30SDK.C30_CLIENT_CONN_TRANS_INST CASCADE CONSTRAINTS PURGE;

CREATE TABLE C30SDK.C30_CLIENT_CONN_TRANS_INST(
  REQUEST_ID			NUMBER	NOT NULL,
  ORDER_ID			VARCHAR2(100)	,
  TRANSACTION_ID			VARCHAR2(100)	,
  ORG_ID			VARCHAR2(100)	NOT NULL,
  ACCT_SEG_ID VARCHAR2(100) NOT NULL,
  CAll_BACK_URL_TYPE     VARCHAR2(100)		NOT NULL,
  REQUEST_XML			CLOB NOT NULL	,
  RESPONSE_XML		CLOB 	,
  REQUEST_DATE		TIMESTAMP (6),
  RESPONSE_DATE   TIMESTAMP (6),
  RESPONSE_CODE   VARCHAR2(100),
  STATUS          VARCHAR2(100)
  );
  CREATE INDEX "C30SDK"."C30_CLIENT_CONN_TRANS_INST_PK" ON "C30SDK"."C30_CLIENT_CONN_TRANS_INST" ("REQUEST_ID");
 
 
  
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('C30_CLIENT_CONN_TRANS_INST');
--spool off
exit rollback