set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/124_create_table_c30_prov_client_config_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 124_create_table_c30_prov_client_config.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_client_config table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_client_config cascade constraints purge
;
create table c30sdk.c30_prov_client_config(
  provision_system_id               number(10)      not null,
  provision_system_type             varchar2(90)      not null,
  connect_url               varchar2(200)      not null,
  user_id	                varchar2(30)      ,
  user_password               	varchar2(100)     ,
  env_name					varchar2(30),
  simulate_request			varchar2(30),
  provision_callback_system_id varchar2(30),
  property1                    varchar2(200),
   property2                    varchar2(200),
   property3                    varchar2(200),
   property4                    varchar2(200),
   property5                    varchar2(200)  
  )
;
  
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_client_config');


--v1.1 added...
begin

insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID, PROVISION_SYSTEM_TYPE,CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001,'PROVISION','https://c30uat-provisioning.gci.com', null, null, 'AWN3UAT1','FALSE', 'AWN3UAT1');

insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001, 'INQUIRY','https://c30uat-inquiry.gci.com/Inquiry',null, null, 'AWN3UAT1','FALSE', null);

--PREPAID Scripts
Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1004,'OCS','http://ocslab.operations.gci.com/ppsxml/prepaid.xml_rpc.server.php',null,null,'AWN3UAT1','FALSE','AWN3UAT1',null,null,null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1005,'MULEPOS','https://gci1uatkenap1.cycle30.com:52003/GCI/UAT','POSUser','yBcwb9Ipt/EpgIprR0zfQN7HyqDb06JO','AWN3UAT1','FALSE',null,'GCIUAT',null,null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1006,'INCOMM','https://iproxy-aur.gci.com/transferredvaluetest.incomm.com/transferedvalue/gateway',null,null,'AWN3UAT1','FALSE','AWN3UAT1',null,null,null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1008,'BLACKHAWK','https://iproxy-aur.gci.com/blast.preprod.blackhawk-net.com/transactionManagement/v2/transaction',null,null,'AWN3UAT1','FALSE', null,null,null,null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1009,'ACSINV','https://api.alaskacommunications.com/tnmanagementqa/api',null,null,'AWN3UAT1','FALSE', null,'#d1[4vjES}@1Xa*Zu4b;Pz5hr&-;_)Vp*9C4','AWNFORCEQA',null,null,null);


c30_put_line(null,'Inserted 7 C30_PROV_CLIENT_CONFIG rows ');

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script C30_PROV_CLIENT_CONFIG');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/

--spool off
exit rollback