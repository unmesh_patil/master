set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 05-03-2013
prompt Created By: Umesha Narayana
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 153_load_table_C30_PROV_SYSTEM_PARAMETERS.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Load C30_PROV_SYSTEM_PARAMETERS
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
begin
  dbms_output.put(chr(10));
  delete c30sdk.C30_PROV_SYSTEM_PARAMETERS;
  c30_put_line(null,'Deleted '||sql%rowcount||' c30sdk.C30_PROV_SYSTEM_PARAMETERS row[s]');

 Insert into C30_PROV_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','IPTRUNK','192.168.1.2',4,null,null,null);
 
 Insert into C30_PROV_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','ACCESS_PHONE','1021',4,null,null,null);

  c30_put_line(null,'Inserted  2 c30sdk.C30_PROV_SYSTEM_PARAMETERS rows');

  commit;
  c30_put_line(null,'commit;');
  c30_put_line(null,'Successfully completed script for config');
exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/
--spool off
-- the commit is aleady done in the script if successful...
exit rollback 