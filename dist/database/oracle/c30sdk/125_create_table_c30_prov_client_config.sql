set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/124_create_table_c30_prov_client_config_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 124_create_table_c30_prov_client_config.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_client_config table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_client_config cascade constraints purge
;
create table c30sdk.c30_prov_client_config(
  provision_system_id               number(10)      not null,
  provision_system_type             varchar2(90)      not null,
  connect_url               varchar2(200)      not null,
  user_id	                varchar2(30)      ,
  user_password               	varchar2(100)     ,
  env_name					varchar2(30),
  simulate_request			varchar2(30),
  provision_callback_system_id varchar2(30),
  property1                    varchar2(200),
   property2                    varchar2(200),
   property3                    varchar2(200),
   property4                    varchar2(200),
   property5                    varchar2(200)  
  )
;
  
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_client_config');


--v1.1 added...
begin
-- c30sdk.C30_PROV_CLIENT_CONFIG (based on existing most-common values, account segments 1->4)
--insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
--values (1001, 'PROVISION','https://c30dev-provisioning.gci.com', null, null, 'DEV','TRUE', 'C3BDV1');
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001, 'PROVISION','https://c30dev-provisioning.operations.gci.com', null, null, 'DEV','FALSE', 'C3B3DEV');
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001, 'PROVISION','https://c30dev-provisioning.operations.gci.com', null, null, 'CFG','TRUE', 'C3B3CFG');
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID, PROVISION_SYSTEM_TYPE,CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001,'PROVISION','https://c30dev-provisioning.gci.com', null, null, 'TST','FALSE', 'C3B3TST');
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID, PROVISION_SYSTEM_TYPE,CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001,'PROVISION','https://c30uat-provisioning.gci.com', null, null, 'UAT','FALSE', 'C3B3UAT');
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID, PROVISION_SYSTEM_TYPE,CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001,'PROVISION','https://apis-provisioning.gci.com', null, null, 'PROD','FALSE', 'C3B1PRD');
--insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
--values (1001, 'INQUIRY','https://c30dev-inquiry.gci.com/Inquiry',null, null, 'DEV','FALSE', null);
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001, 'INQUIRY','https://c30dev-inquiry.operations.gci.com/Inquiry',null, null, 'DEV','TRUE', null);
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001, 'INQUIRY','https://c30dev-inquiry.operations.gci.com/Inquiry',null, null, 'CFG','TRUE', null);
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001, 'INQUIRY','https://c30dev-inquiry.gci.com/Inquiry',null, null, 'TST','FALSE', null);
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001, 'INQUIRY','https://c30uat-inquiry.gci.com/Inquiry',null, null, 'UAT','FALSE', null);
insert into c30sdk.C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE, CONNECT_URL, USER_ID, USER_PASSWORD, ENV_NAME, SIMULATE_REQUEST, provision_callback_system_id)
values (1001,'INQUIRY', 'https://apis-inquiry.gci.com/Inquiry', null, null, 'PROD','FALSE', null);

--PREPAID Scripts
Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1005,'MULEPOS','https://gci1devkenap1.cycle30.com:52003/GCI/DEV',null,null,'DEV','FALSE',null,'GCIDEV','POSUser',null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1005,'MULEPOS','https://gci1devkenap1.cycle30.com:52003/GCI/CFG',null,null,'DEV','FALSE',null,'GCICFG','POSUser',null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1005,'MULEPOS','https://gci1devkenap1.cycle30.com:52003/GCI/TST',null,null,'DEV','FALSE',null,'GCITST','POSUser',null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1005,'MULEPOS','https://gci1devkenap1.cycle30.com:52003/GCI/UAT',null,null,'DEV','FALSE',null,'GCIUAT','POSUser',null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1004,'OCS','https://ocslab.gci.com/ppsxml/prepaid.xml_rpc.server.php',null,null,'DEV','FALSE','C3B3DEV',null,null,null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1004,'OCS','https://ocslab.gci.com/ppsxml/prepaid.xml_rpc.server.php',null,null,'CFG','FALSE','C3B3CFG',null,null,null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1004,'OCS','https://ocslab.gci.com/ppsxml/prepaid.xml_rpc.server.php',null,null,'TST','FALSE','C3B3TST',null,null,null,null,null);

Insert into C30_PROV_CLIENT_CONFIG (PROVISION_SYSTEM_ID,PROVISION_SYSTEM_TYPE,CONNECT_URL,USER_ID,USER_PASSWORD,ENV_NAME,SIMULATE_REQUEST,PROVISION_CALLBACK_SYSTEM_ID,PROPERTY1,PROPERTY2,PROPERTY3,PROPERTY4,PROPERTY5) 
values (1004,'OCS','https://ocslab.gci.com/ppsxml/prepaid.xml_rpc.server.php',null,null,'UAT','FALSE','C3B3UAT',null,null,null,null,null);

c30_put_line(null,'Inserted 10 C30_PROV_CLIENT_CONFIG rows ');

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script C30_PROV_CLIENT_CONFIG');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/

--spool off
exit rollback
