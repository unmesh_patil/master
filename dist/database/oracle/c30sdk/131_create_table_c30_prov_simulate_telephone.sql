set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/129_create_table_c30_prov_trans_inst.txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 129_create_table_c30_prov_trans_inst.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_simulate_telephone
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_simulate_telephone cascade constraints purge
;
create table c30sdk.c30_prov_simulate_telephone(
  external_id               varchar2(144)
 )
;
  

-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_simulate_telephone');

--spool off
exit rollback