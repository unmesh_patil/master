-- Release Notes
-- Config 1.0
-- Date: 06/27/2011
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development

Delete from C30_SDK_CLASS_REF;
COMMIT;

Insert into C30_SDK_CLASS_REF    (CLASS_NAME, CLASS_ID)
 Values    ('com.cycle30.sdk.core.framework.C30SDKFrameworkFactory', 1);

Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.core.framework.C30SDKExceptionMessage', 2);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.invoice.C30BillInvoice', 50);

Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.invoice.C30BillInvoiceDetail', 60);

Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30OrderItem', 30);
 
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30Service', 40);
 
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30Component', 41);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30ProductElement', 42);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30ProductPackage', 43);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30ServiceServiceOrder', 44);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30ServiceExternalId', 45);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.account.C30Account', 10);
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.account.C30VertexGeoCode', 17);
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ( 'com.cycle30.sdk.object.kenan.account.C30AccountExternalId', 11);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30Order', 20);

Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.cycle30.order.C30Order', 21);
 
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.bulkJob.C30BulkJob', 22);
 
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.account.C30AccountNote', 70);
 
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.financial.C30Payment', 80);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.financial.C30AccountBalance', 90);
 
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.financial.C30Deposit', 85);

 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.cycle30.C30AccountBalance', 91);
   
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.search.C30CrossObjectSearch', 100);

Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.search.C30SearchByAddress', 110);

Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30InventoryElement', 46);

Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.kenan.ordering.C30AccountServiceOrder', 47);
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.workpoint.C30Workpoint', 15);

 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.prevalidation.PaymentCreatePreValidation', 802);
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.prevalidation.PaymentCreateWithAccountNotePreValidation', 804);
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.prevalidation.PaymentDeletePreValidation', 805);
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.prevalidation.PaymentFindPreValidation', 806);
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.prevalidation.PaymentReversalPreValidation', 807);
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.brePreValidation.PaymentCreateBrePreValidation', 803);
 
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.prevalidation.AccountSearchWithBalancePreValidation', 101);
 Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.extension.brePreValidation.AccountSearchWithBalanceBrePreValidation', 102);
 
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.cycle30.invoice.C30Invoice', 55);
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.cycle30.invoice.C30InvoiceCharge', 56);
 
-- service inquiry
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.cycle30.service.C30Service', 86);
 
-- service update
Insert into C30_SDK_CLASS_REF   (CLASS_NAME, CLASS_ID)
 Values   ('com.cycle30.sdk.object.cycle30.service.C30Service', 87);

 --Prepaid Scripts
 Insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values ('com.cycle30.sdk.object.sps.C30IMSISearch',120);
 Insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values ('com.cycle30.sdk.object.awnprepaid.C30PrepaidAccount',121);
 Insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values ('com.cycle30.sdk.object.cycle30.payment.C30PrepaidPayment',4);
 Insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values ('com.cycle30.sdk.object.awnprepaid.C30PrepaidAccountBalance',5);
 Insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values ('com.cycle30.sdk.object.awnprepaid.C30PrepaidPhoneInventory',6);
 Insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values ('com.cycle30.sdk.object.awnprepaid.C30PrepaidVoucher',7);
 Insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values ('com.cycle30.sdk.object.cycle30.payment.C30PaymentRegister',8);
 insert into C30_SDK_CLASS_REF (CLASS_NAME,CLASS_ID) values('com.cycle30.sdk.object.cycle30.C30ClientCallback',9);
 
COMMIT;

EXIT

