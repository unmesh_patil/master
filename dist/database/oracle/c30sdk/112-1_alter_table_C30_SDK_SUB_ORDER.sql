-- Release Notes
-- Config 1.0
-- Date: 06/27/2011
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development

alter table C30_SDK_SUB_ORDER
add (
  CLIENT_ID     VARCHAR2(144)
  );
  
COMMIT;
EXIT
