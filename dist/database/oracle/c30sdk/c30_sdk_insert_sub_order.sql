  -- Release Notes
  -- Config 1.0
  -- Date: 06/27/2011
  -- Created By: Ranjith Kumar Nelluri
  -- Change Request: C30 SDK Framework Development

CREATE OR REPLACE PROCEDURE c30_sdk_insert_sub_order(v_client_order_id        VARCHAR2,
                                                     v_client_sub_order_id       VARCHAR2,
													 v_client_sub_order_name     VARCHAR2,
                                                     v_sub_order_type            VARCHAR2,
													 v_sub_order_action_type     VARCHAR2,
													 v_sub_order_action_reason   VARCHAR2,
                                                     v_acct_seg_id               NUMBER,
                                                     v_payload                   BLOB,
													 v_billing_acct_number		 VARCHAR2,
													 v_emf_config_id			 NUMBER,
                                                     v_wp_process_id             VARCHAR2,
                                                     v_sub_order_status_id       NUMBER,
                                                     v_sub_order_create_date     TIMESTAMP,
                                                     v_sub_order_desired_date    TIMESTAMP,
                                                     v_sub_order_complete_date   TIMESTAMP,
                                                     v_sub_order_level           NUMBER,
													 v_client_org_name			 varchar2,
													 v_client_org_id			 varchar2,
													 v_transaction_id            varchar2,
													 v_internal_audit_id         varchar2,
													 v_client_id				 varchar2,
                                                     c30_sdk_insert_sub_order_cv IN OUT cv_types.customer_tp) IS
BEGIN
  -- Inserting data into C30_SDK_SUB_ORDER Table.
  INSERT INTO C30_SDK_SUB_ORDER
    (CLIENT_ORDER_ID,
     CLIENT_SUB_ORDER_ID,
	 CLIENT_SUB_ORDER_NAME,
     SUB_ORDER_TYPE,
	 SUB_ORDER_ACTION_TYPE,
	 SUB_ORDER_ACTION_REASON,
     ACCT_SEG_ID,
     PAYLOAD,
	 BILLING_ACCOUNT_NO,
	 EMF_CONFIG_ID,
     WP_PROCESS_ID,
     SUB_ORDER_STATUS_ID,
     SUB_ORDER_CREATE_DATE,
     SUB_ORDER_DESIRED_DATE,
     SUB_ORDER_COMPLETE_DATE,
     SUB_ORDER_LEVEL,
	 CLIENT_ORG_NAME,
	 CLIENT_ORG_ID,
	 TRANSACTION_ID,
	 INTERNAL_AUDIT_ID,
	 CLIENT_ID
	 IS_CANCELLABLE,
	 IS_RESUBMITTABLE)
  VALUES
    (v_client_order_id,
     v_client_sub_order_id,
	 v_client_sub_order_name,
     v_sub_order_type,
	 v_sub_order_action_type,
	 v_sub_order_action_reason,
     v_acct_seg_id,
     v_payload,
	 v_billing_acct_number,
	 v_emf_config_id,
     v_wp_process_id,
     v_sub_order_status_id,
     v_sub_order_create_date,
     v_sub_order_desired_date,
     v_sub_order_complete_date,
     v_sub_order_level,
	 v_client_org_name,
	 v_client_org_id,
	 v_transaction_id,
	 v_internal_audit_id,
	 v_client_id,
	 1,
	 1);
  --Commit the insert statement.
  OPEN c30_sdk_insert_sub_order_cv FOR
    SELECT v_client_order_id FROM DUAL;
  COMMIT;
END c30_sdk_insert_sub_order;
/
EXIT