set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/124_create_table_c30_prov_client_config_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 124_create_table_c30_prov_client_config.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_client_config table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue

begin

UPDATE c30sdk.C30_PROV_CLIENT_CONFIG set SIMULATE_REQUEST = 'FALSE' WHERE PROVISION_SYSTEM_ID = 1001 AND ENV_NAME like '%TST%';

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script C30_PROV_CLIENT_CONFIG');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/

--spool off
exit rollback