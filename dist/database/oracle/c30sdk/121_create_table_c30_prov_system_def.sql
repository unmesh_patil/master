set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/1120_create_table_c30_prov_system_def_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 120_create_table_c30_prov_system_def.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_system_def table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_system_def cascade constraints purge
;
create table c30sdk.c30_prov_system_def(
  provision_system_id               number(10)      not null,
  provision_system_name             varchar2(64)    not null
  )
;
  
create unique index c30sdk.c30_prov_system_def_pk
  on c30sdk.c30_prov_system_def(provision_system_id)
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_system_def');

--v1.1 added...
begin
-- c30sdk.c30_prov_system_def (based on existing most-common values, account segments 1->4)
insert into c30sdk.c30_prov_system_def(provision_system_id, provision_system_name)
                                          values(1001, 'AWN Wholesale - SPS2');
insert into c30sdk.c30_prov_system_def(provision_system_id, provision_system_name)
                                          values(1002, 'GCI - NSG SPS1');
insert into c30sdk.c30_prov_system_def(provision_system_id, provision_system_name)
                                          values(1003, 'TRIAD - GCI Cable');
insert into c30sdk.c30_prov_system_def(provision_system_id, provision_system_name)
                                          values(1004, 'StreamWIDE OCS - AWN Prepaid');
insert into c30sdk.c30_prov_system_def(provision_system_id, provision_system_name)
                                          values(1005, 'GCI MulePOS');
insert into c30sdk.c30_prov_system_def(provision_system_id, provision_system_name)
                                          values(1006, 'Incomm');		
insert into c30sdk.c30_prov_system_def(provision_system_id, provision_system_name)
                                          values(1007, 'DCN - GlobeComm - 6d');										  
c30_put_line(null,'Inserted 7 c30_prov_system_def rows ');

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script c30_prov_system_def');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/

--spool off
exit rollback