-- Release Notes
-- Config 1.0
-- Date: 06/27/2011
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development 
-- This script will create all the required constraints...


alter table C30_SDK_OBJECT_FIELD_CONFIG drop constraint C30_SDK_CLSS_DEF_ATTR_FK;

alter table C30_SDK_OBJECT_ATTRIBUTES drop constraint C30_SDK_OBJECT_DEF_IS_REQ_CH;
  
alter table C30_SDK_OBJECT_ATTRIBUTES drop constraint C30_SDK_OBJ_DEF_ALLOW_ORIDE_CH;

commit;

EXIT
