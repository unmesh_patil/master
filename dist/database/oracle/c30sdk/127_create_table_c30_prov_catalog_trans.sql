set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/127_create_table_c30_prov_catalog_trans..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 127_create_table_c30_prov_catalog_trans.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_catalog_transtable
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_catalog_trans cascade constraints purge
;
create table c30sdk.c30_prov_catalog_trans(
  request_id			number,
  external_id               varchar2(144),
  telephone_number               varchar2(144),
  catalog_id               varchar2(256),
  feature_id			number,
  request_start_date			timestamp,
  request_complete_date		timestamp,
  provision_system_id	number,
  service_type			varchar2(30),
  status_id				number,
  bill_action_type_reason_code 	number,
  external_transaction_id	varchar2(512),
  c30_transaction_id	number
  )
;
  
create unique index c30sdk.c30_prov_catalog_trans_pk
  on c30sdk.c30_prov_catalog_trans(request_id)
;

create index c30sdk.c30_prov_catalog_tn_idx on c30sdk.c30_prov_catalog_trans(telephone_number);
   
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_catalog_trans');

--spool off
exit rollback