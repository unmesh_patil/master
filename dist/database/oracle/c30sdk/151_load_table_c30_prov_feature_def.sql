set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/151_load_table_c30_prov_feature_def.sql..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 120_create_table_c30_prov_feature_def.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_feature_def table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
begin
  dbms_output.put(chr(10));
  delete c30sdk.c30_prov_feature_def;
  c30_put_line(null,'Deleted '||sql%rowcount||' c30sdk.c30_prov_feature_def row[s]');

  -- Default/ System Used Values 
  -- Dont change these..
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1001, 'SERVICE DISC');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1002, 'SERVICE SUSPEND');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1003, 'SERVICE RESUME');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1004, 'TELEPHONE NUMBER CHANGE');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1005, 'SIM CHANGE');
 
  -- Configurable Items
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1101, 'VOICE Add' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1102, 'VOICE Disconnect' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1103, 'ACS DATA Add' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1104, 'ACS DATA Disconnect' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1105, 'ACS DATA TETH Add' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1106, 'ACS DATA TETH Disconnect' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1107, 'MSG Add' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1108, 'MSG Disconnect' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1109, 'INTLCALL Add');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1110, 'INTLCALL Disconnect');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1111, 'INTLROAM Add');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1112, 'INTLROAM Disconnect');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1113, 'REMROAM Add'); 
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1114, 'REMROAM Disconnect'); 
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1115, 'DATA+MMS Add');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1116, 'DATA+MMS Disconnect');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1117, 'DATA+TETH+MMS Add'); 
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1118, 'DATA+TETH+MMS Disconnect'); 
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1119, 'DATAONLY Add'); 
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1120, 'REMTOLL  Add'); 
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1121, 'REMTOLL  Disconnect'); 
  
  --PREPAID Changes
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1122, 'PREPAID SERVICE Add' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1123, 'PREPAID SERVICE Disconnect' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1124, 'PREPAID DATAONLY Add');   
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1125, 'PREPAID DATAONLY Disconnect');   

  --PREPAID_SUSPEND ADD & DISCONNECT
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1126, 'PREPAID SUSPEND Add');
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1127, 'PREPAID SUSPEND Disconnect');

  --PREPAID Changes
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1128, 'ACS PREPAID SERVICE Add' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name )values(1129, 'ACS PREPAID SERVICE Disconnect' );
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1130, 'ACS PREPAID DATAONLY Add');   
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1131, 'ACS PREPAID DATAONLY Disconnect');  

  -- PREPAD VOICE MAIL CHANGE  ( from VVM to VM vice versa)
  insert into c30sdk.c30_prov_feature_def (feature_id, feature_name) values(1132, 'ACS PREPAID VOICE MAIL CHANGE');  
  
  c30_put_line(null,'Inserted 32 c30sdk.c30_prov_feature_def rows');

  commit;
  c30_put_line(null,'commit;');
  c30_put_line(null,'Successfully completed script for config');
exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/
--spool off
-- the commit is aleady done in the script if successful...
exit rollback