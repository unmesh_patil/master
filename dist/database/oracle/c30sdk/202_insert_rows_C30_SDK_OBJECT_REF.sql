-- Release Notes
-- Config 1.0
-- Date: 06/27/2011 
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development

delete from C30_SDK_OBJECT_REF;
commit;


--All Scripts related to Core Product.
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('FX1.0/PS2.0', NULL, 1, 0, 1);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('FX2.0/PS2.0', NULL, 1, 0, 1);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('Empty Exception Message',NULL, 2, 0, 1);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core AccountBalance Lookup', 1,91, 0, 0,null,null);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core WorkPointJob Lookup', 1,15, 0, 0,null,null);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core WorkPointJob Create', 1,15, 0, 0,null,null);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core Order', 1,21, 0, 0,null,null);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core Bulk Order Job Create', 1,22, 0, 0,null,null);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('VertexGeoCode Find', 17, 0, 0);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core Invoice Lookup', 1,55, 0, 0,null,null);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core Invoice Charge Lookup', 1,56, 0, 0,null,null);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('Core Payment', 1, 80, 0, 0);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('Core Payment Void', 1, 80, 0, 0);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('Core Payment Note Create', 1, 70, 0, 0);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('Core Deposit Create', 1, 85, 0, 0);
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('Core Service Inquiry', 1, 86, 0, 0); 
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL)
values ('Core Service Update', 1, 87, 0, 0); 
insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
values ('Core ClientCallback', 1,9, 0, 0,null,null);

commit;

-- Now create seg_id 3/4 using acct_seg_id 2
-- 3= AWN Wholesale 
insert into C30_SDK_OBJECT_REF
(LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
   select LW_OBJECT_TYPE, '2', CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id
     from C30_SDK_OBJECT_REF where ACCT_SEG_ID='1';

insert into C30_SDK_OBJECT_REF
(LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
   select LW_OBJECT_TYPE, '3', CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id
     from C30_SDK_OBJECT_REF where ACCT_SEG_ID='1';

--4,5 for AWN Prepaid for the GCI and ACS
-- 4= AWN Prepaid GCI
-- 5= AWN Prepaid ACS

insert into C30_SDK_OBJECT_REF
(LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
   select LW_OBJECT_TYPE, '4', CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id
     from C30_SDK_OBJECT_REF where ACCT_SEG_ID='1';
	 
insert into C30_SDK_OBJECT_REF
(LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
   select LW_OBJECT_TYPE, '5', CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id
     from C30_SDK_OBJECT_REF where ACCT_SEG_ID='1';

--Core Product M2M
insert into C30_SDK_OBJECT_REF
(LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
   select LW_OBJECT_TYPE, '10', CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id
     from C30_SDK_OBJECT_REF where ACCT_SEG_ID='1';

	 
commit;

-- ############################################################################
-- Custom Scirpts & Account Segment Specific ones.
----------------------------------------------------------------
--Prepaid Scripts
--Order
--IMSI Search
Insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE,CLASS_ID,PREVALIDATE_CLASS_ID,POST_ENFORCER_CLASS_ID,BRE_PREVALIDATE_CLASS_ID,BRE_POST_ENFORCER_CLASS_ID,IS_DEFAULT,IS_INTERNAL,EXTENSION_CLASS_ID,ACCT_SEG_ID) values ('IMSISearch Lookup',120,null,null,null,null,0,0,null,4);

--Prepaid Account Lookup
Insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE,CLASS_ID,PREVALIDATE_CLASS_ID,POST_ENFORCER_CLASS_ID,BRE_PREVALIDATE_CLASS_ID,BRE_POST_ENFORCER_CLASS_ID,IS_DEFAULT,IS_INTERNAL,EXTENSION_CLASS_ID,ACCT_SEG_ID) values ('PrepaidAccount Lookup',121,null,null,null,null,0,0,null,4);

--Prepaid Balance
Insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE,CLASS_ID,PREVALIDATE_CLASS_ID,POST_ENFORCER_CLASS_ID,BRE_PREVALIDATE_CLASS_ID,BRE_POST_ENFORCER_CLASS_ID,IS_DEFAULT,IS_INTERNAL,EXTENSION_CLASS_ID,ACCT_SEG_ID) values ('PrepaidBalance',5,null,null,null,null,0,0,null,4);

--Prepaid Invetory Lookup
Insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE,CLASS_ID,PREVALIDATE_CLASS_ID,POST_ENFORCER_CLASS_ID,BRE_PREVALIDATE_CLASS_ID,BRE_POST_ENFORCER_CLASS_ID,IS_DEFAULT,IS_INTERNAL,EXTENSION_CLASS_ID,ACCT_SEG_ID) values ('PrepaidPhoneInventory Lookup',6,null,null,null,null,0,0,null,4);

--Prepaid Voucher Lookup
Insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE,CLASS_ID,PREVALIDATE_CLASS_ID,POST_ENFORCER_CLASS_ID,BRE_PREVALIDATE_CLASS_ID,BRE_POST_ENFORCER_CLASS_ID,IS_DEFAULT,IS_INTERNAL,EXTENSION_CLASS_ID,ACCT_SEG_ID) values ('PrepaidVoucher Lookup',7,null,null,null,null,0,0,null,4);

--Prepaid Payment Register
Insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE,CLASS_ID,PREVALIDATE_CLASS_ID,POST_ENFORCER_CLASS_ID,BRE_PREVALIDATE_CLASS_ID,BRE_POST_ENFORCER_CLASS_ID,IS_DEFAULT,IS_INTERNAL,EXTENSION_CLASS_ID,ACCT_SEG_ID) values ('Core Payment Register',8,null,null,null,null,0,0,null,4);

--Prepaid Payment
Insert into C30_SDK_OBJECT_REF (LW_OBJECT_TYPE,CLASS_ID,PREVALIDATE_CLASS_ID,POST_ENFORCER_CLASS_ID,BRE_PREVALIDATE_CLASS_ID,BRE_POST_ENFORCER_CLASS_ID,IS_DEFAULT,IS_INTERNAL,EXTENSION_CLASS_ID,ACCT_SEG_ID) values ('PrepaidPayment',4,null,null,null,null,0,0,null,4);

insert into C30_SDK_OBJECT_REF
(LW_OBJECT_TYPE, ACCT_SEG_ID, CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id)
   select LW_OBJECT_TYPE, '5', CLASS_ID, IS_DEFAULT, IS_INTERNAL, prevalidate_class_id, bre_prevalidate_class_id
     from C30_SDK_OBJECT_REF where ACCT_SEG_ID='4' and LW_OBJECT_TYPE 
	 in('PrepaidPhoneInventory Lookup', 'PrepaidAccount Lookup', 'PrepaidBalance', 'PrepaidVoucher Lookup', 'Core Payment Register', 'PrepaidPayment', 'IMSISearch Lookup');
	 
commit;


EXIT