set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 05-03-2013
prompt Created By: Umesha Narayana
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 133_create_table_c30sdk.c30_prov_system_parameters.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30sdk.c30_prov_system_parameters table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
DROP TABLE C30SDK.C30_PROV_SYSTEM_PARAMETERS CASCADE CONSTRAINTS PURGE;

CREATE TABLE C30SDK.C30_PROV_SYSTEM_PARAMETERS(
  MODULE_NAME			VARCHAR2(10)	NOT NULL,
  PARAM_NAME			VARCHAR2(100)	NOT NULL,
  PARAM_VALUE			VARCHAR2(2000)	NOT NULL,
  ACCOUNT_SEGMENT               NUMBER		NOT NULL,
  ATTRIBUTE1			VARCHAR2(2000)	,
  ATTRIBUTE2			VARCHAR2(2000)	,
  ATTRIBUTE3			VARCHAR2(2000)	
  );
  
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('C30_PROV_SYSTEM_PARAMETERS');
--spool off
exit rollback
