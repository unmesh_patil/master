-- Release Notes
-- Config 1.0
-- Date: 06/27/2011 
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development
-- This script will create all the required constraints...

-- C30_SDK_OBJECT_FIELD_CONFIG
alter table C30_SDK_OBJECT_FIELD_CONFIG
  add constraint C30_SDK_CLSS_DEF_ATTR_FK foreign key (CLASS_ID)
  references C30_SDK_CLASS_REF (CLASS_ID);

alter table C30_SDK_OBJECT_ATTRIBUTES
  add constraint C30_SDK_OBJECT_DEF_IS_REQ_CH
  check (is_required IN (0, 1));
  
alter table C30_SDK_OBJECT_ATTRIBUTES
  add constraint C30_SDK_OBJ_DEF_ALLOW_ORIDE_CH
  check (allow_overrides IN (0, 1));

commit;

EXIT
