
-- Release Notes
-- Config 1.0
-- Date: 06/03/2017
-- Created By: vijayalakshmi yellapu
-- adding index to c30_prov_trans_inst table on request id to resolve slow response times to SPS requests

create index c30sdk.c30_prov_trans_inst_req_id
on C30SDK.C30_PROV_TRANS_INST (request_id)
;

COMMIT;

EXIT

