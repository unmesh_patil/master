set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off

prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 05-17-2013
prompt Created By: Mohamed Arif
prompt Defect Resolved: 
prompt Script file Name: C30_CLIENT_CONN_REQUEST_SEQ.sql
prompt Version: 1.0 Initial Creation for Client Connector
prompt DETAILED EXPLANATION:
prompt Create C30_CLIENT_CONN_REQUEST_SEQ
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop sequence c30sdk.C30_CLIENT_CONN_REQUEST_SEQ
;
CREATE SEQUENCE c30sdk.C30_CLIENT_CONN_REQUEST_SEQ
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
  
 
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('C30_CLIENT_CONN_REQUEST_SEQ');

--spool off
exit rollback
