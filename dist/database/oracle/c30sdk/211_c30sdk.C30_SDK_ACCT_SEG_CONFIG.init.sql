-- Release Notes
-- Config 1.0
-- Date: 06/27/2011
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development / Managing this to Acct Segment Configuration with the Billing Authentication.
-- Modified by Umesh - Added Customer #3 config


delete from C30_SDK_ACCT_SEG_CONFIG;

insert into C30_SDK_ACCT_SEG_CONFIG (ACCT_SEG_ID, C30_API_LOGIN_ID, C30_WEBLOGIC_DATASOURCE)
values (1, 'arbor', 'billingDataSource');

insert into C30_SDK_ACCT_SEG_CONFIG (ACCT_SEG_ID, C30_API_LOGIN_ID, C30_WEBLOGIC_DATASOURCE)
values (2, 'arbor', 'billingDataSource');

insert into C30_SDK_ACCT_SEG_CONFIG (ACCT_SEG_ID, C30_API_LOGIN_ID, C30_WEBLOGIC_DATASOURCE)
values (3, 'arbor', 'billingDataSource');

insert into C30_SDK_ACCT_SEG_CONFIG (ACCT_SEG_ID, C30_API_LOGIN_ID, C30_WEBLOGIC_DATASOURCE)
values (4, 'arbor', 'billingDataSource');

insert into C30_SDK_ACCT_SEG_CONFIG (ACCT_SEG_ID, C30_API_LOGIN_ID, C30_WEBLOGIC_DATASOURCE)
values (5, 'arbor', 'billingDataSource');

insert into C30_SDK_ACCT_SEG_CONFIG (ACCT_SEG_ID, C30_API_LOGIN_ID, C30_WEBLOGIC_DATASOURCE)
values (10, 'arbor', 'billingDataSource');

COMMIT;
EXIT
