create or replace
TRIGGER C30SDK.ORDER_STATUS_TRIG
AFTER UPDATE OF SUB_ORDER_STATUS_ID ON C30_SDK_SUB_ORDER
REFERENCING NEW AS new_row
FOR EACH ROW
DECLARE
  message sys.aq$_jms_text_message;
  enqueue_options dbms_aq.enqueue_options_t;
  message_properties dbms_aq.message_properties_t;
  msgid raw(16);
  message_text varchar(4000);

  v_ErrorCode NUMBER;           -- Variable to hold the error message code
  v_ErrorText VARCHAR2(4000);    -- Variable to hold the error message text
  v_count     NUMBER(10);  		-- Variable to check the callback client configuration
 
BEGIN
  -- XML message format:
  --      <StatusUpdate>
  --          <SubOrderId></SubOrderId>
  --          <OrderId></OrderId>
  --          <TransactionId></TransactionId>
  --          <SubOrderName></SubOrderName>
  --          <SubOrderType></SubOrderType>
  --          <SubOrderActionType></SubOrderActionType>  
  --          <AcctSegId></AcctSegId>
  --          <BillingAccountNo></BillingAccountNo>
  --          <WpProcessId></WpProcessId>
  --          <WpJobId></WpJobId>  
  --          <SubOrderStatusId></SubOrderStatusId>
  --          <OrgId></OrgId>
  --      </StatusUpdate>


message := sys.aq$_jms_text_message.construct;

message_text :=
   '<StatusUpdate>' ||
   		'<SubOrderId>'         || :new_row.CLIENT_SUB_ORDER_ID    || '</SubOrderId>'         ||
        '<TransactionId>'      || :new_row.TRANSACTION_ID         || '</TransactionId>'      ||      
        '<InternalAuditId>'    || :new_row.INTERNAL_AUDIT_ID      || '</InternalAuditId>'    ||      
        '<OrderId>'            || :new_row.CLIENT_ORDER_ID        || '</OrderId>'            ||
        '<SubOrderName>'       || :new_row.CLIENT_SUB_ORDER_NAME  || '</SubOrderName>'       ||
        '<SubOrderType>'       || :new_row.SUB_ORDER_TYPE         || '</SubOrderType>'       ||
		'<SubOrderActionType>' || :new_row.SUB_ORDER_ACTION_TYPE  || '</SubOrderActionType>' ||
		'<AcctSegId>'          || :new_row.ACCT_SEG_ID            || '</AcctSegId>'          ||
		'<BillingAccountNo>'   || :new_row.BILLING_ACCOUNT_NO     || '</BillingAccountNo>'   ||
		'<WpProcessId>'        || :new_row.WP_PROCESS_ID          || '</WpProcessId>'        ||
		'<WpJobId>'            || :new_row.WP_JOB_ID              || '</WpJobId>'            ||
        '<SubOrderStatusId>'   || :new_row.SUB_ORDER_STATUS_ID    || '</SubOrderStatusId>'   ||
		'<IsCancellable>'      || :new_row.IS_CANCELLABLE         || '</IsCancellable>'   ||
		'<IsResubmittable>'    || :new_row.IS_RESUBMITTABLE       || '</IsResubmittable>'   ||
		'<OrgId>'              || :new_row.CLIENT_ORG_ID          || '</OrgId>'              ||
   '</StatusUpdate>';

   message.set_text(message_text);

    --Check for the Client Id
    -- select count(1) into v_count from c30api.client_call_back_config where call_back_type = 'Order' and client_id = :new_row.CLIENT_ORG_ID;
	
     ----------------------------------------
     -- Place message on generic (non-Force)
	 -- queue.
     ----------------------------------------
       -- Only send when status > 0
       IF :new_row.SUB_ORDER_STATUS_ID > 0 THEN
		--IF v_count > 0 THEN        
        
        dbms_aq.enqueue(queue_name => 'C30SDK.ORDER_STATUS_QUEUE',
                 enqueue_options => enqueue_options,
                 message_properties => message_properties,
                 payload => message,
                 msgid => msgid);
       --END IF;
	  END IF;

END;
/

exit;
