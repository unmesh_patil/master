set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/1120_create_table_c30_prov_system_def_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 126_create_table_c30_prov_feature_item_action.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_feature_item_action table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_feature_item_action cascade constraints purge
;
create table c30sdk.c30_prov_feature_item_action(
  feature_action_id               number(10)      not null,
  feature_action_name             varchar2(64)    not null
  )
;
  
create unique index c30sdk.c30_prov_feature_item_ac_pk
  on c30sdk.c30_prov_feature_item_action(feature_action_id)
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_feature_item_action');

--v1.1 added...
begin
-- c30sdk.c30_prov_feature_item_action (based on existing most-common values, account segments 1->4)
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(1, 'ADD');
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(2, 'DISCONNECT');
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(3, 'SUSPEND');
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(4, 'RESUME');
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(5, 'ON');
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(6, 'OFF');										  
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(7, 'SWAP');
insert into c30sdk.c30_prov_feature_item_action(feature_action_id, feature_action_name)
                                          values(8, 'CHANGE');
-- This is only for the Suspend/Resume/Disconnect service, we verify the service before doing any actions aginst it.										  
										  
c30_put_line(null,'Inserted 3 c30_prov_feature_item_action rows ');

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script c30_prov_feature_item_action');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/

--spool off
exit rollback