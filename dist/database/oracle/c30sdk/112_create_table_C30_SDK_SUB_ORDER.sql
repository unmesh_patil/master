-- Release Notes
-- Config 1.0
-- Date: 06/27/2011
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development

drop table C30_SDK_SUB_ORDER;

create table C30_SDK_SUB_ORDER
(
  CLIENT_SUB_ORDER_ID     VARCHAR2(144) NOT NULL,
  CLIENT_ORDER_ID	      VARCHAR2(144),
  CLIENT_SUB_ORDER_NAME	  VARCHAR2(144),
  SUB_ORDER_TYPE          VARCHAR2(40),
  SUB_ORDER_ACTION_TYPE   VARCHAR2(40),
  SUB_ORDER_ACTION_REASON VARCHAR2(2048), 
  ACCT_SEG_ID             NUMBER,
  PAYLOAD                 BLOB,
  BILLING_ACCOUNT_NO 	  VARCHAR2(144),
  EMF_CONFIG_ID		      NUMBER,
  WP_PROCESS_ID 	      VARCHAR2(50),
  WP_JOB_ID 	          VARCHAR2(50),
  SUB_ORDER_STATUS_ID     NUMBER,
  SUB_ORDER_CREATE_DATE   DATE,
  SUB_ORDER_DESIRED_DATE  DATE,
  SUB_ORDER_COMPLETE_DATE DATE,
  SUB_ORDER_LEVEL         NUMBER,
  CLIENT_ORG_NAME         VARCHAR2(255),
  CLIENT_ORG_ID           VARCHAR2(255),
  TRANSACTION_ID          VARCHAR2(512)
)
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
alter table C30_SDK_SUB_ORDER
  add constraint C30_SDK_SO_RDR_PK primary key (CLIENT_SUB_ORDER_ID)
  using index 
  tablespace C30ARBOR_INDEX
  pctfree 20
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
create index C30_SDK_SO_RDR_STS_I on C30_SDK_SUB_ORDER (CLIENT_ORDER_ID, SUB_ORDER_STATUS_ID)
  tablespace C30ARBOR_INDEX
  pctfree 20
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

create index C30_SDK_SO_RDR_PAY_I on C30_SDK_SUB_ORDER (CLIENT_SUB_ORDER_ID, ACCT_SEG_ID)
  tablespace C30ARBOR_INDEX
  pctfree 20
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
create index C30_SDK_SO_TXID_I on C30_SDK_SUB_ORDER (TRANSACTION_ID)
  tablespace C30ARBOR_INDEX
  pctfree 20
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
COMMIT;
EXIT
