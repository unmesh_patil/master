set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/122_create_table_c30_prov_feature_item_def_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 122_create_table_c30_prov_feature_item_def.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_feature_item_def table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_feature_item_def cascade constraints purge
;
create table c30sdk.c30_prov_feature_item_def(
  feature_item_id               number(10)      not null,
  feature_item_name             varchar2(128)    not null,
  feature_item_req_class_name             varchar2(256),
  feature_item_res_class_name             varchar2(256)
  )
;
  
create unique index c30sdk.c30_prov_feature_def_item_pk
  on c30sdk.c30_prov_feature_item_def(feature_item_id)
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_feature_item_def');

--v1.1 added...
begin


insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (101, 'Basic Voice', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (102, 'Suspend Basic Voice', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (103, 'Long Distance', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (104, 'International', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (105, 'Roaming', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (106, 'International Roaming', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (107, 'Lifeline Roaming Block', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (108, 'Caller Identification', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (109, 'Prepaid', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (110, 'Voice Visual Mail', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (111, 'Voice Mail to Email', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (112, 'Short Messaging Service(SMS)/ Text Messaging', 'com.cycle30.prov.request.sps.TextMessagingRequest', 'com.cycle30.prov.resource.sps.TextMessagingResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (113, 'Multimedia Messaging Service (MMS)', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (114, 'Data Service', 'com.cycle30.prov.request.sps.WirelessDataRequest', 'com.cycle30.prov.resource.sps.WirelessDataResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (115, 'Wireless Application Protocol (WAP / IG) ', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (116, 'Blackberry Internet Service (Consumer)', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (117, 'Blackberry Internet Service (Lite)', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (118, 'Blackberry Internet Service (Social)', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (119, 'Blackberry Internet Service (Enterprise / Commercial)', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (120, 'Downloadable Content', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (121, 'Evolution Data Optimized', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (122, 'Devices used for mobile broadband generally connected to a laptop', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (123, 'Wifi Entitlement', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (124, 'Tethering', null, null);
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (125, 'Voice Mail', 'com.cycle30.prov.request.sps.VoiceMailRequest', 'com.cycle30.prov.resource.sps.VoiceMailResource');
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (126, 'Short Code Block', 'com.cycle30.prov.request.sps.TextMessagingRequest', 'com.cycle30.prov.resource.sps.TextMessagingResource');	
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (127, 'Verify Text Service', 'com.cycle30.prov.request.sps.TextMessagingRequest', 'com.cycle30.prov.resource.sps.TextMessagingResource');	
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (128, 'Verify Data Service', 'com.cycle30.prov.request.sps.WirelessDataRequest', 'com.cycle30.prov.resource.sps.WirelessDataResource');	
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (129, 'Verify Voice Service', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');	
insert into C30_PROV_FEATURE_ITEM_DEF (FEATURE_ITEM_ID, FEATURE_ITEM_NAME, FEATURE_ITEM_REQ_CLASS_NAME, FEATURE_ITEM_RES_CLASS_NAME)
values (130, 'Prepaid Suspend', 'com.cycle30.prov.request.sps.BasicVoiceRequest', 'com.cycle30.prov.resource.sps.BasicVoiceResource');

c30_put_line(null,'Inserted 30 c30_prov_feature_item_def rows ');

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script c30_prov_feature_item_def');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/


--spool off
exit rollback