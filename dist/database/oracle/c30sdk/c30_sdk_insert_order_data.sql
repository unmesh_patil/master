  -- Release Notes
  -- Config 1.0
  -- Date: 06/27/2011
  -- Created By: Ranjith Kumar Nelluri
  -- Change Request: C30 SDK Framework Development
 
 CREATE OR REPLACE PROCEDURE c30_sdk_insert_order_data(v_client_order_id          VARCHAR2,
                                                        v_client_order_name          VARCHAR2,
                                                        v_client_id                  VARCHAR2,
                                                        v_order_type                 VARCHAR2,
                                                        v_acct_seg_id                NUMBER,
                                                        v_payload                    BLOB,
                                                        v_is_locked                  NUMBER,
                                                        v_wp_process_id              VARCHAR2,
                                                        v_order_status_id            NUMBER,
                                                        v_order_create_date          TIMESTAMP,
                                                        v_order_desired_date         TIMESTAMP,
                                                        v_order_complete_date        TIMESTAMP,
														v_client_org_name			 varchar2,
														v_client_org_id			     varchar2,
														v_transaction_id             VARCHAR2,
														v_internal_audit_id          VARCHAR2,
														v_is_dependent				 NUMBER,
														v_dependent_order_id		 VARCHAR2,
                                                        c30_sdk_insert_order_data_cv IN OUT cv_types.customer_tp) IS
BEGIN
  -- Inserting data into C30_SDK_ORDER Table.
  INSERT INTO C30_SDK_ORDER
    (CLIENT_ORDER_ID,
     CLIENT_ORDER_NAME,
     CLIENT_ID,
     ORDER_TYPE,
     ACCT_SEG_ID,
     PAYLOAD,
     IS_LOCKED,
     WP_PROCESS_ID,
     ORDER_STATUS_ID,
     ORDER_CREATE_DATE,
     ORDER_DESIRED_DATE,
     ORDER_COMPLETE_DATE,
	 CLIENT_ORG_NAME,
	 CLIENT_ORG_ID,
	 TRANSACTION_ID,
	 INTERNAL_AUDIT_ID,
	 IS_DEPENDENT,
	 DEPENDENT_ORDER_ID,
	 IS_CANCELLABLE,
	 IS_RESUBMITTABLE)
  VALUES
    (v_client_order_id,
     v_client_order_name,
     v_client_id,
     v_order_type,
     v_acct_seg_id,
     v_payload,
     v_is_locked,
     v_wp_process_id,
     v_order_status_id,
     v_order_create_date,
     v_order_desired_date,
     v_order_complete_date,
	 v_client_org_name,
	 v_client_org_id,
	 v_transaction_id,
	 V_internal_audit_id,
	 v_is_dependent,
	 v_dependent_order_id
	 1,
	 1);
      --Commit the insert statement.
  OPEN  c30_sdk_insert_order_data_cv FOR
  SELECT v_client_order_id FROM DUAL;
  COMMIT;
END c30_sdk_insert_order_data;
/
EXIT