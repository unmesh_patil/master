set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/128_create_table_c30_prov_status_def.txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: 128_create_table_c30_prov_status_def.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_prov_status_def table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_prov_status_def cascade constraints purge
;
create table c30sdk.c30_prov_status_def(
  status_id               number(10)      not null,
  status_name             varchar2(256)    not null
  )
;
  
create unique index c30sdk.c30_prov_status_def_pk
  on c30sdk.c30_prov_status_def(status_id)
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_prov_status_def');

--v1.1 added...
begin
-- c30sdk.c30_prov_status_def (based on existing most-common values, account segments 1->4)
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(10, 'Request Initialized');
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(20, 'Request Sent to Provisioning System');
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(21, 'Simulate Request Sent to Provisioning System');
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(30, 'Request is being processed at Provisioning System');
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(60, 'Provisioning SYNC Response Recieved. ');
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(61, 'Provisioning ASYNC Response recieved and the request is being processed at SPS2.');									
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(81, 'Provisioning Request  Simulate Completed');
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(98, 'Provisioning Request  Simulate In Error');									
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(99, 'Provisioning Request In Error');
insert into c30sdk.c30_prov_status_def(status_id, status_name)
                                          values(80, 'Provisioning Request Completed');										  
										  
c30_put_line(null,'Inserted 10 c30_prov_status_def rows ');

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script c30_prov_system_def');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/

--spool off
exit rollback