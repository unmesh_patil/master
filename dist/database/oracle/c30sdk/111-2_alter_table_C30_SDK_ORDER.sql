-- Release Notes
-- Config 1.0
-- Date: 06/09/2013
-- Created By: Umesha Narayana
-- Change Request: C30 SDK Framework Development
-- Add Order manager pick date.

ALTER TABLE C30SDK.C30_SDK_ORDER
ADD (
  ORDER_MANAGER_PICK_DATE DATE
);

EXIT
