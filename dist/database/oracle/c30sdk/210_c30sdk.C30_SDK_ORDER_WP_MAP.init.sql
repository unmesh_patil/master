-- Release Notes
-- Config 1.0
-- Date: 06/27/2011 
-- Created By: Ranjith Kumar Nelluri
-- Modified By: Umesh
-- Change Request: C30 SDK Framework Development


delete from C30_SDK_ORDER_WP_MAP;
commit;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 1, 0, null, null, null, 'Account', 'C30_ACT_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 2, 0, null, null, null, 'Account', 'C30_ACT_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 3, 0, null, null, null, 'Account', 'C30_ACT_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 4, 0, null, null, null, 'Account', 'OCS_Account_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 5, 0, null, null, null, 'Account', 'OCS_Account_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
--insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 5, 0, null, null, null, 'Account', 'OCS_ACCT_ADD_ACS',to_date('01-01-1990', 'dd-mm-yyyy'), null );
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 10, 0, null, null, null, 'Account', 'C30_ACT_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 1, 0, null, null, null, 'Account', 'C30_ACT_Change', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 2, 0, null, null, null, 'Account', 'C30_ACT_Change', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 3, 0, null, null, null, 'Account', 'C30_ACT_Change', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 4, 0, null, null, null, 'Account', 'OCS_Account_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 5, 0, null, null, null, 'Account', 'OCS_Account_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
--insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 5, 0, null, null, null, 'Account', 'OCS_Acct_Chg_ACS',to_date( '01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 10, 0, null, null, null, 'Account', 'C30_ACT_Change', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 1, 1, null, null, null, 'Service', 'C30_Service_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 2, 1, null, null, null, 'Service', 'C30_Service_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 3, 1, null, null, null, 'Service', 'C30_Service_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 4, 1, null, null, null, 'Service', 'OCS_Account_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 5, 1, null, null, null, 'Service', 'OCS_Account_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
--insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 5, 1, null, null, null, 'Service', 'OCS_Acct_Chg_ACS',to_date('01-01-1990', 'dd-mm-yyyy'), null );
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Add', 10, 1, null, null, null, 'Service', 'C30_Service_Add', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 1, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 2, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 3, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 4, 1, null, null, null, 'Service', 'OCS_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 5, 1, null, null, null, 'Service', 'OCS_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
--insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 5, 1, null, null, null, 'Service', 'OCS_Svc_St_C_ACS', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 10, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Disconnect', 11, 1, null, null, null, 'Service', 'DCN_Service_Disc', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 1, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 2, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 3, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 4, 1, null, null, null, 'Service', 'OCS_Service_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 5, 1, null, null, null, 'Service', 'OCS_Service_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
--insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 5, 1, null, null, null, 'Service', 'OCS_Svc_Chg_ACS', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Change', 10, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Suspend', 1, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Suspend', 2, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Suspend', 3, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Suspend', 4, 1, null, null, null, 'Service', 'OCS_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Suspend', 5, 1, null, null, null, 'Service', 'OCS_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
--insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Suspend', 5, 1, null, null, null, 'Service', 'OCS_Svc_St_C_ACS', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Suspend', 10, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Resume', 1, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Resume', 2, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Resume', 3, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Resume', 4, 1, null, null, null, 'Service', 'OCS_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null); 
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Resume', 5, 1, null, null, null, 'Service', 'OCS_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null); 
--insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Resume', 5, 1, null, null, null, 'Service', 'OCS_Svc_St_C_ACS', to_date('01-01-1990', 'dd-mm-yyyy'), null );
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Resume', 10, 1, null, null, null, 'Service', 'C30_Svc_Stat_Chg', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Swap', 1, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Swap', 2, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Swap', 3, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);
insert into C30_SDK_ORDER_WP_MAP (ORDER_TYPE, ACCT_SEG_ID, IS_SERVICE_LEVEL, EMF_CONFIG_ID, ACCOUNT_CATEGORY, ITEM_ID, ITEM_TYPE, WP_PROCESS_ID, ACTIVE_DATE, INACTIVE_DATE) values ('Swap', 10, 1, null, null, null, 'Service', 'C30_Service_Chng', to_date('01-01-1990', 'dd-mm-yyyy'), null);

COMMIT;

EXIT