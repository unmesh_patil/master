-- Create Oracle AQ tables For all the ERROR requests
begin
  sys.dbms_aqadm.create_queue_table(
    queue_table => 'ERROR_QTABLE',
    queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
    sort_list => 'ENQ_TIME',
    compatible => '11.2',
    primary_instance => 0,
    secondary_instance => 0,
    storage_clause => 'pctfree 10 initrans 1 maxtrans 255 storage ( initial 64K minextents 1 maxextents unlimited )');
  commit;
end;
/
begin
  -- Create Oracle Queue
  sys.dbms_aqadm.create_queue(
    queue_name => 'ERROR_QUEUE',
    queue_table => 'ERROR_QTABLE',
    queue_type => sys.dbms_aqadm.normal_queue);
  -- Start the queue
  sys.dbms_aqadm.start_queue('ERROR_QUEUE', true, true);
  -- allow arbor to use the queue
  sys.DBMS_AQADM.grant_queue_privilege (
  	privilege => 'ALL', 
  	queue_name => 'ERROR_QUEUE', 
  	grantee => 'arbor',
  	grant_option => FALSE);
  commit;
end;
/
exit;