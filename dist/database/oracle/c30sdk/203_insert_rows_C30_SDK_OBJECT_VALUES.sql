-- Release Notes
-- Config 1.0
-- Date: 06/27/2011
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development


delete from C30_SDK_OBJECT_VALUES;
commit;

insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('FX1.0/PS2.0', 1, 'FX1.0/PS2.0', 'Factory versioning');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('FX2.0/PS2.0', 1, 'FX2.0/PS2.0', 'Factory versioning');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Empty Exception Message', 1, 'EMP EXC MESS', 'Empty exception message used by the framework.');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core AccountBalance Lookup', 1, 'ACCT LOOKUP ', 'Core Product Account LookUp');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core WorkPointJob Lookup', 1, 'WPJOB LOOKUP ', 'Core WP Job LookUp');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core WorkPointJob Create', 1, 'WPJOB CREATE ', 'Core WP Job Create');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core Order', 1, 'ORDER', 'Core Order Create');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core Bulk Order Job Create', 1, 'ORDER', 'Core Bulk Order Job Create');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('VertexGeoCode Find', 1, 'VertexGeoCode Find', 'VertexGeoCode Find');

insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core Invoice Lookup', 1, 'INVOICE LOOKUP', 'Core Invoice Lookup');
insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core Invoice Charge Lookup', 1, 'INV CHRG LOOKUP', 'Core Invoice Charge Lookup');

insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core Service Inquiry', 1, 'Service Inquiry', 'Core Service Inquiry');

insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE, LANGUAGE_CODE, SHORT_DISPLAY, DISPLAY_VALUE)
values ('Core Service Update', 1, 'Service Update', 'Core Service Update');

commit;

--Prepaid Scripts
--IMSI Search
Insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('IMSISearch Lookup',1,'IMSISearch Lookup','IMSISearch Lookup');

--Prepaid Account Lookup
Insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('PrepaidAccount Lookup',1,'PrepaidAccount Lookup','PrepaidAccount Lookup');

--Prepaid Balance
Insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('PrepaidBalance',1,'OCS Balance/Update','Prepaid Balance/Update');

--Prepaid Inventory Lookup
Insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('PrepaidPhoneInventory Lookup',1,'Prepaid Phone Inventory','PrepaidPhoneInventory Lookup');

--Prepaid Voucher Lookup
Insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('PrepaidVoucher Lookup',1,'Voucher Details','PrepaidVoucher Lookup');

--Client Call Back
Insert into C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('Core ClientCallback',1,'Core ClientCallBack Details','ClientCallBack');
 
--Prepaid Payment Register
Insert into c30sdk.C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('Core Payment Register',1,'Prepaid Payment Details','Core Payment Register');

--Prepaid Payment 
Insert into c30sdk.C30_SDK_OBJECT_VALUES (LW_OBJECT_TYPE,LANGUAGE_CODE,SHORT_DISPLAY,DISPLAY_VALUE) values ('PrepaidPayment',1,'PrepaidPayment','PrepaidPayment');

commit;

EXIT
