set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/c30_sdk_order_status_def.txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: C30_Provisioning_Connector
prompt Script file Name: c30_sdk_order_status_def.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_sdk_order_status_def table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
drop table c30sdk.c30_sdk_order_status_def cascade constraints purge
;
create table c30sdk.c30_sdk_order_status_def(
  status_id               number(10)      not null,
  status_name             varchar2(256)    not null
  )
;
  
create unique index c30sdk.c30_sdk_order_sta_df_pk
  on c30sdk.c30_sdk_order_status_def(status_id)
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_sdk_order_status_def');

--v1.1 added...
begin
-- c30sdk.c30_sdk_order_status_def (based on existing most-common values, account segments 1->4)
insert into c30sdk.c30_sdk_order_status_def(status_id, status_name)
                                          values(0, 'Order Initialized');
insert into c30sdk.c30_sdk_order_status_def(status_id, status_name)
                                          values(10, 'Order InProgress');
insert into c30sdk.c30_sdk_order_status_def(status_id, status_name)
                                          values(20, 'Order Committed');
insert into c30sdk.c30_sdk_order_status_def(status_id, status_name)
                                          values(80, 'Order Completed');
insert into c30sdk.c30_sdk_order_status_def(status_id, status_name)
                                          values(90, 'Order Cancelled');
insert into c30sdk.c30_sdk_order_status_def(status_id, status_name)
                                          values(99, 'Order Failed - InError');									

c30_put_line(null,'Inserted 10 c30_sdk_order_status_def rows ');

commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script c30_sdk_order_status_def');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/

--spool off
exit rollback