-- Release Notes
-- Config 1.0
-- Date: 06/27/2011
-- Created By: Ranjith Kumar Nelluri
-- Change Request: C30 SDK Framework Development
-- To add dependency among the order requests... So that they can be processed in the order of dependency.

alter table C30_SDK_ORDER
add (
  IS_DEPENDENT NUMBER,
  DEPENDENT_ORDER_ID			VARCHAR2(144)
);
COMMIT;

EXIT
