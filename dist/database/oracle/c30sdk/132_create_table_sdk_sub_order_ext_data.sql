set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Ranjith Kumar Nelluri
prompt Script file Name: 132_create_table_sdk_sub_order_ext_data.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Create c30_sdk_sub_order_ext_data
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...
DROP TABLE c30sdk.c30_sdk_sub_order_ext_data CASCADE CONSTRAINTS purge;

CREATE
TABLE C30SDK.C30_SDK_SUB_ORDER_EXT_DATA(
CLIENT_SUB_ORDER_ID		VARCHAR2(144),
TRANSACTION_ID			VARCHAR2(512),
ACCT_SEG_ID			NUMBER,
KEY				VARCHAR2(1000),
VALUE				VARCHAR2(1000),
SEND_FLAG				CHAR(10)
);

CREATE INDEX C30SDK.IDX_SDK_SUB_ORDER_EXT_DATA ON C30SDK.C30_SDK_SUB_ORDER_EXT_DATA
(CLIENT_SUB_ORDER_ID, TRANSACTION_ID);

-- use procedure to set up public synonym and grants (if needed)...
EXEC c30_ddl_standard('C30_SDK_SUB_ORDER_EXT_DATA');
