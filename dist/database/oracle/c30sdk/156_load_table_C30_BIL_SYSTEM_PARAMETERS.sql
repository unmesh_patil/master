set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 05-03-2013
prompt Created By: Umesha Narayana
prompt Script file Name: 153_load_table_C30_BIL_SYSTEM_PARAMETERS.sql
prompt Version: 1.0 Initial Creation for C30_Provisioning_Connector
prompt DETAILED EXPLANATION:
prompt Load C30_BIL_SYSTEM_PARAMETERS
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
begin
  dbms_output.put(chr(10));
  delete c30sdk.C30_BIL_SYSTEM_PARAMETERS;
  c30_put_line(null,'Deleted '||sql%rowcount||' c30sdk.C30_BIL_SYSTEM_PARAMETERS row[s]');

 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','IPTRUNK','192.168.1.2',4,null,null,null);
 
 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','ACCESS_PHONE','1021',4,null,null,null);

 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','AUTO_PAY_FLAG','AUTO_PAY_ON',4,null,null,null);

 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','IPTRUNK','192.168.1.2',5,null,null,null);
 
 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','ACCESS_PHONE','1021',5,null,null,null);

 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('OCS','AUTO_PAY_FLAG','AUTO_PAY_ON',5,null,null,null);
 
 --prod ACS START --
 --Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 --values ('ACSINV','SECRET_KEY','Bz:!3w:YinP/ieuE.3N.st&qL*WVsf8DS8Q#',5,null,null,null);
 
 --Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 --values ('ACSINV','ACS_ORG_ID','AWNFORCEPROD',5,null,null,null);

 --prod ACS END --


--NON prod ACS START --
 
Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
values ('ACSINV','SECRET_KEY','#d1[4vjES}@1Xa*Zu4b;Pz5hr&-;_)Vp*9C4',5,null,null,null);
 
Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
values ('ACSINV','ACS_ORG_ID','AWNFORCEQA',5,null,null,null);
 
--NON prod ACS END --

 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('BLACKHAWK','PRIMARY_ACCOUNT_NMBR','6039534201000000033',4,null,null,null);
 
 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('BLACKHAWK','AQUIRING_ID_CODE','60300003619',4,null,null,null);
 
 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('BLACKHAWK','MERCHANT_TERMINAL_ID','00001     001',4,null,null,null);
 
 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('BLACKHAWK','PRODUCT_ID_UPC','07675016009',4,null,null,null);

 
 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('SPS','ACS_VM_DEFAULT_PASSWORD','1537',5,null,null,null);
 
 Insert into C30_BIL_SYSTEM_PARAMETERS (MODULE_NAME,PARAM_NAME,PARAM_VALUE,ACCOUNT_SEGMENT,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3) 
 values ('SPS','ACS_VVM_SPS_PLAN_ID','13',5,null,null,null);



  c30_put_line(null,'Inserted  12 c30sdk.C30_BIL_SYSTEM_PARAMETERS rows');

  commit;
  c30_put_line(null,'commit;');
  c30_put_line(null,'Successfully completed script for config');
exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/
--spool off
-- the commit is aleady done in the script if successful...
exit rollback