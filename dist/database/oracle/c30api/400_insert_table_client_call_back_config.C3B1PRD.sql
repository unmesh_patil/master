--BIL/CR to Force synchronisation. 
--PRD ENV

Delete from C30API.CLIENT_CALL_BACK_CONFIG;



SET DEFINE OFF;
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000gRb4EAE', 4, 1, 'LifeCycleOrder', '/services/apexrest/LifeCycle', 
    'PRD');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000gRb4EAE', 4, 1, 'Order', '/services/apexrest/OrderStatus', 
    'PRD');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000gRb4EAE', 4, 1, 'Payment', '/services/apexrest/Payment/PaymentLog', 
    'PRD');

Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000gRaaEAE', 5, 1, 'LifeCycleOrder', '/services/apexrest/LifeCycle', 
    'PRD');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000gRaaEAE', 5, 1, 'Order', '/services/apexrest/OrderStatus', 
    'PRD');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000gRaaEAE', 5, 1, 'Payment', '/services/apexrest/Payment/PaymentLog', 
    'PRD');

COMMIT;

exit 