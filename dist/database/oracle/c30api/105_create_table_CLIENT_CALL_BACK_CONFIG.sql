set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/105_create_table_CLIENT_CALL_BACK_CONFIG_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 10-30-2012
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 160_create_table_Client_call_back_config.sql
prompt Version: 1.0 Initial Creation for c30 Bulk Framework
prompt DETAILED EXPLANATION:
prompt Create CLIENT_CALL_BACK_CONFIG table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...


DROP TABLE C30API.CLIENT_CALL_BACK_CONFIG CASCADE CONSTRAINTS PURGE;

CREATE TABLE C30API.CLIENT_CALL_BACK_CONFIG
(
  CLIENT_ID              VARCHAR2(128)     NOT NULL,
  C30_SEGMENTATION_ID    NUMBER                 NOT NULL,
  IS_DEFAULT             NUMBER                 NOT NULL,
  CALL_BACK_TYPE         VARCHAR2(4000)     NOT NULL,
  CALL_BACK_URL          VARCHAR2(256)     NOT NULL,
  ENV_NAME              VARCHAR2(256)     NOT NULL
);

-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('CLIENT_CALL_BACK_CONFIG');

--spool off
exit rollback