set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/1120_create_table_CLIENT_API_VERSION_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 04-11-2013
prompt Created By: Umesha Narayana
prompt Script file Name: 103_create_table_client_api_version.sql
prompt Version: 1.0 Initial Creation for C30 BIL API version functionality
prompt DETAILED EXPLANATION:
prompt Create CLIENT_API_VERSION table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...


DROP TABLE C30API.CLIENT_API_VERSION CASCADE CONSTRAINTS PURGE;

CREATE TABLE C30API.CLIENT_API_VERSION
(
  CLIENT_ID              VARCHAR2(128)     NOT NULL,
  C30_SEGMENTATION_ID    NUMBER                 NOT NULL,
  API_NAME               VARCHAR2(4000)     NOT NULL,
  API_VERSION               VARCHAR2(256)     NOT NULL,
  API_TYPE              VARCHAR2(256)     NOT NULL
);

-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('CLIENT_API_VERSION');

--spool off
exit rollback