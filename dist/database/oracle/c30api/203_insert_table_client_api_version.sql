delete from CLIENT_API_VERSION;
--3=AWN Wholesale -ACS
--4=AWN Prepaid -GCI
--10=Core Product

--DEV Environment
delete from client where client_id in ('LCFAWNGCIDEV','SWAWNGCIDEV','COREPRODUCTQADEV');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCIDEV',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCIDEV',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQADEV',10,'/addresses/geocode','2.0','GET');
commit;

--CFG Environment
delete from client where client_id in ('LCFAWNGCICFG','SWAWNGCICFG','COREPRODUCTQACFG');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCICFG',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCICFG',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQACFG',10,'/addresses/geocode','2.0','GET');
commit;

--TST Environment
delete from client where client_id in ('LCFAWNGCITST','SWAWNGCITST','COREPRODUCTQATST');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCITST',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCITST',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQATST',10,'/addresses/geocode','2.0','GET');
commit;

--UAT Environment
delete from client where client_id in ('LCFAWNGCIUAT','SWAWNGCIUAT');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCIUAT',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCIUAT',4,'/addresses/geocode','2.0','GET');
commit;

--PRD Environment
delete from client where client_id in ('LCFAWNGCIPRD','SWAWNGCIPRD');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCIPRD',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCIPRD',4,'/addresses/geocode','2.0','GET');
commit;

Exit		  
