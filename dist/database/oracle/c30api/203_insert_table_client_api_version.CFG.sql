--3=AWN Wholesale -ACS
--4=AWN Prepaid -GCI
--10=Core Product

--CFG Environment
delete from client_api_version where client_id in ('LCFAWNGCICFG','SWAWNGCICFG','COREPRODUCTQACFG');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCICFG',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCICFG',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQACFG',10,'/addresses/geocode','2.0','GET');
commit;

Exit		  
