--3=AWN Wholesale -ACS
--4=AWN Prepaid -GCI
--10=Core Product

--TST Environment
delete from client_api_version where client_id in ('LCFAWNGCITST','SWAWNGCITST','COREPRODUCTQATST');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCITST',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCITST',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQATST',10,'/addresses/geocode','2.0','GET');
commit;

Exit		  
