--3=AWN Wholesale -ACS
--4=AWN Prepaid -GCI
--10=Core Product

--DEV Environment
delete from client_api_version where client_id in ('LCFAWNGCIDEV','SWAWNGCIDEV','SWAWNACSDEV','COREPRODUCTQADEV10','COREPRODUCTQADEV03','COREPRODUCTQADEV04');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCIDEV',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCIDEV',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNACSDEV',5,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNACSDEV',5,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQADEV10',10,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQADEV03',3,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('COREPRODUCTQADEV04',4,'/addresses/geocode','2.0','GET');

commit;

Exit		  
