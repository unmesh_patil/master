set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool  /opt/app/kenanfx2/arbor/cycbin/deployconfigs/001_create_procedure_c30_ddl_standard_c30api_&_connect_identifier..txt
whenever sqlerror exit
exec user_better_be('c30api');
create or replace procedure c30api.c30_ddl_standard_c30api(
   -- since this is called internally, all parameters are required...
   v_object_name  IN       varchar2
  ,v_owner        IN       varchar2
  ,v_object_type  IN       varchar2
  ,v_body         IN OUT   varchar2 -- for possible email "body"
   )
as
-- Release Notes
-- Config (n/a)
-- Date: 03/27/2012
-- Created By: Chip H.F. LaFurney, Ranjith Kumar Nelluri
-- Defect Resolved: Kenan SDK Framework
--
-- Script file Name: 001_create_procedure_c30_ddl_standard_c30api.sql
-- Version 1.0
-- DETAILED EXPLANATION:
-- Create C30_DDL_STANDARD_c30api procedure
-- Called internally by the C30_DDL_STANDARD procedure, this will not have normal validations.
-- It's job is to grant privileges on a c30api object only.  Regardlesss of what user is logged on,
--    you won't get "ORA-01749: you may not GRANT/REVOKE privileges to/from yourself"
--               or "ORA-01929: no privileges to GRANT"
   l_msg             varchar2(256);
begin

-- no validations!  called internally, so any trimming or uppercase should not be needed

/* for testing only)...
l_msg  := '**** C30_DDL_STANDARD_c30api PROCEDURE ****';
v_body := v_body||l_msg||chr(10)||chr(10);
dbms_output.put_line('(DDLc30) '||l_msg||chr(10));

l_msg := 'Processing user='||user
       ||'; v_object_name='||v_object_name
             ||'; v_owner='||v_owner
       ||'; v_object_type='||v_object_type
;
v_body := v_body||l_msg||chr(10)||chr(10);
dbms_output.put_line('(DDLc30) '||l_msg||chr(10));*/

-- below is set up as "select" statements on usernames and roles that should exist, but won't cause the statements to fail if they don't...
for l_sql IN(
   select 'grant all on '||v_owner||'.'||v_object_name||' to '||username||' with grant option' cmd
     from sys.dba_users
    where username = 'ARBOR'
   UNION
    select 'grant all on '||v_owner||'.'||v_object_name||' to '||username||' with grant option' cmd
     from sys.dba_users
    where username = 'C30SDK'
   UNION
   select 'grant all on '||v_owner||'.'||v_object_name||' to '||role cmd
     from sys.dba_roles
    where role = 'ONEVIEW_TECH'
   UNION
   select 'grant select on '||v_owner||'.'||v_object_name||' to '||role cmd
     from sys.dba_roles
    where role = 'ONEVIEW_SELECTALL'
   and v_object_type IN('TABLE','VIEW','SEQUENCE')
   UNION
   select 'grant '||case when v_object_type IN('TABLE','VIEW','SEQUENCE') then 'select' else 'execute' end||' on '||v_owner||'.'||v_object_name||' to '||username cmd
     from sys.dba_users
    where username = 'C30REPORT'
            ) loop
   l_msg  := l_sql.cmd||';';
   v_body := v_body||l_msg||chr(10)||chr(10);
   dbms_output.put_line(l_msg||chr(10));

   execute immediate l_sql.cmd;

   l_msg  := case when sqlerrm like 'ORA-0000%' then 'Grant succeeded.' else sqlerrm end;
   v_body := v_body||l_msg||chr(10)||chr(10);
   dbms_output.put_line(l_msg||chr(10));
end loop;

exception
   when others then
      v_body := v_body||'ERROR! sqlerrm='||sqlerrm;
      raise;
end c30_ddl_standard_c30api;
/
show errors

create or replace public synonym c30_ddl_standard_c30api for c30api.c30_ddl_standard_c30api
;
-- called internally only; grant only to calling routine owner...
grant all on c30api.c30_ddl_standard_c30api to arborarch with grant option
;
--spool off
exit rollback