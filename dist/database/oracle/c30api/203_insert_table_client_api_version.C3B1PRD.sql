--3=AWN Wholesale -ACS
--4=AWN Prepaid -GCI
--10=Core Product

--PRD Environment
delete from client_api_version;
--GCI
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCIPRD',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCIPRD',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('00Di0000000gRb4EAE',4,'/addresses/geocode','2.0','GET');
--ACS
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNACSPRD',5,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNACSPRD',5,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('00Di0000000gRaaEAE',5,'/addresses/geocode','2.0','GET');

commit;

Exit