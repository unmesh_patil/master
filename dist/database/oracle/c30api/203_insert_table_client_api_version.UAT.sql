--3=AWN Wholesale -ACS
--4=AWN Prepaid -GCI
--10=Core Product

--UAT Environment
delete from client_api_version where client_id in ('LCFAWNGCIUAT','SWAWNGCIUAT');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('LCFAWNGCIUAT',4,'/addresses/geocode','2.0','GET');
Insert into client_api_version (CLIENT_ID,C30_SEGMENTATION_ID,API_NAME,API_VERSION,API_TYPE) 
values ('SWAWNGCIUAT',4,'/addresses/geocode','2.0','GET');
commit;

Exit		  
