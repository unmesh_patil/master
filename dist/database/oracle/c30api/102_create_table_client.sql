set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/1120_create_table_C30_SDK_BULK_JOB_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 10-30-2012
prompt Created By: Ranjith Kumar Nelluri
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 160_create_table_Client.sql
prompt Version: 1.0 Initial Creation for c30 Bulk Framework
prompt DETAILED EXPLANATION:
prompt Create CLIENT table
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...table or view does not exist"...


DROP TABLE C30API.CLIENT CASCADE CONSTRAINTS PURGE;
-- get rid of obsolete table if it exists
DROP TABLE C30API.FORCE_CLIENT CASCADE CONSTRAINTS PURGE;

CREATE TABLE C30API.CLIENT
(
  CLIENT_ID              VARCHAR2(128)     NOT NULL,
  C30_SEGMENTATION_ID    NUMBER                 NOT NULL,
  USERNAME               VARCHAR2(512)     NOT NULL,
  PASSWORD               VARCHAR2(128)     NOT NULL,
  ORG_TOKEN              VARCHAR2(256)     NOT NULL,
  CONSUMER_SECRET        VARCHAR2(256)     NOT NULL,
  CONSUMER_KEY           VARCHAR2(4000)    NOT NULL,
  DATE_ADDED             DATE                   NOT NULL,
  DATE_CHANGED           DATE,
  CHANGED_BY             VARCHAR2(256),
  API_VERSION    VARCHAR2(128) DEFAULT '1.0' NOT NULL,
  constraint client_pk  primary key (CLIENT_ID, C30_SEGMENTATION_ID)
);

-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('CLIENT');

--spool off
exit rollback