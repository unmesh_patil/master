--BIL/CR to Force synchronisation. 

--TST ENV



Delete from C30API.CLIENT_CALL_BACK_CONFIG;


SET DEFINE OFF;
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000eGNoEAM', 4, 0, 'Order', '/services/apexrest/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000eGNoEAM', 4, 0, 'LifeCycleOrder', '/services/apexrest/LifeCycle', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000eGNoEAM', 4, 0, 'Payment', '/services/apexrest/Payment/PaymentLog', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000dkLiEAI', 4, 0, 'Order', '/services/apexrest/GCITraining/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000dNz1', 4, 0, 'Order', '/services/apexrest/ACSOrg1/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000dNyP', 4, 0, 'Order', '/services/apexrest/ACSOrg2/OrderStatus ', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000cPOBEA2', 5, 0, 'Payment', '/services/apexrest/Payment/PaymentLog', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000cPOBEA2', 5, 0, 'Order', '/services/apexrest/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000cPOBEA2', 5, 0, 'LifeCycleOrder', '/services/apexrest/LifeCycle', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000cDNfEAM', 5, 1, 'Payment', '/services/apexrest/PrepaidGCI/Payment/PaymentLog', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000cDNfEAM', 5, 1, 'LifeCycleOrder', '/services/apexrest/PrepaidGCI/LifeCycle', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000cDNfEAM', 5, 1, 'Order', '/services/apexrest/PrepaidGCI/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000c9jrEAA', 5, 0, 'Order', '/services/apexrest/Prepaid_ACS_DEV/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000c7cbEAA', 4, 1, 'Order', '/services/apexrest/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000c7cbEAA', 4, 0, 'LifeCycleOrder', '/services/apexrest/LifeCycle', 
    'TST');
--Insert into C30API.CLIENT_CALL_BACK_CONFIG
--(CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
--ENV_NAME)
--Values
--('00Do0000000ICBxEAO', 4, 0, 'Payment', '/services/apexrest/PPD_GCI_SIT/Payment/PaymentLog', 
--'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000bx9xEAA', 4, 0, 'Order', '/services/apexrest/ACSDev1/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000ZtzYEAS', 5, 0, 'LifeCycleOrder', '/services/apexrest/C30PreUAT/LifeCycle', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000ZtzYEAS', 5, 0, 'Order', '/services/apexrest/C30PreUAT/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000XmAjEAK', 4, 0, 'Order', '/services/apexrest/Prepaid80/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000XmAjEAK', 4, 0, 'LifeCycleOrder', '/services/apexrest/Prepaid80/LifeCycle', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000Hgq4EAC', 4, 0, 'Payment', '/services/apexrest/TestPOC1/Payment/PaymentLog', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000Hgq4EAC', 4, 0, 'LifeCycleOrder', '/services/apexrest/TestPOC1/LifeCycle', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Di0000000Hgq4EAC', 4, 0, 'Order', '/services/apexrest/TestPOC1/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG 
	(CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,
	ENV_NAME) 
	Values 
	('00Do0000000HSRHEA4',4,1,'Order','/services/apexrest/PREPAID_GCI_SIT/OrderStatus',
	'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG 
	(CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,
	ENV_NAME) 
Values 
	('00Do0000000HSQxEAO',5,1,'Order','/services/apexrest/PREPAID_ACSDEV/OrderStatus',
	'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG
   (CLIENT_ID, C30_SEGMENTATION_ID, IS_DEFAULT, CALL_BACK_TYPE, CALL_BACK_URL, 
    ENV_NAME)
 Values
   ('00Do0000000Ho19EAC', 4, 1, 'Order', '/services/apexrest/PREPAIDGCI_DEV/OrderStatus', 
    'TST');
Insert into C30API.CLIENT_CALL_BACK_CONFIG 
	(CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,
	ENV_NAME) 
Values 
	('00DF00000008CYlMAM',5,1,'Order','/services/apexrest/PREPAIDACS_DEV/OrderStatus',
	'TST'); 
Insert into C30API.CLIENT_CALL_BACK_CONFIG 
	(CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,
	ENV_NAME) 
Values 
	('00Do0000000ICC7EAO',5,1,'Order','/services/apexrest/PREPAID_ACS_SIT/OrderStatus',
	'TST');
--Insert into C30API.CLIENT_CALL_BACK_CONFIG 
--(CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,
--ENV_NAME) 
--Values 
--('00Do0000000ICBxEAO',4,0,'Order','/services/apexrest/PPD_GCI_SIT/OrderStatus',
--'TST');	
Insert into C30API.CLIENT_CALL_BACK_CONFIG 
	(CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,
	ENV_NAME) 
Values 
	('00Do0000000IC98EAG',5,1,'Order','/services/apexrest/PrepaidE2ET_ACS/OrderStatus',
	'TST');
--Insert into C30API.CLIENT_CALL_BACK_CONFIG(CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,ENV_NAME) 
--Values ('00Do0000000IEHSEA4',4,1,'Order','/services/apexrest/PrepaidE2ET_GCI/OrderStatus','TST');	
INSERT INTO C30API.client_call_back_config (CLIENT_ID, C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,ENV_NAME )
VALUES('GCIPREPAIDUI',4,1,'Order','/api/v1/rest_order_status/orderStatus','TST');

INSERT INTO C30API.CLIENT_CALL_BACK_CONFIG (CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,ENV_NAME)
VALUES('GCIPREPAIDUI',4,1,'LifeCycleOrder','/api/v1/rest_lifecycle_event/lifecycleevent','TST'); 

INSERT INTO C30API.CLIENT_CALL_BACK_CONFIG (CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,ENV_NAME)
VALUES('GCIPREPAIDUI',4,1,'Payment','/api/v1/rest_lifecycle_event/paymentlog','TST'); 


--INSERT INTO CLIENT_CALL_BACK_CONFIG (CLIENT_ID,C30_SEGMENTATION_ID,IS_DEFAULT,CALL_BACK_TYPE,CALL_BACK_URL,ENV_NAME)VALUES('00Do0000000ICBxEAO',4,0,'LifeCycleOrder','/services/apexrest/PPD_GCI_SIT/LifeCycle','TST');	

COMMIT;

exit 