set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/132_drop_2_c30_stm_feed_views_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: 132_drop_2_c30_stm_feed_views.sql
prompt CA Ticket# : AWN
prompt QC Defect# : Defect 279
prompt System(s)  : K2
prompt Database(s): ALL
prompt Schema(s)  : ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Drop function numbers_only (the actual one is in the C30ARBOR schema)
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: "public synonym to be dropped does not exist" or "table or view does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 11/14/2012 ##       Defect 279 - Drop two views that are deprecated
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
drop public synonym c30_stm_feed_summary_billed;
drop public synonym c30_stm_feed_summary_unbilled;
drop view c30arbor.c30_stm_feed_summary_billed;
drop view c30arbor.c30_stm_feed_summary_unbilled;
--spool off
exit rollback