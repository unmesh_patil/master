set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/105_create_trigger_c30_api_seg_permissions_brtrg_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 105_create_trigger_c30_api_seg_permissions_brtrg.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt
prompt Purpose of Script: Create c30_api_seg_permissions_brtrg trigger
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt mark.doyle@cycle30.com       ## 08/15/2011 ##  Initial Creation for Kenan SQL API
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace trigger c30arbor.c30_api_seg_permissions_brtrg
before insert or update
on c30arbor.c30_api_seg_permissions
for each row
-- Release Notes
-- Config (n/a)
-- Date: 08-15-2011
-- Created By: Mark Doyle
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 105_create_trigger_c30_api_seg_permissions.sql
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.2 Changed for R325955
-- DETAILED EXPLANATION:
-- Create c30_api_seg_permissions_brtrg trigger
--v1.1 Defect 451
--v1.2 the choices for security on packaged entry points are...
--    1) Set up the api_name as "package_name.entry_point_name".  This allows each to have their own security.  The drawback, it requires the "api_name" to be 61 characters (30+1+30).
--    2) Just use the entry_point_name.  The drawback of this option is the possibility of duplicate names (packageA.EPNm and packageB.EPNm -- both have "EPNm").
--    3) Just use the package_name.  The drawback is there would be no differentiation for different entry point security.
-- I chose a hybrid of 2) and 3).  I left the api_name varchar2(30), but you can put set the api_name to any of the following:
--    procedure, package, or function name whether standalone or contained within a package, developer's choice.  (Note updated object types to 'PROCEDURE','PACKAGE','FUNCTION'.)
-- Specifically for R325955, I have a new package that should only be called using the "C30_SF_DRIVER" entry_point.  Therefore, I'll use sys.dba_procedures in place of sys.dba_objects.
declare
  d_count         number(10);
begin
  :new.api_name := lower(trim(:new.api_name));

  select count(*)
  into d_count
  from sys.dba_procedures --v1.2 was dba_objects
  where owner = 'C30ARBOR'
  and object_type IN('PROCEDURE','PACKAGE','FUNCTION') --v1.2 updated types
  and upper(:new.api_name) IN(object_name,procedure_name) --v1.2 allow either (see "choices" above)
  ;

  if d_count = 0 then
    -- v1.1 Defect 451 - Triggers should RAISE errors that calling object[s] will handle in exception section
    raise_application_error(-20010,'API: '||:new.api_name||' does not exist');
  end if;

  -- v1.1 Defect 451 - No exception section in triggers since it becomes "when others then raise:" which is the default
end c30_api_seg_permissions_brtrg;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_api_seg_permissions_brtrg');
--spool off
exit rollback