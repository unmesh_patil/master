set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/125_create_view_c30_settlement_feed_summary_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 125_create_view_c30_settlement_feed_summary.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create c30_settlement_feed_summary view for Settlement Feed processing
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_settlement_feed_summary as
select
-- Release Notes
-- Config (n/a)
-- Date: 09/12/2013
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: N/A
--
-- Script file Name: 125_create_view_c30_settlement_feed_summary.sql
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.0 Initial Creation for R325955
-- DETAILED EXPLANATION:
-- Create c30_settlement_feed_summary view for Settlement Feed processing
       sfb.usage_type                                                "Usage_Type"
     , count(*)                                                      "Number_Of_Calls"
     , to_char(sum(sfb.rated_units_calc),'fm999,999,999,999,990.0')  "Total_Units"
     , sfb.rated_units_type                                          "Units_Type"
     , to_char(sum(sfb.unrounded_amount_calc),'fm999,999,990.0000')  "Total_Cost"
  from c30arbor.c30_settlement_feed_billed sfb
group by sfb.usage_type, sfb.rated_units_type
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_settlement_feed_summary');
--spool off
exit rollback