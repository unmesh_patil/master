set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/101_create_function_c30_get_acct_seg_value_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Script Name: 101_create_function_c30_get_acct_seg_value.sql
prompt CA Ticket# : AWN
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_get_acct_seg_value function for Settlement Feed processing
prompt .................. For a given account segment (v_acct_seg_id) and table keyed by acct_seg_id (v_asi_table),
prompt .................. find the *appropriate row and return its rowid.  *If a row cannot be found for the passed
prompt .................. acct_seg_id and it has a parent_acct_seg_id, find the row for the latter instead.
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: "drop function" may return Oracle error "...object C30_GET_ACCT_SEG_VALUE does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 08/30/2012 ##  1.0 Initial Creation for AWN
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
whenever sqlerror continue
drop function c30arbor.c30_get_acct_seg_value
;
create or replace function c30arbor.c30_get_acct_seg_value(
  v_acct_seg_id           IN      number
 ,v_asi_table             IN      varchar2 -- "asi" stands for acct_seg_id
 ,v_and_clause            IN      varchar2 := null
  )
  return                          rowid
as
-- Release Notes
-- Config (n/a)
-- Date: 08/30/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: AWN
--
-- Script file Name: 101_create_function_c30_get_acct_seg_value.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- [YOUR EMAIL ADDRESS]         ## [[[DATE]]] ##  [v1.x REVISION LOG HERE]
-- DETAILED EXPLANATION:
-- Create c30_get_acct_seg_value function for Settlement Feed processing
  l_sql_stmt                      varchar2(2000);
  l_rowid                         rowid;
  l_acct_seg_id                   c30arbor.c30_acct_seg_def.acct_seg_id%type;
  l_parent_acct_seg_id            c30arbor.c30_acct_seg_def.parent_acct_seg_id%type;

  procedure exec_immediate
  as
  begin
    --**!!c30_put_line(null,'l_acct_seg_id = '||l_acct_seg_id);
    l_sql_stmt := cleantext('select rowid from '||v_asi_table||' where acct_seg_id = '||l_acct_seg_id||' '||v_and_clause,' ','t');
    --**!!c30_put_line(null,'l_sql_stmt = '||l_sql_stmt);
    execute immediate l_sql_stmt into l_rowid;
    --**!!c30_put_line(null,'l_rowid = '||l_rowid);
  end;

begin
l_acct_seg_id := v_acct_seg_id;

begin
exec_immediate;

exception
  when no_data_found then
    --**!!c30_put_line(null,'no_data_found for passed acct_seg_id');

    select c.parent_acct_seg_id
      into l_parent_acct_seg_id
      from c30arbor.c30_acct_seg_def c
     where c.acct_seg_id = l_acct_seg_id;
    --**!!c30_put_line(null,'l_parent_acct_seg_id = '||l_parent_acct_seg_id||' for acct_seg_id = '||l_acct_seg_id);

    if l_parent_acct_seg_id is not null then
      l_acct_seg_id := l_parent_acct_seg_id;

      begin
      exec_immediate;

      exception
        when no_data_found then
          --**!!c30_put_line(null,'(no_data_found for parent_acct_seg_id)');
          null;
      end;
    else
      --**!!c30_put_line(null,'(no parent_acct_seg_id)');
      null;
    end if;
end;

return l_rowid;

/**!! for testing only...
exception
  when others then
    c30_put_line(null,cleantext(sqlerrm,' ','t'));
    raise;*/
end c30_get_acct_seg_value;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_get_acct_seg_value');
--**??...
set termout on
prompt select parameter_value from c30_acct_seg_sys_parameters where rowid = c30arbor.c30_get_acct_seg_value(3,'c30_acct_seg_sys_parameters','and parameter_name = ''debug_switch''');;
prompt exec dbms_output.put_line('x');;
--spool off
exit rollback