set echo off termout on feedback on serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/020_create_view_cmf_package_component_full_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 020_create_view_cmf_package_component_full.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create cmf_package_component_full view for R325955 Settlement Feed Process Improvement
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: "drop table" may return Oracle error "...table or view does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
whenever sqlerror continue
create or replace view c30arbor.cmf_package_component_full as with
  component_definition_xpanded as(
select component_id
     , short_display                                              component_short
     , substr(display_value,1,100)                                component_desc
     , substr(display_value||' ('||component_id||')',1,110)       component_name
  from arbor.component_definition_values
 where language_code = 1)
, package_definition_xpanded as(
select package_id
     , short_display                                              package_short
     , substr(display_value,1,100)                                package_desc
     , substr(display_value||' ('||package_id||')',1,110)         package_name
  from arbor.package_definition_values
 where language_code = 1)
-- Release Notes
-- Config (n/a)
-- Date: 09/12/2013
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: N/A
--
-- Script file Name: 020_create_view_cmf_package_component_full.sql
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.0 Initial Creation for R325955
-- DETAILED EXPLANATION:
-- Create cmf_package_component_full view for R325955 Settlement Feed Process Improvement
select cpc.component_inst_id
     , cpc.component_inst_id_serv
     , cpc.package_inst_id
     , cpc.package_inst_id_serv
     , cpc.component_id
     , cpc.package_id
     , cpc.parent_account_no
     , cpc.parent_subscr_no
     , cpc.parent_subscr_no_resets
     , cpc.component_status
     , cpc.connect_reason
     , cpc.active_dt
     , cpc.inactive_dt
     , cdx.component_short
     , cdx.component_desc
     , cdx.component_name
     , pdx.package_short
     , pdx.package_desc
     , pdx.package_name
     , substr(iam.external_id,1,9)  account_no_desc
     , substr(subscr2extid(cpc.parent_subscr_no,cpc.parent_subscr_no_resets,null,1),1,110)
                                    external_id
     -- the "other" column names...
     , cpc.parent_account_no        account_no
     , cpc.parent_subscr_no         subscr_no
     , cpc.parent_subscr_no_resets  subscr_no_resets
  from arbor.cmf_package_component cpc
  left outer join arbor.cmf
    on     cmf.account_no                =     cpc.parent_account_no
   and     cmf.account_status       not IN    (-2,-3)
   and     cmf.date_active              <= nvl(cpc.inactive_dt,sysdate+1)
   and nvl(cmf.date_inactive,sysdate+1)  >     cpc.active_dt
   and nvl(cmf.date_inactive,sysdate+1) <>     cmf.date_active -- don't use rows where the active and inactive dates are the same
  left outer join component_definition_xpanded cdx
    on cdx.component_id = cpc.component_id
  left outer join package_definition_xpanded pdx
    on pdx.package_id = cpc.package_id
  left outer join arbor.external_id_acct_map iam
    on     iam.account_no                =     cpc.parent_account_no
   and     iam.external_id_type          = nvl(to_number(c30arbor.c30_acct_seg_sys_parameters_vl(cmf.acct_seg_id,'acct_no_ext_id_type')),2)
   and     iam.active_date              <= nvl(cpc.inactive_dt,sysdate+1)
   and nvl(iam.inactive_date,sysdate+1)  >     cpc.active_dt
   and nvl(iam.inactive_date,sysdate+1) <>     iam.active_date -- don't use rows where the active and inactive dates are the same
with read only
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('cmf_package_component_full');
--spool off
exit rollback