set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/120_create_view_c30_settlement_feed_detail_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 120_create_view_c30_settlement_feed_detail.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create c30_settlement_feed_detail view for Settlement Feed processing
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_settlement_feed_detail as
select
-- Release Notes
-- Config (n/a)
-- Date: 09/12/2013
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: N/A
--
-- Script file Name: 120_create_view_c30_settlement_feed_detail.sql
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.0 Initial Creation for R325955
-- DETAILED EXPLANATION:
-- Create c30_settlement_feed_detail view for Settlement Feed processing
       sfb.account_no_desc          "Account_Identifier"
     , sfb.phone_no                 "Phone_No"
     , sfb.billed_cutoff_date_char  "Cutoff_Date"
     , sfb.trans_dt_char            "Usage_Datetime"
     , sfb.point_origin             "Call_From"
     , sfb.point_target             "Call_To"
     , sfb.dialed_digits            "Dialed_Digits"
     , sfb.usage_type               "Usage_Type"
     , sfb.call_from_city           "Call_From_City"
     , sfb.call_from_state          "Call_From_State"
     , sfb.country_code_origin_char "Call_From_Country_Code"
     , sfb.called_city              "Called_City"
     , sfb.called_state             "Called_State"
     , sfb.country_code_target_char "Called_Country_Code"
     , sfb.catalog_id               "Catalog_ID"
     , sfb.imsi                     "IMSI"
     , sfb.primary_units_char       "Units"
     , sfb.units_type               "Units_Type"
     , sfb.rated_units_char         "Rated_Units"
     , sfb.rated_units_type         "Rated_Units_Type"
     , sfb.rate_char                "Rate"
     , sfb.unrounded_amount_char    "Cost"
     , sfb.location                 "Location"
  from c30arbor.c30_settlement_feed_billed sfb
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_settlement_feed_detail');
--spool off
exit rollback