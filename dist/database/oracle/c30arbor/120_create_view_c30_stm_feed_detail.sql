set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/120_create_view_c30_stm_feed_detail_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 120_create_view_c30_stm_feed_detail.sql
prompt CA Ticket# : AWN
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_stm_feed_detail view for Settlement Feed processing
prompt .. This is a "template" only!  It would be very inefficient to render this view without joining with
prompt .... some set of CDR_DATA primary key values msg_id+msg_id2+msg_id_serv[+split_row_num][+cdr_data_partition_key].
prompt .. Instead, it will be used to join against CDR_UNBILLED or CDR_BILLED to complete the associated feed.
prompt .... This prevents the same code from being duplicated in two separate view source code.
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 10/01/2012 ##  1.0 Initial Creation for AWN
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_stm_feed_detail as
with
-- Release Notes
-- Config (n/a)
-- Date: 10/01/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: AWN
--
-- Script file Name: 120_create_view_c30_stm_feed_detail.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 11/14/2012 ##  v1.1 Defect 279 - Remove duplicates coming from join to get IMSI
--                                                "_Fmt" columns become THE columns (no more "as is" columns - output is for Excel)
--                                                Remove "substr" from all lookup values and trim() when used instead
--                                                Remove join on customer_id_equip_map; don't use rows where the active and inactive dates are the same
-- DETAILED EXPLANATION:
-- Create c30_stm_feed_detail view for Settlement Feed processing
  state_values_expanded as(
select country_code
     , state_abbr
     , display_value
  from arbor.state_values
 where language_code = 1
)
, usage_types_xpanded as(
select uty.type_id_usg
     , uty.raw_units_type
     , uty.use_point_class_origin
     , uty.use_point_class_target
  from arbor.usage_types uty
 where uty.type_id_usg IN(select distinct fut.type_id_usg from c30arbor.c30_stm_feed_usage_type fut)
)
, units_type_xpanded as(
select units_type
     , display_value
  from arbor.units_type_values
 where language_code  = 1
)
, lookup_switch_xpanded as(
select gsmsiteid
     , sitecity
  from c30digitalroute.lookup_switch_location
)
-- the purpose of this view is to do all column manipulations (like "trim") before being used by the unbilled and billed views...
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
-- this is the first object in the C30_STM_FEED*/C30_SUM_TYPE* settlement feed series with mixed case column names like "Xxxx_Yyyy".
-- any column like this is for output to the CSV files where the agreed-upon column headers were mixed case and underscores, no spaces.
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
select trim(sfg.account_no_desc)                                "Account_Identifier"
     , trim(iem.external_id)                                    "Phone_No"
     -- TBD within other views using this one:                  "Cutoff_Date"
     , to_char(sfg.trans_dt,'yyyy/mm/dd hh24:mi:ss')            "Usage_Datetime"
     , trim(sfg.point_origin)                                   "Call_From"
     , trim(sfg.point_target)                                   "Call_To"
     -- base Dialed_Digits on "incoming" vs "outgoing" usage types; SMS and Data are null
     , trim(case
              when sfg.type_id_usg IN(952,953,954,963,964,965,966,967,992,993,994,1062) then -- OUTGOING; point_target was dialed
                sfg.point_target
              when sfg.type_id_usg IN(951,961,962,991,1061) then -- INCOMING; point_origin was dialed
                sfg.point_origin
              else
                null
            end)                                                "Dialed_Digits"
     , sut.usage_type                                           "Usage_Type"
     , trim(upo.point_city)                                     "Call_From_City"
     , trim(coalesce(svo.display_value
                    ,upo.point_state_abbr
                    ,pto.display_value))                        "Call_From_State"
     , decode(sfg.country_code_origin
             ,0,null,sfg.country_code_origin)                   "Call_From_Country_Code"
     , trim(upt.point_city)                                     "Called_City"
     , trim(coalesce(svt.display_value
                    ,upt.point_state_abbr
                    ,ptt.display_value))                        "Called_State"
     , decode(sfg.country_code_target
             ,0,null,sfg.country_code_target)                   "Called_Country_Code"
     , trim(iid.catalog_id)                                     "Catalog_ID"
     -- internal spaces prevent Excel's scientific notation:  first 3 digits are the Mobile Country Code (MMC),
     -- second 3 digits are the Mobile Network Code (MNC), and last 9 digits are the Mobile Subscription Identification Number (MSIN)...
     , substr(trim(ie2.external_id),1,3)||' '||substr(trim(ie2.external_id),4,3)||' '||substr(trim(ie2.external_id),7,9)
                                                                "IMSI"
     , trim(to_char(sfg.primary_units,'999,999,999,999,990'))   "Units"
     , trim(ut1.display_value)                                  "Units_Type" --v1.1 use trim
     , trim(to_char(sfg.rated_units,'999,999,999,999,990'))     "Rated_Units"
     , trim(ut2.display_value)                                  "Rated_Units_Type" --v1.1 use trim
     , trim(to_char(sfg.add_unit_rate,'999,990.000000'))        "Rate"
     , trim(to_char(sfg.unrounded_amount,'99,999,990.000000'))  "Cost"
     , trim(case --v1.1 use trim
        -- use the 1st 33 bytes of annotation for provider_id = 2...
        when sfg.provider_id = 2 then
          substr(sfg.annotation,1,33)
        when sfg.provider_id != 2 then
          lsx.sitecity
        else
          null
            end)                                                "Location"
     --**************************************************************
     --************ INTERNAL USE ONLY COLUMNS BELOW HERE ************
     --**************************************************************
     , sfg.min_rate_dt
     , sfg.acct_seg_id
     , sfg.acct_seg_id_desc
     , sfg.msg_id
     , sfg.msg_id2
     , sfg.msg_id_serv
     , sfg.cdr_data_partition_key
     , sfg.element_id
     , sfg.type_id_usg
     , sfg.bill_class
     , sfg.provider_id
     , sfg.jurisdiction
     , sfg.account_no
     , sfg.subscr_no
     , sfg.subscr_no_resets
     , sfg.external_id
     , sfg.external_id_type
     , sfg.point_origin
     , sfg.country_code_origin
     , sfg.point_id_origin
     , sfg.point_target
     , sfg.country_code_target
     , sfg.point_id_target
     , sfg.rate_dt
     , sfg.billing_units_type
     , sfg.component_id
     , sfg.add_implied_decimal
     , sfg.annotation
     , sfg.customer_tag
     , sfg.seqnum_rate_usage
     , sfg.seqnum_rate_usage_overrides
     , sfg.implied_decimal
     , sfg.add_unit_rate
     , sfg.lk_key
     , sfg.min_split_row_num
     , sfg.max_split_row_num
     , sfg.min_bill_class
     , sfg.max_bill_class
     , sfg.min_jurisdiction
     , sfg.max_jurisdiction
     , sfg.trans_dt
     , sfg.primary_units
     , sfg.rated_units
     , sfg.unrounded_amount
     , sfg.cdr_rowcount
     , utx.raw_units_type
     , utx.use_point_class_origin
     , utx.use_point_class_target
     , svc.emf_config_id
     , cir.default_external_id_type
     , cpc.component_inst_id
     , cpc.component_inst_id_serv
     , pto.display_value                        call_from_text
     , ptt.display_value                        called_text
  from c30arbor.c30_stm_feed_grouped sfg
  join usage_types_xpanded utx
    on utx.type_id_usg = sfg.type_id_usg
  join arbor.service svc
    on svc.subscr_no        = sfg.subscr_no
   and svc.subscr_no_resets = sfg.subscr_no_resets
  join arbor.emf_config_id_ref cir
    on cir.emf_config_id = svc.emf_config_id
  --* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  --v1.1 the author hates this logic, but it's not too inefficient since the vast majority of joins will be true on the 1st condition.
  --     for customer_id_equip_map and cmf_package_component we were running into join issues -- missing rows or double rows -- when
  --     usage occurred on the inactive date.  the latter is midnight but the trans_dt always has a time component.  since in Kenan the
  --     inactive date is exclusive, looking for strictly greater than missed end-dated records.  if you look for "or equal" and trunc
  --     the inactive date, you risk picking up a new entry that starts on the same day (double rows).  so if you get a hit on trans_dt
  --     equals the inactive date, you have to make sure there isn't a new row that starts on that same date.
  --* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  join arbor.customer_id_equip_map iem
    on iem.external_id_type = cir.default_external_id_type
   and iem.subscr_no        = sfg.subscr_no
   and iem.subscr_no_resets = sfg.subscr_no_resets
   and iem.active_date     <= sfg.trans_dt
   and( -- open 1 (normal "greater than" inactive date logic)
        (nvl(iem.inactive_date,sfg.trans_dt+1) > sfg.trans_dt)
      or( -- open 2 (inactive date equals [truncated] trans_dt...)
         nvl(iem.inactive_date,sfg.trans_dt+1) = trunc(sfg.trans_dt)
     and 1 > ( -- open 3 (...and no new row is active right on the transaction date)
              select count(*)
                from arbor.customer_id_equip_map xxx
               where     xxx.external_id_type              =       cir.default_external_id_type
                 and     xxx.subscr_no                     =       sfg.subscr_no
                 and     xxx.subscr_no_resets              =       sfg.subscr_no_resets
                 and     xxx.active_date                   = trunc(sfg.trans_dt)
                 and nvl(xxx.inactive_date,sfg.trans_dt+1) > trunc(sfg.trans_dt)
             ) -- close 3
        ) -- close 2
      ) -- close 1
  join arbor.cmf_package_component cpc
    on cpc.component_id                = sfg.component_id
   and cpc.parent_account_no           = sfg.account_no
   and cpc.parent_subscr_no            = sfg.subscr_no
   and cpc.parent_subscr_no_resets     = sfg.subscr_no_resets
   and cpc.active_dt                  <= sfg.trans_dt
   and( -- open 1 (normal "greater than" inactive date logic)
        (nvl(cpc.inactive_dt,sfg.trans_dt+1) > sfg.trans_dt)
      or( -- open 2 (inactive date equals [truncated] trans_dt...)
         nvl(cpc.inactive_dt,sfg.trans_dt+1) = trunc(sfg.trans_dt)
     and 1 > ( -- open 3 (...and no new row is active right on the transaction date)
              select count(*)
                from arbor.cmf_package_component xxx
               where     xxx.component_id                =       sfg.component_id
                 and     xxx.parent_account_no           =       sfg.account_no
                 and     xxx.parent_subscr_no            =       sfg.subscr_no
                 and     xxx.parent_subscr_no_resets     =       sfg.subscr_no_resets
                 and     xxx.active_dt                   = trunc(sfg.trans_dt)
                 and nvl(xxx.inactive_dt,sfg.trans_dt+1) > trunc(sfg.trans_dt)
             ) -- close 3
        ) -- close 2
      ) -- close 1
  --
  -- the c30_stm_feed_usage_type lookup table does NOT have the account segment inheritance
  -- the actual one has to be there -- it does not "defer" to a parent_acct_seg_id value
  -- we have made sure this join will work in the c30_stm_feed_grouped view...
  join c30arbor.c30_stm_feed_usage_type sut
    on     sut.type_id_usg                   =     sfg.type_id_usg
   and nvl(sut.bill_class,-909)              = nvl(sfg.bill_class,-909)
   and nvl(sut.jurisdiction,-909)            = nvl(sfg.jurisdiction,-909)
   and     sut.active_date                  <=     sfg.trans_dt
   and nvl(sut.inactive_date,sfg.trans_dt+1) >     sfg.trans_dt
  join c30arbor.c30_external_inst_id_def iid
    on iid.acct_seg_id              = sfg.acct_seg_id
   and iid.component_inst_id        = cpc.component_inst_id
   and iid.component_inst_id_serv   = cpc.component_inst_id_serv
   and iid.primary_subscr_no        = cpc.parent_subscr_no
   and iid.primary_subscr_no_resets = cpc.parent_subscr_no_resets
  join units_type_xpanded ut1
    on ut1.units_type = utx.raw_units_type
  join units_type_xpanded ut2
    on ut2.units_type = sfg.billing_units_type
  --* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  --* "left outer join"s - possibly no IMSI (20015); rest are based on the value of use_point_class_origin/target *
  --* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  -- note: date criteria using "sfg.trans_dt" assures the joined row was active on the transaction date
  left outer join arbor.customer_id_equip_map ie2
    on     ie2.external_id_type              = 20015 -- for the IMSI
   and     ie2.subscr_no                     = sfg.subscr_no
   and     ie2.subscr_no_resets              = sfg.subscr_no_resets
   --v1.1 Change join to match others getting one row based on what is active on trans_dt
   and     ie2.active_date                  <= sfg.trans_dt
   and nvl(ie2.inactive_date,sfg.trans_dt+1) > sfg.trans_dt
   and nvl(ie2.inactive_date,sysdate+1)     <> ie2.active_date --v1.1 don't use rows where the active and inactive dates are the same
  left outer join arbor.usage_points upo -- "upo" = usage_points for origin
    on upo.point_id               = sfg.point_id_origin
   and utx.use_point_class_origin = 1
  left outer join state_values_expanded svo -- "svo" = state_values for origin
    on svo.country_code = upo.country_code
   and svo.state_abbr   = upo.point_state_abbr
  left outer join arbor.usage_points_text pto -- "pto" = usage_points_text for origin
    on pto.point_id      = upo.point_id
   and pto.language_code = 1
  left outer join arbor.usage_points upt -- "upt" = usage_points for target
    on upt.point_id               = sfg.point_id_target
   and utx.use_point_class_target = 1
  left outer join state_values_expanded svt -- "svt" = state_values for target
    on svt.country_code = upt.country_code
   and svt.state_abbr   = upt.point_state_abbr
  left outer join arbor.usage_points_text ptt -- "ptt" = usage_points_text for target
    on ptt.point_id      = upt.point_id
   and ptt.language_code = 1
  left outer join lookup_switch_xpanded lsx
    on lsx.gsmsiteid IN(substr(sfg.annotation,1,6),substr(sfg.customer_tag,1,6))
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_stm_feed_detail');
--...
--select count(*), sum(cdr_rowcount) cdr_rowcount from c30arbor.c30_stm_feed_detail;
--prompt How many have "Location" null...
--select count(*) from c30arbor.c30_stm_feed_detail where "Location" is null;
--spool off
exit rollback