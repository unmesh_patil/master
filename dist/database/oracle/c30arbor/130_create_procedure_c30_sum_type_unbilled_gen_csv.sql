set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/130_create_procedure_c30_sum_type_unbilled_gen_csv_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 130_create_procedure_c30_sum_type_unbilled_gen_csv.sql
prompt CA Ticket# : AWN
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_sum_type_unbilled_gen_csv procedure for Settlement Feed processing
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 10/01/2012 ##  1.0 Initial Creation for AWN
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace procedure c30arbor.c30_sum_type_unbilled_gen_csv(
  v_acct_seg_id           IN      number
 ,v_put_line              IN      pls_integer := 0 -- 0=output to utl_file only; 1=output to utl_file and dbms_output; 2=output to dbms_output only
)
is
-- Release Notes
-- Config (n/a)
-- Date: 10/01/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: AWN
--
-- Script file Name: 130_create_procedure_c30_sum_type_unbilled_gen_csv.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 10/11/2012 ##  v1.1 Defect 234 - Change default for v_put_line; handle if null is explicitly passed
-- chip.lafurney@cycle30.com    ## 11/14/2012 ##  v1.2 Defect 279 - Only CSV file created is exactly from c30_stm_feed_detail_unbilled view
-- DETAILED EXPLANATION:
-- Create c30_sum_type_unbilled_gen_csv procedure for Settlement Feed processing
  c_put_line             CONSTANT pls_integer := nvl(v_put_line,0); -- keep this in sync with the v_put_line default above
  -- if we are writing to utl_file, no need to save the output if dbms_output limit reached...
  c_save_overflow        CONSTANT boolean     := case when c_put_line IN(0,1) then false else true end;
  -- if we are writing to utl_file ONLY, then "no dbms_output" is non-null...
  c_no_dbms_output       CONSTANT varchar2(1) := case when c_put_line = 0 then 'x' else null end;

  l_file                          utl_file.file_type;
  l_acct_seg_id                   arbor.cmf.acct_seg_id%type;
  l_min_cutoff_date               c30arbor.c30_sum_type_unbilled."Cutoff_Date"%type;
  l_max_cutoff_date               c30arbor.c30_sum_type_unbilled."Cutoff_Date"%type;
  l_rec_count                     pls_integer;
  l_csv_file_name                 varchar2(50);

begin
c30writeTransactionLog('c30_sum_type_unbilled_gen_csv called! v_put_line='||nvl(to_char(v_put_line),'<null>'),null
                      ,p_acct_seg_id => v_acct_seg_id
                      );

if c_put_line > 0 then
  dbms_output.put(chr(10));
end if;

if c_put_line < 2 then
  -- create output log; it is overwritten each time so it has a static name...
  l_file := utl_file.fopen('DEPLOYCONFIGS','summary_by_usage_type_unbilled.log','w');
  c30_put_line(l_file,'C30_SUM_TYPE_UNBILLED_GEN_CSV (CSTUGC) Started at '||systimestamp,true,c_save_overflow,c_no_dbms_output);
  -- otherwise leave l_file unopened (null)
elsif c_put_line = 2 then
  if c_no_dbms_output is null then
    c30_put_line(l_file,'C30_SUM_TYPE_UNBILLED_GEN_CSV (CSTUGC) Started at '||systimestamp||' (dbms_output only)',true,c_save_overflow,c_no_dbms_output);
  else
    -- make sure these aren't mutually exclusive! logic error if c_put_line=2 and c_no_dbms_output is not null!
    c30writeTransactionLog('C30KSASTU001 LOGIC ERROR! c_put_line: '||c_put_line||'; c_no_dbms_output: '||c_no_dbms_output,'error');
  end if;
else
  c30writeTransactionLog('C30KSASTU002 INPUT ERROR! c_put_line: '||c_put_line||' (must be 0, 1, or 2)','error');
end if;

-- validate account segment and load segmented system parameters into a context...
c30setCtxParameters(v_acct_seg_id);
l_acct_seg_id := c30_sys_param('acct_seg_id');
c30_put_line(l_file,'..(CSTUGC) Account Segment: '||l_acct_seg_id,true,c_save_overflow,c_no_dbms_output);

-- ensure segment has the necessary permissions to execute the API
c30validateSegPerms(l_acct_seg_id,'c30_sum_type_unbilled_gen_csv');
c30_put_line(l_file,'..(CSTUGC) Executed c30validateSegPerms',true,c_save_overflow,c_no_dbms_output);

-- create the output summary CSV file; output file has the cutoff date[s] suffix...

-- get cutoff date[s] (these should match the date[s] from the detail above)...
select  min("Cutoff_Date"),max("Cutoff_Date"),     count(*)
  into l_min_cutoff_date, l_max_cutoff_date, l_rec_count
  from c30arbor.c30_sum_type_unbilled
;
if l_rec_count > 0 then
  c30_put_line(l_file,'..(CSTUGC) View c30_sum_type_unbilled record count: '||l_rec_count,true,c_save_overflow,c_no_dbms_output);
else
  c30writeTransactionLog('C30KSASTU004 no records on c30_sum_type_unbilled','error');
end if;

if l_min_cutoff_date = l_max_cutoff_date then
  l_csv_file_name := 'summary_by_usage_type_unbilled_'||replace(l_min_cutoff_date,'/')||'.csv';
else
  l_csv_file_name := 'summary_by_usage_type_unbilled_'||replace(l_min_cutoff_date,'/')||'_to_'||replace(l_max_cutoff_date,'/')||'.csv';
end if;

c30_put_line(l_file,'..(CSTUGC) Creating summary CSV file: '||l_csv_file_name,true,c_save_overflow,c_no_dbms_output);
c30_dump_table_to_csv(
  v_table_name      => 'c30arbor.c30_sum_type_unbilled'
 ,v_directory       => 'DEPLOYCONFIGS'
 ,v_csv_file        => l_csv_file_name
 ,v_del             => '","'
 ,v_put_line        => c_put_line
);

if utl_file.is_open(l_file) then
  utl_file.fclose(l_file);
end if;

exception
  when others then
    if utl_file.is_open(l_file) then
      c30_put_line(l_file,'ERROR: '||cleantext(sqlerrm,' ','t'),true,c_save_overflow,c_no_dbms_output);
      utl_file.fclose(l_file);
    else
      c30_put_line(null,'ERROR: '||cleantext(sqlerrm,' ','t'),true,c_save_overflow,c_no_dbms_output);
    end if;

    -- if the error is internally-generated, just "raise" it...
    if instr(sqlerrm,'C30KSA') > 0 then
      raise;
    else
      c30writeTransactionLog('C30KSASTU999 ERROR: '||sqlerrm,'error');
    end if;
end c30_sum_type_unbilled_gen_csv;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_sum_type_unbilled_gen_csv');
--spool off
exit rollback