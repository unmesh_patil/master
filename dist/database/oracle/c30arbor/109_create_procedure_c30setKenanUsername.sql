set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/109_create_procedure_c30setKenanUsername_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 109_create_procedure_c30setKenanUsername.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30setKenanUsername procedure
prompt NOTE ON "105" NUMBERING - Any object should be able to call this; trigger scripts are also 105, but they don't use this;
prompt ......................... this uses 102-level procedure c30writeTransactionLog
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: "drop procedure" may return Oracle error "...object C30SETKENANUSERNAME does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 04/11/2012 ##  Initial Creation for Kenan SQL API
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
whenever sqlerror continue
drop procedure c30arbor.c30setKenanUsername
;
create or replace procedure c30arbor.c30setKenanUsername(
  v_username          IN  varchar2
 ,v_username_type     IN  varchar2 := null -- default of 'f' set below
 ,v_email_address     IN  varchar2 := null
)
is
-- Release Notes
-- Config (n/a)
-- Date: 04-11-2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 109_create_procedure_c30setKenanUsername.sql
-- Version 1.0 Initial Creation for Kenan SQL API
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 05/07/2012 ##  v1.1 On instant "return", write "user" columns in log table as null:  from TRANSACTION_ID thru FORCE_ID
--                                                (those not automatic in c30_transaction_log_bitrig)
-- DETAILED EXPLANATION:
-- Create c30setKenanUsername procedure
-- Force has a username that can be up to 80 characters; in the Kenan SQL API (and Kenan in general), the limit
-- .. is 30 characters.  This creates and stores a unique, hashed, shorter name for any Force username that
-- .. exceeds 30 bytes (otherwise, the two values are the same).
-- The table "c30_user_logons" supports the maintenance and uniqueness of the username translations.
-- Parameters...
-- .. 1) v_username is the Force or Kenan username.  It cannot be null.
-- .. 2) v_username_type is "f" if the Force username is passed; "k" if the Kenan username is passed
-- .. 3) v_email_address is optional; set it to an email address if you want emails that would detail processing steps
-- Making this autonomous so the insert to c30_user_logons will always be committed
PRAGMA AUTONOMOUS_TRANSACTION;

  -- some objects don't have a username parameter like v_action_who; default this to "user" if null...
  l_username            c30arbor.c30_user_logons.force_username%type := trim(nvl(v_username,user));
  l_username_type       char(1)                                      := substr(trim(lower(v_username_type)),1,1);
  -- if this isn't passed, no emails are sent (except in unusual situations)
  l_email_address       varchar2(81)                                 := trim(v_email_address);
  l_kenan_username      varchar2(30);
  l_trailing_54         varchar2(54);
  l_body                varchar2(4000);
  l_msg                 varchar2(256);

begin
l_msg  := '**** c30setKenanUsername procedure ****';
l_body := l_body||l_msg||chr(10)||chr(10);
l_msg  := 'v_username="'||nvl(v_username,'<null>')||'"; length(l_username)='||length(l_username)
                                                     ||'; v_username_type="'||nvl(v_username_type,'<null>')||'"'
                                                     ||'; v_email_address="'||nvl(v_email_address,'<null>')||'"'
;
l_body := l_body||l_msg||chr(10)||chr(10);
c30writeTransactionLog(l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

-- "f" tells us that "v_action_who" is perhaps from Force; "k" means from Kenan
if l_username_type is null then
  if l_username = user then
    l_msg  := 'Null l_username_type set to "k"';
    l_body := l_body||l_msg||chr(10)||chr(10);
    l_username_type := 'k';
  else
    l_msg  := 'Null l_username_type set to "f"';
    l_body := l_body||l_msg||chr(10)||chr(10);
    l_username_type := 'f';
  end if;
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
end if;

-- trivial validation of email address (could there be an email address like "x@y.z"?)...
if  instr(l_email_address,'@') > 1
and instr(l_email_address,'.') > 3
and length(l_email_address)    > 4 then
  l_msg  := 'Using l_email_address="'||l_email_address||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
end if;

-- validate username type; if l_username_type is 'k', just use it; set the one context value and return
-- NOTE we don't really care if there's a table entry for some internal application that has a username that'll never need a translation
if l_username_type = 'f' then
  null; -- remainder of procedure deals with this
elsif l_username_type = 'k' then
  l_kenan_username := l_username; -- we want this to blow up using standard Oracle error if length(l_username) > 30

  -- it would be unusual, but set the context if it isn't set already...
  if c30_sys_param('kenan_username') is null then
    l_msg  := 'With l_username_type="k", c30_sys_param(''kenan_username'') is null (UNUSUAL?)';
    l_body := l_body||l_msg||chr(10)||chr(10);
    c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    c30arbor.c30_ctx.set_parameter('kenan_username',l_kenan_username);
  elsif l_kenan_username <> c30_sys_param('kenan_username') then
    l_msg  := 'With l_username_type="k", l_kenan_username <> c30_sys_param(''kenan_username'')="'||c30_sys_param('kenan_username')||'" (UNUSUAL?)';
    l_body := l_body||l_msg||chr(10)||chr(10);
    c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    c30arbor.c30_ctx.set_parameter('kenan_username',l_kenan_username);
  end if;

  l_msg  := 'Instant "return" (l_username_type="k") - kenan_username="'||c30_sys_param('kenan_username')||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  if l_email_address is not null then
    c30_send_email(l_email_address,'From c30setKenanUsername',l_body);
  end if;
  return;
else
  c30writeTransactionLog('C30KSAKUN001 Username type ("'||l_username_type||'") must be "f"[orce] or "k"[enan]','error');
end if;

-- when a procedure is called INTERNALLY, the "v_action_who" is already set to the "kenan_username"; EXTERNAL calls are probably "force_username" (below)
-- unfortunately, if a procedure can be called either way, it cannot set the v_username_type value with any confidence
-- so, if the passed l_username is already in the context as "kenan_username", we're done!...
if  l_username = c30_sys_param('kenan_username')
and c30_sys_param('force_username') is not null then
  l_msg  := 'Instant "return" - kenan_username="'||c30_sys_param('kenan_username')||'" (already in context); force_username="'||c30_sys_param('force_username')||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  if l_email_address is not null then
    c30_send_email(l_email_address,'From c30setKenanUsername',l_body);
  end if;
  return;
end if;

-- find out if he passed l_username is already in the context as "force_username"; if so, we're done!
-- since we don't really know the value of l_kenan_username, only do this if it is not null...
if  l_username = c30_sys_param('force_username')
and c30_sys_param('kenan_username') is not null then
  l_msg  := 'Instant "return" - force_username="'||c30_sys_param('force_username')||'" (already in context); kenan_username="'||c30_sys_param('kenan_username')||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  if l_email_address is not null then
    c30_send_email(l_email_address,'From c30setKenanUsername',l_body);
  end if;
  return;
end if;

-- at this point we've ruled out:
--  1) a Kenan username was explicitly passed (via v_username_type "k")
--  2) the username passed matched the Kenan username already in the context
--  3) the username passed matched the Force username already in the context
-- so, we should clear out the entire context before proceeding (it's a brand new transaction)
l_msg  := 'Calling c30arbor.c30_ctx.del_context - force_username="'||c30_sys_param('force_username')||'"; kenan_username="'||c30_sys_param('kenan_username')||'"';
l_body := l_body||l_msg||chr(10)||chr(10);
c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
c30arbor.c30_ctx.del_context;

-- read c30_user_logons for force_username
begin
  select   kenan_username
    into l_kenan_username
    from c30arbor.c30_user_logons
   where force_username = l_username
  ;
  c30arbor.c30_ctx.set_parameter('force_username',l_username);
  c30arbor.c30_ctx.set_parameter('kenan_username',l_kenan_username);
  l_msg  := 'Found l_username="'||l_username||'"; l_kenan_username="'||l_kenan_username||'" (set and return)';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  -- we have to update c30_user_logons just to get the lastest logon information stored (by its trigger)...
  update c30arbor.c30_user_logons
     set kenan_username = l_kenan_username -- note this is a "fake" update since this is the current value
   where force_username = l_username
  ;
  l_msg  := 'Updated '||sql%rowcount||' c30_user_logons row';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
  commit;
  l_msg  := 'Issued commit; (found on table; will return)';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  if l_email_address is not null then
    c30_send_email(l_email_address,'From c30setKenanUsername',l_body);
  end if;
  return;
exception
  when no_data_found then
    l_msg  := 'No_data_found for l_username="'||l_username||'"';
    l_body := l_body||l_msg||chr(10)||chr(10);
    c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
end;

-- it's the first login for this Force username; check how long it is...
if length(l_username) <= 30 then
  -- the Force and KSA usernames are the same...
  l_kenan_username := l_username;
  l_msg  := '<= 30 l_username="'||l_username||'"; l_kenan_username="'||l_kenan_username||'" (set, insert, and return)';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
  c30arbor.c30_ctx.set_parameter('force_username',l_username);
  c30arbor.c30_ctx.set_parameter('kenan_username',l_kenan_username);

  insert into c30arbor.c30_user_logons(  force_username,  kenan_username)
                                values(l_username,l_kenan_username)
  ;
  l_msg  := 'Inserted 1 c30_user_logons row (Force username <= 30)';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
  commit;
  l_msg  := 'Issued commit; (Force username <= 30; will return)';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  if l_email_address is not null then
    c30_send_email(l_email_address,'From c30setKenanUsername',l_body);
  end if;
  return;
end if;

-- ************************************************ finally we get to this object's raison d'�tre!! ************************************************

-- starting with a blank, then moving thru the numbers, generate hash until we have a new unique value (99.99% of the time, should be on first try)...
--************ NOTICE, IF THIS EVER FAILS, CALL THE AUTHOR (HE'LL BE STUNNED), BUT ALL YOU HAVE TO DO IS EXPAND THE LIST TO INCLUDE ALPHA CHARACTERS ************
-- some odd logic here (sorry!):  the first value we want is a blank but "unstring" treats the first value as null; we use "nvl" to make that a single character
-- then, we really would like a "pure" hash on the first go-round, so we "rtrim" those blanks off the end; after that, the numbers won't rtrim anyway...
for l in (select nvl(column_value,' ') fill from table(cast(csv_unstring(' ,0,1,2,3,4,5,6,7,8,9') as varchar2_4000_table))) loop
  l_trailing_54 := rtrim(rpad(substr(l_username,27,54),54,l.fill));
  l_msg  := 'Working with l.fill="'||l.fill||'" for "'||l_trailing_54||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  l_kenan_username := substr(l_username,1,26)||dbms_utility.get_hash_value(l_trailing_54,1000,8192);
  l_msg  := 'Generated hash l_kenan_username="'||l_kenan_username||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

  -- see if it's unique...
  begin
    select   force_username
      into l_username
      from c30arbor.c30_user_logons
     where kenan_username = l_kenan_username
    ;
    l_msg  := 'HASH DUPLICATE! l_username="'||l_username||'" EXISTS; l_kenan_username="'||l_kenan_username||'" (VERY UNUSUAL!)';
    l_body := l_body||l_msg||chr(10)||chr(10);
    c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    -- if this happens, someone needs to know; v1.1 new email group for this...
    l_email_address := nvl(l_email_address,'Cycle30KSADatabaseNotifications@cycle30.com');
    c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    l_kenan_username := null;

  exception
    when no_data_found then -- this is a good thing
      l_msg  := 'No_data_found for l_kenan_username="'||l_kenan_username||'" (we have a unique, new hashed value)';
      l_body := l_body||l_msg||chr(10)||chr(10);
      c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
      c30arbor.c30_ctx.set_parameter('force_username',l_username);
      c30arbor.c30_ctx.set_parameter('kenan_username',l_kenan_username);

      insert into c30arbor.c30_user_logons(  force_username,  kenan_username)
                                    values(      l_username,l_kenan_username)
      ;
      l_msg  := 'Inserted 1 c30_user_logons row (Force username > 30)';
      l_body := l_body||l_msg||chr(10)||chr(10);
      c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
      commit;
      l_msg  := 'Issued commit; (and will exit loop)';
      l_body := l_body||l_msg||chr(10)||chr(10);
      c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
      -- mission accomplished!
      exit;
  end;
  l_msg  := 'End loop - l_kenan_username="'||l_kenan_username||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
end loop;

-- did we succeed or fail?...
if l_kenan_username is not null then
  l_msg  := 'SUCCESS! l_kenan_username="'||l_kenan_username||'"';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog('c30setKenanUsername '||l_msg,'debug_verbose',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
  if l_email_address is not null then
    c30_send_email(l_email_address,'From c30setKenanUsername',l_body);
  end if;
else
  l_msg  := 'C30KSAKUN002 Force Username cannot be uniquely hashed';
  l_body := l_body||l_msg||chr(10)||chr(10);
  c30writeTransactionLog(l_msg,'error');
end if;

exception
  when others then
    c30writeTransactionLog('c30setKenanUsername when others then l_body=['||l_body||']','info',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    -- for any errors, someone should know; v1.1 new email group for this...
    l_email_address := nvl(l_email_address,'Cycle30KSADatabaseNotifications@cycle30.com');
    l_msg  := 'ERROR (at end)='||sqlerrm;
    l_body := l_body||l_msg||chr(10)||chr(10);
    c30_send_email(l_email_address,'From c30setKenanUsername',l_body);

    -- if the error is internally-generated by the Kenan SQL API, just "raise" it...
    if instr(sqlerrm,'C30KSA') > 0 then
      raise;
    else
      c30writeTransactionLog('C30KSAKUN999 ERROR: '||sqlerrm,'error');
    end if;
end c30setKenanUsername;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30setKenanUsername');
--spool off
exit rollback