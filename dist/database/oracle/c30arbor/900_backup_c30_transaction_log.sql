set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/900_backup_c30_transaction_log_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: 900_backup_c30_transaction_log.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : K3 AWN
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Backup the c30_transaction_log
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 09/27/2012 ##       Initial development
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
declare
   l_sql_cmd   varchar2(4000);
   l_sqlerrm   varchar2(4000);
begin

-- drop the old backup table...
l_sql_cmd := 'drop table c30temp.tmp_transaction_log_backup';
c30_put_line(null,chr(10)||l_sql_cmd||';');
c30_no_error(l_sql_cmd,l_sqlerrm);
-- we don't care about the response

-- create the new backup table as a clone...
l_sql_cmd := 'create table c30temp.tmp_transaction_log_backup tablespace c30temp nologging as select * from c30_transaction_log order by chg_date';
c30_put_line(null,chr(10)||l_sql_cmd||';');
c30_no_error(l_sql_cmd,l_sqlerrm);
c30_put_line(null,chr(10)||nvl(l_sqlerrm,'Table created.')||chr(10)||chr(10)||'Number of rows: '||sql%rowcount);

c30_put_line(null,chr(10)||'Successfully completed script for 900_backup_c30_transaction_log');

end;
/
--spool off
exit rollback
