set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/025_create_procedure_c30_dump_table_to_csv_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 025_create_procedure_c30_dump_table_to_csv.sql
prompt CA Ticket# : R296908
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt
prompt Purpose of Script: Create c30_dump_table_to_csv procedure
prompt (Author plagarized from http://asktom.oracle.com/pls/apex/f?p=100:11:0::::P11_QUESTION_ID:88212348059)
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/04/2012 ##  1.0 Initial Creation for AWN
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace procedure c30arbor.c30_dump_table_to_csv(
   v_table_name            IN       varchar2    := null -- optional; defaults to 'sys.dual'
  ,v_directory             IN       varchar2    := null -- optional; defaults to 'DEPLOYCONFIGS'
  ,v_csv_file              IN       varchar2    := null -- optional; defaults to 'c30_dump_table_to_csv_'||userenv('sessionid')||'.csv'
  ,v_del                   IN       varchar2    := null -- optional; defaults to '","'
  ,v_select_sql            IN       varchar2    := null -- optional; complete override of c_select_sql below
  ,v_nls_date_format       IN       varchar2    := null -- optional; defaults to 'mm/dd/yyyy hh24:mi:ss'
  ,v_put_line              IN       pls_integer := 2    -- optional; 0=output to utl_file only; 1=output to utl_file and dbms_output; 2=output to dbms_output only
  ,v_no_number_delims      IN       varchar2    := null --v1.1 optional; any non-null says to remove delimiters from number fields (e.g. "char_col",123,456,"X")
  ,v_order_by              IN       varchar2    := null --v1.3 optional; standard Oracle "order by" clause, with or without the actual "order by" prefix
)
is
-- Release Notes
-- Config (n/a)
-- Date: 10/09/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: R288190
--
-- Script file Name: 025_create_procedure_c30_dump_table_to_csv.sql
-- Version 1.0 Initial Creation for R288190
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 10/09/2012 ##  v1.1 Defect 225 - (Bring up-to-speed with recent changes in K1)
-- chip.lafurney@cycle30.com    ## 10/11/2012 ##  v1.2 Defect 234 - Change default for v_put_line; handle if null is explicitly passed
-- chip.lafurney@cycle30.com    ## 01/14/2013 ##  v1.3 R296908 - Add v_order_by parameter to allowing sorting
-- chip.lafurney@cycle30.com    ## 07/19/2013 ##  v1.4 Don't put trailing double quote when last column is a number and v_no_number_delims is set
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.5 R325955 - Starting getting write_error on new_line; added max_linesize 32767 to fopen function call
-- DETAILED EXPLANATION:
-- Create c30_dump_table_to_csv procedure
   c_put_line              CONSTANT pls_integer    := nvl(v_put_line,2); -- keep this in sync with the v_put_line default above
   c_directory             CONSTANT varchar2(200)  := nvl(trim(v_directory),'DEPLOYCONFIGS');
   c_csv_file              CONSTANT varchar2(200)  := nvl(trim(v_csv_file),'c30_dump_table_to_csv_'||userenv('sessionid')||'.csv');
   c_select_sql            CONSTANT varchar2(4000) := nvl(rtrim(trim(v_select_sql),';'),'select * from '||nvl(trim(v_table_name),'sys.dual'));
   c_nls_date_format       CONSTANT varchar2(40)   := nvl(trim(v_nls_date_format),'mm/dd/yyyy hh24:mi:ss');
   c_order_by              CONSTANT varchar2(400)  := case --v1.3 they can pass the "order by" clause or we'll supply it...
                                                         when trim(v_order_by) is null then
                                                            null
                                                         when itrim(lower(v_order_by),'all') like 'order by%' then
                                                            ' '||itrim(v_order_by,'all')
                                                         else
                                                            ' order by '||itrim(v_order_by,'all')
                                                      end;

   l_delim                          varchar2(20);
   l_del                            varchar2(20) := nvl(trim(v_del),'","');
   l_sep_spot                       pls_integer  := 0;
   l_before                         varchar2(20);
   l_after                          varchar2(20);

   l_sql_line                       varchar2(4000);
   l_sqlerrm                        varchar2(4000);
   l_file                           utl_file.file_type;
   l_cursor                         pls_integer := dbms_sql.open_cursor;
   l_column_val                     varchar2(4000);
   l_status                         pls_integer;
   l_col_cnt                        number := 0;
   l_desc_table                     dbms_sql.desc_tab;
   l_last_col_type                  pls_integer; --v1.4 added

   --* * * * * * * * * * * * * * *
   --* * INTERNAL PROCEDURES * * *
   --* * * * * * * * * * * * * * *
   procedure put(
      p_file  IN  utl_file.file_type
   ,p_line  IN  varchar2
    )
   as
   begin
   --LOTS OF OUTPUT --**!!c30_put_line(null,'(CDT2C) ==> "procedure put"; p_line={'||p_line||'}');
   if c_put_line < 2 then
      utl_file.put(p_file,p_line);
   end if;
   if c_put_line > 0 then
      dbms_output.put(p_line);
   end if;
   end;

   procedure new_line(
      p_file  IN  utl_file.file_type
    )
   as
   begin
   --**!!c30_put_line(null,'(CDT2C) ====> "procedure new_line"; c_put_line='||c_put_line||'; open?='||case when utl_file.is_open(p_file) then 'YES!' else 'NO!!' end);
   if c_put_line < 2 then
      utl_file.new_line(p_file);
   end if;
   if c_put_line > 0 then
      dbms_output.new_line;
   end if;
   end;

begin

--**!!c30_put_line(null,''||chr(10)||'C30_DUMP_TABLE_TO_CSV (CDT2C) Started at '||systimestamp||'; v_table_name='||nvl(v_table_name,'<null>')||'; v_directory='||nvl(v_directory,'<null>'));
--**!!c30_put_line(null,'(CDT2C) v_csv_file='||nvl(v_csv_file,'<null>')||'; v_del='||nvl(v_del,'<null>'));
--**!!c30_put_line(null,'(CDT2C) v_select_sql="'||nvl(v_select_sql,'<null>')||'"');
--**!!c30_put_line(null,'(CDT2C) v_nls_date_format='||nvl(v_nls_date_format,'<null>')||'; v_put_line='||v_put_line||'; v_no_number_delims='||nvl(v_no_number_delims,'<null>'));
--**!!c30_put_line(null,'(CDT2C) CONSTANTS IN EFFECT--c_put_line='||c_put_line||'; c_directory='||c_directory||'; c_csv_file='||c_csv_file||'; c_nls_date_format='||c_nls_date_format);
--**!!c30_put_line(null,'(CDT2C) c_select_sql="'||nvl(c_select_sql,'<null>')||'"');
--**!!c30_put_line(null,'(CDT2C) c_order_by="'||nvl(c_order_by,'<null>')||'"');

if c_put_line < 2 then
   -- create output CSV file...
   l_file := utl_file.fopen(c_directory,c_csv_file,'w',32767); --v1.5 added max_linesize
   --**!!c30_put_line(null,'(CDT2C) utl_file.fopen; c_directory="'||c_directory||'"; c_csv_file="'||c_csv_file||'"');
   -- otherwise leave l_file unopened (null)
elsif c_put_line = 2 then
   --**!!c30_put_line(null,'(CDT2C) (dbms_output only)');
   null;
else
   raise_application_error(-20099,'INPUT ERROR! c_put_line='||c_put_line||' (must be 0, 1, or 2)');
end if;

-- for a complex delimiter, figure out what's before and after the comma (or semi-colon or dash)...
if length(l_del) = 1 then
   null;
else
   -- first one "wins" (here, a comma)...
   l_sep_spot := instr(l_del,',');
   if l_sep_spot = 0 then
      l_sep_spot := instr(l_del,';');
      if l_sep_spot = 0 then
         l_sep_spot := instr(l_del,'-');
      end if;
   end if;

   if l_sep_spot > 0 then
      l_before := rtrim(substr(l_del,1,l_sep_spot-1));
      l_after  := ltrim(substr(l_del,l_sep_spot+1));
      --**!!c30_put_line(null,'(CDT2C) l_sep_spot='||l_sep_spot||'; l_before=['||l_before||']; l_after=['||l_after||']');
   end if;
end if;

l_sql_line := 'alter session set nls_date_format='''||c_nls_date_format||'''';
execute immediate l_sql_line;
--**!!c30_put_line(null,'(CDT2C) Executed '||l_sql_line);

-- the v_select_sql parameter allows the user to specify the exact "select" statement which may mean a different
-- set of column names and in a different order, so we'll create a TMP table clone and extract "select *" from it...
l_sql_line := 'drop table c30arbor.c30_tmp_csv_table cascade constraints purge';
--**!!c30_put_line(null,'(CDT2C) Executed '||l_sql_line);
c30_no_error(l_sql_line,l_sqlerrm);
--**!!c30_put_line(null,'(CDT2C) l_sqlerrm="'||nvl(l_sqlerrm,'<null>')||'"');

l_sql_line := 'create table c30arbor.c30_tmp_csv_table as '||c_select_sql;
execute immediate l_sql_line;
--**!!c30_put_line(null,'(CDT2C) Executed '||l_sql_line);

l_sql_line := 'select * from c30arbor.c30_tmp_csv_table'||c_order_by; --v1.3 added optional order by clause
dbms_sql.parse(l_cursor,l_sql_line,dbms_sql.native);
--**!!c30_put_line(null,'(CDT2C) Executed dbms_sql.parse for "'||l_sql_line||'"');
dbms_sql.describe_columns(l_cursor,l_col_cnt,l_desc_table);
--**!!c30_put_line(null,'(CDT2C) Executed dbms_sql.describe_columns; l_col_cnt='||l_col_cnt);

l_delim := l_before;
for i IN 1..l_col_cnt loop
   put(l_file,l_delim||l_desc_table(i).col_name);
   dbms_sql.define_column(l_cursor,i,l_column_val,4000);
   l_delim := l_del;
end loop;
put(l_file,l_after);
new_line(l_file);

l_status := dbms_sql.execute(l_cursor);
--**!!c30_put_line(null,'(CDT2C) Executed dbms_sql.execute(l_cursor); l_status='||l_status);
--**!!c30_put_line(null,'(CDT2C) v_no_number_delims='||nvl(v_no_number_delims,'<null>')||'; dbms_sql.fetch_rows(l_cursor)='||dbms_sql.fetch_rows(l_cursor));

while(dbms_sql.fetch_rows(l_cursor) > 0) loop
   l_delim := l_before;
   for i IN 1..l_col_cnt loop
      dbms_sql.column_value(l_cursor,i,l_column_val);
      --v1.1 deal with numbers (col_type = 2)
      if v_no_number_delims is not null then -- otherwise, we don't have to do this...
         if l_desc_table(i).col_type = 2 then
            if l_before is not null then
               if l_after is not null then
                  put(l_file,rtrim(l_delim,l_after)||l_column_val);
                  l_delim := ltrim(l_del,l_before);
               else -- l_after is null
                  put(l_file,l_delim||l_column_val);
                  l_delim := ltrim(l_del,l_before);
               end if;
            else -- l_before is null
               if l_after is not null then
                  put(l_file,rtrim(l_delim,l_after)||l_column_val);
                  l_delim := l_del;
               else -- BOTH are null (same as non-number below)
                  put(l_file,l_delim||l_column_val);
                  l_delim := l_del;
               end if;
            end if;
         else -- non-number column
            put(l_file,l_delim||l_column_val);
            l_delim := l_del;
         end if;
      else -- v_no_number_delims is null
         put(l_file,l_delim||l_column_val);
         l_delim := l_del;
      end if;
      l_last_col_type := l_desc_table(i).col_type; --v1.4 added
   end loop;
   --v1.4 Don't put trailing double quote when last column is a number and v_no_number_delims is set
   if v_no_number_delims is null or l_last_col_type != 2 then
      put(l_file,l_after);
   end if;
   --**!!c30_put_line(null,'(CDT2C) Issuing "new_line"; open?='||case when utl_file.is_open(l_file) then 'YES!' else 'NO!!' end);
   new_line(l_file);
end loop;

--**!!c30_put_line(null,'(CDT2C) Issuing dbms_sql.close_cursor(l_cursor)');
dbms_sql.close_cursor(l_cursor);

if utl_file.is_open(l_file) then
   utl_file.fclose(l_file);
   --**!!c30_put_line(null,'(CDT2C) Closed output CSV l_file');
end if;

l_sql_line := 'alter session set nls_date_format=''dd-MON-yy''';
execute immediate l_sql_line;
--**!!c30_put_line(null,'(CDT2C) Executed '||l_sql_line);

exception
   when others then
      --**!!c30_put_line(null,'(CDT2C ERROR) '||dbms_utility.format_call_stack);
      --**!!c30_put_line(null,'(CDT2C ERROR) '||dbms_utility.format_error_backtrace);
      --**!!c30_put_line(null,'(CDT2C ERROR) (before changing) l_sql_line='||l_sql_line);
      l_sql_line := 'alter session set nls_date_format=''dd-MON-yy''';
      execute immediate l_sql_line;
      --**!!c30_put_line(null,'(CDT2C ERROR) Executed '||l_sql_line);

      if utl_file.is_open(l_file) then
         utl_file.fclose(l_file);
         --**!!c30_put_line(null,'(CDT2C ERROR) Closed output CSV l_file');
      end if;

      raise;
end c30_dump_table_to_csv;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_dump_table_to_csv');
--**** notice the double semi-colons so one will show up on the output
--prompt exec c30_dump_table_to_csv('arbor.language_code_values');;
--prompt exec c30_dump_table_to_csv(v_table_name=>'arbor.language_code_values',v_directory=>'DEPLOYCONFIGS',v_csv_file=>'test.csv',v_del=>',',v_put_line=>2);;
--prompt exec c30_dump_table_to_csv(v_select_sql=>'select external_id, account_no, external_id_type, server_id, active_date, inactive_date from external_id_acct_map where rownum < 11;');;
--prompt OPTION: ,v_nls_date_format=>'yyyymmddhh24miss'
--prompt OPTION: ,v_order_by=>'1,2,3,4'
--v1.4 prompt exec c30_dump_table_to_csv(v_select_sql=>'select display_value,input_language,language_code from arbor.language_code_values',v_no_number_delims=>1,v_put_line=>2);;
--prompt exec dbms_output.put_line('x');;
--spool off
exit rollback