set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/150_create_procedure_c30getServiceInfo_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a)
prompt Date: 01-11-2012
prompt Created By: Mark Doyle
prompt Updated By: Chip H.F. LaFurney
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 150_create_procedure_c30getServiceInfo.sql
prompt Version: 1.0 Initial Creation for Kenan SQL API
prompt ....... v1.1 New Kenan SQL API changes (v_transaction_id); call account segment validation proc (c30setCtxParameters)
prompt DETAILED EXPLANATION:
prompt Create c30getServiceInfo procedure
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...object [OBJECT_NAME] does not exist"...
drop procedure c30arbor.c30getServiceInfo
;
create or replace procedure c30arbor.c30getServiceInfo(
  v_transaction_id                varchar2, -- v1.1 added
  v_acct_seg_id                   number,
  v_service_id                    varchar2,
  c30getServiceInfo_cv     IN OUT sys_refcursor
)
is
-- Release Notes
-- Config (n/a)
-- Date: 01-11-2012
-- Created By: Mark Doyle
-- Updated By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 150_create_procedure_c30getServiceInfo.sql
-- Version 1.0 Initial Creation for Kenan SQL API
-- v1.1 New Kenan SQL API changes (v_transaction_id); call account segment validation proc (c30setCtxParameters)
-- DETAILED EXPLANATION:
-- Create c30getServiceInfo procedure

  d_acct_seg_id arbor.cmf.acct_seg_id%type;
  d_account_no arbor.cmf.account_no%type;
  d_subscr_no arbor.service.subscr_no%type;
  d_subscr_no_resets arbor.service.subscr_no_resets%type;
  d_status_id arbor.service_status.status_id%type;
  d_status_descr arbor.status_values.display_value%type;
  d_active_dt arbor.service.service_active_dt%type;
  d_inactive_dt arbor.service.service_inactive_dt%type;
  d_host_service_id arbor.customer_id_equip_map.external_id%type;
  d_is_host number(1);
  d_partner_count number(10);

  i number(10);
  d_identifier_col c30arbor.c30identifiercol := c30arbor.c30identifiercol();
  d_plan_col c30arbor.c30catalogcol := c30arbor.c30catalogcol();
  d_comp_col c30arbor.c30catalogcol := c30arbor.c30catalogcol();

  cursor c_identifiers (v_subscr_no number, v_subscr_no_resets number, v_language_code number) is
  select external_id, eitv.external_id_type, display_value
  from arbor.customer_id_equip_map ciem, external_id_type_values eitv
  where ciem.subscr_no = v_subscr_no
  and ciem.subscr_no_resets = v_subscr_no_resets
  and ciem.external_id_type != 1
  and ciem.is_current = 1
  and ciem.external_id_type = eitv.external_id_type
  and eitv.language_code = v_language_code;

  cursor c_plan (v_subscr_no number, v_subscr_no_resets number, v_language_code number) is
  select catalog_id, external_inst_id, cpc.component_id, display_value, is_host, case is_host when 1 then count(ciim.instance_id) else 0 end as partner_count
  from c30arbor.c30_instances ci, arbor.cmf_package_component cpc, arbor.component_definition_values cdv, c30arbor.c30_instance_id_member ciim
  where ci.subscr_no = v_subscr_no
  and ci.subscr_no_resets = v_subscr_no_resets
  and ci.component_inst_id = cpc.component_inst_id
  and ci.component_inst_id_serv = cpc.component_inst_id_serv
  and cpc.component_id = cdv.component_id
  and cdv.language_code = v_language_code
  and ci.is_active = 1
  and ci.instance_type = 'P'
  and ci.instance_id = ciim.instance_id (+)
  and ciim.is_active (+) = 1
  group by catalog_id, external_inst_id, cpc.component_id, display_value, is_host;

  cursor c_comps (v_subscr_no number, v_subscr_no_resets number, v_language_code number) is
  select catalog_id, external_inst_id, cpc.component_id, display_value
  from c30arbor.c30_instances ci, arbor.cmf_package_component cpc, arbor.component_definition_values cdv
  where ci.subscr_no = v_subscr_no
  and ci.subscr_no_resets = v_subscr_no_resets
  and ci.component_inst_id = cpc.component_inst_id
  and ci.component_inst_id_serv = cpc.component_inst_id_serv
  and cpc.component_id = cdv.component_id
  and cdv.language_code = v_language_code
  and ci.is_active = 1
  and ci.instance_type = 'C';

begin
  c30writeTransactionLog('c30getServiceInfo called v_service_id='||v_service_id,null
                        ,p_transaction_id => v_transaction_id
                        ,p_acct_seg_id => v_acct_seg_id
                        );

  -- v1.1 validate account segment and load segmented system parameters into a context...
  c30setCtxParameters(v_acct_seg_id);
  d_acct_seg_id := c30_sys_param('acct_seg_id');
  -- ensure segment has the necessary permissions to execute the API
  c30validateSegPerms(d_acct_seg_id,'c30getServiceInfo');

 -- determine if service with serviceId exists in the system (service with serviceId should exist in the system)
  begin
    select ss.status_id, sv.display_value, s.parent_account_no, ciem.subscr_no, ciem.subscr_no_resets, s.service_active_dt, s.service_inactive_dt
    into d_status_id, d_status_descr, d_account_no, d_subscr_no, d_subscr_no_resets, d_active_dt, d_inactive_dt
    from arbor.customer_id_equip_map ciem, arbor.service s, arbor.service_status ss, arbor.status_values sv
    where ciem.external_id_type = 1
    and ciem.external_id = v_service_id||'.'||v_acct_seg_id
    and ciem.is_current = 1
    and ciem.subscr_no = s.subscr_no
    and ciem.subscr_no_resets = s.subscr_no_resets
    and s.subscr_no = ss.subscr_no
    and s.subscr_no_resets = ss.subscr_no_resets
    and ss.active_dt <= sysdate
    and (ss.inactive_dt > sysdate or ss.inactive_dt is null)
    and ss.status_id = sv.status_id
    and sv.language_code = c30_sys_param('language_code');
  exception
    when no_data_found then
      c30writeTransactionLog('C30KSAGSI003 serviceId: '||v_service_id||' does not exist in the system','error');
  end;

  begin
    select substr(ciem.external_id,1,instr(ciem.external_id,'.'||d_acct_seg_id,-1,1) -1)
    into d_host_service_id
    from c30_instance_id_member m, c30_external_inst_id_def d, customer_id_equip_map ciem
    where m.subscr_no = d_subscr_no
    and m.subscr_no_resets = d_subscr_no_resets
    and m.is_active = 1
    and m.instance_id = d.instance_id
    and d.instance_type = 'P'
    and d.is_active = 1
    and d.primary_subscr_no = ciem.subscr_no
    and d.primary_subscr_no_resets = ciem.subscr_no_resets
    and ciem.external_id_type = 1
    and ciem.is_current = 1;
  exception
    when no_data_found then
      d_host_service_id := null;
  end;

  i := 1;
  for r1 in c_identifiers (d_subscr_no, d_subscr_no_resets, c30_sys_param('language_code')) loop
    d_identifier_col.extend;
    d_identifier_col(i) := c30arbor.c30identifier(r1.external_id, r1.external_id_type, r1.display_value);
    i := i + 1;
  end loop;

  i := 1;
  for r1 in c_plan (d_subscr_no, d_subscr_no_resets, c30_sys_param('language_code')) loop
    d_plan_col.extend;
    d_plan_col(i) := c30arbor.c30catalog(r1.catalog_id, r1.external_inst_id, r1.component_id, r1.display_value);
    d_is_host := r1.is_host;
    d_partner_count := r1.partner_count;
    i := i + 1;
  end loop;

  i := 1;
  for r1 in c_comps (d_subscr_no, d_subscr_no_resets, c30_sys_param('language_code')) loop
    d_comp_col.extend;
    d_comp_col(i) := c30arbor.c30catalog(r1.catalog_id, r1.external_inst_id, r1.component_id, r1.display_value);
    i := i + 1;
  end loop;

  open c30getServiceInfo_cv for
  select v_service_id, d_subscr_no, d_subscr_no_resets, d_status_id, d_status_descr, d_active_dt, d_inactive_dt,
    d_host_service_id, d_is_host, d_partner_count, d_identifier_col, d_plan_col, d_comp_col
  from sys.dual;

exception
  when others then
    -- if the error is internally-generated by the Kenan SQL API, just "raise" it...
    if instr(sqlerrm,'C30KSA') > 0 then
      raise;
    else
      c30writeTransactionLog('C30KSAGSI999 ERROR: '||sqlerrm,'error');
    end if;
end c30getServiceInfo;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30getServiceInfo');
-- spool off
exit rollback