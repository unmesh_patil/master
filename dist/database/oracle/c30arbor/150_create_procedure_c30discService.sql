set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/150_create_procedure_c30discService_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Config (n/a)
prompt Date: 06-18-2012 
prompt Created By: Mark Doyle
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 150_create_procedure_c30discService.sql
prompt Version: 1.0 Initial Creation for Kenan SQL API
prompt .......  cloned from core kenan procedure emf_disconnect
prompt DETAILED EXPLANATION:
prompt Create c30discService procedure
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...object [OBJECT_NAME] does not exist"...
drop procedure c30arbor.c30discService
;
create or replace procedure c30arbor.c30discService(
  v_transaction_id                varchar2,
  v_acct_seg_id                   number,
  v_service_id                    varchar2,
  v_action_date                   date,
  v_action_who                    varchar2,
  v_disconnect_reason             varchar2          := null,
  v_ext_data                      c30namevaluepairs := null,
  c30discService_cv        IN OUT sys_refcursor,
  v_username_type                 varchar2          := null -- default of 'f' set below
)
is
-- Release Notes
-- Config (n/a)
-- Date: 06-18-2012
-- Created By: Mark Doyle
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 150_create_procedure_c30discService.sql
-- Version 1.0 Initial Creation for Kenan SQL API
-- cloned from core kenan procedure emf_disconnect
-- DETAILED EXPLANATION:
-- Create c30discService procedure

  -- SERVICE_VIEW
  -- field value determined based on api logic
  sv_chg_dt arbor.service_view.chg_dt%type;
  sv_chg_who arbor.service_view.chg_who%type;
  sv_intended_view_effective_dt arbor.service_view.intended_view_effective_dt%type;
  sv_service_inactive_dt arbor.service_view.service_inactive_dt%type;
  sv_subscr_no arbor.service_view.subscr_no%type;
  sv_subscr_no_resets arbor.service_view.subscr_no_resets%type;
  sv_view_created_dt arbor.service_view.view_created_dt%type;
  sv_view_effective_dt arbor.service_view.view_effective_dt%type;
  sv_view_id arbor.service_view.view_id%type;
  sv_view_status arbor.service_view.view_status%type;
  sv_prev_view_id arbor.service_view.prev_view_id%type;
  sv_emf_config_id arbor.service_view.emf_config_id%type;

  -- SERVICE_STATUS
  -- field value determined based on api logic
  -- ss_active_dt arbor.service_status.active_dt%type; -- is already defined as part of SERVICE_VIEW sv_service_active_dt
  -- ss_chg_dt arbor.service_status.chg_dt%type; -- is already defined as part of SERVICE_VIEW sv_chg_dt
  -- ss_chg_who arbor.service_status.chg_who%type; -- is already defined as part of SERVICE_VIEW sv_chg_who
  ss_inactive_dt arbor.service_status.inactive_dt%type;
  ss_status_id arbor.service_status.status_id%type;
  ss_status_reason_id arbor.service_status.status_reason_id%type;
  -- ss_subscr_no arbor.service_status.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- ss_subscr_no_resets arbor.service_status.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets

  -- field value hardcoded - core default (not modifiable)
  ss_status_type_id arbor.service_status.status_type_id%type := 1; -- Service Billing Status

  d_server_id number;
  d_acct_seg_id arbor.cmf.acct_seg_id%type;
  d_account_no cmf.account_no%type;
  d_dft_int_sql varchar2(2000);

  d_component_inst_id arbor.cmf_package_component.component_inst_id%type;
  d_component_inst_id_serv arbor.cmf_package_component.component_inst_id_serv%type;
  d_package_inst_id arbor.cmf_package.package_inst_id%type;
  d_package_inst_id_serv arbor.cmf_package.package_inst_id_serv%type;
  d_external_inst_id c30arbor.c30_external_inst_id_def.external_inst_id%type;
  d_instance_id c30arbor.c30_external_inst_id_def.instance_id%type;
  d_partners number(10);

  d_ext_data_col c30arbor.c30extdatacol := c30arbor.c30extdatacol();
  sed_param_value arbor.service_ext_data.param_value%type;
  pd_param_id arbor.param_def.param_id%type;
  pd_param_name arbor.param_def.param_name%type;
  pd_param_datatype arbor.param_def.param_datatype%type;

  type param_id_idx is table of number(3) index by pls_integer;
  aa_param_id_idx param_id_idx;

  i number(3) := 0;

  cursor c_service_ext_data(v_emf_config_id number) is
  select param_id
  from arbor.ext_param_type_assoc epta
  where epta.base_table = 'SERVICE_VIEW'
  and (entity_type = v_emf_config_id
    or entity_type = 0);

  d_disconnect_reason arbor.disconnect_reason_ref.disconnect_reason%type;
  d_waive_termination number(1) := 0; -- hard coding these for release 1.0 -- 0 = do not waive
  d_waive_unmet_obligation number(1) := 0; -- hard coding these for release 1.0 -- 0 = do not waive

  ciemv_view_id arbor.customer_id_equip_map_view.view_id%type;
  c_refcur sys_refcursor;

  -- v1.2 default of "f" tells us that "v_action_who" is from Force; "k" means from Kenan
  d_username_type char(1) := nvl(substr(trim(lower(v_username_type)),1,1),'f');

  cursor c_external_ids (v_subscr_no number, v_subscr_no_resets number, v_date date) is
  select *
  from arbor.customer_id_equip_map ciem
  where ciem.subscr_no = v_subscr_no
  and ciem.subscr_no_resets = v_subscr_no_resets
  and ciem.active_date <= v_date
  and (ciem.inactive_date > v_date or ciem.inactive_date is null);

  cursor c_comps (v_subscr_no number, v_subscr_no_resets number, v_date date) is
  select ceiid.*
  from c30arbor.c30_external_inst_id_def ceiid, arbor.cmf_package_component cpc
  where ceiid.instance_type = 'C'
  and ceiid.acct_seg_id = d_acct_seg_id
  and ceiid.primary_subscr_no = v_subscr_no
  and ceiid.primary_subscr_no_resets = v_subscr_no_resets
  and ceiid.is_active = 1
  and ceiid.component_inst_id = cpc.component_inst_id
  and ceiid.component_inst_id_serv = cpc.component_inst_id_serv
  and cpc.active_dt <= v_date
  and (cpc.inactive_dt > v_date or cpc.inactive_dt is null);

begin

  -- get "force_username" and "kenan_username" into the context;
  -- it also validates the value of d_username_type...
  c30arbor.c30setKenanUsername(v_action_who,d_username_type);

  c30writeTransactionLog('c30discService called  v_action_date='||to_char(v_action_date,'yyyy-mm-dd hh24:mi:ss')||'; v_service_id='||v_service_id||'; v_disconnect_reason='||v_disconnect_reason,null
                        ,p_transaction_id => v_transaction_id
                        ,p_acct_seg_id => v_acct_seg_id
                        );

  -- validate account segment and load segmented system parameters into a context...
  c30setCtxParameters(v_acct_seg_id);
  d_acct_seg_id := c30_sys_param('acct_seg_id');

  d_dft_int_sql := 'select case lower(casd.seg_parameter_default) when ''null'' then null else '||
      'to_number(casd.seg_parameter_default) end as seg_default '||
    'from c30arbor.c30_api_seg_defaults casd '||
    'where casd.acct_seg_id = :1 '||
    'and casd.api_name = :2 '||
    'and casd.api_parameter_name = :3';

  sv_service_inactive_dt := trunc(v_action_date);

  sv_view_status := 2; -- 2 = Current (only one Current view allowed per entity)
  sv_chg_who := c30_sys_param('kenan_username');
  sv_view_created_dt := sysdate;
  sv_chg_dt := sv_view_created_dt;
  sv_view_effective_dt := sv_service_inactive_dt;
  sv_intended_view_effective_dt := sv_service_inactive_dt;
  ss_inactive_dt := sv_view_created_dt;

  -- determine if service with serviceId exists in the system (service with serviceId should exist in the system)
  begin
    select s.view_id, ss.status_id, s.parent_account_no, ciem.subscr_no, ciem.subscr_no_resets, s.emf_config_id
    into sv_prev_view_id, ss_status_id, d_account_no, sv_subscr_no, sv_subscr_no_resets, sv_emf_config_id
    from arbor.customer_id_equip_map ciem, arbor.service s, arbor.service_status ss
    where ciem.external_id_type = 1
    and ciem.external_id = v_service_id||'.'||v_acct_seg_id
    and ciem.is_current = 1
    and ciem.subscr_no = s.subscr_no
    and ciem.subscr_no_resets = s.subscr_no_resets
    and s.subscr_no = ss.subscr_no
    and s.subscr_no_resets = ss.subscr_no_resets
    and ss.active_dt <= v_action_date
    and (ss.inactive_dt > v_action_date or ss.inactive_dt is null);
  exception
    when no_data_found then
      c30writeTransactionLog('C30KSADSV002 serviceId: '||v_service_id||' does not exist in the system','error');
  end;
 
   -- raise error if service status_id = 2 = Disconnected
  if ss_status_id = 2 then
    c30writeTransactionLog('C30KSADSV003 serviceId: '||v_service_id||' is in a disconnected status [service_status: '||ss_status_id||', subscr_no: '||sv_subscr_no||'/'||sv_subscr_no_resets||']','error');
  end if;


  -- fetch segment defaults for API parameters
  if v_disconnect_reason is null then
    begin
      execute immediate d_dft_int_sql into ss_status_reason_id using d_acct_seg_id, 'c30discservice', 'v_disconnect_reason';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSADSV004 API requires a value for v_disconnect_reason','error');
    end;
  else
    if c30isnumber(v_disconnect_reason) = 1 then
      begin
        select srr.status_reason_id
        into ss_status_reason_id
        from arbor.status_reason_values srv, c30arbor.c30_acct_seg_obj_map casom, c30arbor.c30_acct_seg_def casd, arbor.status_reason_ref srr
        where srv.status_reason_id = v_disconnect_reason
        and casd.acct_seg_id = d_acct_seg_id
        and (casom.acct_seg_id = casd.acct_seg_id or casom.acct_seg_id = casd.parent_acct_seg_id)
        and casom.object_name = 'status_reason_id'
        and to_number(casom.object_value) = srv.status_reason_id 
        and srv.status_reason_id  = srr.status_reason_id;
      exception
        when others then
          c30writeTransactionLog('C30KSADSV005 disconnect reason code: '||v_disconnect_reason||' is not a valid code','error');
      end;
    else
      begin
        select srr.status_reason_id
        into ss_status_reason_id
        from arbor.status_reason_values srv, c30arbor.c30_acct_seg_obj_map casom, c30arbor.c30_acct_seg_def casd, arbor.status_reason_ref srr
        where srv.short_display = v_disconnect_reason
        and casd.acct_seg_id = d_acct_seg_id
        and (casom.acct_seg_id = casd.acct_seg_id or casom.acct_seg_id = casd.parent_acct_seg_id)
        and casom.object_name = 'status_reason_id'
        and to_number(casom.object_value) = srv.status_reason_id 
        and srv.status_reason_id  = srr.status_reason_id;
      exception
        when others then
          c30writeTransactionLog('C30KSADSV005 disconnect reason code: '||v_disconnect_reason||' is not a valid code','error');
      end;
    end if;
  end if;

  -- build extended data collection based on extended data name value pairs passed to the API
  if v_ext_data.exists(1) then
    i := 0;
    for r1 in c_service_ext_data(sv_emf_config_id) loop
      d_ext_data_col.extend;
      i := i + 1;

      begin
        select sed.param_value
        into sed_param_value
        from arbor.service_ext_data sed
        where sed.view_id = sv_prev_view_id
        and sed.param_id = r1.param_id;
      exception
        when no_data_found then
          sed_param_value := null;
      end;

      d_ext_data_col(i) := c30arbor.c30extdata(r1.param_id, null, null, sed_param_value);
      aa_param_id_idx(r1.param_id) := i;
    end loop;

    for i in 1 .. v_ext_data.count loop
      begin
        select pd.param_id, pd.param_name, pd.param_datatype
        into pd_param_id, pd_param_name, pd_param_datatype
        from arbor.param_def pd, arbor.ext_param_type_assoc epta
        where pd.param_name = v_ext_data(i).c30name
        and pd.param_id = epta.param_id
        and epta.base_table = 'SERVICE_VIEW'
        and (entity_type = sv_emf_config_id or entity_type = 0);
        c30writeTransactionLog('c30discService: '||v_ext_data(i).c30name||' - '||v_ext_data(i).c30value,'debug');
      exception
        when no_data_found then
          c30writeTransactionLog('C30KSADSV006 name: '||v_ext_data(i).c30name||' is not configured as Service extended data','error');
      end;

      d_ext_data_col(aa_param_id_idx(pd_param_id)) := c30arbor.c30extdata(pd_param_id, pd_param_name, pd_param_datatype, v_ext_data(i).c30value);

    end loop;
  end if;

  -- if service exists determine whether it has an existing plan
  begin
    select ceiid.external_inst_id, ceiid.instance_id, ceiid.component_inst_id, ceiid.component_inst_id_serv, count(ciim.instance_id) as partners
    into d_external_inst_id, d_instance_id, d_component_inst_id, d_component_inst_id_serv, d_partners
    from c30arbor.c30_external_inst_id_def ceiid, c30arbor.c30_instance_id_member ciim
    where ceiid.instance_type = 'P'
    and ceiid.acct_seg_id = d_acct_seg_id
    and ceiid.primary_subscr_no = sv_subscr_no
    and ceiid.primary_subscr_no_resets = sv_subscr_no_resets
    and ceiid.is_active = 1
    and ceiid.instance_id = ciim.instance_id (+)
    and ciim.is_active (+) = 1
    group by ceiid.external_inst_id, ceiid.instance_id, ceiid.component_inst_id, ceiid.component_inst_id_serv;
  exception
    when no_data_found then
      d_instance_id := null;
    when others then
      c30writeTransactionLog('C30KSADSV007 error determining whether serviceId: '||v_service_id||' [subscr_no: '||sv_subscr_no||'/'||sv_subscr_no_resets||'] has existing plan','error');
  end;

  if d_partners != 0 then
    c30writeTransactionLog('C30KSADSV008 service serviceId: '||v_service_id||' is currently a HOST of an existing plan: '||d_external_inst_id||' with active PARTNERS','error');
  end if;

  -- if HOST without PARTERS then disconnect current plan
  if d_instance_id is not null and d_partners = 0 then
    d_disconnect_reason := 1; -- 1 = force.com Initiated CHANGE THIS PLEASE....

    begin
      c30arbor.c30discComp(
        v_transaction_id         => v_transaction_id,
        v_comp_inst_id           => d_component_inst_id,
        v_comp_inst_id_serv      => d_component_inst_id_serv,
        v_action_date            => sv_service_inactive_dt,
        v_action_who             => c30_sys_param('kenan_username'), -- v1.2 replaced v_action_who
        v_disconnect_reason      => d_disconnect_reason,
        v_waive_termination      => d_waive_termination,
        v_waive_unmet_obligation => d_waive_unmet_obligation,
        v_username_type          => 'k' -- v1.2 tell calling routine it's a "k"enan username
        );

      update c30arbor.c30_external_inst_id_def
      set is_active = 0, chg_date = sv_view_created_dt, chg_eff_date = sv_service_inactive_dt
      where instance_type = 'P'
      and acct_seg_id = d_acct_seg_id
      and instance_id = d_instance_id;

      insert into c30arbor.c30_transaction_item (transaction_id, transaction_type, item_type, external_inst_id, instance_id, view_id, item_inst_id, item_inst_id2,
        action_type, effective_date)
      values (v_transaction_id, 82, 10, d_external_inst_id, d_instance_id, null, d_component_inst_id, d_component_inst_id_serv,
        30, sv_service_inactive_dt);

    exception
      when others then
        c30writeTransactionLog('C30KSADSVS009 error removing plan: '||d_external_inst_id||' from serviceId: '||v_service_id||' [subscr_no: '||sv_subscr_no||'/'||sv_subscr_no_resets||']','error');
    end;
  end if;

  -- if service exists determine whether it is currently part of another plan - if so remove from existing plan (disconnect component)
  begin
    select ciim.instance_id, ciim.component_inst_id, ciim.component_inst_id_serv
    into d_instance_id, d_component_inst_id, d_component_inst_id_serv
    from c30arbor.c30_instance_id_member ciim
    where ciim.subscr_no = sv_subscr_no
    and ciim.subscr_no_resets = sv_subscr_no_resets
    and ciim.is_active = 1;
  exception
    when no_data_found then
      d_instance_id := null;
    when others then
      c30writeTransactionLog('C30KSADSV010 error determining whether serviceId: '||v_service_id||' [subscr_no: '||sv_subscr_no||'/'||sv_subscr_no_resets||'] is part of an existing plan','error');
  end;

  if d_instance_id is not null then
    d_disconnect_reason := 1; -- 1 = force.com Initiated CHANGE THIS PLEASE....

    begin
      c30arbor.c30discComp(
        v_transaction_id         => v_transaction_id,
        v_comp_inst_id           => d_component_inst_id,
        v_comp_inst_id_serv      => d_component_inst_id_serv,
        v_action_date            => sv_service_inactive_dt,
        v_action_who             => c30_sys_param('kenan_username'), -- v1.2 replaced v_action_who
        v_disconnect_reason      => d_disconnect_reason,
        v_waive_termination      => d_waive_termination,
        v_waive_unmet_obligation => d_waive_unmet_obligation,
        v_username_type          => 'k' -- v1.2 tell calling routine it's a "k"enan username
        );

      update c30arbor.c30_instance_id_member
      set is_active = 0, chg_date = sv_view_created_dt, chg_eff_date = sv_service_inactive_dt
      where component_inst_id = d_component_inst_id
      and component_inst_id_serv = d_component_inst_id_serv
      and instance_id = d_instance_id;

      insert into c30arbor.c30_transaction_item (transaction_id, transaction_type, item_type, external_inst_id, instance_id, view_id, item_inst_id, item_inst_id2,
        action_type, effective_date)
      values (v_transaction_id, 82, 15, null, d_instance_id, null, d_component_inst_id, d_component_inst_id_serv,
        30, sv_service_inactive_dt);

    exception
      when others then
        c30writeTransactionLog('C30KSADSV011 error removing serviceId: '||v_service_id||' [subscr_no: '||sv_subscr_no||'/'||sv_subscr_no_resets||'] from existing plan: '||d_external_inst_id,'error');
    end;
  end if;

  -- disconnect any components
  for r1 in c_comps (sv_subscr_no, sv_subscr_no_resets, sv_service_inactive_dt) loop
    begin
      c30arbor.c30discComponent(
        v_transaction_id    => v_transaction_id,
        v_acct_seg_id       => d_acct_seg_id,
        v_catalog_id        => r1.catalog_id,
        v_instance_id       => r1.external_inst_id,
        v_action_date       => sv_service_inactive_dt,
        v_action_who        => c30_sys_param('kenan_username'), -- v1.2 replaced v_action_who
        c30discComponent_cv => c_refcur,
        v_username_type     => 'k' -- v1.2 tell calling routine it's a "k"enan username
        );
    exception
      when others then
        c30writeTransactionLog('C30KSADSV012 error removing component from serviceId: '||v_service_id||' [subscr_no: '||sv_subscr_no||'/'||sv_subscr_no_resets||']','error');
        raise;
    end;
  end loop;

  -- disconnect any active identifiers (external_id)
  for r1 in c_external_ids (sv_subscr_no, sv_subscr_no_resets, sv_service_inactive_dt) loop

    begin
      arbor.sql_get_seq_num(
        v_table_name   => 'CUSTOMER_ID_EQUIP_MAP_VIEW',
        v_seqnum       => ciemv_view_id,
        v_server_id    => d_server_id,
        v_seq_num_type => 1);
    exception
      when others then
        c30writeTransactionLog('c30discService unable to generate ID for CUSTOMER_ID_EQUIP_MAP_VIEW','info');
        raise;
    end;
  
    insert into arbor.customer_id_equip_map_view (view_id, view_status, view_created_dt, view_effective_dt, intended_view_effective_dt,
      prev_view_id, external_id, external_id_type, subscr_no, subscr_no_resets,
      active_date, is_current, is_from_inventory, inactive_date)
    values (ciemv_view_id, sv_view_status, sv_view_created_dt, sv_view_effective_dt, sv_intended_view_effective_dt,
      r1.view_id, r1.external_id, r1.external_id_type, r1.subscr_no, r1.subscr_no_resets,
      r1.active_date, r1.is_current, r1.is_from_inventory, sv_service_inactive_dt);
  
    insert into arbor.external_id_equip_map_view (view_id, view_status, view_created_dt, view_effective_dt, intended_view_effective_dt,
      prev_view_id, external_id, external_id_type, subscr_no, subscr_no_resets,
      active_date, account_no, server_id, is_from_inventory, inactive_date)
    values (ciemv_view_id, sv_view_status, sv_view_created_dt, sv_view_effective_dt, sv_intended_view_effective_dt,
      r1.view_id, r1.external_id, r1.external_id_type, r1.subscr_no, r1.subscr_no_resets,
      r1.active_date, d_account_no, d_server_id, r1.is_from_inventory, sv_service_inactive_dt);

    if r1.external_id_type != 1 then
      insert into c30arbor.c30_transaction_item (transaction_id, transaction_type, item_type, external_inst_id, instance_id, view_id, item_inst_id, item_inst_id2,
        action_type, effective_date)
      values (v_transaction_id, 82, 70, r1.external_id, null, ciemv_view_id, r1.subscr_no, r1.subscr_no_resets,
        30, sv_service_inactive_dt);
    end if;

  end loop;

  begin
    arbor.sql_get_seq_num(
      v_table_name   => 'SERVICE_VIEW',
      v_seqnum       => sv_view_id,
      v_server_id    => d_server_id,
      v_seq_num_type => 1);
  exception
    when others then
      c30writeTransactionLog('c30discService unable to generate ID for SERVICE_VIEW','info');
      raise;
  end;

  -- insert into SERVICE_VIEW
  insert into arbor.service_view(view_id, view_status, view_created_dt, view_effective_dt, intended_view_effective_dt, prev_view_id,
    subscr_no, subscr_no_resets, b_rev_rcv_cost_ctr, b_service_fname, b_service_minit, b_service_lname,
    b_service_company, b_service_phone, b_service_phone2, chg_dt, chg_who, codeword,
    currency_code, display_external_id_type, elig_key1, elig_key2, elig_key3, emf_config_id,
    exrate_class, ixc_provider_id, lec_provider_id, no_bill, nonpub_nonlist, pic_date_active,
    parent_account_no, pop_units, privacy_level, rate_class, restricted_pic, rev_rcv_cost_ctr,
    service_name_pre, service_fname, service_minit, service_lname, service_name_generation, service_company,
    service_phone, service_phone2, sales_channel_id, sim_serial_number, switch_id, timezone,
    is_prepaid, service_active_dt, service_inactive_dt)
  select sv_view_id, sv_view_status, sv_view_created_dt, sv_view_effective_dt, sv_intended_view_effective_dt, sv_prev_view_id,
    subscr_no, subscr_no_resets, b_rev_rcv_cost_ctr, b_service_fname, b_service_minit, b_service_lname,
    b_service_company, b_service_phone, b_service_phone2, sv_chg_dt, sv_chg_who, codeword,
    currency_code, display_external_id_type, elig_key1, elig_key2, elig_key3, emf_config_id,
    exrate_class, ixc_provider_id, lec_provider_id, no_bill, nonpub_nonlist, pic_date_active,
    parent_account_no, pop_units, privacy_level, rate_class, restricted_pic, rev_rcv_cost_ctr,
    service_name_pre, service_fname, service_minit, service_lname, service_name_generation, service_company,
    service_phone, service_phone2, sales_channel_id, sim_serial_number, switch_id, timezone,
    is_prepaid, service_active_dt, sv_service_inactive_dt
  from arbor.service_view
  where view_id = sv_prev_view_id;

  if d_ext_data_col.exists(1) then
    for i in 1 .. d_ext_data_col.count loop
      if (d_ext_data_col(i).param_value is not null) then
        insert into arbor.service_ext_data(view_id, param_id, param_value, param_datatype)
        values(sv_view_id, d_ext_data_col(i).param_id, d_ext_data_col(i).param_value, d_ext_data_col(i).param_datatype);
      end if;
    end loop;
  end if;

  ss_status_id := 2; -- 2 = Disconnected

  -- insert into SERVICE_STATUS
  insert into arbor.service_status(subscr_no, subscr_no_resets, active_dt, status_type_id, status_id, status_reason_id,
    chg_who, chg_dt, inactive_dt)
  values(sv_subscr_no, sv_subscr_no_resets, sv_chg_dt, ss_status_type_id, ss_status_id, ss_status_reason_id,
    sv_chg_who, sv_chg_dt, null);

  -- update SERVICE_ADDRESS_ASSOC
  update arbor.service_address_assoc
  set inactive_dt = sv_service_inactive_dt
  where subscr_no = sv_subscr_no
  and subscr_no_resets = sv_subscr_no_resets
  and account_no = d_account_no;

  insert into c30arbor.c30_transaction_item (transaction_id, transaction_type, item_type, external_inst_id, instance_id, view_id, item_inst_id, item_inst_id2,
    action_type, effective_date)
  values (v_transaction_id, 82, 80, null, null, sv_view_id, sv_subscr_no, sv_subscr_no_resets,
    30, sv_service_inactive_dt);

  update c30arbor.c30_transaction_item set transaction_type = 82
  where transaction_id = v_transaction_id;

  open c30discService_cv for
  select v_service_id, sv_view_id, sv_subscr_no, sv_subscr_no_resets from sys.dual;

exception
  when others then
    -- if the error is internally-generated by the Kenan SQL API, just "raise" it...
    if instr(sqlerrm,'C30KSA') > 0 then
      raise;
    else
      c30writeTransactionLog('C30KSADSV999 ERROR: '||sqlerrm,'error');
    end if;
end c30discService;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30discService');
--spool off
exit rollback