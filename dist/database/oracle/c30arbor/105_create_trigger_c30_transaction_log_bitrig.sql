set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/105_create_trigger_c30_transaction_log_bitrig_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 105_create_trigger_c30_transaction_log_bitrig.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_transaction_log_bitrig trigger
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 11/17/2011 ##  Initial Creation for Kenan SQL API
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
create or replace trigger c30arbor.c30_transaction_log_bitrig
before insert
on c30arbor.c30_transaction_log
for each row
-- Release Notes
-- Config (n/a)
-- Date: 11-17-2011
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 105_create_trigger_c30_transaction_log_bitrig.sql
-- Version: 1.0 Initial Creation for Kenan SQL API
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 03/26/2012 ##  v1.1 Changed Oracle "user" to c30_sys_param('kenan_username')
-- chip.lafurney@cycle30.com    ## 04/30/2012 ##  v1.2 c30_sys_param('kenan_username') can be NULL for objects without v_action_who or v_chg_who calling parameters
-- chip.lafurney@cycle30.com    ## 05/07/2012 ##  v1.3 Defect 451 - Triggers should RAISE errors that calling object[s] will handle in exception section;
--                                                     prevent "ORA-06502: PL/SQL: numeric or value error: character string buffer too small" errors
-- chip.lafurney@cycle30.com    ## 09/06/2012 ##  v1.4 Changed (but left to-be-removed NSL = "new spy logic");
--                                                     added 3 other context fields to be captured if they are non-null
-- chip.lafurney@cycle30.com    ## 09/13/2012 ##  v1.5 Removed NSL = "new spy logic"
-- chip.lafurney@cycle30.com    ## 09/27/2012 ##  v1.5 Add dbms_utility.format_error_backtrace to trigger
-- DETAILED EXPLANATION:
-- Create c30_transaction_log_bitrig trigger to automatically supply values in transaction log
declare
  l_timestamp   timestamp := systimestamp; -- capture this once for all time values below
  l_body        varchar2(4000);
begin
  :new.sid            := sys_context('userenv','sid');
  :new.chg_date_char  := to_char(l_timestamp,'yyyy/mm/dd hh24:mi:ss.ff');
  :new.chg_date       := l_timestamp;
  :new.chg_date_local := systimestamp; -- using l_timestamp removes local timestamp

  --v1.1 this should not be null; it happened in testing and turned out to be a logic error...
  if c30_sys_param('kenan_username') is not null then
    :new.chg_who := c30_sys_param('kenan_username');
  else
    --v1.2 c30_sys_param('kenan_username') can be NULL for objects without v_action_who or v_chg_who calling parameters
    :new.chg_who := user;
    c30arbor.c30_ctx.set_parameter('kenan_username',user);
  end if;

  :new.sessionid   := sys_context('userenv','sessionid');
  :new.terminal    := sys_context('userenv','terminal');
  :new.host        := sys_context('userenv','host');
  :new.client_info := sys_context('userenv','client_info');
  :new.os_user     := sys_context('userenv','os_user');
  :new.ip_address  := sys_context('userenv','ip_address');

  begin
    select program,module
      into :new.program,:new.module
      from v$session
     where sid = sys_context('userenv','sid')
    ;
  exception
     when others then
         l_body := l_body||'program and module ERROR! '||sqlerrm||chr(10);
        :new.program := '(n/a)';
        :new.module  := '(n/a)';
  end;

  --v1.4 added 3 other context fields to be captured if they are non-null
  if c30_sys_param('transaction_id') is not null then
    if :new.transaction_id is null then
      :new.transaction_id := c30_sys_param('transaction_id');
    else -- :new.transaction_id is not null
      if :new.transaction_id <> c30_sys_param('transaction_id') then
        l_body := l_body||':new.transaction_id "'||:new.transaction_id||'" <> context value "'||c30_sys_param('transaction_id')||'"'||chr(10);
        --c30arbor.c30_ctx.set_parameter('transaction_id',:new.transaction_id);
      end if;
    end if;
  end if;

  if c30_sys_param('acct_seg_id') is not null then
    if :new.acct_seg_id is null then
      :new.acct_seg_id := c30_sys_param('acct_seg_id');
    else -- :new.acct_seg_id is not null
      if :new.acct_seg_id <> c30_sys_param('acct_seg_id') then
        l_body := l_body||':new.acct_seg_id "'||:new.acct_seg_id||'" <> context value "'||c30_sys_param('acct_seg_id')||'"'||chr(10);
        --c30arbor.c30_ctx.set_parameter('acct_seg_id',:new.acct_seg_id);
      end if;
    end if;
  end if;

  if c30_sys_param('force_id') is not null then
    if :new.force_id is null then
      :new.force_id := c30_sys_param('force_id');
    else -- :new.force_id is not null
      if :new.force_id <> c30_sys_param('force_id') then
        l_body := l_body||':new.force_id "'||:new.force_id||'" <> context value "'||c30_sys_param('force_id')||'"'||chr(10);
        --c30arbor.c30_ctx.set_parameter('force_id',:new.force_id);
      end if;
    end if;
  end if;

  if l_body is not null then c30_send_email('Cycle30KSADatabaseNotifications@cycle30.com','From c30_transaction_log_bitrig',l_body); end if;

  :new.format_call_stack      := dbms_utility.format_call_stack;
  :new.format_error_backtrace := dbms_utility.format_error_backtrace; --v1.3 added

-- v1.1 Defect 451 - No exception section in triggers since it becomes "when others then raise:" which is the default
end c30_transaction_log_bitrig;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_transaction_log_bitrig');
--spool off
exit rollback