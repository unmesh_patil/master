set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/155_create_procedure_c30addService_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes 
prompt Config (n/a) 
prompt Date: 05-23-2011
prompt Created By: Mark Doyle
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 155_create_procedure_c30addService.sql
prompt Version: 1.0 Initial Creation for Kenan SQL API
prompt DETAILED EXPLANATION:
prompt Create c30addService procedure
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...object [OBJECT_NAME] does not exist"...
drop procedure c30arbor.c30addService
;
create or replace procedure c30arbor.c30addService(
  v_transaction_id                varchar2, -- v1.1 added
  v_acct_seg_id                   number,
  v_service_id                    varchar2,
  v_action_date                   date,
  v_action_who                    varchar2,
  v_acct_id                       varchar2          := null,
  v_emf_config_id                 varchar2          := null,
  v_rate_class                    number            := null,
  v_rev_rcv_cost_ctr              number            := null,
  v_currency_code                 number            := null,
  v_timezone                      number            := null,
  v_franchise_tax_code            number            := null,
  v_vertex_geocode                varchar2          := null,
  v_house_number                  varchar2          := null,
  v_house_number_suffix           varchar2          := null,
  v_prefix_directional            varchar2          := null,
  v_street_name                   varchar2          := null,
  v_street_suffix                 varchar2          := null,
  v_postfix_directional           varchar2          := null,
  v_unit_type                     varchar2          := null,
  v_unit_no                       varchar2          := null,
  v_city                          varchar2          := null,
  v_county                        varchar2          := null,
  v_state                         varchar2          := null,
  v_postal_code                   varchar2          := null,
  v_extended_postal_code          varchar2          := null,
  v_country_code                  number            := null,
  v_connect_reason                varchar2          := null,
  v_ext_data                      c30namevaluepairs := null,
  c30addService_cv            OUT sys_refcursor,
  v_username_type                 varchar2          := null -- default of 'f' set below
)
is
-- Release Notes
-- Config (n/a)
-- Date: 05-23-2011
-- Created By: Mark Doyle
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 155_create_procedure_c30addService.sql
-- Version 1.0 Initial Creation for Kenan SQL API
-- DETAILED EXPLANATION:
-- Create c30addService procedure

  -- SERVICE_VIEW
  -- field value determined based on api logic
  sv_chg_dt arbor.service_view.chg_dt%type;
  sv_chg_who arbor.service_view.chg_who%type;
  sv_intended_view_effective_dt arbor.service_view.intended_view_effective_dt%type;
  sv_parent_account_no arbor.service_view.parent_account_no%type;
  sv_service_active_dt arbor.service_view.service_active_dt%type;
  sv_service_inactive_dt arbor.service_view.service_inactive_dt%type;
  sv_subscr_no arbor.service_view.subscr_no%type;
  sv_subscr_no_resets arbor.service_view.subscr_no_resets%type;
  sv_view_created_dt arbor.service_view.view_created_dt%type;
  sv_view_effective_dt arbor.service_view.view_effective_dt%type;
  sv_view_id arbor.service_view.view_id%type;
  sv_view_status arbor.service_view.view_status%type;
  sv_prev_view_id arbor.service_view.prev_view_id%type;

  -- field value hardcoded - core default (not modifiable)
  sv_b_rev_rcv_cost_ctr arbor.service_view.b_rev_rcv_cost_ctr%type := null;
  sv_b_service_company arbor.service_view.b_service_company%type := null;
  sv_b_service_fname arbor.service_view.b_service_fname%type := null;
  sv_b_service_lname arbor.service_view.b_service_lname%type := null;
  sv_b_service_minit arbor.service_view.b_service_minit%type := null;
  sv_b_service_phone arbor.service_view.b_service_phone%type := null;
  sv_b_service_phone2 arbor.service_view.b_service_phone2%type := null;
  sv_codeword arbor.service_view.codeword%type := null;
  sv_elig_key1 arbor.service_view.elig_key1%type := null;
  sv_elig_key2 arbor.service_view.elig_key2%type := null;
  sv_elig_key3 arbor.service_view.elig_key3%type := null;
  sv_exrate_class arbor.service_view.exrate_class%type := 1;
  sv_is_prepaid arbor.service_view.is_prepaid%type := 0;
  sv_ixc_provider_id arbor.service_view.ixc_provider_id%type := null;
  sv_lec_provider_id arbor.service_view.lec_provider_id%type := null;
  sv_nonpub_nonlist arbor.service_view.nonpub_nonlist%type := null;
  sv_pic_date_active arbor.service_view.pic_date_active%type := null;
  sv_pop_units arbor.service_view.pop_units%type := null;
  sv_privacy_level arbor.service_view.privacy_level%type := 0;
  sv_restricted_pic arbor.service_view.restricted_pic%type := null;
  sv_sales_channel_id arbor.service_view.sales_channel_id%type := null;
  sv_service_company arbor.service_view.service_company%type := null;
  sv_service_fname arbor.service_view.service_fname%type := null;
  sv_service_lname arbor.service_view.service_lname%type := null;
  sv_service_minit arbor.service_view.service_minit%type := null;
  sv_service_name_generation arbor.service_view.service_name_generation%type := null;
  sv_service_name_pre arbor.service_view.service_name_pre%type := null;
  sv_service_phone arbor.service_view.service_phone%type := null;
  sv_service_phone2 arbor.service_view.service_phone2%type := null;
  sv_sim_serial_number arbor.service_view.sim_serial_number%type := null;
  sv_switch_id arbor.service_view.switch_id%type := null;
  sv_display_external_id_type arbor.service_view.display_external_id_type%type := 1;
  sv_no_bill arbor.service_view.no_bill%type := 0;

  -- field value determined by value passed to API or if value passed to API is null then value determined based on default configured for segment
  sv_emf_config_id arbor.service_view.emf_config_id%type;
  sv_rate_class arbor.service_view.rate_class%type;
  sv_rev_rcv_cost_ctr arbor.service_view.rev_rcv_cost_ctr%type;
  sv_currency_code arbor.service_view.currency_code%type;
  sv_timezone arbor.service_view.timezone%type;

  -- MASTER_ADDRESS
  -- field value determined based on api logic
  ma_address_id arbor.master_address.address_id%type;
  -- ma_change_dt arbor.master_address.change_dt%type; -- is already defined as part of SERVICE_VIEW sv_chg_dt
  -- ma_change_who arbor.master_address.change_who%type; -- is already defined as part of SERVICE_VIEW sv_chg_who
  ma_vertex_geocode arbor.master_address.vertex_geocode%type;
  ma_fx_geocode arbor.master_address.fx_geocode%type; -- handled by C30_MASTER_ADDRESS_RTRIG
  ma_is_active arbor.master_address.is_active%type; -- handled by C30_MASTER_ADDRESS_RTRIG
  ma_address_1 arbor.master_address.address_1%type; -- handled by C30_MASTER_ADDRESS_RTRIG
  ma_address_2 arbor.master_address.address_2%type; -- handled by C30_MASTER_ADDRESS_RTRIG
  ma_address_3 arbor.master_address.address_3%type; -- handled by C30_MASTER_ADDRESS_RTRIG
  ma_address_4 arbor.master_address.address_4%type; -- handled by C30_MASTER_ADDRESS_RTRIG

  -- field value hardcoded - core default (not modifiable)
  ma_address_type_id arbor.master_address.address_type_id%type := 1; -- 1 = Not Free-form
  ma_external_address_id arbor.master_address.external_address_id%type := null;
  ma_key_line_code arbor.master_address.key_line_code%type := null;
  ma_nearest_crossstreet arbor.master_address.nearest_crossstreet%type := null;
  ma_optional_endorsement_line arbor.master_address.optional_endorsement_line%type := null;
  ma_postnet_addr_block_barcode arbor.master_address.postnet_address_block_barcode%type := null;
  ma_postnet_barcode arbor.master_address.postnet_barcode%type := null;

  -- field value determined by value passed to API or if value passed to API is null then value determined based on default configured for segment
  ma_franchise_tax_code arbor.master_address.franchise_tax_code%type;
  ma_city arbor.master_address.city%type;
  ma_country_code arbor.master_address.country_code%type;
  ma_county arbor.master_address.county%type;
  ma_extended_postal_code arbor.master_address.extended_postal_code%type;
  ma_house_number arbor.master_address.house_number%type;
  ma_house_number_suffix arbor.master_address.house_number_suffix%type;
  ma_postal_code arbor.master_address.postal_code%type;
  ma_postfix_directional arbor.master_address.postfix_directional%type;
  ma_prefix_directional arbor.master_address.prefix_directional%type;
  ma_state arbor.master_address.state%type;
  ma_street_name arbor.master_address.street_name%type;
  ma_street_suffix arbor.master_address.street_suffix%type;
  ma_unit_no arbor.master_address.unit_no%type;
  ma_unit_type arbor.master_address.unit_type%type;

  -- SERVICE_KEY
  -- field value determined based on api logic
  -- sk_create_dt arbor.service_key.create_dt%type; -- is already defined as part of SERVICE_VIEW sv_view_created_dt
  -- sk_display_address_id arbor.service_key.display_address_id%type; -- is already defined as part of MASTER_ADDRESS ma_address_id
  -- sk_display_ciem_view_id arbor.service_key.display_ciem_view_id%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_view_id
  -- sk_service_franchise_tax_code arbor.service_key.service_franchise_tax_code%type; -- is already defined as part of MASTER_ADDRESS ma_franchise_tax_code
  -- sk_service_geocode arbor.service_key.service_geocode%type; -- is already defined as part of MASTER_ADDRESS ma_fx_geocode
  -- sk_subscr_no arbor.service_key.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- sk_subscr_no_resets arbor.service_key.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets

  -- field value hardcoded - core default (not modifiable)
  sk_b_end_address_id arbor.service_key.b_end_address_id%type := null;
  sk_b_serv_franchise_tax_code arbor.service_key.b_service_franchise_tax_code%type := null;
  sk_b_service_geocode arbor.service_key.b_service_geocode%type := null;
  sk_converted arbor.service_key.converted%type := 0;

  -- SERVICE_STATUS
  -- field value determined based on api logic
  -- ss_active_dt arbor.service_status.active_dt%type; -- is already defined as part of SERVICE_VIEW sv_service_active_dt
  -- ss_chg_dt arbor.service_status.chg_dt%type; -- is already defined as part of SERVICE_VIEW sv_chg_dt
  -- ss_chg_who arbor.service_status.chg_who%type; -- is already defined as part of SERVICE_VIEW sv_chg_who
  -- ss_inactive_dt arbor.service_status.inactive_dt%type; -- is already defined as part of SERVICE_VIEW sv_service_inactive_dt
  ss_status_id arbor.service_status.status_id%type;
  -- ss_subscr_no arbor.service_status.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- ss_subscr_no_resets arbor.service_status.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets
  ss_status_reason_id arbor.service_status.status_reason_id%type;

  -- field value hardcoded - core default (not modifiable)
  ss_status_type_id arbor.service_status.status_type_id%type := 1;

  -- CUSTOMER_ID_EQUIP_MAP_VIEW
  -- field value determined based on api logic
  -- ciemv_active_date arbor.customer_id_equip_map_view.active_date%type; -- is already defined as part of SERVICE_VIEW sv_service_active_dt
  -- ciemv_inactive_date arbor.customer_id_equip_map_view.inactive_date%type; -- is already defined as part of SERVICE_VIEW sv_service_inactive_dt
  -- ciemv_intended_view_effective_dt arbor.customer_id_equip_map_view.intended_view_effective_dt%type; -- is already defined as part of SERVICE_VIEW sv_intended_view_effective_dt
  -- ciemv_subscr_no arbor.customer_id_equip_map_view.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- ciemv_subscr_no_resets arbor.customer_id_equip_map_view.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets
  -- ciemv_view_created_dt arbor.customer_id_equip_map_view.view_created_dt%type; -- is already defined as part of SERVICE_VIEW sv_view_created_dt
  -- ciemv_view_effective_dt arbor.customer_id_equip_map_view.view_effective_dt%type; -- is already defined as part of SERVICE_VIEW sv_view_effective_dt
  ciemv_external_id arbor.customer_id_equip_map_view.external_id%type;
  ciemv_view_id arbor.customer_id_equip_map_view.view_id%type;

  -- field value hardcoded - core default (not modifiable)
  ciemv_is_current arbor.customer_id_equip_map_view.is_current%type := 1; -- uniqueness_type = 1 for external_id_type = 1
  ciemv_external_id_type arbor.customer_id_equip_map_view.external_id_type%type := 1;  -- system hardcoded value for unique external_id
  ciemv_is_from_inventory arbor.customer_id_equip_map_view.is_from_inventory%type := 0;
  ciemv_prev_view_id arbor.customer_id_equip_map_view.prev_view_id%type := null;
  ciemv_view_status arbor.customer_id_equip_map_view.view_status%type := 2; -- 2 = Current (only one Current view allowed per entity)

  -- CUSTOMER_ID_EQUIP_MAP_KEY
  -- field value determined based on api logic
  -- ciemk_active_date arbor.customer_id_equip_map_key.active_date%type; -- is already defined as part of SERVICE_VIEW sv_service_active_dt
  -- ciemk_create_dt arbor.customer_id_equip_map_key.create_dt%type; -- is already defined as part of SERVICE_VIEW sv_view_created_dt
  -- ciemk_external_id arbor.customer_id_equip_map_key.external_id%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_external_id
  -- ciemk_external_id_type arbor.customer_id_equip_map_key.external_id_type%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_external_id_type
  -- ciemk_subscr_no arbor.customer_id_equip_map_key.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- ciemk_subscr_no_resets arbor.customer_id_equip_map_key.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets

  -- EXTERNAL_ID_EQUIP_MAP_VIEW
  -- field value determined based on api logic
  -- eiemv_account_no arbor.external_id_equip_map_view.account_no%type; -- is already defined as part of SERVICE_VIEW sv_parent_account_no
  -- eiemv_active_date arbor.external_id_equip_map_view.active_date%type; -- is already defined as part of SERVICE_VIEW sv_service_active_dt
  -- eiemv_external_id arbor.external_id_equip_map_view.external_id%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_external_id
  -- eiemv_external_id_type arbor.external_id_equip_map_view.external_id_type%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_external_id_type
  -- eiemv_inactive_date arbor.external_id_equip_map_view.inactive_date%type; -- is already defined as part of SERVICE_VIEW sv_service_inactive_dt
  -- eiemv_intended_view_effective_dt arbor.external_id_equip_map_view.intended_view_effective_dt%type; -- is already defined as part of SERVICE_VIEW sv_intended_view_effective_dt
  -- eiemv_is_from_inventory arbor.external_id_equip_map_view.is_from_inventory%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_is_from_inventory
  -- eiemv_prev_view_id arbor.external_id_equip_map_view.prev_view_id%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_prev_view_id
  -- eiemv_server_id arbor.external_id_equip_map_view.server_id%type; -- is already defined as d_server_id
  -- eiemv_subscr_no arbor.external_id_equip_map_view.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- eiemv_subscr_no_resets arbor.external_id_equip_map_view.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets
  -- eiemv_view_created_dt arbor.external_id_equip_map_view.view_created_dt%type; -- is already defined as part of SERVICE_VIEW sv_view_created_dt
  -- eiemv_view_effective_dt arbor.external_id_equip_map_view.view_effective_dt%type; -- is already defined as part of SERVICE_VIEW sv_view_effective_dt
  -- eiemv_view_id arbor.external_id_equip_map_view.view_id%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_view_id
  -- eiemv_view_status arbor.external_id_equip_map_view.view_status%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_view_status

  -- EXTERNAL_ID_EQUIP_MAP_KEY
  -- field value determined based on api logic
  -- eiemk_active_date arbor.external_id_equip_map_key.active_date%type; -- is already defined as part of SERVICE_VIEW sv_service_active_dt
  -- eiemk_create_dt arbor.external_id_equip_map_key.create_dt%type; -- is already defined as part of SERVICE_VIEW sv_view_created_dt
  -- eiemk_external_id arbor.external_id_equip_map_key.external_id%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_external_id
  -- eiemk_external_id_type arbor.external_id_equip_map_key.external_id_type%type; -- is already defined as part of CUSTOMER_ID_EQUIP_MAP_VIEW ciemv_external_id_type
  -- eiemk_subscr_no arbor.external_id_equip_map_key.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- eiemk_subscr_no_resets arbor.external_id_equip_map_key.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets

  -- SERVICE_ADDRESS_ASSOC
  -- field value determined based on api logic
  -- saa_account_no arbor.service_address_assoc.account_no%type; -- is already defined as part of SERVICE_VIEW sv_parent_account_no
  -- saa_active_dt arbor.service_address_assoc.active_dt%type; -- is already defined as part of SERVICE_VIEW sv_service_active_dt
  -- saa_address_id arbor.service_address_assoc.address_id%type; -- is already defined as part of MASTER_ADDRESS ma_address_id
  -- saa_emf_config_id arbor.service_address_assoc.emf_config_id%type; -- is already defined as part of SERVICE_VIEW sv_emf_config_id
  -- saa_inactive_dt arbor.service_address_assoc.inactive_dt%type; -- is already defined as part of SERVICE_VIEW sv_service_inactive_dt
  saa_service_address_assoc_id arbor.service_address_assoc.service_address_assoc_id%type;
  -- saa_subscr_no arbor.service_address_assoc.subscr_no%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no
  -- saa_subscr_no_resets arbor.service_address_assoc.subscr_no_resets%type; -- is already defined as part of SERVICE_VIEW sv_subscr_no_resets

  -- field value hardcoded - core default (not modifiable)
  saa_address_category_id arbor.service_address_assoc.address_category_id%type := 1; -- 1 = Service Address
  saa_association_status arbor.service_address_assoc.association_status%type := 2; -- 2 = Current

  d_account_status arbor.cmf.account_status%type;
  d_acct_seg_id arbor.cmf.acct_seg_id%type;

  d_dft_int_sql varchar2(2000);
  d_dft_str_sql varchar2(2000);
  d_dft_dt_sql varchar2(2000);
  d_ext_data_col c30arbor.c30extdatacol := c30arbor.c30extdatacol();
  d_server_id arbor.local_server_id.server_id%type;
  -- default of "f" tells us that "v_action_who" is from Force; "k" means from Kenan
  d_username_type char(1) := nvl(substr(trim(lower(v_username_type)),1,1),'f');

  d_external_id arbor.customer_id_acct_map.external_id%type;
  d_external_id_type arbor.external_id_type_ref.external_id_type%type;
  d_view_id arbor.service_key.display_ciem_view_id%type;

  pd_param_id arbor.param_def.param_id%type;
  pd_param_name arbor.param_def.param_name%type;
  pd_param_datatype arbor.param_def.param_datatype%type;

  d_addAccountSql varchar2(4000);
  d_addacct_data_col c30arbor.c30extdatacol := c30arbor.c30extdatacol();
  d_api_parameter_name c30arbor.c30_api_para_ext_data_map.api_parameter_name%type;
  j number(10);
  k number(10);

  c_refcur sys_refcursor;

begin

  -- v1.2 immediately get "force_username" and "kenan_username" into the context;
  -- it also validates the value of d_username_type...
  c30arbor.c30setKenanUsername(v_action_who,d_username_type);

  c30writeTransactionLog('c30addService called v_action_date='||to_char(v_action_date,'yyyy-mm-dd hh24:mi:ss')||'; v_acct_id='||v_acct_id||'; v_emf_config_id='||v_emf_config_id
                                        ||'; v_rate_class='||v_rate_class||'; v_rev_rcv_cost_ctr='||v_rev_rcv_cost_ctr,null
                        ,p_transaction_id => v_transaction_id
                        ,p_acct_seg_id => v_acct_seg_id
                        );

  -- validate account segment and load segmented system parameters into a context...
  c30setCtxParameters(v_acct_seg_id);
  d_acct_seg_id := c30_sys_param('acct_seg_id');
  -- ensure segment has the necessary permissions to execute the API
  c30validateSegPerms(d_acct_seg_id,'c30addService');

  d_dft_int_sql := 'select case lower(casd.seg_parameter_default) when ''null'' then null else '||
      'to_number(casd.seg_parameter_default) end as seg_default '||
    'from c30arbor.c30_api_seg_defaults casd '||
    'where casd.acct_seg_id = :1 '||
    'and casd.api_name = :2 '||
    'and casd.api_parameter_name = :3';

  d_dft_str_sql := 'select case lower(casd.seg_parameter_default) when ''null'' then null else '||
      'casd.seg_parameter_default end as seg_default '||
    'from c30arbor.c30_api_seg_defaults casd '||
    'where casd.acct_seg_id = :1 '||
    'and casd.api_name = :2 '||
    'and casd.api_parameter_name = :3';

  d_dft_dt_sql := 'select case lower(casd.seg_parameter_default) when ''null'' then null else '||
      'to_date(casd.seg_parameter_default,''YYYYMMDDHH24MISS'') end as seg_default '||
    'from c30arbor.c30_api_seg_defaults casd '||
    'where casd.acct_seg_id = :1 '||
    'and casd.api_name = :2 '||
    'and casd.api_parameter_name = :3';

  -- determine if serviceId concatenated with the segment already exists
  -- the serviceId should be unique in the segment so adding the segment as part of the external_id should make it unique
  -- we will later create a separate external_id (of segment specific type) for the serviceId itself
  begin
    select ciem.external_id, ss.status_id, ciem.subscr_no, ciem.subscr_no_resets
    into ciemv_external_id, ss_status_id, sv_subscr_no, sv_subscr_no_resets
    from arbor.customer_id_equip_map ciem, arbor.service_status ss 
    where ciem.external_id_type = 1
    and ciem.external_id = v_service_id||'.'||v_acct_seg_id
    and ciem.is_current = 1
    and ciem.subscr_no = ss.subscr_no
    and ciem.subscr_no_resets = ss.subscr_no_resets
    and ss.active_dt <= v_action_date
    and (ss.inactive_dt > v_action_date or ss.inactive_dt is null);
    -- serviceId should not exist in the system
    c30writeTransactionLog('C30KSAASV003 serviceId: '||ciemv_external_id||' already exists in the system; segment: '||v_acct_seg_id||
      ', service_status: '||ss_status_id||', subscr_no: '||sv_subscr_no||'/'||sv_subscr_no_resets,'error');
  exception
    when no_data_found then
      -- set external_id for serviceId type equal to v_service_id.v_acct_seg_id
      ciemv_external_id := v_service_id||'.'||v_acct_seg_id; 
  end;

  -- build extended data collection based on extended data name value pairs passed to the API
  if v_ext_data.exists(1) then
    j := 1;
    k := 1;
    for i in 1 .. v_ext_data.count loop
      begin
        select pd.param_id, pd.param_name, pd.param_datatype, capedm.api_parameter_name
        into pd_param_id, pd_param_name, pd_param_datatype, d_api_parameter_name
        from arbor.param_def pd
        join arbor.ext_param_type_assoc epta on pd.param_id = epta.param_id
        left outer join c30arbor.c30_api_para_ext_data_map capedm on capedm.api_name = 'c30addaccount' and pd.param_id = capedm.param_id and capedm.acct_seg_id = d_acct_seg_id
        where pd.param_name = v_ext_data(i).c30name
        and epta.base_table = 'SERVICE_VIEW';

        c30writeTransactionLog('c30addService: '||v_ext_data(i).c30name||' - '||v_ext_data(i).c30value,'debug');
      exception
        when no_data_found then
          c30writeTransactionLog('C30KSAASV031 name: '||v_ext_data(i).c30name||' is not configured as service extended data','error');
      end;

      if d_api_parameter_name is null then
        d_ext_data_col.extend;
        d_ext_data_col(j) := c30arbor.c30extdata(pd_param_id, pd_param_name, pd_param_datatype, v_ext_data(i).c30value);
        j := j + 1;
      else
        d_addacct_data_col.extend;
        d_addacct_data_col(k) := c30arbor.c30extdata(pd_param_id, d_api_parameter_name, pd_param_datatype, v_ext_data(i).c30value);
        k := k + 1;
      end if;

    end loop;
  end if;

  -- determine if account with accountId exists in the system (account with accountId should exist)
  if v_acct_id is not null then
    begin
      select c.account_no, c.account_status, c.acct_seg_id
      into sv_parent_account_no, d_account_status, d_acct_seg_id
      from arbor.customer_id_acct_map ciam, arbor.cmf c
      where ciam.external_id_type = 1
      and ciam.external_id = v_acct_id||'.'||v_acct_seg_id
      and ciam.is_current = 1 -- since external_id_type = 1 has uniqueness_type = 1 we can check is current = 1
      and ciam.account_no = c.account_no;
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV004 accountId: '||v_acct_id||' does not exist in the system','error');
    end;
  
    if v_acct_seg_id != d_acct_seg_id then
      c30writeTransactionLog('C30KSAASV005 accountId: '||v_acct_id||' exists in segment: '||d_acct_seg_id||' but does not match segment passed: '||v_acct_seg_id,'error');
    end if;
  
    if d_account_status not in (-1,0) then
      c30writeTransactionLog('C30KSAASV006 cannot add service to an account that is in DISC REQ or DISC DONE status - AccountId: '||v_acct_id,'error');
    end if;
  else -- if v_acct_id is null then create a dummy account foreach service create
    begin
      d_addAccountSql := 'begin c30addaccount('||
        'v_transaction_id => '''||v_transaction_id||''','||
        'v_acct_seg_id    => '||d_acct_seg_id||','||
        'v_action_date    => to_date('''||to_char(v_action_date,'YYYYMMDD')||''',''YYYYMMDD''),'||
        'v_action_who     => '''||v_action_who||''',';
 
      if d_addacct_data_col.exists(1) then
        for i in 1 .. d_addacct_data_col.count loop
          d_addAccountSql := d_addAccountSql||d_addacct_data_col(i).param_name||' => '; 
          if d_addacct_data_col(i).param_datatype = 2 then
            d_addAccountSql := d_addAccountSql||'''';
          end if;
          d_addAccountSql := d_addAccountSql||d_addacct_data_col(i).param_value;
          if d_addacct_data_col(i).param_datatype = 2 then
            d_addAccountSql := d_addAccountSql||'''';
          end if;
          d_addAccountSql := d_addAccountSql||',';
        end loop;
      end if;

      d_addAccountSql := d_addAccountSql||'c30addAccount_cv => :1); end;';
      dbms_output.put_line(d_addAccountSql);
      execute immediate d_addAccountSql using c_refcur;
  
      loop
        fetch c_refcur into d_external_id, sv_parent_account_no;
        exit when c_refcur%NOTFOUND;
      end loop;
      close c_refcur;
    exception
      when others then
        c30writeTransactionLog('C30KSAASV034 unable to create auto account for serviceId: '||v_service_id,'error');
    end;
  end if;
  
  -- fetch segment defaults for API parameters
  if v_emf_config_id is null then
    begin
      execute immediate d_dft_int_sql into sv_emf_config_id using d_acct_seg_id, 'c30addservice', 'v_emf_config_id';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV007 API requires a value for v_emf_config_id','error');
    end;
  else
    begin
      select ecir.emf_config_id
      into sv_emf_config_id
      from arbor.emf_config_id_values eciv, c30arbor.c30_acct_seg_obj_map casom, c30arbor.c30_acct_seg_def casd, arbor.emf_config_id_ref ecir
      where (eciv.emf_config_id = v_emf_config_id or eciv.short_display = v_emf_config_id) 
      and casd.acct_seg_id = d_acct_seg_id
      and (casom.acct_seg_id = casd.acct_seg_id or casom.acct_seg_id = casd.parent_acct_seg_id)
      and casom.object_name = 'emf_config_id'
      and to_number(casom.object_value) = eciv.emf_config_id
      and eciv.emf_config_id = ecir.emf_config_id;
    exception
      when others then
        c30writeTransactionLog('C30KSAASV035 unable to validate the service type: '||v_emf_config_id,'error');
    end;
  end if;

  if v_rate_class is null then
    if c30_sys_param('serv_rate_class') = 'use_default' then
      begin
        execute immediate d_dft_int_sql into sv_rate_class using d_acct_seg_id, 'c30addservice', 'v_rate_class';
      exception
        when no_data_found then
          c30writeTransactionLog('C30KSAASV008 API requires a value for v_rate_class','error');
      end;
    elsif c30_sys_param('serv_rate_class') = 'use_account' then
      select rate_class_default into sv_rate_class
      from arbor.cmf
      where account_no = sv_parent_account_no;
    else
      c30writeTransactionLog('C30KSAASV009 API does not support value: '||c30_sys_param('serv_rate_class')||' for system parameter: serv_rate_class','error');
    end if;
  else
    sv_rate_class := v_rate_class;
  end if;

  if v_rev_rcv_cost_ctr is null then
    if c30_sys_param('serv_rev_rcv_cost_ctr') = 'use_default' then
      begin
        execute immediate d_dft_int_sql into sv_rev_rcv_cost_ctr using d_acct_seg_id, 'c30addservice', 'v_rev_rcv_cost_ctr';
      exception
        when no_data_found then
          c30writeTransactionLog('C30KSAASV010 API requires a value for v_rev_rcv_cost_ctr','error');
      end;
    elsif c30_sys_param('serv_rev_rcv_cost_ctr') = 'use_account' then
      select rev_rcv_cost_ctr into sv_rev_rcv_cost_ctr
      from arbor.cmf
      where account_no = sv_parent_account_no;
    else
      c30writeTransactionLog('C30KSAASV011 API does not support value: '||c30_sys_param('serv_rev_rcv_cost_ctr')||' for system parameter: serv_rev_rcv_cost_ctr','error');
    end if;
  else
    sv_rev_rcv_cost_ctr := v_rev_rcv_cost_ctr;
  end if;

  if v_currency_code is null then
    if c30_sys_param('serv_currency_code') = 'use_default' then
      begin
        execute immediate d_dft_int_sql into sv_currency_code using d_acct_seg_id, 'c30addservice', 'v_currency_code';
      exception
        when no_data_found then
          c30writeTransactionLog('C30KSAASV012 API requires a value for v_currency_code','error');
      end;
    elsif c30_sys_param('serv_currency_code') = 'use_account' then
      begin
        select currency_code into sv_currency_code
        from arbor.cmf
        where account_no = sv_parent_account_no;
      exception
        when no_data_found then
          c30writeTransactionLog('C30KSAASV013 API unable to determine currency code of account: '||sv_parent_account_no,'error');
      end;
    else
      c30writeTransactionLog('C30KSAASV014 API does not support value: '||c30_sys_param('serv_currency_code')||' for system parameter: serv_currency_code','error');
    end if;
  else
    sv_currency_code := v_currency_code;
  end if;

  if v_timezone is null then
    begin
      execute immediate d_dft_int_sql into sv_timezone using d_acct_seg_id, 'c30addservice', 'v_timezone';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV015 API requires a value for v_timezone','error');
    end;
  else
    sv_timezone := v_timezone;
  end if;

  if v_franchise_tax_code is null then
    begin
      execute immediate d_dft_int_sql into ma_franchise_tax_code using d_acct_seg_id, 'c30addservice', 'v_franchise_tax_code';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV016 API requires a value for v_franchise_tax_code','error');
    end;
  else
    ma_franchise_tax_code := v_franchise_tax_code;
  end if;

  if v_vertex_geocode is null then
    begin
      execute immediate d_dft_str_sql into ma_vertex_geocode using d_acct_seg_id, 'c30addservice', 'v_vertex_geocode';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV033 API requires a value for v_vertex_geocode','error');
    end;
  else
    ma_vertex_geocode := v_vertex_geocode;
  end if;

  if v_city is null then
    begin
      execute immediate d_dft_str_sql into ma_city using d_acct_seg_id, 'c30addservice', 'v_city';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV017 API requires a value for v_city','error');
    end;
  else
    ma_city := v_city;
  end if;

  if v_country_code is null then
    begin
      execute immediate d_dft_int_sql into ma_country_code using d_acct_seg_id, 'c30addservice', 'v_country_code';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV018 API requires a value for v_country_code','error');
    end;
  else
    ma_country_code := v_country_code;
  end if;

  if v_county is null then
    begin
      execute immediate d_dft_str_sql into ma_county using d_acct_seg_id, 'c30addservice', 'v_county';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV019 API requires a value for v_county','error');
    end;
  else
    ma_county := v_county;
  end if;

  if v_extended_postal_code is null then
    begin
      execute immediate d_dft_str_sql into ma_extended_postal_code using d_acct_seg_id, 'c30addservice', 'v_extended_postal_code';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV020 API requires a value for v_extended_postal_code','error');
    end;
  else
    ma_extended_postal_code := v_extended_postal_code;
  end if;

  if v_house_number is null then
    begin
      execute immediate d_dft_str_sql into ma_house_number using d_acct_seg_id, 'c30addservice', 'v_house_number';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV021 API requires a value for v_house_number','error');
    end;
  else
    ma_house_number := v_house_number;
  end if;

  if v_house_number_suffix is null then
    begin
      execute immediate d_dft_str_sql into ma_house_number_suffix using d_acct_seg_id, 'c30addservice', 'v_house_number_suffix';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV022 API requires a value for v_house_number_suffix','error');
    end;
  else
    ma_house_number_suffix := v_house_number_suffix;
  end if;

  if v_postal_code  is null then
    begin
      execute immediate d_dft_str_sql into ma_postal_code using d_acct_seg_id, 'c30addservice', 'v_postal_code';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV023 API requires a value for v_postal_code','error');
    end;
  else
    ma_postal_code  := v_postal_code;
  end if;

  if v_postfix_directional  is null then
    begin
      execute immediate d_dft_str_sql into ma_postfix_directional  using d_acct_seg_id, 'c30addservice', 'v_postfix_directional';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV024 API requires a value for v_postfix_directional','error');
    end;
  else
    ma_postfix_directional  := v_postfix_directional;
  end if;

  if v_prefix_directional  is null then
    begin
      execute immediate d_dft_str_sql into ma_prefix_directional  using d_acct_seg_id, 'c30addservice', 'v_prefix_directional';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV025 API requires a value for v_prefix_directional','error');
    end;
  else
    ma_prefix_directional  := v_prefix_directional;
  end if;

  if v_state is null then
    begin
      execute immediate d_dft_str_sql into ma_state using d_acct_seg_id, 'c30addservice', 'v_state';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV026 API requires a value for v_state','error');
    end;
  else
    ma_state := v_state;
  end if;

  if v_street_name is null then
    begin
      execute immediate d_dft_str_sql into ma_street_name using d_acct_seg_id, 'c30addservice', 'v_street_name';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV027 API requires a value for v_street_name','error');
    end;
  else
    ma_street_name := v_street_name;
  end if;

  if v_street_suffix is null then
    begin
      execute immediate d_dft_str_sql into ma_street_suffix using d_acct_seg_id, 'c30addservice', 'v_street_suffix';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV028 API requires a value for v_street_suffix','error');
    end;
  else
    ma_street_suffix := v_street_suffix;
  end if;

  if v_unit_no is null then
    begin
      execute immediate d_dft_str_sql into ma_unit_no using d_acct_seg_id, 'c30addservice', 'v_unit_no';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV029 API requires a value for v_unit_no','error');
    end;
  else
    ma_unit_no := v_unit_no;
  end if;

  if v_unit_type is null then
    begin
      execute immediate d_dft_str_sql into ma_unit_type using d_acct_seg_id, 'c30addservice', 'v_unit_type';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV030 API requires a value for v_unit_type','error');
    end;
  else
    ma_unit_type := v_unit_type;
  end if;

  if v_connect_reason is null then
    begin
      execute immediate d_dft_int_sql into ss_status_reason_id using d_acct_seg_id, 'c30addservice', 'v_connect_reason';
    exception
      when no_data_found then
        c30writeTransactionLog('C30KSAASV036 API requires a value for v_connect_reason','error');
    end;
  else
    if c30isnumber(v_connect_reason) = 1 then
      begin
        select srr.status_reason_id
        into ss_status_reason_id
        from arbor.status_reason_values srv, c30arbor.c30_acct_seg_obj_map casom, c30arbor.c30_acct_seg_def casd, arbor.status_reason_ref srr
        where srv.status_reason_id = v_connect_reason
        and casd.acct_seg_id = d_acct_seg_id
        and (casom.acct_seg_id = casd.acct_seg_id or casom.acct_seg_id = casd.parent_acct_seg_id)
        and casom.object_name = 'status_reason_id'
        and to_number(casom.object_value) = srv.status_reason_id 
        and srv.status_reason_id  = srr.status_reason_id;
      exception
        when others then
          c30writeTransactionLog('C30KSAASV037 connect reason code: '||v_connect_reason||' is not a valid code','error');
      end;
    else
      begin
        select srr.status_reason_id
        into ss_status_reason_id
        from arbor.status_reason_values srv, c30arbor.c30_acct_seg_obj_map casom, c30arbor.c30_acct_seg_def casd, arbor.status_reason_ref srr
        where srv.short_display = v_connect_reason
        and casd.acct_seg_id = d_acct_seg_id
        and (casom.acct_seg_id = casd.acct_seg_id or casom.acct_seg_id = casd.parent_acct_seg_id)
        and casom.object_name = 'status_reason_id'
        and to_number(casom.object_value) = srv.status_reason_id 
        and srv.status_reason_id  = srr.status_reason_id;
      exception
        when others then
          c30writeTransactionLog('C30KSAASV037 connect reason code: '||v_connect_reason||' is not a valid code','error');
      end;
    end if;
  end if;

  -- determine external_id_type for segment specific service identifier
  -- also ensure that external_id_type has uniqueness_type = 1
  begin
    select external_id_type into d_external_id_type
    from arbor.emf_config_id_ref ecir, arbor.external_id_type_ref eitr
    where ecir.emf_config_id = sv_emf_config_id
    and ecir.default_external_id_type = eitr.external_id_type
    and eitr.uniqueness_type = 1;
  exception
    when no_data_found then
      c30writeTransactionLog('C30KSAASV032 unable to determine segment specific service identifier type for service type: '||sv_emf_config_id,'error');
  end;

  sv_chg_dt := sysdate;
  sv_chg_who := c30_sys_param('kenan_username');
  sv_intended_view_effective_dt := trunc(v_action_date);
  sv_view_effective_dt := trunc(v_action_date);
  sv_view_created_dt := sv_chg_dt;
  sv_service_active_dt := trunc(v_action_date);
  sv_service_inactive_dt := null;

  arbor.get_next_subscr_no(
    get_next_subscr_no_cv => c_refcur);

  loop
    fetch c_refcur into sv_subscr_no, sv_subscr_no_resets;
    exit when c_refcur%NOTFOUND;
  end loop;
  close c_refcur;

  begin
    arbor.sql_get_seq_num(
      v_table_name   => 'SERVICE_VIEW',
      v_seqnum       => sv_view_id,
      v_server_id    => d_server_id,
      v_seq_num_type => 1);
  exception
    when others then
      c30writeTransactionLog('c30addService unable to generate ID for SERVICE_VIEW','info');
      raise;
  end;

  sv_view_status := 2; -- 2 = Current (only one Current view allowed per entity)
  sv_prev_view_id := null;

  begin
    arbor.sql_get_seq_num(
      v_table_name   => 'ADR_ADDRESS',
      v_seqnum       => ma_address_id,
      v_server_id    => d_server_id,
      v_seq_num_type => 1);
  exception
    when others then
      c30writeTransactionLog('c30addService unable to generate ID for ADR_ADDRESS','info');
      raise;
  end;

  begin
    arbor.sql_get_seq_num(
      v_table_name   => 'CUSTOMER_ID_EQUIP_MAP_VIEW',
      v_seqnum       => ciemv_view_id,
      v_server_id    => d_server_id,
      v_seq_num_type => 1);
  exception
    when others then
      c30writeTransactionLog('c30addService unable to generate ID for CUSTOMER_ID_EQUIP_MAP_VIEW','info');
      raise;
  end;

  ss_status_id := 1; -- 1 = Active

  begin
    arbor.sql_get_seq_num(
      v_table_name   => 'SERVICE_ADDRESS_ASSOC',
      v_seqnum       => saa_service_address_assoc_id,
      v_server_id    => d_server_id,
      v_seq_num_type => 1);
  exception
    when others then
      c30writeTransactionLog('c30addService unable to generate ID for SERVICE_ADDRESS_ASSOC','info');
      raise;
  end;

  -- commented out columns handled by trigger C30_MASTER_ADDRESS_RTRIG
  insert into arbor.master_address (address_id, house_number, house_number_suffix, prefix_directional, street_name,
    street_suffix, postfix_directional, unit_type, unit_no, city,
    state, postal_code, extended_postal_code, country_code, franchise_tax_code,
    county, /*fx_geocode,*/ vertex_geocode, address_type_id, /*address_1,*/
    /*address_2, address_3, address_4,*/ nearest_crossstreet, external_address_id,
    optional_endorsement_line, key_line_code, postnet_barcode, postnet_address_block_barcode, /*is_active,*/
    change_who, change_dt)
   values (ma_address_id, ma_house_number, ma_house_number_suffix, ma_prefix_directional, ma_street_name,
    ma_street_suffix, ma_postfix_directional, ma_unit_type, ma_unit_no, ma_city,
    ma_state, ma_postal_code, ma_extended_postal_code, ma_country_code, ma_franchise_tax_code,
    ma_county, /*ma_fx_geocode,*/ ma_vertex_geocode, ma_address_type_id, /*ma_address_1,*/
    /*ma_address_2, ma_address_3, ma_address_4,*/ ma_nearest_crossstreet, ma_external_address_id,
    ma_optional_endorsement_line, ma_key_line_code, ma_postnet_barcode, ma_postnet_addr_block_barcode, /*ma_is_active,*/
    sv_chg_who, sv_chg_dt);

  -- read back address column values after trigger C30_MASTER_ADDRESS_RTRIG has (possibly) changed them 
  select house_number, house_number_suffix, prefix_directional, street_name,
    street_suffix, postfix_directional, unit_type, unit_no, city,
    state, postal_code, extended_postal_code, country_code, franchise_tax_code,
    county, fx_geocode, vertex_geocode, address_type_id, address_1,
    address_2, address_3, address_4, nearest_crossstreet, external_address_id,
    optional_endorsement_line, key_line_code, postnet_barcode, postnet_address_block_barcode, is_active
  into ma_house_number, ma_house_number_suffix, ma_prefix_directional, ma_street_name,
    ma_street_suffix, ma_postfix_directional, ma_unit_type, ma_unit_no, ma_city,
    ma_state, ma_postal_code, ma_extended_postal_code, ma_country_code, ma_franchise_tax_code,
    ma_county, ma_fx_geocode, ma_vertex_geocode, ma_address_type_id, ma_address_1,
    ma_address_2, ma_address_3, ma_address_4, ma_nearest_crossstreet, ma_external_address_id,
    ma_optional_endorsement_line, ma_key_line_code, ma_postnet_barcode, ma_postnet_addr_block_barcode, ma_is_active
  from arbor.master_address
  where address_id = ma_address_id;

  insert into arbor.service_key (subscr_no, subscr_no_resets, converted, create_dt, display_ciem_view_id,
    display_address_id, b_end_address_id, service_geocode, service_franchise_tax_code, b_service_geocode,
    b_service_franchise_tax_code)
  values (sv_subscr_no, sv_subscr_no_resets, sk_converted, sv_view_created_dt, null,
    ma_address_id, sk_b_end_address_id, ma_fx_geocode, ma_franchise_tax_code, sk_b_service_geocode,
    sk_b_serv_franchise_tax_code);

  insert into arbor.service_status (subscr_no, subscr_no_resets, active_dt, status_type_id, status_id,
    status_reason_id, chg_who, chg_dt, inactive_dt)
  values (sv_subscr_no, sv_subscr_no_resets, sv_service_active_dt, ss_status_type_id, ss_status_id,
    ss_status_reason_id, sv_chg_who, sv_chg_dt, sv_service_inactive_dt);

  insert into arbor.service_view (view_id, view_status, view_created_dt, view_effective_dt, intended_view_effective_dt,
    prev_view_id, subscr_no, subscr_no_resets, b_rev_rcv_cost_ctr, b_service_fname,
    b_service_minit, b_service_lname, b_service_company, b_service_phone, b_service_phone2,
    chg_dt, chg_who, codeword, currency_code, display_external_id_type,
    elig_key1, elig_key2, elig_key3, emf_config_id, exrate_class,
    ixc_provider_id, lec_provider_id, no_bill, nonpub_nonlist, pic_date_active,
    parent_account_no, pop_units, privacy_level, rate_class, restricted_pic,
    rev_rcv_cost_ctr, service_name_pre, service_fname, service_minit, service_lname,
    service_name_generation, service_company, service_phone, service_phone2, sales_channel_id,
    sim_serial_number, switch_id, timezone, is_prepaid, service_active_dt, service_inactive_dt)
  values (sv_view_id, sv_view_status, sv_view_created_dt, sv_view_effective_dt, sv_intended_view_effective_dt,
    sv_prev_view_id, sv_subscr_no, sv_subscr_no_resets, sv_b_rev_rcv_cost_ctr, sv_b_service_fname,
    sv_b_service_minit, sv_b_service_lname, sv_b_service_company, sv_b_service_phone, sv_b_service_phone2,
    sv_chg_dt, sv_chg_who, sv_codeword, sv_currency_code, sv_display_external_id_type,
    sv_elig_key1, sv_elig_key2, sv_elig_key3, sv_emf_config_id, sv_exrate_class,
    sv_ixc_provider_id, sv_lec_provider_id, sv_no_bill, sv_nonpub_nonlist, sv_pic_date_active,
    sv_parent_account_no, sv_pop_units, sv_privacy_level, sv_rate_class, sv_restricted_pic,
    sv_rev_rcv_cost_ctr, sv_service_name_pre, sv_service_fname, sv_service_minit, sv_service_lname,
    sv_service_name_generation, sv_service_company, sv_service_phone, sv_service_phone2, sv_sales_channel_id,
    sv_sim_serial_number, sv_switch_id, sv_timezone, sv_is_prepaid, sv_service_active_dt, sv_service_inactive_dt);

  insert into arbor.customer_id_equip_map_key (external_id, external_id_type, subscr_no, subscr_no_resets, active_date, create_dt)
  values (ciemv_external_id, ciemv_external_id_type, sv_subscr_no, sv_subscr_no_resets, sv_service_active_dt, sv_view_created_dt);

  insert into arbor.customer_id_equip_map_view (view_id, view_status, view_created_dt, view_effective_dt, intended_view_effective_dt,
    prev_view_id, external_id, external_id_type, subscr_no, subscr_no_resets,
    active_date, is_current, is_from_inventory, inactive_date)
  values (ciemv_view_id, ciemv_view_status, sv_view_created_dt, sv_view_effective_dt, sv_intended_view_effective_dt,
    ciemv_prev_view_id, ciemv_external_id, ciemv_external_id_type, sv_subscr_no, sv_subscr_no_resets,
    sv_service_active_dt, ciemv_is_current, ciemv_is_from_inventory, sv_service_inactive_dt);

  insert into arbor.external_id_equip_map_key (external_id, external_id_type, subscr_no, subscr_no_resets, active_date, create_dt)
  values (ciemv_external_id, ciemv_external_id_type, sv_subscr_no, sv_subscr_no_resets, sv_service_active_dt, sv_view_created_dt);

  insert into arbor.external_id_equip_map_view (view_id, view_status, view_created_dt, view_effective_dt, intended_view_effective_dt,
    prev_view_id, external_id, external_id_type, subscr_no, subscr_no_resets,
    active_date, account_no, server_id, is_from_inventory, inactive_date)
  values (ciemv_view_id, ciemv_view_status, sv_view_created_dt, sv_view_effective_dt, sv_intended_view_effective_dt,
    ciemv_prev_view_id, ciemv_external_id, ciemv_external_id_type, sv_subscr_no, sv_subscr_no_resets,
    sv_service_active_dt, sv_parent_account_no, d_server_id, ciemv_is_from_inventory, sv_service_inactive_dt);

  insert into arbor.service_address_assoc (service_address_assoc_id, subscr_no, subscr_no_resets, address_category_id, address_id,
    account_no, active_dt, inactive_dt, association_status, emf_config_id)
  values (saa_service_address_assoc_id, sv_subscr_no, sv_subscr_no_resets, saa_address_category_id, ma_address_id,
    sv_parent_account_no, sv_service_active_dt, sv_service_inactive_dt, saa_association_status, sv_emf_config_id);

  insert into c30arbor.c30_transaction_item (transaction_id, transaction_type, item_type, external_inst_id, instance_id, view_id, item_inst_id, item_inst_id2,
    action_type, effective_date)
  values (v_transaction_id, 80, 80, null, null, sv_view_id, sv_subscr_no, sv_subscr_no_resets,
    10, sv_service_active_dt);

  c30arbor.c30addIdentifier(
    v_transaction_id     => v_transaction_id,
    v_acct_seg_id        => v_acct_seg_id,
    v_service_id         => v_service_id,
    v_action_date        => sv_service_active_dt,
    v_action_who         => sv_chg_who,
    v_identifier_id      => v_service_id,
    v_external_id_type   => d_external_id_type,
    c30addIdentifier_cv  => c_refcur,
    v_username_type      => 'k');

  loop
    fetch c_refcur into ciemv_external_id, d_view_id, sv_subscr_no, sv_subscr_no_resets;
    exit when c_refcur%NOTFOUND;
  end loop;
  close c_refcur;

  update arbor.service_key set display_ciem_view_id = d_view_id
  where subscr_no = sv_subscr_no and subscr_no_resets = sv_subscr_no_resets;

  if d_ext_data_col.exists(1) then
    for i in 1 .. d_ext_data_col.count loop
      insert into arbor.service_ext_data (view_id, param_id, param_value, param_datatype)
      values (sv_view_id, d_ext_data_col(i).param_id, d_ext_data_col(i).param_value, d_ext_data_col(i).param_datatype);
    end loop;
  end if;

  update c30arbor.c30_transaction_item set transaction_type = 80
  where transaction_id = v_transaction_id;

  open c30addService_cv for
  select v_service_id, sv_view_id, sv_subscr_no, sv_subscr_no_resets from sys.dual;

exception
  when others then
    -- if the error is internally-generated by the Kenan SQL API, just "raise" it...
    if instr(sqlerrm,'C30KSA') > 0 then
      raise;
    else
      c30writeTransactionLog('C30KSAASV999 ERROR: '||sqlerrm,'error');
    end if;
end c30addService;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30addService');
-- spool off
exit rollback 