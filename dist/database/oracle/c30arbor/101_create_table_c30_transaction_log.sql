set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/101_create_table_c30_transaction_log_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: 101_create_table_c30_transaction_log.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_transaction_log table
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: "drop table" may return Oracle error "...table or view does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 11/17/2011 ##  Inital development
prompt chip.lafurney@cycle30.com    ## 05/03/2012 ##  v1.1 Defect 451 - Primary Key error appeared; adding log_msg and log_type
prompt chip.lafurney@cycle30.com    ## 05/15/2012 ##  v1.2 Defect 451 - Changed length of "module" to varchar2(64) to match v$session
prompt chip.lafurney@cycle30.com    ## 09/27/2012 ##  v1.3 Add dbms_utility.format_error_backtrace to table
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
whenever sqlerror continue
drop table c30arbor.c30_transaction_log cascade constraints purge
;
create table c30arbor.c30_transaction_log(
   log_msg                 varchar2(2000)                      null
  ,log_type                varchar2(20)    default 'info'  not null
  --**********************************************************************
  -- all the below values are automatically filled in by an insert trigger
  --**********************************************************************
  -- environment variables ("EV") are named after "XXXX" in sys_context('userenv','XXXX')
  ,sid                     number                          not null -- PK1; "EV"
  ,chg_date_char           varchar2(30)                    not null -- PK2; to_char(chg_date,'yyyy/mm/dd hh24:mi:ss.ff')
  ,chg_date                timestamp(6)                    not null
  ,chg_date_local          timestamp with local time zone  not null
  ,chg_who                 varchar2(30)                    not null
  ,sessionid               varchar2(32)                    not null -- "EV"
  ,terminal                varchar2(256)                       null -- "EV"
  ,host                    varchar2(256)                       null -- "EV"
  -- 64 bytes of user session information; e.g. "sys.dbms_application_info.set_client_info('column='||value);"
  ,client_info             varchar2(64)                        null -- "EV"
  ,os_user                 varchar2(64)                        null -- "EV"
  ,ip_address              varchar2(32)                        null -- "EV"
  -- select program,module from v$session where sid = sys_context('userenv','sid')
  ,program                 varchar2(48)                        null
  ,module                  varchar2(64)                        null
  -- optional identifying information
  ,transaction_id          varchar2(512)                       null
  ,acct_seg_id             number(10)                          null
  ,account_no              number(10)                          null
  ,subscr_no               number(10)                          null
  ,subscr_no_resets        number(6)                           null
  ,bill_ref_no             number(10)                          null
  ,bill_ref_resets         number(3)                           null
  ,package_inst_id         number(10)                          null
  ,package_inst_id_serv    number(3)                           null
  ,package_id              number(10)                          null
  ,component_inst_id       number(10)                          null
  ,component_inst_id_serv  number(3)                           null
  ,component_id            number(10)                          null
  ,tracking_id             number(10)                          null
  ,tracking_id_serv        number(3)                           null
  ,force_id                varchar2(1500)                      null
  -- set from dbms_utility.format_call_stack
  ,format_call_stack       varchar2(2000)                      null
  --v1.3 added; set from dbms_utility.format_error_backtrace
  ,format_error_backtrace  varchar2(4000)                      null
   )
;
comment on column c30_transaction_log.sid                     is 'sys_context(''userenv'',''sid'')'
;
comment on column c30_transaction_log.chg_date_char           is 'to_char(chg_date,''yyyy/mm/dd hh24:mi:ss.ff'')'
;
comment on column c30_transaction_log.chg_date                is 'systimestamp'
;
comment on column c30_transaction_log.chg_date_local          is 'systimestamp'
;
comment on column c30_transaction_log.chg_who                 is 'c30_sys_param(''chg_who'')'
;
comment on column c30_transaction_log.sessionid               is 'sys_context(''userenv'',''sessionid'')'
;
comment on column c30_transaction_log.terminal                is 'sys_context(''userenv'',''terminal'')'
;
comment on column c30_transaction_log.host                    is 'sys_context(''userenv'',''host'')'
;
comment on column c30_transaction_log.client_info             is 'sys_context(''userenv'',''client_info''); set with sys.dbms_application_info.set_client_info(''column=''||value); (max 64 bytes)'
;
comment on column c30_transaction_log.os_user                 is 'sys_context(''userenv'',''os_user'')'
;
comment on column c30_transaction_log.ip_address              is 'sys_context(''userenv'',''ip_address'')'
;
comment on column c30_transaction_log.program                 is 'select program from v$session where sid = sys_context(''userenv'',''sid'')'
;
comment on column c30_transaction_log.module                  is 'select module from v$session where sid = sys_context(''userenv'',''sid'')'
;
comment on column c30_transaction_log.transaction_id          is 'c30_sys_param(''transaction_id'') (if non-null)'
;
comment on column c30_transaction_log.acct_seg_id             is 'c30_sys_param(''acct_seg_id'') (if non-null)'
;
comment on column c30_transaction_log.force_id                is 'c30_sys_param(''force_id'') (if non-null)'
;
comment on column c30_transaction_log.format_call_stack       is 'dbms_utility.format_call_stack'
;
comment on column c30_transaction_log.format_error_backtrace  is 'dbms_utility.format_error_backtrace' --v1.3 added
;
create unique index c30arbor.c30_transaction_log_pk
   on c30arbor.c30_transaction_log(sid,chg_date_char,log_msg,log_type)
;
alter table c30arbor.c30_transaction_log add(
   constraint c30_transaction_log_pk
   primary key(sid,chg_date_char,log_msg,log_type)
   using index c30arbor.c30_transaction_log_pk)
;
alter table c30arbor.c30_transaction_log add(
  constraint c30_transaction_log_fk1
  foreign key(acct_seg_id)
  references c30arbor.c30_acct_seg_def(acct_seg_id))
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_transaction_log');
--spool off
exit rollback