set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/R325955_001_New_SF_Process_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: R325955_001_New_SF_Process.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Configuration and Other System Changes for R325955 Settlement Feed Process Improvement
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: Log output with "ORA-00942: table or view does not exist" or "ORA-04043: object XXX_YYY does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
declare
   l_sql_line                       varchar2(4000);
   l_sqlerrm                        varchar2(2000);
begin
dbms_output.put(chr(10));

-- delete (for reruns) then insert permissions for new driver package..
delete c30arbor.c30_api_seg_permissions
 where acct_seg_id IN(3,4)
   and api_name = 'c30_sf_driver'
;
c30_put_line(null,'Deleted '||sql%rowcount||' c30_api_seg_permissions row[s] ("c30_sf_driver")');

insert into c30arbor.c30_api_seg_permissions(acct_seg_id, api_name)
   values(3, 'c30_sf_driver')
;
c30_put_line(null,'Inserted 1 c30_api_seg_permissions rows ("c30_sf_driver")');

-- remove permissions for billed and unbilled "gen CSV" procedures...
delete c30arbor.c30_api_seg_permissions
 where acct_seg_id IN(3,4)
   and api_name like 'c30_s%m%billed_gen_csv'
;
c30_put_line(null,'Deleted '||sql%rowcount||' c30_api_seg_permissions row[s] (unbilled CSV procs)');

-- delete (for reruns) then insert permissions for new "gen CSV" procedure..
delete c30arbor.c30_api_seg_permissions
 where acct_seg_id IN(3,4)
   and api_name = 'c30_settlement_feed_gen_csv'
;
c30_put_line(null,'Deleted '||sql%rowcount||' c30_api_seg_permissions row[s] ("c30_settlement_feed_gen_csv")');

-- drop all the old sf objects and their public synonym; use "c30_no_error" in case of reruns...
l_sql_line := 'drop view c30arbor.c30_stm_feed_dates';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_stm_feed_dates';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop view c30arbor.c30_stm_feed_grouped';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_stm_feed_grouped';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop view c30arbor.c30_stm_feed_detail';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_stm_feed_detail';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop view c30arbor.c30_stm_feed_detail_billed';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_stm_feed_detail_billed';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop view c30arbor.c30_stm_feed_detail_unbilled';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_stm_feed_detail_unbilled';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop view c30arbor.c30_sum_type_billed';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_sum_type_billed';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop view c30arbor.c30_sum_type_unbilled';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_sum_type_unbilled';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop procedure c30arbor.c30_stm_feed_billed_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_stm_feed_billed_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop procedure c30arbor.c30_stm_feed_unbilled_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_stm_feed_unbilled_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop procedure c30arbor.c30_sum_type_billed_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_sum_type_billed_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

l_sql_line := 'drop procedure c30arbor.c30_sum_type_unbilled_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');
l_sql_line := 'drop public synonym c30_sum_type_unbilled_gen_csv';
c30_no_error(l_sql_line,l_sqlerrm);
c30_put_line(null,'Executed "'||l_sql_line||'"; response="'||nvl(l_sqlerrm,'(Normal)')||'"');

-- life's good -- issue final commit and send to dbms_output
commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script for R325955_001_New_SF_Process');

exception
   when others then
      rollback;
      c30_put_line(null,'rollback;');
      c30_put_line(null,sqlerrm);
      raise;
end;
/
--spool off
exit rollback