set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/127_create_view_c30_sum_type_unbilled_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 127_create_view_c30_sum_type_unbilled.sql
prompt CA Ticket# : AWN
prompt QC Defect# : Defect 279
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_sum_type_unbilled view for Settlement Feed processing
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 11/14/2012 ##  1.0 Initial Creation for Defect 279
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_sum_type_unbilled as
select
-- Release Notes
-- Config (n/a)
-- Date: 10/01/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: AWN
--
-- Script file Name: 127_create_view_c30_sum_type_unbilled.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- [YOUR EMAIL ADDRESS]         ## [[[DATE]]] ##  [v1.x REVISION LOG HERE]
-- DETAILED EXPLANATION:
-- Create c30_sum_type_unbilled view for Settlement Feed processing
       to_char(dts.unbilled_cutoff_date,'yyyy/mm/dd')               "Cutoff_Date"
     , sfd."Usage_Type"
     , count(*)                                                     "Number_Of_Calls"
     , trim(to_char(sum(sfd.rated_units),'999,999,999,999,990'))    "Total_Rated_Units"
     , sfd."Rated_Units_Type"
     , trim(to_char(sum(sfd.unrounded_amount),'99,999,990.000000')) "Total_Cost"
  from c30arbor.c30_stm_feed_dates dts
  join arbor.cdr_unbilled cub
    on cub.trans_dt < dts.unbilled_cutoff_date
  join c30arbor.c30_stm_feed_detail sfd
    on sfd.msg_id                 = cub.msg_id
   and sfd.msg_id2                = cub.msg_id2
   and sfd.msg_id_serv            = cub.msg_id_serv
   and sfd.min_split_row_num     <= cub.split_row_num
   and sfd.max_split_row_num     >= cub.split_row_num
   and sfd.cdr_data_partition_key = cub.cdr_data_partition_key
group by to_char(dts.unbilled_cutoff_date,'yyyy/mm/dd')
     , sfd."Usage_Type"
     , sfd."Rated_Units_Type"
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_sum_type_unbilled');
--select count(*), sum("Number_Of_Calls") "Number_Of_Calls" from c30arbor.c30_sum_type_unbilled;
--spool off
exit rollback