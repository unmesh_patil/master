set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/115_create_package_body_c30_stm_feed_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 115_create_package_body_c30_stm_feed.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create c30_stm_feed package body for R325955 Settlement Feed Process Improvement
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace package body c30arbor.c30_stm_feed
is
-- Release Notes
-- Config (n/a)
-- Date: 09/12/2013
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: N/A
--
-- Script file Name: 115_create_package_body_c30_stm_feed.sql
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.0 Initial Creation for R325955
-- DETAILED EXPLANATION:
-- Create c30_stm_feed package (spec) for R325955 Settlement Feed Process Improvement

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * PROCEDURE DRIVER  * * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
-- it is the job of the c30_sf_driver to:
--    1) deal with v_put_line parameter and open output log file (if needed)
--    2) validate the account segment and loading up the context
--    3) checking associated permissions
--    4) call the associated entry point
--    5) throw K3 standard error if raised
procedure c30_sf_driver(
   v_acct_seg_id           IN       number            -- required; account segment
  ,v_entry_point           IN       varchar2          -- required; name of the package entry point being called
  ,v_put_line              IN       pls_integer := 0  -- optional; 0=output to utl_file only; 1=output to utl_file and dbms_output; 2=output to dbms_output only
)
is
   c_entry_point_first     CONSTANT varchar2(30) := 'load_cdr_data';
   c_entry_point_last      CONSTANT varchar2(30) := 'generate_csv_files';
   c_log_file_name         CONSTANT varchar2(40) := 'c30_settlement_feed_process.log';

   l_upd_count                      pls_integer;
   l_entry_point                    varchar2(30);
begin
c30writeTransactionLog('c30_stm_feed.c30_sf_driver called! v_put_line='||nvl(to_char(v_put_line),'<null>')||'; v_entry_point="'||v_entry_point||'"',null,p_acct_seg_id=>v_acct_seg_id);

-- keep this in sync with the v_put_line default...
d_put_line := nvl(v_put_line,0);
-- if we are writing to utl_file, no need to save the output if dbms_output limit reached...
d_save_overflow := case when d_put_line IN(0,1) then false else true end;
-- if we are writing to utl_file ONLY, then "no dbms_output" is non-null...
d_no_dbms_output := case when d_put_line = 0 then 'x' else null end;

if d_put_line > 0 then
   dbms_output.put(chr(10));
end if;

l_entry_point := trim(lower(v_entry_point));

if d_put_line < 2 then
   -- create output log; it is overwritten on the first entry point, then "appended to" for the rest...
   if l_entry_point = c_entry_point_first then
      d_file := utl_file.fopen('DEPLOYCONFIGS',c_log_file_name,'w');
      c30_put_line(d_file,'C30_STM_FEED.DRIVER (CSF.DRV) Started at '||systimestamp||'; l_entry_point='||l_entry_point||'; c_log_file_name='||c_log_file_name||' (write)',true,d_save_overflow,d_no_dbms_output);
   else
      d_file := utl_file.fopen('DEPLOYCONFIGS',c_log_file_name,'a');
      c30_put_line(d_file,'C30_STM_FEED.DRIVER (CSF.DRV) Started at '||systimestamp||'; l_entry_point='||l_entry_point||'; c_log_file_name='||c_log_file_name||' (append)',true,d_save_overflow,d_no_dbms_output);
   end if;
   -- otherwise leave d_file unopened (null)
elsif d_put_line = 2 then
   if d_no_dbms_output is null then
      c30_put_line(d_file,'C30_STM_FEED.DRIVER (CSF.DRV) Started at '||systimestamp||' (dbms_output only); l_entry_point='||l_entry_point||' (no log)',true,d_save_overflow,d_no_dbms_output);
   else
      -- make sure these aren't mutually exclusive! logic error if d_put_line=2 and d_no_dbms_output is not null!
      c30writeTransactionLog('C30STMDRV001 LOGIC ERROR! d_put_line='||d_put_line||'; d_no_dbms_output='||d_no_dbms_output,'error');
   end if;
else
   c30writeTransactionLog('C30STMDRV002 INPUT ERROR! d_put_line='||d_put_line||' (must be 0, 1, or 2)','error');
end if;
c30_put_line(d_file,'(CSF.DRV) d_put_line='||d_put_line||'; d_save_overflow='||case when d_save_overflow then 'True' else 'False' end||'; d_no_dbms_output='||d_no_dbms_output,true,d_save_overflow,d_no_dbms_output);

-- validate account segment and load segmented system parameters into a context...
c30setCtxParameters(v_acct_seg_id);
d_acct_seg_id := c30_sys_param('acct_seg_id');
c30_put_line(d_file,'(CSF.DRV) Account Segment='||d_acct_seg_id,true,d_save_overflow,d_no_dbms_output);

-- ensure segment has the necessary permissions to execute the API
c30validateSegPerms(d_acct_seg_id,'c30_sf_driver');
c30_put_line(d_file,'(CSF.DRV) Executed c30validateSegPerms',true,d_save_overflow,d_no_dbms_output);

-- for possible reruns (entry points other than the first or last), set a value for "d_rowcount_hold" by resetting associated rows...
if  l_entry_point     not IN(c_entry_point_first,c_entry_point_last)
and nvl(d_rowcount_hold,0) = 0 then
   d_timestamp_start := systimestamp;
   l_upd_count       := 1; -- anything but 0
   d_rowcount_hold   := 0;
   loop
      d_sql_line := 'update c30arbor.c30_settlement_feed_billed set '||l_entry_point||' = 0 where '||l_entry_point||' = 1 and rownum <= '||c_batch_max;
      execute immediate(d_sql_line);
      l_upd_count := sql%rowcount;
      if l_upd_count > 0 then
         --**!!c30_put_line(null,'execute immediate "'||d_sql_line||'"; updated '||l_upd_count||' records',true,d_save_overflow,d_no_dbms_output);
         null; -- keeping the above is too much output
      else
         c30_put_line(d_file,'(CSF.DRV) execute immediate "'||d_sql_line||'"; exit (update loop)',true,d_save_overflow,d_no_dbms_output);
         exit;
      end if;
      d_rowcount_hold := d_rowcount_hold + l_upd_count;
      commit;
   end loop;

   d_timestamp_stop := systimestamp;
   c30_put_line(d_file,'(CSF.DRV) reset the table; d_rowcount_hold='||d_rowcount_hold||' (before '||l_entry_point||'); time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

   if d_rowcount_hold = 0 then
      c30writeTransactionLog('C30STMDRV003 LOGIC ERROR! No records to process! d_rowcount_hold='||d_rowcount_hold||' (before '||l_entry_point||')','error');
   end if;
end if;

d_rowcount := 0;
d_sql_line := 'begin c30_stm_feed.'||l_entry_point||'(:acct_seg_id,:rowcount); end;';
c30_put_line(d_file,'(CSF.DRV) execute immediate "'||d_sql_line||'" using IN v_acct_seg_id ('||v_acct_seg_id||'), IN OUT d_rowcount ('||d_rowcount||')',true,d_save_overflow,d_no_dbms_output);
execute immediate(d_sql_line) using IN v_acct_seg_id, IN OUT d_rowcount;
c30_put_line(d_file,'(CSF.DRV) =========> d_rowcount='||nvl(d_rowcount,-1)||'; d_rowcount_hold='||nvl(d_rowcount_hold,-1)||' (after '||l_entry_point||')');

-- record counts must stay the same...
if d_rowcount <> d_rowcount_hold then
   c30writeTransactionLog('C30STMSHL004 LOGIC ERROR! Record count mismatch! d_rowcount='||d_rowcount||' (after '||l_entry_point||')','error');
end if;

c30_put_line(d_file,'(CSF.DRV) End process '||systimestamp,true,d_save_overflow,d_no_dbms_output);

if utl_file.is_open(d_file) then
   utl_file.fclose(d_file);
end if;

exception
   when others then
      if utl_file.is_open(d_file) then
         c30_put_line(d_file,'(CSF.DRV) ERROR='||cleantext(sqlerrm,' ','t'),true,d_save_overflow,d_no_dbms_output);
         utl_file.fclose(d_file);
      else
         c30_put_line(null,'(CSF.DRV) ERROR='||cleantext(sqlerrm,' ','t'),true,d_save_overflow,d_no_dbms_output);
      end if;
      
      -- if the error is internally-generated, just "raise" it...
      if instr(sqlerrm,'C30STM') > 0 then
         raise;
      else
         c30writeTransactionLog('C30STMDRV999 ERROR='||sqlerrm,'error');
      end if;
end c30_sf_driver;

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * PROCEDURE LOAD_CDR_DATA * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
procedure load_cdr_data(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
)
is
   type t_key_record is record(
       billed_cutoff_date_char      c30_settlement_feed_billed.billed_cutoff_date_char%type
     , acct_seg_id                  c30_settlement_feed_billed.acct_seg_id%type
     , msg_id                       c30_settlement_feed_billed.msg_id%type
     , msg_id2                      c30_settlement_feed_billed.msg_id2%type
     , msg_id_serv                  c30_settlement_feed_billed.msg_id_serv%type
     , split_row_num                c30_settlement_feed_billed.split_row_num%type
     , cdr_data_partition_key       c30_settlement_feed_billed.cdr_data_partition_key%type
                              );
   type t_key_rec_table is table of t_key_record;
   l_key_values                     t_key_rec_table;

   cursor c_key_values is
   select to_char(dts.billed_cutoff_date,'yyyy/mm/dd') billed_cutoff_date_char, dts.acct_seg_id
        , cbl.msg_id, cbl.msg_id2, cbl.msg_id_serv, cbl.split_row_num, cbl.cdr_data_partition_key
     from c30arbor.c30_settlement_feed_dates dts
     join arbor.bill_invoice biv
       -- we want zero or -1; other values are -2, -3 and -4; so "greater than minus 2"
       on biv.jnl_status       > c_number_minus_2                    -- bill_invoice_xjnl_status index part 1
      and biv.to_date    between dts.min_to_date and dts.max_to_date -- bill_invoice_xjnl_status index part 2
      and biv.prep_status      = c_number_1
      and biv.prep_error_code is null
      and biv.backout_status   = c_number_0
     join arbor.cdr_billed cbl
       on cbl.bill_ref_no              = biv.bill_ref_no
      and cbl.bill_ref_resets          = biv.bill_ref_resets
      and cbl.account_no               = biv.account_no
      and cbl.billed_unrounded_amount <> c_number_0 -- same as cdr_data.unrounded_amount
    where dts.acct_seg_id = v_acct_seg_id
   ;
begin
c30_put_line(d_file,'LOAD_CDR_DATA (CSF.lcd) Started at '||systimestamp,true,d_save_overflow,d_no_dbms_output);

-- we have to truncate the table to make sure it's empty...
d_sql_line := 'truncate table c30arbor.c30_settlement_feed_billed';
c30_put_line(d_file,'..(CSF.lcd) executing "'||d_sql_line||'"',true,d_save_overflow,d_no_dbms_output);
execute immediate(d_sql_line);
c30_put_line(d_file,'..(CSF.lcd) c30_settlement_feed_billed table truncated',true,d_save_overflow,d_no_dbms_output);

d_timestamp_start := systimestamp;
open c_key_values;
loop
   fetch c_key_values bulk collect into l_key_values;
   exit when c_key_values%notfound;
end loop;
close c_key_values;
d_timestamp_stop := systimestamp;
c30_put_line(d_file,'..(CSF.lcd) LOADED TABLE - l_key_values.count='||l_key_values.count||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

-- this is to insert the initial billed CDR_DATA records
d_timestamp_start := systimestamp;
d_batch_count     := 0;
d_batch           := greatest(least(ceil(l_key_values.count/c_num_batches),c_batch_max),c_batch_min);
c30_put_line(d_file,'..(CSF.lcd) CALC BATCH SIZE - d_batch='||d_batch,true,d_save_overflow,d_no_dbms_output);
loop
   d_batch_count := d_batch_count + 1;
   d_batch_start := ((d_batch_count-1)*d_batch)+1;
   d_batch_stop  := least((d_batch_count*d_batch),l_key_values.count);
   c30_put_line(d_file,'..(CSF.lcd) CDR_DATA count='||d_batch_count||'; start->stop='||d_batch_start||'->'||d_batch_stop,true,d_save_overflow,d_no_dbms_output);

   --================> inner block to capture any possible "forall" exceptions (BELOW) <================
   begin
   forall i IN d_batch_start..d_batch_stop save exceptions
      insert into c30arbor.c30_settlement_feed_billed(
             billed_cutoff_date_char, acct_seg_id
           , cdr_data_partition_key, msg_id, msg_id2, msg_id_serv, split_row_num, type_id_usg
           , bill_class, provider_id, jurisdiction, account_no, subscr_no, subscr_no_resets
           , external_id, external_id_type, point_origin, country_code_origin_char, point_id_origin, point_target
           , country_code_target_char, point_id_target, dialed_digits, trans_dt, trans_dt_char, primary_units_char
           , billing_units_type, annotation, customer_tag, rated_units, unrounded_amount_calc, unrounded_amount_char
           , seqnum_rate_usage, component_id, usage_type, add_unit_rate, location, valdtn
           , load_cdr_data, chg_date, chg_who)
      with rate_usage_xpanded as(select seqnum, add_unit_rate/power(10,add_implied_decimal)/100 add_unit_rate from arbor.rate_usage)
         , lookup_switch_xpanded as(select gsmsiteid, sitecity from c30digitalroute.lookup_switch_location)
      select l_key_values(i).billed_cutoff_date_char, l_key_values(i).acct_seg_id
           , cdr.cdr_data_partition_key, cdr.msg_id, cdr.msg_id2, cdr.msg_id_serv, cdr.split_row_num, cdr.type_id_usg
           , cdr.bill_class, cdr.provider_id, cdr.jurisdiction, cdr.account_no, cdr.subscr_no, cdr.subscr_no_resets
           , cdr.external_id, cdr.external_id_type, trim(cdr.point_origin), to_char(decode(cdr.country_code_origin,0,null,cdr.country_code_origin),'fm999999'), cdr.point_id_origin, trim(cdr.point_target)
           , to_char(decode(cdr.country_code_target,0,null,cdr.country_code_target),'fm999999'), cdr.point_id_target
           -- which number was dialed?  (currently excludes, SMS or Data, usage types 981 or 983, respectively)
           , case when cdr.type_id_usg IN(952,953,954,963,964,965,966,967,992,993,994,1062) then trim(cdr.point_target) -- outgoing calls
                  when cdr.type_id_usg IN(951,961,962,991,1061)                             then trim(cdr.point_origin) -- incoming calls
             end, cdr.trans_dt, to_char(cdr.trans_dt,'yyyy/mm/dd hh24:mi:ss'), to_char(cdr.primary_units,'fm999,999,999,999,990')
           , cdr.billing_units_type, trim(cdr.annotation), trim(cdr.customer_tag), cdr.rated_units
           -- convert using all additional or implied decimal places; currency_code = 1 has 2 implied decimal places
           , cdr.unrounded_amount/power(10,cdr.add_implied_decimal)/100, to_char(round(cdr.unrounded_amount/power(10,cdr.add_implied_decimal)/100,4),'fm99,999,990.0000')
           , cdr.seqnum_rate_usage, cdr.component_id, fut.usage_type, rux.add_unit_rate
           , trim(case when cdr.provider_id  = 2 then substr(cdr.annotation,1,33)
                       when cdr.provider_id != 2 then lsx.sitecity
                  end)
           -- "valdtn" (no split rows and we must find a rate);  (produces "ORA-01476: divisor is equal to zero" if false)...
           , case when cdr.split_row_num = 0 and rux.add_unit_rate is not null then 1 else 1/0 end
           , 1, systimestamp, user
        from arbor.cdr_data cdr
        -- the c30_stm_feed_usage_type lookup table does NOT have the account segment inheritance
        -- the actual one has to be there -- it does not "defer" to a parent_acct_seg_id value
        -- (note: it's important to "join" -- not outer -- here to remove end-dated usage types in this table...)
        join c30arbor.c30_stm_feed_usage_type fut
          on     fut.acct_seg_id                    = v_acct_seg_id
         and     fut.type_id_usg                    = cdr.type_id_usg
         and nvl(fut.bill_class,cdr.bill_class)     = cdr.bill_class
         and nvl(fut.jurisdiction,cdr.jurisdiction) = cdr.jurisdiction
         and     fut.active_date                   <= cdr.trans_dt
         and nvl(fut.inactive_date,cdr.trans_dt+1)  > cdr.trans_dt
         and nvl(fut.inactive_date,sysdate+1)      <> fut.active_date -- don't use rows where the active and inactive dates are the same
        left outer join rate_usage_xpanded rux -- left outer, but missing rows will produce an error
          on rux.seqnum = cdr.seqnum_rate_usage
        left outer join lookup_switch_xpanded lsx
          on lsx.gsmsiteid IN(substr(cdr.annotation,1,6),substr(cdr.customer_tag,1,6))
       where cdr.msg_id                 = l_key_values(i).msg_id
         and cdr.msg_id2                = l_key_values(i).msg_id2
         and cdr.msg_id_serv            = l_key_values(i).msg_id_serv
         and cdr.split_row_num          = l_key_values(i).split_row_num
         and cdr.cdr_data_partition_key = l_key_values(i).cdr_data_partition_key
         and cdr.no_bill                = c_number_0 -- billable
         and cdr.comp_status            = c_number_1 -- normal
         and cdr.cdr_status             = c_number_3 -- guided and rated
      ;
   d_sql_rowcount := sql%rowcount; -- because this may not be the whole batch count with exclusion on c30_stm_feed_usage_type
   v_rowcount     := v_rowcount + d_sql_rowcount;
   commit;
   c30_put_line(d_file,'..(CSF.lcd) BATCH - count='||(d_batch_stop-d_batch_start+1)||'; sql%rowcount='||d_sql_rowcount||'; issued commit!',true,d_save_overflow,d_no_dbms_output);

   exception
      when others then
         d_batch_errors := sql%bulk_exceptions.count;
         c30_put_line(d_file,'..(CSF.lcd) Number of "forall" errors='||d_batch_errors,true,d_save_overflow,d_no_dbms_output);
         for e IN 1..d_batch_errors loop
            c30_put_line(d_file,'..(CSF.lcd) ERROR '||e||'; iteration '||sql%bulk_exceptions(e).error_index||' "'||sqlerrm(-sql%bulk_exceptions(e).error_code)||'"',true,d_save_overflow,d_no_dbms_output);
         end loop;
         rollback;
         c30_put_line(d_file,'..(CSF.lcd) Issued rollback!',true,d_save_overflow,d_no_dbms_output);
         raise;
   end;
   --================> inner block to capture any possible "forall" exceptions (ABOVE) <================

   exit when d_batch_stop = l_key_values.count;
end loop;
d_timestamp_stop := systimestamp;

-- this is the only place where the "hold" count is set (except for reruns); if zero, throw an error...
if v_rowcount = 0 then
   c30writeTransactionLog('C30STMLCD001 LOGIC ERROR! No billed records found! v_rowcount='||v_rowcount,'error');
else
   d_rowcount_hold := v_rowcount;
   c30_put_line(d_file,'..(CSF.lcd) set d_rowcount_hold to v_rowcount',true,d_save_overflow,d_no_dbms_output);
end if;

c30_put_line(d_file,'..(CSF.lcd) at end; v_rowcount='||v_rowcount||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

exception
   when others then
      c30_put_line(d_file,'..(CSF.lcd) ERROR! '||cleantext(sqlerrm,' ','t')||'" at '||systimestamp,true,d_save_overflow,d_no_dbms_output);
      rollback;
      c30_put_line(d_file,'..(CSF.lcd) Issued rollback;',true,d_save_overflow,d_no_dbms_output);
      c30writeTransactionLog('C30STMLCD999 ERROR='||sqlerrm,'error');
end load_cdr_data;

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * PROCEDURE LOAD_ACCT_NO  * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
procedure load_acct_no(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
)
is
   type t_key_record is record(
       acct_seg_id                  c30_settlement_feed_billed.acct_seg_id%type
     , account_no                   c30_settlement_feed_billed.account_no%type
     , min_trans_dt                 c30_settlement_feed_billed.trans_dt%type
     , max_trans_dt                 c30_settlement_feed_billed.trans_dt%type
     -- key values above (those that uniquely determine the values below...)
     , account_no_desc              c30_settlement_feed_billed.account_no_desc%type
                              );
   type t_key_rec_table is table of t_key_record;
   l_key_values                     t_key_rec_table;

   l_acct_no_ext_id_type            c30arbor.c30_settlement_feed_dates.acct_no_ext_id_type%type;

   -- here we use min-max trans_dt to "bind" what will be the update statement...
   cursor c_key_values is
   select sfb.acct_seg_id, sfb.account_no, min(sfb.trans_dt) min_trans_dt, max(sfb.trans_dt) max_trans_dt
        , nvl(trim(iam.external_id),'(missing)') account_no_desc
     from c30arbor.c30_settlement_feed_billed sfb
     left outer join arbor.external_id_acct_map iam
       on     iam.account_no                    = sfb.account_no
      and     iam.external_id_type              = l_acct_no_ext_id_type
      -- this assures the joined row was active on the transaction date...
      and     iam.active_date                  <= sfb.trans_dt
      and nvl(iam.inactive_date,sfb.trans_dt+1) > sfb.trans_dt
      and nvl(iam.inactive_date,sysdate+1)     <> iam.active_date -- don't use rows where the active and inactive dates are the same
   group by sfb.acct_seg_id, sfb.account_no, nvl(trim(iam.external_id),'(missing)')
   ;
begin
c30_put_line(d_file,'LOAD_ACCT_NO (CSF.lan) Started at '||systimestamp,true,d_save_overflow,d_no_dbms_output);

select dts.acct_no_ext_id_type
  into   l_acct_no_ext_id_type
  from c30arbor.c30_settlement_feed_dates dts
 where dts.acct_seg_id = v_acct_seg_id
;
c30_put_line(d_file,'..(CSF.lan) l_acct_no_ext_id_type='||l_acct_no_ext_id_type,true,d_save_overflow,d_no_dbms_output);

-- see if the index exists before drop/create (for possible reruns)...
select decode(count(*),0,0,1)
  into d_index_exists
  from dba_indexes
 where owner         = 'C30ARBOR'
   and index_name like 'C30_SF_BILLED_ACCT_NO_IDX'
   and table_owner   = 'C30ARBOR'
   and table_name    = 'C30_SETTLEMENT_FEED_BILLED'
   and status        = 'VALID'
   and visibility    = 'VISIBLE' -- if we "alter...invisible" for testing, it would have to be rebuilt
;
if d_index_exists = 0 then
   -- we "drop" in case the index exists but is invalid or invisible...
   d_sql_line := 'drop index c30arbor.c30_sf_billed_acct_no_idx';
   c30_no_error(d_sql_line,d_sqlerrm);
   c30_put_line(d_file,'..(CSF.lan) executed "'||d_sql_line||'"; d_sqlerrm="'||nvl(d_sqlerrm,'(Normal)')||'"',true,d_save_overflow,d_no_dbms_output);

   -- create an index like our l_key_values collection...
   d_timestamp_start := systimestamp;
   d_sql_line := 'create index c30arbor.c30_sf_billed_acct_no_idx on c30arbor.c30_settlement_feed_billed(acct_seg_id, account_no, trans_dt) nologging';
   c30_put_line(d_file,'..(CSF.lan) executing "'||d_sql_line||'"',true,d_save_overflow,d_no_dbms_output);
   execute immediate(d_sql_line);
   d_timestamp_stop := systimestamp;
   c30_put_line(d_file,'..(CSF.lan) c30_sf_billed_acct_no_idx index created; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);
end if;

d_timestamp_start := systimestamp;
open c_key_values;
loop
   fetch c_key_values bulk collect into l_key_values;
   exit when c_key_values%notfound;
end loop;
close c_key_values;
d_timestamp_stop := systimestamp;
c30_put_line(d_file,'..(CSF.lan) LOADED TABLE - l_key_values.count='||l_key_values.count||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

-- this is to get the external_id_acct_map trim(external_id) account_no_desc, aka "Account_Identifier"
d_timestamp_start := systimestamp;
d_batch_count     := 0;
d_batch           := greatest(least(ceil(l_key_values.count/c_num_batches),c_batch_max),c_batch_min);
c30_put_line(d_file,'..(CSF.lan) CALC BATCH SIZE - d_batch='||d_batch,true,d_save_overflow,d_no_dbms_output);
loop
   d_batch_count := d_batch_count + 1;
   d_batch_start := ((d_batch_count-1)*d_batch)+1;
   d_batch_stop  := least((d_batch_count*d_batch),l_key_values.count);
   c30_put_line(d_file,'..(CSF.lan) EXT_ID count='||d_batch_count||'; start->stop='||d_batch_start||'->'||d_batch_stop,true,d_save_overflow,d_no_dbms_output);

   --================> inner block to capture any possible "forall" exceptions (BELOW) <================
   begin
   forall i IN d_batch_start..d_batch_stop save exceptions
      update c30arbor.c30_settlement_feed_billed sfb
         set account_no_desc = l_key_values(i).account_no_desc
           , chg_date        = systimestamp
           , chg_who         = user
           , load_acct_no    = 1
       where sfb.acct_seg_id    = l_key_values(i).acct_seg_id
         and sfb.account_no     = l_key_values(i).account_no
         and sfb.trans_dt between l_key_values(i).min_trans_dt and l_key_values(i).max_trans_dt
         and sfb.load_acct_no   = c_load_not_done
      ;
   d_sql_rowcount := sql%rowcount; -- updates occur in batches
   v_rowcount     := v_rowcount + d_sql_rowcount;
   commit;
   c30_put_line(d_file,'..(CSF.lan) BATCH - count='||(d_batch_stop-d_batch_start+1)||'; sql%rowcount='||d_sql_rowcount||'; issued commit!',true,d_save_overflow,d_no_dbms_output);

   exception
      when others then
         d_batch_errors := sql%bulk_exceptions.count;
         c30_put_line(d_file,'..(CSF.lan) Number of "forall" errors='||d_batch_errors,true,d_save_overflow,d_no_dbms_output);
         for e IN 1..d_batch_errors loop
            c30_put_line(d_file,'..(CSF.lan) ERROR '||e||'; iteration '||sql%bulk_exceptions(e).error_index||' "'||sqlerrm(-sql%bulk_exceptions(e).error_code)||'"',true,d_save_overflow,d_no_dbms_output);
         end loop;
         rollback;
         c30_put_line(d_file,'..(CSF.lan) Issued rollback!',true,d_save_overflow,d_no_dbms_output);
         raise;
   end;
   --================> inner block to capture any possible "forall" exceptions (ABOVE) <================

   exit when d_batch_stop = l_key_values.count;
end loop;

-- we assume having this index will slow the next process[es], so drop it...
d_sql_line := 'drop index c30arbor.c30_sf_billed_acct_no_idx';
c30_no_error(d_sql_line,d_sqlerrm);
c30_put_line(d_file,'..(CSF.lan) executed "'||d_sql_line||'"; d_sqlerrm="'||nvl(d_sqlerrm,'(Normal)')||'"',true,d_save_overflow,d_no_dbms_output);
d_timestamp_stop := systimestamp;

c30_put_line(d_file,'..(CSF.lan) at end; v_rowcount='||v_rowcount||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

exception
   when others then
      c30_put_line(d_file,'..(CSF.lan) ERROR! '||cleantext(sqlerrm,' ','t')||'" at '||systimestamp,true,d_save_overflow,d_no_dbms_output);
      rollback;
      c30_put_line(d_file,'..(CSF.lan) Issued rollback;',true,d_save_overflow,d_no_dbms_output);
      c30writeTransactionLog('C30STMLAN999 ERROR='||sqlerrm,'error');
end load_acct_no;

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * PROCEDURE LOAD_UNIT_USAGE * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
procedure load_unit_usage(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
)
is
   type t_key_record is record(
       billing_units_type           c30_settlement_feed_billed.billing_units_type%type
     , type_id_usg                  c30_settlement_feed_billed.type_id_usg%type
     , point_id_origin              c30_settlement_feed_billed.point_id_origin%type
     , point_id_target              c30_settlement_feed_billed.point_id_target%type
     , rated_units                  c30_settlement_feed_billed.rated_units%type
     , add_unit_rate                c30_settlement_feed_billed.add_unit_rate%type
     -- key values above (those that uniquely determine the values below...)
     , call_from_city               c30_settlement_feed_billed.call_from_city%type
     , call_from_state              c30_settlement_feed_billed.call_from_state%type
     , called_city                  c30_settlement_feed_billed.called_city%type
     , called_state                 c30_settlement_feed_billed.called_state%type
     , units_type                   c30_settlement_feed_billed.units_type%type
     , rated_units_calc             c30_settlement_feed_billed.rated_units_calc%type
     , rated_units_char             c30_settlement_feed_billed.rated_units_char%type
     , rated_units_type             c30_settlement_feed_billed.rated_units_type%type
     , rate_char                    c30_settlement_feed_billed.rate_char%type
                              );
   type t_key_rec_table is table of t_key_record;
   l_key_values                     t_key_rec_table;

   cursor c_key_values is
   with usage_types_xpanded as(
   select uty.type_id_usg, uty.raw_units_type, uty.use_point_class_origin, uty.use_point_class_target
     from arbor.usage_types uty
   )
   , units_type_xpanded as(
   select units_type, display_value
     from arbor.units_type_values
    where language_code  = c_number_1
   )
   , state_values_xpanded as(
   select country_code, state_abbr, display_value
     from arbor.state_values
    where language_code = c_number_1
   )
   select distinct
          sfb.billing_units_type
        , sfb.type_id_usg
        , sfb.point_id_origin
        , sfb.point_id_target
        , sfb.rated_units
        , sfb.add_unit_rate
        , trim(upo.point_city)                                       call_from_city
        , trim(coalesce(svo.display_value
                       ,upo.point_state_abbr
                       ,pto.display_value))                          call_from_state
        , trim(upt.point_city)                                       called_city
        , trim(coalesce(svt.display_value
                       ,upt.point_state_abbr
                       ,ptt.display_value))                          called_state
        , trim(ut1.display_value)                                    units_type
          -- "1/10th" for both "Outgoing International Voice" and "Outgoing National Voice"...
        , case
            when ut2.display_value like '1/10th%' then
               sfb.rated_units/10
            else
               sfb.rated_units
          end                                                        rated_units_calc
        , case
            when ut2.display_value like '1/10th%' then
               to_char(sfb.rated_units/10,'fm999,999,999,999,990.0') -- divide rated_units by 10
            else
               to_char(sfb.rated_units,'fm999,999,999,999,990')
          end                                                        rated_units_char
        , case
            when ut2.display_value like '1/10th%' then
               replace(trim(ut2.display_value),'1/10th ') -- remove "1/10th " prefix
            else
               trim(ut2.display_value)
          end                                                        rated_units_type
        , case
            when ut2.display_value like '1/10th%' then
               to_char(sfb.add_unit_rate*10,'fm999,990.000000') -- multiply rate by 10
            else
               to_char(sfb.add_unit_rate,'fm999,990.000000')
          end                                                        rate_char
     from c30arbor.c30_settlement_feed_billed sfb
     join usage_types_xpanded utx
       on utx.type_id_usg = sfb.type_id_usg
     join units_type_xpanded ut1
       on ut1.units_type = utx.raw_units_type
     join units_type_xpanded ut2
       on ut2.units_type = sfb.billing_units_type
     left outer join arbor.usage_points upo -- "upo" = usage_points for origin
       on upo.point_id               = sfb.point_id_origin
      and utx.use_point_class_origin = c_number_1
     left outer join state_values_xpanded svo -- "svo" = state_values for origin
       on svo.country_code = upo.country_code
      and svo.state_abbr   = upo.point_state_abbr
     left outer join arbor.usage_points_text pto -- "pto" = usage_points_text for origin
       on pto.point_id      = upo.point_id
      and pto.language_code = c_number_1
     left outer join arbor.usage_points upt -- "upt" = usage_points for target
       on upt.point_id               = sfb.point_id_target
      and utx.use_point_class_target = c_number_1
     left outer join state_values_xpanded svt -- "svt" = state_values for target
       on svt.country_code = upt.country_code
      and svt.state_abbr   = upt.point_state_abbr
     left outer join arbor.usage_points_text ptt -- "ptt" = usage_points_text for target
       on ptt.point_id      = upt.point_id
      and ptt.language_code = c_number_1
   ;
begin
c30_put_line(d_file,'LOAD_UNIT_USAGE (CSF.luu) Started at '||systimestamp,true,d_save_overflow,d_no_dbms_output);

-- see if the index exists before drop/create (for possible reruns)...
select decode(count(*),0,0,1)
  into d_index_exists
  from dba_indexes
 where owner         = 'C30ARBOR'
   and index_name like 'C30_SF_BILLED_UNIT_USAGE_IDX'
   and table_owner   = 'C30ARBOR'
   and table_name    = 'C30_SETTLEMENT_FEED_BILLED'
   and status        = 'VALID'
   and visibility    = 'VISIBLE' -- if we "alter...invisible" for testing, it would have to be rebuilt
;
if d_index_exists = 0 then
   -- we "drop" in case the index exists but is invalid or invisible...
   d_sql_line := 'drop index c30arbor.c30_sf_billed_unit_usage_idx';
   c30_no_error(d_sql_line,d_sqlerrm);
   c30_put_line(d_file,'..(CSF.luu) executed "'||d_sql_line||'"; d_sqlerrm="'||nvl(d_sqlerrm,'(Normal)')||'"',true,d_save_overflow,d_no_dbms_output);

   -- create an index like our l_key_values collection...
   d_timestamp_start := systimestamp;
   d_sql_line := 'create index c30arbor.c30_sf_billed_unit_usage_idx on c30arbor.c30_settlement_feed_billed(billing_units_type, type_id_usg, point_id_origin, point_id_target) nologging';
   c30_put_line(d_file,'..(CSF.luu) executing "'||d_sql_line||'"',true,d_save_overflow,d_no_dbms_output);
   execute immediate(d_sql_line);
   d_timestamp_stop := systimestamp;
   c30_put_line(d_file,'..(CSF.luu) c30_sf_billed_unit_usage_idx index created; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);
end if;

d_timestamp_start := systimestamp;
open c_key_values;
loop
   fetch c_key_values bulk collect into l_key_values;
   exit when c_key_values%notfound;
end loop;
close c_key_values;
d_timestamp_stop := systimestamp;
c30_put_line(d_file,'..(CSF.luu) LOADED TABLE - l_key_values.count='||l_key_values.count||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

-- this is to get the call_from/called city and state and related rated units
d_timestamp_start := systimestamp;
d_batch_count     := 0;
d_batch           := greatest(least(ceil(l_key_values.count/c_num_batches),c_batch_max),c_batch_min);
c30_put_line(d_file,'..(CSF.luu) CALC BATCH SIZE - d_batch='||d_batch,true,d_save_overflow,d_no_dbms_output);
loop
   d_batch_count := d_batch_count + 1;
   d_batch_start := ((d_batch_count-1)*d_batch)+1;
   d_batch_stop  := least((d_batch_count*d_batch),l_key_values.count);
   c30_put_line(d_file,'..(CSF.luu) UNIT_USAGE count='||d_batch_count||'; start->stop='||d_batch_start||'->'||d_batch_stop,true,d_save_overflow,d_no_dbms_output);

   --================> inner block to capture any possible "forall" exceptions (BELOW) <================
   begin
   forall i IN d_batch_start..d_batch_stop save exceptions
      update c30arbor.c30_settlement_feed_billed sfb
         set call_from_city   = l_key_values(i).call_from_city
           , call_from_state  = l_key_values(i).call_from_state
           , called_city      = l_key_values(i).called_city
           , called_state     = l_key_values(i).called_state
           , units_type       = l_key_values(i).units_type
           , rated_units_char = l_key_values(i).rated_units_char
           , rated_units_type = l_key_values(i).rated_units_type
           , rate_char        = l_key_values(i).rate_char
           , rated_units_calc = l_key_values(i).rated_units_calc
           , chg_date         = systimestamp
           , chg_who          = user
           , load_unit_usage  = 1
       where sfb.billing_units_type = l_key_values(i).billing_units_type
         and sfb.type_id_usg        = l_key_values(i).type_id_usg
         and sfb.point_id_origin    = l_key_values(i).point_id_origin
         and sfb.point_id_target    = l_key_values(i).point_id_target
         and sfb.rated_units        = l_key_values(i).rated_units
         and sfb.add_unit_rate      = l_key_values(i).add_unit_rate
         and sfb.load_unit_usage    = c_load_not_done
      ;
   d_sql_rowcount := sql%rowcount; -- updates occur in batches
   v_rowcount     := v_rowcount + d_sql_rowcount;
   commit;
   c30_put_line(d_file,'..(CSF.luu) BATCH - count='||(d_batch_stop-d_batch_start+1)||'; sql%rowcount='||d_sql_rowcount||'; issued commit!',true,d_save_overflow,d_no_dbms_output);

   exception
      when others then
         d_batch_errors := sql%bulk_exceptions.count;
         c30_put_line(d_file,'..(CSF.luu) Number of "forall" errors='||d_batch_errors,true,d_save_overflow,d_no_dbms_output);
         for e IN 1..d_batch_errors loop
            c30_put_line(d_file,'..(CSF.luu) ERROR '||e||'; iteration '||sql%bulk_exceptions(e).error_index||' "'||sqlerrm(-sql%bulk_exceptions(e).error_code)||'"',true,d_save_overflow,d_no_dbms_output);
         end loop;
         rollback;
         c30_put_line(d_file,'..(CSF.luu) Issued rollback!',true,d_save_overflow,d_no_dbms_output);
         raise;
   end;
   --================> inner block to capture any possible "forall" exceptions (ABOVE) <================

   exit when d_batch_stop = l_key_values.count;
end loop;

-- we assume having this index will slow the next process[es], so drop it...
d_sql_line := 'drop index c30arbor.c30_sf_billed_unit_usage_idx';
c30_no_error(d_sql_line,d_sqlerrm);
c30_put_line(d_file,'..(CSF.luu) executed "'||d_sql_line||'"; d_sqlerrm="'||nvl(d_sqlerrm,'(Normal)')||'"',true,d_save_overflow,d_no_dbms_output);
d_timestamp_stop := systimestamp;

c30_put_line(d_file,'..(CSF.luu) at end; v_rowcount='||v_rowcount||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

exception
   when others then
      c30_put_line(d_file,'..(CSF.luu) ERROR! '||cleantext(sqlerrm,' ','t')||'" at '||systimestamp,true,d_save_overflow,d_no_dbms_output);
      rollback;
      c30_put_line(d_file,'..(CSF.luu) Issued rollback;',true,d_save_overflow,d_no_dbms_output);
      c30writeTransactionLog('C30STMLUU999 ERROR='||sqlerrm,'error');
end load_unit_usage;

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * PROCEDURE LOAD_SVC_PLAN * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
procedure load_svc_plan(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
)
is
   type t_key_record is record(
       subscr_no                    c30_settlement_feed_billed.subscr_no%type
     , subscr_no_resets             c30_settlement_feed_billed.subscr_no_resets%type
     , component_id                 c30_settlement_feed_billed.component_id%type
     , min_trans_dt                 c30_settlement_feed_billed.trans_dt%type
     , max_trans_dt                 c30_settlement_feed_billed.trans_dt%type
     -- key values above (those that uniquely determine the values below...)
     , phone_no                     c30_settlement_feed_billed.phone_no%type
     , catalog_id                   c30_settlement_feed_billed.catalog_id%type
     , imsi                         c30_settlement_feed_billed.imsi%type
                              );
   type t_key_rec_table is table of t_key_record;
   l_key_values                     t_key_rec_table;

   -- here we use min-max trans_dt to "bind" what will be the update statement...
   cursor c_key_values is
   select sfb.subscr_no, sfb.subscr_no_resets, sfb.component_id, min(sfb.trans_dt) min_trans_dt, max(sfb.trans_dt) max_trans_dt
        , case when trim(iem.external_id) is not null then trim(iem.external_id)
               -- if the external ID on the table is the phone number, use it...
               when sfb.external_id_type = c_eit_tn   then trim(sfb.external_id)
                                                      else '(missing)'
          end                                                        phone_no
        , nvl(trim(iid.catalog_id),'(missing)')                      catalog_id
        -- internal spaces prevent Excel's scientific notation:  first 3 digits are the Mobile Country Code (MMC),
        -- second 3 digits are the Mobile Network Code (MNC), and last 9 digits are the Mobile Subscription Identification Number (MSIN)...
        , case when trim(ie2.external_id) is not null then substr(trim(ie2.external_id),1,3)||' '||substr(trim(ie2.external_id),4,3)||' '||substr(trim(ie2.external_id),7,9)
               -- if the external ID on the table is the IMSI, use it...
               when sfb.external_id_type = c_eit_imsi then substr(trim(sfb.external_id),1,3)||' '||substr(trim(sfb.external_id),4,3)||' '||substr(trim(sfb.external_id),7,9)
                                                      else '(missing)'
          end                                                        imsi
     from c30arbor.c30_settlement_feed_billed sfb
     --* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     -- the author hates the subquery logic!  it's not too inefficient since the vast majority of joins will be true on the 1st condition.
     -- for customer_id_equip_map and cmf_package_component we were running into join issues -- missing rows or double rows -- when
     -- usage occurred on the inactive date.  the latter is midnight but the trans_dt always has a time component.  since in Kenan the
     -- inactive date is exclusive, looking for strictly greater than missed end-dated records.  if you look for "or equal" and trunc
     -- the inactive date, you risk picking up a new entry that starts on the same day (double rows).  so if you get a hit on trans_dt
     -- equals the inactive date, you have to make sure there isn't a new row that starts on that same date.
     --* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     -- note: date criteria using "sfb.trans_dt" assures the joined row was active on the transaction date
     left outer join arbor.customer_id_equip_map iem
       on     iem.external_id_type          = c_eit_tn
      and     iem.subscr_no                 = sfb.subscr_no
      and     iem.subscr_no_resets          = sfb.subscr_no_resets
      and     iem.active_date              <= sfb.trans_dt
      and nvl(iem.inactive_date,sysdate+1) <> iem.active_date -- don't use rows where the active and inactive dates are the same
      and( -- open 1 (normal "greater than" inactive date logic)
           (nvl(iem.inactive_date,sfb.trans_dt+1) > sfb.trans_dt)
         or( -- open 2 (inactive date equals [truncated] trans_dt...)
            nvl(iem.inactive_date,sfb.trans_dt+1) = trunc(sfb.trans_dt)
        and 1 >( -- open 3 (...and no new row is active right on the transaction date)
               select count(*)
                 from arbor.customer_id_equip_map xxx
                where     xxx.external_id_type              =       c_eit_tn
                  and     xxx.subscr_no                     =       sfb.subscr_no
                  and     xxx.subscr_no_resets              =       sfb.subscr_no_resets
                  and     xxx.active_date                   = trunc(sfb.trans_dt)
                  and nvl(xxx.inactive_date,sfb.trans_dt+1) > trunc(sfb.trans_dt)
                  and nvl(xxx.inactive_date,sysdate+1)     <>       xxx.active_date -- don't use rows where the active and inactive dates are the same
               ) -- close 3
           ) -- close 2
         ) -- close 1
     left outer join arbor.cmf_package_component cpc
       on     cpc.component_id            = sfb.component_id
      and     cpc.parent_subscr_no        = sfb.subscr_no
      and     cpc.parent_subscr_no_resets = sfb.subscr_no_resets
      and     cpc.active_dt              <= sfb.trans_dt
      and nvl(cpc.inactive_dt,sysdate+1) <> cpc.active_dt -- don't use rows where the active and inactive dates are the same
      and( -- open 1 (normal "greater than" inactive date logic)
           (nvl(cpc.inactive_dt,sfb.trans_dt+1) > sfb.trans_dt)
         or( -- open 2 (inactive date equals [truncated] trans_dt...)
            nvl(cpc.inactive_dt,sfb.trans_dt+1) = trunc(sfb.trans_dt)
        and 1 >( -- open 3 (...and no new row is active right on the transaction date)
               select count(*)
                 from arbor.cmf_package_component xxx
                where     xxx.component_id                =       sfb.component_id
                  and     xxx.parent_subscr_no            =       sfb.subscr_no
                  and     xxx.parent_subscr_no_resets     =       sfb.subscr_no_resets
                  and     xxx.active_dt                   = trunc(sfb.trans_dt)
                  and nvl(xxx.inactive_dt,sfb.trans_dt+1) > trunc(sfb.trans_dt)
                  and nvl(xxx.inactive_dt,sysdate+1)     <>       xxx.active_dt -- don't use rows where the active and inactive dates are the same
               ) -- close 3
           ) -- close 2
         ) -- close 1
     left outer join c30arbor.c30_external_inst_id_def iid
       on iid.acct_seg_id              = v_acct_seg_id
      and iid.component_inst_id        = cpc.component_inst_id
      and iid.component_inst_id_serv   = cpc.component_inst_id_serv
      and iid.primary_subscr_no        = cpc.parent_subscr_no
      and iid.primary_subscr_no_resets = cpc.parent_subscr_no_resets
     --* * * * * * * * * * * * * * * * * * * * * * * *
     --* "left outer join", possibly no IMSI (20015) *
     --* * * * * * * * * * * * * * * * * * * * * * * *
     -- note: date criteria using "sfb.trans_dt" assures the joined row was active on the transaction date
     left outer join arbor.customer_id_equip_map ie2
       on     ie2.external_id_type          = c_eit_imsi
      and     ie2.subscr_no                 = sfb.subscr_no
      and     ie2.subscr_no_resets          = sfb.subscr_no_resets
      and     ie2.active_date              <= sfb.trans_dt
      and nvl(ie2.inactive_date,sysdate+1) <> ie2.active_date -- don't use rows where the active and inactive dates are the same
      and( -- open 1 (normal "greater than" inactive date logic)
           (nvl(ie2.inactive_date,sfb.trans_dt+1) > sfb.trans_dt)
         or( -- open 2 (inactive date equals [truncated] trans_dt...)
            nvl(ie2.inactive_date,sfb.trans_dt+1) = trunc(sfb.trans_dt)
        and 1 >( -- open 3 (...and no new row is active right on the transaction date)
               select count(*)
                 from arbor.customer_id_equip_map xxx
                where     xxx.external_id_type              =       c_eit_imsi
                  and     xxx.subscr_no                     =       sfb.subscr_no
                  and     xxx.subscr_no_resets              =       sfb.subscr_no_resets
                  and     xxx.active_date                   = trunc(sfb.trans_dt)
                  and nvl(xxx.inactive_date,sfb.trans_dt+1) > trunc(sfb.trans_dt)
                  and nvl(xxx.inactive_date,sysdate+1)     <>       xxx.active_date -- don't use rows where the active and inactive dates are the same
               ) -- close 3
           ) -- close 2
         ) -- close 1
   group by sfb.subscr_no, sfb.subscr_no_resets, sfb.component_id
        , case when trim(iem.external_id) is not null then trim(iem.external_id)
               when sfb.external_id_type = c_eit_tn   then trim(sfb.external_id)
                                                      else '(missing)'
          end
        , nvl(trim(iid.catalog_id),'(missing)')
        , case when trim(ie2.external_id) is not null then substr(trim(ie2.external_id),1,3)||' '||substr(trim(ie2.external_id),4,3)||' '||substr(trim(ie2.external_id),7,9)
               when sfb.external_id_type = c_eit_imsi then substr(trim(sfb.external_id),1,3)||' '||substr(trim(sfb.external_id),4,3)||' '||substr(trim(sfb.external_id),7,9)
                                                      else '(missing)'
          end
   ;
begin
c30_put_line(d_file,'LOAD_SVC_PLAN (CSF.lsp) Started at '||systimestamp,true,d_save_overflow,d_no_dbms_output);

-- see if the index exists before drop/create (for possible reruns)...
select decode(count(*),0,0,1)
  into d_index_exists
  from dba_indexes
 where owner         = 'C30ARBOR'
   and index_name like 'C30_SF_BILLED_SVC_PLAN_IDX'
   and table_owner   = 'C30ARBOR'
   and table_name    = 'C30_SETTLEMENT_FEED_BILLED'
   and status        = 'VALID'
   and visibility    = 'VISIBLE' -- if we "alter...invisible" for testing, it would have to be rebuilt
;
if d_index_exists = 0 then
   -- we "drop" in case the index exists but is invalid or invisible...
   d_sql_line := 'drop index c30arbor.c30_sf_billed_svc_plan_idx';
   c30_no_error(d_sql_line,d_sqlerrm);
   c30_put_line(d_file,'..(CSF.lsp) executed "'||d_sql_line||'"; d_sqlerrm="'||nvl(d_sqlerrm,'(Normal)')||'"',true,d_save_overflow,d_no_dbms_output);

   -- create an index like our l_key_values collection...
   d_timestamp_start := systimestamp;
   d_sql_line := 'create index c30arbor.c30_sf_billed_svc_plan_idx on c30arbor.c30_settlement_feed_billed(subscr_no, subscr_no_resets, component_id) nologging';
   c30_put_line(d_file,'..(CSF.lsp) executing "'||d_sql_line||'"',true,d_save_overflow,d_no_dbms_output);
   execute immediate(d_sql_line);
   d_timestamp_stop := systimestamp;
   c30_put_line(d_file,'..(CSF.lsp) c30_sf_billed_svc_plan_idx index created; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);
end if;

d_timestamp_start := systimestamp;
open c_key_values;
loop
   fetch c_key_values bulk collect into l_key_values;
   exit when c_key_values%notfound;
end loop;
close c_key_values;
d_timestamp_stop := systimestamp;
c30_put_line(d_file,'..(CSF.lsp) LOADED TABLE - l_key_values.count='||l_key_values.count||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

-- this is to get the service's external ID type 60 (TN) and 20015 (IMSI) with K3 catalog ID
d_timestamp_start := systimestamp;
d_batch_count     := 0;
d_batch           := greatest(least(ceil(l_key_values.count/c_num_batches),c_batch_max),c_batch_min);
c30_put_line(d_file,'..(CSF.lsp) CALC BATCH SIZE - d_batch='||d_batch,true,d_save_overflow,d_no_dbms_output);
loop
   d_batch_count := d_batch_count + 1;
   d_batch_start := ((d_batch_count-1)*d_batch)+1;
   d_batch_stop  := least((d_batch_count*d_batch),l_key_values.count);
   c30_put_line(d_file,'..(CSF.lsp) SVC_PLAN count='||d_batch_count||'; start->stop='||d_batch_start||'->'||d_batch_stop,true,d_save_overflow,d_no_dbms_output);

   --================> inner block to capture any possible "forall" exceptions (BELOW) <================
   begin
   forall i IN d_batch_start..d_batch_stop save exceptions
      update c30arbor.c30_settlement_feed_billed sfb
         set phone_no      = l_key_values(i).phone_no
           , catalog_id    = l_key_values(i).catalog_id
           , imsi          = l_key_values(i).imsi
           , chg_date      = systimestamp
           , chg_who       = user
           , load_svc_plan = 1
       where sfb.subscr_no        = l_key_values(i).subscr_no
         and sfb.subscr_no_resets = l_key_values(i).subscr_no_resets
         and sfb.component_id     = l_key_values(i).component_id
         and sfb.trans_dt   between l_key_values(i).min_trans_dt and l_key_values(i).max_trans_dt
         and sfb.load_svc_plan    = c_load_not_done
      ;
   d_sql_rowcount := sql%rowcount; -- updates occur in batches
   v_rowcount     := v_rowcount + d_sql_rowcount;
   commit;
   c30_put_line(d_file,'..(CSF.lsp) BATCH - count='||(d_batch_stop-d_batch_start+1)||'; sql%rowcount='||d_sql_rowcount||'; issued commit!',true,d_save_overflow,d_no_dbms_output);

   exception
      when others then
         d_batch_errors := sql%bulk_exceptions.count;
         c30_put_line(d_file,'..(CSF.lsp) Number of "forall" errors='||d_batch_errors,true,d_save_overflow,d_no_dbms_output);
         for e IN 1..d_batch_errors loop
            c30_put_line(d_file,'..(CSF.lsp) ERROR '||e||'; iteration '||sql%bulk_exceptions(e).error_index||' "'||sqlerrm(-sql%bulk_exceptions(e).error_code)||'"',true,d_save_overflow,d_no_dbms_output);
         end loop;
         rollback;
         c30_put_line(d_file,'..(CSF.lsp) Issued rollback!',true,d_save_overflow,d_no_dbms_output);
         raise;
   end;
   --================> inner block to capture any possible "forall" exceptions (ABOVE) <================

   exit when d_batch_stop = l_key_values.count;
end loop;
d_timestamp_stop := systimestamp;

c30_put_line(d_file,'..(CSF.lsp) at end; v_rowcount='||v_rowcount||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

exception
   when others then
      c30_put_line(d_file,'..(CSF.lsp) ERROR! '||cleantext(sqlerrm,' ','t')||'" at '||systimestamp,true,d_save_overflow,d_no_dbms_output);
      rollback;
      c30_put_line(d_file,'..(CSF.lsp) Issued rollback;',true,d_save_overflow,d_no_dbms_output);
      c30writeTransactionLog('C30STMLSP999 ERROR='||sqlerrm,'error');
end load_svc_plan;

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * PROCEDURE GENERATE_CSV_FILES  * * * * * * * * * * * * * * * * * * * * * * * *
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
procedure generate_csv_files(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
)
is
   l_cutoff_date                    varchar2(8);
   l_csv_file_name                  varchar2(50);
   l_put_line                       pls_integer;

begin
c30_put_line(d_file,'GENERATE_CSV_FILES (CSF.gcf) Started at '||systimestamp,true,d_save_overflow,d_no_dbms_output);

-- get cutoff date from c30_settlement_feed_dates...
select to_char(dts.billed_cutoff_date,'yyyymmdd')
  into l_cutoff_date
  from c30arbor.c30_settlement_feed_dates dts
;
-- create the output detail CSV file; output file has the cutoff date[s] suffix...
d_timestamp_start := systimestamp;
l_csv_file_name   := 'settlement_feed_billed_detail_'||l_cutoff_date||'.csv';

-- in testing, with d_put_line = 1 or 2, we're writing serveroutput limited to 1,000,000 bytes
-- the average row length on the table is under 250 bytes but with commas and double quotes, a CSV record is more like 275 bytes
-- if this is a "normal" production-size table, we'll overflow ("ORU-10027: buffer overflow, limit of 1000000 bytes")
-- so, get a count of the records; if > 1,000,000/275 (3,636) rows, round down to 3,500 for the other output, override d_put_line to 0
select   count(*)
  into v_rowcount -- theory here is the detail view record count should match the base table's
  from c30arbor.c30_settlement_feed_detail
;
if v_rowcount <= 3500 then
   l_put_line := d_put_line;
else
   l_put_line := 0;
   c30_put_line(d_file,'..(CSF.gcf) Overriding d_put_line: '||l_put_line||' (output will likely exceed 1,000,000 serveroutput byte limit)',true,d_save_overflow,d_no_dbms_output);
end if;

c30_put_line(d_file,'..(CSF.gcf) Creating detail CSV file: '||l_csv_file_name||' '||systimestamp,true,d_save_overflow,d_no_dbms_output);
c30_dump_table_to_csv(
   v_table_name      => 'c30arbor.c30_settlement_feed_detail'
  ,v_directory       => 'DEPLOYCONFIGS'
  ,v_csv_file        => l_csv_file_name
  ,v_del             => '","'
  ,v_put_line        => l_put_line
  ,v_order_by        => '"Usage_Type","Usage_Datetime"'
);
d_timestamp_stop := systimestamp;
c30_put_line(d_file,'..(CSF.gcf) End DETAIL process '||systimestamp||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

-- create the output summary CSV file; output file has the cutoff date[s] suffix...
d_timestamp_start := systimestamp;
l_csv_file_name   := 'summary_by_usage_type_billed_'||l_cutoff_date||'.csv';

c30_put_line(d_file,'..(CSF.gcf) Creating summary CSV file: '||l_csv_file_name||' '||systimestamp,true,d_save_overflow,d_no_dbms_output);
c30_dump_table_to_csv(
   v_table_name      => 'c30arbor.c30_settlement_feed_summary'
  ,v_directory       => 'DEPLOYCONFIGS'
  ,v_csv_file        => l_csv_file_name
  ,v_del             => '","'
  ,v_put_line        => d_put_line
  ,v_order_by        => '"Usage_Type"'
);
d_timestamp_stop := systimestamp;
c30_put_line(d_file,'..(CSF.gcf) End SUMMARY process '||systimestamp||'; time diff='||(d_timestamp_stop-d_timestamp_start),true,d_save_overflow,d_no_dbms_output);

exception
   when others then
      c30_put_line(d_file,'..(CSF.gcf) ERROR! '||cleantext(sqlerrm,' ','t')||'" at '||systimestamp,true,d_save_overflow,d_no_dbms_output);
      c30writeTransactionLog('C30STMGCF999 ERROR='||sqlerrm,'error');
end generate_csv_files;

end c30_stm_feed;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_stm_feed');
--spool off
exit rollback