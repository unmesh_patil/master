set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/901_restore_c30_transaction_log_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: 901_restore_c30_transaction_log.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : K3 AWN
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : (Typically used shortly after running 900_backup_c30_transaction_log.sql)
prompt 
prompt Purpose of Script: Restore the c30_transaction_log
prompt Expected Outcome : Script is "All or Nothing", all inserts should be successful or rollback
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 09/27/2012 ##       Initial development
prompt chip.lafurney@cycle30.com        ## 09/28/2012 ##       v1.1 Must be specific with the column names for insert
prompt chip.lafurney@cycle30.com        ## 10/02/2012 ##       v1.2 Disable/enable c30_transaction_log_bitrig trigger too
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This is "All or Nothing", all inserts should be successful or rollback
prompt *****************************************************************************************************
-- for this procedure, the block will do the "rollback"; we need to "continue" to re-enable the triggers no matter what
whenever sqlerror continue
alter trigger c30arbor.c30_transaction_log_budtrig disable
;
alter trigger c30arbor.c30_transaction_log_bitrig disable
;
begin
dbms_output.put(chr(10));

-- make sure there are no duplicates...
delete c30arbor.c30_transaction_log
 where(sid,chg_date_char,log_msg,log_type) IN( -- PK columns
    select sid,chg_date_char,log_msg,log_type
      from c30temp.tmp_transaction_log_backup
                                             )
;
c30_put_line(null,'Deleted '||sql%rowcount||' c30_transaction_log row[s] (duplicates)');

-- now insert all backup rows...
-- v1.1 problem sometimes is, the number of columns before and after a change in the table can differ...
insert into c30arbor.c30_transaction_log
      (log_msg,log_type,sid,chg_date_char,chg_date
     , chg_date_local,chg_who,sessionid,terminal,host
     , client_info,os_user,ip_address,program,module
     , transaction_id,acct_seg_id,account_no,subscr_no,subscr_no_resets
     , bill_ref_no,bill_ref_resets,package_inst_id,package_inst_id_serv,package_id
     , component_inst_id,component_inst_id_serv,component_id,tracking_id,tracking_id_serv
     , force_id,format_call_stack)
select log_msg,log_type,sid,chg_date_char,chg_date
     , chg_date_local,chg_who,sessionid,terminal,host
     , client_info,os_user,ip_address,program,module
     , transaction_id,acct_seg_id,account_no,subscr_no,subscr_no_resets
     , bill_ref_no,bill_ref_resets,package_inst_id,package_inst_id_serv,package_id
     , component_inst_id,component_inst_id_serv,component_id,tracking_id,tracking_id_serv
     , force_id,format_call_stack
  from c30temp.tmp_transaction_log_backup
order by chg_date
;
c30_put_line(null,'Inserted '||sql%rowcount||' c30_transaction_log row[s] (from backup)');

-- life's good -- issue final commit and send to dbms_output
commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script for 901_restore_c30_transaction_log');

exception
   when others then
      rollback;
      c30_put_line(null,'rollback;');
      c30_put_line(null,sqlerrm);
      raise;
end;
/
alter trigger c30arbor.c30_transaction_log_budtrig enable
;
alter trigger c30arbor.c30_transaction_log_bitrig enable
;
--spool off
exit rollback
