set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/110_create_types_c30ext_c30name_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Config (n/a)
prompt Date: 08-15-2011
prompt Created By: Mark Doyle
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 110_create_types_c30catalog_c30identifier.sql
prompt Version: 1.0 Initial Creation for Kenan SQL API
prompt DETAILED EXPLANATION:
prompt Create c30namevalue, c30namevaluepairs, c30extdata, and c30extdatacol types
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: These may return Oracle error "...object [OBJECT_NAME] does not exist"...
drop type c30arbor.c30identifiercol
;
drop type c30arbor.c30identifier
;
drop type c30arbor.c30catalogcol
;
drop type c30arbor.c30catalog
;

create or replace type c30arbor.c30catalog
as
object(
  catalog_id         varchar2(512),
  external_inst_id   varchar2(512),
  component_id       number(10), 
  component_descr    varchar2(240)
  );
/
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30catalog');

create or replace type c30arbor.c30catalogcol
as
table of c30arbor.c30catalog;
/
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30catalogcol');

create or replace type c30arbor.c30identifier
as
object(
  external_id             varchar2(144),
  external_id_type        number(6),
  external_id_type_descr  varchar2(240)
  );
/
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30identifier');

create or replace type c30arbor.c30identifiercol
as
table of c30arbor.c30identifier;
/
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30identifiercol');

-- spool off
exit rollback