set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/106_create_trigger_c30_transaction_log_budtrig_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 106_create_trigger_c30_transaction_log_budtrig.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt
prompt Purpose of Script: Create c30_transaction_log_budtrig trigger
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 04/11/2012 ##  Initial Creation for Kenan SQL API
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace trigger c30arbor.c30_transaction_log_budtrig
before update or delete
on c30arbor.c30_transaction_log
for each row
-- Release Notes
-- Config (n/a)
-- Date: 04-11-2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 106_create_trigger_c30_transaction_log_budtrig.sql
-- Version: 1.0 Initial Creation for Kenan SQL API
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 05/07/2012 ##  v1.1 Defect 451 - Triggers should RAISE errors that calling object[s] will handle in exception section
--                                                (note: error messages should NOT have the standard KSA prefix; instead, attempt to use a unique sqlcode);
--                                                checking if c30writeTransactionLog is in the stack was worthless -- ALL KSA transactions use it so it's always there
-- DETAILED EXPLANATION:
-- Create c30_transaction_log_budtrig trigger to prevent updating or deleting the transaction log
-- unless the object doing the change is "C30ARBOR.C30WRITETRANSACTIONLOG"
begin

   --v1.1 Defect 451 - Triggers should RAISE errors that calling object[s] will handle in exception section
   if updating then
      raise_application_error(-20020,'Update of c30_transaction_log not allowed');
   else -- assume deleting
      raise_application_error(-20021,'Delete of c30_transaction_log not allowed');
   end if;

-- v1.1 Defect 451 - No exception section in triggers since it becomes "when others then raise:" which is the default
end c30_transaction_log_budtrig;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_transaction_log_budtrig');
--spool off
exit rollback