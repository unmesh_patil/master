set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
set define off
prompt *****************************************************************************************************
prompt Release Notes
prompt Script file Name: 
prompt Date: 
prompt Created By: Mark Doyle
prompt Defect Resolved: 
prompt Prepaid Account Segment
prompt Special instructions: Needs to be run in UAT, PROD pending approval from Kenan Billing Manager
prompt *****************************************************************************************************
prompt ** On encountering an error this script will rollback in its entirety
prompt *****************************************************************************************************
begin
  dbms_output.put(chr(10)); 

insert into arbor.acct_seg_ref ( acct_seg_id, is_default, is_internal )
values ( 4, 0, 0 );
insert into arbor.acct_seg_values ( acct_seg_id, language_code, short_display, display_value )
values ( 4, 1, 'PRE', 'AWN Prepaid FOR GCI' );
insert into C30_ACCT_SEG_DEF (acct_seg_id, parent_acct_seg_id, pas_account_no, acct_seg_descr, acct_seg_type, default_language_code)
values (4, null, null, 'Prepaid', 2, 1);

insert into CSR_ACCT_SEG (csr_name, acct_seg_id, is_csr_default)
values ('c30arbor', 4, 0);
insert into CSR_ACCT_SEG (csr_name, acct_seg_id, is_csr_default)
values ('arbor', 4, 0);

-- c30_acct_seg_obj_map
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'account_category', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'bill_disp_meth', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'bill_fmt_opt', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'bill_period', 'M01', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'billing_inquiry_center', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'country_code', '840', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'currency_code', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'emf_config_id', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'franchise_tax_code', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'insert_grp_id', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'inventory_type_id', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'language_code', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'mkt_code', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'msg_grp_id', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'owning_cost_ctr', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'rate_class', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'regulatory_id', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'remittance_center', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'return_mail_center', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'rev_rcv_cost_ctr', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'serv_external_id_type', '20008', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'serv_external_id_type', '20015', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'serv_external_id_type', '60', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'service_inquiry_center', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'status_reason_id', '1', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'status_reason_id', '2', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'status_reason_id', '3', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'status_reason_id', '4', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'status_reason_id', '5', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'status_reason_id', '6', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'status_reason_id', '7', 1);
Insert into C30_ACCT_SEG_OBJ_MAP
   (ACCT_SEG_ID, OBJECT_NAME, OBJECT_VALUE, IS_DEFAULT)
 Values
   (4, 'vip_code', '1', 1);

-- c30_api_seg_defaults
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_account_category', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_account_type', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_address1', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_address2', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_address3', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_bill_disp_meth', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_bill_fmt_opt', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_bill_period', 'M01');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_billing_inquiry_center', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_city', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_collection_indicator', '0');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_collection_status', '0');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_company', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_country_code', '840');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_county', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_currency_code', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_extended_postal_code', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_fname', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_franchise_tax_code', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_house_number', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_house_number_suffix', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_insert_grp_id', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_language_code', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_lname', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_minit', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_mkt_code', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_msg_grp_id', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_name_generation', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_name_pre', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_owning_cost_ctr', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_parent_id', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_postal_code', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_postfix_directional', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_prefix_directional', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_rate_class', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_regulatory_id', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_remittance_center', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_return_mail_center', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_rev_rcv_cost_ctr', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_service_inquiry_center', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_state', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_street_name', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_street_suffix', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_unit_no', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_unit_type', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_vertex_geocode', '000000000');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addaccount', 'v_vip_code', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_city', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_connect_reason', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_country_code', '840');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_county', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_currency_code', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_emf_config_id', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_extended_postal_code', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_franchise_tax_code', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_house_number', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_house_number_suffix', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_postal_code', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_postfix_directional', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_prefix_directional', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_rate_class', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_rev_rcv_cost_ctr', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_state', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_street_name', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_street_suffix', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_timezone', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_unit_no', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_unit_type', NULL);
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30addservice', 'v_vertex_geocode', '000000000');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30discservice', 'v_disconnect_reason', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30resumeservice', 'v_resume_reason', '1');
Insert into C30_API_SEG_DEFAULTS
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, SEG_PARAMETER_DEFAULT)
 Values
   (4, 'c30suspendservice', 'v_suspend_reason', '1');

-- c30_acct_seg_sys_parameters
Insert into C30_ACCT_SEG_SYS_PARAMETERS
   (ACCT_SEG_ID, PARAMETER_NAME, PARAMETER_VALUE)
 Values
   (4, 'acct_no_ext_id_type', '2');
Insert into C30_ACCT_SEG_SYS_PARAMETERS
   (ACCT_SEG_ID, PARAMETER_NAME, PARAMETER_VALUE)
 Values
   (4, 'debug_switch', '3');
Insert into C30_ACCT_SEG_SYS_PARAMETERS
   (ACCT_SEG_ID, PARAMETER_NAME, PARAMETER_VALUE)
 Values
   (4, 'language_code', '1');
Insert into C30_ACCT_SEG_SYS_PARAMETERS
   (ACCT_SEG_ID, PARAMETER_NAME, PARAMETER_VALUE)
 Values
   (4, 'segment_package_id', '1');
Insert into C30_ACCT_SEG_SYS_PARAMETERS
   (ACCT_SEG_ID, PARAMETER_NAME, PARAMETER_VALUE)
 Values
   (4, 'serv_currency_code', 'use_account');
Insert into C30_ACCT_SEG_SYS_PARAMETERS
   (ACCT_SEG_ID, PARAMETER_NAME, PARAMETER_VALUE)
 Values
   (4, 'serv_rate_class', 'use_account');
Insert into C30_ACCT_SEG_SYS_PARAMETERS
   (ACCT_SEG_ID, PARAMETER_NAME, PARAMETER_VALUE)
 Values
   (4, 'serv_rev_rcv_cost_ctr', 'use_account');

-- c30_api_seg_permissions 
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30addaccount');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30addcomponent');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30addidentifier');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30addplan');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30addplanmember');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30addservice');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30changeplan');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30changeplanhost');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30disccomponent');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30discidentifier');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30discservice');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30getplaninfo');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30getserviceinfo');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30resumeservice');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30suspendservice');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30swapidentifier');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30updateaccount');
Insert into C30_API_SEG_PERMISSIONS
   (ACCT_SEG_ID, API_NAME)
 Values
   (4, 'c30updateservice');

-- c30_external_catalog_def
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'DATA');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'DATA+MMS');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'DATA+TETH');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'DATA+TETH+MMS');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'DATAONLY');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'INTLCALL');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'INTLROAM');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'MSG');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'REMROAM');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'REMTOLL');
Insert into C30_EXTERNAL_CATALOG_DEF
   (ACCT_SEG_ID, CATALOG_ID)
 Values
   (4, 'VOICE');

-- c30_external_catalog_map
Insert into C30_EXTERNAL_CATALOG_MAP
   (ACCT_SEG_ID, CATALOG_ID, CATALOG_TYPE, PRIMARY_COMPONENT_ID, SECONDARY_COMPONENT_ID)
 Values
   (4, 'VOICE', 'P', 21, 21);
Insert into C30_EXTERNAL_CATALOG_MAP
   (ACCT_SEG_ID, CATALOG_ID, CATALOG_TYPE, PRIMARY_COMPONENT_ID, SECONDARY_COMPONENT_ID)
 Values
   (4, 'DATA', 'C', 22, NULL);
Insert into C30_EXTERNAL_CATALOG_MAP
   (ACCT_SEG_ID, CATALOG_ID, CATALOG_TYPE, PRIMARY_COMPONENT_ID, SECONDARY_COMPONENT_ID)
 Values
   (4, 'DATA+TETH', 'C', 22, NULL);
Insert into C30_EXTERNAL_CATALOG_MAP
   (ACCT_SEG_ID, CATALOG_ID, CATALOG_TYPE, PRIMARY_COMPONENT_ID, SECONDARY_COMPONENT_ID)
 Values
   (4, 'MSG', 'C', 23, NULL);
Insert into C30_EXTERNAL_CATALOG_MAP
   (ACCT_SEG_ID, CATALOG_ID, CATALOG_TYPE, PRIMARY_COMPONENT_ID, SECONDARY_COMPONENT_ID)
 Values
   (4, 'DATA+MMS', 'C', 22, NULL);
Insert into C30_EXTERNAL_CATALOG_MAP
   (ACCT_SEG_ID, CATALOG_ID, CATALOG_TYPE, PRIMARY_COMPONENT_ID, SECONDARY_COMPONENT_ID)
 Values
   (4, 'DATA+TETH+MMS', 'C', 22, NULL);
Insert into C30_EXTERNAL_CATALOG_MAP
   (ACCT_SEG_ID, CATALOG_ID, CATALOG_TYPE, PRIMARY_COMPONENT_ID, SECONDARY_COMPONENT_ID)
 Values
   (4, 'DATAONLY', 'P', 24, 24);

-- c30_api_para_ext_data_map
Insert into C30_API_PARA_EXT_DATA_MAP
   (ACCT_SEG_ID, API_NAME, API_PARAMETER_NAME, PARAM_ID)
 Values
   (4, 'c30addaccount', 'v_bill_period', -1);

  -- issue final commit and print dbms_output
  commit;
  c30_put_line(null,'commit;');
  c30_put_line(null,'Successfully completed script for 200_001_config_awn.sql');
exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/
-- the commit is aleady done in the script if successful...
exit rollback
