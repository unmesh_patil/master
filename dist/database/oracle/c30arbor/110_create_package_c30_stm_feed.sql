set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/110_create_package_c30_stm_feed_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 110_create_package_c30_stm_feed.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create c30_stm_feed package (spec) for R325955 Settlement Feed Process Improvement
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace package c30arbor.c30_stm_feed
is
-- Release Notes
-- Config (n/a)
-- Date: 09/12/2013
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: N/A
--
-- Script file Name: 110_create_package_c30_stm_feed.sql
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.0 Initial Creation for R325955
-- DETAILED EXPLANATION:
-- Create c30_stm_feed package (spec) for R325955 Settlement Feed Process Improvement

-- public variable declarations
   c_batch_max             CONSTANT pls_integer := 25000; -- we experimented with maximum batch size and this seemed most efficient
   c_batch_min             CONSTANT pls_integer := 500;   -- also set a minimum batch size
   c_num_batches           CONSTANT pls_integer := 5;     -- maximum number of batch (limited by the above)
   c_number_1              CONSTANT number(1)   := 1;     -- use in "where" clause to allow bind variable in place of hard-coded 1
   c_number_0              CONSTANT number(1)   := 0;     -- (see explanation above)
   c_number_3              CONSTANT number(1)   := 3;     -- (see explanation above)
   c_number_minus_2        CONSTANT number(1)   := -2;    -- (see explanation above)
   c_load_not_done         CONSTANT c30arbor.c30_settlement_feed_billed.load_cdr_data%type := 0;     -- set to 1 after row loaded
   c_eit_tn                CONSTANT arbor.customer_id_equip_map.external_id_type%type      := 60;    -- "GSM Wireless"
   c_eit_imsi              CONSTANT arbor.customer_id_equip_map.external_id_type%type      := 20015; -- "IMSI"

   d_put_line                       pls_integer;
   d_save_overflow                  boolean;
   d_no_dbms_output                 varchar2(1);
   d_file                           utl_file.file_type;
   d_acct_seg_id                    arbor.cmf.acct_seg_id%type;
   d_sql_line                       varchar2(4000);
   d_sqlerrm                        varchar2(2000);
   d_timestamp_start                timestamp;
   d_timestamp_stop                 timestamp;
   d_batch                          pls_integer; -- calculate batch size as count of key values divided by c_num_batches but no more than c_batch_max
   d_batch_count                    pls_integer; -- which batch is this?
   d_batch_start                    pls_integer; -- calc batch start index
   d_batch_stop                     pls_integer; -- calc batch stop index
   d_batch_errors                   pls_integer; -- count of any "forall" error[s]
   d_sql_rowcount                   pls_integer; -- save sql%rowcount from forall implied cursor
   d_rowcount                       pls_integer; -- used by the driver to call entry points
   d_rowcount_hold                  pls_integer; -- number of rows in table (for process control)
   d_index_exists                   number(1);   -- 0/1 switch when checking if an index exists

procedure c30_sf_driver(
   v_acct_seg_id           IN       number            -- required; account segment
  ,v_entry_point           IN       varchar2          -- required; name of the package entry point being called
  ,v_put_line              IN       pls_integer := 0  -- optional; 0=output to utl_file only; 1=output to utl_file and dbms_output; 2=output to dbms_output only
);

-- note, as a standard we will pass account segment to all entry points; it may not be used inside them...
procedure load_cdr_data(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
);

procedure load_acct_no(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
);

procedure load_unit_usage(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
);

procedure load_svc_plan(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
);

procedure generate_csv_files(
   v_acct_seg_id           IN       number
  ,v_rowcount              IN OUT   pls_integer
);

end c30_stm_feed;
/
show errors
-- note we do not run c30_ddl_standard until the create package body script

--spool off
exit rollback