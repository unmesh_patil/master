set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
-- spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/110_create_view_c30_instances_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Config (n/a)
prompt Date: 07-18-2011 
prompt Created By: Mark Doyle
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 110_create_view_c30_instances.sql
prompt Version: 1.0 Initial Creation for Kenan SQL API
prompt DETAILED EXPLANATION:
prompt Create c30_instances view
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
drop view c30arbor.c30_instances
;
create or replace view c30arbor.c30_instances as
with all_comps as 
(select 1 as is_host, instance_id, component_inst_id, component_inst_id_serv, primary_subscr_no as subscr_no, primary_subscr_no_resets as subscr_no_resets, is_active
 from c30arbor.c30_external_inst_id_def
 where is_active = 1
 union 
 select 0 as is_host, instance_id, component_inst_id, component_inst_id_serv, subscr_no, subscr_no_resets, is_active
 from c30arbor.c30_instance_id_member)
select ceiid.instance_type, ceiid.acct_seg_id, ceiid.external_inst_id, ceiid.catalog_id, a.* from all_comps a, c30arbor.c30_external_inst_id_def ceiid
where a.instance_id = ceiid.instance_id
and ceiid.is_active = 1
order by a.instance_id, a.is_host desc, a.component_inst_id
with read only;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_instances');
-- spool off
exit rollback