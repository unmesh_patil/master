set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/119_create_view_c30_stm_feed_grouped_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 119_create_view_c30_stm_feed_grouped.sql
prompt CA Ticket# : AWN
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_stm_feed_grouped view for Settlement Feed processing
prompt .. This is a "template" only!  It would be very inefficient to render this view without joining with
prompt .... some set of CDR_DATA primary key values msg_id+msg_id2+msg_id_serv[+split_row_num][+cdr_data_partition_key].
prompt .. Instead, it will be used to join against CDR_UNBILLED or CDR_BILLED to complete the associated feed.
prompt .... This prevents the same code from being duplicated in two separate view source code.
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 10/01/2012 ##  1.0 Initial Creation for AWN
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_stm_feed_grouped as
with
-- Release Notes
-- Config (n/a)
-- Date: 10/01/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: AWN
--
-- Script file Name: 119_create_view_c30_stm_feed_grouped.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 11/14/2012 ##  v1.1 Defect 279 - Remove "substr" from all lookup values and trim() when used instead.
--                                                Don't use rows where the active and inactive dates are the same.
--                                                Switch check on "unrounded_amount <> 0" from "having" to "where" clause
-- DETAILED EXPLANATION:
-- Create c30_stm_feed_grouped view for Settlement Feed processing
  acct_seg_values_expanded as(
select acct_seg_id
     , display_value
  from arbor.acct_seg_values
 where language_code = 1
)
, rate_currency_ref_expanded as(
select currency_code
     , implied_decimal
  from arbor.rate_currency_ref
 where     active_date             <= sysdate
   and nvl(inactive_date,sysdate+1) > sysdate
)
       -- 1st, select the "group by" columns...
select sfd.min_rate_dt
     , trim(iam.external_id)        account_no_desc --v1.1 use trim
     , cmf.acct_seg_id
     , trim(svx.display_value)      acct_seg_id_desc --v1.1 use trim
     , cdr.msg_id
     , cdr.msg_id2
     , cdr.msg_id_serv
     , cdr.cdr_data_partition_key
     , cdr.ext_tracking_id
     , cdr.element_id
     , cdr.type_id_usg
     , coalesce(lk1.bill_class,lk2.bill_class,lk3.bill_class,lk4.bill_class)
                                    bill_class
     , cdr.provider_id
     , coalesce(lk1.jurisdiction,lk2.jurisdiction,lk3.jurisdiction,lk4.jurisdiction)
                                    jurisdiction
     , cdr.account_no
     , cdr.subscr_no
     , cdr.subscr_no_resets
     , cdr.external_id
     , cdr.external_id_type
     , cdr.point_origin
     , cdr.country_code_origin
     , cdr.point_id_origin
     , cdr.point_target
     , cdr.country_code_target
     , cdr.point_id_target
     , cdr.rate_dt
     , cdr.billing_units_type
     , cdr.component_id
     , cdr.add_implied_decimal
     , trim(cdr.annotation)         annotation --v1.1 use trim
     , trim(cdr.customer_tag)       customer_tag --v1.1 use trim
     , cdr.seqnum_rate_usage
     , cdr.seqnum_rate_usage_overrides
     , crx.implied_decimal
     , rus.add_unit_rate/power(10,rus.add_implied_decimal)/power(10,crx.implied_decimal) -- converted using all additional or implied decimal places
                                    add_unit_rate
     , cdr.rerated_dt
     -- we might need to know which of these outer joins actually found a row...
     , substr(coalesce(case when lk1.acct_seg_id is not null then 'lk1' end
                      ,case when lk2.acct_seg_id is not null then 'lk2' end
                      ,case when lk3.acct_seg_id is not null then 'lk3' end
                      ,case when lk4.acct_seg_id is not null then 'lk4' end
                      ),1,3)        lk_key
     -- 2nd, select all columns that will need a grouping function...
     , min(cdr.split_row_num)       min_split_row_num
     , max(cdr.split_row_num)       max_split_row_num
     , min(cdr.bill_class)          min_bill_class
     , max(cdr.bill_class)          max_bill_class
     , min(cdr.jurisdiction)        min_jurisdiction
     , max(cdr.jurisdiction)        max_jurisdiction
     , max(cdr.trans_dt)            trans_dt
     , sum(cdr.primary_units)       primary_units
     , sum(cdr.rated_units)         rated_units
     -- (see NOTE above about these calcs)...
     , sum(cdr.unrounded_amount/power(10,cdr.add_implied_decimal)/power(10,crx.implied_decimal)) -- converted using all additional or implied decimal places
                                    unrounded_amount
     , count(*)                     cdr_rowcount
  from c30arbor.c30_stm_feed_dates sfd
  join arbor.cdr_data cdr
    on cdr.rate_dt     >= sfd.min_rate_dt
   and cdr.no_bill      = 0 -- billable
   and cdr.comp_status  = 1 -- normal
   and cdr.cdr_status   = 3 -- guided and rated
   and cdr.type_id_usg IN(select distinct fut.type_id_usg from c30arbor.c30_stm_feed_usage_type fut)
  join arbor.cmf cmf
    on     cmf.account_no                    = cdr.account_no
   -- this assures the joined row was active on the transaction date...
   and     cmf.date_active                  <= cdr.trans_dt
   and nvl(cmf.date_inactive,cdr.trans_dt+1) > cdr.trans_dt
   and nvl(cmf.date_inactive,sysdate+1)     <> cmf.date_active --v1.1 don't use rows where the active and inactive dates are the same
  join arbor.external_id_acct_map iam
    on     iam.account_no                    = cmf.account_no
   and     iam.external_id_type              = c30arbor.c30_acct_seg_sys_parameters_vl(cmf.acct_seg_id,'acct_no_ext_id_type')
   -- this assures the joined row was active on the transaction date...
   and     iam.active_date                  <= cdr.trans_dt
   and nvl(iam.inactive_date,cdr.trans_dt+1) > cdr.trans_dt
   and nvl(iam.inactive_date,sysdate+1)     <> iam.active_date --v1.1 don't use rows where the active and inactive dates are the same
  join acct_seg_values_expanded svx
    on cmf.acct_seg_id = svx.acct_seg_id
  join rate_currency_ref_expanded crx
    on crx.currency_code = cdr.rate_currency_code
  left outer join arbor.rate_usage rus
    on rus.seqnum = cdr.seqnum_rate_usage
  -- the c30_stm_feed_usage_type lookup table does NOT have the account segment inheritance
  -- the actual one has to be there -- it does not "defer" to a parent_acct_seg_id value
  -- lookup1 (lk1) = "type_id_usg, bill_class, and jurisdiction are not null"
  left outer join c30arbor.c30_stm_feed_usage_type lk1
    on     lk1.acct_seg_id                   = cmf.acct_seg_id
   and     lk1.type_id_usg                   = cdr.type_id_usg
   and     lk1.bill_class                    = cdr.bill_class
   and     lk1.jurisdiction                  = cdr.jurisdiction
   and     lk1.active_date                  <= cdr.trans_dt
   and nvl(lk1.inactive_date,cdr.trans_dt+1) > cdr.trans_dt
  -- lookup2 (lk2) = "jurisdiction is null"
  left outer join c30arbor.c30_stm_feed_usage_type lk2
    on     lk2.acct_seg_id                   = cmf.acct_seg_id
   and     lk2.type_id_usg                   = cdr.type_id_usg
   and     lk2.bill_class                    = cdr.bill_class
   and     lk2.jurisdiction                 is null
   and     lk2.active_date                  <= cdr.trans_dt
   and nvl(lk2.inactive_date,cdr.trans_dt+1) > cdr.trans_dt
  -- lookup3 (lk3) = "bill_class is null"
  left outer join c30arbor.c30_stm_feed_usage_type lk3
    on     lk3.acct_seg_id                   = cmf.acct_seg_id
   and     lk3.type_id_usg                   = cdr.type_id_usg
   and     lk3.bill_class                   is null
   and     lk3.jurisdiction                  = cdr.jurisdiction
   and     lk3.active_date                  <= cdr.trans_dt
   and nvl(lk3.inactive_date,cdr.trans_dt+1) > cdr.trans_dt
  -- lookup3 (lk4) = "bill_class and jurisdiction are null"
  left outer join c30arbor.c30_stm_feed_usage_type lk4
    on     lk4.acct_seg_id                   = cmf.acct_seg_id
   and     lk4.type_id_usg                   = cdr.type_id_usg
   and     lk4.bill_class                   is null
   and     lk4.jurisdiction                 is null
   and     lk4.active_date                  <= cdr.trans_dt
   and nvl(lk4.inactive_date,cdr.trans_dt+1) > cdr.trans_dt
 where cdr.unrounded_amount <> 0 --v1.1 switched from "having"
group by
       -- the "key" columns without split_row_num, the c30_stm_feed_usage_type lookup columns,
       --   and the others that should not vary from one split row to another...
       sfd.min_rate_dt
     , iam.external_id
     , cmf.acct_seg_id
     , svx.display_value
     , cdr.msg_id
     , cdr.msg_id2
     , cdr.msg_id_serv
     , cdr.cdr_data_partition_key
     , cdr.ext_tracking_id
     , cdr.element_id
     , cdr.type_id_usg
     , coalesce(lk1.bill_class,lk2.bill_class,lk3.bill_class,lk4.bill_class)
     , cdr.provider_id
     , coalesce(lk1.jurisdiction,lk2.jurisdiction,lk3.jurisdiction,lk4.jurisdiction)
     , cdr.account_no
     , cdr.subscr_no
     , cdr.subscr_no_resets
     , cdr.external_id
     , cdr.external_id_type
     , cdr.point_origin
     , cdr.country_code_origin
     , cdr.point_id_origin
     , cdr.point_target
     , cdr.country_code_target
     , cdr.point_id_target
     , cdr.rate_dt
     , cdr.billing_units_type
     , cdr.component_id
     , cdr.add_implied_decimal
     , cdr.annotation
     , cdr.customer_tag
     , cdr.seqnum_rate_usage
     , cdr.seqnum_rate_usage_overrides
     , crx.implied_decimal
     , rus.add_unit_rate
     , rus.add_implied_decimal
     , cdr.rerated_dt
     , lk1.acct_seg_id
     , lk2.acct_seg_id
     , lk3.acct_seg_id
     , lk4.acct_seg_id
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_stm_feed_grouped');
--...
--select count(*), sum(cdr_rowcount) cdr_rowcount from c30arbor.c30_stm_feed_grouped;
--prompt it would be odd to have any rows here...
--select msg_id, msg_id2, msg_id_serv, min_split_row_num, max_split_row_num, cdr_data_partition_key, count(*)
--  from c30arbor.c30_stm_feed_grouped
--group by msg_id, msg_id2, msg_id_serv, min_split_row_num, max_split_row_num, cdr_data_partition_key
--having count(*) > 1
--;
--prompt it would be odd to have any rows here...
--select count(*) from (
--select nvl(ext_tracking_id,'x'),account_no,subscr_no,trans_dt,point_origin,point_target,primary_units from c30arbor.c30_stm_feed_grouped
--group by nvl(ext_tracking_id,'x'),account_no,subscr_no,trans_dt,point_origin,point_target,primary_units
--having count(*) > 1 and min(nvl(rerated_dt,sysdate)) = max(nvl(rerated_dt,sysdate))
--);
--spool off
exit rollback