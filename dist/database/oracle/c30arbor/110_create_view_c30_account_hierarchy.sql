set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/110_create_view_c30_account_hierarchy_&_connect_identifier..txt
create or replace view c30arbor.c30_account_hierarchy as
select
-- Release Notes
-- Config (n/a)
-- Date: 08/24/2012
-- Created By: Chip H.F. LaFurney
-- Change Request: (n/a per John H.)
--
-- Script file Name: 110_create_view_c30_account_hierarchy.sql
-- Version: 1.0 Re-created for K3 - Chip H.F. LaFurney 08/24/2012
-- DETAILED EXPLANATION
--    I create a view of instantiated data for an account.  (110 numbering since it used [105_create_view_]cmf_full)
--    Really should be used with an account number, this dumps out these parts of its hierachies:
--       1) CMF
--       2) CMF_PACKAGE
--       3) CMF_PACKAGE_COMPONENT
--       4) CMF_COMPONENT_ELEMENT
       --
       --********
       -- Account
       --********
       cmf.account_no
     , cmf.account_no_desc
     , cmf.parent_id
     , cmf.hierarchy_id
     , cmf.account_category
     , cmf.account_category_desc
     , cmf.prev_cutoff_date
     , cmf.bill_name
     , cmf.cust_address
     , cmf.bill_address
     , cmf.rate_class_default
     , cmf.rate_class_default_desc
     , cmf.account_status
     , cmf.account_status_desc
     , cmf.bill_period
     , cmf.bill_period_desc
     , cmf.acct_seg_id
     , cmf.acct_seg_id_desc
       --
       --********
       -- Package
       --********
     , cpk.package_inst_id
     , cpk.package_inst_id_serv
     , cpk.package_id
     -- duplicate so we don't have to worry about typo'ing our SQL query...
     , cpk.parent_account_no
     , case when cpk.package_id is not null then
         substr(pdv.display_value,1,90)
       end                                                           package_desc
     , case when cpk.package_id is not null then
         substr(pdv.display_value||' ('||cpk.package_id||')',1,120)
       end                                                           package_name
     , cpk.package_status
     , cpk.active_dt                                                 cpk_active_dt
     , cpk.inactive_dt                                               cpk_inactive_dt
       --
       --**********
       -- Component
       --**********
     , cpc.component_inst_id
     , cpc.component_inst_id_serv
     , cpc.component_id
     , case when cpc.component_id is not null then
         substr(cdv.display_value,1,90)
       end                                                           component_desc
     , case when cpc.component_id is not null then
         substr(cdv.display_value||' ('||cpc.component_id||')',1,120)
       end                                                           component_name
     , cpc.parent_subscr_no
     , cpc.parent_subscr_no_resets
     -- duplicates so we don't have to worry about typo'ing our SQL query...
     , cpc.parent_subscr_no                                          subscr_no
     , cpc.parent_subscr_no_resets                                   subscr_no_resets
     , iem.external_id
     , cpc.component_status
     , cpc.active_dt                                                 cpc_active_dt
     , cpc.inactive_dt                                               cpc_inactive_dt
       --
       --************
       -- Association
       --************
     , cce.association_id
     , cce.association_id_serv
     , cce.association_type
     , cce.component_element_status
     , cce.active_dt                                                 cce_active_dt
     , cce.inactive_dt                                               cce_inactive_dt
       --
       --*********
       -- Contract
       --*********
     , cct.tracking_id                                               cct_tracking_id
     , cct.tracking_id_serv                                          cct_tracking_id_serv
     , cct.contract_type
     , cct.contract_id
     , cct.contract_level
     , cct.owning_account_no
     , cct.parent_group_no
     , cct.start_dt
     , cct.end_dt
     , cct.element_id                                                cct_element_id
     , cct.commitment_reference
     , cct.view_id                                                   cct_view_id
       --
       --********
       -- Product
       --********
     , prd.tracking_id                                               prd_tracking_id
     , prd.tracking_id_serv                                          prd_tracking_id_serv
     , prd.billing_account_no
     , prd.billing_active_dt
     , prd.billing_inactive_dt
     , prd.chg_who
     , prd.chg_dt
     , prd.element_id
     , prd.product_active_dt
     , prd.product_inactive_dt
     , prd.contract_tracking_id
     , prd.contract_tracking_id_serv
     , prd.view_id                                                   prd_view_id
       --
       --*******************
       -- Product Charge Map
       --*******************
     , pcm.billed_thru_dt
     , pcm.no_bill
     , pcm.active_dt                                                 pcm_active_dt
     , pcm.inactive_dt                                               pcm_inactive_dt
       --
       --********************
       -- Summary level stuff
       --********************
       -- "Active" bucket excludes statuses, contract end_dt, and product dates
     , case when     cmf.account_status    not IN(-2,-3)
             and nvl(cpk.inactive_dt,sysdate+1) > sysdate
             and nvl(cpc.inactive_dt,sysdate+1) > sysdate
             and nvl(cce.inactive_dt,sysdate+1) > sysdate then
               1
            else
               0
       end                                                           active
       -- A "WARNING" column to show when it "active" above but the contract or product has expired
     , case when     cmf.account_status             not IN(-2,-3)
             and nvl(cpk.inactive_dt,sysdate+1)          > sysdate
             and nvl(cpc.inactive_dt,sysdate+1)          > sysdate
             and nvl(cce.inactive_dt,sysdate+1)          > sysdate
             -- so far, "active", but one of the other indicators says "inactive"...
             and(nvl(cct.end_dt,sysdate+1)              <= sysdate
              or nvl(prd.billing_inactive_dt,sysdate+1) <= sysdate
              or nvl(prd.product_inactive_dt,sysdate+1) <= sysdate
              or nvl(pcm.inactive_dt,sysdate+1)         <= sysdate
                ) then
               'WARNING'
       end                                                           warning
     -- null active date are set to today so they won't count...
     -- null inactive date cannot be the "least"!
     , trunc(least   (            cpk.active_dt
                     ,            cpc.active_dt
                     ,            cce.active_dt
                     ,nvl(prd.billing_active_dt,sysdate)
                     ,nvl(prd.product_active_dt,sysdate)
                     ,nvl(        pcm.active_dt,sysdate)
            )        )                                               first_change_date
     -- null dates are set way low so they won't count...
     , trunc(greatest(            cpk.active_dt             ,nvl(        cpk.inactive_dt,'01-jan-70')
                     ,            cpc.active_dt             ,nvl(        cpc.inactive_dt,'01-jan-70')
                     ,            cce.active_dt             ,nvl(        cce.inactive_dt,'01-jan-70')
                     -- reinterpreted as the last change in the hierarchy, not by humans ,nvl(           prd.chg_dt,'01-jan-70')
                     ,nvl(prd.billing_active_dt,'01-jan-70'),nvl(prd.billing_inactive_dt,'01-jan-70')
                     ,nvl(prd.product_active_dt,'01-jan-70'),nvl(prd.product_inactive_dt,'01-jan-70')
                     ,nvl(        pcm.active_dt,'01-jan-70'),nvl(        pcm.inactive_dt,'01-jan-70')
            )        )                                               last_change_date
  from c30arbor.cmf_full cmf
  left outer join arbor.cmf_package_component cpc
    on cpc.parent_account_no IN(cmf.account_no,cmf.parent_id)
  left outer join arbor.customer_id_equip_map iem
    on     iem.subscr_no                 =     cpc.parent_subscr_no
   and     iem.subscr_no_resets          =     cpc.parent_subscr_no_resets
   and     iem.external_id_type          =     3 -- "MSISDN"
   -- has to be true for this child to be "related" to parent
   and     iem.active_date              <= nvl(cpc.inactive_dt,sysdate+1)
   and nvl(iem.inactive_date,sysdate+1)  >     cpc.active_dt
  left outer join arbor.package_definition_values pdv
    on pdv.package_id    = cpc.package_id
   and pdv.language_code = 1
  left outer join arbor.component_definition_values cdv
    on cdv.component_id  = cpc.component_id
   and cdv.language_code = 1
  left outer join arbor.cmf_package cpk
    on cpk.package_inst_id      = cpc.package_inst_id
   and cpk.package_inst_id_serv = cpc.package_inst_id_serv
  left outer join arbor.cmf_component_element cce
    on cce.component_inst_id      = cpc.component_inst_id
   and cce.component_inst_id_serv = cpc.component_inst_id_serv
   and cce.package_inst_id        = cpc.package_inst_id
   and cce.package_inst_id_serv   = cpc.package_inst_id_serv
  left outer join arbor.customer_contract cct
    on cct.tracking_id      = cce.association_id
   and cct.tracking_id_serv = cce.association_id_serv
   and cce.association_type = 2
  left outer join arbor.product prd
    on((prd.tracking_id               = cce.association_id
    and prd.tracking_id_serv          = cce.association_id_serv
    and cce.association_type         IN(0,1)
       )
    or (prd.contract_tracking_id      = cct.tracking_id
    and prd.contract_tracking_id_serv = cct.tracking_id_serv
    and cce.association_type          = 2
       )
      )
  left outer join arbor.product_charge_map pcm
    on pcm.tracking_id        = prd.tracking_id
   and pcm.tracking_id_serv   = prd.tracking_id_serv
   and(pcm.parent_account_no  = prd.parent_account_no
    or pcm.billing_account_no = prd.billing_account_no)
with read only
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_account_hierarchy');
--spool off
exit rollback