set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/102_create_procedure_c30writeTransactionLog_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 102_create_procedure_c30writeTransactionLog.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt
prompt Purpose of Script: Create c30writeTransactionLog procedure
prompt NOTE ON "102" NUMBERING - It's a low number because any KSA object should be able to call this
prompt .. 105 proc c30setCtxParameters calls 103 package c30_ctx (BODY) calls **THIS 102 PROC** calls 102 proc c30insertTransaction and 102 package c30_ctx (SPEC)
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 11/17/2011 ##  Initial Creation for Kenan SQL API
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace procedure c30arbor.c30writeTransactionLog(
   -- *all optional; note the "p" prefix for these parameters to avoid name confusion within the many calling routines
   p_log_msg                  c30arbor.c30_transaction_log.log_msg%type                := '(none)'
  ,p_log_type                 c30arbor.c30_transaction_log.log_type%type               := 'info'
  ,p_transaction_id           c30arbor.c30_transaction_log.transaction_id%type         := null
  ,p_acct_seg_id              c30arbor.c30_transaction_log.acct_seg_id%type            := null
  ,p_account_no               c30arbor.c30_transaction_log.account_no%type             := null
  ,p_subscr_no                c30arbor.c30_transaction_log.subscr_no%type              := null
  ,p_subscr_no_resets         c30arbor.c30_transaction_log.subscr_no_resets%type       := null
  ,p_bill_ref_no              c30arbor.c30_transaction_log.bill_ref_no%type            := null
  ,p_bill_ref_resets          c30arbor.c30_transaction_log.bill_ref_resets%type        := null
  ,p_package_inst_id          c30arbor.c30_transaction_log.package_inst_id%type        := null
  ,p_package_inst_id_serv     c30arbor.c30_transaction_log.package_inst_id_serv%type   := null
  ,p_package_id               c30arbor.c30_transaction_log.package_id%type             := null
  ,p_component_inst_id        c30arbor.c30_transaction_log.component_inst_id%type      := null
  ,p_component_inst_id_serv   c30arbor.c30_transaction_log.component_inst_id_serv%type := null
  ,p_component_id             c30arbor.c30_transaction_log.component_id%type           := null
  ,p_tracking_id              c30arbor.c30_transaction_log.tracking_id%type            := null
  ,p_tracking_id_serv         c30arbor.c30_transaction_log.tracking_id_serv%type       := null
  ,p_force_id                 c30arbor.c30_transaction_log.force_id%type               := null
)
is
-- Release Notes
-- Config (n/a)
-- Date: 11-17-2011
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 102_create_procedure_c30writeTransactionLog.sql
-- Version: 1.0 Initial Creation for Kenan SQL API
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 03/26/2012 ##  v1.1 New Kenan SQL API changes; added p_transaction_id, p_package_id, p_component_id and p_force_id
--                                                     removed PRAGMA AUTONOMOUS_TRANSACTION so this can issue "rollback"
--                                                     now calling c30insertTransactionLog which **IS** the autonomous transaction to insert log row
-- chip.lafurney@cycle30.com    ## 04/26/2012 ##  v1.2 Store transaction_id, acct_seg_id and/or force_id if they are non-null;
--                                                     added logic to avoid issuing "rollback" when called by a trigger
-- chip.lafurney@cycle30.com    ## 05/07/2012 ##  v1.3 Defect 451 - (For the most part, reversing prior changes in earlier v1.3);
--                                                     new value for debug_switch -- "debug_verbose" log types to be stored/displayed except in UAT or PROD
--                                                     (pulled "debug_switch" logic from c30setCtxParameters, now that context value is only handled here)
--                                                     (reverse change in v1.2) DO NOT use the "trio" of context values when writing log
-- chip.lafurney@cycle30.com    ## 07/12/2012 ##  v1.4 Stop sending messages about address errors since the users of the system are having trouble entering a good one
-- chip.lafurney@cycle30.com    ## 09/06/2012 ##  v1.5 Improved above logic by installing c30_get_acct_seg_value function and c30_sys_parameters_def table
-- chip.lafurney@cycle30.com    ## 09/17/2012 ##  v1.6 De-installed c30_get_acct_seg_value function and installed c30_acct_seg_sys_parameters_vl function instead
-- chip.lafurney@cycle30.com    ## 09/27/2012 ##  v1.7 Changed logic about the default debug_switch setting; expand "issued rollback" log message
-- DETAILED EXPLANATION:
-- Create c30writeTransactionLog procedure
-- *The idea for the calling parameters is you could call this with no parameters at all.  That would create an
--    "info" transaction with all of the session information, kind of a "Hello World!" for any object.
  l_log_type            c30arbor.c30_transaction_log.log_type%type := lower(trim(nvl(p_log_type,'info')));
  l_log_type_found      Boolean := false;
  l_body                varchar2(4000);
  l_msg                 varchar2(4000);
  l_nl                  varchar2(2) := chr(10); -- "newline" - either UNIX "chr(10)" (default) or Windows "chr(13)||chr(10)"
  l_instance_name       v$instance.instance_name%type; --v1.3 added
  l_debug_switch        pls_integer;                   --v1.3 added

begin
--**!!c30_put_line(null,chr(10)||rpad('c30writeTransactionLog (CWTL) ',100,'-'));
--v1.3 get a number into l_debug_switch (pulled "debug_switch" logic from c30setCtxParameters, now that context value is only handled here)...
/* Note: I pondered the inefficiency of reading the table "debug_switch" value every time...
         1) we could set up a "debug_switch_date" context value and use "aging" logic (e.g., "Is it more than 2 minutes old?")
         2) we could assume it rarely changes and if it's non-null, use the value
         3) we could just not worry about it
   I decided for now to use the existing value when these were already in the context...

Also note having debugging statements here is a LOT of output so they're commented out "[DashDash]DB if l_debug_switch >= 2"
*/
if           c30_sys_param('debug_switch')                                    is not null
and          c30_sys_param('acct_seg_id')                                     is not null
and coalesce(c30_sys_param('kenan_username'),c30_sys_param('force_username')) is not null then
  l_debug_switch := abs(to_number(c30_sys_param('debug_switch')));
  l_msg  := 'using l_debug_switch='||nvl(to_char(l_debug_switch),'<null>')||'; (ctx)acct_seg_id='||c30_sys_param('acct_seg_id')
                                               ||'; (ctx)kenan_username="'||c30_sys_param('kenan_username')||'"'
                                               ||'; (ctx)force_username="'||c30_sys_param('force_username')||'"';
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
else
  begin
  --v1.5 Improved above logic by installing c30_get_acct_seg_value function...
  --v1.6 De-installed c30_get_acct_seg_value function and installed c30_acct_seg_sys_parameters_vl function instead...
  l_debug_switch := to_number(c30arbor.c30_acct_seg_sys_parameters_vl(coalesce(p_acct_seg_id,to_number(c30_sys_param('acct_seg_id')),-909),'debug_switch'));

  -- if we get here then a) we have a real value for acct_seg_id and b) it (or its parent) has a value for debug_switch...
  if c30_sys_param('acct_seg_id') is null then
    l_msg  := 'default l_debug_switch (no account segment)='||nvl(to_char(l_debug_switch),'<null>');
    l_body := l_body||l_msg||l_nl||l_nl;
    --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
  else
    -- do not set the "debug_switch" context value unless the above criteria are met...
    if coalesce(c30_sys_param('kenan_username'),c30_sys_param('force_username')) is not null then
      c30arbor.c30_ctx.set_parameter('debug_switch',l_debug_switch);
      l_msg  := 'set context debug_switch='||nvl(to_char(l_debug_switch),'<null>');
      l_body := l_body||l_msg||l_nl||l_nl;
      --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
    end if;
  end if;

  exception
    when no_data_found then
      l_debug_switch := null; --v1.7 handle below
  end;
end if;

--v1.7 deal with null debug switch here...
if l_debug_switch is null then
  l_msg  := 'setting default l_debug_switch';
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
  --v1.5 Improved above logic by installing...c30_sys_parameters_def table...
  begin
  select abs(to_number(default_value))
    into l_debug_switch
    from c30arbor.c30_sys_parameters_def
   where parameter_name = 'debug_switch'
  ;
  l_msg  := 'selected default l_debug_switch='||l_debug_switch;
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
  -- send_email... c30_send_email('Cycle30KSADatabaseNotifications@cycle30.com','From c30writeTransactionLog - "'||l_log_type||'"',l_body);
  exception
    when no_data_found then
      l_debug_switch := 3; --v1.4 default to debug level 3
      l_msg  := 'defaulted l_debug_switch='||l_debug_switch;
      l_body := l_body||l_msg||l_nl||l_nl;
      --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
      --v1.4 pull this c30_send_email('Cycle30KSADatabaseNotifications@cycle30.com','From c30writeTransactionLog - "'||l_log_type||'"',l_body);
  end;
end if;

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--v1.3 0="no debug", 1=insert transaction log only, 2=Serveroutput only, 3=Both, >3 verbose output even in UAT or PROD
--     "debug_verbose" is "on" for debug_switch values 2 or 3 (except in UAT or PROD); always on for values >3
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
if l_log_type IN('info','error') then
  if l_debug_switch = 0 then
    -- bump l_debug_switch to 1 for "info" and "error" log types to make sure they're written to the log table...
    l_debug_switch := 1;
  end if;
elsif l_log_type = 'debug' then
  if l_debug_switch = 0 then
    -- for "debug" there's nothing to do unless l_debug_switch > 0...
    --**!!c30_put_line(null,'c30writeTransactionLog "debug" instant "return"; l_debug_switch='||l_debug_switch);
    return;
  end if;
elsif l_log_type = 'debug_verbose' then
  --v1.3 new value for debug_switch -- "debug_verbose" log types to be stored/displayed except in UAT or PROD
  if c30_sys_param('instance_name') is null then
    select v.instance_name
      into l_instance_name
      from v$instance v
    ;
    c30arbor.c30_ctx.set_parameter('instance_name',l_instance_name);
    l_msg  := 'set context instance_name='||l_instance_name;
    l_body := l_body||l_msg||l_nl||l_nl;
    --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
  else
    l_instance_name := c30_sys_param('instance_name');
  end if;

  if l_debug_switch <= 1
  or(l_instance_name like '%UAT%' or l_instance_name like '%PRD%' or l_instance_name like '%PROD%') then
    -- for "debug_verbose" there's nothing to do unless l_debug_switch > 1 and we're not in UAT or PROD...
    --**!!c30_put_line(null,'c30writeTransactionLog "debug_verbose" instant "return"; l_debug_switch='||l_debug_switch||'; l_instance_name='||l_instance_name);
    return;
  end if;
else
  --v1.3 turns out these log type tests replace the original validation logic...
  raise_application_error(-20000,'C30KSAWTL001 Invalid log type="'||p_log_type||'" (must be "info", "error", "debug" or "debug_verbose")'||l_nl||'"'||p_log_msg||'"');
end if;

l_msg  := '**** c30writeTransactionLog procedure ****';
l_body := l_body||l_msg||l_nl||l_nl;
l_msg  := 'p_log_msg="'||p_log_msg||'"'
  ||case when p_transaction_id    is not null then l_nl||'p_transaction_id='        ||p_transaction_id                                   end
  ||case when p_acct_seg_id       is not null then l_nl||'p_acct_seg_id='           ||p_acct_seg_id                                      end
  ||case when p_account_no        is not null then l_nl||'p_account_no='            ||p_account_no                                       end
  ||case when p_subscr_no         is not null then l_nl||'p_subscr_no/resets='      ||p_subscr_no        ||'/'||p_subscr_no_resets       end
  ||case when p_bill_ref_no       is not null then l_nl||'p_bill_ref_no/resets='    ||p_bill_ref_no      ||'/'||p_bill_ref_resets        end
  ||case when p_package_inst_id   is not null then l_nl||'p_package_inst_id/serv='  ||p_package_inst_id  ||'/'||p_package_inst_id_serv   end
  ||case when p_package_id        is not null then l_nl||'p_package_id='            ||p_package_id                                       end
  ||case when p_component_inst_id is not null then l_nl||'p_component_inst_id/serv='||p_component_inst_id||'/'||p_component_inst_id_serv end
  ||case when p_component_id      is not null then l_nl||'p_component_id='          ||p_component_id                                     end
  ||case when p_tracking_id       is not null then l_nl||'p_tracking_id/serv='      ||p_tracking_id      ||'/'||p_tracking_id_serv       end
  ||case when p_force_id          is not null then l_nl||'p_force_id='              ||p_force_id                                         end
;
l_body := l_body||l_msg||l_nl||l_nl;
--DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;

--v1.2 if either of these is non-null, store them in the context (if needed)...
l_msg  := 'testing p_transaction_id='||p_transaction_id||' vs c30_sys_param(''transaction_id'')='||c30_sys_param('transaction_id');
l_body := l_body||l_msg||l_nl||l_nl;
--DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;

if  p_transaction_id is not null
and p_transaction_id <> nvl(c30_sys_param('transaction_id'),'x') then
  l_msg  := 'p_transaction_id='||p_transaction_id;
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
  c30arbor.c30_ctx.set_parameter('transaction_id',p_transaction_id);
  l_msg  := 'set context transaction_id='||p_transaction_id;
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
end if;

l_msg  := 'testing p_force_id='||p_force_id||' vs c30_sys_param(''force_id'')='||c30_sys_param('force_id');
l_body := l_body||l_msg||l_nl||l_nl;
--DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;

if  p_force_id is not null
and p_force_id <> nvl(c30_sys_param('force_id'),'x') then
  l_msg  := 'p_force_id='||p_force_id;
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
  c30arbor.c30_ctx.set_parameter('force_id',p_force_id);
  l_msg  := 'set context force_id='||p_force_id;
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
end if;

-- unlike the above, the c30_ctx_set_parameter procedure sets the 'acct_seg_id' context (along with its related parameters)
-- so we should not set the context value for that here

-- non-"debug" log types are always stored; "debug" log types only if the debug_switch is on...
--v1.3 (see manipulations of debug switch at beginning of procedure)
if l_debug_switch > 0 then
  if l_debug_switch <> 2 then
    -- this procedure is an autonomous transation...
    c30insertTransactionLog(p_log_msg,l_log_type,p_transaction_id,p_acct_seg_id,p_account_no,p_subscr_no,p_subscr_no_resets
                           ,p_bill_ref_no,p_bill_ref_resets,p_package_inst_id,p_package_inst_id_serv,p_package_id,p_component_inst_id,p_component_inst_id_serv
                           ,p_component_id,p_tracking_id,p_tracking_id_serv,p_force_id);
    l_msg  := 'c30insertTransactionLog #1!';
    l_body := l_body||l_msg||l_nl||l_nl;
    --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;
  end if;

  --DB if l_debug_switch >= 2 then c30_put_line(null,p_log_msg||' ('||l_log_type||')'); end if;
end if;

-- this is where all errors in the Kenan SQL API are handled
if l_log_type = 'error' then
  rollback;
  -- this procedure is an autonomous transation...
  c30insertTransactionLog('c30writeTransactionLog issued rollback; check FORMAT_ERROR_BACKTRACE here for actual error line',l_log_type,p_transaction_id,p_acct_seg_id,p_account_no,p_subscr_no,p_subscr_no_resets
                         ,p_bill_ref_no,p_bill_ref_resets,p_package_inst_id,p_package_inst_id_serv,p_package_id,p_component_inst_id,p_component_inst_id_serv
                         ,p_component_id,p_tracking_id,p_tracking_id_serv,p_force_id);
  l_msg  := 'c30insertTransactionLog #2!';
  l_body := l_body||l_msg||l_nl||l_nl;
  --DB if l_debug_switch >= 2 then c30_put_line(null,'(CWTL) '||l_msg); end if;

  -- FOR TESTING ONLY (we don't want emails when it's just internal testing, hence the exclusions)...
  if  p_log_msg                             like '%ORA-%'
  -- we don't want KSA errors unless they're from "when others" ("999 ")
  and(p_log_msg                         not like 'C30KSA%'
   or p_log_msg                             like '%999 %'
     )
  --v1.4 Stop sending messages about address errors since the users of the system are having trouble entering a good one
  and p_log_msg                         not like 'C30KSAAPN999 ERROR: ORA-20040: Postal_code%'
  and p_log_msg                         not like 'C30KSAAPN999 ERROR: ORA-02291: integrity constraint (ARBOR.MASTER_ADDRESS_MD_PRED_FK) violated%'
  and c30_sys_param('kenan_username')         is not null
  and sys_context('userenv','terminal')       <> 'MDOYLE-C30'
  and sys_context('userenv','terminal') not like 'LT-F%'
  and sys_context('userenv','os_user')  not   IN('MDoyle','TPC-CLafurney','mark') then
    -- send_email; v1.3 new email group for this...
    c30_send_email('Cycle30KSADatabaseNotifications@cycle30.com','From c30writeTransactionLog - "'||l_log_type||'"',l_body);
  end if;

  raise_application_error(-20001,p_log_msg);
end if;

end c30writeTransactionLog;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30writeTransactionLog');
--spool off
exit rollback