set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/100_create_table_c30_sys_parameters_def&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 101_create_table_c30_sys_parameters_def.sql
prompt CA Ticket# : Kenan SQL API 
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_sys_parameters_def table
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: "drop table" may return Oracle error "...table or view does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt mark.doyle@cycle30.com       ## 08/15/2011 ##  Initial Creation for Kenan SQL API
prompt chip.lafurney@cycle30.com    ## 05/07/2012 ##  v1.1 Set up account segment system paramter and default values
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
whenever sqlerror continue
drop table c30arbor.c30_sys_parameters_def cascade constraints purge
;
create table c30arbor.c30_sys_parameters_def(
  parameter_name            varchar2(64)    not null,
  default_value             varchar2(64)
  )
;
create unique index c30arbor.c30_sys_parameters_def_pk
  on c30arbor.c30_sys_parameters_def(parameter_name)
;
alter table c30arbor.c30_sys_parameters_def add(
  constraint c30_sys_parameters_def_pk
  primary key(parameter_name)
  using index c30arbor.c30_sys_parameters_def_pk)
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_sys_parameters_def');
-- v1.1 added...
begin
-- C30_SYS_PARAMETERS_DEF 
insert into c30arbor.c30_sys_parameters_def(parameter_name, default_value)
                                          values('acct_no_ext_id_type', null);
insert into c30arbor.c30_sys_parameters_def(parameter_name, default_value)
                                          values('debug_switch', '3');
insert into c30arbor.c30_sys_parameters_def(parameter_name, default_value)
                                          values('serv_currency_code', 'use_account');
insert into c30arbor.c30_sys_parameters_def(parameter_name, default_value)
                                          values('serv_rate_class', 'use_account');
insert into c30arbor.c30_sys_parameters_def(parameter_name, default_value)
                                          values('serv_rev_rcv_cost_ctr', 'use_account');
insert into c30arbor.c30_sys_parameters_def(parameter_name, default_value)
                                          values('segment_package_id', null);
c30_put_line(null,'Inserted 5 c30_sys_parameters_def rows');

commit;
c30_put_line(null,'commit;');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/
--spool off
exit rollback