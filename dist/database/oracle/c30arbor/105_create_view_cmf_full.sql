set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/105_create_view_cmf_full_&_connect_identifier..txt
whenever sqlerror continue
drop view c30arbor.cmf_full
;
create or replace view c30arbor.cmf_full as
with
-- Release Notes
-- Config (n/a)
-- Date: 08/24/2012
-- Created By: Chip H.F. LaFurney
-- Change Request: (n/a per John H.)
--
-- Script file Name: 105_create_view_cmf_full.sql
-- Version: 1.0 Re-created for K3 - Chip H.F. LaFurney 08/24/2012
-- DETAILED EXPLANATION
--    I create a "full" view into the cmf table.  (105 numbering since it used [101_create_function_]c30_acct_seg_sys_parameters_vl)
--    This is part of the new FULL view series to expand all look-up values into description columns.
--
--    While the purpose of this view is (for the most part) to translate codes to descriptions, choices
--    were made to exclude many.  This was usually because the lookup table was empty or had one value...
--       currency_code     always 1 "US Dollars"
--       msg_grp_id        always 1 "Standard"
--       insert_grp_id     always 1 "Standard Insert Group"
--       exrate_class      always 1 "Exchange Rate 1"
--       mkt_code          always 1 "Alaska"
--       owning_cost_ctr   always 1 "Owning-Cost-Center"
--
--    These are "long" columns [length varchar2(60) or more] but are included in functions, below
--    (hence, no "short" version of them)...
--       bill_fname, bill_minit, bill_lname, bill_title, bill_company
--       cust_address1, cust_address2, cust_address3, cust_city, cust_state
--       bill_address1, bill_address2, bill_address3, bill_city, bill_state
--
--    These we just don't appear to use...
--       cust_county, bill_county, plan_id_discount, plan_id_credit, codeword,
--       alt_lname, alt_fname, alt_company_name, dept, title
--
  language_code_xpanded as(
select input_language
     , substr(display_value,1,30)                                 language_code_desc
     , decode(display_value,null,null,substr(display_value||' ('||input_language||')',1,34))
                                                                  language_code_name
  from arbor.language_code_values)
, account_category_xpanded as(
select account_category
     , substr(display_value,1,13)                                 account_category_desc
     , decode(display_value,null,null,substr(display_value||' ('||account_category||')',1,18))
                                                                  account_category_name
  from arbor.account_category_values
 where language_code = 1)
, disconnect_reason_xpanded as(
select disconnect_reason
     , substr(display_value,1,54)                                 disconnect_reason_desc
     , decode(display_value,null,null,substr(display_value||' ('||disconnect_reason||')',1,60))
                                                                  disconnect_reason_name
  from arbor.disconnect_reason_values)
, bill_period_xpanded as(
select bill_period
     , substr(display_value,1,26)                                 bill_period_desc
     , decode(display_value,null,null,substr(display_value||' ('||bill_period||')',1,32))
                                                                  bill_period_name
  from arbor.bill_period_values
 where language_code = 1)
, billing_frequency_xpanded as(
select billing_frequency
     , substr(display_value,1,10)                                 billing_frequency_desc
     , decode(display_value,null,null,substr(display_value||' ('||billing_frequency||')',1,14))
                                                                  billing_frequency_name
  from arbor.billing_frequency_values)
, bill_fmt_opt_xpanded as(
select bill_fmt_opt
     , substr(display_value,1,37)                                 bill_fmt_opt_desc
     , decode(display_value,null,null,substr(display_value||' ('||bill_fmt_opt||')',1,41))
                                                                  bill_fmt_opt_name
  from arbor.bill_fmt_opt_values)
, bill_disp_meth_xpanded as(
select bill_disp_meth
     , substr(display_value,1,10)                                 bill_disp_meth_desc
     , decode(display_value,null,null,substr(display_value||' ('||bill_disp_meth||')',1,14))
                                                                  bill_disp_meth_name
  from arbor.bill_disp_meth_values)
, rate_class_default_xpanded as(
select rate_class rate_class_default
     , substr(describe(description_code),1,25)                    rate_class_default_desc
     , decode(description_code,null,null,substr(describe(description_code,1),1,31))
                                                                  rate_class_default_name
  from arbor.rate_class_descr)
, vip_code_xpanded as(
select vip_code
     , substr(display_value,1,25)                                 vip_code_desc
     , decode(display_value,null,null,substr(display_value||' ('||vip_code||')',1,30))
                                                                  vip_code_name
  from arbor.vip_code_values)
, rev_rcv_cost_center_xpanded as(
select rev_rcv_cost_ctr
     , substr(display_value,1,30)                                 rev_rcv_cost_ctr_desc
     , decode(display_value,null,null,substr(display_value||' ('||rev_rcv_cost_ctr||')',1,35))
                                                                  rev_rcv_cost_ctr_name
  from arbor.rev_rcv_cost_center_values)
, acct_seg_xpanded as(
select acct_seg_id
     , substr(acct_seg_descr,1,35)                                acct_seg_id_desc
     , decode(acct_seg_descr,null,null,substr(acct_seg_descr||' ('||acct_seg_id||')',1,40))
                                                                  acct_seg_id_name
  from c30arbor.c30_acct_seg_def)
select cmf.* -- first, select all columns from the master table
      -- then, get "desc"riptions, name columns ["desc (value)"], or "short" version of the long text columns...
      --    (all lengths specified are based on actual data)
     , substr(iam.external_id,1,15)                               account_no_desc -- the heading is 15 chars; max length on file is 12
     , lcx.language_code_desc
     , lcx.language_code_name
     , acx.account_category_desc
     , acx.account_category_name
     , substr(
       full_name(
          cmf.bill_name_pre
         ,cmf.bill_fname
         ,cmf.bill_minit
         ,cmf.bill_lname
         ,cmf.bill_name_generation
         ,cmf.bill_title
         ,cmf.bill_company
         ,70 -- simply specifying the "70" parameter doesn't change column length, hence "substr"
         ,null
         ,'x')
       ,1,70)                                                     bill_name
     , substr(
       full_address(
          cmf.cust_address1
         ,cmf.cust_address2
         ,cmf.cust_address3
         ,cmf.cust_city
         ,cmf.cust_state
         ,cmf.cust_zip
         ,cmf.cust_country_code
         ,100 -- simply specifying the "100" parameter doesn't change column length, hence "substr"
         ,null
         ,'x')
       ,1,100)                                                    cust_address
     , substr(
       full_address(
          cmf.bill_address1
         ,cmf.bill_address2
         ,cmf.bill_address3
         ,cmf.bill_city
         ,cmf.bill_state
         ,cmf.bill_zip
         ,cmf.bill_country_code
         ,100 -- simply specifying the "100" parameter doesn't change column length, hence "substr"
         ,null
         ,'x')
       ,1,100)                                                    bill_address
     , nvl(case when length(trim(cmf.contact1_name)) > 50 then
               substr(trim(cmf.contact1_name),1,47)||'...'
            else
               substr(trim(cmf.contact1_name),1,50)
       end,null)                                                  contact1_name_short
     , nvl(case when length(trim(cmf.contact2_name)) > 50 then
               substr(trim(cmf.contact2_name),1,47)||'...'
            else
               substr(trim(cmf.contact2_name),1,50)
       end,null)                                                  contact2_name_short
     , drx.disconnect_reason_desc
     , drx.disconnect_reason_name
     -- (note K2 does not have pay_method)
     , bpx.bill_period_desc
     , bpx.bill_period_name
     , blx.billing_frequency_desc
     , blx.billing_frequency_name
     , bfx.bill_fmt_opt_desc
     , bfx.bill_fmt_opt_name
     , bdx.bill_disp_meth_desc
     , bdx.bill_disp_meth_name
     , decode(cmf.bill_hold_code,null,null
             ,1,'Tempoary'
             ,2,'Permanent'
               ,'(Unknown)')                                      bill_hold_code_desc
     , decode(cmf.bill_hold_code,null,null
             ,1,'Tempoary (1)'
             ,2,'Permanent (2)'
               ,'(Unknown:'||cmf.bill_hold_code||')')             bill_hold_code_name
     , rcx.rate_class_default_desc
     , rcx.rate_class_default_name
     , decode(cmf.account_status,null,null
             ,-3,'Pending'
             ,-2,'Disc_Done'
             ,-1,'New'
             , 0,'Current'
                ,'Disc_Req')                                      account_status_desc
     , decode(cmf.account_status,null,null
             ,-3,'Pending (-3)'
             ,-2,'Disc_Done (-2)'
             ,-1,'New (-1)'
             , 0,'Current (0)'
                ,'Disc_Req ('||cmf.account_status||')')           account_status_name
     , decode(cmf.collection_indicator,null,null
             ,0,'Not in Collections'
             ,1,'Automatic Collections'
               ,'(Unknown)')                                      collection_indicator_desc
     , decode(cmf.collection_indicator,null,null
             ,0,'Not in Collections (0)'
             ,1,'Automatic Collections (1)'
               ,'(Unknown:'||cmf.collection_indicator||')')       collection_indicator_name
     , vpx.vip_code_desc
     , vpx.vip_code_name
     , nvl(case when length(trim(cmf.remark)) > 50 then
               substr(trim(cmf.remark),1,47)||'...'
            else
               substr(trim(cmf.remark),1,50)
       end,null)                                                  remark_short
     , nvl(case when length(trim(cmf.cust_email)) > 50 then
               substr(trim(cmf.cust_email),1,47)||'...'
            else
               substr(trim(cmf.cust_email),1,50)
       end,null)                                                  cust_email_short
     , nvl(case when length(trim(cmf.statement_to_email)) > 50 then
               substr(trim(cmf.statement_to_email),1,47)||'...'
            else
               substr(trim(cmf.statement_to_email),1,50)
       end,null)                                                  statement_to_email_short
     -- (note K2 does not have clearing_house_id)
     , rrx.rev_rcv_cost_ctr_desc
     , rrx.rev_rcv_cost_ctr_name
     , asx.acct_seg_id_desc
     , asx.acct_seg_id_name
     , decode(cmf.converted,null,null
             ,0,'Not Converted'
             ,1,'Modified Since'
             ,2,'Converted'
               ,'(Unknown)')                                      converted_desc
     , decode(cmf.converted,null,null
             ,0,'Not Converted (0)'
             ,1,'Modified Since (1)'
             ,2,'Converted (2)'
               ,'(Unknown:'||cmf.converted||')')                  converted_name
     , decode(cmf.global_contract_status,null,null
             ,0,'None Apply'
             ,1,'All Apply'
               ,'(Unknown)')                                      global_contract_status_desc
     , decode(cmf.global_contract_status,null,null
             ,0,'None Apply (0)'
             ,1,'All Apply (1)'
               ,'(Unknown:'||cmf.global_contract_status||')')     global_contract_status_name
     , case when     cmf.account_status       not IN (-2,-3)
             and     cmf.date_active              <= sysdate
             and nvl(cmf.date_inactive,sysdate+1) >= sysdate then
               'Active'
            else
               'Inactive'
       end                                                        status
     , case when     cmf.account_status       not IN (-2,-3)
             and     cmf.date_active              <= sysdate
             and nvl(cmf.date_inactive,sysdate+1) >= sysdate then
               1
            else
               0
       end                                                        active
     -- Because accounts in an HQ are not one-to-one with their tracking ID, pulled "cah." for a Yes/No column
     , decode(
           (select count(*)
              from arbor.contract_assignments_hq cah
             where     cah.account_no         = cmf.account_no
               and     cah.start_dt          <= sysdate
               and nvl(cah.end_dt,sysdate+1) >  sysdate
           ),0,'No '
              ,'Yes')                                             in_hq
  from arbor.cmf cmf
  left outer join arbor.customer_id_acct_map iam
    on     iam.account_no                =     cmf.account_no
   and     iam.external_id_type          =     c30arbor.c30_acct_seg_sys_parameters_vl(cmf.acct_seg_id,'acct_no_ext_id_type')
   -- has to be true for this child to be "related" to parent
   and     iam.active_date              <= nvl(cmf.date_inactive,sysdate+1)
   and nvl(iam.inactive_date,sysdate+1) >=     cmf.date_active
  -- "with" constructs and xpanded joins
  left outer join language_code_xpanded lcx
    on lcx.input_language = cmf.language_code
  left outer join account_category_xpanded acx
    on acx.account_category = cmf.account_category
  left outer join disconnect_reason_xpanded drx
    on drx.disconnect_reason = cmf.disconnect_reason
  left outer join bill_period_xpanded bpx
    on bpx.bill_period = cmf.bill_period
  left outer join billing_frequency_xpanded blx
    on blx.billing_frequency = cmf.billing_frequency
  left outer join bill_fmt_opt_xpanded bfx
    on bfx.bill_fmt_opt = cmf.bill_fmt_opt
  left outer join bill_disp_meth_xpanded bdx
    on bdx.bill_disp_meth = cmf.bill_disp_meth
  left outer join rate_class_default_xpanded rcx
    on rcx.rate_class_default = cmf.rate_class_default
  left outer join vip_code_xpanded vpx
    on vpx.vip_code = cmf.vip_code
  left outer join rev_rcv_cost_center_xpanded rrx
    on rrx.rev_rcv_cost_ctr = cmf.rev_rcv_cost_ctr
  left outer join acct_seg_xpanded asx
    on asx.acct_seg_id = cmf.acct_seg_id
with read only
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('cmf_full');
--spool off
exit rollback