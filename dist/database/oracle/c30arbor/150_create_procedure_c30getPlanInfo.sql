set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/150_create_procedure_c30getPlanInfo_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Config (n/a)
prompt Date: 01-11-2012
prompt Created By: Mark Doyle
prompt Updated By: Chip H.F. LaFurney
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 150_create_procedure_c30getPlanInfo.sql
prompt Version: 1.0 Initial Creation for Kenan SQL API
prompt ....... v1.1 New Kenan SQL API changes (v_transaction_id); call account segment validation proc (c30setCtxParameters)
prompt DETAILED EXPLANATION:
prompt Create c30getPlanInfo procedure
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...object [OBJECT_NAME] does not exist"...
drop procedure c30arbor.c30getPlanInfo
;
create or replace procedure c30arbor.c30getPlanInfo(
  v_transaction_id                varchar2, -- v1.1 added
  v_acct_seg_id                   number,
  v_instance_id                   varchar2,
  c30getPlanInfo_cv     IN OUT sys_refcursor
)
is
-- Release Notes
-- Config (n/a)
-- Date: 01-11-2012
-- Created By: Mark Doyle
-- Updated By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 150_create_procedure_c30getPlanInfo.sql
-- Version 1.0 Initial Creation for Kenan SQL API
-- v1.1 New Kenan SQL API changes (v_transaction_id); call account segment validation proc (c30setCtxParameters)
-- DETAILED EXPLANATION:
-- Create c30getPlanInfo procedure

  d_acct_seg_id arbor.cmf.acct_seg_id%type;
  d_subscr_no arbor.service.subscr_no%type;
  d_subscr_no_resets arbor.service.subscr_no_resets%type;
  d_status_id arbor.service_status.status_id%type;

  d_component_inst_id arbor.cmf_package_component.component_inst_id%type;
  d_component_inst_id_serv arbor.cmf_package_component.component_inst_id_serv%type;
  d_instance_id c30arbor.c30_external_inst_id_def.instance_id%type;
  d_partners number(10);
  d_service_id arbor.customer_id_equip_map.external_id%type;
  d_catalog_id c30arbor.c30_external_inst_id_def.catalog_id%type;

  i number(10);

  c_refcur sys_refcursor;

  cursor c_partners(v_instance_id varchar2) is
  select ciim.component_inst_id, ciim.component_inst_id_serv, substr(ciem.external_id,1,instr(ciem.external_id,'.'||d_acct_seg_id,-1,1) -1) as service_id,
    s.subscr_no, s.subscr_no_resets, ss.status_id
  from c30arbor.c30_instance_id_member ciim, arbor.customer_id_equip_map ciem, arbor.service s, arbor.service_status ss
  where ciim.instance_id = v_instance_id
  and ciim.is_active = 1
  and ciim.subscr_no = ciem.subscr_no
  and ciim.subscr_no_resets = ciem.subscr_no_resets
  and ciem.external_id_type = 1
  and ciem.is_current = 1
  and ciem.subscr_no = s.subscr_no
  and ciem.subscr_no_resets = s.subscr_no_resets
  and s.subscr_no = ss.subscr_no
  and s.subscr_no_resets = ss.subscr_no_resets
  and ss.active_dt <= sysdate
  and (ss.inactive_dt > sysdate or ss.inactive_dt is null);

  d_serv_array c30arbor.c30stringarray := c30arbor.c30stringarray();

begin
  c30writeTransactionLog('c30getPlanInfo called v_instance_id='||v_instance_id,null
                        ,p_transaction_id => v_transaction_id
                        ,p_acct_seg_id => v_acct_seg_id
                        );

  -- v1.1 validate account segment and load segmented system parameters into a context...
  c30setCtxParameters(v_acct_seg_id);
  d_acct_seg_id := c30_sys_param('acct_seg_id');
  -- ensure segment has the necessary permissions to execute the API
  c30validateSegPerms(d_acct_seg_id,'c30getPlanInfo');

  -- determine if plan with external instanceId exists in system 
  begin
    select ceiid.catalog_id, ceiid.instance_id, ceiid.component_inst_id, ceiid.component_inst_id_serv,
      substr(ciem.external_id,1,instr(ciem.external_id,'.'||d_acct_seg_id,-1,1) -1) as service_id, ss.status_id,
      s.subscr_no, s.subscr_no_resets, count(ciim.instance_id) as partners
    into d_catalog_id, d_instance_id, d_component_inst_id, d_component_inst_id_serv, d_service_id, d_status_id,
      d_subscr_no, d_subscr_no_resets, d_partners
    from c30arbor.c30_external_inst_id_def ceiid, arbor.cmf_package_component cpc, c30arbor.c30_instance_id_member ciim,
      arbor.customer_id_equip_map ciem, arbor.service s, arbor.service_status ss
    where ceiid.instance_type = 'P'
    and ceiid.acct_seg_id = d_acct_seg_id
    and ceiid.external_inst_id = v_instance_id
    and ceiid.is_active = 1
    and ceiid.component_inst_id = cpc.component_inst_id
    and ceiid.component_inst_id_serv = cpc.component_inst_id_serv
    and cpc.active_dt <= sysdate
    and (cpc.inactive_dt > sysdate or cpc.inactive_dt is null)
    and ceiid.instance_id = ciim.instance_id (+)
    and ciim.is_active (+) = 1
    and ceiid.primary_subscr_no = ciem.subscr_no
    and ceiid.primary_subscr_no_resets = ciem.subscr_no_resets
    and ciem.external_id_type = 1
    and ciem.is_current = 1
    and ciem.subscr_no = s.subscr_no
    and ciem.subscr_no_resets = s.subscr_no_resets
    and s.subscr_no = ss.subscr_no
    and s.subscr_no_resets = ss.subscr_no_resets
    and ss.active_dt <= sysdate
    and (ss.inactive_dt > sysdate or ss.inactive_dt is null)
    group by ceiid.catalog_id, ceiid.instance_id, ceiid.component_inst_id, ceiid.component_inst_id_serv,
      substr(ciem.external_id,1,instr(ciem.external_id,'.'||d_acct_seg_id,-1,1) -1), ss.status_id, s.subscr_no, s.subscr_no_resets;
  exception
    when no_data_found then
      c30writeTransactionLog('C30KSACPH003 instanceId: '||v_instance_id||' does not exist in the system or is no longer active','error');
  end;

  if d_partners > 0 then
    i := 1;
    for r1 in c_partners(d_instance_id) loop
   
      d_serv_array.extend;
      d_serv_array(i) := c30arbor.c30string(r1.service_id);
      i := i + 1;
    end loop;
  end if;

  open c30getPlanInfo_cv for
  select v_instance_id, d_instance_id, d_catalog_id, d_service_id, d_serv_array
  from sys.dual;

exception
  when others then
    -- if the error is internally-generated by the Kenan SQL API, just "raise" it...
    if instr(sqlerrm,'C30KSA') > 0 then
      raise;
    else
      c30writeTransactionLog('C30KSAGPI999 ERROR: '||sqlerrm,'error');
    end if;
end c30getPlanInfo;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30getPlanInfo');
-- spool off
exit rollback