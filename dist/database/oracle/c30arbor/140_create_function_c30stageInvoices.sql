set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/130_create_function_c30stageInvoices_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Config (n/a)
prompt Date: 11-30-2011
prompt Created By: Chip H.F. LaFurney
prompt Defect Resolved: Kenan SQL API
prompt Script file Name: 130_create_function_c30stageInvoices.sql
prompt Version: 1.0 Initial Creation for Kenan SQL API
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt DETAILED EXPLANATION:
prompt Create c30stageInvoices function
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing"; It does not rollback entirely, but it is rerunnable.
prompt *****************************************************************************************************
whenever sqlerror continue
prompt ** Release Management: This may return Oracle error "...object [OBJECT_NAME] does not exist"...
drop function c30arbor.c30stageInvoices
;
create or replace function c30arbor.c30stageInvoices(
  v_mode              number  := 0
  )
  return number
is
-- Release Notes
-- Config (n/a)
-- Date: 11-30-2011
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 130_create_function_c30stageInvoices.sql
-- Version 1.0
-- v1.1 08-15-2012 Added call to c30writeTransactionLog
-- DETAILED EXPLANATION:
-- Create c30stageInvoice function

  type invCurTyp is ref cursor;
  c_invoices invCurTyp;
  d_bill_ref_no arbor.bill_invoice.bill_ref_no%TYPE;
  d_bill_ref_resets arbor.bill_invoice.bill_ref_resets%TYPE;
  d_status_code c30arbor.c30_invoice.status_code%TYPE;
  d_rtn number(1);
  d_success number(10);
  d_error number(10);
  d_sql varchar2(512);

begin

  if v_mode = 0 then
    d_sql := 'select bill_ref_no, bill_ref_resets, status_code '||
             'from c30arbor.c30_invoice '||
             'where status_code = 300';
  else
    d_sql := 'select bill_ref_no, bill_ref_resets, status_code '||
             'from c30arbor.c30_invoice '||
             'where status_code < 0';
  end if;

  c30writeTransactionLog('c30stageInvoices called v_mode='||v_mode||'; d_sql="'||d_sql||'"');

  open c_invoices for d_sql;

  loop
    fetch c_invoices into d_bill_ref_no, d_bill_ref_resets, d_status_code;

    d_rtn := c30arbor.c30stageInvoice(d_bill_ref_no, d_bill_ref_resets, d_status_code);

    if d_rtn = 0 then
      d_success := d_success + 1;
    else
      d_error := d_error + 1;
    end if;

    exit when c_invoices%notfound;
  end loop;

  close c_invoices;

  return 0;

exception
  when others then
    rollback;
    c30writeTransactionLog('C30KSASIV999 ERROR: '||sqlerrm,'error');
    return 1;
end c30stageInvoices;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30stageInvoices');
--spool off
exit rollback