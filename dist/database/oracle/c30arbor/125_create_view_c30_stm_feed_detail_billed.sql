set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/125_create_view_c30_stm_feed_detail_billed_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 125_create_view_c30_stm_feed_detail_billed.sql
prompt CA Ticket# : AWN
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_stm_feed_detail_billed view for Settlement Feed processing
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 10/01/2012 ##  1.0 Initial Creation for AWN
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_stm_feed_detail_billed as
select
-- Release Notes
-- Config (n/a)
-- Date: 10/01/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: AWN
--
-- Script file Name: 125_create_view_c30_stm_feed_detail_billed.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 10/04/2012 ##  v1.1 Defect 188 - Added 3 bill_invoice columns to where clause for "valid" BIPs
-- chip.lafurney@cycle30.com    ## 11/14/2012 ##  v1.2 Defect 279 - "_Fmt" columns become THE columns (no more "as is" columns - output is for Excel)
-- DETAILED EXPLANATION:
-- Create c30_stm_feed_detail_billed view for Settlement Feed processing
-- this settlement feed for BILLED records has the 1st to end of the current month as the to_date cutoff
-- (that is "no records invoiced before last month")
       sfd."Account_Identifier"
     , sfd."Phone_No"
     , to_char(dts.billed_cutoff_date,'yyyy/mm/dd') "Cutoff_Date"
     , sfd."Usage_Datetime"
     , sfd."Call_From"
     , sfd."Call_To"
     , sfd."Dialed_Digits"
     , sfd."Usage_Type"
     , sfd."Call_From_City"
     , sfd."Call_From_State"
     , sfd."Call_From_Country_Code"
     , sfd."Called_City"
     , sfd."Called_State"
     , sfd."Called_Country_Code"
     , sfd."Catalog_ID"
     , sfd."IMSI"
     , sfd."Units"
     , sfd."Units_Type"
     , sfd."Rated_Units"
     , sfd."Rated_Units_Type"
     , sfd."Rate"
     , sfd."Cost"
     , sfd."Location"
  from c30arbor.c30_stm_feed_dates dts
  join arbor.bill_invoice biv
    on biv.jnl_status      IN(0,-1)                               -- bill_invoice_xjnl_status index part 1
   and biv.to_date    between dts.min_to_date and dts.max_to_date -- bill_invoice_xjnl_status index part 2
   and biv.prep_status      = 1    --v1.1 added
   and biv.prep_error_code is null --v1.1 added
   and biv.backout_status   = 0    --v1.1 added
  join arbor.cdr_billed cbl
    on cbl.bill_ref_no     = biv.bill_ref_no
   and cbl.bill_ref_resets = biv.bill_ref_resets
   and cbl.account_no      = biv.account_no
  join c30arbor.c30_stm_feed_detail sfd
    on sfd.msg_id                 = cbl.msg_id
   and sfd.msg_id2                = cbl.msg_id2
   and sfd.msg_id_serv            = cbl.msg_id_serv
   and sfd.min_split_row_num     <= cbl.split_row_num
   and sfd.max_split_row_num     >= cbl.split_row_num
   and sfd.cdr_data_partition_key = cbl.cdr_data_partition_key
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_stm_feed_detail_billed');
--...
--select count(*) from c30arbor.c30_stm_feed_detail_billed;
--spool off
exit rollback