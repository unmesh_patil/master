set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/105_create_procedure_c30setCtxParameters_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 105_create_procedure_c30setCtxParameters.sql
prompt CA Ticket# : Kenan SQL API
prompt QC Defect# : Kenan SQL API
prompt System(s)  : M2M/C3B
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_ctx_set_parameter procedure
prompt NOTE ON "105" NUMBERING - Any object should be able to call this; trigger scripts are also 105, but they don't use this;
prompt .. **THIS 105 PROC** calls 103 package c30_ctx (BODY) calls 102 proc c30writeTransaction calls 102 proc c30insertTransaction and 102 package c30_ctx (SPEC)
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 12/01/2011 ##  Initial Creation for Kenan SQL API
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
create or replace procedure c30arbor.c30setCtxParameters(
   v_acct_seg_id              c30arbor.c30_transaction_log.acct_seg_id%type := null --v1.1 made optional
)
is
-- Release Notes
-- Config (n/a)
-- Date: 12-01-2011
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 105_create_procedure_c30setCtxParameters.sql
-- Version 1.0 Initial Creation for Kenan SQL API
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 01/20/2012 ##  v1.1 Changed to an optional account segment which gets validated in this proc
-- chip.lafurney@cycle30.com    ## 03/26/2012 ##  v1.2 Added additional criteria for immediate "return"
-- chip.lafurney@cycle30.com    ## 05/07/2012 ##  v1.3 Change c30_put_line to c30writeTransactionLog([msg],'debug_verbose');
--                                                     pulled "immediate return" logic; newly changed c30_acct_seg_sys_parameters values were not getting read;
--                                                     loop through the default values (seg=0) to set all parameters on table
-- mark.doyle@cycle30.com       ## 07/20/2012 ##  v1.4 Starting using c30_sys_parameters_def table
-- chip.lafurney@cycle30.com    ## 09/06/2012 ##  v1.5 Improved above logic by installing c30_get_acct_seg_value function
-- chip.lafurney@cycle30.com    ## 09/17/2012 ##  v1.6 De-installed c30_get_acct_seg_value function and installed c30_acct_seg_sys_parameters_vl function instead
-- chip.lafurney@cycle30.com    ## 09/21/2012 ##  v1.7 Changed multiple debug_verbose logs to one longer log_msg
-- DETAILED EXPLANATION:
-- Create c30_ctx_set_parameter procedure
-- For a passed account segment, load all of its parameters into the 'c30_ctx' context

  l_acct_seg_id               arbor.csr_acct_seg.acct_seg_id%type;
  l_log_msg                   c30arbor.c30_transaction_log.log_msg%type; --v1.7 added

begin

if v_acct_seg_id is null then
  -- try to determine default acct seg for user
  begin
    select   acct_seg_id
      into l_acct_seg_id
      from arbor.csr_acct_seg cas
     where cas.csr_name       = lower(user)
       and cas.is_csr_default = 1
    ;
  exception
    when no_data_found then
      -- had to just pick "min" or "max"; note this won't generate "no_data_found"...
      select min(acct_seg_id)
        into   l_acct_seg_id
        from arbor.csr_acct_seg cas
       where cas.csr_name       = lower(user)
         and cas.is_csr_default = 0
      ;
      if l_acct_seg_id is null then
        c30writeTransactionLog('C30KSASCP001 Unable to determine appropriate Account Segment for user: "'||lower(user)||'"','error');
      end if;
  end;
else
  -- ensure it is a valid acct_seg_id for user
  begin
    select   acct_seg_id
      into l_acct_seg_id
      from arbor.csr_acct_seg cas
     where cas.csr_name    = lower(user)
       and cas.acct_seg_id = v_acct_seg_id
    ;
  exception
    when no_data_found then
      c30writeTransactionLog('C30KSASCP002 Account Segment passed: "'||v_acct_seg_id||'" is invalid for user: "'||lower(user)||'"','error');
  end;
end if;

-- load the newly determined account segment into context; v1.3 moved up...
c30writeTransactionLog('c30setCtxParameters calling c30arbor.c30_ctx.set_parameter(''acct_seg_id'','||l_acct_seg_id||')','debug');
c30arbor.c30_ctx.set_parameter('acct_seg_id',l_acct_seg_id);

--v1.3 N/A: clear the context first; v1.2 only clear the ones on the c30_acct_seg_sys_parameters table;
-- instead, loop through the default names and values (seg=0) to set all parameters on table
-- (by getting all default values, we do not have to null out the values; simply [re-]set them all)...
--v1.5 Improved above logic by installing c30_get_acct_seg_value function
--v1.6 De-installed c30_get_acct_seg_value function and installed c30_acct_seg_sys_parameters_vl function instead
--v1.7 Changed multiple debug_verbose logs to one longer log_msg
l_log_msg := 'c30setCtxParameters set_parameters';
for l_ssp IN(
  with all_parameter_names as(
    select distinct parameter_name from c30arbor.c30_acct_seg_sys_parameters
    UNION
    select parameter_name from c30arbor.c30_sys_parameters_def
  )
  select apn.parameter_name
       , coalesce((select c30arbor.c30_acct_seg_sys_parameters_vl(l_acct_seg_id,apn.parameter_name)
                     from dual)
                 ,(select spd.default_value
                     from c30arbor.c30_sys_parameters_def spd
                    where spd.parameter_name = apn.parameter_name)
                 ) parameter_value
    from all_parameter_names apn
            ) loop
  l_log_msg := l_log_msg||'; '||l_ssp.parameter_name||'="'||nvl(l_ssp.parameter_value,'<null>')||'"';
  c30arbor.c30_ctx.set_parameter(l_ssp.parameter_name,l_ssp.parameter_value);
end loop;
c30writeTransactionLog(l_log_msg,'debug_verbose');

exception
  when others then
    -- if the error is internally-generated by the Kenan SQL API, just "raise" it...
    if instr(sqlerrm,'C30KSA') > 0 then
      raise;
    else
      c30writeTransactionLog('C30KSASCP999 ERROR: '||sqlerrm,'error');
    end if;
end c30setCtxParameters;
/
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30setCtxParameters');
--spool off
exit rollback