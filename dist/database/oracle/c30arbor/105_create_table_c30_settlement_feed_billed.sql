set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/105_create_table_c30_settlement_feed_billed_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 105_create_table_c30_settlement_feed_billed.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create c30_settlement_feed_billed table for R325955 Settlement Feed Process Improvement
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: "drop table" may return Oracle error "...table or view does not exist"
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
whenever sqlerror continue
drop table c30arbor.c30_settlement_feed_billed cascade constraints purge
;
-- note: column lengths typically come from the source Kenan tables; "_char" columns are formatted for Settlement Feed output...
create table c30arbor.c30_settlement_feed_billed(
   billed_cutoff_date_char       varchar2(10)              not null -- format 'yyyy/mm/dd'
  ,acct_seg_id                   number                    not null
  ,cdr_data_partition_key        date                      not null
  ,msg_id                        number(10)                not null
  ,msg_id2                       number(3)                 not null
  ,msg_id_serv                   number(3)                 not null
  ,split_row_num                 number(3)                 not null
  ,type_id_usg                   number(10)                not null
  ,bill_class                    number(6)                 not null
  ,provider_id                   number(10)                not null
  ,jurisdiction                  number(10)                not null
  ,account_no                    number(10)                not null
  ,subscr_no                     number(10)                not null
  ,subscr_no_resets              number(6)                 not null
  ,external_id                   varchar2(144)             not null
  ,external_id_type              number(6)                 not null
  ,point_origin                  varchar2(72)              not null
  ,country_code_origin_char      varchar2(6)                   null -- format 'fm999999'
  ,point_id_origin               number(10)                not null
  ,point_target                  varchar2(72)              not null
  ,country_code_target_char      varchar2(6)                   null -- format 'fm999999'
  ,point_id_target               number(10)                not null
  ,dialed_digits                 varchar2(72)                  null
  ,trans_dt                      date                      not null
  ,trans_dt_char                 varchar2(19)              not null -- format 'yyyy/mm/dd hh24:mi:ss'
  ,primary_units_char            varchar2(19)              not null -- format 'fm999,999,999,999,990'
  ,billing_units_type            number(6)                 not null
  ,annotation                    varchar2(255)             not null
  ,customer_tag                  varchar2(240)             not null
  ,rated_units                   number(18)                not null
  ,unrounded_amount_calc         number                    not null
  ,unrounded_amount_char         varchar2(15)              not null -- format 'fm99,999,990.0000'
  ,seqnum_rate_usage             number(10)                not null
  ,component_id                  number(10)                not null
  ,usage_type                    varchar2(100)             not null
  ,add_unit_rate                 number                    not null
  ,location                      varchar2(240)                 null
  ,valdtn                        number(3)                 not null -- validates split_row_num = 0 or add_unit_rate not found
  ,load_cdr_data                 number(1) default 0       not null -- column[s] above added by this load procedure
--
  ,account_no_desc               varchar2(144)                 null
  ,load_acct_no                  number(1) default 0       not null -- column[s] above added by this load procedure
--
  ,call_from_city                varchar2(105)                 null
  ,call_from_state               varchar2(105)                 null
  ,called_city                   varchar2(105)                 null
  ,called_state                  varchar2(105)                 null
  ,units_type                    varchar2(240)                 null
  ,rated_units_calc              number                        null
  ,rated_units_char              varchar2(21)                  null -- format 'fm999,999,999,999,990.0'
  ,rated_units_type              varchar2(240)                 null
  ,rate_char                     varchar2(15)                  null -- format 'fm999,990.000000'
  ,load_unit_usage               number(1) default 0       not null -- column[s] above added by this load procedure
--
  ,phone_no                      varchar2(144)                 null
  ,catalog_id                    varchar2(100)                 null -- don't need length 512 for actual c30arbor.c30_external_inst_id_def.catalog_id values
  ,imsi                          varchar2(144)                 null
  ,load_svc_plan                 number(1) default 0       not null -- column[s] above added by this load procedure
--
  -- auditing columns...
  ,chg_date                      timestamp(6) with time zone   null
  ,chg_who                       varchar2(30)                  null
   )
;
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_settlement_feed_billed');
--spool off
exit rollback