set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/125_create_view_c30_stm_feed_detail_unbilled_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 125_create_view_c30_stm_feed_detail_unbilled.sql
prompt CA Ticket# : AWN
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Create c30_stm_feed_detail_unbilled view for Settlement Feed processing
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 10/01/2012 ##  1.0 Initial Creation for AWN
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_stm_feed_detail_unbilled as
select
-- Release Notes
-- Config (n/a)
-- Date: 10/01/2012
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: AWN
--
-- Script file Name: 125_create_view_c30_stm_feed_detail_unbilled.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 11/14/2012 ##  v1.1 Defect 279 - "_Fmt" columns become THE columns (no more "as is" columns - output is for Excel)
-- DETAILED EXPLANATION:
-- Create c30_stm_feed_detail_unbilled view for Settlement Feed processing
-- this settlement feed for UNBILLED records has the 15th of the month as the cutoff
-- (that is "no records after the 16th of the month at midnight")
       sfd."Account_Identifier"
     , sfd."Phone_No"
     , to_char(dts.unbilled_cutoff_date,'yyyy/mm/dd') "Cutoff_Date"
     , sfd."Usage_Datetime"
     , sfd."Call_From"
     , sfd."Call_To"
     , sfd."Dialed_Digits"
     , sfd."Usage_Type"
     , sfd."Call_From_City"
     , sfd."Call_From_State"
     , sfd."Call_From_Country_Code"
     , sfd."Called_City"
     , sfd."Called_State"
     , sfd."Called_Country_Code"
     , sfd."Catalog_ID"
     , sfd."IMSI"
     , sfd."Units"
     , sfd."Units_Type"
     , sfd."Rated_Units"
     , sfd."Rated_Units_Type"
     , sfd."Rate"
     , sfd."Cost"
     , sfd."Location"
  from c30arbor.c30_stm_feed_dates dts
  join arbor.cdr_unbilled cub
    on cub.trans_dt < dts.unbilled_cutoff_date
  join c30arbor.c30_stm_feed_detail sfd
    on sfd.msg_id                 = cub.msg_id
   and sfd.msg_id2                = cub.msg_id2
   and sfd.msg_id_serv            = cub.msg_id_serv
   and sfd.min_split_row_num     <= cub.split_row_num
   and sfd.max_split_row_num     >= cub.split_row_num
   and sfd.cdr_data_partition_key = cub.cdr_data_partition_key
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_stm_feed_detail_unbilled');
--...
--select count(*) from c30arbor.c30_stm_feed_detail_unbilled;
--spool off
exit rollback