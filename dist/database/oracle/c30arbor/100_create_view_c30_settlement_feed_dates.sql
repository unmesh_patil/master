set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/100_create_view_c30_settlement_feed_dates_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 100_create_view_c30_settlement_feed_dates.sql
prompt CA Ticket# : R325955 Settlement Feed Process Improvement
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B1)
prompt Database(s): ALL
prompt Schema(s)  : C30ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create c30_settlement_feed_dates view for R325955 Settlement Feed Process Improvement
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt chip.lafurney@cycle30.com    ## 09/12/2013 ##  1.0 Initial Creation for R325955
prompt NOTE: (Revision log is maintained within internal "Release Notes" below.)
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
create or replace view c30arbor.c30_settlement_feed_dates as
select
-- Release Notes
-- Config (n/a)
-- Date: 09/12/2013
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: N/A
--
-- Script file Name: 100_create_view_c30_settlement_feed_dates.sql
-- Version 1.0 Initial Creation for AWN
-- Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
-- chip.lafurney@cycle30.com    ## 09/12/2013 ##  v1.0 Initial Creation for R325955
-- DETAILED EXPLANATION:
-- Create c30_settlement_feed_dates view for R325955 Settlement Feed Process Improvement
     -- usage billed between the min_to_date and max_to_date below has this cutoff...
       trunc(sysdate,'mm')-1                              billed_cutoff_date
     -- used against table BILL_INVOICE with part of an index on to_date; only rows >= this date are selected...
     , trunc(sysdate,'mm')                                min_to_date
     -- used against table BILL_INVOICE with part of an index on to_date; only rows <= this date are selected...
     , trunc(add_months(sysdate,1),'mm')-(1/(24*60*60))   max_to_date -- beginning of next month minus one second
     , 3                                                  acct_seg_id
     , to_number(c30arbor.c30_acct_seg_sys_parameters_vl(3,'acct_no_ext_id_type'))
                                                          acct_no_ext_id_type
  from dual
with read only
;
show errors
-- use procedure to set up public synonym and grants (if needed)...
exec c30_ddl_standard('c30_settlement_feed_dates');
--spool off
exit rollback