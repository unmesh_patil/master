set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/200_012_config_awn_&_connect_identifier..txt
prompt *****************************************************************************************************
prompt Release Notes
prompt Script Name: 200_012_config_awn.sql
prompt CA Ticket# : AWN
prompt QC Defect# : AWN
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL
prompt Schema(s)  : ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: updates required to sync up external_id_types with K1 since we are sharing mediation logic
prompt No Outage is required
prompt Expected Outcome : Script cannot be "All or Nothing"; create object cannot be rolled back
prompt Acceptable Errors: N/A
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                 ## Date       ##  Description (One revision per line,if multiple revisions)
prompt mark.doyle@cycle30.com       ## 09/16/2012 ##  1.0 Initial Creation for AWN
prompt chip.lafurney@cycle30.com    ## 09/26/2012 ##  2.0 Mass-change to update existing values
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 907.868.0130; Mark Doyle 907.868.5346
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; create object cannot be rolled back
prompt *****************************************************************************************************
-- "shotgun approach" to disabling all constraints on external_id_type...
-- SQL at end...
alter table arbor.customer_id_acct_map disable constraint cstmr_d_cct_mp_ch1;
alter table arbor.customer_id_group_map disable constraint cstmr_d_grp_mp_ch1;
alter table arbor.customer_id_equip_map disable constraint cstmr_d_qp_mp_ch2;
alter table arbor.customer_id_equip_map_key disable constraint cstmr_d_qp_mp_ky_ch2;
alter table arbor.customer_id_equip_map_view disable constraint cstmr_d_qp_mp_vw_ch2;
alter table arbor.external_id_acct_map disable constraint extrnl_d_cct_mp_ch1;
alter table arbor.external_id_group_map disable constraint extrnl_d_grp_mp_ch1;
alter table arbor.external_id_equip_map disable constraint extrnl_d_qp_mp_ch2;
alter table arbor.external_id_equip_map_key disable constraint extrnl_d_qp_mp_ky_ch1;
alter table arbor.external_id_equip_map_view disable constraint extrnl_d_qp_mp_vw_ch2;
alter table arbor.external_id_type_values disable constraint extrnl_d_typ_vls_ch1;
alter table arbor.service disable constraint service_serv_dis_ext_fk;
alter table arbor.service_view disable constraint service_view_servv_dis_ext_fk;
-- SQL at end...
alter trigger arbor.cust_id_equip_map_view_actrig disable;
alter trigger arbor.cust_id_equip_map_view_atrig disable;
alter trigger arbor.cust_id_equip_map_view_auctrig disable;
alter trigger arbor.cust_id_equip_map_view_auvtrig disable;
alter trigger arbor.cust_id_equip_map_view_biutrig disable;
alter trigger arbor.cust_id_equip_map_view_bvtrig disable;
alter trigger arbor.ext_id_eqp_map_view_actrig disable;
alter trigger arbor.ext_id_equip_map_view_atrig disable;
alter trigger arbor.ext_id_equip_map_view_auctrig disable;
alter trigger arbor.ext_id_equip_map_view_auvtrig disable;
alter trigger arbor.ext_id_equip_map_view_biutrig disable;
alter trigger arbor.ext_id_equip_map_view_bvtrig disable;
alter trigger arbor.ext_id_equip_map_view_ptrig disable;
alter trigger arbor.external_id_type_ref_atrig disable;
alter trigger arbor.external_id_type_ref_rtrig disable;
alter trigger arbor.service_view_actrig disable;
alter trigger arbor.service_view_atrig disable;
alter trigger arbor.service_view_auctrig disable;
alter trigger arbor.service_view_auvtrig disable;
alter trigger arbor.service_view_biutrig disable;
alter trigger arbor.service_view_bvtrig disable;
begin

delete arbor.external_id_type_values where external_id_type IN(60,20008,20015) and language_code = 1;
c30_put_line(null,'Deleted '||sql%rowcount||' external_id_type_values row[s]');

delete arbor.external_id_type_ref where external_id_type IN(60,20008,20015);
c30_put_line(null,'Deleted '||sql%rowcount||' external_id_type_ref row[s]');

insert into arbor.external_id_type_ref ( external_id_type, uniqueness_type, is_default, is_internal, is_viewable )
values ( 60, 1, 0, 0, 1 );
insert into arbor.external_id_type_ref ( external_id_type, uniqueness_type, is_default, is_internal, is_viewable )
values ( 20008, 1, 0, 0, 1 );
insert into arbor.external_id_type_ref ( external_id_type, uniqueness_type, is_default, is_internal, is_viewable )
values ( 20015, 1, 0, 0, 1 );
c30_put_line(null,'Inserted 3 external_id_type_ref rows');

insert into arbor.external_id_type_values ( external_id_type, language_code, short_display, display_value )
values ( 60, 1, 'MSISDN', 'MSISDN' );
insert into arbor.external_id_type_values ( external_id_type, language_code, short_display, display_value )
values ( 20008, 1, 'ICCID', 'ICCID' );
insert into arbor.external_id_type_values ( external_id_type, language_code, short_display, display_value )
values ( 20015, 1, 'IMSI', 'IMSI' ); 
c30_put_line(null,'Inserted 3 external_id_type_values rows');

update arbor.emf_config_id_ref set default_external_id_type = 60 where emf_config_id = 1;
c30_put_line(null,'Updated '||sql%rowcount||' emf_config_id_ref row[s]');

delete c30arbor.c30_acct_seg_obj_map where object_name = 'serv_external_id_type';
c30_put_line(null,'Deleted '||sql%rowcount||' c30_acct_seg_obj_map row[s]');

insert into c30arbor.c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
values (1, 'serv_external_id_type', '60', 1);
insert into c30arbor.c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
values (2, 'serv_external_id_type', '60', 1);
insert into c30arbor.c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
values (2, 'serv_external_id_type', '20008', 1);
insert into c30arbor.c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
values (2, 'serv_external_id_type', '20015', 1);
c30_put_line(null,'Inserted 4 c30_acct_seg_obj_map rows');

-- fix existing values
update arbor.service set display_external_id_type = decode(display_external_id_type,3,60,4,20008,5,20015,(1/0)) where display_external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' service row[s]');

update arbor.service_view set display_external_id_type = decode(display_external_id_type,3,60,4,20008,5,20015,(1/0)) where display_external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' service_view row[s]');

update arbor.cdr_data_work set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' cdr_data_work row[s]');

update arbor.cdr_data set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' cdr_data row[s]');

update arbor.external_id_equip_map set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' external_id_equip_map row[s]');

update arbor.external_id_equip_map_view set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' external_id_equip_map_view row[s]');

update arbor.external_id_equip_map_key set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' external_id_equip_map_key row[s]');

update arbor.customer_id_equip_map set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' customer_id_equip_map row[s]');

update arbor.customer_id_equip_map_view set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' customer_id_equip_map_view row[s]');

update arbor.customer_id_equip_map_key set external_id_type = decode(external_id_type,3,60,4,20008,5,20015,(1/0)) where external_id_type IN(3,4,5);
c30_put_line(null,'Updated '||sql%rowcount||' customer_id_equip_map_key row[s]');

-- delete old external_id_type ref/values
delete arbor.external_id_type_values where external_id_type IN(3,4,5);
c30_put_line(null,'Deleted '||sql%rowcount||' external_id_type_values row[s] (3,4,5)');

delete arbor.external_id_type_ref where external_id_type IN(3,4,5);
c30_put_line(null,'Deleted '||sql%rowcount||' external_id_type_ref row[s] (3,4,5)');

-- issue final commit and print dbms_output
commit;
c30_put_line(null,'Successfully completed script');
c30_put_line(null,'commit;');

exception
  when others then
    rollback;
    c30_put_line(null,'rollback;');
    c30_put_line(null,'** Error '||sqlerrm);
end;
/
alter table arbor.customer_id_acct_map enable constraint cstmr_d_cct_mp_ch1;
alter table arbor.customer_id_group_map enable constraint cstmr_d_grp_mp_ch1;
alter table arbor.customer_id_equip_map enable constraint cstmr_d_qp_mp_ch2;
alter table arbor.customer_id_equip_map_key enable constraint cstmr_d_qp_mp_ky_ch2;
alter table arbor.customer_id_equip_map_view enable constraint cstmr_d_qp_mp_vw_ch2;
alter table arbor.external_id_acct_map enable constraint extrnl_d_cct_mp_ch1;
alter table arbor.external_id_group_map enable constraint extrnl_d_grp_mp_ch1;
alter table arbor.external_id_equip_map enable constraint extrnl_d_qp_mp_ch2;
alter table arbor.external_id_equip_map_key enable constraint extrnl_d_qp_mp_ky_ch1;
alter table arbor.external_id_equip_map_view enable constraint extrnl_d_qp_mp_vw_ch2;
alter table arbor.external_id_type_values enable constraint extrnl_d_typ_vls_ch1;
alter table arbor.service enable constraint service_serv_dis_ext_fk;
alter table arbor.service_view enable constraint service_view_servv_dis_ext_fk;
alter trigger arbor.cust_id_equip_map_view_actrig enable;
alter trigger arbor.cust_id_equip_map_view_atrig enable;
alter trigger arbor.cust_id_equip_map_view_auctrig enable;
alter trigger arbor.cust_id_equip_map_view_auvtrig enable;
alter trigger arbor.cust_id_equip_map_view_biutrig enable;
alter trigger arbor.cust_id_equip_map_view_bvtrig enable;
alter trigger arbor.ext_id_eqp_map_view_actrig enable;
alter trigger arbor.ext_id_equip_map_view_atrig enable;
alter trigger arbor.ext_id_equip_map_view_auctrig enable;
alter trigger arbor.ext_id_equip_map_view_auvtrig enable;
alter trigger arbor.ext_id_equip_map_view_biutrig enable;
alter trigger arbor.ext_id_equip_map_view_bvtrig enable;
alter trigger arbor.ext_id_equip_map_view_ptrig enable;
alter trigger arbor.external_id_type_ref_atrig enable;
alter trigger arbor.external_id_type_ref_rtrig enable;
alter trigger arbor.service_view_actrig enable;
alter trigger arbor.service_view_atrig enable;
alter trigger arbor.service_view_auctrig enable;
alter trigger arbor.service_view_auvtrig enable;
alter trigger arbor.service_view_biutrig enable;
alter trigger arbor.service_view_bvtrig enable;
--spool off
exit rollback
/*
select 'alter table '||con.r_owner||'.'||con.table_name||' disable constraint '||con.constraint_name||';' sql_line
from sys.dba_constraints con
where con.owner like '%ARBOR'
and exists(select 'x'
           from sys.dba_cons_columns col
           where col.constraint_name = con.constraint_name
and col.column_name like '%EXTERNAL_ID_TYPE')
and con.constraint_type IN('R','U');

select 'select external_id_type, count(*) from '||t.owner||'.'||t.table_name||' where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;' sql_line
from sys.dba_all_tables t
where t.owner like '%ARBOR'
and exists (Select 'x'
            from sys.dba_tab_columns c
            where owner like '%ARBOR'
            and c.table_name = t.table_name
            and c.column_name = 'EXTERNAL_ID_TYPE')
and exists (Select 'x'
            from sys.dba_tab_columns c
            where owner like '%ARBOR'
            and c.table_name = t.table_name
            and c.column_name = 'EXTERNAL_ID')
;
--FIX select external_id_type, count(*) from ARBOR.CUSTOMER_ID_EQUIP_MAP_KEY where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.EXTERNAL_ID_ACCT_MAP where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.EXTERNAL_ID_EQUIP_MAP_KEY where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.CUSTOMER_ID_EQUIP_MAP where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.EXTERNAL_ID_EQUIP_MAP where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.CDR_DATA_WORK where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.CUSTOMER_ID_ACCT_MAP where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.CUSTOMER_ID_EQUIP_MAP_VIEW where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.EXTERNAL_ID_EQUIP_MAP_VIEW where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.EXTERNAL_ID_GROUP_MAP where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.CDR_DATA where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.LBX_ERROR where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.LBX_POST_DATED where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.CDR_OUTCOLLECTS where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.CDR_FREE where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;
--  0 select external_id_type, count(*) from ARBOR.CUSTOMER_ID_GROUP_MAP where external_id_type IN(3,4,5) and external_id is not null group by external_id_type;

select 'select external_id_type, count(*) from '||t.owner||'.'||t.table_name||' where external_id_type IN(3,4,5) group by external_id_type;' sql_line
from sys.dba_all_tables t
where t.owner like '%ARBOR'
and exists (Select 'x'
            from sys.dba_tab_columns c
            where owner like '%ARBOR'
            and c.table_name = t.table_name
            and c.column_name like '%EXTERNAL_ID_TYPE') -- notice we have to pick up DISPLAY_external_id_type and DEFAULT_external_id_type
and not exists (Select 'x'
            from sys.dba_tab_columns c
            where owner like '%ARBOR'
            and c.table_name = t.table_name
            and c.column_name = 'EXTERNAL_ID')
;
--  0 select external_id_type, count(*) from ARBOR.PRODUCT_ELEMENTS where external_id_type IN(3,4,5) group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.EXTERNAL_ID_TYPE_VALUES where external_id_type IN(3,4,5) group by external_id_type;
--FIX select external_id_type, count(*) from ARBOR.EXTERNAL_ID_TYPE_REF where external_id_type IN(3,4,5) group by external_id_type;
--  0 select external_id_type, count(*) from C30ARBOR.C30_INVENTORY_TYPE_REF where external_id_type IN(3,4,5) group by external_id_type;
--FIX select DEFAULT_external_id_type, count(*) from ARBOR.EMF_CONFIG_ID_REF where DEFAULT_external_id_type IN(3,4,5) group by DEFAULT_external_id_type;
--FIX select DISPLAY_external_id_type, count(*) from ARBOR.SERVICE_VIEW where DISPLAY_external_id_type IN(3,4,5) group by DISPLAY_external_id_type;
--FIX select DISPLAY_external_id_type, count(*) from ARBOR.SERVICE where DISPLAY_external_id_type IN(3,4,5) group by DISPLAY_external_id_type;

select csv_string(cursor(
select distinct t.table_name
from sys.dba_all_tables t
where t.owner like '%ARBOR'
and exists (Select 'x'
            from sys.dba_tab_columns c
            where owner like '%ARBOR'
            and c.table_name = t.table_name
            and c.column_name = 'EXTERNAL_ID_TYPE')
),''',''') from dual
;
select lower('alter trigger '||owner||'.'||trigger_name||' disable;') sql_line
from sys.dba_triggers
where owner like '%ARBOR'
and table_name IN(
'EXTERNAL_ID_EQUIP_MAP','CUSTOMER_ID_EQUIP_MAP_VIEW','EXTERNAL_ID_TYPE_VALUES',
'CDR_DATA','CUSTOMER_ID_EQUIP_MAP_KEY','EXTERNAL_ID_TYPE_REF',
'xPRODUCT_ELEMENTS','xC30_INVENTORY_TYPE_REF','EXTERNAL_ID_EQUIP_MAP_KEY',
'xCDR_FREE','xEXTERNAL_ID_ACCT_MAP','xEXTERNAL_ID_GROUP_MAP',
'xCUSTOMER_ID_ACCT_MAP','CDR_DATA_WORK','xLBX_ERROR',
'EXTERNAL_ID_EQUIP_MAP_VIEW','xCUSTOMER_ID_GROUP_MAP','xCDR_OUTCOLLECTS',
'xLBX_POST_DATED','CUSTOMER_ID_EQUIP_MAP','SERVICE','SERVICE_VIEW'
)
order by table_name, trigger_name;

*/