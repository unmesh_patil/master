set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
prompt *****************************************************************************************************
prompt Release Notes
prompt PROJECT: AWN - Alaska Wireless Network
prompt Date: 8-06-2012
prompt Created By: Steve Brock
prompt .
prompt No Outage is required
prompt .
prompt Defect Resolved: RAW_USAGE_FIELD_MAPPING tables
prompt .
prompt    
prompt Description: Some initial values for RAW_USAGE_FIELD_MAPPING tables
prompt .
prompt    
prompt ENVIRONMENT: C3BDV1 only  
prompt .  
prompt  
prompt SPECIAL INSTRUCTIONS: 
prompt .
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing" i.e. if it encounters an error it will rollback entirely
prompt *****************************************************************************************************




--INSERT AND UPDATES TO EXISTING TABLES


declare
  l_error_msg                         varchar2(255); -- max dbms_output line length

begin

  -- descriptions
  -- TIMEZONE_ref 
  -- TIMEZONE_values  
  -- ext_source_id_ref 
  -- ext_source_id_values 
  -- EXT_ACCESS_METHOD_REF 
  -- EXT_ACCESS_METHOD_values 
  -- FILE_TYPE_REF 
  -- FILE_TYPE_values 
  -- ext_contacts 
  -- EXT_LOCAL_DIR_ACCESS 
  -- RAW_USAGE_FIELD_MAPPING

insert into descriptions values ('1016','1','0','Daily Usage Processing','');

insert into TIMEZONE_ref values ('9','AST9AKDT      ','1','1','0');
insert into TIMEZONE_values values ('9','','Alaska Time Zone','1');

insert into ext_source_id_ref values ('1','1','0','0');
insert into ext_source_id_values values ('1','1','Src1','Source Id 1');
--insert into EXT_ACCESS_METHOD_REF values ('1','0','1');
--insert into EXT_ACCESS_METHOD_values values ('1','1','LOCAL','LOCAL Directory Access Method');
--insert into FILE_TYPE_REF values ('0','1','1');
--insert into FILE_TYPE_values values ('0','1','','Arbor Defined File Type');
insert into ext_contacts values ('1','0','1','','1016','1','','0','','','0','/opt/app/kenanfx2/arbor/data/usage/ready','/opt/app/kenanfx2/arbor/data/usage/done','/opt/app/kenanfx2/arbor/data/usage/error','/opt/app/kenanfx2/arbor/data/usage/work','','','',TO_DATE('01/01/2011','DD/MM/YYYY'),'','ARBOR                         ',TO_DATE('01/01/2011','DD/MM/YYYY'),'0','','','','','0','','','0');
insert into EXT_LOCAL_DIR_ACCESS values ('1','/opt/app/kenanfx2/arbor/data/usage/remote/ready','/opt/app/kenanfx2/arbor/data/usage/remote/work','/opt/app/kenanfx2/arbor/data/usage/remote/done');

insert into raw_usage_field_mapping values('GPR',1,'Record Type',3,3,1,1,null,'');
insert into raw_usage_field_mapping values('GPR',2,'Usage Type',5,1,1,0,null,'type_id_usg');
insert into raw_usage_field_mapping values('GPR',3,'Billing Class',5,1,1,0,null,'bill_class');
insert into raw_usage_field_mapping values('GPR',4,'Billed Number Type',5,1,1,0,null,'external_id_type');
insert into raw_usage_field_mapping values('GPR',5,'Billed Number',48,3,1,0,null,'external_id');
insert into raw_usage_field_mapping values('GPR',6,'Trans Date/Time',14,5,1,0,'YYYYMMDDHHMISS','trans_dt');
insert into raw_usage_field_mapping values('GPR',7,'Target Number',20,3,0,0,null,'point_target');
insert into raw_usage_field_mapping values('GPR',8,'Origin Number',20,3,1,0,null,'point_origin');
insert into raw_usage_field_mapping values('GPR',9,'Data',10,1,1,0,null,'primary_units');
insert into raw_usage_field_mapping values('GPR',10,'Filler1',10,1,0,0,null,'second_units');
insert into raw_usage_field_mapping values('GPR',11,'Wholesale Rate',10,1,1,0,null,'third_units');
insert into raw_usage_field_mapping values('GPR',12,'Completion Code',1,1,1,0,null,'comp_status');
insert into raw_usage_field_mapping values('GPR',13,'Service Provider',5,1,1,0,null,'provider_id');
insert into raw_usage_field_mapping values('GPR',14,'Time Zone',3,1,1,0,null,'timezone');
insert into raw_usage_field_mapping values('GPR',15,'Transaction ID',20,3,0,0,null,'trans_id');
insert into raw_usage_field_mapping values('GPR',16,'Filler3',20,3,0,0,null,'customer_tag');
insert into raw_usage_field_mapping values('GPR',17,'Filler4',20,3,0,0,null,'ext_tracking_id');
insert into raw_usage_field_mapping values('GPR',18,'Amount',8,1,0,0,null,'amount');
insert into raw_usage_field_mapping values('GPR',19,'Federal Tax',8,1,0,0,null,'federal_tax');
insert into raw_usage_field_mapping values('GPR',20,'State Tax',8,1,0,0,null,'state_tax');
insert into raw_usage_field_mapping values('GPR',21,'County Tax',8,1,0,0,null,'county_tax');
insert into raw_usage_field_mapping values('GPR',22,'City Tax',8,1,0,0,null,'city_tax');
insert into raw_usage_field_mapping values('GPR',23,'Other Tax',8,1,0,0,null,'other_tax');
insert into raw_usage_field_mapping values('GPR',24,'Content Name',255,3,0,0,null,'annotation');

 dbms_output.put_line('Updated and Inserted Values for AWN SYSTEM_PARAMETERS Table');


  -- issue final commit and print dbms_output
  commit;
  dbms_output.put_line('Successfully completed script for Some initial values for RAW_USAGE_FIELD_MAPPING tables');
  dbms_output.put_line('COMMIT;');


exception
  when others then
    rollback;
      l_error_msg := '** Error '||substr(sqlerrm,1,240);
      dbms_output.put_line(l_error_msg);
      dbms_output.put_line('ROLLBACK;');
end;
/

exit rollback