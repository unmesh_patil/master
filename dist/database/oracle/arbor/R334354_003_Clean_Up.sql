set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
--spool /opt/app/kenanfx2/arbor/gcibin/deployconfigs/R334354_003_Clean_Up_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: R334354_003_Clean_Up.sql
prompt CA Ticket# : R334354
prompt QC Defect# : N/A
prompt System(s)  : K2 and K3
prompt Database(s): ALL
prompt Schema(s)  : ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Clean-up grants issued by ARBOR on standard C30ARBOR procs
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt .................: It is, however, completely rerunnable
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 12/04/2013 ##       R334354 - Initial development
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt ** It is, however, completely rerunnable
prompt *****************************************************************************************************
begin
-- get rid of any possible cross-grants on the two procs we're using...
begin execute immediate('revoke execute on c30arbor.c30_no_error from oneview_tech'); exception when others then null; end;
begin execute immediate('revoke debug on c30arbor.c30_no_error from oneview_tech'); exception when others then null; end;
begin execute immediate('revoke execute on c30arbor.c30_put_line from oneview_tech'); exception when others then null; end;
begin execute immediate('revoke debug on c30arbor.c30_put_line from oneview_tech'); exception when others then null; end;

-- make sure the "right" privileges are there...
c30_ddl_standard('c30_no_error',v_no_dml=>1);
c30_ddl_standard('c30_put_line',v_no_dml=>1);

-- life's good -- issue final commit and send to dbms_output
dbms_output.put_line('Successfully completed script for R334354_003_Clean_Up');
end;
/
--spool off
exit rollback