set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/200_006_config_awn_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: 200_006_config_awn.sql
prompt CA Ticket# : AWN
prompt QC Defect# : N/A
prompt System(s)  : K3 (C3B)
prompt Database(s): ALL 
prompt Schema(s)  : ARBOR
prompt Outage Req : No
prompt Depends on : Part of the 200_NNN_config_AWN series.
prompt 
prompt Purpose of Script: Load access_region_ref/values, point_class_ref/values and point_region_ref/values from K1
prompt Expected Outcome : Script is "All or Nothing", all inserts should be successful or rollback
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 08/30/2012 ##       Initial development
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing" i.e. if it encounters an error it will rollback entirely
prompt *****************************************************************************************************
whenever sqlerror exit rollback
begin
dbms_output.put(chr(10));

-- clear out all tables we're going to load (in opposite order)...
delete arbor.point_region_values;
c30_put_line(null,'Deleted '||sql%rowcount||' point_region_values row[s]');

delete arbor.point_region_ref;
c30_put_line(null,'Deleted '||sql%rowcount||' point_region_ref row[s]');

delete arbor.point_class_values;
c30_put_line(null,'Deleted '||sql%rowcount||' point_class_values row[s]');

delete arbor.point_class_ref;
c30_put_line(null,'Deleted '||sql%rowcount||' point_class_ref row[s]');

delete arbor.access_region_values;
c30_put_line(null,'Deleted '||sql%rowcount||' access_region_values row[s]');

delete arbor.access_region_ref;
c30_put_line(null,'Deleted '||sql%rowcount||' access_region_ref row[s]');

-- ACCESS_REGION_REF
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('120            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('122            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('124            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('126            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('128            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('130            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('132            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('133            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('134            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('136            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('138            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('140            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('220            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('222            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('224            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('226            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('228            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('230            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('232            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('234            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('236            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('238            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('240            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('242            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('244            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('246            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('248            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('250            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('252            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('254            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('256            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('320            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('322            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('324            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('325            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('326            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('328            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('330            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('332            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('334            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('336            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('338            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('340            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('342            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('344            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('346            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('348            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('350            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('352            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('354            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('356            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('358            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('360            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('362            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('364            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('366            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('368            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('370            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('374            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('376            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('420            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('422            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('424            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('426            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('428            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('430            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('432            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('434            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('436            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('438            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('440            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('442            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('444            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('446            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('448            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('44813          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('44814          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('44815          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('450            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45009          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45010          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45011          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45012          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('452            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45204          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45205          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('454            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45402          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45403          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('456            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45601          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('458            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45806          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45807          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('45808          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('460            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('46017          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('46018          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('462            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('464            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('466            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('468            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('470            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('472            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('474            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('476            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('477            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('478            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('480            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('482            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('484            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('486            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('488            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('490            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('492            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('520            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('521            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('522            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('524            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('526            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('528            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('530            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('532            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('534            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('536            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('538            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('540            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('542            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('544            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('546            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('548            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('550            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('552            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('554            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('556            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('558            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('560            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('562            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('564            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('566            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('568            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('570            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('620            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('624            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('626            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('628            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('630            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('632            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('634            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('635            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('636            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('638            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('640            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('644            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('646            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('648            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('650            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('652            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('654            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('656            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('658            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('660            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('664            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('666            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('668            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('670            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('672            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('674            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('676            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('720            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('721            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('722            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('724            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('726            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('728            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('730            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('732            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('734            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('736            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('738            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('740            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('820            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('822            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('824            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('826            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('828            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('830            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('832            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('834            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('870            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('871            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('884            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('888            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('920            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('921            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('922            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('923            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('924            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('927            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('928            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('929            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('932            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('937            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('938            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('93901          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('93902          ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('949            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('951            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('952            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('953            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('956            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('958            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('960            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('961            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('973            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('974            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('976            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('977            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('978            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('980            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('981            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('999            ', 0, 0);
insert into arbor.access_region_ref(access_region, is_default, is_internal)
 values('99999          ', 0, 0);
c30_put_line(null,'Inserted 223 access_region_ref rows');

-- ACCESS_REGION_VALUES
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('120            ', 1, null, 'MAINE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('122            ', 1, null, 'NEW HAMPSHIRE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('124            ', 1, null, 'VERMONT');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('126            ', 1, null, 'WESTERN MASS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('128            ', 1, null, 'EASTERN MASS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('130            ', 1, null, 'RHODE ISLAND');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('132            ', 1, null, 'NEW YORK METRO NY');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('133            ', 1, null, 'POUGHKEEPSIE NY');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('134            ', 1, null, 'ALBANY NEW YORK');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('136            ', 1, null, 'SYRACUSE NEW YORK');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('138            ', 1, null, 'BINGHAMTON NEW YORK');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('140            ', 1, null, 'BUFFALO NEW YORK');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('220            ', 1, null, 'ATLANTIC COASTAL NJ');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('222            ', 1, null, 'DELAWARE VALLEY NJ');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('224            ', 1, null, 'NORTH JERSEY NJ');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('226            ', 1, null, 'CAPITAL PENNSYLVANIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('228            ', 1, null, 'PHILADELPHIA PA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('230            ', 1, null, 'ALTOONA PENNSYLVANIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('232            ', 1, null, 'NORTHEAST - PA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('234            ', 1, null, 'PITTSBURGH PA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('236            ', 1, null, 'WASHINGTON DC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('238            ', 1, null, 'BALTIMORE MARYLAND');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('240            ', 1, null, 'HAGERSTOWN MARYLAND');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('242            ', 1, null, 'SALISBURY MARYLAND');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('244            ', 1, null, 'ROANOKE VIRGINIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('246            ', 1, null, 'CULPEPER VIRGINIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('248            ', 1, null, 'RICHMOND VIRGINIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('250            ', 1, null, 'LYNCHBURG VIRGINIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('252            ', 1, null, 'NORFOLK VIRGINIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('254            ', 1, null, 'CHARLESTON WV');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('256            ', 1, null, 'CLARKSBURG WV');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('320            ', 1, null, 'CLEVELAND OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('322            ', 1, null, 'YOUNGSTOWN OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('324            ', 1, null, 'COLUMBUS OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('325            ', 1, null, 'AKRON OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('326            ', 1, null, 'TOLEDO OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('328            ', 1, null, 'DAYTON OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('330            ', 1, null, 'EVANSVILLE INDIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('332            ', 1, null, 'SOUTH BEND INDIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('334            ', 1, null, 'AUBURN-HUNTINGTON IN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('336            ', 1, null, 'INDIANAPOLIS INDIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('338            ', 1, null, 'BLOOMINGTON INDIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('340            ', 1, null, 'DETROIT MICHIGAN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('342            ', 1, null, 'UPPER PENINSULA MI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('344            ', 1, null, 'SAGINAW MICHIGAN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('346            ', 1, null, 'LANSING MICHIGAN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('348            ', 1, null, 'GRAND RAPIDS MI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('350            ', 1, null, 'NORTHEAST WISCONSIN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('352            ', 1, null, 'NORTHWEST WISCONSIN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('354            ', 1, null, 'SOUTHWEST WISCONSIN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('356            ', 1, null, 'SOUTHEAST WISCONSIN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('358            ', 1, null, 'CHICAGO ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('360            ', 1, null, 'ROCKFORD ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('362            ', 1, null, 'CAIRO ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('364            ', 1, null, 'STERLING ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('366            ', 1, null, 'FORREST ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('368            ', 1, null, 'PEORIA ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('370            ', 1, null, 'CHAMPAIGN ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('374            ', 1, null, 'SPRINGFIELD ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('376            ', 1, null, 'QUINCY ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('420            ', 1, null, 'ASHEVILLE NC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('422            ', 1, null, 'CHARLOTTE NC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('424            ', 1, null, 'GREENSBORO NC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('426            ', 1, null, 'RALEIGH N CAROLINA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('428            ', 1, null, 'WILMINGTON NC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('430            ', 1, null, 'GREENVILLE SC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('432            ', 1, null, 'FLORENCE S CAROLINA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('434            ', 1, null, 'COLUMBIA S CAROLINA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('436            ', 1, null, 'CHARLESTON SC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('438            ', 1, null, 'ATLANTA GEORGIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('440            ', 1, null, 'SAVANNAH GEORGIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('442            ', 1, null, 'AUGUSTA GEORGIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('444            ', 1, null, 'ALBANY GEORGIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('446            ', 1, null, 'MACON GEORGIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('448            ', 1, null, 'PENSACOLA FLORIDA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('44813          ', 1, null, 'PENSACOLA FL WA-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('44814          ', 1, null, 'PENSACOLA FL CR-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('44815          ', 1, null, 'PENSACOLA FL FW-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('450            ', 1, null, 'PANAMA CITY FLORIDA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45009          ', 1, null, 'PANAMA CITY FL PC-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45010          ', 1, null, 'PANAMA CITY FL SJ-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45011          ', 1, null, 'PANAMA CITY FL QC-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45012          ', 1, null, 'PANAMA CITY FL MR-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('452            ', 1, null, 'JACKSONVILLE FL');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45204          ', 1, null, 'JACKSONVIL FL CL-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45205          ', 1, null, 'JACKSONVIL FL LO-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('454            ', 1, null, 'GAINESVILLE FL');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45402          ', 1, null, 'GAINESVILLE FL NW-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45403          ', 1, null, 'GAINESVILLE FL OL-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('456            ', 1, null, 'DAYTONA BEACH FL');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45601          ', 1, null, 'DAYTONA BCH FL PO-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('458            ', 1, null, 'ORLANDO FL');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45806          ', 1, null, 'ORLANDO FL OR-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45807          ', 1, null, 'ORLANDO FL LB-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('45808          ', 1, null, 'ORLANDO FL WI-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('460            ', 1, null, 'SOUTHEAST FLORIDA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('46017          ', 1, null, 'SOUTHEAST FL GG-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('46018          ', 1, null, 'SOUTHEAST FL GR-EA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('462            ', 1, null, 'LOUISVILLE KENTUCKY');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('464            ', 1, null, 'OWENSBORO KENTUCKY');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('466            ', 1, null, 'WINCHESTER KENTUCKY');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('468            ', 1, null, 'MEMPHIS TENNESSEE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('470            ', 1, null, 'NASHVILLE TENNESSEE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('472            ', 1, null, 'CHATTANOOGA TN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('474            ', 1, null, 'KNOXVILLE TENNESSEE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('476            ', 1, null, 'BIRMINGHAM ALABAMA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('477            ', 1, null, 'HUNTSVILLE ALABAMA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('478            ', 1, null, 'MONTGOMERY ALABAMA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('480            ', 1, null, 'MOBILE ALABAMA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('482            ', 1, null, 'JACKSON MISSISSIPPI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('484            ', 1, null, 'BILOXI MISSISSIPPI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('486            ', 1, null, 'SHREVEPORT LOUISIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('488            ', 1, null, 'LAFAYETTE LOUISIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('490            ', 1, null, 'NEW ORLEANS LA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('492            ', 1, null, 'BATON ROUGE LA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('520            ', 1, null, 'ST LOUIS MISSOURI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('521            ', 1, null, 'WESTPHALIA MISSOURI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('522            ', 1, null, 'SPRINGFIELD MISSOURI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('524            ', 1, null, 'KANSAS CITY MISSOURI');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('526            ', 1, null, 'FORT SMITH ARKANSAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('528            ', 1, null, 'LITTLE ROCK ARKANSAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('530            ', 1, null, 'PINE BLUFF ARKANSAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('532            ', 1, null, 'WICHITA KANSAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('534            ', 1, null, 'TOPEKA KANSAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('536            ', 1, null, 'OKLAHOMA CITY OK');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('538            ', 1, null, 'TULSA OKLAHOMA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('540            ', 1, null, 'EL PASO TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('542            ', 1, null, 'MIDLAND TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('544            ', 1, null, 'LUBBOCK TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('546            ', 1, null, 'AMARILLO TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('548            ', 1, null, 'WICHITA FALLS TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('550            ', 1, null, 'ABILENE TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('552            ', 1, null, 'DALLAS TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('554            ', 1, null, 'LONGVIEW TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('556            ', 1, null, 'WACO TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('558            ', 1, null, 'AUSTIN TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('560            ', 1, null, 'HOUSTON TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('562            ', 1, null, 'BEAUMONT TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('564            ', 1, null, 'CORPUS CHRISTI TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('566            ', 1, null, 'SAN ANTONIO TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('568            ', 1, null, 'BROWNSVILLE TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('570            ', 1, null, 'HEARNE TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('620            ', 1, null, 'ROCHESTER MINNESOTA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('624            ', 1, null, 'DULUTH MINNESOTA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('626            ', 1, null, 'ST CLOUD MINNESOTA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('628            ', 1, null, 'MINNEAPOLIS MINN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('630            ', 1, null, 'SIOUX CITY IA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('632            ', 1, null, 'DES MOINES IOWA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('634            ', 1, null, 'DAVENPORT IOWA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('635            ', 1, null, 'CEDAR RAPIDS IOWA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('636            ', 1, null, 'BRAINERD-FARGO ND');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('638            ', 1, null, 'BISMARK NORTH DAKOTA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('640            ', 1, null, 'SOUTH DAKOTA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('644            ', 1, null, 'OMAHA NEBRASKA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('646            ', 1, null, 'GRAND ISLAND NE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('648            ', 1, null, 'GREAT FALLS MT');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('650            ', 1, null, 'BILLINGS MT');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('652            ', 1, null, 'IDAHO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('654            ', 1, null, 'WYOMING');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('656            ', 1, null, 'DENVER CO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('658            ', 1, null, 'COLORADO SPRINGS CO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('660            ', 1, null, 'UTAH');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('664            ', 1, null, 'NEW MEXICO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('666            ', 1, null, 'PHOENIX ARIZONA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('668            ', 1, null, 'TUCSON ARIZONA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('670            ', 1, null, 'EUGENE OREGON');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('672            ', 1, null, 'PORTLAND OREGON');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('674            ', 1, null, 'SEATTLE WASHINGTON');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('676            ', 1, null, 'SPOKANE WASHINGTON');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('720            ', 1, null, 'RENO NEVADA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('721            ', 1, null, 'PAHRUMP NEVADA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('722            ', 1, null, 'SAN FRANCISCO CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('724            ', 1, null, 'CHICO CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('726            ', 1, null, 'SACRAMENTO CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('728            ', 1, null, 'FRESNO CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('730            ', 1, null, 'LOS ANGELES CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('732            ', 1, null, 'SAN DIEGO CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('734            ', 1, null, 'BAKERSFIELD CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('736            ', 1, null, 'MONTEREY CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('738            ', 1, null, 'STOCKTON CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('740            ', 1, null, 'SAN LUIS OBISPO CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('820            ', 1, null, 'PUERTO RICO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('822            ', 1, null, 'VIRGIN ISLANDS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('824            ', 1, null, 'BAHAMAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('826            ', 1, null, 'JAMAICA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('828            ', 1, null, 'DOMINICAN REPUBLIC');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('830            ', 1, null, 'OTHER CARIBBEAN IS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('832            ', 1, null, 'ALASKA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('834            ', 1, null, 'HAWAII');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('870            ', 1, null, 'NO MARIANA ISLANDS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('871            ', 1, null, 'GUAM');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('884            ', 1, null, 'AMERICAN SAMOA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('888            ', 1, null, 'CANADA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('920            ', 1, null, 'CONNECTICUT');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('921            ', 1, null, 'FISHERS ISLAND NY');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('922            ', 1, null, 'CINCINNATI OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('923            ', 1, null, 'LIMA-MANSFIELD OHIO');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('924            ', 1, null, 'ERIE PENNSYLVANIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('927            ', 1, null, 'HARRISONBURG VA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('928            ', 1, null, 'CHARLOTTESVILLE VA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('929            ', 1, null, 'EDINBURG VIRGINIA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('932            ', 1, null, 'BLUE FIELD WV');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('937            ', 1, null, 'RICHMOND INDIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('938            ', 1, null, 'TERRE HAUTE INDIANA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('93901          ', 1, null, 'AVON PARK - EA FL');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('93902          ', 1, null, 'FT MYERS - EA FL');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('949            ', 1, null, 'FAYETTEVILLE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('951            ', 1, null, 'ROCKY MOUNT');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('952            ', 1, null, 'TAMPA FLORIDA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('953            ', 1, null, 'TALAHASSEE FLORIDA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('956            ', 1, null, 'BRISTOL-JOHNSN CY TN');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('958            ', 1, null, 'LINCOLN NEBRASKA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('960            ', 1, null, 'COUER D-ALENE');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('961            ', 1, null, 'SAN ANGELO TEXAS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('973            ', 1, null, 'PALM SPRINGS CA');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('974            ', 1, null, 'ROCHESTER NEW YORK');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('976            ', 1, null, 'MATTOON ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('977            ', 1, null, 'MACOMB ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('978            ', 1, null, 'OLNEY ILLINOIS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('980            ', 1, null, 'NAVAJO TERRITORY AZ');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('981            ', 1, null, 'NAVAJO TERRITORY UT');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('999            ', 1, null, 'RESERVED FOR SVCS');
insert into arbor.access_region_values(access_region, language_code, short_display, display_value)
 values('99999          ', 1, null, 'LATA NOT APPLICABLE');
c30_put_line(null,'Inserted 223 access_region_values rows');

-- POINT_CLASS_REF
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(1, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(43, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(74, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(201, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(202, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(203, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(204, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(205, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(206, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(207, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(208, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(209, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(210, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(212, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(213, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(214, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(215, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(216, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(217, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(218, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(219, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(224, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(225, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(226, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(228, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(229, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(231, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(234, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(239, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(240, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(242, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(246, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(248, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(249, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(250, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(251, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(252, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(253, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(254, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(256, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(260, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(262, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(264, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(267, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(268, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(269, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(270, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(276, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(281, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(283, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(284, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(289, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(301, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(302, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(303, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(304, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(305, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(306, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(307, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(308, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(309, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(310, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(312, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(313, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(314, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(315, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(316, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(317, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(318, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(319, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(320, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(321, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(323, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(325, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(327, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(330, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(331, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(334, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(336, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(337, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(339, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(340, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(343, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(345, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(347, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(351, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(352, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(360, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(361, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(364, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(380, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(385, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(386, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(401, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(402, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(403, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(404, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(405, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(406, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(407, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(408, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(409, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(410, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(412, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(413, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(414, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(415, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(416, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(417, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(418, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(419, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(423, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(424, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(425, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(430, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(431, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(432, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(434, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(435, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(438, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(440, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(441, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(442, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(443, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(445, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(450, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(458, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(469, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(470, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(473, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(475, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(478, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(479, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(480, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(484, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(500, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(501, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(502, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(503, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(504, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(505, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(506, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(507, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(508, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(509, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(510, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(512, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(513, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(514, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(515, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(516, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(517, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(518, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(519, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(520, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(530, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(531, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(533, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(534, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(539, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(540, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(541, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(544, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(551, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(557, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(559, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(561, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(562, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(563, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(564, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(566, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(567, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(570, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(571, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(573, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(574, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(575, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(579, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(580, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(581, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(585, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(586, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(587, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(601, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(602, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(603, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(604, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(605, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(606, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(607, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(608, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(609, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(610, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(612, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(613, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(614, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(615, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(616, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(617, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(618, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(619, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(620, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(623, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(626, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(628, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(630, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(631, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(636, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(639, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(641, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(646, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(647, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(649, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(650, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(651, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(657, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(660, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(661, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(662, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(664, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(667, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(669, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(670, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(671, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(678, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(681, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(682, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(684, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(700, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(701, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(702, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(703, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(704, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(705, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(706, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(707, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(708, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(709, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(710, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(712, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(713, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(714, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(715, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(716, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(717, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(718, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(719, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(720, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(721, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(724, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(727, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(731, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(732, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(734, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(737, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(740, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(747, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(754, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(757, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(758, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(760, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(762, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(763, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(764, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(765, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(767, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(769, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(770, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(772, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(773, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(774, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(775, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(778, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(779, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(780, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(781, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(784, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(785, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(786, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(787, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(801, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(802, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(803, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(804, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(805, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(806, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(807, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(808, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(809, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(810, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(812, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(813, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(814, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(815, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(816, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(817, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(818, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(819, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(828, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(829, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(830, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(831, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(832, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(835, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(843, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(845, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(847, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(848, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(849, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(850, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(856, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(857, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(858, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(859, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(860, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(862, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(863, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(864, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(865, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(867, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(868, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(869, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(870, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(872, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(873, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(876, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(878, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(900, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(901, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(902, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(903, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(904, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(905, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(906, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(907, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(908, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(909, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(910, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(912, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(913, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(914, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(915, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(916, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(917, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(918, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(919, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(920, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(925, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(928, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(929, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(931, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(936, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(937, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(938, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(939, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(940, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(941, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(947, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(949, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(951, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(952, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(954, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(956, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(959, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(970, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(971, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(972, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(973, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(978, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(979, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(980, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(984, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(985, 0, 0);
insert into arbor.point_class_ref(point_class, is_default, is_internal)
 values(989, 0, 0);
c30_put_line(null,'Inserted 374 point_class_ref rows');

-- POINT_CLASS_VALUES
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(1, 1, null, 'Default Point Class');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(43, 1, null, '043');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(74, 1, null, '74');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(201, 1, null, '201');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(202, 1, null, '202');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(203, 1, null, '203');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(204, 1, null, '204');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(205, 1, null, '205');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(206, 1, null, '206');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(207, 1, null, '207');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(208, 1, null, '208');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(209, 1, null, '209');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(210, 1, null, '210');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(212, 1, null, '212');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(213, 1, null, '213');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(214, 1, null, '214');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(215, 1, null, '215');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(216, 1, null, '216');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(217, 1, null, '217');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(218, 1, null, '218');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(219, 1, null, '219');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(224, 1, null, '224');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(225, 1, null, '225');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(226, 1, null, '226');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(228, 1, null, '228');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(229, 1, null, '229');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(231, 1, null, '231');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(234, 1, null, '234');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(239, 1, null, '239');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(240, 1, null, '240');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(242, 1, null, '242');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(246, 1, null, '246');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(248, 1, null, '248');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(249, 1, null, '249');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(250, 1, null, '250');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(251, 1, null, '251');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(252, 1, null, '252');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(253, 1, null, '253');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(254, 1, null, '254');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(256, 1, null, '256');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(260, 1, null, '260');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(262, 1, null, '262');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(264, 1, null, '264');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(267, 1, null, '267');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(268, 1, null, '268');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(269, 1, null, '269');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(270, 1, null, '270');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(276, 1, null, '276');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(281, 1, null, '281');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(283, 1, null, '283');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(284, 1, null, '284');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(289, 1, null, '289');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(301, 1, null, '301');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(302, 1, null, '302');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(303, 1, null, '303');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(304, 1, null, '304');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(305, 1, null, '305');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(306, 1, null, '306');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(307, 1, null, '307');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(308, 1, null, '308');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(309, 1, null, '309');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(310, 1, null, '310');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(312, 1, null, '312');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(313, 1, null, '313');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(314, 1, null, '314');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(315, 1, null, '315');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(316, 1, null, '316');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(317, 1, null, '317');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(318, 1, null, '318');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(319, 1, null, '319');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(320, 1, null, '320');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(321, 1, null, '321');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(323, 1, null, '323');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(325, 1, null, '325');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(327, 1, null, '327');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(330, 1, null, '330');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(331, 1, null, '331');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(334, 1, null, '334');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(336, 1, null, '336');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(337, 1, null, '337');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(339, 1, null, '339');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(340, 1, null, '340');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(343, 1, null, '343');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(345, 1, null, '345');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(347, 1, null, '347');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(351, 1, null, '351');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(352, 1, null, '352');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(360, 1, null, '360');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(361, 1, null, '361');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(364, 1, null, '364');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(380, 1, null, '380');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(385, 1, null, '385');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(386, 1, null, '386');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(401, 1, null, '401');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(402, 1, null, '402');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(403, 1, null, '403');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(404, 1, null, '404');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(405, 1, null, '405');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(406, 1, null, '406');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(407, 1, null, '407');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(408, 1, null, '408');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(409, 1, null, '409');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(410, 1, null, '410');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(412, 1, null, '412');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(413, 1, null, '413');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(414, 1, null, '414');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(415, 1, null, '415');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(416, 1, null, '416');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(417, 1, null, '417');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(418, 1, null, '418');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(419, 1, null, '419');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(423, 1, null, '423');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(424, 1, null, '424');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(425, 1, null, '425');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(430, 1, null, '430');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(431, 1, null, '431');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(432, 1, null, '432');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(434, 1, null, '434');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(435, 1, null, '435');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(438, 1, null, '438');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(440, 1, null, '440');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(441, 1, null, '441');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(442, 1, null, '442');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(443, 1, null, '443');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(445, 1, null, '445');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(450, 1, null, '450');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(458, 1, null, '458');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(469, 1, null, '469');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(470, 1, null, '470');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(473, 1, null, '473');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(475, 1, null, '475');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(478, 1, null, '478');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(479, 1, null, '479');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(480, 1, null, '480');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(484, 1, null, '484');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(500, 1, null, '500');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(501, 1, null, '501');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(502, 1, null, '502');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(503, 1, null, '503');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(504, 1, null, '504');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(505, 1, null, '505');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(506, 1, null, '506');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(507, 1, null, '507');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(508, 1, null, '508');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(509, 1, null, '509');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(510, 1, null, '510');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(512, 1, null, '512');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(513, 1, null, '513');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(514, 1, null, '514');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(515, 1, null, '515');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(516, 1, null, '516');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(517, 1, null, '517');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(518, 1, null, '518');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(519, 1, null, '519');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(520, 1, null, '520');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(530, 1, null, '530');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(531, 1, null, '531');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(533, 1, null, '533');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(534, 1, null, '534');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(539, 1, null, '539');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(540, 1, null, '540');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(541, 1, null, '541');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(544, 1, null, '544');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(551, 1, null, '551');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(557, 1, null, '557');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(559, 1, null, '559');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(561, 1, null, '561');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(562, 1, null, '562');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(563, 1, null, '563');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(564, 1, null, '564');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(566, 1, null, '566');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(567, 1, null, '567');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(570, 1, null, '570');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(571, 1, null, '571');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(573, 1, null, '573');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(574, 1, null, '574');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(575, 1, null, '575');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(579, 1, null, '579');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(580, 1, null, '580');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(581, 1, null, '581');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(585, 1, null, '585');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(586, 1, null, '586');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(587, 1, null, '587');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(601, 1, null, '601');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(602, 1, null, '602');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(603, 1, null, '603');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(604, 1, null, '604');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(605, 1, null, '605');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(606, 1, null, '606');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(607, 1, null, '607');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(608, 1, null, '608');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(609, 1, null, '609');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(610, 1, null, '610');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(612, 1, null, '612');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(613, 1, null, '613');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(614, 1, null, '614');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(615, 1, null, '615');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(616, 1, null, '616');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(617, 1, null, '617');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(618, 1, null, '618');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(619, 1, null, '619');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(620, 1, null, '620');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(623, 1, null, '623');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(626, 1, null, '626');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(628, 1, null, '628');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(630, 1, null, '630');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(631, 1, null, '631');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(636, 1, null, '636');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(639, 1, null, '639');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(641, 1, null, '641');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(646, 1, null, '646');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(647, 1, null, '647');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(649, 1, null, '649');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(650, 1, null, '650');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(651, 1, null, '651');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(657, 1, null, '657');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(660, 1, null, '660');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(661, 1, null, '661');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(662, 1, null, '662');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(664, 1, null, '664');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(667, 1, null, '667');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(669, 1, null, '669');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(670, 1, null, '670');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(671, 1, null, '671');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(678, 1, null, '678');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(681, 1, null, '681');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(682, 1, null, '682');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(684, 1, null, '684');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(700, 1, null, '700');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(701, 1, null, '701');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(702, 1, null, '702');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(703, 1, null, '703');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(704, 1, null, '704');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(705, 1, null, '705');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(706, 1, null, '706');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(707, 1, null, '707');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(708, 1, null, '708');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(709, 1, null, '709');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(710, 1, null, '710');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(712, 1, null, '712');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(713, 1, null, '713');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(714, 1, null, '714');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(715, 1, null, '715');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(716, 1, null, '716');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(717, 1, null, '717');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(718, 1, null, '718');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(719, 1, null, '719');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(720, 1, null, '720');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(721, 1, null, '721');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(724, 1, null, '724');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(727, 1, null, '727');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(731, 1, null, '731');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(732, 1, null, '732');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(734, 1, null, '734');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(737, 1, null, '737');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(740, 1, null, '740');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(747, 1, null, '747');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(754, 1, null, '754');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(757, 1, null, '757');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(758, 1, null, '758');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(760, 1, null, '760');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(762, 1, null, '762');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(763, 1, null, '763');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(764, 1, null, '764');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(765, 1, null, '765');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(767, 1, null, '767');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(769, 1, null, '769');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(770, 1, null, '770');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(772, 1, null, '772');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(773, 1, null, '773');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(774, 1, null, '774');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(775, 1, null, '775');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(778, 1, null, '778');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(779, 1, null, '779');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(780, 1, null, '780');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(781, 1, null, '781');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(784, 1, null, '784');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(785, 1, null, '785');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(786, 1, null, '786');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(787, 1, null, '787');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(801, 1, null, '801');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(802, 1, null, '802');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(803, 1, null, '803');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(804, 1, null, '804');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(805, 1, null, '805');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(806, 1, null, '806');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(807, 1, null, '807');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(808, 1, null, '808');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(809, 1, null, '809');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(810, 1, null, '810');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(812, 1, null, '812');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(813, 1, null, '813');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(814, 1, null, '814');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(815, 1, null, '815');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(816, 1, null, '816');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(817, 1, null, '817');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(818, 1, null, '818');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(819, 1, null, '819');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(828, 1, null, '828');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(829, 1, null, '829');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(830, 1, null, '830');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(831, 1, null, '831');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(832, 1, null, '832');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(835, 1, null, '835');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(843, 1, null, '843');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(845, 1, null, '845');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(847, 1, null, '847');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(848, 1, null, '848');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(849, 1, null, '849');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(850, 1, null, '850');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(856, 1, null, '856');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(857, 1, null, '857');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(858, 1, null, '858');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(859, 1, null, '859');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(860, 1, null, '860');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(862, 1, null, '862');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(863, 1, null, '863');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(864, 1, null, '864');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(865, 1, null, '865');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(867, 1, null, '867');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(868, 1, null, '868');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(869, 1, null, '869');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(870, 1, null, '870');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(872, 1, null, '872');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(873, 1, null, '873');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(876, 1, null, '876');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(878, 1, null, '878');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(900, 1, null, '900');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(901, 1, null, '901');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(902, 1, null, '902');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(903, 1, null, '903');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(904, 1, null, '904');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(905, 1, null, '905');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(906, 1, null, '906');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(907, 1, null, '907');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(908, 1, null, '908');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(909, 1, null, '909');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(910, 1, null, '910');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(912, 1, null, '912');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(913, 1, null, '913');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(914, 1, null, '914');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(915, 1, null, '915');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(916, 1, null, '916');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(917, 1, null, '917');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(918, 1, null, '918');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(919, 1, null, '919');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(920, 1, null, '920');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(925, 1, null, '925');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(928, 1, null, '928');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(929, 1, null, '929');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(931, 1, null, '931');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(936, 1, null, '936');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(937, 1, null, '937');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(938, 1, null, '938');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(939, 1, null, '939');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(940, 1, null, '940');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(941, 1, null, '941');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(947, 1, null, '947');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(949, 1, null, '949');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(951, 1, null, '951');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(952, 1, null, '952');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(954, 1, null, '954');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(956, 1, null, '956');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(959, 1, null, '959');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(970, 1, null, '970');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(971, 1, null, '971');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(972, 1, null, '972');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(973, 1, null, '973');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(978, 1, null, '978');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(979, 1, null, '979');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(980, 1, null, '980');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(984, 1, null, '984');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(985, 1, null, '985');
insert into arbor.point_class_values(point_class, language_code, short_display, display_value)
 values(989, 1, null, '989');
c30_put_line(null,'Inserted 374 point_class_values rows');

-- POINT_REGION_REF
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('700                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('AIN                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('ATC                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('BLG                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('BRD                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('CDA                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('CTV                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('DFT                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('ENP                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('EOC                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('FGB                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('GCI                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('HVL                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('INP                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('INT                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('INTLLAND                ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('INTLMOB                 ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('L48                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('N11                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('OCN                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('ONA                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('PLN                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('PMC                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('RCC                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('RTG                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('SIC                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('SP1                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('SP2                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('TST                     ', 0, 0);
insert into arbor.point_region_ref(point_region, is_default, is_internal)
 values('UFA                     ', 0, 0);
c30_put_line(null,'Inserted 30 point_region_ref rows');

-- POINT_REGION_VALUES
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('700                     ', 1, null, '700');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('AIN                     ', 1, null, 'AIN');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('ATC                     ', 1, null, 'ATC');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('BLG                     ', 1, null, 'BLG');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('BRD                     ', 1, null, 'BRD');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('CDA                     ', 1, null, 'CDA');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('CTV                     ', 1, null, 'CTV');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('DFT                     ', 1, null, 'Default Point Region');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('ENP                     ', 1, null, 'ENP');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('EOC                     ', 1, null, 'EOC');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('FGB                     ', 1, null, 'FGB');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('GCI                     ', 1, null, 'GCI');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('HVL                     ', 1, null, 'HVL');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('INP                     ', 1, null, 'INP');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('INT                     ', 1, null, 'International');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('INTLLAND                ', 1, null, 'International Landline Termination');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('INTLMOB                 ', 1, null, 'International Mobile Termination');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('L48                     ', 1, null, 'L48');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('N11                     ', 1, null, 'N11');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('OCN                     ', 1, null, 'OCN');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('ONA                     ', 1, null, 'ONA');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('PLN                     ', 1, null, 'PLN');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('PMC                     ', 1, null, 'PMC');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('RCC                     ', 1, null, 'RCC');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('RTG                     ', 1, null, 'RTG');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('SIC                     ', 1, null, 'SIC');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('SP1                     ', 1, null, 'SP1');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('SP2                     ', 1, null, 'SP2');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('TST                     ', 1, null, 'TST');
insert into arbor.point_region_values(point_region, language_code, short_display, display_value)
 values('UFA                     ', 1, null, 'UFA');
c30_put_line(null,'Inserted 30 point_region_values rows');

-- life's good -- issue final commit and send to dbms_output
commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script for 200_006_config_awn');

exception
   when others then
      rollback;
      c30_put_line(null,'rollback;');
      c30_put_line(null,sqlerrm);
      raise;
end;
/
--spool off
-- the commit is already done in the script if successful...
exit rollback