set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/cycbin/deployconfigs/200_023_config_awn_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: 200_023_config_awn.sql
prompt CA Ticket# : N/A
prompt QC Defect# : Defect 279
prompt System(s)  : K3
prompt Database(s): AWN
prompt Schema(s)  : ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt 
prompt Purpose of Script: Defect 279 - Insert missing bill class referential rows (501, 502, and 507)
prompt Expected Outcome : Script is "All or Nothing", all inserts should be successful or rollback
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 11/19/2012 ##       Initial development
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing" i.e. if it encounters an error it will rollback entirely
prompt *****************************************************************************************************
whenever sqlerror exit rollback
begin
dbms_output.put(chr(10));

-- bill classes
-- BILL_CLASS_REF
insert into arbor.bill_class_ref values (501,0,0,0);
insert into arbor.bill_class_ref values (502,0,0,0);
insert into arbor.bill_class_ref values (507,0,0,0);
c30_put_line(null,'Inserted 3 bill_class_ref rows (501,502,507)');

-- BILL_CLASS_VALUES
insert into arbor.bill_class_values values (501,1,null,'Mobile to Mobile');
insert into arbor.bill_class_values values (502,1,null,'Non Mobile to Mobile');
insert into arbor.bill_class_values values (507,1,null,'DA without Call Completion');
c30_put_line(null,'Inserted 3 bill_class_values rows (501,502,507)');

-- life's good -- issue final commit and send to dbms_output
commit;
c30_put_line(null,'commit;');
c30_put_line(null,'Successfully completed script for 200_023_config_awn');

exception
   when others then
      rollback;
      c30_put_line(null,'rollback;');
      c30_put_line(null,sqlerrm);
      raise;
end;
/
--spool off
exit rollback