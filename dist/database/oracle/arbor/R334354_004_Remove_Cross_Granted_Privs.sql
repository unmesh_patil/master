set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
--spool /opt/app/kenanfx2/arbor/gcibin/deployconfigs/R334354_004_Remove_Cross_Granted_Privs_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: R334354_004_Remove_Cross_Granted_Privs.sql
prompt CA Ticket# : R334354
prompt QC Defect# : N/A
prompt System(s)  : K2 and K3
prompt Database(s): ALL
prompt Schema(s)  : ARBOR
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Remove "Cross-Granted" Privileges (Issued by ARBOR on C30ARBOR Objects)
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt .................: It is, however, completely rerunnable
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 12/04/2013 ##       R334354 - Initial development
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt ** It is, however, completely rerunnable
prompt *****************************************************************************************************
declare
   l_sqlerrm   varchar2(2000);

   cursor c_sql is
   select distinct
          table_name,owner
        , lower('c30_ddl_standard('''||table_name||''','''||owner||''');') exec_proc
     from dba_tab_privs
    where owner     IN('ARBOR','GCIARBOR','C30ARBOR','GCIREPORT','C30REPORT','C30TEMP')
      and grantor    = user --**** YOU MUST BE LOGGED ON AS THE "GRANTOR" TO DROP THESE CROSS-GRANTED PRIVILEGES! ****
      and owner     != grantor
      and grantee   IN('ARBOR','GCIARBOR','C30ARBOR','GCIREPORT','C30REPORT','C30TEMP','ONEVIEW_TECH','ONEVIEW_SELECTALL')
      and table_name = upper(table_name) -- avoids oddly named Recycle Bin oddballs in K2/K3
      -- TESTING and rownum < 6
   ;
begin
dbms_output.put(chr(10));

if user not IN('ARBOR','GCIARBOR','C30ARBOR','GCIREPORT','C30REPORT','C30TEMP') then
   raise_application_error(-20001,'Must be logged on as "ARBOR", "GCIARBOR", "C30ARBOR", "GCIREPORT", "C30REPORT" or "C30TEMP"');
end if;

for l_sql IN c_sql loop
   -- wipe out the cross-grants...
   for l_gnt IN ( select lower('grant '||privilege||' on '||owner||'.'||table_name||' to '||grantee) grant_priv
                    from dba_tab_privs
                   where owner      = l_sql.owner
                     and grantor    = user --**** YOU MUST BE LOGGED ON AS THE "GRANTOR" TO DROP THESE CROSS-GRANTED PRIVILEGES! ****
                     and owner     != grantor
                     and grantee   IN('ARBOR','GCIARBOR','C30ARBOR','GCIREPORT','C30REPORT','C30TEMP','ONEVIEW_TECH','ONEVIEW_SELECTALL')
                     and table_name = l_sql.table_name
                ) loop
      c30_put_line(null,'*************================> WILL REVOKE GRANT ISSUED BY '||user||': '||l_gnt.grant_priv||';');
   end loop;

   for l_rvk IN ( select lower('revoke '||privilege||' on '||owner||'.'||table_name||' from '||grantee) revoke_priv
                    from dba_tab_privs
                   where owner      = l_sql.owner
                     and grantor    = user --**** YOU MUST BE LOGGED ON AS THE "GRANTOR" TO DROP THESE CROSS-GRANTED PRIVILEGES! ****
                     and owner     != grantor
                     and grantee   IN('ARBOR','GCIARBOR','C30ARBOR','GCIREPORT','C30REPORT','C30TEMP','ONEVIEW_TECH','ONEVIEW_SELECTALL')
                     and table_name = l_sql.table_name
                ) loop
      begin
      --c30_put_line(null,'*************================> EXECUTING "'||l_rvk.revoke_priv||'" <================*************');
      c30_no_error(l_rvk.revoke_priv,l_sqlerrm);
      c30_put_line(null,nvl(l_sqlerrm,'Revoke succeeded.'));
      exception
         when others then
            c30_put_line(null,'*FAILED!!!!* "'||l_sqlerrm||'"');
      end;
   end loop;

   -- now, run the "right" routine...
   begin
   c30_put_line(null,'*************================> EXECUTING "'||l_sql.exec_proc||'" <================*************');
   c30_ddl_standard(l_sql.table_name,l_sql.owner);
   exception
      when others then
         c30_put_line(null,'*FAILED!!!!* "'||l_sqlerrm||'"');
         raise;
   end;
end loop;

-- life's good -- issue final commit and send to dbms_output
dbms_output.put_line('Successfully completed script for R334354_004_Remove_Cross_Granted_Privs');
end;
/
--spool off
exit rollback