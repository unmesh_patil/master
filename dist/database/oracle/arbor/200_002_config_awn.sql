set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
prompt *****************************************************************************************************
prompt Release Notes
prompt PROJECT: AWN - Alaska Wireless Network
prompt Date: 7-20-2012
prompt Created By: Steve Brock
prompt .
prompt No Outage is required
prompt .
prompt Defect Resolved: A.W.N.Data Plans and Usage Types 981 and 983
prompt .
prompt    
prompt Description: A.W.N.Data Plans and Usage Types 981 and 983
prompt .
prompt    
prompt .  
prompt
prompt ENVIRONMENT: C3B only  
prompt .  
prompt  
prompt SPECIAL INSTRUCTIONS: Contact Steve Brock with issues
prompt .
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing" i.e. if it encounters an error it will rollback entirely
prompt *****************************************************************************************************


--INSERT AND UPDATES TO EXISTING TABLES


declare
  l_error_msg                         varchar2(255); -- max dbms_output line length

begin

  -- components - created first in case member is rated by component_id
  -- COMPONENT_DEFINITION_REF (component_level: 1 = acct, 2 = serv)
  -- COMPONENT_DEFINITION_VALUES
  -- COMPONENT_CMF_ELIGIBILITY or COMPONENT_EMF_ELIGIBILITY
  -- GCI_WIRELESS_FEATURE_MAP or GCI_CW_FEATURE_SET_MAP - GCI_CW_DEFAULT_FEATURE_SET      --plan fees or features, anything that requires provisioning, unique per service, depending on emf config id

insert into component_definition_ref values ('6','2',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0','0');
insert into component_definition_ref values ('7','2',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0','0');
insert into component_definition_ref values ('8','2',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0','0');
insert into component_definition_ref values ('9','2',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0','0');
insert into component_definition_values values ('6','1','','3,000 MB Data');
insert into component_definition_values values ('7','1','','5,000 MB Data');
insert into component_definition_values values ('8','1','','10,000 MB Data');
insert into component_definition_values values ('9','1','','20,000 MB Data');

 dbms_output.put_line('Inserted Values for COMPONENT Tables');


  -- usage types
  -- DESCRIPTIONS
  -- RATABLE_UNIT_CLASS_REF
  -- RATABLE_UNIT_CLASS_VALUES
  -- POINT_CATEGORY_REF
  -- POINT_CATEGORY_VALUES
  -- USAGE_TYPES

insert into descriptions values ('1009','1','0','GPRS','');
insert into descriptions values ('1010','1','0','SMS','');
insert into ratable_unit_class_ref values ('3','0',0);
insert into ratable_unit_class_values values ('3','1','','KB','Kilobytes');
insert into point_category_ref values ('1','0','0');
insert into point_category_values values ('1','1','','Phone Number');
insert into usage_types values ('983','3','1','0','0','0','1009','','0','','0','0','','','1279','2','1','3','1','0','0','0','0','0','1','2','200','0','0','1','1','0','0','0','2','0','0','0','0','0','','1','0','0','0','','0','90','0','1','0');
insert into usage_types values ('981','3','1','0','0','0','1010','','0','','0','0','','','1279','2','1','1','1','0','0','0','0','0','1','2','2','0','2','2','1','1','0','0','2','0','0','0','0','0','','1','0','0','0','','0','90','0','1','0');


  -- usage type groups
  -- DESCRIPTIONS
  -- USAGE_TYPE_UNITS
  -- USAGE_TYPE_GROUPS

insert into descriptions values ('1011','1','0','Data Usage Type Group','');
insert into descriptions values ('1012','1','0','Messaging Usage Type Group','');
insert into usage_types_units values ('983','','','','','','201',0,'1','0','0');
insert into usage_types_units values ('981','','','','','','2',0,'1','0','0');
insert into usage_type_groups values ('907','983','1011');
insert into usage_type_groups values ('901','981','1012');

  -- usage points
  -- USAGE_POINTS_TEXT
  -- USAGE_POINTS

  -- rate periods
  -- RATE_PERIOD_REF
  -- RATE_PERIOD_VALUES

  -- bill classes
  -- BILL_CLASS_REF
  -- BILL_CLASS_VALUES

insert into bill_class_ref values ('505','0','0','0');
insert into bill_class_ref values ('506','0','0','0');
insert into bill_class_values values ('505','1','','Incoming Message');
insert into bill_class_values values ('506','1','','Outgoing Message');

  -- point classes
  -- POINT_CLASS_REF
  -- POINT_CLASS_VALUES

insert into point_class_ref values ('1','0','0');
insert into point_class_values values ('1','1','','Default Point Class');

  -- jurisdictions
  -- DESCRIPTIONS
  -- JURISDICTION_CLASS_REF
  -- JURISDICTION_CLASS_VALUES
  -- JURISDICTIONS

insert into descriptions values ('1013','1','0','Domestic Message','');
insert into descriptions values ('1014','1','0','International Message','');
insert into descriptions values ('1015','1','0','Data','');
insert into jurisdiction_class_ref values ('1','1','0');
insert into jurisdiction_class_ref values ('2','0','0');
insert into jurisdiction_class_ref values ('3','0','0');
insert into jurisdiction_class_values values ('1','1','','Intrastate');
insert into jurisdiction_class_values values ('2','1','','Interstate');
insert into jurisdiction_class_values values ('3','1','','International');
insert into jurisdictions values ('10805','1013','2');
insert into jurisdictions values ('10806','1014','2');
insert into jurisdictions values ('10807','1015','1');

 dbms_output.put_line('Inserted Values for USAGE Tables');

  -- products
  -- DESCRIPTIONS
  -- PRODUCT ELEMENTS (level_code: 1 = acct, 2 = serv, 0 = either)
  -- RATE_RC (do NOT insert rate if ICB)

insert into descriptions values ('1005','1','0','3,000 MB Data','');
insert into descriptions values ('1006','1','0','5,000 MB Data','');
insert into descriptions values ('1007','1','0','10,000 MB Data','');
insert into descriptions values ('1008','1','0','20,000 MB Data','');
update product_elements set type_group_usg = 901 where element_id = 3;
insert into product_elements values ('4','1005','1','1','0','0','0','907','0','1','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'','1','2','0','1','1','1','1','0','0','0','0','0','1','0','','','1','0','0','1','0','0','0');
insert into product_elements values ('5','1006','1','1','0','0','0','907','0','1','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'','1','2','0','1','1','1','1','0','0','0','0','0','1','0','','','1','0','0','1','0','0','0');
insert into product_elements values ('6','1007','1','1','0','0','0','907','0','1','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'','1','2','0','1','1','1','1','0','0','0','0','0','1','0','','','1','0','0','1','0','0','0');
insert into product_elements values ('7','1008','1','1','0','0','0','907','0','1','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'','1','2','0','1','1','1','1','0','0','0','0','0','1','0','','','1','0','0','1','0','0','0');
insert into rate_rc values ('0','4','6','0','0','0','1','3','1','1999','0','0','0','0','','0','0','','0','2',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','');
insert into rate_rc values ('0','5','7','0','0','0','1','3','1','2999','0','0','0','0','','0','0','','0','2',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','');
insert into rate_rc values ('0','6','8','0','0','0','1','3','1','5999','0','0','0','0','','0','0','','0','2',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','');
insert into rate_rc values ('0','7','9','0','0','0','1','3','1','9999','0','0','0','0','','0','0','','0','2',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','');

 dbms_output.put_line('Inserted Values for PRODUCT Tables');

  -- define usage splits, rate periods, jurisdictions and usage rates
  -- RATE_USAGE_IMPLIED_DECIMALS
  -- USAGE_TYPES_SPLIT
  -- RATE_PERIODS
  -- RATE_USAGE
  -- AGGR_USAGE_DEFINITIONS_REF
  -- AGGR_USAGE_DEFINITIONS_VALUES
  -- RATE_AGGR_USAGE
  -- AGGR_USAGE_RESTRICTIONS
  -- USAGE_JURISDICTION
  -- GCI_WIRELESS_ROAMING_MAP: to identify whether a call coming in is considered roaming from a plan perspective
  -- GCI_WIRELESS_ELEMENT_KEY
       -- per Chavaunn Ingersol
       -- We only charge roaming for data when customers are roaming Internationally
       -- data roaming in Canada is the same rate as roaming in California is the same as roaming in Fairbanks, AK
  -- EVDO usage probably requires the USG AGGREGATE tables to be populated

insert into rate_usage_implied_decimals values ('1','4');
insert into rate_usage values ('1','0','981','505','10805','0','0','1','0',' ','1','3','0','0','0','0','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'ARBOR                         ','1','2','0','0','4','0','0','2','','0');
insert into rate_usage values ((select max(seqnum)+1 from rate_usage),'0','981','505','10806','0','0','1','0',' ','1','3','0','0','0','0','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'ARBOR                         ','1','2','0','0','4','0','0','2','','0');
insert into rate_usage values ((select max(seqnum)+1 from rate_usage),'0','981','506','10805','0','0','1','0',' ','1','3','0','0','0','0','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'ARBOR                         ','1','2','0','0','4','0','0','2','','0');
insert into rate_usage values ((select max(seqnum)+1 from rate_usage),'0','981','506','10806','0','0','1','0',' ','1','3','0','0','0','0','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'ARBOR                         ','1','2','0','0','4','0','0','2','','0');
insert into aggr_usage_definitions_ref values ('1','6','1','2','',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'0','0');
insert into aggr_usage_definitions_ref values ('2','6','1','2','',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'0','0');
insert into aggr_usage_definitions_ref values ('3','6','1','2','',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'0','0');
insert into aggr_usage_definitions_ref values ('4','6','1','2','',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),'0','0');
insert into aggr_usage_definitions_values values ('1','1','','3000 MB Data');
insert into aggr_usage_definitions_values values ('2','1','','5000 MB Data');
insert into aggr_usage_definitions_values values ('3','1','','10000 MB Data');
insert into aggr_usage_definitions_values values ('4','1','','20000 MB Data');
insert into rate_aggr_usage values ('1','3','1','3072001','1','0','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','3072001','4096001','1','1500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','4096001','5120001','1','3000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','5120001','6144001','1','4500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','6144001','7168001','1','6000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','7168001','8192001','1','7500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','8192001','9216001','1','9000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','9216001','10240001','1','10500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','10240001','11264001','1','12000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','11264001','12288001','1','13500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','12288001','13312001','1','15000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','13312001','14336001','1','16500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','14336001','15360001','1','18000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','15360001','16384001','1','19500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','16384001','17408001','1','21000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','17408001','18432001','1','22500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','18432001','19456001','1','24000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','19456001','20480001','1','25500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','20480001','21504001','1','27000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','21504001','22528001','1','28500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','22528001','23552001','1','30000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','23552001','24576001','1','31500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','24576001','25600001','1','33000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','25600001','26624001','1','34500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','26624001','27648001','1','36000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','27648001','28672001','1','37500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','28672001','29696001','1','39000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','29696001','30720001','1','40500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','30720001','31744001','1','42000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','31744001','32768001','1','43500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','32768001','33792001','1','45000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','33792001','34816001','1','46500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','34816001','35840001','1','48000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','35840001','36864001','1','49500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','36864001','37888001','1','51000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','37888001','38912001','1','52500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','38912001','39936001','1','54000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','39936001','40960001','1','55500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','40960001','41984001','1','57000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','41984001','43008001','1','58500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','43008001','44032001','1','60000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','44032001','45056001','1','61500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','45056001','46080001','1','63000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','46080001','47104001','1','64500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','47104001','48128001','1','66000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','48128001','49152001','1','67500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','49152001','50176001','1','69000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','50176001','51200001','1','70500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','51200001','52224001','1','72000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','52224001','53248001','1','73500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','53248001','54272001','1','75000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','54272001','55296001','1','76500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','55296001','56320001','1','78000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','56320001','57344001','1','79500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','57344001','58368001','1','81000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','58368001','59392001','1','82500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','59392001','60416001','1','84000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','60416001','61440001','1','85500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','61440001','62464001','1','87000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','62464001','63488001','1','88500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','63488001','64512001','1','90000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','64512001','65536001','1','91500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','65536001','66560001','1','93000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','66560001','67584001','1','94500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','67584001','68608001','1','96000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','68608001','69632001','1','97500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','69632001','70656001','1','99000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','70656001','71680001','1','100500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','71680001','72704001','1','102000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','72704001','73728001','1','103500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','73728001','74752001','1','105000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','74752001','75776001','1','106500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','75776001','76800001','1','108000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','76800001','77824001','1','109500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','77824001','78848001','1','111000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','78848001','79872001','1','112500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','79872001','80896001','1','114000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','80896001','81920001','1','115500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','81920001','82944001','1','117000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','82944001','83968001','1','118500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','83968001','84992001','1','120000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','84992001','86016001','1','121500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','86016001','87040001','1','123000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','87040001','88064001','1','124500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','88064001','89088001','1','126000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','89088001','90112001','1','127500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','90112001','91136001','1','129000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','91136001','92160001','1','130500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','92160001','93184001','1','132000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','93184001','94208001','1','133500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','94208001','95232001','1','135000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','95232001','96256001','1','136500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','96256001','97280001','1','138000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','97280001','98304001','1','139500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','98304001','99328001','1','141000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','99328001','100352001','1','142500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','100352001','101376001','1','144000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','101376001','102400001','1','145500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','102400001','103424001','1','147000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','103424001','104448001','1','148500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','104448001','105472001','1','150000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','105472001','106496001','1','151500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','106496001','107520001','1','153000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','107520001','108544001','1','154500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','108544001','109568001','1','156000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','109568001','110592001','1','157500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','110592001','111616001','1','159000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','111616001','112640001','1','160500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','112640001','113664001','1','162000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','113664001','114688001','1','163500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','114688001','115712001','1','165000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','115712001','116736001','1','166500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','116736001','117760001','1','168000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','117760001','118784001','1','169500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','118784001','119808001','1','171000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','119808001','120832001','1','172500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','120832001','121856001','1','174000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','121856001','122880001','1','175500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','122880001','123904001','1','177000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','123904001','124928001','1','178500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','124928001','125952001','1','180000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','125952001','126976001','1','181500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','126976001','128000001','1','183000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','128000001','129024001','1','184500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','129024001','130048001','1','186000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','130048001','131072001','1','187500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','131072001','132096001','1','189000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','132096001','133120001','1','190500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','133120001','134144001','1','192000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','134144001','135168001','1','193500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','135168001','136192001','1','195000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','136192001','137216001','1','196500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','137216001','138240001','1','198000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','138240001','139264001','1','199500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','139264001','140288001','1','201000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','140288001','141312001','1','202500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','141312001','142336001','1','204000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','142336001','143360001','1','205500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','143360001','144384001','1','207000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','144384001','145408001','1','208500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','145408001','146432001','1','210000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','146432001','147456001','1','211500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','147456001','148480001','1','213000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','148480001','149504001','1','214500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','149504001','150528001','1','216000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','150528001','151552001','1','217500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','151552001','152576001','1','219000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','152576001','153600001','1','220500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','153600001','154624001','1','222000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','154624001','155648001','1','223500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','155648001','156672001','1','225000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','156672001','157696001','1','226500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','157696001','158720001','1','228000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','158720001','159744001','1','229500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','159744001','160768001','1','231000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','160768001','161792001','1','232500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','161792001','162816001','1','234000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','162816001','163840001','1','235500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','163840001','164864001','1','237000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','164864001','165888001','1','238500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','165888001','166912001','1','240000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','166912001','167936001','1','241500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','167936001','168960001','1','243000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','168960001','169984001','1','244500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','169984001','171008001','1','246000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','171008001','172032001','1','247500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','172032001','173056001','1','249000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','173056001','174080001','1','250500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','174080001','175104001','1','252000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','175104001','176128001','1','253500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','176128001','177152001','1','255000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','177152001','178176001','1','256500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','178176001','179200001','1','258000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','179200001','180224001','1','259500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','180224001','181248001','1','261000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','181248001','182272001','1','262500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','182272001','183296001','1','264000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','183296001','184320001','1','265500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','184320001','185344001','1','267000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','185344001','186368001','1','268500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','186368001','187392001','1','270000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','187392001','188416001','1','271500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','188416001','189440001','1','273000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','189440001','190464001','1','274500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','190464001','191488001','1','276000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','191488001','192512001','1','277500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','192512001','193536001','1','279000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','193536001','194560001','1','280500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','194560001','195584001','1','282000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','195584001','196608001','1','283500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','196608001','197632001','1','285000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','197632001','198656001','1','286500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','198656001','199680001','1','288000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','199680001','200704001','1','289500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','200704001','201728001','1','291000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','201728001','202752001','1','292500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','202752001','203776001','1','294000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','203776001','204800001','1','295500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','204800001','205824001','1','297000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','205824001','206848001','1','298500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','206848001','207872001','1','300000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','207872001','208896001','1','301500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','208896001','209920001','1','303000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('1','3','209920001','','1','303000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','1','5120001','1','0','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','5120001','6144001','1','1500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','6144001','7168001','1','3000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','7168001','8192001','1','4500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','8192001','9216001','1','6000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','9216001','10240001','1','7500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','10240001','11264001','1','9000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','11264001','12288001','1','10500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','12288001','13312001','1','12000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','13312001','14336001','1','13500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','14336001','15360001','1','15000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','15360001','16384001','1','16500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','16384001','17408001','1','18000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','17408001','18432001','1','19500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','18432001','19456001','1','21000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','19456001','20480001','1','22500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','20480001','21504001','1','24000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','21504001','22528001','1','25500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','22528001','23552001','1','27000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','23552001','24576001','1','28500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','24576001','25600001','1','30000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','25600001','26624001','1','31500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','26624001','27648001','1','33000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','27648001','28672001','1','34500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','28672001','29696001','1','36000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','29696001','30720001','1','37500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','30720001','31744001','1','39000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','31744001','32768001','1','40500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','32768001','33792001','1','42000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','33792001','34816001','1','43500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','34816001','35840001','1','45000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','35840001','36864001','1','46500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','36864001','37888001','1','48000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','37888001','38912001','1','49500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','38912001','39936001','1','51000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','39936001','40960001','1','52500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','40960001','41984001','1','54000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','41984001','43008001','1','55500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','43008001','44032001','1','57000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','44032001','45056001','1','58500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','45056001','46080001','1','60000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','46080001','47104001','1','61500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','47104001','48128001','1','63000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','48128001','49152001','1','64500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','49152001','50176001','1','66000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','50176001','51200001','1','67500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','51200001','52224001','1','69000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','52224001','53248001','1','70500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','53248001','54272001','1','72000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','54272001','55296001','1','73500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','55296001','56320001','1','75000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','56320001','57344001','1','76500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','57344001','58368001','1','78000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','58368001','59392001','1','79500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','59392001','60416001','1','81000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','60416001','61440001','1','82500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','61440001','62464001','1','84000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','62464001','63488001','1','85500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','63488001','64512001','1','87000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','64512001','65536001','1','88500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','65536001','66560001','1','90000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','66560001','67584001','1','91500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','67584001','68608001','1','93000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','68608001','69632001','1','94500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','69632001','70656001','1','96000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','70656001','71680001','1','97500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','71680001','72704001','1','99000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','72704001','73728001','1','100500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','73728001','74752001','1','102000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','74752001','75776001','1','103500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','75776001','76800001','1','105000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','76800001','77824001','1','106500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','77824001','78848001','1','108000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','78848001','79872001','1','109500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','79872001','80896001','1','111000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','80896001','81920001','1','112500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','81920001','82944001','1','114000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','82944001','83968001','1','115500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','83968001','84992001','1','117000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','84992001','86016001','1','118500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','86016001','87040001','1','120000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','87040001','88064001','1','121500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','88064001','89088001','1','123000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','89088001','90112001','1','124500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','90112001','91136001','1','126000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','91136001','92160001','1','127500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','92160001','93184001','1','129000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','93184001','94208001','1','130500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','94208001','95232001','1','132000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','95232001','96256001','1','133500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','96256001','97280001','1','135000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','97280001','98304001','1','136500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','98304001','99328001','1','138000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','99328001','100352001','1','139500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','100352001','101376001','1','141000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','101376001','102400001','1','142500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','102400001','103424001','1','144000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','103424001','104448001','1','145500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','104448001','105472001','1','147000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','105472001','106496001','1','148500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','106496001','107520001','1','150000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','107520001','108544001','1','151500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','108544001','109568001','1','153000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','109568001','110592001','1','154500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','110592001','111616001','1','156000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','111616001','112640001','1','157500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','112640001','113664001','1','159000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','113664001','114688001','1','160500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','114688001','115712001','1','162000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','115712001','116736001','1','163500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','116736001','117760001','1','165000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','117760001','118784001','1','166500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','118784001','119808001','1','168000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','119808001','120832001','1','169500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','120832001','121856001','1','171000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','121856001','122880001','1','172500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','122880001','123904001','1','174000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','123904001','124928001','1','175500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','124928001','125952001','1','177000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','125952001','126976001','1','178500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','126976001','128000001','1','180000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','128000001','129024001','1','181500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','129024001','130048001','1','183000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','130048001','131072001','1','184500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','131072001','132096001','1','186000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','132096001','133120001','1','187500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','133120001','134144001','1','189000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','134144001','135168001','1','190500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','135168001','136192001','1','192000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','136192001','137216001','1','193500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','137216001','138240001','1','195000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','138240001','139264001','1','196500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','139264001','140288001','1','198000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','140288001','141312001','1','199500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','141312001','142336001','1','201000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','142336001','143360001','1','202500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','143360001','144384001','1','204000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','144384001','145408001','1','205500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','145408001','146432001','1','207000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','146432001','147456001','1','208500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','147456001','148480001','1','210000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','148480001','149504001','1','211500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','149504001','150528001','1','213000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','150528001','151552001','1','214500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','151552001','152576001','1','216000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','152576001','153600001','1','217500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','153600001','154624001','1','219000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','154624001','155648001','1','220500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','155648001','156672001','1','222000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','156672001','157696001','1','223500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','157696001','158720001','1','225000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','158720001','159744001','1','226500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','159744001','160768001','1','228000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','160768001','161792001','1','229500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','161792001','162816001','1','231000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','162816001','163840001','1','232500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','163840001','164864001','1','234000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','164864001','165888001','1','235500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','165888001','166912001','1','237000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','166912001','167936001','1','238500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','167936001','168960001','1','240000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','168960001','169984001','1','241500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','169984001','171008001','1','243000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','171008001','172032001','1','244500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','172032001','173056001','1','246000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','173056001','174080001','1','247500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','174080001','175104001','1','249000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','175104001','176128001','1','250500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','176128001','177152001','1','252000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','177152001','178176001','1','253500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','178176001','179200001','1','255000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','179200001','180224001','1','256500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','180224001','181248001','1','258000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','181248001','182272001','1','259500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','182272001','183296001','1','261000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','183296001','184320001','1','262500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','184320001','185344001','1','264000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','185344001','186368001','1','265500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','186368001','187392001','1','267000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','187392001','188416001','1','268500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','188416001','189440001','1','270000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','189440001','190464001','1','271500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','190464001','191488001','1','273000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','191488001','192512001','1','274500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','192512001','193536001','1','276000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','193536001','194560001','1','277500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','194560001','195584001','1','279000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','195584001','196608001','1','280500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','196608001','197632001','1','282000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','197632001','198656001','1','283500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','198656001','199680001','1','285000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','199680001','200704001','1','286500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','200704001','201728001','1','288000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','201728001','202752001','1','289500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','202752001','203776001','1','291000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','203776001','204800001','1','292500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','204800001','205824001','1','294000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','205824001','206848001','1','295500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','206848001','207872001','1','297000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','207872001','208896001','1','298500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','208896001','209920001','1','300000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('2','3','209920001','','1','300000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','1','10240001','1','0','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','10240001','11264001','1','1500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','11264001','12288001','1','3000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','12288001','13312001','1','4500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','13312001','14336001','1','6000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','14336001','15360001','1','7500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','15360001','16384001','1','9000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','16384001','17408001','1','10500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','17408001','18432001','1','12000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','18432001','19456001','1','13500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','19456001','20480001','1','15000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','20480001','21504001','1','16500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','21504001','22528001','1','18000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','22528001','23552001','1','19500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','23552001','24576001','1','21000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','24576001','25600001','1','22500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','25600001','26624001','1','24000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','26624001','27648001','1','25500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','27648001','28672001','1','27000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','28672001','29696001','1','28500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','29696001','30720001','1','30000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','30720001','31744001','1','31500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','31744001','32768001','1','33000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','32768001','33792001','1','34500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','33792001','34816001','1','36000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','34816001','35840001','1','37500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','35840001','36864001','1','39000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','36864001','37888001','1','40500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','37888001','38912001','1','42000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','38912001','39936001','1','43500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','39936001','40960001','1','45000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','40960001','41984001','1','46500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','41984001','43008001','1','48000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','43008001','44032001','1','49500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','44032001','45056001','1','51000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','45056001','46080001','1','52500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','46080001','47104001','1','54000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','47104001','48128001','1','55500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','48128001','49152001','1','57000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','49152001','50176001','1','58500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','50176001','51200001','1','60000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','51200001','52224001','1','61500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','52224001','53248001','1','63000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','53248001','54272001','1','64500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','54272001','55296001','1','66000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','55296001','56320001','1','67500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','56320001','57344001','1','69000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','57344001','58368001','1','70500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','58368001','59392001','1','72000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','59392001','60416001','1','73500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','60416001','61440001','1','75000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','61440001','62464001','1','76500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','62464001','63488001','1','78000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','63488001','64512001','1','79500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','64512001','65536001','1','81000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','65536001','66560001','1','82500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','66560001','67584001','1','84000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','67584001','68608001','1','85500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','68608001','69632001','1','87000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','69632001','70656001','1','88500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','70656001','71680001','1','90000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','71680001','72704001','1','91500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','72704001','73728001','1','93000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','73728001','74752001','1','94500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','74752001','75776001','1','96000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','75776001','76800001','1','97500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','76800001','77824001','1','99000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','77824001','78848001','1','100500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','78848001','79872001','1','102000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','79872001','80896001','1','103500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','80896001','81920001','1','105000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','81920001','82944001','1','106500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','82944001','83968001','1','108000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','83968001','84992001','1','109500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','84992001','86016001','1','111000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','86016001','87040001','1','112500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','87040001','88064001','1','114000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','88064001','89088001','1','115500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','89088001','90112001','1','117000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','90112001','91136001','1','118500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','91136001','92160001','1','120000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','92160001','93184001','1','121500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','93184001','94208001','1','123000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','94208001','95232001','1','124500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','95232001','96256001','1','126000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','96256001','97280001','1','127500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','97280001','98304001','1','129000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','98304001','99328001','1','130500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','99328001','100352001','1','132000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','100352001','101376001','1','133500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','101376001','102400001','1','135000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','102400001','103424001','1','136500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','103424001','104448001','1','138000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','104448001','105472001','1','139500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','105472001','106496001','1','141000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','106496001','107520001','1','142500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','107520001','108544001','1','144000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','108544001','109568001','1','145500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','109568001','110592001','1','147000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','110592001','111616001','1','148500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','111616001','112640001','1','150000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','112640001','113664001','1','151500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','113664001','114688001','1','153000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','114688001','115712001','1','154500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','115712001','116736001','1','156000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','116736001','117760001','1','157500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','117760001','118784001','1','159000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','118784001','119808001','1','160500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','119808001','120832001','1','162000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','120832001','121856001','1','163500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','121856001','122880001','1','165000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','122880001','123904001','1','166500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','123904001','124928001','1','168000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','124928001','125952001','1','169500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','125952001','126976001','1','171000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','126976001','128000001','1','172500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','128000001','129024001','1','174000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','129024001','130048001','1','175500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','130048001','131072001','1','177000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','131072001','132096001','1','178500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','132096001','133120001','1','180000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','133120001','134144001','1','181500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','134144001','135168001','1','183000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','135168001','136192001','1','184500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','136192001','137216001','1','186000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','137216001','138240001','1','187500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','138240001','139264001','1','189000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','139264001','140288001','1','190500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','140288001','141312001','1','192000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','141312001','142336001','1','193500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','142336001','143360001','1','195000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','143360001','144384001','1','196500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','144384001','145408001','1','198000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','145408001','146432001','1','199500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','146432001','147456001','1','201000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','147456001','148480001','1','202500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','148480001','149504001','1','204000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','149504001','150528001','1','205500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','150528001','151552001','1','207000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','151552001','152576001','1','208500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','152576001','153600001','1','210000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','153600001','154624001','1','211500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','154624001','155648001','1','213000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','155648001','156672001','1','214500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','156672001','157696001','1','216000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','157696001','158720001','1','217500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','158720001','159744001','1','219000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','159744001','160768001','1','220500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','160768001','161792001','1','222000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','161792001','162816001','1','223500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','162816001','163840001','1','225000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','163840001','164864001','1','226500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','164864001','165888001','1','228000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','165888001','166912001','1','229500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','166912001','167936001','1','231000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','167936001','168960001','1','232500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','168960001','169984001','1','234000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','169984001','171008001','1','235500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','171008001','172032001','1','237000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','172032001','173056001','1','238500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','173056001','174080001','1','240000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','174080001','175104001','1','241500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','175104001','176128001','1','243000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','176128001','177152001','1','244500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','177152001','178176001','1','246000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','178176001','179200001','1','247500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','179200001','180224001','1','249000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','180224001','181248001','1','250500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','181248001','182272001','1','252000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','182272001','183296001','1','253500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','183296001','184320001','1','255000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','184320001','185344001','1','256500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','185344001','186368001','1','258000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','186368001','187392001','1','259500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','187392001','188416001','1','261000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','188416001','189440001','1','262500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','189440001','190464001','1','264000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','190464001','191488001','1','265500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','191488001','192512001','1','267000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','192512001','193536001','1','268500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','193536001','194560001','1','270000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','194560001','195584001','1','271500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','195584001','196608001','1','273000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','196608001','197632001','1','274500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','197632001','198656001','1','276000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','198656001','199680001','1','277500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','199680001','200704001','1','279000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','200704001','201728001','1','280500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','201728001','202752001','1','282000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','202752001','203776001','1','283500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','203776001','204800001','1','285000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','204800001','205824001','1','286500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','205824001','206848001','1','288000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','206848001','207872001','1','289500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','207872001','208896001','1','291000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','208896001','209920001','1','292500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','209920001','210944001','1','294000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','210944001','211968001','1','295500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','211968001','212992001','1','297000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','212992001','214016001','1','298500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('3','3','214016001','','1','300000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','1','20480001','1','0','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','20480001','21504001','1','1500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','21504001','22528001','1','3000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','22528001','23552001','1','4500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','23552001','24576001','1','6000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','24576001','25600001','1','7500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','25600001','26624001','1','9000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','26624001','27648001','1','10500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','27648001','28672001','1','12000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','28672001','29696001','1','13500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','29696001','30720001','1','15000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','30720001','31744001','1','16500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','31744001','32768001','1','18000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','32768001','33792001','1','19500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','33792001','34816001','1','21000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','34816001','35840001','1','22500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','35840001','36864001','1','24000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','36864001','37888001','1','25500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','37888001','38912001','1','27000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','38912001','39936001','1','28500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','39936001','40960001','1','30000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','40960001','41984001','1','31500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','41984001','43008001','1','33000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','43008001','44032001','1','34500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','44032001','45056001','1','36000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','45056001','46080001','1','37500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','46080001','47104001','1','39000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','47104001','48128001','1','40500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','48128001','49152001','1','42000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','49152001','50176001','1','43500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','50176001','51200001','1','45000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','51200001','52224001','1','46500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','52224001','53248001','1','48000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','53248001','54272001','1','49500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','54272001','55296001','1','51000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','55296001','56320001','1','52500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','56320001','57344001','1','54000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','57344001','58368001','1','55500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','58368001','59392001','1','57000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','59392001','60416001','1','58500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','60416001','61440001','1','60000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','61440001','62464001','1','61500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','62464001','63488001','1','63000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','63488001','64512001','1','64500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','64512001','65536001','1','66000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','65536001','66560001','1','67500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','66560001','67584001','1','69000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','67584001','68608001','1','70500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','68608001','69632001','1','72000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','69632001','70656001','1','73500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','70656001','71680001','1','75000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','71680001','72704001','1','76500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','72704001','73728001','1','78000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','73728001','74752001','1','79500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','74752001','75776001','1','81000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','75776001','76800001','1','82500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','76800001','77824001','1','84000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','77824001','78848001','1','85500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','78848001','79872001','1','87000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','79872001','80896001','1','88500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','80896001','81920001','1','90000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','81920001','82944001','1','91500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','82944001','83968001','1','93000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','83968001','84992001','1','94500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','84992001','86016001','1','96000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','86016001','87040001','1','97500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','87040001','88064001','1','99000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','88064001','89088001','1','100500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','89088001','90112001','1','102000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','90112001','91136001','1','103500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','91136001','92160001','1','105000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','92160001','93184001','1','106500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','93184001','94208001','1','108000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','94208001','95232001','1','109500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','95232001','96256001','1','111000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','96256001','97280001','1','112500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','97280001','98304001','1','114000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','98304001','99328001','1','115500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','99328001','100352001','1','117000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','100352001','101376001','1','118500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','101376001','102400001','1','120000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','102400001','103424001','1','121500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','103424001','104448001','1','123000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','104448001','105472001','1','124500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','105472001','106496001','1','126000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','106496001','107520001','1','127500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','107520001','108544001','1','129000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','108544001','109568001','1','130500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','109568001','110592001','1','132000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','110592001','111616001','1','133500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','111616001','112640001','1','135000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','112640001','113664001','1','136500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','113664001','114688001','1','138000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','114688001','115712001','1','139500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','115712001','116736001','1','141000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','116736001','117760001','1','142500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','117760001','118784001','1','144000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','118784001','119808001','1','145500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','119808001','120832001','1','147000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','120832001','121856001','1','148500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','121856001','122880001','1','150000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','122880001','123904001','1','151500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','123904001','124928001','1','153000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','124928001','125952001','1','154500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','125952001','126976001','1','156000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','126976001','128000001','1','157500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','128000001','129024001','1','159000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','129024001','130048001','1','160500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','130048001','131072001','1','162000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','131072001','132096001','1','163500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','132096001','133120001','1','165000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','133120001','134144001','1','166500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','134144001','135168001','1','168000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','135168001','136192001','1','169500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','136192001','137216001','1','171000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','137216001','138240001','1','172500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','138240001','139264001','1','174000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','139264001','140288001','1','175500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','140288001','141312001','1','177000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','141312001','142336001','1','178500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','142336001','143360001','1','180000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','143360001','144384001','1','181500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','144384001','145408001','1','183000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','145408001','146432001','1','184500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','146432001','147456001','1','186000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','147456001','148480001','1','187500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','148480001','149504001','1','189000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','149504001','150528001','1','190500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','150528001','151552001','1','192000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','151552001','152576001','1','193500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','152576001','153600001','1','195000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','153600001','154624001','1','196500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','154624001','155648001','1','198000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','155648001','156672001','1','199500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','156672001','157696001','1','201000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','157696001','158720001','1','202500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','158720001','159744001','1','204000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','159744001','160768001','1','205500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','160768001','161792001','1','207000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','161792001','162816001','1','208500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','162816001','163840001','1','210000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','163840001','164864001','1','211500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','164864001','165888001','1','213000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','165888001','166912001','1','214500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','166912001','167936001','1','216000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','167936001','168960001','1','217500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','168960001','169984001','1','219000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','169984001','171008001','1','220500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','171008001','172032001','1','222000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','172032001','173056001','1','223500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','173056001','174080001','1','225000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','174080001','175104001','1','226500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','175104001','176128001','1','228000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','176128001','177152001','1','229500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','177152001','178176001','1','231000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','178176001','179200001','1','232500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','179200001','180224001','1','234000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','180224001','181248001','1','235500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','181248001','182272001','1','237000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','182272001','183296001','1','238500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','183296001','184320001','1','240000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','184320001','185344001','1','241500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','185344001','186368001','1','243000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','186368001','187392001','1','244500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','187392001','188416001','1','246000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','188416001','189440001','1','247500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','189440001','190464001','1','249000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','190464001','191488001','1','250500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','191488001','192512001','1','252000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','192512001','193536001','1','253500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','193536001','194560001','1','255000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','194560001','195584001','1','256500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','195584001','196608001','1','258000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','196608001','197632001','1','259500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','197632001','198656001','1','261000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','198656001','199680001','1','262500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','199680001','200704001','1','264000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','200704001','201728001','1','265500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','201728001','202752001','1','267000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','202752001','203776001','1','268500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','203776001','204800001','1','270000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','204800001','205824001','1','271500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','205824001','206848001','1','273000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','206848001','207872001','1','274500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','207872001','208896001','1','276000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','208896001','209920001','1','277500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','209920001','210944001','1','279000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','210944001','211968001','1','280500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','211968001','212992001','1','282000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','212992001','214016001','1','283500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','214016001','215040001','1','285000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','215040001','216064001','1','286500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','216064001','217088001','1','288000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','217088001','218112001','1','289500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','218112001','219136001','1','291000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','219136001','220160001','1','292500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','220160001','221184001','1','294000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','221184001','222208001','1','295500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','222208001','223232001','1','297000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','223232001','224256001','1','298500','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into rate_aggr_usage values ('4','3','224256001','','1','300000','','','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'',TO_DATE('01/01/2011','DD/MM/YYYY'),TO_DATE('01/01/2011','DD/MM/YYYY'),'arboradm');
insert into aggr_usage_restrictions values ('1','5','983','0');
insert into aggr_usage_restrictions values ('1','2','4','0');
insert into aggr_usage_restrictions values ('2','5','983','0');
insert into aggr_usage_restrictions values ('2','2','5','0');
insert into aggr_usage_restrictions values ('3','5','983','0');
insert into aggr_usage_restrictions values ('3','2','6','0');
insert into aggr_usage_restrictions values ('4','5','983','0');
insert into aggr_usage_restrictions values ('4','2','7','0');
insert into usage_jurisdiction values ('1','10807','4','983','0','0','0','0','','','0','0','','','','0','','0','','','','120','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0');
insert into usage_jurisdiction values ((select max(seqnum)+1 from usage_jurisdiction),'10807','5','983','0','0','0','0','','','0','0','','','','0','','0','','','','120','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0');
insert into usage_jurisdiction values ((select max(seqnum)+1 from usage_jurisdiction),'10807','6','983','0','0','0','0','','','0','0','','','','0','','0','','','','120','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0');
insert into usage_jurisdiction values ((select max(seqnum)+1 from usage_jurisdiction),'10807','7','983','0','0','0','0','','','0','0','','','','0','','0','','','','120','0','0',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0');
insert into usage_jurisdiction values ((select max(seqnum)+1 from usage_jurisdiction),'10805','3','981','0','0','0','0','','','0','0','','','AK    ','0','','0','','','','130','840','840',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0');
insert into usage_jurisdiction values ((select max(seqnum)+1 from usage_jurisdiction),'10806','3','981','0','0','0','0','','','0','0','','','','0','AK    ','0','','','','130','840','840',TO_DATE('01/01/2011','DD/MM/YYYY'),'','0','0');

 dbms_output.put_line('Inserted Values for USAGE RATING Tables');

  -- nrcs
  -- DESCRIPTIONS
  -- NRC_TRANS_DESCR (trigger_level: 0 = acct, 1 = serv)
  -- RATE_NRC (do NOT insert rate if ICB)
  
  -- discounts (NOTE: send email to #UIS if configuring a new discount)
  -- DESCRIPTIONS
  -- DISCOUNT_DEFINITIONS (discount_level: 1 = serv, 2 = acct)
  -- DISCOUNT_TARGETS (the "things" you want to discount)
  -- DISCOUNT_RESTRICTIONS (the "things" you want to look at to qualify for the discount)
  -- RATE_DISCOUNT (if using targets AND restriction, then also set range origin and terminus)
  -- PLAN_ID_DISCOUNT_REF
  -- PLAN_ID_DISCOUNT_VALUES
  -- DISCOUNT_PLANS

  -- unit credits
  -- DESCRIPTIONS
  -- UNIT_CR_DEFINITIONS (unit_cr_level: 1 = acct, 2 = serv) (unit_cr_distrib: 1 = acct, 2 = serv)
  -- UNIT_CR_RESTRICTIONS (the "things" you want to credit)
  -- RATE_UNIT_CR
  -- PLAN_ID_CREDIT_REF
  -- PLAN_ID_CREDIT_VALUES
  -- UNIT_CR_PLANS
  -- for each new SHARED bucket unit credit, add an insert into GCI_WIRELESS_UNIT_CR_KEY:
  -- insert into gci_wireless_unit_cr_key values (1, <new unit credit id>, 'plan unit credits');

  -- contracts
  -- DESCRIPTIONS
  -- CONTRACT_TYPES (allow_acct: 1 = acct, 0 = not ANT allow_serv: 1 = serv, 0 = not)

  -- packages
  -- PACKAGE_GROUP_REF
  -- PACKAGE_GROUP_VALUES
  -- PACKAGE_DEFINITION_REF
  -- PACKAGE_DEFINITION_VALUES
  -- PACKAGE_CMF_ELIGIBILITY

  -- finally associate members created to components and components created to packages
  -- PACKAGE_COMPONENT_MEMBERS (member_type: 1 = product, 2 = contract)
  -- PACKAGE_COMPONENTS

insert into package_component_members values ('6','1','4','1','140','0','0','0','1',TO_DATE('01/01/2011','DD/MM/YYYY'),'');
insert into package_component_members values ('7','1','5','1','140','0','0','0','1',TO_DATE('01/01/2011','DD/MM/YYYY'),'');
insert into package_component_members values ('8','1','6','1','140','0','0','0','1',TO_DATE('01/01/2011','DD/MM/YYYY'),'');
insert into package_component_members values ('9','1','7','1','140','0','0','0','1',TO_DATE('01/01/2011','DD/MM/YYYY'),'');
insert into package_components values ('1','6','0','9999',TO_DATE('01/01/2011','DD/MM/YYYY'),'');
insert into package_components values ('1','7','0','9999',TO_DATE('01/01/2011','DD/MM/YYYY'),'');
insert into package_components values ('1','8','0','9999',TO_DATE('01/01/2011','DD/MM/YYYY'),'');
insert into package_components values ('1','9','0','9999',TO_DATE('01/01/2011','DD/MM/YYYY'),'');

  -- associate the components to the account segment and catalog id
  -- C30_EXTERNAL_CATALOG_DEF
  -- C30_EXTERNAL_CATALOG_MAP

insert into c30_external_catalog_def values ('3','DATA3000MB');
insert into c30_external_catalog_def values ('3','DATA5000MB');
insert into c30_external_catalog_def values ('3','DATA10000MB');
insert into c30_external_catalog_def values ('3','DATA20000MB');
insert into c30_external_catalog_map values ('3','DATA3000MB','C','6','');
insert into c30_external_catalog_map values ('3','DATA5000MB','C','7','');
insert into c30_external_catalog_map values ('3','DATA10000MB','C','8','');
insert into c30_external_catalog_map values ('3','DATA20000MB','C','9','');

  -- open items
  -- OPEN_ITEM_ID_REF
  -- OPEN_ITEM_ID_VALUES
  -- GLOBAL_OPEN_ITEM_ID_MAP
  -- inserting new open_item_id_ref creates an insert for new open_item_id in global_account_balances 
  -- hence just an update is necessary to update the default priorty which set to 0 and the default regulatory_id which is set to 0
  -- and the default active_dt which is set to the sysdate
  -- GLOBAL_ACCOUNT_BALANCES

  -- taxes  (NOTE: confirm with Linda McMahon)
  -- TAX_CODES_COMM

  -- adjustments (NOTE: 9 rows per product, 5 rows per nrc and 10 rows per usage type)
  -- use the following stored procedure - it will create the necessary DESCRIPTIONS and
  -- ADJ_TRANS_DESCR config per entity
  -- To use the proc - do a call like the following where target_type: 1 = Product, 2 = NRC and 3 = Usage Types
  -- and the target_id is the id of the entity - be sure to call this after you have done the inserts into
  -- PRODUCT_ELEMENTS, NRC_TRANS_DESC and/or USAGE_TYPES
  -- For Kenan 2.0 use c30_config_adj

  -- journals  (NOTE: needs sign off from Cherie Sinclair)
  --           (NOTE: jnl config only required when adding a new product, nrc or usage)
  -- JNL_CUSTOM
  -- JNL_KEYS_DEFAULT
  -- use the following stored procedure - it will create the necessary JNL_KEYS_DEFAULT config per entity
  -- To use the proc - do a call like the following where target_type: 1 = Product, 2 = NRC and 3 = Usage Types
  -- and the target_id is the id of the entity - be sure to call this after you have done the inserts into
  -- PRODUCT_ELEMENTS, NRC_TRANS_DESC and/or USAGE_TYPES. If the entity is to be journalled by account category then
  -- set the v_acct_cat_split => 1 and pass in the appropriate jnl_code_id for comm, cons and whls. If the entity does
  -- not have to be journalled by account category then set v_acct_cat_split => 0 and pass in only the jnl_code_id's
  -- for cons (comm and whls will be null)
  -- The proc creates jnl config for the entity itself plus adjustments and discount/unit credit of the entity
    --determine the product that will be modeled for the journal set up
    --select * from jnl_config_display_vw where id_value_x = 35501 and inactive_date is null
    --csub_cr = GL_CODE, journalcode_display_value = cons/comm/whls, jnl_code_id = values to use for v_comm/v_cons/v_whls
    --could use   trunc(sysdate, 'MM')  at the end to give the date of the current month, first day of the month


  -- issue final commit and print dbms_output
  commit;
  dbms_output.put_line('Successfully completed script for AWN Data Plans and Usage Types 981 and 983');
  dbms_output.put_line('COMMIT;');

exception
  when others then
    rollback;
      l_error_msg := '** Error '||substr(sqlerrm,1,240);
      dbms_output.put_line(l_error_msg);
      dbms_output.put_line('ROLLBACK;');
end;
/

exit rollback