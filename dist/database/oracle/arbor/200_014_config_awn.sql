set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
prompt *****************************************************************************************************
prompt Release Notes
prompt PROJECT: AWN - Alaska Wireless Network
prompt Date: 8-27-2012
prompt File Name: c30bdv1_RUFM_WL2_setup_20120827.sql
prompt Created By: Steve Brock
prompt .
prompt No Outage is required
prompt .
prompt Defect Resolved: RAW_USAGE_FIELD_MAPPING tables
prompt .
prompt    
prompt Description: RAW_USAGE_FIELD_MAPPING inserts for voice usage
prompt .
prompt    
prompt ENVIRONMENT: C3BDV1 only  
prompt .  
prompt  
prompt SPECIAL INSTRUCTIONS: Some additional table entries needed for usage processing are in the GPR script - c30bdv1_RUFM_initial_setup_20120806.sql
prompt .
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing" i.e. if it encounters an error it will rollback entirely
prompt *****************************************************************************************************




--INSERT AND UPDATES TO EXISTING TABLES


declare
  l_error_msg                         varchar2(255); -- max dbms_output line length

begin

  -- descriptions
  -- TIMEZONE_ref 
  -- TIMEZONE_values  
  -- ext_source_id_ref 
  -- ext_source_id_values 
  -- EXT_ACCESS_METHOD_REF 
  -- EXT_ACCESS_METHOD_values 
  -- FILE_TYPE_REF 
  -- FILE_TYPE_values 
  -- ext_contacts 
  -- EXT_LOCAL_DIR_ACCESS 
  -- RAW_USAGE_FIELD_MAPPING

insert into raw_usage_field_mapping values('WL2',1,'Record Type',3,3,1,1,null,'');
insert into raw_usage_field_mapping values('WL2',2,'Usage Type',5,1,1,0,null,'type_id_usg');
insert into raw_usage_field_mapping values('WL2',3,'Billing Class',5,1,1,0,null,'bill_class');
insert into raw_usage_field_mapping values('WL2',4,'Billed Number Type',5,1,1,0,null,'external_id_type');
insert into raw_usage_field_mapping values('WL2',5,'Billed Number',48,3,1,0,null,'external_id');
insert into raw_usage_field_mapping values('WL2',6,'Trans Date/Time',14,5,1,0,'YYYYMMDDHHMISS','trans_dt');
insert into raw_usage_field_mapping values('WL2',7,'Target Number',20,3,1,0,null,'point_target');
insert into raw_usage_field_mapping values('WL2',8,'Origin Number',20,3,1,0,null,'point_origin');
insert into raw_usage_field_mapping values('WL2',9,'Duration',10,1,1,0,null,'primary_units');
insert into raw_usage_field_mapping values('WL2',10,'In-community Indicator',10,1,1,0,null,'second_units');
insert into raw_usage_field_mapping values('WL2',11,'Wholesale Rate',10,1,1,0,null,'third_units');
insert into raw_usage_field_mapping values('WL2',12,'Completion Code',1,1,1,0,null,'comp_status');
insert into raw_usage_field_mapping values('WL2',13,'Service Provider',5,1,1,0,null,'provider_id');
insert into raw_usage_field_mapping values('WL2',14,'Time Zone',3,1,1,0,null,'timezone');
insert into raw_usage_field_mapping values('WL2',15,'Filler1',20,3,0,0,null,'trans_id');
insert into raw_usage_field_mapping values('WL2',16,'Filler2',20,3,0,0,null,'customer_tag');
insert into raw_usage_field_mapping values('WL2',17,'Filler3',20,3,0,0,null,'ext_tracking_id');
insert into raw_usage_field_mapping values('WL2',18,'Amount',8,1,0,0,null,'amount');
insert into raw_usage_field_mapping values('WL2',19,'Federal Tax',8,1,0,0,null,'federal_tax');
insert into raw_usage_field_mapping values('WL2',20,'State Tax',8,1,0,0,null,'state_tax');
insert into raw_usage_field_mapping values('WL2',21,'County Tax',8,1,0,0,null,'county_tax');
insert into raw_usage_field_mapping values('WL2',22,'City Tax',8,1,0,0,null,'city_tax');
insert into raw_usage_field_mapping values('WL2',23,'Other Tax',8,1,0,0,null,'other_tax');
insert into raw_usage_field_mapping values('WL2',24,'Roaming Location',255,3,0,0,null,'annotation');

 dbms_output.put_line('Inserted Values for AWN Rate Usage Field Mapping Table for Voice Usage');


  -- issue final commit and print dbms_output
  commit;
  dbms_output.put_line('Successfully completed script for RAW_USAGE_FIELD_MAPPING inserts for voice usage');
  dbms_output.put_line('COMMIT;');


exception
  when others then
    rollback;
      l_error_msg := '** Error '||substr(sqlerrm,1,240);
      dbms_output.put_line(l_error_msg);
      dbms_output.put_line('ROLLBACK;');
end;
/

exit rollback