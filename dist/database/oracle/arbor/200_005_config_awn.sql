set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0
prompt *****************************************************************************************************
prompt Release Notes
prompt PROJECT: AWN - Alaska Wireless Network
prompt Date: 8-20-2012
prompt Created By: Steve Brock
prompt .
prompt No Outage is required
prompt .
prompt Defect Resolved: A.W.N. The Simplified Voice Data Text Usage Plans
prompt .
prompt    
prompt Description: A.W.N. Usage Plans - just the components and guiding elements 
prompt .
prompt    
prompt .  
prompt
prompt ENVIRONMENT: C3B only  
prompt .  
prompt  
prompt SPECIAL INSTRUCTIONS: Contact Steve Brock with issues
prompt .
prompt *****************************************************************************************************
prompt ** This is set to run "All or Nothing" i.e. if it encounters an error it will rollback entirely
prompt *****************************************************************************************************

drop procedure c30arbor.c30discPlan;
drop procedure c30arbor.c30getAccountBalance;
drop procedure c30arbor.c30getInvoice;
drop procedure c30arbor.c30getInvoiceCharges;
drop procedure c30arbor.c30updatePlan;
drop procedure c30arbor.c30swapPlan;

declare
  l_error_msg                         varchar2(255); -- max dbms_output line length

begin
  -- associate the components to the account segment and catalog id
  -- C30_EXTERNAL_CATALOG_DEF
  -- C30_EXTERNAL_CATALOG_MAP

  delete c30_external_catalog_map;
  delete c30_external_catalog_def;
  insert into c30_external_catalog_def values ('3','VOICE');
  insert into c30_external_catalog_def values ('3','DATA');
  insert into c30_external_catalog_def values ('3','DATA+TETH');
  insert into c30_external_catalog_def values ('3','MSG');
  insert into c30_external_catalog_def values ('3','REMROAM');
  insert into c30_external_catalog_def values ('3','INTLROAM');
  insert into c30_external_catalog_def values ('3','INTLCALL');
  insert into c30_external_catalog_map values ('3','VOICE','P','21','21');
  insert into c30_external_catalog_map values ('3','DATA','C','22','');
  insert into c30_external_catalog_map values ('3','DATA+TETH','C','22','');
  insert into c30_external_catalog_map values ('3','MSG','C','23','');
  

  insert into arbor.param_def ( param_id, param_name, param_datatype, assoc_enumeration_id, is_addressable, param_external_id, validation_rules )
  values ( 1, 'CarrierFrom', 2, null, null, null, null );
  insert into arbor.param_def ( param_id, param_name, param_datatype, assoc_enumeration_id, is_addressable, param_external_id, validation_rules )
  values ( 2, 'CarrierTo', 2, null, null, null, null );
  insert into arbor.param_values ( param_id, language_code, short_display, display_value, validation_rules_desc )
  values ( 1, 1, null, 'Carrier Porting From', null );
  insert into arbor.param_values ( param_id, language_code, short_display, display_value, validation_rules_desc )
  values ( 2, 1, null, 'Carrier Porting To', null );
  insert into arbor.ext_param_type_assoc ( base_table, entity_type, param_id, grouping_id, default_value, is_required, attribute_display_order )
  values ( 'SERVICE_VIEW', 0, 1, null, null, null, null );
  insert into arbor.ext_param_type_assoc ( base_table, entity_type, param_id, grouping_id, default_value, is_required, attribute_display_order )
  values ( 'SERVICE_VIEW', 0, 2, null, null, null, null );

  insert into c30_api_seg_defaults (acct_seg_id, api_name, api_parameter_name, seg_parameter_default)
  values (1, 'c30addservice', 'v_connect_reason', '1');
  insert into c30_api_seg_defaults (acct_seg_id, api_name, api_parameter_name, seg_parameter_default)
  values (3, 'c30addservice', 'v_connect_reason', '1');

  -- insert into status_reason_ref (status_reason_id, is_default, is_internal)
  -- values (1, 1, 1);
  insert into status_reason_ref (status_reason_id, is_default, is_internal)
  values (2, 0, 1);
  insert into status_reason_ref (status_reason_id, is_default, is_internal)
  values (3, 0, 1);
  insert into status_reason_ref (status_reason_id, is_default, is_internal)
  values (4, 0, 1);
  insert into status_reason_ref (status_reason_id, is_default, is_internal)
  values (5, 0, 1);
  -- insert into status_reason_values (status_reason_id, language_code, short_display, display_value)
  -- values (1, 1, 'C3B', 'Cycle30 API Initiated');
  insert into status_reason_values (status_reason_id, language_code, short_display, display_value)
  values (2, 1, 'PORTIN', 'Port In');
  insert into status_reason_values (status_reason_id, language_code, short_display, display_value)
  values (3, 1, 'PORTOUT', 'Port Out');
  insert into status_reason_values (status_reason_id, language_code, short_display, display_value)
  values (4, 1, 'NETSWAP', 'Network Swap');
  insert into status_reason_values (status_reason_id, language_code, short_display, display_value)
  values (5, 1, 'NONPAY', 'Nonpayment');

  -- insert into c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
  -- values (1, 'status_reason_id', '1', 1);
  -- insert into c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
  -- values (2, 'status_reason_id', '1', 1);
  insert into c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
  values (2, 'status_reason_id', '2', 1);
  insert into c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
  values (2, 'status_reason_id', '3', 1);
  insert into c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
  values (2, 'status_reason_id', '4', 1);
  insert into c30_acct_seg_obj_map (acct_seg_id, object_name, object_value, is_default)
  values (2, 'status_reason_id', '5', 1);


  -- insert into status_reason_map (status_reason_id)
  -- values (1, 1);
  -- insert into status_reason_map (status_reason_id)
  -- values (1, 2);
  -- insert into status_reason_map (status_reason_id)
  -- values (1, 3);
  insert into status_reason_map (status_reason_id, status_id)
  values (2, 1);
  insert into status_reason_map (status_reason_id, status_id)
  values (3, 2);
  insert into status_reason_map (status_reason_id, status_id)
  values (4, 1);
  insert into status_reason_map (status_reason_id, status_id)
  values (5, 2);
  insert into status_reason_map (status_reason_id, status_id)
  values (5, 3);


  -- issue final commit and print dbms_output
  commit;
  dbms_output.put_line('Successfully completed script for A.W.N. The Simplified Voice Data Text Usage Plans');
  dbms_output.put_line('COMMIT;');

exception
  when others then
    rollback;
      l_error_msg := '** Error '||substr(sqlerrm,1,240);
      dbms_output.put_line(l_error_msg);
      dbms_output.put_line('ROLLBACK;');
end;
/

exit rollback