-- Release Notes
-- Config 1.0
-- Date: 05/22/2013
-- Created By: JayBharat Vurla
-- Change Request: C30 SDK Batch Framework Development

	Delete from "C30BATCH"."C30_SDK_BATCH_FLOW"; 
	
	COMMIT;

	Insert into C30BATCH.C30_SDK_BATCH_FLOW (BATCH_FLOW_ID,BATCH_FLOW_NAME) 
	values (1,'LCF_BATCH_FLOW');
	
	Insert into C30BATCH.C30_SDK_BATCH_FLOW (BATCH_FLOW_ID,BATCH_FLOW_NAME) 
	values (2,'BatchFlowAgent');

	COMMIT;

	EXIT
 