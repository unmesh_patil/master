-- Release Notes
-- Config 1.0
-- Date: 05/22/2013
-- Created By: JayBharat Vurla
-- Change Request: C30 SDK Batch Framework Development

	Delete from "C30BATCH"."C30_SDK_JOB_CONFIG"; 
	
	COMMIT;
	Insert into C30BATCH.C30_SDK_JOB_CONFIG (JOB_CONFIG_ID,BATCH_FLOW_ID,STATUS_ID,JOB_CONFIG_NAME,APP_CONTXT_FILE_NAME,JOB_NAME) 
	values (1,1,6,'BatchRunApproveJobTest','BatchRunExecutionApproveTestContext.xml','FrameWorkBatchTestFlow_approve_BatchRun_TestJob');
	
	Insert into C30BATCH.C30_SDK_JOB_CONFIG (JOB_CONFIG_ID,BATCH_FLOW_ID,STATUS_ID,JOB_CONFIG_NAME,APP_CONTXT_FILE_NAME,JOB_NAME) 
	values (2,1,3,'FrameWorkBatchTestFlow_Regular_Process_Job_Name','LcfRegularProcessContext.xml','LcfBatchFlow_RegularProcessJob');
	
	Insert into C30BATCH.C30_SDK_JOB_CONFIG (JOB_CONFIG_ID,BATCH_FLOW_ID,STATUS_ID,JOB_CONFIG_NAME,APP_CONTXT_FILE_NAME,JOB_NAME) 
	values (3,1,8,'Batch_Flow_Errror_Process','LcfErrorProcessContext.xml','LcfBatchFlow_ErrorProcessJob');
	
	COMMIT;

	EXIT
 