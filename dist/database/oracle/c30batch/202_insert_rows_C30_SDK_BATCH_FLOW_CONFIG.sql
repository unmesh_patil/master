-- Release Notes
-- Config 1.0
-- Date: 05/22/2013
-- Created By: JayBharat Vurla
-- Change Request: C30 SDK Batch Framework Development

	Delete from "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG"; 
	
	COMMIT;

	Insert into C30BATCH.C30_SDK_BATCH_FLOW_CONFIG (BATCH_FLOW_CONFIG_ID,BATCH_FLOW_ID,SRC_DIR_PATH,WORK_DIR_PATH,ARCHIVE_DIR_PATH,MAX_LINE_ITEM_ERROR_COUNT,AUTO_APPROVAL_FLAG,APPROVAL_NOTIF_EMAIL) 
	values (1,1,null,'/opt/app/kenanfx2/batch/data/prepaid/awn/lcf/working/','/opt/app/kenanfx2/batch/data/prepaid/awn/lcf/archive/',10,'T','test@capgemini.com');
	
	Insert into C30BATCH.C30_SDK_BATCH_FLOW_CONFIG (BATCH_FLOW_CONFIG_ID,BATCH_FLOW_ID,SRC_DIR_PATH,WORK_DIR_PATH,ARCHIVE_DIR_PATH,MAX_LINE_ITEM_ERROR_COUNT,AUTO_APPROVAL_FLAG,APPROVAL_NOTIF_EMAIL) 
	values (2,2,null,'/opt/app/kenanfx2/batch/data/prepaid/awn/lcf/working/','/opt/app/kenanfx2/batch/data/prepaid/awn/lcf/archive/',3,'T','test@capgemini.com');
	
	COMMIT;

	EXIT
 
