-- Release Notes
-- Config 1.0
-- Date: 05/22/2013
-- Created By: JayBharat Vurla
-- Change Request: C30 SDK Batch Framework Development

  DROP TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" ;
  
  CREATE TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" 
   (	
   "BATCH_FLOW_CONFIG_ID" NUMBER(19,0), 
	"BATCH_FLOW_ID" NUMBER(19,0), 
	"SRC_DIR_PATH" VARCHAR2(512 BYTE), 
	"WORK_DIR_PATH" VARCHAR2(512 BYTE), 
	"ARCHIVE_DIR_PATH" VARCHAR2(512 BYTE), 
	"MAX_LINE_ITEM_ERROR_COUNT" NUMBER(19,0), 
	"AUTO_APPROVAL_FLAG" CHAR(1 BYTE), 
	"APPROVAL_NOTIF_EMAIL" VARCHAR2(512 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

  CREATE UNIQUE INDEX "C30BATCH"."SYS_C007473" ON "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" ("BATCH_FLOW_CONFIG_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

  ALTER TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" MODIFY ("BATCH_FLOW_CONFIG_ID" NOT NULL ENABLE);
  
  ALTER TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" MODIFY ("BATCH_FLOW_ID" NOT NULL ENABLE);
  
  ALTER TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" MODIFY ("MAX_LINE_ITEM_ERROR_COUNT" NOT NULL ENABLE);
  
  ALTER TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" MODIFY ("AUTO_APPROVAL_FLAG" NOT NULL ENABLE);
  
  ALTER TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" MODIFY ("APPROVAL_NOTIF_EMAIL" NOT NULL ENABLE);
  
  ALTER TABLE "C30BATCH"."C30_SDK_BATCH_FLOW_CONFIG" ADD PRIMARY KEY ("BATCH_FLOW_CONFIG_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;

 

  COMMIT;

  EXIT
 