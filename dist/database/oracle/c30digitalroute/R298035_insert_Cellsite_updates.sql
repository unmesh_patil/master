set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off sqlpluscompatibility 9.2.0

prompt *****************************************************************************************************
prompt Release Notes
prompt R298035-Update Digital Route Cellsite Table
prompt File: R298035_insert_Cellsite_updates.sql
prompt Date: 01/15/2012
prompt Created By: Cory Cresap
prompt Description : Add 1 Cellsites - this should be run in PROD 


delete from C30DIGITALROUTE.LOOKUP_SWITCH_LOCATION
where gsmsiteid in (
'KEN429'
);


insert into C30DIGITALROUTE.LOOKUP_SWITCH_LOCATION values
('KEN429','42','KEU429','ATT Sterling Longmere','Soldotna');


commit;
dbms_output.put_line('commit;');
dbms_output.put_line('Successfully completed script R293076_insert_Cellsite_updates.sql');