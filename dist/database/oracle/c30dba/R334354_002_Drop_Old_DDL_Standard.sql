set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/gcibin/deployconfigs/R334354_002_Drop_Old_DDL_Standard_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: R334354_002_Drop_Old_DDL_Standard.sql
prompt CA Ticket# : R334354
prompt QC Defect# : N/A
prompt System(s)  : K2 and K3
prompt Database(s): ALL
prompt Schema(s)  : C30DBA
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Drop old versions of DDL Standard procedure
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt .................: It is, however, completely rerunnable
prompt Acceptable Errors: None
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 12/04/2013 ##       R334354 - Initial development
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt ** It is, however, completely rerunnable
prompt *****************************************************************************************************
declare
   l_sqlerrm   varchar2(2000);
begin
dbms_output.put(chr(10));

-- all of these are somewhere, but not everywhere, hence using "no error" proc...
c30_no_error('drop public     synonym  c30_ddl_standard_arbor',l_sqlerrm);
if l_sqlerrm not like 'ORA-01432:%' then c30_put_line(null,l_sqlerrm); end if;
c30_no_error('drop public     synonym  c30_ddl_standard_c30arbor',l_sqlerrm);
if l_sqlerrm not like 'ORA-01432:%' then c30_put_line(null,l_sqlerrm); end if;
c30_no_error('drop public     synonym  c30_ddl_standard_c30sdk',l_sqlerrm);
if l_sqlerrm not like 'ORA-01432:%' then c30_put_line(null,l_sqlerrm); end if;
c30_no_error('drop public     synonym  c30_ddl_standard_c30api',l_sqlerrm);
if l_sqlerrm not like 'ORA-01432:%' then c30_put_line(null,l_sqlerrm); end if;
c30_no_error('drop public     synonym  c30_ddl_standard',l_sqlerrm);
if l_sqlerrm not like 'ORA-01432:%' then c30_put_line(null,l_sqlerrm); end if;

c30_no_error('drop procedure     arbor.c30_ddl_standard_arbor',l_sqlerrm);
if l_sqlerrm not like 'ORA-04043:%' and l_sqlerrm not like 'ORA-01435:%' then c30_put_line(null,'1-'||l_sqlerrm); end if;
c30_no_error('drop procedure  c30arbor.c30_ddl_standard_c30arbor',l_sqlerrm);
if l_sqlerrm not like 'ORA-04043:%' and l_sqlerrm not like 'ORA-01435:%' then c30_put_line(null,'2-'||l_sqlerrm); end if;
c30_no_error('drop procedure    c30sdk.c30_ddl_standard_c30sdk',l_sqlerrm);
if l_sqlerrm not like 'ORA-04043:%' and l_sqlerrm not like 'ORA-01435:%' then c30_put_line(null,'3-'||l_sqlerrm); end if;
c30_no_error('drop procedure    c30api.c30_ddl_standard_c30api',l_sqlerrm);
if l_sqlerrm not like 'ORA-04043:%' and l_sqlerrm not like 'ORA-01435:%' then c30_put_line(null,'4-'||l_sqlerrm); end if;
c30_no_error('drop procedure arborarch.c30_ddl_standard',l_sqlerrm);
if l_sqlerrm not like 'ORA-04043:%' and l_sqlerrm not like 'ORA-01435:%' then c30_put_line(null,'5-'||l_sqlerrm); end if;

-- life's good -- issue final commit and send to dbms_output
c30_put_line(null,'Successfully completed script for R334354_002_Drop_Old_DDL_Standard');
end;
/
--spool off
exit rollback