set echo off serveroutput on size 1000000 linesize 1000 pagesize 50000 trimspool on tab off
--spool /opt/app/kenanfx2/arbor/gcibin/deployconfigs/001_create_c30_ddl_standard_proc_&_connect_identifier..txt
prompt **************************************************************************************************
prompt Release Notes
prompt Script Name: 001_create_c30_ddl_standard_proc.sql
prompt CA Ticket# : R334354
prompt QC Defect# : N/A
prompt System(s)  : K2 and K3
prompt Database(s): All
prompt Schema(s)  : C30DBA
prompt Outage Req : No
prompt Depends on : N/A
prompt Purpose of Script: Create C30_DDL_STANDARD procedure
prompt Expected Outcome : Script cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt Acceptable Errors: You may see "ORA-01435: user does not exist" in log
prompt *****************************************************************************************************
prompt Revision Block
prompt Author Email                     ## Date       ##       Description (One revision per line, if multiple revisions)
prompt chip.lafurney@cycle30.com        ## 12/04/2013 ##       R334354 - Initial development
prompt *****************************************************************************************************
prompt Special instructions:
prompt Contact Information: Chip LaFurney, 541-678-3368
prompt Replicate  : No
prompt BRE Code   : No
prompt THS Code   : No
prompt *****************************************************************************************************
prompt ** This cannot be "All or Nothing"; object drop/create cannot be rolled back
prompt *****************************************************************************************************
create or replace procedure c30dba.c30_ddl_standard(
   v_object_name    IN  varchar2 := null -- required, but we'll validate it
  ,v_owner          IN  varchar2 := null -- optional, and we'll validate it
  ,v_object_type    IN  varchar2 := null -- optional, and we'll validate it
  ,v_no_dml         IN  varchar2 := null -- new parameter; any non-blank passed means "no DML" (applicable to procedures and packages only)
  ,v_email_address  IN  varchar2 := null -- optional; pass your email address to get a log sent to you
   )
as
-- Release Notes
-- Config (n/a)
-- Date: 12/04/2013
-- Created By: Chip H.F. LaFurney
-- Defect Resolved: Kenan SQL API
--
-- Script file Name: 001_create_c30_ddl_standard_proc.sql
-- Version 1.0
-- DETAILED EXPLANATION:
-- Create C30_DDL_STANDARD procedure
--**** WARNING TO DEVELOPER:  You cannot use c30/gci_put_line in this proc.  They need to be able to use c30_ddl_standard for their own public synonym and grants.
   c_lf           CONSTANT varchar2(2) := chr(10);
   c_cr_lf        CONSTANT varchar2(2) := chr(13)||chr(10);

   l_body                  varchar2(32767);
   l_msg                   varchar2(2000);
   l_object_name           sys.dba_objects.object_name%type;
   l_object_name_tmp       sys.dba_objects.object_name%type;
   l_owner                 sys.dba_objects.owner%type;
   l_object_type           sys.dba_objects.object_type%type;
   l_spec_only             Boolean := false;
   l_status                sys.dba_objects.status%type;
   l_count                 pls_integer;
   l_count_user            pls_integer;
   l_min_owner             sys.dba_objects.owner%type;
   l_max_owner             sys.dba_objects.owner%type;
   l_min_object_type       sys.dba_objects.object_type%type;
   l_max_object_type       sys.dba_objects.object_type%type;
   l_max_text              pls_integer;
   type t_owner_all_csv is table of varchar2(100) -- CSV like 'xxx' or 'xxx,yyy,zzz'
                           index by varchar2(30); -- owner name
   l_owner_all_csv         t_owner_all_csv;
   l_all_csv               varchar2(100);

begin
dbms_output.put(c_lf);

l_msg  := '**** C30_DDL_STANDARD PROCEDURE ****';
l_body := l_msg;
--**!!dbms_output.put_line('(DDLstd) '||l_msg);
l_msg  := 'Input values v_object_name='||nvl(v_object_name,'<null>')||'; v_owner='||nvl(v_owner,'<null>')||'; v_object_type='||nvl(v_object_type,'<null>')||'; v_email_address="'||nvl(v_email_address,'<null>')||'"';
l_body := l_body||c_lf||l_msg;
--**!!dbms_output.put_line('(DDLstd) '||l_msg);

-- validate object name...
if v_object_name is not null then
   l_object_name := upper(trim(v_object_name));
   if l_object_name = 'C30_DDL_STANDARD' then
      l_msg  := c_lf||'C30_DDL_STANDARD - cannot be called for itself';
      l_body := l_body||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);
      -- no need to send email for this error
      return;
   end if;
else
   l_msg  := c_lf||'C30_DDL_STANDARD - v_object_name parameter is required';
   l_body := l_body||c_lf||l_msg;
   dbms_output.put_line(c_lf||l_msg);
   -- no need to send email for this error
   return;
end if;

l_msg  := 'Processing l_object_name='||l_object_name;
l_body := l_body||c_lf||l_msg;
--**!!dbms_output.put_line('(DDLstd) '||l_msg);

-- check for each piece separately; first, the owner...
if trim(v_owner) is null then
   begin
   select   count(*), object_name    ,  min(owner), max(owner)
     into l_count,  l_object_name_tmp,l_min_owner,l_max_owner
     from sys.dba_objects
    where(owner     like 'C30%' or owner like 'GCI%' or owner like '%ARBOR' or owner = user)
      and upper(object_name) = l_object_name
      and object_type        = nvl(upper(trim(v_object_type)),object_type)
      and object_type       != 'SYNONYM'
   group by object_name
   ;
   exception
      when no_data_found then
         l_count := 0;
   end;

   if l_count = 0 then
      l_msg  := c_lf||'C30_DDL_STANDARD - Cannot find object_name "'||v_object_name||'"';
      l_body := l_body||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);
      -- no need to send email for this error
      return;
   elsif l_min_owner = l_max_owner then -- always true if l_count = 1
      l_owner := l_min_owner;
   else
      -- if one of the multiple owners is "user", go with that...
      select  count(*)
       into l_count_user
       from sys.dba_objects
      where owner       = user
        and object_name = l_object_name_tmp
        and object_type = nvl(upper(trim(v_object_type)),object_type)
      ;
      if l_count_user = 1 then
         l_owner := user;
      else
         l_msg  := c_lf||'C30_DDL_STANDARD Logic Error! - Found '||l_count||' owners for object_name "'||v_object_name||'"; min/max owners='||l_min_owner||'/'||l_max_owner;
         l_body := l_body||c_lf||l_msg;
         dbms_output.put_line(c_lf||l_msg);
         -- no need to send email for this error
         if v_email_address is not null then
            c30_send_email('cycle30devdatabasealerts@gci.com'||case when v_email_address is not null then ';'||v_email_address end,'From c30_ddl_standard (LOGIC ERROR)',replace(l_body,c_lf,c_cr_lf));
         end if;
         return;
      end if;
   end if;
else
   l_owner := upper(trim(v_owner));
end if;

-- check if we've found a non-uppercase object_name (like, with JAVA objects)...
if l_object_name_tmp <> l_object_name then
   l_object_name := l_object_name_tmp;
   l_msg  := 'Mixed case l_object_name='||l_object_name;
   l_body := l_body||c_lf||l_msg;
   --**!!dbms_output.put_line('(DDLstd) '||l_msg);
end if;

l_msg  := 'Processing l_owner='||l_owner||'; l_count='||l_count;
l_body := l_body||c_lf||l_msg;
--**!!dbms_output.put_line('(DDLstd) '||l_msg);

-- next, the object_type...
if trim(v_object_type) is null then
   select  count(*), min(object_type),  max(object_type)
    into l_count,  l_min_object_type, l_max_object_type
    from sys.dba_objects
   where owner        = l_owner
     and object_name  = l_object_name
     and object_type != 'SYNONYM'
   ;
   if l_count = 0 then
      l_msg  := c_lf||'C30_DDL_STANDARD Logic Error! - Cannot find object_type for "'||l_owner||'.'||v_object_name||'"';
      l_body := l_body||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);
      if v_email_address is not null then
         c30_send_email('cycle30devdatabasealerts@gci.com'||case when v_email_address is not null then ';'||v_email_address end,'From c30_ddl_standard (LOGIC ERROR)',replace(l_body,c_lf,c_cr_lf));
      end if;
      return;
   -- check if the 1st four characters of the min/max object_types are the same; again we're handling
   -- INDEX+INDEX PARTITION; PACKAGE+PACKAGE BODY; TABLE+TABLE PARTITION+TABLE SUBPARTITION; TYPE+TYPE BODY
   elsif substr(l_min_object_type,1,4) = substr(l_max_object_type,1,4) then -- always true if l_count = 1
      -- for count > 1, pick "TABLE", "INDEX", "TYPE BODY" or "PACKAGE BODY"; the latter pair are the "max"...
      l_msg  := 'Found '||l_count||' object types for "'||l_owner||'.'||l_object_name||'"; min/max types='||l_min_object_type||'/'||l_max_object_type;
      l_body := l_body||c_lf||l_msg;
      --**!!dbms_output.put_line('(DDLstd) '||l_msg);
      if l_max_object_type IN('TYPE BODY','PACKAGE BODY') then
         l_object_type := l_max_object_type;
      -- deal with types/packages with only a SPEC (no body); examples arbor.CV_TYPES and arbor.VIEW_TYPES
      elsif l_count            = 1
        and l_max_object_type IN('TYPE','PACKAGE') then
         l_object_type := l_max_object_type;
         l_spec_only   := true;
         l_msg  := 'Setting l_spec_only := true for object_type='||l_object_type;
         l_body := l_body||c_lf||l_msg;
         --**!!dbms_output.put_line('(DDLstd) '||l_msg);
      else
         l_object_type := l_min_object_type;
      end if;
   else
      l_msg  := c_lf||'C30_DDL_STANDARD Logic Error! - Found '||l_count||' object types for "'||l_owner||'.'||v_object_name||'"; min/max types='||l_min_object_type||'/'||l_max_object_type;
      l_body := l_body||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);
      if v_email_address is not null then
         c30_send_email('cycle30devdatabasealerts@gci.com'||case when v_email_address is not null then ';'||v_email_address end,'From c30_ddl_standard (LOGIC ERROR)',replace(l_body,c_lf,c_cr_lf));
      end if;
      return;
   end if;
else
   l_object_type := upper(trim(v_object_type));
end if;

l_msg  := 'Processing l_object_type='||l_object_type;
l_body := l_body||c_lf||l_msg;
--**!!dbms_output.put_line('(DDLstd) '||l_msg);

-- the object must be valid...
select   status
  into l_status
  from sys.dba_objects
 where owner        = l_owner
   and object_name  = l_object_name
   and object_type  = l_object_type
;
if l_status != upper('valid') then
   l_msg  := c_lf||'C30_DDL_STANDARD '||l_object_type||' '||l_owner||'.'||l_object_name||' is status "'||l_status||'"; will not continue';
   l_body := l_body||c_lf||l_msg;
   dbms_output.put_line(c_lf||l_msg);
   l_msg  := '...Listing of object lines with error[s]:';
   l_body := l_body||c_lf||l_msg;
   dbms_output.put_line(c_lf||l_msg);
   -- deal with "invalid" objects by outputting the line[s] with the error[s]...
   select max(length(cleantext(dsr.text,' ')))+1
     into l_max_text
     from sys.dba_source dsr
    where dsr.owner = l_owner
      and dsr.name  = l_object_name
      and dsr.type  = l_object_type
   ;
   for l_err IN(
      select distinct '...'||replace(to_char(dsr.line,'fm000000'),'0','.')||
           case when dsr.line = der.line then '*' else ' ' end||
           rpad(cleantext(dsr.text,' '),l_max_text)||' '||
           case when dsr.line = der.line then
               (select csv_string(cursor(
                select cleantext(err.text,' ')
                  from sys.dba_errors err
                 where err.owner = der.owner
                   and err.name  = der.name
                   and err.type  = der.type
                   and err.line  = der.line
                ),'; ') from dual)
             end line
        from sys.dba_errors der
        join sys.dba_source dsr
          on dsr.owner      = der.owner
         and dsr.name       = der.name
         and dsr.type       = der.type
         and dsr.line between(der.line-3) and (der.line+3)
       where der.owner = l_owner
         and der.name  = l_object_name
         and der.type  = l_object_type
      order by 1
               ) loop
      l_msg  := substr(l_err.line,1,2000); -- don't let it overflow
      l_body := l_body||c_lf||l_msg;
      dbms_output.put_line(l_msg);
      if length(l_body) > 32000 then
         l_msg  := '>>>OVERFLOW<<<';
         l_body := l_body||c_lf||l_msg;
         dbms_output.put_line(l_msg);
         exit;
      end if;
   end loop;
   if v_email_address is not null then
      c30_send_email('cycle30devdatabasealerts@gci.com'||case when v_email_address is not null then ';'||v_email_address end,'From c30_ddl_standard (LOGIC ERROR)',replace(l_body,c_lf,c_cr_lf));
   end if;
   return;
end if;

-- there are only certain object types we care about for public synonyms and grants...
if l_object_type IN('TABLE','VIEW','SEQUENCE','CONTEXT','FUNCTION','TYPE BODY','PACKAGE BODY','PROCEDURE')
or l_spec_only then
   -- create the public synonym (no real "loop" here, just easier to clone)
   for l_sql IN(
      select 'create or replace public synonym '||l_object_name||' for '||l_owner||'.'||l_object_name cmd
        from sys.dual
               ) loop
      l_msg  := l_sql.cmd||';';
      l_body := l_body||c_lf||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);

      execute immediate l_sql.cmd;

      l_msg  := case when sqlerrm like 'ORA-0000%' then 'Synonym created.' else sqlerrm end;
      l_body := l_body||c_lf||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);
   end loop;

   -- deal with which grantee[s] get the "all" privilege...

   -- load up array of owner to who-gets-"ALL"-grant CSVs
   l_owner_all_csv('ARBOR')    := 'gciarbor,c30arbor';
   l_owner_all_csv('GCIARBOR') := 'arbor';

   begin
   l_all_csv := l_owner_all_csv(l_owner);
   l_msg := 'With l_owner "'||l_owner||'"; csv="'||l_all_csv||'"';
   l_body := l_body||c_lf||c_lf||l_msg;
   --**!!dbms_output.put_line('(DDLstd) '||l_msg);
   exception
      when no_data_found then
         l_all_csv := 'arbor'; -- default
         l_msg := 'DEFAULT l_owner "'||l_owner||'" csv="'||l_all_csv||'"';
         l_body := l_body||c_lf||c_lf||l_msg;
         --**!!dbms_output.put_line('(DDLstd) '||l_msg);
   end;

   for l_csv IN(select upper(trim(column_value)) grantee from table(cast(csv_unstring(l_all_csv) as varchar2_4000_table))) loop
      l_msg := 'Set grantee="'||l_csv.grantee||'"';
      l_body := l_body||c_lf||c_lf||l_msg;
      --**!!dbms_output.put_line('(DDLstd) '||l_msg);

      -- we still loop through this to make sure the grantee is a valid user in this environment...
      for l_sql IN(
         select 'grant all on '||l_owner||'.'||l_object_name||' to '||username||' with grant option' cmd
           from sys.dba_users
          where username = l_csv.grantee
                  ) loop
         l_msg  := l_sql.cmd||';';
         l_body := l_body||c_lf||c_lf||l_msg;
         dbms_output.put_line(c_lf||l_msg);

         execute immediate l_sql.cmd;

         l_msg  := case when sqlerrm like 'ORA-0000%' then 'Grant succeeded.' else sqlerrm end;
         l_body := l_body||c_lf||c_lf||l_msg;
         dbms_output.put_line(c_lf||l_msg);
      end loop; -- l_sql
   end loop; -- l_csv

   -- below is set up as "select" statements on usernames and roles that should exist, but won't cause the statements to fail if they don't...
   for l_sql IN(
      select 'grant all on '||l_owner||'.'||l_object_name||' to '||role cmd
        from sys.dba_roles
       where role = 'ONEVIEW_TECH'
      UNION
      -- select,references privileges on a TABLE, VIEW, or SEQUENCE do not depend on v_no_dml switch...
      select 'grant select on '||l_owner||'.'||l_object_name||' to '||role cmd  --note: "ORA-01931: cannot grant REFERENCES to a role"
        from sys.dba_roles
       where role = 'ONEVIEW_SELECTALL'
         and l_object_type IN('TABLE','VIEW','SEQUENCE')
      UNION
      select 'grant select'||case when l_object_type <> 'SEQUENCE' then ',references' end||' on '||l_owner||'.'||l_object_name||' to '||username cmd
        from sys.dba_users
       where username IN('GCIREPORT','C30REPORT','C30TEMP')
         and l_object_type IN('TABLE','VIEW','SEQUENCE')
      UNION
      -- execute privileges on a CONTEXT, FUNCTION or TYPE BODY do not depend on v_no_dml switch...
      select 'grant execute on '||l_owner||'.'||l_object_name||' to '||role cmd
        from sys.dba_roles
       where role = 'ONEVIEW_SELECTALL'
         and l_object_type IN('CONTEXT','FUNCTION','TYPE BODY')
      UNION
      select 'grant execute on '||l_owner||'.'||l_object_name||' to '||username cmd
        from sys.dba_users
       where username IN('GCIREPORT','C30REPORT','C30TEMP')
         and l_object_type IN('CONTEXT','FUNCTION','TYPE BODY')
      UNION
      --                                              ***********************************
      -- execute privileges on a PROCEDURE or PACKAGE *****DEPENDS ON V_NO_DML SWITCH****
      --                                              ***********************************...
      select 'grant execute on '||l_owner||'.'||l_object_name||' to '||role cmd
        from sys.dba_roles
       where role = 'ONEVIEW_SELECTALL'
         and l_object_type not IN('TABLE','VIEW','SEQUENCE','CONTEXT','FUNCTION','TYPE','TYPE BODY') -- "not IN" syntax allows for "l_spec_only" packages
         and trim(v_no_dml) is not null
      UNION
      select 'grant execute on '||l_owner||'.'||l_object_name||' to '||username cmd
        from sys.dba_users
       where username IN('GCIREPORT','C30REPORT','C30TEMP')
         and l_object_type not IN('TABLE','VIEW','SEQUENCE','CONTEXT','FUNCTION','TYPE','TYPE BODY') -- "not IN" syntax allows for "l_spec_only" packages
         and trim(v_no_dml) is not null
               ) loop
      l_msg  := l_sql.cmd||';';
      l_body := l_body||c_lf||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);

      execute immediate l_sql.cmd;

      l_msg  := case when sqlerrm like 'ORA-0000%' then 'Grant succeeded.' else sqlerrm end;
      l_body := l_body||c_lf||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);
   end loop;

   -- warn developer about new parameter...
   if l_object_type not IN('TABLE','VIEW','SEQUENCE','CONTEXT','FUNCTION','TYPE','TYPE BODY') -- "not IN" syntax allows for "l_spec_only" packages
   and trim(v_no_dml) is null then
      l_msg  := '* * * * Execute Privileges NOT Granted on '||l_owner||'.'||l_object_name||' to Read-Only Users/Roles';
      l_body := l_body||c_lf||c_lf||l_msg;
      dbms_output.put_line(c_lf||l_msg);
      l_msg  := '* * * * You must pass ",v_no_dml=>1" (any non-blank) to override this (confirming it does no DML)';
      l_body := l_body||c_lf||l_msg;
      dbms_output.put_line(l_msg);
   end if;
else -- l_object_type not IN('TABLE','VIEW','SEQUENCE','CONTEXT','FUNCTION','TYPE BODY','PACKAGE BODY','PROCEDURE')
   -- other typical object types are 'SEQUENCE','LOB','QUEUE','TRIGGER','SYNONYM'
   l_msg  := 'Skipping '||l_object_type||' '||l_owner||'.'||l_object_name||' (no public synonym or grants)';
   l_body := l_body||c_lf||c_lf||l_msg;
   dbms_output.put_line(c_lf||l_msg);
end if;

l_msg  := '*ATTENTION RELEASE MANAGEMENT*';
l_body := l_body||c_lf||c_lf||l_msg;
dbms_output.put_line(c_lf||l_msg);
l_msg  := 'Successfully completed script for '||l_object_type||' '||l_owner||'.'||l_object_name;
l_body := l_body||c_lf||c_lf||l_msg;
dbms_output.put_line(c_lf||l_msg);

if v_email_address is not null then
   c30_send_email(v_email_address,'From c30_ddl_standard',replace(l_body,c_lf,c_cr_lf));
end if;

exception
   when others then
      l_body := l_body||c_lf||c_lf||'ERROR! sqlerrm='||sqlerrm;
      c30_send_email('cycle30devdatabasealerts@gci.com'||case when v_email_address is not null then ';'||v_email_address end,'From c30_ddl_standard (FATAL ERROR)',replace(l_body,c_lf,c_cr_lf));
      raise;
end c30_ddl_standard;
/
show errors
create or replace public synonym c30_ddl_standard for c30dba.c30_ddl_standard
;
-- "everyone" should be able to execute this...
grant execute on c30_ddl_standard to public
;
--spool off
exit rollback