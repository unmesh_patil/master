#!/bin/sh

ps auxw | grep BatchFlowAgent | grep -v grep > /dev/null

if [ $? != 0 ]
then
  mv $HOME/logs/batchAgent.log $HOME/logs/batchAgent_$(date +"%Y%m%d%H%M").log
  nohup $HOME/bin/batchAgent.sh > $HOME/logs/batchAgent.log 2>&1&
fi

