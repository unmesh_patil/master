#!/bin/bash

#. /opt/app/kenanfx2/batch/gci.bashrc


APPNAME="WpMonitor Program"
echo Running $APPNAME


ARBORDIR=/opt/app/kenanfx2/arbor ; export ARBORDIR
BATCHHOME=$HOME ; export BATCHHOME

JAVA=${JAVA_HOME}/bin/java
BATCH_LIB_DIR=${BATCHHOME}/java/lib/local ; export BATCH_LIB_DIR 
SHARED_LIB_DIR=${BATCHHOME}/java/lib/local  ; export SHARED_LIB_DIR 
EXT_LIB_DIR=$BATCHHOME/java/lib/ext   ; export EXT_LIB_DIR 
CONFIG_DIR=${BATCHHOME}/java/conf   ; export CONFIG_DIR 


ORACLE_LIB_PATH=$ORACLE_HOME/jdbc/lib/ojdbc14.jar ; export ORACLE_LIB_PATH 

APP_INST_DIR=$BATCH_LIB_DIR  ; export APP_INST_DIR 

APPJAR=${APP_INST_DIR}/ProcessMonitor.jar   ; export APPJAR


ORACLE_HOME=/opt/app/oracle/product/11.2.0/client_1; export  ORACLE_HOME


APPJAR=$APP_INST_DIR/ProcessMonitor.jar; export APPJAR

JAVA_HOME=/opt/jdk1.5.0_22; export JAVA_HOME
CLASS_PATH=$APITS_CLIENT_DIR/apits-lib; export CLASS_PATH

CLASSPATH=${BATCHHOME}/lib/ojdbc6.jar; export CLASSPATH


CLASSPATH=$CLASSPATH:}:${APPJAR}; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${SHARED_LIB_DIR}/batchfeedsfwk.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${SHARED_LIB_DIR}/batchfeedscommon.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${SHARED_LIB_DIR}/UtilityFramework.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${SHARED_LIB_DIR}/objectspace.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/mail.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/wpClient.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/wpCommon.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/WorkPointProperties.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/xercesImpl.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/xml-api.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/xmlunit-1.1.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/log4j-1.2.8.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/commons-httpclient-2.0-alpha3.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/commons-logging-1.0.3.jar ; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/ojdbc6.jar; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/dms.jar; export CLASSPATH
CLASSPATH=$CLASSPATH:}:${EXT_LIB_DIR}/ojdl.jar; export CLASSPATH

for i in `ls -1 ${SHARED_LIB_DIR}/C30MON_*.jar` ; do
export CLASSPATH=$CLASSPATH:$i
done

# Dynamic Linker Path
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/app/kenanfx2/arbor/site_specific/apits/javaclient/lib ; export LD_LIBRARY_PATH
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${ARBORDIR}/bsdm_site/lib   ; export LD_LIBRARY_PATH
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${TUXDIR}/lib ;  export LD_LIBRARY_PATH

LOGDIR=${BATCHHOME}/logs
REPORTDIR=${LOGDIR}/reports
LOGFILENAME=${LOGDIR}/logs/collbat.${TIMESTAMP_ID}
LOCKDIR=${REPORTDIR}/wpLock

#if [ -d "$LOCKDIR" ]; then
    #echo WpMonitor is already running. 
    #echo Lock Dir $LOCKDIR found.  
    #echo Exiting.
#    echo 1
#    exit 1
#fi

#### PARAMETER DEFINITIONS  ##############################################################################################
# configdir = place where an alternative properties file can be found will be in the directory contained in the value of the shell variable CONFIG_DIR. 
#   homedir = application home directory path, is established by the shell variable $HOME.
########################################################################################################################


java -Dconfigdir=$CONFIG_DIR -Dhomedir=$HOME   com.cycle30.monitor.k3.MON_WP $REPORTDIR $1  $2 $3  
#&

echo "($APPNAME) Return code = $?"

exit $?

