#!/bin/sh

# Check for the right number of arguments.
if [ "$#" -lt "3" ]; then
  echo "Usage: $0 <revision> <userid> <password> [<branchtag>]"
  exit 1
else
  if [ $# = 3 ]; then
    REVISION=$1;
    SVN_USER=$2;
    SVN_PASSWORD=$3;
    BRANCH="trunk";
  else
    if [ $# = 4 ]; then
      REVISION=$1;
      SVN_USER=$2;
      SVN_PASSWORD=$3;
      BRANCH=branches/$4;
    else
      echo "Usage: $0 <revision> <userid> <password> [<branchtag>]"
    fi
  fi
fi

SHORTREV=`expr $REVISION - 1`;
BASE_DEST=dist
BASE_TRUNK=dist/$BRANCH
PROJ=batch
DEST=$BASE_DEST/$PROJ;

DEPLOY_DIR=$HOME/deploygci

REVISION_RANGE=$SHORTREV:$1

#echo REVISION_RANGE=$REVISION_RANGE

BASE_URL=https://subversion.assembla.com/svn/c30billing/awn/integration

svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $REVISION_RANGE $BASE_URL/$BASE_TRUNK/$PROJ | sed -e "s/^MM*[ \t]*//" | sed -e "s/^ [AM][ \t]*//" | sed -e "s/^A[ \t]*//" | sed -e "/^D[ \t]*/ d" | sed -e "s|$BASE_URL/$BASE_TRUNK/||" > tmp.file
svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $REVISION_RANGE $BASE_URL/$BASE_TRUNK/$PROJ | sed -e "/^MM*[ \t]*/ d" | sed -e "/^ [AM][ \t]*/ d" | sed -e "/^A[ \t]*/ d" | sed -e "s/^D[ \t]*//" | sed -e "s|$BASE_URL/$BASE_TRUNK/$PROJ/||" > tmpd.file

# echo done with diff
# echo DEPLOY=[$DEPLOY_DIR/$DEST]
if [ -e $DEPLOY_DIR/$DEST ]
   then
     rm -rf $DEPLOY_DIR/$DEST
fi

#echo done with directory check 

echo Done with export of buildscripts

while read line
do
#  echo line=$line
#  echo dirname=`dirname $line`

  echo Deleting $line from $HOME
  rm -f $HOME/$line
done < tmpd.file
rm -f tmpd.file
echo Done reading delete list

while read line
do
  HAS_FILES=1
#  echo line=$line
#  echo dirname=`dirname $line`

#  echo Checking out $line to $DEPLOY_DIR/$BASE_DEST/$line
  mkdir -p $DEPLOY_DIR/$BASE_DEST/`dirname $line`
svn export --username=$SVN_USER --password=$SVN_PASSWORD -r $REVISION $BASE_URL/$BASE_TRUNK/$line $DEPLOY_DIR/$BASE_DEST/$line
#svn export --username=$SVN_USER --password=$SVN_PASSWORD -r$REVISION $BASE_URL/dist/$BRANCH/$line $DEPLOY_DIR/$DEST/$line
done < tmp.file
rm -f tmp.file
echo Done reading list


#BUILD MODULE
if [ -n "$HAS_FILES" ]; then
  svn export --force --username=$SVN_USER --password=$SVN_PASSWORD $BASE_URL/dist/$BRANCH/buildscripts/batch $DEPLOY_DIR/$DEST

  tolower() { echo $1 | tr "[:upper:]" "[:lower:]"; }
  DEPLOY_ENVIRONMENT=$(tolower ${SYSTEM_NAME}${SYSTEM_TYPE}${ENVIRONMENT})

  cd $DEPLOY_DIR/$DEST
  ant -DENVIRONMENT=$DEPLOY_ENVIRONMENT
else
   echo NO FILES FOUND TO BE DEPLOYED
fi

rm -rf $DEPLOY_DIR
