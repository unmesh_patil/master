#!/bin/sh
#
#-----------------------------------------------------------------------------
#
# daemon script for running Jenkins Continuous Integration server
# on Red Hat Linux Enterprise. Utilizes /etc/rc.d/init.d/functions for
# the 'daemon' functionality.
#
# Author:   Mark Dougherty
# Date:     10/18/2012
#
#-----------------------------------------------------------------------------


# Source the RHEL daemon function library.
. /etc/rc.d/init.d/functions

# Set env variables
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="Jenkins CI"
DAEMON_USER=jenkinsci
PIDFILE=/opt/app/jenkinsci/jenkins.pid
JENKINS_ARGS="--httpPort=50080 --logfile=/opt/app/jenkinsci/logs/jenkins_`date +"%Y-%m-%d_%H-%M"`.log"
JAVA=/opt/jdk1.6.0_18/bin/java
DAEMON="$JAVA -jar /opt/app/jenkinsci/jenkins.war $JENKINS_ARGS"


start() {
        echo -n $"Starting $NAME... "
        daemon --pidfile="$PIDFILE" "$DAEMON &"
        pid=`ps -ef | grep jenkins.war | grep -v grep | awk '{print $2}'`
        if [ -n "$pid" ]; then
            echo $pid > "$PIDFILE"
        fi
        echo ""
        return $RETVAL
}
stop() {
        echo -n $"Stopping $NAME... "
        killproc -p "$PIDFILE" -d 10 "$DAEMON"
        RETVAL="$?"
        echo
        [ $RETVAL = 0 ] && rm -f "$PIDFILE"
        return "$RETVAL"
}

case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        stop
        start
        ;;
  *)
        echo "Usage: $NAME {start|stop|restart}" >&2
        exit 1
        ;;
esac

exit $RETVAL