sqlplus -s /opt/app/kenanfx2/batch/LineStatusQuery.log <<EOF > /opt/app/kenanfx2/batch/LineStatusQueryResult.log
connect c30batch/c3004batch@c3b3dev
select LINE_NUMBER, CURRENT_STATUS, LAST_UPDATED from c30_sdk_line_item where SRC_DATA_CONTAINER_ID=$1 and CURRENT_STATUS in ('Read Error', 'Validation Error', 'Write Error', 'Failed') and rownum=$2 order by LINE_NUMBER asc 
exit
EOF
# Pass the First Argument as the SDCID and the Second Argument as the Row limit