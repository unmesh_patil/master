sqlplus -s /opt/app/kenanfx2/batch/FileStatusQuery.log <<EOF > /opt/app/kenanfx2/batch/FileStatusQueryResult.log
connect c30batch/c3004batch@c3b3dev
 SELECT SRC_DATA_CONTAINER_ID AS SDCID, CURRENT_RETRY_COUNT AS RETRY_COUNT, CREATED, LAST_UPDATED, RESOURCE_NAME AS FILE_NAME, SDS.SRC_DATA_CONTAINER_STATUS_NAME AS STATUS_NAME FROM C30_SDK_SRC_DATA_CONTAINER SDC, C30_SDK_SRC_DAT_CONTAINER_STAT SDS WHERE SDC.CURRENT_STATUS_ID = SDS.SRC_DATA_CONTAINER_STATUS_ID and batch_flow_name=$1 and limit<=$2 desc
exit
EOF
#First Argument is the Batch Flow Name and the Second Argument is the Row Number 


