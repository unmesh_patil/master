To run File status, you need 3 arguments:
command, batchFlowName, maxRecordToDisplay
For example:
	fileStatus, LCF_BATCH_FLOW, 2
	
	And here is the output:
	This line is not printed #FILE_STATUS:sdcId, retry_count, created, last_updated, file name, status name
	FILE_STATUS:2,1,1:16:01 PM,1:19:29 PM,lcfFileSample_regular_03.csv,Completed Success
	
To run Line status, you need 3 arguments:
command, sdcId, maxRecordToDisplay
For example:
	lineStatus, 2, 20
	
	And here is the output:
	This line is not printed #LINE_STATUS:LINE_NUMBER, LAST_UPDATED, CURRENT_STATUS
	LINE_STATUS:3,10:36:46 AM,Validation Error
	LINE_STATUS:5,10:36:46 AM,Failed
	LINE_STATUS:7,10:39:49 AM,Failed
