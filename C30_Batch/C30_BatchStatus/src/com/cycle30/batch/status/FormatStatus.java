/**
 * 
 */
package com.cycle30.batch.status;

import java.text.MessageFormat;
import java.util.Map;

/**
 * @author Venkat Bhat
 *
 */
public class FormatStatus {
	//id, retry_count, created, last_updated, fine name, status name
	static final String SDC_STATUS_MSG = 
			"FILE_STATUS:{0,number,#},{1,number,#},{2,date,yyyyMMdd_HHmmss},{3,date,yyyyMMdd_HHmmss},{4},{5}";
	
	//LINE_NUMBER, LAST_UPDATED, CURRENT_STATUS
		static final String LINE_STATUS_MSG = 
				"LINE_STATUS:{0,number,integer},{1,date,yyyyMMdd_HHmmss},{2}";
	
	static MessageFormat SDC_MF = new MessageFormat(SDC_STATUS_MSG);
	
	static MessageFormat LINE_MF = new MessageFormat(LINE_STATUS_MSG);
	
	private static String getSdcStatusAsString(Map<String, Object> statusMap) {
		return SDC_MF.format(getSdcObjects(statusMap));
	}
	
	private static String getLineStatusAsString(Map<String, Object> statusMap) {
		return LINE_MF.format(getLineObjects(statusMap));
	}
	
	public static String getStatusAsString(String kind, Map<String, Object> statusMap) {
		if(kind == null || kind.equalsIgnoreCase("SDC")) {
			return getSdcStatusAsString(statusMap);
		} else if(kind.equalsIgnoreCase("line")) {
			return getLineStatusAsString(statusMap);
		}
		return null;
	}
	
	private static Object[] getLineObjects(Map<String, Object> statusMap) {
		Object[] objects = new Object[3];
		objects[0] = statusMap.get("LINE_NUMBER");
		objects[1] = statusMap.get("LAST_UPDATED");
		objects[2] = statusMap.get("CURRENT_STATUS");
		return objects;
	}
	
	private static Object[] getSdcObjects(Map<String, Object> statusMap) {
		Object[] objects = new Object[6];
		objects[0] = statusMap.get("SRC_DATA_CONTAINER_ID");
		objects[1] = statusMap.get("CURRENT_RETRY_COUNT");
		objects[2] = statusMap.get("CREATED");
		objects[3] = statusMap.get("LAST_UPDATED");
		objects[4] = statusMap.get("RESOURCE_NAME");
		objects[5] = statusMap.get("STATUS_NAME");
		return objects;
	}

}
