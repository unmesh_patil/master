package com.cycle30.batch.status;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class StatusDao {
	
	final static String SELECT_SDC = 
			"select * from (select sdc.SRC_DATA_CONTAINER_ID, sdc.RESOURCE_NAME, sdc.CURRENT_RETRY_COUNT, sdc.CREATED, sdc.LAST_UPDATED, s.src_data_container_status_name as STATUS_NAME from c30_sdk_src_data_container sdc, c30_sdk_src_dat_container_stat s where sdc.batch_flow_name=? and sdc.current_status_id=s.src_data_container_status_id order by sdc.LAST_UPDATED desc) where ROWNUM <= ?";
	
	final static String SELECT_SDC_Resource = 
			"select sdc.SRC_DATA_CONTAINER_ID, sdc.RESOURCE_NAME, sdc.CURRENT_RETRY_COUNT, sdc.CREATED, sdc.LAST_UPDATED, s.src_data_container_status_name as STATUS_NAME from c30_sdk_src_data_container sdc, c30_sdk_src_dat_container_stat s where sdc.batch_flow_name=? and sdc.RESOURCE_NAME=? and sdc.current_status_id=s.src_data_container_status_id order by sdc.LAST_UPDATED desc";
	final static String SELECT_LINE_ITEM =
			"select * from (select LINE_NUMBER, CURRENT_STATUS, LAST_UPDATED from c30_sdk_line_item where SRC_DATA_CONTAINER_ID=? and CURRENT_STATUS in ('Read Error', 'Validation Error', 'Write Error', 'Failed') order by LINE_NUMBER asc ) where ROWNUM <= ?";
	private JdbcTemplate jdbcTemplate;
	
	public List<Map<String, Object>> getSdcStatus(String batchFlowName, String fileName) {
		List<Map<String, Object>> results = jdbcTemplate.query(SELECT_SDC_Resource, 
				new Object[]{batchFlowName,fileName},
				new ColumnMapRowMapper());
		return results;
	}
	public List<Map<String, Object>> getSdcStatus(String batchFlowName, int maxRecords) {
		List<Map<String, Object>> results = jdbcTemplate.query(SELECT_SDC, 
				new Object[]{batchFlowName, new Integer(maxRecords)},
				new ColumnMapRowMapper());
		return results;
	}
	public List<Map<String, Object>> getSdcStatus(Long sdcId, int maxRecords) {
		List<Map<String, Object>> results = jdbcTemplate.query(SELECT_LINE_ITEM, 
				new Object[]{sdcId, new Integer(maxRecords)},
				new ColumnMapRowMapper());
		return results;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	

}
