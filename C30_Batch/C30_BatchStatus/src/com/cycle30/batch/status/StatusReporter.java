/**
 * 
 */
package com.cycle30.batch.status;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.cycle30.batch.status.FormatStatus;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

/**
 * @author Venkat Bhat
 *
 */
public class StatusReporter {
	//private static Logger logger = Logger.getLogger(StatusReporter.class);
	
	String contextName;
	ApplicationContext context;
	
	StatusDao statusDao;
	
	
	public StatusReporter(String contextName) {
		super();
		this.contextName = contextName;
		context = new ClassPathXmlApplicationContext(contextName);
		statusDao = context.getBean("statusDao", StatusDao.class);
	}
	
	public void reportStatus(String batchFlowName, int maxRecords) {
		List<Map<String, Object>> results = statusDao.getSdcStatus(batchFlowName, maxRecords);
		for (Iterator<Map<String, Object>> iterator = results.iterator(); iterator.hasNext();) {
			Map<String, Object> result = iterator.next();
			System.out.println(FormatStatus.getStatusAsString("sdc",result));
		}
	}
	
	public void reportStatus(String batchFlowName, String fileName) {
		List<Map<String, Object>> results = statusDao.getSdcStatus(batchFlowName, fileName);
		if(results == null || results.isEmpty()) {
			System.out.println("Nothing to Print");
			return;
		}
		for (Iterator<Map<String, Object>> iterator = results.iterator(); iterator.hasNext();) {
			Map<String, Object> result = iterator.next();
			System.out.println(FormatStatus.getStatusAsString("sdc",result));
		}
	}
	
	public void reportStatus(Long sdcId, int maxRecords) {
		List<Map<String, Object>> results = statusDao.getSdcStatus(sdcId, maxRecords);
		if(results == null || results.isEmpty()) {
			System.out.println("Nothing to Print");
			return;
		}
		for (Iterator<Map<String, Object>> iterator = results.iterator(); iterator.hasNext();) {
			Map<String, Object> result = iterator.next();
			System.out.println(FormatStatus.getStatusAsString("line",result));
		}
	}

	public static void main(String args[]) throws Exception {
		validateAndExecute(args);
	}
	private static void executeSDCStatusReport(String args[]) throws Exception {
		validateFileStatusArgs(args);
		String batchFlowName = args[1];
		String batchFlowName2 = args[2];
		batchFlowName2 = batchFlowName2.trim();
		int maxRecords = Integer.parseInt(batchFlowName2);
		StatusReporter sr = new StatusReporter("StatusReporterContext.xml");
		sr.reportStatus(batchFlowName, maxRecords);
	}
	
	private static void executeFileStatusReport(String args[]) throws Exception {
		int batchFlowNameIndex = 0;
		int fileNameIndex = 1;
		if(args.length >= 3) {
			batchFlowNameIndex = 1;
			fileNameIndex = 2;
		}
		String batchFlowName = args[batchFlowNameIndex];
		String filename = args[fileNameIndex];
		StatusReporter sr = new StatusReporter("StatusReporterContext.xml");
		sr.reportStatus(batchFlowName,filename);
	}
	
	private static void executeLineStatusReport(String args[]) throws Exception {
		validateLineStatusArgs(args);
		Long sdcId = Long.valueOf(args[1]);
		int maxRecords = Integer.valueOf(args[2]);
		StatusReporter sr = new StatusReporter("StatusReporterContext.xml");
		sr.reportStatus(sdcId, maxRecords);
	}
	
	private static void validateAndExecute(String args[]) throws Exception {
		if(args == null || args.length <1) throw new IllegalStateException("No input provided. Usage is java StatusReporter command(fileStatus|lineStatus), ...");
		String command = args[0];
		Assert.notNull(command, "Command is not provided. Usage is java StatusReporter command(fileStatus|lineStatus), ... ");
		if(command.equalsIgnoreCase("fileStatus")) {
			executeSDCStatusReport(args);
			return;
		} else if(command.equalsIgnoreCase("lineStatus")) {
			executeLineStatusReport(args);
			return;
		} else if(args.length >= 2) {
			executeFileStatusReport(args);
		} else {
			throw new IllegalStateException("Unknown command. Usage is java StatusReporter command(fileStatus|lineStatus), ... ");
		}
		
	}
	
	private static void validateFileStatusArgs(String args[]) throws Exception {
		if(args.length != 3) {
			throw new IllegalStateException("Usage is java StatusReporter fileStatus, LCF_BATCH_flow, maxRecords ");
		}
	}
	
	private static void validateLineStatusArgs(String args[]) throws Exception {
		if(args.length != 3) {
			throw new IllegalStateException("Usage is java StatusReporter lineStatus, sdcId, maxRecords ");
		}
	}
		
	public StatusDao getStatusDao() {
		return statusDao;
	}

	public void setStatusDao(StatusDao statusDao) {
		this.statusDao = statusDao;
	}
	

}
