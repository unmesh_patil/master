/**
 * 
 */
package com.cycle30.rest.support;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.log4j.Logger;

/**
 * @author Venkat Bhat
 *
 */
public class GatewayURL {
	private static Logger logger = Logger.getLogger(GatewayURL.class);
	
	private GatewayUrlRoot gatewayUrlRoot;
	private String path;
	
	private transient URL url;
	private transient URI uri;
	
	public URI getURI() {
		if(uri == null) {
			getURL();
			try {
				uri = url.toURI();
			} catch (URISyntaxException e) {
				logger.error(e);
			}
		}
		return uri;
	}
	
	public URL getURL() {
		if(url == null) {
			try {
				url = new URL(gatewayUrlRoot.getProtocol(), gatewayUrlRoot.getHost(), gatewayUrlRoot.getPort(), path);
			} catch (MalformedURLException e) {
				logger.error(e);
			}
		}
		return url;
	}
	
	
	
	public GatewayUrlRoot getGatewayUrlRoot() {
		return gatewayUrlRoot;
	}
	public void setGatewayUrlRoot(GatewayUrlRoot gatewayUrlRoot) {
		this.gatewayUrlRoot = gatewayUrlRoot;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	

}
