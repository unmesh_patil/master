/**
 * 
 */
package com.cycle30.rest.support;


/**
 * @author Venkat Bhat
 *
 */
public class RestCallStatus {
	
	protected boolean callSuccess;
	protected boolean resourceAccessError = false;
	protected boolean serverError = false;
	protected boolean httpClientError = false;
	
	private Object payLoad;
	private String restCallReference;
	
	public String getRestCallReference() {
		return restCallReference;
	}

	public void setRestCallReference(String restCallReference) {
		this.restCallReference = restCallReference;
	}

	public RestCallStatus() {
		callSuccess = false;
		resourceAccessError = true;
	}
	
	public RestCallStatus(Object payLoad) {
		callSuccess = true;
		this.payLoad = payLoad;
	}
	
	public Object getPayLoad() {
		return payLoad;
	}
	
	public boolean getRetryStatus() {
		if(!callSuccess  && (resourceAccessError || serverError)) return true;
		return false;
	}
	
	public String toString() {
		if(callSuccess) return "Call Success";
		if(resourceAccessError) return "Resource Access Exception";
		if(serverError) return "Server error";
		return "Http Client Error";
	}

	public boolean isCallSuccess() {
		return callSuccess;
	}

	public boolean isResourceAccessError() {
		return resourceAccessError;
	}

	public boolean isServerError() {
		return serverError;
	}

	public boolean isHttpClientError() {
		return httpClientError;
	}
	
	
	
	
	

}
