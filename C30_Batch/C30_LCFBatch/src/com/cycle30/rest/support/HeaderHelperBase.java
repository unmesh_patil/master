/**
 * 
 */
package com.cycle30.rest.support;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * @author Venkat Bhat
 *
 */
public abstract class HeaderHelperBase<T> {
	private static Logger logger = Logger.getLogger(HeaderHelperBase.class);
	
	public static final String X_DIGEST = "X-Digest";
	public static final String X_CLIENT_ID = "X-Org-Id";
	public static final String X_ENV_ID = "X-Env-Id";
	public static final String DATE = "Date";
	public static final String X_TRANS_ID = "X-Trans-Id";
	
	private String orgId;
	private String envId;
	private String secretKey;
	
	private String acsorgId;
	private String acssecretKey;
	
	private static SimpleDateFormat formatDateHeader = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
	private static SimpleDateFormat formatDigest = new SimpleDateFormat("yyyyMMdd");
	private String lastDate;
	private String lastDigest;

	
	protected String getDigest() {
		Date current = new Date();
		String currentDate = formatDigest.format(current);
		//is this different from lastDate ???
		boolean reCalculate = true;

		if(reCalculate) {
			logger.info("Calculating digest...");
			StringBuffer sbuf = new StringBuffer(512);
			sbuf.append(orgId)
			.append(secretKey)
			.append(currentDate);
			String md5Digest = getDigest(sbuf.toString());
			synchronized (this){
				lastDate = currentDate;
				lastDigest = md5Digest;
			}
		}
		return lastDigest;
	}
	
	protected String getACSDigest() {
		Date current = new Date();
		String currentDate = formatDigest.format(current);
		
		//is this different from lastDate ???
		boolean reCalculate = true;

		if(reCalculate) {
			logger.info("Calculating ACS digest...");
			StringBuffer sbuf = new StringBuffer(512);
			sbuf.append(acsorgId)
			.append(acssecretKey)
			.append(currentDate);
			String md5Digest = getDigest(sbuf.toString());
			synchronized (this){
				lastDate = currentDate;
				lastDigest = md5Digest;
			}
		}
		return lastDigest;
	}

	protected synchronized String getDigest(String salt) {
		String md5Digest = "no digest";
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.reset();
			digest.update(salt.getBytes());
			byte[] hash = digest.digest();
			md5Digest = getHexString(hash);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return md5Digest;
	}
	
	protected String getHexString(byte[] hash) {
		StringBuffer sbuf = new StringBuffer(512);
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xFF & hash[i]);
			if(hex.length() == 1) sbuf.append('0');
			sbuf.append(hex);
		}
		return sbuf.toString();
	}
	
	protected String getDateHeader() {
		Date current = new Date();
		return formatDateHeader.format(current);
	}
	
	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getEnvId() {
		return envId;
	}

	public String getAcsorgId() {
		return acsorgId;
	}

	public void setAcsorgId(String acsorgId) {
		this.acsorgId = acsorgId;
	}

	public String getAcssecretKey() {
		return acssecretKey;
	}

	public void setAcssecretKey(String acssecretKey) {
		this.acssecretKey = acssecretKey;
	}

	public void setEnvId(String envId) {
		this.envId = envId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	
}
