/**
 * 
 */
package com.cycle30.rest.support;

/**
 * @author Venkat Bhat
 *
 */
public interface UniqueRequesIdProvider {
	
	String getUniqueId();

}
