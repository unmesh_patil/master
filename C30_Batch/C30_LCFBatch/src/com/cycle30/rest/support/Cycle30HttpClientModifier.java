/**
 * 
 */
package com.cycle30.rest.support;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * @author Venkat Bhat
 *
 */
public class Cycle30HttpClientModifier {
	
	DefaultHttpClient httpClient;
	private int sslPort;
	
	public void init() throws Exception {
		SSLSocketFactory ssf = getSSLSocketFactory();
		ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		SchemeRegistry sr = httpClient.getConnectionManager().getSchemeRegistry();
		sr.register(new Scheme("https", sslPort, ssf));
		
	}
	
	protected SSLSocketFactory getSSLSocketFactory() throws Exception {
		SSLContext ctx = SSLContext.getInstance("TLS");
		X509TrustManager tm = new X509TrustManager() {

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
				;
			}

			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
				;
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			 
				
			};
		ctx.init(null, new TrustManager[]{tm}, null);
		return new SSLSocketFactory(ctx);
	}

	public DefaultHttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(DefaultHttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public int getSslPort() {
		return sslPort;
	}

	public void setSslPort(int sslPort) {
		this.sslPort = sslPort;
	}
	
	

}
