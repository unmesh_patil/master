/**
 * 
 */
package com.cycle30.rest.support.spring;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.cycle30.rest.support.HeaderHelperBase;

/**
 * @author Venkat Bhat
 *
 */
public abstract class SpringHeaderHelperBase<T> extends HeaderHelperBase<T> {
	private static Logger logger = Logger.getLogger(SpringHeaderHelperBase.class);
	
	private List<MediaType> acceptableMediaTypes;
	
	public HttpHeaders getHttpHeaders(T item) {
		// Prepare header
		HttpHeaders headers = new HttpHeaders();
		addMediaTypes(headers);
		addDigest(headers);
		addClientId(headers);
		addEnvId(headers);
		addDateToHeader(headers);
		addTransId(headers,item);
		addCustomHeaders(headers,item);
		return headers;
	}
	
	public HttpHeaders getACSHttpHeaders(T item) {
		// Prepare ACS header
		HttpHeaders headers = new HttpHeaders();
		addMediaTypes(headers);
		addACSDigest(headers);		
		addACSClientId(headers);		
		addEnvId(headers);
		addDateToHeader(headers);
		addTransId(headers,item);
		addCustomHeaders(headers,item);
		return headers;
	}
	
	protected void addDigest(HttpHeaders headers) {
		headers.add(X_DIGEST, getDigest());
	}
	
	protected void addACSDigest(HttpHeaders headers) {
		headers.add(X_DIGEST, getACSDigest());
	}
	
	protected void addCustomHeaders(HttpHeaders headers, T item) {
		
	}
	
	protected void addClientId(HttpHeaders headers) {
		headers.add(X_CLIENT_ID, getOrgId());
	}
	
	protected void addACSClientId(HttpHeaders headers) {
		headers.add(X_CLIENT_ID, getAcsorgId());
	}
	
	protected void addEnvId(HttpHeaders headers) {
		headers.add(X_ENV_ID, getEnvId());
	}
	
	protected void addDateToHeader(HttpHeaders headers) {
		headers.add(DATE, getDateHeader());
	}
	
	abstract protected void addTransId(HttpHeaders headers, T item);	
	
	
	protected void addMediaTypes(HttpHeaders headers) {
		if(acceptableMediaTypes == null || acceptableMediaTypes.isEmpty()) {
			acceptableMediaTypes = new ArrayList<MediaType>();
			acceptableMediaTypes.add(MediaType.APPLICATION_XML);
			logger.info("The acceptable media type is NOT setup. Assuming APPLICATION_XML");
		} 
		headers.setAccept(acceptableMediaTypes);
	}

	public List<MediaType> getAcceptableMediaTypes() {
		return acceptableMediaTypes;
	}

	public void setAcceptableMediaTypes(List<MediaType> acceptableMediaTypes) {
		this.acceptableMediaTypes = acceptableMediaTypes;
	}
	
	

}
