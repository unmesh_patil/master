/**
 * 
 */
package com.cycle30.rest.support.spring;

import java.net.URI;
import java.net.URL;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import com.cycle30.rest.support.GatewayURL;

/**
 * @author Venkat Bhat
 *
 */
public class RestInvokerBase {
	private static Logger logger = Logger.getLogger(RestInvokerBase.class);
	
	//generally injected
	private URL invokeUrl;
	private RestTemplate restTemplate;
	
	//internally maintained
	private URI uri;
	
	protected URI getUri() {
		return uri;
	}

	public URL getInvokeUrl() {
		return invokeUrl;
	}

	public void setInvokeUrl(URL invokeUrl) {
		this.invokeUrl = invokeUrl;
		uri = getUri(invokeUrl);
	}
	
	public URI getUri(Object object) {
		if(object instanceof GatewayURL) return getUri((GatewayURL)object);
		if(object instanceof URL) return getUri((URL)object);
		return null;
	}
	
	public URI getUri(GatewayURL url) {
		Assert.notNull(url, "GatewayURL param is null");
		return url.getURI();
	}
	
	public URI getUri(URL url) {
		Assert.notNull(url, "URL param is null");
		URI temp = null;
		try {
			temp =  url == null ?  null : url.toURI();
		} catch (Exception e) {
			logger.error(e);
		}
		return temp;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

}
