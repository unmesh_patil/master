/**
 * 
 */
package com.cycle30.rest.support.spring;

import java.net.URI;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import com.cycle30.rest.support.HeaderHelperBase;
import com.cycle30.rest.support.RestCallStatus;

/**
 * @author Venkat Bhat
 *
 */
public class GenericRestInvoker extends RestInvokerBase {
	private static Logger logger = Logger.getLogger(GenericRestInvoker.class);
	
	public static final String GET = "GET";
	public static final String POST = "POST";
	
	private GenericHeaderHelper headerHelper;
	
	private Map<String,Object>  invokeUrlMap;
	
	HttpHeaders headers = null;
	
	public GenericRestInvoker() {
		
	}
	
	public RestCallStatus invokeRestCall(String method, Object payload, Class responseType, String tenantId) {
		return invokeRestCall(getUri(), method, payload, responseType, tenantId);
	}
	
	public RestCallStatus invokeRestCall(String invokeUrlKey, String method, Object payload, Class responseType, String tenantId) {
		Assert.notNull(invokeUrlMap, "invokeUrlMap is null");
		return invokeRestCall(getUri(invokeUrlMap.get(invokeUrlKey)), method, payload, responseType, tenantId);
	}
	
	public RestCallStatus invokeRestCall(URI uri, String method, Object payload, Class responseType, String tenantId) {

		if (tenantId.equalsIgnoreCase("GCI")) {
			headers = headerHelper.getHttpHeaders(payload);
		} else if (tenantId.equalsIgnoreCase("ACS")) {
			headers = headerHelper.getACSHttpHeaders(payload);
		}
		
		HttpEntity<Object> entity = new HttpEntity<Object>(payload, headers);
		
		SpringRestCallStatus status = null;
		try {
			if(logger.isDebugEnabled()) logger.debug("Invoking REST at URL: " + uri.toASCIIString());
			ResponseEntity result = 
				getRestTemplate().exchange(uri, getHttpMethod(method), entity, responseType);
			status = new SpringRestCallStatus(result.getBody());
		} catch (RestClientException e) {
			logger.error(e);
			if(e instanceof HttpStatusCodeException) {
				HttpStatusCodeException se = (HttpStatusCodeException)e;
				logger.error("More info:" + se.getResponseBodyAsString());
			} 
			status = new SpringRestCallStatus(e);
		} 
		status.setRestCallReference(headers.getFirst(HeaderHelperBase.X_TRANS_ID));
		if(logger.isDebugEnabled()) logger.info("Transaction ID for setting in restcallreference:->"+headers.getFirst(HeaderHelperBase.X_TRANS_ID));
		if(logger.isDebugEnabled()) logger.debug("Rest call status is: " + status.toString());
		return status;
	}
	
	protected HttpMethod getHttpMethod(String method) {
		Assert.notNull(method, "Method is NULL");
		if(POST.equalsIgnoreCase(method)) return HttpMethod.POST;
		return HttpMethod.GET;
	}

	public Map<String, Object> getInvokeUrlMap() {
		return invokeUrlMap;
	}

	public void setInvokeUrlMap(Map<String, Object> invokeUrlMap) {
		this.invokeUrlMap = invokeUrlMap;
	}

	public GenericHeaderHelper getHeaderHelper() {
		return headerHelper;
	}

	public void setHeaderHelper(GenericHeaderHelper headerHelper) {
		this.headerHelper = headerHelper;
	}
	
	

}
