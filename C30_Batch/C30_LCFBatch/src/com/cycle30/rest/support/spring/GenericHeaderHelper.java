/**
 * 
 */
package com.cycle30.rest.support.spring;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;

import com.cycle30.rest.support.UniqueRequesIdProvider;


/**
 * @author Venkat Bhat
 *
 */
public class GenericHeaderHelper extends SpringHeaderHelperBase<Object> {
	private static Logger logger = Logger.getLogger(GenericHeaderHelper.class);

	/* (non-Javadoc)
	 * @see com.cycle30.batch.lcf.rest.HeaderHelperBase#addTransId(org.springframework.http.HttpHeaders, java.lang.Object)
	 */
	@Override
	protected void addTransId(HttpHeaders headers, Object item) {
		String guid = null;
		if(item instanceof UniqueRequesIdProvider) {
			UniqueRequesIdProvider p = (UniqueRequesIdProvider)item;
			guid = p.getUniqueId();
		} 
		if(guid == null) {
			UUID uuid = UUID.randomUUID();
			guid = uuid.toString();
		}
		headers.add(X_TRANS_ID, guid);
		if(logger.isDebugEnabled()) logger.debug("Adding Trans id: " + guid);
	}

}
