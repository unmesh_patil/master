/**
 * 
 */
package com.cycle30.rest.support.spring;

import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import com.cycle30.rest.support.RestCallStatus;

/**
 * @author Venkat Bhat
 * @param <T>
 * @param <T>
 *
 */
public class SpringRestCallStatus extends RestCallStatus {
	
	public SpringRestCallStatus() {
		;
	}
	
	public SpringRestCallStatus(Object payLoad) {
		super(payLoad);
	}
	
	public SpringRestCallStatus(RestClientException e) {
		callSuccess = false;
		if(e instanceof ResourceAccessException) {
			resourceAccessError = true;
		} else if(e instanceof HttpServerErrorException) {
			serverError = true;
		} else {
			httpClientError = true;
		}
	}

}
