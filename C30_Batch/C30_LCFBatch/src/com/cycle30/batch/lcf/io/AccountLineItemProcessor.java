/**
 * 
 */
package com.cycle30.batch.lcf.io;


import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.Assert;

import com.cycle30.batch.domain.LineItem;
import com.cycle30.batch.lcf.dao.AccountLineItemDao;
import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.batch.lcf.rest.domain.AccountList;
import com.cycle30.batch.lcf.rest.domain.LifeCycleRequest;
import com.cycle30.batch.lcf.rest.domain.OrderRequest;
import com.cycle30.rest.support.RestCallStatus;
import com.cycle30.rest.support.spring.GenericRestInvoker;

/**
 * @author Venkat Bhat
 *
 */
public class AccountLineItemProcessor implements ItemProcessor<LineItem, LineItem> {
	private static Logger logger = Logger.getLogger(AccountLineItemProcessor.class);
	
	//private RestInvoker restInvoker;
	private GenericRestInvoker restInvoker;
	
	private AccountLineItemDao accountLineItemDao;
	
	private boolean createDisconnectOrderInBILForEvent02;
	//remove this TODO
	//static int i =1;
	
	
	@Override
	public LineItem process(LineItem item) throws Exception {
		Assert.notNull(restInvoker, "RestInvoker is NOT setup");
		//return processLineItem(item);
		
		AccountLineItem accountLineItem = getAccountLineItemDao().getAccountLineItem(item.getLineItemId());
		//set Line Item
		accountLineItem.setLineItemAttributes(item);
		if(logger.isDebugEnabled())logger.debug(accountLineItem.toString());
		//get account list payload
		AccountList accounts = new AccountList(accountLineItem);
		LifeCycleRequest lifeCycleRequest = new LifeCycleRequest();
        lifeCycleRequest.setAccountList(accounts);

        String status = LineItem.STATUS_FAIL;
		String tenantId = null;
		try {
			//if( (i++ % 6) == 0 ) throw new IllegalStateException("Failing every sixth Account line item");
			//invoke rest call
			tenantId = accountLineItem.getTenantId();
			if(logger.isDebugEnabled())logger.debug("TenantId = "+tenantId);
			RestCallStatus restCallStatus =
					restInvoker.invokeRestCall(LineItemProcessorHelper.getEventtype(accountLineItem.getEventType()), GenericRestInvoker.POST, 
							lifeCycleRequest, String.class, tenantId);
			if(restCallStatus.isCallSuccess()) {
				status = LineItem.STATUS_COMPLETE;
				if(logger.isDebugEnabled())logger.debug("Account Line Item processed successfuly for Force Call with id: " + item.getLineItemId()+":Eventtype "+accountLineItem.getEventType());
				//Invoke the rest call for disconnect
				if(LineItemProcessorHelper.EVENT_TYPE_DISCONNECT.equals(accountLineItem.getEventType()) && isCreateDisconnectOrderInBILForEvent02()){
					status=invokeCallForDisconnect(accountLineItem);
				}
			} else {
				status = LineItem.STATUS_FAIL;
				logger.error("The line item with id " + item.getLineItemId() + " failed due to: " + restCallStatus.toString() );
			}
			accountLineItem.setForceTransactionId(restCallStatus.getRestCallReference());
			getAccountLineItemDao().updateLineItemTransaction(accountLineItem);
		} catch (Exception e) {
			status = LineItem.STATUS_FAIL;
			logger.error("The line item with id " + item.getLineItemId() + " failed due to:" + e.getMessage());
		}
		item.setCurrentStatus(status);
		return item;
	}
	
	public boolean isCreateDisconnectOrderInBILForEvent02() {
		return createDisconnectOrderInBILForEvent02;
	}

	public void setCreateDisconnectOrderInBILForEvent02(
			boolean createDisconnectOrderInBILForEvent02) {
		this.createDisconnectOrderInBILForEvent02 = createDisconnectOrderInBILForEvent02;
	}

	public AccountLineItemDao getAccountLineItemDao() {
		return accountLineItemDao;
	}

	public void setAccountLineItemDao(AccountLineItemDao accountLineItemDao) {
		this.accountLineItemDao = accountLineItemDao;
	}

	public GenericRestInvoker getRestInvoker() {
		return restInvoker;
	}

	public void setRestInvoker(GenericRestInvoker restInvoker) {
		this.restInvoker = restInvoker;
	}
	
	private String invokeCallForDisconnect(AccountLineItem accountLineItem){
		String clientOrderId="LCFBATCH_"+accountLineItem.getLineItemId();
		String status = LineItem.STATUS_FAIL;
		String tenantId = null;
		if(logger.isDebugEnabled())logger.debug("Invoking the call for disconnect with clientorderid : " + clientOrderId);
		OrderRequest orderRequestPayload=LineItemProcessorHelper.getOrderRequestPayload(accountLineItem,clientOrderId);
			try {
				//if( (i++ % 6) == 0 ) throw new IllegalStateException("Failing every sixth Account line item");
				//invoke rest call
				tenantId = accountLineItem.getTenantId();
				if(logger.isDebugEnabled())logger.debug("TenantId = "+tenantId);				
				RestCallStatus restCallStatus =
						restInvoker.invokeRestCall(accountLineItem.getEventType(), GenericRestInvoker.POST, orderRequestPayload, String.class,tenantId);
				if(restCallStatus.isCallSuccess()) {
					status = LineItem.STATUS_COMPLETE;
					if(logger.isDebugEnabled())logger.debug("Account Line Item processed successfuly for disconnect Call with id: " + accountLineItem.getLineItemId());
					
				} else {
					status = LineItem.STATUS_FAIL;
					logger.error("The line item with id " + accountLineItem.getLineItemId() + " failed due to: " + restCallStatus.toString() );
				}
				accountLineItem.setBilTransactionId(restCallStatus.getRestCallReference());
				accountLineItem.setClientOrderId(clientOrderId);
			} catch (Exception e) {
				status = LineItem.STATUS_FAIL;
				logger.error("The line item with id " + accountLineItem.getLineItemId() + " failed due to:" + e.getMessage());
			}
		return status;
	}
}
