/**
 * 
 */
package com.cycle30.batch.lcf.io;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import com.cycle30.batch.lcf.dao.AccountLineItemDao;
import com.cycle30.batch.lcf.domain.AccountLineItem;

/**
 * @author venbhat
 *
 */
public class AccountLineItemWriter implements ItemWriter<AccountLineItem> {
	private static Logger logger = Logger.getLogger(AccountLineItemWriter.class);
	
	private AccountLineItemDao accountLineItemDao;



	public AccountLineItemDao getAccountLineItemDao() {
		return accountLineItemDao;
	}

	public void setAccountLineItemDao(AccountLineItemDao accountLineItemDao) {
		this.accountLineItemDao = accountLineItemDao;
	}

	@Override
	public void write(List<? extends AccountLineItem> account) throws Exception {
		if(logger.isDebugEnabled()) logger.debug("Writing AccountLineItem list");
		for (AccountLineItem accountLineItem : account) {
			getAccountLineItemDao().insertAccountLineItem(accountLineItem);
		}
	}
	
	

}
