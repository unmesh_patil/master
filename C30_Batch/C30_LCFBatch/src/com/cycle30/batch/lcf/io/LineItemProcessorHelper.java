/**
 * 
 */
package com.cycle30.batch.lcf.io;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.batch.lcf.rest.domain.OrderRequest;
import com.cycle30.batch.lcf.rest.domain.ServiceObject;
import com.cycle30.batch.lcf.rest.domain.ServiceObjectList;

/**
 * @author Venkat Bhat
 *
 */
public class LineItemProcessorHelper  {
	private static Logger logger = Logger.getLogger(LineItemProcessorHelper.class);
	
	public static String EVENT_TYPE_AUTORECHARGE="01";
	
	public static String EVENT_TYPE_DISCONNECT="02";
	
	public static OrderRequest getOrderRequestPayload(AccountLineItem accountLineItem, String clientOrderId){
		OrderRequest orderRequest= new OrderRequest();
		ServiceObjectList serviceObjectList = new ServiceObjectList();
		orderRequest.setServiceObjectList(serviceObjectList);
		orderRequest.setActionWho("LCF Batch");
		orderRequest.setClientOrderId(clientOrderId);//Clientorderid
		orderRequest.setClientOrderName(clientOrderId);//Clientorderid
		orderRequest.setOrderDesiredDate(new Date());
		orderRequest.setRequestType("Disconnect Service order");
		
		ServiceObject serviceObject = new ServiceObject();
		serviceObject.setClientAccountId(accountLineItem.getSubscribeId());//OCS SubscriberId D6
		serviceObject.setClientActionType("DISCONNECT");
		serviceObject.setClientActionTypeReason("Disconnect to Aging");//As per ranjith mail Changed from OCS Initiated to 
		serviceObject.setClientId(accountLineItem.getLineId());//SubscriberLineId D8
		if(accountLineItem.getPhoneNumber()!=null && accountLineItem.getPhoneNumber().length() == 11){
			serviceObject.setClientServicePrimaryIdentifier(accountLineItem.getPhoneNumber().substring(1));
		}else{
			serviceObject.setClientServicePrimaryIdentifier(accountLineItem.getPhoneNumber());// TN Number D6
		}
		List<ServiceObject> objectList = new ArrayList<ServiceObject>();
		objectList.add(serviceObject);
		serviceObjectList.setServiceObject(objectList);
		
		return orderRequest;
	}
	
	public static String getEventtype(String eventType){
		if(LineItemProcessorHelper.EVENT_TYPE_DISCONNECT.equals(eventType)){
        	eventType=LineItemProcessorHelper.EVENT_TYPE_AUTORECHARGE;
        }
		return eventType;
	}
	/*private  static OrderRequest getOrderRequestPayloadTest(){
		OrderRequest orderRequest= new OrderRequest();
		ServiceObjectList serviceObjectList = new ServiceObjectList();
		orderRequest.setServiceObjectList(serviceObjectList);
		orderRequest.setActionWho("LCF Batch");
		orderRequest.setClientOrderId("VVV");//Clientorderid
		orderRequest.setClientOrderName("VVVV");//Clientorderid
		orderRequest.setOrderDesiredDate(new Date());
		orderRequest.setRequestType("Disconnect Service order");
		
		ServiceObject serviceObject = new ServiceObject();
		serviceObject.setClientAccountId("hghjghj");//OCS SubscriberId D6
		serviceObject.setClientActionType("DISCONNECT");
		serviceObject.setClientActionTypeReason("OCS Initiated");
		serviceObject.setClientId("VVVvvv");//SubscriberLineId D8
		
			serviceObject.setClientServicePrimaryIdentifier("FDFGDFg");// TN Number D6
		
		List<ServiceObject> objectList = new ArrayList<ServiceObject>();
		objectList.add(serviceObject);
		serviceObjectList.setServiceObject(objectList);
		
		return orderRequest;
	}
	
	public static void main(String args[]){
		getOrderRequestPayloadTest().toXml();
	}*/
}
