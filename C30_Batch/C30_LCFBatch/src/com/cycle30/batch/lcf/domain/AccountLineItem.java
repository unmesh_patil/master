/**
 * 
 */
package com.cycle30.batch.lcf.domain;

import java.io.Serializable;

import com.cycle30.batch.domain.LineItem;
import com.cycle30.rest.support.UniqueRequesIdProvider;

/**
 * @author Venkat Bhat
 *
 */
public class AccountLineItem extends LineItem implements UniqueRequesIdProvider, Serializable {

	private static final long serialVersionUID = -8801557222002805161L;
	
	private Long id;
	
	private String recordType;
	private String eventType;
	private String tenantId;
	private String phoneNumber;
	private String forceAccountNumber;
	private String subscribeId;
	private String pricePlan;
	private String lineId;
	private String imsi;
	private String future1;
	private String future2;
	private String forceTransactionId;
	private String bilTransactionId;
	private String clientOrderId;
		
	public String getUniqueId() {
		StringBuilder sbuf = new StringBuilder(128);
		sbuf.append("SDC_ID_")
		.append(getSrcDataContainerId())
		.append("_ITEM_ID_")
		.append(getLineItemId());
		return sbuf.toString();
	}
	
	public String toString() {
		StringBuilder sbuf = new StringBuilder(128);
		sbuf.append("Id: ")
		.append(getId())
		.append("; Phone Number: ")
		.append(getPhoneNumber())
		.append("; Event Type :")
		.append(getEventType());
		
		return sbuf.toString();
	}
	
	
	public Long getId() {
		return id == null?  getLineItemId() : id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getForceAccountNumber() {
		return forceAccountNumber;
	}
	public void setForceAccountNumber(String forceAccountNumber) {
		this.forceAccountNumber = forceAccountNumber;
	}
	public String getSubscribeId() {
		return subscribeId;
	}
	public void setSubscribeId(String subscribeId) {
		this.subscribeId = subscribeId;
	}
	public String getPricePlan() {
		return pricePlan;
	}
	public void setPricePlan(String pricePlan) {
		this.pricePlan = pricePlan;
	}
	public String getLineId() {
		return lineId;
	}
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getFuture1() {
		return future1;
	}

	public void setFuture1(String future1) {
		this.future1 = future1;
	}

	public String getFuture2() {
		return future2;
	}

	public void setFuture2(String future2) {
		this.future2 = future2;
	}
	
	public String getForceTransactionId() {
		return forceTransactionId;
	}

	public void setForceTransactionId(String forceTransactionId) {
		this.forceTransactionId = forceTransactionId;
	}

	public String getBilTransactionId() {
		return bilTransactionId;
	}

	public void setBilTransactionId(String bilTransactionId) {
		this.bilTransactionId = bilTransactionId;
	}

	public void setLineItemAttributes(LineItem item) {
		super.copyLineItemValues(item);
	}
	
	public String getClientOrderId() {
		return clientOrderId;
	}

	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}
}
