/**
 * 
 */
package com.cycle30.batch.lcf.rest.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.rest.support.UniqueRequesIdProvider;


/**
 * @author Venkat Bhat
 *
 */
@XmlRootElement(name="AccountList")
public class AccountList implements UniqueRequesIdProvider, Serializable {
	private static final long serialVersionUID = -1407039133378645140L;

	private int size;
	
	private List<Account> data;
	
	private transient String guid;
	
	public AccountList() {
		
	}
	
	public AccountList(AccountLineItem lineItem) {
		Account account = new Account(lineItem);
		List<Account> accounts = new ArrayList<Account>(4);
		accounts.add(account);
		setData(accounts);
		guid = lineItem.getUniqueId();
	}
	
	public String getUniqueId() {
		return guid;
	}
	
	

	@XmlAttribute(name="size")
	public int getSize() {
		size = data == null ? 0 : data.size();
		return size;
	}
	
	@XmlElement(name="Account")
	public List<Account> getData() {
		return data;
	}

	public void setData(List<Account> data) {
		this.data = data;
		size = data == null ? 0 : data.size();
	}

	public String toString() {
		StringBuilder sbuf = new StringBuilder(512);
		sbuf.append("The list sise is: ")
		.append(getSize())
		.append(";  Accounts:{");
		
		for(Account account: data) {
			sbuf.append("Account[")
			.append(account.toString())
			.append("];");
		}
		sbuf.append("}");
		return sbuf.toString();
	}
	

}
