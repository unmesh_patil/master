package com.cycle30.batch.lcf.rest.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name="LifeCycleRequest")
public class LifeCycleRequest implements Serializable {

	
	private AccountList accountList;
	
	@XmlElement(name="AccountList")
	public AccountList getAccountList() {
		return accountList;
	}

	public void setAccountList(AccountList accountList) {
		this.accountList = accountList;
	}
	public LifeCycleRequest(){
		
	}
}
