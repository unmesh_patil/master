package com.cycle30.batch.lcf.rest.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder ={"clientId","clientAccountId","clientActionType","clientServicePrimaryIdentifier","clientActionTypeReason"})
@XmlRootElement(name="ServiceObject")
public class ServiceObject {
	
	private String clientId;
	private String clientAccountId;
	private String clientActionType;
	private String clientServicePrimaryIdentifier;
	private String clientActionTypeReason;
	
	@XmlAttribute(name="ClientId")
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	@XmlAttribute(name="ClientAccountId")
	public String getClientAccountId() {
		return clientAccountId;
	}
	public void setClientAccountId(String clientAccountId) {
		this.clientAccountId = clientAccountId;
	}
	
	@XmlAttribute(name="ClientActionType")
	public String getClientActionType() {
		return clientActionType;
	}
	public void setClientActionType(String clientActionType) {
		this.clientActionType = clientActionType;
	}
	@XmlAttribute(name="ClientServicePrimaryIdentifier")
	public String getClientServicePrimaryIdentifier() {
		return clientServicePrimaryIdentifier;
	}
	public void setClientServicePrimaryIdentifier(
			String clientServicePrimaryIdentifier) {
		this.clientServicePrimaryIdentifier = clientServicePrimaryIdentifier;
	}
	@XmlAttribute(name="ClientActionTypeReason")
	public String getClientActionTypeReason() {
		return clientActionTypeReason;
	}
	public void setClientActionTypeReason(String clientActionTypeReason) {
		this.clientActionTypeReason = clientActionTypeReason;
	}
	
	
}



