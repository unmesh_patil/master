package com.cycle30.batch.lcf.rest.domain;

import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"requestType", "orderDesiredDate", "actionWho", "clientOrderId", "clientOrderName","serviceObjectList"  } )
@XmlRootElement(name="OrderRequest")
public class OrderRequest {
	private String requestType;
	private Date orderDesiredDate;
	private String actionWho;
	private String clientOrderId;
	private String clientOrderName;
	private ServiceObjectList serviceObjectList;
	
	@XmlElement(name="ServiceObjectList")
	public ServiceObjectList getServiceObjectList() {
		return serviceObjectList;
	}

	public void setServiceObjectList(ServiceObjectList serviceObjectList) {
		this.serviceObjectList = serviceObjectList;
	}

	@XmlElement(name="RequestType")
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	@XmlElement(name="OrderDesiredDate")
	public Date getOrderDesiredDate() {
		return orderDesiredDate;
	}

	public void setOrderDesiredDate(Date orderDesiredDate) {
		this.orderDesiredDate = orderDesiredDate;
	}
	
	@XmlElement(name="ActionWho")
	public String getActionWho() {
		return actionWho;
	}

	public void setActionWho(String actionWho) {
		this.actionWho = actionWho;
	}
	
	@XmlElement(name="ClientOrderId")
	public String getClientOrderId() {
		return clientOrderId;
	}

	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}
	@XmlElement(name="ClientOrderName")
	public String getClientOrderName() {
		return clientOrderName;
	}

	public void setClientOrderName(String clientOrderName) {
		this.clientOrderName = clientOrderName;
	}
	
	/*public void toXml() {
	    try {
	        JAXBContext ctx = JAXBContext.newInstance(OrderRequest.class);
	        Marshaller marshaller = ctx.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	        marshaller.marshal(this, System.out);
	    }
	    catch (Exception
	            e) 
	    {
	    	e.printStackTrace();
	    		//catch exception 
	    }
	}*/
}

