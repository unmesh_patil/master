/**
 * 
 */
package com.cycle30.batch.lcf.rest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;

import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.batch.lcf.rest.domain.AccountList;
import com.cycle30.rest.support.HeaderHelperBase;
import com.cycle30.rest.support.RestCallStatus;
import com.cycle30.rest.support.spring.RestInvokerBase;
import com.cycle30.rest.support.spring.SpringRestCallStatus;

/**
 * @author Venkat Bhat
 *
 */
public class RestInvoker extends RestInvokerBase {
	private static Logger logger = Logger.getLogger(RestInvoker.class);

	private AccountLineItemHeaderHelper headerHelper;
	
	
	public RestCallStatus doPost(AccountLineItem lineItem) {
		if(logger.isDebugEnabled()) logger.debug("Entering method doPost ...");

		Assert.notNull(getRestTemplate(), "RestTemplate is NOT setup");
		Assert.notNull(getInvokeUrl(), "URL is NOT setup");
		Assert.notNull(lineItem, "Account Line Item is NULL");
				
		AccountList accounts = new AccountList(lineItem);
		String tenantId = lineItem.getTenantId();
		HttpHeaders headers = null;
		if (tenantId.equalsIgnoreCase("GCI")) {
			headers = headerHelper.getHttpHeaders(lineItem);
		} else if (tenantId.equalsIgnoreCase("ACS")) {
			headers = headerHelper.getACSHttpHeaders(lineItem);
		}
		
		// create HttpEntity
		HttpEntity<AccountList> entity = new HttpEntity<AccountList>(accounts, headers);
		
		SpringRestCallStatus status;
		try {
			if(logger.isDebugEnabled()) logger.debug("about to invoke REST call...");
			ResponseEntity<String> result = getRestTemplate().exchange(getUri(), HttpMethod.POST, entity, String.class);
			status = new SpringRestCallStatus(result.getBody());
			
		} catch (RestClientException e) {
			status = new SpringRestCallStatus(e);
		} 
		if(logger.isDebugEnabled()){logger.info("Transaction ID in RestInvoker:->"+headers.getFirst(HeaderHelperBase.X_TRANS_ID));}
		if(logger.isDebugEnabled()) logger.debug("Rest call status is: " + status.toString());
		return status;
	}
	

	public AccountLineItemHeaderHelper getHeaderHelper() {
		return headerHelper;
	}

	public void setHeaderHelper(AccountLineItemHeaderHelper headerHelper) {
		this.headerHelper = headerHelper;
	}
	
	
	
	
	

}
