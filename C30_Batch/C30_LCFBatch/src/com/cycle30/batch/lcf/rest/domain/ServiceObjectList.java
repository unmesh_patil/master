/**
 * 
 */
package com.cycle30.batch.lcf.rest.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.rest.support.UniqueRequesIdProvider;


/**
 * @author Venkat Bhat
 *
 */
@XmlRootElement(name="ServiceObjectList")
public class ServiceObjectList implements  Serializable {
	private static final long serialVersionUID = -1407039133378645140L;

	private int size;
	
	private List<ServiceObject> serviceObject;
	
	@XmlAttribute(name="Size")
	public int getSize() {
		size = serviceObject == null ? 0 : serviceObject.size();
		return size;
	}
	
	@XmlElement(name="ServiceObject")
	public List<ServiceObject> getServiceObject() {
		return serviceObject;
	}

	public void setServiceObject(List<ServiceObject> serviceObject) {
		this.serviceObject = serviceObject;
		size = serviceObject == null ? 0 : serviceObject.size();
	}

	public ServiceObjectList() {
		
	}
}
