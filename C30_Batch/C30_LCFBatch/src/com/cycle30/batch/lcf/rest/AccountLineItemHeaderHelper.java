/**
 * 
 */
package com.cycle30.batch.lcf.rest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;

import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.rest.support.spring.SpringHeaderHelperBase;

/**
 * @author Venkat Bhat
 *
 */
public class AccountLineItemHeaderHelper extends SpringHeaderHelperBase<AccountLineItem> {
	private static Logger logger = Logger.getLogger(AccountLineItemHeaderHelper.class);

	@Override
	protected void addTransId(HttpHeaders headers, AccountLineItem item) {
		StringBuilder sbuf = new StringBuilder(128);
		sbuf.append("SDC_ID_")
		.append(item.getSrcDataContainerId())
		.append("_ITEM_ID_")
		.append(item.getLineItemId());
		headers.add(X_TRANS_ID, sbuf.toString());
		if(logger.isDebugEnabled()) logger.debug("Adding Trans id: " + sbuf.toString());
	}

	@Override
	protected void addCustomHeaders(HttpHeaders headers, AccountLineItem item) {
		//do nothing just an example
	}
	
	
	

	
	
	
	
	

}
