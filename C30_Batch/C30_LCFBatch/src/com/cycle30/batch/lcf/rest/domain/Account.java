/**
 * 
 */
package com.cycle30.batch.lcf.rest.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.cycle30.batch.lcf.domain.AccountLineItem;


/**
 * @author Venkat Bhat
 *
 */
@XmlType(propOrder = {"eventType", "telNumber", "accountId", "subscriberId", "subscriberProfile", "imsi"  } )
@XmlRootElement(name="Account")
public class Account {
	
	private String eventType;
	private String telNumber;
	private String accountId;
	private String subscriberId;
	private String subscriberProfile;
	private String imsi;
	
	public Account() {
		
	}
	
	public Account(AccountLineItem lineItem) {
		this.eventType = lineItem.getEventType();
		if (lineItem.getPhoneNumber().length() == 11) {
			this.telNumber = lineItem.getPhoneNumber().substring(1);
		} else {
			this.telNumber = lineItem.getPhoneNumber();
		}
		this.accountId = lineItem.getForceAccountNumber();
		this.subscriberId = lineItem.getSubscribeId();
		this.subscriberProfile = lineItem.getPricePlan();
		this.imsi = lineItem.getImsi();
	}
	
	public String toString() {
		StringBuilder sbuf = new StringBuilder(512);
		sbuf.append("The event type is: ")
		.append(eventType)
		.append("; the Account id is: ")
		.append(accountId);		
		return sbuf.toString();
	}
	
	
	@XmlElement(name="EventType")
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	@XmlElement(name="TN")
	public String getTelNumber() {
		return telNumber;
	}
	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}
	@XmlElement(name="AccountId")
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	@XmlElement(name="SubscriberId")
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	@XmlElement(name="SubscriberProfile")
	public String getSubscriberProfile() {
		return subscriberProfile;
	}
	public void setSubscriberProfile(String subscriberProfile) {
		this.subscriberProfile = subscriberProfile;
	}
	@XmlElement(name="IMSI")
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	
	

}
