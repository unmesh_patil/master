/**
 * 
 */
package com.cycle30.batch.lcf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cycle30.batch.lcf.domain.AccountLineItem;

/**
 * @author Venkat Bhat
 *
 */
public class AccountLineItemRowMapper implements RowMapper<AccountLineItem> {

	@Override
	public AccountLineItem mapRow(ResultSet rs, int arg1) throws SQLException {
		AccountLineItem item = new AccountLineItem();
		item.setId(rs.getLong("LINE_ITEM_ID"));
		item.setEventType(rs.getString("EVENT_TYPE"));
		item.setTenantId(rs.getString("TENANT_ID"));
		item.setPhoneNumber(rs.getString("PHONE_NUMBER"));
		item.setForceAccountNumber(rs.getString("FORCE_ACCOUNT_NUMBER"));
		item.setSubscribeId(rs.getString("SUBSCRIBER_ID"));
		item.setPricePlan(rs.getString("PRICE_PLAN"));
		item.setLineId(rs.getString("OCS_LINE_ID"));
		item.setImsi(rs.getString("SIM_IMSI"));
		return item;
	}

}
