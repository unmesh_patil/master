/**
 * 
 */
package com.cycle30.batch.lcf.dao;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.cycle30.batch.lcf.domain.AccountLineItem;

/**
 * @author Venkat Bhat
 *
 */
public class AccountLineItemDao {
	private static Logger logger = Logger.getLogger(AccountLineItemDao.class);
	
	private static final String INSERT_STMT = 
			"INSERT INTO C30_SDK_LCF_ACCOUNT_LINE_ITEM (LINE_ITEM_ID, EVENT_TYPE, TENANT_ID, PHONE_NUMBER, FORCE_ACCOUNT_NUMBER, SUBSCRIBER_ID, PRICE_PLAN, OCS_LINE_ID, SIM_IMSI, CREATED) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SELECT_STMT =
			"select * from C30_SDK_LCF_ACCOUNT_LINE_ITEM where LINE_ITEM_ID = ?";
	
	private static final String UPDATE_TRANS = 
		"UPDATE C30_SDK_LCF_ACCOUNT_LINE_ITEM SET TRANSACTION_ID_FORCE=?,TRANSACTION_ID_BIL=?,CLIENT_ORDER_ID=? WHERE LINE_ITEM_ID = ?";
	
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public AccountLineItem getAccountLineItem(Long id) {
		return jdbcTemplate.queryForObject(SELECT_STMT, new AccountLineItemRowMapper(), id);
	}
	
	public void insertAccountLineItem(AccountLineItem account) {
		if(logger.isDebugEnabled()) logger.debug("inserting account: " +  account.toString());
		getJdbcTemplate().update(INSERT_STMT, 
				account.getId(),
				account.getEventType(),
				account.getTenantId(),
				account.getPhoneNumber(),
				account.getForceAccountNumber(),
				account.getSubscribeId(),
				account.getPricePlan(),
				account.getLineId(),
				account.getImsi(),
				new Date());
	}
	
	public void updateLineItemTransaction(AccountLineItem accountLineItem){
		if(logger.isDebugEnabled()) logger.debug("Updating the transaction id for force " +  accountLineItem.getForceTransactionId());
		if(logger.isDebugEnabled()) logger.debug("Updating the transaction id for bil " +  accountLineItem.getBilTransactionId()+" : clientorderId "+accountLineItem.getClientOrderId());
		getJdbcTemplate().update(UPDATE_TRANS,accountLineItem.getForceTransactionId(),accountLineItem.getBilTransactionId(),accountLineItem.getClientOrderId(),accountLineItem.getLineItemId());
	}

}
