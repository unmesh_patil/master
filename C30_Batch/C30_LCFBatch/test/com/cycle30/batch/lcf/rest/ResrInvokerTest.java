/**
 * 
 */
package com.cycle30.batch.lcf.rest;

import java.net.URL;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.rest.support.RestCallStatus;

/**
 * @author Venkat Bhat
 *
 */
public class ResrInvokerTest {
	private static Logger logger = Logger.getLogger(ResrInvokerTest.class);
	
	private static URL INVOKE_URL_SERVER_UNREACHABLE;
	private static URL INVOKE_URL;
	private static URL INVOKE_URL_4XX;
	private static RestInvoker restInvoker;
	private static AccountLineItemHeaderHelper headerHelper;
	private static RestTemplate restTemplate;
	
	@org.junit.BeforeClass
	public static void init() {
		logger.info("Entering method init ...");
		try {
			INVOKE_URL_SERVER_UNREACHABLE = new URL("http://192.168.1.69:8088//account-rest-provider/sample/accountList/");
			INVOKE_URL = new URL("http://192.168.1.69:8080//account-rest-provider/sample/accountList/");
			INVOKE_URL_4XX = new URL("http://192.168.1.69:8080//account-rest-provider/sample/accountList404/");
			
			headerHelper = new AccountLineItemHeaderHelper();
			restTemplate = new RestTemplate();
			
			restInvoker = new RestInvoker();
			restInvoker.setHeaderHelper(headerHelper);
			restInvoker.setRestTemplate(restTemplate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Exiting method init ...");
	}
	
	@Test
	public void testRegularPost() {
		logger.info("Entering method testRegularPost ...");
		restInvoker.setInvokeUrl(INVOKE_URL);
		try {
			AccountLineItem item =  getAccountLineItem();
			RestCallStatus restCallStatus = restInvoker.doPost(item);
			logger.info("Status is: " + restCallStatus);
			Assert.assertEquals(true, restCallStatus.isCallSuccess());
		} catch (Exception e) {
			logger.error("Error occured!");
			logger.error(e);
		}
		logger.info("Exiting method testRegularPost ...");
	}
	
	@Test
	public void testServerNotAvailable() {
		logger.info("Entering method testServerNotAvailable ...");
		restInvoker.setInvokeUrl(INVOKE_URL_SERVER_UNREACHABLE);
		try {
			AccountLineItem item =  getAccountLineItem();
			RestCallStatus restCallStatus = restInvoker.doPost(item);
			logger.info("Status is: " + restCallStatus);
			Assert.assertEquals(true, restCallStatus.isResourceAccessError());
		} catch (Exception e) {
			logger.error("Error occured!");
			logger.error(e);
		}
		logger.info("Exiting method testServerNotAvailable ...");
	}
	
	@Test
	public void testServerError() {
		logger.info("Entering method testServerError ...");
		restInvoker.setInvokeUrl(INVOKE_URL);
		try {
			AccountLineItem item =  getAccountLineItem();
			item.setImsi("throw 500");
			RestCallStatus restCallStatus = restInvoker.doPost(item);
			logger.info("Status is: " + restCallStatus);
			Assert.assertEquals(true, restCallStatus.isServerError());
		} catch (Exception e) {
			logger.error("Error occured!");
			logger.error(e);
		}
		logger.info("Exiting method testServerError ...");
	}
	
	@Test
	public void test404Error() {
		logger.info("Entering method test404Error ...");
		restInvoker.setInvokeUrl(INVOKE_URL_4XX);
		try {
			AccountLineItem item =  getAccountLineItem();
			RestCallStatus restCallStatus = restInvoker.doPost(item);
			logger.info("Status is: " + restCallStatus);
			Assert.assertEquals(true, restCallStatus.isHttpClientError());
		} catch (Exception e) {
			logger.error("Error occured!");
			logger.error(e);
		}
		logger.info("Exiting method test404Error ...");
	}
	
	
	
	private static AccountLineItem getAccountLineItem() {
		AccountLineItem item = new AccountLineItem();
		//line item stuff
		item.setLineItemId(10L);
		item.setSrcDataContainerId(4L);
		item.setLineNumber(6L);
		item.setCurrentStatus(AccountLineItem.STATUS_SUCCESS);
		item.setVersion(2L);
		//other stuff
		item.setRecordType("DR");
		item.setEventType("01");
		item.setTenantId("GCI");
		item.setPhoneNumber("9075550008");
		item.setForceAccountNumber("ADGK10008");
		item.setSubscribeId("120008");
		item.setPricePlan("GCI_LOCAL_UNLIMITED");
		item.setImsi("31137000243");
		return item;
	}

}
