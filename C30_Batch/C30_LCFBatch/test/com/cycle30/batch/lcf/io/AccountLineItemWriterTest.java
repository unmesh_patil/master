package com.cycle30.batch.lcf.io;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Test;

import com.cycle30.batch.dao.BatchAgentContextDAO;
import com.cycle30.batch.domain.BatchAgentContext;
import com.cycle30.batch.domain.JobParam;
import com.cycle30.batch.lcf.dao.AccountLineItemDao;
import com.cycle30.batch.lcf.domain.AccountLineItem;

public class AccountLineItemWriterTest  {
	@Test
 	public void testRun() throws Exception{
		AccountLineItemDao accountLineItemDaoMock=EasyMock.createMock(AccountLineItemDao.class);
		List<AccountLineItem> listAccount=new ArrayList<AccountLineItem>();
		listAccount.add(new AccountLineItem());
		AccountLineItemWriter accountLineItemWriter=new AccountLineItemWriter();
		accountLineItemWriter.setAccountLineItemDao(accountLineItemDaoMock);
		accountLineItemWriter.write(listAccount);
		accountLineItemDaoMock.insertAccountLineItem(EasyMock.anyObject(AccountLineItem.class));
		EasyMock.expectLastCall().anyTimes();
		accountLineItemWriter.write(listAccount);
		EasyMock.replay(accountLineItemDaoMock);
	}
}
