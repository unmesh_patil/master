package com.cycle30.batch.lcf.io;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Test;

import com.cycle30.batch.dao.BatchAgentContextDAO;
import com.cycle30.batch.domain.BatchAgentContext;
import com.cycle30.batch.domain.JobParam;
import com.cycle30.batch.domain.LineItem;
import com.cycle30.batch.lcf.dao.AccountLineItemDao;
import com.cycle30.batch.lcf.domain.AccountLineItem;
import com.cycle30.rest.support.RestCallStatus;
import com.cycle30.rest.support.spring.GenericRestInvoker;

public class AccountLineItemProcessorTest  {
	@Test
 	public void testRun() throws Exception{
		AccountLineItemDao accountLineItemDaoMock=EasyMock.createMock(AccountLineItemDao.class);
		RestCallStatus restCallStatusMock=EasyMock.createMock(RestCallStatus.class);
		LineItem item=new LineItem();
		item.setLineItemId(new Long(20));
		GenericRestInvoker restInvokerMock=EasyMock.createMock(GenericRestInvoker.class);
		List<AccountLineItem> listAccount=new ArrayList<AccountLineItem>();
		listAccount.add(new AccountLineItem());
		AccountLineItemProcessor accountLineItemWriter=new AccountLineItemProcessor();
		accountLineItemWriter.setAccountLineItemDao(accountLineItemDaoMock);
		accountLineItemWriter.setRestInvoker(restInvokerMock);
		EasyMock.expect(accountLineItemDaoMock.getAccountLineItem(EasyMock.anyObject(Long.class))).andReturn(new AccountLineItem());
		EasyMock.expect(restInvokerMock.invokeRestCall(EasyMock.anyObject(String.class),EasyMock.anyObject(Object.class), EasyMock.anyObject(Class.class),EasyMock.anyObject(String.class))).andReturn(restCallStatusMock);
		EasyMock.expect(restCallStatusMock.isCallSuccess()).andReturn(true);
		EasyMock.replay(accountLineItemDaoMock,restInvokerMock,restCallStatusMock);
		
		LineItem lineItem=accountLineItemWriter.process(item);
		Assert.assertNotNull(lineItem);
		
		
	}
}
