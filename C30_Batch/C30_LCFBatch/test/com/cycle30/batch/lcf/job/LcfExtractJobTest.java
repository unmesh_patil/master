/**
 * 
 */
package com.cycle30.batch.lcf.job;

import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.core.JobLaunchBaseHelper;

/**
 * @author Venkat Bhat
 *
 */
public class LcfExtractJobTest extends JobLaunchBaseHelper {
	
	private static Logger logger = Logger.getLogger(LcfExtractJobTest.class);
	
	
	public LcfExtractJobTest(String contextName, String jobName) {
		super(contextName, jobName);
	}
	
	public static void main(String args[]) throws Exception {
		logger.info("Starting test for Extract Step");
		
		LcfExtractJobTest myTest = new LcfExtractJobTest(
				"LcfExtractJob.xml", 
				"LcfBatchFlow_ExtractJob");
		
		myTest.getJobParametersBuilder()
			.addString(CoreBatchConstant.BATCH_FLOW_PARAM_NAME, "LCF_BATCH_FLOW")
			.addString(CoreBatchConstant.FILE_PARAM_NAME, "C:/work/Cycle30/samplecode/lcfFileSample_regular_01.csv")
			.addString(CoreBatchConstant.FILE_PARAM_NAME, args[0])
			.addDate(CoreBatchConstant.TIMESTAMP, new Date());
		
		myTest.run();
		
		
	}

}
