/**
 * 
 */
package com.cycle30.batch.agent;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Venkat Bhat
 *
 */
public class BatchFlowAgent {
	private static Logger logger = Logger.getLogger(BatchFlowAgent.class);
	
	private static final String AGENT_HELPER_BEAN_NAME = "agentHelper";
	
	String contextName;
	ApplicationContext context;
	AgentHelper agentHelper;
	Long sleepingTimeInMilliSeconds = 60000L;
	int numberOfTimesToRun = -1;
	int currentCount = 0;
	
	public BatchFlowAgent(String contextName) {
		this.contextName = contextName;
		context = new ClassPathXmlApplicationContext(contextName);
		agentHelper = context.getBean(AGENT_HELPER_BEAN_NAME, AgentHelper.class);
		logger.info("Context Initialization complete...");
	}
	
	public void run() throws Exception {
		logger.info("Starting run()...");
		while(numberOfTimesToRun == -1 || currentCount < numberOfTimesToRun) {
			//invoke
			agentHelper.runPendingJob();
			
			//now update BATCH_FLOW_CONFIG where ID = 2,  increment some count, update timestamp
			//TODO
			// ......
			
			//now sleep
			Thread.sleep(sleepingTimeInMilliSeconds);
			currentCount++;
		}
	}
	
	
	public static void main(String args[]) throws Exception {
		BatchFlowAgent agent = new BatchFlowAgent("BatchFlowAgentContext.xml");
		agent.run();
	}

}
