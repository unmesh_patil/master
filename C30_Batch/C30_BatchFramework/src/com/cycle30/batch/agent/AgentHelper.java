/**
 * 
 */
package com.cycle30.batch.agent;

import java.util.List;

import org.apache.log4j.Logger;

import com.cycle30.batch.dao.BatchAgentContextDAO;
import com.cycle30.batch.domain.BatchAgentContext;
import com.cycle30.batch.domain.JobParam;

/**
 * @author Venkat Bhat
 *
 */
public class AgentHelper {
	private static Logger logger = Logger.getLogger(AgentHelper.class);
	
	private BatchAgentContextDAO batchAgentContextDAO;
	private AgentJavaProcessHelper agentJavaProcessHelper;
	
	public void runPendingJob() {
		//check for status match
		logger.info("Entering runPendingJob() ...");
		List<BatchAgentContext> batchAgentContexts = getBatchAgentContextDAO().getPendingJobDetails();
		if(batchAgentContexts != null && !batchAgentContexts.isEmpty()) {
			BatchAgentContext batchAgentContext = batchAgentContexts.get(0);
			//Setting the list of job params
			List<JobParam> jobParams = getBatchAgentContextDAO().getJobParam(batchAgentContext.getJobConfigId());
			batchAgentContext.setJobParams(jobParams);
			
			getAgentJavaProcessHelper().invokeBatchJob(batchAgentContext);
		}
		
		logger.info("Exiting runPendingJob() ...");
	}

	public BatchAgentContextDAO getBatchAgentContextDAO() {
		return batchAgentContextDAO;
	}

	public void setBatchAgentContextDAO(BatchAgentContextDAO batchAgentContextDAO) {
		this.batchAgentContextDAO = batchAgentContextDAO;
	}

	public AgentJavaProcessHelper getAgentJavaProcessHelper() {
		return agentJavaProcessHelper;
	}

	public void setAgentJavaProcessHelper(
			AgentJavaProcessHelper agentJavaProcessHelper) {
		this.agentJavaProcessHelper = agentJavaProcessHelper;
	}
	
	

}
