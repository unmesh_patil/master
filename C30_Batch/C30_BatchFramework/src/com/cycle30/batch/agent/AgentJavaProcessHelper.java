/**
 * 
 */
package com.cycle30.batch.agent;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.domain.BatchAgentContext;
import com.cycle30.batch.domain.JobParam;

/**
 * @author Venkat Bhat
 *
 */
public class AgentJavaProcessHelper {
	
	private static Logger logger = Logger.getLogger(AgentJavaProcessHelper.class);
	
	public void invokeBatchJob(BatchAgentContext batchAgentContext) {
		logger.info("Entering invokeBatchJob(...) ...");
		
		logger.info("loading xml file for "+batchAgentContext.getContextFileName());
		ApplicationContext context = new ClassPathXmlApplicationContext(batchAgentContext.getContextFileName());
		JobLauncher joblauncher = context.getBean(JobLauncher.class);
		Job job = (Job)context.getBean(batchAgentContext.getJobName());		
		
		try {
			logger.info("starting batch job ...");
			joblauncher.run(job, getJobParametersBuilder(batchAgentContext).toJobParameters());
		} catch (Exception e) {
			logger.error(e);
		} 
		logger.info("Exiting invokeBatchJob(...) ...");
		
		
	}
	
	protected JobParametersBuilder getJobParametersBuilder(BatchAgentContext batchAgentContext) {
		JobParametersBuilder jobparameterBuilder = new JobParametersBuilder();
		
		String batchFlowName = batchAgentContext.getBatchFlowName();
		if(logger.isDebugEnabled()) logger.debug("adding jobs param " + CoreBatchConstant.BATCH_FLOW_PARAM_NAME + " with a value of " + batchFlowName);
		jobparameterBuilder.addString(CoreBatchConstant.BATCH_FLOW_PARAM_NAME, batchFlowName);
		
		Long id = batchAgentContext.getSrcDataContainerID();
		if(id != null) {
			if(logger.isDebugEnabled()) logger.debug("adding jobs param " + CoreBatchConstant.SDC_ID_PARAM_NAME + " with a value of " + id);
			jobparameterBuilder.addLong(CoreBatchConstant.SDC_ID_PARAM_NAME, id);
		}
		
		id = batchAgentContext.getVersion();
		if(id != null) {
			if(logger.isDebugEnabled()) logger.debug("adding jobs param " + CoreBatchConstant.SDC_VERSION_PARAM_NAME + " with a value of " + id);
			jobparameterBuilder.addLong(CoreBatchConstant.SDC_VERSION_PARAM_NAME, id);
		}
		
		id = batchAgentContext.getCurrentStatusID();
		if(id != null) {
			if(logger.isDebugEnabled()) logger.debug("adding jobs param " + CoreBatchConstant.STATUS_REF_NAME + " with a value of " + id);
			jobparameterBuilder.addLong(CoreBatchConstant.STATUS_REF_NAME, id);
		}
		
		id = batchAgentContext.getCurrentRetryCount();
		if(id != null) {
			if(logger.isDebugEnabled()) logger.debug("adding jobs param " + CoreBatchConstant.SDC_RETRY_COUNT_PARAM_NAME + " with a value of " + id);
			jobparameterBuilder.addLong(CoreBatchConstant.SDC_RETRY_COUNT_PARAM_NAME, id);
		}
		
		
		//get job params
		List<JobParam> jobParams = batchAgentContext.getJobParams();
		if(jobParams != null) {
			for (JobParam jobParam : jobParams) {
				jobparameterBuilder.addString(jobParam.getParamName(), jobParam.getParamValue());
			}
		}
		//add date
		jobparameterBuilder.addDate(CoreBatchConstant.TIMESTAMP, new Date());
		
		return jobparameterBuilder;
	}
	
	

}
