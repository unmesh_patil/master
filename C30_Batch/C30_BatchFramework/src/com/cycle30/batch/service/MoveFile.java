package com.cycle30.batch.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.SourceDataContainerDAO;
import com.cycle30.batch.domain.BatchFlowConfig;
import com.cycle30.batch.domain.SrcDataContainer;

public class MoveFile implements Tasklet, StepExecutionListener {
	
	private static Logger logger = Logger.getLogger(MoveFile.class);
	
	// if the following is false then the job parameter contains path + file name
	// if it is true, then the file path and filename are retrieved from 
	// SDC details identified by sdc_id param name under job parameter
	private boolean archive;
	
	//the place where the file will be moved to
	// this is optional. If this is not set by developer,
	//get it from batch flow config based on isArchive property
	private String targetDirectory;
	
	private SourceDataContainerDAO sourceDataContainerDAO;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		String fileName = "";	//sourceFilePath + path separator + sourceFileName
		
		String destDirectory = targetDirectory;
		
		//get job params
		Map<String, Object> jobParameterMap=chunkContext.getStepContext().getJobParameters();
		//get batch flow config
		ExecutionContext jobContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		BatchFlowConfig  batchFlowConfig = (BatchFlowConfig) jobContext.get(CoreBatchConstant.BATCH_FLOW_CONFIG_PARAM_NAME);
		
		if(archive) {
			if(logger.isDebugEnabled()) logger.debug("Archive mode is true");
			//get Source file path and file name from SDC
			//retrive SDC id
			Long sdcId;
			String sdcParamId = (String)jobParameterMap.get(CoreBatchConstant.SDC_ID_PARAM_NAME);
			if(sdcParamId == null){
				sdcId=jobContext.getLong(CoreBatchConstant.SDC_ID_PARAM_NAME);
			}else{
				sdcId=Long.valueOf(sdcParamId);
			}
			//retrive - uncomment and implement  TBD
			SrcDataContainer srcDataContainer=getSourceDataContainerDAO().getSourceDataContainer(sdcId).get(0);
			//create filename by adding path and name
			String createdFileName=srcDataContainer.getResourcePathName()+"/"+srcDataContainer.getResourceName();
			fileName=createdFileName;
			if(logger.isDebugEnabled())logger.debug("created file name is "+createdFileName);
			if(destDirectory == null || destDirectory.trim().length() == 0) {
				//get it from Batch Flow Config
				if(logger.isDebugEnabled()) logger.debug("Retrieving Target ARCHIVE dir from Batch Flow Config");
				destDirectory = batchFlowConfig.getArchiveDirPath();
			}
		} else {
			if(logger.isDebugEnabled()) logger.debug("Archive mode is false");
			//get file name from Job Param
			fileName = (String)jobParameterMap.get(CoreBatchConstant.FILE_PARAM_NAME);
			//get work directory
			if(destDirectory == null || destDirectory.trim().length() == 0) {
				//get it from Batch Flow Config
				if(logger.isDebugEnabled()) logger.debug("Retrieving Target WORK dir from Batch Flow Config");
				destDirectory = batchFlowConfig.getWorkDirPath();
			}
		}
		if(logger.isDebugEnabled()) logger.debug("Dest dir to move file: " + destDirectory);
		File source = new File(fileName);
		File des = new File(destDirectory);
		FileUtils.moveFileToDirectory(source, des, true);
		
		StringBuilder sbuf = new StringBuilder(512);
		sbuf.append(destDirectory).append(source.getName());
		
		jobContext.put(CoreBatchConstant.FILE_PARAM_NAME, sbuf.toString());
		
		return RepeatStatus.FINISHED;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

	
	//==================================================
	
	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public String getTargetDirectory() {
		return targetDirectory;
	}

	public void setTargetDirectory(String targetDirectory) {
		this.targetDirectory = targetDirectory;
	}

	public SourceDataContainerDAO getSourceDataContainerDAO() {
		return sourceDataContainerDAO;
	}

	public void setSourceDataContainerDAO(
			SourceDataContainerDAO sourceDataContainerDAO) {
		this.sourceDataContainerDAO = sourceDataContainerDAO;
	}

	
	
	
	
}
