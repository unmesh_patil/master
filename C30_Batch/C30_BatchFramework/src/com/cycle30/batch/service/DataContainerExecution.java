package com.cycle30.batch.service;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.SourceDataContainerDAO;
import com.cycle30.batch.domain.SrcDataContainer;


public class DataContainerExecution implements Tasklet,StepExecutionListener{
	private static Logger logger = Logger.getLogger(DataContainerExecution.class);
	@Autowired
	private SourceDataContainerDAO srcDataContainerDAO;

	
	public SourceDataContainerDAO getSrcDataContainerDAO() {
		return srcDataContainerDAO;
	}

	public void setSrcDataContainerDAO(SourceDataContainerDAO srcDataContainerDAO) {
		this.srcDataContainerDAO = srcDataContainerDAO;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws Exception {
		Map<String, Object> jobParameterMap=chunkContext.getStepContext().getJobParameters();
		ExecutionContext jobContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		
		String resourcePath = jobContext.getString(CoreBatchConstant.FILE_PARAM_NAME);
		String batchFlowName = (String)jobParameterMap.get(CoreBatchConstant.BATCH_FLOW_PARAM_NAME);
		
		//final String FPATH = "/home/mem/index.html";
		// Filename fileName = new Filename(resourcePath, '/', '.');
	    File file = new File(resourcePath);
	    String absolutePath = file.getAbsolutePath();
	    String resourcePathName = absolutePath.substring(0,absolutePath.lastIndexOf(File.separator));
	    if(logger.isDebugEnabled()) logger.debug("Path name is "+resourcePathName);
	    if(logger.isDebugEnabled()) logger.debug("File name is "+file.getName());
	    
		SrcDataContainer srcDataContainer = new SrcDataContainer();
		srcDataContainer.setBatchFlowName(batchFlowName);
		srcDataContainer.setResourcePathName(resourcePathName);
		srcDataContainer.setResourceName(file.getName());
		srcDataContainer.setCurrentStatusID(0L);
		srcDataContainer.setVersion(1L);
		srcDataContainer.setCreated(new Date());
		srcDataContainer.setLastUpdated(new Date());
				
		srcDataContainer = srcDataContainerDAO.createSourceDataContainer(srcDataContainer);
		if(logger.isDebugEnabled()) logger.debug("The created id id "+srcDataContainer.getSrcDataContainerID());
		
		
		jobContext.put(CoreBatchConstant.SDC_ID_PARAM_NAME, srcDataContainer.getSrcDataContainerID());
		return RepeatStatus.FINISHED;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
	/*	if(sdcID!=null){
			System.err.println("writing the new SDC ID into jobcontext");
			stepExecution.getJobExecution().getExecutionContext().put("SDC_ID", sdcID);
		}*/
		
		return null;
	}
}
