package com.cycle30.batch.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.SourceDataContainerDAO;

public class SDCStatusExecution implements Tasklet,StepExecutionListener {
	private static Logger logger = Logger.getLogger(SDCStatusExecution.class); 
	
	//This is an optional
	//If the following value is true then
	//the statusId should be collected from configured value
	//else statusid value should pick from job context
	private boolean priority;
	
	//If true, get the version value from Job Parameter
	private boolean conditionalUpdate ;
	//
	private Long statusID;
	
	public boolean isConditionalUpdate() {
		return conditionalUpdate;
	}

	public void setConditionalUpdate(boolean conditionalUpdate) {
		this.conditionalUpdate = conditionalUpdate;
	}

	public Long getStatusID() {
		return statusID;
	}

	public void setStatusID(Long statusID) {
		this.statusID = statusID;
	}

	public boolean isPriority() {
		return priority;
	}

	public void setPriority(boolean priority) {
		this.priority = priority;
	}

	private SourceDataContainerDAO srcDataContainerDAO;

	
	public SourceDataContainerDAO getSrcDataContainerDAO() {
		return srcDataContainerDAO;
	}

	public void setSrcDataContainerDAO(SourceDataContainerDAO srcDataContainerDAO) {
		this.srcDataContainerDAO = srcDataContainerDAO;
	}
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		//stepExecution.getJobExecution().getExecutionContext().put(CoreBatchConstant.STATUS_PARAM_NAME, statusID);
		return null; 
	}
	
	

	@Override
	public RepeatStatus execute(StepContribution contribution,	ChunkContext chunkContext) throws Exception {
		Map<String, Object> jobParameterMap=chunkContext.getStepContext().getJobParameters();
		if(logger.isDebugEnabled()) logger.debug("Priorty is "+isPriority());
		if(logger.isDebugEnabled()) logger.debug("ConditionalUpdate is "+isConditionalUpdate());
		
		ExecutionContext jobContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		//sdc id
		Long sdcId;
		Long sdcParamId = (Long)jobParameterMap.get(CoreBatchConstant.SDC_ID_PARAM_NAME);
		
		if(sdcParamId == null){
			sdcId=jobContext.getLong(CoreBatchConstant.SDC_ID_PARAM_NAME);
			if(logger.isDebugEnabled()) logger.debug("The SDC ID retrieved from Job Execution Context is " + sdcId);
		}else{
			sdcId = sdcParamId;
			if(logger.isDebugEnabled()) logger.debug("sdcParamId from Job config params : " + sdcParamId);
		}
		
		
		//If true statusID should be captured from the configured value
		Long statusIdTemp;
		if(isPriority()){
			statusIdTemp = statusID;
			if(logger.isDebugEnabled()) logger.debug("Status id is retrived from the job config, and the value is: " +
					statusIdTemp == null? "Null" : statusIdTemp.toString());
			jobContext.put(CoreBatchConstant.STATUS_PARAM_NAME, statusID);
		}else{
			//we will collect the status id from the context
			statusIdTemp = jobContext.getLong(CoreBatchConstant.PENDING_SDC_STATUS);
			if(statusIdTemp != null)
				if(logger.isDebugEnabled()) logger.debug("Status id is retrived from job context, and the value is: " + statusIdTemp);
					//statusIdTemp == null? "Null" : statusIdTemp.toString());
			//TODO
			//statusIdTemp could be empty in some cases and in such cases it should be retirved from Job Params
		}
		
		Long version = null;
		if(isConditionalUpdate()) {
			Long ver =  (Long) chunkContext.getStepContext().getJobParameters().get(CoreBatchConstant.SDC_VERSION_PARAM_NAME);
			logger.debug("Version value from Job params:  " + ver);
			if(ver != null) version = Long.valueOf(ver);
		}
		
		if(logger.isDebugEnabled()) logger.debug("Version is " + version);
		if(logger.isDebugEnabled()) logger.debug("The SDC stats value for update is " + statusIdTemp);
		if(version == null) srcDataContainerDAO.updateSDCStatus(sdcId,statusIdTemp);
		else srcDataContainerDAO.updateSDCStatus(sdcId,statusIdTemp,version);
		return RepeatStatus.FINISHED;
	}
	
}
