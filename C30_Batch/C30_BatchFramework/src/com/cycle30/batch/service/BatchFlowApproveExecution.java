package com.cycle30.batch.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.BatchFlowExecutionDAO;
import com.cycle30.batch.domain.BatchFlowConfig;
import com.cycle30.batch.domain.SDCStatus;

public class BatchFlowApproveExecution implements Tasklet ,StepExecutionListener{
	private static Logger logger = Logger.getLogger(BatchFlowApproveExecution.class);
	
	private BatchFlowExecutionDAO batchFlowExecutionDAO;
	private BatchFlowConfig batchFlowConfig = null; 
	private String autoApproval;
	private ApprovalDelegate approvalDelegate;
	
	public ApprovalDelegate getApprovalDelegate() { 
		return approvalDelegate;
	}

	public void setApprovalDelegate(ApprovalDelegate approvalDelegate) {
		this.approvalDelegate = approvalDelegate;
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution, org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		ExecutionContext jobContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		Map<String, Object> jobParameterMap=chunkContext.getStepContext().getJobParameters();
		//String batchFlowName = (String)jobParameterMap.get(CoreBatchConstant.BATCH_FLOW_PARAM_NAME);
		Long sdcID= (Long)jobParameterMap.get(CoreBatchConstant.SDC_ID_PARAM_NAME);
		SDCStatus status = SDCStatus.APPROVAL_PENDING;

		batchFlowConfig = (BatchFlowConfig) jobContext.get(CoreBatchConstant.BATCH_FLOW_CONFIG_PARAM_NAME);
		if(logger.isDebugEnabled())	logger.debug("batchFlowConfig :  "+ batchFlowConfig );
		 
		if(logger.isDebugEnabled())	logger.debug("batchFlowConfig Exist in Job Execution context");	
		autoApproval = batchFlowConfig.getAutoApprovalFlag();
		logger.info("autoApproval value: " + autoApproval);
			
		if(autoApproval.equals(CoreBatchConstant.TRUE)){
			//auto approved
			status = SDCStatus.APPROVED;
			if(logger.isDebugEnabled())	 logger.debug("Auto approval ...");
		}else{
			//invoke delaegate as art of approval
			if(approvalDelegate==null){
				//if there is no delegate, this is a manual approval
				//somebody will have to change the value in database manually
				if(logger.isDebugEnabled())	logger.debug("Delegate is null ....");
				status = SDCStatus.APPROVAL_PENDING;
			}else{
				try{
					if(logger.isDebugEnabled())	logger.debug("Delegate is NOT null ....");
					approvalDelegate.approve(sdcID);
					status = SDCStatus.APPROVED;
					if(logger.isDebugEnabled())	logger.debug("delegate OK'ed approval");
				}catch (Exception e) {
					status = SDCStatus.APPROVAL_PENDING;
					logger.error("delegate working on SDC ID: " + sdcID);
					logger.error("delegate threw approval exception with message: " + e.getMessage());
				}
			}
			
		}
		jobContext.put(CoreBatchConstant.PENDING_SDC_STATUS, status.getId());
		if(logger.isDebugEnabled())	 logger.debug("The approval process has run and the approval status is: " + status.getName());
		return RepeatStatus.FINISHED;
	}
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		return null;
	}

	public BatchFlowExecutionDAO getBatchFlowExecutionDAO() {
		return batchFlowExecutionDAO;
	}

	public void setBatchFlowExecutionDAO(BatchFlowExecutionDAO batchFlowExecutionDAO) {
		this.batchFlowExecutionDAO = batchFlowExecutionDAO;
	}
}
