package com.cycle30.batch.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.LineItemDAO;
import com.cycle30.batch.domain.BatchFlowConfig;
import com.cycle30.batch.domain.SDCStatus;

public class ComputeSDCStatus implements Tasklet,StepExecutionListener{
	private static Logger logger = Logger.getLogger(ComputeSDCStatus.class);

	
	private LineItemDAO lineItemDAO;
	
	
	public LineItemDAO getLineItemDAO() {
		return lineItemDAO;
	}
	public void setLineItemDAO(LineItemDAO lineItemDAO) {
		this.lineItemDAO = lineItemDAO;
	}
	
	
	@Override
	public ExitStatus afterStep(StepExecution arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void beforeStep(StepExecution arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext chunkContext) throws Exception {
		Map<String, Object> jobParameterMap = chunkContext.getStepContext().getJobParameters();
		Long sdcID = (Long)jobParameterMap.get(CoreBatchConstant.SDC_ID_PARAM_NAME);
		Long sdcRetryCount = (Long)jobParameterMap.get(CoreBatchConstant.SDC_RETRY_COUNT_PARAM_NAME);
		if(sdcRetryCount == null) sdcRetryCount = 0L; 	
		
		ExecutionContext jobContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		BatchFlowConfig config= (BatchFlowConfig)jobContext.get(CoreBatchConstant.BATCH_FLOW_CONFIG_PARAM_NAME);
		Long maxErrorRetryLimit = config.getMaxErrorRetryLimit();
		Long maxLineItemErrorCount = config.getMaxAllowedErrorCount();	
		
		SDCStatus sdcStatus=SDCStatus.FAILED;		
		 
		int processFailureCount = lineItemDAO.getFailedProcessedLineItemCount(sdcID);
		int readFailureCount = lineItemDAO.getFailedReadLineItemCount(sdcID);
		int errorCount = readFailureCount + processFailureCount;
		
		if(logger.isDebugEnabled()) {
			logger.debug("The total number of failed process Line Items: " + processFailureCount );
			logger.debug("The total number of failed Read Line Items: " + readFailureCount );
			logger.debug("The max allowed line item error: " + maxLineItemErrorCount);
			
			logger.debug("The max error retry count limit is " + maxErrorRetryLimit);
			logger.debug("The current SDC retry count: " + sdcRetryCount);
		}
		
		if(errorCount == 0) {
			//no errors
			sdcStatus = SDCStatus.SUCCESS;
		} else {
			//there were some errors
			//if(no process errors  OR we can retry anymore)
			if(processFailureCount == 0 || sdcRetryCount >= maxErrorRetryLimit-1) {
				//we can not retry, so this is it
				if(errorCount > maxLineItemErrorCount)	{
					//the errors are more than what is allowed
					sdcStatus = SDCStatus.FAILED;
				} else {
					//the errors are less than what is allowed
					sdcStatus = SDCStatus.COMPLETED_WITH_ERRORS;
				}
			} else {
				//error process will retry later
				sdcStatus = SDCStatus.PARTIAL_RETRY;
			}			
		}

		if(logger.isDebugEnabled()) logger.debug("The SDC status determined to be: " + sdcStatus.getName());
		jobContext.put(CoreBatchConstant.PENDING_SDC_STATUS, sdcStatus.getId());
		return RepeatStatus.FINISHED;
	}
	
	
	
	/*private int getIntValue(String s, int defaultValue) {
		int rv = defaultValue;
		if(s != null) {
			try {
				rv = Integer.valueOf(s);
			} catch (NumberFormatException e) {
				;
			}
		}
		return rv;
	}*/

}
