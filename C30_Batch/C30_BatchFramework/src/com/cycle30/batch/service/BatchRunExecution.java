package com.cycle30.batch.service;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.BatchRunDAO;
import com.cycle30.batch.domain.BatchFlowConfig;
import com.cycle30.batch.domain.BatchRun;
/**
 * 
 * @author apadhi
 * This tasklet is used for making an entry to BatchRun table
 * It is commom for all the module
 *
 */
public class BatchRunExecution implements Tasklet,StepExecutionListener {
	private static Logger logger = Logger.getLogger(BatchRunExecution.class);
	@Autowired
	private BatchRunDAO batcRunDAO;
	/**
	 * 
	 * @return batcRunDAO
	 */
	public BatchRunDAO getBatcRunDAO() {
		return batcRunDAO;
	}
	/**
	 * 
	 * @param batcRunDAO
	 */
	public void setBatcRunDAO(BatchRunDAO batcRunDAO) {
		this.batcRunDAO = batcRunDAO;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		
		Map<String, Object> jobParameterMap=chunkContext.getStepContext().getJobParameters();
		logger.debug("Entry Batch run Execution");
		Map<String, Object> jobContext = chunkContext.getStepContext().getJobExecutionContext();
		BatchFlowConfig config= (BatchFlowConfig)jobContext.get(CoreBatchConstant.BATCH_FLOW_CONFIG_PARAM_NAME);
		Long sdcID = (Long)jobContext.get(CoreBatchConstant.SDC_ID_PARAM_NAME);
		if(sdcID==null){
			sdcID=(Long)jobParameterMap.get(CoreBatchConstant.SDC_ID_PARAM_NAME);
		}
		Long statusID = (Long)jobContext.get(CoreBatchConstant.STATUS_PARAM_NAME);
		
		BatchRun batchRun = new BatchRun();
		batchRun.setBatchFlowId(config.getBatchFlowID());
		batchRun.setSrcDataContainerId(sdcID);
		batchRun.setStatusId(statusID);
		batchRun.setJobInstanceId(chunkContext.getStepContext().getStepExecution().getJobExecution().getJobInstance().getId());
		batchRun.setCreated(new Date());
		
		batcRunDAO.insertToBatchRun(batchRun);
		return RepeatStatus.FINISHED;
	}

}
