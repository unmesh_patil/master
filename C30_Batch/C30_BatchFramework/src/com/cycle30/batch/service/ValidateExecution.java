package com.cycle30.batch.service;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.LineItemDAO;
import com.cycle30.batch.domain.BatchFlowConfig;
import com.cycle30.batch.domain.SDCStatus;

public class ValidateExecution implements Tasklet,StepExecutionListener {
	private static Logger logger = Logger.getLogger(ValidateExecution.class);
	private LineItemDAO lineItemDAO;
	private ValidationDelegate validationDelegate;

	public LineItemDAO getLineItemDAO() {
		return lineItemDAO;
	}

	public void setLineItemDAO(LineItemDAO lineItemDAO) {
		this.lineItemDAO = lineItemDAO; 
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext chunkContext)
			throws Exception {
		// TODO Auto-generated method stub
		//Map<String, Object> jobContext = chunkContext.getStepContext().getJobExecutionContext();
		ExecutionContext jobContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		BatchFlowConfig config= (BatchFlowConfig)jobContext.get(CoreBatchConstant.BATCH_FLOW_CONFIG_PARAM_NAME);
		
		Long sdcID = (Long)jobContext.get(CoreBatchConstant.SDC_ID_PARAM_NAME);
		int failedLineCount = lineItemDAO.getFailedReadLineItemCount(sdcID);
		if(logger.isDebugEnabled())logger.debug("The number of Failed item during read is "+failedLineCount);
		long maxAllowedErrorCount = config.getMaxAllowedErrorCount();
		if(logger.isDebugEnabled())logger.debug("The max number of Failed item allowed is "+maxAllowedErrorCount);
		
		SDCStatus status = SDCStatus.VALIDATION_FAILED;
		String msg = "";
		if(failedLineCount > maxAllowedErrorCount){
			msg = "Exceeded max number of allowed error count.";
		}else{
			//invoke delegate if present
			if(validationDelegate != null) {
				try {
					validationDelegate.validate(sdcID);
					status = SDCStatus.VALIDATION_SUCCESS;
				} catch (Exception e) {
					msg = e.getMessage();
					status = SDCStatus.VALIDATION_FAILED;
				}
			} else {
				status = SDCStatus.VALIDATION_SUCCESS;
			}		
		}
		storeStatus(jobContext, status, msg);
		return RepeatStatus.FINISHED;
	}
	
	private void storeStatus(ExecutionContext jobContext, SDCStatus status, String msg) {
		if(logger.isDebugEnabled())logger.debug("The Validation status is: " + status.getName());
		if(logger.isDebugEnabled())logger.debug("The Validation message is: " + msg);
		jobContext.put(CoreBatchConstant.PENDING_SDC_STATUS, status.getId());
		jobContext.put(CoreBatchConstant.PENDING_SDC_STATUS_MESSAGE, msg);
	}

	@Override
	public ExitStatus afterStep(StepExecution arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void beforeStep(StepExecution arg0) {
		// TODO Auto-generated method stub
		
	}

	public ValidationDelegate getValidationDelegate() {
		return validationDelegate;
	}

	public void setValidationDelegate(ValidationDelegate validationDelegate) {
		this.validationDelegate = validationDelegate;
	}
	

}
