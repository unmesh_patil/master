/**
 * 
 */
package com.cycle30.batch.service;

/**
 * @author Venkat Bhat
 *
 */
public interface ValidationDelegate {
	
	/**
	 * @param srcDataContainerID   
	 * @throws Exception
	 * The developer provides a concrete implementation that takes in source data conatiner id. Using this, developer
	 * can retrieve all the records that are batch job specific and apply validation to the entire content or all
	 * line items. Do not use this for line item by line item validation.
	 */
	void validate(Long srcDataContainerID) throws Exception;

}
