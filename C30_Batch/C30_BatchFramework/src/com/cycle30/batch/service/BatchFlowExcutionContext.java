package com.cycle30.batch.service;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.BatchFlowExecutionDAO;
import com.cycle30.batch.domain.BatchFlowConfig;
/**
 * 
 * @author apadhi
 * This tasklet is used for UpdateJobeexecutionContext step of all the module
 * This will fetch the data from lookup table and put the object to the context
 *
 */
public class BatchFlowExcutionContext implements Tasklet, StepExecutionListener {
	private static Logger logger = Logger.getLogger(BatchFlowExcutionContext.class);
	
	private BatchFlowExecutionDAO batchFlowExecutionDAO;
	/**
	 * 
	 * @return batchFlowExecutionDAO
	 */
	public BatchFlowExecutionDAO getBatchFlowExecutionDAO() {
		return batchFlowExecutionDAO;
	}
	/**
	 * 
	 * @param batchFlowExecutionDAO
	 */
	public void setBatchFlowExecutionDAO(BatchFlowExecutionDAO batchFlowExecutionDAO) {
		this.batchFlowExecutionDAO = batchFlowExecutionDAO; 
	}
	
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		Map<String, Object> jobParameterMap=chunkContext.getStepContext().getJobParameters();
		String batchFlowName = (String)jobParameterMap.get(CoreBatchConstant.BATCH_FLOW_PARAM_NAME);
		if(logger.isDebugEnabled()) logger.debug("Batch Flow name obtained from Job Param is " + batchFlowName);
		//Lookup the the batchFloWName
		List<BatchFlowConfig> configList = batchFlowExecutionDAO.getBatchFlowConfig(batchFlowName);
		
		if(configList.isEmpty()){
			logger.error("config list is empty");
		} else {
			if(logger.isInfoEnabled()) logger.info("config list is NOT empty");
			BatchFlowConfig config=configList.get(0);
			
			ExecutionContext jobContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
			jobContext.put(CoreBatchConstant.BATCH_FLOW_CONFIG_PARAM_NAME, config);
		}
		return RepeatStatus.FINISHED;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {		
		return null;
	}
	
}
