/**
 * 
 */
package com.cycle30.batch.io;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import com.cycle30.batch.dao.LineItemDAO;
import com.cycle30.batch.domain.LineItem;

/**
 * @author Venkat Bhat
 *
 */
public class LineItemStatusWriter implements ItemWriter<LineItem> {
	private static Logger logger = Logger.getLogger(LineItemStatusWriter.class);
	
	private LineItemDAO lineItemDAO;

	public LineItemDAO getLineItemDAO() {
		return lineItemDAO;
	}

	public void setLineItemDAO(LineItemDAO lineItemDAO) {
		this.lineItemDAO = lineItemDAO;
	}

	@Override
	public void write(List<? extends LineItem> items) throws Exception {
		for (LineItem lineItem : items) {
			lineItemDAO.updateLineItemStatus(lineItem);
		}
		
	}

}
