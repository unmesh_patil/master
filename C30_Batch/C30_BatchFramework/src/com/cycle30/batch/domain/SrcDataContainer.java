package com.cycle30.batch.domain;

import java.util.Date;

public class SrcDataContainer {
	
	private Long srcDataContainerID;  
	private String batchFlowName;
	private String resourcePathName;
	private String resourceName;
	private Long currentStatusID;
	private Long version;
	private Date created;
	private Date lastUpdated;
	
	
	public Long getSrcDataContainerID() {
		return srcDataContainerID;
	}
	public void setSrcDataContainerID(Long srcDataContainerID) {
		this.srcDataContainerID = srcDataContainerID;
	}
	public String getBatchFlowName() {
		return batchFlowName;
	}
	public void setBatchFlowName(String batchFlowName) {
		this.batchFlowName = batchFlowName;
	}
	public String getResourcePathName() {
		return resourcePathName;
	}
	public void setResourcePathName(String resourcePathName) {
		this.resourcePathName = resourcePathName;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public Long getCurrentStatusID() {
		return currentStatusID;
	}
	public void setCurrentStatusID(Long currentStatusID) {
		this.currentStatusID = currentStatusID;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
	
	
	
		
	
}
