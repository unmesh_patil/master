package com.cycle30.batch.domain;

import java.io.Serializable;

public class BatchFlow implements Serializable {
	Integer batchFlowID;
	String batchFlowName;
	public Integer getBatchFlowID() {
		return batchFlowID;
	}
	public void setBatchFlowID(Integer batchFlowID) {
		this.batchFlowID = batchFlowID;
	}
	public String getBatchFlowName() {
		return batchFlowName;
	}
	public void setBatchFlowName(String batchFlowName) {
		this.batchFlowName = batchFlowName;
	}
}
