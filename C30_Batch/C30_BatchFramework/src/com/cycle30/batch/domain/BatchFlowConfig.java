package com.cycle30.batch.domain;

import java.io.Serializable;

public class BatchFlowConfig implements Serializable {
	
	private static final long serialVersionUID = -6834269841240338657L;
	
	private Long batchFlowConfigID;
	private Long batchFlowID;
	private String srcDirPath;
	private String workDirPath;
	private String archiveDirPath;
	private Long maxAllowedErrorCount;
	private Long maxErrorRetryLimit = 5L;
	private String autoApprovalFlag;
	private String approvalEmail;
	
	public Long getBatchFlowConfigID() {
		return batchFlowConfigID;
	}
	public void setBatchFlowConfigID(Long batchFlowConfigID) {
		this.batchFlowConfigID = batchFlowConfigID;
	}
	public Long getBatchFlowID() {
		return batchFlowID;
	}
	public void setBatchFlowID(Long batchFlowID) {
		this.batchFlowID = batchFlowID;
	}
	public String getSrcDirPath() {
		return srcDirPath;
	}
	public void setSrcDirPath(String srcDirPath) {
		this.srcDirPath = srcDirPath;
	}
	public String getWorkDirPath() {
		return workDirPath;
	}
	public void setWorkDirPath(String workDirPath) {
		this.workDirPath = workDirPath;
	}
	public String getArchiveDirPath() {
		return archiveDirPath;
	}
	public void setArchiveDirPath(String archiveDirPath) {
		this.archiveDirPath = archiveDirPath;
	}
	public Long getMaxAllowedErrorCount() {
		return maxAllowedErrorCount;
	}
	public void setMaxAllowedErrorCount(Long maxAllowedErrorCount) {
		this.maxAllowedErrorCount = maxAllowedErrorCount;
	}
	public String getAutoApprovalFlag() {
		return autoApprovalFlag;
	}
	public void setAutoApprovalFlag(String autoApprovalFlag) {
		this.autoApprovalFlag = autoApprovalFlag;
	}
	public String getApprovalEmail() {
		return approvalEmail;
	}
	public void setApprovalEmail(String approvalEmail) {
		this.approvalEmail = approvalEmail;
	}
	public Long getMaxErrorRetryLimit() {
		return maxErrorRetryLimit;
	}
	public void setMaxErrorRetryLimit(Long maxErrorRetryLimit) {
		this.maxErrorRetryLimit = maxErrorRetryLimit;
	}

	
	
}
