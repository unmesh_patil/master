package com.cycle30.batch.domain;

import java.util.Date;

public class LineItem {
	public static final String STATUS_SUCCESS = "Success";
	public static final String STATUS_READ_ERROR = "Read Error";
	public static final String STATUS_VLD_ERROR = "Validation Error";
	public static final String STATUS_WRITE_ERROR = "Write Error";
	public static final String STATUS_FAIL = "Failed";
	public static final String STATUS_PENDING = "Pending";
	public static final String STATUS_COMPLETE = "Complete";
	
	private Long lineItemId;
	private Long srcDataContainerId;
	private Long lineNumber;
	private String currentStatus;
	private Long version;
	private Date created;
	private Date lastUpdated;
	private String itemContent;
	
	protected void copyLineItemValues(LineItem item) {
		lineItemId = item.getLineItemId();
		srcDataContainerId = item.getSrcDataContainerId();
		lineNumber = item.getLineNumber();
		currentStatus = item.getCurrentStatus();
		version = item.getVersion();
	}
	
	public String getItemContent() {
		return itemContent;
	}
	public void setItemContent(String itemContent) {
		this.itemContent = itemContent;
	}
	public Long getLineItemId() {
		return lineItemId;
	}
	public void setLineItemId(Long lineItemId) {
		this.lineItemId = lineItemId;
	}
	public Long getSrcDataContainerId() {
		return srcDataContainerId;
	}
	public void setSrcDataContainerId(Long srcDataContainerId) {
		this.srcDataContainerId = srcDataContainerId;
	}
	public Long getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(Long lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	



}
