/**
 * 
 */
package com.cycle30.batch.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Venkat Bhat
 *
 */
public class BatchAgentContext implements Serializable {

	private static final long serialVersionUID = -456880115406007213L;
	
	private String contextFileName;
	private String jobName;
	private String batchFlowName;
	private Long srcDataContainerID;
	private Long currentStatusID;
	private Long currentRetryCount;
	private Long version;
	private Long jobConfigId; 
	
	private List<JobParam> jobParams;
	
	public void addJobParamEntry(String name, String value) {
		if(jobParams == null) jobParams = new ArrayList<JobParam>(8);
		jobParams.add(new JobParam(name, value));
	}
	
	
	public String getContextFileName() {
		return contextFileName;
	}
	public void setContextFileName(String contextFileName) {
		this.contextFileName = contextFileName;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getBatchFlowName() {
		return batchFlowName;
	}
	public void setBatchFlowName(String batchFlowName) {
		this.batchFlowName = batchFlowName;
	}
	public Long getSrcDataContainerID() {
		return srcDataContainerID;
	}
	public void setSrcDataContainerID(Long srcDataContainerID) {
		this.srcDataContainerID = srcDataContainerID;
	}
	public Long getCurrentStatusID() {
		return currentStatusID;
	}
	public void setCurrentStatusID(Long currentStatusID) {
		this.currentStatusID = currentStatusID;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public List<JobParam> getJobParams() {
		return jobParams;
	}
	public void setJobParams(List<JobParam> jobParams) {
		this.jobParams = jobParams;
	}	
	public Long getJobConfigId() {
		return jobConfigId;
	}
	public void setJobConfigId(Long jobConfigId) {
		this.jobConfigId = jobConfigId;
	}
	public Long getCurrentRetryCount() {
		return currentRetryCount;
	}
	public void setCurrentRetryCount(Long currentRetryCount) {
		this.currentRetryCount = currentRetryCount;
	}
}
