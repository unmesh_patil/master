/**
 * 
 */
package com.cycle30.batch.domain;

/**
 * @author Venkat
 *
 */
public enum SDCStatus {
	//in progress
	IN_PROGRESS(1L, "In Progress"),
	VALIDATION_FAILED(2L, "Validation Failed"),
	VALIDATION_SUCCESS(3L, "Validation Success"),
	APPROVAL_JOB_IN_PROGRESS(4L, "Approval Job In Progress"),
	APPROVAL_PENDING(5L, "Pending Approval"),
	APPROVED(6L, "Approved"),
	REGULAR_JOB_IN_PROGRESS(7L, "Regular Job In Progress"),
	PARTIAL_RETRY(8L, "Partial Retry"),
	RETRY_START(9L, "Retry start"),
	FAILED(10L, "Failed"),
	COMPLETED_WITH_ERRORS(11L, "Completed With Errors"),
	SUCCESS(12L, "Completed Success"),
	ERROR_JOB_IN_PROGRESS(13L, "Error Job In Progress");
	
	//AUTO_APPROVED(13L, "Auto Approved");
	
	
	
	private Long id;
	private String name;
	
	private SDCStatus(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	

}
