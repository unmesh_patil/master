package com.cycle30.batch.domain;

import java.util.Date;

public class BatchRun {
	 private Long batchRunId;
	 private Long srcDataContainerId;
	 private Long batchFlowId;
	 private Long statusId;
	 private Long jobInstanceId;
	 private Date created;
	public Long getBatchRunId() {
		return batchRunId;
	}
	public void setBatchRunId(Long batchRunId) {
		this.batchRunId = batchRunId;
	}
	public Long getSrcDataContainerId() {
		return srcDataContainerId;
	}
	public void setSrcDataContainerId(Long srcDataContainerId) {
		this.srcDataContainerId = srcDataContainerId;
	}
	public Long getBatchFlowId() {
		return batchFlowId;
	}
	public void setBatchFlowId(Long batchFlowId) {
		this.batchFlowId = batchFlowId;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public Long getJobInstanceId() {
		return jobInstanceId;
	}
	public void setJobInstanceId(Long jobInstanceId) {
		this.jobInstanceId = jobInstanceId;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	 
}
