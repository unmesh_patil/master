/**
 * 
 */
package com.cycle30.batch.domain;

import java.io.Serializable;

/**
 * @author Venkat Bhat
 *
 */
public class JobParam implements Serializable {

	private static final long serialVersionUID = 1952340705794148102L;
	
	Long id;
	String paramName;
	String paramValue;
	
	public JobParam() {
		
	}
	
	public JobParam(String name, String value) {
		this.paramName = name;
		this.paramValue = value;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamValue() {
		return paramValue;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	
	
	

}
