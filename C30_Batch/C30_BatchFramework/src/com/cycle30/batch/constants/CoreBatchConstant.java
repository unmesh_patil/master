/**
 * 
 */
package com.cycle30.batch.constants;

/**
 * @author Venkat Bhat
 *
 */
public interface CoreBatchConstant {
	
	
	//mostly Job Parameter names
	String FILE_PARAM_NAME = "fw_fileName";
	String BATCH_FLOW_PARAM_NAME = "fw_batchFlowName";
	String TIMESTAMP = "fw_timestamp";
	
	String SDC_ID_PARAM_NAME = "fw_sdc_id";
	String SDC_VERSION_PARAM_NAME = "fw_sdc_version";
	String STATUS_PARAM_NAME = "fw_status_id";
	String STATUS_REF_NAME = "fw_status_ref_id"; 
	String SDC_RETRY_COUNT_PARAM_NAME = "fw_sdc_retry_count";
	
	//mostly Execution Context param names
	String BATCH_FLOW_CONFIG_PARAM_NAME = "fw_batchFlowConfig";
	 
	String PENDING_SDC_STATUS="fw_pending_sdc_status";
	String PENDING_SDC_STATUS_MESSAGE = "fw_pending_sdc_status_message";
	String SDC_STATUS="fw_sdc_status";
	String TRUE = "T";
	String FALSE ="F";
	String IN_POGRESS = "InProgress";
	String COMP_SUCCESS = "Completed Success";
	
	String PROCESS_TYPE="process_type";
}
