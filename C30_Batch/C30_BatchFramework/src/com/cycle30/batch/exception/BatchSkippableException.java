/**
 * 
 */
package com.cycle30.batch.exception;

/**
 * @author Venkat Bhat
 *
 */
public class BatchSkippableException extends BatchException {
	
	private static final long serialVersionUID = 1190233345765148282L;
	
	
	public BatchSkippableException(int lineNumber, Exception ex) {
		super(lineNumber, ex);
	}

}
