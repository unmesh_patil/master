/**
 * 
 */
package com.cycle30.batch.exception;

/**
 * @author venbhat
 *
 */
public class BatchException extends Exception {

	private static final long serialVersionUID = -6799951445231680496L;
	
	private int lineNumber;
	
	public BatchException(int lineNumber, Exception ex) {
		super(ex);
		this.lineNumber = lineNumber;
	}

	public int getLineNumber() {
		return lineNumber;
	}
	
	

}
