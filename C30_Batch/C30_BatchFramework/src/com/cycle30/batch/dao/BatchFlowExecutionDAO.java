package com.cycle30.batch.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cycle30.batch.domain.BatchFlow;
import com.cycle30.batch.domain.BatchFlowConfig;
/**
 * 
 * @author gkatchi
 *
 */
public class BatchFlowExecutionDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private Map<String, String> batchflowdaoQueries;
	/**
	 * 
	 * @return JdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	/**
	 * 
	 * @param jdbcTemplate jdbcTemplate
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	public Map<String, String> getBatchflowdaoQueries() {
		return batchflowdaoQueries;
	}
	/**
	 * 
	 * @param lineitemdaoQueries 
	 */
	public void setBatchflowdaoQueries(Map<String, String> batchflowdaoQueries) {
		this.batchflowdaoQueries = batchflowdaoQueries;
	}
	/**
	 * 
	 * @param batchFlowbo object of type BatchFlow
	 * @return batchId
	 */
	public Integer saveBatchFlow(BatchFlow batchFlowbo){
		
		jdbcTemplate.update(batchflowdaoQueries.get("INSERT_BATCH_FLOW"),batchFlowbo.getBatchFlowName());
		Integer batchID= jdbcTemplate.queryForInt(batchflowdaoQueries.get("SELECT_BATCH_ID"));
		return batchID;
	}
	/**
	 * 
	 * @param batchFlowConfigbo instanceof BatchFlowCofig
	 */
	public void saveBatchFlowConfig(BatchFlowConfig batchFlowConfigbo){
		
		jdbcTemplate.update(batchflowdaoQueries.get("INSERT_BATCH_FLOW_CONFIG"),
				batchFlowConfigbo.getBatchFlowID(),
				batchFlowConfigbo.getSrcDirPath(),
				batchFlowConfigbo.getWorkDirPath(),
				batchFlowConfigbo.getArchiveDirPath(),
				batchFlowConfigbo.getMaxAllowedErrorCount(),
				batchFlowConfigbo.getAutoApprovalFlag(),
				batchFlowConfigbo.getApprovalEmail());
	}
	/**
	 * 
	 * @param batchFlowName
	 * @return
	 */
	public List<BatchFlowConfig> getBatchFlowConfig(String batchFlowName){
		List<BatchFlowConfig> configList= (List<BatchFlowConfig>) jdbcTemplate.query(batchflowdaoQueries.get("SELECT_CONFIG"), new Object[]{batchFlowName}, new ConfigMapper());
		return configList;
	}
	
	/**
	 * 
	 * @author gkatchi
	 * row mapper class mappig table batch_Flow_cofig
	 *
	 */
	private static final class ConfigMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			BatchFlowConfig config = new BatchFlowConfig();
			config.setBatchFlowConfigID(rs.getLong("BATCH_FLOW_CONFIG_ID"));
			config.setBatchFlowID(rs.getLong("BATCH_FLOW_ID"));
			config.setSrcDirPath(rs.getString("SRC_DIR_PATH"));
			config.setWorkDirPath(rs.getString("WORK_DIR_PATH"));
			config.setArchiveDirPath(rs.getString("ARCHIVE_DIR_PATH"));
			config.setMaxAllowedErrorCount(rs.getLong("MAX_LINE_ITEM_ERROR_COUNT"));
			config.setAutoApprovalFlag(rs.getString("AUTO_APPROVAL_FLAG"));
			config.setApprovalEmail(rs.getString("APPROVAL_NOTIF_EMAIL"));
			return config;
		}
	}

}
