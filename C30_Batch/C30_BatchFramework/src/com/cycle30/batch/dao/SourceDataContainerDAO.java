package com.cycle30.batch.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.incrementer.DataFieldMaxValueIncrementer;

import com.cycle30.batch.domain.SDCStatus;
import com.cycle30.batch.domain.SrcDataContainer;
	/**
	 * 
	 * @author gkatchi
	 *
	 */
public class SourceDataContainerDAO {
	
	private static Logger logger = Logger.getLogger(SourceDataContainerDAO.class);
	
	private JdbcTemplate jdbcTemplate;
	private Map<String, String> sdcdaoQueries;
	/**
	 * 
	 * @return jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
    /**
     * 
     * @param jdbcTemplate  instance of JDBCTemplate
     */

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * 
	 * @return Queries Map
	 */
	
	public Map<String, String> getSdcdaoQueries() {
		return sdcdaoQueries;
	}
	/**
	 * 
	 * @param batchrundaoQueries 
	 */
	public void setSdcdaoQueries(Map<String, String> sdcdaoQueries) {
		this.sdcdaoQueries = sdcdaoQueries;
	}

	private SimpleJdbcInsert insertSDC;
    private DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer;
    private DataFieldMaxValueIncrementer statusIdMaxValueIncrementer;
    
    /**
     * 
     * @param dataSource instance of DataSource
     */
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.insertSDC =
                new SimpleJdbcInsert(dataSource)
                        .withTableName("SRC_DATA_CONTAINER");
    }

	/**
	 * 
	 * @param srcDataContainer instance of SrcDataContainer
	 * @return object of updated SrcDataContainer
	 */
	public SrcDataContainer createSourceDataContainer(SrcDataContainer srcDataContainer) {
		//create a sequence
		Long id = dataFieldMaxValueIncrementer.nextLongValue();
		//set the value
		srcDataContainer.setSrcDataContainerID(id);
		if(logger.isDebugEnabled()) logger.debug("The sequuence id obtained is "+ srcDataContainer.getSrcDataContainerID());
		
		/*SqlParameterSource parameters = new BeanPropertySqlParameterSource(srcDataContainer);
		insertSDC.execute(parameters);*/
		
		jdbcTemplate.update(sdcdaoQueries.get("INSERT_SRC_DATA_CONTAINER"),
				srcDataContainer.getSrcDataContainerID(),
				srcDataContainer.getBatchFlowName(),
				srcDataContainer.getResourcePathName(),
				srcDataContainer.getResourceName(),
				srcDataContainer.getCurrentStatusID(),
				srcDataContainer.getVersion(),
				srcDataContainer.getCreated(),
				srcDataContainer.getLastUpdated());
		
		return srcDataContainer;
	}
	/**
	 * 
	 * @param sdcID instance of Long
	 * @param statusID instance of Long
	 */
	public void updateSDCStatus(Long sdcID, Long statusID){ 
		//get current version from looking up using sdcId
		List<SrcDataContainer> srcDataList= getSourceDataContainer(sdcID);
		Long version = srcDataList.get(0).getVersion();
		if(logger.isDebugEnabled())logger.debug("Current version for sdcID is "+version);
		
		jdbcTemplate.update(sdcdaoQueries.get("INSERT_SDC_LINK"), getStatusIdMaxValueIncrementer().nextLongValue(),statusID,sdcID,new Date());
		jdbcTemplate.update(sdcdaoQueries.get("UPDATE_SDC_STATUS"),statusID,new Date(),version+1,sdcID);
		updateSDCRetryCount(statusID, sdcID);
	}
	/**
	 * 
	 * @param sdcID instance of Long
	 * @param statusID instance of Long
	 * @param version insatnce of Long
	 */
	public void updateSDCStatus(Long sdcID, Long statusID, Long version){
		jdbcTemplate.update(sdcdaoQueries.get("INSERT_SDC_LINK"), getStatusIdMaxValueIncrementer().nextLongValue(),statusID,sdcID,new Date());
		jdbcTemplate.update(sdcdaoQueries.get("UPDATE_SDC_STATUS_WITH_VERSION"),statusID,new Date(),version+1,sdcID,version);
		updateSDCRetryCount(statusID, sdcID);
	}
	/**
	 * 
	 * @param statusID instance of Long
	 * @param sdcID instance of Long
	 */
	private void updateSDCRetryCount(Long statusID, Long sdcID) {
		if(statusID != SDCStatus.RETRY_START.getId()) return;
		Long currentRetryCount = jdbcTemplate.queryForLong(sdcdaoQueries.get("SELECT_RETRY_COUNT"),sdcID);
		jdbcTemplate.update(sdcdaoQueries.get("UPDATE_SDC_RETRY_COUNT"), currentRetryCount+1L, sdcID, currentRetryCount);
	}
	
	/**
	 * 
	 * @return DataFieldMaxValueIncrementer
	 */
	public DataFieldMaxValueIncrementer getDataFieldMaxValueIncrementer() {
		return dataFieldMaxValueIncrementer; 
	}
	/**
	 * 
	 * @param dataFieldMaxValueIncrementer
	 */
	public void setDataFieldMaxValueIncrementer(
		DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer) {
		this.dataFieldMaxValueIncrementer = dataFieldMaxValueIncrementer;
	}
	// To get SDC_ID from TABLE SRC_DATA_CONTAINER 
	/*public Long getSdcIDFromDataContainer(){
		Long sdcID= jdbcTemplate.queryForLong(SELECT_SDC_ID);
		return sdcID;
	}*/
	
	/*
	 * @param sdcId instance of Long
	 * return instance of list of srcdataContainer Objects
	 * This method can be reused to get sourcedatacontainer obje
	 * by passing SDCID
	 */
	
	public List<SrcDataContainer> getSourceDataContainer(Long sdcId ){
		List<SrcDataContainer> srcDataContainer = (List<SrcDataContainer>)jdbcTemplate.query(sdcdaoQueries.get("SELECT_VERSION_FROM_SDCID"), new Object[]{sdcId}, new SRCDataContainerMapper());
		//return jdbcTemplate.queryForLong(SELECT_VERSION_FROM_SDCID, sdcId);
		return srcDataContainer;
	}
	
	/**
	 * 
	 * @author gkatchi
	 * row mapper class
	 */

	private static final class SRCDataContainerMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			SrcDataContainer srcDataContainer = new SrcDataContainer();
			srcDataContainer.setVersion(rs.getLong("VERSION"));
			srcDataContainer.setResourcePathName(rs.getString("RESOURCE_PATH_NAME"));
			srcDataContainer.setResourceName(rs.getString("RESOURCE_NAME"));
			//can be added for other field also  on required basis
			return srcDataContainer;
		}
	}
	/**
	 * 
	 * @return DataFieldMaxValueIncrementer
	 */

	public DataFieldMaxValueIncrementer getStatusIdMaxValueIncrementer() {
		return statusIdMaxValueIncrementer;
	}
	/**
	 * 
	 * @param statusIdMaxValueIncrementer 
	 */

	public void setStatusIdMaxValueIncrementer(
			DataFieldMaxValueIncrementer statusIdMaxValueIncrementer) {
		this.statusIdMaxValueIncrementer = statusIdMaxValueIncrementer;
	}
	
	
	
}
