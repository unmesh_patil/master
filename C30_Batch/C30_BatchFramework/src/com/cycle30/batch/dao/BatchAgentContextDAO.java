/**
 * 
 */
package com.cycle30.batch.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cycle30.batch.domain.BatchAgentContext;
import com.cycle30.batch.domain.JobParam;

/**
 * @author Venkat Bhat
 *
 */
public class BatchAgentContextDAO {
	private static Logger logger = Logger.getLogger(BatchAgentContextDAO.class);
	
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * 
	 * @return JdbcTemplate
	 */
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	/**
	 * 
	 * @param jdbcTemplate 
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
    private Map<String, String> batchagentdaoQueries;
	
	/**
	 * 
	 * @return Queries Map
	 */
	
	public Map<String, String> getBatchagentdaoQueries() {
		return batchagentdaoQueries;
	}
	/**
	 * 
	 * @param batchagentdaoqueries 
	 */
	public void setBatchagentdaoQueries(Map<String, String> batchagentdaoQueries) {
		this.batchagentdaoQueries = batchagentdaoQueries;
	}
	/**
	 * 
	 * @return list of BatchAgentContextObjects
	 */
	public List<BatchAgentContext> getPendingJobDetails() {
		List<BatchAgentContext> batchAgentContexts = null;
		batchAgentContexts= (List<BatchAgentContext>)jdbcTemplate.query(batchagentdaoQueries.get("GET_NEXT_SDC_FOR_JOB"),new BatchAgentContextMapper());
		return batchAgentContexts;
	}
	/**
	 * 
	 * @param jobConfigId
	 * @return list of jobparam objects
	 */
	public List<JobParam> getJobParam(Long jobConfigId){
		List<JobParam> jobParamList=jdbcTemplate.query(batchagentdaoQueries.get("GET_JOB_PARAM"),new Object[]{jobConfigId},new JobParamMapper());
		return jobParamList;
	}		
	/**
	 *  row mapper class for mapping sourcedatacotainer table
	 */
	private static final class BatchAgentContextMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			BatchAgentContext batchAgentContext = new BatchAgentContext();
			batchAgentContext.setBatchFlowName(rs.getString("batch_flow_name"));
			batchAgentContext.setSrcDataContainerID(rs.getLong("src_data_container_id"));
			batchAgentContext.setVersion(rs.getLong("version"));
			batchAgentContext.setCurrentStatusID(rs.getLong("current_status_id"));
			batchAgentContext.setCurrentRetryCount(rs.getLong("current_retry_count"));
			batchAgentContext.setContextFileName(rs.getString("app_contxt_file_name"));
			batchAgentContext.setJobName(rs.getString("job_name"));
			batchAgentContext.setJobConfigId(rs.getLong("JOB_CONFIG_ID"));
			//can be added for other field also  on required basis
			return batchAgentContext;
		}
	}

	/**
	 *  row mapper class for mapping jobconfig table
	 */
	private static final class JobParamMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			JobParam jobParamMapper = new JobParam();
			jobParamMapper.setId(rs.getLong("JOB_CONFIG_JOB_PARAMS_ID"));
			jobParamMapper.setParamName(rs.getString("JOB_CONFIG_PARAM_NAME"));
			jobParamMapper.setParamValue(rs.getString("JOB_CONFIG_PARAM_VALUE"));
			//can be added for other field also  on required basis
			return jobParamMapper;
		}
	}
}
