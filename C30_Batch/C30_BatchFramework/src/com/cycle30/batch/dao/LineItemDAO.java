package com.cycle30.batch.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.incrementer.DataFieldMaxValueIncrementer;

import com.cycle30.batch.domain.LineItem;
/**
 * 
 * @author gkatchi
 *
 */
public class LineItemDAO {
	
		
	private JdbcTemplate jdbcTemplate;
	private Map<String, String> lineitemdaoQueries;
	private DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer;
	/**
	 * 
	 * @return jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	/**
	 * 
	 * @param jdbcTemplate
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	/**
	 * 
	 * @return DataFieldMaxValueIncrementer
	 */
	public DataFieldMaxValueIncrementer getDataFieldMaxValueIncrementer() {
		return dataFieldMaxValueIncrementer;
	}
	/**
	 * 
	 * @param dataFieldMaxValueIncrementer
	 */
	public void setDataFieldMaxValueIncrementer(
			DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer) {
		this.dataFieldMaxValueIncrementer = dataFieldMaxValueIncrementer;
	}	
	/**
	 * 
	 * @return Queries Map
	 */
	
	public Map<String, String> getLineitemdaoQueries() {
		return lineitemdaoQueries;
	}
	/**
	 * 
	 * @param lineitemdaoQueries 
	 */
	public void setLineitemdaoQueries(Map<String, String> lineitemdaoQueries) {
		this.lineitemdaoQueries = lineitemdaoQueries;
	}
	/**
	 * 
	 * @param lineItem instance of LineItem
	 */
	public void insertIntoLineItem(LineItem lineItem){
		//create a sequence
		Long id = dataFieldMaxValueIncrementer.nextLongValue();
		//set the value
		lineItem.setLineItemId(id);
		Date date = new Date();
		jdbcTemplate.update(lineitemdaoQueries.get("INSERT_LINE_ITEM"),
				lineItem.getLineItemId(),
				lineItem.getSrcDataContainerId(),
				lineItem.getLineNumber(),
				lineItem.getCurrentStatus(),
				lineItem.getVersion(),
				date,
				date);
		
	}
	/**
	 * 
	 * @param sdcId instance of long
	 * @return error count
	 */
	public Integer getFailedReadLineItemCount(Long sdcId){
		//retrieve line item count that is NOT in LineItem.STATUS_SUCCESS
		Integer error_count=jdbcTemplate.queryForInt(lineitemdaoQueries.get("SELECT_FAILED_READ_LINE_ITEM_COUNT"), sdcId);
		
		return error_count;
	}
	/**
	 * 
	 * @param sdcId instance of Log
	 * @return errorCount
	 */
	public Integer getFailedProcessedLineItemCount(Long sdcId){
		//retrieve line item count that is NOT in LineItem.STATUS_COMPLETE
		Integer error_count=jdbcTemplate.queryForInt(lineitemdaoQueries.get("SELECT_FAILED_PROCESS_LINE_ITEM_COUNT"), sdcId);
		
		return error_count;
	}
	/**
	 * 
	 * @param sdcId instance of Long
	 * @return list of LineItems
	 */
	public List<LineItem> getFailedLineItems(Long sdcId){
		List<LineItem> lineItemList=(List<LineItem>)jdbcTemplate.query(lineitemdaoQueries.get("SELECT_FAILED_LINE_ITEMS"), 
				new Object[]{sdcId,LineItem.STATUS_FAIL},new LineItemContainerMapper());
		return lineItemList;
	}

	/**
	 * 
	 * @author gkatchi
	 * rowmapper class mapping LINE_ITEM table
	 */
	private static final class LineItemContainerMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			LineItem lineItem=new LineItem();
			lineItem.setLineItemId(rs.getLong("LINE_ITEM_ID"));
			lineItem.setSrcDataContainerId(rs.getLong("SRC_DATA_CONTAINER_ID"));
			lineItem.setCurrentStatus(rs.getString("CURRENT_STATUS"));
			lineItem.setLastUpdated(rs.getDate("LAST_UPDATED"));
			lineItem.setCreated(rs.getDate("CREATED"));
			//can be added for other field also  on required basis
			return lineItem;
		}
	}
	/**
	 * 
	 * @param status instanceof String
	 * @param lineItemId instanceof Long
	 */
	public void updateLineStatus1(String status,Long lineItemId){
		jdbcTemplate.update(lineitemdaoQueries.get("LINE_ITEM_STATUS"), status,lineItemId);
		
	}
	/**
	 * 
	 * @param item instance Of LineItem
	 */
	public void updateLineItemStatus(LineItem item){
		Long newVersion = item.getVersion() + 1L;
		//use status and newVersion for update, and
		//id for conditional update
		jdbcTemplate.update(lineitemdaoQueries.get("LINE_ITEM_STATUS_VERSION"),item.getCurrentStatus(),newVersion, new Date(), item.getLineItemId());
	}
	/**
	 * 
	 * @param item instance of LineItem
	 * @param newStatus instance of String
	 */
	public void updateLineItemStatus(LineItem item, String newStatus) {
		Long newVersion = item.getVersion() + 1L;
		//use newStatus and newVersion for update, and
		//id and current version for conditional clause
		jdbcTemplate.update(lineitemdaoQueries.get("LINE_ITEM_STATUS_VERSION_CONDITIONAL"),newStatus,newVersion, new Date(), item.getLineItemId(),item.getVersion());
	}
	
	
	
		
}
