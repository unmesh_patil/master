package com.cycle30.batch.dao;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.incrementer.DataFieldMaxValueIncrementer;

import com.cycle30.batch.domain.BatchRun;
/**
 * 
 * @author gkatchi
 *
 */
public class BatchRunDAO  {
	private static Logger logger = Logger.getLogger(BatchRunDAO.class);
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer;
	/**
	 * 
	 * @return DataFieldMaxValueIncrementer
	 */
	public DataFieldMaxValueIncrementer getDataFieldMaxValueIncrementer() {
		return dataFieldMaxValueIncrementer;
	}
	/**
	 * 
	 * @param dataFieldMaxValueIncrementer
	 */
	public void setDataFieldMaxValueIncrementer(
			DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer) {
		this.dataFieldMaxValueIncrementer = dataFieldMaxValueIncrementer; 
	}
	/**
	 * 
	 * @return jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	/**
	 * 
	 * @param jdbcTemplate
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
    }
    private Map<String, String> batchrundaoQueries;
	
	/**
	 * 
	 * @return Queries Map
	 */
	
	public Map<String, String> getBatchrundaoQueries() {
		return batchrundaoQueries;
	}
	/**
	 * 
	 * @param batchrundaoQueries 
	 */
	public void setBatchrundaoQueries(Map<String, String> batchrundaoQueries) {
		this.batchrundaoQueries = batchrundaoQueries;
	}
	/**
	 * 
	 * @param batchRun instance of BatchRun
	 */
	public void insertToBatchRun(BatchRun batchRun){
		//create a sequence
		Long id = dataFieldMaxValueIncrementer.nextLongValue();
		//set the value
		batchRun.setBatchRunId(id);
		if(logger.isDebugEnabled()) logger.debug("The sequuence id obtained is "+ batchRun.getBatchRunId());
		jdbcTemplate.update(batchrundaoQueries.get("INSERT_BATCH_RUN"),
				batchRun.getBatchRunId(),
				batchRun.getSrcDataContainerId(),
				batchRun.getBatchFlowId(),
				batchRun.getStatusId(),
				batchRun.getJobInstanceId(),
				batchRun.getCreated());
	}
	
}
