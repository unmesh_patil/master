package com.cycle30.batch.core;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @author Venkat Bhat
 *
 */
public class JobLaunchBaseHelper {
	private static Logger logger = Logger.getLogger(JobLaunchBaseHelper.class);
	
	 
	
	String contextName;
	JobLauncher joblauncher;
	Job job;
	
	protected JobParametersBuilder jobparameterBuilder;
	
	
	public JobLaunchBaseHelper(String contextName, String jobName) {
		ApplicationContext context = new ClassPathXmlApplicationContext(contextName);
		joblauncher= context.getBean(JobLauncher.class);
		job = (Job) context.getBean(jobName);
		jobparameterBuilder = new JobParametersBuilder();
	}
	
	
	public JobParametersBuilder getJobParametersBuilder() {
		return jobparameterBuilder;
	}
	
	public void run() throws Exception {
		logger.info("Running job: " + job.getName());
		
		JobExecution jobExecution = joblauncher.run(job, jobparameterBuilder.toJobParameters());	
		
		logger.info("Job ended with status "+jobExecution.getStatus());
	}
	
	

}
