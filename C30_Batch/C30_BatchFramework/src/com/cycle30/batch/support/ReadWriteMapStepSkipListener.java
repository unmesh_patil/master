/**
 * 
 */
package com.cycle30.batch.support;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.LineItemDAO;
import com.cycle30.batch.domain.LineItem;
import com.cycle30.batch.exception.BatchException;

/**
 * @author Venkat Bhat
 *
 */
public class ReadWriteMapStepSkipListener<T extends LineItem, S extends LineItem> implements SkipListener<LineItem, LineItem>, StepExecutionListener {
	
	private static Logger logger = Logger.getLogger(ReadWriteMapStepSkipListener.class);
	
	private LineItemDAO lineItemDAO;
	
	protected Long srcDataContainerId;	
	
	protected void createLineItemWithErors(Throwable t) {
		if(logger.isDebugEnabled()) logger.debug("Entering method: createLineItemWithErors(Throwable t)");
		int lineNumber = -1;
		String message = "unkown";
		
		if(t != null) {
			message = t.getMessage();
			BatchException cause = null;
			if(t instanceof BatchException) {
				cause = (BatchException)t;
			} else {
				t = t.getCause();
				if(t instanceof BatchException) cause = (BatchException)t;
			}
			if(cause != null) {
				lineNumber = cause.getLineNumber();
				message = cause.getMessage();
			}
			createLineItemWithErors(lineNumber, LineItem.STATUS_READ_ERROR, message);
		}
		
	}
	
	protected void createLineItemWithErors(int lineNumber, String status, String message) {
		if(logger.isDebugEnabled()) logger.debug("Entering method: createLineItemWithErors(int lineNumber, String message)");
		if(logger.isDebugEnabled()) logger.debug("The failure message is: " + message);
		LineItem lineItem = new LineItem();
		lineItem.setSrcDataContainerId(srcDataContainerId);
		lineItem.setLineNumber(new Long(lineNumber));
		lineItem.setCurrentStatus(status);
		lineItem.setVersion(1L);
		Date date = new Date();
		lineItem.setCreated(date);
		lineItem.setLastUpdated(date);
		lineItemDAO.insertIntoLineItem(lineItem);
		
		if(logger.isDebugEnabled()) logger.debug("Exiting method: createLineItemWithErors(BatchException be)");
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution arg0) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public void beforeStep(StepExecution stepExecution) {
		//retrieve SRC Id for later use
		if(logger.isDebugEnabled()) logger.debug("Entering method: beforeStep ...");
		ExecutionContext jobContext = stepExecution.getJobExecution().getExecutionContext();
		if(logger.isDebugEnabled()) logger.debug("Getting  source data cotainer id");
		srcDataContainerId = jobContext.getLong(CoreBatchConstant.SDC_ID_PARAM_NAME);
		if(logger.isDebugEnabled()) logger.debug("Source data cotainer id is: " + srcDataContainerId);
	}

	public LineItemDAO getLineItemDAO() {
		return lineItemDAO;
	}

	public void setLineItemDAO(LineItemDAO lineItemDAO) {
		this.lineItemDAO = lineItemDAO;
	}

	@Override
	public void onSkipInProcess(LineItem lineItem, Throwable t) {
		createLineItemWithErors(lineItem.getLineNumber().intValue(),LineItem.STATUS_VLD_ERROR, t.getMessage());
	}

	@Override
	public void onSkipInRead(Throwable t) {
		createLineItemWithErors(t);
	}

	@Override
	public void onSkipInWrite(LineItem lineItem, Throwable t) {
		createLineItemWithErors(lineItem.getLineNumber().intValue(),LineItem.STATUS_WRITE_ERROR, t.getMessage());
	}




	

}
