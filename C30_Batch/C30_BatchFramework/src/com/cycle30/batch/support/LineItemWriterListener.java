/**
 * 
 */
package com.cycle30.batch.support;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;

import com.cycle30.batch.constants.CoreBatchConstant;
import com.cycle30.batch.dao.LineItemDAO;
import com.cycle30.batch.domain.LineItem;

/**
 * @author Venkat Bhat
 *
 */
public class LineItemWriterListener implements StepExecutionListener, ItemWriteListener<LineItem> {
	
	private static Logger logger = Logger.getLogger(LineItemWriterListener.class);
	
	
	private LineItemDAO lineItemDAO;
	
	protected Long srcDataContainerId;	

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution arg0) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public void beforeStep(StepExecution stepExecution) {
		//retrieve SRC Id for later use
		if(logger.isDebugEnabled()) logger.debug("Entering method: beforeStep ...");
		ExecutionContext jobContext = stepExecution.getJobExecution().getExecutionContext();
		if(logger.isDebugEnabled()) logger.debug("Getting  source data cotainer id");
		srcDataContainerId = jobContext.getLong(CoreBatchConstant.SDC_ID_PARAM_NAME);
		if(logger.isDebugEnabled()) logger.debug("Source data cotainer id is: " + srcDataContainerId);
	}
	
	@Override
	public void beforeWrite(List<? extends LineItem> lineItems) {
		for (LineItem lineItem : lineItems) {
			if(logger.isDebugEnabled()) logger.debug("Saving Line Item with line number: " + lineItem.getLineNumber());
			//line number is already set
			lineItem.setSrcDataContainerId(srcDataContainerId);
			lineItem.setCurrentStatus(LineItem.STATUS_SUCCESS);
			lineItem.setVersion(1L);
			Date date = new Date();
			lineItem.setCreated(date);
			lineItem.setLastUpdated(date);
			lineItemDAO.insertIntoLineItem(lineItem);
			//after the above call line_item_id is set which is needed for
			//saving the job specific domain
		}
	}

	@Override
	public void afterWrite(List<? extends LineItem> lineItems) {
		//do nothing
	}


	@Override
	public void onWriteError(Exception arg0, List<? extends LineItem> lineItems) {
		//do nothing
	}

	public LineItemDAO getLineItemDAO() {
		return lineItemDAO;
	}

	public void setLineItemDAO(LineItemDAO lineItemDAO) {
		this.lineItemDAO = lineItemDAO;
	}
	
}
