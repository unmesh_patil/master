/**
 * 
 */
package com.cycle30.batch.support;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.cycle30.batch.domain.LineItem;

/**
 * @author Venkat Bhat
 *
 */
public class Cycle30ItemProcessor implements ItemProcessor<Object, Object> {
	private static Logger logger = Logger.getLogger(Cycle30ItemProcessor.class);

	@Override
	public Object process(Object input) throws Exception {
		if(input != null && input instanceof LineItem) return input;
		//do not want to prcoess this
		if(logger.isDebugEnabled()) logger.debug("Filtering record:" + input.toString());
		return null;
	}
	
	

}
