/**
 * 
 */
package com.cycle30.batch.support;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemReadListener;

import com.cycle30.batch.dao.LineItemDAO;
import com.cycle30.batch.domain.LineItem;

/**
 * @author Venkat Bhat
 *
 */
public class LineItemReadListener implements ItemReadListener<LineItem> {
	private static Logger logger = Logger.getLogger(LineItemReadListener.class);
	
	private LineItemDAO lineItemDAO;


	public LineItemDAO getLineItemDAO() {
		return lineItemDAO;
	}

	public void setLineItemDAO(LineItemDAO lineItemDAO) {
		this.lineItemDAO = lineItemDAO;
	}

	@Override
	public void afterRead(LineItem item) {
		//update status to pending ....
		lineItemDAO.updateLineItemStatus(item, LineItem.STATUS_PENDING);
	}

	@Override
	public void beforeRead() {
		// do nothing
		
	}

	@Override
	public void onReadError(Exception arg0) {
		// do nothing
		
	}
	
	

}
