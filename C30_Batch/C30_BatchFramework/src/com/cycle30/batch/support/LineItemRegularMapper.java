package com.cycle30.batch.support;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cycle30.batch.domain.LineItem;


public  class LineItemRegularMapper implements RowMapper<LineItem> {
	
	public LineItem mapRow(ResultSet rs, int rowNum) throws SQLException {
		LineItem lineItem=new LineItem();
		lineItem.setLineItemId(rs.getLong("LINE_ITEM_ID"));
		lineItem.setSrcDataContainerId(rs.getLong("SRC_DATA_CONTAINER_ID"));
		lineItem.setLineNumber(rs.getLong("LINE_NUMBER"));
		lineItem.setCurrentStatus(rs.getString("CURRENT_STATUS"));
		lineItem.setVersion(rs.getLong("VERSION"));
		return lineItem;
	}
}