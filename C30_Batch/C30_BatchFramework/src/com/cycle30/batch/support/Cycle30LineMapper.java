/**
 * 
 */
package com.cycle30.batch.support;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;

import com.cycle30.batch.domain.LineItem;
import com.cycle30.batch.exception.BatchException;
import com.cycle30.batch.exception.BatchSkippableException;

/**
 * @author Venkat Bhat
 * @param <T>
 *
 */
public class Cycle30LineMapper<T> extends DefaultLineMapper<T> {
	private static Logger logger = Logger.getLogger(Cycle30LineMapper.class);
	
	private boolean convertToSkipException = true;
	
	public T mapLine(String line, int lineNumber) throws Exception {
		T t = null;
		try {
			//delegate to the base class
			t = super.mapLine(line, lineNumber);
		} catch (Exception e) {
			logger.error("Error while reading line number: " + lineNumber, e);
			if(convertToSkipException) throw new BatchSkippableException(lineNumber,e);
			throw new BatchException(lineNumber, e);
		}
		//now if this is a LineItem or a derived class from there of
		if(t != null && t instanceof LineItem) {
			if(logger.isDebugEnabled()) logger.debug("Setting line number: " + lineNumber);
			LineItem lineItem = (LineItem)t;
			lineItem.setLineNumber(new Long(lineNumber));
		}
		return t;
	}

	public boolean isConvertToSkipException() {
		return convertToSkipException;
	}

	public void setConvertToSkipException(boolean convertToSkipException) {
		this.convertToSkipException = convertToSkipException;
	}
	
	
	
	
	

}
