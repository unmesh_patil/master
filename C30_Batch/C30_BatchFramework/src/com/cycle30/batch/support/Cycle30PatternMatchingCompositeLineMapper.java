/**
 * 
 */
package com.cycle30.batch.support;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;

import com.cycle30.batch.domain.LineItem;

/**
 * @author Venkat Bhat
 *
 */
public class Cycle30PatternMatchingCompositeLineMapper<T> extends PatternMatchingCompositeLineMapper<T> {
	private static Logger logger = Logger.getLogger(Cycle30PatternMatchingCompositeLineMapper.class);
	
	@Override
	public T mapLine(String line, int lineNumber) throws Exception {
		T t = super.mapLine(line, lineNumber);
		//now if this is a LineItem or a derived class from there of
		if(t != null && t instanceof LineItem) {
			if(logger.isDebugEnabled()) logger.debug("Setting line number: " + lineNumber);
			LineItem lineItem = (LineItem)t;
			lineItem.setLineNumber(new Long(lineNumber));
		}
		return t;
	}
	
	

}
