package com.cycle30.fxweb.test;


import static org.junit.Assert.*;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.fxweb.util.DomUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;


/**
 * 
 * Automated JUnit test case for Web Enablement purposes.
 * 
 */
public class TestGeocodeGet {


    private static Client client;


    @BeforeClass
    public static void oneTimeSetUp() {
        client = Client.create();  // do one time and reuse
    }
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }


    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
      
    @Test
    public void testGet() throws Exception {
            
        //System.setProperty("javax.net.ssl.trustStore", "C:\\mule-3.2.0-c3b\\apps\\WebEnablement\\truststorem2m.jks");
        System.setProperty("javax.net.ssl.trustStore", "/opt/app/jenkinsci/mule-3.2.0/apps/WebEnablement/truststore.jks");
        
        //WebResource webResource = client.resource("https://localhost:52003/GCI/DEV");  
        WebResource webResource = client.resource("https://m2m1devkenap1.cycle30.com:52020/C3B/DEV/addresses/geocode"); 
        
        MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
        queryParams.add("city",  "Anchorage");
        queryParams.add("state", "Ak");
        queryParams.add("zip",   "99503");
        
        WebResource.Builder wrb = webResource.queryParams(queryParams).getRequestBuilder();
    
        wrb.header("X-Env-Id",   "M2MDEV");
        wrb.header("X-Trans-Id", "1234");
        wrb.header("X-Org-Id",   "3456");
        try {
           ClientResponse clientResponse = wrb.get(ClientResponse.class);
           int status = clientResponse.getStatus();
           String body = clientResponse.getEntity(String.class);
           
           boolean statusOk = (status==200);
           assertTrue("GET method did not return OK (200) status", statusOk);
        } catch (Exception e) {
            e.printStackTrace();
        }

        
    }

}
