package com.cycle30.fxweb.resource;

import java.util.HashMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.PrepaidAccountResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidAccount;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.prov.data.Constants;


/** 
 * 
 * Prepaid Account resource object
 * 
 */

public class AccountPrepaidResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(AccountPrepaidResourceImpl.class);   
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
        
        
        return null; 
    }
    

    /**
     * Process the HTTP GET request. This will be a single-account
     * 'find' or 'history' based on whether URI query params exist.  
     * 
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
    	PrepaidAccountResponseImpl response = new PrepaidAccountResponseImpl(); 
        response.setResponseType(ResponseTypes.response.PrepaidAccountResponse.toString());
        response.setResponseCode(Response.Status.OK);

        // get customer Subscriber number which is nothing but the accountNumber 
        // search type, and set the response property appropriately.
        RequestProperties props = request.getRequestProperties();
        String subscriberNumber = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), "subscriberNumber");

        String accountProperty = FxWebUtil.prepaidAccountProperty(request.getRequestProperties());
        
        HashMap<String, ?> results = new HashMap();

		try {

			results = new SdkPrepaidAccount().getPrepaidAccountDetails(request);
			
		} catch (FxWebException we) {

			// Form generic error for ResponseFormatter purposes
			String error = we.getFormattedMessage();
			HashMap excMap = new HashMap();
			excMap.put("ExceptionCode", ""); // placeholder
			excMap.put("MsgText", error);
		}			
		
        // Format the response
		String formattedResponse = formatPrepaidAccountResponse(response, accountProperty, results);
		response.setResponseBody(formattedResponse);

		response.setResponseCode(Response.Status.OK);
    
		return response;
    
    }
    
    
 
	/**
	 * Format Prepaid Account Search/History response. 
	 * 
	 * <AccountSearchResponse> 
	 *     <Status> #Defines overall status
	 *        <Message>SUCCESS</message>
	 *        <Code>0</code>
	 *     </Status> 
	 * <AccountList size="1"> 
	 *   <Account > 
	 *        <History>	#This tag to be ignored for search Response
	 *        
	 *        // Direct response from StreamWIDE Prepaid system
	 *        
     *   	  </History>
	 *   </Account>
	 * </AccountList>
	 * </AccountSearcResponse> 
	 *   
	 * 
	 * 
	 * @param response
	 * @param results
	 * @return
	 */
    
    private String formatPrepaidAccountResponse(PrepaidAccountResponseImpl response, String accountProperty, HashMap<String, ?> results) throws FxWebException {
    	
    	
		// Set status to 0=success as default
		String code = "0";
		String message = "SUCCESS";
		String 	exceptionCode = null;
		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        
		// Default response = OK
        Response.Status errorStatus = Response.Status.OK;
        response.setResponseCode(errorStatus);
        
        // HashMap with the key of "0".            
        HashMap resultMap = (HashMap)results.get("0");

		xmlResponse.append("<AccountSearchResponse>");
		xmlResponse.append("<Status>");
		
        // See if exception occurred. If so, get the error message 
        exceptionCode = (String)resultMap.get("ExceptionCode");
        
        if (exceptionCode != null) {
        	message = (String)resultMap.get("MsgText");
			xmlResponse.append("<Message>" + message + "</Message>");
			xmlResponse.append("<Code>" + exceptionCode + "</Code>");
			xmlResponse.append("</Status>");

        } else {
    	
	    		xmlResponse.append("<Message>" + message + "</Message>");
	    		xmlResponse.append("<Code>" + code + "</Code>");
	    		xmlResponse.append("</Status>");
	    		int listSize = 1;
	    		
    			xmlResponse.append("<AccountList size=\"" + listSize + "\">");	   
    			if (accountProperty.equalsIgnoreCase("history")) {
    				xmlResponse.append("<History>");
    			}
    			
    			//Append OCS response XML
	    		String origrespXML = resultMap.get("PrepaidAccountDetailsXML").toString();
	    		String removeheaderXML = origrespXML.replace(Constants.OCS_RESPONSE_XML_HEADER, "");
	    		String OCSfinalXML = removeheaderXML.replace(Constants.OCS_RESPONSE_METHODS_TAG, "<methodResponse>");
	    		xmlResponse.append(OCSfinalXML);
	    		
    			if (accountProperty.equalsIgnoreCase("history")) {
    				xmlResponse.append("</History>");
    			}
    	    	xmlResponse.append("</AccountList>");			
    	    	
			}	    	
        
		xmlResponse.append("</AccountSearchResponse>");
		return xmlResponse.toString();        
    	
    }

}