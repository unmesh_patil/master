package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.HistoryPrepaidAccountResponseImpl;
import com.cycle30.fxweb.response.PrepaidBalanceAccountResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;


/** 
 * OCS  resource object
 */

public class HistoryPrepaidResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(AccountBalPrepaidResourceImpl.class);   
    
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
        
        
        return null; 
    }
    
    

    /**
     * Process the HTTP GET request. This will be always a single-account
     * 'find' search based on whether URI query 
     * params exist.  
     * 
     * If an account number was found either on the URI path or as a query param,
     * invoke the single-account 'finder' method.  Otherwise, use the 'search' 
     * method.
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
    	PrepaidBalanceAccountResponseImpl response = new PrepaidBalanceAccountResponseImpl(); 
        //response.setResponseType(ResponseTypes.ACCOUNT_SEARCH_RESPONSE);
        response.setResponseType(ResponseTypes.response.AccountSearchResponse.toString());
        response.setResponseCode(Response.Status.OK);
        //  get customer Subscriber number which is nothing but the accountNumber 
        // search type, and set the response property appropriately.
        RequestProperties props = request.getRequestProperties();
        String subscriberNumber = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), "subscriberNumber");
        
        
        // Get system id from request header.
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
       
        
		HashMap<String, HashMap> results = new HashMap();

		HashMap<String, String> history = new HashMap();
		try {
		
			history.put("GCI_COMMON", "200");
			history.put("GCI_VOICE_LOCAL", "1000");
			history.put("GCI_VOICE_NAT_LIMITED", "1000");
		
				String key = String.valueOf("0");
				results.put(key, history);

				String xml = formatHistoryPrepaidResponse(response, results);
				response.setResponseBody(xml);
			}

        
        catch (FxWebException we) {

            // Form generic error for ResponseFormatter purposes
            String error = we.getFormattedMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);
        }

      
 
        return response;
    }
    
    
    
    
 
	/**
	 * Format Account Balance Prepaid Search response. The response is quite simplistic, requiring just
	 * the correlation id from the original request, the tracking id from the
	 * SDK response, and a status structure. The end result is:
	 * 
	 * <AccountSearchResponse> 
	 *     <Status> #Defines overall status
	 *        <Message>SUCCESS</message>
	 *        <Code>0</code>
	 *     </Status> 
	 * <AccountList size="1"> 
	 *   <Account > 
	 *        <History>
	 *        	<BalanceTypeLabel>GCI_COMMON</BalanceTypeLabel>
     *     		<BalanceUnits>
     *       		<Type>MONEY|| UNITS|| SUBSCRIPTION ||TIME || VOLUME </Type>
     *        		<Value>10000</Value>
     *     		</BalanceUnits>
     *     		<ExpirationDate>2012-07-22T09:30:47.0Z</ExpirationDate>
     *   	  </History>
	 *   </Account>
	 * </AccountList>
	 * </AccountSearcResponse> 
	 *   
	 * 
	 * 
	 * @param response
	 * @param results
	 * @return
	 */
	private String formatHistoryPrepaidResponse(PrepaidBalanceAccountResponseImpl response,
			HashMap<String, HashMap> results) throws FxWebException {
		// Set status to 0=success, 1=failure
		String code = "0";
		String message = "SUCCESS";

		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<AccountSearchResponse>");
		xmlResponse.append("<Status>");
		xmlResponse.append("<Message>" + message + "</Message>");
		xmlResponse.append("<Code>" + code + "</Code>");
		xmlResponse.append("</Status>");

		boolean accountBalOk = true;

		Response.Status errorStatus = Response.Status.OK;

		// Go through the Map of HashMaps, and generate a Inventory structure
		// for each
		Iterator i = results.keySet().iterator();
		while (i.hasNext()) {

			int count = 1;
			int seq = 1;
			String key = (String) i.next();
			HashMap result = (HashMap) results.get(key);

		
			String exception = null;

			// Get all the Balances
			String commonBal = (String) result.get("GCI_COMMON");
			String voiceLocal = (String) result.get("GCI_VOICE_LOCAL");
			String voice_nat = (String) result.get("GCI_VOICE_NAT_LIMITED");


			// Get the system id (eg. K1, K2) already set in the response
			String system = response.getSystemId();

			// Generate all XML elements.

			xmlResponse.append("<AccountList size=1>");
			xmlResponse.append("<Account >");
			
			xmlResponse.append("<History>");
			   xmlResponse.append("<BalanceTypeLabel>GCI_COMMON</BalanceTypeLabel>");
			    xmlResponse.append("<BalanceUnits>");
			     xmlResponse.append("<Type>MONEY|| UNITS|| SUBSCRIPTION ||TIME || VOLUME </Type>");
			     xmlResponse.append("<Value>" +commonBal  + "</Value>"); 
			xmlResponse.append("<BalanceUnits>");
			xmlResponse.append("<ExpirationDate>2012-07-27T09:28:47.0Z</ExpirationDate>");
		
	xmlResponse.append("</History>");
	
		} // end while()

		xmlResponse.append("</Account></AccountList></AccountSearchResponse>");
		
		

		// if all Balance failed (didn't find one that was ok, set return
		// status to error condition.
		if (accountBalOk == false) {
			response.setResponseCode(errorStatus);
		}
		log.info(xmlResponse);

		return xmlResponse.toString();
	}

}