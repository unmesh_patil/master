package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Random;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.request.OrderRequestImpl;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.RequestTransformer;
import com.cycle30.fxweb.request.ResourceRequestInf;

import com.cycle30.fxweb.response.OrderResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.sdkinterface.SdkOrderProcessor;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;


/** 
 * Kenan resource object
 */

public class OrderResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(OrderResourceImpl.class);   
    
    private static final String COMPLETE = "80";
    private static final String SIMULATE_COMPLETE = "81";
    private static final String ERROR = "99";
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("PUT")) {
            ResourceResponseInf response = putResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("POST")) {
            ResourceResponseInf response = postResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("DELETE")) {
            ResourceResponseInf response = deleteResource(request);
            return response;
        }
        
      return null; 
    }
    
    

    /**
     * Process the HTTP GET request for an order.  
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
        OrderResponseImpl response = new OrderResponseImpl(); 
        
        String requestId = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), "requestid");
        
        response.setResponseCode(Response.Status.OK);
        
        // Get system id from request header.
        RequestProperties props = request.getRequestProperties();
        HttpHeaders headers = props.getHeaders();
        response.setSystemId(FxWebUtil.getKenanVersion(headers));
        
        HashMap<String, HashMap> results = new HashMap<String, HashMap> ();
        
        
        try { 
           results = (HashMap<String, HashMap>) new SdkOrderProcessor().getOrderStatus(requestId);
        }
        
        catch (FxWebException we) {

            // Form generic error for ResponseFormatter purposes
            String error = we.getFormattedMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);
        }

        String xmlResult = formatRequestStatusResponse(requestId, results);
        
        response.setResponseBody(xmlResult);
 
        return response;

    }
    
    
    /**
     * Process the HTTP DELETE request for an order.  
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf deleteResource(ResourceRequestInf request) throws FxWebException {
   
        OrderResponseImpl response = new OrderResponseImpl(); 
        
        String requestId = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), "requestid");
        log.info("deleteResource - requestid = "+requestId);
        response.setResponseCode(Response.Status.OK);
        
        // Get system id from request header.
        RequestProperties props = request.getRequestProperties();
        HttpHeaders headers = props.getHeaders();
        response.setSystemId(FxWebUtil.getKenanVersion(headers));
        
        String orderId = FxWebUtil.getPathParamValue(props.getPathParams(), "orderId");
        
		String message     = "SUCCESS";
		String respCode    = "0";        
        
        try {
        	//Update Order Status to Cancel
            new SdkOrderProcessor().deleteOrderStatus(requestId, orderId);
        } catch (FxWebException we) {
			// Form generic error for ResponseFormatter purposes
			String error = we.getMessage();
			message = error;
			respCode = "1";
		}
        
        //Send Response
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<OrderResponse><Order>");
		xmlResponse.append("<OrderId>"     + orderId     + "</OrderId>");
		xmlResponse.append("<Status>" +
				"<Message>" + message  + "</Message>" +
				"<Code>"    + respCode + "</Code>" +
				"</Status>");
		xmlResponse.append("</Order></OrderResponse>");
		response.setResponseCode(Response.Status.OK);

		response.setResponseBody(xmlResponse.toString());

		return response;

    }
    
    
    
    /**
     * Handle PUT request. This is used to update the status of an 
     * order.
     *
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf putResource(ResourceRequestInf request) throws FxWebException {
        
        OrderResponseImpl response = new OrderResponseImpl(); 
        response.setResponseCode(Response.Status.OK);
        
        // Get system id from request header.
        RequestProperties props = request.getRequestProperties();
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
        HashMap results = new HashMap();
        
        try { 
            
           results = new SdkOrderProcessor().updateOrderStatus(request);
                     
           // If a porting action is detected via a callback message from a 
           // network provider, manually create a 'dependent' order and process it.
           Document dom = props.getPayload();
           boolean isPortingAction = FxWebUtil.isPortingAction(dom);
           boolean isPrepaid = FxWebUtil.isPrepaid(dom);           
           
           if (isPortingAction) {

               if (!isPrepaid) {

                   // create a temporary request and property objects for this 'manual'
                   // order process.
                   ResourceRequestInf tempRequest = new OrderRequestImpl();
                   
                   RequestProperties properties = new RequestProperties();
                   properties.setClientId(props.getClientId());  
                   properties.setTransactionId(props.getTransactionId());
                   properties.setInternalId(props.getInternalId());
            	   
                   // Manually generate payload and set it in the properties
                   Document portDom = FxWebUtil.generatePortingOrderPayload(dom);
                   properties.setPayload(portDom);
                   
                   // Make sure to do any XML cleanup as if this were a payload
                   // received from an external client.
                   RequestTransformer.transformXml(properties);
                   
                   // Set new-request with new-properties
                   tempRequest.setRequestProperties(properties); 
                   
                   results = new SdkOrderProcessor().processOrder(tempRequest);
            	   
               } else {
            	   log.info("Prepaid Porting, Not required to create an Extended Data Update order");
               }
           }
           
        } 
        
        catch (FxWebException we) {

            // Form generic error for ResponseFormatter purposes
            String error = we.getMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);

        }

        String xmlResult = formatPutResponse(request, response, results);
        response.setResponseBody(xmlResult);

        return response;
        
    }
    
    
    /**
     * Handle POST request
     * 
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf postResource(ResourceRequestInf request) throws FxWebException {
        
        OrderResponseImpl response = new OrderResponseImpl(); 
        
        // Get system id from request header.
        RequestProperties props = request.getRequestProperties();
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
        //--------------------------------------------------------------------------
        // Do special check for AWN - if POST is detected, then have to assume 
        // it's a request to update the order status (same as PUT request from
        // other clients).  Transform their special version of the status payload
        // into the standardized version, then invoke our PUT method.
        // Check header (if supplied) or path (special for AWN purposes).
        //--------------------------------------------------------------------------
        boolean awn = FxWebUtil.isAwnRequest(request);
   
        if (awn == true) {
            Document doc = FxWebUtil.transformAwnPayload(request);
            props.setPayload(doc);
            
            // Manually generate transaction id since not supplied by NSG
            long transId = new Random().nextLong();
            if (transId < 0) transId *= -1;  // make positive #
            String transactionId = Long.toString(transId);
            props.setTransactionId(transactionId);
            
            response = (OrderResponseImpl)putResource(request);
            return response;
            
        }
        
        
        HashMap results = new HashMap();
        
        try { 
           results = new SdkOrderProcessor().processOrder(request);
        }
        
        catch (FxWebException we) {

            // Form generic error for ResponseFormatter purposes
            String error = we.getFormattedMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);
        }

        String xmlResult = formatResponse(request, response, results);
        
        response.setResponseBody(xmlResult);
 
        return response;
        
    }

    /**
     * Format find response.  
     * 
     * <OrderResponse>
     *   
     *       <OrderId></OrderId>
     *       <OrderStatus>Committed/Completed</OrderStatus>
     *           
     *       <Account>
     *         <Status>
     *           <System>K2</System>
     *           <Message>SUCCESS</Message>
     *           <Code>0</Code>
     *         </Status>
     *         <AccountNumber>12345</AccountNumber>
     *       </Account>
     *           
     *     </Order>
     * </OrderResponse>
     * 
     * 
     * @param response
     * @param results
     * @return
     */
    private String formatResponse(ResourceRequestInf request,
                                   OrderResponseImpl response,
                                   HashMap results)  throws FxWebException {
        

        
        RequestProperties props = request.getRequestProperties();
        String systemId = FxWebUtil.getKenanVersion(props.getHeaders());
        
        
        // Default response = OK
        response.setResponseCode(Response.Status.OK);
        
        Document dom = request.getRequestProperties().getPayload();
        
        String acctNumber  = FxWebUtil.getClientAccountId(dom);
        if (acctNumber == null) {
            acctNumber = "unknown";
        }
        
        String orderId = DomUtils.getElementValue(dom, "ClientOrderId");
        
        //TODO - refactor this based on client id.  Below is SPS-specific
        if (orderId==null) {
            orderId = DomUtils.getElementValue(dom, "idRequest");
        }
        
        String orderStatus = "Submitted";
        String message     = "SUCCESS";
        String respCode    = "0";
        

        // See if exception occurred
        HashMap<String, String> orderResponse = (HashMap)results.get("0");
        if (orderResponse == null) {
            message  = "No response data returned from SDK";
            respCode = "1";
        }
        
        String excCode = null;
        if (orderResponse != null)  {
            excCode = orderResponse.get("ExceptionCode"); 
            String addlErrorText = orderResponse.get("AdditionalErrorText"); 
            
            if (excCode != null) {
                if (excCode.length()==0) {
                    message  = orderResponse.get("MsgText"); 
                    respCode = "1";
                    response.setResponseCode(Response.Status.BAD_REQUEST);
                }
                else {
                    MappedError me = new MappedError(excCode, addlErrorText);
                    message        = me.getExternalError();
                    respCode       = me.getExceptionCode();
                    // Set HTTP response code
                    Response.Status errorStatus = me.getStatus();
                    response.setResponseCode(errorStatus);
                }
            }
        }  
        
        
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        xmlResponse.append("<OrderResponse><Order>");
        xmlResponse.append("<OrderId>"     + orderId     + "</OrderId>");
        xmlResponse.append("<OrderStatus>" + orderStatus + "</OrderStatus>");
        
        xmlResponse.append("<Status>" +
                              "<System>"  + systemId + "</System>" +
                              "<Message>" + message  + "</Message>" +
                              "<Code>"    + respCode + "</Code>" +
                           "</Status>");
        xmlResponse.append("<AccountNumber>" + acctNumber + "</AccountNumber>");

        
        xmlResponse.append("</Order></OrderResponse>");
        return xmlResponse.toString();

        
    }
    
    
    /**
     * Format put response.  
     * 
     * <OrderUpdateResponse>
     *   
     *       <OrderId></OrderId>
     *       <ExtCorrelationId></ExtCorrelationId>
     *       <Status>
     *         <Code>0=ok, <>0 =error</Code>
     *         <Message></Message>
     *       </Status>
     *       
     *       <ErrorDetails>
     *         <Error>
     *            <ServiceId></ServiceId>
     *            <Code></Code>
     *            <Message></Message>
     *         </Error>
     *           . . .
     *       </ErrorDetails>
     *
     * </OrderUpdateResponse>
     * 
     * 
     * @param response
     * @param results
     * @return
     */
    private String formatPutResponse(ResourceRequestInf request,
                                      OrderResponseImpl response,
                                      HashMap results)  throws FxWebException {
        

        
        RequestProperties props = request.getRequestProperties();
        String systemId = FxWebUtil.getKenanVersion(props.getHeaders());
        
        
        // Default response = OK
        response.setResponseCode(Response.Status.OK);
        
        Document dom = request.getRequestProperties().getPayload();      
        String orderId     = DomUtils.getElementValue(dom, "OrderId");   
        String extCorrId   = DomUtils.getElementValue(dom, "ExtCorrelationId");
        
        String message     = "SUCCESS";
        String respCode    = "0";
        

        // See if exception occurred
        HashMap<String, String> orderResponse = (HashMap)results.get("0");
        String excCode = null;
        if (orderResponse != null)  {
            excCode = orderResponse.get("ExceptionCode"); 
            String addlErrorText = orderResponse.get("AdditionalErrorText"); 
            
            if (excCode != null) {
                if (excCode.length()==0) {
                    message  = orderResponse.get("MsgText"); 
                    respCode = "1";
                    response.setResponseCode(Response.Status.BAD_REQUEST);
                }
                else {
                    MappedError me = new MappedError(excCode, addlErrorText);
                    message        = me.getExternalError();
                    respCode       = me.getExceptionCode();
                    // Set HTTP response code
                    Response.Status errorStatus = me.getStatus();
                    response.setResponseCode(errorStatus);
                }
            }
        }  
              
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        xmlResponse.append("<OrderUpdateResponse>");
        xmlResponse.append("<OrderId>"           + orderId   + "</OrderId>");
        xmlResponse.append("<ExtCorrelationId>"  + extCorrId + "</ExtCorrelationId>");
        
          xmlResponse.append("<Status>" +
                                "<Message>" + message  + "</Message>" +
                                "<Code>"    + respCode + "</Code>" +
                             "</Status>");
          // Future use
          xmlResponse.append("<ErrorDetails></ErrorDetails>");
                             
        xmlResponse.append("</OrderUpdateResponse>");
        return xmlResponse.toString();

        
    }
    
    
    
    /**
     * Format request status inquiry results. Result will be in format of:
     * 
     *       <OrderInquiryResponse>
     *              <Order>
     *                  <RequestId>" + orderId + "</RequestId>
     *                  <Requestdate>2012-07-27T09:28:47.0Z</Requestdate>
     *                  <Status>
     *                     <Code></Code>
     *                     <Message>IN PROGRESS</Message>
     *                     <ErrorDetails></ErrorDetails>
     *                  </Status>
     *                  <ServiceStatusList>
     *                     <Service>
     *                        <FeatureId></FeatureId>
     *                        <Type>Voice</Type>
     *                        <Status>
     *                           <Code>0</Code>
     *                           <Message>SUCCESS</Message>
     *                        </Status>
     *                        <CompleteDate>2012-07-27T09:30:47.0Z</CompleteDate>
     *                     </Service>
     *                     <Service>
     *                        <FeatureId></FeatureId>
     *                        <Type>SMS</Type>
     *                       <Status>
     *                           <Code>0</Code>
     *                           <Message>SUCCESS</Message>
     *                        </Status>
     *                        <CompleteDate>2012-05-27T09:34:21.1Z</CompleteDate>
     *                    </Service>
     *                      . . .
     *                  </ServiceStatusList>
     *              </Order>
     *      </OrderInquiryResponse>
     * 
     *      
     * @param requestId
     * @param results
     * @return
     * @throws FxWebException
     */
    private String formatRequestStatusResponse(String requestId, 
                                                HashMap<String, HashMap> results)  
                                                      throws FxWebException {
        
        StringBuffer xml = new StringBuffer(FxWebConstants.XML_HEADER);
        
        xml.append("<OrderInquiryResponse><Order><RequestId>" + requestId + "</RequestId>");
        
        // Check for error - will be in one/only map entry with "0" key
        boolean inquiryError = checkRequestInquiryError(results, xml);
        if (inquiryError) {
            return xml.toString();
        }

        
        // Go through all service status entries in results map, and generate
        // necessary XML (while tracking overall status).
        StringBuffer serviceXml = new StringBuffer();
        int serviceIdx = 0;
        
        boolean allComplete   = true;
        boolean errorOccurred = false;
        String requestDate     = null;
        String logMessage      = null;
        
        serviceXml.append("<ServiceStatusList>");  
        
        HashMap<String, String> service = null;
        
        do {
            
            String key = Integer.toString(serviceIdx++);
            service = results.get(key);
            
            if (service != null) {
                
                // Get single request date and log message
                requestDate = service.get("RequestDate");
                logMessage  = service.get("LogMessage");
                
                // Form Service XML section    
                serviceXml.append("<Service>");  
                
                  serviceXml.append("<FeatureId>" + service.get("FeatureId") + "</FeatureId>");
                  
                  String featureName = service.get("FeatureName");
                  if (featureName == null || featureName.equalsIgnoreCase("null")) {
                      featureName="";
                  }
                  serviceXml.append("<Type>" + featureName + "</Type>");
                  serviceXml.append("<SubRequestId>" + service.get("SubRequestId") + "</SubRequestId>");
                  
                  serviceXml.append("<Status>");
                      String statusId = service.get("StatusId");
                      String code = "0";
                      if (statusId.equals(COMPLETE)==false && statusId.equalsIgnoreCase(SIMULATE_COMPLETE)==false) {
                          code = statusId;  // use actual status if not complete
                      }
                      serviceXml.append("<Code>"    + code  + "</Code>");
                      serviceXml.append("<Message>" + service.get("StatusName") + "</Message>");
                  serviceXml.append("</Status>");
                  
                  String completeDate = service.get("ResponseDate");
                  if (completeDate == null) completeDate = "";
                  serviceXml.append("<CompleteDate>" + completeDate + "</CompleteDate>");  
                  
                serviceXml.append("</Service>");  
                
                // Check each status for 'global' status. '80' means completed
                if (statusId.equals(COMPLETE)==false && statusId.equalsIgnoreCase(SIMULATE_COMPLETE)==false) {
                    allComplete = false;
                }
                if (statusId.equals(ERROR) == true) {
                    errorOccurred = true;
                }
            }            

            
        } while (service != null);
        
        serviceXml.append("</ServiceStatusList>");  
        
        
        // Set the single request date and global status/message
        xml.append("<RequestDate>" + requestDate + "</RequestDate>");
        
        
        // Clean up the log message, if it exists.
        if (logMessage == null || logMessage.equalsIgnoreCase("null")) {
            logMessage = "";
        }
        // Strip the prefix found in all messages
        String logMsg = logMessage.replaceAll("JavaException: com.cycle30.sdk.toolkit.exception.C30SDKToolkitException: ", "");  
        
        // Status will be SUCCESS, ERROR or IN PROGRESS
        xml.append("<Status>");  
            String code = "0";
            String message = "SUCCESS";
            
            // If provisioning error found, or if a error log message found...
            if (errorOccurred || (logMsg != null && logMsg.length()>0)) {
                code = "1";
                message = "ERROR";
            } 
            else if (allComplete == false) {
                code = "";
                message = "IN PROGRESS";
            }
    
            
            xml.append("<Code>"         + code    + "</Code>");
            xml.append("<Message>"      + message + "</Message>");
            xml.append("<ErrorDetails>" + logMsg  + "</ErrorDetails>");
        xml.append("</Status>");
        
        
        // append service XML
        xml.append(serviceXml);
        
        // terminating tax
        xml.append("</Order></OrderInquiryResponse>");
        
        String xmlString = xml.toString();
        
        return xmlString;
        
    }

    
    
    /**
     * 
     * @param results
     * @param xml
     * @return
     */
    private boolean checkRequestInquiryError(HashMap<String, HashMap> results, StringBuffer xml) {
        
        HashMap<String, String> hm = results.get("0");
    
        String message = "";
        String excCode = null;
        
        if (hm == null) {
            excCode = "1";
            message = "No results found for request id provided.";
        }    
        else {
            excCode = hm.get("ExceptionCode");
            message = hm.get("MsgText");
        }
        
        // If no results or error occurred, return failure code/message
        if (excCode != null) {
            xml.append("<RequestDate></RequestDate>");
            xml.append("<Status>");
            xml.append("<Code>"    + excCode + "</Code>");
            xml.append("<Message>" + message + "</Message>");
            xml.append("</Status>");
            xml.append("<ServiceStatusList/>");
            xml.append("</Order></OrderInquiryResponse>");
            return true;
        }
        
        return false;
    }


}
