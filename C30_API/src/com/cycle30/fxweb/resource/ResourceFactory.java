package com.cycle30.fxweb.resource;


import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;


/**
 * Factory class for FX Web Enablement resource objects.
 *
 */
public class ResourceFactory {

    // Instantiate singleton
    private static final ResourceFactory self = new ResourceFactory();

    private static final Logger log = Logger.getLogger(ResourceFactory.class);

    private static final String EXCEPTION_SOURCE = "RESOURCE-FACTORY";


    //-------------------------------------------------------------------------
    /** Private constructor.
     */
    private ResourceFactory() {
        super();
    }


    //-------------------------------------------------------------------------
    /**
     * Return instance of singleton class
     *
     * @return ResourceRequestFactory instance.
     */
    public static ResourceFactory getInstance() {
        return self;
    }



    // Instantiate correct Fx Web resource interface based on the resource name
    public ResourceInf getResourceImpl(String resourceType) throws FxWebException  {

        if (resourceType.startsWith("account")) {
            return new AccountResourceImpl();
        }
        if (resourceType.startsWith("jobstatus")) {
            return new JobStatusResourceImpl();
        }
        if (resourceType.startsWith("order")) {
            return new OrderResourceImpl();
        }
        if (resourceType.startsWith("geocode")) {
            return new GeoCodeResourceImpl();
        }
        if (resourceType.startsWith("clientcallback")) {
            return new ClientCallbackResourceImpl();
        }
        if (resourceType.startsWith("invoiceCharges")) {
            return new InvoiceChargesResourceImpl();
	    }
        if (resourceType.startsWith("invoice")) {
            return new InvoiceResourceImpl();
        }
        if (resourceType.startsWith("payment")) {
            return new PaymentResourceImpl();
        }
        if (resourceType.startsWith("prepaidpayment")) {
            return new PrepaidPaymentResourceImpl();
        }
        if (resourceType.startsWith("service")) {
            return new ServiceResourceImpl();
        }
        if (resourceType.startsWith("healthcheck")) {
		    return new HealthCheckResourceImpl();
        }
        if (resourceType.startsWith("inventory")) {
            return new InventoryResourceImpl();
        }
        if (resourceType.startsWith("simInventory")) {
            return new SIMInventoryResourceImpl();
        }

        if (resourceType.startsWith("phoneSearchInventory")) {
            return new PhoneInventorySearchResourceImpl();
        }
        if (resourceType.startsWith("prepaidAccountBal")){
              return new AccountBalPrepaidResourceImpl();
        }
        if (resourceType.startsWith("prepaidAccount")){
            return new AccountPrepaidResourceImpl();
        }

        if (resourceType.startsWith("voucher")) {
           return new VoucherResourceImpl();
        }
        

        // If not found,
        String error = "Unable to instantiate correct resource type (account, service, etc) based on resource type " + resourceType;
        log.error(error);

        throw new FxWebException (error, EXCEPTION_SOURCE);


    }
}
