package com.cycle30.fxweb.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;


import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.provision.nsg.NsgProvisioningUtils;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.response.ServiceResponseImpl;
import com.cycle30.fxweb.sdkinterface.SdkServiceStatus;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.object.cycle30.service.CatalogInstance;
import com.cycle30.sdk.object.cycle30.service.IdentifierInstance;


/** 
 * Kenan resource object
 */

public class ServiceResourceImpl implements ResourceInf {

	public static Logger log = Logger.getLogger(ServiceResourceImpl.class);   



	// Handle the resource request, returning a response instance.
	public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {


		// Get the original HTTP method from the 'http.method' header
		RequestProperties properties = request.getRequestProperties();
		String method = FxWebUtil.getRequestMethod(properties.getHeaders());


		// Instantiate the response        
		if (method.equalsIgnoreCase("GET")) {
			ResourceResponseInf response = getResource(request);
			return response;
		}


		return null; 
	}



	/**
	 * Process the HTTP GET request. 
	 * 
	 * Check the path params to see what type of service information is being requested,
	 * eg. a health check. 
	 *     
	 * @param request
	 * @return Resource response instance
	 */
	private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {

		ServiceResponseImpl response = new ServiceResponseImpl(); 
		response.setResponseType(ResponseTypes.response.ServiceResponse.toString());

		// 
		RequestProperties props = request.getRequestProperties();
		MultivaluedMap<String, String> pathParams  = props.getPathParams();
		MultivaluedMap<String, String> queryParams = props.getQueryParams();
		ArrayList<String> keys = FxWebUtil.getServiceKeyParams(queryParams);


		// Get system id from request header.
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		// See if this is a health check request.
		String uri = props.getURI();
		if (uri != null && uri.indexOf("healthcheck")!=0 ) {
			doHealthCheck(request, response, keys);
			return response;
		}


		return response;
	}




	/**
	 * 
	 * @param request
	 * @return
	 */
	private void doHealthCheck(ResourceRequestInf request, 
			ServiceResponseImpl response, 
			ArrayList<String> keys)   throws FxWebException {


		// See if billing and/or network status requested
		boolean getBillingStatus = false;
		boolean getNetworkStatus = false;

		for (int i=0; i<keys.size(); i++) {
			String s = keys.get(i);
			if (s.equalsIgnoreCase("BillingStatus")) getBillingStatus = true;
			if (s.equalsIgnoreCase("NetworkStatus")) getNetworkStatus = true;
		}

		RequestProperties properties = request.getRequestProperties();

		// Get the client id to be able to determine which provisioning systemto invoke.
		String clientId = properties.getClientId();
		if (clientId == null || clientId.length()<3) {
			throw new FxWebException("Expected client id of at least 3 characters. Received" + clientId, "");
		}

		MultivaluedMap<String, String> pathParams  = properties.getPathParams();
		String serviceId = FxWebUtil.getPathParamValue(pathParams, "serviceid");
		if (serviceId == null || serviceId.length()<10) {
			throw new FxWebException("Expected service id of at least 10 characters. Received" + serviceId, "");
		}


		// Do network status if requested
		String networkHcRequest = "";     
		String networkHcResult  = "<NetworkProvisionDetails/>"; 

		if (getNetworkStatus== true) {
			//if (NsgProvisioningUtils.isNsgClient(clientId)) {   
			//networkHcRequest = NsgProvisioningUtils.buildHealthCheckRequest(clientId, serviceId);
			//String nsgResult = NsgProvisioningUtils.invokeNsgServiceInquiry(clientId, networkHcRequest);

			// We will be using Provisioning Connector for the Service Inquiry
			HashMap input = new HashMap();
			input.put("TelephoneNumber",serviceId);
			input.put("AcctSegId", ClientCache.getAccountSegmentByClientId(properties.getClientId()));
			input.put("RequestName", Constants.REQUEST_SUBSCRIBER_INQUIRY);

			HashMap awnReponse = null;
			try {
				C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
				awnReponse = rq.awnInquiryRequest(input);
				String nsgResult = (String)awnReponse.get("ResponseXML");
				networkHcResult  = NsgProvisioningUtils.formatNsgHealthCheck(nsgResult);
				log.info("Response from NSG : " + nsgResult);
			} 
			catch (C30ProvConnectorException e) {
				networkHcResult=       "<NetworkProvisionDetails>" +
						"<Status>" +
						"<Message>"+e.getMessage()+"</Message>" +
						"<Code>1</Code>" +
						"</Status>" +
						"</NetworkProvisionDetails>";
			}
			//}
		} 



		// See if billing status request made.  If so, need to include billing status,
		// provisioning history 
		String provisioningHistory  = "";

		// default billing status result
		String billingStatusResult  = "";

		if (getBillingStatus== true) {  
			billingStatusResult = "<BillingStatus>" +
					"<Status>" +
					"<Message>SUCCESS</Message>" +
					"<Code>0</Code>" +
					"</Status>" +
					"</BillingStatus>";
			HashMap<String, HashMap> results = new SdkServiceStatus().billingStatusInquiry(request);        
			HashMap<String, Object> status = results.get("0"); // Actual results in parent Map
			billingStatusResult = formatServiceStatus(status);
		}

		// Check if provisioning history is being requested.
		String historyParam = FxWebUtil.getQueryParamValue(properties.getQueryParams(), "includeHistory");
		if (historyParam != null && historyParam.equalsIgnoreCase("yes")) {
			HashMap<String, HashMap> results = new SdkServiceStatus().provisioningStatusInquiry(request);        
			provisioningHistory = formatProvisioningDetails(results); 
		}

		// Default to OK global status.
		response.setResponseCode(Response.Status.OK);


		// Concatenate billing and network results, and set as response body.
		String healthCheckResult = FxWebConstants.XML_HEADER +  
				"<HealthCheckResponse>" +
				billingStatusResult  + 
				provisioningHistory  +
				networkHcResult      +
				"</HealthCheckResponse>";

		response.setResponseBody(healthCheckResult);

	}



	/**
	 * Format billing status from the service status results. Resultant
	 * format will be:
	 * 
	 * <BillingStatus>
	 *     <Service>
	 *       <HostServiceId>9072224444</HostServiceId>
	 *       <ServiceStatus>Active</ServiceStatus>
	 *       <ActiveDate>20120622</ActiveDate>
	 *       <InactiveDate></InactiveDate>
	 *       <IdentifierObjectList>
	 *         <IdentifierObject>
	 *           <IdentifierId>9071234567</IdentifierId>
	 *           <IdentifierName>PhoneNumber</IdentifierName>
	 *         </IdentifierObject>
	 *         <IdentifierObject>
	 *           <IdentifierId>311370000240032</IdentifierId>
	 *           <IdentifierName>IMSI</IdentifierName>
	 *         </IdentifierObject>
	 *       </IdentifierObjectList>
	 *       <ComponentObjectList>
	 *         <ComponentObject>
	 *           <ComponentId>111</ComponentId>
	 *           <ComponentName>Feature111</ComponentName>
	 *         </ComponentObject>
	 *         <ComponentObject>
	 *           <ComponentId>222</ComponentId>
	 *           <ComponentName>Feature222</ComponentName>
	 *         </ComponentObject>
	 *       </ComponentObjectList>
	 *       <PlanObjectList>
	 *          <PlanObject>
	 *            <PlanId>999</PlanId>
	 *            <PlanName>PackageAbc</PlanName>
	 *            <InstanceId>8887</InstanceId>
	 *          </PlanObject>
	 *       </PlanObjectList>
	 *     </Service>
	 *   </BillingStatus>
	 * 
	 * @param results
	 * @return
	 */
	private String formatServiceStatus(HashMap<String, ?> results) {

		StringBuffer xmlResult = new StringBuffer("<BillingStatus><Service>");

		// Short circuit the formatting if an error occurred.
		String excCode = (String)results.get("ExceptionCode");
		if (excCode != null) {
			String msg = (String)results.get("MsgDescr");
			String err = "Error: " + excCode + " Message: " + msg;
			xmlResult.append(err);
			xmlResult.append("</Service></BillingStatus>");
			return xmlResult.toString();
		}

		String hostId       = (String)results.get("HostServiceId");
		String statusId     = (String)results.get("StatusId");
		String statusDescr  = (String)results.get("StatusDescr");
		String activeDate   = (String)results.get("ActiveDate");
		String inactiveDate = (String)results.get("InactiveDate");

		// Get collection of identifiers, plans and components.
		ArrayList<IdentifierInstance> identifiers = (ArrayList)results.get("IdentifierCollection");
		ArrayList<CatalogInstance>    plans       = (ArrayList)results.get("PlanCollection");
		ArrayList<CatalogInstance>    components  = (ArrayList)results.get("ComponentCollection");

		// Clean up strings if 'null' literal
		if (hostId == null || hostId.equalsIgnoreCase("null")) {
			hostId = "";
		}
		if (inactiveDate == null || inactiveDate.equalsIgnoreCase("null")) {
			inactiveDate = "";
		}

		// Build XML result
		xmlResult.append("<HostServiceId>" + hostId       + "</HostServiceId>");
		xmlResult.append("<ServiceStatus>" + statusDescr  + "</ServiceStatus>");
		xmlResult.append("<ActiveDate>"    + activeDate   + "</ActiveDate>");
		xmlResult.append("<InactiveDate>"  + inactiveDate + "</InactiveDate>");

		// Spin through lists, building appropriate XML
		if (identifiers != null && identifiers.size()>0) {
			xmlResult.append("<IdentifierObjectList>");
			for (IdentifierInstance id : identifiers) {
				xmlResult.append("<IdentifierObject>");
				xmlResult.append("<IdentifierId>"   + id.getExternalId()  + "</IdentifierId>");
				xmlResult.append("<IdentifierName>" + id.getDescription() + "</IdentifierName>");
				xmlResult.append("</IdentifierObject>");
			}
			xmlResult.append("</IdentifierObjectList>");
		}

		if (plans != null && plans.size()>0) {
			xmlResult.append("<PlanObjectList>");
			for (CatalogInstance plan : plans) {
				xmlResult.append("<PlanObject>");
				xmlResult.append("<PlanId>"     + plan.getCatalogId()      + "</PlanId>");
				xmlResult.append("<PlanName>"   + plan.getDescription()    + "</PlanName>");
				xmlResult.append("<InstanceId>" + plan.getExternalInstId() + "</InstanceId>");
				xmlResult.append("</PlanObject>");
			}
			xmlResult.append("</PlanObjectList>");
		}

		if (components != null && components.size()>0) {
			xmlResult.append("<ComponentObjectList>");
			for (CatalogInstance component : components) {
				xmlResult.append("<ComponentObject>");
				xmlResult.append("<ComponentId>"   + component.getCatalogId()      + "</ComponentId>");
				xmlResult.append("<ComponentName>" + component.getDescription()    + "</ComponentName>");
				xmlResult.append("<InstanceId>"    + component.getExternalInstId() + "</InstanceId>");
				xmlResult.append("</ComponentObject>");
			}
			xmlResult.append("</ComponentObjectList>");
		}


		xmlResult.append("</Service></BillingStatus>"); 
		String xml = xmlResult.toString();

		return xml;
	}





	/**
	 * Format results of provisioning history query.  Basic format will be:
	 * 
	 *    <ProvisioningHistoryList>
	 *     <ProvisioningInstance>  1 per C30_PROV_CATALOG_TRANs row (multiple per TN) <----+
	 *                                                                                     |
	 *       <RequestId>  C30_PROV_TRANS_INST.REQUEST_ID                                   |
	 *                                                                                     |
	 *       <RequestPayload>  original <OrderRequest> payload from REQUEST_AUDIT          |
	 *                                                                                     |
	 *       <RequestReceivedDate>    REQUEST_AUDIT.MESSAGE_DATE                           |
	 *       <RequestCompletedDate>   RESPONSE_AUDIT.MESSAGE_DATE  (time of insert)        |
	 *                                                                                     |
	 *       <ProvisioningRequestList>                                                     ^
	 *         <ProvisioningRequest>       1 C30_PROV_TRANS_INST per each C30_PROV_CATALOG_TRANS 
	 *
	 *           <ProvisionRequestId>       C30_PROV_INST_TRANS.SUB_REQUEST_ID
	 *           <ProvisionRequestDate>     C30_PROV_INST_TRANS.REQUEST_START_DATE
	 *           <ProvisionCompletedDate>   C30_PROV_INST_TRANS.REQUEST_COMPLETED_DATE
	 *           <NetworkProvisionStatus>   Status description (join to C30_PROV_STATUS_DEF)
	 *                  
	 *           <NetworkProvisionPayload>  C30_PROV_INST_TRANS.REQUEST_XML
	 *           </NetworkProvisionPayload>
	 *                  
	 *         </ProvisioningRequest>
	 *       </ProvisioningRequestList>
	 *     </ProvisioningInstance>
	 *        </ProvisioningHistoryList>        
	 * 
	 * @param results
	 * @return
	 */
	private String formatProvisioningDetails(HashMap<String, ?> response) {

		StringBuffer xmlResult = new StringBuffer("<ProvisioningHistoryList>");

		// Each set provisioning instance results are in their own HashMap with
		// keys "0".."n"

		// check for error first.
		HashMap result = (HashMap)response.get("0");
		String excCode = (String)result.get("ExceptionCode");
		if (excCode != null) {
			String msg = (String)result.get("MsgDescr");
			String err = "Error: " + excCode + " Message: " + msg;
			xmlResult.append(err);
			xmlResult.append("</ProvisioningHistoryList>");
			return xmlResult.toString();
		}

		// Go through set of results, formatting each provisioning catalog instances
		// that exists for the service.
		for (int i=0; i<response.size(); i++) {

			String key = Integer.toString(i);
			HashMap provInstance = (HashMap)response.get(key);
			if (provInstance == null) {
				break;
			}

			xmlResult.append("<ProvisioningInstance>");

			xmlResult.append("<RequestId>"            + provInstance.get("RequestId")         + "</RequestId>");
			xmlResult.append("<RequestPayload>"       + provInstance.get("OriginalRequest")   + "</RequestPayload>");
			xmlResult.append("<RequestReceivedDate>"  + provInstance.get("OrderRequestDate")  + "</RequestReceivedDate>");
			xmlResult.append("<RequestCompletedDate>" + provInstance.get("OrderResponseDate") + "</RequestCompletedDate>");

			xmlResult.append("<ProvisioningRequestList>");

			// Iterate over provisioning instances that belong to this catalog instance.
			// The instances are each in a hash map keyed by the 'sub request' id.
			HashMap<String, HashMap> provRequests = (HashMap)provInstance.get("ProvRequests");
			if (provRequests != null) {
				Iterator it = provRequests.keySet().iterator();
				while (it.hasNext()) {
					String provKey = (String)it.next();
					HashMap<String, String> request = provRequests.get(provKey);
					xmlResult.append("<ProvisioningRequest>");

					xmlResult.append("<ProvisionRequestId>"      + request.get("SubRequestId")     + "</ProvisionRequestId>");
					xmlResult.append("<ProvisionRequestDate>"    + request.get("ProvRequestDate")  + "</ProvisionRequestDate>");
					xmlResult.append("<ProvisionCompletedDate>"  + request.get("ProvResponseDate") + "</ProvisionCompletedDate>");
					xmlResult.append("<NetworkProvisionStatus>"  + request.get("ProvStatus")       + "</NetworkProvisionStatus>");
					xmlResult.append("<NetworkProvisionPayload>" + request.get("ProvRequestXml")   + "</NetworkProvisionPayload>");

					xmlResult.append("</ProvisioningRequest>");
				}
			}

			xmlResult.append("</ProvisioningRequestList>");


			xmlResult.append("</ProvisioningInstance>");
		}


		xmlResult.append("</ProvisioningHistoryList>");

		String xml = xmlResult.toString();

		return xml;

	}

}
