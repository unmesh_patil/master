package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.payment.C30PaymentActionType;
import com.cycle30.fxweb.request.wrapper.payment.PaymentRequest;
import com.cycle30.fxweb.request.wrapper.payment.PaymentVersion20Object;
import com.cycle30.fxweb.response.ClientCallbackResponseImpl;
import com.cycle30.fxweb.response.PaymentResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkClientCallback;
import com.cycle30.fxweb.sdkinterface.SdkPaymentCreate;
import com.cycle30.fxweb.sdkinterface.SdkPaymentRegister;
import com.cycle30.fxweb.sdkinterface.SdkPaymentReversal;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;


/**
 * 
 * Force callback resource instance
 *
 */
public class ClientCallbackResourceImpl implements ResourceInf  {

	public static final Logger log = Logger.getLogger(ClientCallbackResourceImpl.class); 


	public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {


		// Get the original HTTP method from the 'http.method' header
		RequestProperties properties = request.getRequestProperties();
		String method = FxWebUtil.getRequestMethod(properties.getHeaders());
		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();


		if (method.equalsIgnoreCase("POST") ) {
			ResourceResponseInf response = postResource(request);
			return response;
		}

		return null; 
	}




	/**
	 * 
	 * @param request
	 * @return
	 */
	private ResourceResponseInf postResource(ResourceRequestInf request) throws FxWebException {

		ClientCallbackResponseImpl response = new ClientCallbackResponseImpl(); 
		// End-resultant map
		HashMap<String, HashMap> results = new HashMap();

		// Default to OK status
		response.setResponseCode(Response.Status.OK);

		// Always a multi-response type since multiple payments are always possible.
		response.setMultiResponseType(true);

		// Get/set system id from request header.
		RequestProperties props = request.getRequestProperties();
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		response.setResponseType(ResponseTypes.response.ClientCallbackResponse.toString());

		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();


		try
		{
			results = (HashMap<String, HashMap>) new SdkClientCallback().clientCallback(request,  doc);
		}
		catch (FxWebException iae) 
		{

			// Form generic error for ResponseFormatter purposes
			//String excCode = iae.getExceptionCode();
			String msg = iae.getMessage();

			HashMap excMap = new HashMap(); 
			excMap.put("ExceptionCode", iae.getMsgCode());  
			excMap.put("MsgText", msg);
			results.put("0", excMap);

		}



		// Format SDK response(s)
		String xml = formatClientcallbackResponse(response, results);

		response.setResponseBody(xml);

		return response;



	}




	/**
	 * Format ClientCallbackResponse .  The response is quite simplistic, requiring just 
	 * the correlation id from the original request, the tracking id from the 
	 * SDK response, and a status structure.  The end result is:
	 * 
	 *  <ClientCallbackResponse>
	 *          <Status>
	 *              <System>K2</system>
	 *              <Message>SUCCESS</message>
	 *                <Code>0</code>
	 *           </Status>
	 *   </ClientCallbackResponse>
	 *
	 * 
	 * @param response
	 * @param results
	 * @return
	 */
	private String formatClientcallbackResponse(ClientCallbackResponseImpl response, 
			HashMap<String, HashMap> results) 
					throws FxWebException {


		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<ClientCallbackResponse><Status>");

		boolean callbackOk = false;

		Response.Status errorStatus = Response.Status.OK;


		// Go through the Map of HashMaps, and generate a payment structure for each
		Iterator i = results.keySet().iterator();
		while (i.hasNext()) {

			String key  = (String)i.next();
			HashMap result = (HashMap)results.get(key);

			String exception  = (String)result.get("ExceptionCode");

			String code = "0";
			String message = "SUCCESS";
			
			// Get the 'client friendly' error message 
			if (exception != null) 
			{
				MappedError me = new MappedError(exception);
				message        = me.getExternalError();
				errorStatus    = me.getStatus();
				code           = me.getExceptionCode();
			
				// Get the original correlation id and account no
				message = (String)result.get("MsgText");
			}

			xmlResponse.append("<Message>" + message + "</Message>");
			xmlResponse.append("<Code>"    + code + "</Code>");
			xmlResponse.append("</Status>");

		}  // end while()

		xmlResponse.append("</ClientCallbackResponse>");

		// if all payments failed (didn't find one that was ok, set return 
		// status to error condition. 
		if (callbackOk == false) {
			response.setResponseCode(errorStatus);
		}

		return xmlResponse.toString();
	}



}
