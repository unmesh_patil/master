package com.cycle30.fxweb.resource;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.ResourceResponseInf;



public interface ResourceInf {

    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException;
    
}
