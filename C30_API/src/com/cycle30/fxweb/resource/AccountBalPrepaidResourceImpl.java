package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.cycle30.fxweb.cache.ClientOCSCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.PrepaidBalanceRequestImpl;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.awnprepaid.balance.Account;
import com.cycle30.fxweb.request.wrapper.awnprepaid.balance.BalanceObject;
import com.cycle30.fxweb.response.PrepaidBalanceAccountResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidBalanceCreateUpdate;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidBalanceFinder;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;

/**
 * OCS resource object
 */

public class AccountBalPrepaidResourceImpl implements ResourceInf {

	public static Logger log = Logger
			.getLogger(AccountBalPrepaidResourceImpl.class);
	
	// Handle the resource request, returning a response instance.
	public ResourceResponseInf handleRequest(ResourceRequestInf request)
			throws FxWebException {

		// Get the original HTTP method from the 'http.method' header
		RequestProperties properties = request.getRequestProperties();
		String method = FxWebUtil.getRequestMethod(properties.getHeaders());

		// Instantiate the response
		if (method.equalsIgnoreCase("GET")) {
			ResourceResponseInf response = getResource(request);
			return response;
		}

		if (method.equalsIgnoreCase("POST")) {
			ResourceResponseInf response;
			response = postResource(request);
			return response;
		}

		return null;
	}

	/**
	 * Process the HTTP GET request. This will be always a single-account 'find'
	 * search based on whether URI query params exist.
	 * 
	 * If an account number was found either on the URI path or as a query
	 * param, invoke the single-account 'finder' method. Otherwise, use the
	 * 'search' method.
	 * 
	 * @param request
	 * @return Resource response instance
	 */
	private ResourceResponseInf getResource(ResourceRequestInf request)
			throws FxWebException {

		PrepaidBalanceAccountResponseImpl response = new PrepaidBalanceAccountResponseImpl();
		// response.setResponseType(ResponseTypes.ACCOUNT_SEARCH_RESPONSE);
		response.setResponseType(ResponseTypes.response.AccountSearchResponse
				.toString());
		response.setResponseCode(Response.Status.OK);
		// get customer Subscriber number which is nothing but the accountNumber
		// search type, and set the response property appropriately.
		RequestProperties props = request.getRequestProperties();
		String subscriberNumber = FxWebUtil.getPathParamValue(request
				.getRequestProperties().getPathParams(), "subscriberNumber");
		String transactionId=props.getTransactionId();
		log.info("transactionId ="+transactionId+" subscriberNumber "+subscriberNumber);
		// Set Subscriber Details in the Cache Before calling SDK 
		 if(subscriberNumber !=null && ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId)==null){
         	ClientOCSCache.setSubscriberDetailsInCache(subscriberNumber,props.getTransactionId());
         }
         
		PrepaidBalanceRequestImpl prepaidAccountBalanceRequest = new PrepaidBalanceRequestImpl();
		prepaidAccountBalanceRequest=(PrepaidBalanceRequestImpl) request;
		prepaidAccountBalanceRequest.setSubscriberId(subscriberNumber);

		// Get system id from request header.
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		HashMap<String, ?> results = new HashMap();
		HashMap<Integer, HashMap<String, String>> bundleResult = null;

		try {

			results = new SdkPrepaidBalanceFinder()
					.getPrepaidBalance(prepaidAccountBalanceRequest);
			
	
			HashMap ResponseXML = (HashMap) results.get("0");
			bundleResult = new HashMap<Integer, HashMap<String, String>>();
			HashMap<String, String> balanceResultMap = null;
			
			if(ResponseXML !=null && ResponseXML.get("ExceptionCode")!= null){
				String exceptionCode= ResponseXML.get("ExceptionCode").toString();
				String exceptionMsg = ResponseXML.get("MsgText").toString();
				balanceResultMap = new HashMap<String, String>();
				balanceResultMap.put("ExceptionCode", exceptionCode);
				balanceResultMap.put("MsgText", exceptionMsg);
				bundleResult.put(0, balanceResultMap);
			}else{

			Document doc = DomUtils.stringToDom(ResponseXML.get("PrepaidBalanceXML").toString());
			doc.getDocumentElement().normalize();
			
			NodeList structList = doc.getElementsByTagName("struct");
			
			if(structList !=null ){
			for (int i = 0; i < structList.getLength(); i++) {

				NodeList memberList = ((Element) structList.item(i))
						.getElementsByTagName("member");
				balanceResultMap = new HashMap<String, String>();
				for (int j = 0; j < memberList.getLength(); j++) {

					String Name = (String) ((Element) memberList.item(j))
							.getElementsByTagName("name").item(0)
							.getChildNodes().item(0).getNodeValue();
					String Value = (String) ((Element) memberList.item(j))
							.getElementsByTagName("value").item(0)
							.getChildNodes().item(0).getTextContent();
					/*if ("SubscriberId".equalsIgnoreCase(Name)) {
						balanceResultMap.put(Name, Value);
					} else */if ("BalanceTypeLabel".equalsIgnoreCase(Name)) {
						balanceResultMap.put(Name, Value);
					} else if ("ExpirationDate".equalsIgnoreCase(Name)) {
						balanceResultMap.put(Name, Value);
					} else if ("Value".equalsIgnoreCase(Name)) {
						balanceResultMap.put(Name, Value);
					}
				}
				bundleResult.put(i, balanceResultMap);
			}
		}
			
	}	
		} catch (FxWebException we) {
			ClientOCSCache.clearSubscriberDetailsInCache(subscriberNumber);			
			throw we;
		}
		
		String xml = formatAccountBalPrepaidResponse(response, bundleResult,subscriberNumber,transactionId);
		response.setResponseBody(xml);
		ClientOCSCache.clearSubscriberDetailsInCache(subscriberNumber);
		return response;
	}
	/**
	 * Format Account Balance Prepaid Search response. The response is quite
	 * simplistic, requiring just the correlation id from the original request,
	 * the tracking id from the SDK response, and a status structure. The end
	 * result is:
	 * 
	 * <AccountSearchResponse> 
	 *     <Status> #Defines overall status
	 *         <Message>SUCCESS</message>
	 *         <Code>0</code> 
	 *      </Status>
	 *   <AccountList size="1">
	 *     <Account >
	 *       <BalanceList size="2">
	 *         <Balance>
	 *           <BalanceTypeLabel>GCI_COMMON</BalanceTypeLabel>
	 *            <BalanceUnits>
	 *           <Type>MONEY|| UNITS|| SUBSCRIPTION ||TIME || VOLUME </Type>
	 * 	      	 <Value>10000</Value> 
	 *            </BalanceUnits>
	 *            <ExpirationDate>2012-07-22T09:30:47.0Z</ExpirationDate>
	 *         </Balance>
	 *         <Balance>
	 *           <BalanceTypeLabel>GCI_COMMON</BalanceTypeLabel>
	 *            <BalanceUnits>
	 * 				<Type>MONEY|| UNITS|| SUBSCRIPTION ||TIME || VOLUME </Type>
	 * 				<Value>10000</Value> </BalanceUnits>
	 * 				<ExpirationDate>2012-07-22T09:30:47.0Z</ExpirationDate>
	 *          </Balance>
	 *          </BalanceList>
	 *       </Account>
	 *    </AccountList>
	 *  </AccountSearcResponse>
	 * 
	 * 
	 * 
	 * @param response
	 * @param bundleResult
	 * @return
	 */
	private String formatAccountBalPrepaidResponse(
			PrepaidBalanceAccountResponseImpl response,
			HashMap<Integer, HashMap<String, String>> bundleResult, String subscriberNumber,String transactionId)
			throws FxWebException {

		int totalCount = 1;

		// Default status to success
		String code = "0";
		String message = "SUCCESS";

		// The results will be in a separate HashMap with the key of "0".
		HashMap resultMap = bundleResult.get(0);

		// Default response = OK
		Response.Status errorStatus = Response.Status.OK;
		response.setResponseCode(errorStatus);

		// See if exception occurred. If so, get the 'client friendly' error
		// message
		String exceptionCode = (String) resultMap.get("ExceptionCode");
		if (exceptionCode != null) {
			totalCount = 0;
			StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
			xmlResponse.append("<AccountSearchResponse>");
			xmlResponse.append("<Status>");
			xmlResponse.append("<Message>" + (String)resultMap.get("MsgText") + "</Message>");
			xmlResponse.append("<Code>" + exceptionCode + "</Code>");
			xmlResponse.append("</Status>");
			xmlResponse.append("</AccountSearchResponse>");
			return xmlResponse.toString();
		}else{

		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<AccountSearchResponse>");
		xmlResponse.append("<Status>");
		xmlResponse.append("<Message>" + message + "</Message>");
		xmlResponse.append("<Code>" + code + "</Code>");
		xmlResponse.append("</Status>");

		boolean accountBalOk = true;

		// Go through the Map of HashMaps, and generate a Inventory structure
		// for each
		// Get the system id (eg. K1, K2) already set in the response
		//String system = response.getSystemId();
		String subProfileLabel="";
		String subStateLabel="";
		String balanceLabelType="";
				
			if (ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId) != null) {
				log.info("subscriber details present in cache");
				if (ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberProfileLabel") != null)subProfileLabel = (String) ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberProfileLabel");
				if (ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberStateLabel") != null)subStateLabel = (String) ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberStateLabel");
			} else {
				log.info("subscriber details not present in cache so again called set subscriber details In cache");
				ClientOCSCache.setSubscriberDetailsInCache(subscriberNumber,transactionId);
				if(ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId) != null){
				if (ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberProfileLabel") != null)subProfileLabel = (String) ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberProfileLabel");
				if (ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberStateLabel") != null)subStateLabel = (String) ClientOCSCache.getSubscriberDetails(subscriberNumber,transactionId).get("SubscriberStateLabel");
				}
			}


		// Generate all XML elements.

		xmlResponse.append("<AccountList size=\""+totalCount+"\">");
		xmlResponse.append("<Account>");
		xmlResponse.append("<SubscriberProfileLabel>" +subProfileLabel +"</SubscriberProfileLabel>");
		xmlResponse.append("<SubscriberStatus>" +subStateLabel +"</SubscriberStatus>");
	
		xmlResponse.append("<BalanceList size=\""+ bundleResult.size()+"\">");
		
		for (int i = 0; i < bundleResult.size(); i++) {
			resultMap = bundleResult.get(i);
			if(ClientOCSCache.getBalanceUnitsType().get(resultMap.get("BalanceTypeLabel")) !=null )balanceLabelType=(String)ClientOCSCache.getBalanceUnitsType().get(resultMap.get("BalanceTypeLabel"));
			xmlResponse.append("<Balance>");
			xmlResponse
					.append("<BalanceTypeLabel>"
							+ resultMap.get("BalanceTypeLabel")
							+ "</BalanceTypeLabel>");
			xmlResponse.append("<BalanceUnits>");
			xmlResponse
					.append("<Type>"+ balanceLabelType
							+"</Type>");
			xmlResponse.append("<Value>" + resultMap.get("Value") + "</Value>");
			xmlResponse.append("</BalanceUnits>");
			xmlResponse.append("<ExpirationDate>"
					+ resultMap.get("ExpirationDate") + "</ExpirationDate>");
			xmlResponse.append("</Balance>");
			balanceLabelType="";

		} // end while()

		xmlResponse
				.append("</BalanceList></Account></AccountList></AccountSearchResponse>");

		// if all Balance failed (didn't find one that was ok, set return
		// status to error condition.
		if (accountBalOk == false) {
			response.setResponseCode(errorStatus);
		}
		log.info(xmlResponse);
       
		return xmlResponse.toString();
		}
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	private ResourceResponseInf postResource(ResourceRequestInf request)
			throws FxWebException {

		PrepaidBalanceAccountResponseImpl response = new PrepaidBalanceAccountResponseImpl();
		// response.setResponseType(ResponseTypes.ACCOUNT_SEARCH_RESPONSE);
		response.setResponseType(ResponseTypes.response.AccountSearchResponse
				.toString());
		response.setResponseCode(Response.Status.OK);
		// get customer Subscriber number which is nothing but the accountNumber
		// search type, and set the response property appropriately.
		RequestProperties props = request.getRequestProperties();
		String subscriberNumber = FxWebUtil.getPathParamValue(props.getPathParams(), "subscriberNumber");
		
		int errorCount =0;
		
		// Set Subscriber Details in the Cache Before calling SDK 
		 if(subscriberNumber !=null && ClientOCSCache.getSubscriberDetails(subscriberNumber,props.getTransactionId())==null){
         	ClientOCSCache.setSubscriberDetailsInCache(subscriberNumber,props.getTransactionId());
         }
         
		
		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();

		// Unmarshall the DOM document into a list of Inventory objects.
		Account balanceList = getBalanceList(doc);

		
		// Go through the embedded Balance Object , and invoke SDK for each
		List<BalanceObject> l = balanceList.getBalanceList().getBalance();

		Iterator<BalanceObject> i = l.iterator();
		int balanceNo = 0;
		
		 HashMap<String, HashMap<String, String>> bundleResult= new HashMap<String, HashMap<String, String>>();
		while (i.hasNext()) {
			
			BalanceObject balance = i.next();
			String sequence=null;
			if(balance.getSequence()!= null){
				sequence= balance.getSequence().toString();
			}
		
			

			 HashMap<String, ?> results = new SdkPrepaidBalanceCreateUpdate().updatePrepaidBalance(request, balance);

			 HashMap ResponseXML = (HashMap) results.get("0");
			
			 if(ResponseXML.get("PrepaidBalanceXML")!=null){
			 String key = String.valueOf(balanceNo);
			 HashMap<String, String> balanceResultMap = new HashMap<String, String>();
			    		 balanceResultMap.put("0", "SUCCESS");
			    		 bundleResult.put(key, balanceResultMap);	 
			 }else if(ResponseXML !=null && ResponseXML.get("ExceptionCode")!= null){
				    errorCount++;
					String exceptionCode= ResponseXML.get("ExceptionCode").toString();
					String exceptionMsg = ResponseXML.get("MsgText").toString();
				    HashMap<String, String>	balanceResultMap = new HashMap<String, String>();
					balanceResultMap.put("ExceptionCode", exceptionCode);
					balanceResultMap.put("MsgText", exceptionMsg);
					balanceResultMap.put("Sequence",sequence );
					String key = String.valueOf(balanceNo);
					bundleResult.put(key, balanceResultMap);
				}
			balanceNo++;

		}

		String xml = formatAccountUpdateResponse(response, bundleResult,subscriberNumber,errorCount);
		ClientOCSCache.clearSubscriberDetailsInCache(subscriberNumber);
		response.setResponseBody(xml);

		return response;

	}

	/**
	 * 
	 * @param doc
	 * @return
	 * @throws FxWebException
	 */
	private Account getBalanceList(Document doc)
			throws FxWebException {

		// Unmarshall request into JavaBean wrappers
		Account balanceRequest = null;

		try {

			JAXBContext context = JAXBContext
					.newInstance(Account.class);
			Unmarshaller u = context.createUnmarshaller();

			balanceRequest = (Account) u.unmarshal(doc);

		} catch (Exception e) {
			log.error(e);
			throw new FxWebException(e,
					"Unable to unmarshall Account Balance Update DOM document");
		}

		return balanceRequest;

	}

	/**
	 * Format Balance Account Update response.  The end result is:
	 * 
	 * <AccountBalanceResponse> <Status> #Defines overall status
	 * <Message>SUCCESS</message> <Code>0</code> </Status>
	 * </AccountBalanceResponse>
	 * 
	 * 
	 * @param response
	 * @param bundleResult
	 * @param errorCount 
	 * @return
	 */
	private String formatAccountUpdateResponse(
			PrepaidBalanceAccountResponseImpl response,
			HashMap<String, HashMap<String, String>> bundleResult, String subscriberNumber, int errorCount) throws FxWebException {
		// Set status to 0=success, 1=failure
		String code = "0";
		String message = "SUCCESS";
		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
	//	StringBuffer xmlErrResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		boolean accountUpdateOk = true;
		
		xmlResponse.append("<AccountBalanceResponse>");
		
		if(errorCount > 0){
				accountUpdateOk=false;
			
			 xmlResponse.append("<Status>");
			 code="OCS-BALANCE-FAILED";
			 message="Account Balance Create/Update failed";
			 Response.Status errorStatus = Response.Status.OK;
				xmlResponse.append("<Message>" + message + "</Message>");
				xmlResponse.append("<Code>" + code + "</Code>");
				xmlResponse.append("</Status>");
				xmlResponse.append("<Account>");
				xmlResponse.append("<AccountNumber>" + subscriberNumber +"</AccountNumber>" );
				xmlResponse.append("<BalanceList size=\""+bundleResult.size()+"\">");
			for(int i=0;i< bundleResult.size();i++){
				HashMap<?, ?> resultMap = bundleResult.get(String.valueOf(i));
				if( resultMap.get("ExceptionCode")!=null){
					xmlResponse.append("<Balance sequence=\""+(i+1)+"\">");
					xmlResponse.append("<Status>");
					xmlResponse.append("<Message>" + (String)resultMap.get("MsgText") + "</Message>");
					xmlResponse.append("<Code>" + (String) resultMap.get("ExceptionCode") + "</Code>");
					xmlResponse.append("</Status>");
					xmlResponse.append("</Balance>");
					
				}else{
					code = "0";
					message = "SUCCESS";
					xmlResponse.append("<Balance sequence=\""+(i+1)+"\">");
					xmlResponse.append("<Status>");
					xmlResponse.append("<Message>" +message+ "</Message>");
					xmlResponse.append("<Code>"+code+"</Code>");
					xmlResponse.append("</Status>");
					xmlResponse.append("</Balance>");
				}
				
			}
			xmlResponse.append("</BalanceList>");
			xmlResponse.append("</Account>");
			response.setResponseCode(errorStatus);
		}else if(accountUpdateOk){
		    	xmlResponse.append("<Status>");
				xmlResponse.append("<Message>" + message + "</Message>");
				xmlResponse.append("<Code>" + code + "</Code>");
				xmlResponse.append("</Status>");
				
		}
			xmlResponse.append("</AccountBalanceResponse>");
	 
		return xmlResponse.toString();
	}

}