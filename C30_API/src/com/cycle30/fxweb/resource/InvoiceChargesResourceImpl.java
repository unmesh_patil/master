package com.cycle30.fxweb.resource;

import java.util.HashMap;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.InvoiceChargesResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkInvoiceChargesFinder;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;


/** 
 * Kenan resource object
 */

public class InvoiceChargesResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(InvoiceChargesResourceImpl.class);   
    
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }

        return null; 
    }
    
    

    /**
     * Process the HTTP GET request. 
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
        InvoiceChargesResponseImpl response = new InvoiceChargesResponseImpl(); 
        response.setResponseType(ResponseTypes.response.InvoiceChargesResponse.toString());
        

        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> pathParams  = props.getPathParams();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
       
        // Get system id from request header.
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
        HashMap results = new HashMap();
        
        String responseXml = "";
        
        
        try {
            results = new SdkInvoiceChargesFinder().invoiceChargesFind(request);
            responseXml = formatInvoiceChargesGetResponse(request, response, results);
        }
        catch (FxWebException we) {

            // Form generic error for ResponseFormatter purposes
            String error = we.getFormattedMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);
        }


        response.setResponseBody(responseXml);

        
        return response;
    }
    
   
    /**
     * Format InvoiceCharges search response. 
     * 
     *<InvoiceChargesResponse>
     *  <StatusList>
     *    <Status>
     *      <System>K2</System>
     *      <Message>SUCCESS</Message>
     *      <Code>0</Code>
     *    </Status>
     *  </StatusList>
     *  <InvoiceCharges>
     *    <System id="K2">
     *    <InvoiceNumber>3001000</InvoiceNumber>
     *    <TotalCount>2</TotalCount>
     *      <InvoiceCharge>
     *        <TrackingId>46</TrackingId>
     *        <ChargeType>1</ChargeType>
     *        <ChargeDescription>ScriptPro Plan</ChargeDescription>
     *        <TotalAmount>10</TotalAmount>
     *        <FederalAmount></FederalAmount>
     *        <StateAmount></StateAmount>
     *        <CountyAmount></CountyAmount>
     *        <CityAmount></CityAmount>
     *        <OtherAmount></OtherAmount>
     *        <Units>1</Units>
     *        <TransactionDate></TransactionDate>
     *        <FromDate>2011-08-01</FromDate>
     *        <ToDate>2011-09-01</ToDate>
     *      </InvoiceCharge>
     *      <InvoiceCharge>
     *        <TrackingId>47</TrackingId>
     *        <ChargeType>4</ChargeType>
     *        <ChargeDescription>US Sales Tax</ChargeDescription>
     *        <TotalAmount>.5</TotalAmount>
     *        <FederalAmount>.5</FederalAmount>
     *        <StateAmount>0</StateAmount>
     *        <CountyAmount>0</CountyAmount>
     *        <CityAmount>0</CityAmount>
     *        <OtherAmount>0</OtherAmount>
     *        <Units>0</Units>
     *        <TransactionDate>2011-08-01</TransactionDate>
     *        <FromDate>2011-08-01</FromDate>
     *        <ToDate>2011-09-01</ToDate>
     *      </InvoiceCharge>
     *    </System>
     *  </InvoiceCharges>
     *</InvoiceChargesResponse>
     *
     * 
     * @param response
     * @param results
     * @return
     */
    private String formatInvoiceChargesGetResponse(ResourceRequestInf request,
                                   InvoiceChargesResponseImpl response, 
                                   HashMap results)   throws FxWebException {
        
        RequestProperties props = request.getRequestProperties();
        String systemId = FxWebUtil.getKenanVersion(props.getHeaders());
        String invoiceNumber = (String) props.getPathParams().getFirst("invoiceNumber");
        
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        xmlResponse.append("<InvoiceChargesResponse>");
        
        // Default response = OK
        Response.Status errorStatus = Response.Status.OK;
        response.setResponseCode(errorStatus);
        
                
        // Build the <StatusList> structure
        xmlResponse.append("<StatusList>");        
        xmlResponse.append("<Status><System>" + systemId + "</System>");
        
        // Default status to success
        String code = "0";
        String message = "SUCCESS";
        
        // The results will be in a separate HashMap with the key of "0".            
        HashMap resultMap = (HashMap)results.get("0");

        // See if exception occurred. If so, get the 'client friendly' error message 
        String excCode = (String)resultMap.get("ExceptionCode");
        String exceptionMessage = (String)resultMap.get("MsgText");
        if (excCode != null) {
            // remove any double quote chars in exception code
            String exceptionCode = excCode.replaceAll("\"", "");
            MappedError me = new MappedError(exceptionCode,exceptionMessage);
            message        = me.getExternalError() + ";" + me.getAddlErrorText();
            errorStatus    = me.getStatus();
            code           = me.getExceptionCode();
            response.setResponseCode(errorStatus);
        } 
            
        xmlResponse.append("<Message>" + message + "</Message>");        
        xmlResponse.append("<Code>"    + code + "</Code>");
        xmlResponse.append("</Status>");
        
        // Closing status-list element
        xmlResponse.append("</StatusList>");

        // Create the K2 <Invoices> response
        xmlResponse.append("<InvoiceCharges>");
        xmlResponse.append("<System id=\"" + systemId + "\">");
        
        // Get/format TotalCount and Force idjust once (from initial result map from above)
        String total = (String)resultMap.get("TotalCount");
        if (total==null || total.equals("null")) {
            total="0";
        }
        xmlResponse.append("<InvoiceNumber>"    + invoiceNumber + "</InvoiceNumber>"); 
        xmlResponse.append("<TotalCount>" + total   + "</TotalCount>"); 

        
        // Build InvoiceCharge payload(s) if no exception occurred.
        if (excCode == null) {
            
            for (int i=0; i<results.size(); i++) {
                
               String key = String.valueOf(i);
               HashMap<String, String> result = (HashMap)results.get(key);
               
               String trackingId        = result.get("TrackingId");
               String chargeType        = result.get("ChargeType");    
               String chargeDescription = result.get("ChargeDescription"); 
               String totalAmount       = result.get("TotalAmount"); 
               String federalAmount     = result.get("FederalAmount"); 
               String stateAmount       = result.get("StateAmount"); 
               String countyAmount      = result.get("CountyAmount"); 
               String cityAmount        = result.get("CityAmount"); 
               String otherAmount       = result.get("OtherAmount"); 
               String units             = result.get("Units"); 
               String transactionDate   = result.get("TransactionDate"); 
               String fromDate          = result.get("FromDate"); 
               String toDate            = result.get("ToDate"); 
               
               // do some data cleanup
               if (totalAmount != null && totalAmount.equals("null"))   totalAmount = "";
               if (federalAmount != null && federalAmount.equals("null")) federalAmount = "";
               if (stateAmount != null && stateAmount.equals("null"))   stateAmount = "";
               if (countyAmount != null && countyAmount.equals("null")) countyAmount = "";
               if (cityAmount != null && cityAmount.equals("null"))   cityAmount = "";
               if (otherAmount != null && otherAmount.equals("null")) otherAmount = "";
               if (units != null && units.equals("null")) units = "";
               if (transactionDate != null && transactionDate.length()>10) transactionDate = transactionDate.substring(0, 10);
               if (fromDate != null && fromDate.length()>10) fromDate = fromDate.substring(0, 10);
               if (toDate != null && toDate.length()>10) toDate = toDate.substring(0, 10);
               if (transactionDate != null && transactionDate.equals("null"))   transactionDate = "";
               if (fromDate != null && fromDate.equals("null")) fromDate = "";
               if (toDate != null && toDate.equals("null")) toDate = "";
               
               
               // Format Invoice(s)
               xmlResponse.append("<InvoiceCharge>");
                   xmlResponse.append("<TrackingId>" + trackingId  + "</TrackingId>");
                   xmlResponse.append("<ChargeType>" + chargeType  + "</ChargeType>");
                   xmlResponse.append("<ChargeDescription>"    + chargeDescription     + "</ChargeDescription>");    
                   xmlResponse.append("<TotalAmount>"    + totalAmount     + "</TotalAmount>"); 
                   xmlResponse.append("<FederalAmount>"+ federalAmount + "</FederalAmount>"); 
                   xmlResponse.append("<StateAmount>"    + stateAmount     + "</StateAmount>"); 
                   xmlResponse.append("<CountyAmount>" + countyAmount  + "</CountyAmount>"); 
                   xmlResponse.append("<CityAmount>"    + cityAmount     + "</CityAmount>"); 
                   xmlResponse.append("<OtherAmount>"    + otherAmount     + "</OtherAmount>"); 
                   xmlResponse.append("<Units>" + units  + "</Units>"); 
                   xmlResponse.append("<TransactionDate>"+ transactionDate + "</TransactionDate>"); 
                   xmlResponse.append("<FromDate>"    + fromDate     + "</FromDate>"); 
                   xmlResponse.append("<ToDate>" + toDate  + "</ToDate>");
               xmlResponse.append("</InvoiceCharge>");
            }
        }
        
        xmlResponse.append("</System>");


        // payload end elements
        xmlResponse.append("</InvoiceCharges></InvoiceChargesResponse>");
        
        
        return xmlResponse.toString();
        
    }

}
