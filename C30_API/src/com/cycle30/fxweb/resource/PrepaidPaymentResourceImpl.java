package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.payment.C30PaymentActionType;
import com.cycle30.fxweb.request.wrapper.payment.PaymentRequest;
import com.cycle30.fxweb.request.wrapper.payment.PaymentVersion20Object;
//import com.cycle30.fxweb.request.wrapper.payment.C30PaymentActionType;
//import com.cycle30.fxweb.request.wrapper.payment.PaymentRequest;
//import com.cycle30.fxweb.request.wrapper.payment.PaymentVersion20Object;
import com.cycle30.fxweb.response.PrepaidPaymentResponseImpl;
//import com.cycle30.fxweb.response.PaymentResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkPaymentCreate;
import com.cycle30.fxweb.sdkinterface.SdkPaymentRegister;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidPayment;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;


/**
 * 
 * Prepaid Payment resource instance
 *
 */
public class PrepaidPaymentResourceImpl implements ResourceInf  {

	public static final Logger log = Logger.getLogger(PrepaidPaymentResourceImpl.class); 


	public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {


		// Get the original HTTP method from the 'http.method' header
		RequestProperties properties = request.getRequestProperties();
		String method = FxWebUtil.getRequestMethod(properties.getHeaders());
		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();

		if (method.equalsIgnoreCase("POST") ) {
        	log.info("POSTing Payment");
			ResourceResponseInf response = postResource(request);
			return response;
		}

		return null; 
	}


	/**
	 * 
	 * @param request
	 * @return
	 */
	private ResourceResponseInf postResource(ResourceRequestInf request) throws FxWebException {

		PrepaidPaymentResponseImpl response = new PrepaidPaymentResponseImpl(); 
		
		// Default to OK status
		response.setResponseCode(Response.Status.OK);

		// Always a multi-response type since multiple payments are always possible.
		response.setMultiResponseType(true);

		// Get/set system id from request header.
		RequestProperties props = request.getRequestProperties();
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		response.setResponseType(ResponseTypes.response.PrepaidPaymentResponse.toString());

		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();
	
		HashMap<String, HashMap> results = new HashMap();
	
		try
		{
			results = (HashMap<String, HashMap>) new SdkPrepaidPayment().logPrepaidPayment(request,  doc);
		}
		catch (FxWebException exc) 
		{

			// Form generic error for ResponseFormatter purposes
			//String excCode = iae.getExceptionCode();
			String msg = exc.getMessage();

			HashMap excMap = new HashMap(); 
			excMap.put("ExceptionCode", exc.getMsgCode());  
			excMap.put("MsgText", msg);
			results.put("0", excMap);

		}

		// Format SDK response(s)
		String xml = formatPrepaidPaymentResponse(response, results);

		response.setResponseBody(xml);

		return response;

	}


	/**
	 * Format Prepaid Payment Response .  The response is quite simplistic, requiring just 
	 * the correlation id from the original request, the tracking id from the 
	 * SDK response, and a status structure.  The end result is:
	 * 
	 *  <PrepaidPaymentResponse>
	 *          <Status>
	 *              <System>K2</system>
	 *              <Message>SUCCESS</message>
	 *                <Code>0</code>
	 *           </Status>
	 *   </PrepaidPaymentResponse>
	 *
	 * 
	 * @param response
	 * @param results
	 * @return
	 */
	private String formatPrepaidPaymentResponse(PrepaidPaymentResponseImpl response, 
			HashMap<String, HashMap> results) 
					throws FxWebException {
    	log.info("Send Payment Logging Response");

		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<PrepaidPaymentResponse><Status>");

		boolean callbackOk = false;

		Response.Status errorStatus = Response.Status.OK;


		// Go through the Map of HashMaps, and generate a payment structure for each
		Iterator i = results.keySet().iterator();
		while (i.hasNext()) {

			String key  = (String)i.next();
			HashMap result = (HashMap)results.get(key);

			String exception  = (String)result.get("ExceptionCode");
			//Changed the code parameter to Non-Standard response as the code expected is I0001 by IVR
			String code = "I0001";
			String message = "SUCCESS";
			
			// Get the 'client friendly' error message 
			if (exception != null) 
			{
				MappedError me = new MappedError(exception);
				message        = me.getExternalError();
				errorStatus    = me.getStatus();
				//Changed the code parameter to Non-Standard response as the code expected is E0001 by IVR for error scenarios
				code           = "E0001"; 
			
				// Get the original correlation id and account no
				message = (String)result.get("MsgText");
			}

			xmlResponse.append("<Message>" + message + "</Message>");
			xmlResponse.append("<Code>"    + code + "</Code>");
			xmlResponse.append("</Status>");

		}  // end while()

		xmlResponse.append("</PrepaidPaymentResponse>");

		// if all payments failed (didn't find one that was ok, set return 
		// status to error condition. 
		if (callbackOk == false) {
			response.setResponseCode(errorStatus);
		}

		return xmlResponse.toString();
	}

}
