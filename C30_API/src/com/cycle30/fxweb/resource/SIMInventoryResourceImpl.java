package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.InventoryResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkIMSISearch;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;

// SDK Objects
import com.cycle30.sdk.object.C30SDKAttributeConstants;



public class SIMInventoryResourceImpl implements ResourceInf {

	public static final Logger log = Logger.getLogger(SIMInventoryResourceImpl.class);
	@Override
	public ResourceResponseInf handleRequest(ResourceRequestInf request)
			throws FxWebException {

	
			// Get the original HTTP method from the 'http.method' header
			RequestProperties properties = request.getRequestProperties();
			String method = FxWebUtil.getRequestMethod(properties.getHeaders());

			if (method.equalsIgnoreCase("GET")) {
				ResourceResponseInf response;

				response = getResource(request);
				return response;

			}

			return null;
		}

		/**
		 * Process the HTTP GET request.
		 * 
		 * @param request
		 * @return Resource response instance
		 */
		private ResourceResponseInf getResource(ResourceRequestInf request)
				throws FxWebException {

			InventoryResponseImpl response = new InventoryResponseImpl();
			
			// Default to OK status
			response.setResponseCode(Response.Status.OK);
			response.setResponseType(ResponseTypes.response.InventoryResponse.toString());

			//Get the serial number from request
			String serialNumber = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(),"serialNumber");

			log.info("Request - serial Number : " + serialNumber);

			//Get system id from request header.
			RequestProperties props = request.getRequestProperties();
			
            String orgId = props.getClientId();
            String acctsegId = ClientCache.getSegmentationId(orgId);
            log.info("Request - orgId : "+orgId + "acctsegId : "+acctsegId); 
            if (acctsegId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "SIMInventory");
            }
            
			HashMap results = new HashMap();
            try { 
    				// Initiate IMSI search request
            		results = new SdkIMSISearch().getIMSI(request);
            }
            catch (FxWebException we) {

                String error = we.getFormattedMessage();
                HashMap excMap = new HashMap(); 
                excMap.put("ExceptionCode", "");  // placeholder
                excMap.put("MsgText", error);
                results.put("0", excMap);
            }

            // Format the response
			String formattedResponse = formatInventoryResponse(response, results);
			response.setResponseBody(formattedResponse);

			return response;
		}

	

/**
 * Format SIM Phone Inventory response. The response is quite simplistic, requiring just
 * the serial number from the original request, the network id from the
 * SDK response, and a status structure. The end result is:
 * 
 * <InventoryResponse>
 *     <Status> #Defines overall status
 *       <Message>SUCCESS</message>
 *       <Code>0</code>
 *     </Status>
 * 
 *   <InventoryList size="10"> 
 *        <Inventory> 
 *           <SerialNumber></SerialNumber>
 *           <NetworkId></ NetworkId >
 *        </Inventory>
 *   </InventoryList>
 * </InventoryResponse>
 * 
 * 
 * @param response
 * @param results
 * @return
 */
private String formatInventoryResponse(InventoryResponseImpl response, HashMap<String, HashMap> results) throws FxWebException {
		// Set status to 0=success as default
		String code = "0";
		String message = "SUCCESS";
		String ntwrkid = null;
		String serialNumber = null;
		String 	exceptionCode = null;
		
		// Default response = OK
        Response.Status errorStatus = Response.Status.OK;
        response.setResponseCode(errorStatus);
        
        // HashMap with the key of "0".            
        HashMap resultMap = (HashMap)results.get("0");

		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        StringBuffer ResponseBody = new StringBuffer();		
		xmlResponse.append("<InventoryResponse>");
		xmlResponse.append("<Status>");
		
        // See if exception occurred. If so, get the error message 
        exceptionCode = (String)resultMap.get("ExceptionCode");
    	
        if (exceptionCode != null) {
        	message = (String)resultMap.get("MsgText");
			xmlResponse.append("<Message>" + message + "</Message>");
			xmlResponse.append("<Code>" + exceptionCode + "</Code>");
			xmlResponse.append("</Status>");
			
			MappedError me = new MappedError(exceptionCode);
			errorStatus    = me.getStatus();

        } else {
        	
    		xmlResponse.append("<Message>" + message + "</Message>");
    		xmlResponse.append("<Code>" + code + "</Code>");
    		xmlResponse.append("</Status>");
        	
	    	// Go through the Map of HashMaps, and generate a Inventory structure for each inventory
	    	int listSize = results.size();
	    	
			Iterator i = results.keySet().iterator();
			xmlResponse.append("<InventoryList size=\"" + listSize + "\">");
			while (i.hasNext()) {
		
				String key = (String) i.next();
				HashMap result = (HashMap) results.get(key);
		
				// Get the Network id and Serial Number
				ntwrkid = null;
				serialNumber = null;
				serialNumber = result.get(C30SDKAttributeConstants.ATTRIB_SERIAL_NUMBER).toString();
				// Generate all XML elements.
				ResponseBody.append("<Inventory>");
				ResponseBody.append("<SerialNumber>" + serialNumber + "</SerialNumber>");				
				ntwrkid = result.get(C30SDKAttributeConstants.ATTRIB_NETWORK_ID).toString();
				ResponseBody.append("<NetworkId>" + ntwrkid + "</NetworkId>");				
				ResponseBody.append("</Inventory>");
	
			} // end while()
			
			// Build the <InventoryList> structure
	    	
	    	xmlResponse.append(ResponseBody);
	    	xmlResponse.append("</InventoryList>");			
        }
		
		xmlResponse.append("</InventoryResponse>");
	
		return xmlResponse.toString();
	}

}
