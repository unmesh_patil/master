package com.cycle30.fxweb.resource;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.inventory.InventoryRequest;
import com.cycle30.fxweb.request.wrapper.inventory.InventoryRequestObject;
import com.cycle30.fxweb.response.PhoneInventoryResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidInventorySearch;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidInventoryUpdate;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;

public class PhoneInventorySearchResourceImpl implements ResourceInf {

	public static final Logger log = Logger
			.getLogger(PhoneInventorySearchResourceImpl.class);

	public ResourceResponseInf handleRequest(ResourceRequestInf request)
			throws FxWebException {

		// Get the original HTTP method from the 'http.method' header
		RequestProperties properties = request.getRequestProperties();
		String method = FxWebUtil.getRequestMethod(properties.getHeaders());

		if (method.equalsIgnoreCase("GET")) {
			ResourceResponseInf response;

			response = getResource(request);
			return response;

		}
		if(method.equalsIgnoreCase("POST")) {
			ResourceResponseInf response;

			response = postResource(request);
			return response;
		}
		return null;
	}


	/**
	 * Process the HTTP GET request.
	 * 
	 * @param request
	 * @return Resource response instance
	 */
	private ResourceResponseInf getResource(ResourceRequestInf request)
			throws FxWebException {

		PhoneInventoryResponseImpl response = new PhoneInventoryResponseImpl();

		// Default to OK status
		response.setResponseCode(Response.Status.OK);
		response.setResponseType(ResponseTypes.response.InventoryResponse
				.toString());

		// Get system id from request header.
		RequestProperties props = request.getRequestProperties();
		HttpHeaders headers = props.getHeaders();
		response.setSystemId(FxWebUtil.getKenanVersion(headers));

		String orgId = props.getClientId();
		String acctsegId = ClientCache.getSegmentationId(orgId);
		log.info("Request - orgId : " + orgId + "acctsegId : " + acctsegId);
		if (acctsegId == null) {
			throw new FxWebException(
					"Could not retrieve segmentation id from cache for client id: "
							+ orgId, "PhoneInventory");
		}

		HashMap<String, ?> results = new HashMap<String, String>();
		HashMap<Integer, HashMap<String, String>> bundleResult =  new HashMap<Integer, HashMap<String, String>>();

		try {

			results = new SdkPrepaidInventorySearch()
			.getPhoneInventory(request);
			HashMap ResponseXML = (HashMap) results.get("0");
			bundleResult = new HashMap<Integer, HashMap<String, String>>();
			HashMap<String, String> balanceResultMap = null;

			if(ResponseXML !=null && ResponseXML.get("ExceptionCode")!= null){
				String exceptionCode= ResponseXML.get("ExceptionCode").toString();
				String exceptionMsg = ResponseXML.get("MsgText").toString();
				balanceResultMap = new HashMap<String, String>();
				balanceResultMap.put("ExceptionCode", exceptionCode);
				balanceResultMap.put("MsgText", exceptionMsg);
				bundleResult.put(0, balanceResultMap);
			}else if(ResponseXML !=null){
				log.info("Response XMl Prepaid Inventory" + ResponseXML);

				try {
					bundleResult = DomUtils.parseXMLForGCIRetreiveDetails(ResponseXML
							.get("InventoryResponseXML").toString());
				} catch (Exception e) {
					throw new FxWebException(e, e.getMessage());
				}

			}

		} catch (FxWebException we) {

			throw we;
			}

		String xml = formatInventoryResponse(response, bundleResult);
		response.setResponseBody(xml);

		return response;
	}
	
	/**
	 * Process the HTTP POST request.
	 * 
	 * @param request
	 * @return Resource response instance
	 */
	private ResourceResponseInf postResource(ResourceRequestInf request)
			throws FxWebException {

		PhoneInventoryResponseImpl response = new PhoneInventoryResponseImpl();

		// Default to OK status
		response.setResponseCode(Response.Status.OK);

		// Always a multi-response type since multiple payments are always
		// possible.
		response.setMultiResponseType(true);

		// Get/set system id from request header.
		RequestProperties props = request.getRequestProperties();
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		response.setResponseType(ResponseTypes.response.InventoryResponse
				.toString());
		
		String orgId = props.getClientId();
		String acctsegId = ClientCache.getSegmentationId(orgId);
		log.info("Request - orgId : " + orgId + "acctsegId : " + acctsegId);
		if (acctsegId == null) {
			throw new FxWebException(
					"Could not retrieve segmentation id from cache for client id: "
							+ orgId, "PhoneInventoryUpdate");
		}

		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();

		// Unmarshall the DOM document into a list of Inventory objects.
		InventoryRequest invr = getInventoryList(doc);

		// End-resultant map
		HashMap<String, ?> results = new HashMap<String, String>();
		HashMap<String, HashMap<String, String>> bundleResult= new HashMap<String, HashMap<String, String>>();

		// Go through the embedded Inventory, and invoke SDK for each
		List<InventoryRequestObject> l = invr.getInventoryList().getInventory();

		Iterator i = l.iterator();
		int inventoryNo = 0;
         int errorCount=0;
     	bundleResult = new HashMap<String, HashMap<String, String>>();
     	HashMap<String, String> inventoryResultMap = null;
		while (i.hasNext()) {

			InventoryRequestObject inventory = (InventoryRequestObject) i.next();
			  
			String payload = getInventoryRequesObjectMarshall(inventory);
			try {

			  results =  new SdkPrepaidInventoryUpdate().updatePhoneInventory(request,inventory, doc, payload);

			    HashMap<?, ?> ResponseXML = (HashMap<?, ?>) results.get("0");			
				
				
				String teleNo =""; 
				String ntwrkId ="";
				String invstatus="";
				String saleschanelId="";
				String operatorId=""; 
				 String key = String.valueOf(inventoryNo);

				if(ResponseXML !=null && ResponseXML.get("ExceptionCode")!= null){
					errorCount++;
					String exceptionCode= ResponseXML.get("ExceptionCode").toString();
					String exceptionMsg = ResponseXML.get("MsgText").toString();
					inventoryResultMap = new HashMap<String, String>();
					inventoryResultMap.put("ExceptionCode", exceptionCode);
					inventoryResultMap.put("MsgText", exceptionMsg);
					bundleResult.put(key, inventoryResultMap);
				}else {
					inventoryResultMap = new HashMap<String, String>();
					
					if(inventory.getTelephoneNumber() !=null)teleNo=inventory.getTelephoneNumber().toString();
					if(inventory.getNetworkId() !=null)ntwrkId=inventory.getNetworkId();
					if(inventory.getInvStatus() !=null)invstatus=inventory.getInvStatus();
					if(inventory.getSalesChannelId() !=null)saleschanelId=inventory.getSalesChannelId();
					if(inventory.getOperatorId() !=null)ntwrkId=inventory.getOperatorId();
					
					inventoryResultMap.put("NetworkId", ntwrkId);
					inventoryResultMap.put("TelephoneNumber", teleNo);
					inventoryResultMap.put("InvStatus",invstatus);
					inventoryResultMap.put("SalesChannelId", saleschanelId);
					inventoryResultMap.put("OperatorId",operatorId);
					
					bundleResult.put(key, inventoryResultMap);
						
					}
				inventoryNo++;

			} catch (FxWebException we) {

				throw we;
			}
		}
		log.info("Bundle Result Map " +bundleResult.toString());

		String xml = formatInventoryUpdateResponse(response, bundleResult,errorCount);
		response.setResponseBody(xml);

		return response;

	}

	private InventoryRequest getInventoryList(Document doc)
			throws FxWebException {

		// Unmarshall request into JavaBean wrappers
		InventoryRequest invr = null;

		try {

			JAXBContext context = JAXBContext
					.newInstance(InventoryRequest.class);
			Unmarshaller u = context.createUnmarshaller();

			invr = (InventoryRequest) u.unmarshal(doc);

		} catch (Exception e) {
			log.error(e);
			throw new FxWebException(e,
					"Unable to unmarshall inventory DOM document");
		}

		return invr;

	}
	
	
	private String getInventoryRequesObjectMarshall(InventoryRequestObject invReqObj)
			throws FxWebException {
		String marshalledString=null;
	
		try {
			 // Create a stringWriter to hold the XML
	        final StringWriter stringWriter = new StringWriter();

			
			JAXBContext context = JAXBContext
					.newInstance(InventoryRequestObject.class);
			Marshaller u = context.createMarshaller();

			u.marshal(invReqObj, stringWriter);
			marshalledString=stringWriter.toString();

		} catch (Exception e) {
			log.error(e);
			throw new FxWebException(e,
					"Unable to marshall InventoryRequestObject DOM document");
		}

		return marshalledString;

	}


	

	/**
	 * Format Phone Inventory response. The response is quite simplistic, requiring just
	 * the correlation id from the original request, the tracking id from the
	 * SDK response, and a status structure. The end result is:
	 * 
	 * <InventoryResponse>
	 *     <Status> #Defines overall status
	 *       <Message>SUCCESS</message>
	 *       <Code>0</code>
	 *     </Status>
	 * 
	 *   <InventoryList size="10"> 
	 *        <Inventory sequence=1> 
	 *           <TelephoneNumber></TelephoneNumber>
	 *           <InvStatus></InvStatus>
	 *           <CreateDate></CreateDate>
	 *           <NetworkId></ NetworkId >
	 *           <SalesChannelId></SalesChannelId>
	 *           <OperatorId></ OperatorId>
	 *        </Inventory>
	 *   </InventoryList>
	 * </InventoryResponse>
	 * 
	 * 
	 * @param response
	 * @param methodType 
	 * @param results
	 * @return
	 */
	private String formatInventoryResponse(PhoneInventoryResponseImpl response,
			HashMap<Integer, HashMap<String, String>> bundleResult)
					throws FxWebException {
		// Set status to 0=success, 1=failure
		String code = "0";
		String message = "SUCCESS";

		Response.Status errorStatus = Response.Status.OK;
		response.setResponseCode(errorStatus);

		// See if exception occurred. If so, get the 'client friendly' error
		// message
		HashMap resultMap = (HashMap) bundleResult.get(0);
		String exceptionCode=null;
		if(resultMap.get("ExceptionCode")!=null){
			exceptionCode = (String)resultMap.get("ExceptionCode") ;
		}

		if (exceptionCode != null) {
			message=(String)resultMap.get("MsgText");
			code=exceptionCode;
			StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
			xmlResponse.append("<InventoryResponse>");
			xmlResponse.append("<Status>");
			xmlResponse.append("<Message>" + message + "</Message>");
			xmlResponse.append("<Code>" + code + "</Code>");
			xmlResponse.append("</Status></InventoryResponse>");
			return xmlResponse.toString();
		}
		else{

			StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
			xmlResponse.append("<InventoryResponse>");
			xmlResponse.append("<Status>");
			xmlResponse.append("<Message>" + message + "</Message>");
			xmlResponse.append("<Code>" + code + "</Code>");
			xmlResponse.append("</Status>");

			boolean inventoryOk = true;


			String system = response.getSystemId();

			// Generate all XML elements.

			xmlResponse.append("<InventoryList size=\""+bundleResult.size()+"\">");
			for (int i = 0; i < bundleResult.size(); i++) {
				resultMap = (HashMap<?, ?>) bundleResult.get(i);
				//			xmlResponse.append("<Inventory sequence=\"" + (i+1) +"\">");
				xmlResponse.append("<Inventory>");
				xmlResponse.append("<TelephoneNumber>"
						+ resultMap.get("TelephoneNumber").toString()
						+ "</TelephoneNumber>");
				xmlResponse.append("<InvStatus>"
						+ resultMap.get("Status").toString() + "</InvStatus>");
				xmlResponse.append("<CreateDate>" +resultMap.get("CreateDate").toString()+"</CreateDate>");
				xmlResponse.append("<NetworkId>" +resultMap.get("NetworkId").toString()+"</NetworkId>");
				xmlResponse.append("<SalesChannelId>" +resultMap.get("SalesChannelId").toString()+"</SalesChannelId>");
				xmlResponse.append("<OperatorId>" +resultMap.get("OperatorId").toString()+"</OperatorId>");
				xmlResponse.append("<NetworkDeviceId>" +resultMap.get("NetworkDeviceId").toString()+"</NetworkDeviceId>");
				xmlResponse.append("</Inventory>");
			} // end while()

			xmlResponse.append("</InventoryList></InventoryResponse>");

			// if all inventory failed (didn't find one that was ok, set return
			// status to error condition.
			if (inventoryOk == false) {
				response.setResponseCode(errorStatus);
			}
			log.info(xmlResponse);

			return xmlResponse.toString();
		}
	}
	
	
	private String formatInventoryUpdateResponse(
			PhoneInventoryResponseImpl response,
			HashMap<String, HashMap<String, String>> bundleResult,  int errorCount) throws FxWebException {
		// Set status to 0=success, 1=failure
		String code = "0";
		String message = "SUCCESS";
		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
	//	StringBuffer xmlErrResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		boolean inventoryUpdateOk = true;
		
		xmlResponse.append("<InventoryResponse>");
		
		if(errorCount > 0){
			inventoryUpdateOk=false;
			
			 code="GCI-UPDATE-FAILED";
			 message="Inventory Update failed";
			 Response.Status errorStatus = Response.Status.OK; 
			 response.setResponseCode(errorStatus);
		}
		 		xmlResponse.append("<Status>");
				xmlResponse.append("<Message>" + message + "</Message>");
				xmlResponse.append("<Code>" + code + "</Code>");
				xmlResponse.append("</Status>");
				xmlResponse.append("<InventoryList size=\""+bundleResult.size()+"\">");
			for(int i=0;i< bundleResult.size();i++){
				HashMap<?, ?> resultMap = bundleResult.get(String.valueOf(i));
				if( resultMap.get("ExceptionCode")!=null){
					xmlResponse.append("<Inventory sequence=\""+(i+1)+"\">");
					xmlResponse.append("<Status>");
					xmlResponse.append("<Message>" + (String)resultMap.get("MsgText") + "</Message>");
					xmlResponse.append("<Code>" + (String) resultMap.get("ExceptionCode") + "</Code>");
					xmlResponse.append("</Status>");
					xmlResponse.append("</Inventory>");
					
				}else{
					code = "0";
					message = "SUCCESS";
					xmlResponse.append("<Inventory sequence=\""+(i+1)+"\">");
					xmlResponse.append("<Status>");
					xmlResponse.append("<Message>" +message+ "</Message>");
					xmlResponse.append("<Code>"+code+"</Code>");
					xmlResponse.append("</Status>");
					xmlResponse.append("<TelephoneNumber>"
							+ resultMap.get("TelephoneNumber").toString()
							+ "</TelephoneNumber>");
					xmlResponse.append("<InvStatus>"
							+ resultMap.get("InvStatus").toString() + "</InvStatus>");
					xmlResponse.append("<NetworkId>" +resultMap.get("NetworkId").toString()+"</NetworkId>");
					xmlResponse.append("<SalesChannelId>" +resultMap.get("SalesChannelId").toString()+"</SalesChannelId>");
					xmlResponse.append("<OperatorId>" +resultMap.get("OperatorId").toString()+"</OperatorId>");
					xmlResponse.append("</Inventory>");
				}
				
			}
			xmlResponse.append("</InventoryList>");
			xmlResponse.append("</InventoryResponse>");
		
		return xmlResponse.toString();
	}
	
}