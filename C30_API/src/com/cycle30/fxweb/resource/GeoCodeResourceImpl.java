package com.cycle30.fxweb.resource;

import java.util.HashMap;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.cache.ClientAPIVersionCache;
import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.GeoCodeResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkGeoCodeFinder;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;


/** 
 * Kenan resource object
 */

public class GeoCodeResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(GeoCodeResourceImpl.class);   
    
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("PUT")) {
            ResourceResponseInf response = putResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("POST")) {
            ResourceResponseInf response = postResource(request);
            return response;
        }
        
        return null; 
    }
    
    

    /**
     * Process the HTTP GET request. 
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
        GeoCodeResponseImpl response = new GeoCodeResponseImpl(); 

        response.setResponseType(ResponseTypes.response.GeoCodeSearchResponse.toString());
        
        // Attempt to get customer number to see if this is a single or multi account 
        // search type, and set the response property appropriately.
        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> pathParams  = props.getPathParams();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
        String acctNo = FxWebUtil.getCustomerNumber(pathParams, queryParams, null);
 
        // Get system id from request header.
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
        HashMap results = new HashMap();
        
        try { 
            
            results = new SdkGeoCodeFinder().geoCodeFind(request);         
            
        }

        catch (FxWebException we) {

            String error = we.getFormattedMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);
        }

        String result = formatGeoCodeFindResponse(request, response, results);
        response.setResponseBody(result);
 
        return response;
    }
    
    
    
    /**
     * Handle PUT request
     * 
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf putResource(ResourceRequestInf request) {
        GeoCodeResponseImpl response = new GeoCodeResponseImpl(); 
        response.setResponseBody("account put ok");
        response.setResponseCode(Response.Status.OK);
        
        return response;
    }
    
    
    
    /**
     * Handle POST request
     * 
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf postResource(ResourceRequestInf request) {
        GeoCodeResponseImpl response = new GeoCodeResponseImpl(); 
        response.setResponseBody("account post ok");
        response.setResponseCode(Response.Status.OK);
        
        return response;
    }
    
    

    /**
     * Format GeoCode find response. 
     * 
     * <GeoCodeSearchResponse>
     * 
     *   <StatusList>
     *     <Status>
     *       <System>K1</System>
     *       <Message></Message>
     *       <Code></Code>
     *     </Status>
     *   </StatusList>
     *   
     *   <GeoCodes>   
     *   
     *     <System id=K1>
     *       <TotalCount></TotalCount>
     *       
     *       <GeoCode>
     *         . . .
     *       </GeoCode>
     *       
     *       <GeoCode>
     *          <County>ANCHORAGE BOROUGH</County>
     *          <GeoCode>840001020200010</GeoCode>
     *       </GeoCode>    
     *        *       
     *     </System>
     *     
     *     
     *   </GeoCodes> 
     *       
     * </GeoCodeSearchResponse>
     *
     * 
     * @param response
     * @param results
     * @return
     */
    private String formatGeoCodeFindResponse(ResourceRequestInf request,
                                              GeoCodeResponseImpl response, 
                                              HashMap<String, ?> results)   throws FxWebException {
        
        int totalCount = 1;
        String	sdkAPIVersion = "2.0";
        String 	exceptionCode = null;
    	String 	county  = null;
    	String 	geoCode = null;
        
        
        RequestProperties props = request.getRequestProperties();
        String systemId = FxWebUtil.getKenanVersion(props.getHeaders());
        sdkAPIVersion = props.getSdkApiVersion();
        log.info("sdkAPIVersion : "+sdkAPIVersion);
        
             
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        StringBuffer ResponseBody = new StringBuffer();
        xmlResponse.append("<GeoCodeSearchResponse><Status>");        
        
        if (sdkAPIVersion == null || sdkAPIVersion.equalsIgnoreCase("1.0")) {
	        xmlResponse.append("<Status><System>" + systemId + "</System>");            
        }
        
        // Default status to success
        String code = "0";
        String message = "SUCCESS";
        
        // Default response = OK
        Response.Status errorStatus = Response.Status.OK;
        response.setResponseCode(errorStatus);

        // HashMap with the key of "0".            
        HashMap resultMap = (HashMap)results.get("0");
        
        // See if exception occurred. If so, get the 'client friendly' error message 
        exceptionCode = (String)resultMap.get("ExceptionCode");
        String exceptionMessage = (String)resultMap.get("MsgText");
    	log.info("exceptionCode : " +exceptionCode);
    	
        if (exceptionCode != null) {
        	MappedError me = new MappedError(exceptionCode,exceptionMessage);
            message        = me.getExternalError();
            errorStatus    = me.getStatus();
            code           = me.getExceptionCode();
            response.setResponseCode(errorStatus);
            
	        xmlResponse.append("<Message>" + message + "</Message>");        
	        xmlResponse.append("<Code>"    + code + "</Code>");
	        xmlResponse.append("</Status>");
	        
	        if (sdkAPIVersion == null || sdkAPIVersion.equalsIgnoreCase("1.0")) {

		        // Create the K2 <GeoCodes> response
		        xmlResponse.append("<GeoCodes>");
		        xmlResponse.append("<TotalCount>0</TotalCount>");	        	
		        xmlResponse.append("</GeoCodes>");
		        
	        } else if (sdkAPIVersion.equalsIgnoreCase("2.0")) {
		        xmlResponse.append("<GeoCodeList size=\"0\">");
		        xmlResponse.append("<GeoCode></GeoCode></GeoCodeList>");
	        }
	        	
        } else {
        	
            // Get the SDK api version from output
        	sdkAPIVersion = resultMap.get(FxWebConstants.C30_SDK_API_VERSION).toString();
            log.info("sdkapiVersion : "+sdkAPIVersion);
            
            if (sdkAPIVersion == null || sdkAPIVersion.equalsIgnoreCase("1.0")) {
            	
            	log.info("Build the <StatusList> structure");
    	        // Build the <StatusList> structure
    	        xmlResponse.append("<StatusList>");
    	        xmlResponse.append("<Status><System>" + systemId + "</System>");
    	        
	            // If nothing returned, set values accordingly so nulls
	            // won't be in XML response.
    	        county  = (String)resultMap.get("County");
    	        geoCode = (String)resultMap.get("GeoCode");
	            if (geoCode== null || geoCode.equals("null")) {
	                geoCode = "";
	                county  = "";
	                totalCount = 0;
	            }
	            
	            // Default to one since only supporting single Geocode
	            xmlResponse.append("<TotalCount>" + totalCount + "</TotalCount>"); 
	            
	            // Format GeoCode(s)
	            xmlResponse.append("<GeoCode>");
	                    
	                xmlResponse.append("<County>"  + county  + "</County>");
	                xmlResponse.append("<GeoCode>" + geoCode + "</GeoCode>");
	                
	            xmlResponse.append("</GeoCode>");
		        xmlResponse.append("</System></GeoCodes>");
            	
            	
            } else if (sdkAPIVersion.equalsIgnoreCase("2.0")) {
    	    	// Go through the Map of HashMaps, and generate a Geocode structure for each result
    	    	int total = results.size();
    	    	log.info("total : " +total);

    	    	xmlResponse.append("<Message>" + message + "</Message>");        
    	        xmlResponse.append("<Code>"    + code + "</Code>");
    	        xmlResponse.append("</Status>");
    	    	
    	    	for (int i=0; i<results.size(); i++) {
                    
                    String key = String.valueOf(i);
                    HashMap<String, ?> result = (HashMap)results.get(key);
                    
    				// Get the other result values
    	            ResponseBody.append("<GeoCode>");

                    county = (String)result.get("County");
                    geoCode  = (String)result.get("GeoCode");
                    ResponseBody.append("<County>" + county + "</County>");
                    ResponseBody.append("<GeoCode>" + geoCode + "</GeoCode>");
                    
    	            ResponseBody.append("</GeoCode>");
                }
                
    	        // Build the <GeoCodeList> structure
    	    	xmlResponse.append("<GeoCodeList size=\"" + total + "\">");
    	    	xmlResponse.append(ResponseBody);
    	    	xmlResponse.append("</GeoCodeList>");
            	
            } 
        	
        }
    
        // response end element
        xmlResponse.append("</GeoCodeSearchResponse>");
        
        return xmlResponse.toString();
        
    }
    
}
