package com.cycle30.fxweb.resource;

import java.util.HashMap;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.InvoiceResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkInvoiceFinder;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;


/** 
 * Kenan resource object
 */

public class InvoiceResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(InvoiceResourceImpl.class);   
    
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("PUT")) {
            ResourceResponseInf response = putResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("POST")) {
            ResourceResponseInf response = postResource(request);
            return response;
        }
        
        return null; 
    }
    
    

    /**
     * Process the HTTP GET request. 
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
        InvoiceResponseImpl response = new InvoiceResponseImpl(); 
        response.setResponseType(ResponseTypes.response.AccountSearchResponse.toString());
        

        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> pathParams  = props.getPathParams();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
        String acctNo = FxWebUtil.getCustomerNumber(pathParams, queryParams, null);
        
        
        // Get system id from request header.
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
        HashMap results = new HashMap();
        
        // Check if request is for simple invoice information, or invoice(s)
        // based on a prior payment distribution id.
        String distributionId = FxWebUtil.getQueryParamValue(queryParams, "distributionId");
        
        String responseXml = "";
        
        try { 
            if (distributionId == null || distributionId.length()==0) {
                results = new SdkInvoiceFinder().invoiceFind(request);
                responseXml = formatInvoiceSearchResponse(request, response, results);
            }
            else {
                results = new SdkInvoiceFinder().getInvoicesForPayment(request, distributionId);
                responseXml = formatInvoiceDistrResponse(request, response, results, distributionId);
            }
        }
        
        catch (FxWebException we) {

            // Form generic error for ResponseFormatter purposes
            String error = we.getFormattedMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);
        }


        response.setResponseBody(responseXml);

        
        return response;
    }
    
    
    /**
     * Handle PUT request
     * 
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf putResource(ResourceRequestInf request) {
        InvoiceResponseImpl response = new InvoiceResponseImpl(); 
        response.setResponseBody("invoice put ok");
        response.setResponseCode(Response.Status.OK);
        
        return response;
    }
    
    
    /**
     * Handle POST request
     * 
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf postResource(ResourceRequestInf request) {
        InvoiceResponseImpl response = new InvoiceResponseImpl(); 
        response.setResponseBody("invoice post ok");
        response.setResponseCode(Response.Status.OK);
        
        return response;
    }

    /**
     * Format Invoice search response. 
     * 
     *<InvoiceSearchResponse>
     *
     *  <StatusList>
     *    <Status>
     *      <System>K2</System>
     *      <Message>SUCCESS</Message>
     *      <Code>0</Code>
     *    </Status>
     *  </StatusList>
     *  
     *  <Invoices>
     *    <System id="K2">
     *      <ClientId>12345</ClientId>
     *      <TotalCount>10</TotalCount>
     *      
     *      <Invoice>
     *        <InvoiceNumber>12345</InvoiceNumber>
     *        <StatementDate>2012-01-02</StatementDate>
     *        <NewCharges>25.95</NewCharges>
     *        <BalanceDue>123.75</BalanceDue>
     *        <PaymentDueDate>2012-02-01</PaymentDueDate>
     *        <ClosedDate></ClosedDate>
     *        <DisputeAmount></DisputeAmount>
     *      </Invoice>
     *      
     *      <Invoice>
     *         ...
     *      </Invoice>
     *      
     *    </System>
     *  </Invoices>
     *</InvoiceSearchResponse>

     *
     * 
     * @param response
     * @param results
     * @return
     */
    private String formatInvoiceSearchResponse(ResourceRequestInf request,
                                   InvoiceResponseImpl response, 
                                   HashMap results)   throws FxWebException {
        
        RequestProperties props = request.getRequestProperties();
        String systemId = FxWebUtil.getKenanVersion(props.getHeaders());
        String clientId  = props.getAccountNo();
        
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        xmlResponse.append("<InvoiceSearchResponse>");
        
        // Default response = OK
        Response.Status errorStatus = Response.Status.OK;
        response.setResponseCode(errorStatus);
        
                
        // Build the <StatusList> structure
        xmlResponse.append("<StatusList>");        
        xmlResponse.append("<Status><System>" + systemId + "</System>");
        
        
        //Generic Error message and Text.
        
        // Default status to success
        String code = "0";
        String message = "SUCCESS";
        
        // The results will be in a separate HashMap with the key of "0".            
        HashMap resultMap = (HashMap)results.get("0");

        
        // See if exception occurred. If so, get the 'client friendly' error message 
        String excCode = (String)resultMap.get("ExceptionCode");
        String exceptionMessage = (String)resultMap.get("MsgText");
        if (excCode != null) {
            // remove any double quote chars in exception code
            String exceptionCode = excCode.replaceAll("\"", "");
            MappedError me = new MappedError(exceptionCode,exceptionMessage);
            message        = me.getExternalError() + ";" + me.getAddlErrorText();
            errorStatus    = me.getStatus();
            code           = me.getExceptionCode();
            response.setResponseCode(errorStatus);
        } 
            
        xmlResponse.append("<Message>" + message + "</Message>");        
        xmlResponse.append("<Code>"    + code + "</Code>");

        xmlResponse.append("</Status>");
        
        // Closing status-list element
        xmlResponse.append("</StatusList>");

        // Create the K2 <Invoices> response
        xmlResponse.append("<Invoices>");
        xmlResponse.append("<System id=\"" + systemId + "\">");
        
        // Get/format TotalCount and client it just once (from initial result map from above)
        String total = (String)resultMap.get("TotalCount");
        if (total==null || total.equals("null")) {
            total="0";
        }
        xmlResponse.append("<ClientId>"   + clientId + "</ClientId>"); 
        xmlResponse.append("<TotalCount>" + total    + "</TotalCount>"); 

        
        // Build Invoice payload(s) if no exception occurred.
        if (excCode == null) {
            
            for (int i=0; i<results.size(); i++) {
                
               String key = String.valueOf(i);
               HashMap<String, String> result = (HashMap)results.get(key);
               
               String invoiceNumber  = result.get("InvoiceNumber");
               String statementDate  = result.get("StatementDate");    
               String newCharges     = result.get("NewCharges"); 
               String balanceDue     = result.get("BalanceDue"); 
               String paymentDueDate = result.get("PaymentDueDate"); 
               String closedDate     = result.get("ClosedDate"); 
               String disputeAmount  = result.get("DisputeAmount"); 
               
               // do some data cleanup
               if (closedDate != null && closedDate.equals("null"))   closedDate = "";
               if (disputeAmount != null && disputeAmount.equals("null")) disputeAmount = "";
               if (statementDate != null && statementDate.length()>10) statementDate = statementDate.substring(0, 10);
               if (paymentDueDate != null && paymentDueDate.length()>10) paymentDueDate = paymentDueDate.substring(0, 10);
               if (closedDate != null && closedDate.length()>10) closedDate = closedDate.substring(0, 10);
               
               // Format Invoice(s)
               xmlResponse.append("<Invoice>");
                   xmlResponse.append("<InvoiceNumber>" + invoiceNumber  + "</InvoiceNumber>");
                   xmlResponse.append("<StatementDate>" + statementDate  + "</StatementDate>");
                   xmlResponse.append("<NewCharges>"    + newCharges     + "</NewCharges>");    
                   xmlResponse.append("<BalanceDue>"    + balanceDue     + "</BalanceDue>"); 
                   xmlResponse.append("<PaymentDueDate>"+ paymentDueDate + "</PaymentDueDate>"); 
                   xmlResponse.append("<ClosedDate>"    + closedDate     + "</ClosedDate>"); 
                   xmlResponse.append("<DisputeAmount>" + disputeAmount  + "</DisputeAmount>"); 
               xmlResponse.append("</Invoice>");
            }
        }
        
        xmlResponse.append("</System>");


        // payload end elements
        xmlResponse.append("</Invoices></InvoiceSearchResponse>");
        
        
        return xmlResponse.toString();
        
    }

    
    /**
     * Format Invoice search by payment distribution response. 
     * 
     *<InvoiceDistributionResponse>
     *
     *  <StatusList>
     *    <Status>
     *      <System>K2</System>
     *      <Message>SUCCESS</Message>
     *      <Code>0</Code>
     *    </Status>
     *  </StatusList>
     *  
     *  <Invoices>
     *  
     *    <System id="K2">
     *      <ClientId>12345</ClientId>
     *      <DistributionId>5678</DistributionId>
     *      <TotalCount>10</TotalCount>
     *      
     *      <Invoice>
     *        <InvoiceNumber>12345</InvoiceNumber>
     *        <StatementDate>2012-01-02</StatementDate>
     *        <NewCharges>25.95</NewCharges>
     *        <BalanceDue>123.75</BalanceDue>
     *        <PaymentDueDate>2012-02-01</PaymentDueDate>
     *        <ClosedDate></ClosedDate>
     *        <DisputeAmount></DisputeAmount>
     *      </Invoice>
     *      
     *      <Invoice>
     *         ...
     *      </Invoice>
     *      
     *    </System>
     *  </Invoices>
     *</InvoiceDistributionResponse>
     *
     * 
     * @param response
     * @param results
     * @return
     */
    private String formatInvoiceDistrResponse(ResourceRequestInf request,
                                   InvoiceResponseImpl response, 
                                   HashMap results,
                                   String distributionId)   throws FxWebException {
        
        RequestProperties props = request.getRequestProperties();
        String systemId = FxWebUtil.getKenanVersion(props.getHeaders());
        String clientId = props.getAccountNo();
        
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        xmlResponse.append("<InvoiceDistributionResponse>");
        
        // Default response = OK
        Response.Status errorStatus = Response.Status.OK;
        response.setResponseCode(errorStatus);
        
                
        // Build the <StatusList> structure
        xmlResponse.append("<StatusList>");        
        xmlResponse.append("<Status><System>" + systemId + "</System>");
        
        // Default status to success
        String code = "0";
        String message = "SUCCESS";
        
        // The results will be in a separate HashMap with the key of "0".            
        HashMap resultMap = (HashMap)results.get("0");

        // See if exception occurred. If so, get the 'client friendly' error message 
        String excCode = (String)resultMap.get("ExceptionCode");
        String exceptionMessage = (String)resultMap.get("MsgText");
        if (excCode != null) {
            // remove any double quote chars in exception code
            String exceptionCode = excCode.replaceAll("\"", "");
            MappedError me = new MappedError(exceptionCode,exceptionMessage);
            message        = me.getExternalError() + ";" + me.getAddlErrorText();
           
            errorStatus    = me.getStatus();
            code           = me.getExceptionCode();
            response.setResponseCode(errorStatus);
        } 
            
        xmlResponse.append("<Message>" + message + "</Message>");        
        xmlResponse.append("<Code>"    + code + "</Code>");
        xmlResponse.append("</Status>");
        
        // Closing status-list element
        xmlResponse.append("</StatusList>");

        // Create the K2 <Invoices> response
        xmlResponse.append("<Invoices>");
        xmlResponse.append("<System id=\"" + systemId + "\">");
        
        // Get/format TotalCount and client id just once (from initial result map from above)
        String total = (String)resultMap.get("TotalCount");
        if (total==null || total.equals("null")) {
            total="0";
        }
        xmlResponse.append("<ClientId>"       + clientId       + "</ClientId>"); 
        xmlResponse.append("<DistributionId>" + distributionId + "</DistributionId>");
        //xmlResponse.append("<TotalCount>"     + total   + "</TotalCount>"); 
        
    // TODO - replace hardwired results with data from requirements.
    xmlResponse.append("<TotalCount>" + "1"   + "</TotalCount>"); 
    xmlResponse.append("<Invoice>");
    xmlResponse.append("<InvoiceNumber>12345</InvoiceNumber>");
    xmlResponse.append("<StatementDate>2012-01-02</StatementDate>");
    xmlResponse.append("<NewCharges>25.95</NewCharges>");
    xmlResponse.append("<BalanceDue>123.75</BalanceDue>");
    xmlResponse.append("<PaymentDueDate>2012-02-01</PaymentDueDate>");
    xmlResponse.append("<ClosedDate></ClosedDate>");
    xmlResponse.append("<DisputeAmount></DisputeAmount>");
    xmlResponse.append("</Invoice>");
    xmlResponse.append("</System>");


        // payload end elements
        xmlResponse.append("</Invoices></InvoiceDistributionResponse>");
        
        
        return xmlResponse.toString();

        
    }
}
