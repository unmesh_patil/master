package com.cycle30.fxweb.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.HealthCheckResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkGeoCodeFinder;
import com.cycle30.fxweb.sdkinterface.SdkPaymentCreate;
import com.cycle30.fxweb.sdkinterface.SdkPaymentReversal;
import com.cycle30.fxweb.service.ResourceController;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;



/**
 *
 * Health Check resource instance
 *
 */
public class HealthCheckResourceImpl implements ResourceInf  {

    public static final Logger log = Logger.getLogger(HealthCheckResourceImpl.class);


    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {


        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());


        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = postResource(request);
            return response;
        }

        return null;
    }




    /**
     *
     * @param request
     * @return
     */
    private ResourceResponseInf postResource(ResourceRequestInf request) throws FxWebException {

        HealthCheckResponseImpl response = new HealthCheckResponseImpl();

        // Default to OK status
        response.setResponseCode(Response.Status.OK);

        // Always a multi-response type since multiple payments are always possible.
        response.setMultiResponseType(false);

        // Get/set system id from request header.
        RequestProperties props = request.getRequestProperties();
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

        response.setResponseType(ResponseTypes.response.HealthCheckResponse.toString());

        // No need to parse the body or get URL params, they should be empty

        // End-resultant map
        HashMap<String, HashMap> results = new HashMap();
        
        results.put("C30API", checkC30API());
        results.put("Workpoint", checkWorkpoint());
        results.put("SQLAPI", checkSQLAPI(request));
        results.put("OCS", checkOCS(request));
        HashMap forceCallback = checkForceCallback(request);
        if (forceCallback != null)
        	results.put("Force Callback", forceCallback);

        // Format SDK response(s)
        String xml = formatHealthCheckResponse(response, results);

        response.setResponseBody(xml);

        return response;



    }


    /**
     *
     * @return HashMap<String, String>
     */
     private HashMap<String, String> checkC30API(){

    	 HashMap<String, String> result = new HashMap<String, String>();
    	 result.put("Name", "C30API");
    	 result.put("Status", "OK");
    	 result.put("StatusCode", "0");
    	 return result;
    }


    /**
     *
     * @return HashMap<String, String>
     */
     private HashMap<String, String> checkWorkpoint(){

    	 HashMap<String, String> result = new HashMap<String, String>();
    	 result.put("Name", "Workpoint");
    	 try {

	    	 // Make an HTTP Callout and Ensure the Server is up and responding
    		 // Assumption is that Workpoint Web Logic is on the same server and using the default http admin port
	    	 Client c = Client.create();
	    	 WebResource r = c.resource("http://localhost:9092");
	    	 ClientResponse resp = r.get(ClientResponse.class);
	    	 int status = resp.getStatus();
	         if (status == 200) {
	         	 result.put("Status", "OK");
	         	 result.put("StatusCode", "0");
	         } else {
	         	 result.put("Status", "HTTP Problem: " + status);
	         	 result.put("StatusCode", "" + status);
	         }
	     } catch (Exception e) {
         	 result.put("Status", "Error - Workpoint offline or unreachable");
         	 result.put("StatusCode", "1000");
	    	 e.printStackTrace();
	     }
     	 return result;
     }

   /**
    *
    * @return HashMap<String, String>
    */
    private HashMap<String, String> checkSQLAPI(ResourceRequestInf request){

        HashMap<String, String> result = new HashMap<String, String>();
        result.put("Name", "SQLAPI");

    	// As long as the call does not throw an error, everything is Ok
    	// The block will throw an error if the our hard coded GEO does not exist
    	try {
    		RequestProperties props = request.getRequestProperties();
    		MultivaluedMap<String, String> params = props.getQueryParams();
            params.add("city",  "Anchorage");
            params.add("state", "Ak");
            params.add("zip",   "99503");
            props.setQueryParams(params);
            request.setRequestProperties(props);
            HashMap geoResults = new SdkGeoCodeFinder().geoCodeFind(request);
            // Call went through OK
            result.put("Status", "OK");
            result.put("StatusCode", "0");
    	} catch (FxWebException e) {
            // There was a problem with the call, possible SQL API issue
    		result.put("Status", "Geocode Lookup known good call is returning an error.");
            result.put("StatusCode", "1000");
            log.error("Exception occured"+e.getMessage());
			//e.printStackTrace();
		}

        return result;
    }

    /**
    *This method will check OCS health check 
    * @return HashMap<String, String>
    */
    private HashMap<String, String> checkOCS(ResourceRequestInf request){

        HashMap<String, String> result = new HashMap<String, String>();
        result.put("Name", "OCS");
   	
    	try {
    		
    		HashMap<String, String> ProvCLIInquiry=new HashMap<String, String>();
    		//giving some TN to check OCS Health check .This TN could be ACTIVE or Not
    		ProvCLIInquiry.put(Constants.PREPAID_CLI,FxWebConstants.TEST_CLI);
    		String CLI = ProvCLIInquiry.get(Constants.PREPAID_CLI);
			log.info("Send Subscriber.GetByCLI Inquiry for Telephonenumber : "+CLI);
			ProvCLIInquiry.put(Constants.ACCT_SEG_ID,"4");
			ProvCLIInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,"BIL");				
			ProvCLIInquiry.put(Constants.C30_TRANSACTION_ID,"111");
			ProvCLIInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);			
			ProvCLIInquiry.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_GET_BYCLI);			
			C30ProvisionRequest ocsrequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = ocsrequest.ocsInquiryRequest(ProvCLIInquiry);
			log.info("Response XML : "+response.get("ResponseXML"));
			
			int waitTimerIterations = 0;
			//Wait for the response 
			while (true)
			{
				//Wait for some to get the response.
				try {
					Thread.sleep(Constants.THREAD_SLEEP_TIME);
					} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
	
				if (response == null ) 
					{
					result.put("Status", "Not Connecting to OCS.");
		            result.put("StatusCode", "1000");	
					}
	
				if (response !=null) 
				{
					//Get CLI Status
					String retString = C30ProvConnectorUtils.getOCSMemberValuefromResponse(response.get("ResponseXML").toString(), 
							FxWebConstants.PREPAID_OCS_SUBSCRIBER_STATE_LABEL);
					if(retString !=null && retString.equalsIgnoreCase("ACTIVE"))
					{
						 // Call went through OK
			            result.put("Status", "OK");
			            result.put("StatusCode", "0");	
					}
					
					if(retString == null)
					{
						String retVal=C30ProvConnectorUtils.getOCSMemberValuefromResponse(response.get("ResponseXML").toString(), 
								"faultCode");
						if(retVal!=null && retVal.equalsIgnoreCase("159"))
						{
							 // Call went through OK
				            result.put("Status", "OK");
				            result.put("StatusCode", "0");
						}
					}			
					break;
				}
			
				if (waitTimerIterations >Constants.THREAD_SLEEP_TIME_ITERATIONS)
				{
					result.put("Status", "Not Connecting to OCS.");
		            result.put("StatusCode", "1000");	
				}
					
	
				waitTimerIterations++;
			}
			
		
		
    	} catch (Exception e) {
            // There was a problem with the OCS call
    		result.put("Status", "Not Connecting to OCS.");
            result.put("StatusCode", "1000");
			//e.printStackTrace();
            log.error("Exception occured"+e.getMessage());
		}
    	 return result;
    }
   
    private HashMap<String,String> checkForceCallback(ResourceRequestInf request){

        HashMap<String, String> result = new HashMap<String, String>();
        Response clientCallbackResponse = null;
        ResourceController rc=new ResourceController();
        try{
        	// Taking some example request to check call back works fine or not  
        	String xmlDoc="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
    				+ "<PaymentRequest>\n"
    				+ "<PaymentList size=\"1\">\n"
    				+ "<Payment paymentAction=\"PAYMENT\" sequence=\"1\"><PaymentSalesChannel>Self Care</PaymentSalesChannel>\n"
    				+ "<Amount></Amount><CheckNumber></CheckNumber><TransactionDateTime>2016-08-16 00:00:00</TransactionDateTime>\n"
    				+ "<TenderType>Visa</TenderType><AccountNumber></AccountNumber><Note>Payment Made</Note><ExtCorrelationId>+"
    				+ "</ExtCorrelationId><Currency>USD</Currency></Payment></PaymentList></PaymentRequest>";
           	
		Document doc = C30DOMUtils.stringToDom(xmlDoc);
		
		RequestProperties properties = request.getRequestProperties();
     	request.getRequestProperties().setURI("clientcallback");
                 
     	final MultivaluedMap<String, String> queryParams = properties.getQueryParams();
		request.getRequestProperties().setQueryParams(queryParams);
		properties.setQueryParams(queryParams);
              
       final MultivaluedMap<String, String> pathParams  =properties.getPathParams();
         properties.setPathParams(pathParams);
        request.getRequestProperties().setPathParams(pathParams); 
        request.getRequestProperties().setQueryParams(queryParams);
        
        HttpHeaders header  =properties.getHeaders();
        final MultivaluedMap<String, String> headerParams  =header.getRequestHeaders();
        List<String> postList=new ArrayList<String>();
        postList.add("POST");
        List<String> requestList=new ArrayList<String>();
        log.info("going get Env- id");
        String envId=FxWebUtil.getHeaderValue(header, "X-Env-Id");
        log.info("envId ="+envId);
        //getting Env-Id
        if(envId.equalsIgnoreCase("AWN3DEV1")){
        requestList.add("/AWN3/DEV1/clientcallback");
        headerParams.put("http.method",postList);
        headerParams.put("http.request",requestList);
        }
        else if(envId.equalsIgnoreCase("AWN3TST1"))
        {
        	 requestList.add("/AWN3/TST1/clientcallback");
             headerParams.put("http.method",postList);
             headerParams.put("http.request",requestList);	
        }
        else if(envId.equalsIgnoreCase("AWN3UAT1"))
        {
        	 requestList.add("/AWN3/UAT1/clientcallback");
             headerParams.put("http.method",postList);
             headerParams.put("http.request",requestList);	
        }
        else if(envId.equalsIgnoreCase("C3B1PRD"))
        {
        	 requestList.add("/C3B1/PRD/clientcallback");
             headerParams.put("http.method",postList);
             headerParams.put("http.request",requestList);
        }
        else
        {
        	log.info("No environment id Found");
        	
        }
        properties.setHeaders(header);
        properties.setPayload(doc);
        request.getRequestProperties().setHeaders(header);
        request.getRequestProperties().setPayload(doc);
             
        UriInfo ui= new UriInfo() {
		        	
			@Override
			public UriBuilder getRequestUriBuilder() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public URI getRequestUri() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public MultivaluedMap<String, String> getQueryParameters(boolean arg0) {
				// TODO Auto-generated method stub
				return queryParams;
			}
			
			@Override
			public MultivaluedMap<String, String> getQueryParameters() {
				// TODO Auto-generated method stub
				return queryParams;
			}
			
			@Override
			public List<PathSegment> getPathSegments(boolean arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List<PathSegment> getPathSegments() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public MultivaluedMap<String, String> getPathParameters(boolean arg0) {
				// TODO Auto-generated method stub
				return pathParams;
			}
			
			@Override
			public MultivaluedMap<String, String> getPathParameters() {
				// TODO Auto-generated method stub
				return pathParams;
			}
			
			@Override
			public String getPath(boolean arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getPath() {
				// TODO Auto-generated method stub
				return "clientcallback";
			}
			
			@Override
			public List<String> getMatchedURIs(boolean arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List<String> getMatchedURIs() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List<Object> getMatchedResources() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public UriBuilder getBaseUriBuilder() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public URI getBaseUri() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public UriBuilder getAbsolutePathBuilder() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public URI getAbsolutePath() {
				// TODO Auto-generated method stub
				return null;
			}
		};	
	           
	        String path = ui.getPath();
	        properties.setURI(path);
	    
      	 clientCallbackResponse=rc.clientCallbackResImpl(doc,properties.getHeaders(),ui);
		 result.put("Name", "ForceCallback");
         String response="<?xml version=\"1.0\" encoding=\"utf-8\"?><ClientCallbackResponse><Status><Message>SUCCESS</Message><Code>0</Code></Status></ClientCallbackResponse>";
         if (clientCallbackResponse.getEntity().toString().equalsIgnoreCase(response)){
         	// Expected Response - Everything is OK
           	result.put("Status", "OK");
            result.put("StatusCode", "0");
         }
         else {
         	// Got something unexpected
           	result.put("Status", "Callback made but XML response does not match expected result");
            result.put("StatusCode", "2000");
         }


 	} catch (Exception e) {
         // There was a problem with the callback
 		result.put("Status", "Error making HTTP callback");
        result.put("StatusCode", "1000");
        log.error("Exception occured"+e.getMessage());
			//e.printStackTrace();
		}
        return result;
    }

    /**
     * Format Health Check response.  The response is quite simplistic,
     * each system gets a Status. Code=0 and Message=SUCCESS indicate
     * a healthy system. Other codes and messages will be returned in
     * the event of a system issue.
     *
     *<HealthCheckResponse>
     *  <StatusList>
     *    <Status>
     *      <System>K2</System>
     *      <Message>SUCCESS</Message>
     *      <Code>0</Code>
     *    </Status>
     *  </StatusList>
     *  <SystemList>
     *    <TotalCount>2</TotalCount>
     *    <System>
     *      <Name>C30API</Name>
     *      <Status>OK</Status>
     *      <StatusCode>0</StatusCode>
     *    </System>
     *    <System>
     *      <Name>SQLAPI</Name>
     *      <Status>Unavailable</Status>
     *      <StatusCode>404</StatusCode>
     *    </System>
     *  </SystemList>
     *</HealthCheckResponse>
     *
     * @param response
     * @param results
     * @return
     */
    private String formatHealthCheckResponse(HealthCheckResponseImpl response,
                                          HashMap<String, HashMap> results)
                                                  throws FxWebException {

    	// Status=OK, Code=0, is the only good status
    	// The other status bubble up from the underlying dependency
    	// and differ between each one

        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        xmlResponse.append("<HealthCheckResponse><StatusList><Status>");
        xmlResponse.append("<Message>SUCCESS</Message>");
        xmlResponse.append("<Code>0</Code>");
        xmlResponse.append("</Status></StatusList><SystemList>");

        xmlResponse.append("<TotalCount>" + results.size() + "</TotalCount>");

        for(HashMap<String, String> result : results.values()){
        	xmlResponse.append("<System>");
            xmlResponse.append("<Name>" + result.get("Name") + "</Name>");
            xmlResponse.append("<Status>" + result.get("Status") + "</Status>");
            xmlResponse.append("<StatusCode>" + result.get("StatusCode") + "</StatusCode>");
            xmlResponse.append("</System>");
        }
        /*
        xmlResponse.append("<TotalCount>1</TotalCount>");

        xmlResponse.append("<System>");
        xmlResponse.append("<Name>C30API</Name>");
        xmlResponse.append("<Status>OK</Status>");
        xmlResponse.append("<StatusCode>0</StatusCode>");
        xmlResponse.append("</System>");
        */

        xmlResponse.append("</SystemList></HealthCheckResponse>");

        return xmlResponse.toString();
    }

}
