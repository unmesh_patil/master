package com.cycle30.fxweb.resource;

import java.io.IOException;
import java.util.HashMap;

import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.response.VoucherResponseImpl;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidVoucherDetails;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;



/** 
 * Voucher resource object
 * 
 */

public class VoucherResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(VoucherResourceImpl.class);   
    
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
         
        return null; 
    }
    
    
    /**
     * Process the HTTP GET request. 
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
    		VoucherResponseImpl response = new VoucherResponseImpl(); 
    		response.setResponseType(ResponseTypes.response.VoucherResponse.toString());
    	
	    	//Get the Voucher PIN        
	        String voucherpinCode = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), FxWebConstants.OCS_PREPAID_VOOUCHER_PIN);
	        log.info("voucherpinCode = "+voucherpinCode);

	        String voucherAction = FxWebUtil.voucherAction(request.getRequestProperties());
	        log.info("voucherAction = "+voucherAction);
	        
	        HashMap<String, ?> results = new HashMap();
			HashMap<Integer, HashMap<String, String>> bundleResult = null;

	        log.info("voucherAction = "+voucherAction);			
			try {

				results = new SdkPrepaidVoucherDetails().getPrepaidVoucherDetails(request);
				
			} catch (FxWebException we) {

				throw we;
			}			
	        log.info("Start - Format Response");
            // Format the response
			String formattedResponse = formatVoucherResponse(response, voucherAction,voucherpinCode.length(), results);
			response.setResponseBody(formattedResponse);
	        log.info("End - Format Response");
			response.setResponseCode(Response.Status.OK);
        
        return response;
    }
        

    private String formatVoucherResponse(VoucherResponseImpl response, String voucherAction, int pinLength, HashMap<String, ?> results) throws FxWebException {
			// Set status to 0=success as default
			String code = "0";
			String message = "SUCCESS";
			String 	exceptionCode = null;
			//HashMap<String, String> balanceResultMap = null;
			StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
	        StringBuffer ResponseBody = new StringBuffer();
	        
			// Default response = OK
	        Response.Status errorStatus = Response.Status.OK;
	        response.setResponseCode(errorStatus);
	        
	        // HashMap with the key of "0". 
	        
	        HashMap resultMap = (HashMap)results.get("0");
	
			xmlResponse.append("<VoucherResponse>");
			xmlResponse.append("<Status>");
			
	        // See if exception occurred. If so, get the error message 
	        exceptionCode = (String)resultMap.get("ExceptionCode");
    	
	        if (exceptionCode != null) {
	        	message = (String)resultMap.get("MsgText");
				xmlResponse.append("<Message>" + message + "</Message>");
				xmlResponse.append("<Code>" + exceptionCode + "</Code>");
				xmlResponse.append("</Status>");
				
	
	        } else {
        	
		    		xmlResponse.append("<Message>" + message + "</Message>");
		    		xmlResponse.append("<Code>" + code + "</Code>");
		    		xmlResponse.append("</Status>");
		    		int listSize = 0;
		    		if("status".equalsIgnoreCase(voucherAction) && pinLength ==12){
		    			
		    			Document doc = DomUtils.stringToDom(resultMap.get("VoucherDetailsXML").toString());
		    			doc.getDocumentElement().normalize();
		    			
		    			NodeList structList = doc.getElementsByTagName("struct");
		    			
						for(int i=0; i<structList.getLength(); i++){
							
					        NodeList memberList =  ((Element) structList.item(i)).getElementsByTagName("member");
					       // balanceResultMap = new HashMap<String, String>();
							ResponseBody.append("<Voucher>");
							String voucherValue=null;
					        for(int j=0; j<memberList.getLength();j++){
					  
							    String Name = (String) ((Element) memberList.item(j)).getElementsByTagName("name").  
							                    item(0).getChildNodes().item(0).getNodeValue();  
							    String Value = (String) ((Element) memberList.item(j)).getElementsByTagName("value").  
							            item(0).getChildNodes().item(0).getTextContent();
							  
							    if("CardProfileLabel".equalsIgnoreCase(Name)){
							    	//balanceResultMap.put(Name,Value);   
							    	voucherValue=Value;
							    	ResponseBody.append("<CardProfileLabel>" + Value + "</CardProfileLabel>");
								    listSize++;							    	
							    }else if("CardStateLabel".equalsIgnoreCase(Name)){
									ResponseBody.append("<CardStatusLabel>" + Value + "</CardStatusLabel>");
							    	//balanceResultMap.put(Name,Value); 
							    }else if("CardGUID".equalsIgnoreCase(Name)){
							    	ResponseBody.append("<CardGUID>" + Value + "</CardGUID>");
							    	//balanceResultMap.put(Name,Value); 
							    }else if("ExpirationDate".equalsIgnoreCase(Name)){
							    	ResponseBody.append("<ExpirationDate>" + Value + "</ExpirationDate>");
							    	//balanceResultMap.put(Name,Value); 
							    } 
							    
					        }
					        ResponseBody.append("<VoucherValue>" + getVoucherValue(voucherValue) + "</VoucherValue>");
							ResponseBody.append("</Voucher>");				        
						}
	    			
		    			xmlResponse.append("<VoucherList size=\"" + listSize + "\">");
		    	    	xmlResponse.append(ResponseBody);
		    	    	xmlResponse.append("</VoucherList>");			
		    			
		    		}else if ("status".equalsIgnoreCase(voucherAction) && pinLength ==10){
		    			HashMap incommResultMap= new HashMap();
		    			HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap();
		    			
		    			log.info("Reached the Incomm Format Block");
		    			try {
		    				
		    				if(resultMap.get("VoucherDetailsXML")!=null){
		    					log.info(resultMap.get("VoucherDetailsXML").toString());
		    					 bundleResult = DomUtils.parseXMLIncommResponseXML((resultMap.get("VoucherDetailsXML")).toString());
		    				}
		    				
						
						if(bundleResult.get(0) !=null){
							log.info("ResponseXML looks Good after parsing" );
							incommResultMap=bundleResult.get(0);
							listSize++;
						}
						
							ResponseBody.append("<Voucher>");
							
							ResponseBody.append("<CardProfileLabel>" + incommResultMap.get("UPC")  + "</CardProfileLabel>");
							ResponseBody.append("<CardStatusLabel>" + incommResultMap.get("RespMsg") + "</CardStatusLabel>");
							ResponseBody.append("<CardGUID>" + incommResultMap.get("SerialNumber") + "</CardGUID>");
							ResponseBody.append("<ExpirationDate></ExpirationDate>");
							ResponseBody.append("<VoucherValue>" + incommResultMap.get("FaceValue") + "</VoucherValue>");
							ResponseBody.append("</Voucher>");
							
							xmlResponse.append("<VoucherList size=\"" + listSize + "\">");
			    	    	xmlResponse.append(ResponseBody);
			    	    	xmlResponse.append("</VoucherList>");	
			    	    	
		    			} catch (Exception e) {
							throw new FxWebException(e, e.getMessage());
						} 
		    			
		    		}else if ("status".equalsIgnoreCase(voucherAction) && ( pinLength==11)){
	    			HashMap blackHawkResultMap= new HashMap();
	    			HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap();
	    			
	    			log.info("Reached the BlackHawk Format Block");
	    			try {
	    				
	    				if(resultMap.get("VoucherDetailsXML")!=null){
	    					log.info(resultMap.get("VoucherDetailsXML").toString());
	    					 bundleResult = DomUtils.parseXMLBlackHawkResponseXML((resultMap.get("VoucherDetailsXML")).toString());
	    				}
	    				
					
					if(bundleResult.get(0) !=null){
						log.info("ResponseXML looks Good after parsing" );
						blackHawkResultMap=bundleResult.get(0);
						listSize++;
					}
					
						ResponseBody.append("<Voucher>");
						
						ResponseBody.append("<CardProfileLabel>" + blackHawkResultMap.get("UPC")  + "</CardProfileLabel>");
						ResponseBody.append("<CardStatusLabel>Card is Active</CardStatusLabel>");
						ResponseBody.append("<CardGUID>" + blackHawkResultMap.get("SerialNumber") + "</CardGUID>");
						ResponseBody.append("<ExpirationDate></ExpirationDate>");
						ResponseBody.append("<VoucherValue>" + getBlackhawkVoucherValue(blackHawkResultMap.get("FaceValue").toString()) + "</VoucherValue>");
						ResponseBody.append("</Voucher>");
						
						xmlResponse.append("<VoucherList size=\"" + listSize + "\">");
		    	    	xmlResponse.append(ResponseBody);
		    	    	xmlResponse.append("</VoucherList>");	
		    	    	
	    			} catch (Exception e) {
						throw new FxWebException(e, e.getMessage());
					} 
	    			
		    		}
					
				}	    	
				xmlResponse.append("</VoucherResponse>");
				return xmlResponse.toString();
        }
    /**
     * 
     * @param voucherValue
     * @return
     */
     private static String getVoucherValue(String voucherValue) {
		int lastIndex= voucherValue.lastIndexOf("_");
		 voucherValue= voucherValue.substring(lastIndex+1);
		 voucherValue=voucherValue+".00";
		
		return voucherValue;
	}
     
     
     
     /**
      * 
      * @param voucherValue
      * @return
      */
      private static String getBlackhawkVoucherValue(String voucherValue) {
    	String voucherValueUpdated=voucherValue.replace("C", "0");
 		Double exactValue =  ((( new Double(voucherValueUpdated)/100))); 		
 		return exactValue.toString();
 	}
      
  }