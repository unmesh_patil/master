package com.cycle30.fxweb.resource;

import java.util.HashMap;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;


import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.AccountResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseFormatter;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.SdkAccountFinder;
import com.cycle30.fxweb.util.FxWebUtil;


/** 
 * Kenan resource object
 */

public class AccountResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(AccountResourceImpl.class);   
    
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("PUT")) {
            ResourceResponseInf response = putResource(request);
            return response;
        }
        
        if (method.equalsIgnoreCase("POST")) {
            ResourceResponseInf response = postResource(request);
            return response;
        }
        
        return null; 
    }
    
    

    /**
     * Process the HTTP GET request. This could be either a single-account
     * 'find', or a multi-account 'search' based on whether URI query 
     * params exist.  
     * 
     * If an account number was found either on the URI path or as a query param,
     * invoke the single-account 'finder' method.  Otherwise, use the 'search' 
     * method.
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
        AccountResponseImpl response = new AccountResponseImpl(); 
        //response.setResponseType(ResponseTypes.ACCOUNT_SEARCH_RESPONSE);
        response.setResponseType(ResponseTypes.response.AccountSearchResponse.toString());
        
        // Attempt to get customer number to see if this is a single or multi account 
        // search type, and set the response property appropriately.
        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> pathParams  = props.getPathParams();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
        String acctNo = FxWebUtil.getCustomerNumber(pathParams, queryParams, null);
        
        
        // Get system id from request header.
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
        HashMap results = new HashMap();
        
        try { 
           results = new SdkAccountFinder().accountFind(request);
        }
        
        catch (FxWebException we) {

            // Form generic error for ResponseFormatter purposes
            String error = we.getFormattedMessage();
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", "");  // placeholder
            excMap.put("MsgText", error);
            results.put("0", excMap);
        }

        ResponseFormatter.formatResponse(request, response, results, true);
 
        return response;
    }
    
    
    /**
     * Handle PUT request
     * 
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf putResource(ResourceRequestInf request) {
        AccountResponseImpl response = new AccountResponseImpl(); 
        response.setResponseBody("account put ok");
        response.setResponseCode(Response.Status.OK);
        
        return response;
    }
    
    
    /**
     * Handle POST request
     * 
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf postResource(ResourceRequestInf request) {
        AccountResponseImpl response = new AccountResponseImpl(); 
        response.setResponseBody("account post ok");
        response.setResponseCode(Response.Status.OK);
        
        return response;
    }


    
}
