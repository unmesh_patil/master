package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.payment.C30PaymentActionType;
import com.cycle30.fxweb.request.wrapper.payment.PaymentRequest;
import com.cycle30.fxweb.request.wrapper.payment.PaymentVersion20Object;
import com.cycle30.fxweb.response.PaymentResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkPaymentCreate;
import com.cycle30.fxweb.sdkinterface.SdkPaymentRegister;
import com.cycle30.fxweb.sdkinterface.SdkPaymentReversal;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;


/**
 * 
 * Kenan payment resource instance
 *
 */
public class PaymentResourceImpl implements ResourceInf  {

	public static final Logger log = Logger.getLogger(PaymentResourceImpl.class); 


	public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {


		// Get the original HTTP method from the 'http.method' header
		RequestProperties properties = request.getRequestProperties();
		String method = FxWebUtil.getRequestMethod(properties.getHeaders());
		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();

		// Unmarshall the DOM document into a list of payment objects.
		PaymentRequest pr = getPaymentList(doc);
		int sizeofPayments=0;
		if(pr !=null ){
			List l = pr.getPaymentList().getPayment();
			sizeofPayments=l.size();
		}



		if (method.equalsIgnoreCase("POST") ) {
			ResourceResponseInf response = postResource(request);
			return response;
		}

		if (method.equalsIgnoreCase("DELETE")) {
			ResourceResponseInf response = deleteResource(request);
			return response;
		}

		return null; 
	}




	/**
	 * 
	 * @param request
	 * @return
	 */
	private ResourceResponseInf postResource(ResourceRequestInf request) throws FxWebException {

		PaymentResponseImpl response = new PaymentResponseImpl(); 

		// Default to OK status
		response.setResponseCode(Response.Status.OK);

		// Always a multi-response type since multiple payments are always possible.
		response.setMultiResponseType(true);

		// Get/set system id from request header.
		RequestProperties props = request.getRequestProperties();
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		response.setResponseType(ResponseTypes.response.PaymentResponse.toString());

		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();

		// Unmarshall the DOM document into a list of payment objects.
		PaymentRequest pr = getPaymentList(doc);
		

		// End-resultant map
		HashMap<String, HashMap> results = new HashMap();
		HashMap<String, HashMap<String, String>> bundleResult= new HashMap<String, HashMap<String, String>>();


		// Go through the embedded payments, and invoke SDK for each
		List l = pr.getPaymentList().getPayment();

		Iterator i = l.iterator();
		int paymentNo = 0;

		while (i.hasNext()) {

			PaymentVersion20Object paymentObject = (PaymentVersion20Object)i.next();
			//HashMap<String, HashMap<String, String>> bundleResult= new HashMap<String, HashMap<String, String>>();

			try { 
				HashMap pmtResult = new HashMap();
				// Based on the payment action we need to decide which route to take.
				if (paymentObject.getPaymentAction().value().equalsIgnoreCase(C30PaymentActionType.PAYMENT.toString()))
				{
					// Action is Payment and work on the payment create for core.
					pmtResult = new SdkPaymentCreate().createPayment(request, paymentObject, doc);

					// For each SDK result, add to our 'global' set of results with keys of
					// "0", "1", etc.  This is to simulate an SDK multi-result set that 
					// returns multiple results in that fashion.  For each invocation of
					// a payment transaction, there'll be just one response with a key of
					// "0", so get that HashMap and plug into the global result with new key.
					HashMap interimResult = (HashMap)pmtResult.get("0");

					// Push the original correlation id and account # into the result map since it's 
					// needed in the response structure
					String acctNo = paymentObject.getAccountNumber();
					String corrId = paymentObject.getExtCorrelationId();
					interimResult.put("ExtCorrelationId", corrId);
					interimResult.put("AccountNumber", acctNo);
					interimResult.put("Action", C30PaymentActionType.PAYMENT.toString());

					String key = String.valueOf(paymentNo);
					bundleResult.put(key, interimResult);

				}
				//Registering payment
				if (paymentObject.getPaymentAction().value().equalsIgnoreCase(C30PaymentActionType.REGISTER.toString()))
				{
					// Action is Payment Register and work on the payment register for core.

					pmtResult = new SdkPaymentRegister().createRegisterPrepaidPayment(request, paymentObject, doc);

					HashMap ResponseXML = (HashMap) pmtResult.get("0");

					if(ResponseXML.get("PrepaidPaymentXML")!=null)
					{
						String key = String.valueOf(paymentNo);
						HashMap<String, String> paymentResultMap = new HashMap<String, String>();
						paymentResultMap.put("0", "SUCCESS");
						String acctNo = paymentObject.getAccountNumber();
						paymentResultMap.put("AccountNumber", acctNo);
						paymentResultMap.put("Action",(C30PaymentActionType.REGISTER.toString()));
						bundleResult.put(key, paymentResultMap);	
						
					}
					else if(ResponseXML !=null && ResponseXML.get("ExceptionCode")!= null)
					{
						String exceptionCode= ResponseXML.get("ExceptionCode").toString();
						String exceptionMsg = ResponseXML.get("MsgText").toString();
						HashMap<String, String>	paymentResultMap = new HashMap<String, String>();
						paymentResultMap.put("Action",(C30PaymentActionType.REGISTER.toString()));
						String acctNo = paymentObject.getAccountNumber();
						paymentResultMap.put("ExceptionCode", exceptionCode);
						paymentResultMap.put("AccountNumber", acctNo);
						paymentResultMap.put("MsgText", exceptionMsg);
						paymentResultMap.put("Sequence", (new Integer (paymentNo)).toString() );
						String key = String.valueOf(paymentNo);
						bundleResult.put(key, paymentResultMap);
					}

				}

				paymentNo++;

			}


			catch (C30SDKInvalidAttributeException iae) 
			{

				// Form generic error for ResponseFormatter purposes
				String excCode = iae.getExceptionCode();
				String msg = iae.getMessage();

				HashMap excMap = new HashMap(); 
				excMap.put("ExceptionCode", excCode);  
				excMap.put("MsgText", msg);
				bundleResult.put("0", excMap);

				// Stop processing the set of payments if fatal error occurs
				break;
			}

		}

		// Format SDK response(s)
		
		String xml = formatPaymentResponse(response, bundleResult,paymentNo);

		response.setResponseBody(xml);

		return response;



	}



	/**
	 * 
	 * @param doc
	 * @return
	 * @throws FxWebException
	 */
	private PaymentRequest getPaymentList(Document doc) throws FxWebException {

		// Unmarshall request into JavaBean wrappers
		PaymentRequest pr = null;

		try {

			JAXBContext context = JAXBContext.newInstance(PaymentRequest.class);
			Unmarshaller u = context.createUnmarshaller();

			pr = (PaymentRequest)u.unmarshal(doc);

		} catch (Exception e) {
			log.error(e);
			throw new FxWebException(e, "Unable to unmarshall payment DOM document");
		}

		return pr;


	}



	/**
	 * Format payment response.  The response is quite simplistic, requiring just 
	 * the correlation id from the original request, the tracking id from the 
	 * SDK response, and a status structure.  The end result is:
	 * 
	 *  <PaymentResponse>
	 *      <Payments>
	 *        <Payment>               
	 *          <ExtCorrelationId></ExtCorrelationId>
	 *          <TrackingId></TrackingId>
	 *          <DistributionId></DistributionId>
	 *          <Status>
	 *            <System>K2</system>
	 *                  <Message>SUCCESS</message>
	 *                  <Code>0</code>
	 *            </Status>
	 *        </Payment>
	 *        <Payment>               
	 *          <ExtCorrelationId></ExtCorrelationId>
	 *          <TrackingId></TrackingId>
	 *          <Status>
	 *              <System>K2</system>
	 *              <Message>SUCCESS</message>
	 *                <Code>0</code>
	 *           </Status>
	 *        </Payment>
	 *      </Payments>
	 *   </PaymentResponse>
	 *
	 * 
	 * @param response
	 * @param bundleResult
	 * @return
	 */
	private String formatPaymentResponse(PaymentResponseImpl response, 
			HashMap<String, HashMap<String, String>> bundleResult,int paymentNo) 
					throws FxWebException {


		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<PaymentResponse>");

		boolean paymentsOk = false;

		Response.Status errorStatus = Response.Status.OK;
		xmlResponse.append("<PaymentList   size=\""+paymentNo+"\">");

		// Go through the Map of HashMaps, and generate a payment structure for each
		Iterator i = bundleResult.keySet().iterator();
		while (i.hasNext()) {

			String key  = (String)i.next();
			HashMap result = (HashMap)bundleResult.get(key);
			
			String actionType=(String)result.get("Action");
          
			if(C30PaymentActionType.PAYMENT.toString().equalsIgnoreCase(actionType)){
			// Get the tracking id, or possible SDK exception code
			String trackingId = (String)result.get("TrackingId");
			String distributionId = (String)result.get("SourceId");  // called 'SourceId' in the response
			String exception  = (String)result.get("ExceptionCode");

			// Get the original correlation id and account no
			String correlId = (String)result.get("ExtCorrelationId");
			String acctNo   = (String)result.get("AccountNumber");

			// Get the system id (eg. K1, K2) already set in the response
			String system = response.getSystemId();

			// Set status to 0=success, 1=failure
			String code = "0";
			String message = "SUCCESS";

			// Get the 'client friendly' error message 
			if (exception != null) {
				MappedError me = new MappedError(exception);
				message        = me.getExternalError();
				errorStatus    = me.getStatus();
				code           = me.getExceptionCode();
			}

			if (trackingId != null) {
				paymentsOk = true; // got at least one Ok payment response
			}

			if (trackingId == null || trackingId.equalsIgnoreCase("null")) {
				trackingId = "";
			}

			if (correlId == null || correlId.equalsIgnoreCase("null")) {
				correlId = "";
			}

			// Generate all XML elements.
			xmlResponse.append("<Payment   PaymentAction=\""+actionType+"\">");
			xmlResponse.append("<AccountNumber>"    + acctNo         + "</AccountNumber>");
			xmlResponse.append("<ExtCorrelationId>" + correlId       + "</ExtCorrelationId>");
			xmlResponse.append("<TrackingId>"       + trackingId     + "</TrackingId>");
			xmlResponse.append("<DistributionId>"   + distributionId + "</DistributionId>");
			xmlResponse.append("<Status>");
			//xmlResponse.append("<System>"  + system + "</System>");
			xmlResponse.append("<Message>" + message + "</Message>");
			xmlResponse.append("<Code>"    + code + "</Code>");
			xmlResponse.append("</Status>");
			xmlResponse.append("</Payment>");
			
			}else if(C30PaymentActionType.REGISTER.toString().equalsIgnoreCase(actionType)){
				
				String acctNo   = (String)result.get("AccountNumber");
				String exceptionCode = (String) result.get("ExceptionCode");
				if (exceptionCode != null) {
				
					xmlResponse.append("<Payment   PaymentAction=\""+actionType+"\">");
					xmlResponse.append("<AccountNumber>"    + acctNo         + "</AccountNumber>");
					xmlResponse.append("<Status>");
					xmlResponse.append("<Message>" + (String)result.get("MsgText") + "</Message>");
					xmlResponse.append("<Code>" + exceptionCode + "</Code>");
					xmlResponse.append("</Status>");
					xmlResponse.append("</Payment>");
					
				}else{

					String code = "0";
					String message = "SUCCESS";

					xmlResponse.append("<Payment   PaymentAction=\""+actionType+"\">");
				xmlResponse.append("<AccountNumber>"    + acctNo         + "</AccountNumber>");
				xmlResponse.append("<Status>");
				xmlResponse.append("<Message>" + message + "</Message>");
				xmlResponse.append("<Code>" + code + "</Code>");
				xmlResponse.append("</Status>");
				xmlResponse.append("</Payment>");
				}
			}

		}  // end while()

		xmlResponse.append("</PaymentList></PaymentResponse>");

		// if all payments failed (didn't find one that was ok, set return 
		// status to error condition. 
		if (paymentsOk == false) {
			response.setResponseCode(errorStatus);
		}

		return xmlResponse.toString();
	}




	/**
	 * 
	 * A DELETE request will reverse an existing payment.
	 * 
	 * @param request
	 * @return
	 */
	private ResourceResponseInf deleteResource(ResourceRequestInf request) throws FxWebException {

		PaymentResponseImpl response = new PaymentResponseImpl(); 

		// Default to OK status
		response.setResponseCode(Response.Status.OK);

		// Only one response is supported for reversals.
		response.setMultiResponseType(false);

		// Get/set system id from request header.
		RequestProperties props = request.getRequestProperties();
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		response.setResponseType(ResponseTypes.response.PaymentReversal.toString());

		HashMap results = new HashMap();

		try { 

			results = new SdkPaymentReversal().reversePayment(request);

			// Push the original tracking id into the result map since it's 
			// needed in the response structure
			String trackingId = FxWebUtil.getPathParamValue(props.getPathParams(), "trackingId");
			results.put("TrackingId", trackingId);

		}
		catch (C30SDKInvalidAttributeException iae) {

			// Form generic error for ResponseFormatter purposes
			String excCode = iae.getExceptionCode();
			String msg = iae.getMessage();

			HashMap excMap = new HashMap(); 
			excMap.put("ExceptionCode", excCode);  
			excMap.put("MsgText", msg);
			results.put("0", excMap);

		}


		// Format SDK reversal response
		String xml = formatReversalResponse(response, results);

		response.setResponseBody(xml);


		return response;

	}







	/**
	 * Format reversal response.  The response is very similar to a payment response,
	 * but keep them separate so can extend/modify in the future if necessary. Biggest
	 * difference is that at time of this writing, only single reversal is supported.
	 * The end result is:
	 * 
	 *  <PaymentReversalResponse>
	 *      <Reversals>
	 *        <Reversal>               
	 *          <TrackingId></TrackingId>
	 *          <Status>
	 *            <System>K2</system>
	 *            <Message>SUCCESS</message>
	 *            <Code>0</code>
	 *            </Status>
	 *        </Reversal>
	 *      </Reversals>
	 *   </PaymentReversalResponse>
	 *
	 * 
	 * @param response
	 * @param results
	 * @return
	 */
	private String formatReversalResponse(PaymentResponseImpl response, 
			HashMap results)   throws FxWebException {


		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<PaymentReversalResponse><Reversals>");

		Response.Status errorStatus = Response.Status.OK;


		// Get the single HashMap result under the "0" key
		HashMap result = (HashMap)results.get("0");

		// Get the tracking id, or possible SDK exception code
		String trackingId = (String)result.get("TrackingId");
		String exception  = (String)result.get("ExceptionCode");

		if (trackingId == null || trackingId.equalsIgnoreCase("null")) {
			trackingId = "";
		}

		// Get the system id (eg. K1, K2) already set in the response
		String system = response.getSystemId();

		// Set status to 0=success, 1=failure
		String code = "0";
		String message = "SUCCESS";

		// Get the 'client friendly' error message 
		if (exception != null) {
			MappedError me = new MappedError(exception);
			message        = me.getExternalError();
			errorStatus    = me.getStatus();
			code           = me.getExceptionCode();
		}


		// Generate all XML elements.
		xmlResponse.append("<Reversal>");
		xmlResponse.append("<TrackingId>" + trackingId + "</TrackingId>");
		xmlResponse.append("<Status>");
		xmlResponse.append("<System>"  + system + "</System>");
		xmlResponse.append("<Message>" + message + "</Message>");
		xmlResponse.append("<Code>"    + code + "</Code>");
		xmlResponse.append("</Status>");
		xmlResponse.append("</Reversal>");


		xmlResponse.append("</Reversals></PaymentReversalResponse>");


		return xmlResponse.toString();
	}




}
