package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;


import com.cycle30.fxweb.exception.FxWebException;


import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;

import com.cycle30.fxweb.response.DepositResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkDeposit;

import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;


/**
 * 
 * Kenan deposit resource instance
 *
 */
public class DepositResourceImpl implements ResourceInf  {
    
    public static final Logger log = Logger.getLogger(DepositResourceImpl.class); 
    
    
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
        
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
        
        
        if (method.equalsIgnoreCase("POST")) {
            ResourceResponseInf response = postResource(request);
            return response;
        }
        
        return null; 
    }
    
    
    
    
    /**
     * 
     * @param request
     * @return
     */
    private ResourceResponseInf postResource(ResourceRequestInf request) throws FxWebException {
        
        DepositResponseImpl response = new DepositResponseImpl(); 
        
        // Default to OK status
        response.setResponseCode(Response.Status.OK);
        
        // Always a multi-response type since multiple Deposits are always possible.
        response.setMultiResponseType(true);
        
        // Get/set system id from request header.
        RequestProperties props = request.getRequestProperties();
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        

        response.setResponseType(ResponseTypes.response.DepositResponse.toString());
        
        // RequestValidator class already made sure there's really a payload.
        Document doc = request.getRequestProperties().getPayload();
        String acctNo = DomUtils.getElementValue(doc, "AccountNumber");

        
        HashMap results = null;
        try { 
           
            results = new SdkDeposit().createDeposit(request, doc);          
        }

        catch (C30SDKInvalidAttributeException iae) {

            // Form generic error for ResponseFormatter purposes
            String excCode = iae.getExceptionCode();
            String msg = iae.getMessage();
            
            HashMap excMap = new HashMap(); 
            excMap.put("ExceptionCode", excCode);  
            excMap.put("MsgText", msg);
            results.put("0", excMap);
        } 
        
        // Format SDK response
        String xml = formatDepositResponse(acctNo, response, results);
        
        response.setResponseBody(xml);
        
        return response;


       
    }
    
    
    
    
    /**
     * Format deposit response.  The response is quite simplistic, requiring just 
     * the correlation id from the original request, the tracking id from the 
     * SDK response, and a status structure.  The end result is:
     * 
     *  <DepositResponse>
     *      <Deposits>
     *        <Deposit>               
     *          <ExtCorrelationId></ExtCorrelationId>
     *          <TrackingId></TrackingId>
     *          <Status>
     *            <System>K2</system>
     *                  <Message>SUCCESS</message>
     *                  <Code>0</code>
     *            </Status>
     *        </Deposit>
     *        <Deposit>               
     *          <ExtCorrelationId></ExtCorrelationId>
     *          <TrackingId></TrackingId>
     *          <Status>
     *              <System>K2</system>
     *              <Message>SUCCESS</message>
     *                <Code>0</code>
     *           </Status>
     *        </Deposit>
     *      </Deposits>
     *   </DepositResponse>
     *
     * 
     * @param response
     * @param results
     * @return
     */
    private String formatDepositResponse(String acctNo,
                                          DepositResponseImpl response, 
                                          HashMap<String, HashMap> results) 
                                                  throws FxWebException {
        
        
        StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
        xmlResponse.append("<DepositResponse>");
        
        boolean depositsOk = false;
        
        Response.Status errorStatus = Response.Status.OK;
        
        
        // Go through the Map of HashMaps, and generate a deposit structure for each
        Iterator i = results.keySet().iterator();
        while (i.hasNext()) {
            
            String key  = (String)i.next();
            HashMap result = (HashMap)results.get(key);
                        
            // Get the tracking id, or possible SDK exception code
            String trackingId = (String)result.get("TrackingId");
            String exception  = (String)result.get("ExceptionCode");
            
            // Get the system id (eg. K1, K2) already set in the response
            String system = response.getSystemId();
            
            // Set status to 0=success, 1=failure
            String code = "0";
            String message = "SUCCESS";
            
            // Get the 'client friendly' error message 
            if (exception != null) {
                MappedError me = new MappedError(exception);
                message        = me.getExternalError();
                errorStatus    = me.getStatus();
                code           = me.getExceptionCode();
                response.setResponseCode(errorStatus);
            }
            
            if (trackingId != null) {
                depositsOk = true; // got at least one Ok deposit response
            }
           
            if (trackingId == null || trackingId.equalsIgnoreCase("null")) {
                trackingId = "";
            }
   
            
            // Generate all XML elements.
            xmlResponse.append("<Deposit>");
              xmlResponse.append("<AccountNumber>"    + acctNo     + "</AccountNumber>");
              xmlResponse.append("<TrackingId>"       + trackingId + "</TrackingId>");
              xmlResponse.append("<Status>");
                 xmlResponse.append("<System>"  + system + "</System>");
                 xmlResponse.append("<Message>" + message + "</Message>");
                 xmlResponse.append("<Code>"    + code + "</Code>");
              xmlResponse.append("</Status>");
            xmlResponse.append("</Deposit>");
            
        }  // end while()
        
        xmlResponse.append("</DepositResponse>");


        return xmlResponse.toString();
    }
    
    



    
}
