package com.cycle30.fxweb.resource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.inventory.InventoryRequest;
import com.cycle30.fxweb.request.wrapper.inventory.InventoryRequestObject;
import com.cycle30.fxweb.response.InventoryResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.awnprepaid.SdkPrepaidInventoryUpdate;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;

public class InventoryResourceImpl implements ResourceInf {

	public static final Logger log = Logger
			.getLogger(InventoryResourceImpl.class);

	public ResourceResponseInf handleRequest(ResourceRequestInf request)
			throws FxWebException {

		// Get the original HTTP method from the 'http.method' header
		RequestProperties properties = request.getRequestProperties();
		String method = FxWebUtil.getRequestMethod(properties.getHeaders());

		if (method.equalsIgnoreCase("POST")) {
			ResourceResponseInf response;
			response = postResource(request);
			return response;

		}
		if (method.equalsIgnoreCase("GET")) {
			ResourceResponseInf response;

			response = getResource(request);
			return response;

		}

		return null;
	}

	/**
	 * Process the HTTP GET request.
	 * 
	 * @param request
	 * @return Resource response instance
	 */
	private ResourceResponseInf getResource(ResourceRequestInf request)
			throws FxWebException {

		InventoryResponseImpl response = new InventoryResponseImpl();

		// Default to OK status
		response.setResponseCode(Response.Status.OK);
		response.setResponseType(ResponseTypes.response.InventoryResponse
				.toString());

		String telephoneNumber = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), "telephoneNumber");

		// Get system id from request header.

		// Get system id from request header.
		RequestProperties props = request.getRequestProperties();
		HttpHeaders headers = props.getHeaders();
		response.setSystemId(FxWebUtil.getKenanVersion(headers));

		HashMap<String, HashMap> results = new HashMap();

		HashMap<String, String> interimResult = new HashMap();

		
		interimResult.put("TelephoneNumber", telephoneNumber);
		interimResult.put("InvStatus", "Available");
		interimResult.put("OperatorId", "1");
		interimResult.put("NetworkId", "123456");
		interimResult.put("OperatorId", "GCI");
		interimResult.put("NetworkDeviceId", "XXXX");


		String key = String.valueOf("0");
		results.put(key, interimResult);

		String xml = formatInventoryResponse(response, results);

		response.setResponseBody(xml);

		return response;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	private ResourceResponseInf postResource(ResourceRequestInf request)
			throws FxWebException {

		InventoryResponseImpl response = new InventoryResponseImpl();

		// Default to OK status
		response.setResponseCode(Response.Status.OK);

		// Always a multi-response type since multiple payments are always
		// possible.
		response.setMultiResponseType(true);

		// Get/set system id from request header.
		RequestProperties props = request.getRequestProperties();
		response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));

		response.setResponseType(ResponseTypes.response.InventoryResponse
				.toString());
		
		String orgId = props.getClientId();
		String acctsegId = ClientCache.getSegmentationId(orgId);
		log.info("Request - orgId : " + orgId + "acctsegId : " + acctsegId);
		if (acctsegId == null) {
			throw new FxWebException(
					"Could not retrieve segmentation id from cache for client id: "
							+ orgId, "PhoneInventoryUpdate");
		}

		// RequestValidator class already made sure there's really a payload.
		Document doc = request.getRequestProperties().getPayload();

		// Unmarshall the DOM document into a list of Inventory objects.
		InventoryRequest invr = getInventoryList(doc);

		// End-resultant map
		HashMap<String, HashMap> results = new HashMap();

		// Go through the embedded Inventory, and invoke SDK for each
		List<InventoryRequestObject> l = invr.getInventoryList().getInventory();

		Iterator i = l.iterator();
		int inventoryNo = 0;

		while (i.hasNext()) {

			InventoryRequestObject inventory = (InventoryRequestObject) i.next();

		//	 HashMap invResult = new SdkPrepaidInventoryUpdate().updatePhoneInventory(request,inventory, doc);

			// For each SDK result, add to our 'global' set of results with keys
			// of
			// "0", "1", etc. This is to simulate an SDK multi-result set that
			// returns multiple results in that fashion. For each invocation of
			// a Inventory transaction, there'll be just one response with a key
			// of
			// "0", so get that HashMap and plug into the global result with new
			// key.
			HashMap<String, String> interimResult = new HashMap();// =
																	// (HashMap)pmtResult.get("0");

			// Push the original Network id and telephone Number # into the
			// result map since it's and all the elements from the XML
			// needed in the response structure
			String teleNo = inventory.getTelephoneNumber().toString();
			String ntwrkId = inventory.getNetworkId();
			interimResult.put("NetworkId", ntwrkId);
			interimResult.put("TelephoneNumber", teleNo);
			interimResult.put("InvStatus", inventory.getInvStatus());
			interimResult.put("SalesChannelId", inventory.getSalesChannelId());
			interimResult.put("OperatorId", inventory.getOperatorId());
		
			String key = String.valueOf(inventoryNo);
			results.put(key, interimResult);

			inventoryNo++;

		}

		String xml = formatInventoryResponse(response, results);
		response.setResponseBody(xml);

		return response;

	}

	/**
	 * 
	 * @param doc
	 * @return
	 * @throws FxWebException
	 */
	private InventoryRequest getInventoryList(Document doc)
			throws FxWebException {

		// Unmarshall request into JavaBean wrappers
		InventoryRequest invr = null;

		try {

			JAXBContext context = JAXBContext
					.newInstance(InventoryRequest.class);
			Unmarshaller u = context.createUnmarshaller();

			invr = (InventoryRequest) u.unmarshal(doc);

		} catch (Exception e) {
			log.error(e);
			throw new FxWebException(e,
					"Unable to unmarshall inventory DOM document");
		}

		return invr;

	}

	/**
	 * Format Phone Inventory response. The response is quite simplistic, requiring just
	 * the correlation id from the original request, the tracking id from the
	 * SDK response, and a status structure. The end result is:
	 * 
	 * <InventoryResponse>
	 *     <Status> #Defines overall status
	 *       <Message>SUCCESS</message>
	 *       <Code>0</code>
	 *     </Status>
	 * 
	 *   <InventoryList size="10"> 
	 *        <Inventory sequence=1> 
	 *           <TelephoneNumber></TelephoneNumber>
	 *           <InvStatus></InvStatus>
	 *           <CreateDate></CreateDate>
	 *           <NetworkId></ NetworkId >
	 *           <SalesChannelId></SalesChannelId>
	 *           <OperatorId></ OperatorId>
	 *        </Inventory>
	 *   </InventoryList>
	 * </InventoryResponse>
	 * 
	 * 
	 * @param response
	 * @param results
	 * @return
	 */
	private String formatInventoryResponse(InventoryResponseImpl response,
			HashMap<String, HashMap> results) throws FxWebException {
		// Set status to 0=success, 1=failure
		String code = "0";
		String message = "SUCCESS";

		StringBuffer xmlResponse = new StringBuffer(FxWebConstants.XML_HEADER);
		xmlResponse.append("<InventoryResponse>");
		xmlResponse.append("<Status>");
		xmlResponse.append("<Message>" + message + "</Message>");
		xmlResponse.append("<Code>" + code + "</Code>");
		xmlResponse.append("</Status>");

		boolean inventoryOk = true;

		Response.Status errorStatus = Response.Status.OK;

		// Go through the Map of HashMaps, and generate a Inventory structure
		// for each
		Iterator i = results.keySet().iterator();
		while (i.hasNext()) {

			int count = 1;
			int seq = 1;
			String key = (String) i.next();
			HashMap result = (HashMap) results.get(key);

			// Get the tracking id, or possible SDK exception code
			String exception = null;

			// Get the original Network id and telephone no
			String ntwrkid = (String) result.get("NetworkId");
			String teleNo = (String) result.get("TelephoneNumber");

			// Get the other XML elements

			String invstatus = (String) result.get("InvStatus");
			String saleChnlId = (String) result.get("SalesChannelId");
			String oprtId = (String) result.get("OperatorId");

			// Get the system id (eg. K1, K2) already set in the response
			String system = response.getSystemId();

			// Generate all XML elements.

			xmlResponse.append("<InventoryList size=\"" + count++ + "\">");
			xmlResponse.append("<Inventory    sequence=\"" + seq + "\">");
			xmlResponse.append("<TelephoneNumber>" + teleNo
					+ "</TelephoneNumber>");
			xmlResponse.append("<InvStatus>" + invstatus + "</InvStatus>");
			xmlResponse
					.append("<CreateDate>2012-07-27T09:28:47.0Z</CreateDate>");
			xmlResponse.append("<NetworkId>" + ntwrkid + "</NetworkId>");
			xmlResponse.append("<SalesChannelId>" + saleChnlId
					+ "</SalesChannelId>");
			xmlResponse.append("<OperatorId>" + oprtId + "</OperatorId>");
			xmlResponse.append("<NetworkDeviceId>XXXX</NetworkDeviceId>");
			xmlResponse.append("</Inventory>");

		} // end while()

		xmlResponse.append("</InventoryList></InventoryResponse>");

		// if all inventory failed (didn't find one that was ok, set return
		// status to error condition.
		if (inventoryOk == false) {
			response.setResponseCode(errorStatus);
		}

		return xmlResponse.toString();
	}

}
