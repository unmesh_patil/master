package com.cycle30.fxweb.resource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;


import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.JobStatusRequestImpl;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;

import com.cycle30.fxweb.response.JobStatusResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.response.ResponseFormatter;
import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.sdkinterface.SdkJobStatus;
import com.cycle30.fxweb.util.FxWebUtil;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;


/** 
 * Kenan resource object
 */

public class JobStatusResourceImpl implements ResourceInf {
    
    public static Logger log = Logger.getLogger(JobStatusResourceImpl.class);   
    
    
    
    // Handle the resource request, returning a response instance.
    public ResourceResponseInf handleRequest(ResourceRequestInf request) throws FxWebException {
        
  
        // Get the original HTTP method from the 'http.method' header
        RequestProperties properties = request.getRequestProperties();
        String method = FxWebUtil.getRequestMethod(properties.getHeaders());
       
        
        // Instantiate the response        
        if (method.equalsIgnoreCase("GET")) {
            ResourceResponseInf response = getResource(request);
            return response;
        }
        
        
        return null; 
    }
    
    

    /**
     * Process the HTTP GET request. 
     *     
     * @param request
     * @return Resource response instance
     */
    private ResourceResponseInf getResource(ResourceRequestInf request) throws FxWebException {
   
        JobStatusResponseImpl response = new JobStatusResponseImpl(); 
        response.setResponseType(ResponseTypes.response.JobStatusResponse.toString());
        
        // Get system id from request header.
        RequestProperties props = request.getRequestProperties();
        response.setSystemId(FxWebUtil.getKenanVersion(props.getHeaders()));
        
        
        // Get Job id from path
        MultivaluedMap pathParams = props.getPathParams();
        String jobId = FxWebUtil.getPathParamValue(pathParams, "jobId");
        JobStatusRequestImpl jobStatusRequest = (JobStatusRequestImpl)request; // cast to concrete request type
        jobStatusRequest.setJobId(jobId);
        

        HashMap results = new HashMap();
        
        try { 
           results = new SdkJobStatus().getJobStatus(jobStatusRequest);
        }
        
        catch (FxWebException we) {

            String error = we.getFormattedMessage();
            response.setResponseBody(error);
            response.setResponseCode(Response.Status.INTERNAL_SERVER_ERROR);
            return response;
        }

        // See if an error occurred (eg. not-found). If so, get the error text
        // and throw an exception.  Start by getting top-level result, and look
        // for "ExceptionCode" value 
        HashMap map = (HashMap)results.get("0");
        if (map != null) {
            String  excCode = (String)map.get("ExceptionCode");
            if (excCode != null) {
               MappedError me = ResponseFormatter.getSdkError(results);
               if (me != null) {
                   throw new FxWebException(new Exception(me.getExternalError()), Response.Status.INTERNAL_SERVER_ERROR, "JobStatusRetrieval");
               }
            }
        }   
        
        formatJobImage(results, response);
        
        return response;
        
    }
    
    
    
    /**
     * 
     * @param results
     * @param response
     */
    private void formatJobImage (HashMap results, JobStatusResponseImpl response) 
                                             throws FxWebException
    {
        // Get the job image byte array, and convert to .jpg file

            
        HashMap map = (HashMap)results.get("0");
        
        if (map == null) {
            throw new FxWebException ("No result returned by SDK from job status request (no top level '0' in result map)", ""); 
        }
        
        // Get the byte array stream
        ByteArrayOutputStream baos = (ByteArrayOutputStream)map.get("JobImage");
        if (baos == null) {
            throw new FxWebException ("No 'JobImage' ByteArrayOutputStream returned by SDK in job status result.", ""); 
        }
        
        // Convert to array of bytes.
        byte[] data = null;
        data = baos.toByteArray();
        
        if (data.length==0) {
            throw new FxWebException ("No 'JobImage' data (ByteArrayOutputStream size=0) returned by SDK in job status result.", "");  
        }

        
        try {
            
            // Create random output file, and convert to .jpg
            Random random = new Random();
            long l = random.nextLong();
            
            // Don't allow "-" prefixed file names.
            if (l < 0) {
                l *= -1;
            }
            String fileName = l + ".jpg";
            String fnameB64 = l + ".b64";
            
            // Get output directory from env variable.
            String outputDir = System.getenv("JOB_STATUS_IMG_OUTPUT");
            if (outputDir == null) {
                throw new FxWebException("JOB_STATUS_IMG_OUTPUT env variable not defined", "ImageConversion");
            }
            
            File outputFile   = new File(outputDir + fileName);
            File outputFile64 = new File(outputDir + fnameB64);
            File outputFile2 = new File(outputDir + ".decode");
            
            log.info("Generating job status .jpg image file " + outputDir + fileName);
            
            FileOutputStream fos   = new FileOutputStream(outputFile);
            FileOutputStream fos64 = new FileOutputStream(outputFile64);
            //FileOutputStream fos2 = new FileOutputStream(outputFile2);
            
            byte[] buf = new byte[512];
            
            ByteArrayInputStream input = new ByteArrayInputStream(data);
            int bytesRead = 0;
            while((bytesRead = input.read(buf)) != -1){
                
                fos.write(buf, 0, bytesRead);
                
                byte[] base64Bytes = Base64.encodeBase64(buf);
                fos64.write(base64Bytes, 0, bytesRead);
                
                //byte[] decoded = Base64.decodeBase64(base64Bytes);
                //fos2.write(decoded, 0, bytesRead);
            }
            
            fos.flush();
            fos.close();
            
            fos64.flush();
            fos64.close();
            
            //fos2.flush();
            //fos2.close();
            
            response.setResponseBody(outputFile64);
            response.setResponseCode(Response.Status.OK);


        }
        catch (IOException ioe) {
            throw new FxWebException (ioe, Response.Status.INTERNAL_SERVER_ERROR, "JobImageConversion");
        }
 
    }
    
}
