package com.cycle30.fxweb.util;


import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cycle30.fxweb.exception.FxWebException;



public class DomUtils {

    private static final Logger log = Logger.getLogger(DomUtils.class);
    
    //--------------------------------------------------------------------------
    /** Get the child node list from underneath the parent element name passed.
     *  Recursively traverse DOM tree attempting to find it.
     * 
     * @param currentList Current node list to process.
     * @return parentElement The parent element name we're looking for.
     */
    public static synchronized NodeList getChildNodes (NodeList currentList,
                                             String parentElement) {
        
        
        //  iterate through list looking for element name.
        int i = currentList.getLength();
        for (int j = 0; j < currentList.getLength(); j++) {
            
            Node childNode = currentList.item(j);
            if (childNode == null) {
                return null;
            }
            
            // we only care about element nodes.
            if (childNode.getNodeType() != Node.ELEMENT_NODE)
                continue;
            
            // if we have a node name match, return the nodes child list
            String childNodeName = childNode.getNodeName();
            if (childNodeName != null && childNodeName.equalsIgnoreCase(parentElement)) {
                return childNode.getChildNodes();
            }
            
            // this child element name doesn't match, so recursively invoke method  
            // with it's child nodes.
            NodeList list = getChildNodes(childNode.getChildNodes(), parentElement);
            if (list != null) {
                return list;
            }

        }
        
        return null;
        
    }
    
    
    //---------------------------------------------------------------------------
    /** Get all values associated with an element name 
     * 
     * @param domDocument
     * @param elementName
     * 
     */
    public static synchronized ArrayList getElementValues(Document domDocument, String elementName)  {
       
       ArrayList elementValues = new ArrayList();
       
       // Get all nodes associated with the element name.
       NodeList nodelist = domDocument.getElementsByTagName(elementName);           
       if (null == nodelist.item(0)) {
               return elementValues;    
       }
       
       int count = nodelist.getLength();
       
       // Keep getting values until we run out
       for (int i = 0; i < nodelist.getLength(); i++) {

           Node node = nodelist.item(i);
           if (node == null) {
               continue;
           }
           Node child = node.getFirstChild();
           if (child == null) {
               continue;
           }
           String nodeValue = child.getNodeValue();
           elementValues.add(nodeValue);
            
       }  
       
       return elementValues;
       
   }
    
    
    //---------------------------------------------------------------------------
    /** Get single value associated with an element name 
     * 
     * @param dom
     * @param elementName
     * 
     */
    public static synchronized String getElementValue (Document dom, String elementName) {
        
        NodeList nodeList = dom.getElementsByTagName(elementName);
        if (nodeList == null) {
            return null;
        }
        
        Node node = nodeList.item(0);
        if (node == null) {
            return null;
        }
        
        String value = "";
        Node n = node.getFirstChild();
        if (n != null) {
            value = n.getNodeValue();
        }

        return value;
    }
    
    
    /**
     * 
     * @param dom
     * @return XML string
     */
    public static synchronized String domToXtring (Document dom) throws Exception {
        
        String xml = "";
        
        if (dom == null) {
            return "";
        }
        
        try {
            
            // transform DOM response to string
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(dom);
            
            trans.transform(source, result);
            xml = sw.toString(); 
        
        }  catch (Exception e) { 
            throw e;
        }
        
        return xml;
    }
    
    
    /**
     * 
     * @param xml
     * @return DOM document
     */
    public static synchronized Document stringToDom(String xml) throws FxWebException {
        
        Document domDocument = null;
        
        
        if (xml == null) {
            return null;
        }
        
        try {
            
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder        = factory.newDocumentBuilder();
            domDocument                    = builder.parse(new InputSource(new StringReader(xml)));
            
          } catch (Exception e) {
             log.error(e);
             throw new FxWebException(e, "Unable to convert xml string " + xml + " to a DOM document");
          }
          
          return domDocument;
    }
    
    
    
    /**
     * 
     * @param dom
     * @return attribute value
     */
    public static synchronized String getAttributeValue(Document dom, String elementName, String attrName) {
        
        String attrValue = "";
        
        // get all tags in the document with element name
        NodeList account = dom.getElementsByTagName(elementName);
        if (account == null) {
            return null;
        }
        
        for( int i = 0; i < account.getLength(); i++ ) {
          Element el = (Element) account.item( i );
          attrValue = el.getAttribute(attrName);
        }
        
        return attrValue;
    }
/**
 * 
 * @param xmlString
 * @return
 * @throws ParserConfigurationException
 * @throws SAXException
 * @throws IOException
 */
    public static Document getDocument(String xmlString)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlString));
        return docBuilder.parse(is);
    }
    
    /**
     * 
     * @param responseXML
     * @param Label
     * @param Unit
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
	public static HashMap<String, String> parseXMLForBalanceUnits(String responseXML) throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(responseXML);
		NodeList structList = doc.getElementsByTagName("struct");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> balanceResultMap = null;
		for (int i = 0; i < structList.getLength(); i++) {

			NodeList memberList = ((Element) structList.item(i))
					.getElementsByTagName("member");
			balanceResultMap = new HashMap<String, String>();
			for (int j = 0; j < memberList.getLength(); j++) {

				String Name = (String) ((Element) memberList.item(j))
						.getElementsByTagName("name").item(0)
						.getChildNodes().item(0).getNodeValue();
				String Value = (String) ((Element) memberList.item(j))
						.getElementsByTagName("value").item(0)
						.getChildNodes().item(0).getTextContent();

				if ("Label".equalsIgnoreCase(Name)) {
					balanceResultMap.put(Name, Value);
			    	} 
				if ("Unit".equalsIgnoreCase(Name)) {
					balanceResultMap.put(Name, Value);
			       }
			bundleResult.put(i, balanceResultMap);
		  }
		}
		
		 HashMap<String, String> consilidatedMap = new HashMap<String, String>();
		 for(int i=0;i<bundleResult.size();i++ ){
		 HashMap<?, ?> resultMap = (HashMap<?, ?>)bundleResult.get(i);
		       consilidatedMap.put((String)resultMap.get("Label"), (String)resultMap.get("Unit"));
			}
		 return consilidatedMap;
	}
	
	
	
	public static HashMap<Integer, HashMap<String, String>> parseXMLBlackHawkResponseXML(String responseXML) throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(responseXML);
		NodeList structList = doc
				.getElementsByTagName("transaction");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> blackhawkrspMap = null;
		for (int i = 0; i < structList.getLength(); i++) {

			blackhawkrspMap = new HashMap<String,String>();

			
			
			String FaceValue = "0";
			String UPC = "";	
			String SerialNumber="";
			
		 
		
			blackhawkrspMap.put("RespMsg", "Card is Active");

			if (((Element) structList.item(i)).getElementsByTagName("transactionUniqueId")
					.item(0).getChildNodes().item(0) != null) {
				SerialNumber = (String) ((Element) structList.item(i))
						.getElementsByTagName("transactionUniqueId").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			blackhawkrspMap.put("SerialNumber", SerialNumber);

			if (((Element) structList.item(i))
					.getElementsByTagName("balanceAmount").item(0).getChildNodes()
					.item(0) != null) {
				FaceValue = (String) ((Element) structList.item(i))
						.getElementsByTagName("balanceAmount").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			blackhawkrspMap.put("FaceValue", FaceValue);

			if (((Element) structList.item(i)).getElementsByTagName("productId")
					.item(0).getChildNodes().item(0) != null) {
				UPC = (String) ((Element) structList.item(i))
						.getElementsByTagName("productId").item(0).getChildNodes()
						.item(0).getNodeValue();
			}
			blackhawkrspMap.put("UPC", UPC);
			

			bundleResult.put(0, blackhawkrspMap);

		}
			return bundleResult; 
			 
	}

	
	
	
	
	public static HashMap<Integer, HashMap<String, String>> parseXMLIncommResponseXML(String responseXML) throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(responseXML);
		NodeList structList = doc
				.getElementsByTagName("TransferredValueTxnResp");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> incomrspMap = null;
		for (int i = 0; i < structList.getLength(); i++) {

			incomrspMap = new HashMap<String,String>();

			String RespCode = "";
			String RespMsg = "";
			String FaceValue = "";
			String UPC = "";	
			String SerialNumber="";
			 
				  
			if (((Element) structList.item(i)).getElementsByTagName("RespCode")
					.item(0).getChildNodes().item(0) != null) {
				RespCode = (String) ((Element) structList.item(i))
						.getElementsByTagName("RespCode").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			incomrspMap.put("RespCode", RespCode);

			if (((Element) structList.item(i)).getElementsByTagName("RespMsg")
					.item(0).getChildNodes().item(0) != null) {
				RespMsg = (String) ((Element) structList.item(i))
						.getElementsByTagName("RespMsg").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			incomrspMap.put("RespMsg", RespMsg);

			if (((Element) structList.item(i))
					.getElementsByTagName("FaceValue").item(0).getChildNodes()
					.item(0) != null) {
				FaceValue = (String) ((Element) structList.item(i))
						.getElementsByTagName("FaceValue").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			incomrspMap.put("FaceValue", FaceValue);

			if (((Element) structList.item(i)).getElementsByTagName("UPC")
					.item(0).getChildNodes().item(0) != null) {
				UPC = (String) ((Element) structList.item(i))
						.getElementsByTagName("UPC").item(0).getChildNodes()
						.item(0).getNodeValue();
			}
			incomrspMap.put("UPC", UPC);
			
			
 NodeList extensionList =  ((Element) structList.item(i)).getElementsByTagName("Extension");
			 
			 for(int j=0;j<extensionList.getLength();j++){
			if(((Element) extensionList.item(j)).getElementsByTagName("Name").  
					item(0).getChildNodes().item(0)!= null){
			String	name = (String) ((Element) extensionList.item(j)).getElementsByTagName("Name").  
						item(0).getChildNodes().item(0).getNodeValue();  
		
			if("SerialNumber".equalsIgnoreCase(name)){
				 if(((Element) extensionList.item(j)).getElementsByTagName("Value").  
				            item(0).getChildNodes().item(0) !=null ) {
					    SerialNumber = ((String)((Element) extensionList.item(j)).getElementsByTagName("Value").item(0).getChildNodes().item(0).getNodeValue());
				 }
				 incomrspMap.put("SerialNumber",SerialNumber);
		    }
		}
			 
	}
			

			bundleResult.put(0, incomrspMap);

		}
			return bundleResult; 
			 
	}


	public static HashMap<Integer, HashMap<String, String>> parseXMLForSubcriberDetails(String responseXML) throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(responseXML);
		NodeList structList = doc.getElementsByTagName("struct");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> subscriberDetailsResultMap = null;
		subscriberDetailsResultMap = new HashMap<String, String>();
		for(int i=0; i<structList.getLength();i++){
			  
	        NodeList memberList =  ((Element) structList.item(i)).getElementsByTagName("member");
	     
	        for(int j=0; j<memberList.getLength();j++){
	    
			  
				    String Name = (String) ((Element) memberList.item(j)).getElementsByTagName("name").  
				                    item(0).getChildNodes().item(0).getNodeValue();  
				    String Value = (String) ((Element) memberList.item(j)).getElementsByTagName("value").  
				            item(0).getChildNodes().item(0).getTextContent();
				  
				    if ("SubscriberProfileLabel".equalsIgnoreCase(Name)) {
				    	subscriberDetailsResultMap.put(Name, Value);
				    	} 
					if ("SubscriberStateLabel".equalsIgnoreCase(Name)) {
						subscriberDetailsResultMap.put(Name, Value);
				       }
					if ("CLI".equalsIgnoreCase(Name)) {
						subscriberDetailsResultMap.put(Name, Value);
				       }
					if ("SubscriberLineId".equalsIgnoreCase(Name)) {
						subscriberDetailsResultMap.put(Name, Value);
				       }
					if ("NumberSubType".equalsIgnoreCase(Name)) {
						subscriberDetailsResultMap.put(Name, Value);
				       }
	           }
	       
		}
		 bundleResult.put(0, subscriberDetailsResultMap);
	
		 return bundleResult;
	}

	
	
	public static HashMap<String, List<String>> parseXMLForSubscriberProfiles(
			String responseXML) throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(responseXML);
		NodeList structList = doc.getElementsByTagName("struct");
		List<HashMap<String, String>> bundleResultList= new ArrayList<HashMap<String,String>>();
		HashMap<String, String> profilebalanceTypes = null;
		Set<String> setProfileBalanceLabel = new HashSet<String>();
		for (int i = 0; i < structList.getLength(); i++) {

			NodeList memberList = ((Element) structList.item(i))
					.getElementsByTagName("member");
			profilebalanceTypes = new HashMap<String, String>();

			for (int j = 0; j < memberList.getLength(); j++) {

				String Name = (String) ((Element) memberList.item(j))
						.getElementsByTagName("name").item(0).getChildNodes()
						.item(0).getNodeValue();
				String Value = (String) ((Element) memberList.item(j))
						.getElementsByTagName("value").item(0).getChildNodes()
						.item(0).getTextContent();
				if ("BalanceTypeLabel".equalsIgnoreCase(Name)) {
					profilebalanceTypes.put(Name, Value);
				} else if ("SubscriberProfileLabel".equalsIgnoreCase(Name)) {
					profilebalanceTypes.put(Name, Value);
					setProfileBalanceLabel.add(Value);
				}
			}

			if (profilebalanceTypes.get("BalanceTypeLabel") != null) {
				bundleResultList.add(profilebalanceTypes);
			}
		}

		List<String> balanceTypesList = null;
		HashMap<String, List<String>> profileBalanceMapList = new HashMap<String, List<String>>();
		for (String profileLabel : setProfileBalanceLabel) {
			balanceTypesList = new ArrayList<String>();
				for (HashMap<String, String> balnceResultMap : bundleResultList) {
					if(balnceResultMap.containsValue(profileLabel)){
						balanceTypesList.add(balnceResultMap.get("BalanceTypeLabel"));
					}
						
				}
				profileBalanceMapList.put(profileLabel, balanceTypesList);
			}
		return profileBalanceMapList;
	}
	
	
	
	public static HashMap<Integer, HashMap<String, String>> parseXMLForGCIRetreiveDetails(
			String responseXML) throws ParserConfigurationException,
			SAXException, IOException {
		Document doc = getDocument(responseXML);
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		NodeList structList = doc.getElementsByTagName("Inventory");
		HashMap<String, String> invList = null;
		if (structList != null && structList.getLength() != 0) {
			for (int i = 0; i < structList.getLength(); i++) {

				invList = new HashMap<String, String>();
				String telePhNmbr = "";
				String createDate = "";
				String statusValue = "";
				String ntwrkId = "";
				String operatorId = "";
				String ntwrkDeviceId = "";
				String salesChannelId = "";

				if (((Element) structList.item(i))
						.getElementsByTagName("Status").item(0).getChildNodes()
						.item(0) != null) {
					statusValue = (String) ((Element) structList.item(i))
							.getElementsByTagName("Status").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				invList.put("Status", statusValue);

				if (((Element) structList.item(i))
						.getElementsByTagName("TelephoneNumber").item(0)
						.getChildNodes().item(0) != null) {
					telePhNmbr = (String) ((Element) structList.item(i))
							.getElementsByTagName("TelephoneNumber").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				invList.put("TelephoneNumber", telePhNmbr);
				
				
				if (((Element) structList.item(i))
						.getElementsByTagName("NetworkId").item(0) != null) {
					if (((Element) structList.item(i))
							.getElementsByTagName("NetworkId").item(0)
							.getChildNodes().item(0) != null) {
						ntwrkId = (String) ((Element) structList.item(i))
								.getElementsByTagName("NetworkId").item(0)
								.getChildNodes().item(0).getTextContent();
					}

				}
				
				invList.put("NetworkId", ntwrkId);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("CreateDate").item(0) != null) {
				if (((Element) structList.item(i))
						.getElementsByTagName("CreateDate").item(0)
						.getChildNodes().item(0) != null) {
					createDate = (String) ((Element) structList.item(i))
							.getElementsByTagName("CreateDate").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				
			}
				invList.put("CreateDate", createDate);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("OperatorId").item(0) != null) {			
				
				if (((Element) structList.item(i))
						.getElementsByTagName("OperatorId").item(0)
						.getChildNodes().item(0) != null) {
					operatorId = (String) ((Element) structList.item(i))
							.getElementsByTagName("OperatorId").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				
			}
				invList.put("OperatorId", operatorId);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("NetworkDeviceId").item(0) != null) {
				
				if (((Element) structList.item(i))
						.getElementsByTagName("NetworkDeviceId").item(0)
						.getChildNodes().item(0) != null) {

					ntwrkDeviceId = (String) ((Element) structList.item(i))
							.getElementsByTagName("NetworkDeviceId").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				
				}
				invList.put("NetworkDeviceId", ntwrkDeviceId);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("SalesChannelId").item(0) != null) {
				if (((Element) structList.item(i))
						.getElementsByTagName("SalesChannelId").item(0)
						.getChildNodes().item(0) != null) {

					salesChannelId = (String) ((Element) structList.item(i))
							.getElementsByTagName("SalesChannelId").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				}
				invList.put("SalesChannelId", salesChannelId);
				bundleResult.put(i, invList);
			}
			
		}
		return bundleResult;
	}

}