package com.cycle30.fxweb.util;

import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate.encryptor.HibernatePBEEncryptorRegistry;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.AccountResponseImpl;
import com.cycle30.fxweb.response.OrderResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.sdkinterface.MappedError;



/**
 * 
 * FX Web enablement general-purpose utility class.
 *
 */
public class FxWebUtil {
    
    public static Logger log = Logger.getLogger(FxWebUtil.class); 
    
    
    //--------------------------------------------------------------------------
   /**
    * Get customer number depending on the HTTP method type.
    * 
    * @param pathParams URA path parameters
    * @param queryParams URI query parameters
    * @param dom document
    * @return http method
    */
   public static synchronized String getCustomerNumber(MultivaluedMap<String, String> pathParams, 
                                                           MultivaluedMap<String, String> queryParams, 
                                                           Document dom) 
  {  
       

       String customerNo = getCustomerNumberFromPathParam(pathParams);
       
       // if null, see if it was a passed as a query param, eg. &accountNo=12346677
       if (customerNo == null) {
           customerNo = getCustomerNumberFromQueryParam(queryParams);
       }
       
       if (customerNo == null && dom != null) {
           customerNo = getCustomerNumberFromDom(dom);
       }

       return customerNo;
   }
   
   
    //--------------------------------------------------------------------------
    /**
     * Get customer number from URI path param, eg. /account/123456789
     * 
     * @param pathParams Map
     * @return customer number
     */
     public static synchronized String getCustomerNumberFromPathParam(Map pathParams) {
        
         String customerNo = null;
         
         if (pathParams == null) {
             return null;
         }
         
         // The backing Map type is MultiValuedMap, and each map entry
         // is stored as an ArrayList.  Should only ever have 1 entry.
         ArrayList list = (ArrayList)pathParams.get(FxWebConstants.ACCOUNT_NO_IDENTIFIER); 
         
         if (list != null && list.size() > 0) {
             customerNo = (String)list.get(0);
         }
         
         return customerNo;
     }
     
     
     //--------------------------------------------------------------------------
     /**
      * Get URI path param value, eg. /account/123456789
      * 
      * @param pathParams Map
      * @return customer number
      */
      public static synchronized String getPathParamValue(Map pathParams, String paramName) {
         
          String paramValue = null;
          
          if (pathParams == null) {
              return null;
          }
          
          // The backing Map type is MultiValuedMap, and each map entry is an ArrayList.  
          ArrayList list = (ArrayList)pathParams.get(paramName); 
          
          if (list != null && list.size() > 0) {
              paramValue = (String)list.get(0);
          }
          
          return paramValue;
      }
     
     //--------------------------------------------------------------------------
     /**
      * Get customer number from DOM document (this will be for POST XML payload 
      * only - PUT has the customer number on the URI path)
      * 
      * @param queryParams Map
      * @return customer number
      */
      public static synchronized String getCustomerNumberFromQueryParam(Map queryParams) {
         
          String customerNo = null;
          
          if (queryParams == null) {
              return null;
          }
          
          // Dump the parameters that were passed in the URI. The backing Map type for each entry, 
          // is a LinkedList.  Should only ever have 1 element for each list.
           LinkedList list = (LinkedList)queryParams.get(FxWebConstants.ACCOUNT_NO_IDENTIFIER); 
          
          if (list != null && list.size() > 0) {
              customerNo = (String)list.get(0);
          }
          
          return customerNo;
      }
      
     
    //--------------------------------------------------------------------------
    /**
     * Get customer number from DOM document (this will be for POST XML payload 
     * only - PUT has the customer number on the URI path)
     * 
     * @param dom
     * @return customer number
     */
     public static synchronized String getCustomerNumberFromDom(Document dom) {
        
         String customerNo = DomUtils.getElementValue(dom, FxWebConstants.DOM_ACCOUNT_NO_IDENTIFIER);
         
         return customerNo;
     }
      

    //--------------------------------------------------------------------------
    /**
    * Get customer number from either XML element of XML attribute.  The number
    * will be in the XML "<Account clientId=" attribute when adding an account, or 
    * the XML <ClientAccountId> when adding a plan. 
    * 
    * @param dom document
    * @return http method
    */
    public static synchronized String getClientAccountId(Document dom) throws FxWebException {      
       String customerNo = "";
       customerNo = DomUtils.getElementValue (dom, "<ClientAccountId>");
       
       if (customerNo == null || customerNo.length()==0) {
           customerNo = DomUtils.getAttributeValue(dom, "Account", "ClientId");
       }

       return customerNo;
    }
   

   
     

     //--------------------------------------------------------------------------
    /**
     * Get the HTTP method
     * 
     * @param headers
     * @return http method
     */
    public static synchronized String getRequestMethod(HttpHeaders headers) {  
 
        String method = getHeaderValue(headers, "http.method");

        return method;
    }
    

    
    
    //--------------------------------------------------------------------------
    /**
     * Get the Kenan version via custom "kenan.version" header.
     * Note - the header is added in WebServiceBroker, and is only relevant
     *        for the GCI implementation of FxWebEnablement.
     * 
     * @param headers
     * @return http method
     */
    public static synchronized String getKenanVersion(HttpHeaders headers) {          
        
        if (headers == null) {
            return "K2";
        }
        
        String version = getHeaderValue(headers, "kenan.version");
        if (version == null) {
            return "K2";
        }

        return version;
    }
    
    
    //--------------------------------------------------------------------------
    /**
     * 
     * @param headers
     * @param key
     * @return header value
     */
    public static synchronized String getHeaderValue(HttpHeaders headers, String key)  {  
    
        for (String header : headers.getRequestHeaders().keySet()) {
            
            String value = headers.getRequestHeader(header).get(0);
            if (header.equalsIgnoreCase(key)) {
                return value;
            }
         }
        
        return null;
    }
    

    
    //--------------------------------------------------------------------------
    /**
     * 
     * @param headers
     */
    public static synchronized HashMap getHeaderMap(HttpHeaders headers)  {  
        
        HashMap<String,String> map = new HashMap<String,String>();
        
        if (headers == null) {
            return map;
        }
            
        for (String header : headers.getRequestHeaders().keySet()) {
           String value = headers.getRequestHeader(header).get(0);
           map.put(header, value);
        }

        return map;
        
    }
    

    //--------------------------------------------------------------------------
    /**
     * Recursively traverse the map, looking for nested Maps and transforming
     * all items into XML elements
     * 
     *  @param map
     *  @param xml StringBuffer
     */
    @SuppressWarnings("rawtypes")
    public static synchronized void mapToXml(HashMap<String, ?> map, StringBuffer xml) {
        
  
       for( Map.Entry<String,?> e : map.entrySet() ) {
           
           String key   = e.getKey();
           Object value = e.getValue();    
           
           
           // Omit SDK "#NAME" and "ObjectType" elements. They return the name of 
           // the SDK objects, and not something a client will need/care about.
           // They also can't be filtered in the SDK.
           if (key.equals("#NAME") || key.equalsIgnoreCase("ObjectType")) {
              continue;
           }
           
           // Omit SDK "TotalCount" since it can occur in multiple instances, but already 
           // derived as a root-level XML element in ResponseFormatter
           if (key.equals("TotalCount")) {
               continue;
            }
           
           key   = cleanupXmlElementName(key);
           value = cleanupXmlElementValue(value);      

           // Check for extended data elements/enumerations, which have sub-structures.  The 
           // extended data is returned as an ArrayList of HashMap instances.
           if (key.equalsIgnoreCase("ExtendedDataList") || key.equalsIgnoreCase("EnumerationDataList")) {
               
               if ( value != null && value instanceof ArrayList ==true) {
                   ArrayList al = (ArrayList)value;
                   String data = "";
                   
                   if (key.equalsIgnoreCase("ExtendedDataList")) {
                       data = getExtendedDataValues(al);
                   } else {
                       data = getEnumerationDataValues(al, xml);
                   }
                   
                   xml.append(data);
                   
               }
               continue;
           }
           
           
           // Recurse through embedded maps
           if (value instanceof Map) {
               xml.append("<" + key + ">");
               HashMap<String, ?> hm = (HashMap<String, ?>)value;
               FxWebUtil.mapToXml(hm, xml);
               xml.append("</" + key + ">");
               continue;               
           }
           
           
           xml.append("<" + key + ">" + value + "</" + key + ">");
           
        }
                    
    }
    
    
    /**
     * Create a structure of:
     * 
     * <ExtendedDataList>
     *   <ExtendedData>
     *      <Id>10</Id>
     *      <Name>TelcoAccount</Name>
     *      <Value>123123</Value>
     *   </ExtendedData>
     *  ...
     * </ExtendedDataList>
     * 
     * @param extendedData
     * @return String of XML formatted extended data elements
     */
    private static synchronized String getExtendedDataValues(ArrayList extendedData) {
        
        StringBuffer sb = new StringBuffer("<ExtendedDataList>");
        
        // Extended data is an array of HashMaps, and the value as a Java object of 
        // Boolean, Integer, etc. So use 'toString()' to get the actual string value.
        for (Object hm : extendedData) {
            
            if (hm instanceof HashMap) {
                
                HashMap map  = (HashMap)hm;
                String name  = (String)map.get("ParamName");
                Object i     = map.get("ParamId");
                String id    = i.toString();
                Object value = map.get("ParamValue");
  
                name  = cleanupXmlElementName(name);
                value = cleanupXmlElementValue(value);
                
                //log.info("Extended data found. Id: " + id + " Name: " + name + " Value: " + value.toString());   
                
                sb.append("<ExtendedData>");
                sb.append("<Id>"    + id    + "</Id>");
                sb.append("<Name>"  + name  + "</Name>");
                sb.append("<Value>" + value.toString() + "</Value>");
                sb.append("</ExtendedData>");
            }
        }
        
        sb.append("</ExtendedDataList>");
        
        return sb.toString();
        
    }
    

    
    
    /**
     * Ceate a structure of:
     * 
     * <EnumerationDataList>
     *   <EnumerationData>
     *      <Id>10</Id>
     *      <Name>TelcoAccount</Name>
     *      <Value>123123</Value>
     *   </EnumerationData>
     *  ...
     * </EnumerationDataList>
     * 
     * @param EnumerationDataList
     * @return String of XML formatted enumeration elements
     */
    private static synchronized String getEnumerationDataValues(ArrayList EnumerationDataList, 
                                                                   StringBuffer xml) {
        
        StringBuffer sb = new StringBuffer("<EnumerationDataList>");
        
        // Enumeration data is an array of HashMaps, and the value as a Java object of 
        // Boolean, Integer, etc. So use 'toString()' to get the actual string value.
        for (Object hm : EnumerationDataList) {
            
            if (hm instanceof HashMap) {
                
                HashMap map  = (HashMap)hm;
                String name  = (String)map.get("Name");
                Object i     = map.get("Id");
                String id    = i.toString();
                Object value = map.get("Value");
  
                name  = cleanupXmlElementName(name);
                value = cleanupXmlElementValue(value);
                
                
                sb.append("<EnumerationData>");
                sb.append("<Id>"    + id    + "</Id>");
                sb.append("<Name>"  + name  + "</Name>");
                sb.append("<Value>" + value.toString() + "</Value>");
                sb.append("</EnumerationData>");
            }
        }
        
        sb.append("</EnumerationDataList>");
        
        return sb.toString();
        
    }
    

    
    
    
    
    
    /**
     * Get the resource type (e.g. "account", "service", etc)
     * 
     * @param response
     */
    public static synchronized String getResourceType(ResourceResponseInf response) {     
        
        if (response instanceof AccountResponseImpl) {
            return "Account";
        }
        if (response instanceof OrderResponseImpl) {
            return "Order";
        }

        return "";
        
    }
    
    

    /**
     * 
     * @param msg
     * @param responseCode
     * @param sourceSystem
     * @return XML error message
     */
    public static synchronized String formatXmErrorlMessage(String msg, Response.Status responseCode, String sourceSystem) {
        
        StringBuffer xmlMessage = new StringBuffer();
        
        xmlMessage.append("<error>");
        xmlMessage.append("<code>" + responseCode.getStatusCode() + "</code>");
        xmlMessage.append("<source>" + sourceSystem + "</source>");
        xmlMessage.append("<description>" + msg + "</description>");
        xmlMessage.append("</error>");
        
        return xmlMessage.toString();
        
    }
    
    
    
    //--------------------------------------------------------------------------
    /**
     * Get specific query param value
     * 
     * @param queryParams
     * @param paramName
     * @return param value
     */
    public static synchronized String getQueryParamValue(MultivaluedMap<String, String> queryParams, String paramName)  {  
    
        if (queryParams == null) {
            return null;
        }
        
        Iterator<String> i = queryParams.keySet().iterator();
        

        while (i.hasNext()) {
           
           String key = (String)i.next();            
           LinkedList<?> ll = (LinkedList<?>)queryParams.get(key);
           String value = (String)ll.get(0);
           
           if (key.equalsIgnoreCase(paramName)) {
              return value;
           }

        }
        
        return null;
        
    }
    
    
    
     
    /**
     * 
     * @param ui
     * @return formatted list with newline chars between each entry
     */
    public static synchronized String getFormattedQueryParamList(UriInfo ui) {

        // Get the parameters that were passed in the URI. The backing Map type for each entry, 
        // is a LinkedList.  Should only ever have 1 element for each list.
        MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
        Iterator<String> i = queryParams.keySet().iterator();
        StringBuffer sb = new StringBuffer("");
        
        while (i.hasNext()) {
            String key = (String)i.next();            
            LinkedList<?> ll = (LinkedList<?>)queryParams.get(key);
            String value = (String)ll.get(0);
            sb.append("   " + key + "=" + value + "\n");
        }
        
        return sb.toString();
    }
    
    
    /**
     * Strip out characters that result in invalid XML element names.
     * 
     * @param elementName
     * @return
     */
    private static synchronized String cleanupXmlElementName(String elementName) {
        
        String xmlElement = "";
        
        String temp1 = elementName.replaceAll(" ", "");
        String temp2 = temp1.replaceAll("/", "");
        
        xmlElement = temp2; 
        
        return xmlElement;
    }
    
    
    
    /**
     * Clean up XML values where appropriate.
     * 
     * @param elementName
     * @return
     */
    private static synchronized Object cleanupXmlElementValue(Object elementValue) {
        
        if (elementValue instanceof String ==false) {
            return elementValue;
        }
            
        
        if (elementValue==null || elementValue.equals("null") ) {
            elementValue="";
        }
        
        // Get rid of embedded '&' chars
        String temp = (String)elementValue;  // cast to String
        if (temp.contains("&") ) {
            elementValue = temp.replaceAll("&", "");
        }
        
        return elementValue;
    }
    
    
    
    /**
     * 
     * @param xml
     * @param elementName
     * @return XML element value
     */
    public static synchronized String getXmlStringElementValue (String xml, String elementName) {
        
        if (xml == null || xml.length()==0) {
            return "";
        }
        
        
        String beginElement = "<" + elementName + ">";
        int start = xml.indexOf(beginElement);
        
        String endElement = "</" + elementName + ">";
        int end   = xml.indexOf(endElement);
        
        if (start == -1 || end == -1) {
            return null;
        }
        
        // Get description w/i the <description> tags
        start += (elementName.length()) + 2; // start of data = start element (+ 2 for '<' and '>' chars)
        String value = xml.substring(start, end);
        
        return value;
        
    }
    
    
    

    
    
    
    /**
     * 
     * @return decrypted password
     */
    public static synchronized String decryptPassword(String encryptedPassword) throws FxWebException {
        
        // decrypt the password.
        
        String decryptedPwd = null;
        
        try {
                      
            String decryptionPassword = System.getenv(FxWebConstants.ENCRYPTION_KEY_ENV_VAR);
            if (decryptionPassword == null) {
                throw new FxWebException(FxWebConstants.ENCRYPTION_KEY_ENV_VAR + " env variable not found!", "SessionManager");
            }

            StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
            strongEncryptor.setAlgorithm("PBEWithMD5AndDES");
            strongEncryptor.setPassword(decryptionPassword);
            HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
            registry.registerPBEStringEncryptor("configurationHibernateEncryptor", strongEncryptor);
            
            decryptedPwd = strongEncryptor.decrypt(encryptedPassword);

        } 
        catch (Exception e) {
            throw new FxWebException("Error decrypting db password.", "SessionManager");
        }
        
        return decryptedPwd;
        
    }
    
    
    
    //--------------------------------------------------------------------------
    /**
     * Get MD5 hash from salted value, and validate it against what client provided
     * 
     * @param headers
     * @param clientDigest
     * @return true if it matches C30 hash.
     */
    public static synchronized boolean validateDigest(HttpHeaders headers, String clientDigest) {

        try {

            String clientId = FxWebUtil.getHeaderValue(headers, "X-Org-Id");
            
            // Get client 'secret' hash value from CLIENT table cache.
            String secretHash = ClientCache.getConsumerSecret(clientId);
            if (secretHash == null) {
                log.error("\"Secret key not found in db for validating X-Digest value!");
                return false;
            }
            
            // Get Date header value and parse into YYYYMMDD format
            String dateString = FxWebUtil.getHeaderValue(headers, "Date");
            //SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
            //Date d = formatter.parse(dateString);
            /** GET ONLY GMT DATE **/
            String newDateString = dateString.substring(5,16);
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
            Date d = formatter.parse(newDateString);
            formatter = new SimpleDateFormat("yyyyMMdd");
            String yyyymmdd = formatter.format(d);
            
            // Full 'salted' value:
            String saltValue = clientId + secretHash + yyyymmdd;
            
            log.info("Validating client " + clientId + " X-Digest value of " + clientDigest);

            // get hash value
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.reset();
            byte[] nameBytes = saltValue.getBytes();
            digest.update(nameBytes);
            byte[] hash = digest.digest();

            // convert hash bytes to hex string value
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xFF & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }

            String md5Digest = hexString.toString();
            
            if (md5Digest.equalsIgnoreCase(clientDigest)) {
                return true;
            }

        } catch (Exception e) {
            log.error(e);
            return false;
        }

        return false;
    }
    
    
    
    /**
     * Get one or more 'key' query param values for a service request, 
     * eg. service/9071234567/healthcheck?key=BillingStatus&key=NetworkStatus  
     * 
     * @param queryParams
     * @return list of key values
     */
    public static synchronized ArrayList<String> getServiceKeyParams(MultivaluedMap<String, String> queryParams) {
        
        ArrayList<String> keyList = new ArrayList<String>();
        
        Iterator i = queryParams.keySet().iterator();
        
        while (i.hasNext()) {
            
            String s = (String)i.next();
            if (s.equalsIgnoreCase("key")) {
                
                List<String> paramList = queryParams.get(s);
                if (paramList != null && paramList.size() > 0) {
                   for (int j=0; j<paramList.size(); j++)  {
                       String value = paramList.get(j);
                       keyList.add(value);
                   }
                   break; //  done retrieveing all 'key' query param values.
                }
            }
            
        }
        
        return keyList;
        
    }
    
    /**
     * Transform the one-off AWN order status update payload into the 
     * API standardized format.
     * 
     * AWN format:
     *  <provisioningResults>
     *     <idRequest>ACS.10_2</idRequest>
     *     <errNumber>231</errNumber>
     *     <errMessage>SPS is demonstrating a sample error type</errMessage>
     *     <port>
     *        <idPort></idPort>
     *        <ported></ported>
     *        <direction></direction>
     *        <tn></tn>
     *        <portInCarrier></portInCarrier>
     *        <portOutCarrier></portOutCarrier>
     *        <requestedAction></requestedAction>
     *		  <portingPoolTN></portingPoolTN>        
     *     </port>
     *  </provisioningResults>
     *  
     * API standard format:
     * <OrderUpdateRequest>
     *     <ProviderId>Providerxyz</ProviderId>
     *     <ParamList>
     *            <Key></Key>
     *            <Value></Value>
     *     </ParamList>
     *     <OrderList>
     *         <Order>
     *            <OrderId>OrdId180</OrderId>
     *            <ExtCorrelationId>01928438</ExtCorrelationId>
     *            <Status>SUCCESS</Status>
     *            <Error>0</Error>
     *            <PortingDetails>
     *                <PortingId></PortingId>
     *                <PortedFlag></PortedFlag>
     *                <Direction></Direction>
     *                <TelephoneNo></TelephoneNo>
     *                <PortInCarrier></PortInCarrier>
     *                <PortOutCarrier></PortOutCarrier>
     *                <Action></Action>
     *           	  <PortingPoolTN></PortingPoolTN>     
     *            </PortingDetails>
     *        </Order>
     *    </OrderList>
     * </OrderUpdateRequest>
     * 
     * 
     * @param request
     * @return transformed Document
     */
    public static synchronized Document transformAwnPayload(ResourceRequestInf request) throws FxWebException {
        
        Document newDom = null;
        String awnXml = null; 
        
        Document awnDom = request.getRequestProperties().getPayload();
        
        try {
            awnXml = DomUtils.domToXtring(awnDom);
        } catch (Exception e) {
            throw new FxWebException(e,"");
        }
        
        String id     = DomUtils.getElementValue(awnDom, "idRequest");
        String errNo  = DomUtils.getElementValue(awnDom, "errNumber");
        String errMsg = DomUtils.getElementValue(awnDom, "errMessage");
        String carrier = null;
        
        if (errNo == null || errNo.equalsIgnoreCase("0")) {
            errNo = "SUCCESS";
        }

        //Get the provider id based on the request id
        if (id.indexOf(FxWebConstants.ACS_CLIENT_ID_VALUE) == 0) {
        	carrier = FxWebConstants.ACS_CLIENT_ID_VALUE;
        } else if (id.indexOf(FxWebConstants.GCI_CLIENT_ID_VALUE) == 0) {
        	carrier = FxWebConstants.GCI_CLIENT_ID_VALUE;
        }
        
        StringBuffer sb = new StringBuffer(FxWebConstants.XML_HEADER);
        sb.append("<OrderUpdateRequest>");
        sb.append("<ProviderId>"+carrier+"</ProviderId>");
        sb.append("<ParamList/>");
        
        sb.append("<OrderList>");
        sb.append("<Order>");
        sb.append("<OrderId>"          + id     + "</OrderId>");
        sb.append("<ExtCorrelationId>" + id     + "</ExtCorrelationId>");
        sb.append("<Status>"           + errNo  + "</Status>");
        sb.append("<Error>"            + errMsg + "</Error>");
        
        //----- AWN-specific porting properties
        String portId         = DomUtils.getElementValue(awnDom, "idPort");
        String ported         = DomUtils.getElementValue(awnDom, "ported");
        String direction      = DomUtils.getElementValue(awnDom, "direction");
        String telephoneNo    = DomUtils.getElementValue(awnDom, "tn");
        String portInCarrier  = DomUtils.getElementValue(awnDom, "portInCarrier");       
        String portOutCarrier = DomUtils.getElementValue(awnDom, "portOutCarrier");
        String action         = DomUtils.getElementValue(awnDom, "requestedAction");
        String poolTN         = DomUtils.getElementValue(awnDom, "portingPoolTN");        
        if (portId != null) {
          sb.append("<PortingDetails>");
             sb.append("<PortingId>"      + portId         + "</PortingId>");
             sb.append("<PortedFlag>"     + ported         + "</PortedFlag>");
             sb.append("<Direction>"      + direction      + "</Direction>");
             sb.append("<TelephoneNo>"    + telephoneNo    + "</TelephoneNo>");
             sb.append("<PortInCarrier>"  + portInCarrier  + "</PortInCarrier>");
             sb.append("<PortOutCarrier>" + portOutCarrier + "</PortOutCarrier>");
             sb.append("<Action>"         + action         + "</Action>");
             sb.append("<PortingPoolTN>"  + poolTN         + "</PortingPoolTN>");             
          sb.append("</PortingDetails>");
        }
        //-----
        
        sb.append("</Order>");
        sb.append("</OrderList>");
        sb.append("</OrderUpdateRequest>");
        
        String xml = sb.toString();        
        newDom = DomUtils.stringToDom(xml);
        
        log.info("Transformed NSG order update XML from: " + awnXml + " to Cycle30 std format: " + xml);

        return newDom;
    }
    
    
    /**
     * If this is an Order POST to the /awn resource, then it's an AWN request.
     * 
     * @param request
     */
    public static synchronized boolean isAwnRequest(ResourceRequestInf request) {
        
        boolean awn = false;
        
        RequestProperties props = request.getRequestProperties();

        return isAwnRequest(props);
    }
    
    
    /**
     * 
     * @param props
     * @return true if request from AWN
     */
    public static synchronized boolean isAwnRequest(RequestProperties props) {
            
        String method = FxWebUtil.getRequestMethod(props.getHeaders());
        if (method.equalsIgnoreCase("POST")==false) {
            return false;
        }
            
        String uri = props.getURI();
        if (uri.indexOf("awn") != -1) {
            return true;
        }
        
        return false;

    }

    /**
     * If this is an Order POST to the /payments/prepaid resource, then it's an AWN prepaid payment request.
     * 
     * @param request
     */
    public static synchronized boolean isprepaidPmt(ResourceRequestInf request) {
        
        RequestProperties props = request.getRequestProperties();

        return isprepaidPmt(props);
    }
    
    
    /**
     * 
     * @param props
     * @return true if request from Prepaid Payments
     */
    public static synchronized boolean isprepaidPmt(RequestProperties props) {
            
        String method = FxWebUtil.getRequestMethod(props.getHeaders());
        if (method.equalsIgnoreCase("POST")==false) {
            return false;
        }
            
        String uri = props.getURI();
        log.info("prepaid payment uri : "+uri);
        if (uri.indexOf("payments/prepaid") != -1) {
            return true;
        }
        
        return false;

    }
    
    /**
     * 
     * @param props
     * @return prepaid account property type ('Search' vs 'History')
     */
    public static synchronized String prepaidAccountProperty(RequestProperties props) {

    	String	accountPropertyType = null;
            
        String uri = props.getURI();

        if (uri.indexOf("/prepaid/history") != -1) {
        	accountPropertyType = "history";
        	log.info("Account Property = "+accountPropertyType);        	
        } else if (uri.indexOf("/prepaid") != -1) {
        	accountPropertyType = "search";
        	log.info("Account Property = "+accountPropertyType);        	
        } 

        return accountPropertyType;

    }
    
    /**
     * 
     * @param props
     * @return voucher action type ('block' vs 'unblock')
     */
    public static synchronized String voucherAction(RequestProperties props) {

    	String	voucherActionType = null;
            
        String uri = props.getURI();
        if (uri.indexOf("/block") != -1) {
        	voucherActionType = "block";
        	log.info("voucherActionType = "+voucherActionType);        	
        } else if (uri.indexOf("/unblock") != -1) {
        	voucherActionType = "unblock";
        	log.info("voucherActionType = "+voucherActionType);        	
        } else if (uri.indexOf("/status") != -1) {
        	
        	voucherActionType = "status";
        }

        return voucherActionType;

    }
    
    /**
     * 
     * @param dom
     * @return true if request from AWN is for a port in/out
     * 
     * 
     */
    public static synchronized boolean isPortingAction(Document dom) {

        // Check the porting indicator. 
        //
    	//   <PortedFlag>1</PortedFlag>
    	//   <PortedFlag>0</PortedFlag>
        //
        String ported = DomUtils.getElementValue(dom, "PortedFlag");
        if (ported != null && ported.length()>0) {
            if (ported.equals("0")) return false;
            if (ported.equals("1")) return true;
            return true;
        }
        
        return false;

    }
    
    
    /**
     * 
     * @param dom
     * @return true if request from AWN is for a prepaid
     * 
     * 
     */
    public static synchronized boolean isPrepaid(Document dom) {

        // Check the prepaid indicator. 
        //
    	//   <ExtCorrelationId>ACS.PREPAID.1132_1</ExtCorrelationId>
    	//   
        //
        String extCorerelatId = DomUtils.getElementValue(dom, FxWebConstants.XML_TAG_EXT_CORRELATION_ID);
        if (extCorerelatId.toUpperCase().indexOf(FxWebConstants.ATTRIB_PREPAID) != -1) {
            return true;
        }
        
        return false;

    }
    
    /**
     * 
     * @param dom
     * @return true if request from AWN is for a prepaid
     * 
     * 
     */
    public static synchronized String getTelephoneNumber(Document dom) {

        // Get the Temporary Telephone Number 
        //
    	
        String telephoneNumber = null;
        telephoneNumber = DomUtils.getElementValue(dom, FxWebConstants.XML_TAG_PORTING_POOL_TN);
        
        return telephoneNumber;

    }
    
    /**
     * Remove the XML header '<?xml version="1.0" encoding="utf-8"?>'
     * 
     * @param xmlPayload
     * @return modified payload
     */
    public static synchronized String removeXmlHeader(String xmlPayload) {
        
        String response = xmlPayload;
        
        if (xmlPayload == null) {
            return null;
        }
        
        int startIdx = xmlPayload.indexOf("<?");
        if (startIdx == -1) {
            return xmlPayload;
        }
        
        int endIdx = xmlPayload.indexOf(">", startIdx);
        if (endIdx == -1) {
            return xmlPayload;
        }
        
        String tmp = xmlPayload.substring(endIdx+1);
        
        return tmp;
    }
    
    
    
    /**
     * Manually generate a porting order XML document. Resultant DOM document
     * will be in the form:
     * 
     * <?xml version="1.0" encoding="utf-8"?>
     * <OrderRequest>
     *  <RequestType>Service Change</RequestType>
     *  <OrderDesiredDate>2012-07-22T09:30:47.0Z</OrderDesiredDate>
     *  <ActionWho>C30SDK</ActionWho>
     *  <ClientOrderId>C30SDK.9072223333.20120912040501222</ClientOrderId>
     *  <IsDependent>Y</IsDependent>
     *  <DependentOrderId>1234</DependentOrderId>
     *  <ServiceObjectList Size="1">
     *    <ServiceObject ClientId="9072223333"
     *                   ClientActionType="CHANGE">
     *         <ExtendedDataList>
     *           <ExtendedData>
     *              <ExtendedDataValue>CarrierFrom</ExtendedDataValue>
     *              <ExtendedDataName>GCI</ExtendedDataName>
     *           </ExtendedData>
     *           <ExtendedData>
     *              <ExtendedDataValue>CarrierTo</ExtendedDataValue>
     *              <ExtendedDataName>ACS</ExtendedDataName>
     *           </ExtendedData>
     *           <ExtendedData>
     *              <ExtendedDataValue>Action</ExtendedDataValue>
     *              <ExtendedDataName>Add Voice</ExtendedDataName>
     *           </ExtendedData>                 
     *      </ExtendedDataList>  
     *    </ServiceObject>
     *  </ServiceObjectList>
     * </OrderRequest>
     * 
     * @param dom Document with porting details
     */
    public static synchronized Document generatePortingOrderPayload(Document dom) 
                                               throws FxWebException {
        

        StringBuffer xml = new StringBuffer(FxWebConstants.XML_HEADER);
        
        xml.append("<OrderRequest>");
        xml.append("<RequestType>Service Change</RequestType>");
        
        //String orderDate = getGmtTime(true);
        //xml.append("<OrderDesiredDate>" + orderDate + "</OrderDesiredDate>");
        // leave order date empty so backend processes will default to current date.
        xml.append("<OrderDesiredDate></OrderDesiredDate>");
        
        xml.append("<ActionWho>C30API</ActionWho>");
        
        String orderId = "C30API.port." + getGmtTime(false);
        xml.append("<ClientOrderId>" + orderId + "</ClientOrderId>");
        
        xml.append("<IsDependent>1</IsDependent>");
        xml.append("<DependentOrderId></DependentOrderId>");
        
        // Set <ServiceObjectList> attribute values.
        xml.append("<ServiceObjectList Size=\"1\">");
        String telephoneNo = DomUtils.getElementValue(dom, "TelephoneNo");
        if (telephoneNo == null || telephoneNo.length()==0) {
            throw new FxWebException("No telephone number in network provider porting message.", "");
        }
        
        xml.append("<ServiceObject ClientId=\"" + telephoneNo + "\"");
        xml.append("               ClientActionType=\"CHANGE\"> ");
        
        // Set up extended data list for: carrier-from, carrier-to and action
        String carrierFrom = DomUtils.getElementValue(dom, "PortOutCarrier");
        String carrierTo   = DomUtils.getElementValue(dom, "PortInCarrier");         
        String action      = DomUtils.getElementValue(dom, "Action");
        
        xml.append("<ExtendedDataList>");    
        
           xml.append("<ExtendedData>");
           xml.append("<ExtendedDataName>CarrierFrom</ExtendedDataName>");
           xml.append("<ExtendedDataValue>" + carrierFrom + "</ExtendedDataValue>");
           xml.append("</ExtendedData>");
           
           xml.append("<ExtendedData>");
           xml.append("<ExtendedDataName>CarrierTo</ExtendedDataName>");
           xml.append("<ExtendedDataValue>" + carrierTo + "</ExtendedDataValue>");
           xml.append("</ExtendedData>");
           
           xml.append("<ExtendedData>");
           xml.append("<ExtendedDataName>Action</ExtendedDataName>");
           xml.append("<ExtendedDataValue>" + action + "</ExtendedDataValue>");
           xml.append("</ExtendedData>"); 
           
        xml.append("</ExtendedDataList>");
        
        // final closing XML elements
        xml.append("</ServiceObject>");
        xml.append("</ServiceObjectList>");
        
        xml.append("</OrderRequest>");
        
        
        String xmlStr = xml.toString();
        log.info("Converted port XML message to: " + xmlStr);
        
        Document portDom = DomUtils.stringToDom(xmlStr);
        
        return portDom;
    }
    
    
    
    /**
     * Get current GMT time in either log or short format.
     * 
     * @param longFormat
     * @return
     */
    public static synchronized String getGmtTime(boolean longFormat) {
        
        final int millisPerMinute = 60000;  
        final int minPerHr = 60;  
        Date date = new Date();
        
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG );  
        TimeZone zone = dateFormat.getTimeZone();  
        
        // Default to long format
        DateFormat dateNow = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat timeNow = new SimpleDateFormat("HH:mm:ss.0");
        String timeSep  = "T";
        String tzSuffix = "Z";
        
        if (longFormat==false) {
            dateNow = new SimpleDateFormat("yyyyMMdd");
            timeNow = new SimpleDateFormat("HHmmssSSS");
            timeSep  = ".";
            tzSuffix = "";
        } 
        
        int minutes = zone.getOffset( date.getTime() ) / millisPerMinute;  
        int hours   = minutes / minPerHr;  
        zone  = zone.getTimeZone( "GMT Time" +(hours>=0?"+":"") + hours+":" + minutes);
        
        dateNow.setTimeZone(zone);  
        timeNow.setTimeZone(zone);
        String gmtDate = dateNow.format(date) + timeSep + timeNow.format(date) + tzSuffix; 
        
        return gmtDate;
 
   } 
    
   
}
