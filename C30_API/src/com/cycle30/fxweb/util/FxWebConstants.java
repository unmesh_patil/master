package com.cycle30.fxweb.util;

import java.util.HashMap;


public class FxWebConstants {

    
    public static final String ORG_ID_HEADER_NAME      = "X-Org-Id";
    public static final String PROVIDER_ID_HEADER_NAME = "X-Provider-Id";
    public static final String TRANS_ID_HEADER_NAME    = "X-Trans-Id";
   
    
    // URI path/query parameter names
    public static final String ACCOUNT_NO_IDENTIFIER      = "accountNumber";
    public static final String DOM_ACCOUNT_NO_IDENTIFIER  = "AccountNumber";
    public static final String TRACKING_ID_IDENTIFIER     = "trackingId";
    public static final String INVOICE_NO_IDENTIFIER      = "invoiceNumber";

    // SDK Object Constants
    public static final String SPS_SIM_SERIAL_NUMBER      	= "SerialNumber";
    public static final String TRANSACTION_ID      			= "TransactionId";
    public static final String SPS_SIM_SERIAL_NUMBER_ICCID  = "ICCID";
    public static final String C30_SDK_API_VERSION  		= "C30SdkApiVersion";
    
    public static final String OCS_PREPAID_SUBSCRIBER_NUMBER="SubscriberId";
    public static final String OCS_PREPAID_BALANCE_ACTION="BalanceAction";
    public static final String OCS_PREPAID_BALANCE_OBJECT="BalanceObject";
    public static final String PREPAID_CLIENT_ID="ClientId";
    public static final String PREPAID_TELEPHONE_NUMBER="TelephoneNumber";
    public static final String PREPAID_COMMUNITY="Community";
    public static final String PREPAID_SALES_CHANNEL_ID="SalesChannelId";
    public static final String PREPAID_INV_STATUS="InvStatus";
    public static final String PREPAID_SIZE="Size";
    public static final String OCS_PREPAID_VOOUCHER_PIN="VoucherPIN";
    public static final String OCS_PREPAID_VOUCHER_ACTION="VoucherAction";
    public static final String PREPAID_ACCOUNT_ACTION="PrepaidAccountAction";
    public static final String OCS_RESELLER_LABEL="ResellerLabel";
    public static final String RESPONSE_XML = "ResponseXML";
    public static final String PREPAID_SEGMENT_CACHE_ID = "4";
    public static final String ATTRIB_PREPAID = "PREPAID";    
    
    public static final String XML_TAG_EXT_CORRELATION_ID = "ExtCorrelationId";
    public static final String XML_TAG_PORTING_POOL_TN = "PortingPoolTN";
	public static final	String C30_PROV_SPS_TEMP_TELEPHONE_NO = "SPSTemporaryTN";    
	public static final String C30_EXT_DATA_SEND_FLAG_FALSE = "FALSE";	
    
    
    // System environment variables (set in script).
    public static final String ENCRYPTION_KEY_ENV_VAR    = "C30APPENCKEY";
    public static final String CLIENT_ID_ENV_VAR         = "C30ENVIRONID";
    public static final String TRANS_ID_ENV_VAR          = "TRANS_ID_REQUIRED";
    public static final String DIGEST_VALIDATION_ENV_VAR = "VALIDATE_DIGEST";
    
    // Payment reversal transaction types
    public static final String K2_REVERSAL_TRANS_TYPE = "-60";
    
    // Known Kenan authentication error code
    public static final String API_SESSION_ERROR_CODE = "API-SESSION-0001";
    
    
    // List of valid URI parameters
    public static final String[] VALID_PARAMS = {"accountNumber",
                                                    "city", 
                                                    "state",
                                                    "zip",
                                                    "start",
                                                    "size",
                                                    "distributionId",
                                                    "key",
                                                    "includeHistory",
                                                    "historyDays",
                                                    "telephoneNumber",
                                                    "salesChannelId",
                                                    "invStatus",
                                                    "community",
                                                    "serialNumber"};
    
    // Generic XML header used for all responses.
    public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    
    // Deposit payment types
    public final static HashMap<String, String> VALID_DEPOSIT_PAYMENT_TYPES = new HashMap<String, String>();
    static {
        VALID_DEPOSIT_PAYMENT_TYPES.put("Check", "1");
        VALID_DEPOSIT_PAYMENT_TYPES.put("CC",    "2");
        VALID_DEPOSIT_PAYMENT_TYPES.put("Cash",  "3");
        VALID_DEPOSIT_PAYMENT_TYPES.put("MO",    "4");
        
    }

    // Deposit types
    public final static HashMap<String, String> VALID_DEPOSIT_TYPES = new HashMap<String, String>();
    static {
        VALID_DEPOSIT_TYPES.put("General",     "1");
        VALID_DEPOSIT_TYPES.put("Non-General", "2");        
    }

    public static final String DEFAULT_AWN_CLIENT_ID  = "AWN";  
    public static final String ACS_CLIENT_ID  = "5";    
    public static final String GCI_CLIENT_ID  = "4";
    public static final String ACS_CLIENT_ID_VALUE  = "ACS";    
    public static final String GCI_CLIENT_ID_VALUE  = "GCI";
    //PW-114 Health check api constants
    public static final String PREPAID_OCS_SUBSCRIBER_STATE_LABEL = "SubscriberStateLabel";
    public static final String TEST_CLI="9078559202";
        
}
