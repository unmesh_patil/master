package com.cycle30.fxweb.response;

import java.io.File;

import javax.ws.rs.core.Response;


/**
 * 
 * Workpoint job status response
 *
 */
public class JobStatusResponseImpl implements ResourceResponseInf {

    // Note that the job status is an image file, unlike many other
    // response types that are String (e.g. XML).  The ResourceResponseInf
    // is defined to return an Object.
    private Object responseBody;
    
    private Response.Status responseCode;
    private String responseType;
    private String systemId;
    
    
    /**
     * @return the responseBody
     */
    public Object getResponseBody() {
        return responseBody;
    }

    /**
     * @param responseBody the responseBody to set
     */
    public void setResponseBody(Object responseBody) {
        this.responseBody = responseBody;
    }
    


    // Workpoint job image file handler
    private File jobImage;

    
    /**
     * @return the jobImage
     */
    public File getJobImage() {
        return jobImage;
    }

    /**
     * @param jobImage the jobImage to set
     */
    public void setJobImage(File jobImage) {
        this.jobImage = jobImage;
    }

    /**
     * @return the responseType
     */
    public String getResponseType() {
        return responseType;
    }
    
    /**
     * @param responseType the responseType to set
     */
    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
    /**
     * @return the systemId
     */
    public String getSystemId() {
        return systemId;
    }
    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    /**
     * @return the responseCode
     */
    public Response.Status getResponseCode() {
        return responseCode;
    }
    
    
    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(Response.Status responseCode) {
        this.responseCode = responseCode;
    }




}
