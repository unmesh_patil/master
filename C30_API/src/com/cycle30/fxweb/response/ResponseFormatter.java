package com.cycle30.fxweb.response;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.core.Response;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.sdkinterface.MappedError;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;

/**
 * 
 * General purpose class for formatting an SDK response into the 
 * Fx Web enablement response format
 *
 */
public class ResponseFormatter {

    
    /**
     * Format the SDK response, and set the appropriate response status code.
     * 
     * @param request
     * @param response
     * @param sdkResponse
     * @param createStatusList 
     */
    public static synchronized void formatResponse(ResourceRequestInf request, 
                                                       ResourceResponseInf response, 
                                                       HashMap<String, ?> sdkResponse,
                                                       boolean createStatusList) throws FxWebException {       
        
        
        // Default response status.
        Response.Status sdkStatus = Response.Status.OK;
        
        // Get the resource and response type, eg. <AccountSearchResponse>
        String resourceName = FxWebUtil.getResourceType(response); 
        String responseType = response.getResponseType();
        
        // Resource name XML element name, eg. '<Account>', '<Payment>', etc. 
        // Note. no name for authentication.
        String resourceStartElement = "<" + resourceName + ">";
        String resourceEndElement   = "</" + resourceName + ">";
        if (resourceName == null || resourceName.length()==0) {
            resourceStartElement = "";
            resourceEndElement   = "";
        }
        
        // Transform to appropriate output type.
        StringBuffer responseBody = new StringBuffer(FxWebConstants.XML_HEADER);
        
       
        // see if an SDK error occurred
        String sdkError = "";
        sdkStatus = getResponseStatus(sdkResponse);
        
        // Null means an SDK error occurred, so get the mapped SDK error.
        MappedError mappedError = null;
        if (sdkStatus == null) {
            mappedError = getSdkError(sdkResponse);
            sdkStatus   = mappedError.getStatus();
            sdkError    = mappedError.getExternalError();
        }
               
            
        // Set the initial response type, eg. '<AccountSearchResponse>'
        responseBody.append("<" + responseType + ">");   
        
        // If a root-level <StatusList> is required for this request type,
        // build it and add it to the working structure.
        if (createStatusList) {
            String statusList = createStatusList(response, mappedError);
            responseBody.append(statusList);
        }
        
        formatResponseStartElements(responseBody, response, sdkResponse);
        
        // For each response, convert to XML wrapped by the resource type (eg <Account>.  
        // Each response HashMap has sequential key values of "0", "1", "2"...N
        //
        // Don't do this however if an SDK/SDK error occurred.  The HashMap entry will
        // have a stacktrace, etc that we don't want to convert to XML
        if (sdkError != null && sdkError.length()==0) {
            for (int i=0; i<sdkResponse.size(); i++) {
                
               String key = String.valueOf(i);
               HashMap<String, ?> result = (HashMap)sdkResponse.get(key);
               
               responseBody.append(resourceStartElement);
               FxWebUtil.mapToXml(result, responseBody);
               responseBody.append(resourceEndElement);
            } 
        }
        
        formatResponseEndElements(responseBody, response);
        
        // Final XML element, eg. </AccountSearchResponse>
        responseBody.append("</" + response.getResponseType() + ">");
        
        
        // Set the reponse body to the generated text.
        response.setResponseBody(responseBody.toString());
        
        // Set the HTTP response code
        response.setResponseCode(sdkStatus);
        
    }
    

    
    /**
     * Set the initial XML elements for multi-response. For example:
     * 
     * <Accounts>
     *    <System id=K2>
     *          <TotalCount></TotalCount>
     *
     * 
     * 
     * @param xml
     * @param response
     */
    private static synchronized void formatResponseStartElements(StringBuffer xml, 
                                                                     ResourceResponseInf response,
                                                                     HashMap sdkResponse) {
             
        String resourceType = FxWebUtil.getResourceType(response);    
        
        // Pluralize the resource type, eg 'Accounts'
        xml.append("<" + resourceType + "s" + ">");
        
        // Set the system id
        xml.append("<System id=\"" + response.getSystemId() + "\">");
        
        // Total count from the first instance of objects in the SDK response map.
        // It will actually be in each instance, but we only want it once and as 
        // a root-level XML element
        HashMap map = (HashMap)sdkResponse.get("0");
        String totalCount = "";
        if (map != null) {
           totalCount = (String)map.get("TotalCount");
           if (totalCount == null || totalCount.equalsIgnoreCase("null")) {
               totalCount = "0";
           }
        }
        
        xml.append("<TotalCount>" + totalCount + "</TotalCount>");
        
    }
    
    
    
    /**
     * Close the initial XML elements for multi-response. For example:
     * 
     *   </System>
     * </Accounts>
     * 
     * @param xml
     * @param response
     */
    private static synchronized void formatResponseEndElements(StringBuffer xml, 
                                                                   ResourceResponseInf response) {
             
        String resourceType = FxWebUtil.getResourceType(response);    
       
        // Close the system id
        xml.append("</System>");
        
        // Cloase the pluralized  resource type, eg 'Accounts'
        xml.append("</" + resourceType + "s" + ">");

        
    }
    
    
    /**
     * Create <StatusList> structure, e.g.
     *   <StatusList>
     *     <Status>
     *       <System>K2</System>
     *       <Message>SUCCESS</Message>
     *       <Code>0</Code>
     *     </Status>
     *  </StatusList>
     * 
     * @param xml
     * @param response
     */
    private static synchronized String createStatusList(ResourceResponseInf response,
                                                           MappedError mappedError) {
                
        String system = response.getSystemId();
        String code   = "0";
        
        String msg = "SUCCESS";        
        if (mappedError != null) {
           msg  = mappedError.getExternalError();
           code = mappedError.getExceptionCode();
        }
        
        StringBuffer statusList = new StringBuffer("<StatusList><Status>");
        
        //statusList.append("<System>"  + system + "</System>");
        statusList.append("<Message>" + msg    + "</Message>");
        statusList.append("<Code>"    + code   + "</Code>");
        
        statusList.append("</Status></StatusList>");
        
        return statusList.toString();
        
    }
    
    
    /**
     * 
     * @param sdkResponse
     * @return
     */
    private static synchronized Response.Status getResponseStatus(Map sdkResponse) {
        
        Response.Status status = Response.Status.OK;
        
        // If no exception, return 'OK'
        String exceptionCode = getExceptionCode(sdkResponse);
        if (exceptionCode == null) {
            return status;
        }

        return null;
        
 
    }

    
    /**
     * Format the SDK response into a standardized format.
     * 
     * @param sdkResponse
     * @return A populated MappedError instance
     */
    public static synchronized MappedError getSdkError(Map sdkResponse) 
                                                          throws FxWebException {
        

        // Get the SDK error code        
        String exceptionCode = getExceptionCode(sdkResponse);        
        String exceptionMessage = getExceptionMessageText(sdkResponse);
        
        // Get the application-level and 'user-friendly' versions of the SDK error
        // by constructing a MappedError from the exception code.  The MappedError
        // will self-populate itself with the appropriate error messages and 
        // HTTP response code.
        MappedError mappedError = null;
        
        if (exceptionCode != null && exceptionCode.length()>0) {
            mappedError  = new MappedError(exceptionCode, exceptionMessage);
            
        } else {
            // Build MappedError manually if no exception code (should never happen on SDK error).
            mappedError = new MappedError();
            String sdkError = getExceptionMessageText(sdkResponse);
            String errorMsg = "Unexpected error occurred: " + sdkError;
            Response.Status status   = Response.Status.INTERNAL_SERVER_ERROR;
            mappedError.setExternalError(errorMsg);
            mappedError.setStatus(status);
            mappedError.setExceptionCode("1");  // Set to default '1' error condition
        }
        

        return mappedError;
        
    }
    


    /**
     *  Check SDK response for an exception code, and translate it to 
     *  the appropriate HTTP response code.  Any exception code will 
     *  be under the top level "0" key/hashMap
     *  
     * @param sdkResponse
     * @return
     */
    private synchronized static String getExceptionCode(Map sdkResponse) {
        
        HashMap map = (HashMap)sdkResponse.get("0");
        if (map == null) {
            return null;
        }
        
        String exceptionCode = (String)map.get("ExceptionCode");
        if (exceptionCode != null) {
            return exceptionCode;
        }
       
        return null;
    }
    


    /**
     *  Check response for an exception message under "MsgText" under the 
     *  top level "0" key/hashMap
     *  
     * @param sdkResponse
     * @return
     */
    private synchronized static String getExceptionMessageText(Map sdkResponse) {
        
        HashMap map = (HashMap)sdkResponse.get("0");
        if (map == null) {
            return null;
        }
        
        String msgText = (String)map.get("MsgText");
        if (msgText != null) {
            return msgText;
        }
       
        return null;
    }
    
}
