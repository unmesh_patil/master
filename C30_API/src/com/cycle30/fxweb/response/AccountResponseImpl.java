package com.cycle30.fxweb.response;

import java.util.HashMap;

import javax.ws.rs.core.Response;


/**
 * 
 * Kenan account response
 *
 */
public class AccountResponseImpl implements ResourceResponseInf {

    private String responseBody;
    private Response.Status responseCode;
    private String responseType;
    private String systemId;





    /**
     * @return the responseType
     */
    public String getResponseType() {
        return responseType;
    }
    
    /**
     * @param responseType the responseType to set
     */
    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
    /**
     * @return the systemId
     */
    public String getSystemId() {
        return systemId;
    }
    /**
     * @param systemId the systemId to set
     */
    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }
    /**
     * @return the responseBody
     */
    public String getResponseBody() {
        return responseBody;
    }
    /**
     * @param responseBody the responseBody to set
     */
    public void setResponseBody(Object responseBody) {
        this.responseBody = (String)responseBody;
    }
    /**
     * @return the responseCode
     */
    public Response.Status getResponseCode() {
        return responseCode;
    }
    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(Response.Status responseCode) {
        this.responseCode = responseCode;
    }

    
    


}
