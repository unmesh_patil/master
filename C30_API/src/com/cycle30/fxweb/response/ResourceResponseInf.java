package com.cycle30.fxweb.response;

import java.util.HashMap;

import javax.ws.rs.core.Response;



/**
 * 
 * Resource response interface
 *
 */
public interface ResourceResponseInf {
    
    public Object getResponseBody();
    public void setResponseBody(Object body);
    
    public Response.Status getResponseCode();
    public void setResponseCode(Response.Status responseCode);

    public String getResponseType();
    public String getSystemId();

}
