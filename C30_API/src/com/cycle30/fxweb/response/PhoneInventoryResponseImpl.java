package com.cycle30.fxweb.response;

import java.util.HashMap;

import javax.ws.rs.core.Response;

public class PhoneInventoryResponseImpl implements ResourceResponseInf {

	private String responseBody;
	private Response.Status responseCode;
	private String responseType;
	private String systemId;

	private boolean multiResponseType;

	// Map of the XML elements to include in the response
	private static HashMap filterMap;

	/**
	 * @return the multiResponseType
	 */
	public boolean isMultiResponseType() {
		return multiResponseType;
	}

	/**
	 * @param multiResponseType
	 *            the multiResponseType to set
	 */
	public void setMultiResponseType(boolean multiResponseType) {
		this.multiResponseType = multiResponseType;
	}

	/**
	 * @return the filterMap
	 */
	public HashMap getFilterMap() {
		return filterMap;
	}

	/**
	 * @param filterMap
	 *            the filterMap to set
	 */
	public void setFilterMap(HashMap filterMap) {
		this.filterMap = filterMap;
	}

	/**
	 * @return the responseType
	 */
	public String getResponseType() {
		return responseType;
	}

	/**
	 * @param responseType
	 *            the responseType to set
	 */
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	/**
	 * @return the responseBody
	 */
	public String getResponseBody() {
		return responseBody;
	}

	/**
	 * @param responseBody
	 *            the responseBody to set
	 */
	public void setResponseBody(Object responseBody) {
		this.responseBody = (String) responseBody;
	}

	/**
	 * @return the responseCode
	 */
	public Response.Status getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(Response.Status responseCode) {
		this.responseCode = responseCode;
	}

	@Override
	public String getSystemId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
}
