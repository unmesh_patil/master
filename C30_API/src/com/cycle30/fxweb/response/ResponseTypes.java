package com.cycle30.fxweb.response;


/**
 *
 * Static response types for XML header and other uses
 *
 */
public class ResponseTypes {

    // Response types

    public static enum response {AccountSearchResponse,
                                   JobStatusResponse,
                                   orderStatusResponse,
                                   GeoCodeSearchResponse,
                                   InvoiceSearchResponse,
                                   InvoiceChargesResponse,
                                   PaymentResponse,
                                   PrepaidPaymentResponse,                                   
                                   InventoryResponse,
                                   PaymentReversal,
                                   DepositResponse,
                                   ServiceResponse,
                                   AccountBalanceResponse,
                                   HealthCheckResponse,
                                   PrepaidAccountResponse,
                                   ClientCallbackResponse,
                                   VoucherResponse};

}
