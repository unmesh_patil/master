package com.cycle30.fxweb.service;



import java.io.File;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.exception.FxWebRuntimeException;
import com.cycle30.fxweb.request.RequestHandler;

import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;






/**
 *
 * Class that implements Jersey JAX-RS services for accessing Kenan resources.
 *
 */

@Path("/")
public class ResourceController  {

	public static Logger log = Logger.getLogger(ResourceController.class);



	//-------------------------------------------------------------------------
	// Account list GET.
	//-------------------------------------------------------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/accounts")
	public Response acctListHandler(@Context HttpHeaders headers,
			@Context UriInfo ui) {

		String qParams = FxWebUtil.getFormattedQueryParamList(ui);
		log.info("ResourceController received Account GET request with query params:\n" + qParams);

		Response acctResponse = null;

		try {
			acctResponse = new RequestHandler().handleRequest("account", headers, ui, null);

			return acctResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//-------------------------------------------------------------------------
	// Account  GET by Account Number.
	//-------------------------------------------------------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/account/{accountNumber}")
	public Response accountGetDirectListHandler(@PathParam(FxWebConstants.ACCOUNT_NO_IDENTIFIER) String acctno,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		String qParams = FxWebUtil.getFormattedQueryParamList(ui);
		log.info("ResourceController received Invoice GET request with query params:\n" + qParams);

		Response acctResponse = null;

		try {
			acctResponse = new RequestHandler().handleRequest("account", headers, ui, null);

			return acctResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//-------------------------------------------------------------------------
	// Account invoice GET.
	//-------------------------------------------------------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/account/{accountNumber}/invoices")
	public Response invoiceListHandler(@PathParam(FxWebConstants.ACCOUNT_NO_IDENTIFIER) String acctno,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		String qParams = FxWebUtil.getFormattedQueryParamList(ui);
		log.info("ResourceController received Invoice GET request with query params:\n" + qParams);

		Response acctResponse = null;

		try {
			acctResponse = new RequestHandler().handleRequest("invoice", headers, ui, null);

			return acctResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//-------------------------------------------------------------------------
	// Invoice Charges GET.
	//-------------------------------------------------------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/invoice/{invoiceNumber}/invoiceCharges")
	public Response invoiceChargeListHandler(@PathParam(FxWebConstants.INVOICE_NO_IDENTIFIER) String invno,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		String qParams = FxWebUtil.getFormattedQueryParamList(ui);
		log.info("ResourceController received Invoice Charges GET request with query params:\n" + qParams);

		Response invResponse = null;

		try {
			invResponse = new RequestHandler().handleRequest("invoiceCharges", headers, ui, null);

			return invResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}



	//----------------------------
	// Order processing services
	//----------------------------
	@POST
	@Produces({"application/xml"})
	@Path("/orders")
	@Consumes("application/xml")
	//public Response acctPostHandler(Document doc,
	public Response ordertPostHandler(String xml,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Orders POST request. XML: " + xml);

		Response orderResponse = null;

		try {
			// Convert XML string to DOM document
			Document doc = DomUtils.stringToDom(xml);

			orderResponse = new RequestHandler().handleRequest("order", headers, ui, doc);


		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}


		return orderResponse;
	}

		
	//----------------------------
	// Order processing services
	//----------------------------
	@DELETE
	@Produces({"application/xml"})
	@Path("/orders/{orderId}")
	@Consumes("application/xml")
	//public Response acctPostHandler(Document doc,
	public Response orderDeletetHandler(@PathParam("orderId") String orderId,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Orders DELETE request. OrderId : " + orderId);

		Response orderResponse = null;

		try {
		
			orderResponse = new RequestHandler().handleRequest("order", headers, ui, null);

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}


		return orderResponse;
	}


	//----------------------------
	// Order processing services
	//----------------------------
	@POST
	@Produces({"application/xml"})
	@Path("/orders/awn")
	@Consumes("application/xml")
	public Response acctPostAwnHandler(Document doc,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received AWN orders POST request...");

		Response orderResponse = null;

		try {

			orderResponse = new RequestHandler().handleRequest("order", headers, ui, doc);

			return orderResponse;


		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}
	}

	//----------------------------
    // DCN-6d-Globecomm Callback request Handler
	//----------------------------
		@POST
		@Produces({"application/xml"})
		@Path("/orders/dcn")
		@Consumes("application/xml")
		public Response acctPostDCNHandler(Document doc,
				@Context HttpHeaders headers,
				@Context UriInfo ui) {

			log.info("ResourceController received AWN orders POST request...");

			Response orderResponse = null;

			try {

				orderResponse = new RequestHandler().handleRequest("order", headers, ui, doc);

				return orderResponse;


			} catch (FxWebException fwe) {
				log.error(fwe);
				throw new FxWebRuntimeException(fwe);
			}
		}

	// Update order status
	@PUT
	@Produces({"application/xml"})
	@Path("/orders")
	@Consumes("application/xml")
	public Response orderPutHandler(Document doc,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Orders PUT request...");

		Response orderResponse = null;

		try {

			orderResponse = new RequestHandler().handleRequest("order", headers, ui, doc);
			return orderResponse;


		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}
	}




	@GET
	@Produces({"application/xml"})
	@Path("/request/{requestid}/status")
	public Response orderStatusGetHandler(@PathParam("requestid") String requestid,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received GET request for order status id: " + requestid + " ...");

		//System.getProperties().list(System.out);

		Response statusResponse = null;

		try {

			statusResponse = new RequestHandler().handleRequest("orderstatus", headers, ui, null);

			return statusResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}
	}



	//----------------------------
	// Job status services
	//----------------------------
	@GET
	@Produces({"image/*"})
	@Path("/jobstatus/{jobId}")
	public Response jobStatusGetHandler(@PathParam("jobId") String jobId,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received GET request for job status id: " + jobId + " ...");

		System.setProperty("java.awt.headless", "true");
		System.getProperties().list(System.out);

		Response jobStatusResponse = null;

		try {

			jobStatusResponse = new RequestHandler().handleRequest("jobstatus", headers, ui, null);

			// Make sure instance is really a File vs. text
			Object o = jobStatusResponse.getEntity();
			if (o instanceof File) {
				File f = (File)jobStatusResponse.getEntity();
				String mt = new MimetypesFileTypeMap().getContentType(f);
				jobStatusResponse =  Response.ok(f, mt).build();
			}

			return jobStatusResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}
	}



	//---------------------------
	//  Kenan addresses
	//---------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/addresses/geocode")
	public Response geoCodeGetHandler(@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("Resource controller received GeoCode GET request...");

		Response addressResponse = null;

		try {
			addressResponse = new RequestHandler().handleRequest("geocode", headers, ui, null);
			return addressResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}
	}



	//---------------------------
	//  Kenan payment resources
	//---------------------------
	@POST
	@Produces({"application/xml"})
	@Path("/payments")
	@Consumes(MediaType.APPLICATION_XML)
	public Response paymentPostHandler(Document doc,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Payments POST request...");

		Response paymentResponse = null;

		try {
			paymentResponse = new RequestHandler().handleRequest("payment", headers, ui, doc);
			return paymentResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//---------------------------
	//  Prepaid payment resources
	//---------------------------
	@POST
	@Produces({"application/xml"})
	@Path("/payments/prepaid")
	@Consumes(MediaType.APPLICATION_XML)
	public Response prepaidPaymentPostHandler(Document doc,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Payments POST request...");

		Response paymentResponse = null;

		try {
			paymentResponse = new RequestHandler().handleRequest("prepaidpayment", headers, ui, doc);
			return paymentResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}
	
	//---------------------------
	//  Client Interface resources
	// This is used to send the messages back to force.
	//---------------------------
	@POST
	@Produces({"application/xml"})
	@Path("/clientcallback")
	@Consumes(MediaType.APPLICATION_XML)
	public Response clientCallbackResImpl(Document doc,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Payments POST request...");

		Response clientCallbackResponse = null;

		try {
			clientCallbackResponse = new RequestHandler().handleRequest("clientcallback", headers, ui, doc);
			return clientCallbackResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}


	// Payment reversal support
	@DELETE
	@Produces({"application/xml"})
	@Path("/account/{accountNumber}/payment/{trackingId}")
	public Response paymentReversalHandler(@PathParam(FxWebConstants.ACCOUNT_NO_IDENTIFIER) String acctno,
			@PathParam(FxWebConstants.TRACKING_ID_IDENTIFIER) String trackingId,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Payments DELETE request for account no " + acctno + "...");

		Response paymentResponse = null;

		try {
			paymentResponse = new RequestHandler().handleRequest("payment", headers, ui, null);
			return paymentResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}


	@POST
	@Produces({"application/xml"})
	@Path("/deposits")
	@Consumes(MediaType.APPLICATION_XML)
	public Response depositPostHandler(Document doc,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Deposits POST request...");

		Response depositResponse = null;

		try {
			depositResponse = new RequestHandler().handleRequest("deposit", headers, ui, doc);
			return depositResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}



	//---------------------------
	//  Inventory resources
	//---------------------------


	@GET
	@Produces({"application/xml"})
	@Path("/inventory/phone/{telephoneNumber}")
	public Response phoneInventoryGetHandler(@PathParam("telephoneNumber") String telephoneNumber,@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Inventory GET request...");

		Response inventoryResponse = null;

		try {
			inventoryResponse = new RequestHandler().handleRequest("phoneSearchInventory", headers, ui, null);
			return inventoryResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	@GET
	@Produces({"application/xml"})
	@Path("/inventory/phones")
	public Response phoneInventorySearchGetHandler(@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Inventory GET request...");

		Response inventoryResponse = null;

		try {
			inventoryResponse = new RequestHandler().handleRequest("phoneSearchInventory", headers, ui, null);
			return inventoryResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}


	@GET
	@Produces({"application/xml"})
	@Path("/inventory/sim/{serialNumber}")
	public Response simInventoryGetHandler(@PathParam("serialNumber") String serialNumber,@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Inventory GET request...");

		Response inventoryResponse = null;

		try {
			inventoryResponse = new RequestHandler().handleRequest("simInventory", headers, ui, null);
			return inventoryResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}




	@POST
	@Produces({"application/xml"})
	@Path("/inventory/phones")
	@Consumes(MediaType.APPLICATION_XML)
	public Response inventoryUpdatePostHandler(Document doc,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Inventory POST request...");

		Response inventoryResponse = null;

		try {
			inventoryResponse = new RequestHandler().handleRequest("phoneSearchInventory", headers, ui, doc);
			return inventoryResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//---------------------------
	//  Account Balance Prepaid resources
	//---------------------------


	@GET
	@Produces({"application/xml"})
	@Path("/account/{subscriberNumber}/prepaid/balances")
	public Response accountBalancesPrepaidGetHandler(@PathParam("subscriberNumber") String subscriberNumber,@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received account Balances Prepaid GET request...");

		Response accountBalPrepaidResponse = null;

		try {
			accountBalPrepaidResponse = new RequestHandler().handleRequest("prepaidAccountBal", headers, ui, null);
			return accountBalPrepaidResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	@POST
	@Produces({"application/xml"})
	@Path("/account/{subscriberNumber}/prepaid/balances")
	@Consumes(MediaType.APPLICATION_XML)
	public Response updateBalancesPrepaidGetHandler(@PathParam("subscriberNumber") String subscriberNumber,Document doc,@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received account Balances Prepaid POST request...");

		Response accountBalPrepaidResponse = null;

		try {
			accountBalPrepaidResponse = new RequestHandler().handleRequest("prepaidAccountBal", headers, ui, doc);
			return accountBalPrepaidResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//------------------------------------
	//  Prepaid Account History resources
	//------------------------------------

	@GET
	@Produces({"application/xml"})
	@Path("/account/{subscriberNumber}/prepaid/history")
	public Response accountHistotyPrepaidGetHandler(@PathParam("subscriberNumber") String subscriberNumber,@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Prepaid Account History  GET request...");

		Response accountHistoryPrepaidResponse = null;

		try {
			accountHistoryPrepaidResponse = new RequestHandler().handleRequest("prepaidAccount", headers, ui, null);
			return accountHistoryPrepaidResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//------------------------------------
	//  Prepaid Account Search resources
	//------------------------------------

	@GET
	@Produces({"application/xml"})
	@Path("/account/{subscriberNumber}/prepaid")
	public Response accountSearchPrepaidGetHandler(@PathParam("subscriberNumber") String subscriberNumber,@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Prepaid Account Search GET request...");

		Response accountSearchPrepaidResponse = null;

		try {
			accountSearchPrepaidResponse = new RequestHandler().handleRequest("prepaidAccount", headers, ui, null);
			return accountSearchPrepaidResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}    




	//---------------------------
	//  Service healthchecks
	//---------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/service/{serviceid}/healthcheck")
	public Response healthCheckGetHandler(@PathParam("serviceid") String serviceId,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("Resource controller received Service healthcheck GET request for service id " + serviceId + "...");

		Response healthcheckResponse = null;

		try {
			healthcheckResponse = new RequestHandler().handleRequest("service", headers, ui, null);
			return healthcheckResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}
	}

	// Health Check
	@GET
	@Produces({"application/xml"})
	@Path("/systems/healthcheck")
	public Response healthCheckHandler(@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Health Check GET request");

		Response healthCheckResponse = null;

		try {
			healthCheckResponse = new RequestHandler().handleRequest("healthcheck", headers, ui, null);
			return healthCheckResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}

	//-------------------------------------------------------------------------
	// Voucher Status GET.
	//-------------------------------------------------------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/voucher/{VoucherPIN}/status")
	public Response voucherStatusHandler(@PathParam("VoucherPIN") String voucherPIN,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {


		log.info("ResourceController received Voucher Status GET request ");

		Response voucherStatusResponse = null;

		try {
			voucherStatusResponse = new RequestHandler().handleRequest("voucher", headers, ui, null);

			return voucherStatusResponse;

		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}

	}    

	//----------------------------
	// Voucher BLOCK Processing 
	//----------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/voucher/{VoucherPIN}/block")
	public Response voucherblockPostHandler(@PathParam("VoucherPIN") String voucherPIN,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Voucher Block GET request ");

		Response voucherBlockResponse = null;

		try {

			voucherBlockResponse = new RequestHandler().handleRequest("voucher", headers, ui, null);


		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}


		return voucherBlockResponse;
	}

	//----------------------------
	// Voucher UNBLOCK Processing 
	//----------------------------
	@GET
	@Produces({"application/xml"})
	@Path("/voucher/{VoucherPIN}/unblock")
	public Response voucherunblockPostHandler(@PathParam("VoucherPIN") String voucherPIN,
			@Context HttpHeaders headers,
			@Context UriInfo ui) {

		log.info("ResourceController received Voucher UnBlock GET request ");

		Response voucherunBlockResponse = null;

		try {

			voucherunBlockResponse = new RequestHandler().handleRequest("voucher", headers, ui, null);


		} catch (FxWebException fwe) {
			log.error(fwe);
			throw new FxWebRuntimeException(fwe);
		}


		return voucherunBlockResponse;
	}
}