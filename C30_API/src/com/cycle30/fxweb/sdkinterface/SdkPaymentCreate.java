package com.cycle30.fxweb.sdkinterface;


import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.payment.PaymentVersion20Object;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;


/**
 * Class representing an C30SDK payment instance
 *
 */
public class SdkPaymentCreate {

    private static final String EXCEPTION_SOURCE    = "SDK-PAYMENT";
    
    public static Logger log = Logger.getLogger(SdkPaymentCreate.class);   


    
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> createPayment(ResourceRequestInf request,
                                             PaymentVersion20Object payment,
                                             Document domDocument) 
                                 throws FxWebException, C30SDKInvalidAttributeException {
    

        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        
        try {
            
            // Instantiate the SDK factory, and get the account-find object.
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Payment create SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.create.payment");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();


            String orgId = requestProps.getClientId();
            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "InvoiceFind");
            } 
           
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            
            // Set the various payment attributes.
            setPaymentAttributes(sdkObject, payment, domDocument);
            
            // Manually add the transaction id from the original HTTP header value.
            String transactionId = request.getRequestProperties().getTransactionId();
            sdkObject.setNameValuePair("ExtTransactionId", transactionId);
            
            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");
            
            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw e;
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
       

    }
    
    /**
     * 
     * Populate various payment attributes based on what was passed in the Payment
     * payload.
     * 
     * @param paymentObjectsdk
     * @param request
     */
    private void setPaymentAttributes(C30SDKObject paymentObjectsdk, 
                                       PaymentVersion20Object payment,
                                       Document doc) 
                         throws C30SDKInvalidAttributeException {
        

        
        // Get payment properties
        String acctNo = payment.getAccountNumber();

        String amount      = payment.getAmount();
        String currency    = payment.getCurrency();
        String tenderType  = payment.getTenderType();
        String checkNumber = payment.getCheckNumber();
        String extTransId  = payment.getExtTransactionId();
        String extCorrId   = payment.getExtCorrelationId();
      
        String locationId  = DomUtils.getElementValue(doc, "LocationId");
        String agentId     = DomUtils.getElementValue(doc, "AgentId");
        String notes       = DomUtils.getElementValue(doc, "Note"); 
        String distrAmt    = DomUtils.getElementValue(doc, "AmountDistribution");

        // Convert fraction amount to whole number, eg. 123.95 needs to be 12395.
        String intAmount = amount;
        String temp[] = amount.split("\\.");
        if (temp.length > 1) {
            intAmount = temp[0] + temp[1];
        }
        
        paymentObjectsdk.setNameValuePair("Note", notes);
        paymentObjectsdk.setNameValuePair("AMOUNT",  intAmount);
        paymentObjectsdk.setNameValuePair("ACCOUNTEXTERNALID", acctNo);
        paymentObjectsdk.setNameValuePair("LocationId", locationId);
        paymentObjectsdk.setNameValuePair("ExtCorrelationId", extCorrId);
        paymentObjectsdk.setNameValuePair("CheckNumber", checkNumber);
        paymentObjectsdk.setNameValuePair("TENDERTYPE", tenderType);
        paymentObjectsdk.setNameValuePair("AgentId", agentId);
        paymentObjectsdk.setNameValuePair("AmountDistribution", "");        
        paymentObjectsdk.setNameValuePair("TRANSTYPE", "1");
        
        
    }
    
}
