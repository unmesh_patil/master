package com.cycle30.fxweb.sdkinterface;


import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;


/** 
 * 
 *  General purpose utility class specifically for SDK purposes
 *
 */
public class SdkUtil {

    private static final String PROPERTIES_FILE_NAME = "c30apiSdkProps";
    private static ResourceBundle sdkProperties = null;
    
    public static Logger log = Logger.getLogger(SdkUtil.class);   
    
    
    

    
    /**
     * 
     * @param propertyName
     * @return SDK property value
     */
    public synchronized static String getSdkProperty(String propertyName) throws FxWebException {
        
        if (sdkProperties == null) {
            getSdkProperties(); 
        }
        
        String propertyValue = null;
        try {
            propertyValue = sdkProperties.getString(propertyName);
        } catch (Exception e) {
            // swallow any 'not found' exception
        }
        
        return propertyValue;
    }
    
    
    
    /**
     *  Get SDK properties from sdk.properties file.
     */
    private synchronized static void getSdkProperties() throws FxWebException {
        
        try {
            Locale locale = Locale.getDefault();
            sdkProperties = ResourceBundle.getBundle(PROPERTIES_FILE_NAME, locale);
        } catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "Error opening " + PROPERTIES_FILE_NAME + ".properties file.");
        }

    }
    

    

}
