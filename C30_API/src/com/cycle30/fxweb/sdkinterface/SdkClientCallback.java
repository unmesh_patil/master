package com.cycle30.fxweb.sdkinterface;


import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.payment.PaymentVersion20Object;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;


/**
 * Class representing an C30SDK Force callback instance
 *
 */
public class SdkClientCallback {

    private static final String EXCEPTION_SOURCE    = "SDK-CORE-CLIENTCALLBACK";
    
    public static Logger log = Logger.getLogger(SdkPaymentCreate.class);   


    
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> clientCallback(ResourceRequestInf request,
                                             Document domDocument) 
                                 throws FxWebException {
    

        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        
        try {
            
            // Instantiate the SDK factory, and get the account-find object.
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Payment create SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.clientcallback.instance");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();


            String orgId = requestProps.getClientId();
            String segId = ClientCache.getSegmentationId(orgId);
            
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            String xmlString = DomUtils.domToXtring(domDocument);
            
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            sdkObject.setNameValuePair("Payload", xmlString);
            
            // Manually add the transaction id from the original HTTP header value.
            String transactionId = request.getRequestProperties().getTransactionId();
            sdkObject.setNameValuePair("TransactionId", transactionId);
            
        
            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");
            
            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        } catch (Exception e) {
        	 log.error(e);
        	 throw new FxWebException(e.getMessage(), EXCEPTION_SOURCE);
		}  
       

    }
    
    
    
}
