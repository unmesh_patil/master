package com.cycle30.fxweb.sdkinterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.SIMInventoryRequestImpl;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.fxweb.util.FxWebConstants;    


/**
 * Class representing an SDK IMSI Search instance
 *
 */
public class SdkIMSISearch {

    private static final String EXCEPTION_SOURCE    = "SDK-SPS-IMSI-SEARCH";
    
    public static Logger log = Logger.getLogger(SdkIMSISearch.class);   

  
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> getIMSI(ResourceRequestInf request) 
                                 throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the IMSI Search object
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.sps.imsi.search");

            // Get segmentation id for this client
            RequestProperties requestProps = request.getRequestProperties();
            String orgId   = requestProps.getClientId();  
            String transId = requestProps.getTransactionId();
            String segId = ClientCache.getSegmentationId(orgId);
            log.info("segId : " +segId + "orgId : " +orgId + "transId : " +transId);            
            
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "getIMSI");
            }     
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            
            // Set the serial number id attribute
            String simSerialNumber = FxWebUtil.getPathParamValue(requestProps.getPathParams(), "serialNumber");
  
            if ( simSerialNumber != null && simSerialNumber.trim().length()>0 ) {
                sdkObject.setNameValuePair(FxWebConstants.SPS_SIM_SERIAL_NUMBER, simSerialNumber);         
            }
            
            // Set required transaction id
            sdkObject.setNameValuePair(FxWebConstants.TRANSACTION_ID, transId);

            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
        catch (Exception e) {
            log.error(e);
            throw new FxWebException(e.getMessage(), EXCEPTION_SOURCE);
        }  
       

    }

    
}
