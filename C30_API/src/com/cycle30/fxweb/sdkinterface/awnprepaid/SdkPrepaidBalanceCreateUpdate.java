package com.cycle30.fxweb.sdkinterface.awnprepaid;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.cache.ClientOCSCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.awnprepaid.balance.BalanceObject;
import com.cycle30.fxweb.sdkinterface.SdkConnectionFactory;
import com.cycle30.fxweb.sdkinterface.SdkUtil;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
      

/**
 * Class representing an SDK Prepaid Account Balance Update instance
 *
 */
public class SdkPrepaidBalanceCreateUpdate {

    private static final String EXCEPTION_SOURCE    = "SDK-PREPAID-BALANCE-CREATE-UPDATE";
    
    public static Logger log = Logger.getLogger(SdkPrepaidBalanceCreateUpdate.class);   

  
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> updatePrepaidBalance(ResourceRequestInf request, BalanceObject balance) 
                                 throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Prepaid Account Balance object
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.prepaid.balance.find.update");

            // Get segmentation id for this client
            RequestProperties props = request.getRequestProperties();
            String orgId   = props.getClientId();  
            String transId = props.getTransactionId();

            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "Update Prepaid Balance");
            }  
 
            String subscriberId = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), "subscriberNumber");
            
            if(subscriberId==null ){
            	 throw new FxWebException("Subscriber Number is Null : " + subscriberId, "Update Prepaid Balance");
            } 
            
            if(balance ==null ){
           	 throw new FxWebException("Balance Object is Null  : " + subscriberId, "Update Prepaid Balance");
           } 
        
            log.info("Balance Object "+ balance.toString());
            Integer segmentationId = Integer.valueOf(segId);
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            
            //Set the required Client Id
            
            sdkObject.setNameValuePair(FxWebConstants.PREPAID_CLIENT_ID, orgId);

            // Set Subscriber id attribute
                sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_SUBSCRIBER_NUMBER, subscriberId);  
                if(balance.getBalanceAction()!=null){
                	 sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_ACTION, balance.getBalanceAction().value());
                }if(balance.getBalanceTypeLabel() !=null){
                	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_LABEL_TYPE,balance.getBalanceTypeLabel());
                }if(balance.getBalanceReason() !=null){
                	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_REASON,balance.getBalanceReason());
                }if(balance.getBalanceUnits()!=null){
                	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_VALUE,balance.getBalanceUnits().getValue());
                }if(balance.getExpirationDate()!=null){
                    sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_EXPIRATION_DATE, balance.getExpirationDate());
                }if(balance.getVoucherPINCode()!=null){
                     sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_VOUCHER_PIN, balance.getVoucherPINCode());
                }if(balance.getReversalRequestId()!=null){
                    sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_REVERSAL_REQUEST_ID, balance.getReversalRequestId());
                }
                
                if(ClientOCSCache.getSubscriberDetails(subscriberId,transId) !=null){
                 	String subscriberCLI =  ClientOCSCache.getSubscriberDetails(subscriberId,transId).get("CLI");
                 	String subscriberCurrentPlan= ClientOCSCache.getSubscriberDetails(subscriberId,transId).get("SubscriberProfileLabel");
                	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_SUBSCRIBER_CLI, subscriberCLI);
                	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OCS_CHARGING_EVENT_LABEL, subscriberCurrentPlan);
                }
               
            
              
            // Set required transaction id
            sdkObject.setNameValuePair(FxWebConstants.TRANSACTION_ID, transId);

            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
            
  
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
        catch (Exception e) {
            log.error(e);
            throw new FxWebException(e.getMessage(), EXCEPTION_SOURCE);
        }  
       

    }
    

    
}
