package com.cycle30.fxweb.sdkinterface.awnprepaid;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.inventory.InventoryRequestObject;
import com.cycle30.fxweb.sdkinterface.SdkConnectionFactory;
import com.cycle30.fxweb.sdkinterface.SdkUtil;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;


/**
 * Class representing an SDK PREPAID PHONE INVENTORY Update instance
 *
 */
public class SdkPrepaidInventoryUpdate {

    private static final String EXCEPTION_SOURCE    = "SDK-PREPAID-PHONE-UPDATE";
    
    public static Logger log = Logger.getLogger(SdkPrepaidInventoryUpdate.class);   

  
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> updatePhoneInventory(ResourceRequestInf request, InventoryRequestObject invReqObj,Document domDocument, String payload) 
                                 throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Prepaid Phone Inventory Update object
            //String sdkObjectName = SdkUtil.getSdkProperty("sdk.prepaid.phoneinventory.update");
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.prepaid.phonenumber.inquiry");

            // Get segmentation id for this client
            RequestProperties requestProps = request.getRequestProperties();
            String orgId   = requestProps.getClientId();  
            String transId = requestProps.getTransactionId();
            String segId = ClientCache.getSegmentationId(orgId);
            log.info("segId : " +segId + "orgId : " +orgId + "transId : " +transId);            
            
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "Update TN Inventory");
            }  
            
            
            if(invReqObj ==null ){
              	 throw new FxWebException("Inventory Object is Null  : " , "Update TN Inventory");
              }
            else if(invReqObj !=null ) {
            	  if(invReqObj.getTelephoneNumber()==null || invReqObj.getAction()==null ){
            		  throw new FxWebException("Inventory Mandatory parameters is Null  : " + invReqObj.toString(), "Update TN Inventory");
            	  }
            	  //PW-256 vijaya => commenting below code to allow ROR to update action to change TN status from Retain to Available  
            	 /* else if(!(invReqObj.getAction().value().equalsIgnoreCase("RETAIN"))){
            		  throw new FxWebException("Inventory Action should be RETAIN  : XML contains the action - " + invReqObj.getAction().value(), "Update TN Inventory");
            	  }*/
            	   log.info("Inventory Object "+ invReqObj.toString());
              }
           
           

            
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
           sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
          
           //Set the required Client Id           
           sdkObject.setNameValuePair(FxWebConstants.PREPAID_CLIENT_ID, orgId);
           
            // Set required transaction id
            sdkObject.setNameValuePair(FxWebConstants.TRANSACTION_ID, transId);
            
            if(invReqObj.getAction()!=null){
            	 sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_ACTION_TYPE, invReqObj.getAction().value());
            }if(invReqObj.getTelephoneNumber() !=null){
            	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NUMBER,invReqObj.getTelephoneNumber().toString());
            }if(invReqObj.getInvStatus() !=null){
            	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INV_STATUS,invReqObj.getInvStatus().toString());
            }if(invReqObj.getNetworkId() !=null){
            	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NETWORK_ID,invReqObj.getNetworkId().toString());
            }else{
            	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NETWORK_ID,"");
            }
            if(invReqObj.getOperatorId() !=null){
                sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_OPERATOR_ID, invReqObj.getOperatorId().toString());
            }else{
            	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_OPERATOR_ID, "".toString());
            }
            if(invReqObj.getSalesChannelId() !=null){
                sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SALES_CHANNEL_ID, invReqObj.getSalesChannelId().toString());
            }else{
            	sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SALES_CHANNEL_ID, "".toString());
            }
            if(payload !=null){
                sdkObject.setNameValuePair(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INVENTORY_PAYLOAD,payload);
            }


            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
        catch (Exception e) {
            log.error(e);
            throw new FxWebException(e.getMessage(), EXCEPTION_SOURCE);
        }  
       

    }

    
}
