package com.cycle30.fxweb.sdkinterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;

      

/**
 * Class representing an SDK invoice find instance
 *
 */
public class SdkInvoiceFinder {


    private static final String INVC_FIND_ATTRIBUTE_NAME   = "ClientId";
    private static final String EXCEPTION_SOURCE  = "SDK-INVOICE-FIND";
    
    public static Logger log = Logger.getLogger(SdkInvoiceFinder.class);   

   
    /**
     * 
     * Get basic invoice information.
     * 
     * @param request
     */
    public HashMap<String, ?> invoiceFind(ResourceRequestInf request)  throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Invoice search SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.invoice.search");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();


            String orgId = requestProps.getClientId();
            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "InvoiceFind");
            } 
           
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            

            // Set the external id attribute
            String externalId = requestProps.getAccountNo();
            if ( externalId != null && externalId.trim().length()>0) {
                sdkObject.setNameValuePair(INVC_FIND_ATTRIBUTE_NAME,   externalId);  
            }
            
            // Set required transaction id
            String transId = requestProps.getTransactionId();
            sdkObject.setNameValuePair("TransactionId", transId);

            // Set the various properties passed as URL params.
            setInvoiceSearchAttributes(sdkObject, request);

            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  

       

    }
    
    /**
     * 
     * Populate various Invoice search attributes based on what was passed as query 
     * parameters in the HTTP request.
     * 
     * @param order
     * @param request
     */
    private void setInvoiceSearchAttributes(C30SDKObject sdkObject, ResourceRequestInf request) 
                                                throws FxWebException {
        
        // go through query params (eg. /abc/efg?firstName=abc&lastName=xyz)
        // and set appropriate attribute.
        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
        Iterator<String> i = queryParams.keySet().iterator();
       
        String key = "";
        String value = "";
        
        try {
           while (i.hasNext()) {
               
               key = (String)i.next();            
               LinkedList<?> ll = (LinkedList<?>)queryParams.get(key);
               String temp = (String)ll.get(0);
               
               // Replace any '*' wildcard with SDK '%' wildcard value
               value = temp.replace('*', '%'); 
               
               // If param exists, but no value supplied... skip it.
               if (value != null && value.length()==0) {
                   continue;
               }
               else if (key.equalsIgnoreCase("start")) {
                   sdkObject.setNameValuePair("Start", value);
               }
               else if (key.equalsIgnoreCase("size")) {
                   sdkObject.setNameValuePair("Size", value);
               }


           }
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(new Exception("Invalid parameter name or value supplied in request. " +
                                                     "param name: '" + key + "' value: '" + value + "'"), 
                                      Response.Status.INTERNAL_SERVER_ERROR, "Find Invoice");
        }
        
    }
    
    
    
    /**
     * 
     * Get invoices for a particular payment distribution.  This
     * requires getting the invoice #'s associated with the distributions,
     * then getting the invoices themselves.
     * 
     * @param request
     */
    public HashMap<String, ?> getInvoicesForPayment(ResourceRequestInf request, String distributionId) 
                                          throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
      // TODO - remove hardwired result
      HashMap hm1 = new HashMap();
      HashMap hm2 = new HashMap();
      hm1.put("0", hm2);
      return hm1;
      
       /* try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Distribution search SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.pmt.distribution.search");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();


            String orgId = requestProps.getClientId();
            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "AccountFind");
            } 
           
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            

            // Set the external id
            String externalId = requestProps.getAccountNo();
            if ( externalId != null && externalId.trim().length()>0) {
                sdkObject.setNameValuePair(INVC_FIND_ATTRIBUTE_NAME,   externalId);  
            }
            
            // Set required transaction id
            String transId = requestProps.getTransactionId();
            sdkObject.setNameValuePair("DistributionId", distributionId);

            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  

       */

    }
    
}
