package com.cycle30.fxweb.sdkinterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientAPIVersionCache;
import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.util.FxWebConstants;


/**
 * Class representing an SDK GeoCode instance
 *
 */
public class SdkGeoCodeFinder {

    private static final String EXCEPTION_SOURCE  = "SDK-GEOCODE-FIND";
    
    public static Logger log = Logger.getLogger(SdkAccountFinder.class);   

    
    /** 
     * 
     * @param request
     */
    public HashMap<String, ?> geoCodeFind(ResourceRequestInf request) 
                                 throws FxWebException {

        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {      

            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request); 
            
            // Get the Account Find SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.geocode.object.name");

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName);
            
            // Set required transaction id
            RequestProperties requestProps = request.getRequestProperties();
            String transId = requestProps.getTransactionId();
            String sdkAPIVersion = requestProps.getSdkApiVersion();
            if (sdkAPIVersion == null)
            {
            	throw new FxWebException ("No API Version found for the given Account SegmentId, ClientId. Not Authorized", EXCEPTION_SOURCE);
            }
            log.info("transId = " + transId + " sdkAPIVersion : " + sdkAPIVersion);

            // Set the transaction id and SDK API version
            sdkObject.setNameValuePair(FxWebConstants.TRANSACTION_ID, transId);
            sdkObject.setNameValuePair(FxWebConstants.C30_SDK_API_VERSION, sdkAPIVersion);
            
            // Set find attributes from query params
            setSearchAttributes(sdkObject, request);

            
            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  

    }
    
    
    
    /**
     * 
     * Populate various search attributes based on what was passed as query 
     * parameters in the HTTP request.
     * 
     * @param order
     * @param request
     */
    private void setSearchAttributes(C30SDKObject sdkObject, ResourceRequestInf request) 
                         throws FxWebException {
        
        // go through query params (eg. /address/geocode?zip=99999&city=xyz)
        // and set appropriate attribute.
        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
        Iterator<String> i = queryParams.keySet().iterator();
       
        String key = "";
        String value = "";
        
        try {
           while (i.hasNext()) {
               
               key = (String)i.next();            
               LinkedList<?> ll = (LinkedList<?>)queryParams.get(key);
               String temp = (String)ll.get(0);
               
               // Replace any '*' wildcard with SDK '%' wildcard value
               value = temp.replace('*', '%'); 
               
               // If param exists, but no value supplied... skip it.
               if (value != null && value.length()==0) {
                   continue;
               }
               
               if (key.equalsIgnoreCase("city")) {
                   sdkObject.setNameValuePair("City", value);
               }
               else if (key.equalsIgnoreCase("state")) {
                   sdkObject.setNameValuePair("State", value);
               }
               else if (key.equalsIgnoreCase("zip")) {
                   sdkObject.setNameValuePair("Zip", value);
               }

           }


        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(new Exception("Invalid parameter name or value supplied in request. " +
                                                     "param name: '" + key + "' value: '" + value + "'"), 
                                      Response.Status.INTERNAL_SERVER_ERROR, "Find Account");
        }
        
    }
    
}
