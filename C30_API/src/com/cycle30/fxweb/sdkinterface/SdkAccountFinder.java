package com.cycle30.fxweb.sdkinterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;

      

/**
 * Class representing an SDK account find instance
 *
 */
public class SdkAccountFinder {

    private static final String EXCEPTION_SOURCE  = "SDK-ACCOUNT-FIND";   
    public static Logger log = Logger.getLogger(SdkAccountFinder.class);   


    
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> accountFind(ResourceRequestInf request) 
                                 throws FxWebException {
    

        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       

            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);
      
            
            // Get the Account Find SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.account.find");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();
            String orgId = requestProps.getClientId();
            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "AccountFind");
            }
           
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            

            // Set the external id/segment attributes for a single-account lookup if an account 
            // was supplied by the client app.  
            String externalId = requestProps.getAccountNo();
            if ( externalId != null && externalId.trim().length()>0) {
                sdkObject.setNameValuePair("ClientId",   externalId);  
            }

            // Set required transaction id
            String transId = requestProps.getTransactionId();
            sdkObject.setNameValuePair("TransactionId", transId);
            
            // Set the various properties passed as URL params.
            setAccountSearchAttributes(sdkObject, request);

            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  

       

    }
    
    /**
     * 
     * Populate various Account search attributes based on what was passed as query 
     * parameters in the HTTP request.
     * 
     * @param order
     * @param request
     */
    private void setAccountSearchAttributes(C30SDKObject sdkObject, ResourceRequestInf request) 
                         throws FxWebException {
        
        // go through query params (eg. /abc/efg?firstName=abc&lastName=xyz)
        // and set appropriate attribute.
        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
        Iterator<String> i = queryParams.keySet().iterator();
       
        String key = "";
        String value = "";
        
        try {
           while (i.hasNext()) {
               
               key = (String)i.next();            
               LinkedList<?> ll = (LinkedList<?>)queryParams.get(key);
               String temp = (String)ll.get(0);
               
               // Replace any '*' wildcard with SDK '%' wildcard value
               value = temp.replace('*', '%'); 
               
               // If param exists, but no value supplied... skip it.
               if (value != null && value.length()==0) {
                   continue;
               }
               
               if (key.equalsIgnoreCase("firstName")) {
                   sdkObject.setNameValuePair("BillFName", value);
               } else if (key.equalsIgnoreCase("lastName")) {
                   sdkObject.setNameValuePair("BillLName", value);
               }
               else if (key.equalsIgnoreCase("billCompany")) {
                   sdkObject.setNameValuePair("BillCompany", value);
               }
               else if (key.equalsIgnoreCase("phone")) {
                   sdkObject.setNameValuePair("CustPhone1", value);
               }
               else if (key.equalsIgnoreCase("address1")) {
                   sdkObject.setNameValuePair("BillAddress1", value);
               }
               else if (key.equalsIgnoreCase("city")) {
                   sdkObject.setNameValuePair("BillCity", value);
               }
               else if (key.equalsIgnoreCase("state")) {
                   sdkObject.setNameValuePair("BillState", value);
               }
               else if (key.equalsIgnoreCase("zip")) {
                   sdkObject.setNameValuePair("BillZip", value);
               }
               else if (key.equalsIgnoreCase("start")) {
                   sdkObject.setNameValuePair("StartRecord", value);
               }
               else if (key.equalsIgnoreCase("size")) {
                   sdkObject.setNameValuePair("ReturnSize", value);
               }
               else if (key.equalsIgnoreCase("activeSubscribers")) {             // active subscribers only?
                   sdkObject.setNameValuePair("IncludeInactiveAccounts", "true");     
                   if (value.equalsIgnoreCase("yes")) {                           // API param opposite verbage as SDK attribute
                       sdkObject.setNameValuePair("IncludeInactiveAccounts", "false"); 
                   }
               }

           }
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(new Exception("Invalid parameter name or value supplied in request. " +
                                                     "param name: '" + key + "' value: '" + value + "'"), 
                                      Response.Status.INTERNAL_SERVER_ERROR, "Find Account");
        }
        
    }
    
}
