package com.cycle30.fxweb.sdkinterface;


import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;


import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
//import com.cycle30.sdk.lightweight.object.kenan.framework.LightweightUser;



/** 
 * sdk connection factory singleton.  
 * 
 * Note: the initialize() and destroy() methods are called by Mule via 
 *       Spring configuration.
 *       
 */
public class SdkConnectionFactory {

    private static final String EXCEPTION_SOURCE = "SDK-CONNECTION-FACTORY";  
    
    private static SdkConnectionFactory self = new SdkConnectionFactory();
 
    private static C30SDKFrameworkFactory factory ;
    private static SecurityManager sm = null;
    
    public static Logger log = Logger.getLogger(SdkConnectionFactory.class);   
    
    // Spring-injected properties
    private static String kenanVersion;
    private static boolean deferInit;

    

    
    /**
     * @return the deferInit
     */
    public boolean isDeferInit() {
        return deferInit;
    }

    /**
     * @param deferInit the deferInit to set
     */
    public void setDeferInit(boolean deferInit) {
        SdkConnectionFactory.deferInit = deferInit;
    }

    /**
     * @return the kenanVersion
     */
    public String getKenanVersion() {
        return kenanVersion;
    }

    /**
     * @param kenanVersion the kenanVersion to set
     */
    public void setKenanVersion(String kenanVersion) {
        this.kenanVersion = kenanVersion;
    }




    //------------------------------------------------------------------
    /**
     *  private constructor
     */
    private SdkConnectionFactory() {   
    }
    
    


    /**
     * 
     */
    public void initialize() throws FxWebException {
        
       // Configurable value to do lazy/deferred initialization vs. at startup.
       if (deferInit) {
           return;
       }
       
       getSdkConnection(null);
       kenanVersion = null;  // reset after initialization
       log.info("SDK factory initialized...");
       
    }


    /**
     * 
     */
    public void destroy()  {
        factory.logout();         
     }
    
  
    
    //--------------------------------------------------------------------------
    /** Get singleton instance of the connection
     */  
    public static synchronized SdkConnectionFactory getInstance() {
        
        return self;

    }
    
    
    //--------------------------------------------------------------------------
    /**
     * Get a Kenan connection that will be re-used for all requests.  This method 
     * will ONLY be invoked via Spring configuration (via the initialize() method).
     * 
     * @return the factory
     */
    public C30SDKFrameworkFactory getSdkConnection (ResourceRequestInf request) throws FxWebException {
           
        if (factory == null) {
            
            // Get default Kenan version-specific SDK properties
            SdkConnectionProperties sdkProps = getSdkProperties(request);
            
            try {

                String fxVersion = sdkProps.getFxVersion();
                String psVersion = sdkProps.getPsVersion();
                
                log.debug("SdkConnectionFactory attempting to create SDK factory..."); 
                
                factory = new C30SDKFrameworkFactory(fxVersion, psVersion); 
                
                log.debug("Successfully created SDK factory...");
               
            } catch (Exception e) {                
                log.error(e);
                // Throw a fatal 'server' error;
                throw new FxWebException(e, Response.Status.INTERNAL_SERVER_ERROR, EXCEPTION_SOURCE);
            } 
        }
        
        return factory;
    }
    

    
    //---------------------------------------------------------------------------
    /**
     * Get various default SDK properties.
     * 
     * @param request
     * @return Connection properties
     */
    public SdkConnectionProperties getSdkProperties(ResourceRequestInf request) 
                                                             throws FxWebException {
        

        // default to needing authentication.
        boolean authenticate = true;
        
        SdkConnectionProperties props = new SdkConnectionProperties();
        
        //  Get FX and PS versions from .properties file.
        String fxVersion = SdkUtil.getSdkProperty("sdk.fxversion");
        String psVersion = SdkUtil.getSdkProperty("sdk.psversion");
        

        // Set properties in object
        props.setFxVersion(fxVersion);
        props.setPsVersion(psVersion);
        
        
        return props;
    }

    



}
