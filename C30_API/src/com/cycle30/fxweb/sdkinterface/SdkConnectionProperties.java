package com.cycle30.fxweb.sdkinterface;


public class SdkConnectionProperties {

    
    private String fxVersion;
    private String psVersion;
    
    
    /**
     * @return the fxVersion
     */
    public String getFxVersion() {
        return fxVersion;
    }
    /**
     * @param fxVersion the fxVersion to set
     */
    public void setFxVersion(String fxVersion) {
        this.fxVersion = fxVersion;
    }
    /**
     * @return the psVersion
     */
    public String getPsVersion() {
        return psVersion;
    }
    /**
     * @param psVersion the psVersion to set
     */
    public void setPsVersion(String psVersion) {
        this.psVersion = psVersion;
    }
    


}
