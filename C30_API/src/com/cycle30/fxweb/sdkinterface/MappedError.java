package com.cycle30.fxweb.sdkinterface;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;


public class MappedError {

    // SDK error properties files
    private final static String SDK_PROPERTIES_FILE_NAME = "C30SDK_Core_Error_Text";
    private final static String KENAN_ERROR_PROPERTIES_FILE_NAME = "C30SDK_Kenan_Error_Text";
    private final static String WORKPOINT_ERROR_PROPERTIES_FILE_NAME = "C30WP_Core_Error_Text";
    
    // API error properties
    private final static String API_PROPERTIES_FILE_NAME = "c30apiErrorText";
    
    private static ResourceBundle sdkErrorProperties       = null;
    private static ResourceBundle kenanErrorProperties     = null;
    private static ResourceBundle workpointErrorProperties = null;
    private static ResourceBundle apiErrorProperties       = null;
    
    private static final Logger log = Logger.getLogger(MappedError.class);

    
    private String internalError;
    private String externalError;
    private Response.Status status;
    private String exceptionCode;
    private String addlErrorText;


    /**
     * Constructor to self-populate error messages based on the
     * SDK exception code.
     * 
     * @param exceptionCode
     */
    public MappedError (String exceptionCode) throws FxWebException {
        this.exceptionCode = exceptionCode;
        setErrorMessages(exceptionCode);
    }
    
    
    /**
     * Alternate constructor that accepts additional error text.
     * 
     * @param exceptionCode
     */
    public MappedError (String exceptionCode, String addlErrorText) throws FxWebException {
        this.exceptionCode = exceptionCode;
     
        // Check for literal 'null' in additional error text
        if (addlErrorText != null && addlErrorText.equalsIgnoreCase("null")) {
            addlErrorText = null;
        }
        this.addlErrorText = exceptionCode;
        
        setErrorMessages(exceptionCode);        
        
        // If additional error text was provided, append it to the external
        // error message.
        if (addlErrorText != null && addlErrorText.length()!=0) {
            externalError += " Additional error details: " + addlErrorText;
        }
      
    }  
    
    /*
     * Generic constructor.
     * 
     * @param exceptionCode
     */
    public MappedError () throws FxWebException {
    }
    
    
    /**
     * @return the addlErrorText
     */
    public String getAddlErrorText() {
        return addlErrorText;
    }


    /**
     * @param addlErrorText the addlErrorText to set
     */
    public void setAddlErrorText(String addlErrorText) {
        this.addlErrorText = addlErrorText;
    }


    /**
     * @return the exceptionCode
     */
    public String getExceptionCode() {
        return exceptionCode;
    }

    /**
     * @param exceptionCode the exceptionCode to set
     */
    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }
    
    /**
     * @return the externalError
     */
    public String getExternalError() {
        return externalError;
    }

    /**
     * @param externalError the externalError to set
     */
    public void setExternalError(String externalError) {
        this.externalError = externalError;
    }

    /**
     * @return the errorMessage
     */
    public String getInternalErrorMessage() {
        return internalError;
    }
    /**
     * @param errorMessage the errorMessage to set
     */
    public void setInternalErrorMessage(String errorMessage) {
        this.internalError = errorMessage;
    }
    /**
     * @return the status
     */
    public Response.Status getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(Response.Status status) {
        this.status = status;
    }
    
    
    
    /**
     *  Get SDK error properties file.
     */
    private static synchronized void getSdkErrorProperties() throws FxWebException {
      
        
        try {
            Locale locale = Locale.getDefault();
            sdkErrorProperties = ResourceBundle.getBundle(SDK_PROPERTIES_FILE_NAME, locale);
        } catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "Error opening " + SDK_PROPERTIES_FILE_NAME + ".properties file.");
        }
    }
    
    
    /**
     *  Get Kenan error properties file.
     */
    private static synchronized void getKenanErrorProperties() throws FxWebException {
      
        
        try {
            Locale locale = Locale.getDefault();
            kenanErrorProperties = ResourceBundle.getBundle(KENAN_ERROR_PROPERTIES_FILE_NAME, locale);
        } catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "Error opening " + KENAN_ERROR_PROPERTIES_FILE_NAME + ".properties file.");
        }
    }
    
    
    
    /**
     *  Get Workpoint error properties file.
     */
    private static synchronized void getWorkpointErrorProperties() throws FxWebException {
      
        
        try {
            Locale locale = Locale.getDefault();
            workpointErrorProperties = ResourceBundle.getBundle(WORKPOINT_ERROR_PROPERTIES_FILE_NAME, locale);
        } catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "Error opening " + WORKPOINT_ERROR_PROPERTIES_FILE_NAME + ".properties file.");
        }
    }
    
    
    
    
    
    /**
     *  Get API error properties file.
     */
    private static synchronized void getApiErrorProperties() throws FxWebException {
      
        
        try {
            Locale locale = Locale.getDefault();
            apiErrorProperties = ResourceBundle.getBundle(API_PROPERTIES_FILE_NAME, locale);
        } catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "Error opening " + API_PROPERTIES_FILE_NAME + ".properties file.");
        }
    }
    
    
    
    /**
     *  Get the internal and external error messages.  Need to format the external message
     *  into something 'user-friendly' vs. the raw SDK error message
     *  
     *  @param exceptionCode
     *  
     */
    private void setErrorMessages(String exceptionCode) throws FxWebException {
        

        // Get SDK error properties file
        if (sdkErrorProperties == null) {
            getSdkErrorProperties();    
        }
        
        // Get Kenan error properties file (in SDK)
        if (kenanErrorProperties == null) {
            getKenanErrorProperties();    
        }
        
        // Get API error properties file
        if (apiErrorProperties == null) {
            getApiErrorProperties();    
        }
        
        // Get Workpoint error properties file
        if (workpointErrorProperties == null) {
            getWorkpointErrorProperties();    
        }
        
        // Check authentication error.  This needs to trump all other types
        // since it's the first thing that can happen on a request (don't
        // allow it to be returned as a 'middleware' error').
        String authErrMsg = checkAuthenticationError(exceptionCode);
        if (authErrMsg != null) {
            status = Response.Status.UNAUTHORIZED;
            externalError = authErrMsg; 
            return;
        } 
        
        // Check if general 'not found' condition occurred.
        String notFoundMsg = checkObjectNotfound(exceptionCode);
        if (notFoundMsg != null) {
            status = Response.Status.NOT_FOUND;
            externalError = notFoundMsg; 
            return;
        }
        
        // Check if general 'bad request' condition occurred.
        String badReqMsg = checkBadRequest(exceptionCode);
        if (badReqMsg != null) {
            status = Response.Status.BAD_REQUEST;
            externalError = badReqMsg; 
            return;
        }   
        
        
        // Check for a general SDK error.
        String kenanErrMsg = checkKenanSdkError(exceptionCode);
        if (kenanErrMsg != null) {
            internalError = kenanErrMsg; 
            externalError = "Kenan middleware error: " + exceptionCode + "-"+ kenanErrMsg; 
            status        = Response.Status.INTERNAL_SERVER_ERROR;
            return;
        }
        
        // Check for a Workpoint error.
        String workpointErrMsg = checkWorkpointError(exceptionCode);
        if (workpointErrMsg != null) {
            internalError = workpointErrMsg; 
            externalError = "Workpoint error: " + exceptionCode + "-"+ kenanErrMsg; 
            status        = Response.Status.INTERNAL_SERVER_ERROR;
            return;
        }
        
        
        //Check if this is Central Repository i.e., SQLAPIs
        if (exceptionCode != null && exceptionCode.equalsIgnoreCase("null")==false &&  exceptionCode.startsWith("C30KSA") ) {
        	externalError = "Error occured from Cetral Repository (Central Repository exception code: " + exceptionCode + ").";
        	status        = Response.Status.INTERNAL_SERVER_ERROR;
            return;
        }
        
        // Default to internal server error, and append 'Contact Cycle30' error ONLY if no 
        // error mapping can be made in SDK error properties
        status = Response.Status.INTERNAL_SERVER_ERROR;
        externalError = "A severe error occurred (SDK exception code: " + exceptionCode + ").";
        
        // Get the corresponding SDK error message
        if (exceptionCode != null && exceptionCode.equalsIgnoreCase("null")==false) {
            // Don't allow app to bomb if key isn't in the .properties file
            try {
               internalError = sdkErrorProperties.getString(exceptionCode);
               externalError = "A severe error occurred (SDK exception: " + exceptionCode + "-" + internalError + ")";
            } catch (RuntimeException e) {   
            }
        }
        
        if (this.internalError == null) {
            internalError = "SDK exception code " + exceptionCode + " not found in " + SDK_PROPERTIES_FILE_NAME + ".properties";
        }
        

        log.error("SDK error code " + exceptionCode + ". " + internalError);
       
    }
    
    
    /**
     * 
     * @param exceptionCode
     * @return not found msg
     */
    private String checkObjectNotfound (String exceptionCode) {
        
        String notFoundMsg = null;
        
        String apiErrorKey = "NF-" + exceptionCode;
        try {
            notFoundMsg = apiErrorProperties.getString(apiErrorKey);
        } catch (MissingResourceException mr) {
            // swallow this exception if it's not found.
        }
        return notFoundMsg;
        
    }
    
    
    /**
     * 
     * @param exceptionCode
     * @return not found msg
     */
    private String checkBadRequest (String exceptionCode) {
        
        
        String badReqMsg = null;
        
        String apiErrorKey = "BR-" + exceptionCode;
        try {
            badReqMsg = apiErrorProperties.getString(apiErrorKey);
        } catch (MissingResourceException mr) {
            // swallow this if it's not found.
        }
        
        // append the exception code
        if (badReqMsg != null) {
            badReqMsg += " (SDK error " + exceptionCode + ")";
        }
        return badReqMsg;
        
    }
    
    /**
     * 
     * @param exceptionCode
     * @return not found msg
     */
    private String checkAuthenticationError (String exceptionCode) {
        
        String notFoundMsg = null;
        
        String apiErrorKey = "AU-" + exceptionCode;
        try {
            notFoundMsg = apiErrorProperties.getString(apiErrorKey);
        } catch (MissingResourceException mr) {
            // swallow this exception if it's not found.
        }
        return notFoundMsg;
        
    }
    
    /**
     * 
     * @param exceptionCode (directly from SDK)
     * @return not found msg
     */
    private String checkKenanSdkError (String exceptionCode) {
        
        String kenanErrMsg = null;
        
        try {
            kenanErrMsg = kenanErrorProperties.getString(exceptionCode);
        } catch (MissingResourceException mr) {
        }
        return kenanErrMsg;
        
    }
    
    /**
     * 
     * @param exceptionCode (directly from SDK)
     * @return not found msg
     */
    private String checkWorkpointError (String exceptionCode) {
        
        String wpErrMsg = null;
        
        try {
            wpErrMsg = workpointErrorProperties.getString(exceptionCode);
        } catch (MissingResourceException mr) {
        }
        return wpErrMsg;
        
    }
    
}
