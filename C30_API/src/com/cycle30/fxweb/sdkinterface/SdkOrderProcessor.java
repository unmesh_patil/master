package com.cycle30.fxweb.sdkinterface;

import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.cycle30.order.C30OrderSqlUtils;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
      

/**
 * Class representing an SDK Order instance
 *
 */
public class SdkOrderProcessor {


    private static final String EXCEPTION_SOURCE = "SDK-ORDER-PROCESSOR";
    
    public static Logger log = Logger.getLogger(SdkOrderProcessor.class);   

  
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> processOrder(ResourceRequestInf request) 
                                 throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the SDK order object
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.order.object.name");

            // Get segmentation id for this client
            RequestProperties requestProps = request.getRequestProperties();
            String orgId   = requestProps.getClientId();  
            Document dom   = requestProps.getPayload();
            String transId = requestProps.getTransactionId();
            
            // Transform the document to a simple String value.  
            String xmlString = DomUtils.domToXtring(dom);
            log.info("Processing order with XML: " + xmlString);

            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "OrderProcess");
            }
           
            Integer segmentationId = Integer.valueOf(segId);

            // Create the object using the name/seg id.
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            
            // Set the XML attribute, which is an XML string respresenting everything
            // to do with the order request.  Also set the org and trans id attributes.
            sdkObject.setNameValuePair("OrderXML",      xmlString);         
            sdkObject.setNameValuePair("ClientOrgId",   orgId); 
            sdkObject.setNameValuePair("TransactionId", transId);
            
            // For orders, include the internal (audit) id that's generated in the
            // AuditLogger component when the request was logged.
            String internalId = requestProps.getInternalId();
            sdkObject.setNameValuePair("InternalAuditId", internalId);
            
            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();

            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            // If MsgText exists, then means a problem occurred
            //String msgText = 
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
        catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }

    }
    
    /**
     * This will only be used for provisioning status updates from network provider 
     * (vs. Kenan updates), and updates individual provisioning status values.
     * 
     * The expected payload from the provider:
     * 
     *  <OrderUpdateRequest>
     *      <ProviderId>Providerxyz</ProviderId>
     *          <ParamList>  <!-- not used by SPS -->
     *             <Key></Key>
     *             <Value></Value>
     *             <Key></Key>
     *             <Value></Value>
     *          </ParamList>
     *      <OrderId>4_1</OrderId>  <!-- id provided by C30 to SPS on initial prov request -->
     *      <Status>Failure</Status>
     *      <Error>Error msg</Error>
     *      <ExtCorrelationId>01928438</ExtCorrelationId>  <!-- optional from SPS -->
     *  </OrderUpdateRequest>
     *  
     * 
     * @param request
     */
    public HashMap<String, ?> updateOrderStatus(ResourceRequestInf request) 
                                      throws FxWebException {
    
        HashMap results = new HashMap();
        

        Document dom = request.getRequestProperties().getPayload();
        String orderId = DomUtils.getElementValue(dom, "OrderId");
        String status  = DomUtils.getElementValue(dom, "Status");
        String error   = DomUtils.getElementValue(dom, "Error");
        String transId = request.getRequestProperties().getTransactionId();
        String acctSegId = null;
        
        if (orderId == null) {
            orderId = "";
        } else {
            if (orderId.indexOf(FxWebConstants.ACS_CLIENT_ID_VALUE) == 0) {
            	acctSegId = FxWebConstants.ACS_CLIENT_ID;
            } else if (orderId.indexOf(FxWebConstants.GCI_CLIENT_ID_VALUE) == 0) {
            	acctSegId = FxWebConstants.GCI_CLIENT_ID;
            }
        }
        
        if (error != null && error.equalsIgnoreCase("SUCCESS")==false) {
            log.info ("Error provisioning service for order " + orderId);
            log.info ("Error message received: " + error);
        }
        
        // Convert SUCCESS or FAILURE to numeric code value for update
        String statusId = C30SDKValueConstants.SVC_PROVISION_SUCCESS;
        if (status.equalsIgnoreCase("SUCCESS")==false) {
            statusId = C30SDKValueConstants.SVC_PROVISION_FAILURE;
        }

        //----------------------------------------------------------------------
        // Need to take (possible) special actions for ACS port in/out messages
        boolean awn = FxWebUtil.isAwnRequest(request);
        boolean updateProvStatus = true;
        
        // No provisioning status to update if non ACS/GCI Prepaid message (eg. GCI K1)
        if (awn==true && orderId.startsWith("ACS")==false && orderId.startsWith("GCI")==false) {
            updateProvStatus = false;
        }  
        //----------------------------------------------------------------------
        
        //Validate the Porting Action and Prepaid Request
        boolean isPortingAction = FxWebUtil.isPortingAction(dom);
        boolean isPrepaid = FxWebUtil.isPrepaid(dom);           
        log.info ("isPortingAction =  " + isPortingAction);
        log.info ("isPrepaid = " + isPrepaid);
        
        try {
            
            String xml = DomUtils.domToXtring(dom); 
            
            //Check for the Porting Action
            if (isPortingAction) {
                if (isPrepaid) {
             	   //Get the temporary TN returned by SPS
             	   String telephoneNumber = FxWebUtil.getTelephoneNumber(dom);
             	   if (telephoneNumber != null) {
 						//Store Temporary TN returned by SPS in Sub Order Extended Data in the database.
 						C30OrderSqlUtils.insertExtendedData(orderId, FxWebConstants.C30_PROV_SPS_TEMP_TELEPHONE_NO, telephoneNumber, 
 								FxWebConstants.C30_EXT_DATA_SEND_FLAG_FALSE, acctSegId);			
             	   } else {
             		   log.info("Telephone Number Did not received in SPS Response");
             		  statusId = C30SDKValueConstants.SVC_PROVISION_FAILURE;            		   
             	   }
                }
            }            
            
            if (updateProvStatus) {
               log.info("Order status received from client: " + status + ". Converted to: " + statusId);
               log.info("Updating order id: " + orderId + " with Cycle30 status id: " + statusId);

               // Update order status 
               C30OrderSqlUtils.updateProvisioningStatus(statusId, xml, orderId);
            }
                  
       
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, "");
        }
        catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, "");
        }
        catch (SQLException e) {
            log.error(e);
            throw new FxWebException(e, "");
        }
        catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "");
        }  
          
        
        return results; 
    }
    

    
    
    /**
     * Get order status.  This is request-id based (vs. order id), since
     * multiple requests can be made using the same order id. 

     * @param requestId
     */
    public HashMap<String, ?> getOrderStatus(String requestId) throws FxWebException {
    
        HashMap results = new HashMap();

        try {
            
            // Go through result set, and create map entry for each service.
            results = C30OrderSqlUtils.getRequestStatus(requestId);

        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, "");
        } catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, "");
        }catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "");
        }
          
  
        return results; 
    }

	public void deleteOrderStatus(String requestId,
			String orderId) throws FxWebException {

	        try {
	            
	            // Go through result set, and create map entry for each service.
	           C30OrderSqlUtils.changeOrderStatus(orderId, new Integer("40"));

	        } catch (C30SDKInvalidAttributeException e) {
	            log.error(e);
	            throw new FxWebException(e, "");
	        } catch (C30SDKObjectConnectionException e) {
	            log.error(e);
	            throw new FxWebException(e, "");
	        }catch (Exception e) {
	            log.error(e);
	            throw new FxWebException(e, "");
	        }
	          
	  
	}
}
