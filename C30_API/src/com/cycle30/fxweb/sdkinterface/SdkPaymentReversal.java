package com.cycle30.fxweb.sdkinterface;


import java.util.HashMap;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.framework.C30SDKUser;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.request.wrapper.payment.Payment;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
 

/**
 * Class representing an C30SDK payment reversal nstance
 *
 */
public class SdkPaymentReversal {


    private static final String EXCEPTION_SOURCE = "SDK-REVERSAL";
    
    public static Logger log = Logger.getLogger(SdkPaymentReversal.class);   


    
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> reversePayment(ResourceRequestInf request) 
                                 throws FxWebException, C30SDKInvalidAttributeException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject order;
        
        try {
            
            // Instantiate the SDK factory, and get the account-find object.
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Payment reversal SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.reverse.payment");

            order = sdkFactory.createC30SDKObject(sdkObjectName);

            // Set the various reversal attributes.
            String externalId = request.getRequestProperties().getAccountNo();
            String trackingId = FxWebUtil.getPathParamValue(request.getRequestProperties().getPathParams(), "trackingId");

            order.setNameValuePair("AccountExternalId", externalId);
            order.setNameValuePair("TrackingId", trackingId);
            
            
            // Set transaction type 
            String transType = FxWebConstants.K2_REVERSAL_TRANS_TYPE;
            order.setNameValuePair("TransType", transType);
            
     
            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = order.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");
            
            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }
            
            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            
        } catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw e;
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }    
        

        
    }
    
}
