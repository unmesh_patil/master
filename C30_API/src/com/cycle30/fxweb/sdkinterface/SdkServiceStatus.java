package com.cycle30.fxweb.sdkinterface;


import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.cycle30.service.ServiceSqlUtils;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.util.FxWebUtil;

      

/**
 * Class representing an SDK service status instance
 *
 */
public class SdkServiceStatus {


    private static final String EXCEPTION_SOURCE  = "SDK-SERVICE-STATUS";
    
    public static Logger log = Logger.getLogger(SdkServiceStatus.class);   

   
    /**
     * 
     * Get billing status information.
     * 
     * @param request
     */
    public HashMap<String, HashMap> billingStatusInquiry(ResourceRequestInf request)  throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Invoice search SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.service.inquiry");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();


            String orgId = requestProps.getClientId();
            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "ServiceInquiry");
            } 
           
            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            

            // Set the service id attribute
            String serviceId = FxWebUtil.getPathParamValue(requestProps.getPathParams(), "serviceid");
            sdkObject.setNameValuePair("ServiceExternalId", serviceId);  
            
            // Set required transaction id
            String transId = requestProps.getTransactionId();
            sdkObject.setNameValuePair("TransactionId", transId);


            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  

    }

    
    
    /**
     * 
     * Get provisioning status information.
     * 
     * @param request
     */
    public HashMap<String, HashMap> provisioningStatusInquiry(ResourceRequestInf request)  throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {

            // Get the Invoice search SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.service.inquiry");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();

            // Set the service id attribute
            String serviceId = FxWebUtil.getPathParamValue(requestProps.getPathParams(), "serviceid");

            HashMap response = ServiceSqlUtils.queryProvisioningStatus(serviceId);

            // Sanity check - must have something available.
            if (response == null || response.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return response;
            

        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
        catch (Exception e) {
            log.error(e);
            throw new FxWebException(e.getMessage(), EXCEPTION_SOURCE);
        }  


    }
    
    
}

