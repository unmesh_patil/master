package com.cycle30.fxweb.sdkinterface;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.JobStatusRequestImpl;
import com.cycle30.fxweb.request.RequestProperties;
      

/**
 * Class representing an SDK Job Status instance
 *
 */
public class SdkJobStatus {

    private static final String EXCEPTION_SOURCE    = "SDK-JOB-STATUS-FIND";
    
    public static Logger log = Logger.getLogger(SdkJobStatus.class);   

  
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> getJobStatus(JobStatusRequestImpl request) 
                                 throws FxWebException {
    
        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
                       
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Job Status object
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.job.status.find");

            // Get segmentation id for this client
            RequestProperties props = request.getRequestProperties();
            String orgId   = props.getClientId();  
            String transId = props.getTransactionId();

            String segId = ClientCache.getSegmentationId(orgId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + orgId, "AccountFind");
            }     
            Integer segmentationId = Integer.valueOf(segId);
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);
            

            // Set job id attribute
            String jobId = request.getJobId();
            if ( jobId != null && jobId.trim().length()>0) {
                sdkObject.setNameValuePair("WpJobId", jobId);         
            }
            
            // Set required transaction id
            sdkObject.setNameValuePair("TransactionId", transId);

            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
            

 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }        
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
        catch (Exception e) {
            log.error(e);
            throw new FxWebException(e.getMessage(), EXCEPTION_SOURCE);
        }  
       

    }
    

    
}
