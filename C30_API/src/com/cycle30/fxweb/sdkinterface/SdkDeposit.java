package com.cycle30.fxweb.sdkinterface;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;


import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;


/**
 * Class representing an C30SDK Deposit instance
 *
 */
public class SdkDeposit {

    private static final String EXCEPTION_SOURCE    = "SDK-PAYMENT";
    
    public static Logger log = Logger.getLogger(SdkDeposit.class);   
    
    /**
     * 
     * @param request
     */
    public HashMap<String, ?> createDeposit(ResourceRequestInf request,
                                             Document domDocument) 
                                 throws FxWebException, C30SDKInvalidAttributeException {

        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;
        
        try {
            
            // Instantiate the SDK factory, and get the account-find object.
            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Deposit create SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.create.deposit");

            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName);
    
            // Set the various deposit attributes.
            setDepositAttributes(sdkObject, domDocument);
     
            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();
 
            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");
            
            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }
            
            return results;
            

        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
            
        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw e;
            
        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }  
       

    }
    
    /**
     * 
     * Populate various deposit attributes based on what was passed in the Deposit
     * payload.
     * 
     * @param order
     * @param request
     */
    private void setDepositAttributes(C30SDKObject sdkObject, 
                                       Document dom) 
                         throws C30SDKInvalidAttributeException {
        

        // Get deposit properties
        String acctNo       = DomUtils.getElementValue(dom, "AccountNumber");
        String depositAmt   = DomUtils.getElementValue(dom, "Amount");
        String depositType  = DomUtils.getElementValue(dom, "DepositType");
        String userId       = DomUtils.getElementValue(dom, "ActionWho");
        String pmtMethod    = DomUtils.getElementValue(dom, "PaymentMethod");
        String ccAuthCode   = DomUtils.getElementValue(dom, "CreditCardAuthNo");
        String ccAuthDate   = DomUtils.getElementValue(dom, "CreditCardAuthDate");
        
        
        // Convert fraction amount to whole number, eg. 123.95 needs to be 12395.
        String intAmount = depositAmt;
        String temp[] = depositAmt.split("\\.");
        if (temp.length > 1) {
            intAmount = temp[0] + temp[1];
        }
        
        // get current date in MM/dd/yyyy format to satisfy underlying SDK
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String dateReceived = formatter.format(today);
        
        sdkObject.setNameValuePair("AccountExternalId", acctNo);
        sdkObject.setNameValuePair("DateReceived",      dateReceived);        
        sdkObject.setNameValuePair("DepositAmount",     intAmount);
        sdkObject.setNameValuePair("ChgWho",            userId);
        
        // Translate deposit type, payment type to internal codes. The values
        // passed were already validatd in RequestValidator.java
        String depositTypeCode = FxWebConstants.VALID_DEPOSIT_TYPES.get(depositType);
        sdkObject.setNameValuePair("DepositType", depositTypeCode);

        String paymentTypeCode = FxWebConstants.VALID_DEPOSIT_PAYMENT_TYPES.get(pmtMethod);
        sdkObject.setNameValuePair("PayMethod", paymentTypeCode);

        // if CC auth code provided, add it and the date as name/value pairs
        // purposes. Date needs to be converted from MM-DD to MMDD
        if (ccAuthCode != null) {
            sdkObject.setNameValuePair("ManualCcauthCode", ccAuthCode);
            String tempString = ccAuthDate.replaceAll("-", "");
            sdkObject.setNameValuePair("ManualCcauthDate", tempString);
        }

    }
    
}
