package com.cycle30.fxweb.sdkinterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.RequestProperties;
import com.cycle30.fxweb.request.ResourceRequestInf;



/**
 * Class representing an SDK invoice charges find instance
 *
 */
public class SdkInvoiceChargesFinder {


    private static final String INVC_FIND_ATTRIBUTE_NAME   = "ForceId";
    private static final String EXCEPTION_SOURCE  = "SDK-INVOICE-CHARGES-FIND";

    public static Logger log = Logger.getLogger(SdkInvoiceChargesFinder.class);


    /**
     *
     * Get basic invoice information.
     *
     * @param request
     */
    public HashMap<String, ?> invoiceChargesFind(ResourceRequestInf request)  throws FxWebException {

        C30SDKFrameworkFactory  sdkFactory = null;
        C30SDKObject sdkObject;

        try {

            sdkFactory = SdkConnectionFactory.getInstance().getSdkConnection(request);

            // Get the Invoice charges search SDK object name
            String sdkObjectName = SdkUtil.getSdkProperty("sdk.invoice.charge.search");

            // Get segmentation id for the client, and convert to integer
            RequestProperties requestProps = request.getRequestProperties();


            String clientId = requestProps.getClientId();
            String segId = ClientCache.getSegmentationId(clientId);
            if (segId == null) {
                throw new FxWebException("Could not retrieve segmentation id from cache for client id: " + clientId, "InvoiceChargesFind");
            }

            Integer segmentationId = Integer.valueOf(segId);

            // Instantiate SDK object
            sdkObject = sdkFactory.createC30SDKObject(sdkObjectName, segmentationId);


            // Set the external id attribute
            String externalId = requestProps.getAccountNo();
            if ( externalId != null && externalId.trim().length()>0) {
                sdkObject.setNameValuePair(INVC_FIND_ATTRIBUTE_NAME,   externalId);
            }

            // Set required transaction id
            String transId = requestProps.getTransactionId();
            sdkObject.setNameValuePair("TransactionId", transId);

             // Set required InvoiceId from the URL ***path is camelCase, SDK attr is Caps***
            sdkObject.setNameValuePair("InvoiceNumber", requestProps.getPathParams().getFirst("invoiceNumber"));

            // Track elapsed time
            long start = System.currentTimeMillis();

            // Process the transaction.
            C30SDKObject response = null;

            response = sdkObject.process();

            long elapsedTimeSeconds = (System.currentTimeMillis()-start) / 1000;
            log.info("SDK process() took " + elapsedTimeSeconds + " seconds to complete...");

            // Get the HashMap representation of the SDK results
            HashMap results = null;
            if (response != null) {
                results = response.toHashMap(true);
            }

            // Sanity check - must have something available.
            if (results == null || results.size()==0) {
                throw new FxWebException ("No result returned from SDK request", EXCEPTION_SOURCE);
            }

            return results;


        }  catch (C30SDKObjectException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);

        }  catch (C30SDKObjectConnectionException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);

        } catch (C30SDKInvalidAttributeException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);

        } catch (C30SDKInvalidConfigurationException e) {
            log.error(e);
            throw new FxWebException(e, EXCEPTION_SOURCE);
        }
        catch (C30SDKException lwe) {
            log.error(lwe);
            throw new FxWebException(lwe.getMessage(), EXCEPTION_SOURCE);
        }



    }


}
