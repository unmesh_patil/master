package com.cycle30.fxweb.cache;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.util.FxWebUtil;



/** Class for caching client API version information */
public class ClientAPIVersionCache {
    
    private static final Logger log  = Logger.getLogger(ClientAPIVersionCache.class);

    // Keep static map of client API version properties (inner class).
    private static HashMap<String, ClientAPIVersionProperties> clientapiversionCache = new HashMap<String, ClientAPIVersionProperties>();
    
    
    private static final String CLIENT_API_VERSION_QUERY = 
            "select client_id, c30_segmentation_id, api_name, api_version, api_type " +
            " from client_api_version";
    
    
    //------------------------------------------------
    // Spring-injected properties.
    //------------------------------------------------
    private static String dbUrl;
    private static String dbDriver;
    private static String dbUser;
    private static String password;
    

    public String getDbUrl() {
        return dbUrl;
    }
    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }
    public String getDbDriver() {
        return dbDriver;
    }
    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }
    public String getDbUser() {
        return dbUser;
    }
    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    //------------------------------------------------
    
    

    /**
     * Validate client id
     * 
     * @param clientId
     * @return true if client exists.
     */
    public static boolean clientExists(String clientId) {
        
    	ClientAPIVersionProperties clientapiVerProps = clientapiversionCache.get(clientId);
        if (clientapiVerProps == null) {
            return false;
        }
        
        return true;
    }
    
    
    
    /**
     * Get segmentation id associated with client id from cache.
     * 
     * @param clientId
     * @return segmentation id
     */
    public static String getSegmentationId(String clientId) {
        
        String segId = null;
        
        ClientAPIVersionProperties clientapiVerProps = clientapiversionCache.get(clientId);
        if (clientapiVerProps != null) {
            segId = clientapiVerProps.getSegmentationId();
        }

        return segId;
        
    }
    
    
    /**
     * Get API name value associated with client id from cache.
     * 
     * @param clientId
     * @return API name value
     */
    public static String getAPIName(String clientId) {
        
        String APIName = null;
        
        ClientAPIVersionProperties clientapiVerProps = clientapiversionCache.get(clientId);
        if (clientapiVerProps != null) {
            APIName = clientapiVerProps.getAPIName();
        }

        return APIName;
        
    }
    
    /**
     * Get API version value associated with client id from cache.
     * 
     * @param clientId
     * @return API version value
     */
    public static String getAPIVersion(String clientId) {
        
        String APIVersion = null;
        
        ClientAPIVersionProperties clientapiVerProps = clientapiversionCache.get(clientId);
        if (clientapiVerProps != null) {
        	APIVersion = clientapiVerProps.getAPIVersion();
        }

        return APIVersion;
        
    }    

    /**
     * Get API type value associated with client id from cache.
     * 
     * @param clientId
     * @return API type value
     */
    public static String getAPIType(String clientId) {
        
        String APIType = null;
        
        ClientAPIVersionProperties clientapiVerProps = clientapiversionCache.get(clientId);
        if (clientapiVerProps != null) {
        	APIType = clientapiVerProps.getAPIType();
        }

        return APIType;
        
    }    
    
    //--------------------------------------------------------------------------
    /** Initialization method (invoked at startup by Mule configuration).  Use
     *  it to load the CLIENT API VERSION from the database configuration.
     * 
     */
    public void initialize() throws FxWebException {
                
        Connection conn = null;
        
        try {

            // Get database connection.  
            Class.forName(dbDriver);
            String pwd = FxWebUtil.decryptPassword(password);
            conn = DriverManager.getConnection(dbUrl, dbUser, pwd);
            
            // Get the single URI associated with this environment.
            PreparedStatement ps = null;      
                
            log.info("Preparing SQL: " + CLIENT_API_VERSION_QUERY);
            ps = conn.prepareStatement(CLIENT_API_VERSION_QUERY);
            
            ResultSet rs = ps.executeQuery();

            // Update cache with each client found
            while (rs.next()) {
               String clientId		= rs.getString(1);
               String segId         = rs.getString(2);
               String apiName    	= rs.getString(3);
               String apiVersion 	= rs.getString(4);
               String apiType		= rs.getString(5);
               
               log.info("Caching org id: " + clientId + " segmentation id: " + segId+ " apiName: " + apiName);
               
               ClientAPIVersionProperties clientapiVerProps = new ClientAPIVersionProperties();
               clientapiVerProps.setClientId(clientId);
               clientapiVerProps.setSegmentationId(segId);
               clientapiVerProps.setAPIName(apiName);
               clientapiVerProps.setAPIVersion(apiVersion);
               clientapiVerProps.setAPIType(apiType);

               
               clientapiversionCache.put(clientId, clientapiVerProps);
               
            }
            
        } catch (SQLException se) {
            log.error(se);
            throw new FxWebException(se, "Client API version cache init");   
        } catch (ClassNotFoundException e) {
            throw new FxWebException (e, "Client API version cache init");
        }
        
        finally {
            if (conn != null) 
               try {
                 conn.close(); 
               } 
               catch (Exception e) {
                   log.error(e); 
               }
        }
            
    }
    
    
    /**
     * 
     * @return Simple string list of org/seg ids
     */
    public static String getClientList() {
        
        StringBuffer clientList = new StringBuffer("(Org/Seg ids) ");
        
        Iterator<String> i = clientapiversionCache.keySet().iterator();
        int count = 0;
        while (i.hasNext()) {
            String clientId = i.next();
            ClientAPIVersionProperties clientapiVerProps = clientapiversionCache.get(clientId);
            
            if (count > 0) {
                clientList.append(", ");
            }
            clientList.append(clientId + ":" + clientapiVerProps.getSegmentationId());
            count++;
        }
        
        return clientList.toString();
        
    }
    
   
    
    
    //-------------------------------------------------------------------------
    /** 
     * Inner class representing client API version properties 
     * 
     */
    private class ClientAPIVersionProperties {
        
        private String clientId;
        private String segmentationId;
        private String apiName;
        private String apiVersion;
        private String apiType;

        
        public String getClientId() {
            return clientId;
        }
        
        public void setClientId(String clientId) {
            this.clientId = clientId;
        }
        
        public String getSegmentationId() {
            return segmentationId;
        }
        public void setSegmentationId(String segmentationId) {
            this.segmentationId = segmentationId;
        }
        
        public String getAPIName() {
            return apiName;
        }
        public void setAPIName(String apiName) {
            this.apiName = apiName;
        }
        
        public String getAPIVersion() {
            return apiVersion;
        }
        public void setAPIVersion(String apiVersion) {
            this.apiVersion = apiVersion;
        }

        public String getAPIType() {
            return apiType;
        }
        public void setAPIType(String apiType) {
            this.apiType = apiType;
        }


    }
}
