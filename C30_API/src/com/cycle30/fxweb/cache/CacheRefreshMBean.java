package com.cycle30.fxweb.cache;

 
/**
 *
 */
public interface CacheRefreshMBean {

    public String refreshCache(String cacheName);
    
}
