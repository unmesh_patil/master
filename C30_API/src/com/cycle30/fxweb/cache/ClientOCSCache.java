package com.cycle30.fxweb.cache;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.object.C30SDKAttributeConstants;



/** This class will have all the details related to Subscriber and all the Balance Type and 
 * it's corresponding Units in the Cahce.
 * 
 */
public class ClientOCSCache {
    
    private static final Logger log  = Logger.getLogger(ClientOCSCache.class);

    // Keep static map of client API version properties (inner class).
    private static HashMap<String, OCSBalanceUnitsTypeProperties> ocsBalanceUnitsMappingCache = new HashMap<String, OCSBalanceUnitsTypeProperties>();
    
    private static HashMap<String, OCSGetAllSubcriberProfileTypeProperties> ocsGetAllSubscriberprofilesCache = new HashMap<String, OCSGetAllSubcriberProfileTypeProperties>();
    
    static HashMap<String, OCSSubscriberDetailsProperties> ocsSubscriberDetailsCache = new HashMap<String, OCSSubscriberDetailsProperties>();
    
	private static C30ProvisionRequest provRequestinstance=null;
	   
    ClientOCSCache(){
    	try {
    		provRequestinstance=C30ProvisionRequest.getInstance();
		} catch (C30ProvConnectorException e) {
			e.printStackTrace();
		}
    }
    
    //------------------------------------------------
    // Spring-injected properties.
    //------------------------------------------------
    private  String resellerLabel;

    
    public String getResellerLabel() {
		return this.resellerLabel;
	}

	public  void setResellerLabel(String resellerLabel) {
		this.resellerLabel = resellerLabel;
	}


    
/** Get the Balance Type and Units From the cache.
 * 
 * @return
 */
  public static  HashMap<String, String> getBalanceUnitsType() {
        
        HashMap<String, String>  BalanceTypeUnits = null;
        
        OCSBalanceUnitsTypeProperties ocsBalanceTypesUnitProps = ocsBalanceUnitsMappingCache.get(FxWebConstants.PREPAID_SEGMENT_CACHE_ID);
        if (ocsBalanceTypesUnitProps != null) {
        	BalanceTypeUnits = ocsBalanceTypesUnitProps.getBalanceUnitsType();
        }

        return BalanceTypeUnits;
        
    }    
  
  
  /** Get the SubscriberProfiles Balances From the cache.
   * 
   * @return
   */
    public static  HashMap<String, List<String>> getSubscriberProfileBalanceType(String profileName) {
          
          HashMap<String, List<String>>  susbcriberprofileBalanceTypes = null;
          
          OCSGetAllSubcriberProfileTypeProperties ocsProfilesProps = ocsGetAllSubscriberprofilesCache.get(FxWebConstants.PREPAID_SEGMENT_CACHE_ID);
          if (ocsProfilesProps != null) {
        	  susbcriberprofileBalanceTypes = ocsProfilesProps.getSubscriberProfiles(profileName);
          }

          return susbcriberprofileBalanceTypes;
          
      }    
    
  /**
   *  This method is invoked from the Spring configured Class in the server configuration
   * @throws FxWebException
   */
  
    public void initialize() throws FxWebException {
 
    	try {
    		
        	HashMap<String, String> PrepaidBalInquiry = new HashMap<String, String>();
			PrepaidBalInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_BALANCE_UNITS_CACHE);
			HashMap<?, ?> response = provRequestinstance.ocsInquiryRequest(PrepaidBalInquiry);
			String responseXML=(String) response.get(Constants.RESPONSE_XML);
			HashMap<String, String> balanceUnitsType =null;
			if(response.get(Constants.RESPONSE_XML) !=null && responseXML!=null ){
				balanceUnitsType=DomUtils.parseXMLForBalanceUnits(responseXML) ;
				OCSBalanceUnitsTypeProperties ocsBalanceUnitsProps = new OCSBalanceUnitsTypeProperties();
	            
	        	ocsBalanceUnitsProps.setBalanceUnitsType(balanceUnitsType);
	        	ocsBalanceUnitsMappingCache.put(FxWebConstants.PREPAID_SEGMENT_CACHE_ID, ocsBalanceUnitsProps);
	    
			}else{
				ocsBalanceUnitsMappingCache.put(FxWebConstants.PREPAID_SEGMENT_CACHE_ID, null);
			}
        	
        	loadAllSubscriberProfiles();
        	
        } catch (Exception e) {
        	log.info("Exception while Loading the OCS Static Data for Cahce" + e.getMessage());
        }
        
            
    }

    /** Load all the Subscriber profile and it's Balance Types in Cache
     * 
     * @throws C30ProvConnectorException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public  void  loadAllSubscriberProfiles() throws C30ProvConnectorException, ParserConfigurationException, SAXException, IOException{
    	HashMap<String, String> subscriberProfilesInquiry = new HashMap<String, String>();
    	subscriberProfilesInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_PROFILES_CACHE);
    	subscriberProfilesInquiry.put(FxWebConstants.OCS_RESELLER_LABEL, getResellerLabel());
		HashMap<?, ?> response = provRequestinstance.ocsInquiryRequest(subscriberProfilesInquiry);
		String responseXML=(String) response.get(Constants.RESPONSE_XML);
		HashMap<String, List<String>> subscriberProfiles =null;
		if(response.get(Constants.RESPONSE_XML) !=null && responseXML!=null ){
			subscriberProfiles=DomUtils.parseXMLForSubscriberProfiles(responseXML) ;
			OCSGetAllSubcriberProfileTypeProperties ocsProfilesProps = new OCSGetAllSubcriberProfileTypeProperties();
	        
			ocsProfilesProps.setSubscriberProfiles(subscriberProfiles);
			ocsGetAllSubscriberprofilesCache.put(FxWebConstants.PREPAID_SEGMENT_CACHE_ID,ocsProfilesProps );	        
		}else{
			ocsGetAllSubscriberprofilesCache.put(FxWebConstants.PREPAID_SEGMENT_CACHE_ID,null );
		}
		
      }
    
  //-------------------------------------------------------------------------
    /** 
     * Inner class representing SubcriberProfiles properties 
     * 
     */
    private static class OCSGetAllSubcriberProfileTypeProperties {
    	HashMap<String, List<String>> subscriberProfileBalanceType=null;

		public HashMap<String, List<String>> getSubscriberProfiles(String profileBalanceType) {
			return subscriberProfileBalanceType;
		}

		public void setSubscriberProfiles(HashMap<String, List<String>> subscriberProfiles) {
			this.subscriberProfileBalanceType = subscriberProfiles;
		} 
    }

    /** get the Subscriber Details from the cache
     * 
     * @param subscriberId
     * @return
     * @throws FxWebException 
     */
    public static  HashMap<String, String> getSubscriberDetails(String subscriberId,String transactionId) throws FxWebException {
        
        HashMap<String, String>  subscriberDetails = null;
        
        OCSSubscriberDetailsProperties ocsSubscriberProps = ocsSubscriberDetailsCache.get(subscriberId);
        if (ocsSubscriberProps != null) {
        	subscriberDetails = ocsSubscriberProps.getSubscriberDetails(subscriberId);
        }
        else{ // added code changes for PSST -345
        	try {
        		setSubscriberDetailsInCache(subscriberId,transactionId);
				OCSSubscriberDetailsProperties ocsSubscriberPropts = ocsSubscriberDetailsCache.get(subscriberId);
			    if (ocsSubscriberPropts != null) {
			       	subscriberDetails = ocsSubscriberPropts.getSubscriberDetails(subscriberId);
			      }
			} catch (FxWebException e) {
				 log.info("Exception while loading get subscriber props from Cache " + e.getMessage());
		        	throw new FxWebException(e, subscriberId);
		           
			}
        }	
        return subscriberDetails;
        
    }    
    
    
    /** Clear the cache for  Subsrciber Details from the cache
     * 
     * @param subscriberId
     * @return
     */
    public static synchronized void clearSubscriberDetailsInCache(String subscriberId) {
        OCSSubscriberDetailsProperties ocsSubscriberProps = ocsSubscriberDetailsCache.get(subscriberId);
        if (ocsSubscriberProps != null) {
        	ocsSubscriberProps=null;
        	ocsSubscriberDetailsCache.put(subscriberId, ocsSubscriberProps);
        }
    }    
    
	//-------------------------------------------------------------------------
    /** 
     * Inner class representing Balance Units Mapping properties 
     * 
     */
    private static class OCSBalanceUnitsTypeProperties {
        
        private HashMap<String, String> BalanceUnitsType;
     	
        
        public HashMap<String, String> getBalanceUnitsType() {
			return BalanceUnitsType;
		}
		public void setBalanceUnitsType(HashMap<String, String> balanceUnitsType) {
			BalanceUnitsType = balanceUnitsType;
		}
	 }
    
   /**
    * Set the Subscriber Details in the Cache from the C30 API Resource classes, if not present in the cache.
    * @param subscriberId
    * @throws FxWebException
    */
    public static synchronized void setSubscriberDetailsInCache(String subscriberId, String transId) throws FxWebException {
    	
    	try {
        	HashMap<String, String> PrepaidSubsriberDetails = new HashMap<String, String>();
        	PrepaidSubsriberDetails.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_DETAILS);
        	PrepaidSubsriberDetails.put(Constants.EXTERNAL_TRANSACTION_ID, transId);
        	PrepaidSubsriberDetails.put(FxWebConstants.OCS_PREPAID_SUBSCRIBER_NUMBER, subscriberId);
   			HashMap<?, ?> response = provRequestinstance.ocsInquiryRequest(PrepaidSubsriberDetails);
   			if(response.get(Constants.RESPONSE_XML) !=null) {
			String responseXML=(String) response.get(Constants.RESPONSE_XML);
			HashMap<Integer, HashMap<String, String>> subscriberDetails =null;
			if(response.get(Constants.RESPONSE_XML) !=null && responseXML!=null ){
				subscriberDetails=DomUtils.parseXMLForSubcriberDetails(responseXML) ;
			}
			OCSSubscriberDetailsProperties ocsSubscriberProps = new OCSSubscriberDetailsProperties();
            if(subscriberDetails.get(0) !=null){
			ocsSubscriberProps.setSubscriberDetails(subscriberDetails.get(0));
			ocsSubscriberDetailsCache.put(subscriberId, ocsSubscriberProps);
            }
   		 }
        } catch (Exception e) {
        	 log.info("Exception while loading the subscriber details in Cache " + e.getMessage());
        	throw new FxWebException(e, subscriberId);
           
        }
            
    }

	//-------------------------------------------------------------------------
    /** 
     * Inner class representing Subscriber details properties 
     * 
     */  
    private static class OCSSubscriberDetailsProperties {
        
        private HashMap<String, String> subscriberDetails;

		public HashMap<String, String> getSubscriberDetails(String SubscriberId) {
			return subscriberDetails;
		}

		public void setSubscriberDetails(HashMap<String, String> subscriberDetails) {
			this.subscriberDetails = subscriberDetails;
		}
     	
    
	 }
}
