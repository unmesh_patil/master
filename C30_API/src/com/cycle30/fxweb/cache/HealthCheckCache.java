package com.cycle30.fxweb.cache;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.util.FxWebUtil;



/** Class for caching service health check information */
public class HealthCheckCache {
    
    private static final Logger log  = Logger.getLogger(HealthCheckCache.class);

    // Keep static map of health check properties (inner class).
    private static HashMap<String, HealthCheckProperties> healthCheckCache = new HashMap<String, HealthCheckProperties>();
    
    
    private static final String CLIENT_QUERY = 
            "select client_id, c30_segmentation_id, gsm_health_check_url " +
            " from health_check";
    
    
    //------------------------------------------------
    // Spring-injected properties.
    //------------------------------------------------
    private static String dbUrl;
    private static String dbDriver;
    private static String dbUser;
    private static String password;
    private static String envName;
    

    public String getDbUrl() {
        return dbUrl;
    }
    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }
    public String getDbDriver() {
        return dbDriver;
    }
    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }
    public String getDbUser() {
        return dbUser;
    }
    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    //------------------------------------------------
    

    
    
    /**
     * Get GSM URL associated with client id from cache.
     * 
     * @param clientId
     * @return segmentation id
     */
    public static String getGsmUrl(String clientId) {
        
        String segId = null;
        
        HealthCheckProperties clientProps = healthCheckCache.get(clientId);
        if (clientProps != null) {
            segId = clientProps.getGsmUrl();
        }

        return segId;
        
    }
    

    

    //--------------------------------------------------------------------------
    /** Initialization method (invoked at startup by Mule configuration).  Use
     *  it to load the NSG validation URL from the database configuration.
     * 
     */
    public void initialize() throws FxWebException {
                
        Connection conn = null;
        
        try {

            // Get database connection.  
            Class.forName(dbDriver);
            String pwd = FxWebUtil.decryptPassword(password);
            conn = DriverManager.getConnection(dbUrl, dbUser, pwd);
            
            // Get the single URI associated with this environment.
            PreparedStatement ps = null;      
                
            log.info("Preparing SQL: " + CLIENT_QUERY);
            ps = conn.prepareStatement(CLIENT_QUERY);
            
            ResultSet rs = ps.executeQuery();

            // Update cache with each client found
            while (rs.next()) {
               String clientId = rs.getString(1);
               String segId    = rs.getString(2);
               String gsmHcUrl = rs.getString(3);
               
               log.info("Caching health check information. Id: " + clientId + " segmentation id: " + segId + " GSM URL: " + gsmHcUrl);
               
               HealthCheckProperties hcProps = new HealthCheckProperties();
               hcProps.setClientId(clientId);
               hcProps.setSegmentationId(segId);
               hcProps.setGsmUrl(gsmHcUrl);
               
               healthCheckCache.put(clientId, hcProps);
               
            }
            

            
        } catch (SQLException se) {
            log.error(se);
            throw new FxWebException(se, "Health check cache init");   
        } catch (ClassNotFoundException e) {
            throw new FxWebException (e, "Health check  cache init");
        }
        
        finally {
            if (conn != null) 
               try {
                 conn.close(); 
               } 
               catch (Exception e) {
                   log.error(e); 
               }
        }
            
    }
    
    
    /**
     * 
     * @return Simple string list of org/seg ids
     */
    public static String getHealthCheckList() {
        
        StringBuffer clientList = new StringBuffer("(Org/Seg/GSM Url) ");
        
        Iterator<String> i = healthCheckCache.keySet().iterator();
        int count = 0;
        while (i.hasNext()) {
            String clientId = i.next();
            HealthCheckProperties props = healthCheckCache.get(clientId);
            
            if (count > 0) {
                clientList.append(", ");
            }
            clientList.append(clientId + ":" + props.getSegmentationId() + ":" + props.getGsmUrl());
            count++;
        }
        
        return clientList.toString();
        
    }
    
    
    
    
    
    //-------------------------------------------------------------------------
    /** 
     * Inner class representing health check properties 
     * 
     */
    private class HealthCheckProperties {
        
        private String clientId;
        private String segmentationId;
        private String gsmUrl;


        public String getGsmUrl() {
            return gsmUrl;
        }
        public void setGsmUrl(String gsmUrl) {
            this.gsmUrl = gsmUrl;
        }
        
        public String getClientId() {
            return clientId;
        }
        public void setClientId(String clientId) {
            this.clientId = clientId;
        }
        
        public String getSegmentationId() {
            return segmentationId;
        }
        public void setSegmentationId(String segmentationId) {
            this.segmentationId = segmentationId;
        }
        


    }
}
