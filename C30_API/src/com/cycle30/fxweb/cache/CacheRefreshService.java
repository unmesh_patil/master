package com.cycle30.fxweb.cache;

import com.cycle30.fxweb.cache.CacheRefreshMBean;
import com.cycle30.fxweb.exception.FxWebException;

/**
 * 
 * MBean class to allow on-demand refresh a specific cache.
 *
 */
public class CacheRefreshService implements CacheRefreshMBean{
    

    private String result="";
    private String cacheName = "Client";
    
    
    /**
     * @return the cacheName
     */
    public String getCacheName() {
        return cacheName;
    }



    
    /**
     * Refresh specific cache based on the name.
     * 
     */
    public String refreshCache(String cacheId) {
        
        result = "Ok";
        
        if (cacheId != null && cacheId.length()>0) {
            this.cacheName = cacheId;
        }
        
        // Refresh the static  client cache Map
        if (cacheName.equalsIgnoreCase("Client")) {
            try {
                new ClientCache().initialize(); 
                result = ClientCache.getClientList();
            } catch (FxWebException fwe) {
                result = fwe.getMessage();
            }
        } 
        else {
            result = "Cache '" + cacheName + "' not recognized...";
        }
        
        return result;
        
    }

}
