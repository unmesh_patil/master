package com.cycle30.fxweb.cache;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.util.FxWebUtil;



/** Class for caching client information */
public class ClientCache {
    
    private static final Logger log  = Logger.getLogger(ClientCache.class);

    // Keep static map of client properties (inner class).
    private static HashMap<String, ClientProperties> clientCache = new HashMap<String, ClientProperties>();
    
    
    private static final String CLIENT_QUERY = 
            "select client_id, c30_segmentation_id, consumer_key, consumer_secret, org_token, username, api_version " +
            " from client";
    
    
    //------------------------------------------------
    // Spring-injected properties.
    //------------------------------------------------
    private static String dbUrl;
    private static String dbDriver;
    private static String dbUser;
    private static String password;
    private static String envName;
    

    public String getDbUrl() {
        return dbUrl;
    }
    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }
    public String getDbDriver() {
        return dbDriver;
    }
    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }
    public String getDbUser() {
        return dbUser;
    }
    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    //------------------------------------------------
    
    

    /**
     * Validate client id
     * 
     * @param clientId
     * @return true if client exists.
     */
    public static boolean clientExists(String clientId) {
        
        ClientProperties clientProps = clientCache.get(clientId);
        if (clientProps == null) {
            return false;
        }
        
        return true;
    }
    
    
    
    /**
     * Get segmentation id associated with client id from cache.
     * 
     * @param clientId
     * @return segmentation id
     */
    public static String getSegmentationId(String clientId) {
        
        String segId = null;
        
        ClientProperties clientProps = clientCache.get(clientId);
        if (clientProps != null) {
            segId = clientProps.getSegmentationId();
        }

        return segId;
        
    }
    
    
    /**
     * Get consumer secret value associated with client id from cache.
     * 
     * @param clientId
     * @return secret hash value
     */
    public static String getConsumerSecret(String clientId) {
        
        String secret = null;
        
        ClientProperties clientProps = clientCache.get(clientId);
        if (clientProps != null) {
            secret = clientProps.getConsumerSecret();
        }

        return secret;
        
    }
    
    /**
     * Get API version associated with client id from cache.
     * 
     * @param clientId
     * @return API version value
     */
    public static String getVersionInfoByClientId(String clientId) {
        
        String apiVersion = null;
        
        ClientProperties clientProps = clientCache.get(clientId);
        if (clientProps != null) {
        	apiVersion = clientProps.getVersionInfo();
        }

        return apiVersion;
        
    }
    
    /**
     * Get API version associated with Account Segment id from cache.
     * 
     * @param clientId
     * @return API version value
     */
    public static String getAccountSegmentByClientId(String clientId) {
        
        String acctSegId = null;
        
        ClientProperties clientProps = clientCache.get(clientId);
        if (clientProps != null) {
        	acctSegId = clientProps.getSegmentationId();
        }

        return acctSegId;
        
    }

    /**Get API version associated with  Segment Id
     * 
     * @param segmentationId
     * @return
     */
	public static String getVersionInfoByAcctSegmentId(String segmentationId) {
		 String apiVersion = null;
	        
	        ClientProperties clientProps = clientCache.get(segmentationId);
	        if (clientProps != null) {
	        	apiVersion = clientProps.getVersionInfo();
	        }

	        return apiVersion;
	}
    //--------------------------------------------------------------------------
    /** Initialization method (invoked at startup by Mule configuration).  Use
     *  it to load the NSG validation URL from the database configuration.
     * 
     */
    public void initialize() throws FxWebException {
                
        Connection conn = null;
        
        try {

            // Get database connection.  
            Class.forName(dbDriver);
            String pwd = FxWebUtil.decryptPassword(password);
            conn = DriverManager.getConnection(dbUrl, dbUser, pwd);
            
            // Get the single URI associated with this environment.
            PreparedStatement ps = null;      
                
            log.info("Preparing SQL: " + CLIENT_QUERY);
            ps = conn.prepareStatement(CLIENT_QUERY);
            
            ResultSet rs = ps.executeQuery();

            // Update cache with each client found
            while (rs.next()) {
               String clientId       = rs.getString(1);
               String segId          = rs.getString(2);
               String consumerKey    = rs.getString(3);
               String consumerSecret = rs.getString(4);
               String orgToken       = rs.getString(5);
               String userName       = rs.getString(6);
               String apiVersion     = rs.getString(7);               
               
               log.info("Caching org id: " + clientId + " segmentation id: " + segId);
               
               ClientProperties clientProps = new ClientProperties();
               clientProps.setClientId(clientId);
               clientProps.setSegmentationId(segId);
               clientProps.setConsumerKey(consumerKey);
               clientProps.setConsumerSecret(consumerSecret);
               clientProps.setToken(orgToken);
               clientProps.setUserName(userName);
               clientProps.setVersionInfo(apiVersion);
               
               clientCache.put(clientId, clientProps);
               if (apiVersion!=null && !apiVersion.equalsIgnoreCase(""))
            	   clientCache.put(segId, clientProps);

               
            }
            

            
        } catch (SQLException se) {
            log.error(se);
            throw new FxWebException(se, "Client cache init");   
        } catch (ClassNotFoundException e) {
            throw new FxWebException (e, "Client cache init");
        }
        
        finally {
            if (conn != null) 
               try {
                 conn.close(); 
               } 
               catch (Exception e) {
                   log.error(e); 
               }
        }
            
    }
    
    
    /**
     * 
     * @return Simple string list of org/seg ids
     */
    public static String getClientList() {
        
        StringBuffer clientList = new StringBuffer("(Org/Seg ids) ");
        
        Iterator<String> i = clientCache.keySet().iterator();
        int count = 0;
        while (i.hasNext()) {
            String clientId = i.next();
            ClientProperties props = clientCache.get(clientId);
            
            if (count > 0) {
                clientList.append(", ");
            }
            clientList.append(clientId + ":" + props.getSegmentationId());
            count++;
        }
        
        return clientList.toString();
        
    }
    
    
    
    
    
    //-------------------------------------------------------------------------
    /** 
     * Inner class representing client properties 
     * 
     */
    private class ClientProperties {
        
        private String clientId;
        private String segmentationId;
        private String consumerKey;
        private String consumerSecret;
        private String token;
        private String userName;
        private String apiVersion;        

        
        public String getClientId() {
            return clientId;
        }
        public void setClientId(String clientId) {
            this.clientId = clientId;
        }
        
        public String getSegmentationId() {
            return segmentationId;
        }
        public void setSegmentationId(String segmentationId) {
            this.segmentationId = segmentationId;
        }
        
        public String getConsumerKey() {
            return consumerKey;
        }
        public void setConsumerKey(String consumerKey) {
            this.consumerKey = consumerKey;
        }
        
        public String getConsumerSecret() {
            return consumerSecret;
        }
        public void setConsumerSecret(String consumerSecret) {
            this.consumerSecret = consumerSecret;
        }

        public String getToken() {
            return token;
        }
        public void setToken(String token) {
            this.token = token;
        }

        public String getUserName() {
            return userName;
        }
        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getVersionInfo() {
            return apiVersion;
        }
        public void setVersionInfo(String apiVersion) {
            this.apiVersion = apiVersion;
        }
        
        
    }





}
