package com.cycle30.fxweb.log;

import java.io.StringWriter;


import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;


import com.cycle30.audit.AuditLogger;
import com.cycle30.audit.data.AuditRequest;
import com.cycle30.audit.data.AuditResponse;
import com.cycle30.audit.exception.AuditException;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.request.ResourceRequestInf;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;


/**
 * 
 * Class to handle database audit-logging functionality.
 *
 */
public class FxWebLogger {

    private static final String AUDIT_APPLICATION_ID = "FX_WEB_ENABLEMENT";
    private static final String EXCEPTION_SOURCE = "AUDIT-LOGGING";   
    private static Logger log = Logger.getLogger(FxWebLogger.class);  
    

    
    private long auditId;
  
    /**
     * Create request audit entry.
     * 
     * @param request
     */
    public void auditRequest(ResourceRequestInf request) throws FxWebException {
        
        AuditRequest ar = new AuditRequest();
        AuditLogger  al = new AuditLogger();
        
        HttpHeaders headers = request.getRequestProperties().getHeaders();
        
        String clientId = FxWebUtil.getHeaderValue(headers, FxWebConstants.ORG_ID_HEADER_NAME);
        if (clientId == null || clientId.length()==0) {
            clientId = FxWebUtil.getHeaderValue(headers, FxWebConstants.PROVIDER_ID_HEADER_NAME);
            if (clientId == null || clientId.length()==0) clientId = "unknown";
        }
        
        // Set audit request properties.
        ar.setApplicationId(AUDIT_APPLICATION_ID);
        
        ar.setClientId(clientId);
        ar.setUserId("");
        ar.setResourceMethod(FxWebUtil.getRequestMethod(headers));
        ar.setReponseFormat(FxWebUtil.getHeaderValue(headers, "Accept"));
        String host = FxWebUtil.getHeaderValue(headers, "Host");
        String req  = FxWebUtil.getHeaderValue(headers, "http.request");
        ar.setResourceRequested(host + req);
        ar.setClientTransId(FxWebUtil.getHeaderValue(headers, FxWebConstants.TRANS_ID_HEADER_NAME));

        // Get payload string for PUT and POST requests.
        Document dom = request.getRequestProperties().getPayload();
        String payload = getPayloadString(dom);
        
        ar.setPayloadReceived(payload);
        
        
        try {
            
           auditId = al.logRequest(ar);
           request.getRequestProperties().setInternalId(Long.toString(auditId));
           
           // Write details to log file
           logRequestDetails(ar, auditId);
           
        }
        catch (AuditException ae) {
            throw new FxWebException(ae, Response.Status.INTERNAL_SERVER_ERROR, EXCEPTION_SOURCE);
        }  
        
    }
    
    /**
     * Create response audit entry.
     * 
     * @param response
     */
    public void auditResponse(ResourceRequestInf request, ResourceResponseInf response) throws FxWebException {
        
        AuditResponse ar = new AuditResponse();
        AuditLogger   al = new AuditLogger();
        
        ar.setApplicationId(AUDIT_APPLICATION_ID);
        ar.setMessageId(auditId);
        
        // Get response body value/type
        Object o = response.getResponseBody();
        String respStr = "";
        if (o instanceof String) {
            respStr = (String)o;
        } else {
            respStr = o.getClass().getName();
        }    
            
        ar.setResponseValue(respStr);

        
        int status = response.getResponseCode().getStatusCode();
        ar.setReponseCode(String.valueOf(status));
        
        HttpHeaders headers = request.getRequestProperties().getHeaders();
        ar.setClientTransId(FxWebUtil.getHeaderValue(headers, FxWebConstants.TRANS_ID_HEADER_NAME));
        
        
        try {
            
            al.logResponse(ar);
            
            // Write details to log file
            logResponseDetails(ar);
            
         }
         catch (AuditException ae) {
             throw new FxWebException(ae, Response.Status.INTERNAL_SERVER_ERROR, EXCEPTION_SOURCE);
         } 
    }
        
    
    /**
     * 
     * @param dom
     * @return
     */
    private String getPayloadString(Document dom) throws FxWebException {
        
        String payload = "";
        
        if (dom == null)  {
            return null;
        }
        
        try {
            
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(dom);
            transformer.transform(source, result);
            
            return result.getWriter().toString();
          } 
        catch(TransformerException ex) {
            log.error(ex);
            throw new FxWebException(ex, Response.Status.INTERNAL_SERVER_ERROR, EXCEPTION_SOURCE);
          }
        
    }
    

   
    /**
     * 
     * @param ar
     */
    private void logRequestDetails(AuditRequest ar, long auditId) {  
                
        // Need to obfuscate the password on authentication requests.
        String resource = ar.getResourceRequested();
        String payload  = ar.getPayloadReceived();
        
        log.info("");
        log.info("    Request details:");
        log.info("     Application id: " + ar.getApplicationId());
        log.info("           Audit id: " + auditId);
        log.info("          Client id: " + ar.getClientId());
        log.info("            User id: " + ar.getUserId());
        log.info("        HTTP method: " + ar.getResourceMethod());
        log.info("           Resource: " + resource);
        log.info("   PUT/POST payload: " + payload);
        log.info("    Response format: " + ar.getReponseFormat());   
        log.info("");
    }
    
    /**
     * 
     * @param ar
     */
    private void logResponseDetails(AuditResponse ar) {  
        log.info("");
        log.info("  Response details:");
        log.info("    Application id:   " + ar.getApplicationId());
        log.info("        Message id:   " + ar.getMessageId());
        log.info("     Response code:   " + ar.getReponseCode());
        log.info("     Response body:   " + ar.getResponseValue());
        log.info("");
    }
    
}
