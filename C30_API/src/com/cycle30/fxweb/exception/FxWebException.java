package com.cycle30.fxweb.exception;


import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;

import com.cycle30.fxweb.response.ResponseTypes;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;
import com.cycle30.sdk.exception.C30SDKException;


/** 
 * General purpose exception handler for FxWeb requests
 * 
 *
 */
public class FxWebException extends Exception {
        
        
    private static final Logger log = Logger.getLogger(FxWebException.class);

    
    // Use this as the HTTP response code if set w/i the application when 
    // an exception is caught. 
    private Response.Status responseCode;
    private String msgCode;
    
    // Standardized message in either XML or JSON format.  This will be returned
    // to the client when an error occurs vs. the stacktrace message.
    private String formattedMessage;
    
    
    // Indicator for where exception occurred, eg SDK, audit-logger, etc.
    private String sourceSystem;
    
    

    //--------------------------------------------------------------------------
    //  Getters/setters
    //--------------------------------------------------------------------------
    
    /**
     * @return the sourceSystem
     */
    public String getSourceSystem() {
        return sourceSystem;
    }
    /**
     * @param sourceSystem the sourceSystem to set
     */
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
    /**
     * @return the formattedMessage
     */
    public String getFormattedMessage() {
        return formattedMessage;
    }
    /**
     * @param formattedMessage the formattedMessage to set
     */
    public void setFormattedMessage(String formattedMessage) {
        this.formattedMessage = formattedMessage;
    }
    /**
     * @return the responseCode
     */
    public Response.Status getResponseCode() {
        return responseCode;
    }
    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(Response.Status responseCode) {
        this.responseCode = responseCode;
    }

    
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    
    /**
     * 
     * @param e original exception.
     * @param status
     */
    public FxWebException(Exception e, Response.Status status, String source, ResponseTypes.response resp) {
        super(e);
        responseCode = status;
        sourceSystem = source;
        formatMessage(e.getMessage(), resp);
        log.error(e);     
    }


    /**
     * 
     * @param e original exception.
     * @param status
     */
    public FxWebException(Exception e, Response.Status status, String source) {
        super(e);
        responseCode = status;
        sourceSystem = source;
        formatMessage(e);
        log.error(e);     
    }


    /**
     * 
     * @param str String exception message.
     */
    public FxWebException(String str, String source) {
        super(str);
        
        // default response code (assume request was invalid)
        this.responseCode = Response.Status.BAD_REQUEST; 
        sourceSystem = source;
        formatMessage(str);
        log.error(str);
    } 
    

    /**
     */
    public FxWebException(Exception e, String source) {
        super(e);
        
        // default response code (assume request was invalid)
        this.responseCode = Response.Status.BAD_REQUEST; 
        sourceSystem = source;
        if (e instanceof C30SDKException) 
        	msgCode = ((C30SDKException) e).getExceptionCode();
        
        formatMessage(e);
        log.error(e);
    }

    

    public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	/**
     * Format the exception message into XML/JSON
     */
    private void formatMessage(Exception e) {
        
        String msg = "";
        if (e == null) {
            msg = "(no message available from FxWebException.formatMessage e.getMessage())";
        } 
        else {
            msg = e.getMessage();
        }
        this.formattedMessage = FxWebConstants.XML_HEADER + 
                                 FxWebUtil.formatXmErrorlMessage(msg, responseCode, sourceSystem);  
    }
    
    
    /**
     * Format the exception message into XML/JSON
     */
    private void formatMessage(String msg) {
        this.formattedMessage = FxWebConstants.XML_HEADER + 
                                 FxWebUtil.formatXmErrorlMessage(msg, responseCode, sourceSystem);  
    }
    
    
    
    /**
     * Format the exception message into XML/JSON
     */
    private void formatMessage(String msg, ResponseTypes.response resp) {
        
        // wrap the error with the response type, eg. <AccountSearchResponse> based 
        // on the enum value passed
        
        this.formattedMessage = FxWebConstants.XML_HEADER + 
                                "<" + resp + ">" + 
                                 FxWebUtil.formatXmErrorlMessage(msg, responseCode, sourceSystem) +
                                "</" + resp + ">";
    }
    
 

}
