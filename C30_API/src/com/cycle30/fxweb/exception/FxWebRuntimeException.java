package com.cycle30.fxweb.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;


/**
 * 
 * Class for implementing JAX-RS unchecked exception that can be thrown/handled
 * by the container.
 *
 */
public class FxWebRuntimeException extends WebApplicationException {

    
    // Exception that will use the status code set w/i the application
    public FxWebRuntimeException(FxWebException fwe) {
        super(Response.status(fwe.getResponseCode()).entity(fwe.getFormattedMessage()).type("text/plain").build());
    }
    
    
    // Default/generic 'bad request' exception.
    public FxWebRuntimeException(String message) {
    //    super(Response.status(Response.Status.BAD_REQUEST).entity(message).type("text/plain").build());
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(message).type("text/plain").build());
    }
    
}