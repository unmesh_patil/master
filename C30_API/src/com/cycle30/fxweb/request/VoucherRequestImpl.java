package com.cycle30.fxweb.request;




/**
 * 
 *  Class representing a OCS Voucher resource request
 *
 */
public class VoucherRequestImpl implements ResourceRequestInf {

    
    // General request properties
    private RequestProperties requestProperties;
    
    
    // Voucher-Specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public RequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general and Voucher-Specific properties
     * 
     * @param requestProperties the requestProperties to set
     */
    public void setRequestProperties(RequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set Voucher-Specific properties...
    }

    
}
