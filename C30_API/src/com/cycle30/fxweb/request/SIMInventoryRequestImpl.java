package com.cycle30.fxweb.request;

public class SIMInventoryRequestImpl implements ResourceRequestInf {
	
	   // General request properties
 private RequestProperties requestProperties;


 /**
  * @return the requestProperties
  */
 public RequestProperties getRequestProperties() {
     return requestProperties;
 }

 
 /**
  * Set general and account-specific properties
  * 
  * @param requestProperties the requestProperties to set
  */
 public void setRequestProperties(RequestProperties requestProperties) {
     
     this.requestProperties = requestProperties;

 }


}
