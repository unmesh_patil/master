package com.cycle30.fxweb.request;



import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.w3c.dom.Document;


public class RequestProperties {

    private String accountNo;
    private String userId;
    private String clientId;;
    private String sessionId;
    private String URI;
    private MultivaluedMap queryParams;
    private MultivaluedMap pathParams;
    private HttpHeaders headers;
    private Document payload;
    private String transactionId;
    private String requestError;
    private String internalId;
    private String sdkApiVersion;



    public String getSdkApiVersion() {
		return sdkApiVersion;
	}
	public void setSdkApiVersion(String sdkApiVersion) {
		this.sdkApiVersion = sdkApiVersion;
	}
	public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public String getInternalId() {
        return internalId;
    }
    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }
    public String getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    public String getRequestError() {
        return requestError;
    }
    public void setRequestError(String requestError) {
        this.requestError = requestError;
    }
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    public Document getPayload() {
        return payload;
    }
    public void setPayload(Document payload) {
        this.payload = payload;
    }
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    public HttpHeaders getHeaders() {
        return headers;
    }
    public void setHeaders(HttpHeaders headers) {
        this.headers = headers;
    }
    public MultivaluedMap getPathParams() {
        return pathParams;
    }
    public void setPathParams(MultivaluedMap pathParams) {
        this.pathParams = pathParams;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getURI() {
        return URI;
    }
    public void setURI(String uRI) {
        URI = uRI;
    }
    public MultivaluedMap getQueryParams() {
        return queryParams;
    }
    public void setQueryParams(MultivaluedMap queryParams) {
        this.queryParams = queryParams;
    }



}
