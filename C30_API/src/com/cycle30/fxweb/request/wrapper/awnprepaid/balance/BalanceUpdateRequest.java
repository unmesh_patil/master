package com.cycle30.fxweb.request.wrapper.awnprepaid.balance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Account")
@XmlAccessorType(XmlAccessType.FIELD)
public class BalanceUpdateRequest {

	
	@XmlElement(name="BalanceList")
    private BalanceList Balst;

	public BalanceList getBalst() {
		return Balst;
	}

	public void setBalst(BalanceList balst) {
		Balst = balst;
	}

	
	
}
