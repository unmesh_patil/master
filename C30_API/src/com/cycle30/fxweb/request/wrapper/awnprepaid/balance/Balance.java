package com.cycle30.fxweb.request.wrapper.awnprepaid.balance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Balance")
@XmlAccessorType(XmlAccessType.FIELD)
public class Balance {

	@XmlElement
	private String BalanceTypeLabel;

	@XmlElement
	private String BalanceReason;

	@XmlElement
	private BalanceUnits Balunits;

	@XmlElement
	private String ExpirationDate;

	@XmlElement
	private String VoucherPINCode;

	@XmlEnum(String.class)
	public enum BalanceAction {
		WalletBalance, VoucherRedeem, AdjustWalletBalance, BalanceRefill
	};

	@XmlAttribute
	private String sequence;

	public String getBalanceTypeLabel() {
		return BalanceTypeLabel;
	}

	public void setBalanceTypeLabel(String balanceTypeLabel) {
		BalanceTypeLabel = balanceTypeLabel;
	}

	public String getBalanceReason() {
		return BalanceReason;
	}

	public void setBalanceReason(String balanceReason) {
		BalanceReason = balanceReason;
	}

	public BalanceUnits getBalunits() {
		return Balunits;
	}

	public void setBalunits(BalanceUnits balunits) {
		Balunits = balunits;
	}

	public String getExpirationDate() {
		return ExpirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		ExpirationDate = expirationDate;
	}

	public String getVoucherPINCode() {
		return VoucherPINCode;
	}

	public void setVoucherPINCode(String voucherPINCode) {
		VoucherPINCode = voucherPINCode;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

}
