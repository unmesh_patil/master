package com.cycle30.fxweb.request.wrapper.awnprepaid.balance;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "<BalanceList>")
@XmlAccessorType(XmlAccessType.FIELD)
public class BalanceList {

	@XmlElement(name = "Balance")
	private List<Balance> balance;

	@XmlAttribute
	private String Size;

	public String getSize() {
		return Size;
	}

	public void setSize(String size) {
		Size = size;
	}

	public List<Balance> getBalance() {
		return balance;
	}

	public void setBalance(List<Balance> balance) {
		this.balance = balance;
	}

}
