package com.cycle30.fxweb.request.wrapper.awnprepaid.balance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BalanceUnits")
@XmlAccessorType(XmlAccessType.FIELD)
public class BalanceUnits {
	
	  @XmlElement
	    private String Type;
	    
	    @XmlElement
	    private String Value;

		public String getType() {
			return Type;
		}

		public void setType(String type) {
			Type = type;
		}

		public String getValue() {
			return Value;
		}

		public void setValue(String value) {
			Value = value;
		}
	    

}
