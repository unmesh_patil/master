package com.cycle30.fxweb.request.wrapper.inventory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Inventory")
@XmlAccessorType(XmlAccessType.FIELD)
public class Inventory {
	
	    @XmlElement
	    private String TelephoneNumber;
	    
	    @XmlElement
	    private String InvStatus;
	    
	    @XmlElement
	    private String NetworkId;
	    
	    @XmlElement
	    private String SalesChannelId;
	    
	    @XmlElement
	    private String OperatorId;

	    @XmlEnum(String.class)
	    public enum Action {UPDATE,RESERVE,ASSIGN,RETAIN,RESET_PIN,REFRESH};	
        
        @XmlAttribute
        private String sequence;
        
        @XmlAttribute
        private String NetworkDeviceId; 
        
        public String getTelephoneNumber() {
			return TelephoneNumber;
		}

		public void setTelephoneNumber(String telephoneNumber) {
			TelephoneNumber = telephoneNumber;
		}

		public String getInvStatus() {
			return InvStatus;
		}

		public void setInvStatus(String invStatus) {
			InvStatus = invStatus;
		}

		public String getNetworkId() {
			return NetworkId;
		}

		public void setNetworkId(String networkId) {
			NetworkId = networkId;
		}

		public String getSalesChannelId() {
			return SalesChannelId;
		}

		public void setSalesChannelId(String salesChannelId) {
			SalesChannelId = salesChannelId;
		}

		public String getOperatorId() {
			return OperatorId;
		}

		public void setOperatorId(String operatorId) {
			OperatorId = operatorId;
		}



		public String getSequence() {
			return sequence;
		}

		public String getNetworkDeviceId() {
			return NetworkDeviceId;
		}

		public void setNetworkDeviceId(String networkDeviceId) {
			NetworkDeviceId = networkDeviceId;
		}

		public void setSequence(String sequence) {
			this.sequence = sequence;
		}

		
}
