package com.cycle30.fxweb.request.wrapper.inventory;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "<InventoryList>")
@XmlAccessorType(XmlAccessType.FIELD)
public class InventoryList {

	@XmlElement(name="Inventory")
    private List<Inventory> invntry;
	
	 @XmlAttribute
     private String Size;

	
	public String getSize() {
		return Size;
	}

	public void setSize(String size) {
		Size = size;
	}

	public List<Inventory> getInvntry() {
		return invntry;
	}

	public void setInvntry(List<Inventory> invntry) {
		this.invntry = invntry;
	}
}
