package com.cycle30.fxweb.request.wrapper.payment;



import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Payments")
@XmlAccessorType(XmlAccessType.FIELD)
public class Payments {

    @XmlElement(name="Payment")
    private List<Payment> Pmts;
    
    @XmlElement
    private String StoreId;
   
    @XmlElement
    private String AgentId;

    @XmlElement
    private String AmountDistribution;
    
    /**
     * @return the amountDistribution
     */
    public String getAmountDistribution() {
        return AmountDistribution;
    }

    /**
     * @param amountDistribution the amountDistribution to set
     */
    public void setAmountDistribution(String amountDistribution) {
        AmountDistribution = amountDistribution;
    }

    @XmlElement
    private String Note;

    
    /**
     * @return the payments
     */
    public List<Payment> getPayments() {
        return Pmts;
    }

    public void setPayments(List<Payment> payments) {
        this.Pmts = payments;
    } 
    
    /**
     * @return the storeId
     */
    public String getStoreId() {
        return StoreId;
    }

    /**
     * @param storeId the storeId to set
     */
    public void setStoreId(String storeId) {
        StoreId = storeId;
    }

    /**
     * @return the agentId
     */
    public String getAgentId() {
        return AgentId;
    }

    /**
     * @param agentId the agentId to set
     */
    public void setAgentId(String agentId) {
        AgentId = agentId;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return Note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        Note = note;
    }
    

    


    
}
