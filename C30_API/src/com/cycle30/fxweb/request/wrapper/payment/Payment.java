package com.cycle30.fxweb.request.wrapper.payment;


import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javax.xml.bind.annotation.*;


@XmlRootElement(name = "Payment")
@XmlAccessorType(XmlAccessType.FIELD)
public class Payment {

    @XmlElement
    private String AccountNumber;
    
    @XmlElement
    private String Amount;
    
    @XmlElement
    private String Currency;
    
    @XmlElement
    private String TenderType;
    
    @XmlElement
    private String CheckNumber;
    
    @XmlElement
    private String ExtTransactionId;
    
    @XmlElement
    private String ExtCorrelationId;

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return AccountNumber;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        AccountNumber = accountNo;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return Amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        Amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return Currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        Currency = currency;
    }

    /**
     * @return the checkNumber
     */
    public String getCheckNumber() {
        return CheckNumber;
    }

    /**
     * @param checkNumber the checkNumber to set
     */
    public void setCheckNumber(String checkNumber) {
        CheckNumber = checkNumber;
    }

    /**
     * @return the extTransactionId
     */
    public String getExtTransactionId() {
        return ExtTransactionId;
    }

    /**
     * @param extTransactionId the extTransactionId to set
     */
    public void setExtTransactionId(String extTransactionId) {
        ExtTransactionId = extTransactionId;
    }

    /**
     * @return the extCorrelationid
     */
    public String getExtCorrelationid() {
        return ExtCorrelationId;
    }

    /**
     * @param extCorrelationid the extCorrelationid to set
     */
    public void setExtCorrelationid(String extCorrelationid) {
        ExtCorrelationId = extCorrelationid;
    }

    public void setTenderType(String tenderType) {
        TenderType = tenderType;
    }

    public String getTenderType() {
        return TenderType;
    }
    
    

    
}
