package com.cycle30.fxweb.request;


/**
 * 
 *  Class representing a OCS account resource request
 *	
 *
 */
 
 
public class AccountPrepaidRequestImpl  implements ResourceRequestInf {

    
    // General request properties
    private RequestProperties requestProperties;
    
    
    // OCS Account-specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public RequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general and account-specific properties
     * 
     * @param requestProperties the requestProperties to set
     */
    public void setRequestProperties(RequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set account-specific properties...
    }


    
}
