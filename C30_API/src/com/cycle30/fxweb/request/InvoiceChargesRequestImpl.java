package com.cycle30.fxweb.request;


/**
 * 
 *  Class representing a Invoice Charges resource request
 *
 */
public class InvoiceChargesRequestImpl implements ResourceRequestInf {

    
    // General request properties
    private RequestProperties requestProperties;
     
    
    /**
     * @return the requestProperties
     */
    public RequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general properties
     * 
     * @param requestProperties the requestProperties to set
     */
    public void setRequestProperties(RequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
    }
    
}
