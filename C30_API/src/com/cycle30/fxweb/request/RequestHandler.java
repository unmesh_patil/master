package com.cycle30.fxweb.request;


import java.util.Random;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientAPIVersionCache;
import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.log.FxWebLogger;
import com.cycle30.fxweb.resource.ResourceFactory;
import com.cycle30.fxweb.resource.ResourceInf;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;


/** 
 * 
 * Handle HTTP request - this is a general-purpose class called by both
 * resource controller classes for Kenan v1 and v2.  It's responsible for 
 * the following basic functionality:
 * 
 * 1. Instantiate the correct resource request based on the resource type,
 *    e.g. "account", "service", then populate the request properties.
 * 
 * 2. Instantiate the correct core resource instance, again based on the
 *    resource type.
 *    
 * 3. Invoke the resource's handleRequest() method, which will perform the
 *    appropriate action for a GET, PUT, POST based on the HTTP method 
 *    stored as a header value ('http.method').
 *    
 * 4. Return the resulting ResourceResponse object.
 *
 */
public class RequestHandler {

   // private static final java.util.logging.Logger log = Logger.getLogger(RequestHandler.class);
    
    
    /**
     * Return a JAX-RS 'Response' result from the client request.
     * 
     * @param resourceType
     * @param headers
     * @param ui
     * @return response
     */
    public Response handleRequest (String resourceType, 
                                           HttpHeaders headers, 
                                           UriInfo ui,
                                           Document payload) throws FxWebException {
 
        Response response = null;
        
        // Get the request associated with the type of resource requested
        ResourceRequestInf request = ResourceRequestFactory.getInstance().getRequestImpl(resourceType);

        RequestProperties requestProperties = setProperties(headers, ui, payload);
        request.setRequestProperties(requestProperties);
 
        // Get the associated resource instance
        ResourceInf resource = ResourceFactory.getInstance().getResourceImpl(resourceType);
        
        // Check if reques is valid.  An exception will be thrown if not.
        new RequestValidator().validateRequest(request);

        // Generate request audit entry.
        FxWebLogger webLogger = new FxWebLogger();
        webLogger.auditRequest(request);

        boolean prepaidPmt = FxWebUtil.isprepaidPmt(request);
        if (!prepaidPmt) {
            // Transform payload (if required)
            RequestTransformer.transformXml(requestProperties);
        }        
        
        // Verify the request is ok, eg. the payload XML is valid
        ResourceResponseInf result = null;

        // Different/simplistic validation for AWN order status updates vs. orders, etc.   
        boolean awn = FxWebUtil.isAwnRequest(request);
        if (!awn) {
            String sdkAPIVersion = ClientAPIVersionCache.getAPIVersion(requestProperties.getClientId()); 
            if (sdkAPIVersion == null){
            //	log.debug("SDK API Version is null and clientId is" + requestProperties.getClientId());
                sdkAPIVersion = ClientCache.getVersionInfoByClientId(requestProperties.getClientId());
                //If APIVersion is still null, then read the Version based on AccountSegmentId 
                if (sdkAPIVersion == null)
                {
              //  	log.debug("SDK API Version is null and clientId is" + requestProperties.getClientId());

                	sdkAPIVersion = ClientCache.getVersionInfoByAcctSegmentId(ClientCache.getAccountSegmentByClientId(requestProperties.getClientId()));
                }
            }   
            
            //If Version is NULL then the client is not supposed to do any operations on that. So, we directly throw error.
            if (sdkAPIVersion==null) 
            {
            	throw new FxWebException ("No API Version found for the given Account SegmentId, ClientId. Not Authorized","AU-UNAUTH-NOVERSION"); 
            }
            requestProperties.setSdkApiVersion(sdkAPIVersion);
        } 
        
        // Handle the request
        result = resource.handleRequest(request);

        
        // Log the response
        webLogger.auditResponse(request, result);
        
        // Return the status and body from the result
        ResponseBuilder builder = Response.status(result.getResponseCode());
        builder.entity(result.getResponseBody());

        response = builder.build();
        
        return response;
        
    }
    
    
    /**
     * 
     * @param headers
     * @param ui
     * @param payload
     * @return
     */
    private RequestProperties setProperties(HttpHeaders headers, UriInfo ui, Document payload) throws FxWebException{
        
        RequestProperties props = new RequestProperties();
        
        // Set properties associated with the original HTTP request
        MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
        MultivaluedMap<String, String> pathParams  = ui.getPathParameters();
        
        props.setQueryParams(queryParams);
        props.setPathParams(pathParams);
        props.setHeaders(headers);
        props.setPayload(payload);
        
        String path = ui.getPath();
        props.setURI(path);
        
      
        
        
        // Get the acct external id from either the URI path param, query param or from the DOM, 
        // depending on the HTTP method used by the client.
        String externalId = FxWebUtil.getCustomerNumber(pathParams, queryParams, payload);
        props.setAccountNo(externalId);
        
        
        // Get/set client id (if exists). Normally required except for NSG 
        // order status updates.
        String orgId = FxWebUtil.getHeaderValue(headers, FxWebConstants.ORG_ID_HEADER_NAME);    
        if (FxWebUtil.isAwnRequest(props) ) {
           orgId = FxWebConstants.DEFAULT_AWN_CLIENT_ID; 
        }
        
        props.setClientId(orgId);

        // Get/set transaction id
        String transId = FxWebUtil.getHeaderValue(headers, FxWebConstants.TRANS_ID_HEADER_NAME);      

        props.setTransactionId(transId);
        
        return props;
        
    }



}
