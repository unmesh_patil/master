package com.cycle30.fxweb.request;


public interface ResourceRequestInf {

    public void setRequestProperties(RequestProperties props);
    public RequestProperties getRequestProperties();

}
