package com.cycle30.fxweb.request;




/**
 * 
 *  Class representing a Kenan service resource request
 *
 */
public class ServiceRequestImpl implements ResourceRequestInf {

    
    // General request properties
    private RequestProperties requestProperties;
    
    
    // Service-specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public RequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general and service-specific properties
     * 
     * @param requestProperties the requestProperties to set
     */
    public void setRequestProperties(RequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set service-specific properties...
    }


    
}
