package com.cycle30.fxweb.request;


/**
 * 
 *  Class representing a Prepaid payment resource request
 *
 */
public class PrepaidPaymentRequestImpl implements ResourceRequestInf {

    
    // General request properties
    private RequestProperties requestProperties;


    /**
     * @return the requestProperties
     */
    public RequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general properties
     * 
     * @param requestProperties the requestProperties to set
     */
    public void setRequestProperties(RequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;

    }


    
}
