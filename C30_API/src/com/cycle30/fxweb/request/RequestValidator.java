package com.cycle30.fxweb.request;


import java.util.ArrayList;
import java.util.Iterator;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;


import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.ClientCache;
import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.response.InvalidPayloadResponseImpl;
import com.cycle30.fxweb.response.ResourceResponseInf;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;



public class RequestValidator {
    
    private static final Logger log = Logger.getLogger(RequestValidator.class);
    
    
    
    /**
     * 
     * @param request
     */
    public void validateRequest(ResourceRequestInf request) throws FxWebException {
        
        
        HttpHeaders headers = request.getRequestProperties().getHeaders();
        
        // Get system environment variable values
        String digestEnvVar = System.getenv(FxWebConstants.DIGEST_VALIDATION_ENV_VAR);
        String envTransId   = System.getenv(FxWebConstants.TRANS_ID_ENV_VAR);
        
        // Validate the client ID header matches the current environment.
        String envClientId = System.getenv(FxWebConstants.CLIENT_ID_ENV_VAR);
        if (envClientId == null) {
            throw new FxWebException(new Exception(FxWebConstants.CLIENT_ID_ENV_VAR + " environment variable not found!"), Response.Status.INTERNAL_SERVER_ERROR, "Request Validation");
        }
  

        // Different/simplistic validation for AWN order status updates vs. orders, etc.   
        boolean awn = FxWebUtil.isAwnRequest(request);
        if (awn) {
            validateAwnHeaders(request);
        } 
        else {
            String missingHeaders = checkHeaders(headers);
            if (missingHeaders != null) {
               throw new FxWebException("Missing required HTTP headers: " + missingHeaders, "Request Validation");
            }
        }     
        
        // Validate environment.
        String envId = FxWebUtil.getHeaderValue(headers, "X-Env-Id");
        if (envId == null || envId.equals(envClientId)==false) {
            throw new FxWebException("X-Env-Id HTTP header value '" + envId + "' doesn't match expected value.",  "Request Validation");
        }
        
        // Done with validation for AWN request.
        if (awn) {
            return;
        }
        
        // Validate client/org id
        String clientId = FxWebUtil.getHeaderValue(headers, "X-Org-Id");
        if (ClientCache.clientExists(clientId) == false) {
            throw new FxWebException("Unrecognized X-Org-Id HTTP header value: " + clientId, "Request Validation"); 
        }
        
        
        // See if X-Trans-Id header is required, and if so if it's supplied.
        if (envTransId == null) {
            throw new FxWebException(new Exception(FxWebConstants.TRANS_ID_ENV_VAR + " environment variable not found!"), Response.Status.INTERNAL_SERVER_ERROR, "Request Validation");
        }
        if (envTransId.equalsIgnoreCase("yes"))  {
            String transId = FxWebUtil.getHeaderValue(headers, "X-Trans-Id");
            if (transId == null || transId.length()==0) {
                throw new FxWebException("Required 'X-Trans-Id' HTTP header not supplied in the request.","Request Validation");
            }
        }
        
        // See if the digest validation is required.
        if (digestEnvVar == null) {
            throw new FxWebException(new Exception(FxWebConstants.DIGEST_VALIDATION_ENV_VAR + " environment variable not found!"), Response.Status.INTERNAL_SERVER_ERROR, "Request Validation");
        }
        if (digestEnvVar.equalsIgnoreCase("yes"))  {
            String digestValue = FxWebUtil.getHeaderValue(headers, "X-Digest");
            if (digestValue == null || digestValue.length()==0) {
                throw new FxWebException("Required 'X-Digest' HTTP header not supplied in the request.","Request Validation");
            }
            if (FxWebUtil.validateDigest(headers, digestValue) == false) {

                throw new FxWebException("Invalid 'X-Digest' HTTP header value.", "Request Validation");
            }
        }
        
          
        // Check for unknown URI parameters
        String invalidParams = checkUriParams(request);
        if (invalidParams != null) {
            throw new FxWebException("Unknown URI params supplied in request: " + invalidParams, "Validation");
        }
        
        // Get the type of request.
        String httpMethod = FxWebUtil.getRequestMethod(headers);
        
        // Only care about validating the payload for a PUT or POST method.
        if (httpMethod == null) {
            throw new FxWebException("Missing HTTP method (GET, PUT, POST, DELETE)", "Validation");
        }
        
        // Check for 'key' values on a health check.
        if (request instanceof ServiceRequestImpl) {
            
            String uri = request.getRequestProperties().getURI();
            if (uri != null && uri.indexOf("healthcheck")!=0 ) {
                
                boolean keyFound = false;
                ArrayList<String> keys = FxWebUtil.getServiceKeyParams(request.getRequestProperties().getQueryParams());
                if (keys.size() == 0) {
                    throw new FxWebException ("Health check requires at least on 'key' parameter", ""); 
                }
                for (String key : keys) {
                    if (key.equalsIgnoreCase("BillingStatus") || key.equalsIgnoreCase("NetworkStatus")) {
                        keyFound = true;
                    }
                }
                if (keyFound == false) {
                    throw new FxWebException ("Health check requires a BillingStatus and/or NetworkStatus 'key' parameter", ""); 
                }
            }
        }
        
        if (httpMethod.equalsIgnoreCase("GET") || httpMethod.equalsIgnoreCase("DELETE")) {
            return;
        }
        
        // convert the DOM payload to an XML string for validation
        String xmlString = null;
        Document dom = request.getRequestProperties().getPayload();
        
        try {
            xmlString = DomUtils.domToXtring(dom);
        } catch (Exception e) {
            throw new FxWebException(e, "XML payload conversion");
        }
        
        // error if no payload found.
        if (dom == null || xmlString == null || xmlString.length()==0) {
            throw new FxWebException ("No XML payload found for HTTP " + httpMethod + " request.", "Request Validation");
        }
        
        // Check for valid deposit values
        if (request instanceof DepositRequestImpl) {
            String depositErrors = checkDepositValues(request);
            if (depositErrors != null && depositErrors.length()>0) {
                throw new FxWebException ("Invalid deposit values: " + depositErrors, ""); 
            }
        }
        

 
    }
    
    
    
    /**
     * Build a response when the request is determined to be invalid, for
     * example the PUT/POST XML payload doesn't match the related schema. 
     * 
     * @param request
     * @return invalid response instance
     */
    public ResourceResponseInf generateInvalidRequestResult(ResourceRequestInf request) {
        
        ResourceResponseInf response = new InvalidPayloadResponseImpl();
        
        return response;
    }
    
    
    
    /**
     * 
     * @param headers
     * @return Missing headers
     */
    private String checkHeaders( HttpHeaders headers ) {
        
        StringBuffer missing = new StringBuffer();

        if (FxWebUtil.getHeaderValue(headers, "X-Env-Id") == null) {
            missing.append("X-Env-Id "); 
        }
        
        if (FxWebUtil.getHeaderValue(headers, "X-Org-Id") == null) {
            missing.append("X-Org-Id "); 
        }
        
        if (FxWebUtil.getHeaderValue(headers, "http.request") == null) {
            missing.append("http.request ");
        }
        
        if (FxWebUtil.getHeaderValue(headers, "Date") == null) {
            missing.append("Date ");
        }
        
        if (FxWebUtil.getHeaderValue(headers, "X-Digest") == null) {
            missing.append("X-Digest ");
        }
        
        if (missing.length()==0) {
            return null;  
        }
        
        return missing.toString();
        
    }
    
    
    /**
     * 
     * @param request
     * @return
     */
    private String checkUriParams(ResourceRequestInf request) {
        
        StringBuffer invalidParams = new StringBuffer();  
        
        RequestProperties props = request.getRequestProperties();
        MultivaluedMap<String, String> queryParams = props.getQueryParams();
        Iterator<String> i = queryParams.keySet().iterator();
       
        String key = "";
        

        while (i.hasNext()) {
           
           key = (String)i.next();                
           
           boolean match=false;
           for (String s : FxWebConstants.VALID_PARAMS) {
               if (s.equalsIgnoreCase(key)) {
                   match=true;
               }               
           }
           if (match==false) {
               invalidParams.append(key + " ");
           }
        }
        
        if (invalidParams.length()==0) {
            return null;  
        }
        
        return invalidParams.toString();
    }
    
    
    
    
    /**
     * 
     * @param request
     * @return string of errors, if any
     */
    private String checkDepositValues (ResourceRequestInf request) {
        
        String errors = "";
        
        RequestProperties props = request.getRequestProperties();
        Document dom = props.getPayload();
        
        String depositType = DomUtils.getElementValue(dom, "DepositType");
        String despositCode   = FxWebConstants.VALID_DEPOSIT_TYPES.get(depositType);
        if (despositCode == null) {
            errors += "Unknown deposit type: " + depositType + ". ";
        }
        
        String paymentMethod = DomUtils.getElementValue(dom, "PaymentMethod");
        String paymentCode   = FxWebConstants.VALID_DEPOSIT_PAYMENT_TYPES.get(paymentMethod);
        if (paymentCode == null) {
            errors += "Unknown deposit payment method: " + paymentMethod + ". ";
        }
        
        
        // Credit card type must have authcode and authdate
        String ccAuthCode = DomUtils.getElementValue(dom, "CreditCardAuthNo");
        String ccAuthDate = DomUtils.getElementValue(dom, "CreditCardAuthDate");
        if (paymentMethod.equals("CC")) {
            
           if (ccAuthCode==null || ccAuthCode.length()==0) {
               errors += "Credit card auth code required. ";
           }
           if (ccAuthDate==null || ccAuthDate.length()==0) {
               errors += "Credit card auth date required. ";
           }
           // Date needs to be in simple MM-DD format
           if (ccAuthDate!=null  && ccAuthDate.length()>0 && ccAuthDate.length()!= 5) {
              errors += "CC auth date format is invalid (needs to be MM-DD).";
           }
           
        }

        return errors;
        
    }
    
    
    
    
    /**
     * Special/limited HTTP header validation for AWN orders.
     * 
     * @param request
     */
    private void validateAwnHeaders(ResourceRequestInf request) throws FxWebException {
        
        StringBuffer missing = new StringBuffer();

        HttpHeaders headers = request.getRequestProperties().getHeaders();
        if (FxWebUtil.getHeaderValue(headers, "X-Env-Id") == null) {
            missing.append("X-Env-Id "); 
        }

        String missingStr = missing.toString();
        if (missingStr.length()!=0) {
            throw new FxWebException("Missing required HTTP headers: " + missingStr, "Request Validation"); 
        }
    }

}
