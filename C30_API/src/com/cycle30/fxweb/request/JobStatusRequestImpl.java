package com.cycle30.fxweb.request;




/**
 * 
 *  Class representing a Workpoint job status resource request
 *
 */
public class JobStatusRequestImpl implements ResourceRequestInf {

    
    // General request properties
    private RequestProperties requestProperties;
    
    
    // Job status-specific properties
    String jobId;

    /**
     * @return the jobId
     */
    public String getJobId() {
        return jobId;
    }


    /**
     * @param jobId the jobId to set
     */
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }


    /**
     * @return the requestProperties
     */
    public RequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general properties
     * 
     * @param requestProperties the requestProperties to set
     */
    public void setRequestProperties(RequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;

    }


    
}
