package com.cycle30.fxweb.request;

import org.apache.log4j.Logger;

import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.resource.PhoneInventorySearchResourceImpl;
import com.cycle30.fxweb.resource.PrepaidPaymentResourceImpl;


/**
 *
 * Resource request factory.
 *
 */
public class ResourceRequestFactory {

    // Instantiate singleton
    private static final ResourceRequestFactory self = new ResourceRequestFactory();

    private static final Logger log = Logger.getLogger(ResourceRequestFactory.class);

    private static final String EXCEPTION_SOURCE = "REQUEST-FACTORY";


    //-------------------------------------------------------------------------
    /** Private constructor. */
    private ResourceRequestFactory() {
        super();
    }


    //-------------------------------------------------------------------------
    /**
     * Return instance of singleton class
     *
     * @return ResourceRequestFactory instance.
     */
    public static ResourceRequestFactory getInstance() {
        return self;
    }


    //-------------------------------------------------------------------------
    // Instantiate correct Fx Web request interface based on the resource name
    /**
     * @param resourceName
     * @return Resource request
     */
    public ResourceRequestInf getRequestImpl(String resourceName) throws FxWebException  {

        if (resourceName.startsWith("account")) {
            return new AccountRequestImpl();
        }
        if (resourceName.startsWith("jobstatus")) {
            return new JobStatusRequestImpl();
        }
        if (resourceName.startsWith("order")) {
            return new OrderRequestImpl();
        }
        if (resourceName.startsWith("geocode")) {
            return new GeoCodeRequestImpl();
        }
        if (resourceName.startsWith("invoiceCharges")) {
		    return new InvoiceChargesRequestImpl();
        }
        if (resourceName.startsWith("invoice")) {
            return new InvoiceRequestImpl();
        }
        if (resourceName.startsWith("payment")) {
            return new PaymentRequestImpl();
        } 
        if (resourceName.startsWith("prepaidpayment")) {
            return new PrepaidPaymentRequestImpl();
        }
        if (resourceName.startsWith("clientcallback")) {
            return new ClientCallbackRequestImpl();
        }
        if (resourceName.startsWith("deposit")) {
            return new DepositRequestImpl();
        }
        if (resourceName.startsWith("service")) {
            return new ServiceRequestImpl();
        }
        if (resourceName.startsWith("healthcheck")) {
		    return new HealthCheckRequestImpl();
        }
        if (resourceName.startsWith("inventory")) {
            return new InventoryRequestImpl();
        }
        if (resourceName.startsWith("simInventory")) {
            return new SIMInventoryRequestImpl();
        }
        if (resourceName.startsWith("phoneSearchInventory")) {
            return new PhoneInventorySearchRequestImpl();
        }
        if (resourceName.startsWith("prepaidAccountBal")) {
             return new PrepaidBalanceRequestImpl();
        }
        if (resourceName.startsWith("prepaidAccount")) {
            return new AccountPrepaidRequestImpl();
        }
        if (resourceName.startsWith("voucher")) {
            return new VoucherRequestImpl();
        }

        // If not found
        String error = "Unable to instantiate correct request type (account, order, etc) based on resource name " + resourceName;
        log.error(error);

        throw new FxWebException (error, EXCEPTION_SOURCE);
    }
}
