package com.cycle30.fxweb.request;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;


import com.cycle30.fxweb.exception.FxWebException;
import com.cycle30.fxweb.util.DomUtils;




/** Class used for transforming XML to SDK format */
public class RequestTransformer {

    
    private static final Logger log = Logger.getLogger(RequestTransformer.class);

    
    
    /**
     * 
     * @param props
     * @throws FxWebException
     */
    public static synchronized void transformXml(RequestProperties props) throws FxWebException {
        
        try {  
            
            // Get payload (POST/PUT methods)
            Document doc = props.getPayload();
            if (doc == null) {
                return;
            }
            
            String originalXml = DomUtils.domToXtring(doc);

            
            // Need to add xsi:nil="true" to empty XML elements.  This is because SDK
            // use of JAXB inserts values if attribute not there, and causes problems
            String xml = addNilAttribute(originalXml);
  
            log.info("Transformed XML: " + xml);

            // Convert back to DOM document, and set as the payload.
            Document newDoc = DomUtils.stringToDom(xml);
            props.setPayload(newDoc);
             

        } catch (Exception e) {
            log.error(e);
            throw new FxWebException(e, "" );
        } 

    
    }
    
    
    
    /**
     * 
     * @param xml
     * @return
     */
    private static synchronized String addNilAttribute(String xml) {
        
        // Sanity/future-proof check. If original xml already added nil,
        // don't do again. 
        if (xml.indexOf("xsi:nil=\"true\"") > 0) {
            return xml;
        }
        
        // Need to inject namespace attribute for SDK validation.
        //String temp = xml.replaceAll("<OrderRequest", "<OrderRequest xmlns=\"www.cycle30.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
        String temp = xml.replaceAll("<OrderRequest", "<OrderRequest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
        
        log.info("Adding nil=true attribute to all empty XML elements...");
        String newXml = temp.replaceAll("/>", " xsi:nil=\"true\"/>");
        
        // 09/16/12 temp/quick fix for AWN defect 104.  Rip out xsi:nil="true" when incorrectly inserted
        // at end of attributes list for element that only has attributes (and no associated data). 
        // For example:
        //    <ServiceObject ClientActionType="SUSPEND" ClientActionTypeReason="" ClientId="9073857829" xsi:nil="true"/>
        // causes parsing/schema validation error
        String finalXml = newXml.replaceAll("\" xsi:nil=\"true\"/>", "\"/>");
        
        return finalXml;
        
    }

            
}
