package com.cycle30.fxweb.request;




/**
 * 
 *  Class representing a Kenan account resource request
 *
 */
public class InvoiceRequestImpl implements ResourceRequestInf {

    
    // General request properties
    private RequestProperties requestProperties;
    
    
    // Account-specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public RequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general and account-specific properties
     * 
     * @param requestProperties the requestProperties to set
     */
    public void setRequestProperties(RequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set account-specific properties...
    }


    
}
