package com.cycle30.fxweb.provision.util;


public class ProvisioningUtil {



    private static String truststoreLocation;
    
    public static String getTruststoreLocation() {
        return truststoreLocation;
    }
    
    //-----------------------------------------------------------
    // Note:  *** Spring injected via XML in ssl-config.xml ***
    //-----------------------------------------------------------
    public void setTruststoreLocation(String tsLocation) {
        truststoreLocation = tsLocation;
    }

    
    
}
