package com.cycle30.fxweb.provision.nsg;


import java.io.IOException;
import java.net.UnknownHostException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.fxweb.cache.HealthCheckCache;
import com.cycle30.fxweb.exception.FxWebException;

import com.cycle30.fxweb.provision.util.ProvisioningUtil;
import com.cycle30.fxweb.util.DomUtils;
import com.cycle30.fxweb.util.FxWebConstants;
import com.cycle30.fxweb.util.FxWebUtil;



/** 
 * General purpose class for implementing provisiong GCI NSG-specific 
 *  utilities.
 *
 */
public class NsgProvisioningUtils {

    
    // List of clients that use the NSG provisioning system
    public static final String[] NSG_CLIENT_PREFIXES = {"GCI", "ACS"};
     
    public static Logger log = Logger.getLogger(NsgProvisioningUtils.class);   
    


    
    //-------------------------------------------------------------------------
    /** 
     * Build the appropriate NSG health check XML payload 
     * 
     */
    public static String buildHealthCheckRequest(String clientId, String serviceId) {

        
        // Get carrier id (eg.'ACS', 'GCI', etc). This assumes 3-char carrier id!
        String carrierId = clientId.substring(0, 3);
        
        // See if this is a telephone # or ICCD request based on # of
        // chars in service id.  20 digits==ICCID
        String serviceElementXml = "";
        if (serviceId.length() != 20) {
            serviceElementXml = "<telephoneNumber>" + serviceId + "</telephoneNumber>";
        }
        else {
            serviceElementXml = "<iccid>" + serviceId + "</iccid>";
        }
        
        String xml = FxWebConstants.XML_HEADER +
                "<wireless>" +  
                " <inquiry>" + 
                "  <idCarrier>" + carrierId + "</idCarrier>" +
                "   <method>getSubscriberInformation</method>" +
                    serviceElementXml +
                " </inquiry>" +   
               "</wireless>";
        
        
        return xml;
        
        
    }
    
    
    
    
    //-------------------------------------------------------------------------
    /**
     * @param clientId
     * @param healthCheckPayload
     * @return  inquiry results
     */
    public static String invokeNsgServiceInquiry(String clientId, String healthCheckPayload ) 
                                     throws FxWebException {

        
        // Get the NSG health check URL from the cache (populated at start up).
        String healthCheckUrl = HealthCheckCache.getGsmUrl(clientId);
        if (healthCheckUrl == null) {
            throw new FxWebException ("NSG health check URL not found for client id " + clientId, "");
        }
        
        // Convert XML string to Document
        Document doc = DomUtils.stringToDom(healthCheckPayload);
        

        // Invoke NSG web service using POST (even though retrieving data). 
        String responseData = "";
        String nsgResponse  = "";                 
                
        try {
            
            // Make sure system trust store location has been set so
            // can successfully do an SSL connection (https).
            String truststoreLoc = System.getProperty("javax.net.ssl.trustStore");
            if (truststoreLoc == null) {
                truststoreLoc = ProvisioningUtil.getTruststoreLocation();
                System.setProperty("javax.net.ssl.trustStore", truststoreLoc);
                log.info("JVM property 'javax.net.ssl.trustStore' set to " + truststoreLoc);
            }
                    
            HttpClient client = new HttpClient();
    
            PostMethod method = new PostMethod(healthCheckUrl);
            method.setRequestBody(healthCheckPayload);
            method.setRequestHeader("Content-type", "application/xml");
            
            log.info("Sending POST request to NSG URL: " + healthCheckUrl + " using payload: " + healthCheckPayload);
            int statusCode = client.executeMethod( method );
            log.info("HTTP request return status code: " + statusCode);
            
            responseData = method.getResponseBodyAsString();
            
            nsgResponse = FxWebUtil.removeXmlHeader(responseData);
            
            
        } catch (HttpException he) {
            log.error(he);
            return "<error>" + he.getMessage() + "</error>";
           
        } catch (IOException ie) {
            log.error(ie);
            String err = ie.getMessage();
            if (ie instanceof UnknownHostException) {
                err = "Unknown host " + healthCheckUrl;
            }
            return "<error>" + err + "</error>";
         }
        
        
        
        log.info("NSG health check response: " + responseData);
        
        return nsgResponse;
    }
    
    
    
    //-------------------------------------------------------------------------
    /**
     * @param nsgResult
     * @return formatted health check
     */
    public static String formatNsgHealthCheck(String nsgResult) {
        
        StringBuffer result = new StringBuffer();
        
        result.append("<NetworkProvisionDetails>");
           result.append(nsgResult); 
        result.append("</NetworkProvisionDetails>");
        
        String xml = result.toString();
        
        return xml;
    }
    
    
    
    //-------------------------------------------------------------------------
    /**
     * 
     * @param clientId
     * @return true if is an NSG client
     */
    public static boolean isNsgClient(String clientId) {
        
        String clientPrefix = clientId.substring(0, 3);
        
        
        for (String s : NSG_CLIENT_PREFIXES) {
            if (clientPrefix.equalsIgnoreCase(s) ) {
                return true;
            }
        }
        
        return false;
    }
    
    

    
}
