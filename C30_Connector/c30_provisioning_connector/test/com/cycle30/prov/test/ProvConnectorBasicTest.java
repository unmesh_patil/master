package com.cycle30.prov.test;

import static org.junit.Assert.fail;

import java.util.Date;
import java.util.HashMap;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.prov.auth.ProvAuthClientDataSource;
import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30DOMUtils;

public class ProvConnectorBasicTest {

	@Test
	public void testC30WPAddAccountApi() {

		try {

			ProvAuthClientDataSource p = ProvAuthClientDataSource.getInstance();
			ProvAuthClientObject obj = p.getInstance().getAuthObject(1001, Constants.PROV_INQUIRY);
			System.out.println(obj.getWeburl());

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}



	@Test
	public void imsiVerficationTest() {

		try {

			HashMap input = new HashMap();
			//Error
			// ICCID In use 
			//input.put("ICCID", "89013113700709000000");
			input.put("AcctSegId", "3");
			// Success
			input.put("ICCID", "89013113708000000355");
			//input.put("ICCID", "89013111234709000042");
			input.put("RequestName", "IMSIVerifcation");

			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap response = rq.awnInquiryRequest(input) ;
			System.out.println(response.get("IMSI"));
			System.out.println(response.get("ResponseXML"));
		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void validateSIMTest() {

		try {

			HashMap input = new HashMap();
			//Error
			// ICCID In use 
			//input.put("ICCID", "89013113700709000000");
			input.put("AcctSegId", "3");
			// Success
			input.put("ICCID", "89013113708000000355");
			//input.put("ICCID", "89013113700709000042");
			input.put("RequestName", "ValidateSIM");

			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap response = rq.awnInquiryRequest(input) ;
			System.out.println(response.get("IMSI"));
			System.out.println(response.get("ResponseXML"));
		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void getRequestStatus() {

		try {

			HashMap input = new HashMap();
			input.put("RequestId", "113");
			input.put("RequestName", "SPSRequestStatusByRequestId");

			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap response = rq.awnInquiryRequest(input) ;
			System.out.println(response.get("Status"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void validateTelephoneInquiry() {

		try {

			HashMap input = new HashMap();

			input.put("TelephoneNumber", "9073957591");
			input.put("AcctSegId", "1");
			input.put("RequestName", "TelephoneNumberInquiry");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap response = rq.awnInquiryRequest(input) ;
			System.out.println(response.get("TelephoneNumber"));
			System.out.println(response.get("ResponseXML"));
		} catch (C30ProvConnectorException e) {
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void getSubscriberInformationByTelephoneNUmber() throws XPathExpressionException, C30SDKToolkitException {

		try {

			HashMap input = new HashMap();
			//Not working
			//input.put("TelephoneNumber", "9077391661");
			//Working
			input.put("TelephoneNumber", "9073857833");
			input.put("AcctSegId", "3");
			input.put("RequestName", "SubscriberInquiry");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap response = rq.awnInquiryRequest(input) ;

			//System.out.println(response.get("TelephoneNumber"));
			//System.out.println(response.get("ResponseXML"));

			String respXml = (String) response.get("ResponseXML");
			Document respXmlDOM = C30DOMUtils.stringToDom(respXml);

			XPathFactory xpf = XPathFactory.newInstance();
			XPath xp = xpf.newXPath();
			String text = xp.evaluate("//wireless/inquiry/method/services/data/service/data",
					respXmlDOM.getDocumentElement());
			System.out.println(text);

			/*String sersXml = C30ProvConnectorUtils.getXmlStringElementValue(respXml, "services");
			System.out.println(sersXml);
			sersXml =  C30ProvConnectorUtils.getXmlStringElementValue(sersXml, "data");
			System.out.println(sersXml);
			sersXml =  C30ProvConnectorUtils.getXmlStringElementValue(sersXml, "data");
			System.out.println(sersXml);
			 */
		} catch (C30ProvConnectorException e) {
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void getSubscriberInformationByICCIDIccId() {

		try {

			HashMap input = new HashMap();
			//Not working
			//input.put("ICCID", "89013113700806219091");
			//Working
			//input.put("ICCID", "89013113700806219099");
			input.put("ICCID", "89013113708000000314");
			input.put("AcctSegId", "1");
			input.put("RequestName", "SubscriberInquiry");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap response = rq.awnInquiryRequest(input) ;

			System.out.println(response.get("TelephoneNumber"));
			System.out.println(response.get("ResponseXML"));
		} catch (C30ProvConnectorException e) {
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}


	@Test
	public void testSqlDate() {

		Date sysdate = new Date();
		System.out.println(sysdate);
		//new java.sql.Date(((Date)paramValue).getTime())
		System.out.println(sysdate.getTime());
		System.out.println(new java.sql.Timestamp(((Date)sysdate).getTime()));
		fail("Not yet implemented");
	}


	@Test
	public void getSPSRequestHistoryByTelephoneNUmber() {

		try {

			HashMap input = new HashMap();
			//Not working
			input.put("TelephoneNumber", "9073857835");
			// Not Working
			//input.put("TelephoneNumber", "9078300629");
			input.put("AcctSegId", "3");
			input.put("RequestName", "SPSRequestHistoryInquiry");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap response = rq.awnInquiryRequest(input) ;

			System.out.println(response.get("TelephoneNumber"));
			System.out.println(response.get("ResponseXML"));
		} catch (C30ProvConnectorException e) {
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void createVoiceService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9077391661");
			input.put("ICCID", "99913113709999000389");
			input.put("CatalogId", "VOICE");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getExceptionCode());
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void suspendService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			//input.put("ICCID", "89013113708000000314");
			input.put("CatalogId", "SERVICE");
			input.put("CatalogActionId", "SUSPEND");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	@Test
	public void resumeService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			//input.put("ICCID", "89013113708000000314");
			input.put("CatalogId", "SERVICE");
			input.put("CatalogActionId", "RESUME");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void swapTelephoneNumber() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9078107196");
			input.put("NewTelephoneNumber", "9078107196");
			input.put("ICCID", "89013113708000000314");
			input.put("CatalogId", "TELEPHONENUMBER");
			input.put("CatalogActionId", "SWAP");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void discService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9077391661");
			//input.put("ICCID", "89013113708000000314");
			input.put("CatalogId", "SERVICE");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void discServiceList() {


		String[] telephoneList = new String[]{"9074142034",
"9074142035",
"9074142036",
"9074142037",
"9074142038",
"9074142039",
"9073857829",
"9073857845",
"9073857846",
"9073857847",
"9073857851",
"9073857852",
"9073857853",
"9073857854",
"9073857856",
"9073857857",
"9074207663"};

		String output = "";
		java.lang.System.setProperty("C30APPENCKEY", "c3N3wpwd");
		System.out.println(System.getenv("C30APPENCKEY"));
		
		for (String tpNo : telephoneList){
			try {
				HashMap input = new HashMap();
				input.put("ExternalTransactionId", "ACS0001");
				input.put("C30TransactionId","123");
				input.put("AcctSegId", "3");
				input.put("TelephoneNumber", tpNo);
				//input.put("ICCID", "89013113708000000314");
				input.put("CatalogId", "SERVICE");
				input.put("CatalogActionId", "Disconnect");
				C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
				HashMap resp = rq.awnProvisioningRequest(input) ;
				String requestId = ((Integer) resp.get("RequestId")).toString();
				String respCode = (String) resp.get("ResponseCode");
				System.out.println(resp.get("RequestId"));
				System.out.println(resp.get("ResponseCode"));
				output = output + tpNo+ " : " + respCode +"\n";

			} catch (C30ProvConnectorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				output = output + tpNo+ " : ERROR : " + e.getMessage() +"\n";
			}
			System.out.println(output);
		}
		fail("Not yet implemented");
	}

	@Test
	public void discVoiceService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			//input.put("ICCID", "89013113708000000314");
			input.put("CatalogId", "VOICE");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	@Test
	public void removeRoamingAdd() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9077391661");
			input.put("ICCID", "99913113709999000389");
			input.put("CatalogId", "REMROAM");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void removeRoamingDisconnect() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "REMROAM");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void internationRoamingDisconnect() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "INTLROAM");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	@Test
	public void internationRoamingAdd() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9077391661");
			input.put("ICCID", "99913113709999000389");
			input.put("CatalogId", "INTLROAM");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	
	@Test
	public void internationCallingAdd() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9077391661");
			input.put("ICCID", "99913113709999000389");
			input.put("CatalogId", "INTLCALL");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	@Test
	public void internationCallingDisconnect() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "INTLCALL");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	
	@Test
	public void tollResAdd() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "REMTOLL");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	@Test
	public void tollResDisc() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "REMTOLL");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	
	@Test
	public void dataOnlyAdd() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9077391633");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "DATAONLY");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
	
	@Test
	public void createTextService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "MSG");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void disconnectTextService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9078107196");
			//input.put("ICCID", "89013113708000000330");
			input.put("CatalogId", "MSG");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void suspendTextService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "MSG");
			input.put("CatalogActionId", "Suspend");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void createDataService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857833");
			input.put("ICCID", "89013113708000000314");
			input.put("CatalogId", "DATA");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void discDataService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			//input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "DATA");
			input.put("CatalogActionId", "Disconnect");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void createDataTethService() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "3");
			input.put("TelephoneNumber", "9073857835");
			input.put("ICCID", "89013113708000000488");
			input.put("CatalogId", "DATA+TETH");
			input.put("CatalogActionId", "Add");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

	@Test
	public void serviceDisconnect1() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "1");
			input.put("TelephoneNumber", "9073957591");
			//input.put("ICCID", "89013113708000000306");
			input.put("CatalogId", "SERVICE");
			input.put("CatalogActionId", "DISCONNECT");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} 
		catch (C30ProvConnectorException e) {
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
}
