package com.cycle30.prov.test;

import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Test;

import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;

public class GlobecommProvTest {
	@Test
	public void addGlobecommSubscriber() {

		try {

			HashMap input = new HashMap();
			input.put("ExternalTransactionId", "ACS0001");
			input.put("C30TransactionId","123");
			input.put("AcctSegId", "10");
			input.put("AccountNumber", "10");

			input.put("MSISDN", "9073857835");
			input.put("IMSI", "89013113708000000314");
			input.put("CatalogId", "BASIC_SUBSCRIBER_PLAN");
			input.put("CatalogActionId", "ADD");
			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.globeCommProvisioningRequest(input) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}
}
