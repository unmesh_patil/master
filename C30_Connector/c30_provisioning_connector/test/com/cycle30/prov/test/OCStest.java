package com.cycle30.prov.test;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.utils.C30ProvConnectorUtils;

public class OCStest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String requestPayload = "";
		String requestPayloadBody = "";
		String name = null;
		String value = null;
		String dataType = Constants.OCS_XML_TAG_STRING;		

		try {

			HashMap requestHashMap = new HashMap();
			requestHashMap.put("ExternalTransactionId", "GCI0001");
			requestHashMap.put("C30TransactionId","11111");
			requestHashMap.put("AcctSegId", "4");
			requestHashMap.put("RequestName", "PrepaidAccountSubscriber");
			requestHashMap.put("MethodName", "Subscriber.create");
			requestHashMap.put("SubscriberProfileLabel", "PLAN_NOT_SELECTED");
			requestHashMap.put("Pincode", "34567");
			requestHashMap.put("FirstName", "0x00000AdcdAAA");
			requestHashMap.put("LastName", "AUTOPAY");
			
			if (requestHashMap.containsKey("RequestName")) {
				System.out.println(requestHashMap.get("RequestName"));
			}
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL)) {
				System.out.println(requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString());
			}
			

			C30ProvisionRequest rq = C30ProvisionRequest.getInstance();
			HashMap resp = rq.awnprepaidOrderRequest(requestHashMap) ;
			System.out.println(resp.get("RequestId"));
			System.out.println(resp.get("ResponseCode"));

			System.out.println("Feature RequestId : "+resp.get("RequestId") +" Feature ResponseCode: "+ resp.get("ResponseCode"));
			System.out.println(resp.get("ResponseXML"));

		} catch (C30ProvConnectorException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getExceptionCode());
			e.printStackTrace();
		}



		
		
	}


}
