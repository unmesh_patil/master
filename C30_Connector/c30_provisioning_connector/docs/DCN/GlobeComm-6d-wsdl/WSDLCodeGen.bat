rem wsimport.exe -d  ./src/ -keep -verbose -Xnocompile -p com.cycle30.prov.globecomm.webservice.subscriber AddSubscriber.wsdl
rem Subscriber
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile AddSubscriber.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile UpdateSubscriber.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile DeleteSubscriber.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile FetchSubscriber.wsl
rem APN
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile AddAPN.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile DeleteAPN.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile FetchAPN.wsdl
rem Bulk
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile BulkAddSubscriber.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile BulkDeleteSubscriber.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile BulkUpdateSubscriber.wsdl
rem others
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile ManageServiceProfile.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile ManageSubscriberService.wsdl
wsimport.exe -d  ./src/ -keep -verbose   -Xnocompile TransactionHistory.wsdl