package com.cycle30.prov.response;

import java.util.HashMap;

public interface IProvisioningResponse {

	 public HashMap getResponse();
	 public void setResponseMap(HashMap responseMap);
}
