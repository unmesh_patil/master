package com.cycle30.prov.log;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import java.sql.SQLException;

/** Request and response logging
 * 
 * @author rnelluri
 *
 */
public class ProvRequestLogger {

	private static Logger log = Logger.getLogger(ProvRequestLogger.class);  

	public void auditCatalogRequest(IProvisioningRequest request, C30JDBCDataSource datasource) throws C30ProvConnectorException {

		C30ProvCatalogTransaction catalogTransaction = new C30ProvCatalogTransaction();
		ProvisioningRequestProperties provRequestProp = request.getRequestProperties();

		catalogTransaction.setC30TransactionId(provRequestProp.getC30TransactionId());
		catalogTransaction.setExternalTransactionId(provRequestProp.getExternalTransactionId());
		if (provRequestProp.getRequestMap().containsKey(Constants.ICCID)) {
			catalogTransaction.setExternalId((String) provRequestProp.getRequestMap().get(Constants.ICCID));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.TELEPHONE_NUMBER)) {
			catalogTransaction.setTelephonNumber((String) provRequestProp.getRequestMap().get(Constants.TELEPHONE_NUMBER));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.ATTR_FEATURE_ID)) {
			catalogTransaction.setFeatureId((Integer) provRequestProp.getRequestMap().get(Constants.ATTR_FEATURE_ID));
		}
		catalogTransaction.setProvisionSystemId(provRequestProp.getSystemId());
		if (provRequestProp.getRequestMap().containsKey(Constants.ATTR_CATALOG_ID)) {
		catalogTransaction.setCatalogId( (String) provRequestProp.getRequestMap().get(Constants.ATTR_CATALOG_ID));
		}
		catalogTransaction.setRequestId(provRequestProp.getRequestId());
		if (provRequestProp.getRequestMap().containsKey(Constants.ATTR_STATUS_ID)) {
			catalogTransaction.setStatusId((Integer) provRequestProp.getRequestMap().get(Constants.ATTR_STATUS_ID));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.EXTERNAL_TRANSACTION_ID)) {		
			catalogTransaction.setExternalId((String) provRequestProp.getRequestMap().get(Constants.EXTERNAL_TRANSACTION_ID));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.C30_TRANSACTION_ID)) {		
			catalogTransaction.setC30TransactionId(provRequestProp.getC30TransactionId());
		}

		log.debug(catalogTransaction.toString());
		try {
		catalogTransaction.dbProcess(datasource);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	public void auditFeatureItemRequest(IProvisioningRequest request, C30JDBCDataSource datasource) throws C30ProvConnectorException {

		C30ProvFeatureItemTransaction featureItemTransaction = new C30ProvFeatureItemTransaction();
		ProvisioningRequestProperties provRequestProp = request.getRequestProperties();

		if (provRequestProp.getRequestMap().containsKey(Constants.ICCID)) {		
			featureItemTransaction.setExternalId((String) provRequestProp.getRequestMap().get(Constants.ICCID));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.TELEPHONE_NUMBER)) {
			featureItemTransaction.setTelephoneNumber((String) provRequestProp.getRequestMap().get(Constants.TELEPHONE_NUMBER));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.ATTR_FEATURE_ID)) {
			featureItemTransaction.setFeatureId((Integer) provRequestProp.getRequestMap().get(Constants.ATTR_FEATURE_ID));
		}
		featureItemTransaction.setRequestId(provRequestProp.getRequestId());
		featureItemTransaction.setSubRequestId(provRequestProp.getSubRequestId());
		if (provRequestProp.getRequestMap().containsKey(Constants.ATTR_STATUS_ID)) {		
			featureItemTransaction.setStatusId((Integer) provRequestProp.getRequestMap().get(Constants.ATTR_STATUS_ID));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.ATTR_FEATURE_ITEM_ID)) {
			featureItemTransaction.setFeatureItemId((Integer) provRequestProp.getRequestMap().get(Constants.ATTR_FEATURE_ITEM_ID));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.REQUEST_XML)) {		
			featureItemTransaction.setRequestXml((String) provRequestProp.getRequestMap().get(Constants.REQUEST_XML));
		}
		if (provRequestProp.getRequestMap().containsKey(Constants.EXTERNAL_TRANSACTION_ID)) {		
			featureItemTransaction.setExternalId((String) provRequestProp.getRequestMap().get(Constants.EXTERNAL_TRANSACTION_ID));
		}
		log.debug(featureItemTransaction.toString());
		System.out.println("Feature Item = "+featureItemTransaction.toString());
		
		featureItemTransaction.dbProcess(datasource);

	}

	
	public void auditRequest(IProvisioningRequest request, C30JDBCDataSource datasource) throws C30ProvConnectorException {

		C30ProvCatalogTransaction planTransaction = new C30ProvCatalogTransaction();
		ProvisioningRequestProperties provRequestProp = request.getRequestProperties();

		planTransaction.setC30TransactionId(provRequestProp.getC30TransactionId());
		planTransaction.setExternalTransactionId(provRequestProp.getExternalTransactionId());
		//planTransaction.setExternalId(externalId);
		//planTransaction.setTelephonNumber(telephonNumber);
		//planTransaction.setFeatureId(featureId);
		//planTransaction.setProvisionSystemId(provisionSystemId)
		//planTransaction.setPlanId(planId);
		planTransaction.setRequestId(provRequestProp.getRequestId());
		//planTransaction.setStatusId(statusId);

		planTransaction.dbProcess(datasource);

	}


	/**
	 * 
	 * @param dom
	 * @return
	 */
	private String getPayloadString(Document dom) throws C30ProvConnectorException {

		String payload = "";

		if (dom == null)  {
			return null;
		}

		try {

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			StreamResult result = new StreamResult(new StringWriter());
			DOMSource source = new DOMSource(dom);
			transformer.transform(source, result);

			return result.getWriter().toString();
		} 
		catch(TransformerException ex) {
			log.error(ex);
			throw new C30ProvConnectorException(ex, "PROV-007");
		}

	}



	
}
