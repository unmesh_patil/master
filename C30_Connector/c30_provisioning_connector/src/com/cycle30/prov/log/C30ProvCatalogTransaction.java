package com.cycle30.prov.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;

/** this is to store the data in the C30_PROV_PLAN_TRANS table
 * 
 * @author rnelluri
 *
 */
public class C30ProvCatalogTransaction {

	 private static Logger log = Logger.getLogger(C30ProvCatalogTransaction.class);  
	 
	private String telephoneNumber;
	private String externalId;
	private String catalogId;
	private Integer featureId;
	private Integer provisionSystemId;
	private Date requestDate;
	private String serviceType;
	private String externalTransactionId;
	private Integer c30TransactionId;
	private Integer statusId;
	private Integer requestId;
	
	public C30ProvCatalogTransaction(){}

	public C30ProvCatalogTransaction(String telephonNumber, String externalId,
			String catalogId, Integer featureId, Integer provisionSystemId,
			Date requestDate, String serviceType,
			String externalTransactionId, Integer c30TransactionId,
			Integer statusId, Integer requestId) {
		super();
		this.telephoneNumber = telephonNumber;
		this.externalId = externalId;
		this.catalogId = catalogId;
		this.featureId = featureId;
		this.provisionSystemId = provisionSystemId;
		this.requestDate = requestDate;
		this.serviceType = serviceType;
		this.externalTransactionId = externalTransactionId;
		this.c30TransactionId = c30TransactionId;
		this.statusId = statusId;
		this.requestId = requestId;
	}



	public String getTelephonNumber() {
		return telephoneNumber;
	}



	public void setTelephonNumber(String telephonNumber) {
		this.telephoneNumber = telephonNumber;
	}



	public String getExternalId() {
		return externalId;
	}



	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}



	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public Integer getFeatureId() {
		return featureId;
	}



	public void setFeatureId(Integer featureId) {
		this.featureId = featureId;
	}



	public Integer getProvisionSystemId() {
		return provisionSystemId;
	}



	public void setProvisionSystemId(Integer provisionSystemId) {
		this.provisionSystemId = provisionSystemId;
	}



	public Date getRequestDate() {
		return requestDate;
	}



	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}



	public String getServiceType() {
		return serviceType;
	}



	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}



	public String getExternalTransactionId() {
		return externalTransactionId;
	}



	public void setExternalTransactionId(String externalTransactionId) {
		this.externalTransactionId = externalTransactionId;
	}



	public Integer getC30TransactionId() {
		return c30TransactionId;
	}



	public void setC30TransactionId(Integer c30TransactionId) {
		this.c30TransactionId = c30TransactionId;
	}



	public Integer getStatusId() {
		return statusId;
	}



	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}



	public Integer getRequestId() {
		return requestId;
	}



	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}


	@Override
	public String toString() {
		return "C30ProvPlanTransaction [telephoneNumber=" + telephoneNumber
				+ ", externalId=" + externalId + ", catalogId=" + catalogId
				+ ", featureId=" + featureId + ", provisionSystemId="
				+ provisionSystemId + ", requestDate=" + requestDate
				+ ", serviceType=" + serviceType + ", externalTransactionId="
				+ externalTransactionId + ", c30TransactionId="
				+ c30TransactionId + ", statusId=" + statusId + ", requestId="
				+ requestId + "]";
	}

	/** Inserts the data into the database.
	 * @throws C30ProvConnectorException 
	 * 
	 */
	public void dbProcess(C30JDBCDataSource datasource) throws C30ProvConnectorException
	{
		// Store the data in the database

		log.info("Storing the data in the database with the Transaction Information");
		String query = SQLConstants.PROV_INSERT_C30_PROV_CATALOG_TRANS;
		log.debug("Query : "+ query);
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try
		{
			ps = datasource.getPreparedStatement(query);

			if (requestId!= null) ps.setInt(1, requestId); 
			ps.setString(2, externalId);
			ps.setString(3, telephoneNumber);
			ps.setString(4, catalogId);
			if (featureId!= null) {ps.setInt(5, featureId);} else {ps.setNull(5, java.sql.Types.INTEGER);}
			ps.setInt(6, provisionSystemId);
			ps.setString(7, serviceType);
			if (statusId!= null) {ps.setInt(8, statusId);} else {ps.setNull(8, java.sql.Types.INTEGER);}			
			ps.setString(9, externalTransactionId);
			if (c30TransactionId!= null) {ps.setInt(10, c30TransactionId);} else {ps.setNull(10, java.sql.Types.INTEGER);}
			
			resultSet  = ps.executeQuery();
			
			log.debug(this.toString());
			
		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-007");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}		
	}
	
}
