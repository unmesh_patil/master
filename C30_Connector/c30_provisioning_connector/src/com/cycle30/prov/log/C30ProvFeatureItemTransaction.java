package com.cycle30.prov.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;

/** c30_prov_trans_inst insert data
 * 
 * @author rnelluri
 *
 */
public class C30ProvFeatureItemTransaction {


	private static Logger log = Logger.getLogger(C30ProvFeatureItemTransaction.class);  

	private String telephoneNumber;
	private String externalId;
	private String requestXml;
	private String responseXml;
	private Integer featureId;
	private Integer featureItemId;
	private Date requestDate;
	private Date responseDate;
	private String serviceType;
	private String responseCode;
	private Integer statusId;
	private Integer requestId;
	private String subRequestId;
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getRequestXml() {
		return requestXml;
	}
	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}
	public String getResponseXml() {
		return responseXml;
	}
	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}
	public Integer getFeatureId() {
		return featureId;
	}
	public void setFeatureId(Integer featureId) {
		this.featureId = featureId;
	}
	public Integer getFeatureItemId() {
		return featureItemId;
	}
	public void setFeatureItemId(Integer featureItemId) {
		this.featureItemId = featureItemId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Date getResponseDate() {
		return responseDate;
	}
	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Integer getRequestId() {
		return requestId;
	}
	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	public String getSubRequestId() {
		return subRequestId;
	}
	public void setSubRequestId(String subRequestId) {
		this.subRequestId = subRequestId;
	}
	
	@Override
	public String toString() {
		return "C30ProvFeatureItemTransaction [telephoneNumber="
				+ telephoneNumber + ", externalId=" + externalId
				+ ", requestXml=" + requestXml + ", responseXml=" + responseXml
				+ ", featureId=" + featureId + ", featureItemId="
				+ featureItemId + ", requestDate=" + requestDate
				+ ", responseDate=" + responseDate + ", serviceType="
				+ serviceType + ", responseCode=" + responseCode
				+ ", statusId=" + statusId + ", requestId=" + requestId
				+ ", subRequestId=" + subRequestId + "]";
	}
	
	public C30ProvFeatureItemTransaction(String telephoneNumber,
			String externalId, String requestXml, String responseXml,
			Integer featureId, Integer featureItemId, Date requestDate,
			Date responseDate, String serviceType, String responseCode,
			Integer statusId, Integer requestId, String subRequestId) {
		super();
		this.telephoneNumber = telephoneNumber;
		this.externalId = externalId;
		this.requestXml = requestXml;
		this.responseXml = responseXml;
		this.featureId = featureId;
		this.featureItemId = featureItemId;
		this.requestDate = requestDate;
		this.responseDate = responseDate;
		this.serviceType = serviceType;
		this.responseCode = responseCode;
		this.statusId = statusId;
		this.requestId = requestId;
		this.subRequestId = subRequestId;
	}

	public C30ProvFeatureItemTransaction() {
		// TODO Auto-generated constructor stub
	}
	/** Inserts the data into the database.
	 * @throws C30ProvConnectorException 
	 * 
	 */
	public void dbProcess(C30JDBCDataSource datasource) throws C30ProvConnectorException
	{
		// Store the data in the database
		
		log.info("Storing the data in the database with the C30_prov_trans_inst");
		String query = SQLConstants.PROV_INSERT_C30_PROV_TRANS_INST;
		log.debug("Query : "+ query);
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try
		{
			
			ps = datasource.getPreparedStatement(query);
			
			if (requestId!= null ) ps.setInt(1, requestId); else ps.setNull(1, java.sql.Types.INTEGER);
			if (subRequestId!= null ) ps.setString(2, subRequestId);  else ps.setNull(2, java.sql.Types.CHAR);
			if (featureId!= null)ps.setInt(3, featureId);else ps.setNull(3, java.sql.Types.INTEGER);
			if (featureItemId!= null) ps.setInt(4, featureItemId); else ps.setNull(4, java.sql.Types.INTEGER);
			ps.setString(5, externalId);
			ps.setString(6, telephoneNumber);
			ps.setString(7, requestXml);
			ps.setString(8, responseXml);
			
			ps.setDate(9, C30ProvConnectorUtils.getSQLDateFromUtilDate(responseDate));
			ps.setString(10, responseCode);
			ps.setInt(11, statusId);
			
			resultSet = ps.executeQuery();
			
			log.debug(this.toString());
			
		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-010");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}		
	}
	

}
