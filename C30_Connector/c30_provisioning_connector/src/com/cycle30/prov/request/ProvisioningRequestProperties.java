package com.cycle30.prov.request;

import java.util.HashMap;

import com.cycle30.prov.auth.ProvAuthClientObject;

public class ProvisioningRequestProperties {

	private Integer systemId;
	private String resourceName;
	private HashMap requestMap;
	private Integer acctSegId;
	private String externalTransactionId;
	private Integer c30TransactionId;
	private String methodName;	
	private ProvAuthClientObject authClient;
	/** Main request and subrequest */
	private Integer requestId;
	private String subRequestId;
	/**Add/Disconnect, etc., **/
	private Integer requestType;
	
	/** Request and response */
	
	
	public Integer getSystemId() {
		return systemId;
	}
	public Integer getAcctSegId() {
		return acctSegId;
	}
	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}
	public Integer getRequestType() {
		return requestType;
	}
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}
	public Integer getRequestId() {
		return requestId;
	}
	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	public String getSubRequestId() {
		return subRequestId;
	}
	public void setSubRequestId(String subRequestId) {
		this.subRequestId = subRequestId;
	}
	public String getmethodName() {
		return methodName;
	}
	public void setmethodName(String methodName) {
		this.methodName = methodName;
	}	
	public String getExternalTransactionId() {
		return externalTransactionId;
	}
	public void setExternalTransactionId(String externalTransactionId) {
		this.externalTransactionId = externalTransactionId;
	}
	public Integer getC30TransactionId() {
		return c30TransactionId;
	}
	public void setC30TransactionId(Integer c30TransactionId) {
		this.c30TransactionId = c30TransactionId;
	}
	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public HashMap getRequestMap() {
		return requestMap;
	}
	public void setRequestMap(HashMap requestMap) {
		this.requestMap = requestMap;
	}
	public ProvAuthClientObject getAuthClient() {
		return authClient;
	}
	public void setAuthClient(ProvAuthClientObject authClient) {
		this.authClient = authClient;
	}
	
}
