package com.cycle30.prov.request.globecomm;

import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;

/**
 * @author User
 *
 */
public class DCNSubscriberRequest implements IProvisioningRequest {

    // General request properties
    private ProvisioningRequestProperties requestProperties;
    
    
    // DCN Subcriber specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public ProvisioningRequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set DCN Subcriber specific properties
     * 
     * @param ProvisioningRequestProperties the ProvisioningRequestProperties to set
     */
    public void setRequestProperties(ProvisioningRequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set Subscriber Balance-specific properties...
    }
    
}
