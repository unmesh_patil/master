package com.cycle30.prov.request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientDataSource;
import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvFeature;
import com.cycle30.prov.config.C30ProvFeatureCatalogMap;
import com.cycle30.prov.config.C30ProvFeatureItem;
import com.cycle30.prov.config.FeatureActions;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.sps.CatalogProvisionRequest;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.resource.ProvisioningResourceFactory;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.ProvsioningResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;


public class ProvisioningRequestHandler {

	private static final Logger log = Logger.getLogger(ProvisioningRequestHandler.class);
	private C30JDBCDataSource datasource = null;
	Boolean reqTextCall  = false;
	Boolean reqMailCall=false;
	Boolean reqDataCall=false;


	/**
	 * Return a JAX-RS 'Response' result from the client request.
	 * 
	 * @param resourceType
	 * @param headers
	 * @param ui
	 * @return response
	 */
	public IProvisioningResponse handleProvisionRequest (Integer systemId, HashMap inputRequest) throws C30ProvConnectorException {

		IProvisioningResponse response = null;
		int i = 1;
		try{
			datasource  = C30SDKDataSourceUtils.getDataSourceFromSDKPool();

			//get Request Name
			String resourceType = (String) inputRequest.get(Constants.ATTR_REQUEST_NAME);
			log.debug("resourceType =  " +resourceType);
	
			// Get the request associated with the type of resource requested
			IProvisioningRequest request = ProvisioningRequestFactory.getInstance().getRequestImpl(resourceType);
			inputRequest.put(Constants.ATTR_STATUS_ID, Constants.STATUS_REQUEST_INITIALIZED);
	
			ProvisioningRequestProperties requestProperties = setProperties(systemId, inputRequest);
	
			//Adding the requestId
			Integer requestId = getPlanRequestId(datasource);
			requestProperties.setRequestId(requestId);

			//SubrequestId = RequestId+"_"+Index
			//Need to calculate the subRequestId based on the acct segment id too..
			String carrierId = C30ProvConnectorUtils.getCarrierIdByAcctSegId(requestProperties.getAcctSegId());
			String subRequestId  = carrierId+"."+requestId.toString()+"."+requestProperties.getAuthClient().getSystemType();			
			requestProperties.setSubRequestId(subRequestId);
	
			request.setRequestProperties(requestProperties);
			checkSimulateRequest(request);

			new ProvRequestLogger().auditCatalogRequest(request, datasource);
	
			// Get the associated resource instance
			IProvisioningResource resource = ProvisioningResourceFactory.getInstance().getResourceImpl(resourceType);
			resource.setDatasource(datasource);
			response = resource.handleRequest(request);
		
		}//END TRY
		catch(Exception e)
		{

			if (e instanceof C30ProvConnectorException) 
				throw (C30ProvConnectorException)e;
			else
				throw new C30ProvConnectorException(e.getMessage(), e, "PROV-CON-003");
		}
		finally{
			try {
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(datasource.getConnection());
			}
			catch(Exception e)
			{
				throw new C30ProvConnectorException(e, "PROV-CON-001");
			}
		}

		return response;
	}

	//Simulate Request or Not Decision
	private boolean checkSimulateRequest(IProvisioningRequest request) throws C30ProvConnectorException {
		//Check if we need to simulate the request 
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		//If the request for this is True then no need to verify against the Exception Telephone List table.
		//Otherwise check the exception table to get the wether the given telephone number only need simulation or not.
		boolean simulateRequest = authData.isSimulateRequest();
		if (!simulateRequest)
		{
			boolean simulateTelephone = false;
			boolean simulateIccId = false;

			if (request.getRequestProperties().getRequestMap().get(Constants.TELEPHONE_NUMBER) != null )
				simulateTelephone= C30ProvConnectorUtils.isSimulatationRequiredForGivenExternalId((String) request.getRequestProperties().getRequestMap().get(Constants.TELEPHONE_NUMBER), datasource);
			if (request.getRequestProperties().getRequestMap().get(Constants.ICCID) != null )
				simulateIccId = C30ProvConnectorUtils.isSimulatationRequiredForGivenExternalId((String) request.getRequestProperties().getRequestMap().get(Constants.ICCID), datasource);

			if (simulateTelephone || simulateIccId)
				simulateRequest= true;
			else
				simulateRequest =false;
		}

		request.getRequestProperties().getRequestMap().put(Constants.ATTR_SIMULATE_REQUEST,simulateRequest);
		log.debug("Request Simulate = " +simulateRequest);
		return simulateRequest;

	}

	/**
	 * 
	 * @param systemId
	 * @param rollback
	 * @param inputRequest
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public synchronized IProvisioningResponse handlePlanProvisionRequest (Integer systemId, boolean rollback, HashMap inputRequest) throws C30ProvConnectorException {

		IProvisioningResponse response = null;
		try
		{
			datasource  = C30SDKDataSourceUtils.getDataSourceFromSDKPool();

			validateInputHashMap(inputRequest);
			//CatalogId and CatalogActionType
			String catalogId = (String) inputRequest.get(Constants.ATTR_CATALOG_ID);
			String catalogActionId = (String) inputRequest.get(Constants.ATTR_CATALOG_ACTION_ID);
			log.info("catalogId == "+catalogId);
			log.info("catalogActionId =="+catalogActionId);
	
			// Get the request associated with the type of resource requested
			//Each plan can have multiple Provisioning Requests
			HashMap<String, C30ProvFeatureCatalogMap>  awnSPSFeatureCatalogList = C30ProvConfig.getInstance().getProvfeaturePlanList();
			C30ProvFeatureCatalogMap featureCatalogMap = awnSPSFeatureCatalogList.get(catalogId+catalogActionId.toUpperCase()+systemId);
	
			if (featureCatalogMap == null) throw new C30ProvConnectorException("PROV-CATALOG-001");

			if ( inputRequest.get(Constants.ACCT_SEG_ID) != null && 
					! featureCatalogMap.getAcctSegId().equals(new Integer(0))  &&
					! featureCatalogMap.getAcctSegId().equals(new Integer((String) inputRequest.get(Constants.ACCT_SEG_ID)))) 
				throw new C30ProvConnectorException("PROV-VALIDATE-006");
			//Get the featureId from the featurePlanMap
			C30ProvFeature feature =  null;
	
			if (!rollback )
				feature = featureCatalogMap.getFeature();
			else
				feature =featureCatalogMap.getRollbackFeature();
			
			inputRequest.put(Constants.ATTR_FEATURE_ID, featureCatalogMap.getFeature().getFeatureId());
			inputRequest.put(Constants.ATTR_STATUS_ID, Constants.STATUS_REQUEST_INITIALIZED);

			CatalogProvisionRequest planRequest = new CatalogProvisionRequest();
			ProvisioningRequestProperties planRequestProperties = setProperties(systemId,  inputRequest);

			//Adding the requestId
			Integer requestId = getPlanRequestId(datasource);
			planRequestProperties.setRequestId(requestId);
			planRequest.setRequestProperties(planRequestProperties);

			//Store PlanRequest properties in the database.
			new ProvRequestLogger().auditCatalogRequest(planRequest, datasource);
			
			//Group all the same Resource ClassIds, so that all the featureItems would be using the same 
			//Get featureItems and its actions from the feature 
			ArrayList<ArrayList<FeatureActions>> featureResourceList = feature.getResourceList();
			int size=featureResourceList.size();
			int i = 1;
			
			if(catalogId.equalsIgnoreCase(Constants.CATALOG_PREPAID) && catalogActionId.equalsIgnoreCase(Constants.CATALOG_ACTION_ADD)){
				log.info("Prepaidservice ADD");
				//PW-65 code changes
				//modified code to send only one voice request this request itself includes international incoming call and international outgoing call as OFF
				for (ArrayList<FeatureActions> featureItemActionsForGivenResource : featureResourceList)
				{
				String resouceClassName =  featureItemActionsForGivenResource.get(0).getFeatureItem().getFeatureItemResourceClassName() ;
				if(resouceClassName.equalsIgnoreCase(Constants.BASIC_VOICE_RESOURCE)){
						provisioningRequest(featureItemActionsForGivenResource,systemId,inputRequest,requestId,i,size);
						i++;
						break;
				}
				}
				//send only one request for Voice mail,SMS and DATA
				//send provisioning request for other than Basic voice resources
				for (ArrayList<FeatureActions> featureItemActionsForGivenResource : featureResourceList)
				{
					 String resouceClassName =  featureItemActionsForGivenResource.get(0).getFeatureItem().getFeatureItemResourceClassName() ;
						
					 if(!resouceClassName.equalsIgnoreCase(Constants.BASIC_VOICE_RESOURCE)){
						 
						  inputRequest.put(Constants.REQUEST_XML,"");
						  provisioningRequestForSMSDATAVOICE(featureItemActionsForGivenResource,systemId,inputRequest,requestId,i,size);
					      i++;
					 }
				}
					
			}
			//else part for other catalogids like prepaid suspend ,Prepaid Dataonly Suspend Resume and also for Disconnect orders
			else{
				log.info("Not prepaid service Add work order");
				
				for (ArrayList<FeatureActions> featureItemActionsForGivenResource : featureResourceList)
				{
					provisioningRequest(featureItemActionsForGivenResource,systemId,inputRequest,requestId,i,size);
					i++;
				}
							
			}
			
			String responseValue = "";
			int waitTimerIterations = 0;
			
			//Wait for the response 
			while (true)
			{
				//Wait for some to get the response.
				try {
					Thread.sleep(Constants.THREAD_SLEEP_TIME);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
	
				//Make SQL call to get the actual request Status
				Integer statusId = C30ProvConnectorUtils.getStatusofRequestId(requestId.toString(),datasource);
	
				if (statusId == null ) throw new C30ProvConnectorException("PROV-INQ-011");
	
				if (statusId.equals(Constants.STATUS_REQUEST_COMPLETED)  || 
						statusId.equals(Constants.STATUS_REQUEST_SIMULATE_COMPLETED)) 
				{
					responseValue =  Constants.ATTR_RESPONSE_SUCCESS;
					break;
				}
	
				if (statusId.equals(Constants.STATUS_REQUEST_IN_ERROR)  || 
						statusId.equals(Constants.STATUS_REQUEST_SIMULATE_IN_ERROR)) 
					throw new C30ProvConnectorException("PROV-014");
	
				if (waitTimerIterations > Constants.THREAD_SLEEP_TIME_ITERATIONS)
					throw new C30ProvConnectorException("PROV-TIMEOUT");
	
	
				waitTimerIterations++;
			}

			// Populate the response
			response = new ProvsioningResponse();
			HashMap resp = new HashMap();
			resp.put(Constants.ATTR_REQUEST_ID, requestId);
			resp.put(Constants.ATTR_RESPONSE_CODE, responseValue);
			response.setResponseMap(resp);
		}
		catch(Exception e)
		{
			if (e instanceof C30ProvConnectorException) 
				throw (C30ProvConnectorException)e;
			else
				throw new C30ProvConnectorException(e.getMessage(), e, "PROV-CON-003");
		}
		finally{
			try{
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(datasource.getConnection());
			}
			catch(Exception e)
			{
				throw new C30ProvConnectorException(e, "PROV-CON-001");
			}
		}//END TRY
		
		return response;
	}

	/**
	 * @param featureItemActionsForGivenResource
	 * @param systemId
	 * @param inputRequest
	 * @param requestId
	 * @param i
	 * @param resListSize
	 * @throws C30ProvConnectorException
	 */
	public void provisioningRequest(ArrayList<FeatureActions> featureItemActionsForGivenResource,Integer systemId, HashMap inputRequest,Integer requestId,int i,int resListSize) throws C30ProvConnectorException
	{
		Class requestClass = null;
		Class resourceClass = null;
		
		try
		{
			requestClass = Class.forName(featureItemActionsForGivenResource.get(0).getFeatureItem().getFeatureItemRequestClassName());
			resourceClass = Class.forName(featureItemActionsForGivenResource.get(0).getFeatureItem().getFeatureItemResourceClassName());
		}
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-005");
		}
		IProvisioningRequest request =null;
		String resouceClassName =  featureItemActionsForGivenResource.get(0).getFeatureItem().getFeatureItemResourceClassName() ;
		log.info("Inside provisioning Request and Resource Class Name ="+resouceClassName);
		if (requestClass != null)
		request = ProvisioningRequestFactory.getInstance().getRequestImpl(requestClass);
		ProvisioningRequestProperties requestProperties = setProperties(systemId,  inputRequest);
		requestProperties.setRequestId(requestId);
		//Get a unique subRequestId
		//SubrequestId = RequestId+"_"+Index
		//Need to calculate the subRequestId based on the acct segment id too..
		String carrierId = C30ProvConnectorUtils.getCarrierIdByAcctSegId(requestProperties.getAcctSegId());
		String subRequestId  = carrierId+"."+requestId.toString()+"_"+i;
		requestProperties.setSubRequestId(subRequestId);
		requestProperties.getRequestMap().put(Constants.ATTR_FEATURE_ITEM_ACTIONS, featureItemActionsForGivenResource);
		//Set the featureId, planId into the HashMap
		request.setRequestProperties(requestProperties);
		//Check if we need to simulate the request 
		boolean simulateRequest = checkSimulateRequest(request);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_INITIALIZED, datasource); 
		// Get the associated resource instance
		IProvisioningResource resource = ProvisioningResourceFactory.getInstance().getResourceImpl(resourceClass);
		resource.setDatasource(datasource);
		resource.handleRequest(request);
		//Check if the request was successful
		//Then only we can proceed to the next request
		if(resouceClassName.equalsIgnoreCase(Constants.BASIC_VOICE_RESOURCE) && i < resListSize){
			waitForSubProcessToComplete(subRequestId, simulateRequest);	
			}
	 	}
	//sending one provisioning request for DATA,SMS and Voice
	public void provisioningRequestForSMSDATAVOICE(ArrayList<FeatureActions> featureItemActionsForGivenResource,Integer systemId, HashMap inputRequest,Integer requestId,int i,int resListSize) throws C30ProvConnectorException
	{
		Class requestClass = null;
		Class resourceClass = null;
		
		try
		{
			//PW-65 Created new class to send simultaneously SMS,DATA and Mail request to SPS
			requestClass=Class.forName("com.cycle30.prov.request.sps.SmsDataVoiceMailRequest");
			resourceClass=Class.forName("com.cycle30.prov.resource.sps.SmsDataVoiceMailResource");
						
		}
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-005");
		}
		IProvisioningRequest request =null;
		String resouceClassName="com.cycle30.prov.resource.sps.SmsDataVoiceMailResource";
		if (requestClass != null)
		request = ProvisioningRequestFactory.getInstance().getRequestImpl(requestClass);
		ProvisioningRequestProperties requestProperties = setProperties(systemId,  inputRequest);
		requestProperties.setRequestId(requestId);
		String carrierId = C30ProvConnectorUtils.getCarrierIdByAcctSegId(requestProperties.getAcctSegId());
		String subRequestId  = carrierId+"."+requestId.toString()+"_"+i;
		requestProperties.setSubRequestId(subRequestId);
		requestProperties.getRequestMap().put(Constants.ATTR_FEATURE_ITEM_ACTIONS, featureItemActionsForGivenResource);
		//Set the featureId, planId into the HashMap
		request.setRequestProperties(requestProperties);
		//Check if we need to simulate the request 
		boolean simulateRequest = checkSimulateRequest(request);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_INITIALIZED, datasource); 
		// Get the associated resource instance
		IProvisioningResource resource = ProvisioningResourceFactory.getInstance().getResourceImpl(resourceClass);
		resource.setDatasource(datasource);
		resource.handleRequest(request);
		//**** To Send parallel Execution of voice mail ,Data,Text ****** //
		reqTextCall=(Boolean)request.getRequestProperties().getRequestMap().get(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_TEXT);
		reqDataCall=(Boolean)request.getRequestProperties().getRequestMap().get(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA);
		reqMailCall=(Boolean)request.getRequestProperties().getRequestMap().get(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_VOICE_MAIL);
		
		if(reqTextCall !=null && reqDataCall !=null && reqMailCall !=null){
		if(reqTextCall && reqDataCall && reqMailCall)
			{
				//request.getRequestProperties().getRequestMap().put(Constants.REQUEST_XML, sb.toString());
				resource.handleRequest(request);
			}
		}
			
 	}

	
	/** This checks as if the given subrequest is completed or not
	 * 
	 * @param subRequestId
	 * @throws C30ProvConnectorException 
	 */
	private void waitForSubProcessToComplete(String subRequestId, boolean simulateRequest) throws C30ProvConnectorException {

		int waitTimerIterations = 0;
		while ( C30ProvConnectorUtils.getStatusofSubProcessId(subRequestId, datasource).equals(Constants.STATUS_REQUEST_SYNC_RESPONSE_RECIEVED))
		{
			try {
				Thread.sleep(Constants.THREAD_SLEEP_TIME);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			waitTimerIterations++;

			if (waitTimerIterations > Constants.THREAD_SLEEP_TIME_ITERATIONS)
				throw new C30ProvConnectorException("PROV-TIMEOUT");
		}

		if ( C30ProvConnectorUtils.getStatusofSubProcessId(subRequestId, datasource).equals(Constants.STATUS_REQUEST_SIMULATE_COMPLETED) ||
				C30ProvConnectorUtils.getStatusofSubProcessId(subRequestId, datasource).equals(Constants.STATUS_REQUEST_COMPLETED)
				)
			return;


		if ( C30ProvConnectorUtils.getStatusofSubProcessId(subRequestId, datasource).equals(Constants.STATUS_REQUEST_SIMULATE_IN_ERROR) || 
				C30ProvConnectorUtils.getStatusofSubProcessId(subRequestId, datasource).equals(Constants.STATUS_REQUEST_IN_ERROR ))
			throw new C30ProvConnectorException("PROV-014");

	}

	/** Validate the input HashMap for the provisioning request.
	 * @throws C30ProvConnectorException 
	 * 
	 */
	private void validateInputHashMap(HashMap inputRequest) throws C30ProvConnectorException {
		//CatalogId and CatalogActionType
		String catalogId = (String) inputRequest.get(Constants.ATTR_CATALOG_ID);
		if (C30ProvConnectorUtils.isEmptyorNull(catalogId))
		{
			throw new C30ProvConnectorException("PROV-VALIDATE-001");
		}

		String catalogActionId = (String) inputRequest.get(Constants.ATTR_CATALOG_ACTION_ID);
		if (C30ProvConnectorUtils.isEmptyorNull(catalogActionId))
		{
			throw new C30ProvConnectorException("PROV-VALIDATE-002");
		}

		String acctSegid = (String) inputRequest.get(Constants.ACCT_SEG_ID);
		if (C30ProvConnectorUtils.isEmptyorNull(acctSegid))
		{
			throw new C30ProvConnectorException("PROV-VALIDATE-007");
		}

		String extTransId = (String) inputRequest.get(Constants.EXTERNAL_TRANSACTION_ID);
		if (C30ProvConnectorUtils.isEmptyorNull(extTransId))
		{
			throw new C30ProvConnectorException("PROV-VALIDATE-008");
		}

		String c30TransId = (String) inputRequest.get(Constants.C30_TRANSACTION_ID);
		if (C30ProvConnectorUtils.isEmptyorNull(c30TransId))
		{
			throw new C30ProvConnectorException("PROV-VALIDATE-009");
		}

	}

	/** Get the requestId for the given Catalog from the sequence.
	 * 
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public synchronized Integer getPlanRequestId(C30JDBCDataSource datasource) throws C30ProvConnectorException 
	{

		log.info("Storing the data in the database with the Transaction Information");
		String query = SQLConstants.PROV_GET_PLAN_REQUEST_SEQUENCE_INFO;
		log.debug("Query : "+ query);
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		Integer requestId = null;
		try
		{

			ps = datasource.getPreparedStatement(query);
			resultSet  = ps.executeQuery();

			if (resultSet.next()) 
			{
				requestId= resultSet.getInt("REQUEST_ID");
			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-009");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();					
				}
			} 
			catch (Exception ex)
			{
				log.error("Exception: "+ex.getMessage());
			}
		}
		return requestId;	

	}

	/** Setting the properties in the request 
	 * 
	 * @param systemId
	 * @param requestName
	 * @param input
	 * @return
	 * @throws C30ProvConnectorException 
	 */
	private ProvisioningRequestProperties setProperties(Integer systemId,  HashMap input) throws C30ProvConnectorException
	{

		if (input == null) 	throw new C30ProvConnectorException ("PROV-006");

		ProvAuthClientObject authClient = ProvAuthClientDataSource.getInstance().getAuthObject(systemId, (String) input.get(Constants.PROVISION_REQUEST_TYPE));

		log.info("authClient : " +authClient.getWeburl());

		ProvisioningRequestProperties prop = new ProvisioningRequestProperties();

		//Read the input HashMap for the ExternalTransactionId and C30TransactionId
		String externalTransactionId =null;
		if ( input.get(Constants.EXTERNAL_TRANSACTION_ID) !=null) 
		{
			externalTransactionId = (String) input.get(Constants.EXTERNAL_TRANSACTION_ID);
			prop.setExternalTransactionId(externalTransactionId);
		}
		Integer c30TransactionId = null;
		if (input.get(Constants.C30_TRANSACTION_ID) !=null)
		{
			// This is passed as string..
			c30TransactionId = new Integer((String) input.get(Constants.C30_TRANSACTION_ID));
			prop.setC30TransactionId(c30TransactionId);
		}

		prop.setRequestMap(input);
		prop.setSystemId(systemId);
		prop.setAuthClient(authClient);
		Integer acctSegId =null;
		if (input.get(Constants.ACCT_SEG_ID) !=null)
		{
			// This is passed as string..
			acctSegId = new Integer((String) input.get(Constants.ACCT_SEG_ID));
			prop.setAcctSegId(acctSegId);
		}



		return prop;

	}

	public void setDatasource(C30JDBCDataSource datasource) {
		this.datasource = datasource;
	}

}
