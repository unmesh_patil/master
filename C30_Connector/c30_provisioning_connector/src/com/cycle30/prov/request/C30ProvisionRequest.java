package com.cycle30.prov.request;

import java.util.HashMap;

import com.cycle30.prov.auth.ProvAuthClientDataSource;
import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.response.IProvisioningResponse;

public class C30ProvisionRequest {

	
	private static C30ProvisionRequest provInstance = null;
	private static ProvAuthClientObject authClient = null;

	/** Using the dataSource Instance to Initialize only once.
	 * 
	 * @return
	 * @throws ProvAuthClientDataSource
	 */
	public static C30ProvisionRequest getInstance() throws C30ProvConnectorException
	{
		if(provInstance == null)
			provInstance = new C30ProvisionRequest();
		return provInstance;
	}


	/** 
	 * @throws C30ProvConnectorException 
	 * 
	 */
	C30ProvisionRequest() throws C30ProvConnectorException
	{
		//Load all the configuration for the Feature and Plans.
		C30ProvConfig.getInstance();
		
	}
	

	/** Handles all the direct Provision Requests
	 * @throws C30ProvConnectorException 
	 * 
	 */
	public HashMap awnProvisioningRequest(HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_PROVISION);
		return ((IProvisioningResponse)provisioningRequest(Constants.VENDOR_AWN_SPS, input)).getResponse();
		
	}
	/** Handles all the direct Provision Requests
	 * @throws C30ProvConnectorException 
	 * 
	 */
	public IProvisioningResponse provisioningRequest(Integer systemId,  HashMap input) throws C30ProvConnectorException
	{
		
		return new ProvisioningRequestHandler().handlePlanProvisionRequest(systemId, false, input);
		
	}

	/** Handles all the Prepaid Order requests
	 * @throws C30ProvConnectorException 
	 * 
	 */
	public HashMap awnprepaidOrderRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_OCS_PROVISION);
		return ((IProvisioningResponse) inquiryRequest(Constants.VENDOR_AWN_OCS,  input)).getResponse();
		
	}	
	
	
	/** Handles all the plan provisioning requests directly
	 * @throws C30DeviceCareException 
	 * 
	 */
	public IProvisioningResponse  inquiryRequest(Integer systemId, HashMap input) throws C30ProvConnectorException
	{
		
		return new ProvisioningRequestHandler().handleProvisionRequest(systemId,input);
		
	}
	/** Handles all the plan provisioning requests directly
	 * @throws C30DeviceCareException 
	 * 
	 */
	public HashMap awnInquiryRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_INQUIRY);
		return ((IProvisioningResponse) inquiryRequest(Constants.VENDOR_AWN_SPS,  input)).getResponse();
		
	}
	
	
	
	/** Rollback request for the given catalogId
	 * 
	 * @param input
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public HashMap awnProvisioningRollbackRequest(HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_PROVISION);
		return ((IProvisioningResponse)provisioningRolblackRequest(Constants.VENDOR_AWN_SPS, input)).getResponse();
		
	}
	/** Handles all the direct Provision Requests
	 * @throws C30ProvConnectorException 
	 * 
	 */
	
	
	/** Handles all the OCS Enquiry  requests directly
	 * @throws C30DeviceCareException 
	 * 
	 */
	public HashMap ocsInquiryRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_OCS_PROVISION);
		return ((IProvisioningResponse) inquiryRequest(Constants.VENDOR_AWN_OCS,  input)).getResponse();
		
	}
	
	
	/** Handles all the INCOMM Enquiry  requests directly
	 * @throws C30DeviceCareException 
	 * 
	 */
	public HashMap incommInquiryRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_INCOMM_PROVISION);
		return ((IProvisioningResponse) inquiryRequest(Constants.VENDOR_AWN_PREPAID_INCOMM,  input)).getResponse();
		
	}
	
	
	
	/** Handles all the BLACKHAWK Enquiry  requests directly
	 * @throws C30DeviceCareException 
	 * 
	 */
	public HashMap blackHawkInquiryRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_BLACKHAWK_PROVISION);
		return ((IProvisioningResponse) inquiryRequest(Constants.VENDOR_AWN_PREPAID_BLACKHAWK,  input)).getResponse();
		
	}

	
	/** Handles all the GLOBECOMM  requests directly
	 * @throws C30DeviceCareException 
	 * 
	 */
	public HashMap globeCommProvisioningRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_GLOBECOMM_PROVISION);
		return ((IProvisioningResponse)provisioningRequest(Constants.VENDOR_DCN_GLOBECOMM, input)).getResponse();
	}

	public IProvisioningResponse provisioningRolblackRequest(Integer systemId,  HashMap input) throws C30ProvConnectorException
	{
		
		return new ProvisioningRequestHandler().handlePlanProvisionRequest(systemId, true, input);
		
	}
	
	
	/** Handles all the MulePOS Phone Number Enquiry  requests directly
	 * @throws C30DeviceCareException 
	 * 
	 */
	public HashMap prepaidPhoneInventoryInquiryRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_MULEPOS_PREPAID_PROVISION);
		return ((IProvisioningResponse) inquiryRequest(Constants.VENDOR_AWN_PREPAID_SPS,  input)).getResponse();
		
	}
	
	/** Handles all the ACS TN Inventory requests directly
	 * @throws C30ProvConnectorException 
	 * 
	 */
	public HashMap prepaidPhoneInventoryInquiryACSRequest( HashMap input) throws C30ProvConnectorException
	{
		input.put(Constants.PROVISION_REQUEST_TYPE, Constants.PROV_ACS_PREPAID_TN_INV_PROVISION);
		return ((IProvisioningResponse) inquiryRequest(Constants.VENDOR_AWN_PREPAID_ACS,  input)).getResponse();
		
	}

	
}
