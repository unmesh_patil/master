package com.cycle30.prov.request;


/** This implementers the request related to different Vendors
 * 
 * @author rnelluri
 *
 */
public interface IProvisioningRequest {
	
	public void setRequestProperties(ProvisioningRequestProperties props);
    public ProvisioningRequestProperties getRequestProperties();

}

