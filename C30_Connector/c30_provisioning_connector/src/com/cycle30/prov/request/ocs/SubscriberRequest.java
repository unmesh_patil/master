/**
 * 
 */
package com.cycle30.prov.request.ocs;

import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;

/**
 * @author User
 *
 */
public class SubscriberRequest implements IProvisioningRequest {

    // General request properties
    private ProvisioningRequestProperties requestProperties;
    
    
    // Subscriber-specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public ProvisioningRequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general and Subscriber-specific properties
     * 
     * @param ProvisioningRequestProperties the ProvisioningRequestProperties to set
     */
    public void setRequestProperties(ProvisioningRequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set Subscriber-specific properties...
    }
    
}
