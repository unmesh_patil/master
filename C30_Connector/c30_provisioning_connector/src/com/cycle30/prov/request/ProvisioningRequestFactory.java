package com.cycle30.prov.request;

import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.acs.inquiry.GetACSPhoneInventoryRequest;
import com.cycle30.prov.request.blackhawk.inquiry.BlackHawkVoucherRequest;
import com.cycle30.prov.request.gci.inquiry.GetGCIPhoneInventoryRequest;
import com.cycle30.prov.request.gci.inquiry.GetGCISessionIdRequest;
import com.cycle30.prov.request.incomm.inquiry.IncommVoucherRequest;
import com.cycle30.prov.request.ocs.SubscriberBalanceRequest;
import com.cycle30.prov.request.ocs.SubscriberProfileRequest;
import com.cycle30.prov.request.ocs.SubscriberRequest;
import com.cycle30.prov.request.ocs.inquiry.GetPrepaidBalanceRequest;
import com.cycle30.prov.request.ocs.inquiry.GetPrepaidBalanceTypesRequest;
import com.cycle30.prov.request.ocs.inquiry.GetPrepaidProfilesRequest;
import com.cycle30.prov.request.ocs.inquiry.GetPrepaidSubcriberDetailsRequest;
import com.cycle30.prov.request.ocs.inquiry.PrepaidAccountRequest;
import com.cycle30.prov.request.ocs.inquiry.PrepaidVoucherRequest;
import com.cycle30.prov.request.sps.inquiry.GetIMSIRequest;
import com.cycle30.prov.request.sps.inquiry.SPSRequestHistoryRequest;
import com.cycle30.prov.request.sps.inquiry.SPSRequestStatusRequest;
import com.cycle30.prov.request.sps.inquiry.SubscriberInquiryRequest;
import com.cycle30.prov.request.sps.inquiry.TelephoneNumberInquiryRequest;
import com.cycle30.prov.request.sps.inquiry.ValidateSIMRequest;



/**
 * 
 * Resource request factory.
 *
 */
public class ProvisioningRequestFactory {

	// Instantiate singleton
	private static final ProvisioningRequestFactory self = new ProvisioningRequestFactory();

	private static final Logger log = Logger.getLogger(ProvisioningRequestFactory.class);

	private static final String EXCEPTION_SOURCE = "REQUEST-FACTORY";


	//-------------------------------------------------------------------------
	/** Private constructor. */
	private ProvisioningRequestFactory() {
		super();
	}


	//-------------------------------------------------------------------------
	/**
	 * Return instance of singleton class
	 * 
	 * @return ResourceRequestFactory instance.
	 */
	public static ProvisioningRequestFactory getInstance() {
		return self;
	}


	/**Instantiate correct  request interface based on the resource name
	 * 
	 * @param resourceName
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public IProvisioningRequest getRequestImpl(String resourceName) throws C30ProvConnectorException  {

		if (resourceName == null ) throw new C30ProvConnectorException ("PROV-CATALOG-002");
		
		if (resourceName.startsWith(Constants.REQUEST_IMSI_VERIFICATION)) {
			return new GetIMSIRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_VALIDATE_SIM)) {
			return new ValidateSIMRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_TELEPHONE_NUMBER_VERIFICATION)) {
			return new TelephoneNumberInquiryRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_SUBSCRIBER_INQUIRY)) {
			return new SubscriberInquiryRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_SPS_REQUEST_HISTORY_INQUIRY)) {
			return new SPSRequestHistoryRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_SPS_REQUEST_STATUS_BY_REQUEST_ID)) {
			return new SPSRequestStatusRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_SPS_REQUEST_STATUS_BY_TRANSACTION_ID)) {
			return new SPSRequestStatusRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_BALANCE_INQUIRY)) {
			return new GetPrepaidBalanceRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_GCI_MULEPOS_SESSION_ID_INQUIRY)) {
			return new GetGCISessionIdRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_GCI_PHONE_INVENTORY_INQUIRY)) {
			return new GetGCIPhoneInventoryRequest();			
		}
		if (resourceName.startsWith(Constants.REQUEST_GCI_PHONE_INVENTORY_UPADTE)) {
			return new GetGCIPhoneInventoryRequest();			
		}
		if (resourceName.equalsIgnoreCase(Constants.REQUEST_ACS_PHONE_INVENTORY_INQUIRY)) {
			return new GetACSPhoneInventoryRequest();			
		}
		if (resourceName.equalsIgnoreCase(Constants.REQUEST_ACS_PHONE_INVENTORY_UPADTE)) {
			return new GetACSPhoneInventoryRequest();			
		}
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_VOUCHER_STATUS)) {
			return new PrepaidVoucherRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_BALANCE_UNITS_CACHE)) {
			return new GetPrepaidBalanceTypesRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_DETAILS)) {
			return new GetPrepaidSubcriberDetailsRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_OCS_ACCOUNT_HISTORY) || resourceName.startsWith(Constants.REQUEST_OCS_ACCOUNT_SEARCH)) {
			return new PrepaidAccountRequest();
		}	
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER) || resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_UPDATE_SUBSCRIBER)) {
			return new SubscriberRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_PROFILE)) {
			return new SubscriberProfileRequest();
		}		
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_ORDER_BAL_UPDATE)) {
			return new SubscriberBalanceRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_OCS_PREPAID_PROFILES_CACHE)) {
			return new GetPrepaidProfilesRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_INCOMM_PREPAID_VOUCHER_STATUS)) {
			return new IncommVoucherRequest();
		}
		if (resourceName.startsWith(Constants.REQUEST_BLACK_HAWK_PREPAID_VOUCHER_STATUS)) {
			return new BlackHawkVoucherRequest();
		}

		
		// If not found
				String error = "Unable to instantiate correct request type based on resource name " + resourceName;
				log.error(error);

		throw new C30ProvConnectorException (error, "PROV-005");
	}

	/** Instantiate the correct Request based on the class supplied
	 * 
	 * @param resourceClass
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public IProvisioningRequest getRequestImpl(Class resourceClass) throws C30ProvConnectorException  {
		try
		{
			return (IProvisioningRequest) resourceClass.newInstance();
		}
		catch(Exception e)
		{
			// If not found
			String error = "Unable to instantiate correct request type ("+resourceClass.toString()+") based on resource name " ;
			log.error(error);

			throw new C30ProvConnectorException (e, "PROV-005");
		}
	}
}
