package com.cycle30.prov.request.sps;

import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;

/** This is the main provision request from the workpoint or any other system
 * i.e., using the c30_provisioning_connector
 * 
 * @author rnelluri
 *
 */
public class CatalogProvisionRequest  implements IProvisioningRequest {

    // General request properties
    private ProvisioningRequestProperties requestProperties;
    
    
    // Service-specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public ProvisioningRequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general and Service-specific properties
     * 
     * @param ProvisioningRequestProperties the ProvisioningRequestProperties to set
     */
    public void setRequestProperties(ProvisioningRequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set Service-specific properties...
    }
    
}

