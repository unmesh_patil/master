package com.cycle30.prov.request.sps.inquiry;

import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;

public class SubscriberInquiryRequest    implements IProvisioningRequest {

    // General request properties
    private ProvisioningRequestProperties requestProperties;
    
    
    // Service-specific properties
    // ...
    
    
    
    /**
     * @return the requestProperties
     */
    public ProvisioningRequestProperties getRequestProperties() {
        return requestProperties;
    }

    
    /**
     * Set general and Service-specific properties
     * 
     * @param ProvisioningRequestProperties the ProvisioningRequestProperties to set
     */
    public void setRequestProperties(ProvisioningRequestProperties requestProperties) {
        
        this.requestProperties = requestProperties;
        
        // Set Service-specific properties...
    }
    
}

