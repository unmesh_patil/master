package com.cycle30.prov.auth;

import java.util.Date;
import java.util.HashMap;

import com.cycle30.prov.data.Constants;

/** This stores all the authentication data required for the Provsioning business object.
 * 
 * @author rnelluri
 *
 */
public class ProvAuthClientObject {



	/** Default constructor
	 * 
	 * @param systemId
	 * @param weburl
	 * @param userName
	 * @param password
	 * @param addtlInfo
	 * @param userSessionId
	 * @param systemType
	 * @param sessionExpDate
	 * @param simulateRequest
	 */
	public ProvAuthClientObject(Integer provSystemId, String weburl,
			String userName, String password, String addtlInfo,
			 String systemType, 
			boolean simulateRequest, String provCallbackSystemId, String property1, String property2) {
		super();
		this.provSystemId = provSystemId;
		this.weburl = weburl;
		this.userName = userName;
		this.password = password;
		this.addtlInfo = addtlInfo;
		this.systemType = systemType;
		this.simulateRequest = simulateRequest;
		this.provCallbackSystemId = provCallbackSystemId;
		this.property1=property1;
		this.property2=property2;
	}



	/** This return the AuthCare object as String 
	 * 
	 * @param make
	 * @return
	 */
	@Override
	public String toString() {
		return "ProvAuthClientObject [systemId=" + provSystemId + ", weburl="
				+ weburl + ", userName=" + userName + ", password= ********" 
				+ ", addtlInfo=" + addtlInfo + ", userSessionId="
				+ userSessionId + ", systemType=" + systemType
				+ ", sessionExpDate=" + sessionExpDate + ", simulateRequest="
				+ simulateRequest + ", property1=" + property1 
				+ "property2="  +property2 +"]";
	}

	
	
	/** This return the AuthCare object as hashMap 
	 * 
	 * @param make
	 * @return
	 */
	public HashMap toHashMap(Integer systemId)
	{
		HashMap returnMap = new HashMap();
		
		if (systemId.equals(Constants.VENDOR_AWN_SPS))
		{
			returnMap.put(Constants.PROV_AUTH_CLIENT_USER_NAME, this.userName);
			returnMap.put(Constants.PROV_AUTH_CLIENT_USER_PASSWORD, this.password);
			returnMap.put(Constants.PROV_AUTH_CLIENT_WEBURL, this.weburl);
		}
		
		return returnMap;
	}


	
	public Integer getProvSystemId() {
		return provSystemId;
	}



	public void setProvSystemId(Integer provSystemId) {
		this.provSystemId = provSystemId;
	}



	public String getWeburl() {
		return weburl;
	}



	public void setWeburl(String weburl) {
		this.weburl = weburl;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getAddtlInfo() {
		return addtlInfo;
	}



	public void setAddtlInfo(String addtlInfo) {
		this.addtlInfo = addtlInfo;
	}



	public String getUserSessionId() {
		return userSessionId;
	}



	public void setUserSessionId(String userSessionId) {
		this.userSessionId = userSessionId;
	}



	public String getSystemType() {
		return systemType;
	}



	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}



	public Date getSessionExpDate() {
		return sessionExpDate;
	}



	public void setSessionExpDate(Date sessionExpDate) {
		this.sessionExpDate = sessionExpDate;
	}



	public boolean isSimulateRequest() {
		return simulateRequest;
	}



	public void setSimulateRequest(boolean simulateRequest) {
		this.simulateRequest = simulateRequest;
	}



	public String getSslTrustStoreLocation() {
		return sslTrustStoreLocation;
	}



	public void setSslTrustStoreLocation(String sslTrustStoreLocation) {
		this.sslTrustStoreLocation = sslTrustStoreLocation;
	}



	public String getProvCallbackSystemId() {
		return provCallbackSystemId;
	}



	public void setProvCallbackSystemId(String provCallbackSystemId) {
		this.provCallbackSystemId = provCallbackSystemId;
	}


	public String getproperty1() {
		return property1;
	}



	public void setproperty1(String property1) {
		this.property1 = property1;
	}



	public String getproperty2() {
		return property2;
	}



	public void setproperty2(String property2) {
		this.property2 = property2;
	}


	Integer provSystemId;
	String weburl;
	String userName;
	String password;
	String addtlInfo;
	String userSessionId;
	String systemType;
	Date sessionExpDate;
	boolean simulateRequest;
	String sslTrustStoreLocation;
	String provCallbackSystemId;
	String property1; // it is X-Env-Id in case of GCI MulePOS Connector 
	String property2; // It is X-User-Id in case of GCI MulePOS Connector
	}
