package com.cycle30.prov.auth;

import com.cycle30.prov.exception.C30ProvConnectorException;

/** This is used to implement the Authentication request and response.
 * 
 * @author rnelluri
 *
 */
public interface IProvisionConnectorAuthRequest {
	
	/** Make the call with the Webservice for authentication and return the session */
	public  String authenticate() throws C30ProvConnectorException;
}
