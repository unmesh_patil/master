package com.cycle30.prov.auth;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate.encryptor.HibernatePBEEncryptorRegistry;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;

/** this holds all the data from the database. This is a singleton class
 * 
 * @author rnelluri
 *
 */
public class ProvAuthClientDataSource {

	private static Logger log = Logger.getLogger(ProvAuthClientDataSource.class);

	public static final ResourceBundle propertyFileResource = ResourceBundle.getBundle(Constants.C30_PROPERTY_TEXT_FILE_NAME);


	private static ProvAuthClientDataSource authInstance = null;
	private HashMap authDataMap;

	/** Using the dataSource Instance to Initialize only once.
	 * 
	 * @return
	 * @throws ProvAuthClientDataSource
	 */
	public static ProvAuthClientDataSource getInstance() throws C30ProvConnectorException
	{
		if(authInstance == null)
			authInstance = new ProvAuthClientDataSource();
		return authInstance;
	}


	/** This is used to get the AuthCare from the database 
	 * @throws C30DeviceCareException 
	 * 
	 */
	ProvAuthClientDataSource() throws C30ProvConnectorException
	{
		authDataMap = getAuthenticateRequestHashMap(propertyFileResource.getString("ENVNAME"));    	
	}


	/** Return the value of the Environmental Variable
	 * 
	 * @param key
	 * @return
	 */
	public String getValueFromSDKPropFile(String key)
	{
		return propertyFileResource.getString(key);
	}

	/** This is used to get the AuthenticateObject
	 * 
	 * @param make
	 * @return
	 */
	public ProvAuthClientObject getAuthObject(Integer systemId, String systemType)
	{
		ProvAuthClientObject data = (ProvAuthClientObject) authDataMap.get(systemId.toString()+"_"+systemType);
		return data;
	}

	/** This is used to get the AuthenticateObject as HashMap
	 * 
	 * @param make
	 * @return
	 */
	public HashMap getAuthObjectAsHashMap(Integer systemId)
	{
		ProvAuthClientObject data = (ProvAuthClientObject) authDataMap.get(systemId);
		return data.toHashMap(systemId);
	}

	/** This is used to get the authentication request data from the database.
	 * 
	 * @param make
	 * @return
	 * @throws C30ProvConnectorException 
	 * @throws Exception 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws C30DeviceCareException 
	 */
	private HashMap<String, ProvAuthClientObject> getAuthenticateRequestHashMap(String env) throws C30ProvConnectorException  {
		HashMap<String, ProvAuthClientObject> response = new HashMap();


		log.info("Fetching all the authentication data from database");
		String query = SQLConstants.PROV_QUERY_GET_CLIENT_AUTH_DATA;
		C30JDBCDataSource datasource  = null;		
		ResultSet resultSet  = null;
		PreparedStatement ps = null;

		try
		{
			datasource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();
			ps = datasource.getPreparedStatement(query);
			ps.setString(1, env);
			log.debug("Query : "+ query + ", Env = " +env); 
			resultSet  = ps.executeQuery();

			while (resultSet.next()) 
			{
				String pwd = resultSet.getString("PWD");
				pwd = decryptPassword(pwd);
				ProvAuthClientObject dcAuthObject = new ProvAuthClientObject(new Integer(resultSet.getString("PROV_SYSTEM_ID")), 
						resultSet.getString("URL"), 
						resultSet.getString("USER_ID"),
						pwd,null,
						resultSet.getString("SYSTEM_TYPE"), new Boolean(resultSet.getString("SIMULATE_REQUEST")).booleanValue(),
						resultSet.getString("PROV_CALLBACK_SYSTEM_ID"),
						resultSet.getString("GCI_ENV"),
						resultSet.getString("GCI_USER"));

				dcAuthObject.setSslTrustStoreLocation(getValueFromSDKPropFile("ssl.truststore.location"));
				response.put(resultSet.getString("PROV_SYSTEM_ID")+"_"+ resultSet.getString("SYSTEM_TYPE"), dcAuthObject);
				log.debug(dcAuthObject.toString());
			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-001");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(datasource.getConnection());
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30ProvConnectorException(ex, "PROV-001");
			}
		}		
		return response;
	}

	/** Decrypt db password stored in Advanced Encryption Standard (AES) format.
	 * 
	 * @param pwd
	 * @return
	 * @throws Exception
	 */
	private  String decryptPassword(String pwd) throws Exception {


		String decryptedPwd = "";
		String decryptionPassword = "";
		if (pwd == null || (pwd !=null && pwd.equalsIgnoreCase(""))) return "";
		// decrypt the password.
		try {
			log.info("Input string :"+ pwd);

			if (decryptionPassword.trim().length() == 0){
				String decryptionUserKey = propertyFileResource.getString("DBENCRYPTKEY");  
				decryptionPassword = System.getenv(decryptionUserKey);
			}
			StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
			strongEncryptor.setAlgorithm(Constants.C30_DECRYPTION_ALOG);
			strongEncryptor.setPassword(decryptionPassword);
			HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
			registry.registerPBEStringEncryptor("configurationHibernateEncryptor", strongEncryptor);
			decryptedPwd = strongEncryptor.decrypt(pwd);

		} 
		catch (Exception e) {
			throw new Exception ("Error decrypting password: ", e);
		}

		return decryptedPwd;

	}
}
