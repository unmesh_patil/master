package com.cycle30.prov.utils;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.DateFormat;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestHandler;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.util.C30DOMUtils;

public class C30ProvConnectorUtils {

	private static Logger log = Logger.getLogger(C30ProvConnectorUtils.class);

	/** This is to find if the string is Null or Empty
	 *
	 * @param input
	 * @return
	 */
	public static boolean isEmptyorNull(String input)
	{
		boolean retValue = true;
		if(input!= null && !input.equalsIgnoreCase(""))
			retValue = false;

		return retValue;
	}


	/** Returns the Carrier Id based on the acct segment id
	 *
	 * @param acctSegId
	 * @return
	 */
	public static String getCarrierIdByAcctSegId(Integer acctSegId) {

		String retValue= Constants.GCI;

		if (acctSegId == null ) return retValue;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_1)) return Constants.GCI;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_2)) return Constants.GCI;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_3)) return Constants.ACS;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_4)) return Constants.GCI_PREPAID;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_5)) return Constants.ACS_PREPAID;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_10)) return Constants.DCN;

		return "";

	}

	/** Returns the Carrier Id based on the acct segment id
	 *
	 * @param acctSegId
	 * @return
	 */
	   public static String getInquiryCarrierIdByAcctSegId(Integer acctSegId)
	    {
	        String retValue = "GCI";
	        if(acctSegId == null) { return retValue; }
	        if(acctSegId.equals(Constants.ACCT_SEG_ID_1)) { return "GCI"; }
	        if(acctSegId.equals(Constants.ACCT_SEG_ID_2)) { return "GCI"; }
	        if(acctSegId.equals(Constants.ACCT_SEG_ID_3)) { return "ACS"; }
	        if(acctSegId.equals(Constants.ACCT_SEG_ID_4)) { return "GCI";}
	        if(acctSegId.equals(Constants.ACCT_SEG_ID_5)) { return "ACS";}
	        if(acctSegId.equals(Constants.ACCT_SEG_ID_10)) { return "DCN";}
	        else { return ""; }
	    }


	public static String getVoiceMailBoxNumberForGivenAcctSegment(
			ProvisioningRequestProperties reqProp) {

		String retValue= Constants.ACS_MAILBOX_NUMBER;
		Integer acctSegId = reqProp.getAcctSegId();
		if (acctSegId == null ) return retValue;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_1)) return Constants.GCI_MAILBOX_NUMBER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_2)) return Constants.GCI_MAILBOX_NUMBER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_3)) return Constants.ACS_MAILBOX_NUMBER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_4)) return Constants.GCI_MAILBOX_NUMBER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_5)) return Constants.ACS_MAILBOX_NUMBER;

		return "";

	}

	public static String getNoReplyTimerForGivenAcctSegment(
			ProvisioningRequestProperties reqProp) {

		String retValue= Constants.ACS_NO_REPLY_TIMER;
		Integer acctSegId = reqProp.getAcctSegId();
		if (acctSegId == null ) return retValue;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_1)) return Constants.GCI_NO_REPLY_TIMER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_2)) return Constants.GCI_NO_REPLY_TIMER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_3)) return Constants.ACS_NO_REPLY_TIMER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_4)) return Constants.GCI_NO_REPLY_TIMER;
		if (acctSegId.equals(Constants.ACCT_SEG_ID_5)) return Constants.ACS_NO_REPLY_TIMER;
		return "";

	}

	/**
	 *
	 * @param xml
	 * @param elementName
	 * @return XML element value
	 */
	public static synchronized String getXmlStringElementValue (String xml, String elementName) {

		if (xml == null || xml.length()==0) {
			return "";
		}


		String beginElement = "<" + elementName + ">";
		int start = xml.indexOf(beginElement);

		String endElement = "</" + elementName + ">";
		int end   = xml.indexOf(endElement);

		if (start == -1 || end == -1) {
			return null;
		}

		// Get description w/i the <description> tags
		start += (elementName.length()) + 2; // start of data = start element (+ 2 for '<' and '>' chars)
		String value = xml.substring(start, end);

		return value;

	}

	/** return the Date for the input string
	 *
	 * @param dateStr
	 * @return
	 * @throws ParseException
	 */
	public static Date getDateFromString(String dateStr) throws ParseException{

		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		Date a = (Date)formatter.parse(dateStr);
		return a;
	}

	/** Return the sql date for the input String
	 *
	 * @param dateStr
	 * @return
	 * @throws ParseException
	 */
	public static java.sql.Date getSQLDateFromString(String dateStr) throws ParseException{

		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		Date a = (Date)formatter.parse(dateStr);
		java.sql.Date sqlDate = new java.sql.Date(((Date)a).getTime());
		return sqlDate;
	}

	/** Return the OCS request format date for the input String
	 *
	 * @param dateStr
	 * @param NoofDays
	 * @param clientFormat
	 * @return
	 * @throws ParseException
	 */
    public static String getOCSDateFormatFromString(String dateStr, String NoofDays, boolean clientFormat)
            throws ParseException
        {
            String OCSDateString = null;
            SimpleDateFormat ocsResponseformat = new SimpleDateFormat();
            if(!clientFormat) {
                ocsResponseformat = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
            } else {
                ocsResponseformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            }
            SimpleDateFormat ocsRequestformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try
            {
                java.util.Date curDate = ocsResponseformat.parse(dateStr);
                log.info((new StringBuilder()).append("curDate = ").append(curDate).toString());
                Calendar cal = Calendar.getInstance();
                cal.setTime(curDate);
                if(Integer.valueOf(NoofDays).intValue() > 0) {
                    cal.add(5, Integer.valueOf(NoofDays).intValue());
                }
                OCSDateString = ocsRequestformat.format(cal.getTime());
                log.info((new StringBuilder()).append("OCSDateString = ").append(OCSDateString).toString());
            }
            catch(ParseException e) {
                e.printStackTrace();
                log.error("OCS Date Formatting Error : ");
            }
            return OCSDateString;
        }

	/** Convert the Util date to Sql Date
	 *
	 * @param inputDate
	 * @return
	 * @throws ParseException
	 */
	public static java.sql.Date getSQLDateFromUtilDate(Date inputDate) throws ParseException{
		if (inputDate != null)
		{
			java.sql.Date sqlDate = new java.sql.Date(((Date)inputDate).getTime());
			return sqlDate;
		}
		else
			return null;
	}

	/** Returns the proper XML tag for every request
	 *
	 * @param tagName
	 * @param value
	 * @param includeNull
	 * @param includeEmptyString
	 * @return
	 */
	public static String createXmlTag(String tagName, String value, boolean includeNull, boolean includeEmptyString) {
		boolean buildTag = true;
		String xmlStr = "";

		if (value == null && !includeNull) {
			buildTag = false;
		}
		else if ("".equals(value) && !includeEmptyString) {
			buildTag = false;
		}
		if (buildTag) {
			xmlStr = "<" + tagName + ">" + value + "</" + tagName + ">\n";
		}
		return xmlStr;
	}

	/** Enclosing the string in the <provisioning> tags.
	 * @param tagName
	 * @param value
	 * @return
	 */
	public static String encloseInProvisioningTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = "<provisioning>\n" + value + "\n</provisioning>";
		}
		return xmlStr;
	}

	/** This is used for all the wireless Inquiry
	 *
	 * @param value
	 * @return
	 */
	public static String encloseWirelessInquiryTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = "<wireless>\n<inquiry>\n" + value + "</inquiry>\n</wireless>";
		}
		return xmlStr;
	}


	/** This is used for all the OCS Inquiry
	 *
	 * @param value
	 * @return
	 */
	public static String encloseOCSWirelessInquiryTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.OCS_XML_HEADER + value + Constants.OCS_XML_FOOTER;
		}
		return xmlStr;
	}
	
	/** This is used for all the BlackHawk Inquiry
	 *
	 * @param value
	 * @return
	 */
	public static String encloseBlackHawkInquiryTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.BLACKHAWK_XML_HEADER + value + Constants.BLACKHAWK_XML_FOOTER;
		}
		return xmlStr;
	}


	/** This is used for all the OCS Inquiry
	 *
	 * @param value
	 * @return
	 */
	public static String encloseIncommInquiryTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.INCOMM_XML_HEADER + value + Constants.INCOMM_XML_FOOTER;
		}
		return xmlStr;
	}


	public static String encloseGCIMulePosTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.GCI_MULE_POS_XML_HEADER_TAG + value + Constants.GCI_MULE_POS_XML_FOOTER_TAG;
		}
		return xmlStr;

	}
	
	public static String encloseACSTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.PREPAID_ACS_XML_HEADER_TAG + value + Constants.PREPAID_ACS_XML_FOOTER_TAG;
		}
		return xmlStr;

	}


	/** This is used for all the GLOBECOMM Inquiry
	 *
	 * @param value
	 * @return
	 */
	public static String encloseGLOBECOMMEnvelopeTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.GLOBECOMM_XML_HEADER + value + Constants.GLOBECOMM_XML_FOOTER;
		}
		return xmlStr;
	}



	/** This is used for all the GLOBECOMM Inquiry
	 *
	 * @param value
	 * @return
	 */
	public static String encloseGLOBECOMMAddSubcriberTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.GLOBECOMM_ADD_SUBSCRIBER_START_ROOT_TAG + value + Constants.GLOBECOMM_ADD_SUBSCRIBER_END_ROOT_TAG;
		}
		return xmlStr;
	}



 /**
  *
  */
	public static String encloseOCSWirelessUpdateExpiryDateTags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = Constants.OCS_XML_HEADER + value + Constants.OCS_XML_FOOTER_EXPIRY_DATE;
		}
		return xmlStr;
	}

	/** This is used for all the OCS Inquiry
	 *
	 * @param value
	 * @return
	 */
	public static String encloseOCSInquiryTagsNoParam(String value) {
		boolean buildTag = true;
		String xmlStr = "";
		if (buildTag) {
			xmlStr = Constants.OCS_XML_HEADER + value + Constants.OCS_XML_FOOTER_NO_VALUES;
		}
		return xmlStr;
	}

	/** This is used for all the OCS Request
	 *
	 * @param value
	 * @return
	 */
	public static String encloseOCSRequestRootTags(String value, String headertags, String footertags) {
		String xmlStr = "";

		if (footertags == null) {
			xmlStr = "<methodCall>\n" + headertags +"\n" + value + "</methodCall>";
		}
		else {
			xmlStr = "<methodCall>\n" + headertags +"\n" + value +"\n" + footertags + "\n</methodCall>";
		}

		return xmlStr;
	}

	/** This is used for all the OCS Request which requires one set of data
	 *
	 * @param value
	 * @return
	 */
	public static String encloseOCSRequestRootlevel1Tags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = "<params><param>\n" + value + "</param></params>";
		}
		return xmlStr;
	}

	/** This is used for all the OCS Request which requires multiple set of data
	 *
	 * @param value
	 * @return
	 */
	public static String encloseOCSRequestRootlevel2Tags(String value) {
		boolean buildTag = true;
		String xmlStr = "";

		if (buildTag) {
			xmlStr = "<params><param><value><struct>\n" + value + "</struct></value></param></params>";
		}
		return xmlStr;
	}

	/** This is used for all the OCS Request which requires multiple set of same data
	 *
	 * @param value
	 * @return
	 */
	public static String encloseOCSRequestRootlevel3Tags(String value) {
		String xmlStr = "";

//		xmlStr = "<param><value><array><data><value><struct>\n" + value + "</struct></value></data></array></value></param>";
        xmlStr = "<data><value><struct>" + value +"</struct></value></data>";

		return xmlStr;
	}

	/** This is used for all the OCS Request which requires multiple set of same data
	 *
	 * @param value
	 * @return
	 */
	   public static String encloseOCSDataMultiplyTags(String value)
	    {
	        String xmlStr = "";

	        xmlStr = "<value><struct>"+value+"</struct></value>";

	        return xmlStr;
	    }

		/** This is used for all the OCS Request which requires multiple set of same data
		 *
		 * @param value
		 * @return
		 */
	    public static String wrapOCSDataMultiplyTags(String value)
	    {
	        String xmlStr = "";
	        xmlStr = "<param><value><array><data>" + value + "</data></array></value></param>";
	        return xmlStr;
	    }


		/** This is used for all the OCS Request which requires member set of data
		 *
		 * @param value
		 * @return
		 */

	    public static String encloseOCSRequestMemberTags(String name, String value, String dataType)
	    {
	        String xmlStr = "";
	        xmlStr = "\n<member>\n<name>"+ name +"</name>\n<value><" + dataType + ">" + value+ "</"+ dataType+ "></value>\n</member>";

	        return xmlStr;
	    }

		/** This is used for all the OCS Request which requires member set of data with NULL values
		 *
		 * @param value
		 * @return
		 */

	    public static String encloseOCSRequestMemberNullValueTags(String name, String value)
	    {
	        String xmlStr = "";
	        xmlStr = "\n<member>\n<name>"+ name +"</name>\n<value>"+ value+ "</value>\n</member>";

	        return xmlStr;
	    }

		/** This is used for all the OCS Request which requires param set of data
		 *
		 * @param value
		 * @return
		 */
	    public static String encloseOCSRequestParamTags(String value, String dataType, boolean includeNull)
	    {
	        String xmlStr = "";
	        if((value != null || "".equals(value)) && !includeNull)
	        {
	            xmlStr = "\n<param>\n<value><" + dataType + ">" + value + "</" + dataType + "></value>\n</param>";
	        } else
	        {
	            xmlStr = "\n<param>\n<value><" + dataType + "></" + dataType + "></value>\n</param>";
	        }
	        return xmlStr;
	    }


	/** This is used for all the OCS Request data name and value
	 *
	 * @param name
	 * @param value
	 * @param dataType
	 * @return
	 */
	public static String encloseOCSRequestValueTags(String name, String value, String dataType) {
		String xmlStr = "";

		xmlStr = "\n<member>\n<name><"+ name +"></name>\n<value><"+ dataType +">" + value +"</"+ dataType +"></value>\n</member>";

		return xmlStr;
	}

	/** This is used for all the OCS Value Request Tags
	 *
	 * @param value
	 * @param dataType
	 * @return
	 */
	public static String encloseOCSRequestParamTags(String value, String dataType) {
		String xmlStr = "";

		xmlStr = "\n<param>\n<value><"+ dataType +">" + value +"</"+ dataType +"></value>\n</param>";

		return xmlStr;
	}

	/**  To return this common string for all the Provision Requests.
	 * <serviceType>GSM</serviceType>
		<systemId>Cycle30</systemId>
		<idRequest>GCI.11_1</idRequest>
	 *
	 * @param reqProp
	 * @return
	 */
	public static String getGSMCarriIDsystemIdDetails(ProvisioningRequestProperties reqProp) {
		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_SERVICE_TYPE, Constants.GSM, false, false);
		reqXml = reqXml + getCarriIDsystemIdDetails(reqProp);

		return reqXml;
	}

	/**  To return this common string for all the Provision Requests.
	 *
		<systemId>Cycle30</systemId>
		<idRequest>GCI.11_1</idRequest>
	 *
	 * @param reqProp
	 * @return
	 */
	public static String getCarriIDsystemIdDetails(ProvisioningRequestProperties reqProp) {
		String reqXml = "";

		if (reqProp.getAuthClient().getProvCallbackSystemId() != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_SYSTEM_ID, reqProp.getAuthClient().getProvCallbackSystemId(), false, false);
		else
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_SYSTEM_ID, Constants.SOURCE_SYSTEM_ID, false, false);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ID_REQUEST, reqProp.getSubRequestId(), false, false);

		return reqXml;
	}

	/** Return as if we need to simulate the request or not for the given telephone number.
	 *
	 * @param env
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public static boolean isSimulatationRequiredForGivenExternalId(String telephoneNumber, C30JDBCDataSource datasource) throws C30ProvConnectorException  {

		log.info("Fetching all the authentication data from database");
		String query = SQLConstants.PROV_QUERY_IS_SIMULATE_TELEPHONE_NUMBER;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		boolean retValue  = false;

		try
		{
			ps = datasource.getPreparedStatement(query);
			ps.setString(1, telephoneNumber);

			resultSet  = ps.executeQuery();

			if (resultSet.next())
			{
				int count = resultSet.getInt("COUNT");

				if (count == 0) retValue = false; else retValue = true;
			}//END if

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-011");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}
		return retValue;
	}


	/** Check the response and figure if the simulation is Success or not
	 *
	 * @param response
	 * @throws C30ProvConnectorException
	 */
	public static boolean checkIfRequestisSuccess(String responseXml) throws C30ProvConnectorException {

		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-012");
		}

		/** Error Response ...
		 * <?xml version="1.0" ?>
			<provisioning>
			  <wirelessVoice>
			    <error>
			      <message>Success</message>
			      <number>0</number>
			    </error>
			    <iccid>89013113708000000314</iccid>
			    <idrequest>ACS.26_1</idrequest>
			  </wirelessVoice>
			</provisioning>
		 */
		// See if error occurred.
		String errorNo = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_NUMBER);
		if (errorNo == null || errorNo.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-012");
		}

		if (errorNo.equals("0")==true) {
			return true;
		}

		return false;
	}
	/** Update the status of the request and response
	 *
	 * @param request
	 * @param statusRequestSimulateCompleted
	 * @param response
	 * @throws C30ProvConnectorException
	 */
	//PW-65
	public void updateStatusAndResponseXMLForSubRequestTableForOptimization(IProvisioningRequest request,
			Integer statusId, String response, C30JDBCDataSource datasource,String subrequestId) throws C30ProvConnectorException {

		String query = SQLConstants.PROV_QUERY_UPDATE_RESPONSE_TRANS_INST;
		ResultSet resultSet = null;
		PreparedStatement ps = null;

		try
		{
			log.info("Updating the reponse and statusid in the prov_trans_inst table");
			ps = datasource.getPreparedStatement(query);
			ps.setInt(1,statusId);
			ps.setString(2, response);
			ps.setString(3, subrequestId);
			log.debug("Query = " + query +"statusId == "+statusId+"subrequestId = " +subrequestId);
			resultSet = ps.executeQuery();

		} catch(Exception e) {
			throw new C30ProvConnectorException(e, "PROV-011");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("Exception PROV TRAN INST: "+ex.getMessage());
			}
		}

		//CATALOG Status
		try{
			if (statusId.equals(Constants.STATUS_REQUEST_IN_ERROR))
			{
				query = SQLConstants.PROV_UPDATE_PROV_CATALOG_STATUS_TO_ERROR;
				ps = datasource.getPreparedStatement(query);
				ps.setInt(1,request.getRequestProperties().getRequestId());
			}
			else
			{
				query = SQLConstants.PROV_UPDATE_PROV_CATALOG_STATUS;
				ps = datasource.getPreparedStatement(query);
				ps.setString(1,subrequestId);
				ps.setString(2,subrequestId);
			}

			log.debug("Query = " + query +" RequestId = "+ request.getRequestProperties().getRequestId() +", and subrequestId = " +
					request.getRequestProperties().getSubRequestId()+"passed subrequest id= "+subrequestId);

			resultSet = ps.executeQuery();

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-011");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("Exception CATALOG: "+ex.getMessage());
			}
		}
	}
	/** Update the status of the request and response
	 *
	 * @param request
	 * @param statusRequestSimulateCompleted
	 * @param response
	 * @throws C30ProvConnectorException
	 */
	public static void updateStatusAndResponseXMLForSubRequestTable(IProvisioningRequest request,
			Integer statusId, String response, C30JDBCDataSource datasource) throws C30ProvConnectorException {

		String query = SQLConstants.PROV_QUERY_UPDATE_RESPONSE_TRANS_INST;
		ResultSet resultSet = null;
		PreparedStatement ps = null;

		try
		{
			log.info("Updating the reponse and statusid in the prov_trans_inst table.");
			ps = datasource.getPreparedStatement(query);
			ps.setInt(1,statusId);
			ps.setString(2, response);
			ps.setString(3, request.getRequestProperties().getSubRequestId());

			resultSet = ps.executeQuery();

		} catch(Exception e) {
			throw new C30ProvConnectorException(e, "PROV-011");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("Exception PROV TRAN INST: "+ex.getMessage());
			}
		}

		//CATALOG Status
		try{
			if (statusId.equals(Constants.STATUS_REQUEST_IN_ERROR))
			{
				query = SQLConstants.PROV_UPDATE_PROV_CATALOG_STATUS_TO_ERROR;
				ps = datasource.getPreparedStatement(query);
				ps.setInt(1,request.getRequestProperties().getRequestId());
			}
			else
			{
				query = SQLConstants.PROV_UPDATE_PROV_CATALOG_STATUS;
				ps = datasource.getPreparedStatement(query);
				ps.setString(1,request.getRequestProperties().getSubRequestId());
				ps.setString(2, request.getRequestProperties().getSubRequestId());
			}

			log.debug("Query = " + query +" RequestId = "+ request.getRequestProperties().getRequestId() +", and subrequestId = " +
					request.getRequestProperties().getSubRequestId());

			resultSet = ps.executeQuery();

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-011");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("Exception CATALOG: "+ex.getMessage());
			}
		}
	}

	/** Update the status of Request
	 *
	 * @param request
	 * @param statusId
	 * @param response
	 * @throws C30ProvConnectorException
	 */
	public static void updateStatusOfRequestTable(IProvisioningRequest request,
			Integer statusId, C30JDBCDataSource datasource ) throws C30ProvConnectorException {
		log.debug("Updating the reponse and statusid in the prov_trans_inst table.");
		String query = SQLConstants.PROV_QUERY_PROV_CATALOG_TRANS_STATUS;
		log.debug("Query : "+ query + " , status_id = " +statusId + ", RequestId = " + request.getRequestProperties().getRequestId());
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try
		{
			ps = datasource.getPreparedStatement(query);
			ps.setInt(1,statusId);
			ps.setInt(2, request.getRequestProperties().getRequestId());

			resultSet = ps.executeQuery();
		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-011");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("updateStatusOfRequestTable - Exception: "+ex.getMessage());
			}
		}
	}



	/** Update the status of Request with response and for the given Sub Request ID
	 *
	 * @param request
	 * @param statusId
	 * @param response
	 * @throws C30ProvConnectorException
	 */
	public static void updateStatusAndResponseXMLForRequestTable(IProvisioningRequest request,
			Integer statusId,String response, C30JDBCDataSource datasource ) throws C30ProvConnectorException {
		log.debug("Updating the reponse and statusid in the prov_trans_inst table.");
		String query = SQLConstants.PROV_QUERY_UPDATE_RESPONSE_TRANS_INST_SYNC;
		log.debug("Query : "+ query + " , status_id = " +statusId +  "Response = " +response + ",  Sub RequestId = " + request.getRequestProperties().getSubRequestId());

		ResultSet resultSet = null;
		PreparedStatement ps = null;

		try
		{
			ps = datasource.getPreparedStatement(query);
			ps.setInt(1,statusId);
			ps.setString(2,response);
			ps.setString(3, request.getRequestProperties().getSubRequestId());

			resultSet = ps.executeQuery();
		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-011");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("updateStatusAndResponseXMLForRequestTable - Exception: "+ex.getMessage());
			}
		}
	}
	
	/**
	 * PW-65 Optimization in SPS calls .
	 * BIL has implemented threading concept to send DATA,MAIL and Text calls 
	 * simultaneously.Observed that some calls are not updated in DB for Simulate false
	 * so, duplicating the below method for data,mail and text instead of doing synchronized.
	 * If we synchronize again thread will lock and again time conuming so not using synchronized.
	 */
	//pw-65
	public void updateResponseIfTheRequestIsSuccessInSentToSPSForOptimization(String responseXml,  IProvisioningRequest request, C30JDBCDataSource datasource,String subrequestId) throws C30ProvConnectorException
	{
		//If the request is for simulation then directly update the request tables.
		
		if ((Boolean) request.getRequestProperties().getRequestMap().get(Constants.ATTR_SIMULATE_REQUEST))
		{
			//For Simulation
			if (checkIfRequestisSuccess(responseXml))
			{
				// Update the status  and responseXML
				updateStatusAndResponseXMLForSubRequestTableForOptimization(request, Constants.STATUS_REQUEST_SIMULATE_COMPLETED, responseXml, datasource,subrequestId);
			}
			else
			{
				// Update the status  and responseXML
				//C30ProvConnectorUtils.updateStatusAndResponseXML(request, Constants.STATUS_REQUEST_SIMULATE_IN_ERROR, response);
				updateStatusAndResponseXMLForSubRequestTableForOptimization(request, Constants.STATUS_REQUEST_SIMULATE_COMPLETED, responseXml, datasource,subrequestId);
			}
		}
		else
		{
			//Actual Request sent to SPS..
			if (checkIfRequestisSuccess(responseXml))
			{
				// Update the status  and responseXML
				updateStatusAndResponseXMLForSubRequestTableForOptimization(request, Constants.STATUS_REQUEST_SYNC_RESPONSE_RECIEVED, responseXml, datasource,subrequestId);
			}
			else
			{
				// Update the status  and responseXML
				//C30ProvConnectorUtils.updateStatusAndResponseXML(request, Constants.STATUS_REQUEST_SIMULATE_IN_ERROR, response);
				updateStatusAndResponseXMLForSubRequestTableForOptimization(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource,subrequestId);

				String error = getErrorMsgFromResponseXML(responseXml);
				// Throw Exception here .. as the request is failed.
				throw new C30ProvConnectorException(error, "PROV-014");
			}
		}

	}



	/** This will udpate if the input request is simulate the update the reponse directly
	 *
	 * @param responseXml
	 * @param request
	 * @throws C30ProvConnectorException
	 */
	public static void updateResponseIfTheRequestIsSuccessInSentToSPS(String responseXml,  IProvisioningRequest request, C30JDBCDataSource datasource) throws C30ProvConnectorException
	{
		//If the request is for simulation then directly update the request tables.

		if ((Boolean) request.getRequestProperties().getRequestMap().get(Constants.ATTR_SIMULATE_REQUEST))
		{
			//For Simulation
			if (checkIfRequestisSuccess(responseXml))
			{
				// Update the status  and responseXML
				updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_SIMULATE_COMPLETED, responseXml, datasource);
			}
			else
			{
				// Update the status  and responseXML
				//C30ProvConnectorUtils.updateStatusAndResponseXML(request, Constants.STATUS_REQUEST_SIMULATE_IN_ERROR, response);
				updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_SIMULATE_COMPLETED, responseXml, datasource);
			}
		}
		else
		{
			//Actual Request sent to SPS..
			if (checkIfRequestisSuccess(responseXml))
			{
				// Update the status  and responseXML
				updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_SYNC_RESPONSE_RECIEVED, responseXml, datasource);
			}
			else
			{
				// Update the status  and responseXML
				//C30ProvConnectorUtils.updateStatusAndResponseXML(request, Constants.STATUS_REQUEST_SIMULATE_IN_ERROR, response);
				updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);

				String error = getErrorMsgFromResponseXML(responseXml);
				// Throw Exception here .. as the request is failed.
				throw new C30ProvConnectorException(error, "PROV-014");
			}
		}

	}

	/** Retrieveing the Error Message from the XML
	 *
	 * @param responseXml
	 * @return
	 */
	private static String getErrorMsgFromResponseXML(String responseXml) {
		String errorMsg ="";
		// See if error occurred.
		String errorNo = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_NUMBER);

		// non-zero return is an error
		if (errorNo.equals("0")==false) {
			errorMsg = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_MESSAGE);
		}
		return errorMsg;
	}


	/** Get status of SubOrder Process Id
	 *
	 * @param subRequestId
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public static Integer getStatusofSubProcessId( String subRequestId, C30JDBCDataSource datasource) throws C30ProvConnectorException {
		log.info("Get statusId for the given subRequestId");
		String query = SQLConstants.PROV_QUERY_GET_SUB_PROCESS_STATUS;

		PreparedStatement ps = null;
		ResultSet resultSet  = null;
		Integer statusId = null;

		try
		{
			ps = datasource.getPreparedStatement(query);
			ps.setString(1,subRequestId);

			resultSet = ps.executeQuery();

			if (resultSet.next())
			{
				statusId = resultSet.getInt("STATUS_ID");

			}//END if
			log.debug("Query : "+ query + " - SubReqId = "+ subRequestId +" - StatusId = "+ statusId);
		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-013");
		}
		finally {
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("getStatusofSubProcessId - Exception: "+ex.getMessage());
			}
		}
		return statusId;

	}
	
	
	
	/** Get Request XML for the given RequestId
	 *
	 * @param RequestId
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public static String getRequestXMLForBlackHawkReversalRequestId( String requestId, C30JDBCDataSource datasource) throws C30ProvConnectorException {
		log.info("Get Request XML for the given RequestId");
		String query = SQLConstants.PROV_QUERY_GET_REQUEST_XML;

		PreparedStatement ps = null;
		ResultSet resultSet  = null;
		String requestXML = null;

		try
		{
			ps = datasource.getPreparedStatement(query);
			ps.setString(1,requestId);

			resultSet = ps.executeQuery();

			if (resultSet.next())
			{
				Clob clob = resultSet.getClob("REQUEST_XML");
				   if (clob != null) {  
					   requestXML = clob.getSubString(1, (int) clob.length());
				   }

			}//END if
			log.debug("Query : "+ query + " - ReqId = "+ requestId +" - REQUEST_XML = "+ requestXML);
		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-013");
		}
		finally {
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("getRequestXMLForBlackHawkReversalRequestId - Exception: "+ex.getMessage());
			}
		}
		//requestXML.replace("\"", "");
		return requestXML;

	}



	/** Get status of Request Id
	 *
	 * @param requestId
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public static Integer getStatusofRequestId( String requestId, C30JDBCDataSource datasource) throws C30ProvConnectorException {
		log.info("Get statusId for the given RequestId");
		String query = SQLConstants.PROV_QUERY_GET_REQUEST_STATUS;

		PreparedStatement ps = null;
		ResultSet resultSet = null;
		Integer statusId = null;

		try
		{
			ps = datasource.getPreparedStatement(query);
			ps.setString(1,requestId);

			resultSet  = ps.executeQuery();

			if (resultSet.next())
			{
				statusId = resultSet.getInt("STATUS_ID");

			}//END if
			log.debug("Query : "+ query + " - RequestId = "+ requestId +" - StatusId = "+ statusId);
		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-013");
		}
		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			}
			catch (Exception ex){
				log.error("getStatusofRequestId - Exception: "+ex.getMessage());
			}
		}
		return statusId;

	}


	/** Returns the Simulate TRUE/FALSE based on the input request
	 *
	 * @param reqProp
	 * @return
	 */
	public static String simulateRequest(ProvisioningRequestProperties reqProp) {

		if (reqProp.getRequestMap().get(Constants.ATTR_SIMULATE_REQUEST) != null && ((Boolean)reqProp.getRequestMap().get(Constants.ATTR_SIMULATE_REQUEST)).booleanValue() )
			return "<simulate>"+reqProp.getRequestMap().get(Constants.ATTR_SIMULATE_REQUEST)+"</simulate>\n";

		return "";
	}

	/** Use input XML to get xPath
	 *
	 * @param inputXML
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public static String getXmlTagValueFromXPath(String inputXML, String xPath) throws C30ProvConnectorException
	{
		Document respXmlDOM;
		String retString ="";
		try {
			respXmlDOM = C30DOMUtils.stringToDom(inputXML);


			XPathFactory xpf = XPathFactory.newInstance();
			XPath xp = xpf.newXPath();
			retString = xp.evaluate(xPath, //"//wireless/inquiry/method/services/data/service/data",
					respXmlDOM.getDocumentElement());
		}
		catch (Exception e) {
			throw new C30ProvConnectorException(e, "PROV-016");
		}
		return retString;
	}

	public static Document getDocument(String xmlString)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlString));
        return docBuilder.parse(is);
    }

	/** Use input XML and Member Name to get Member Values
	 *
	 * @param inputXML
	 * @return
	 * @throws C30ProvConnectorException
	 */
   public static String getOCSMemberValuefromResponse(String inputXML, String memberName)
	        throws ParserConfigurationException, SAXException, IOException
	    {
	        String returnValue = null;
	        Document doc = getDocument(inputXML);
	        doc.getDocumentElement().normalize();
	        NodeList structList = doc.getElementsByTagName("struct");
	        for(int i = 0; i < structList.getLength(); i++)
	        {
	            NodeList memberList = ((Element)structList.item(i)).getElementsByTagName("member");
	            for(int j = 0; j < memberList.getLength(); j++)
	            {
	                String Name = ((Element)memberList.item(j)).getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue();
	                String Value = ((Element)memberList.item(j)).getElementsByTagName("value").item(0).getChildNodes().item(0).getTextContent();
	                if(Name.equalsIgnoreCase(memberName))
	                {
	                    returnValue = Value;
	                }
	            }

	        }

	        return returnValue;
	    }


	/** Use input XML to get BalanceTypes
	 *
	 * @param inputXML
	 * @return
	 * @throws C30ProvConnectorException
	 */
   public static List<String> getProfileBalancesofSubscriber(String inputXML)
	        throws ParserConfigurationException, SAXException, IOException
	    {
	        String memberName = "BalanceTypeLabel";
	        Document doc = getDocument(inputXML);
	        log.info((new StringBuilder()).append("getProfileBalancesofSubscriber-inputXML: ").append(inputXML).toString());
	        doc.getDocumentElement().normalize();
	        NodeList structList = doc.getElementsByTagName("struct");
	        List<String> balanceType = new ArrayList<String>();
	        try
	        {
	            for(int i = 0; i < structList.getLength(); i++)
	            {
	                NodeList memberList = ((Element)structList.item(i)).getElementsByTagName("member");
	                for(int j = 0; j < memberList.getLength(); j++)
	                {
	                    String Name = ((Element)memberList.item(j)).getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue();
	                    String Value = ((Element)memberList.item(j)).getElementsByTagName("value").item(0).getChildNodes().item(0).getTextContent();
	                    if(Name.equalsIgnoreCase(memberName))
	                    {
	                        balanceType.add(Value);
	                    }
	                }

	            }

	            log.info("getProfileBalancesofSubscriber-balanceType: " + balanceType.toString());
	        }
	        catch(Exception e)
	        {
	            e.printStackTrace();
	        }
	        return balanceType;
	    }

	/** Use input XML to Subscriber Profile BalanceTypes
	 *
	 * @param inputXML
	 * @return
	 * @throws C30ProvConnectorException
	 */
   public static String getBalancesofSubscriberProfile(String inputXML, List<String> balanceType)
	        throws ParserConfigurationException, SAXException, IOException
	    {
	        String balTypePayload = "";
	        String subscriberbal = "";
	        try
	        {
	            Document doc = getDocument(inputXML);
	            doc.getDocumentElement().normalize();
	            NodeList structList = doc.getElementsByTagName(Constants.OCS_XML_TAG_STRUCT);
		        List<String> profileBalanceType = new ArrayList<String>();

	            for(int i = 0; i < structList.getLength(); i++)
	            {
	                NodeList memberList = ((Element)structList.item(i)).getElementsByTagName(Constants.OCS_XML_TAG_MEMBER);
	                for(int j = 0; j < memberList.getLength(); j++)
	                {
	                    String Name = ((Element)memberList.item(j)).getElementsByTagName(Constants.OCS_XML_TAG_NAME).item(0).getChildNodes().item(0).getNodeValue();
	                    String Value = ((Element)memberList.item(j)).getElementsByTagName(Constants.OCS_XML_TAG_VALUE).item(0).getChildNodes().item(0).getTextContent();
	                    if(Name.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_LABEL))
	                    {
	                        profileBalanceType.add(Value);
	                    }
	                }

	            }

	    		for (String balance : balanceType) {
	    			//Exclude Balance "COMMON" and "GCI_DATA_PKG" (Addon Package)
	                if(!profileBalanceType.contains(balance) && !balance.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON)
	                		&& !balance.contains(Constants.PREPAID_ADD_ON_PKG_BALANCE)) {
	                		log.info("balance = "+balance);
	    					balTypePayload = balTypePayload + "<value><string>"+balance+"</string></value>";
	    					subscriberbal = balance;
	    	        }
	    	    }
	        }
	        catch(Exception e)
	        {
	            e.printStackTrace();
	        }

	        if (subscriberbal == "" || subscriberbal.isEmpty() || subscriberbal == null) {
	        	balTypePayload = "";
	        }

	        return balTypePayload.toString();
	    }


	/** Use input XML to get Profile BalanceTypes
	 *
	 * @param inputXML
	 * @return
	 * @throws C30ProvConnectorException
	 */

   public static String getSubsciberProfileBalanceTypes(String inputXML, HashMap requestHashMap)
	        throws ParserConfigurationException, SAXException, IOException, C30ProvConnectorException
	    {
	        String balTypePayload = "";
	        String ExpirationDate = null;
	        String BalExpirationDate = null;
	        String balName = null;
	        String balLabel = null;
	        String balValue = null;
	        String prepaidAction = null;
	        Document doc = getDocument(inputXML);
	        doc.getDocumentElement().normalize();
	        NodeList structList = doc.getElementsByTagName(Constants.OCS_XML_TAG_STRUCT);
	        StringBuffer finalbalTypePayload = new StringBuffer();
	        try
	        {
				log.info("getSubsciberProfileBalanceTypes = "+requestHashMap.toString());
	            //Get Plan Expiration Date
	    		if (requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE)) {
					ExpirationDate = getOCSDateFormatFromString(requestHashMap.get(Constants.PREPAID_EXPIRATIONDATE).toString(), "0", true);
	    		} else {
		            Calendar cal = Calendar.getInstance();
		            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 23, 59, 59);
		            java.util.Date CalExpirationDate = cal.getTime();
		            DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
		            ExpirationDate = df.format(CalExpirationDate).toString();
		            ExpirationDate = getOCSDateFormatFromString(ExpirationDate, "30", false);
	    		}

				//Get Balance Expiration Date
		  		if (requestHashMap.containsKey(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE)) {
		  			try {
				  			BalExpirationDate = C30ProvConnectorUtils.getOCSDateFormatFromString(requestHashMap.get(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE).toString(),
				  					"0", true);
					} catch (ParseException e) {
						e.printStackTrace();
				    	log.error("getCOMMONBalance-OCS Date Formatting Error : "+e);
						throw new C30ProvConnectorException(e, e.getMessage());
				    }
		  		}

				// Build Balance Type.
				if (requestHashMap.containsKey(Constants.ATTR_PREAPID_ACTION)) {
					prepaidAction = requestHashMap.get(Constants.ATTR_PREAPID_ACTION).toString();
				}

				// Build Balance Type.
				if (requestHashMap.containsKey(Constants.PREPAID_BALANCE_TYPE_LABEL)) {
					balLabel = requestHashMap.get(Constants.PREPAID_BALANCE_TYPE_LABEL).toString();
				}
				//Build Wallet Value
				if (requestHashMap.containsKey(Constants.PREPAID_VALUE)) {
					//If the value is 10099, then the value should be $100.99
					String Value = requestHashMap.get(Constants.PREPAID_VALUE).toString();

					if (balLabel.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON)) {
						if (Integer.valueOf(Value) > 0) {
							//balValue = new StringBuffer(Value).insert(Value.length()-2, ".").toString();
							//Made the code Generic to Handle any cents.
							 balValue= new Double( ((Double.parseDouble(Value)/100))).toString();
							
						} else {
							balValue = "0.00";
						}
					}
				}

	            for(int i = 0; i < structList.getLength() && i <= 0; i++)
	            {
	                NodeList memberList = ((Element)structList.item(i)).getElementsByTagName(Constants.OCS_XML_TAG_MEMBER);
	                for(int j = 0; j < memberList.getLength(); j++)
	                {
	                    String Name = ((Element)memberList.item(j)).getElementsByTagName(Constants.OCS_XML_TAG_NAME).item(0).getChildNodes().item(0).getNodeValue();
	                    String Value = ((Element)memberList.item(j)).getElementsByTagName(Constants.OCS_XML_TAG_VALUE).item(0).getChildNodes().item(0).getTextContent();

                        //Get the Balance Type
	                    if(Name.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_LABEL)) {
	                        balTypePayload = "";
	                        balName = Value;

	                        //Do NOT Add COMMON Balance for SWAP Plan and Extend Expiry Date and Plan Recharge
	                        if (!(prepaidAction != null &&
	                        		(prepaidAction.equalsIgnoreCase(Constants.PREPAID_PLAN_CHANGE) ||
	                        		prepaidAction.equalsIgnoreCase(Constants.PREPAID_EXTEND_EXPIRYDATE) ||
	                        		prepaidAction.equalsIgnoreCase(Constants.PREPAID_PLAN_RECHARGE_RESERVE)) &&
	                        		balName.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON))) {
		                        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Name, balName, Constants.OCS_XML_TAG_STRING);

		                        //for Balance Type COMMON, BalanceExpiry date to be populated
		                        if (Value.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON) && BalExpirationDate != null) {
			                        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, BalExpirationDate,
			                        		Constants.OCS_XML_TAG_STRING);
			                        balTypePayload = balTypePayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, balValue,
			                        		Constants.OCS_XML_TAG_DOUBLE);
		                        } else {
			                        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, ExpirationDate,
			                        		Constants.OCS_XML_TAG_STRING);
		                        }
	                        }
	                    }

                            //Following comment and if statement created by Umesh and applied by Jesse Harris 2/2/2014
                            //DO NOT build "Value" element of balances for Extend Expiry Date
			    // ** commented out as it is caused new errors
			    //if (!prepaidAction.equalsIgnoreCase(Constants.PREPAID_EXTEND_EXPIRYDATE)) {

	                	//Get the Value of the BalanceType
	                	if(Name.equalsIgnoreCase(Constants.PREPAID_BALANCE_INITIAL_VALUE)) {
		            	    //Do NOT Add COMMON Balance for SWAP Plan
	                	    if (!(balName.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON))) {
    		        	        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, Value, Constants.OCS_XML_TAG_DOUBLE);
	                	     }

	                             if (balTypePayload != "") {
		                        balTypePayload = encloseOCSDataMultiplyTags(balTypePayload);
		           	        finalbalTypePayload.append(balTypePayload);
	                	     }
	                	}
		            //}
	                }
	            }
	        }
	        catch(ParseException e)
	        {
	            e.printStackTrace();
	            throw new C30ProvConnectorException(e, e.getMessage());
	        }

	        return finalbalTypePayload.toString();
	    }


   /** Use OCSresponse
	 *
	 * @param OCSresponse
	 * @param faultCode
	 * @param faultString
	 * @return
	 * @throws C30ProvConnectorException
	 */

	public static HashMap<Integer, HashMap<String, String>> parseOCSFaultResponse(String OCSresponse,
			String faultCode , String faultString) throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(OCSresponse);
		doc.getDocumentElement().normalize();
		NodeList structList = doc.getElementsByTagName("struct");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> balanceResultMap = null;
		for (int i = 0; i < structList.getLength(); i++) {

			NodeList memberList = ((Element) structList.item(i))
					.getElementsByTagName("member");
			balanceResultMap = new HashMap<String, String>();
			for (int j = 0; j < memberList.getLength(); j++) {

				String Name = (String) ((Element) memberList.item(j))
						.getElementsByTagName("name").item(0)
						.getChildNodes().item(0).getNodeValue();
				String Value = (String) ((Element) memberList.item(j))
						.getElementsByTagName("value").item(0)
						.getChildNodes().item(0).getTextContent();

				if (faultCode.equalsIgnoreCase(Name)) {
					balanceResultMap.put(Name, Value);
			    	}
				if (faultString.equalsIgnoreCase(Name)) {
					balanceResultMap.put(Name, Value);
			       }
			bundleResult.put(i, balanceResultMap);
		  }
		}
		return bundleResult;


	}

	public static HashMap<Integer, HashMap<String, String>> parseGlobeCommFaultResponse(String GlobecommResponse) throws ParserConfigurationException, SAXException, IOException {

			Document doc = getDocument(GlobecommResponse);
			NodeList structList = doc
					.getElementsByTagName("soapenv:Envelope");
			HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
			HashMap<String, String> globeCommMap = null;
			for (int i = 0; i < structList.getLength(); i++) {

				globeCommMap = new HashMap<String,String>();

				String RespCode = "";
				String RespMsg = "";

				if (((Element) structList.item(i)).getElementsByTagName("ax22:responseCode")
						.item(0).getChildNodes().item(0) != null) {
					RespCode = (String) ((Element) structList.item(i))
							.getElementsByTagName("ax22:responseCode").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				globeCommMap.put("RespCode", RespCode);

				if (((Element) structList.item(i)).getElementsByTagName("ax22:responseDesc")
						.item(0).getChildNodes().item(0) != null) {
					RespMsg = (String) ((Element) structList.item(i))
							.getElementsByTagName("ax22:responseDesc").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				globeCommMap.put("RespMsg", RespMsg);


				bundleResult.put(0, globeCommMap);

			}
				return bundleResult;

		}


	public static HashMap<Integer, HashMap<String, String>> parseIncommFaultResponse(String IncommResponse) throws ParserConfigurationException, SAXException, IOException {

		Document doc = getDocument(IncommResponse);
		NodeList structList = doc
				.getElementsByTagName("TransferredValueTxnResp");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> incomrspMap = null;
		for (int i = 0; i < structList.getLength(); i++) {

			incomrspMap = new HashMap<String,String>();

			String RespCode = "";
			String RespMsg = "";
			String faceValue="";

			if (((Element) structList.item(i)).getElementsByTagName("RespCode")
					.item(0).getChildNodes().item(0) != null) {
				RespCode = (String) ((Element) structList.item(i))
						.getElementsByTagName("RespCode").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			incomrspMap.put("RespCode", RespCode);

			if (((Element) structList.item(i)).getElementsByTagName("RespMsg")
					.item(0).getChildNodes().item(0) != null) {
				RespMsg = (String) ((Element) structList.item(i))
						.getElementsByTagName("RespMsg").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			incomrspMap.put("RespMsg", RespMsg);

			if (((Element) structList.item(i)).getElementsByTagName("FaceValue")
					.item(0).getChildNodes().item(0) != null) {
				faceValue = (String) ((Element) structList.item(i))
						.getElementsByTagName("FaceValue").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			incomrspMap.put("FaceValue", faceValue);


			bundleResult.put(0, incomrspMap);

		}
			return bundleResult;

	}
	
	
	
public static HashMap<Integer, HashMap<String, String>> parseBlackHawkFaultResponse(String BlackHawkResponse) throws ParserConfigurationException, SAXException, IOException {
		
		Document doc = getDocument(BlackHawkResponse);
		NodeList structList = doc
				.getElementsByTagName("transaction");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> blcakhawkrspMap = null;
		
		if(structList !=null){
			if(structList.getLength() == 0 ){
				log.info("Invalid Request data to Blackhawk system - please check the request XML");
				throw new ParserConfigurationException("Invalid Request data to Blackhawk system - please check the request XML");
			}
		}
		for (int i = 0; i < structList.getLength(); i++) {

			blcakhawkrspMap = new HashMap<String,String>();

			String RespCode = "";
			String RespMsg = "";
			String faceValue="0";
				  
			if (((Element) structList.item(i)).getElementsByTagName("responseCode")
					.item(0).getChildNodes().item(0) != null) {
				RespCode = (String) ((Element) structList.item(i))
						.getElementsByTagName("responseCode").item(0)
						.getChildNodes().item(0).getNodeValue();
			}
			
			if (((Element) structList.item(i)).getElementsByTagName("balanceAmount")
					.item(0).getChildNodes().item(0) != null) {
				String  faceValue1= (String) ((Element) structList.item(i))
						.getElementsByTagName("balanceAmount").item(0)
						.getChildNodes().item(0).getNodeValue();
				 faceValue=(faceValue1.replace("C", ""));
			}
			
			RespMsg = C30ProvConnectorUtils.getBlackHawErrorMessageForGivenCode(RespCode);
			blcakhawkrspMap.put("RespCode", RespCode);		
			blcakhawkrspMap.put("RespMsg", RespMsg);
		
			/*Locale locale = new Locale("en", "US");
			NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
			fmt.setMaximumFractionDigits(2);			
			blcakhawkrspMap.put("FaceValue", fmt.format(new Double(faceValue)));*/
			blcakhawkrspMap.put("FaceValue", faceValue.trim());
		
			
			bundleResult.put(0, blcakhawkrspMap);

		}
			return bundleResult; 
			 
	}



   /** Use OCSresponse
	 *
	 * @param responseXml
	 *
	 * @return
	 * @throws C30ProvConnectorException
	*/
    public static HashMap populateOCSResponse(String responseXml)
            throws C30ProvConnectorException {

    	if(responseXml == null || responseXml.length() == 0) {
                throw new C30ProvConnectorException("OCS-SUB-001");
        }

    	String errorNo = null;
        String errorMsg = null;
        HashMap response = new HashMap();

        if(responseXml != null) {
            response.put("ResponseXML", responseXml);
            try
            {
                HashMap errordetails = parseOCSFaultResponse(responseXml, "faultCode", "faultString");
                if(errordetails != null && errordetails.get(Integer.valueOf(0)) != null)
                {
                    HashMap stackDetails = (HashMap)errordetails.get(Integer.valueOf(0));
                    if(stackDetails.get("faultCode") != null && stackDetails.get("faultString") != null)
                    {
                        errorNo = (new StringBuilder()).append("OCS-").append(stackDetails.get("faultCode").toString()).toString();
                        errorMsg = stackDetails.get("faultString").toString();
                    }
                }
            }
            catch(ParserConfigurationException e)
            {
                e.printStackTrace();
            }
            catch(SAXException e)
            {
                e.printStackTrace();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }

        if((errorNo != null))
        {
        	if (errorNo.equalsIgnoreCase("OCS-159")) {
               	response.put("ResponseCode", "OCS-159");
        	} else {
        		throw new C30ProvConnectorException("Error Message from OCS: " + errorMsg, errorNo);
        	}
        } else
        {
           	response.put("ResponseCode", "0");
        }
        return response;
    }

	public static HashMap<Integer, HashMap<String, String>> parseGCIFaultResponse(
			String GCIresponse, String Code, String Message)
			throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(GCIresponse);
		doc.getDocumentElement().normalize();
		NodeList statList = doc.getElementsByTagName("StatusList");
		

		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> errList = null;

		for (int i = 0; i < statList.getLength(); i++) {

			errList = new HashMap<String, String>();

			String sytemK1Value = (String) ((Element) statList.item(i))
					.getElementsByTagName("System").item(0).getChildNodes()
					.item(0).getNodeValue();

			if ("K1".equalsIgnoreCase(sytemK1Value)) {
				String ErrorCode = (String) ((Element) statList.item(i))
						.getElementsByTagName(Code).item(0).getChildNodes()
						.item(0).getTextContent();
				String ErrorMsg = (String) ((Element) statList.item(i))
						.getElementsByTagName(Message).item(0)
						.getChildNodes().item(0).getTextContent();
                if( !("0".equalsIgnoreCase(ErrorCode)&& "SUCCESS".equalsIgnoreCase(ErrorMsg))){
				errList.put("ErrorCode", ErrorCode);
				errList.put("ErrorMsg", ErrorMsg);
                }
			}

		}
		bundleResult.put(0, errList);

		return bundleResult;
	}
	
	
	
	public static HashMap<Integer, HashMap<String, String>> parseACSFaultResponse(
			String ACSresponse, String Code, String Message)
			throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(ACSresponse);
		doc.getDocumentElement().normalize();
		NodeList statList = doc.getElementsByTagName("StatusList");
		
		if(statList !=null){
			if(statList.getLength() == 0 ) throw new ParserConfigurationException("Invalid ACS Response XML");
		}


		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> errList = null;

		for (int i = 0; i < statList.getLength(); i++) {

			errList = new HashMap<String, String>();

			String sytemK1Value = (String) ((Element) statList.item(i))
					.getElementsByTagName("System").item(0).getChildNodes()
					.item(0).getNodeValue();

			if ("ACSTN1".equalsIgnoreCase(sytemK1Value)) {
				String ErrorCode = (String) ((Element) statList.item(i))
						.getElementsByTagName(Code).item(0).getChildNodes()
						.item(0).getTextContent();
				String ErrorMsg = (String) ((Element) statList.item(i))
						.getElementsByTagName(Message).item(0)
						.getChildNodes().item(0).getTextContent();
                if( !("0".equalsIgnoreCase(ErrorCode)&& "SUCCESS".equalsIgnoreCase(ErrorMsg))){
				errList.put("ErrorCode", ErrorCode);
				errList.put("ErrorMsg", ErrorMsg);
                }
			}

		}
		bundleResult.put(0, errList);

		return bundleResult;
	}


	/** This is the response formation for the Simulate Request
	 *
	 * @param responseXml
	 * @return
	 *
	 */

	public static String getSimulateIMSINumberResponse(String ICCID, String methodName, String acctSegId) {
		String resXml = "";
		String IMSI = String.valueOf((long)(Math.random()*1000000000 + 404370000000000L));
		String prepaidValue = null;
		if (acctSegId.equalsIgnoreCase("4") || acctSegId.equalsIgnoreCase("5")) {
			prepaidValue = "TRUE";
		} else {
			prepaidValue = "FALSE";
		}

		resXml = "<wireless><inquiry>";
		resXml = resXml + "<method name=\""+methodName+"\"><data>";
		resXml = resXml + "<prepaid>"+prepaidValue+"</prepaid>";
		resXml = resXml + "<iccid>"+ICCID+"</iccid>";
		resXml = resXml + "<imsi>"+IMSI+"</imsi>";
		resXml = resXml + "</data><error><message>Success</message>";
		resXml = resXml + "<number>0</number>";
		resXml = resXml + "</error></method></inquiry></wireless>";
		return resXml;
	}

	/**
	 * This method will retrieve the System ParamValue for the given System ParamName and System Module Name
	 * @param paramlist
	 * @param provSystemParamModuleOcs
	 * @param prepaidIptrunk
	 * @return
	 */
	public static String getParamValueFromParamList(
			HashMap<Integer, C30ProvSystemParameters> paramlist,
			String provSystemParamModuleOcs, String prepaidIptrunk) {
		String paramValue = null;
		for (int i = 0; i < paramlist.size(); i++) {
			if (paramlist.get(i).getModuleName()
					.equalsIgnoreCase(provSystemParamModuleOcs)
					&& paramlist.get(i).getParamName()
							.equalsIgnoreCase(prepaidIptrunk))
				paramValue = paramlist.get(i).getParamValue();
		}

		return paramValue;
	}
	
	/**
	 * This method will retrieve the System ParamValue for the given System ParamName and System Module Name
	 * @param c30ProvSystemParameters
	 * @param provSystemParamModuleOcs
	 * @param prepaidIptrunk
	 * @return
	 */
	public static String getParamValueFromParamListForAccSeg(
			HashMap<Integer, C30ProvSystemParameters> paramlist,
			String provSystemParamModule, String paramName, Integer accSegId) {
		String paramValue = null;
		for (int i = 0; i < paramlist.size(); i++) {
			if (paramlist.get(i).getModuleName()
					.equalsIgnoreCase(provSystemParamModule)
					&& paramlist.get(i).getParamName()
							.equalsIgnoreCase(paramName) && paramlist.get(i).getAcctSegmentId().equals(accSegId)){
				paramValue = paramlist.get(i).getParamValue();
			}
		}

		return paramValue;
	}

	/**
	 * This method will overrides the request Id , Sub request Id in the request Object
	 * @param request
	 * @param datasource
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public static IProvisioningRequest generateNewRequestId(IProvisioningRequest request, C30JDBCDataSource datasource) throws C30ProvConnectorException{
		ProvisioningRequestHandler prh = new ProvisioningRequestHandler();
		prh.setDatasource(datasource);
		Integer requestId= prh.getPlanRequestId(datasource);
		request.getRequestProperties().setRequestId(requestId);
		String carrierId = C30ProvConnectorUtils.getCarrierIdByAcctSegId(request.getRequestProperties().getAcctSegId());
		String subRequestId  = carrierId+"."+requestId.toString()+"."+request.getRequestProperties().getAuthClient().getSystemType();
		request.getRequestProperties().setSubRequestId(subRequestId);
		return request;

	}


	/**
	 * This method will give the TN Status String for the value
	 * @param invSatus
	 * @return statusId
	 *
	 */

	public static String getTNStatusIdForGivenStatus(String invSatus) {

		String statusId=null;

		HashMap<String, String> statusMapping = new HashMap<String,String>();
		statusMapping.put("Available","1");
		statusMapping.put("Assigned","2");
		statusMapping.put("Awaiting Return","3");
		statusMapping.put("Aging","4");
		statusMapping.put("Not Available","5");
		statusMapping.put("Blacklisted","6");
		statusMapping.put("Ported Out","7");
		statusMapping.put("Reserved","8");
		statusMapping.put("Sim_Pre_Order","9");
		statusMapping.put("Sim_Post_Order","10");
		statusMapping.put("Refurbish","14");
		statusMapping.put("Damaged","15");
		statusMapping.put("Removed","16");
		statusMapping.put("Research","17");
		statusMapping.put("Expired (purchase block)","18");
		statusMapping.put("Soft Dial Tone In Use","19");
		statusMapping.put("Prep","20");
		statusMapping.put("ACS In Use","24");
		statusMapping.put("GCI Pickup","25");
		statusMapping.put("Equipment Return","26");
		statusMapping.put("Customer Drop Off","27");
		statusMapping.put("On Hold","28");
		statusMapping.put("Return for Repair","29");
		statusMapping.put("Return for Warranty","30");
		statusMapping.put("Swap","31");
		statusMapping.put("Purchase Block","32");
		statusMapping.put("Upgrade Pending Assigned","33");
		statusMapping.put("Loaded","51");
		statusMapping.put("Retained","100");
		statusMapping.put("Internal_Deleted","200");

		if(statusMapping.containsKey(invSatus)){
          statusId=statusMapping.get(invSatus).toString();
		}
		return statusId;

	}
	
	
	
	public static String getBlackHawErrorMessageForGivenCode(String errorcode) {
		
		String errorMsg=null;
		
		HashMap<String, String> errorMapping = new HashMap<String,String>();
		errorMapping.put("01","Approved � balance unavailable");
		errorMapping.put("02","Refer to card issuer");
		errorMapping.put("03","Approved � balance unavailable on external account number");
		errorMapping.put("04","Already Redeemed");
		errorMapping.put("05","Error account problem");
		errorMapping.put("06","Invalid expiration date");
		errorMapping.put("07","Unable to process");
		errorMapping.put("08","Card not found");
		errorMapping.put("12","Invalid transaction");
		errorMapping.put("13","Invalid amount");
		errorMapping.put("14","Invalid Product");
		errorMapping.put("16","Invalid status change");
		errorMapping.put("17","Invalid merchant");
		errorMapping.put("18","Invalid Phone Number");
		errorMapping.put("20","Invalid Pin");
		errorMapping.put("21","Card already active");
		errorMapping.put("30","Bad track2 - format error");
		errorMapping.put("33","Expired card");
		errorMapping.put("34","Already reversed");
		errorMapping.put("35","Already voided");
		errorMapping.put("36","Restricted card");
		errorMapping.put("37","Restricted External Account");
		errorMapping.put("41","Lost card");
		errorMapping.put("42","Lost External Account");
		errorMapping.put("43","Stolen card");
		errorMapping.put("44","Stolen External Account");
		errorMapping.put("51","Insufficient funds");
		errorMapping.put("54","Expired External Account");
		errorMapping.put("55","Max recharge reached");
		errorMapping.put("56","Advance less amount / enter lesser amount");
		errorMapping.put("58","Request not permitted by merchant location");
		errorMapping.put("59","Request not permitted by processor");
		errorMapping.put("61","Exceeds withdrawal amount / over limit");	
		errorMapping.put("62","Exceeds financial limit");
		errorMapping.put("65","Exceeds withdrawal frequency limit");
		errorMapping.put("66","Exceeds transaction count limit");
		errorMapping.put("69","Format error -bad data");
		errorMapping.put("71","Invalid External Account number");
		errorMapping.put("94","Duplicate transaction");
		errorMapping.put("95","Cannot Reverse the Original Transaction");
		errorMapping.put("99","General decline");

		// Soft Declines 
		errorMapping.put("74","Unable to route / System Error");
		errorMapping.put("15","Time Out occurred- Auth Server not available /responding");
		
		if(errorMapping.containsKey(errorcode)){
          errorMsg=errorMapping.get(errorcode).toString();
		}
		return errorMsg;

	}


	public static HashMap<Integer, HashMap<String, String>> parseXMLForGCIRetreiveDetails(
			String responseXML) throws ParserConfigurationException,
			SAXException, IOException {
		Document doc = getDocument(responseXML);
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		NodeList structList = doc.getElementsByTagName("Inventory");
		HashMap<String, String> invList = null;
		if (structList != null && structList.getLength() != 0) {
			for (int i = 0; i < structList.getLength(); i++) {

				invList = new HashMap<String, String>();
				String telePhNmbr = "";
				String createDate = "";
				String statusValue = "";
				String ntwrkId = "";
				String operatorId = "";
				String ntwrkDeviceId = "";
				String salesChannelId = "";

				if (((Element) structList.item(i))
						.getElementsByTagName("Status").item(0).getChildNodes()
						.item(0) != null) {
					statusValue = (String) ((Element) structList.item(i))
							.getElementsByTagName("Status").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				invList.put("Status", statusValue);

				if (((Element) structList.item(i))
						.getElementsByTagName("TelephoneNumber").item(0)
						.getChildNodes().item(0) != null) {
					telePhNmbr = (String) ((Element) structList.item(i))
							.getElementsByTagName("TelephoneNumber").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				invList.put("TelephoneNumber", telePhNmbr);
				
				
				if (((Element) structList.item(i))
						.getElementsByTagName("NetworkId").item(0) != null) {
					if (((Element) structList.item(i))
							.getElementsByTagName("NetworkId").item(0)
							.getChildNodes().item(0) != null) {
						ntwrkId = (String) ((Element) structList.item(i))
								.getElementsByTagName("NetworkId").item(0)
								.getChildNodes().item(0).getTextContent();
					}

				}
				
				invList.put("NetworkId", ntwrkId);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("CreateDate").item(0) != null) {
				if (((Element) structList.item(i))
						.getElementsByTagName("CreateDate").item(0)
						.getChildNodes().item(0) != null) {
					createDate = (String) ((Element) structList.item(i))
							.getElementsByTagName("CreateDate").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				
			}
				invList.put("CreateDate", createDate);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("OperatorId").item(0) != null) {			
				
				if (((Element) structList.item(i))
						.getElementsByTagName("OperatorId").item(0)
						.getChildNodes().item(0) != null) {
					operatorId = (String) ((Element) structList.item(i))
							.getElementsByTagName("OperatorId").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				
			}
				invList.put("OperatorId", operatorId);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("NetworkDeviceId").item(0) != null) {
				
				if (((Element) structList.item(i))
						.getElementsByTagName("NetworkDeviceId").item(0)
						.getChildNodes().item(0) != null) {

					ntwrkDeviceId = (String) ((Element) structList.item(i))
							.getElementsByTagName("NetworkDeviceId").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				
				}
				invList.put("NetworkDeviceId", ntwrkDeviceId);
				
				if (((Element) structList.item(i))
						.getElementsByTagName("SalesChannelId").item(0) != null) {
				if (((Element) structList.item(i))
						.getElementsByTagName("SalesChannelId").item(0)
						.getChildNodes().item(0) != null) {

					salesChannelId = (String) ((Element) structList.item(i))
							.getElementsByTagName("SalesChannelId").item(0)
							.getChildNodes().item(0).getNodeValue();
				}
				}
				invList.put("SalesChannelId", salesChannelId);

				bundleResult.put(i, invList);
			}

		}
		return bundleResult;
	}
	// PW-38 
		public static String getDataBalance(String balanceResponseXm) throws ParserConfigurationException, SAXException, IOException, C30ProvConnectorException{
			// TODO Auto-generated method stub
			log.info("Inside Databalance call");
			String memberName = "BalanceTypeLabel";
	        Document doc=null;
	        String databalane="0";
			 doc = getDocument(balanceResponseXm);
		     doc.getDocumentElement().normalize();
		     NodeList structList = doc.getElementsByTagName(Constants.OCS_XML_TAG_STRUCT);
			 HashMap<String,String> balanceHashmap=new HashMap<String,String>();
			        
			            for(int i = 0; i < structList.getLength(); i++)
			            {
			                NodeList memberList = ((Element)structList.item(i)).getElementsByTagName("member");
			                String key=new String();
			                String value=new String();
			                for(int j = 0; j < memberList.getLength(); j++)
			                {
			                	 
			                    String Name = ((Element)memberList.item(j)).getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue();
			                    String Value = ((Element)memberList.item(j)).getElementsByTagName("value").item(0).getChildNodes().item(0).getTextContent();
			                   // log.info("J="+j+"=Name="+Name+"=value="+Value);
			                    
			                    if(Name.equalsIgnoreCase(memberName))
			                    {	                                    
			                        key=Value;
			                    }
			                    if(Name.equalsIgnoreCase("Value")) {
			                    	value=Value;
			                    		                    	
			                    }
			                    
			                }
			                balanceHashmap.put(key,value);
			            }
			            if(balanceHashmap.containsKey("GCI_UL_DATA"))
			            {
			            	log.info("get Data balance " +balanceHashmap.get("GCI_UL_DATA").toString());
			            	databalane=balanceHashmap.get("GCI_UL_DATA").toString();
			            }
			            
			     	
				
			return databalane;
	}
		
		//PW-38
		public static String getSubsciberProfileBalanceTypesForExtendExpiryDate(String inputXML, HashMap requestHashMap,String dataBalance)
		        throws ParserConfigurationException, SAXException, IOException, C30ProvConnectorException, ParseException
		    {
		        String balTypePayload = "";
		        String ExpirationDate = null;
		        String BalExpirationDate = null;
		        String balName = null;
		        String balLabel = null;
		        String balValue = null;
		        String prepaidAction = null;
		        boolean dataKey=false;
		        Document doc = getDocument(inputXML);
		        doc.getDocumentElement().normalize();
		        NodeList structList = doc.getElementsByTagName(Constants.OCS_XML_TAG_STRUCT);
		        StringBuffer finalbalTypePayload = new StringBuffer();
		       	log.info("getSubsciberProfileBalanceTypes = "+requestHashMap.toString());
		            //Get Plan Expiration Date
		    		if (requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE)) {
						ExpirationDate = getOCSDateFormatFromString(requestHashMap.get(Constants.PREPAID_EXPIRATIONDATE).toString(), "0", true);
						} else {
			            Calendar cal = Calendar.getInstance();
			            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 23, 59, 59);
			            java.util.Date CalExpirationDate = cal.getTime();
			            DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
			            ExpirationDate = df.format(CalExpirationDate).toString();
			            ExpirationDate = getOCSDateFormatFromString(ExpirationDate, "30", false);
			      		}
					//Get Balance Expiration Date
			  		if (requestHashMap.containsKey(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE)) {
		  			try {
					  			BalExpirationDate = C30ProvConnectorUtils.getOCSDateFormatFromString(requestHashMap.get(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE).toString(),
					  					"0", true);
						} catch (ParseException e) {
							e.printStackTrace();
					    	log.error("getCOMMONBalance-OCS Date Formatting Error : "+e);
							throw new C30ProvConnectorException(e, e.getMessage());
					    }
			  		}
					// Build Balance Type.
					if (requestHashMap.containsKey(Constants.ATTR_PREAPID_ACTION)) {
						prepaidAction = requestHashMap.get(Constants.ATTR_PREAPID_ACTION).toString();
					}

					// Build Balance Type.
					if (requestHashMap.containsKey(Constants.PREPAID_BALANCE_TYPE_LABEL)) {
						balLabel = requestHashMap.get(Constants.PREPAID_BALANCE_TYPE_LABEL).toString();
					}
					//Build Wallet Value
					if (requestHashMap.containsKey(Constants.PREPAID_VALUE)) {
						//If the value is 10099, then the value should be $100.99
						String Value = requestHashMap.get(Constants.PREPAID_VALUE).toString();
						if (balLabel.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON)) {
							log.info("Balance type common");
							if (Integer.valueOf(Value) > 0) {
								//balValue = new StringBuffer(Value).insert(Value.length()-2, ".").toString();
								//Made the code Generic to Handle any cents.
								 balValue= new Double( ((Double.parseDouble(Value)/100))).toString();
						
							} else {
								balValue = "0.00";
								log.info("balValue="+balValue);
							}
						}
					}

		            for(int i = 0; i < structList.getLength() && i <= 0; i++)
		            {
		                NodeList memberList = ((Element)structList.item(i)).getElementsByTagName(Constants.OCS_XML_TAG_MEMBER);
		                for(int j = 0; j < memberList.getLength(); j++)
		                {
		                    String Name = ((Element)memberList.item(j)).getElementsByTagName(Constants.OCS_XML_TAG_NAME).item(0).getChildNodes().item(0).getNodeValue();
		                    String Value = ((Element)memberList.item(j)).getElementsByTagName(Constants.OCS_XML_TAG_VALUE).item(0).getChildNodes().item(0).getTextContent();
		                  //  log.info("J="+j+"=Name="+Name+"=value="+Value);
		                    
	                        //Get the Balance Type
		                    if(Name.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_LABEL)) {
		                        balTypePayload = "";
		                        balName = Value;
		                        //PW-38
		                        if(Value.equalsIgnoreCase("GCI_UL_DATA"))
		                        {
		                        	dataKey=true;
		                        }

		                        if (!(prepaidAction != null &&
		                        		(prepaidAction.equalsIgnoreCase(Constants.PREPAID_PLAN_CHANGE) ||
		                        		prepaidAction.equalsIgnoreCase(Constants.PREPAID_EXTEND_EXPIRYDATE) ||
		                        		prepaidAction.equalsIgnoreCase(Constants.PREPAID_PLAN_RECHARGE_RESERVE)) &&
		                        		balName.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON))) {
			                        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Name, balName, Constants.OCS_XML_TAG_STRING);
			                        //for Balance Type COMMON, BalanceExpiry date to be populated
			                        if (Value.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON) && BalExpirationDate != null) {
				                        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, BalExpirationDate,
				                        		Constants.OCS_XML_TAG_STRING);
				                        balTypePayload = balTypePayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, balValue,
				                        		Constants.OCS_XML_TAG_DOUBLE);
			                        } else {
				                        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, ExpirationDate,
				                        		Constants.OCS_XML_TAG_STRING);
			                        }
		                        }
		                    }

	                            	//Get the Value of the BalanceType
		                	if(Name.equalsIgnoreCase(Constants.PREPAID_BALANCE_INITIAL_VALUE)) {
		                		    //Do NOT Add COMMON Balance for SWAP Plan
		                	    if (!(balName.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON))) {
		                	    log.info("Data Key =="+dataKey);
		                	    	if(dataKey)
		         	                    {
		         	                   		 balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, dataBalance, Constants.OCS_XML_TAG_DOUBLE);
		         	                  		 dataKey=false;
		       	                    	}
		                	    	else{
	    		        	        balTypePayload = balTypePayload + encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, Value, Constants.OCS_XML_TAG_DOUBLE);
	    		        	  	    	}
		                	     }

		                             if (balTypePayload != "") {
			                        balTypePayload = encloseOCSDataMultiplyTags(balTypePayload);
			                        finalbalTypePayload.append(balTypePayload);
		                	     }
		                	}
			            //}
		                }
		            }
		       
		          return finalbalTypePayload.toString();
		    }
}


