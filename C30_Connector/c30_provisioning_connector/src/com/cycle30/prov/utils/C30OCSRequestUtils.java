package com.cycle30.prov.utils;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Calendar;   
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvFeatureCatalogMap;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.util.C30DOMUtils;

public class C30OCSRequestUtils {

	private static Logger log = Logger.getLogger(C30OCSRequestUtils.class);


	/** Get the Subscriber Create XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberCreateXml(HashMap requestHashMap) throws C30ProvConnectorException {

		String requestPayload = "";
		
		try {
			
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL, 
					requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_PINCODE)) {
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_PINCODE, 
					requestHashMap.get(Constants.PREPAID_PINCODE).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_FIRST_NAME)) {
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_FIRST_NAME, 
					requestHashMap.get(Constants.PREPAID_FIRST_NAME).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_LAST_NAME)) {
				log.info("Get the Last Name Details");
				HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
				String autopayFlag=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,
						Constants.PREPAID_PARAM_AUTO_PAY_FLAG);
				log.info("autopayFlag =" + autopayFlag);
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_LAST_NAME, 
						autopayFlag, Constants.OCS_XML_TAG_STRING);
			}

			requestPayload = C30ProvConnectorUtils.encloseOCSRequestRootlevel2Tags(requestPayload);
			
			return requestPayload;
			
		} catch(Exception e) {
			throw new C30ProvConnectorException(e, "OCS-006");
		}
	}

	/** Get the Subscriber Update XML for Add Orders
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberUpdateXml(HashMap requestHashMap) throws C30ProvConnectorException {

		String requestPayload = "";	
		String level1payload = "";
		String level2payload = "";
		
		try {
			
			// Build Subscriber.
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
				level1payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBERID, 
						requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), Constants.OCS_XML_TAG_STRING);
			}
			//PW-169
			if(requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE)) {
				level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, 
						requestHashMap.get(Constants.PREPAID_EXPIRATIONDATE).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL)) {
				level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL, 
						requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_PINCODE)) {		
				level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_PINCODE, 
						requestHashMap.get(Constants.PREPAID_PINCODE).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_LAST_NAME)) {
				if (requestHashMap.get(Constants.PREPAID_LAST_NAME).toString().equalsIgnoreCase(Constants.PREPAID_OCS_NULL_VALUE)) {
					level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberNullValueTags(Constants.PREPAID_LAST_NAME, 
							Constants.PREPAID_OCS_NULL_VALUE);
				} else {
					HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
					String autopayFlag=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,
							Constants.PREPAID_PARAM_AUTO_PAY_FLAG);
					level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_LAST_NAME, 
							autopayFlag, Constants.OCS_XML_TAG_STRING);
				}
			}
			//Build Subscriber Line.
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_LINE_ID)) {
				level2payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBER_LINE_ID, 
						requestHashMap.get(Constants.PREPAID_SUBSCRIBER_LINE_ID).toString(), Constants.OCS_XML_TAG_INT);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_CLI)) {
				level2payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_CLI, 
						requestHashMap.get(Constants.PREPAID_CLI).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_NUMBERSUBTYPE)) {		
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_NUMBERSUBTYPE, 
					requestHashMap.get(Constants.PREPAID_NUMBERSUBTYPE).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_PREFERRED_LANG)) {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_PREFERRED_LANG, 
					requestHashMap.get(Constants.PREPAID_PREFERRED_LANG).toString(), Constants.OCS_XML_TAG_STRING);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS)) {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS, 
					requestHashMap.get(Constants.PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS).toString(), Constants.OCS_XML_TAG_BOOLEAN);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_BALANCE_WARNING_SMS)) {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_BALANCE_WARNING_SMS, 
					requestHashMap.get(Constants.PREPAID_BALANCE_WARNING_SMS).toString(), Constants.OCS_XML_TAG_BOOLEAN);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_IMSI)) {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_IMSI, 
					requestHashMap.get(Constants.PREPAID_IMSI).toString(), Constants.OCS_XML_TAG_STRING);
			}
			
			if (level2payload == "") {
				//Build Final XML		
				requestPayload = C30ProvConnectorUtils.encloseOCSRequestRootlevel2Tags(level1payload);
			} else {
				level2payload = C30ProvConnectorUtils.encloseOCSRequestRootlevel3Tags(level2payload);
				level2payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBER_LINES, 
						level2payload, Constants.OCS_XML_TAG_ARRAY);
				//Build Final XML		
				requestPayload = C30ProvConnectorUtils.encloseOCSRequestRootlevel2Tags(level1payload+level2payload);
			}

			return requestPayload;			
		} catch(Exception e) {
			throw new C30ProvConnectorException(e, "OCS-007");
		}
		
	}

	/** Get the Subscriber Line Update XML for Change Orders
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberLineUpdateXml(HashMap requestHashMap) {

		String requestPayload = "";	
	
		// Build XML
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_LINE_ID)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBER_LINE_ID, 
					requestHashMap.get(Constants.PREPAID_SUBSCRIBER_LINE_ID).toString(), Constants.OCS_XML_TAG_INT);
		}
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_SUBSCRIBERID, 
					requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), Constants.OCS_XML_TAG_INT);
		}
		if (requestHashMap.containsKey(Constants.PREPAID_IMSI)) {
		requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_IMSI, 
				requestHashMap.get(Constants.PREPAID_IMSI).toString(), Constants.OCS_XML_TAG_STRING);
		}
		if (requestHashMap.containsKey(Constants.PREPAID_CLI)) {
		requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_CLI, 
				requestHashMap.get(Constants.PREPAID_CLI).toString(), Constants.OCS_XML_TAG_STRING);
		}
		
		//Build Final XML		
		requestPayload = C30ProvConnectorUtils.encloseOCSRequestRootlevel2Tags(requestPayload);

		return requestPayload;
		
	}

	/** Get the Subscriber Line Delete XML for Change Orders
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberLineDeleteXml(HashMap requestHashMap) {

		String requestPayload = "";	
		String methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_LINE_DELETE,false,false);
	
		// Build XML request.
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_LINE_ID)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBER_LINE_ID).toString(), 
					Constants.OCS_XML_TAG_INT, false);
		}
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
		requestPayload = Constants.OCS_RESPONSE_XML_HEADER+C30ProvConnectorUtils.encloseOCSRequestRootTags(methodTags, requestPayload, null);				
		return requestPayload;
		
	}
	
	/** Get the Subscriber ActivateByCLI XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberActivateByCLIXml(HashMap requestHashMap) {

		String requestPayload = "";
		
		// Build XML request.
		if (requestHashMap.containsKey(Constants.PREPAID_CLI)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_CLI).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		}
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
				
		return requestPayload;
	}

	/** Get the SubscriberLineByCLI XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberLineByCLIXml(HashMap requestHashMap) {

		String requestPayload = "";
		String methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_LINE_GET_BYCLI,false,false);		
		// Build XML request.
		if (requestHashMap.containsKey(Constants.PREPAID_CLI)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_CLI).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		}
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
		requestPayload = Constants.OCS_RESPONSE_XML_HEADER+C30ProvConnectorUtils.encloseOCSRequestRootTags(methodTags, requestPayload, null);				
		return requestPayload;
	}
	
	/** Get the SubscriberByCLI XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberByCLIXml(HashMap requestHashMap) {

		String requestPayload = "";
		String methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_GET_BYCLI,false,false);		
		// Build XML request.
		if (requestHashMap.containsKey(Constants.PREPAID_CLI)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_CLI).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		}
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
		requestPayload = Constants.OCS_RESPONSE_XML_HEADER+C30ProvConnectorUtils.encloseOCSRequestRootTags(methodTags, requestPayload, null);				
		return requestPayload;
	}

	/** Get the Subscriber.GetBalances XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberGetBalancesXml(HashMap requestHashMap) {

		String requestPayload = "";
		String methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_GET_BALANCES,false,false);
		// Build XML request.
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
					Constants.OCS_XML_TAG_INT, false);
		}
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
		requestPayload = Constants.OCS_RESPONSE_XML_HEADER+C30ProvConnectorUtils.encloseOCSRequestRootTags(methodTags, requestPayload, null);
		
		return requestPayload;
	}
	
	/** Get the Subscriber Recharge XML
	 * 
	 * @param requestHashMap 
	 * @param Value
	 * @param ExpirationDate
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberRechargeXml(HashMap requestHashMap, String Value, String ExpirationDate)	
	{

		String requestPayload = "";
		String level1payload = "";		
		String level2payload = "";
		
		// Build SubscriberId.
		requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
				Constants.OCS_XML_TAG_INT, false);
		// Build COMMON Balance Type.
		level1payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_BALANCE_TYPE_LABEL, 
				requestHashMap.get(Constants.PREPAID_BALANCE_TYPE_LABEL).toString(), Constants.OCS_XML_TAG_STRING);
		level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, 
				Value, Constants.OCS_XML_TAG_DOUBLE);
		level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, 
				ExpirationDate, Constants.OCS_XML_TAG_STRING);
		level1payload = C30ProvConnectorUtils.encloseOCSRequestParamTags(C30ProvConnectorUtils.encloseOCSRequestRootlevel3Tags(level1payload), 
				Constants.OCS_XML_TAG_ARRAY, false);
		//Build Reason.
		level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_REASON).toString(), 
				Constants.OCS_XML_TAG_STRING, false);
		//Build Final XML
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload+level1payload+level2payload, false, false);
		
		return requestPayload;		
		
	}
	
	/** Get the Subscriber Credit XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberCreditXml(HashMap requestHashMap) {
		
		String requestPayload = "";
		String level1payload = "";		
		String level2payload = "";
		String ExpirationDate = null;	
		String Label = null;		
		
		try {
			if (requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE)) {
				ExpirationDate = C30ProvConnectorUtils.getOCSDateFormatFromString(requestHashMap.get(Constants.PREPAID_EXPIRATIONDATE).toString(), "0", true);
				log.info("ExpirationDate : "+ExpirationDate);
			}
		}
    	catch (ParseException e)
    	{
    	        e.printStackTrace();
    	        log.error("OCS Date Formatting Error : "+e);
    	}   
		
		// Build SubscriberId
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
					Constants.OCS_XML_TAG_INT, false);
		}
		// Build Balance Type.
		if (requestHashMap.containsKey(Constants.PREPAID_BALANCE_TYPE_LABEL)) {
			Label = requestHashMap.get(Constants.PREPAID_BALANCE_TYPE_LABEL).toString();			
			level1payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_BALANCE_TYPE_LABEL, 
					Label, Constants.OCS_XML_TAG_STRING);
		}
		if (requestHashMap.containsKey(Constants.PREPAID_VALUE)) {
			//If the value is 10099, then the value should be $100.99
			String Value = requestHashMap.get(Constants.PREPAID_VALUE).toString();

			if (Label.equalsIgnoreCase(Constants.PREPAID_BALANCE_TYPE_COMMON)) {
				if (Integer.valueOf(Value) > 0) {
					Value = new StringBuffer(Value).insert(Value.length()-2, ".").toString();
				}
			}
			level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, Value, Constants.OCS_XML_TAG_DOUBLE);
		}
		if (requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE)) {
			level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, 
					ExpirationDate, Constants.OCS_XML_TAG_STRING);
		}
		level1payload = C30ProvConnectorUtils.encloseOCSRequestParamTags(C30ProvConnectorUtils.encloseOCSRequestRootlevel3Tags(level1payload), 
				Constants.OCS_XML_TAG_ARRAY, false);
		//Build Reason.
		if (requestHashMap.containsKey(Constants.PREPAID_REASON)) {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_REASON).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		} else {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestParamTags("", Constants.OCS_XML_TAG_STRING, false);
		}
		//Build Final XML
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload+level1payload+level2payload, false, false);		
		return requestPayload;		
		
	}
	
	/** Used to get Subscriber.Credit with COMMON Balance XML
	 * 
	 * @param HasMap
	 * @return Subscriber.Credit with COMMON Balance XML
	 * 
	 * @throws C30ProvConnectorException 
	 */	     

  public static String getSubscriberCreditXmlforCOMMONBalance(HashMap requestHashMap) {
		String requestPayload = "";
		String level1payload = "";		
		String level2payload = "";			
		String ExpirationDate = null;
		
		// Build XML request.
		String methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_CREDIT,false,false);
		System.out.println("methodTags = "+methodTags);
		// Build SubscriberId
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
					Constants.OCS_XML_TAG_INT, false);
		}

		// Build Balance Type.
		level1payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_BALANCE_TYPE_LABEL, 
				Constants.PREPAID_BALANCE_TYPE_COMMON, Constants.OCS_XML_TAG_STRING);
		//Build Wallet Value
		if (requestHashMap.containsKey(Constants.PREPAID_VALUE)) {
			//If the value is 10099, then the value should be $100.99
			String Value = requestHashMap.get(Constants.PREPAID_VALUE).toString();
			if (Integer.valueOf(Value) > 0) {
				Value = new StringBuffer(Value).insert(Value.length()-2, ".").toString();
			}
			level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, Value, Constants.OCS_XML_TAG_DOUBLE);
		}

		//Build Expiration Date if 
		// kmr request to allow recharge reserve to be sent to OCS
		//if (!requestHashMap.get(Constants.PREPAID_BALANCE_ACTION).toString().equalsIgnoreCase(Constants.PREPAID_PLAN_RECHARGE_RESERVE)){
			if (requestHashMap.containsKey(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE) && 
					requestHashMap.get(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE) != null) {
				try {		  			
			  			ExpirationDate = C30ProvConnectorUtils.getOCSDateFormatFromString(requestHashMap.get(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE).toString(),
			  					"0", true);
				} catch (ParseException e) {
					e.printStackTrace();
			    } 
				level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, ExpirationDate, Constants.OCS_XML_TAG_STRING);
			}
		//}
		
		level1payload = C30ProvConnectorUtils.encloseOCSRequestParamTags(C30ProvConnectorUtils.encloseOCSRequestRootlevel3Tags(level1payload), 
				Constants.OCS_XML_TAG_ARRAY, false);

		//Build Reason.
		if (requestHashMap.containsKey(Constants.PREPAID_REASON)) {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_REASON).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		} else {
			level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestParamTags("", Constants.OCS_XML_TAG_STRING, false);
		}

		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload+level1payload+level2payload, false, false);
		requestPayload = Constants.OCS_RESPONSE_XML_HEADER+C30ProvConnectorUtils.encloseOCSRequestRootTags(requestPayload, methodTags, null);
		
		return requestPayload;
  	}
  
	/** Get the EventCharging Direct Debit XML
	 * 
	 * @param requestHashMap
	 * @return requestPayload
	 * 
	 */
	public static String getEventChargingDirectDebitXml(HashMap requestHashMap) {

		String requestPayload = "";

		// Build XML request.
		//Build Reason
		requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(null,Constants.OCS_XML_TAG_STRING, true);
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_CLI)) {	
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBER_CLI).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		}
		requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(null,Constants.OCS_XML_TAG_STRUCT, true);
		if (requestHashMap.containsKey(Constants.PREPAID_CHARGE_EVENT_LABEL)) {
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_CHARGE_EVENT_LABEL).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		}
		if (requestHashMap.containsKey(Constants.PREPAID_IPTRUNK)) {
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_IPTRUNK).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		}
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
		
		return requestPayload;	
	    
	}			

	/** Get the Subscriber Recharge By CLI PIN XML
	 * 
	 * @param requestHashMap
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberRechargeByCLIPINXml(HashMap requestHashMap)
			throws C30ProvConnectorException {

		String requestPayload = "";

		try {
			// Build XML request.
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_CLI)) {	
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBER_CLI).toString(), 
						Constants.OCS_XML_TAG_STRING, false);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_PINCODE)) {
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_PINCODE).toString(), 
						Constants.OCS_XML_TAG_STRING, false);
			}
			if (requestHashMap.containsKey(Constants.PREPAID_BALANCE_VOUCHER_PHONE_ACCESS)) {
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_BALANCE_VOUCHER_PHONE_ACCESS).toString(), 
						Constants.OCS_XML_TAG_STRING, false);
			} else {
				HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
				String acessPhoneCode=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,Constants.PREPAID_PARAM_ACCESS_PHONE);
				log.info("AcessPhone =" + acessPhoneCode);
				requestHashMap.put(Constants.PREPAID_BALANCE_VOUCHER_PHONE_ACCESS, acessPhoneCode);
				
			}
			if (requestHashMap.containsKey(Constants.PREPAID_IPTRUNK)) {
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_IPTRUNK).toString(), 
						Constants.OCS_XML_TAG_STRING, false);
			} else {
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(null,Constants.OCS_XML_TAG_STRING, true);
			}
			// Build Final XML request.
			requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
			
			return requestPayload;	
		}
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "OCS-005");
		}
	}	

	/** Get the Subscriber GetBalances XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */	
	public static String getPrepaidBalanceXml(HashMap<String, String> balrequestHashMap) {
		String reqXml = "";

		reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_BALANCES, false, false);
		reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, (String) balrequestHashMap.get(Constants.SUBSCRIBER_ID), false, false);
		log.info("getPrepaidBalanceXml = "+reqXml);
		return reqXml;
	}	
	
	
	/** Get the SubscriberProfileGetByLabel XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberProfileGetByLabel(HashMap requestHashMap) {

		String requestPayload = "";
		log.info("Fetching Balance Type Label ");
		log.info("Balance Type Label " +requestHashMap.get(Constants.PREPAID_LABEL).toString());
		// Build XML request.
		requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_LABEL).toString(), Constants.OCS_XML_TAG_STRING, false);
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
		log.info("SubscriberProfile.GetByLabel Payload = "+requestPayload);
		return requestPayload;
	}
	

	/** Get the Subscriber Disconnect XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberDisconnectXml(HashMap requestHashMap) {

		String requestPayload = "";
		String dataTypeInt = Constants.OCS_XML_TAG_INT;	    
		// Build XML request. 

		return requestPayload;
	}
	//Added by vijaya PW-43
	/**
	 * Get the Subscriber Delete XML for Change Orders
	 * 
	 * @param requestHashMap
	 * @return requestPayload
	 * 
	 */
	public static String getSubscriberDeleteXml(HashMap requestHashMap) {

		String requestPayload = "";
		String methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_DELETE,false, false);

		// Build XML request.
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), Constants.OCS_XML_TAG_INT, false);
		}
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
		requestPayload = Constants.OCS_RESPONSE_XML_HEADER + C30ProvConnectorUtils.encloseOCSRequestRootTags(methodTags,requestPayload, null);
		
		return requestPayload;
	}
			
	
}
	

