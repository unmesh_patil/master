package com.cycle30.prov.config;

/** It is same as c30_prov_feature_plan_map
 * 
 * @author rnelluri
 *
 */
public class C30ProvFeatureCatalogMap {
	C30ProvFeature feature;
	C30ProvFeature rollbackFeature;
	Integer emfConfigId;
	String catalogId;
	String catalogType;
	Integer acctSegId;
	Integer provisionSystemId;
	public C30ProvFeature getFeature() {
		return feature;
	}
	public void setFeature(C30ProvFeature feature) {
		this.feature = feature;
	}
	public C30ProvFeature getRollbackFeature() {
		return rollbackFeature;
	}
	public void setRollbackFeature(C30ProvFeature rollbackFeature) {
		this.rollbackFeature = rollbackFeature;
	}
	public Integer getEmfConfigId() {
		return emfConfigId;
	}
	public void setEmfConfigId(Integer emfConfigId) {
		this.emfConfigId = emfConfigId;
	}
	public String getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}
	public String getCatalogType() {
		return catalogType;
	}
	public void setCatalogType(String catalogType) {
		this.catalogType = catalogType;
	}
	public Integer getAcctSegId() {
		return acctSegId;
	}
	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}
	public Integer getProvisionSystemId() {
		return provisionSystemId;
	}
	public void setProvisionSystemId(Integer provisionSystemId) {
		this.provisionSystemId = provisionSystemId;
	}
	
	@Override
	public String toString() {
		return "C30ProvFeaturePlanMap [feature=" + feature
				+ ", rollbackFeature=" + rollbackFeature + ", emfConfigId="
				+ emfConfigId + ", catalogId=" + catalogId + ", catalogType="
				+ catalogType + ", acctSegId=" + acctSegId
				+ ", provisionSystemId=" + provisionSystemId + "]";
	}
	
	public C30ProvFeatureCatalogMap(C30ProvFeature feature,
			C30ProvFeature rollbackFeature, Integer emfConfigId,
			String catalogId, String catalogType, Integer acctSegId,
			Integer provisionSystemId) {
		super();
		this.feature = feature;
		this.rollbackFeature = rollbackFeature;
		this.emfConfigId = emfConfigId;
		this.catalogId = catalogId;
		this.catalogType = catalogType;
		this.acctSegId = acctSegId;
		this.provisionSystemId = provisionSystemId;
	}


}
