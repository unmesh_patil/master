package com.cycle30.prov.config;


/** This is a direct mapping for the table 
 * C30_BIL_SYSTEM_PARAMETERS
 * 
 * @author Umesh
 *
 */
public class C30ProvSystemParameters {

	String moduleName;
	String paramName;
	String paramValue;
	Integer acctSegmentId;
	String attribute1;	
	String attribute2;
	String attribute3;
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamValue() {
		return paramValue;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public Integer getAcctSegmentId() {
		return acctSegmentId;
	}
	public void setAcctSegmentId(Integer acctSegmentId) {
		this.acctSegmentId = acctSegmentId;
	}
	public String getAttribute1() {
		return attribute1;
	}
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	public String getAttribute2() {
		return attribute2;
	}
	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}
	public String getAttribute3() {
		return attribute3;
	}
	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}
	

	public C30ProvSystemParameters(String moduleName, String paramName, String paramValue,
			Integer acctSegmentId, String attribute1, String attribute2, String attribute3) {
		super();
		this.moduleName = moduleName;
		this.paramName = paramName;
		this.paramValue = paramValue;
		this.acctSegmentId = acctSegmentId;		
		this.attribute1 = attribute1;
		this.attribute2 = attribute2;
		this.attribute3 = attribute3;
	}
	
	@Override
	public String toString() {
		return "C30ProvSystemParameters [moduleName=" + moduleName + ", paramName="
				+ paramName + ", paramValue=" + paramValue + ", acctSegmentId=" + acctSegmentId + ", attribute1="
				+ attribute1 + ", attribute2=" + attribute2 + ", attribute3=" + attribute3 + "]";
	}
	
}
