package com.cycle30.prov.config;

import java.util.ArrayList;
import java.util.HashMap;

/** This is a direct mapping for the table 
 * C30_PROV_FEATURE_CONFIG
 * 
 * @author rnelluri
 *
 */
public class C30ProvFeature {

	Integer featureId;
	String featureName;
	ArrayList<ArrayList<FeatureActions>>  resourceList;

	public Integer getFeatureId() {
		return featureId;
	}
	public void setFeatureId(Integer featureId) {
		this.featureId = featureId;
	}
	public String getFeatureName() {
		return featureName;
	}
	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}
	public ArrayList<ArrayList<FeatureActions>>   getResourceList() {
		return resourceList;
	}
	public void setResourceList(
			ArrayList<ArrayList<FeatureActions>>  resourceList) {
		this.resourceList = resourceList;
	}

	public C30ProvFeature(Integer featureId, String featureName,
			ArrayList<ArrayList<FeatureActions>>   resourceList) {
		super();
		this.featureId = featureId;
		this.featureName = featureName;
		this.resourceList = resourceList;
	}
	
	@Override
	public String toString() {
		return "C30ProvFeature [featureId=" + featureId + ", featureName="
				+ featureName + ", resourceList=" + resourceList + "]";
	}
	
	
	
}
