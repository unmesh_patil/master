package com.cycle30.prov.config;

public class C30ProvSubscriberProfileDCN {

	String accountNumber=null;
	Integer acctSegId=null;
	String subsciberProfileId=null;
	String subsciberProfileName=null;
	String roamingProfileId=null;
	String fixedIpAddress=null;
	String susbscriberType=null;
	String networkType=null;
	Integer isDefault=null;
	String catalogId=null;
	



	public C30ProvSubscriberProfileDCN(String accountNumber,
			Integer acctSegId, String subsciberProfileId,
			String subsciberProfileName, String roamingProfileId,
			String fixedIpAddress, String susbscriberType,
			String networkType, Integer isDefault, String catalogId) {
		super();
		this.accountNumber=accountNumber;
		this.acctSegId = acctSegId;
		this.subsciberProfileId = subsciberProfileId;
		this.subsciberProfileName = subsciberProfileName;
		this.roamingProfileId = roamingProfileId;
		this.fixedIpAddress = fixedIpAddress;
		this.susbscriberType = susbscriberType;
		this.networkType = networkType;
		this.isDefault = isDefault;
		this.catalogId= catalogId;
		
	}
	
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Integer getAcctSegId() {
		return acctSegId;
	}
	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}
	public String getSubsciberProfileId() {
		return subsciberProfileId;
	}
	public void setSubsciberProfileId(String subsciberProfileId) {
		this.subsciberProfileId = subsciberProfileId;
	}
	public String getSubsciberProfileName() {
		return subsciberProfileName;
	}
	public void setSubsciberProfileName(String subsciberProfileName) {
		this.subsciberProfileName = subsciberProfileName;
	}
	public String getRoamingProfileId() {
		return roamingProfileId;
	}
	public void setRoamingProfileId(String roamingProfileId) {
		this.roamingProfileId = roamingProfileId;
	}
	public String getFixedIpAddress() {
		return fixedIpAddress;
	}
	public void setFixedIpAddress(String fixedIpAddress) {
		this.fixedIpAddress = fixedIpAddress;
	}
	public String getSusbscriberType() {
		return susbscriberType;
	}
	public void setSusbscriberType(String susbscriberType) {
		this.susbscriberType = susbscriberType;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public Integer getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}	
	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}
	
}
