package com.cycle30.prov.config;

public class FeatureActions {
	
	C30ProvFeatureItem featureItem;
	Integer featureItemActionId;
	Integer sequence;
	Integer featureItemSPSPlanId;
	
	
	public C30ProvFeatureItem getFeatureItem() {
		return featureItem;
	}
	public void setFeatureItem(C30ProvFeatureItem featureItem) {
		this.featureItem = featureItem;
	}
	public Integer getFeatureItemActionId() {
		return featureItemActionId;
	}
	public void setFeatureItemActionId(Integer featureItemActionId) {
		this.featureItemActionId = featureItemActionId;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public Integer getFeatureItemSPSPlanId() {
		return featureItemSPSPlanId;
	}
	public void setFeatureItemSPSPlanId(Integer featureItemSPSPlanId) {
		this.featureItemSPSPlanId = featureItemSPSPlanId;
	}
	@Override
	public String toString() {
		return "FeatureActions [featureItem=" + featureItem
				+ ", featureItemActionId=" + featureItemActionId
				+ ", sequence=" + sequence + ", featureItemSPSPlanId="
				+ featureItemSPSPlanId + "]";
	}
	
	public FeatureActions(C30ProvFeatureItem featureItem,
			Integer featureItemActionId, Integer sequence,
			Integer featureItemSPSPlanId) {
		super();
		this.featureItem = featureItem;
		this.featureItemActionId = featureItemActionId;
		this.sequence = sequence;
		this.featureItemSPSPlanId = featureItemSPSPlanId;
	}
	
	
	
}