package com.cycle30.prov.config;

/** This is direct configuration for the c30_prov_feature_def.
 * 
 * @author rnelluri
 *
 */
public class C30ProvFeatureItem {

	Integer featureItemId;
	String featureItemName;
	String featureItemRequestClassName;
	String featureItemResourceClassName;
	
	
	public String getFeatureItemRequestClassName() {
		return featureItemRequestClassName;
	}


	public void setFeatureItemRequestClassName(String featureItemRequestClassName) {
		this.featureItemRequestClassName = featureItemRequestClassName;
	}


	public String getFeatureItemResourceClassName() {
		return featureItemResourceClassName;
	}


	public void setFeatureItemResourceClassName(String featureItemResourceClassName) {
		this.featureItemResourceClassName = featureItemResourceClassName;
	}


	public Integer getFeatureItemId() {
		return featureItemId;
	}


	public void setFeatureItemId(Integer featureItemId) {
		this.featureItemId = featureItemId;
	}


	public String getFeatureItemName() {
		return featureItemName;
	}


	public void setFeatureItemName(String featureItemName) {
		this.featureItemName = featureItemName;
	}



	public C30ProvFeatureItem(Integer featureItemId, String featureItemName,
			String featureItemRequestClassName,
			String featureItemResourceClassName) {
		super();
		this.featureItemId = featureItemId;
		this.featureItemName = featureItemName;
		this.featureItemRequestClassName = featureItemRequestClassName;
		this.featureItemResourceClassName = featureItemResourceClassName;
	}


	@Override
	public String toString() {
		return "C30ProvFeatureItem [featureItemId=" + featureItemId
				+ ", featureItemName=" + featureItemName
				+ ", featureItemRequestClassName="
				+ featureItemRequestClassName
				+ ", featureItemResourceClassName="
				+ featureItemResourceClassName + "]";
	}


	
	
}
