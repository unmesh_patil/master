package com.cycle30.prov.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cycle30.prov.C30ProvConnectorBuildVersion;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;

/** This holds all the provisioning config
 * 
 * @author rnelluri
 *
 */
public class C30ProvConfig {

	private static C30ProvConfig instance=null;
	private static Logger log = Logger.getLogger(C30ProvConfig.class);
	private static HashMap<Integer, C30ProvFeatureItem> featureItemList = new HashMap();
	private static HashMap<Integer, C30ProvFeature> featureList = new HashMap();
	private static HashMap<String, C30ProvFeatureCatalogMap> provfeatureCatalogList = new HashMap();
	private C30JDBCDataSource datasource  = null;
	private static HashMap<Integer, C30ProvSystemParameters> ProvSystemParameterList = new HashMap();


	public  HashMap<Integer, C30ProvFeatureItem> getFeatureItemList() {
		return featureItemList;
	}

	public  HashMap<Integer, C30ProvFeature> getFeatureList() {
		return featureList;
	}

	public  HashMap<String, C30ProvFeatureCatalogMap> getProvfeaturePlanList() {
		return provfeatureCatalogList;
	}

	
	
	public  HashMap<Integer, C30ProvSystemParameters> getProvSystemParameterList() {
		return ProvSystemParameterList;
	}

	public static C30ProvConfig getInstance() throws C30ProvConnectorException
	{
		if(instance == null)
			instance = new C30ProvConfig();
		return instance;
	}

	private C30ProvConfig() throws C30ProvConnectorException
	{
		log.error("C30_Prov_Connector Build Version : "+ C30ProvConnectorBuildVersion.LATEST_BUILD_VERSION);

		try
		{
			datasource  = C30SDKDataSourceUtils.getDataSourceFromSDKPool();
			//This loads all the config.
			log.debug("Loading Provisioning config Started...");
			//Load c30featureItem table
			loadC30ProvFeatureItemTable();

			//Load c30FeatureTable
			loadC30ProvFeatureTable();

			//Load c30FeaturePlan Table
			loadC30ProvFeaturePlanTable();

			//Load c30FeaturePlan Table
			loadC30SystemParametersTable();
			
			//Log the feature list.
			log.debug("Loading Provisioning config completed...");
			

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-004");
		}
		finally{
			//Return the connection back to pool
			try {
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(datasource.getConnection());
			} catch (Exception e) {
				log.error("Exception: "+e.getMessage());
				throw new C30ProvConnectorException(e, "PROV-004");
			}
		}
		
	}

	/** Load all the feature_Plan config
	 * @throws C30ProvConnectorException 
	 * 
	 */
	private void loadC30ProvFeaturePlanTable() throws C30ProvConnectorException {

		log.info("Fetching all the loadC30ProvFeatureItemTable data from database");
		String query = SQLConstants.PROV_QUERY_GET_PROV_FEATURE_CATALOG_CONFIG;
		log.debug("Executing Query : "+ query);
		ResultSet resultSet = null;
		PreparedStatement ps = null;

		try
		{

			ps = datasource.getPreparedStatement(query);
			resultSet  = ps.executeQuery();

			while (resultSet.next()) 
			{
				Integer featureId  = resultSet.getInt("feature_id");
				Integer rollbackFeatureId  = resultSet.getInt("rollback_feature_id");
				Integer emfConfigId  = resultSet.getInt("emf_config_id");
				Integer acctSegId  = resultSet.getInt("acct_seg_id");
				Integer provSystemId  = resultSet.getInt("provision_system_id");
				String catalogId = resultSet.getString("catalog_id");
				String catalogType = resultSet.getString("catalog_type");
				String catalogActionName= resultSet.getString("catalog_action_name");

				C30ProvFeatureCatalogMap fPlanMap = new C30ProvFeatureCatalogMap(featureList.get(featureId), featureList.get(rollbackFeatureId),
						emfConfigId, catalogId,catalogType, acctSegId, provSystemId);


					provfeatureCatalogList.put(catalogId+catalogActionName+provSystemId, fPlanMap);
			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-004");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}

			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30ProvConnectorException(ex, "PROV-004");
			}
		}		

	}

	/** this load all the data from the c30ProvFeatureTable
	 * @throws C30ProvConnectorException 
	 * 
	 */
	private void loadC30ProvFeatureTable() throws C30ProvConnectorException {

		log.debug("Fetching all the loadC30ProvFeatureTable data from database");
		String query = SQLConstants.PROV_QUERY_GET_PROV_FEATURE_CONFIG;
		log.debug("Executing Query : "+ query);
		ResultSet resultSet = null;
		PreparedStatement ps = null;

		try
		{

			ps = datasource.getPreparedStatement(query);
			resultSet  = ps.executeQuery();

			Integer oldFeatureId = null;
			String oldFeatureName = null;
			Integer  featureId = null;
			String  featureName = null;

			String prevResClassId = null;
			ArrayList<FeatureActions> fActionList = new ArrayList();
			HashMap <String, ArrayList<FeatureActions>> resList1 = new HashMap();
			ArrayList<ArrayList<FeatureActions>> resList = new ArrayList();

			while (resultSet.next()) 
			{
				featureId  = resultSet.getInt("feature_id");
				featureName  = resultSet.getString("feature_name");
				Integer featureItemId  = resultSet.getInt("feature_item_id");
				Integer featureItemActionId  = resultSet.getInt("feature_item_action_id");
				Integer sequenceId = resultSet.getInt("sequence_id");
				Integer featureItemSPSPlanId = resultSet.getInt("feature_item_sps_plan_id");
				String featureItemResClassName = resultSet.getString("class_name");

				FeatureActions faction  = new FeatureActions( featureItemList.get(featureItemId), featureItemActionId, sequenceId, featureItemSPSPlanId);

				if (oldFeatureId != null  && !oldFeatureId.equals(featureId))
				{
					// In this case we group all the requests in to a single Group...
					resList.add( fActionList);

					C30ProvFeature feature = new C30ProvFeature(oldFeatureId,oldFeatureName, resList);
					featureList.put(oldFeatureId, feature);

					//Not Same as previous featureId 
					fActionList = new ArrayList();
					resList = new ArrayList();
					fActionList.add(faction);
					
				}


				if (oldFeatureId == null || oldFeatureId.equals(featureId) ) 
				{

					if (prevResClassId != null && !prevResClassId.equalsIgnoreCase(featureItemResClassName))
					{
						// In this case we group all the requests in to a single Group...
						resList.add( fActionList);
						fActionList = new ArrayList();
						// for the first featureId
						fActionList.add(faction);
					}
					else
					{
						//resList.put(featureItemResClassName, fActionList);
						// for the first featureId
						fActionList.add(faction);
					
					}

				}

				oldFeatureId =featureId;
				oldFeatureName = featureName;
				prevResClassId   = featureItemResClassName;
			}//END WHILE
			resList.add( fActionList);
			C30ProvFeature feature = new C30ProvFeature(featureId,featureName, resList);

			featureList.put(featureId, feature);


		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-004");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30ProvConnectorException(ex, "PROV-004");
			}
		}				
	}

	/** This loads all the c30ProvFeatureItemTable
	 * @throws C30ProvConnectorException 
	 * 
	 */
	private void loadC30ProvFeatureItemTable() throws C30ProvConnectorException {

		String query = SQLConstants.PROV_QUERY_GET_PROV_FEATURE_ITEM_DEF;
		log.debug("Executing Query : "+ query);
		ResultSet resultSet = null;
		PreparedStatement ps = null;

		try
		{
			ps = datasource.getPreparedStatement(query);
			resultSet  = ps.executeQuery();

			while (resultSet.next()) 
			{
				Integer featureItemId  = resultSet.getInt("feature_item_id");
				String featureItemName = resultSet.getString("feature_item_name");
				String featureItemReqClassName = resultSet.getString("feature_item_req_class_name");
				String featureItemResClassName = resultSet.getString("feature_item_res_class_name");
				C30ProvFeatureItem fItem = new C30ProvFeatureItem(featureItemId, featureItemName, featureItemReqClassName,featureItemResClassName);

				featureItemList.put(featureItemId, fItem);
			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-004");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}

			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30ProvConnectorException(ex, "PROV-004");
			}
		}		
	}
	
	/** This loads all the C30ProvSystemParametersTable
	 * @throws C30ProvConnectorException 
	 * 
	 */
	private void loadC30SystemParametersTable() throws C30ProvConnectorException {

		String query = SQLConstants.PROV_QUERY_GET_BIL_SYSTEM_PARAMETERS;
		log.debug("Executing Query : "+ query);
		ResultSet resultSet = null;
		PreparedStatement ps = null;

		try
		{
			ps = datasource.getPreparedStatement(query);
			resultSet  = ps.executeQuery();
			
			int i=0;
			while (resultSet.next()) 
			{
				String moduleName  = resultSet.getString("module_name");
				String paramName  = resultSet.getString("param_name");
				String paramValue  = resultSet.getString("param_value");
				Integer acctSegmentId  = resultSet.getInt("account_segment");
				String attribute1  = resultSet.getString("attribute1");	
				String attribute2  = resultSet.getString("attribute2");
				String attribute3  = resultSet.getString("attribute3");				
				C30ProvSystemParameters paramList = new C30ProvSystemParameters(moduleName, paramName, paramValue,
						acctSegmentId, attribute1, attribute2, attribute3);

				ProvSystemParameterList.put(i, paramList);
				i++;
			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-004");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}

			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30ProvConnectorException(ex, "PROV-004");
			}
		}		
	}	
}
