package com.cycle30.prov.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cycle30.prov.C30ProvConnectorBuildVersion;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.data.SQLConstants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;

/** This holds all the provisioning related to DCN config
 * 
 * 
 *
 */
public class C30ProvCustomConfig {

	private static C30ProvCustomConfig instance=null;
	private static Logger log = Logger.getLogger(C30ProvCustomConfig.class);
	private C30JDBCDataSource datasource  = null;
	private static HashMap<String, C30ProvSubscriberProfileDCN> subscriberProfileList = new HashMap();
	


	public  HashMap<String, C30ProvSubscriberProfileDCN> getSubscriberProfileList() {
		return subscriberProfileList;
	}

	
	public static C30ProvCustomConfig getInstance() throws C30ProvConnectorException
	{
		if(instance == null)
			instance = new C30ProvCustomConfig();
		return instance;
	}

	private C30ProvCustomConfig() throws C30ProvConnectorException
	{
		log.error("C30_Prov_Connector Build Version : "+ C30ProvConnectorBuildVersion.LATEST_BUILD_VERSION);

		try
		{
			datasource  = C30SDKDataSourceUtils.getDataSourceFromSDKPool();
			//This loads all the config.
			log.debug("Loading Provisioning config Started...");
			//Load C30_PROV_C_DCN_SUBSCR_PROFILE table
			loadC30ProvDCNsubscriberProfileTable();

			log.debug("Loading DCN Provisioning config completed...");

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-004");
		}
		finally {
			try 
			{
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(datasource.getConnection());
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30ProvConnectorException(ex, "PROV-004");
			}
		}
		
		
	}

	/** Load all the feature_Plan config
	 * @throws C30ProvConnectorException 
	 * 
	 */
	private void loadC30ProvDCNsubscriberProfileTable() throws C30ProvConnectorException {

		log.info("Fetching all the loadC30ProvFeatureItemTable data from database");
		String query = SQLConstants.PROV_QUERY_GET_DCN_SUBSCRIBER_PROFILE;
		log.debug("Executing Query : "+ query);
		ResultSet resultSet = null;		
		PreparedStatement ps = null;

		try
		{
			
			ps = datasource.getPreparedStatement(query);
			resultSet  = ps.executeQuery();

			while (resultSet.next()) 
			{
				String accountNumber  = resultSet.getString("account_no");
				Integer acctSegId  = resultSet.getInt("account_seg_id");
				String subsciberProfileId= resultSet.getString("subscriber_profile_id");
				String subsciberProfileName= resultSet.getString("subscriber_profile_name");
				String roamingProfileId= resultSet.getString("roaming_profile_id");
				String fixedIpAddress= resultSet.getString("fixed_ip_address");
				String susbscriberType= resultSet.getString("subscriber_type");
				String networkType= resultSet.getString("network_type");
				Integer isDefault  = resultSet.getInt("is_default");
				String catalogId  = resultSet.getString("catalog_id");
				C30ProvSubscriberProfileDCN susbciberprofileDCN = new C30ProvSubscriberProfileDCN(accountNumber, acctSegId,
						subsciberProfileId, subsciberProfileName,roamingProfileId, fixedIpAddress, susbscriberType,networkType, isDefault, catalogId);

				if (isDefault.equals(new Integer(1)))
						subscriberProfileList.put(acctSegId.toString()+"_"+accountNumber, susbciberprofileDCN);
				subscriberProfileList.put(acctSegId.toString()+"_"+accountNumber+"_"+catalogId, susbciberprofileDCN);
				
				
			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			throw new C30ProvConnectorException(e, "PROV-004");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}		

	}

}
