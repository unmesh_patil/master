package com.cycle30.prov.data;


/** Holds the data constants used in this particular module
 * 
 * @author rnelluri
 *
 */
public class Constants {

	/** Store the exception/ error property file name */
	public static String C30_ERROR_TEXT_FILE_NAME = "C30Prov_Error_Text"; 
	public static String C30_OCS_ERROR_TEXT_FILE_NAME = "C30OCS_Error_Text";
	
	/** Store the Property file name */
	public static String C30_PROPERTY_TEXT_FILE_NAME ="c30sdk";

	/** AES Encryption and Decryption stuff **/
	public final static String C30_DECRYPTION_ALOG = "PBEWithMD5AndDES";

	/** Holds all the registered vendors */
	public static final Integer VENDOR_AWN_SPS = new Integer(1001);	
	public static final Integer VENDOR_GCI_SPS = new Integer(1002);	
	public static final Integer VENDOR_GCI_TRIAD = new Integer(1003);
	public static final Integer VENDOR_AWN_OCS = new Integer(1004);	
	public static final Integer VENDOR_AWN_PREPAID_SPS = new Integer(1005);
	public static final Integer VENDOR_AWN_PREPAID_INCOMM = new Integer(1006);
	public static final Integer VENDOR_DCN_GLOBECOMM = new Integer(1007);
	public static final Integer VENDOR_AWN_PREPAID_BLACKHAWK = new Integer(1008);
	public static final Integer VENDOR_AWN_PREPAID_ACS = new Integer(1009);
	

	public static final Integer	VIEW_STATUS_CURRENT = new Integer(1);
	public static final Integer	VIEW_STATUS_OLD = new Integer(0);
	
	public static final String HTTP_POST = "POST";
	public static final String HTTP_GET = "GET";
	public static final String HTTP_PUT = "PUT";
	public static final String HTTP_DELETE = "DELETE";

	
	// Generic OCS XML header used for all request.
    public static final String OCS_XML_HEADER = "<?xml version=\"1.0\"?>\n<methodCall>";
    public static final String OCS_RESPONSE_XML_HEADER = "<?xml version=\"1.0\"?>";
    public static final String OCS_RESPONSE_METHODS_TAG = "<methodResponse xmlns:ex='http://ws.apache.org/xmlrpc/namespaces/extensions'>";
 
    public static final String OCS_XML_HEADER_EXTN = "<params>\n<param>\n<value>";
    
    public static final String OCS_XML_STRUCT_LABEL_EXTN = "</value></param><param><value><array><data><value><struct><member><name>BalanceTypeLabel</name><value>";
    public static final String OCS_XML_STRUCT_LABEL_EXTN_NO_ARRAY = "</value></param><param><value><struct><member><name>BalanceTypeLabel</name><value>";
    public static final String OCS_XML_STRUCT_VALUE_EXTN="</value></member><member><name>Value</name><value>";
    public static final String OCS_XML_STRUCT_NO_VALUE_EXTN_EXPIRATION_EXTN="</value></member><member><name>ExpirationDate</name><value>";
    public static final String OCS_XML_STRUCT_EXPIRATION_EXTN="</value></member><member><name>ExpirationDate</name><value>"; 
    public static final String OCS_STRUCT_END_EXPIRYDATE_EXTN_TAG="</value></member></struct></value></data></array></value>";
    public static final String OCS_STRUCT_END_EXPIRYDATE_EXTN_TAG_NO_ARRAY="</value></member></struct></value>";
    public static final String OCS_STRUCT_END_FOOTER="</value></member></struct></value></data></array></value></param><param><value>";
    
    public static final String OCS_XML_GENERIC_EXTN="</value></param><param><value>";
	// Generic OCS XML footer used for all request.
    public static final String OCS_XML_FOOTER = "</value>\n</param>\n</params>\n</methodCall>";
    public static final String OCS_XML_FOOTER_EXPIRY_DATE = "</param></params>\n</methodCall>";
    
    public static final String OCS_XML_FOOTER_NO_VALUES = "<params></params>\n</methodCall>";
    
 // Generic OCS XML header used for all request.
    public static final String GCI_XML_HEADER = "<?xml version=\"1.0\"  encoding=\"UTF-8\"?>"; 
    public static final String GCI_XML_HEADER_EXTN = "<AuthenticationRequest>";
    public static final String GCI_XML_FOOTER = "</AuthenticationRequest>";
    public static final String PREPAID_GCI_ATTRIBUTE_PASSWORD="Password";
    public static final String PREPAID_GCI_PASSWORD_VALUE="pos04use";
    
    //GCI MulePOS XML for POST
    
    public static final String GCI_MULE_POS_XML_HEADER_TAG="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Inventory><PhoneNumberInventory><Inventory>";
    public static final String GCI_MULE_POS_XML_FOOTER_TAG = "</Inventory></PhoneNumberInventory></Inventory>";
    public static final String GCI_MULE_POS_XML_TELEPHONE_NUMEBR_TAG="TelephoneNumber";
    public static final String GCI_MULE_POS_XML_STATUS_ID_TAG="Status";
    public static final String GCI_MULE_POS_XML_SALES_CHANNEL_ID_TAG="SalesChannelId";
    public static final String GCI_MULE_POS_XML_OPERATOR_ID_TAG="OperatorId";
    public static final String GCI_MULE_POS_XML_DISCONNECT_REASON_TAG="DisconnectReason";
    public static final String GCI_MULE_POS_XML_REQ_SYSTEM_ID="ReqSystemId";
    
    
    
   //ACS TN Inventory XML for POST
    
    public static final String PREPAID_ACS_XML_HEADER_TAG="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Inventory><PhoneNumberInventory><Inventory>";
    public static final String PREPAID_ACS_XML_FOOTER_TAG = "</Inventory></PhoneNumberInventory></Inventory>";
    public static final String PREPAID_ACS_XML_TELEPHONE_NUMEBR_TAG="TelephoneNumber";
    public static final String PREPAID_ACS_XML_STATUS_ID_TAG="Status";
    public static final String PREPAID_ACS_XML_SALES_CHANNEL_ID_TAG="SalesChannelId";
    public static final String PREPAID_ACS_XML_OPERATOR_ID_TAG="OperatorId";

    
    
    //Generic Incomm header
    public static final String INCOMM_XML_HEADER="<?xml version=\"1.0\" encoding=\"UTF-8\"?><TransferredValueTxn><TransferredValueTxnReq><ReqCat>TransferredValue</ReqCat>";
    public static final String INCOMM_XML_FOOTER = "</TransferredValueTxnReq></TransferredValueTxn>";
    
    //Generic Blackhawk header
    
    public static final String BLACKHAWK_XML_HEADER="<?xml version=\"1.0\" encoding=\"UTF-8\"?><request> " +
            "\n<header>" +
            "   \n <signature>BHNUMS</signature>" +
            "   \n <details>" +
            "   \n <productCategoryCode>01</productCategoryCode>" + 
            "   \n       <specVersion>04</specVersion>" +
            "    \n   </details>" +
            "\n</header>" +
            "   \n<transaction>\n";
    public static final String BLACKHAWK_XML_FOOTER="</transaction></request>";

    
    //Generic GLOBECOMM Header 
    
    public static final String GLOBECOMM_XML_HEADER="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:subscriber=\"http://subscriber.adapter.ws.provisioning.sixdee.com\" xmlns:commonDTO=\"http://common.dto.ws.provisioning.sixdee.com/xsd\" xmlns:subscriberDTO=\"http://subscriber.dto.ws.provisioning.sixdee.com/xsd\">" +
                                                  "<soapenv:Header/><soapenv:Body>"	;
    public static final String GLOBECOMM_XML_FOOTER="</soapenv:Body></soapenv:Envelope>";
    
    public static final String GLOBECOMM_SOAP_ACTION_HEADER_NAME="SOAPAction";
    public static final String GLOBECOMM_SOAP_ACTION_HEADER_VALUE="urn: AddSubscriber";
    public static final String GLOBECOMM_ADD_SUBSCRIBER_URI="/AddSubscriber";
    
    public static final String GLOBECOMM_SUBSCRIBER_NAMESPACE="subscriber:";
    public static final String GLOBECOMM_COMMON_DTO_NAMESPACE="commonDTO:";
    public static final String GLOBECOMM_SUBSCRIBER_DTO_NAMESPACE="subscriberDTO:";
    
    //enclosing GLOBECOMM Add Subscriber root Tags 
    public static final String GLOBECOMM_ADD_SUBSCRIBER_START_ROOT_TAG="<subscriber:addSubscriber><subscriber:requestDTO>";
    public static final String GLOBECOMM_ADD_SUBSCRIBER_END_ROOT_TAG="</subscriber:requestDTO></subscriber:addSubscriber>";
    
    //enclosing GLOBECOMM COMMON DATA
    public static final String GLOBECOMM_COMMON_DATA_SET_TAG="commonDTO:dataSet";
    
    
    public static final String GLOBECOMM_COMMON_DATA_SET_PARAM_NAME_TAG="commonDTO:paramName";
    public static final String GLOBECOMM_COMMON_DATA_SET_PARAM_VALUE_TAG="commonDTO:paramValue";
    
    public static final String GLOBECOMM_COMMON_DTO_REQUEST_ID_TAG="commonDTO:requestId";
    public static final String GLOBECOMM_COMMON_DTO_SOURCE_NODE_TAG="commonDTO:sourceNode";
    public static final String GLOBECOMM_COMMON_DTO_TIME_STAMP_TAG="commonDTO:timestamp";
    
    
    //enclosing GLOBECOMM SUBSCRIBER DTO DATA  Tags
    public static final String GLOBECOMM_ADD_SUBSCRIBER_DTO_DATA_TAG="subscriberDTO:subscriberData";
    
    public static final String GLOBECOMM_SUBSCRIBER_DTO_OPERATOR_TAG="subscriberDTO:operator";
    public static final String GLOBECOMM_SUBSCRIBER_FIXED_IP_TAG="subscriberDTO:fixedIPAddress";
    public static final String GLOBECOMM_SUBSCRIBER_NETWORK_TYPE_TAG="subscriberDTO:networkType";
    public static final String GLOBECOMM_SUBSCRIBER_PROFILE_ID_TAG="subscriberDTO:profileId";
    public static final String GLOBECOMM_SUBSCRIBER_ROAMING_PROFILE_ID_TAG="subscriberDTO:roamingProfileId";
    public static final String GLOBECOMM_SUBSCRIBER_TYPE_TAG="subscriberDTO:subscriberType";
    
    //enclosing GLOBECOMM SUBSCRIBER DTO INFO  Tags
    public static final String GLOBECOMM_ADD_SUBSCRIBER_DTO_INFO_TAG="subscriberDTO:subscriberInfo";
    public static final String GLOBECOMM_SUBSCRIBER_IMSI_TAG="subscriberDTO:imsi";
    public static final String GLOBECOMM_SUBSCRIBER_MSIDN_TAG="subscriberDTO:msisdn";
    
    
    
    // Add Subcriber DCN -GLOBECOMM paramaters
    
    public static final String GLOBECOMM_REQUEST_ID="requestId";
    public static final String GLOBECOMM_SOURCE_NODE="sourceNode";
    public static final String GLOBECOMM_TIME_STAMP="timestamp";
    public static final String GLOBECOMM_SUBSCRIBER_OPERATOR="operator";
    public static final String GLOBECOMM_SUBSCRIBER_FIXED_IP="fixedIPAddress";
    public static final String GLOBECOMM_SUBSCRIBER_NETWORK_TYPE="networkType";
    public static final String GLOBECOMM_SUBSCRIBER_PROFILE_ID="profileId";
    public static final String GLOBECOMM_SUBSCRIBER_ROAMING_PROFILE_ID="roamingProfileId";
    public static final String GLOBECOMM_SUBSCRIBER_TYPE="subscriberType";
    public static final String GLOBECOMM_SUBSCRIBER_IMSI="IMSI";
    public static final String GLOBECOMM_SUBSCRIBER_MSIDN="MSISDN";
    
    
   
    
    
    public static final String INCOMM_XML_TAG_REQUEST_ACTION="ReqAction";
    public static final String INCOMM_XML_TAG_DATE="Date";
    public static final String INCOMM_XML_TAG_TIME="Time";
    public static final String INCOMM_XML_TAG_PARTNER_NAME="PartnerName";
    public static final String INCOMM_XML_TAG_CARD_ACTION_INFO="CardActionInfo";
    public static final String INCOMM_XML_TAG_PIN="PIN";
    public static final String INCOMM_XML_TAG_SRC_REF_NUM="SrcRefNum";
    
    
    public static final String BLACKHAWK_XML_TAG_MESSAGE_IDENTIFIER_TYPE="f0";
    public static final String BLACKHAWK_XML_TAG_PRIMARY_ACCOUNT_NUMBER="primaryAccountNumber";
    public static final String BLACKHAWK_XML_TAG_PROCESSING_CODE="processingCode";
    public static final String BLACKHAWK_XML_TAG_TRANSACTION_AMOUNT="transactionAmount";
    public static final String BLACKHAWK_XML_TAG_TRANSMISSION_DATE_TIME_YYMMDDHHMMSS="transmissionDateTime";
    public static final String BLACKHAWK_XML_TAG_SYSTEM_TRACE_AUDIT="systemTraceAuditNumber";
    public static final String BLACKHAWK_XML_TAG_LOCAL_TRANSACTION_TIME="localTransactionTime";
    public static final String BLACKHAWK_XML_TAG_LOCAL_TRANSACTION_DATE="localTransactionDate";
    public static final String BLACKHAWK_XML_TAG_MERCHANT_CODE="merchantCategoryCode";
    public static final String BLACKHAWK_XML_TAG_POINT_OF_SERVICE_ENTRY_MODE="pointOfServiceEntryMode";
    public static final String BLACKHAWK_XML_TAG_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE="acquiringInstitutionIdentifier";
    //public static final String BLACKHAWK_XML_TAG_TRACK_2_DATA="f35";
    public static final String BLACKHAWK_XML_TAG_RETRIEVAL_REF_NUMBER="retrievalReferenceNumber";
    public static final String BLACKHAWK_XML_TAG_MERCHANT_TERMINAL_ID="merchantTerminalId";
    public static final String BLACKHAWK_XML_TAG_MERCHANT_IDENTIFIER="merchantIdentifier";
    //public static final String BLACKHAWK_XML_TAG_CARD_STORE_ID="f41.1";
    //public static final String BLACKHAWK_XML_TAG_CARD_TERM_ID="f41.2";
    //public static final String BLACKHAWK_XML_TAG_CARD_ACCEPTOR_IDENTIFICATION_CODE="f42";
    public static final String BLACKHAWK_XML_TAG_MERCHANT_LOCATION="merchantLocation";
    public static final String BLACKHAWK_XML_TAG_TRANSACTION_CURRENCY_CODE="transactionCurrencyCode";
    public static final String BLACKHAWK_XML_TAG_ADDITIONAL_TRANSACTION_FIELDS="additionalTxnFields";
    public static final String BLACKHAWK_XML_TAG_PRODUCT_ID="productId";
    public static final String BLACKHAWK_XML_TAG_REDEMPTION_PIN="redemptionPin";
    
	public final static String PREPAID_PARAM_PRIMARY_ACCOUNT_NUMBER = "PRIMARY_ACCOUNT_NMBR"; 
    public final static String PREPAID_PARAM_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE = "AQUIRING_ID_CODE";
	public final static String PREPAID_PARAM_MERCHANT_TERMINAL_ID = "MERCHANT_TERMINAL_ID";
	public final static String PREPAID_PARAM_PRODUCT_ID = "PRODUCT_ID_UPC";
	
	
	
    
    public final static String PROV_SYSTEM_PARAM_MODULE_BLACKHAWK = "BLACKHAWK";    
    
    
    public final static String BLACKHAWK_REVERSAL_URI="/reverse";

    
    		
	/** Auth Client attributes **/
	public final static String PROV_AUTH_CLIENT_SYSTEM_ID = "";
	public final static String PROV_AUTH_CLIENT_WEBURL = "";
	public final static String PROV_AUTH_CLIENT_USER_NAME = "";
	public final static String PROV_AUTH_CLIENT_USER_PASSWORD = "";
	public final static String PROV_AUTH_CLIENT_ADDTL_INFO = "";
	public final static String PROV_AUTH_CLIENT_USER_SESSION_ID = "";
	public final static String PROV_AUTH_CLIENT_SESSION_EXP_DATE = "";

	// OCS Auth Client attributes
	public final static String OCS_AUTH_CLIENT_SYSTEM_ID = "";
	public final static String OCS_AUTH_CLIENT_WEBURL = "";
	public final static String OCS_AUTH_CLIENT_USER_NAME = "";
	public final static String OCS_AUTH_CLIENT_USER_PASSWORD = "";
	public final static String OCS_AUTH_CLIENT_ADDTL_INFO = "";
	public final static String OCS_AUTH_CLIENT_USER_SESSION_ID = "";
	public final static String OCS_AUTH_CLIENT_SESSION_EXP_DATE = "";
	

	public final static String REQUEST_IMSI_VERIFICATION = "IMSIVerifcation";
	public final static String REQUEST_VALIDATE_SIM = "ValidateSIM";
	public final static String EXTERNAL_TRANSACTION_ID = "ExternalTransactionId";
	public final static String C30_TRANSACTION_ID = "C30TransactionId";
	public final static String ACCT_SEG_ID = "AcctSegId";
	public final static String ACCOUNT_NUMBER = "AccountNumber";

	public final static String ICCID = "ICCID";
	public static final String IMSI = "IMSI";
	public final static String NEW_ICCID = "NewICCID";
	public final static String TELEPHONE_NUMBER = "TelephoneNumber";
	public final static String NEW_TELEPHONE_NUMBER = "NewTelephoneNumber";
	public final static String REQUEST_XML = "RequestXML";
	public final static String REQUEST_ID = "RequestId";
	public final static String RESPONSE_XML = "ResponseXML";
	public final static String SPS_PLAN_ID = "SPSPlanId";	
	
	
	//OCS Constants
	public final static String PROV_SYSTEM_PARAM_MODULE_OCS = "OCS";
	public final static String OCS_SUBSCRIBER_ID = "OCSSubscriberId";	
	public final static String OCS_SUBSCRIBER_LINE_ID = "OCSSubscriberLineId";
	public final static String SUBSCRIBER_ID = "SubscriberId";
	public final static String PREPAID_ACCOUNT_ACTION = "PrepaidAccountAction";
	public final static String PREPAID_ACCOUNT_HISTORY_ACTION = "history";
	public final static String PREPAID_ACCOUNT_SEARCH_ACTION = "search";
	public final static String PREPAID_IPTRUNK = "IpTrunk";
	public final static String PREPAID_PARAM_IPTRUNK = "IPTRUNK";
	public final static String PREPAID_PARAM_ACCESS_PHONE = "ACCESS_PHONE";
	public final static String PREPAID_PARAM_AUTO_PAY_FLAG = "AUTO_PAY_FLAG";	
	public final static String PREPAID_CHARGE_EVENT_LABEL="ChargeableEventLabel";	
	public static final String PREPAID_BALANCE_WARNING_SMS = "SendBalanceWarningsSms";
	public static final String PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS = "SendSubscriberExpiryWarningSms";	
	public static final String PREPAID_ATTRIB_WALLET_VALUE = "WalletValue";
	public static final String PREPAID_ATTRIB_BALANCE_EXPIRY_DATE = "BalanceExpirationDate";
	
	public static final String PREPAID_METERED_PLAN = "_MT_";

	public final static String VOUCHER_PIN="VoucherPIN";
	public final static String VOUCHER_ACTION="VoucherAction" ;
	public final static String VOUCHER_GUID="VoucherGUID" ;
	public final static String REVERSAL_REQUEST_ID="ReversalRequestId";
	
	
	
	public final static String SUBCRIBER_DCN_ACTION="SubscriberAction" ;
	
	
	public final static String VOUCHER_BLOCK_ACTION="block" ;
	public final static String VOUCHER_UNBLOCK_ACTION="unblock" ;
	public final static String VOUCHER_STATUS_ACTION="status";
	
	public final static String VOUCHER_STATUS_INCOMM_ACTION="StatInq";
	public final static String VOUCHER_REDEEM_INCOMM_ACTION="Redeem";
	
	public final static String VOUCHER_STATUS_BLACKHAWK_ACTION="315400";
	public final static String VOUCHER_REDEEM_BLACKHAWK_ACTION="615400";

	
	public final static String PREPAID_BALANCE_ACTION="BalanceAction";
	public final static String PREPAID_BALANCE_BALANCE_TYPE="BalanceTypeLabel";
	public final static String PREPAID_BALANCE_BALANCE_REASON="BalanceReason";
	public final static String PREPAID_BALANCE_BALANCE_Value="BalanceValue";
	public final static String PREPAID_BALANCE_EXPIRATION_DATE="ExpirationDate";
	public final static String PREPAID_BALANCE_VOUCHER_PHONE_ACCESS="AccessPhone";
	public final static String PREPAID_WALLET_BALANCE_LABEL="COMMON";
	public final static String PREPAID_BALANCE_CHARGING_EVENT_LABEL="ChargeableEventLabel";
	
	
	public final static String PREPAID_WALLET_REFILL="WalletRefill";
	public final static String PREPAID_VOUCHER_REDEEM="VoucherRedeem";
	public final static String PREPAID_BALANCE_REFILL="BalanceRefill";
	public final static String PREPAID_EXTEND_EXPIRYDATE="ExtendExpiryDate";
	public final static String PREPAID_PLAN_RECHARGE_RESERVE="PlanRechargeReserve";
	public final static String PREPAID_PLAN_CHANGE="PlanChange";	

	
	//GCI Prepaid Constants
	public final static String GCI_SESSION_AUTHENTICATE = "/authenticate";
	public final static String GCI_PHONE_INVENTORY_INQUIRY="/inventory/phones";
	
	
	public final static String PREPAID_GCI_ATTRIBUTE_COMMUNITY="Community";
	public final static String PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER="TelephoneNumber";
	public final static String PREPAID_GCI_ATTRIBUTE_INV_STATUS="InvStatus";
	public final static String PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID="SalesChannelId";
	public final static String PREPAID_GCI_ATTRIBUTE_OPERATOR_ID="OperatorId";
	public final static String PREPAID_GCI_ATTRIBUTE_NETWORK_ID="NetworkId";
	public final static String PREPAID_GCI_ATTRIBUTE_DISCONNECT_REASON="disconnectReason";
	public final static String PREPAID_GCI_ATTRIBUTE_REQ_SYSTEM_ID="reqSystemId";
	
	public final static String PREPAID_GCI_ATTRIBUTE_SIZE="Size";
	public final static String PREPAID_GCI_ATTRIBUTE_SESSION_ID="GCI-SessionId";
	public final static String PREPAID_GCI_ATTRIBUTE_INVENTORY_PAYLOAD="Payload";
	public static final String PREPAID_GCI_ATTRIBURE_ACTION_TYPE="ActionType";
	
	public final static String GCI_MULE_POS_PARAM_COMMUNITY="community";
	public final static String GCI_MULE_POS_PARAM_TELEPHONE_NUMBER="telephoneNumber";
	public final static String GCI_MULE_POS_PARAM_SALES_CHANNEL_ID="salesChannelId";
	public final static String GCI_MULE_POS_PARAM_INV_STATUS="invStatus";
	public final static String GCI_MULE_POS_PARAM_SIZE="size";
	
	public final static String PREPAID_GCI_HEADERS_CLIENT_ID="X-Client-ID";
	public final static String PREPAID_GCI_HEADERS_USER_ID="X-User-ID";
	public final static String PREPAID_GCI_HEADERS_SESSION_ID="X-Session-ID";
	
	
	// GCI Action constants
	public final static String GCI_MULE_POS_ACTION_UPDATE="";
	public final static String GCI_MULE_POS_ACTION_ASSIGN="";
	public final static String GCI_MULE_POS_ACTION_RESERVE="";
	
	// ACS Prepaid Constants
	public final static String ACS_PHONE_INVENTORY_INQUIRY="/inventory/phones";
	
	
	public final static String PREPAID_ACS_ATTRIBUTE_COMMUNITY="Community";
	public final static String PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER="TelephoneNumber";
	public final static String PREPAID_ACS_ATTRIBUTE_INV_STATUS="InvStatus";
	public final static String PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID="SalesChannelId";
	public final static String PREPAID_ACS_ATTRIBUTE_OPERATOR_ID="OperatorId";
	public final static String PREPAID_ACS_ATTRIBUTE_NETWORK_ID="NetworkId";
	
	public final static String PREPAID_ACS_ATTRIBUTE_SIZE="Size";
	public final static String PREPAID_ACS_ATTRIBUTE_SESSION_ID="GCI-SessionId";
	public final static String PREPAID_ACS_ATTRIBUTE_INVENTORY_PAYLOAD="Payload";
	public static final String PREPAID_ACS_ATTRIBURE_ACTION_TYPE="ActionType";
	
	public final static String PREPAID_ACS_PARAM_COMMUNITY="community";
	public final static String PREPAID_ACS_PARAM_TELEPHONE_NUMBER="telephoneNumber";
	public final static String PREPAID_ACS_PARAM_SALES_CHANNEL_ID="salesChannelId";
	public final static String PREPAID_ACS_PARAM_INV_STATUS="invStatus";
	public final static String PREPAID_ACS_PARAM_SIZE="size";
	
	public final static String PREPAID_ACS_HEADER_X_ORG_ID="X-Org-Id";
	public final static String PREPAID_ACS_HEADER_X_DIGEST="X-Digest";
	public final static String PREPAID_ACS_HEADER_DATE="Date";
	
	public final static String PREPAID_PARAM_SECRET_KEY = "SECRET_KEY";
	public final static String PREPAID_PARAM_ACS_ORG_ID = "ACS_ORG_ID";
	
	public final static String PROV_SYSTEM_PARAM_MODULE_ACSINV = "ACSINV";
	
	
	// ACS Action constants
	public final static String PREPAID_ACS_ACTION_UPDATE="";
	public final static String PREPAID_ACS_ACTION_ASSIGN="";
	public final static String PREPAID_ACS_ACTION_RESERVE="";
	

	public final static String SOURCE_SYSTEM_ID = "Cycle30";
	public final static String DEFAULT_VOICE_MAIL_PASSWORD = "1234";

	public final static String AWN_WIRELESS_PROVISION = "/WirelessProvision";
	public final static String AWN_WIRELESS_VALIDATION = "/WirelessValidate";
	public final static String AWN_WIRELESS_INQUIRY = "/WirelessInquiry";
	public final static String PROV_INQUIRY = "INQUIRY";
	public final static String PROV_PROVISION = "PROVISION";
	public final static String PROV_OCS_PROVISION = "OCS";
	public final static String PROV_MULEPOS_PREPAID_PROVISION = "MULEPOS";
	public final static String PROV_INCOMM_PROVISION = "INCOMM";
	public final static String PROV_GLOBECOMM_PROVISION = "GLOBECOMM";
	public final static String PROV_BLACKHAWK_PROVISION = "BLACKHAWK";
	public final static String PROV_ACS_PREPAID_TN_INV_PROVISION ="ACSINV";
	

	public final static String ATTR_FEATURE_ID="FeatureId";
	public final static String ATTR_FEATURE_ITEM_ID="FeatureItemId";
	public final static String ATTR_FEATURE_ITEM_ACTIONS="FeatureItemActions";
	public static final String ATTR_STATUS_ID = "StatusId";
	public static final String ATTR_PLAN_ID = "PlanId";
	public static final String ATTR_CATALOG_ID = "CatalogId";
	public static final String ATTR_REQUEST_NAME = "RequestName";   
	public static final String ATTR_PREAPID_ACTION = "PrepaidAction";	
	public static final String ATTR_CATALOG_ACTION_ID = "CatalogActionId";
	public static final String ATTR_REQUEST_ID = "RequestId";
	public static final String ATTR_RESPONSE_CODE = "ResponseCode";
	public static final String ATTR_SIMULATE_REQUEST = "SimulateRequest";
	public static final String C30_ACS_PREPAID_VOICE_VVM = "VVM_";
	
	
	public static final String ATTR_RESPONSE_SUCCESS = "SUCCESS";
	public static final String ATTR_RESPONSE_FAILURE = "FAILED";

	//Value Constants
	public final static Integer STATUS_REQUEST_INITIALIZED = new Integer(10);
	public final static Integer STATUS_REQUEST_SENT_TO_SPS = new Integer(20);
	public final static Integer STATUS_REQUEST_SIMULATE_SENT_TO_SPS = new Integer(21);
	public final static Integer STATUS_REQUEST_PROCESSING_AT_SPS = new Integer(30);
	public final static Integer STATUS_REQUEST_SYNC_RESPONSE_RECIEVED = new Integer(60);
	public final static Integer STATUS_REQUEST_ASYNC_RESPONSE_RECIEVED = new Integer(61);
	public final static Integer STATUS_REQUEST_SIMULATE_COMPLETED = new Integer(81);
	public final static Integer STATUS_REQUEST_IN_ERROR = new Integer(99);
	public final static Integer STATUS_REQUEST_SIMULATE_IN_ERROR = new Integer(98);
	public final static Integer STATUS_REQUEST_COMPLETED = new Integer(80);
	
	public final static String STATUS = "Status";
	public final static String STATUS_REQUEST_IS_COMPLETED = "COMPLETED";
	public final static String STATUS_REQUEST_PENDING = "PENDING";
	public final static String STATUS_REQUEST_ERROR = "ERROR";
	
	public final static Integer FEATURE_ITEM_ACTION_TYPE_ADD = new Integer(1);
	public final static Integer FEATURE_ITEM_ACTION_TYPE_DISCONNECT = new Integer(2);
	public final static Integer FEATURE_ITEM_ACTION_TYPE_SUSPEND = new Integer(3);
	public final static Integer FEATURE_ITEM_ACTION_TYPE_RESUME = new Integer(4);
	public final static Integer FEATURE_ITEM_ACTION_TYPE_ON = new Integer(5);
	public final static Integer FEATURE_ITEM_ACTION_TYPE_OFF = new Integer(6);
	public final static Integer FEATURE_ITEM_ACTION_TYPE_SWAP = new Integer(7);
	public final static Integer FEATURE_ITEM_ACTION_TYPE_CHANGE = new Integer(8);
	
	public static final Integer ACCT_SEG_ID_1 = new Integer(1);
	public static final Integer ACCT_SEG_ID_2 = new Integer(2);
	public static final Integer ACCT_SEG_ID_3 = new Integer(3);
	public static final Integer ACCT_SEG_ID_4 = new Integer(4); // GCI PREPAID
	public static final Integer ACCT_SEG_ID_5 = new Integer(5); // ACS PREPAID	
	public static final Integer ACCT_SEG_ID_10 = new Integer(10); // DCN

	
	public static final String REQUEST_TELEPHONE_NUMBER_VERIFICATION = "TelephoneNumberInquiry";
	public static final String REQUEST_SUBSCRIBER_INQUIRY = "SubscriberInquiry";
	public static final String REQUEST_SPS_REQUEST_HISTORY_INQUIRY = "SPSRequestHistoryInquiry";
	public static final String PROVISION_REQUEST_TYPE = "ProvisionRequestType";
	public static final String REQUEST_SPS_REQUEST_STATUS_BY_REQUEST_ID = "SPSRequestStatusByRequestId";
	public static final String REQUEST_SPS_REQUEST_STATUS_BY_TRANSACTION_ID = "SPSRequestStatusByTransactionId";
	
	//Inquiry Actions
	public static final String SPS_INQUIRY_VALIDATE_SIM="validateSIM";
	public static final String SPS_INQUIRY_GET_IMSI="getIMSI";
	public static final String SPS_INQUIRY_VALIDATE_TN="validateTN";
	public static final String SPS_INQUIRY_GET_SERVICE_ORDER_HISTORY="getServiceOrderHistory";
	public static final String SPS_INQUIRY_GET_SUBSCRIBER_INFO	="getSubscriberInformation";

	//Provisioning XML Tags
	public static final String SPS_PROV_XML_TAG_ACTION = "action";
	public static final String SPS_PROV_XML_TAG_METHOD = "method";
	public static final String SPS_PROV_XML_TAG_SERVICE_TYPE = "serviceType";
	public static final String SPS_PROV_XML_TAG_SYSTEM_ID	 = "systemId";
	public static final String SPS_PROV_XML_TAG_ID_CARRIER	 = "idCarrier";
	public static final String SPS_PROV_XML_TAG_ID_REQUEST	 = "idRequest";
	public static final String SPS_PROV_XML_TAG_NUMBER = "number";
	public static final String SPS_PROV_XML_TAG_MESSAGE = "message";
	public static final String SPS_PROV_XML_TAG_SEND = "send";
	public static final String SPS_PROV_XML_TAG_ENABLED = "enabled";
	
	
	

	// Wireless XML Tags
	public static final String SPS_WIRELESS_XML_TAG_WIRELESS_VOICE = "wirelessVoice";
	public static final String SPS_WIRELESS_XML_TAG_WIRELESS_VOICE_MAIL = "voiceMail";
	public static final String SPS_WIRELESS_XML_TAG_WIRELESS_DATA = "wirelessData";
	public static final String SPS_WIRELESS_XML_TAG_WIRELESS_TEXT = "textMessaging";

	public static final String SPS_WIRELESS_XML_TAG_ICCID	 = "iccid";
	public static final String SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER	 = "telephoneNumber";
	public static final String SPS_WIRELESS_XML_TAG_NEW_ICCID	 = "newIccid";
	public static final String SPS_WIRELESS_XML_TAG_NEW_TELEPHONE_NUMBER	 = "newTelephoneNumber";
	public static final String SPS_WIRELESS_XML_TAG_IMSI	 = "imsi";
	public static final String SPS_WIRELESS_XML_TAG_PLAN_ID	 = "planId";
	public static final String SPS_WIRELESS_XML_TAG_CATEGORY	 = "category";
	public static final String SPS_WIRELESS_XML_TAG_PLATFORM_TYPE	 = "platformType";
	public static final String SPS_WIRELESS_XML_TAG_MAILBOX_NUMBER = "mailboxNumber";
	public static final String SPS_WIRELESS_XML_TAG_PASSWORD	 = "password";
	public static final String SPS_WIRELESS_XML_TAG_SUSPEND = "suspend";
	public static final String SPS_WIRELESS_XML_TAG_PREPAID_SUSPEND = "prepaidSuspend";	
	public static final String SPS_WIRELESS_XML_TAG_PROVISIONED  ="provisioned"; 
	public static final String SPS_WIRELESS_XML_TAG_SHORT_CODE_BLOCK  ="shortCodeBlock"; 
	public static final String SPS_WIRELESS_XML_TAG_SMS	 = "sms";
	
	public final static String PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD = "ACS_VM_DEFAULT_PASSWORD";
	public final static String PREPAID_PARAM_ACS_VVM_SPS_PLAN_ID = "ACS_VVM_SPS_PLAN_ID";
	public final static String PROV_SYSTEM_PARAM_MODULE_SPS = "SPS";
	
	
	//List of FeatureItems
	public static final Integer FEATURE_ITEM_BASIC_VOICE = new Integer (101);
	public static final Integer FEATURE_ITEM_SUSPEND_BASIC_VOICE = new Integer (102);
	public static final Integer FEATURE_ITEM_LONG_DISTANCE = new Integer (103);
	public static final Integer FEATURE_ITEM_INTERNATIONAL = new Integer (104);
	public static final Integer FEATURE_ITEM_ROAMING = new Integer (105);
	public static final Integer FEATURE_ITEM_INTERNATIONAL_ROAMING = new Integer (106);
	public static final Integer FEATURE_ITEM_LIFELINE_ROAMING_BLOCK = new Integer (107);
	public static final Integer FEATURE_ITEM_CALLER_ID = new Integer (108);
	public static final Integer FEATURE_ITEM_PREPAID = new Integer (109);
	public static final Integer FEATURE_ITEM_VISUAL_VOICE_MAIL = new Integer (110);
	public static final Integer FEATURE_ITEM_VOICEMAIL_TO_EMAIL = new Integer (111);
	public static final Integer FEATURE_ITEM_SMS = new Integer (112);
	public static final Integer FEATURE_ITEM_MMS = new Integer (113);
	public static final Integer FEATURE_ITEM_DATA_SERVICE = new Integer (114);
	public static final Integer FEATURE_ITEM_WAP = new Integer (115);
	public static final Integer FEATURE_ITEM_BLACKBERRY_INTERNET_CONSUMER = new Integer (116);
	public static final Integer FEATURE_ITEM_BLACKBERRY_INTERNET_LITE = new Integer (117);
	public static final Integer FEATURE_ITEM_BLACKBERRY_INTERNET_SOIAL = new Integer (118);
	public static final Integer FEATURE_ITEM_BLACKBERRY_INTERNET_COMMERCIAL = new Integer (119);
	public static final Integer FEATURE_ITEM_DOWNLOADABLE_CONTENT = new Integer (120);
	public static final Integer FEATURE_ITEM_EVOLUTION_DATA_OPTIMIZED = new Integer (121);	
	public static final Integer FEATURE_ITEM_MOBILE_BROADBAND_WITH_INTERNET = new Integer (122);
	public static final Integer FEATURE_ITEM_WIFI_ENTITLEMENT = new Integer (123);
	public static final Integer FEATURE_ITEM_TETHERING = new Integer (124);
	public static final Integer FEATURE_ITEM_VOICE_MAIL = new Integer (125);
	public static final Integer FEATURE_ITEM_SHORT_CODE_BLOCK = new Integer (126);
	public static final Integer FEATURE_ITEM_VERIFY_TEXT_SERVICE = new Integer (127);
	public static final Integer FEATURE_ITEM_VERIFY_DATA_SERVICE = new Integer (128);
	public static final Integer FEATURE_ITEM_VERIFY_VOICE_SERVICE = new Integer (129);
	public static final Integer FEATURE_ITEM_PREPAID_SUSPEND = new Integer (130);
	public static final Integer FEATURE_ITEM_VERIFY_VOICE_MAIL_SERVICE = new Integer (131);	
	
	//Wireless Voice XML Tags

	//Data XML Tags


	//DCN Tags
	public static final Integer FEATURE_ITEM_DCN_SUBSCRIBER = new Integer (140);	

	public static final String OCS_XML_METHOD_NAME = "methodName";
	public static final String ATTR_OCS_METHOD_NAME = "MethodName";


	//OCS Method - Subscriber
	public static final String OCS_SUBSCRIBER_ACTIVATE_BY_CLI = "Subscriber.ActivateByCLI";
	public static final String OCS_SUBSCRIBER_CREATE = "Subscriber.Create";
	public static final String OCS_SUBSCRIBER_CREDIT = "Subscriber.Credit";
	public static final String OCS_SUBSCRIBER_CREDITSET = "Subscriber.CreditSet";
	public static final String OCS_SUBSCRIBER_GET_BALANCES = "Subscriber.GetBalances";
	public static final String OCS_SUBSCRIBER_GET_BYID = "Subscriber.GetById";	
	public static final String OCS_SUBSCRIBER_GET_BYCLI = "Subscriber.GetByCLI";	
	public static final String OCS_SUBSCRIBER_LINE_GET_BYCLI = "SubscriberLine.GetByCLI";	
	public static final String OCS_SUBSCRIBER_HISTORY = "Subscriber.History";
	public static final String OCS_SUBSCRIBER_RECHARGE = "Subscriber.Recharge";
	public static final String OCS_SUBSCRIBER_RECHARGE_BY_CLI_PIN = "Subscriber.RechargeByCLIPIN";	
	public static final String OCS_SUBSCRIBER_REMOVE_BALANCE = "Subscriber.RemoveBalance";
	public static final String OCS_SUBSCRIBER_UPDATE = "Subscriber.Update";	
	public static final String OCS_SUBSCRIBER_UPDATE_WALLET_BALANCE_CREDIT ="Subscriber.Credit";
	public static final String OCS_SUBSCRIBER_UPDATE_WALLET_BALANCE_DEBIT="Subscriber.Debit";
	public static final String OCS_SUBSCRIBER_UPDATE_RECHARGE_VOUCHER="Subscriber.RechargeByCLIPIN";
	public static final String OCS_SUBSCRIBER_UPDATE_BALANCE_REFILL_CREDIT="Subscriber.Credit";
	public static final String OCS_SUBSCRIBER_UPDATE_BALANCE_REFILL_DEBIT="Subscriber.Debit";
	public static final String OCS_SUBSCRIBER_UPDATE_EXTENDEXPIRYDATE_REFILL="Subscriber.UpdateBalance";
	public static final String OCS_SUBSCRIBERLINE_GET_BYCLI = "SubscriberLine.GetByCLI";
	//OCS Method - Balances
	public static final String OCS_SUBSCRIBER_BALANCES = "Subscriber.GetBalances";
	public static final String OCS_SUBSCRIBER_UPDATEBALANCE = "UpdateBalances";
	
	//OCS Method - SubscriberLine
	public static final String OCS_SUBSCRIBER_LINE_UPDATE = "SubscriberLine.Update";
	public static final String OCS_SUBSCRIBER_LINE_DELETE = "SubscriberLine.Delete";
	
	//OCS Method - Charge Events
	public static final String OCS_EVENT_CHARGING_DIRECT_DEBIT = "EventCharging.DirectDebit";
	public static final String OCS_CHARGEABLE_EVENT_GET_ALL = "ChargeableEvent.GetAll";
	
	//OCS Method - Get all Balance Types used to set in Cache 
	public static final String OCS_SUBSCRIBER_BALNACE_UNITS_MAPPINGS = "BalanceType.GetAll";

	//OCS Method - Get all Profiles and Chargeable Events
	public static final String OCS_ALL_PROFILES="SubscriberProfile.GetAll";
	public static final String OCS_GET_PROFILE_BALANCE_TYPES="SubscriberProfile.GetByLabel";


	//WALLET REFILL Extend Days
	public static final String PREPAID_WALLET_REFILL_25 = "25";
	public static final String PREPAID_WALLET_REFILL_50 = "50";
	public static final String PREPAID_WALLET_REFILL_100 = "100";
	public static final String PREPAID_WALLET_REFILL_200 = "200";

	public static final String PREPAID_WALLET_REFILL_EXTEND_25 = "60";
	public static final String PREPAID_WALLET_REFILL_EXTEND_50 = "90";
	public static final String PREPAID_WALLET_REFILL_EXTEND_100 = "120";
	public static final String PREPAID_WALLET_REFILL_EXTEND_200 = "180";

	
	//OCS Methods - Voucher
	public static final String OCS_VOUCHER_STATUS = "Card.GetByPincode";
	public static final String OCS_VOUCHER_BLOCK = "Card.Block";
	public static final String OCS_VOUCHER_UNBLOCK = "Card.Unblock";
	
	public static final String OCS_FAULT_CODE_XML_TAG = "faultCode";
	public static final String OCS_FAULT_STRING_XML_TAG="faultString";
	
	//OCS REQUEST NAME
	public static final String REQUEST_OCS_PREPAID_SUBSCRIBER_BALANCE_INQUIRY="PrepaidBalance";
	public static final String REQUEST_OCS_PREPAID_SUBSCRIBER="PrepaidAccountSubscriber";
	public static final String REQUEST_OCS_PREPAID_SUBSCRIBER_PROFILE="PrepaidSubscriberProfile";	
	public static final String REQUEST_OCS_PREPAID_UPDATE_SUBSCRIBER="PrepaidUpdateAccountSubscriber";	
	public static final String REQUEST_OCS_PREPAID_ORDER_BAL_UPDATE="PrepaidOrderBalanceUpdate";	
	public static final String REQUEST_OCS_ACCOUNT_HISTORY="PrepaidAccountHistory";
	public static final String REQUEST_OCS_ACCOUNT_SEARCH="PrepaidAccountSearch";	
	
	//Balance Units Cache 
	public static final String REQUEST_OCS_PREPAID_BALANCE_UNITS_CACHE="CacheBalanceUnits";
	
	// Proflies and it Balance Type Cache
	public static final String REQUEST_OCS_PREPAID_PROFILES_CACHE="ProfilesCache";
	public static final String OCS_RESELLER_LABEL="ResellerLabel";	
	
	//Subscriber Details Cahce
	public static final String REQUEST_OCS_PREPAID_SUBSCRIBER_DETAILS="GetSubscriberDetails";
	
	public static final String REQUEST_OCS_PREPAID_VOUCHER_STATUS="PrepaidVoucher";
	public static final String REQUEST_OCS_PREPAID_VOUCHER_BLOCK_UNBLOCK="VoucherBlockUnBlock";
	
	public static final String REQUEST_INCOMM_PREPAID_VOUCHER_STATUS="IncommVoucher";
	
	public static final String REQUEST_BLACK_HAWK_PREPAID_VOUCHER_STATUS="BlackHawkVoucher";
	
	//GLOBECOMM 
	public static final String REQUEST_DCN_ADD_SUBSCRIBER="AddSubcriber";
	// Prepaid Kenan Phone Inventory Constants
	public static final String REQUEST_GCI_MULEPOS_SESSION_ID_INQUIRY="MulePOSInventorySessionId";
	public static final String REQUEST_GCI_PHONE_INVENTORY_INQUIRY="GCIPrepaidPhoneInventory";
	public static final String REQUEST_GCI_PHONE_INVENTORY_UPADTE="InventoryUpdatePrepaid";
	
	//Prepaid ACS Inventory Constants
	public static final String REQUEST_ACS_PHONE_INVENTORY_INQUIRY="ACSPrepaidPhoneInventory";
	public static final String REQUEST_ACS_PHONE_INVENTORY_UPADTE="ACSInventoryUpdatePrepaid";
	
	
	
	
	//OCS XML Tags
	public static final String OCS_INQUIRY_XML_TAG_METHOD = "methodName";
	public static final String OCS_INQUIRY_XML_INT_VALUE_TAG = "int";
	public static final String OCS_INQUIRY_XML_STRING_VALUE_TAG = "string";
	public static final String OCS_XML_TAG_STRING = "string";
	public static final String OCS_XML_TAG_INT = "int";	
	public static final String OCS_XML_TAG_ARRAY = "array";
	public static final String OCS_XML_TAG_STRUCT = "struct";
	public static final String OCS_XML_TAG_DOUBLE = "double";	
	public static final String OCS_XML_TAG_BOOLEAN = "boolean";	
	public static final String OCS_XML_TAG_NAME = "name";	
	public static final String OCS_XML_TAG_VALUE = "value";
	public static final String OCS_XML_TAG_MEMBER = "member";
	public static final String OCS_XML_TAG_PARAMS = "params";
	public static final String OCS_XML_TAG_LEVEL1 = "<params><param>";
	public static final String OCS_XML_TAG_LEVEL2 = "<params><param><value><struct>";
	public static final String OCS_XML_TAG_LEVEL3 = "<value><array><data><value><struct>";
	
	public static final String OCS_XML_TAG_LEVEL1_CLOSE = "<params><param>";
	public static final String OCS_XML_TAG_LEVEL2_CLOSE = "</struct></value></param></params>";
	public static final String OCS_XML_TAG_LEVEL3_CLOSE = "</struct></value></data></array></value>";
	
	//OCS Member Names
	public static final String PREPAID_SUBSCRIBER_PROFILE_LABEL = "SubscriberProfileLabel";
	public static final String PREPAID_PINCODE = "Pincode";
	public static final String PREPAID_FIRST_NAME = "FirstName";
	public static final String PREPAID_LAST_NAME = "LastName";
	public static final String PREPAID_SUBSCRIBERID = "SubscriberId";
	public static final String PREPAID_EXPIRATIONDATE = "ExpirationDate";
	public static final String PREPAID_SUBSCRIBER_LINES = "SubscriberLines";
	public static final String PREPAID_CLI = "CLI";
	public static final String PREPAID_NUMBERSUBTYPE = "NumberSubType";
	public static final String PREPAID_PREFERRED_LANG = "PreferredLang";
	public static final String PREPAID_IMSI = "IMSI";
	public static final String PREPAID_BALANCE_TYPE_LABEL = "BalanceTypeLabel";
	public static final String PREPAID_BALANCE_INITIAL_VALUE = "InitialValue";	
	public static final String PREPAID_CHARGE_EVENT_BALANCE_TYPE_LABEL="EventBalanceTypeLabel";	
	public static final String PREPAID_VALUE = "Value";	
	public final static String PREPAID_SUBSCRIBER_CLI="SubscriberCLI";	
	public final static String PREPAID_REASON="Reason";
	public final static String PREPAID_SUBSCRIBER_LINE_ID="SubscriberLineId";
	public final static String PREPAID_LABEL="Label";
	public final static String PREPAID_BALANCE_TYPE_COMMON="COMMON";
	public final static String PREPAID_ADD_ON_PKG_BALANCE="DATA_PKG";	
	public static final String PREPAID_OCS_NULL_VALUE = "<ex:nil/>";	
	
	
	//Other direct Term constants
	public static final String VOICEMAIL = "VOICEMAIL";
	public static final String WIRELESS = "WIRELESS";
	public static final String DISCONNECT = "DISCONNECT";
	public static final String DELETE = "DELETE";
	public static final String ADD = "ADD";
	public static final String CHANGE = "CHANGE";
	public static final String GSM = "GSM";
	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";
	public static final String GCI = "GCI";
	public static final String ACS = "ACS";
	public static final String GCI_PREPAID = "GCI.PREPAID";
	public static final String ACS_PREPAID = "ACS.PREPAID";
	public static final String DCN = "DCN";
	
	
	//public final static String SIM = "SIM";
	public static final String ACS_MAILBOX_NUMBER = "9074415100";
	public static final String GCI_MAILBOX_NUMBER = "9074440068";
	public static final String ACS_NO_REPLY_TIMER = "20";
	public static final String GCI_NO_REPLY_TIMER = "20";
	public static final long THREAD_SLEEP_TIME = 3000;
	// kmr 323150 
	public static final int THREAD_SLEEP_TIME_ITERATIONS = 100; // So that total timeout will be 300 sec..
	
	// SPS Service Status
	public static final String SPS_SERVICE_STATUS_ACTIVE = "Active";
	public static final String SPS_SERVICE_STATUS_INACTIVE = "Inactive";
	public static final String SPS_SERVICE_STATUS_SUSPENDED = "Suspended";
	public static final String SPS_VOICE_MAIL_STATUS_INACTIVE = "0";	
	public static final String SPS_VOICE_MAIL_STATUS_ACTIVE = "1";	
	public static final String EXCEP_PORT_IN_ERROR = "2009"; // This number is in the process of being ported out.
	public static final String BASIC_VOICE_RESOURCE="com.cycle30.prov.resource.sps.BasicVoiceResource";
	public static final String CATALOG_PREPAID="PREPAID SERVICE";
	public static final String CATALOG_ACTION_ADD="ADD";
	public static final String CDMASIM="89013113701405212386";
	//PW -43 Added by vijaya.
	public static final String OCS_SUBSCRIBER_DELETE = "Subscriber.Delete";
	//PW-65 Added by vijaya.
	public final static String TEXT_REQUEST_XML = "TextRequestXML";
	public final static String MAIL_REQUEST_XML = "MailRequestXML";
	public final static String DATA_REQUEST_XML = "DataRequestXML";
	public final static String TEXT_REQUEST_ID = "TextRequestID";
	public final static String MAIL_REQUEST_ID = "MailRequestID";
	public final static String DATA_REQUEST_ID = "DataRequestID";
}
