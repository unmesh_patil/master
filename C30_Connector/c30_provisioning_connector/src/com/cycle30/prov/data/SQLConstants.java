package com.cycle30.prov.data;

/** This stores all the required SQL Constants in this project 
 * 
 * @author rnelluri
 *
 */
public class SQLConstants {

	public static final String PROV_QUERY_GET_CLIENT_AUTH_DATA = "select provision_system_id PROV_SYSTEM_ID, PROVISION_SYSTEM_TYPE SYSTEM_TYPE, " +
			" connect_url URL, user_id USER_ID, user_password PWD, env_name ENV, SIMULATE_REQUEST, provision_callback_system_id PROV_CALLBACK_SYSTEM_ID, " +
			" property1 GCI_ENV , property2 GCI_USER  from c30sdk.c30_prov_client_config where env_name = ?";

	
	public static final String PROV_QUERY_GET_PROV_FEATURE_ITEM_DEF = " select feature_item_id, feature_item_name, " +
			" feature_item_req_class_name, feature_item_res_class_name from c30sdk.c30_prov_feature_item_def";
	
	public static final String PROV_QUERY_GET_BIL_SYSTEM_PARAMETERS = " select module_name, param_name, " +
			" param_value, account_segment, attribute1, attribute2, attribute3 from c30_bil_system_parameters";
	
	public static final String PROV_QUERY_GET_DCN_SUBSCRIBER_PROFILE = " select account_no, account_seg_id, " +
			" subscriber_profile_id, is_default,subscriber_profile_name,roaming_profile_id,fixed_ip_address,subscriber_type, catalog_id, " +
			"  network_type from C30_PROV_C_DCN_SUBSCR_PROFILE " ;
			
	
	public static final String PROV_QUERY_GET_PROV_FEATURE_CONFIG =  "select fd.feature_id,feature_name, fc.feature_item_id, " +
			"           fc.feature_item_action_id, sequence_id , feature_item_sps_plan_id, id.feature_item_res_class_name class_name" +
			"            from c30_prov_feature_config fc, c30_prov_feature_def fd , c30_prov_feature_item_action act, c30_prov_feature_item_def id" +
			"           where fd.feature_id  = fc.feature_id " +
			"            and act.feature_action_id = fc.feature_item_action_id" +
			"            and fc.feature_item_id = id.feature_item_id order by feature_id, sequence_id ";
	
	public static final String PROV_QUERY_GET_PROV_FEATURE_CATALOG_CONFIG =  "select cm.feature_id, rollback_feature_id, emf_config_id,	 catalog_id, catalog_type," +
			" acct_seg_id, provision_system_id, act.feature_action_name catalog_action_name " +
			"        from c30_prov_feature_catalog_map cm, c30_prov_feature_item_action act " +
			"        where cm.catalog_action_id = act.feature_action_id";
	
	public static final String PROV_INSERT_C30_PROV_CATALOG_TRANS = " insert into c30sdk.c30_prov_catalog_trans" +
			" (request_id, external_id,telephone_number,catalog_id, feature_id, request_start_date, " +
			" provision_system_id, service_type, status_id,external_transaction_id, c30_transaction_id) " +
			" values (?,?,?,?,?,sysdate,?,?,?,?,? )";
	
	public static final String PROV_INSERT_C30_PROV_TRANS_INST = " insert into c30sdk.c30_prov_trans_inst" +
			" (request_id, sub_request_id, feature_id, feature_item_id," +
			"  external_id,telephone_number, request_xml, response_xml, request_date, response_date, " +
			" response_code, status_id ) " +
			" values (?,?,?,?,?,?,?,?,sysdate,?,?,?)";

	public static final String PROV_GET_PLAN_REQUEST_SEQUENCE_INFO = "select C30_PROV_REQUEST_SEQ.NEXTVAL REQUEST_ID from dual";


	public static final String PROV_QUERY_IS_SIMULATE_TELEPHONE_NUMBER = "select count(*) COUNT from c30_prov_simulate_telephone where external_id= ?";


	public static final String PROV_QUERY_UPDATE_RESPONSE_TRANS_INST = "UPDATE C30_PROV_TRANS_INST SET STATUS_ID = ?, RESPONSE_XML =? WHERE SUB_REQUEST_ID = ?";
	
	public static final String PROV_QUERY_UPDATE_RESPONSE_TRANS_INST_SYNC = "UPDATE C30_PROV_TRANS_INST SET STATUS_ID = ?, RESPONSE_XML =?, RESPONSE_DATE=sysdate WHERE SUB_REQUEST_ID = ?";

	public static final String PROV_QUERY_PROV_CATALOG_TRANS_STATUS = "UPDATE C30_PROV_CATALOG_TRANS SET STATUS_ID = ?, REQUEST_COMPLETE_DATE=sysdate WHERE REQUEST_ID = ?";
	
	 public static String PROV_UPDATE_PROV_CATALOG_STATUS_TO_ERROR = "update c30_prov_catalog_trans " +
             " set request_complete_date=sysdate, status_id=99 " +
          " where request_id  = ? ";
	 // See if all provisioning instances are complete.  Update the single
    // c30_prov_catalog_trans row when all instances with the same request
    // id are complete (status=80)
	
    public static String PROV_UPDATE_PROV_CATALOG_STATUS =
            "update c30_prov_catalog_trans " +
                    "set request_complete_date=sysdate, status_id=80 " +
                 "where request_id  in  (select request_id from c30_prov_trans_inst where sub_request_id=?) " +
                    "and " +
                  "(select count(*) from c30_prov_trans_inst a, c30_prov_trans_inst b " +
                   "where  a.request_id=b.request_id " +
                      "and  b.sub_request_id=? " +
                      "and   a.status_id not in (80, 81)  " +
                  ") = 0";

	public static final String PROV_QUERY_GET_SUB_PROCESS_STATUS = "SELECT STATUS_ID FROM C30_PROV_TRANS_INST WHERE  SUB_REQUEST_ID = ?";
	
	public static final String PROV_QUERY_GET_REQUEST_XML = "SELECT REQUEST_XML FROM C30_PROV_TRANS_INST WHERE  REQUEST_ID = ?";

	public static final String PROV_QUERY_GET_REQUEST_STATUS = "SELECT STATUS_ID FROM c30_prov_catalog_trans WHERE  REQUEST_ID = ?";
}
