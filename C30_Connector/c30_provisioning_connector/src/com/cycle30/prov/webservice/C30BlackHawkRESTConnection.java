package com.cycle30.prov.webservice;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientDataSource;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;

public class C30BlackHawkRESTConnection {

	private static Logger log = Logger.getLogger(C30BlackHawkRESTConnection.class);


	/** This method is responsible for the POST call
	 * @param request 
	 * 
	 * @param url
	 * @param input
	 * @return
	 * @throws C30ProvConnectorException 
	 */
	public String makeBlackHawkPOSTCall(String webUrl, String inputXml) throws C30ProvConnectorException
	{
		String responseXml = "";

		// Invoke web service API via HTTP POST 
		try {

			// If the BlackHawk URL is not resolved at startup, throw an exception
			if (webUrl == null) {
				throw new C30ProvConnectorException ("BLACKHAWK-002");
			}

		
			ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new NameValuePair("Content-Type", "application/xml"));
			headers.add(new NameValuePair("Accept", "application/xml"));
			
			
		
			
			C30RestConnection restConn = new C30RestConnection();
			responseXml = restConn.makeCall(webUrl, Constants.HTTP_POST, headers, inputXml, null,null);
			
		
		}
		catch (Exception he) {
			String error = he.getMessage();
			log.info("ERROR - BLACKHAWK REST Call : " + error);
			throw new C30ProvConnectorException(he, "BLACKHAWK-003"); 

		}

		log.info("Retrieved XML response from Client: " + responseXml);


			
		return responseXml;
	}
}
