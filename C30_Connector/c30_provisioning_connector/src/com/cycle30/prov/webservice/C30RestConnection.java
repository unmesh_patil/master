package com.cycle30.prov.webservice;


import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class C30RestConnection {


	private static Logger log = Logger.getLogger(C30RestConnection.class);
	public  Client jerseyClient;

	public C30RestConnection(){

	}

	/** This will make the call to any http server with Rest Jersey Client or Commons Http Client
	 * 
	 * @param webUrl
	 * @param httpBasicAuthFilter 
	 * @param gciClientId
	 * @param gciUserId
	 * @param gciSessionId
	 * @param xmltoPost
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public String makeCall(String webUrl, String httpMethod, ArrayList<NameValuePair> headers,
			String payload, ArrayList<NameValuePair> params, HTTPBasicAuthFilter httpBasicAuthFilter) throws C30ProvConnectorException
			{
		String responseXml = "";

		// invoke web service api via HTTP POST or HTTP GET
		try {
			//java.lang.System.setProperty("javax.net.debug", "ssl");
			//System.setProperty("https.protocols", "TLSv1,SSLv3");
			// If the  URL wasn't resolved at startup, throw an exception
			if (webUrl == null) {
				throw new C30ProvConnectorException ("PROV-CONN-006");
			}

			String useJerseyClient =  java.lang.System.getProperty("USE_JERSEY_HTTP_CLIENT");

			if(useJerseyClient != null && useJerseyClient.equalsIgnoreCase("TRUE"))
			{
				//client=C30JerseyRestClient.createClient();
				jerseyClient = Client.create();
				jerseyClient.addFilter(new LoggingFilter());

				if (httpBasicAuthFilter != null) {
					jerseyClient.addFilter(httpBasicAuthFilter);
				}			
				// Set up HTTP request
				if (httpMethod.equalsIgnoreCase(Constants.HTTP_POST))
				{
					responseXml =postJerseyRequest(payload, webUrl, headers);
				}
				if (httpMethod.equalsIgnoreCase(Constants.HTTP_PUT))
				{
					responseXml =putJerseyRequest(payload, webUrl, headers);
				}
				if (httpMethod.equalsIgnoreCase(Constants.HTTP_GET))
				{
					MultivaluedMap<String, String> mParams = new MultivaluedMapImpl();
					for (NameValuePair p : params) 
					{
						mParams.add(p.getName(), p.getValue());
					}
					responseXml =getJerseyHttpRequest(mParams,  webUrl, headers);
				}
			}
			else
			{
				
				// Set up HTTP request
				if (httpMethod.equalsIgnoreCase(Constants.HTTP_POST))
				{
					responseXml =postHttpClientRequest(payload, webUrl, headers);
				}
				if (httpMethod.equalsIgnoreCase(Constants.HTTP_PUT))
				{
					responseXml =putHttpClientRequest(payload, webUrl, headers);
				}
				if (httpMethod.equalsIgnoreCase(Constants.HTTP_GET))
				{
					responseXml =getHttpClientRequest(params,  webUrl, headers);
				}
			}

		}   //added by sundar for PW-113
		    catch(TimeoutException e){
			log.error("TimeoutException......"+e);
			e.printStackTrace();  
			throw new C30ProvConnectorException(e, "PROV-CONN-005");
			}
			catch(ExecutionException e){
		    log.error("ExecutionException......"+e);
		    e.printStackTrace();	
		    throw new C30ProvConnectorException(e, "PROV-CONN-005");
			}
	        catch(ConnectException e){
	    	log.error("ConnectException......"+e);
	    	e.printStackTrace();
	    	throw new C30ProvConnectorException(e, "PROV-CONN-005");
			}		
			catch(SocketTimeoutException e){
		    log.error("SocketTimeoutException......"+e);
		     e.printStackTrace();
		    throw new C30ProvConnectorException(e, "PROV-CONN-005");
			}
	        catch(InterruptedIOException e){
	        log.error("InterruptedIOException......"+e);
	       	e.printStackTrace();
	       	throw new C30ProvConnectorException(e, "PROV-CONN-005");
			}		
	        catch(RemoteException e){
	        log.error("RemoteException......"+e);
			e.printStackTrace();
			throw new C30ProvConnectorException(e, "PROV-CONN-005");
			}
	        catch(IOException e){
	        log.error("IOException......"+e);
			e.printStackTrace();	
			throw new C30ProvConnectorException(e, "PROV-CONN-005");
			}
		   catch (Exception he) {
			log.error(he);
			he.printStackTrace();
			String error = he.getMessage();
			if (error.indexOf("404")>0) {
				throw new C30ProvConnectorException(error, he);
			} else {
				throw new C30ProvConnectorException(he, "PROV-CONN-005");
			}
			}
			catch (Throwable he) {
				log.error("Throwable......"+he);
				log.error(he);
					he.printStackTrace();					
					throw new C30ProvConnectorException(he, "PROV-CONN-005");		
			}
		log.info("Retrieved XML response from Client: " + responseXml);

		return responseXml;
			}

	/** GET - Http Request
	 * 
	 * @param params
	 * @param url
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public   String getJerseyHttpRequest(MultivaluedMap params, String url, ArrayList<NameValuePair> headers) throws Exception
	{
		WebResource webResource = jerseyClient.resource(url); 
		WebResource.Builder wrb = webResource
				.queryParams(params)
				.getRequestBuilder();

		for (NameValuePair h : headers) {
			wrb.header(h.getName(), h.getValue());
		}


		ClientResponse clientResponse =  wrb.get(ClientResponse.class);

		int statusCode = clientResponse.getStatus();
		String body = clientResponse.getEntity(String.class);
		if ((statusCode==404) && (body == null)) {
			String error = "Severe error:  " + url + " not found or unavailable (HTTP response 404)";
			log.error(error);
			throw new C30ProvConnectorException(error+body, "PROV-CONN-001");       
		}
		//  Invalid Call Method
		if (statusCode==405) {
			String error = "Severe error:  " + url + " Invalid Call Method (HTTP response 405)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-004");       
		}			
		// NSG internal server error.
		if ((statusCode==500) && (body == null)) {			
			String error = "Severe error:  service at " + url + " returned internal server error 500";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-002");       
		}

		return body;

	}


	/** PUT -  REST Request
	 * 
	 * @param xmlRequest
	 * @param url
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public   String putJerseyRequest(String payload, String url, ArrayList<NameValuePair> headers) throws Exception
	{
		Document payloadDom = C30DOMUtils.stringToDom(payload);

		WebResource webResource = jerseyClient.resource(url); 
		WebResource.Builder wrb = webResource.getRequestBuilder();

		for (NameValuePair h : headers) {
			wrb.header(h.getName(), h.getValue());
		}

		ClientResponse clientResponse = wrb.put(ClientResponse.class, payloadDom);

		int statusCode = clientResponse.getStatus();
		String body = clientResponse.getEntity(String.class);
		if (statusCode==404) {
			String error = "Severe error:  " + url + " not found or unavailable (HTTP response 404)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-001");       
		}
		//  Invalid Call Method
		if (statusCode==405) {
			String error = "Severe error:  " + url + " Invalid Call Method (HTTP response 405)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-004");       
		}			
		// NSG internal server error.
		if (statusCode == 500) {
			String error = "Severe error:  service at " + url + " returned internal server error 500";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-002");       
		}

		return body;
	}

	/**  Posting the Http Payload with Jersey Client
	 * 
	 * @param payload
	 * @param url
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public   String postJerseyRequest(String payload, String url, ArrayList<NameValuePair> headers) throws Exception
	{
		Document payloadDom = C30DOMUtils.stringToDom(payload);

		WebResource webResource = jerseyClient.resource(url); 
		WebResource.Builder wrb = webResource.getRequestBuilder();

		for (NameValuePair h : headers) {
			wrb.header(h.getName(), h.getValue());
		}

		ClientResponse clientResponse = wrb.post(ClientResponse.class, payloadDom);

		int statusCode = clientResponse.getStatus();
		String body = clientResponse.getEntity(String.class);
		if (statusCode==404) {
			String error = "Severe error:  " + url + " not found or unavailable (HTTP response 404)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-001");       
		}
		//  Invalid Call Method
		if (statusCode==405) {
			String error = "Severe error:  " + url + " Invalid Call Method (HTTP response 405)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-004");       
		}			
		// NSG internal server error.
		if (statusCode == 500) {
			String error = "Severe error:  service at " + url + " returned internal server error 500";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-002");       
		}

		return body;
	}


	/** Posting the http Request with Http Client
	 * 
	 * @param payload
	 * @param url
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public   String postHttpClientRequest(String payload, String url, ArrayList<NameValuePair> headers) throws Exception
	{

		HttpClient client = new HttpClient();
		//added by sundar for PW-113
		client.setTimeout(30000);
		PostMethod method = new PostMethod(url);
		for (NameValuePair h : headers) {
			method.setRequestHeader(h.getName(), h.getValue());
		}

		log.info("Sending POST request to  URL: " + url + " using payload: " + payload);
		method.setRequestBody(payload);

		int statusCode = client.executeMethod( method );
		log.info("HTTP request return status code: " + statusCode);

		String responseData = method.getResponseBodyAsString();

		if (statusCode==404) {
			String error = "Severe error:  " + url + " not found or unavailable (HTTP response 404)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-001");       
		}
		//  Invalid Call Method
		if (statusCode==405) {
			String error = "Severe error:  " + url + " Invalid Call Method (HTTP response 405)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-004");       
		}			
		// NSG internal server error.
		if (statusCode == 500) {
			String error = "Severe error:  service at " + url + " returned internal server error 500";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-002");       
		}

		return responseData;
	}

	/** Put Http Client Request with HttpClient
	 * 
	 * @param payload
	 * @param url
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public   String putHttpClientRequest(String payload, String url, ArrayList<NameValuePair> headers) throws Exception
	{

		HttpClient client = new HttpClient();
		PutMethod method= new PutMethod(url);
		for (NameValuePair h : headers) {
			method.setRequestHeader(h.getName(), h.getValue());
		}

		log.info("Sending PUT request to  URL: " + url + " using payload: " + payload);
		method.setRequestBody(payload);

		int statusCode = client.executeMethod( method );
		log.info("HTTP request return status code: " + statusCode);

		String responseData = method.getResponseBodyAsString();

		if (statusCode==404) {
			String error = "Severe error:  " + url + " not found or unavailable (HTTP response 404)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-001");       
		}
		//  Invalid Call Method
		if (statusCode==405) {
			String error = "Severe error:  " + url + " Invalid Call Method (HTTP response 405)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-004");       
		}			
		// NSG internal server error.
		if (statusCode == 500) {
			String error = "Severe error:  service at " + url + " returned internal server error 500";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-002");       
		}

		return responseData;
	}

	/** Making the Http Get call with the HttpClient
	 * 
	 * @param params
	 * @param url
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public   String getHttpClientRequest( ArrayList<NameValuePair> params, String url, ArrayList<NameValuePair> headers) throws Exception
	{

		HttpClient client = new HttpClient();
		GetMethod method= new GetMethod(url);
		for (NameValuePair h : headers) {
			method.setRequestHeader(h.getName(), h.getValue());
		}

		NameValuePair[] aParams = new NameValuePair[params.size()];
		int i =0;
		for (NameValuePair p : params)
		{
			aParams[i]  = p;
			i++;
		}

		method.setQueryString(aParams);
		log.info("Sending GET request to  URL: " + url );
		int statusCode = client.executeMethod( method );
		log.info("HTTP request return status code: " + statusCode);

		String body = method.getResponseBodyAsString();


		if ((statusCode==404) && (body == null)) {
			String error = "Severe error:  " + url + " not found or unavailable (HTTP response 404)";
			log.error(error);
			throw new C30ProvConnectorException(error+body, "PROV-CONN-001");       
		}
		//  Invalid Call Method
		if (statusCode==405) {
			String error = "Severe error:  " + url + " Invalid Call Method (HTTP response 405)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-004");       
		}			
		// NSG internal server error.
		if ((statusCode==500) && (body == null)) {			
			String error = "Severe error:  service at " + url + " returned internal server error 500";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-002");       
		}
		// ACS TN Inventory
		if (statusCode==500) {			
			String error = "Severe error:  service at " + url + " returned internal server error 500";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-008");       
		}
		if (statusCode==403) {			
			String error = "Severe error:  " + url + " Invalid Call(HTTP response 403)";
			log.error(error);
			throw new C30ProvConnectorException(error, "PROV-CONN-007");       
		}

		return body;

	}

}
