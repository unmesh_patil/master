package com.cycle30.prov.webservice;

import java.util.ArrayList;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;

import com.cycle30.prov.exception.C30ProvConnectorException;

public class C30ProvRESTConnection {

	private static Logger log = Logger.getLogger(C30ProvRESTConnection.class);


	/** This method is responsible for the POST call
	 * @param request 
	 * 
	 * @param url
	 * @param input
	 * @return
	 * @throws C30ProvConnectorException 
	 */
	public String makePOSTCall(String webUrl, String inputXml, String sslTrustStoreLocation) throws C30ProvConnectorException
	{
		String responseXml = "";

		// invoke web service api via HTTP POST 
		try {

			// If the NSG URL wasn't resolved at startup, throw an exception
			if (webUrl == null) {
				throw new C30ProvConnectorException ("PROV-002");
			}

			ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new NameValuePair("Content-type", "application/xml"));


			C30RestConnection restConn = new C30RestConnection();
			responseXml = restConn.makeCall(webUrl, "POST", headers, inputXml, null,null);
		}
		catch (Exception he) {
			String error = he.getMessage();
			log.info("ERROR - PROV REST Call : " + error);
			throw new C30ProvConnectorException(he, "PROV-003"); 

		}

		log.info("Retrieved XML response from Client: " + responseXml);



		return responseXml;
	}
}
