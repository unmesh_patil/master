package com.cycle30.prov.webservice;

import java.util.ArrayList;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;

public class C30ACSRESTConnection {

	private static Logger log = Logger.getLogger(C30ACSRESTConnection.class);


	/** This will make the call to the ACS TN Inventory 
	 * 
	 * @param webUrl
	 * @param xmltoPost
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public String makeACSCall(String webUrl, String httpMethod, ArrayList<NameValuePair> headers, String payload, ArrayList<NameValuePair> params) throws C30ProvConnectorException
	{
		String responseXml = "";

		// invoke web service api via HTTP POST 
		try {

			// If the  URL wasn't resolved at startup, throw an exception
			if (webUrl == null) {
				throw new C30ProvConnectorException ("ACS-TN-002");
			}


		    	C30RestConnection restConn = new C30RestConnection();
		    	if(Constants.HTTP_POST.equalsIgnoreCase(httpMethod)){
				responseXml = restConn.makeCall(webUrl, Constants.HTTP_POST, headers, payload, null,null);
		    	}else if(Constants.HTTP_GET.equalsIgnoreCase(httpMethod)){
		    		responseXml = restConn.makeCall(webUrl, Constants.HTTP_GET, headers, null, params,null);
		    	}else if(Constants.HTTP_PUT.equalsIgnoreCase(httpMethod)){
				responseXml = restConn.makeCall(webUrl, Constants.HTTP_PUT, headers, payload, null,null);
		    	}


		}
		catch (Exception he) {
			String error = he.getMessage();
			log.info("ERROR - ACS TN INVENTORY REST Call : " + error);
			if (error.indexOf("404")>0) {
				throw new C30ProvConnectorException(he.getMessage(), he);				
			} else {
				throw new C30ProvConnectorException(he, "ACS-TN-003");				
			}
		}

		log.info("Retrieved XML response from Client: " + responseXml);



		return responseXml;
	}
}
