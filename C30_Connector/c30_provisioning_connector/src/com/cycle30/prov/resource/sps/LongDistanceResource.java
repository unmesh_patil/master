package com.cycle30.prov.resource.sps;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class LongDistanceResource  extends IProvisioningResource{

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {
		
		String urlExtension = Constants.AWN_WIRELESS_INQUIRY;
		
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();
		
		
		String xml = "   <wireless>   <inquiry>      <idCarrier>GCI</idCarrier>      <method>validateSIM</method> " +
				"     <iccid>12345678901234567890</iccid>   </inquiry></wireless> ";
		
		//        <intrastateCalls>OFF</intrastateCalls>
		C30ProvRESTConnection restConnection = new C30ProvRESTConnection();
		restConnection.makePOSTCall(authData.getWeburl()+urlExtension , xml, authData.getSslTrustStoreLocation());

		return null;
	}

}