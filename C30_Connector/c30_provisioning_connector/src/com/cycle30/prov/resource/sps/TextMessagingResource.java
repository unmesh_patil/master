package com.cycle30.prov.resource.sps;

import java.util.ArrayList;
import java.util.HashMap;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvFeatureItem;
import com.cycle30.prov.config.FeatureActions;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class TextMessagingResource  extends IProvisioningResource{

	Integer basicTextAction = null;
	Integer shortCodeBlockAction  = null;
	Integer verifyServiceAction  = null;

	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		String urlExtension = Constants.AWN_WIRELESS_PROVISION;
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();

		String reqXml = "";

		ArrayList<FeatureActions> featureItemActionsForGivenResource =  (ArrayList<FeatureActions>) requestHashMap.get(Constants.ATTR_FEATURE_ITEM_ACTIONS);
		//Loop through each Voice Resource 
		for (FeatureActions fActions : featureItemActionsForGivenResource)
		{
			C30ProvFeatureItem featureItem =  fActions.getFeatureItem();
			Integer featureItemId = featureItem.getFeatureItemId();
			Integer featureItemActionId = fActions.getFeatureItemActionId();

			if (featureItemId.equals(Constants.FEATURE_ITEM_SMS))
			{
				basicTextAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_SHORT_CODE_BLOCK))
			{
				shortCodeBlockAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_VERIFY_TEXT_SERVICE))
			{
				verifyServiceAction = featureItemActionId;
			}
			
		}

		// Basic Text Add Action
		if (basicTextAction!= null &&  basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
		{
			//Validate the input Data.
			validateInputData(requestHashMap);
			reqXml = reqXml +getAddTextMsgActionXml(reqProp, requestHashMap);
		}

		if (basicTextAction!= null && basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);
			reqXml = reqXml + getDisconnectTextMsgActionXml(reqProp, requestHashMap);
		}

		if (basicTextAction!= null && basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_SUSPEND))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);
			reqXml = reqXml +getSuspendTextMsgActionXml(reqProp, requestHashMap);
		}
		if (basicTextAction!= null && basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_RESUME))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);
			reqXml = reqXml +getResumeTextMsgActionXml(reqProp, requestHashMap);
		}
		if (basicTextAction!= null && basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_CHANGE))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);
			reqXml = reqXml +getChangeTextMsgActionXml(reqProp, requestHashMap);
		}

		reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_TEXT, reqXml, false, false);
		reqXml = C30ProvConnectorUtils.encloseInProvisioningTags(reqXml);

		reqProp.getRequestMap().put(Constants.REQUEST_XML, reqXml);

		
		C30ProvRESTConnection restConnection = new C30ProvRESTConnection();

		//Store feature Item request in the database.
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);

		
		//Verify if the text messaging is already provisioned 
		boolean needToMakeTextCalls = true;
		if (verifyServiceAction != null
				&& (verifyServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON)
						|| verifyServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			needToMakeTextCalls = verifyIfTextServiceIsPresent(reqProp, requestHashMap);
		}
		
		String response = "";
		if (!reqXml.equalsIgnoreCase("") && needToMakeTextCalls) 
		{
			response = restConnection.makePOSTCall(authData.getWeburl()+urlExtension , reqXml, authData.getSslTrustStoreLocation());
			C30ProvConnectorUtils.updateResponseIfTheRequestIsSuccessInSentToSPS(response, request, datasource);
		}
		
		// Make sure we mark the request complete, as there is need to make the call.
		if (!needToMakeTextCalls)
		{
			C30ProvConnectorUtils.updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, response, datasource);
		}
		
		return null;
	}

	/** This is to verify is the Text Service is alreay present or not incase of Disconnect/ Suspend/ Resume
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private boolean verifyIfTextServiceIsPresent(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) {
		
		boolean retValue = true;
		
		HashMap input = new HashMap();
		if (requestHashMap.get(Constants.ICCID) != null ) 
		input.put(Constants.ICCID, requestHashMap.get(Constants.ICCID));
		
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			input.put(Constants.TELEPHONE_NUMBER,requestHashMap.get(Constants.TELEPHONE_NUMBER));

		input.put(Constants.ACCT_SEG_ID, requestHashMap.get(Constants.ACCT_SEG_ID));
		input.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_SUBSCRIBER_INQUIRY);
		C30ProvisionRequest rq;
		try {
			rq = C30ProvisionRequest.getInstance();
			
			//Get the current service status
			String sendTag = Constants.SPS_SERVICE_STATUS_INACTIVE;
			HashMap response =null;
			try
			{
				response = rq.awnInquiryRequest(input) ;
				String responseXml   = (String) response.get(Constants.RESPONSE_XML);

				//Parse the response for the //wireless/inquiry/method/services/data/service/sms
				sendTag = C30ProvConnectorUtils.getXmlTagValueFromXPath(responseXml, "//wireless/inquiry/method/services/data/service/sms");
			}
			catch(Exception e)
			{
				sendTag = Constants.SPS_SERVICE_STATUS_INACTIVE;
			}
				
			if (basicTextAction!= null && basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_SUSPEND))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= true;
				else
					retValue = false;
			}

			if (basicTextAction!= null && basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_RESUME))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_SUSPENDED))
					retValue= true;
				else
					retValue = false;
			}
			
			if (basicTextAction!= null && (basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT) || 
					basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF)))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= true;
				else
					retValue = false;
			}
			if (basicTextAction!= null && (basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD) ||
					basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) ))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= false;
				else
					retValue = true;
			}
			
		} catch (C30ProvConnectorException e) {
			retValue = false;
		}
		
		
		return retValue;

	}

	/** Validate all the input parameters
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputData(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-003");
		//Validate Telephone Number
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-004");
	}

	/** Validate all the input parameters For Delete/Change actions
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForChangeDisconnect(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null &&  requestHashMap.get(Constants.TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-010");

	}
	/** Create TextMessaging XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getAddTextMsgActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.ADD, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		//SMS Block
		String smsBlock = "";
		smsBlock = smsBlock + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PROVISIONED, Constants.TRUE, false, false);
		smsBlock = smsBlock + getShortCodeBlockString();

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SMS, smsBlock, false, false);

		return reqXml;
	}


	/** Disconnect Text XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getDisconnectTextMsgActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.DELETE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		return reqXml;
	}

	/** Suspend Text Msg XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getSuspendTextMsgActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SUSPEND, Constants.TRUE, false, false);

		return reqXml;
	}

	/** Resume Text Service
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getResumeTextMsgActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SUSPEND, Constants.FALSE, false, false);

		return reqXml;
	}
	/** Change Text Msg XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getChangeTextMsgActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		//SMS Block
		String smsBlock = "";
		smsBlock = smsBlock + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PROVISIONED, Constants.TRUE, false, false);
		smsBlock = smsBlock + getShortCodeBlockString();

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SMS, smsBlock, false, false);


		return reqXml;
	}

	/** This returns the correct shortCodeBlock based on the shortCodeBlock Action
	 * 
	 * @return
	 */
	private String getShortCodeBlockString()
	{
		String shortCodeBlockString="";

		if (shortCodeBlockAction != null && shortCodeBlockAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
			shortCodeBlockString = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SHORT_CODE_BLOCK, Constants.TRUE, false, false);
		else if (shortCodeBlockAction != null && shortCodeBlockAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT))
			shortCodeBlockString =  C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SHORT_CODE_BLOCK, Constants.FALSE, false, false);
		else
			shortCodeBlockString =  C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SHORT_CODE_BLOCK, Constants.FALSE, false, false);

		return shortCodeBlockString;
	}
}
