package com.cycle30.prov.resource.sps;

import java.util.ArrayList;
import java.util.HashMap;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvFeatureItem;
import com.cycle30.prov.config.FeatureActions;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;
import com.cycle30.sdk.datasource.C30JDBCDataSource;

public class BasicVoiceResource  extends IProvisioningResource{

	Integer basicVoiceAction = null;
	Integer longDistanceAction  = null;
	Integer internationlRoamingAction  = null;
	Integer roamingAction  = null;
	Integer internationalCallingAction  = null;
	Integer callerIdAction  = null;
	Integer suspendBasicVoiceAction = null;
	Integer prepaidSuspendAction = null;	
	/** This to verify if the voice is already provisioned or not **/
	Integer verifyServiceAction  = null;
	
	
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		String urlExtension = Constants.AWN_WIRELESS_PROVISION;
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();


		String reqXml = "";

		ArrayList<FeatureActions> featureItemActionsForGivenResource =  (ArrayList<FeatureActions>) requestHashMap.get(Constants.ATTR_FEATURE_ITEM_ACTIONS);
		//Loop through each Voice Resource 
		for (FeatureActions fActions : featureItemActionsForGivenResource)
		{
			C30ProvFeatureItem featureItem =  fActions.getFeatureItem();
			Integer featureItemId = featureItem.getFeatureItemId();
			Integer featureItemActionId = fActions.getFeatureItemActionId();

			if (featureItemId.equals(Constants.FEATURE_ITEM_PREPAID_SUSPEND))
			{
				prepaidSuspendAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_BASIC_VOICE))
			{
				basicVoiceAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_SUSPEND_BASIC_VOICE))
			{
				suspendBasicVoiceAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_LONG_DISTANCE))
			{
				longDistanceAction = featureItemActionId;
			}	
			if (featureItemId.equals(Constants.FEATURE_ITEM_INTERNATIONAL))
			{
				internationalCallingAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_INTERNATIONAL_ROAMING))
			{
				internationlRoamingAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_ROAMING))
			{
				roamingAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_CALLER_ID))
			{
				callerIdAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_VERIFY_VOICE_SERVICE))
			{
				verifyServiceAction = featureItemActionId;
			}
		}

		// Prepaid Suspend Add Action
		if (prepaidSuspendAction != null && prepaidSuspendAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD) )
		{
			//Validate the input Data.
			validateInputDataForChange(requestHashMap);

			reqXml = reqXml + getBasicVoiceChangeActionXml(reqProp);
		}
		// Prepaid Suspend Disconnect Action
		if (prepaidSuspendAction != null && prepaidSuspendAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT) )
		{
			//Validate the input Data.
			validateInputDataForChange(requestHashMap);

			reqXml = reqXml + getBasicVoiceChangeActionXml(reqProp);
		}
		// Basic Voice Add Action
		if (basicVoiceAction!= null && basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD) )
		{
			//Validate the input Data.
			validateInputDataForConnect(requestHashMap);

			reqXml = reqXml + getBasicVoiceAddActionXml(reqProp);
		}
		// Basic Voice Change Action
		if (basicVoiceAction!= null && basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_CHANGE) )
		{
			//Validate the input Data.
			validateInputDataForChange(requestHashMap);

			reqXml = reqXml + getBasicVoiceChangeActionXml(reqProp);
		}
		// Basic Voice Disconnect Action
		if (basicVoiceAction!= null && basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT) )
		{
			//Validate the input Data.
			validateInputDataForChange(requestHashMap);

			reqXml = reqXml + getBasicVoiceDisconnectActionXml(reqProp);
		}
		// Basic Voice Swap Action for Telephone Number and ICCID
		if (basicVoiceAction!= null && basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_SWAP) )
		{
			//Validate the input Data.
			validateInputDataForSwap(requestHashMap);

			reqXml = reqXml + getBasicVoiceSwapActionXml(reqProp);
		}

		//Place in the wirelessVoice Tag..
		reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_VOICE,reqXml, false, false);

		reqXml = C30ProvConnectorUtils.encloseInProvisioningTags(reqXml);

		reqProp.getRequestMap().put(Constants.REQUEST_XML, reqXml);

		//Store feature Item request in the database.
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);

		C30ProvRESTConnection restConnection = new C30ProvRESTConnection();


		//Verify if the text messaging is already provisioned 
		boolean needToMakeVoiceProvisionnigCall = true;
		if (verifyServiceAction != null
				&& (verifyServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON)
						|| verifyServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			needToMakeVoiceProvisionnigCall = verifyIfVoiceServiceIsPresent(reqProp, requestHashMap);
		}

		String response = "";
		if (!reqXml.equalsIgnoreCase("") && needToMakeVoiceProvisionnigCall) 
		{
			response = restConnection.makePOSTCall(authData.getWeburl()+urlExtension , reqXml, authData.getSslTrustStoreLocation());
			C30ProvConnectorUtils.updateResponseIfTheRequestIsSuccessInSentToSPS(response, request, datasource);
		}

		// Make sure we mark the request complete, as there is need to make the call.
		if (!needToMakeVoiceProvisionnigCall)
		{
			C30ProvConnectorUtils.updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, response, datasource);
		}

		return null;
	}


	/** Validate all the input parameters For Connect Action
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForConnect(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-003");
		//Validate Telephone Number
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-004");
	}

	/** Validate all the input parameters For Delete/Change actions
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForChange(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null &&  requestHashMap.get(Constants.TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-010");

	}

	/** Verifying the values for Swap
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException
	 */
	private void validateInputDataForSwap(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null &&  requestHashMap.get(Constants.TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-010");

		if (requestHashMap.get(Constants.NEW_ICCID) == null &&  requestHashMap.get(Constants.NEW_TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-012");
	}

	/** Method for disconnecting a service
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getBasicVoiceDisconnectActionXml(ProvisioningRequestProperties reqProp) {
		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.DELETE, false, false);

		if (reqProp.getRequestMap().get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) reqProp.getRequestMap().get(Constants.ICCID), false, false);

		if (reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);


		return reqXml;
	}


	/** Get the Basic Voice Add XML
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getBasicVoiceAddActionXml(ProvisioningRequestProperties reqProp) 
	{

		String reqXml = "";
		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.ADD, false, false);

		if (reqProp.getRequestMap().get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) reqProp.getRequestMap().get(Constants.ICCID), false, false);

		if (reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);
		
		if (suspendBasicVoiceAction != null && (suspendBasicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || suspendBasicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SUSPEND, Constants.TRUE, false, false);
		} 
		
		reqXml = reqXml + "<features>\n";

		//Working on the Barring Request
		reqXml = reqXml + getCallBarringXML(reqProp);

		//Call fwd re
		reqXml = reqXml + getCallForwardingXML(reqProp);

		//Default Call completion Details
		reqXml = reqXml + getCallCompletion(reqProp);

		//Caller Id Features
		reqXml = reqXml + getCallerIdXml(reqProp);

		//Working on Operator Defined barring 
		//PW-65
		//reqXml = reqXml + getOperatorBarringXML(reqProp);
		reqXml = reqXml + getOperatorBarringXMLForAddBasicVoice(reqProp);

		reqXml = reqXml + "</features>\n";

		return reqXml;
	}


	/** Get the Basic Voice Add XML
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getBasicVoiceChangeActionXml(ProvisioningRequestProperties reqProp) 
	{

		String reqXml = "";
		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (reqProp.getRequestMap().get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) reqProp.getRequestMap().get(Constants.ICCID), false, false);

		if (reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		if (prepaidSuspendAction != null && (prepaidSuspendAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || prepaidSuspendAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PREPAID_SUSPEND, Constants.TRUE, false, false);
		} 
		else if (prepaidSuspendAction != null && (prepaidSuspendAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || prepaidSuspendAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
		{
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PREPAID_SUSPEND, Constants.FALSE, false, false);
		}
		else if (suspendBasicVoiceAction != null && (suspendBasicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || suspendBasicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SUSPEND, Constants.TRUE, false, false);
		} 
		else if (suspendBasicVoiceAction != null && (suspendBasicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || suspendBasicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
		{
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SUSPEND, Constants.FALSE, false, false);
		}
		else
		{
			reqXml = reqXml + "<features>\n";

			//Working on the Barring Request
			reqXml = reqXml + getCallBarringXML(reqProp);

			//Call fwd re
			reqXml = reqXml + getCallForwardingXML(reqProp);

			//Default Call completion Details
			reqXml = reqXml + getCallCompletion(reqProp);

			//Caller Id Features
			reqXml = reqXml + getCallerIdXml(reqProp);

			//Working on Operator Defined barring
			reqXml = reqXml + getOperatorBarringXML(reqProp);

			reqXml = reqXml + "</features>\n";
		}
		return reqXml;
	}


	/** Swaping the telephone number and ICCID..
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getBasicVoiceSwapActionXml(ProvisioningRequestProperties reqProp) 
	{

		String reqXml = "";
		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (reqProp.getRequestMap().get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) reqProp.getRequestMap().get(Constants.ICCID), false, false);

		if (reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER), false, false);

		if (reqProp.getRequestMap().get(Constants.NEW_ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_NEW_ICCID, (String) reqProp.getRequestMap().get(Constants.NEW_ICCID), false, false);

		if (reqProp.getRequestMap().get(Constants.NEW_TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_NEW_TELEPHONE_NUMBER, (String) reqProp.getRequestMap().get(Constants.NEW_TELEPHONE_NUMBER), false, false);



		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);


		return reqXml;
	}
	/** Get the barring XML
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getCallBarringXML( ProvisioningRequestProperties  reqProp )
	{
		String callBarringXML = "";

		callBarringXML = callBarringXML + "<barring>\n";
		callBarringXML = callBarringXML + " <allIncomingCalls>\n";
		callBarringXML = callBarringXML + " 	<provisioned>TRUE</provisioned>\n";
		callBarringXML = callBarringXML + " 	<active>FALSE</active>\n";
		callBarringXML = callBarringXML + " </allIncomingCalls>\n";
		callBarringXML = callBarringXML + " <allOutgoingCalls>\n";
		callBarringXML = callBarringXML + "		<provisioned>TRUE</provisioned>\n";
		callBarringXML = callBarringXML + "		<active>FALSE</active>\n";
		callBarringXML = callBarringXML + " </allOutgoingCalls>\n";
		callBarringXML = callBarringXML + "	<incomingCallsRoaming>\n";
		callBarringXML = callBarringXML + "		 <provisioned>TRUE</provisioned>\n";
		callBarringXML = callBarringXML + "		 <active>FALSE</active>\n";
		callBarringXML = callBarringXML + "	</incomingCallsRoaming>\n";
		callBarringXML = callBarringXML + "	<allOutgoingCallsInternational>\n";
		callBarringXML = callBarringXML + "		<provisioned>TRUE</provisioned>\n";
		callBarringXML = callBarringXML + "		<active>FALSE</active>\n";
		callBarringXML = callBarringXML + "	</allOutgoingCallsInternational>\n";
		callBarringXML = callBarringXML + "	<offNetworkInternationalCalls>\n";
		callBarringXML = callBarringXML + "		<provisioned>TRUE</provisioned>\n";
		callBarringXML = callBarringXML + "		<active>FALSE</active>\n";
		callBarringXML = callBarringXML + "	</offNetworkInternationalCalls>\n";
		callBarringXML = callBarringXML + "</barring>\n";


		return callBarringXML;
	}

	/** Get the call Completion XML
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getCallCompletion( ProvisioningRequestProperties  reqProp )
	{
		String callCompletionXML = "";

		callCompletionXML = callCompletionXML + "<completionOfCall>\n";
		callCompletionXML = callCompletionXML + "   <busySubscribers>FALSE</busySubscribers>\n ";            
		callCompletionXML = callCompletionXML + "   <noReply>FALSE</noReply>     \n ";                      
		callCompletionXML = callCompletionXML + "   <subscriberNotReachable>FALSE</subscriberNotReachable> \n "; 
		callCompletionXML = callCompletionXML + "</completionOfCall>\n";

		return callCompletionXML;
	}

	/** Get caller ID XML
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getCallerIdXml( ProvisioningRequestProperties  reqProp )
	{
		String callerIDXml = "";

		if (callerIdAction!= null && (callerIdAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || callerIdAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			callerIDXml = callerIDXml + "<lineIdentification>\n";
			callerIDXml = callerIDXml + "   <callingPresentation>TRUE</callingPresentation>  \n";       
			callerIDXml = callerIDXml + "  <callingRestriction>TRUE</callingRestriction>\n  ";
			callerIDXml = callerIDXml + "  <connectedPresentation>FALSE</connectedPresentation>\n";
			callerIDXml = callerIDXml + "  <connectedRestriction>FALSE</connectedRestriction>\n";
			callerIDXml = callerIDXml + "</lineIdentification>\n";
		}
		else if (callerIdAction!= null && (callerIdAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || callerIdAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
		{
			callerIDXml = callerIDXml + "<lineIdentification>\n";
			callerIDXml = callerIDXml + "   <callingPresentation>FALSE</callingPresentation> \n ";       
			callerIDXml = callerIDXml + "  <callingRestriction>FALSE</callingRestriction>  \n";
			callerIDXml = callerIDXml + "  <connectedPresentation>FALSE</connectedPresentation>\n";
			callerIDXml = callerIDXml + "  <connectedRestriction>FALSE</connectedRestriction>\n";
			callerIDXml = callerIDXml + "</lineIdentification>\n";
		}
		
		return callerIDXml;
	}

	/** Get the operator Barring XML
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getOperatorBarringXML( ProvisioningRequestProperties  reqProp )
	{
		String operatorBarringXML = "";
		String incomingCallTag="";
		String outgoingCallTag="";
		String roamingCallTag="";
		String interStateCallTag="";


		operatorBarringXML = operatorBarringXML + "<operatorBarring>\n";
		operatorBarringXML = operatorBarringXML + "	 <premiumEntertainmentCalls>OFF</premiumEntertainmentCalls>\n";  
		operatorBarringXML = operatorBarringXML + "  <premiumInformationCalls>OFF</premiumInformationCalls>\n  ";

		//Basicvoice Actino  Adding
		if (basicVoiceAction != null && (basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			incomingCallTag="INTERNATIONAL_ROAM";
			outgoingCallTag="INTERNATIONAL";
			roamingCallTag="INTERNATIONAL";
			interStateCallTag ="OFF";
		}
		
		//International Outgoing calls Adding
		if (internationalCallingAction != null && (internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			incomingCallTag="OFF";
			outgoingCallTag="OFF";
		}
		//International Outgoing calls Disconnect
		if (internationalCallingAction != null && (internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
		{
			outgoingCallTag="INTERNATIONAL";
			incomingCallTag="INTERNATIONAL_ROAM";
		}

		//International Roaming calls Adding
		if (internationlRoamingAction != null && (internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			roamingCallTag= "OFF";
			incomingCallTag="OFF";
		}
		//International Roaming calls Disconnecting
		if (internationlRoamingAction != null && (internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
		{
			roamingCallTag= "INTERNATIONAL"; //Remove Roaming
			incomingCallTag="INTERNATIONAL_ROAM";
		}
		
		//Roaming calls Add
		if (roamingAction != null && (roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
			roamingCallTag= "INTERNATIONAL";
		//Roaming calls Disc
		if (roamingAction != null && (roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
			roamingCallTag= "ON"; //Remove Roaming

		//Long Distance Calls 
		if (longDistanceAction != null && (longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
			interStateCallTag ="OFF";
		if (longDistanceAction != null && (longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
			interStateCallTag ="ON";

		if (incomingCallTag!= null && !incomingCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "	 <incomingCalls>"+incomingCallTag+"</incomingCalls>\n";
		if (outgoingCallTag!= null && !outgoingCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "	 <outgoingCalls>"+outgoingCallTag+"</outgoingCalls>\n";
		if (roamingCallTag!= null && !roamingCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "	 <roamingCalls>"+roamingCallTag+"</roamingCalls>  \n";
		if (interStateCallTag!= null && !interStateCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "  <interstateCalls>"+interStateCallTag+"</interstateCalls>\n";

		operatorBarringXML = operatorBarringXML + "</operatorBarring>\n";

		return operatorBarringXML;
	}
	
	/**
	 * Get the Operator Barring XML for Add Basic voice
	 * @param reqProp
	 * @return
	 */
	private String getOperatorBarringXMLForAddBasicVoice( ProvisioningRequestProperties  reqProp )
	{
		String operatorBarringXML = "";
		String incomingCallTag="";
		String outgoingCallTag="";
		String roamingCallTag="";
		String interStateCallTag="";


		operatorBarringXML = operatorBarringXML + "<operatorBarring>\n";
		operatorBarringXML = operatorBarringXML + "	 <premiumEntertainmentCalls>OFF</premiumEntertainmentCalls>\n";  
		operatorBarringXML = operatorBarringXML + "  <premiumInformationCalls>OFF</premiumInformationCalls>\n  ";

		//Basicvoice Action  Adding
		if (basicVoiceAction != null && (basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			//this value is changed to OFF bz in first voice call itself INTERNATIONAL calling should be active to avoid second voice call
			//PW-65 provisioning Optimization
			/*Existing value
			 * incomingCallTag="INTERNATIONAL_ROAM";
			   outgoingCallTag="INTERNATIONAL";
			   roamingCallTag="INTERNATIONAL";
			   interStateCallTag ="OFF";
			 */
			incomingCallTag="OFF";
			outgoingCallTag="OFF";
			roamingCallTag="INTERNATIONAL";
			interStateCallTag ="OFF";
		}
		
		//International Outgoing calls Adding
		if (internationalCallingAction != null && (internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			incomingCallTag="OFF";
			outgoingCallTag="OFF";
		}
		//International Outgoing calls Disconnect
		if (internationalCallingAction != null && (internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || internationalCallingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
		{
			outgoingCallTag="INTERNATIONAL";
			incomingCallTag="INTERNATIONAL_ROAM";
		}

		//International Roaming calls Adding
		if (internationlRoamingAction != null && (internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			roamingCallTag= "OFF";
			incomingCallTag="OFF";
		}
		//International Roaming calls Disconnecting
		if (internationlRoamingAction != null && (internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || internationlRoamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
		{
			roamingCallTag= "INTERNATIONAL"; //Remove Roaming
			incomingCallTag="INTERNATIONAL_ROAM";
		}
		
		//Roaming calls Add
		if (roamingAction != null && (roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
			roamingCallTag= "INTERNATIONAL";
		//Roaming calls Disc
		if (roamingAction != null && (roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || roamingAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
			roamingCallTag= "ON"; //Remove Roaming

		//Long Distance Calls 
		if (longDistanceAction != null && (longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) || longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
			interStateCallTag ="OFF";
		if (longDistanceAction != null && (longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF) || longDistanceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT)))
			interStateCallTag ="ON";

		if (incomingCallTag!= null && !incomingCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "	 <incomingCalls>"+incomingCallTag+"</incomingCalls>\n";
		if (outgoingCallTag!= null && !outgoingCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "	 <outgoingCalls>"+outgoingCallTag+"</outgoingCalls>\n";
		if (roamingCallTag!= null && !roamingCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "	 <roamingCalls>"+roamingCallTag+"</roamingCalls>  \n";
		if (interStateCallTag!= null && !interStateCallTag.equalsIgnoreCase("")) operatorBarringXML = operatorBarringXML + "  <interstateCalls>"+interStateCallTag+"</interstateCalls>\n";

		operatorBarringXML = operatorBarringXML + "</operatorBarring>\n";

		return operatorBarringXML;
	}

	/** Get call forward XML
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getCallForwardingXML( ProvisioningRequestProperties  reqProp )
	{
		String callFwdXml = "";
		//Call forwarding for basic voice stuff
		callFwdXml = callFwdXml + "<callForwarding>\n";
		callFwdXml = callFwdXml + " <subscriberBusy>\n";
		callFwdXml = callFwdXml + "       <provisioned>TRUE</provisioned>\n";
		callFwdXml = callFwdXml + "       <forwardToTelephoneNumber>"+C30ProvConnectorUtils.getVoiceMailBoxNumberForGivenAcctSegment(reqProp)+"</forwardToTelephoneNumber>\n";
		callFwdXml = callFwdXml + "       <active>TRUE</active>\n";
		callFwdXml = callFwdXml + "  </subscriberBusy>\n";
		callFwdXml = callFwdXml + "  <notReachable>\n";
		callFwdXml = callFwdXml + "        <provisioned>TRUE</provisioned>\n";
		callFwdXml = callFwdXml + "         <forwardToTelephoneNumber>"+C30ProvConnectorUtils.getVoiceMailBoxNumberForGivenAcctSegment(reqProp)+"</forwardToTelephoneNumber>\n";
		callFwdXml = callFwdXml + "         <active>TRUE</active>   \n";
		callFwdXml = callFwdXml + "     </notReachable>\n";
		callFwdXml = callFwdXml + "  <noReply>\n";
		callFwdXml = callFwdXml + "		<provisioned>TRUE</provisioned>\n";
		callFwdXml = callFwdXml + "		<forwardToTelephoneNumber>"+C30ProvConnectorUtils.getVoiceMailBoxNumberForGivenAcctSegment(reqProp)+"</forwardToTelephoneNumber>\n";
		callFwdXml = callFwdXml + "		<noReplyTimer>"+C30ProvConnectorUtils.getNoReplyTimerForGivenAcctSegment(reqProp)+"</noReplyTimer>\n";
		callFwdXml = callFwdXml + "		<active>TRUE</active>\n";
		callFwdXml = callFwdXml + " </noReply>      \n";
		callFwdXml = callFwdXml + " <unconditional>\n";
		callFwdXml = callFwdXml + "		<provisioned>TRUE</provisioned>\n";
		callFwdXml = callFwdXml + "		<forwardToTelephoneNumber>"+C30ProvConnectorUtils.getVoiceMailBoxNumberForGivenAcctSegment(reqProp)+"</forwardToTelephoneNumber>\n";
		callFwdXml = callFwdXml + "		<active>FALSE</active>\n";
		callFwdXml = callFwdXml + "	 </unconditional>     \n ";
		callFwdXml = callFwdXml + "</callForwarding>\n";

		return callFwdXml;
	}

	/** This is to verify is the Text Service is alreay present or not incase of Disconnect/ Suspend/ Resume
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private boolean verifyIfVoiceServiceIsPresent(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) {

		boolean retValue = true;

		HashMap input = new HashMap();
		if (requestHashMap.get(Constants.ICCID) != null  ) 
			input.put(Constants.ICCID, requestHashMap.get(Constants.ICCID));

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			input.put(Constants.TELEPHONE_NUMBER,requestHashMap.get(Constants.TELEPHONE_NUMBER));

		input.put(Constants.ACCT_SEG_ID, requestHashMap.get(Constants.ACCT_SEG_ID));
		input.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_SUBSCRIBER_INQUIRY);
		C30ProvisionRequest rq;
		try {
			rq = C30ProvisionRequest.getInstance();

			//Get the current service status
			String sendTag = Constants.SPS_SERVICE_STATUS_INACTIVE;
			HashMap response =null;
			try
			{
				response = rq.awnInquiryRequest(input) ;
				String responseXml   = (String) response.get(Constants.RESPONSE_XML);

				//Parse the response for the //wireless/inquiry/method/services/data/service/voice
				sendTag = C30ProvConnectorUtils.getXmlTagValueFromXPath(responseXml, "//wireless/inquiry/method/services/data/service/voice");
			}
			catch(Exception e)
			{
				//In case of NosubscriberInformationFound error or any other exception
				sendTag = Constants.SPS_SERVICE_STATUS_INACTIVE;
				if (e instanceof C30ProvConnectorException)
				{
					//i.e., the number is in the process of porting. So, it would be in the status of Active.
					if (((C30ProvConnectorException) e).getExceptionCode().equalsIgnoreCase(Constants.EXCEP_PORT_IN_ERROR))
					{
						sendTag = Constants.SPS_SERVICE_STATUS_ACTIVE;
					}
						
				}
			}

			if (basicVoiceAction!= null && basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_SUSPEND))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= true;
				else
					retValue = false;
			}

			if (basicVoiceAction!= null && basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_RESUME))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_SUSPENDED))
					retValue= true;
				else
					retValue = false;
			}

			if (basicVoiceAction!= null && (basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT) ||
					basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF)) )
			{
				if (sendTag != null && (sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE) ||
						sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_SUSPENDED)))
					retValue= true;
				else
					retValue = false;
			}
			if (basicVoiceAction!= null && (basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD) || 
					basicVoiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) ))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= false;
				else
					retValue = true;
			}

		} catch (C30ProvConnectorException e) {
			retValue = false;
		}


		return retValue;

	}
}