package com.cycle30.prov.resource.sps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvFeatureItem;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.prov.config.FeatureActions;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class SmsDataVoiceMailResource extends IProvisioningResource{
	
	Integer basicTextAction = null;
	Integer shortCodeBlockAction  = null;
	Boolean textCall  = false;
	Boolean mailCall=false;
	Boolean dataCall=false;
	Boolean reqTextCall  = false;
	Boolean reqMailCall=false;
	Boolean reqDataCall=false;
	Integer planId = null;
	Integer vMailServiceAction  = null;
	Integer verifyVMServiceAction = null;
	Integer dataServiceAction = null;
	Integer dataPlanId = null;
	private static Logger log = Logger.getLogger(SmsDataVoiceMailResource.class);
	
	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {
		
		
		
		final String urlExtension = Constants.AWN_WIRELESS_PROVISION;
		final ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
		String reqXml="";
		reqTextCall=(Boolean)reqProp.getRequestMap().get(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_TEXT);
		reqDataCall=(Boolean)reqProp.getRequestMap().get(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA);
		reqMailCall=(Boolean)reqProp.getRequestMap().get(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_VOICE_MAIL);
					
		if(reqTextCall !=null && reqDataCall !=null && reqMailCall !=null){
		if(reqTextCall && reqDataCall && reqMailCall){
			log.info("ALL TRUE SO POSTING ALL REQUEST to SPS ");
			
			final C30ProvRESTConnection restConnection = new C30ProvRESTConnection();
			final IProvisioningRequest reqMail=request;
			final IProvisioningRequest reqData=request;
			final IProvisioningRequest reqText=request;
			final C30ProvConnectorUtils utilsObj=new C30ProvConnectorUtils();
			final class SPSThread extends Thread
			{
				String name;
				SPSThread(String name)
				{
					this.name=name;
				}
				@Override
				   public void run() {
					   if(name.equalsIgnoreCase("Mail"))
					   {
						 try {
					    log.info("Thread Concept implemented");
						String mailreqXml=reqMail.getRequestProperties().getRequestMap().get(Constants.MAIL_REQUEST_XML).toString();
						String mailresponse = restConnection.makePOSTCall(authData.getWeburl()+urlExtension , mailreqXml, authData.getSslTrustStoreLocation());
						String mailId=reqMail.getRequestProperties().getRequestMap().get(Constants.MAIL_REQUEST_ID).toString();
						log.info("Mail id="+mailId);
						reqMail.getRequestProperties().setSubRequestId(mailId);
						//C30ProvConnectorUtils.updateResponseIfTheRequestIsSuccessInSentToSPSForMail(mailresponse, reqTh, datasource);
						utilsObj.updateResponseIfTheRequestIsSuccessInSentToSPSForOptimization(mailresponse, reqMail, datasource,mailId);
						
											
						} catch (C30ProvConnectorException e) {
							log.error("Error occured in mailthread"+e.getMessage());
							// TODO Auto-generated catch block
							}
					   }
					   else if(name.equalsIgnoreCase("DATA"))
					   {
							
							try {
								String datareqXml=reqData.getRequestProperties().getRequestMap().get(Constants.DATA_REQUEST_XML).toString();
								String dataresponse = restConnection.makePOSTCall(authData.getWeburl()+urlExtension , datareqXml, authData.getSslTrustStoreLocation());	
								String dataId=reqData.getRequestProperties().getRequestMap().get(Constants.DATA_REQUEST_ID).toString();
								reqData.getRequestProperties().setSubRequestId(dataId);
								log.info("dataId="+dataId);
								utilsObj.updateResponseIfTheRequestIsSuccessInSentToSPSForOptimization(dataresponse, reqData, datasource,dataId);
								
							} catch (C30ProvConnectorException e) {
								// TODO Auto-generated catch block
								log.error("Error occured in datathread"+e.getMessage());
								
							}
					   }
					   else if(name.equalsIgnoreCase("Text"))
					   {
						   try {
								String textreqXml=reqText.getRequestProperties().getRequestMap().get(Constants.TEXT_REQUEST_XML).toString();
								String textresponse = restConnection.makePOSTCall(authData.getWeburl()+urlExtension , textreqXml, authData.getSslTrustStoreLocation());
								String textId=reqText.getRequestProperties().getRequestMap().get(Constants.TEXT_REQUEST_ID).toString();
								reqText.getRequestProperties().setSubRequestId(textId);
								log.info("textId ="+textId);
								utilsObj.updateResponseIfTheRequestIsSuccessInSentToSPSForOptimization(textresponse, reqText, datasource,textId);
								
							} catch (C30ProvConnectorException e) {
								// TODO Auto-generated catch block
								log.error("Error occured in textthread"+e.getMessage());
								
							}
					   }
				        }
				
			}
			
			 Thread[] threads = {
			         new SPSThread("Mail"),
			         new SPSThread("Text"),
			         new SPSThread("DATA")
			      };
			      for (Thread t : threads) {
			         t.start();
			      }
			     
			     
			}
		}
		else{
		ArrayList<FeatureActions> featureItemActionsForGivenResource =  (ArrayList<FeatureActions>) requestHashMap.get(Constants.ATTR_FEATURE_ITEM_ACTIONS);
		//Loop through each Voice Resource 
		for (FeatureActions fActions : featureItemActionsForGivenResource)
		{
			C30ProvFeatureItem featureItem =  fActions.getFeatureItem();
			Integer featureItemId = featureItem.getFeatureItemId();
			Integer featureItemActionId = fActions.getFeatureItemActionId();
			if (featureItemId.equals(Constants.FEATURE_ITEM_SMS))
			{
				
				basicTextAction = featureItemActionId;
				log.info("basicTextAction="+basicTextAction);
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_SHORT_CODE_BLOCK))
			{
				shortCodeBlockAction = featureItemActionId;
				log.info("shortCodeBlockAction="+shortCodeBlockAction);
			}
			
			//Check Voice Mail Action
			if (featureItemId.equals(Constants.FEATURE_ITEM_VOICE_MAIL))
			{
				vMailServiceAction = featureItemActionId;
				if((requestHashMap.get(Constants.C30_ACS_PREPAID_VOICE_VVM) != null && requestHashMap.get(Constants.C30_ACS_PREPAID_VOICE_VVM) != "") && (requestHashMap.get(Constants.C30_ACS_PREPAID_VOICE_VVM).toString().equalsIgnoreCase(Constants.TRUE))) {
					
				String planIdforVVMfromBillSystemParamter=	C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_SPS,
							Constants.PREPAID_PARAM_ACS_VVM_SPS_PLAN_ID,reqProp.getAcctSegId());
				
				if(planIdforVVMfromBillSystemParamter ==null && "".equalsIgnoreCase(planIdforVVMfromBillSystemParamter))
					throw new C30ProvConnectorException("PROV-VALIDATE-015");
				
				planId = new Integer(planIdforVVMfromBillSystemParamter);
								
				}else{
					
				planId = fActions.getFeatureItemSPSPlanId();	
				
				}
				
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_DATA_SERVICE))
			{
				
				dataServiceAction = featureItemActionId;
				dataPlanId = fActions.getFeatureItemSPSPlanId();
				
			}
				
		}
		
		// Basic Text Add Action
		if (basicTextAction!= null &&  basicTextAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
		{
			//Validate the input Data.
			validateInputDataForText(requestHashMap);
			reqXml = reqXml +getAddTextMsgActionXml(reqProp, requestHashMap);
			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_TEXT, reqXml, false, false);
			//Adding provisioning tags 
			reqXml = C30ProvConnectorUtils.encloseInProvisioningTags(reqXml);
			//log.info("reqXml text =="+reqXml);
			requestHashMap.put(Constants.REQUEST_XML, reqXml);
			new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
			requestHashMap.put(Constants.TEXT_REQUEST_XML, reqXml);
			textCall=true;
			requestHashMap.put(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_TEXT, textCall);
			requestHashMap.put(Constants.TEXT_REQUEST_ID,reqProp.getSubRequestId());
			
		}
		
		//Voice Mail Add Action
		if (vMailServiceAction!= null &&  vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
			{
					//Validate the input Data.
					validateInputDataForMail(requestHashMap);
					String acsVMPassword=C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_SPS,
							Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD,reqProp.getAcctSegId());
					if(acsVMPassword ==null && "".equalsIgnoreCase(acsVMPassword))
						throw new C30ProvConnectorException("PROV-VALIDATE-014");
		
					requestHashMap.put(Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD,acsVMPassword);
					//Get the Voice Mail Request XML
					reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_VOICE_MAIL, getAddVoiceMailActionXml(reqProp, requestHashMap), false, false);
					reqXml = C30ProvConnectorUtils.encloseInProvisioningTags(reqXml);
					requestHashMap.put(Constants.REQUEST_XML, reqXml);
					new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
					requestHashMap.put(Constants.MAIL_REQUEST_XML, reqXml);
				//	log.info("Mail xml="+reqXml);
					mailCall=true;
					requestHashMap.put(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_VOICE_MAIL, mailCall);
					requestHashMap.put(Constants.MAIL_REQUEST_ID,reqProp.getSubRequestId());
			}
		if (dataServiceAction != null  && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
		{
			//Validate the input Data.
			validateInputDataForConnect(requestHashMap);
			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getWirelessDataAddXML(reqProp, requestHashMap), false, false);
			reqXml = C30ProvConnectorUtils.encloseInProvisioningTags(reqXml);
			requestHashMap.put(Constants.REQUEST_XML, reqXml);
			new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
			requestHashMap.put(Constants.DATA_REQUEST_XML, reqXml);
			//log.info("data xml="+reqXml);
			dataCall=true;
			requestHashMap.put(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, dataCall);
			requestHashMap.put(Constants.DATA_REQUEST_ID,reqProp.getSubRequestId());
		}
	}
			
		return null;
	}

	/** Validate all the input parameters
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForText(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-003");
		//Validate Telephone Number
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-004");
	}
	
	/** Create TextMessaging XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getAddTextMsgActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		//reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetailsRemoveID(reqProp);
		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.ADD, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		//SMS Block
		String smsBlock = "";
		smsBlock = smsBlock + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PROVISIONED, Constants.TRUE, false, false);
		smsBlock = smsBlock + getShortCodeBlockString();

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SMS, smsBlock, false, false);

		return reqXml;
	}
	/** This returns the correct shortCodeBlock based on the shortCodeBlock Action
	 * 
	 * @return
	 */
	private String getShortCodeBlockString()
	{
		String shortCodeBlockString="";

		if (shortCodeBlockAction != null && shortCodeBlockAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
			shortCodeBlockString = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SHORT_CODE_BLOCK, Constants.TRUE, false, false);
		else if (shortCodeBlockAction != null && shortCodeBlockAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT))
			shortCodeBlockString =  C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SHORT_CODE_BLOCK, Constants.FALSE, false, false);
		else
			shortCodeBlockString =  C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SHORT_CODE_BLOCK, Constants.FALSE, false, false);

		return shortCodeBlockString;
	}

	/** Validate all the input parameters
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForMail(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate Telephone Number
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-004");
	}
	/** Add VoiceMail actions
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getAddVoiceMailActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException
	{
		String reqXml = "";
		String TelephoneNo = null;
		String vmPassword = null;
		TelephoneNo = requestHashMap.get(Constants.TELEPHONE_NUMBER).toString();
		vmPassword = TelephoneNo.substring(Math.max(0, TelephoneNo.length() - 5));
		
		//reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetailsRemoveID(reqProp);
		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.ADD, false, false);

		
		//Request Type
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_CATEGORY, Constants.VOICEMAIL, false, false);
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLATFORM_TYPE, Constants.WIRELESS, false, false);
		
		//Dynamic Data
		if (planId != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLAN_ID, planId.toString() , false, false);
		else
			throw new C30ProvConnectorException("PROV-VALIDATE-013");
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_MAILBOX_NUMBER,(String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);
		if(Constants.ACCT_SEG_ID_4.toString().equalsIgnoreCase(( requestHashMap.get(Constants.ACCT_SEG_ID)).toString())){
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PASSWORD, vmPassword, false, false);
			
		}else if(Constants.ACCT_SEG_ID_5.toString().equalsIgnoreCase(( requestHashMap.get(Constants.ACCT_SEG_ID)).toString())){
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PASSWORD, (String) requestHashMap.get(Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD), false, false);
		}		
		
		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		return reqXml;
	}
	/** Validate all the input parameters
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForConnect(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-003");
		//Validate Telephone Number
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-004");
	}
	/** Get the Wireless Data Add XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 * @throws C30ProvConnectorException 
	 */
	private String getWirelessDataAddXML(ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException 
	{
		
		String reqXml = "";

		//reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetailsRemoveID(reqProp);
		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.ADD, false, false);
		log.info("reqXml"+reqXml);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);
		//Dynamic Data
		if (dataPlanId != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLAN_ID, dataPlanId.toString() , false, false);
		else
			throw new C30ProvConnectorException("PROV-VALIDATE-013");
		
		return reqXml;
	}

}
