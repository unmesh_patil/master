package com.cycle30.prov.resource.sps;

import java.util.ArrayList;
import java.util.HashMap;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvFeatureItem;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.prov.config.FeatureActions;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class VoiceMailResource  extends IProvisioningResource{

	Integer planId = null;
	Integer vMailServiceAction  = null;
	Integer verifyVMServiceAction = null;

	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		String urlExtension = Constants.AWN_WIRELESS_PROVISION;
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();

		String reqXml = "";
		
		
		ArrayList<FeatureActions> featureItemActionsForGivenResource =  (ArrayList<FeatureActions>) requestHashMap.get(Constants.ATTR_FEATURE_ITEM_ACTIONS);
		//Loop through each Voice Resource 
		for (FeatureActions fActions : featureItemActionsForGivenResource)
		{
			C30ProvFeatureItem featureItem =  fActions.getFeatureItem();
			Integer featureItemId = featureItem.getFeatureItemId();
			Integer featureItemActionId = fActions.getFeatureItemActionId();

			//Verify Voice Mail Service
			if (featureItemId.equals(Constants.FEATURE_ITEM_VERIFY_VOICE_MAIL_SERVICE))
			{
				verifyVMServiceAction = featureItemActionId;
				
			}
			//Check Voice Mail Action
			if (featureItemId.equals(Constants.FEATURE_ITEM_VOICE_MAIL))
			{
				vMailServiceAction = featureItemActionId;
				if((requestHashMap.get(Constants.C30_ACS_PREPAID_VOICE_VVM) != null && requestHashMap.get(Constants.C30_ACS_PREPAID_VOICE_VVM) != "") && (requestHashMap.get(Constants.C30_ACS_PREPAID_VOICE_VVM).toString().equalsIgnoreCase(Constants.TRUE))) {
					
				String planIdforVVMfromBillSystemParamter=	C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_SPS,
							Constants.PREPAID_PARAM_ACS_VVM_SPS_PLAN_ID,reqProp.getAcctSegId());
				
				if(planIdforVVMfromBillSystemParamter ==null && "".equalsIgnoreCase(planIdforVVMfromBillSystemParamter))
					throw new C30ProvConnectorException("PROV-VALIDATE-015");
				
				planId = new Integer(planIdforVVMfromBillSystemParamter);
				
				}else{
					
				planId = fActions.getFeatureItemSPSPlanId();	
				
				}
				
			}
				
		}
		

		//Voice Mail Add Action
		if (vMailServiceAction!= null &&  vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
		{
			//Validate the input Data.
			validateInputData(requestHashMap);
			
			String acsVMPassword=C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_SPS,
					Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD,reqProp.getAcctSegId());
			if(acsVMPassword ==null && "".equalsIgnoreCase(acsVMPassword))
				throw new C30ProvConnectorException("PROV-VALIDATE-014");
			
			requestHashMap.put(Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD,acsVMPassword);

			
			//Get the Voice Mail Request XML
			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_VOICE_MAIL, getAddVoiceMailActionXml(reqProp, requestHashMap), false, false);
		}
		
		
		//Voice Mail Add Change
				if (vMailServiceAction!= null &&  vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_CHANGE))
				{
					//Validate the input Data.
					validateInputData(requestHashMap);
					
					String acsVMPassword=C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_SPS,
							Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD,reqProp.getAcctSegId());
					if(acsVMPassword ==null && "".equalsIgnoreCase(acsVMPassword))
						throw new C30ProvConnectorException("PROV-VALIDATE-014");
					
					requestHashMap.put(Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD,acsVMPassword);

					
					//Get the Voice Mail Request XML
					reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_VOICE_MAIL, getChangeVoiceMailActionXml(reqProp, requestHashMap), false, false);
				}

		//Voice Mail Disconnect Action
		if (vMailServiceAction != null  && vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);
			//Get the Voice Mail Request XML
			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getDisconnectVoiceMailActionXml(reqProp, requestHashMap), false, false);
		}
		
		reqXml = C30ProvConnectorUtils.encloseInProvisioningTags(reqXml);
		
		reqProp.getRequestMap().put(Constants.REQUEST_XML, reqXml);
		
		//Store feature Item request in the database.
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);

		C30ProvRESTConnection restConnection = new C30ProvRESTConnection();

		String response = "";
		
		//Verify if the VM is already provisioned 
		boolean needToMakeVMCalls = true;
		if (verifyVMServiceAction != null
				&& (verifyVMServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON)
						|| vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			needToMakeVMCalls = verifyIfvMailServiceIsPresent(reqProp, requestHashMap);
		}		
		
		if (!reqXml.equalsIgnoreCase("") && needToMakeVMCalls) 
		{
			response = restConnection.makePOSTCall(authData.getWeburl()+urlExtension , reqXml, authData.getSslTrustStoreLocation());
			C30ProvConnectorUtils.updateResponseIfTheRequestIsSuccessInSentToSPS(response, request, datasource);
		}		

		// Make sure we mark the request complete, as there is need to make the call.
		if (!needToMakeVMCalls)
		{
			C30ProvConnectorUtils.updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, response, datasource);
		}
		
		return null;
	}
	
	/** Validate all the input parameters
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputData(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate Telephone Number
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-004");
	}

	/** Validate all the input parameters For Delete/Change actions
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForChangeDisconnect(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null &&  requestHashMap.get(Constants.TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-010");
		
	}

	/** Add VoiceMail actions
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getAddVoiceMailActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException
	{
		String reqXml = "";
		String TelephoneNo = null;
		String vmPassword = null;
		TelephoneNo = requestHashMap.get(Constants.TELEPHONE_NUMBER).toString();
		vmPassword = TelephoneNo.substring(Math.max(0, TelephoneNo.length() - 5));
		
		reqXml = reqXml + C30ProvConnectorUtils.getCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.ADD, false, false);

		
		//Request Type
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_CATEGORY, Constants.VOICEMAIL, false, false);
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLATFORM_TYPE, Constants.WIRELESS, false, false);
		
		//Dynamic Data
		if (planId != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLAN_ID, planId.toString() , false, false);
		else
			throw new C30ProvConnectorException("PROV-VALIDATE-013");
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_MAILBOX_NUMBER,(String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);
		if(Constants.ACCT_SEG_ID_4.toString().equalsIgnoreCase(( requestHashMap.get(Constants.ACCT_SEG_ID)).toString())){
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PASSWORD, vmPassword, false, false);
			
		}else if(Constants.ACCT_SEG_ID_5.toString().equalsIgnoreCase(( requestHashMap.get(Constants.ACCT_SEG_ID)).toString())){
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PASSWORD, (String) requestHashMap.get(Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD), false, false);
		}		
		
		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		return reqXml;
	}
	
	/** Disconnect VoiceMail actions
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getDisconnectVoiceMailActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException
	{
		String reqXml = "";
		String TelephoneNo = null;
		String vmPassword = null;
		TelephoneNo = requestHashMap.get(Constants.TELEPHONE_NUMBER).toString();
		vmPassword = TelephoneNo.substring(Math.max(0, TelephoneNo.length() - 5));
		
		reqXml = reqXml + C30ProvConnectorUtils.getCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.DELETE, false, false);

		
		//Request Type
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_CATEGORY, Constants.VOICEMAIL, false, false);
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLATFORM_TYPE, Constants.WIRELESS, false, false);
		
		//Dynamic Data
		if (planId != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLAN_ID, planId.toString() , false, false);
		else
			throw new C30ProvConnectorException("PROV-VALIDATE-013");
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_MAILBOX_NUMBER,(String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);
			
		
		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		return reqXml;
	}
	
	/** Change VoiceMail actions
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getChangeVoiceMailActionXml(ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException
	{
		String reqXml = "";
		String TelephoneNo = null;
		String vmPassword = null;
		TelephoneNo = requestHashMap.get(Constants.TELEPHONE_NUMBER).toString();
		vmPassword = TelephoneNo.substring(Math.max(0, TelephoneNo.length() - 5));
		
		reqXml = reqXml + C30ProvConnectorUtils.getCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		
		//Request Type
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_CATEGORY, Constants.VOICEMAIL, false, false);
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLATFORM_TYPE, Constants.WIRELESS, false, false);
		
		//Dynamic Data
		if (planId != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLAN_ID, planId.toString() , false, false);
		else
			throw new C30ProvConnectorException("PROV-VALIDATE-013");
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_MAILBOX_NUMBER,(String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);
		if(Constants.ACCT_SEG_ID_4.toString().equalsIgnoreCase(( requestHashMap.get(Constants.ACCT_SEG_ID)).toString())){
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PASSWORD, vmPassword, false, false);
			
		}else if(Constants.ACCT_SEG_ID_5.toString().equalsIgnoreCase(( requestHashMap.get(Constants.ACCT_SEG_ID)).toString())){
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PASSWORD, (String) requestHashMap.get(Constants.PREPAID_PARAM_DEFAULT_ACS_VOICE_MAIL_PASSWORD), false, false);
		}	
		
		
		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		return reqXml;
	}
	
	/** This is to verify is the Voice Mail Service is already present or not in case of Add/Disconnect
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private boolean verifyIfvMailServiceIsPresent(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) {

		boolean retValue = true;

		HashMap input = new HashMap();
		if (requestHashMap.get(Constants.ICCID) != null ) 
			input.put(Constants.ICCID, requestHashMap.get(Constants.ICCID));

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			input.put(Constants.TELEPHONE_NUMBER,requestHashMap.get(Constants.TELEPHONE_NUMBER));

		input.put(Constants.ACCT_SEG_ID, requestHashMap.get(Constants.ACCT_SEG_ID));
		input.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_SUBSCRIBER_INQUIRY);

		C30ProvisionRequest rq;
		try {
			rq = C30ProvisionRequest.getInstance();

			String sendTag = Constants.SPS_VOICE_MAIL_STATUS_INACTIVE;
			HashMap response =null;
			try
			{
				response = rq.awnInquiryRequest(input) ;
				String responseXml   = (String) response.get(Constants.RESPONSE_XML);

				//Parse the response for the //wireless/inquiry/method/networks/network/data/features/visualVoicemail
				sendTag = C30ProvConnectorUtils.getXmlTagValueFromXPath(responseXml, "//wireless/inquiry/method/networks/network/data/features/visualVoicemail");
			}
			catch(Exception e)
			{
				sendTag = Constants.SPS_VOICE_MAIL_STATUS_INACTIVE;
			}

			if (vMailServiceAction!= null && (vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD) ||
					vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) ))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_VOICE_MAIL_STATUS_ACTIVE))
					retValue= false;
				else
					retValue = true;
			}
			
			if (vMailServiceAction!= null && (vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT) || 
					vMailServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF)))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_VOICE_MAIL_STATUS_ACTIVE))
					retValue= true;
				else
					retValue = false;
			}

		} catch (C30ProvConnectorException e) {
			retValue = false;
		}

		return retValue;

	}
	
}
