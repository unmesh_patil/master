package com.cycle30.prov.resource.sps;

import java.util.ArrayList;
import java.util.HashMap;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvFeatureItem;
import com.cycle30.prov.config.FeatureActions;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class WirelessDataResource   extends IProvisioningResource{

	Integer planId = null;
	Integer verifyServiceAction  = null;
	Integer dataServiceAction = null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		String urlExtension = Constants.AWN_WIRELESS_PROVISION;
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();



		String reqXml = "";

		ArrayList<FeatureActions> featureItemActionsForGivenResource =  (ArrayList<FeatureActions>) requestHashMap.get(Constants.ATTR_FEATURE_ITEM_ACTIONS);
		//Loop through each Voice Resource 
		for (FeatureActions fActions : featureItemActionsForGivenResource)
		{
			C30ProvFeatureItem featureItem =  fActions.getFeatureItem();
			Integer featureItemId = featureItem.getFeatureItemId();
			Integer featureItemActionId = fActions.getFeatureItemActionId();

			if (featureItemId.equals(Constants.FEATURE_ITEM_VERIFY_DATA_SERVICE))
			{
				verifyServiceAction = featureItemActionId;
			}
			if (featureItemId.equals(Constants.FEATURE_ITEM_DATA_SERVICE))
			{
				dataServiceAction = featureItemActionId;
				planId = fActions.getFeatureItemSPSPlanId();
			}
		}

		//Now working onthe core dataActions

		if (dataServiceAction != null  && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD))
		{
			//Validate the input Data.
			validateInputDataForConnect(requestHashMap);
			//Sometimes same dataplan is added twice. In the case first will be add and other will be change.
			boolean havetoMakeDataAddCallOrChangeCall  = verifyIfDataServiceIsPresent(reqProp, requestHashMap);
			if (!havetoMakeDataAddCallOrChangeCall)
			{
				reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getWirelessDataChangeXML(reqProp, requestHashMap), false, false);
				dataServiceAction = Constants.FEATURE_ITEM_ACTION_TYPE_CHANGE;
			}
			else
			{
				reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getWirelessDataAddXML(reqProp, requestHashMap), false, false);
			}

		}
		if (dataServiceAction != null  && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);

			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getWirelessDataDisconnectXML(reqProp, requestHashMap), false, false);
		}
		if (dataServiceAction != null  && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_SUSPEND))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);

			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getWirelessDataSuspendXML(reqProp, requestHashMap), false, false);
		}
		if (dataServiceAction != null  && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_RESUME))
		{
			//Validate the input Data.
			validateInputDataForChangeDisconnect(requestHashMap);

			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getWirelessDataResumeXML(reqProp, requestHashMap), false, false);
		}
		// Basic Voice Swap Action for Telephone Number and ICCID
		if (dataServiceAction != null  && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_SWAP) )
		{
			//Validate the input Data.
			validateInputDataForSwap(requestHashMap);
			reqXml = C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_WIRELESS_DATA, getWirelessDataTelephoneSwapXML(reqProp, requestHashMap), false, false);
		}


		reqXml = C30ProvConnectorUtils.encloseInProvisioningTags(reqXml);

		reqProp.getRequestMap().put(Constants.REQUEST_XML, reqXml);

		//Store feature Item request in the database.
		new ProvRequestLogger().auditFeatureItemRequest(request,datasource);

		C30ProvRESTConnection restConnection = new C30ProvRESTConnection();


		//Verify if the text messaging is already provisioned 
		boolean needTOMakeDataCalls = true;
		if (verifyServiceAction != null && 
				(verifyServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON) 
						|| verifyServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD)))
		{
			needTOMakeDataCalls = verifyIfDataServiceIsPresent(reqProp, requestHashMap);
		}

		String response = "";
		if (!reqXml.equalsIgnoreCase("") && needTOMakeDataCalls) 
		{
			response = restConnection.makePOSTCall(authData.getWeburl()+urlExtension , reqXml, authData.getSslTrustStoreLocation());
			C30ProvConnectorUtils.updateResponseIfTheRequestIsSuccessInSentToSPS(response, request,datasource);
		}

		// Make sure we mark the request complete, as there is need to make the call.
		if (!needTOMakeDataCalls)
		{
			C30ProvConnectorUtils.updateStatusAndResponseXMLForSubRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, response, datasource);
		}

		return null;
	}

	/** Swapping the telephone number and ICCID.. Then the data need to be changed as well.
	 * 
	 * @param reqProp
	 * @return
	 */
	private String getWirelessDataTelephoneSwapXML(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";
		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (reqProp.getRequestMap().get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) reqProp.getRequestMap().get(Constants.ICCID), false, false);

		if (reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) reqProp.getRequestMap().get(Constants.TELEPHONE_NUMBER), false, false);

		if (reqProp.getRequestMap().get(Constants.NEW_TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_NEW_TELEPHONE_NUMBER, (String) reqProp.getRequestMap().get(Constants.NEW_TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		return reqXml;
	}


	/** Verifying the values for Swap
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException
	 */
	private void validateInputDataForSwap(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null &&  requestHashMap.get(Constants.TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-010");

		if (requestHashMap.get(Constants.NEW_ICCID) == null &&  requestHashMap.get(Constants.NEW_TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-012");
	}

	/** This is to verify is the Data Service is already present or not incase of Disconnect/ Suspend/ Resume
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private boolean verifyIfDataServiceIsPresent(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) {

		boolean retValue = true;

		HashMap input = new HashMap();
		if (requestHashMap.get(Constants.ICCID) != null ) 
			input.put(Constants.ICCID, requestHashMap.get(Constants.ICCID));

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			input.put(Constants.TELEPHONE_NUMBER,requestHashMap.get(Constants.TELEPHONE_NUMBER));

		input.put(Constants.ACCT_SEG_ID, requestHashMap.get(Constants.ACCT_SEG_ID));
		input.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_SUBSCRIBER_INQUIRY);

		C30ProvisionRequest rq;
		try {
			rq = C30ProvisionRequest.getInstance();

			String sendTag = Constants.SPS_SERVICE_STATUS_INACTIVE;
			HashMap response =null;
			try
			{
				response = rq.awnInquiryRequest(input) ;
				String responseXml   = (String) response.get(Constants.RESPONSE_XML);

				//Parse the response for the //wireless/inquiry/method/services/data/service/data
				sendTag = C30ProvConnectorUtils.getXmlTagValueFromXPath(responseXml, "//wireless/inquiry/method/services/data/service/data");
			}
			catch(Exception e)
			{
				sendTag = Constants.SPS_SERVICE_STATUS_INACTIVE;
			}

			if ( dataServiceAction != null && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_SUSPEND))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= true;
				else
					retValue = false;
			}

			if ( dataServiceAction != null && dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_RESUME))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_SUSPENDED))
					retValue= true;
				else
					retValue = false;
			}

			if ( dataServiceAction != null &&  (dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_DISCONNECT) ||
					dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_OFF)))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= true;
				else
					retValue = false;
			}

			if ( dataServiceAction != null && (dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ADD) ||
					dataServiceAction.equals(Constants.FEATURE_ITEM_ACTION_TYPE_ON)))
			{
				if (sendTag != null && sendTag.equalsIgnoreCase(Constants.SPS_SERVICE_STATUS_ACTIVE))
					retValue= false;
				else
					retValue = true;
			}

		} catch (C30ProvConnectorException e) {
			retValue = false;
		}

		return retValue;

	}
	/** Validate all the input parameters
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForConnect(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-003");
		//Validate Telephone Number
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) == null ) 
			throw new C30ProvConnectorException("PROV-VALIDATE-004");
	}

	/** Validate all the input parameters For Delete/Change actions
	 * 
	 * @param requestHashMap
	 * @throws C30ProvConnectorException 
	 */
	private void validateInputDataForChangeDisconnect(HashMap requestHashMap) throws C30ProvConnectorException 
	{
		//Validate SIM
		if (requestHashMap.get(Constants.ICCID) == null &&  requestHashMap.get(Constants.TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-VALIDATE-010");

	}

	/** Get the Wireless Data Add XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 * @throws C30ProvConnectorException 
	 */
	private String getWirelessDataAddXML(ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.ADD, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		//Dynamic Data
		if (planId != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLAN_ID, planId.toString() , false, false);
		else
			throw new C30ProvConnectorException("PROV-VALIDATE-013");

		return reqXml;
	}

	/** Disconnect the wireless Data XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getWirelessDataDisconnectXML(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.DELETE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		return reqXml;
	}

	/** Working on Suspend Data XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getWirelessDataSuspendXML(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SUSPEND, Constants.TRUE, false, false);

		return reqXml;
	}
	/** Working on Change Data XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getWirelessDataChangeXML(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);
		//Dynamic Data
		if (planId != null)
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_PLAN_ID, planId.toString() , false, false);

		return reqXml;
	}
	/** Working on Resume Data XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String getWirelessDataResumeXML(ProvisioningRequestProperties reqProp, HashMap requestHashMap) 
	{

		String reqXml = "";

		reqXml = reqXml + C30ProvConnectorUtils.getGSMCarriIDsystemIdDetails(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ACTION, Constants.CHANGE, false, false);

		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);

		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml +  C30ProvConnectorUtils.simulateRequest(reqProp);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_SUSPEND, Constants.FALSE, false, false);

		return reqXml;
	}

}
