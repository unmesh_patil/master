package com.cycle30.prov.resource.sps.inquiry;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class ValidateSIMResource extends IProvisioningResource{

	boolean simulateRequest = false;
	String iccid = null;
	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(ValidateSIMResource.class);

		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		
		simulateRequest = (Boolean) requestHashMap.get(Constants.ATTR_SIMULATE_REQUEST);
		
		String acctSegId = (String) requestHashMap.get(Constants.ACCT_SEG_ID);		
		if ( requestHashMap.get(Constants.ICCID) != null ) {
			iccid = (String) requestHashMap.get(Constants.ICCID);
		}
		else {
			throw new C30ProvConnectorException("PROV-INQ-001");
		}

		String responseXml = null;
		if (!simulateRequest) {
			String xml = C30ProvConnectorUtils.encloseWirelessInquiryTags(getIMSIVerificationXml(reqProp, requestHashMap));
			log.info("getIMSIVerificationXml = " + xml);
			C30ProvRESTConnection restConnection = new C30ProvRESTConnection();
			responseXml = restConnection.makePOSTCall(authData.getWeburl() , xml, authData.getSslTrustStoreLocation());
			
		} else {
			responseXml = C30ProvConnectorUtils.getSimulateIMSINumberResponse(iccid, "validateSIM", acctSegId);
		}


		//Populating the response.
		GenericInquiryResponse imsiResponse = new GenericInquiryResponse();
		try
		{
			HashMap responseMap = populateResponse(responseXml);
			imsiResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}
		
		return imsiResponse;
	}

	
	private String getIMSIVerificationXml(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) {
		String reqXml = "";
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_METHOD, Constants.SPS_INQUIRY_VALIDATE_SIM, false, false);
		
		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ID_CARRIER, C30ProvConnectorUtils.getInquiryCarrierIdByAcctSegId(reqProp.getAcctSegId()).trim(), false, false);
		
		//reqXml = reqXml + "<simulate>true</simulate>";

		return reqXml;
	}

	
	/** This is the reponse formation for the Resource
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap populateResponse(String responseXml) throws C30ProvConnectorException {

		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-012");
		}

		/** Error Response ...
		 * <wireless>
				  <inquiry>
				    <method name="validateSIM">
				      <data>
				        <iccid>89013113700709000000</iccid>
				      </data>
				      <error>
				        <message>The IDICC ( 89013113700709000000 ) is in use.</message>
				        <number>1022</number>
				      </error>
				    </method>
				  </inquiry>
				</wireless>
		 */
		// See if error occurred.
		String errorNo = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_NUMBER);
		if (errorNo == null || errorNo.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-003");
		}

		// non-zero return is an error
		if (errorNo.equals("0")==false) {
			String errorMsg = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_MESSAGE);
			throw new C30ProvConnectorException("Error returned in SIM validation: " + errorMsg, "PROV-INQ-004");
		}

		/** Success
		 * <wireless>
				  <inquiry>
				    <method name="validateSIM">
				      <data>
				        <iccid>89013113700709000042</iccid>
				        <imsi>311370000000004</imsi>
				      </data>
				      <error>
				        <message>Success</message>
				        <number>0</number>
				      </error>
				    </method>
				  </inquiry>
				</wireless>

		 */
		// get IMSI from successful response
		String imsi = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_WIRELESS_XML_TAG_IMSI);
		if (imsi == null || imsi.length()==0) {
			throw new C30ProvConnectorException("No IMSI returned in SIM validation.", "PROV-INQ-004");
		}

		HashMap response = new HashMap();
		response.put(Constants.IMSI, imsi);
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;


	}

}
