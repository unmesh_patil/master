package com.cycle30.prov.resource.sps.inquiry;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class SPSRequestHistoryResource  extends IProvisioningResource{

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(ValidateSIMResource.class);

		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		
		//Construct the XML Dynamically
		String xml = C30ProvConnectorUtils.encloseWirelessInquiryTags(getSPSRequestHistoryXml(reqProp, requestHashMap));

		C30ProvRESTConnection restConnection = new C30ProvRESTConnection();
		String responseXml = restConnection.makePOSTCall(authData.getWeburl() , xml, authData.getSslTrustStoreLocation());

		//Populating the response.
		GenericInquiryResponse imsiResponse = new GenericInquiryResponse();
		try
		{
			HashMap responseMap = populateResponse(responseXml);
			imsiResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return imsiResponse;
	}

	
	private String getSPSRequestHistoryXml(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException {
		String reqXml = "";
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_METHOD, Constants.SPS_INQUIRY_GET_SERVICE_ORDER_HISTORY, false, false);
		
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);
		else
			throw new C30ProvConnectorException("PROV-INQ-009");
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ID_CARRIER, C30ProvConnectorUtils.getInquiryCarrierIdByAcctSegId(reqProp.getAcctSegId()).trim(), false, false);
		
		return reqXml;
	}

	
	/** This is the reponse formation for the Resource
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap populateResponse(String responseXml) throws C30ProvConnectorException {

		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-002");
		}

		/** Error Response ...
		 *<wireless>
			  <inquiry>
			    <method name="getServiceOrderHistory">
			      <data></data>
			      <error>
			        <message>No service order found for telephone number ( 9078300629 ).</message>
			        <number>1082</number>
			      </error>
			    </method>
			  </inquiry>
			</wireless>
		 */
		// See if error occurred.
		String errorNo = C30ProvConnectorUtils.getXmlStringElementValue(responseXml,Constants.SPS_PROV_XML_TAG_NUMBER);
		if (errorNo == null || errorNo.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-007");
		}

		// non-zero return is an error
		if (errorNo.equals("0")==false) {
			String errorMsg = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_MESSAGE);
			throw new C30ProvConnectorException(errorMsg, "PROV-INQ-007");
		}

		/** Success
		<wireless>
		   <inquiry>
		      <method name=”getServiceOrderHistory”>
		         <error>
		            <number>0</number>
		            <message>SUCCESS</message>
		         </error>
		         <data>
		            <telephoneNumber>9071234567</telephoneNumber>
		            <serviceOrders>
		               <serviceOrder>
		                  <dateReceived>2012-01-01 08:00:00</dateReceived>
		                  <dateCompleted>2012-01-01 08:00:00</dateCompleted>
		                  <requestId>GCI.AddSample01</requestId>
		                  <requestType>ADD</requestType>
		                  <status>SUCCESS</status>
		               </serviceOrder>
		            </serviceOrders>
		         </data>
		      </method>
		   </inquiry>
		</wireless>

		 */
		// get TelephoneNumber from successful response
		String telephoneNumber = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER);
		if (telephoneNumber == null || telephoneNumber.length()==0) {
			throw new C30ProvConnectorException("No Telephone returned in Telephone validation.", "PROV-INQ-007");
		}

		HashMap response = new HashMap();
		response.put(Constants.TELEPHONE_NUMBER, telephoneNumber);
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;


	}

}

