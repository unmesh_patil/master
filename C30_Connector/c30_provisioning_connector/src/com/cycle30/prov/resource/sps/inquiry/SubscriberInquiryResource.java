package com.cycle30.prov.resource.sps.inquiry;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class SubscriberInquiryResource     extends IProvisioningResource{

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(SubscriberInquiryResource.class);

		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();

		//Construct the XML Dynamically
		String xml = C30ProvConnectorUtils.encloseWirelessInquiryTags(getSubscriberInformationXml(reqProp, requestHashMap));
		log.info("getSubscriberInformationXml = " + xml);
		C30ProvRESTConnection restConnection = new C30ProvRESTConnection();
		String responseXml = restConnection.makePOSTCall(authData.getWeburl() , xml, authData.getSslTrustStoreLocation());
		//Populating the response.
		GenericInquiryResponse imsiResponse = new GenericInquiryResponse();
		try
		{
			HashMap responseMap = populateResponse(responseXml);
			imsiResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return imsiResponse;
	}

	/** Get the Dynamic XML
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private String getSubscriberInformationXml(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException {
		String reqXml = "";

		if (requestHashMap.get(Constants.ICCID) == null  && requestHashMap.get(Constants.TELEPHONE_NUMBER) == null) 
			throw new C30ProvConnectorException("PROV-INQ-008");

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_METHOD, Constants.SPS_INQUIRY_GET_SUBSCRIBER_INFO, false, false);
		if (requestHashMap.get(Constants.ICCID) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_ICCID, (String) requestHashMap.get(Constants.ICCID), false, false);
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);

		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ID_CARRIER, C30ProvConnectorUtils.getInquiryCarrierIdByAcctSegId(reqProp.getAcctSegId()).trim(), false, false);

		//reqXml = reqXml + "<simulate>true</simulate>";

		return reqXml;
	}


	/** This is the reponse formation for the Resource
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap populateResponse(String responseXml) throws C30ProvConnectorException {

		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-002");
		}

		/** Error Response ...
		 * <wireless>
			  <inquiry>
			    <method name="getSubscriberInformation">
			      <inventory>
			        <data></data>
			        <error>
			          <message>No subscriber information found.</message>
			          <number>2004</number>
			        </error>
			      </inventory>
			      <networks>
			      </networks>
			    </method>
			  </inquiry>
			</wireless>
		 */
		// See if error occurred.
		String errorNo = C30ProvConnectorUtils.getXmlStringElementValue(responseXml,Constants.SPS_PROV_XML_TAG_NUMBER);
		if (errorNo == null || errorNo.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-007");
		}

		// non-zero return is an error
		if (errorNo.equals("0")==false) 
		{
			if (errorNo.equals(Constants.EXCEP_PORT_IN_ERROR))
			{
				/** 
				 * <error>
				        <message>This number is in the process of being ported out.</message>
				          <number>2009</number>
					</error>
					-- This is case where we need to throw this particular error. so that we can treat it as success.
				 */
				String errorMsg = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_MESSAGE);
				throw new C30ProvConnectorException(errorMsg, errorNo);
			}
			else
			{
				String errorMsg = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_MESSAGE);
				throw new C30ProvConnectorException(errorMsg, "PROV-INQ-007");
			}
		}

		/** Success
		<wireless>
		  <inquiry>
		    <method name="getSubscriberInformation">
		      <inventory>
		        <data>
		          <activationDate>2008-09-07 13:22:13</activationDate>
		          <sim>
		            <iccid>89013113700806219099</iccid>
		            <imsi>311370000021909</imsi>
		            <puk1>81758893</puk1>
		            <puk2>83045517</puk2>
		          </sim>
		          <technology>GSM</technology>
		          <telephoneNumber>9078300629</telephoneNumber>
		        </data>
		        <error>
		          <message>Success</message>
		          <number>0</number>
		        </error>
		      </inventory>
		      <networks>
		        <network name="hlr">
		          <data>
		            <authentication>
		              <sim>1</sim>
		            </authentication>
		            <features>
		              <blackberry>0</blackberry>
		              <data>
		                <enabled>0</enabled>
		                <rate>GPRS|EDGE</rate>
		              </data>
		              <mms>0</mms>
		              <sms>
		                <receive>1</receive>
		                <send>0</send>
		              </sms>
		              <tethering>0</tethering>
		              <visualVoicemail>0</visualVoicemail>
		              <voice>
		                <barringAllIncomingCalls>
		                  <active>0</active>
		                  <provisioned>1</provisioned>
		                </barringAllIncomingCalls>
		                <barringAllOutgoingCalls>
		                  <active>0</active>
		                  <provisioned>1</provisioned>
		                </barringAllOutgoingCalls>
		                <barringAllOutgoingCallsInternational>
		                  <active>0</active>
		                  <provisioned>1</provisioned>
		                </barringAllOutgoingCallsInternational>
		                <barringIncomingCallsRoaming>
		                  <active>0</active>
		                  <provisioned>1</provisioned>
		                </barringIncomingCallsRoaming>
		                <barringOffNetworkInternationalCalls>
		                  <active>1</active>
		                  <provisioned>1</provisioned>
		                </barringOffNetworkInternationalCalls>
		                <callForwardingNoReply>
		                  <active>1</active>
		                  <forwardToTelephoneNumber>19074440068</forwardToTelephoneNumber>
		                  <noReplyTimer>20</noReplyTimer>
		                  <provisioned>1</provisioned>
		                </callForwardingNoReply>
		                <callForwardingNotReachable>
		                  <active>1</active>
		                  <forwardToTelephoneNumber>19074440068</forwardToTelephoneNumber>
		                  <provisioned>1</provisioned>
		                </callForwardingNotReachable>
		                <callForwardingSubscriberBusy>
		                  <active>1</active>
		                  <forwardToTelephoneNumber>19074440068</forwardToTelephoneNumber>
		                  <provisioned>1</provisioned>
		                </callForwardingSubscriberBusy>
		                <callForwardingUnConditional>
		                  <active>0</active>
		                  <forwardToTelephoneNumber>19074440068</forwardToTelephoneNumber>
		                  <provisioned>1</provisioned>
		                </callForwardingUnConditional>
		                <callHold>1</callHold>
		                <callWaiting>1</callWaiting>
		                <lineIdentificationCallingPresentation>1</lineIdentificationCallingPresentation>
		                <lineIdentificationCallingRestriction>1</lineIdentificationCallingRestriction>
		                <lockSupplementaryServices>0</lockSupplementaryServices>
		                <multiPartyService>1</multiPartyService>
		                <operatorBarring>
		                  <incomingCalls>INTERNATIONAL_ROAM</incomingCalls>
		                  <interstateCalls>OFF</interstateCalls>
		                  <lifeLineBlock>OFF</lifeLineBlock>
		                  <outgoingCalls>INTERNATIONAL</outgoingCalls>
		                  <premiumInformationCalls>OFF</premiumInformationCalls>
		                  <roamingCalls>INTERNATIONAL</roamingCalls>
		                </operatorBarring>
		              </voice>
		              <wap>0</wap>
		            </features>
		            <msc>12085979075 ( UNKNOWN )</msc>
		            <register>1</register>
		            <roaming>1</roaming>
		            <service>VOICE AND DATA</service>
		            <vlr>12085979076 ( UNKNOWN )</vlr>
		          </data>
		          <error>
		            <message>Success</message>
		            <number>0</number>
		          </error>
		        </network>
		        <network name="wifi">
		          <data>
		            <entitlement>0</entitlement>
		            <macAddress>NOT AVAILABLE</macAddress>
		          </data>
		          <error>
		            <message>Success</message>
		            <number>0</number>
		          </error>
		        </network>
		      </networks>
		    </method>
		  </inquiry>
		</wireless>
		 */
		// get TelephoneNumber from successful response
		String telephoneNumber = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER);
		if (telephoneNumber == null || telephoneNumber.length()==0) {
			throw new C30ProvConnectorException("No Telephone returned in Telephone validation.", "PROV-INQ-007");
		}

		HashMap response = new HashMap();
		response.put(Constants.TELEPHONE_NUMBER, telephoneNumber);
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;


	}

}
