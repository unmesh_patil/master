package com.cycle30.prov.resource.sps.inquiry;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ProvRESTConnection;

public class TelephoneNumberInquiryResource  extends IProvisioningResource{

	boolean simulateRequest = false;
	String telephoneNumber = null;
	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(TelephoneNumberInquiryResource.class);

		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		
		simulateRequest = (Boolean) requestHashMap.get(Constants.ATTR_SIMULATE_REQUEST);	

		if ( requestHashMap.get(Constants.TELEPHONE_NUMBER) != null ) {
			telephoneNumber = (String) requestHashMap.get(Constants.TELEPHONE_NUMBER);
		}
		else {
			throw new C30ProvConnectorException("PROV-INQ-010");
		}		
		
		String responseXml = null;		
		if (!simulateRequest) {
			//Build the Dynamic XML
			String xml = C30ProvConnectorUtils.encloseWirelessInquiryTags(getValidateTNXml(reqProp, requestHashMap));
			log.info("getValidateTNXml = " + xml);
			C30ProvRESTConnection restConnection = new C30ProvRESTConnection();
			responseXml = restConnection.makePOSTCall(authData.getWeburl() , xml, authData.getSslTrustStoreLocation());
		}
		else {
			responseXml = getSimulateValidateTNResponse(telephoneNumber);
		}

		//Populating the response.
		GenericInquiryResponse imsiResponse = new GenericInquiryResponse();
		try
		{
			HashMap responseMap = populateResponse(responseXml);
			imsiResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}
		
		return imsiResponse;
	}
	
	private String getValidateTNXml(
			ProvisioningRequestProperties reqProp, HashMap requestHashMap) throws C30ProvConnectorException {
		String reqXml = "";
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_METHOD, Constants.SPS_INQUIRY_VALIDATE_TN, false, false);
		
		if (requestHashMap.get(Constants.TELEPHONE_NUMBER) != null ) 
			reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER, (String) requestHashMap.get(Constants.TELEPHONE_NUMBER), false, false);
		else
			throw new C30ProvConnectorException("PROV-INQ-010");
		
		reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_ID_CARRIER, C30ProvConnectorUtils.getInquiryCarrierIdByAcctSegId(reqProp.getAcctSegId()).trim(), false, false);
		
		//reqXml = reqXml + "<simulate>true</simulate>";

		return reqXml;
	}

	/** This is the response formation for the Simulate Request
	 * 
	 * @param responseXml
	 * @return
	 * 
	 */
	
	private String getSimulateValidateTNResponse(String telephoneNumber) {
		String resXml = "";

		resXml = "<wireless><inquiry>";
		resXml = resXml + "<method name=\"validateTN\"><data>";
		resXml = resXml + "<telephoneNumber>"+telephoneNumber+"</telephoneNumber>";
		resXml = resXml + "</data><error><message>Success</message>";
		resXml = resXml + "<number>0</number>";
		resXml = resXml + "</error></method></inquiry></wireless>";
		return resXml;
	}


	/** This is the reponse formation for the Resource
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap populateResponse(String responseXml) throws C30ProvConnectorException {

		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-002");
		}

		/** Error Response ...
		 * <wireless>
			  <inquiry>
			    <method name="validateTN">
			      <data>
			        <telephoneNumber>90739575911</telephoneNumber>
			      </data>
			      <error>
			        <message>Invalid telephone number format.</message>
			        <number>1011</number>
			      </error>
			    </method>
			  </inquiry>
			</wireless>
		 */
		// See if error occurred.
		String errorNo = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_NUMBER);
		if (errorNo == null || errorNo.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-003");
		}

		// non-zero return is an error
		if (errorNo.equals("0")==false) {
			String errorMsg = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_PROV_XML_TAG_MESSAGE);
			throw new C30ProvConnectorException("Error returned in Telephone validation: " + errorMsg, "PROV-INQ-006");
		}

		/** Success
		<wireless>
		  <inquiry>
		    <method name="validateTN">
		      <data>
		        <telephoneNumber>9073957591</telephoneNumber>
		      </data>
		      <error>
		        <message>Success</message>
		        <number>0</number>
		      </error>
		    </method>
		  </inquiry>
		</wireless>
		*/
		// get TelephoneNumber from successful response
		String telephoneNumber = C30ProvConnectorUtils.getXmlStringElementValue(responseXml, Constants.SPS_WIRELESS_XML_TAG_TELEPHONE_NUMBER);
		if (telephoneNumber == null || telephoneNumber.length()==0) {
			throw new C30ProvConnectorException("No Telephone returned in Telephone validation.", "PROV-INQ-005");
		}

		HashMap response = new HashMap();
		response.put(Constants.TELEPHONE_NUMBER, telephoneNumber);
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;


	}

}
