package com.cycle30.prov.resource.sps.inquiry;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;

public class SPSRequestStatusResource   extends IProvisioningResource{

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(SPSRequestStatusResource.class);

		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();

		String responseStatus ="";
		//Get the RequestId
		String requestId ="";
		if ( requestHashMap.get(Constants.REQUEST_ID) != null)
			requestId = (String) requestHashMap.get(Constants.REQUEST_ID);
		else
			throw new C30ProvConnectorException("PROV-VALIDATE-011");


		//Make SQL call to get the actual request Status
		Integer statusId = C30ProvConnectorUtils.getStatusofRequestId(requestId,datasource);

		if (statusId == null ) throw new C30ProvConnectorException("PROV-INQ-011");
		
		if (statusId.equals(Constants.STATUS_REQUEST_COMPLETED)  || 
				statusId.equals(Constants.STATUS_REQUEST_SIMULATE_COMPLETED) ) 
			responseStatus = Constants.STATUS_REQUEST_IS_COMPLETED;

		if (statusId.equals(Constants.STATUS_REQUEST_IN_ERROR)  || 
				statusId.equals(Constants.STATUS_REQUEST_SIMULATE_IN_ERROR) ) 
			responseStatus = Constants.STATUS_REQUEST_ERROR;

		if (statusId.equals(Constants.STATUS_REQUEST_INITIALIZED)  || 
				statusId.equals(Constants.STATUS_REQUEST_SENT_TO_SPS) || 
				statusId.equals(Constants.STATUS_REQUEST_SIMULATE_SENT_TO_SPS) ||
				statusId.equals(Constants.STATUS_REQUEST_PROCESSING_AT_SPS) ||
				statusId.equals(Constants.STATUS_REQUEST_SYNC_RESPONSE_RECIEVED) ||
				statusId.equals(Constants.STATUS_REQUEST_ASYNC_RESPONSE_RECIEVED)   ) 
			responseStatus = Constants.STATUS_REQUEST_PENDING;

		//Populating the response.
		GenericInquiryResponse genericResp = new GenericInquiryResponse();

		HashMap responseMap = new HashMap();
		responseMap.put(Constants.STATUS, responseStatus);

		genericResp.setResponseMap(responseMap);

		return genericResp;
	}


}
