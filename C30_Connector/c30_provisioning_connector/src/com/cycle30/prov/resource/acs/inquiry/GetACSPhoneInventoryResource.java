package com.cycle30.prov.resource.acs.inquiry;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30ACSRESTConnection;

public class GetACSPhoneInventoryResource  extends IProvisioningResource{

	boolean simulateRequest = false;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(GetACSPhoneInventoryResource.class);

		log.info("Reached the GetACSPhoneInventoryResource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		ArrayList<NameValuePair> headers=null;
		String secretKey=null;
		String acsOrgId=null;
		
		final Date currentTime = new Date();
	
		final SimpleDateFormat sdf = new SimpleDateFormat("EEE,d MMM yyyy hh:mm:ss z");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		if(requestHashMap.get(Constants.ACCT_SEG_ID)==null) {
			throw new C30ProvConnectorException(" Error Message from Provisioning Connector : " + "Account Segment is NULL", "ACS-EXCEPTION-002");
		}
		
		/**
		 * Property1 will hold the ACS_SECRET_KEY and Property2 will hold ACS_ORG_ID header values 	
		 */
		if ( (authData.getproperty1()!=null  && authData.getproperty2() !=null)){
			 secretKey=authData.getproperty1(); 	
			 acsOrgId=authData.getproperty2();
		}
		
		if((acsOrgId ==null && "".equalsIgnoreCase(acsOrgId)) || (secretKey ==null && "".equalsIgnoreCase(secretKey)) ){
			throw new C30ProvConnectorException(" Error Message from C30ProvConnectorException : " + "ACS Headers are NULL", "ACS-EXCEPTION-002"); 
		}
		
		simulateRequest = (Boolean) requestHashMap.get(Constants.ATTR_SIMULATE_REQUEST);
		
       	 headers = new ArrayList<NameValuePair>();
         headers.add(new NameValuePair("Content-type", "application/xml"));
       	 
			headers.add(new NameValuePair(Constants.PREPAID_ACS_HEADER_X_ORG_ID, acsOrgId));
			try {
				headers.add(new NameValuePair(Constants.PREPAID_ACS_HEADER_X_DIGEST, getACSDigestValue(acsOrgId,secretKey,log)));
			} catch (Exception e1) {
				throw new C30ProvConnectorException(e1.getMessage()); 
			}
			headers.add(new NameValuePair(Constants.PREPAID_ACS_HEADER_DATE, sdf.format(currentTime)));
			log.info("ACS Headers  + " + acsOrgId +"--- "+ sdf.format(currentTime));
	C30ACSRESTConnection restConnection = new C30ACSRESTConnection();
        String webURL =  authData.getWeburl().concat(Constants.ACS_PHONE_INVENTORY_INQUIRY);
        String responseXml=null;
        
        if(requestHashMap.get(Constants.PREPAID_ACS_ATTRIBURE_ACTION_TYPE) ==null){
        	
        StringBuffer parametersToLog= new StringBuffer();
        parametersToLog.append(Constants.ACS_PHONE_INVENTORY_INQUIRY+ "?");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        
        if(requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_COMMUNITY)!=null && requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SIZE) !=null){
        	parametersToLog.append(Constants.PREPAID_ACS_PARAM_COMMUNITY+"="+requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_COMMUNITY)+"|");
        	params.add(new NameValuePair(Constants.PREPAID_ACS_PARAM_COMMUNITY, requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_COMMUNITY).toString()));
        	parametersToLog.append(Constants.PREPAID_ACS_PARAM_SIZE+"="+requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SIZE));
        	params.add(new NameValuePair(Constants.PREPAID_ACS_PARAM_SIZE,requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SIZE).toString()));
       
           }
         if(requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER)!=null){
	    	   parametersToLog.append(Constants.PREPAID_ACS_PARAM_TELEPHONE_NUMBER+"="+requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER)+"|");
	    	   params.add(new NameValuePair(Constants.PREPAID_ACS_PARAM_TELEPHONE_NUMBER,requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER).toString()));
	        }
         if(requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS)!=null){
	        	 parametersToLog.append(Constants.PREPAID_ACS_PARAM_INV_STATUS+"="+requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS)+"|");
	        	params.add(new NameValuePair(Constants.PREPAID_ACS_PARAM_INV_STATUS,requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS).toString()));
	        }
          if(requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID)!=null){
	        	 parametersToLog.append(Constants.PREPAID_ACS_PARAM_SALES_CHANNEL_ID+"="+requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID)+"|");
	        	params.add(new NameValuePair(Constants.PREPAID_ACS_PARAM_SALES_CHANNEL_ID,requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID).toString()));
	        }	
	        	
	        		
	    		if(params.isEmpty()) {	    			
	    			throw new C30ProvConnectorException(" Error Message from ACS : " + "Params are NULL", "ACS-EXCEPTION-002");
	    		}else{
	    			
	    			 log.info("Web URL for Prepaid Phone Inventory Inquiry Query : " + webURL );
		    		 requestHashMap.put(Constants.REQUEST_XML, parametersToLog.toString());
		    		 new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
		    		 responseXml = restConnection.makeACSCall(webURL,Constants.HTTP_GET,headers, null,params);
				} 
	    		 
        
        }else if(requestHashMap.get(Constants.PREPAID_ACS_ATTRIBURE_ACTION_TYPE) !=null ){
        	
        	// (String) requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_INVENTORY_PAYLOAD);
        	String salesChannelId="";
        	String operatorId="";
        	if (requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID)!=null){
        		 salesChannelId=(String)requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID);
        	}
        	if (requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_OPERATOR_ID)!=null){
        		operatorId=(String)requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_OPERATOR_ID);
        	}
   
        	String payload= C30ProvConnectorUtils.encloseACSTags(getPayloadforACS(reqProp, requestHashMap,salesChannelId,operatorId));
        	log.info("XML  Formed " +payload);
        	 requestHashMap.put(Constants.REQUEST_XML, payload);
     		 new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
     		 responseXml = restConnection.makeACSCall(webURL,Constants.HTTP_PUT,headers,payload ,null);
        }

		//Populating the response.
		GenericInquiryResponse authenticationResponse = new GenericInquiryResponse();
		try
		{
			HashMap responseMap =populateResponse(request,responseXml);
			authenticationResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return authenticationResponse;
	}

	private String getPayloadforACS(ProvisioningRequestProperties reqProp, HashMap requestHashMap,String salesChannelId ,String operatorId) {
		String reqXml="";
		String statusId="";
		Logger log = Logger.getLogger(GetACSPhoneInventoryResource.class);
		log.info("Check the requestHashMap "+requestHashMap.toString());
		
		
		if(requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS) !=null ){
			statusId=C30ProvConnectorUtils.getTNStatusIdForGivenStatus((String)requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS));
		}
		
		if (requestHashMap.containsKey(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER)) {	
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.PREPAID_ACS_XML_TELEPHONE_NUMEBR_TAG,requestHashMap.get(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER).toString(), false, false);				
		}
			reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.PREPAID_ACS_XML_STATUS_ID_TAG,statusId, false, false);			
	        reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.PREPAID_ACS_XML_SALES_CHANNEL_ID_TAG,salesChannelId, true, true);
			reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.PREPAID_ACS_XML_OPERATOR_ID_TAG,operatorId,true,true);
		
		log.info("XML Formed  "+reqXml);
		return reqXml;
		
	}

	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {


		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-012");
		}

		String  errorNo=null;
		String errorMsg=null;
		if(responseXml !=null){
			try {
        HashMap<?, ?> errordetails= C30ProvConnectorUtils.parseACSFaultResponse(responseXml, "Code", "Message");
        if(errordetails !=null &&  errordetails.get(0)!=null){
          HashMap<?, ?> stackDetails=	(HashMap<?, ?>) errordetails.get(0);
           
            if(stackDetails.get("ErrorCode") !=null && stackDetails.get("ErrorMsg") !=null){
            	 errorNo= "ACS-"+stackDetails.get("ErrorCode").toString();
            	 errorMsg= stackDetails.get("ErrorMsg").toString() ;
					}

				}
			} catch (Exception e) {
				throw new C30ProvConnectorException(e, "PROV-019");
			}
		}

		// non-zero return is an error
		if (errorNo != null ) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			throw new C30ProvConnectorException(" Error Message from ACS : " + errorMsg, errorNo);
		} 


		HashMap<String, String> response = new HashMap<String,String>();
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		response.put(Constants.RESPONSE_XML, responseXml);
		if(request.getRequestProperties().getRequestMap().get(Constants.PREPAID_ACS_ATTRIBURE_ACTION_TYPE) ==null){
			try {
				HashMap<Integer, HashMap<String, String>> bundleResult =  new HashMap<Integer, HashMap<String, String>>();
				bundleResult=C30ProvConnectorUtils.parseXMLForGCIRetreiveDetails(responseXml);
				HashMap<String,String> resultMap = (HashMap<String, String>) bundleResult.get(0);
				response.put(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER, resultMap.get(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER));
				response.put(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS, resultMap.get(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS));
				response.put(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID, resultMap.get(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID));
				response.put(Constants.PREPAID_ACS_ATTRIBUTE_OPERATOR_ID, resultMap.get(Constants.PREPAID_ACS_ATTRIBUTE_OPERATOR_ID));
				response.put("CreateDate", resultMap.get("CreateDate"));
				response.put("NetworkId", resultMap.get("NetworkId"));				
				
			} catch (Exception  e) {
				throw new C30ProvConnectorException(" Error Message while Parsing the Response from ACS : " + e.getMessage(), "1");
			}
		}
		return response;
		

	}
	
	String getACSDigestValue(String acsOrgId,String secretKey, Logger log) throws NoSuchAlgorithmException{
		MessageDigest msgd= MessageDigest.getInstance("MD5");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		String datetransformed=(sdf.format(new Date()));
		msgd.update((acsOrgId.concat(secretKey)+datetransformed).getBytes());
		 byte byteData[]= msgd.digest();
		 StringBuffer sb= new StringBuffer();
		 for(int i=0; i <byteData.length;i++){
			 sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		 }
		 
		 log.info("Digest Value :" +sb.toString());
		 return sb.toString();
		
	}

}

