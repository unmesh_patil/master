package com.cycle30.prov.resource.incomm.inquiry;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30IncommRESTConnection;
import com.cycle30.prov.webservice.C30OCSRESTConnection;

public class IncommVoucherResource  extends IProvisioningResource{

	boolean simulateRequest = false;
	String voucherPIN = null;
	String voucherAction=null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(IncommVoucherResource.class);

		log.info("Reached the Incomm Voucher Status Resource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String, String> requestHashMap = reqProp.getRequestMap();
       
		if ( requestHashMap.get(Constants.VOUCHER_PIN) != null && requestHashMap.get(Constants.VOUCHER_ACTION) !=null )
		{
			voucherPIN = (String) requestHashMap.get(Constants.VOUCHER_PIN);
			voucherAction= (String)requestHashMap.get(Constants.VOUCHER_ACTION);
			log.info("VoucherPIN  " + voucherPIN );
			log.info("VoucherAction  " + voucherAction );
		}
		else
		{
			throw new C30ProvConnectorException(" Error Message from Incomm : " + "Vocuher PIN or Action is Null ", "INCOMM-VOUCHER-001");
		}
		
		

		String xml=null;
		 xml = C30ProvConnectorUtils.encloseIncommInquiryTags(getIncommVoucherStatusXml(reqProp, requestHashMap));
		 log.info("XML formed for the INCOMM Voucher Status Request" + xml);
		

		 requestHashMap.put(Constants.REQUEST_XML, xml);
		 new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
		C30IncommRESTConnection restConnection = new C30IncommRESTConnection();
		String responseXml = restConnection.makeIncommPOSTCall(authData.getWeburl(), xml);

		//Populating the response.
		GenericInquiryResponse response = new GenericInquiryResponse();
		try
		{
			HashMap<String, String> responseMap = populateResponse(request,responseXml);
			response.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return response;
	}

	
	private String getIncommVoucherStatusXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String partnerName = Constants.GCI; //default
		if (reqProp.getAcctSegId().equals(Constants.ACCT_SEG_ID_5) || reqProp.getAcctSegId().equals(Constants.ACCT_SEG_ID_3)){ // ACS prepaid or regular ACS
			 // Changed the partnerNAme as GCI - GCI has acquired wireless ACS ,post 02/02/2015 in PROD all the ACS Incomm Vouchers needs to be redeemed as GCI
			 //partnerName = "ACS Wireless"; 
			partnerName = Constants.GCI;
		}
		
		String reqXml = "";

		if (requestHashMap.get(Constants.VOUCHER_PIN) != null ) {
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.INCOMM_XML_TAG_REQUEST_ACTION,(String)requestHashMap.get(Constants.VOUCHER_ACTION), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.INCOMM_XML_TAG_DATE, new SimpleDateFormat("yyyymmdd").format(new Date()), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.INCOMM_XML_TAG_TIME, new SimpleDateFormat("hhmmss").format(new Date()), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.INCOMM_XML_TAG_PARTNER_NAME, partnerName, false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.INCOMM_XML_TAG_CARD_ACTION_INFO, 
		    C30ProvConnectorUtils.createXmlTag(Constants.INCOMM_XML_TAG_PIN,(String) requestHashMap.get(Constants.VOUCHER_PIN), false, false) +
		    C30ProvConnectorUtils.createXmlTag(Constants.INCOMM_XML_TAG_SRC_REF_NUM, (new Integer((new Random().nextInt(Integer.MAX_VALUE))).toString()), false, false),false,false);
		    		
		}
		return reqXml;
	}


	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap<String, String> populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {


		if (responseXml == null || responseXml.length() == 0) {
			throw new C30ProvConnectorException("INCOMM-VOUCHER-001");
		}
		
		String errorNo = null;
		String errorMsg = null;
		String faceValue=null;
		if (responseXml != null) {
			try {
				HashMap<?, ?> errordetails = C30ProvConnectorUtils.parseIncommFaultResponse(responseXml);
				if (errordetails != null && errordetails.get(0) != null) {
					HashMap<?, ?> stackDetails = (HashMap<?, ?>) errordetails.get(0);

					if (stackDetails.get("RespCode") != null && stackDetails.get("RespMsg") != null) {
						errorNo = "INCOMM-"+ stackDetails.get("RespCode").toString();
						errorMsg = stackDetails.get("RespMsg").toString();
					}
					if (stackDetails.get("FaceValue") != null ) {
						faceValue =  stackDetails.get("FaceValue").toString();
					}

				}
			} catch (Exception e) {
				throw new C30ProvConnectorException(e, "PROV-017");
			}
		}
		
		// Success    4001 Card is Active		
		// Success 0 Upon successful redeem
		// Error code 4002 Card is Deactive
		// Error code 4003 Card is Redeemed
		// Error code 4006 Card Not Found
		// Error code 43 Card is Invalid
		// Error code 46 Card is Deactive
		// Error code 38 Card is Redeemed
		// Error code 82 Unable to Redeem
		

		// non-zero return is an error
		if (errorNo != null &&  !(errorNo.equalsIgnoreCase("INCOMM-4001") || errorNo.equalsIgnoreCase("INCOMM-0") )) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			throw new C30ProvConnectorException(" Error Message from INCOMM : " + errorMsg, errorNo);
		} 
		
		HashMap<String, String> response = new HashMap<String, String>();
		response.put(Constants.RESPONSE_XML, responseXml);
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		response.put("FaceValue", faceValue);
		return response;


	}

}

