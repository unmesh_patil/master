package com.cycle30.prov.resource.globecomm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvCustomConfig;
import com.cycle30.prov.config.C30ProvFeatureItem;
import com.cycle30.prov.config.C30ProvSubscriberProfileDCN;
import com.cycle30.prov.config.FeatureActions;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30GlobeCommConnection;

public class DCNSubscriberResource  extends IProvisioningResource{

	boolean simulateRequest = false;
	Integer subscriberAction=null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(DCNSubscriberResource.class);

		log.info("Reached the Add Subcriber For DCN Resource class" );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();

		ArrayList<FeatureActions> featureItemActionsForGivenResource =  (ArrayList<FeatureActions>) requestHashMap.get(Constants.ATTR_FEATURE_ITEM_ACTIONS);
		//Loop through each Voice Resource 
		for (FeatureActions fActions : featureItemActionsForGivenResource)
		{
			C30ProvFeatureItem featureItem =  fActions.getFeatureItem();
			Integer featureItemId = featureItem.getFeatureItemId();
			Integer featureItemActionId = fActions.getFeatureItemActionId();

			if (featureItemId.equals(Constants.FEATURE_ITEM_DCN_SUBSCRIBER))
			{
				subscriberAction = featureItemActionId;
			}

		}

		if (subscriberAction ==null)
		{
			throw new C30ProvConnectorException(" Error Message from GLOBECOMM : " + "Subscriber Action is Null ", "GLOBECOMM-001");
		}

		//Update the RequestId
		requestHashMap.put(Constants.GLOBECOMM_REQUEST_ID, reqProp.getRequestId());
		requestHashMap.put(Constants.GLOBECOMM_SOURCE_NODE, reqProp.getAuthClient().getProvCallbackSystemId());
		C30ProvSubscriberProfileDCN subcriberprofile=null;

		/** Getting the profileId based on the accountsegment, accountNumber and catalogId */
		String getCriteria = requestHashMap.get(Constants.ACCT_SEG_ID)
				+"_"+requestHashMap.get(Constants.ACCOUNT_NUMBER) +"_"+requestHashMap.get(Constants.ATTR_CATALOG_ID);

		if(C30ProvCustomConfig.getInstance() !=null)
			subcriberprofile =  C30ProvCustomConfig.getInstance().getSubscriberProfileList().get(getCriteria);

		/** If the combination of the catalogId, AccountSegment and AccountNumber is not configured then we read the
		 *  default profileId for the given account segment and accountnumber.
		 *  
		 */
		getCriteria = requestHashMap.get(Constants.ACCT_SEG_ID)	+"_"+requestHashMap.get(Constants.ACCOUNT_NUMBER);;
		if(subcriberprofile !=null && (subcriberprofile.getSubsciberProfileId()!=null && subcriberprofile.getNetworkType()!=null))
			subcriberprofile =  C30ProvCustomConfig.getInstance().getSubscriberProfileList().get(getCriteria);


		if(subcriberprofile !=null && (subcriberprofile.getSubsciberProfileId()!=null && subcriberprofile.getNetworkType()!=null))
		{
			//Other configurationItems but they are Hardcoded now..
			requestHashMap.put(Constants.GLOBECOMM_SUBSCRIBER_PROFILE_ID,subcriberprofile.getSubsciberProfileId().toString());
			requestHashMap.put(Constants.GLOBECOMM_SUBSCRIBER_NETWORK_TYPE,subcriberprofile.getNetworkType().toString());

		}else{

			throw new C30ProvConnectorException(" Error Message from GLOBECOMM : " + "Mandatory Parameter is Not configured In provsioning Table ", "GLOBECOMM-001");

		}


		requestHashMap.put(Constants.GLOBECOMM_SUBSCRIBER_FIXED_IP,subcriberprofile.getFixedIpAddress());
		requestHashMap.put(Constants.GLOBECOMM_SUBSCRIBER_OPERATOR,subcriberprofile.getSubsciberProfileName());
		requestHashMap.put(Constants.GLOBECOMM_SUBSCRIBER_ROAMING_PROFILE_ID,subcriberprofile.getRoamingProfileId());
		requestHashMap.put(Constants.GLOBECOMM_SUBSCRIBER_NETWORK_TYPE,subcriberprofile.getNetworkType());
		requestHashMap.put(Constants.GLOBECOMM_SUBSCRIBER_TYPE,subcriberprofile.getSusbscriberType());


		if (  requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_IMSI) ==null|| requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_MSIDN) ==null)
		{

			throw new C30ProvConnectorException(" Error Message from GLOBECOMM : " + "Mandatory Parameter is Null ", "GLOBECOMM-001");
		}


		//Add DCN Subscriber Request formation
		String xml=null;
		xml = C30ProvConnectorUtils.encloseGLOBECOMMEnvelopeTags(getAddDCNSubscriberXml(reqProp, requestHashMap));
		log.info("XML formed for the Add DCN Subscriber Request" + xml);


		requestHashMap.put(Constants.REQUEST_XML, xml);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);

		ArrayList<NameValuePair> headers=null;

		headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair(Constants.GLOBECOMM_SOAP_ACTION_HEADER_NAME,Constants.GLOBECOMM_SOAP_ACTION_HEADER_VALUE));

		String webURL =  authData.getWeburl().concat(Constants.GLOBECOMM_ADD_SUBSCRIBER_URI);


		C30GlobeCommConnection globeCommConn = new C30GlobeCommConnection();
		String responseXml = globeCommConn.makeGlobeCommPOSTCall(webURL, xml,headers, authData.getUserName(), authData.getPassword());


		//Populating the response.
		GenericInquiryResponse response = new GenericInquiryResponse();
		try
		{
			HashMap<String, String> responseMap = populateResponse(request,responseXml);
			response.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return response;
	}


	private String getAddDCNSubscriberXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String reqXml = "";

		reqXml= reqXml+ C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_COMMON_DATA_SET_TAG, 
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_COMMON_DATA_SET_PARAM_NAME_TAG,"", true, true)  +
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_COMMON_DATA_SET_PARAM_VALUE_TAG,"", true, true), false, false);

		reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_COMMON_DTO_REQUEST_ID_TAG,requestHashMap.get(Constants.GLOBECOMM_REQUEST_ID).toString(), false, false);
		reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_COMMON_DTO_SOURCE_NODE_TAG,(String) requestHashMap.get(Constants.GLOBECOMM_SOURCE_NODE), false, false);
		reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_COMMON_DTO_TIME_STAMP_TAG,new SimpleDateFormat("ddmmyyyy hh:mm:ss:mmm: Z").format(new Date()), false, false);
		reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_DTO_OPERATOR_TAG, (String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_OPERATOR),true,true);

		reqXml= reqXml+ C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_ADD_SUBSCRIBER_DTO_DATA_TAG,
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_FIXED_IP_TAG, (String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_FIXED_IP),true,true) +
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_NETWORK_TYPE_TAG, (String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_NETWORK_TYPE),false,false) +
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_PROFILE_ID_TAG, (String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_PROFILE_ID),false,false) +
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_ROAMING_PROFILE_ID_TAG,(String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_ROAMING_PROFILE_ID),true,true) +
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_TYPE_TAG, (String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_TYPE),false,false), false,false);


		reqXml= reqXml +C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_ADD_SUBSCRIBER_DTO_INFO_TAG, 
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_IMSI_TAG, (String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_IMSI),false,false) +
				C30ProvConnectorUtils.createXmlTag(Constants.GLOBECOMM_SUBSCRIBER_MSIDN_TAG, (String) requestHashMap.get(Constants.GLOBECOMM_SUBSCRIBER_MSIDN),false,false) ,false,false);	

		reqXml= C30ProvConnectorUtils.encloseGLOBECOMMAddSubcriberTags(reqXml);
		return reqXml;
	}


	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap<String, String> populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {


		if (responseXml == null || responseXml.length() == 0) {
			throw new C30ProvConnectorException("GLOBECOMM-ADDSUBSCRIBER-001");
		}

		String errorNo = null;
		String errorMsg = null;
		if (responseXml != null) {
			try {
				HashMap<?, ?> errordetails = C30ProvConnectorUtils.parseGlobeCommFaultResponse(responseXml);
				if (errordetails != null && errordetails.get(0) != null) {
					HashMap<?, ?> stackDetails = (HashMap<?, ?>) errordetails.get(0);

					if (stackDetails.get("RespCode") != null && stackDetails.get("RespMsg") != null) {
						errorNo = "GLOBECOMM-"+ stackDetails.get("RespCode").toString();
						errorMsg = stackDetails.get("RespMsg").toString();
					}
				}
			} catch (Exception e) {
				throw new C30ProvConnectorException(e, "PROV-017");
			}
		}

		/*---------------- Response Codes from GLOBECOMM ---- 
		SC0000	SUCCESS 
		SC0002	FAILURE-Missing Mandatory Fields.
		SC0003	FAILURE-Undefined tag is coming with request.
		SC0004	FAILURE- Client  IP Validation Failed
		SY0001	FAILURE-Unkown System Failure
		SY0500	FAILURE- Network Connection Time Out
		SY0501	FAILURE- Network Read Time Out
		SY0502	FAILURE- DB Connection Link Failure 
		SY0503	FAILURE- DB Query Execution Failure
		SY0504	FAILURE- System Threshold Limit Reached
       -----------------------------------------------------*/ 

		// non-zero return is an error
		if (errorNo != null &&  !(errorNo.equalsIgnoreCase("GLOBECOMM-SC0000") || errorNo.equalsIgnoreCase("GLOBECOMM-0") )) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			throw new C30ProvConnectorException(" Error Message from GLOBECOMM : " + errorMsg, errorNo);
		} 

		HashMap<String, String> response = new HashMap<String, String>();
		response.put(Constants.RESPONSE_XML, responseXml);
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);

		return response;


	}

}

