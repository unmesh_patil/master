package com.cycle30.prov.resource.ocs;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30OCSRequestUtils;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;

public class SubscriberBalanceResource  extends IProvisioningResource{

	String headerXML = null;
	String requestBodyXML = null;
	String footerXML = null;
	StringBuffer headerStr = new StringBuffer(Constants.OCS_XML_HEADER);
    StringBuffer footerStr = new StringBuffer();

	Logger log = Logger.getLogger(SubscriberBalanceResource.class);
	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {
		
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String,String> requestHashMap = reqProp.getRequestMap();

		String requestXml = "";
		String footerTags = "";
		String methodTags = "";
		log.info("requestHashMap : "+requestHashMap.toString());
		//Request Header Tag
		requestXml = Constants.OCS_RESPONSE_XML_HEADER;
		String methodName = requestHashMap.get("MethodName").toString();
		
		methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, methodName,false,false);
		
		if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_RECHARGE)) {
			
			HashMap<String, String> balrequestHashMap = new HashMap();
			String xml=null;
			String ExpirationDate = null;
			String Value = null;
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
				balrequestHashMap.put(Constants.PREPAID_SUBSCRIBERID, requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString());
			}

			//Call GET Subscriber Balances to retrieve the current expiration date of the COMMON Balance
			xml = C30OCSRequestUtils.getSubscriberGetBalancesXml(balrequestHashMap);			
			
			requestHashMap.put(Constants.REQUEST_XML, xml);
			C30ProvConnectorUtils.generateNewRequestId(request,datasource);
			new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
			
			//Make a POST call to OCS
	        C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
			String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), xml);
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
			//Populating the response.
			GenericInquiryResponse subscriberResponse = new GenericInquiryResponse();
			
			try
			{
				HashMap<String, String> responseMap = C30ProvConnectorUtils.populateOCSResponse(responseXml);
				
				subscriberResponse.setResponseMap(responseMap);
				ExpirationDate = C30ProvConnectorUtils.getOCSMemberValuefromResponse(responseXml.toString(), Constants.PREPAID_EXPIRATIONDATE);	
				
				//If the value is 10099, then the value should be $100.99
				Value = requestHashMap.get(Constants.PREPAID_VALUE).toString();
		   		String NofDays = Value.substring(0, Value.length() -2);
				Value = new StringBuffer(Value).insert(Value.length()-2, ".").toString();
				log.info("Value = " +Value);
				log.info("NofDays = " +NofDays);
				ExpirationDate = C30ProvConnectorUtils.getOCSDateFormatFromString(ExpirationDate, NofDays, false);
				log.info("ExpirationDate = "+ExpirationDate);
			}
			catch(C30ProvConnectorException excp)
			{
				throw excp;
			}			
			catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new C30ProvConnectorException("ERROR- Subscriber.GetBalances: "+e.getMessage());				
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new C30ProvConnectorException("ERROR- Subscriber.GetBalances: "+e.getMessage());				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new C30ProvConnectorException("ERROR- Subscriber.GetBalances: "+e.getMessage());				
			}
			catch(Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new C30ProvConnectorException("ERROR- Subscriber.GetBalances: "+e.getMessage());				
			}				
			
			log.info("getSubscriberRechargeXml START ");
	        //Subscriber.Recharge XML		
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberRechargeXml(requestHashMap, Value, ExpirationDate), methodTags, footerTags);

			
		} 
		
        log.info("requestXml : " + requestXml);
        
		
		requestHashMap.put(Constants.REQUEST_XML, requestXml);
		C30ProvConnectorUtils.generateNewRequestId(request,datasource);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
		//Make a POST call to OCS
        C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
		String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), requestXml);
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		//Populating the response.
		GenericInquiryResponse subscriberResponse = new GenericInquiryResponse();
		
		try
		{
			HashMap<String, String> responseMap = C30ProvConnectorUtils.populateOCSResponse(responseXml);
			subscriberResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException excp)
		{
			throw excp;
		}

		return subscriberResponse;
		
	}

	/** Get the Subscriber GetBalances XML
	 * 
	 * @param requestHashMap 
	 * @return requestPayload
	 * 
	 */	
	private String getPrepaidBalanceXml(HashMap<?, ?> balrequestHashMap) {
		String reqXml = "";

		reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_BALANCES, false, false);
		reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, (String) balrequestHashMap.get(Constants.SUBSCRIBER_ID), false, false);

		return reqXml;
	}	

/** Get the Subscriber Recharge XML
	 * 
	 * @param requestHashMap 
	 * @param Value
	 * @param ExpirationDate
	 * @return requestPayload
	 * 
	 */
	private String getSubscriberRechargeXml(HashMap requestHashMap, String Value, String ExpirationDate)	
	{

		String requestPayload = "";
		String level1payload = "";		
		String level2payload = "";
		
//		//If the value is 10099, then the value should be $100.99
//		String Value = requestHashMap.get(Constants.PREPAID_VALUE).toString();
//		Value = new StringBuffer(Value).insert(Value.length()-2, ".").toString();
		
//		log.info("Value = " +Value);
//		ExpirationDate = C30ProvConnectorUtils.getOCSDateFormatFromString(ExpirationDate, Value);
		
		// Build SubscriberId.
		requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
				Constants.OCS_XML_TAG_INT, false);
		// Build COMMON Balance Type.
		level1payload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_BALANCE_TYPE_LABEL, 
				requestHashMap.get(Constants.PREPAID_BALANCE_TYPE_LABEL).toString(), Constants.OCS_XML_TAG_STRING);
		level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, 
				Value, Constants.OCS_XML_TAG_DOUBLE);
//		level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, 
//				requestHashMap.get(Constants.PREPAID_EXPIRATIONDATE).toString(), Constants.OCS_XML_TAG_STRING);
		level1payload = level1payload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, 
				ExpirationDate, Constants.OCS_XML_TAG_STRING);
		level1payload = C30ProvConnectorUtils.encloseOCSRequestParamTags(C30ProvConnectorUtils.encloseOCSRequestRootlevel3Tags(level1payload), 
				Constants.OCS_XML_TAG_ARRAY, false);
		//Build Reason.
		level2payload = level2payload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_REASON).toString(), 
				Constants.OCS_XML_TAG_STRING, false);
		//Build Final XML
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload+level1payload+level2payload, false, false);
		
		return requestPayload;		
		
	}

}