package com.cycle30.prov.resource.ocs.inquiry;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;

public class GetPrepaidBalanceTypesResource  extends IProvisioningResource{

	

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(GetPrepaidBalanceResource.class);

		log.info("Reached the GetPrepaidBalanceResource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String, String> requestHashMap = reqProp.getRequestMap();
       
		
	
		String xml = C30ProvConnectorUtils.encloseOCSInquiryTagsNoParam(getPrepaidBalanceTypesXml(reqProp, requestHashMap));
        log.info("XML formed for the OCS get Balance Request" + xml);
        
		C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
		requestHashMap.put(Constants.REQUEST_XML, xml);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);

		String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), xml);

		//Populating the response.
		GenericInquiryResponse balanceResponse = new GenericInquiryResponse();
		try
		{
			HashMap<?, ?> responseMap = populateResponse(request,responseXml);
			balanceResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return balanceResponse;
	}


	private String getPrepaidBalanceTypesXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String reqXml = "";
		reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_BALNACE_UNITS_MAPPINGS, false, false);
		return reqXml;
	}


	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap<String, String> populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {

		HashMap<String, String> response = new HashMap<String, String>();
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;


	}

}

