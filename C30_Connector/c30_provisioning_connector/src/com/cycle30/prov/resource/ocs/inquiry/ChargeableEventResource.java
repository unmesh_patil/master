package com.cycle30.prov.resource.ocs.inquiry;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;

public class ChargeableEventResource  extends IProvisioningResource{

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(ChargeableEventResource.class);

		log.info("Reached the Get Chargeable Event Resource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String, String> requestHashMap = reqProp.getRequestMap();
		String xml = null;
		if (requestHashMap.get(Constants.ATTR_REQUEST_NAME) !=null) {
			//Call GET Chargeable Events
			xml = C30ProvConnectorUtils.encloseOCSInquiryTagsNoParam(requestHashMap.get(Constants.ATTR_REQUEST_NAME).toString());	
			log.info("xml = "+xml);
			
		}
		else
		{
			throw new C30ProvConnectorException("ERROR Could not find OCS Event Charge Request Name");
		}

		requestHashMap.put(Constants.REQUEST_XML, xml);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
       
		//Make a call to OCS
		C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
		String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), xml);

		//Populating the response.
		GenericInquiryResponse EventLabelResponse = new GenericInquiryResponse();
		try
		{
			log.info("responseXml = "+responseXml);
			HashMap<String, String> responseMap = C30ProvConnectorUtils.populateOCSResponse(responseXml);
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
			EventLabelResponse.setResponseMap(responseMap);
			log.info("responseMap = "+responseMap.toString());				
			String EventLabel = C30ProvConnectorUtils.getOCSMemberValuefromResponse(responseXml.toString(), "Label");	
			log.info("EventLabel = "+EventLabel);

		}
		catch(C30ProvConnectorException excp)
		{
			throw excp;
		}			
		catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new C30ProvConnectorException("ERROR- GetChargeableEvent: "+e.getMessage());			
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new C30ProvConnectorException("ERROR- GetChargeableEvent: "+e.getMessage());			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new C30ProvConnectorException("ERROR- GetChargeableEvent: "+e.getMessage());			
		}
		catch(Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new C30ProvConnectorException("ERROR- GetChargeableEvent: "+e.getMessage());			
		}				

		return EventLabelResponse;
	}

}

