package com.cycle30.prov.resource.ocs.inquiry;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;

public class PrepaidVoucherResource  extends IProvisioningResource{

	boolean simulateRequest = false;
	String voucherPIN = null;
	String voucherAction=null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(PrepaidVoucherResource.class);

		log.info("Reached the GetPrepaid Voucher Status Resource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String, String> requestHashMap = reqProp.getRequestMap();
       
		if ( requestHashMap.get(Constants.VOUCHER_PIN) != null && requestHashMap.get(Constants.VOUCHER_ACTION) !=null )
		{
			voucherPIN = (String) requestHashMap.get(Constants.VOUCHER_PIN);
			voucherAction= (String)requestHashMap.get(Constants.VOUCHER_ACTION);
			log.info("VoucherPIN  " + voucherPIN );
			log.info("VoucherAction  " + voucherAction );
		}
		else
		{
			throw new C30ProvConnectorException(" Error Message from OCS : " + "Vocuher PIN or Action is Null ", "OCS-VOUCHER-001");
		}

		String xml=null;
		if(requestHashMap.get(Constants.VOUCHER_GUID)==null ){
		 xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(getPrepaidVoucherStatusXml(reqProp, requestHashMap));
		 log.info("XML formed for the OCS get Voucher Status Request" + xml);
		}else if (Constants.VOUCHER_BLOCK_ACTION.equalsIgnoreCase(voucherAction)){
			 xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(getPrepaidBlockVoucherXml(reqProp, requestHashMap));
			 log.info("XML formed for the OCS get Voucher Block Request" + xml);
		}else if(Constants.VOUCHER_UNBLOCK_ACTION.equalsIgnoreCase(voucherAction)){
			 xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(getPrepaidUnBlockVoucherXml(reqProp, requestHashMap));
			 log.info("XML formed for the OCS get Voucher UnBlock Request" + xml);
		}

		requestHashMap.put(Constants.REQUEST_XML, xml);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
       
		C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
		String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), xml);

		//Populating the response.
		GenericInquiryResponse balanceResponse = new GenericInquiryResponse();
		try
		{
			HashMap<String, String> responseMap = populateResponse(request,responseXml);
			balanceResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return balanceResponse;
	}

	
	private String getPrepaidVoucherStatusXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String reqXml = "";

		//reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_BALANCES, false, false);

		if (requestHashMap.get(Constants.VOUCHER_PIN) != null ) {
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_VOUCHER_STATUS, false, false);
		    reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.VOUCHER_PIN), false, false);
		}
		return reqXml;
	}

	private String getPrepaidBlockVoucherXml(ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String reqXml = "";

		//reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_BALANCES, false, false);

		if (requestHashMap.get(Constants.VOUCHER_PIN) != null && requestHashMap.get(Constants.VOUCHER_GUID)!=null){
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_VOUCHER_BLOCK, false, false);
		    reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.VOUCHER_GUID), false, false);
		}
		    return reqXml;
     	
		
	  }		

	private String getPrepaidUnBlockVoucherXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String reqXml = "";

		//reqXml = reqXml + C30ProvConnectorUtils.createXmlTag(Constants.SPS_PROV_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_BALANCES, false, false);

		if (requestHashMap.get(Constants.VOUCHER_PIN) != null && requestHashMap.get(Constants.VOUCHER_GUID)!=null){
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_VOUCHER_UNBLOCK, false, false);
		    reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.VOUCHER_GUID), false, false);
		}
    
		return reqXml;
  }

	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap<String, String> populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {


		if (responseXml == null || responseXml.length() == 0) {
			throw new C30ProvConnectorException("OCS-VOUCHER-001");
		}
		String errorNo = null;
		String errorMsg = null;
		if (responseXml != null) {
			try {
				HashMap<?, ?> errordetails = C30ProvConnectorUtils.parseOCSFaultResponse(responseXml, "faultCode","faultString");
				if (errordetails != null && errordetails.get(0) != null) {
					HashMap<?, ?> stackDetails = (HashMap<?, ?>) errordetails.get(0);

					if (stackDetails.get("faultCode") != null && stackDetails.get("faultString") != null) {
						errorNo = "OCS-"+ stackDetails.get("faultCode").toString();
						errorMsg = stackDetails.get("faultString").toString();
					}

				}
			} catch (Exception e) {
				throw new C30ProvConnectorException(e, "PROV-017");
			}
		}

		// non-zero return is an error
		if (errorNo != null) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			throw new C30ProvConnectorException(" Error Message from OCS : " + errorMsg, errorNo);
		}

		
		HashMap<String, String> response = new HashMap<String, String>();
		response.put(Constants.RESPONSE_XML, responseXml);
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		return response;


	}

}

