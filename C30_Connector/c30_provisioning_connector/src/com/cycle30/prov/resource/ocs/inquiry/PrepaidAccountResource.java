package com.cycle30.prov.resource.ocs.inquiry;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;

public class PrepaidAccountResource  extends IProvisioningResource{

	String prepaidAccount = null;
	String prepaidAccountAction=null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(PrepaidAccountResource.class);

		log.info("Reached the GetPrepaid Account Status Resource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String, String> requestHashMap = reqProp.getRequestMap();
		
		if ( requestHashMap.get(Constants.SUBSCRIBER_ID) != null && requestHashMap.get(Constants.ATTR_REQUEST_NAME) !=null )
		{
			prepaidAccount = (String) requestHashMap.get(Constants.SUBSCRIBER_ID);
			prepaidAccountAction= (String)requestHashMap.get(Constants.ATTR_REQUEST_NAME);
			log.info("prepaidAccount  " + prepaidAccount );
			log.info("prepaidAccountAction  " + prepaidAccountAction );
			
		}
		else
		{
			throw new C30ProvConnectorException("OCS-ACCOUNT-001");
		}

		String xml=null;
		
		if (Constants.REQUEST_OCS_ACCOUNT_HISTORY.equalsIgnoreCase(prepaidAccountAction)){
			 xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(getPrepaidAccountHistoryXml(prepaidAccount));
			 log.info("XML formed for the OCS Account History Request" + xml);
		}else if(Constants.REQUEST_OCS_ACCOUNT_SEARCH.equalsIgnoreCase(prepaidAccountAction)){
			 xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(getPrepaidAccountSearchXml(prepaidAccount));
			 log.info("XML formed for the OCS Account Search Request" + xml);
		}
		
		requestHashMap.put(Constants.REQUEST_XML, xml);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);

       
		//Make a call to OCS
		C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
		String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), xml);

		//Populating the response.
		GenericInquiryResponse prepaidAccountResponse = new GenericInquiryResponse();
		try
		{
			HashMap<String, String> responseMap = populateResponse(request,responseXml);
			prepaidAccountResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return prepaidAccountResponse;
	}

	/** This is the request formation for the Account Search Resource
	 * 
	 * @param prepaidAccount
	 * @param prepaidAccountAction
	 * @return
	 * @throws C30ProvConnectorException
	 */
	
	private String getPrepaidAccountSearchXml(String prepaidAccount) {
			String reqXml = "";
	
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_GET_BYID, false, false);
		    reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, prepaidAccount, false, false);

		    return reqXml;
		
	  }		

	/** This is the request formation for the Account History Resource
	 * 
	 * @param prepaidAccount
	 * @param prepaidAccountAction
	 * @return
	 * @throws C30ProvConnectorException
	 */
	
	private String getPrepaidAccountHistoryXml(String prepaidAccount) {
		String reqXml = "";

		reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_HISTORY, false, false);
	    reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, prepaidAccount, false, false);
	    reqXml= reqXml + "</value></param><param>\n<value>" + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, "", false, true);
	    reqXml= reqXml + "</value></param>\n<param><value>" + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, "", false, true);

	    return reqXml;
	
  }		


	/** This is the response formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap<String, String> populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {

		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("OCS-ACCOUNT-002");
		}
		String  errorNo=null;
		String errorMsg=null;
		if(responseXml !=null){
			try {
			        HashMap<?, ?> errordetails= C30ProvConnectorUtils.parseOCSFaultResponse(responseXml, "faultCode", "faultString");
			        if(errordetails !=null &&  errordetails.get(0)!=null){
			          HashMap<?, ?> stackDetails=	(HashMap<?, ?>) errordetails.get(0);
			            if(stackDetails.get("faultCode") !=null && stackDetails.get("faultString") !=null){
			            	 errorNo= "OCS-"+stackDetails.get("faultCode").toString();
			            	 errorMsg= stackDetails.get("faultString").toString() ;
			            }
			        }
			} catch (Exception e) {
				throw new C30ProvConnectorException(e, "PROV-017"); 
			} 
		}

		// non-zero return is an error
		if (errorNo != null ) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			throw new C30ProvConnectorException(" Error Message from OCS : " + errorMsg, errorNo);
		} 

		HashMap<String, String> response = new HashMap<String, String>();
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;

	}

}

