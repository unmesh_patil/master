package com.cycle30.prov.resource.ocs;

import java.util.HashMap;
import java.text.ParseException;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.utils.C30OCSRequestUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.datasource.C30JDBCDataSource;

public class SubscriberResource  extends IProvisioningResource{


	String headerXML = null;
	String requestBodyXML = null;
	String footerXML = null;
	StringBuffer headerStr = new StringBuffer(Constants.OCS_XML_HEADER);
    StringBuffer footerStr = new StringBuffer();
	boolean simulateRequest = false;
	
	Logger log = Logger.getLogger(SubscriberResource.class);
	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {
		
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		
		simulateRequest = (Boolean) requestHashMap.get(Constants.ATTR_SIMULATE_REQUEST);
		String requestXml = "";
		String footerTags = "";
		String methodTags = "";
		log.info("requestHashMap : "+requestHashMap.toString());
		//Request Header Tag
		requestXml = Constants.OCS_RESPONSE_XML_HEADER;
		String methodName = requestHashMap.get(Constants.ATTR_OCS_METHOD_NAME).toString();
		
		methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, methodName,false,false);
		
        //Subscriber.Create XML		
		if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_CREATE)) {
			//Get Subscriber.Create XML
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberCreateXml(requestHashMap), 
					methodTags, footerTags);
			
		}  else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_UPDATE)) {
			String requestType = requestHashMap.get(Constants.ATTR_REQUEST_NAME).toString();
	        //Subscriber.Update XML			
			if (requestType.equalsIgnoreCase(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER)) {
				//PW-169
				if(!requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE)){
				if(requestHashMap.get(Constants.ICCID).toString().equalsIgnoreCase(Constants.CDMASIM)){
					log.info("Not calling Get IMSI For CDMA Accounts");
				}
				else{
				//Get the IMSI number
				String IMSINumber = GetIMSINumber(requestHashMap);
				requestHashMap.put(Constants.IMSI, IMSINumber);
				}
				}
				else{
					log.info("Expiration Date exists in Hashmap = "+requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE));
				}

			}
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberUpdateXml(requestHashMap), methodTags, footerTags);			
			
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_ACTIVATE_BY_CLI)) {
	        //Subscriber.ActivateByCLI XML        
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberActivateByCLIXml(requestHashMap), 
					methodTags, footerTags);
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_CREDITSET)) {
	        //Subscriber.CreditSet XML		
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(getSubscriberCreditSetXml(requestHashMap), methodTags, footerTags);
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_CREDIT)) {
	        //Subscriber.Create XML        
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberCreditXml(requestHashMap), 
					methodTags, footerTags);			
		} else if (methodName.equalsIgnoreCase(Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT)) {
			HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
			String iptrunkValue=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,
					Constants.PREPAID_PARAM_IPTRUNK);
			requestHashMap.put(Constants.PREPAID_IPTRUNK, iptrunkValue);
			//EventCharging.DirectDebit XML        
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getEventChargingDirectDebitXml(requestHashMap), 
					methodTags, footerTags);
		} else if (methodName.equalsIgnoreCase(Constants.OCS_GET_PROFILE_BALANCE_TYPES)) {
	        //SubscriberProfile.GetByLabel XML        
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberProfileGetByLabel(requestHashMap), 
					methodTags, footerTags);
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_LINE_UPDATE)) {
			if (requestHashMap.containsKey(Constants.ICCID)) {
				//Get the IMSI number
				String IMSINumber = GetIMSINumber(requestHashMap);
				requestHashMap.put(Constants.IMSI, IMSINumber);
			}
	        //SubscriberLine.Update XML        
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberLineUpdateXml(requestHashMap), 
					methodTags, footerTags);			
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_UPDATE_RECHARGE_VOUCHER)) {
	        //Subscriber.RechargeByCLIPIN XML        
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberRechargeByCLIPINXml(requestHashMap),
					methodTags, footerTags);			
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_GET_BYCLI)) {
	        //Subscriber.RechargeByCLIPIN XML        
			requestXml = C30OCSRequestUtils.getSubscriberByCLIXml(requestHashMap);			
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_LINE_GET_BYCLI)) {
	        //Subscriber.RechargeByCLIPIN XML        
			requestXml = C30OCSRequestUtils.getSubscriberLineByCLIXml(requestHashMap);			
		} else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_LINE_DELETE)) {
	        //Subscriber.RechargeByCLIPIN XML        
			requestXml = C30OCSRequestUtils.getSubscriberLineDeleteXml(requestHashMap);			
		}
		//Added by vijaya PW-43
		/*else if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_DELETE)) {
		    //Subscriber.Delete XML        
		    requestXml = C30OCSRequestUtils.getSubscriberDeleteXml(requestHashMap);			
		}*/

		log.info("Make a POST call to OCS ");
		//Make a POST call to OCS
		log.info("initialize REST Connection");

		requestHashMap.put(Constants.REQUEST_XML, requestXml);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
        C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
		log.info("requestXml: "+requestXml);
		String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), requestXml);
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		//Populating the response.
		GenericInquiryResponse subscriberResponse = new GenericInquiryResponse();
		
		try
		{
			HashMap<String, String> responseMap = C30ProvConnectorUtils.populateOCSResponse(responseXml);
			subscriberResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException excp)
		{
			log.info("Exception = " +excp);
			throw excp;
		}

		return subscriberResponse;
		
	}

	
	/** Get the Subscriber Credit Set XML
	 * 
	 * @param requestHashMap
	 * @return requestPayload
	 * 
	 */
	private String getSubscriberCreditSetXml(HashMap requestHashMap) throws C30ProvConnectorException {
		String requestPayload = "";
		String planName = null;
		String balName = null;		
		String commonBalPayload=null;
		String balanceTypesPayload ="";
		String ExpirationDate=null;
		try {
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL)) {
				planName = requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString();
			} else if (requestHashMap.containsKey(Constants.PREPAID_CHARGE_EVENT_BALANCE_TYPE_LABEL)) {
				balName = requestHashMap.get(Constants.PREPAID_CHARGE_EVENT_BALANCE_TYPE_LABEL).toString();
			}

			// Build SubscriberId.
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {		
				requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
						Constants.OCS_XML_TAG_INT, false);
			}
			
			if (planName != null && balName == null) {
			    //Get Balance Types associated to subscriber profile 
			    balanceTypesPayload = GetSubscriberProfileBalanceTypes(planName, requestHashMap);
			    balanceTypesPayload = C30ProvConnectorUtils.wrapOCSDataMultiplyTags(balanceTypesPayload);
			    requestPayload = requestPayload+balanceTypesPayload;
				
			} else if (balName != null) {
				if (requestHashMap.containsKey(Constants.PREPAID_CHARGE_EVENT_BALANCE_TYPE_LABEL)) {
					balanceTypesPayload = C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_BALANCE_TYPE_LABEL, 
							balName , Constants.OCS_XML_TAG_STRING);
				}
				if (requestHashMap.containsKey(Constants.PREPAID_EXPIRATIONDATE)) {
					ExpirationDate = C30ProvConnectorUtils.getOCSDateFormatFromString(requestHashMap.get(Constants.PREPAID_EXPIRATIONDATE).toString(), "0", true);
					balanceTypesPayload = balanceTypesPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_EXPIRATIONDATE, 
							ExpirationDate , Constants.OCS_XML_TAG_STRING);
				}
				if (requestHashMap.containsKey(Constants.PREPAID_VALUE)) {
					balanceTypesPayload = balanceTypesPayload + C30ProvConnectorUtils.encloseOCSRequestMemberTags(Constants.PREPAID_VALUE, 
							requestHashMap.get(Constants.PREPAID_VALUE).toString() , Constants.OCS_XML_TAG_DOUBLE);
				}
				balanceTypesPayload = C30ProvConnectorUtils.encloseOCSDataMultiplyTags(balanceTypesPayload);
				balanceTypesPayload = C30ProvConnectorUtils.wrapOCSDataMultiplyTags(balanceTypesPayload);
			    requestPayload = requestPayload+balanceTypesPayload;			
			}

			//Build Reason.
			if (requestHashMap.containsKey(Constants.PREPAID_REASON)) {	    
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_REASON).toString(), 
						Constants.OCS_XML_TAG_STRING, false);
			} else {
				requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(null,Constants.OCS_XML_TAG_STRING, false);
			}
			//Build Final XML
			requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
			return requestPayload;			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new C30ProvConnectorException("ERROR-getSubscriberCreditSetXml: "+e.getMessage());				
		} 

	}	


	/**This method is responsible for getting the balance type associated to profiles
	 * 
	 * @param requestHashMap
	 * @param ProfileLabel
	 * 
	 * @return result
	 *  
	 */
	private String GetSubscriberProfileBalanceTypes(String ProfileLabel, HashMap requestHashMap) {
	
		String acctSegId = requestHashMap.get(Constants.ACCT_SEG_ID).toString();
		String result=null;
		// TODO Auto-generated method stub
		try{

			HashMap<String, String> balrequestHashMap = new HashMap<String, String>();			
			balrequestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);
			balrequestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_GET_PROFILE_BALANCE_TYPES);
			balrequestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,requestHashMap.get(Constants.EXTERNAL_TRANSACTION_ID).toString());				
			balrequestHashMap.put(Constants.C30_TRANSACTION_ID,requestHashMap.get(Constants.C30_TRANSACTION_ID).toString());
			balrequestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		
			balrequestHashMap.put(Constants.PREPAID_LABEL, ProfileLabel);
			
			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap responseXML = prepaidRequest.awnprepaidOrderRequest(balrequestHashMap);
			log.info("GetSubscriberProfileBalanceTypes - " + responseXML.get("ResponseXML"));
			result = C30ProvConnectorUtils.getSubsciberProfileBalanceTypes(responseXML.get("ResponseXML").toString(), requestHashMap);
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " + acctSegId + " SubscriberProfile.GetByLabel: "+e);
			e.printStackTrace();
		}		
		
		return result;
	}	
	

	/**This method is responsible for getting the balance type associated to profiles
	 * 
	 * @param requestHashMap
	 * @throws C30SDKToolkitException
	 * @return IMSIIdentifierId
	 *  
	 */
	private String GetIMSINumber(HashMap requestHashMap) {
	
		// TODO Auto-generated method stub
		String IMSIIdentifierId=null;
		HashMap<String, String> ProvEnquiry = new HashMap<String, String>();
		
		try{
			log.info("Send IMSI Inquiry for ICCID : "+requestHashMap.get(Constants.ICCID));			
			ProvEnquiry.put("AcctSegId",requestHashMap.get(Constants.ACCT_SEG_ID).toString());
			ProvEnquiry.put("ICCID",requestHashMap.get(Constants.ICCID).toString());
			ProvEnquiry.put("RequestName", Constants.REQUEST_IMSI_VERIFICATION);		
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap response = request.awnInquiryRequest(ProvEnquiry) ;
			IMSIIdentifierId = response.get("IMSI").toString();
			log.info("IMSI Retreived - IdentifierIdIn : "+IMSIIdentifierId);
			log.info("Response XML : "+response.get("ResponseXML"));	
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " + requestHashMap.get(Constants.ACCT_SEG_ID).toString() + " GetIMSINumber : "+e);
			e.printStackTrace();
		}		
		
		return IMSIIdentifierId;
	}	
	
}