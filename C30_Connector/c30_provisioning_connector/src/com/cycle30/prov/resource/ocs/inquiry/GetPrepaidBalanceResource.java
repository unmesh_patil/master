package com.cycle30.prov.resource.ocs.inquiry;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30OCSRequestUtils;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;

public class GetPrepaidBalanceResource  extends IProvisioningResource{
	Logger log = Logger.getLogger(GetPrepaidBalanceResource.class);

	boolean simulateRequest = false;
	String subscriberId = null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		

		log.info("Reached the GetPrepaidBalanceResource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String, String> requestHashMap = reqProp.getRequestMap();
		C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
		
	//	simulateRequest = (Boolean) requestHashMap.get(Constants.ATTR_SIMULATE_REQUEST);

		if ( requestHashMap.get(Constants.SUBSCRIBER_ID) != null )
		{
			subscriberId = (String) requestHashMap.get(Constants.SUBSCRIBER_ID);
			log.info("Subscriber Id " + subscriberId);
		}
		else
		{
			throw new C30ProvConnectorException(" Error Message from OCS : " + "Subscriber is Null ", "OCS-EXCEPTION-002");
		}
		String xml=null;
		if( requestHashMap.get(Constants.PREPAID_BALANCE_ACTION)==null ){
		 xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(getPrepaidBalanceXml(reqProp, requestHashMap));
        log.info("XML formed for the OCS get Balance Request" + xml);
		}else if (requestHashMap.get(Constants.PREPAID_BALANCE_ACTION)!=null && 
				requestHashMap.get(Constants.PREPAID_BALANCE_ACTION).toString().equalsIgnoreCase(Constants.PREPAID_WALLET_REFILL)){
		 xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(updatePrepaidWalletRefillXml(reqProp, requestHashMap));
	        log.info("XML formed for the OCS Update Wallet Refill Request" + xml);	
		}else if (requestHashMap.get(Constants.PREPAID_BALANCE_ACTION)!=null && 
				requestHashMap.get(Constants.PREPAID_BALANCE_ACTION).toString().equalsIgnoreCase(Constants.PREPAID_VOUCHER_REDEEM)){
			HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
			String acessPhoneCode=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,
					Constants.PREPAID_PARAM_ACCESS_PHONE);
			log.info("AcessPhone =" + acessPhoneCode);
			requestHashMap.put(Constants.PREPAID_BALANCE_VOUCHER_PHONE_ACCESS, acessPhoneCode);
			
			xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(voucherReedeemXml(reqProp, requestHashMap));
	        log.info("XML formed for the OCS Voucher Reedeem  Request" + xml);	
			}else if (requestHashMap.get(Constants.PREPAID_BALANCE_ACTION)!=null && 
					requestHashMap.get(Constants.PREPAID_BALANCE_ACTION).toString().equalsIgnoreCase(Constants.PREPAID_BALANCE_REFILL)){
				xml = C30ProvConnectorUtils.encloseOCSWirelessInquiryTags(updatePrepaidBalanceRefilleXml(reqProp, requestHashMap));
		        log.info("XML formed for the OCS Balance Refill Request" + xml);	
			}else if (requestHashMap.get(Constants.PREPAID_BALANCE_ACTION)!=null && 
					requestHashMap.get(Constants.PREPAID_BALANCE_ACTION).toString().equalsIgnoreCase(Constants.PREPAID_EXTEND_EXPIRYDATE)){
				xml = C30ProvConnectorUtils.encloseOCSWirelessUpdateExpiryDateTags(updatePrepaidBalanceExpiryDateXml(reqProp, requestHashMap));
		        log.info("XML formed for the OCS Balance Expiry Date Request" + xml);	
			}else if (requestHashMap.get(Constants.PREPAID_BALANCE_ACTION)!=null && 
				requestHashMap.get(Constants.PREPAID_BALANCE_ACTION).toString().equalsIgnoreCase(Constants.PREPAID_PLAN_RECHARGE_RESERVE)){
				log.info("Plan Recharge Reserve requestHashMap = " +requestHashMap);
				log.info("Make a call to update the Wallet Balance with Amount specified in the Request XML");
				HashMap<String, String> paramforwalletRefill= new HashMap<String,String>();

				paramforwalletRefill.put(Constants.SUBSCRIBER_ID,(String)requestHashMap.get(Constants.SUBSCRIBER_ID));
				paramforwalletRefill.put(Constants.PREPAID_VALUE,(String)requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_Value));
				// kmr Force doesn't send this date all the time 87
				if (requestHashMap.containsKey(Constants.PREPAID_BALANCE_EXPIRATION_DATE) &&
						requestHashMap.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE) != null) 
					paramforwalletRefill.put(Constants.PREPAID_ATTRIB_BALANCE_EXPIRY_DATE,(String)requestHashMap.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE));
				// end of modify
				paramforwalletRefill.put(Constants.PREPAID_REASON,(String)requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_REASON));
				paramforwalletRefill.put(Constants.PREPAID_BALANCE_ACTION,(String)requestHashMap.get(Constants.PREPAID_BALANCE_ACTION));				
				log.info("Plan Recharge Reserve paramforwalletRefill = " +paramforwalletRefill);
				//Get Subscribe.credit COMMON Balance XML
				String walletUpdateXml = C30OCSRequestUtils.getSubscriberCreditXmlforCOMMONBalance(paramforwalletRefill);				
				log.info("XML formed for the wallet Refill Update for the Plan Recharge Reserve : " + walletUpdateXml);
				String walletUpdateResponseXml= restConnection.makeOCSPOSTCall(authData.getWeburl(), walletUpdateXml);
				log.info("Actual Response XML for the wallet refill for the Plan recharge reservre Update for the Plan Recharge Reserve : " + walletUpdateResponseXml);
				HashMap<?, ?> responseMap = populateResponse(request, walletUpdateResponseXml);
			
				if(responseMap.get(Constants.RESPONSE_XML)!=null){	
					log.info("Validate XML after parsing any error in wallet refill Plan recharge reserve" + responseMap.get(Constants.RESPONSE_XML).toString());
				xml = updatePrepaidBalancePlanRechargeReserveXml(reqProp, requestHashMap);
				log.info("XML formed for the Direct Debit evnent charging for the Plan Recharge Reserve" + xml);
				}
	        	
		}
		requestHashMap.put(Constants.REQUEST_XML, xml);
		new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
		String responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), xml);

		//Populating the response.
		GenericInquiryResponse balanceResponse = new GenericInquiryResponse();
		try
		{
			HashMap<?, ?> responseMap = populateResponse(request,responseXml);
			balanceResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return balanceResponse;
	}

/**This method will get the Balance for the given Subscriber ID
 * 
 * @param reqProp
 * @param requestHashMap
 * @return
 */
	private String getPrepaidBalanceXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String reqXml = "";

		if (requestHashMap.get(Constants.SUBSCRIBER_ID) != null ) 
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_BALANCES, false, false);
		reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, (String) requestHashMap.get(Constants.SUBSCRIBER_ID), false, false);

		return reqXml;
	}

	
	private String updatePrepaidWalletRefillXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String	balanceUpdatemethod=null;
		double  creditingValue=0;
		
		if( (String) requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_Value)!=null ){
			  creditingValue= Double.parseDouble(requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_Value).toString());
			
			if(creditingValue >= 0){
				balanceUpdatemethod= Constants.OCS_SUBSCRIBER_UPDATE_WALLET_BALANCE_CREDIT;
			}else if(creditingValue < 0){
				balanceUpdatemethod= Constants.OCS_SUBSCRIBER_UPDATE_WALLET_BALANCE_DEBIT;
				creditingValue=(creditingValue*-1);
			}
		}
		    
		String reqXml = "";

		   reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD,balanceUpdatemethod , false, false);
		   reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, (String) requestHashMap.get(Constants.SUBSCRIBER_ID), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_LABEL_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_TYPE), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_VALUE_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_DOUBLE,new Double(creditingValue).toString(), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_EXPIRATION_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE), false, false);
		   reqXml= reqXml + Constants.OCS_STRUCT_END_FOOTER + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_REASON), false, false);
		 
	
		return reqXml;
		
	}

	/**This method will Redeem the Voucher for the Voucher Redeem action
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String voucherReedeemXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
	    
		String reqXml = "";

		   reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_UPDATE_RECHARGE_VOUCHER, false, false);
		   reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_SUBSCRIBER_CLI), false, false);
		   reqXml= reqXml + Constants.OCS_XML_GENERIC_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.VOUCHER_PIN), false, false);
		   reqXml= reqXml + Constants.OCS_XML_GENERIC_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_VOUCHER_PHONE_ACCESS), false, false);
		   reqXml= reqXml + Constants.OCS_XML_GENERIC_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, "", false, false);
		
		 return reqXml;
	}
	
	/** This method will Update the Balance other than Wallet
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String  updatePrepaidBalanceRefilleXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
		String	balanceRefillUpdatemethod=null;
		double  creditingValue=0;
		
		if( (String) requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_Value)!=null ){
			  creditingValue= Double.parseDouble(requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_Value).toString());
			
			if(creditingValue >= 0){
				balanceRefillUpdatemethod= Constants.OCS_SUBSCRIBER_UPDATE_BALANCE_REFILL_CREDIT;
			}else if(creditingValue < 0){
				balanceRefillUpdatemethod= Constants.OCS_SUBSCRIBER_UPDATE_BALANCE_REFILL_DEBIT;
				creditingValue=(creditingValue*-1);
			}
		}
	    
		String reqXml = "";

		   reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, balanceRefillUpdatemethod, false, false);
		   reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, (String) requestHashMap.get(Constants.SUBSCRIBER_ID), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_LABEL_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_TYPE), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_VALUE_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_DOUBLE,new Double(creditingValue).toString(), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_EXPIRATION_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE), false, false);
		   reqXml= reqXml + Constants.OCS_STRUCT_END_FOOTER + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_REASON), false, false);


		 return reqXml;
	}
	
	/**This Method is to trigger Charge Event
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return requestXml
	 * 
	 * @throws C30ProvConnectorException 
	 */
	private String  updatePrepaidBalancePlanRechargeReserveXml(
			ProvisioningRequestProperties reqProp, HashMap<String, String> requestHashMap) throws C30ProvConnectorException {
	    
		String requestXml = "";
		requestXml = Constants.OCS_RESPONSE_XML_HEADER;

		String methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD,Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT, false, false);

		log.info("getEventChargingDirectDebitXml START ");
		HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
		String iptrunkValue=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,Constants.PREPAID_PARAM_IPTRUNK);
		log.info("IPTrunk =" + iptrunkValue);
		requestHashMap.put(Constants.PREPAID_IPTRUNK, iptrunkValue);
		// EventCharging.DirectDebit XML
		requestXml = requestXml	+ C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getEventChargingDirectDebitXml(requestHashMap),methodTags, null);
		log.info("updatePrepaidBalancePlanRechargeReserveXml : " + requestXml);
		return requestXml;
	}
	
	

	/**This Method update the Expiration Date of any given Balance Type
	 * 
	 * @param reqProp
	 * @param requestHashMap
	 * @return
	 */
	private String  updatePrepaidBalanceExpiryDateXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) {
	    
		String reqXml = "";

		   reqXml= C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_TAG_METHOD, Constants.OCS_SUBSCRIBER_UPDATE_EXTENDEXPIRYDATE_REFILL, false, false);
		   reqXml= reqXml + Constants.OCS_XML_HEADER_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_INT_VALUE_TAG, (String) requestHashMap.get(Constants.SUBSCRIBER_ID), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_LABEL_EXTN_NO_ARRAY + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_TYPE), false, false);
		   reqXml= reqXml + Constants.OCS_XML_STRUCT_NO_VALUE_EXTN_EXPIRATION_EXTN + C30ProvConnectorUtils.createXmlTag(Constants.OCS_INQUIRY_XML_STRING_VALUE_TAG, (String) requestHashMap.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE), false, false);
		   reqXml= reqXml + Constants.OCS_STRUCT_END_EXPIRYDATE_EXTN_TAG_NO_ARRAY ;


		 return reqXml;
	}

	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap<String, String> populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {


		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException(" Error Message from OCS : " + "Response is Null ", "OCS-001");
		}

		String  errorNo=null;
		String errorMsg=null;
		if (responseXml != null) {
			try {
				HashMap<?, ?> errordetails = C30ProvConnectorUtils.parseOCSFaultResponse(responseXml, "faultCode","faultString");
				if (errordetails != null && errordetails.get(0) != null) {
					HashMap<?, ?> stackDetails = (HashMap<?, ?>) errordetails.get(0);

					if (stackDetails.get("faultCode") != null && stackDetails.get("faultString") != null) {
						errorNo = "OCS-"+ stackDetails.get("faultCode").toString();
						errorMsg = stackDetails.get("faultString").toString();
					}

				}
			} catch (Exception e) {
				throw new C30ProvConnectorException(e, "PROV-017");
			}
		}

		// non-zero return is an error
		if (errorNo != null ) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			throw new C30ProvConnectorException(" Error Message from OCS : " + errorMsg, errorNo);
		}
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);

		HashMap<String, String> response = new HashMap<String, String>();
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;

	}

}

