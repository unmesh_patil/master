package com.cycle30.prov.resource.ocs;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestHandler;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.utils.C30OCSRequestUtils;
import com.cycle30.prov.webservice.C30OCSRESTConnection;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;


import javax.xml.parsers.ParserConfigurationException;

public class SubscriberProfileResource  extends IProvisioningResource{


	String headerXML = null;
	String requestBodyXML = null;
	String footerXML = null;
	StringBuffer headerStr = new StringBuffer(Constants.OCS_XML_HEADER);
    StringBuffer footerStr = new StringBuffer();
	boolean simulateRequest = false;
	
	Logger log = Logger.getLogger(SubscriberProfileResource.class);
	
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {
		
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		simulateRequest = (Boolean) requestHashMap.get(Constants.ATTR_SIMULATE_REQUEST);
		String requestXml = "";
		String responseXml = null;
		String footerTags = "";
		String methodTags = "";
		log.info("requestHashMap : "+requestHashMap.toString());
		//Request Header Tag
		requestXml = Constants.OCS_RESPONSE_XML_HEADER;
		String methodName = requestHashMap.get(Constants.ATTR_OCS_METHOD_NAME).toString();
		String prepaidAction = requestHashMap.get(Constants.ATTR_PREAPID_ACTION).toString();
		log.info("prepaidAction = "+prepaidAction);
		
		if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_UPDATEBALANCE)) {
			methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_REMOVE_BALANCE,false,false);
		} else {
			methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, methodName,false,false);			
		}

		//Extend Expiry Date
		if (prepaidAction.equalsIgnoreCase(Constants.PREPAID_EXTEND_EXPIRYDATE)) {
			
			requestXml = getPlanExtendExpiryDate(requestXml, methodName, requestHashMap, methodTags, footerTags,authData);
			log.info("prepaidAction = "+ prepaidAction +" requestXml = "+requestXml);			
		}
		
		//Recharge Plan
		if (prepaidAction.equalsIgnoreCase(Constants.PREPAID_PLAN_RECHARGE_RESERVE)) {
			
			requestXml = getPlanRecharge(requestXml, methodName, requestHashMap, methodTags, footerTags);
			log.info("prepaidAction = "+ prepaidAction +" requestXml = "+requestXml);			
		}
				
		//Change Plan
		if (prepaidAction.equalsIgnoreCase(Constants.PREPAID_PLAN_CHANGE)) {
			
			requestXml = getPlanChange(requestXml, methodName, requestHashMap, methodTags, footerTags, authData,request);
			log.info("prepaidAction = "+ prepaidAction +" requestXml = "+requestXml);
		}		
		
		if ((requestXml != "") || (!requestXml.isEmpty())) {
			log.info("Make a POST call to OCS ");
			log.info(" SubscriberProfileResource - requestXml :  "+requestXml);

			requestHashMap.put(Constants.REQUEST_XML, requestXml);
			C30ProvConnectorUtils.generateNewRequestId(request,datasource);
			new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
			//Make a POST call to OCS
	        C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
			responseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), requestXml);
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		}

		//Populating the response.
		GenericInquiryResponse subscriberResponse = new GenericInquiryResponse();
		
		try
		{
			HashMap<String, String> responseMap = new HashMap<String, String>();

			if (responseXml != null) {
			log.info("populateOCSResponse ");
			responseMap = C30ProvConnectorUtils.populateOCSResponse(responseXml);
			log.info("responseMap= "+responseMap.toString());
			} else {
				responseMap.put("ResponseCode", "0");
				responseMap.put("ResponseXML", responseXml);
			}
			subscriberResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException excp)
		{
			throw excp;
		}

		return subscriberResponse;
		
	}

	/** Get the getPlanExtendExpiryDate XML
	 * 
	 * @param requestHashMap
	 * @return requestPayload
	 * 
	 */
	private String getPlanExtendExpiryDate(String requestXml, String methodName, 
			HashMap requestHashMap, String methodTags, String footerTags,ProvAuthClientObject authData) throws C30ProvConnectorException{
		
		//Update Expiry Date
		if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_CREDITSET)) {
			
	        //Subscriber.CreditSet XML		
			try {
				requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(getSubscriberCreditSetXmlforExtendExpiryDate(requestHashMap,authData), methodTags, footerTags);
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("Error = "+e.getMessage());
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("Error = "+e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("Error = "+e.getMessage());
			} catch (C30ProvConnectorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("Error = "+e.getMessage());
				throw e;
			}
			//requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(getSubscriberCreditSetXml(requestHashMap), methodTags, footerTags);
			
		}		
		
		return requestXml;
	}

	/** Get the getPrepaidExtendExpiryDate XML
	 * 
	 * @param requestHashMap
	 * @return requestPayload
	 * 
	 */
	private String getPlanRecharge(String requestXml, String methodName, 
			HashMap requestHashMap, String methodTags, String footerTags) throws C30ProvConnectorException{
		
		try {
			if (methodName.equalsIgnoreCase(Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT)) {
				HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
				String iptrunkValue=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,
						Constants.PREPAID_PARAM_IPTRUNK);
				
				requestHashMap.put(Constants.PREPAID_IPTRUNK, iptrunkValue);
		        //EventCharging.DirectDebit XML        
				requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getEventChargingDirectDebitXml(requestHashMap), methodTags, footerTags);
			}		
			
			//Update Balances for the NEW Subscriber Profile
			if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_CREDITSET)) {
		        //Subscriber.CreditSet XML		
				requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(getSubscriberCreditSetXml(requestHashMap), methodTags, footerTags);
			}	
		} catch(C30ProvConnectorException excp){
			log.info("Exception = " +excp);
			throw excp;
		}
		return requestXml;
	}
	
	
	/** Process PLAN Change
	 * 
	 * @param requestHashMap
	 * @param request 
	 * @return requestPayload
	 * 
	 */
	private String getPlanChange(String requestXml, String methodName, 
			HashMap requestHashMap, String methodTags, String footerTags, ProvAuthClientObject authData, IProvisioningRequest request) throws C30ProvConnectorException {
		
		log.info("getPlanChange");
		//Update NEW Subscriber Profile
		if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_UPDATE)) {
	        //Subscriber.Update XML			
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberUpdateXml(requestHashMap), methodTags, footerTags);			
		} 

		//Update Balances for the NEW Subscriber Profile
		if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_CREDITSET)) {
	        //Subscriber.CreditSet XML		
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(getSubscriberCreditSetXml(requestHashMap), methodTags, footerTags);
		}		

		//Debit Plan Fee
		if (methodName.equalsIgnoreCase(Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT)) {
			HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
			String iptrunkValue=C30ProvConnectorUtils.getParamValueFromParamList(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_OCS,
					Constants.PREPAID_PARAM_IPTRUNK);
				
			requestHashMap.put(Constants.PREPAID_IPTRUNK, iptrunkValue);
	        //EventCharging.DirectDebit XML        
			requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getEventChargingDirectDebitXml(requestHashMap), methodTags, footerTags);
		}		
		
		//Remove Balances NOT associated to NEW Subscriber Profile		
		try {

			if (methodName.equalsIgnoreCase(Constants.OCS_SUBSCRIBER_UPDATEBALANCE)) {

				HashMap<String, String> subBalrequestHashMap = new HashMap();
				HashMap<String, String> profileBalrequestHashMap = new HashMap();
				List<String> subBalanceType = new ArrayList<String>();
				
				String subscriberBalancexml=null;
				String profileBalancexml=null;
				if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
					subBalrequestHashMap.put(Constants.PREPAID_SUBSCRIBERID, requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString());
				}
				
				//Call GET Subscriber Balances to retrieve the current balances
				log.info("Get Subscriber Balances ");
				subscriberBalancexml = C30OCSRequestUtils.getSubscriberGetBalancesXml(subBalrequestHashMap);
				
				requestHashMap.put(Constants.REQUEST_XML, subscriberBalancexml);
				C30ProvConnectorUtils.generateNewRequestId(request,datasource);
				new ProvRequestLogger().auditFeatureItemRequest(request, datasource);

				//Make a POST call to OCS
		        C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
				String subBalresponseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), subscriberBalancexml);

				C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, subBalresponseXml, datasource);
				C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
				//Populating the response.
				GenericInquiryResponse subBalResponse = new GenericInquiryResponse();
				
				try
				{
					log.info("Get Subscriber Balances responseXml = "+subBalresponseXml);
					HashMap<String, String> subBalResponseMap = C30ProvConnectorUtils.populateOCSResponse(subBalresponseXml);
					log.info("Get Subscriber Balances ResponseMap = "+subBalResponseMap.toString());				
					subBalResponse.setResponseMap(subBalResponseMap);
		        	subBalanceType = C30ProvConnectorUtils.getProfileBalancesofSubscriber(subBalresponseXml.toString());
					log.info("BalanceTypes associated to subscriber = "+subBalanceType.toString());
				}
				catch(Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new C30ProvConnectorException("ERROR- UpdateBalances: "+e.getMessage());				
				}				
				
				//Get Balances for the Profile
				if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL)) {
					log.info("SubscriberProfileLabel = "+requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString());
					profileBalrequestHashMap.put(Constants.PREPAID_LABEL, requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString());
				}
				methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_GET_PROFILE_BALANCE_TYPES,false,false);

				footerTags = null;
		        //SubscriberProfile.GetByLabel XML        
				profileBalancexml = C30ProvConnectorUtils.encloseOCSRequestRootTags(C30OCSRequestUtils.getSubscriberProfileGetByLabel(profileBalrequestHashMap), methodTags, footerTags);
				profileBalancexml = Constants.OCS_RESPONSE_XML_HEADER+profileBalancexml;
				
				
				requestHashMap.put(Constants.REQUEST_XML, profileBalancexml);
				C30ProvConnectorUtils.generateNewRequestId(request,datasource);
				new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
				//Make a POST call to OCS
		        C30OCSRESTConnection restConn = new C30OCSRESTConnection();
				String respXml = restConn.makeOCSPOSTCall(authData.getWeburl(), profileBalancexml);

				//Populating the response.
				GenericInquiryResponse ProfileBalResponse = new GenericInquiryResponse();
				C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, profileBalancexml, datasource);
				C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
				try
				{
					HashMap<String, String> ProfileBalResponseMap = C30ProvConnectorUtils.populateOCSResponse(respXml);
					ProfileBalResponse.setResponseMap(ProfileBalResponseMap);
					String balRemoveRequestXml = C30ProvConnectorUtils.getBalancesofSubscriberProfile(respXml, subBalanceType);

					 if (balRemoveRequestXml == "" || balRemoveRequestXml.isEmpty() || balRemoveRequestXml == null) {
						 requestXml = "";
					} else {
						String requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
								Constants.OCS_XML_TAG_INT, false);
						balRemoveRequestXml = C30ProvConnectorUtils.wrapOCSDataMultiplyTags(balRemoveRequestXml);
						requestPayload = requestPayload +balRemoveRequestXml;
						requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);
						methodTags = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_METHOD_NAME, Constants.OCS_SUBSCRIBER_REMOVE_BALANCE,false,false);
						requestXml = requestXml + C30ProvConnectorUtils.encloseOCSRequestRootTags(requestPayload, methodTags, footerTags);
					}
				}
				catch(Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new C30ProvConnectorException("ERROR- UpdateBalances: "+e.getMessage());				
				}			

			}
			
		} catch(C30ProvConnectorException excp){
			log.info("excp = " +excp);
			throw excp;
		}

		return requestXml;
	}
	
	/** Get the Subscriber Credit Set XML
	 * 
	 * @param requestHashMap
	 * @return requestPayload
	 * 
	 */
	private String getSubscriberCreditSetXml(HashMap requestHashMap) {
		String requestPayload = "";
		String planName = "";
		
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL)) {
			planName = requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString();
		}
		// Build SubscriberId.
		if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {		
			requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
					Constants.OCS_XML_TAG_INT, false);
		}
	    //Get Balance Types associated to subscriber profile 
	    String balanceTypesPayload = GetSubscriberProfileBalanceTypes(planName, requestHashMap);
	    balanceTypesPayload = C30ProvConnectorUtils.wrapOCSDataMultiplyTags(balanceTypesPayload);
	    requestPayload = requestPayload+balanceTypesPayload;

	    //Build Reason.
		if (requestHashMap.containsKey(Constants.PREPAID_REASON)) {	    
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_REASON).toString(), 
					Constants.OCS_XML_TAG_STRING, false);
		} else {
			requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(null,Constants.OCS_XML_TAG_STRING, false);
		}
		//Build Final XML
		requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);		
		return requestPayload;
		
	}	

	
	
	/**This method is responsible for getting the balance type associated to profiles
	 * 
	 * @param requestHashMap
	 * @param ProfileLabel
	 * 
	 * @return result
	 *  
	 */
	private String GetSubscriberProfileBalanceTypes(String ProfileLabel, HashMap requestHashMap) {
	
		String acctSegId = requestHashMap.get(Constants.ACCT_SEG_ID).toString();
		String result=null;
		// TODO Auto-generated method stub
		try{

			HashMap<String, String> balrequestHashMap = new HashMap<String, String>();			
			balrequestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);
			balrequestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_GET_PROFILE_BALANCE_TYPES);
			balrequestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,requestHashMap.get(Constants.EXTERNAL_TRANSACTION_ID).toString());				
			balrequestHashMap.put(Constants.C30_TRANSACTION_ID,requestHashMap.get(Constants.C30_TRANSACTION_ID).toString());
			balrequestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		
			balrequestHashMap.put(Constants.PREPAID_LABEL, ProfileLabel);
			
			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap responseXML = prepaidRequest.awnprepaidOrderRequest(balrequestHashMap);
			log.info("GetSubscriberProfileBalanceTypes - " + responseXML.get("ResponseXML"));
			result = C30ProvConnectorUtils.getSubsciberProfileBalanceTypes(responseXML.get("ResponseXML").toString(), requestHashMap);
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " + acctSegId + " SubscriberProfile.GetByLabel: "+e);
			e.printStackTrace();
		}		
		
		return result;
	}	
	
	//PW-38
	
		private String getSubscriberCreditSetXmlforExtendExpiryDate(HashMap requestHashMap,ProvAuthClientObject authData) throws ParserConfigurationException, SAXException, IOException, C30ProvConnectorException {
			String requestPayload = "";
			String planName = "";
			HashMap<String, String> subBalrequestHashMap = new HashMap();
			String subscriberBalanceReqxml=null;
			String dataBalance=null;
			log.info("getSubscriberCreditSetXmlforExtendExpiryDate ****");
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL)) {
				planName = requestHashMap.get(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL).toString();
				
			}
			// Build SubscriberId.
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {		
				requestPayload = C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString(), 
						Constants.OCS_XML_TAG_INT, false);
				}
		    //Get Balance Types associated to subscriber profile 
			if (requestHashMap.containsKey(Constants.PREPAID_SUBSCRIBERID)) {
				subBalrequestHashMap.put(Constants.PREPAID_SUBSCRIBERID, requestHashMap.get(Constants.PREPAID_SUBSCRIBERID).toString());
			}
			
			//PW-38 invoke Subscriber Balances to retrieve current balance of subscriber
			subscriberBalanceReqxml = C30OCSRequestUtils.getSubscriberGetBalancesXml(subBalrequestHashMap);
			//Make a POST call to OCS
	        C30OCSRESTConnection restConnection = new C30OCSRESTConnection();
			String subBalresponseXml=null;
			try {
				subBalresponseXml = restConnection.makeOCSPOSTCall(authData.getWeburl(), subscriberBalanceReqxml);
				dataBalance = C30ProvConnectorUtils.getDataBalance(subBalresponseXml);		
				String balanceTypesPayload = GetSubscriberProfileBalanceTypesforExtendExpiryDate(planName,requestHashMap,dataBalance);
				balanceTypesPayload = C30ProvConnectorUtils.wrapOCSDataMultiplyTags(balanceTypesPayload);
				requestPayload = requestPayload+balanceTypesPayload;

				//Build Reason.
				if (requestHashMap.containsKey(Constants.PREPAID_REASON)) {	    
					requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(requestHashMap.get(Constants.PREPAID_REASON).toString(), 
						Constants.OCS_XML_TAG_STRING, false);
				} else {
					requestPayload = requestPayload + C30ProvConnectorUtils.encloseOCSRequestParamTags(null,Constants.OCS_XML_TAG_STRING, false);
			}
			//Build Final XML
			requestPayload = C30ProvConnectorUtils.createXmlTag(Constants.OCS_XML_TAG_PARAMS, requestPayload, false, false);		
			} catch (C30ProvConnectorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}
			
			return requestPayload;
			
		}	
		//PW-38
		private String GetSubscriberProfileBalanceTypesforExtendExpiryDate(String ProfileLabel, HashMap requestHashMap,String dataBalance) {
			
			String acctSegId = requestHashMap.get(Constants.ACCT_SEG_ID).toString();
			String result=null;
			// TODO Auto-generated method stub
			try{
				log.info("Inside GetSubscriberProfileBalanceTypesforExtendExpiryDate ");
				HashMap<String, String> balrequestHashMap = new HashMap<String, String>();			
				balrequestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);
				balrequestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_GET_PROFILE_BALANCE_TYPES);
				balrequestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,requestHashMap.get(Constants.EXTERNAL_TRANSACTION_ID).toString());				
				balrequestHashMap.put(Constants.C30_TRANSACTION_ID,requestHashMap.get(Constants.C30_TRANSACTION_ID).toString());
				balrequestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		
				balrequestHashMap.put(Constants.PREPAID_LABEL, ProfileLabel);
				
				//Create a Prepaid System Request Instance
				C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
				HashMap responseXML = prepaidRequest.awnprepaidOrderRequest(balrequestHashMap);
				log.info("GetSubscriberProfileBalanceTypes - " + responseXML.get("ResponseXML"));
				result = C30ProvConnectorUtils.getSubsciberProfileBalanceTypesForExtendExpiryDate(responseXML.get("ResponseXML").toString(), requestHashMap,dataBalance);
			}// End - Try
			catch (Exception e)
			{
				log.error("ERROR - Account Segment : " + acctSegId + " SubscriberProfile.GetByLabel: "+e);
				e.printStackTrace();
			}		
			
			return result;
		}	
		
}