package com.cycle30.prov.resource.blackhawk.inquiry;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.config.C30ProvConfig;
import com.cycle30.prov.config.C30ProvSystemParameters;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30BlackHawkRESTConnection;

public class BlackHawkVoucherResource  extends IProvisioningResource{

	boolean simulateRequest = false;
	String voucherPIN = null;
	String voucherAction=null;
	String reversalRequestId=null;
	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(BlackHawkVoucherResource.class);

		log.info("Reached the BlackHawk Voucher Status Resource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();
		
		String webURL =  authData.getWeburl();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap<String, String> requestHashMap = reqProp.getRequestMap();
		HashMap<Integer, C30ProvSystemParameters> paramlist = C30ProvConfig.getInstance().getProvSystemParameterList();
       
		if ( requestHashMap.get(Constants.VOUCHER_PIN) != null && requestHashMap.get(Constants.VOUCHER_ACTION) !=null )
		{
			voucherPIN = (String) requestHashMap.get(Constants.VOUCHER_PIN);
			voucherAction= (String)requestHashMap.get(Constants.VOUCHER_ACTION);
			log.info("VoucherPIN  " + voucherPIN );
			log.info("VoucherAction  " + voucherAction );
		}
		else
		{
			throw new C30ProvConnectorException(" Error Message from Provisioning Connector  : " + "Voucher PIN or Action is Null ", "BLACKHAWK-VOUCHER-001");
		}
		String pan=C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_BLACKHAWK,
				Constants.PREPAID_PARAM_PRIMARY_ACCOUNT_NUMBER,reqProp.getAcctSegId() );
		
		
		String aid=C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_BLACKHAWK,
				Constants.PREPAID_PARAM_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE,reqProp.getAcctSegId());
		
		
		String mid=C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_BLACKHAWK,
				Constants.PREPAID_PARAM_MERCHANT_TERMINAL_ID,reqProp.getAcctSegId());
		
		String pid=C30ProvConnectorUtils.getParamValueFromParamListForAccSeg(paramlist,Constants.PROV_SYSTEM_PARAM_MODULE_BLACKHAWK,
				Constants.PREPAID_PARAM_PRODUCT_ID,reqProp.getAcctSegId());
		
		
		if( (pan !=null || "".equalsIgnoreCase(pan)) && (aid!=null ||"".equalsIgnoreCase(aid)) && (mid !=null ||"".equalsIgnoreCase(mid) ) && (pid !=null ||"".equalsIgnoreCase(pid) ) ){
			requestHashMap.put(Constants.PREPAID_PARAM_PRIMARY_ACCOUNT_NUMBER,pan);
			requestHashMap.put(Constants.PREPAID_PARAM_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE,aid);
			requestHashMap.put(Constants.PREPAID_PARAM_MERCHANT_TERMINAL_ID,mid);
			requestHashMap.put(Constants.PREPAID_PARAM_PRODUCT_ID,pid);
		}else{
			
			throw new C30ProvConnectorException(" Error Message from Provisioning Connector  : " + "BlackHawk Mandatory Param is Null ", "BLACKHAWK-VOUCHER-002");
			
		}
					
		String xml=null;	
		if (requestHashMap.get(Constants.REVERSAL_REQUEST_ID) != null  )
		{
			String toreplace="";
			String randomNumber=(new Integer((new Random().nextInt(999999))).toString());			
			toreplace=C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_TRANSMISSION_DATE_TIME_YYMMDDHHMMSS,getDateDesiredDateFormatInUTC("yyMMddHHmmss"), false, false);
			toreplace= toreplace + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_SYSTEM_TRACE_AUDIT,randomNumber, false, false);
			toreplace=(toreplace.replaceAll("\n|\r", ""));
			reversalRequestId = (String) requestHashMap.get(Constants.REVERSAL_REQUEST_ID);
			String dbxml=C30ProvConnectorUtils.getRequestXMLForBlackHawkReversalRequestId(reversalRequestId,datasource);
			dbxml=(dbxml.replaceAll("\n|\r", ""));
			xml=dbxml.replaceFirst("<transmissionDateTime>.*</systemTraceAuditNumber>", toreplace);
			log.info("ReversalRequestId  " + reversalRequestId );
			log.info("XML From the DB for the BLACKHAWK Voucher Status/Redeem Request for Reversal " + xml);
			webURL=webURL.concat(Constants.BLACKHAWK_REVERSAL_URI);
			if(xml==null) throw new C30ProvConnectorException(" Error Message from Provisioning Connector  : " + "BlackHawk Request XML Not Found for Given RequestId ", "BLACKHAWK-VOUCHER-002");
			
		}else{
			  xml = C30ProvConnectorUtils.encloseBlackHawkInquiryTags(getBlackHawkVoucherStatusXml(reqProp, requestHashMap));
			 log.info("XML formed for the BLACKHAWK Voucher Status/Redeem Request without Reversal " + xml);

			 requestHashMap.put(Constants.REQUEST_XML, xml);
			 new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
		}
		
		if(xml==null) throw new C30ProvConnectorException(" Error Message from Provisioning Connector  : " + "BlackHawk Request XML ", "BLACKHAWK-VOUCHER-002");
		
		 C30BlackHawkRESTConnection restConnection = new C30BlackHawkRESTConnection();
		
		 String responseXml ="";
		 responseXml=restConnection.makeBlackHawkPOSTCall(webURL, xml);

		 //Populating the response.
		GenericInquiryResponse response = new GenericInquiryResponse();
		try
		{
			HashMap<String, String> responseMap = populateResponse(request,responseXml);
			response.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return response;
	}

	
	private static String getBlackHawkVoucherStatusXml(
			ProvisioningRequestProperties reqProp, HashMap<?, ?> requestHashMap) throws C30ProvConnectorException {
		String reqXml = "";
		

		if (requestHashMap.get(Constants.VOUCHER_PIN) != null ) {
			
			
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_PRIMARY_ACCOUNT_NUMBER,requestHashMap.get(Constants.PREPAID_PARAM_PRIMARY_ACCOUNT_NUMBER).toString(), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_PROCESSING_CODE,requestHashMap.get(Constants.VOUCHER_ACTION).toString(), false, false);
		    if((Constants.VOUCHER_STATUS_BLACKHAWK_ACTION).equalsIgnoreCase(requestHashMap.get(Constants.VOUCHER_ACTION).toString())){
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_TRANSACTION_AMOUNT,"000000000000", false, false);
		    }else{
		    	 reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_TRANSACTION_AMOUNT,requestHashMap.get(Constants.PREPAID_BALANCE_BALANCE_Value).toString(), false, false);
		    }			  
		    
		   String radomNumber=(new Integer((new Random().nextInt(999999))).toString());
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_TRANSMISSION_DATE_TIME_YYMMDDHHMMSS,getDateDesiredDateFormatInUTC("yyMMddHHmmss"), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_SYSTEM_TRACE_AUDIT,radomNumber, false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_LOCAL_TRANSACTION_TIME,getDateDesiredDateFormatInUTC("HHmmss"), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_LOCAL_TRANSACTION_DATE,getDateDesiredDateFormatInUTC("yyMMdd"), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_MERCHANT_CODE,"5411", false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_POINT_OF_SERVICE_ENTRY_MODE,"041", false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE,requestHashMap.get(Constants.PREPAID_PARAM_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE).toString(), false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_RETRIEVAL_REF_NUMBER,"000000"+radomNumber, false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_MERCHANT_TERMINAL_ID,requestHashMap.get(Constants.PREPAID_PARAM_MERCHANT_TERMINAL_ID).toString()+"   ",false,false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_MERCHANT_IDENTIFIER, requestHashMap.get(Constants.PREPAID_PARAM_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE).toString()+"    ", false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_MERCHANT_LOCATION, "GCI STORE", false, false);
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_TRANSACTION_CURRENCY_CODE, "840", false, false);	    
		    reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_ADDITIONAL_TRANSACTION_FIELDS, 
		     C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_PRODUCT_ID,requestHashMap.get(Constants.PREPAID_PARAM_PRODUCT_ID).toString(), false, false)+
		     C30ProvConnectorUtils.createXmlTag(Constants.BLACKHAWK_XML_TAG_REDEMPTION_PIN,requestHashMap.get(Constants.VOUCHER_PIN).toString(), false, false),false,false);
		    		
		}
		return reqXml;
	}


	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap<String, String> populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {
		
		String requestId= request.getRequestProperties().getRequestId().toString();
		if(reversalRequestId != null)requestId=reversalRequestId;

		if((Constants.VOUCHER_STATUS_BLACKHAWK_ACTION).equalsIgnoreCase(voucherAction)){
			if (responseXml == null || responseXml.length() == 0) {
				throw new C30ProvConnectorException("BLACKHAWK-VOUCHER-001");
			}
		}else if (responseXml == null || responseXml.length() == 0) {
			throw new C30ProvConnectorException(requestId  , "BLACKHAWK-REVERSAL");
		}
		
		String errorNo = null;
		String errorMsg = null;
		String faceValue=null;
		if (responseXml != null) {
			try {
				HashMap<?, ?> errordetails = C30ProvConnectorUtils.parseBlackHawkFaultResponse(responseXml);
				if (errordetails != null && errordetails.get(0) != null) {
					HashMap<?, ?> stackDetails = (HashMap<?, ?>) errordetails.get(0);

					if (stackDetails.get("RespCode") != null && stackDetails.get("RespMsg") != null) {
						errorNo = "BLACKHAWK-"+ stackDetails.get("RespCode").toString();
						errorMsg= stackDetails.get("RespMsg").toString();
					}
					faceValue=stackDetails.get("FaceValue").toString();
					
				}
			} catch (Exception e) {
				if(e instanceof ParserConfigurationException && (Constants.VOUCHER_REDEEM_BLACKHAWK_ACTION).equalsIgnoreCase(voucherAction)){
					throw new C30ProvConnectorException(requestId  , "BLACKHAWK-REVERSAL");
				}else
				throw new C30ProvConnectorException(e, "PROV-020");
			}
		}
		
		

		// non-zero other than one,return is an error
		if (errorNo != null &&  !(errorNo.equalsIgnoreCase("BLACKHAWK-00"))) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			
			if(errorNo.equalsIgnoreCase("BLACKHAWK-74") || errorNo.equalsIgnoreCase("BLACKHAWK-15") ){
				
			throw new C30ProvConnectorException(requestId, "BLACKHAWK-REVERSAL");
			}else{
				throw new C30ProvConnectorException(errorMsg, errorNo);
			}
		} 
		
				
		HashMap<String, String> response = new HashMap<String, String>();
		response.put(Constants.RESPONSE_XML, responseXml);
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		response.put("FaceValue", faceValue);
		return response;


	}

	
	static String getDateDesiredDateFormatInUTC( String format){
		SimpleDateFormat  sdf = new SimpleDateFormat(format);
		   sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		return sdf.format(new Date());
		 
	 }
}

