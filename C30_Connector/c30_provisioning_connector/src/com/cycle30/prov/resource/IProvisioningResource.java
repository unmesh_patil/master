package com.cycle30.prov.resource;

import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.sdk.datasource.C30JDBCDataSource;


public abstract class IProvisioningResource {
	
	// Base datasource for all the resources
	protected C30JDBCDataSource datasource = null;
	
	

	public C30JDBCDataSource getDatasource() {
		return datasource;
	}


	public void setDatasource(C30JDBCDataSource datasource) {
		this.datasource = datasource;
	}

	public IProvisioningResource(){}

	public IProvisioningResource(C30JDBCDataSource datasource)
	{
		this.datasource = datasource;
	}

	public abstract IProvisioningResponse handleRequest(IProvisioningRequest request) throws C30ProvConnectorException;

}
