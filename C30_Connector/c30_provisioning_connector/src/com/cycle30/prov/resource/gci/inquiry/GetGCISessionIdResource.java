package com.cycle30.prov.resource.gci.inquiry;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;

import com.cycle30.prov.C30ProvisioningConnector;
import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30MulePOSRESTConnection;

public class GetGCISessionIdResource  extends IProvisioningResource{

	boolean simulateRequest = false;
	String gciEnv = null;
	String gciUserId=null;
	String gciPassword=null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(GetGCISessionIdResource.class);

		log.info("Reached the GetGCISessionIdResource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();


		ArrayList<NameValuePair> headers=null;

		if ( authData.getproperty1()!=null  )
		{
		   gciEnv=authData.getproperty1();
  	       gciUserId=authData.getUserName();
		   gciPassword=authData.getPassword();

		   headers = new ArrayList<NameValuePair>();
		   headers.add(new NameValuePair(Constants.PREPAID_GCI_HEADERS_CLIENT_ID,gciEnv));
		   headers.add(new NameValuePair(Constants.PREPAID_GCI_HEADERS_USER_ID, gciUserId));
		   
		}
		else
		{
			throw new C30ProvConnectorException("PROV-INQ-001");
		}

		StringBuffer xml= new StringBuffer();
		xml.append(Constants.GCI_XML_HEADER); 
		xml.append(Constants.GCI_XML_HEADER_EXTN);
		xml.append(C30ProvConnectorUtils.createXmlTag(Constants.PREPAID_GCI_ATTRIBUTE_PASSWORD,gciPassword,false,false));
		xml.append(Constants.GCI_XML_FOOTER);

        log.info("XML formed for the GCI session Authentication Request" + xml);
        C30MulePOSRESTConnection restConnection = new C30MulePOSRESTConnection();
        String webURL =  authData.getWeburl().concat(Constants.GCI_SESSION_AUTHENTICATE);
        
        
		String responseXml = restConnection.makeGCICall(webURL,Constants.HTTP_POST,headers,xml.toString(),null);

		//Populating the response.
		GenericInquiryResponse authenticationResponse = new GenericInquiryResponse();
		try
		{
			HashMap responseMap = populateResponse(responseXml);
			authenticationResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return authenticationResponse;
	}

	/** This is the reponse formation for the Resource
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap populateResponse(String responseXml) throws C30ProvConnectorException {


		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-012");
		}


		HashMap response = new HashMap();
		response.put(Constants.RESPONSE_XML, responseXml);
		return response;


	}

}

