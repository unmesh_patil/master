package com.cycle30.prov.resource.gci.inquiry;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.log.ProvRequestLogger;
import com.cycle30.prov.request.IProvisioningRequest;
import com.cycle30.prov.request.ProvisioningRequestProperties;
import com.cycle30.prov.resource.IProvisioningResource;
import com.cycle30.prov.response.IProvisioningResponse;
import com.cycle30.prov.response.sps.GenericInquiryResponse;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.prov.webservice.C30MulePOSRESTConnection;

public class GetGCIPhoneInventoryResource  extends IProvisioningResource{

	boolean simulateRequest = false;
	String gciEnv = null;
	String gciUserId=null;
	String gciSessionId=null;

	@Override
	public IProvisioningResponse handleRequest(IProvisioningRequest request)
			throws C30ProvConnectorException {

		Logger log = Logger.getLogger(GetGCIPhoneInventoryResource.class);

		log.info("Reached the GetGCIPhoneInventoryResource " );
		ProvAuthClientObject authData = request.getRequestProperties().getAuthClient();

		ProvisioningRequestProperties  reqProp = request.getRequestProperties();
		HashMap requestHashMap = reqProp.getRequestMap();
		ArrayList<NameValuePair> headers=null;
		
	//	simulateRequest = (Boolean) requestHashMap.get(Constants.ATTR_SIMULATE_REQUEST);

		if ( (authData.getproperty1()!=null  && authData.getUserName() !=null) && requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID)!= null )
		{
		   gciEnv=authData.getproperty1();
		   gciUserId=authData.getUserName();
           gciSessionId= requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID).toString();
            headers = new ArrayList<NameValuePair>();
			headers.add(new NameValuePair(Constants.PREPAID_GCI_HEADERS_CLIENT_ID,gciEnv));
			headers.add(new NameValuePair(Constants.PREPAID_GCI_HEADERS_USER_ID, gciUserId));
			if(gciSessionId !=null && !("".equals(gciSessionId)))
			{
				headers.add(new NameValuePair(Constants.PREPAID_GCI_HEADERS_SESSION_ID,gciSessionId ));
			}

		   
		}
		else
		{
			throw new C30ProvConnectorException(" Error Message from GCI : " + "GCI Headers are NULL", "GCI-EXCEPTION-001");
		}

	    C30MulePOSRESTConnection restConnection = new C30MulePOSRESTConnection();
        String webURL =  authData.getWeburl().concat(Constants.GCI_PHONE_INVENTORY_INQUIRY);
        String responseXml=null;
        
        if(requestHashMap.get(Constants.PREPAID_GCI_ATTRIBURE_ACTION_TYPE) ==null){
        	
        StringBuffer parametersToLog= new StringBuffer();
        parametersToLog.append(Constants.GCI_PHONE_INVENTORY_INQUIRY+ "?");
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        
        if(requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_COMMUNITY)!=null && requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SIZE) !=null){
        	parametersToLog.append(Constants.GCI_MULE_POS_PARAM_COMMUNITY+"="+requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_COMMUNITY)+"|");
        	params.add(new NameValuePair(Constants.GCI_MULE_POS_PARAM_COMMUNITY, requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_COMMUNITY).toString()));
        	parametersToLog.append(Constants.GCI_MULE_POS_PARAM_SIZE+"="+requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SIZE));
        	params.add(new NameValuePair(Constants.GCI_MULE_POS_PARAM_SIZE,requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SIZE).toString()));
       
           }
         if(requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER)!=null){
	    	   parametersToLog.append(Constants.GCI_MULE_POS_PARAM_TELEPHONE_NUMBER+"="+requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER)+"|");
	    	   params.add(new NameValuePair(Constants.GCI_MULE_POS_PARAM_TELEPHONE_NUMBER,requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER).toString()));
	        }
         if(requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS)!=null){
	        	 parametersToLog.append(Constants.GCI_MULE_POS_PARAM_INV_STATUS+"="+requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS)+"|");
	        	params.add(new NameValuePair(Constants.GCI_MULE_POS_PARAM_INV_STATUS,requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS).toString()));
	        }
          if(requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID)!=null){
	        	 parametersToLog.append(Constants.GCI_MULE_POS_PARAM_SALES_CHANNEL_ID+"="+requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID)+"|");
	        	params.add(new NameValuePair(Constants.GCI_MULE_POS_PARAM_SALES_CHANNEL_ID,requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID).toString()));
	        }	
	        	
	        		
	    		if(params.isEmpty()) {	    			
	    			throw new C30ProvConnectorException(" Error Message from GCI : " + "Params are NULL", "GCI-EXCEPTION-002");
	    		}else{
	    			
	    			 log.info("Web URL for Prepaid Phone Inventory Inquiry Query : " + webURL );
		    		 requestHashMap.put(Constants.REQUEST_XML, parametersToLog.toString());
		    		 new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
		    		 responseXml = restConnection.makeGCICall(webURL,Constants.HTTP_GET,headers, null,params);
				} 
	    		 
        
        }else if(requestHashMap.get(Constants.PREPAID_GCI_ATTRIBURE_ACTION_TYPE) !=null ){
        	
        	// (String) requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_INVENTORY_PAYLOAD);
        	String salesChannelId="";
        	String operatorId="";
        	if (requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID)!=null){
        		 salesChannelId=(String)requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID);
        	}
        	if (requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_OPERATOR_ID)!=null){
        		operatorId=(String)requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_OPERATOR_ID);
        	}
   
        	String payload= C30ProvConnectorUtils.encloseGCIMulePosTags(getPayloadforGCI(reqProp, requestHashMap,salesChannelId,operatorId));
        	log.info("XML  Formed " +payload);
        	 requestHashMap.put(Constants.REQUEST_XML, payload);
     		 new ProvRequestLogger().auditFeatureItemRequest(request, datasource);
     		 responseXml = restConnection.makeGCICall(webURL,Constants.HTTP_PUT,headers,payload ,null);
        }

		//Populating the response.
		GenericInquiryResponse authenticationResponse = new GenericInquiryResponse();
		try
		{
			HashMap responseMap =populateResponse(request,responseXml);
			authenticationResponse.setResponseMap(responseMap);
		}
		catch(C30ProvConnectorException e)
		{
			throw e;
		}

		return authenticationResponse;
	}

	private String getPayloadforGCI(ProvisioningRequestProperties reqProp, HashMap requestHashMap,String salesChannelId ,String operatorId) {
		String reqXml="";
		String statusId="";
		String disconnectReason = "";  //disconnectReason and reqSystemId added by shahshai for PSST-55
        String reqSystemId = "";
		Logger log = Logger.getLogger(GetGCIPhoneInventoryResource.class);
		log.info("Check the requestHashMap "+requestHashMap.toString());
		if(requestHashMap.containsKey(Constants.PREPAID_GCI_ATTRIBUTE_DISCONNECT_REASON)&&requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_DISCONNECT_REASON) !=null){
			disconnectReason = (String)requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_DISCONNECT_REASON);
		}
		if(requestHashMap.containsKey(Constants.PREPAID_GCI_ATTRIBUTE_REQ_SYSTEM_ID)&&requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_REQ_SYSTEM_ID) !=null){
			reqSystemId = (String)requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_REQ_SYSTEM_ID);
		}
		
		if(requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS) !=null ){
			statusId=C30ProvConnectorUtils.getTNStatusIdForGivenStatus((String)requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS));
		}
		
		if (requestHashMap.containsKey(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER)) {	
			reqXml= C30ProvConnectorUtils.createXmlTag(Constants.GCI_MULE_POS_XML_TELEPHONE_NUMEBR_TAG,requestHashMap.get(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER).toString(), false, false);				
		}
			reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GCI_MULE_POS_XML_STATUS_ID_TAG,statusId, false, false);			
	        reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GCI_MULE_POS_XML_SALES_CHANNEL_ID_TAG,salesChannelId, true, true);
	        reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GCI_MULE_POS_XML_DISCONNECT_REASON_TAG,disconnectReason,true,true);
	        reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GCI_MULE_POS_XML_REQ_SYSTEM_ID,reqSystemId,true,true);
			reqXml= reqXml + C30ProvConnectorUtils.createXmlTag(Constants.GCI_MULE_POS_XML_OPERATOR_ID_TAG,operatorId,true,true);
		
		log.info("XML Formed  "+reqXml);
		return reqXml;
		
	}

	/** This is the reponse formation for the Resource
	 * @param request 
	 * 
	 * @param responseXml
	 * @return
	 * @throws C30ProvConnectorException
	 */
	private HashMap populateResponse(IProvisioningRequest request, String responseXml) throws C30ProvConnectorException {


		if (responseXml == null || responseXml.length()==0) {
			throw new C30ProvConnectorException("PROV-INQ-012");
		}

		String  errorNo=null;
		String errorMsg=null;
		if(responseXml !=null){
			try {
        HashMap<?, ?> errordetails= C30ProvConnectorUtils.parseGCIFaultResponse(responseXml, "Code", "Message");
        if(errordetails !=null &&  errordetails.get(0)!=null){
          HashMap<?, ?> stackDetails=	(HashMap<?, ?>) errordetails.get(0);
           
            if(stackDetails.get("ErrorCode") !=null && stackDetails.get("ErrorMsg") !=null){
            	 errorNo= "GCI-"+stackDetails.get("ErrorCode").toString();
            	 errorMsg= stackDetails.get("ErrorMsg").toString() ;
					}

				}
			} catch (Exception e) {
				throw new C30ProvConnectorException(e, "PROV-018");
			}
		}

		// non-zero return is an error
		if (errorNo != null ) {
			C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, responseXml, datasource);
			C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_IN_ERROR, datasource);
			throw new C30ProvConnectorException(" Error Message from GCI : " + errorMsg, errorNo);
		} 


		HashMap<String, String> response = new HashMap<String,String>();
		C30ProvConnectorUtils.updateStatusAndResponseXMLForRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, responseXml, datasource);
		C30ProvConnectorUtils.updateStatusOfRequestTable(request, Constants.STATUS_REQUEST_COMPLETED, datasource);
		response.put(Constants.RESPONSE_XML, responseXml);
		if(request.getRequestProperties().getRequestMap().get(Constants.PREPAID_GCI_ATTRIBURE_ACTION_TYPE) ==null){
			try {
				HashMap<Integer, HashMap<String, String>> bundleResult =  new HashMap<Integer, HashMap<String, String>>();
				bundleResult=C30ProvConnectorUtils.parseXMLForGCIRetreiveDetails(responseXml);
				HashMap<String,String> resultMap = (HashMap<String, String>) bundleResult.get(0);
				response.put(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER, resultMap.get(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER));
				response.put(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS, resultMap.get(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS));
				response.put(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID, resultMap.get(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID));
				response.put(Constants.PREPAID_GCI_ATTRIBUTE_OPERATOR_ID, resultMap.get(Constants.PREPAID_GCI_ATTRIBUTE_OPERATOR_ID));
				response.put("CreateDate", resultMap.get("CreateDate"));
				response.put("NetworkId", resultMap.get("NetworkId"));				
				
			} catch (Exception  e) {
				throw new C30ProvConnectorException(" Error Message while Parsing the Response from GCI : " + e.getMessage(), "1");
			}
		}
		return response;
		

	}

}

