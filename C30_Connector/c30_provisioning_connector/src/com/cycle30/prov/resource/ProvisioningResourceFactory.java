package com.cycle30.prov.resource;

import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.resource.acs.inquiry.GetACSPhoneInventoryResource;
import com.cycle30.prov.resource.blackhawk.inquiry.BlackHawkVoucherResource;
import com.cycle30.prov.resource.gci.inquiry.GetGCIPhoneInventoryResource;
import com.cycle30.prov.resource.gci.inquiry.GetGCISessionIdResource;
import com.cycle30.prov.resource.incomm.inquiry.IncommVoucherResource;
import com.cycle30.prov.resource.ocs.SubscriberBalanceResource;
import com.cycle30.prov.resource.ocs.SubscriberProfileResource;
import com.cycle30.prov.resource.ocs.SubscriberResource;
import com.cycle30.prov.resource.ocs.inquiry.GetPrepaidBalanceResource;
import com.cycle30.prov.resource.ocs.inquiry.GetPrepaidBalanceTypesResource;
import com.cycle30.prov.resource.ocs.inquiry.GetPrepaidProfilesResource;
import com.cycle30.prov.resource.ocs.inquiry.GetPrepaidSubcriberDetailsResource;
import com.cycle30.prov.resource.ocs.inquiry.PrepaidAccountResource;
import com.cycle30.prov.resource.ocs.inquiry.PrepaidVoucherResource;
import com.cycle30.prov.resource.sps.inquiry.GetIMSIResource;
import com.cycle30.prov.resource.sps.inquiry.SPSRequestHistoryResource;
import com.cycle30.prov.resource.sps.inquiry.SPSRequestStatusResource;
import com.cycle30.prov.resource.sps.inquiry.SubscriberInquiryResource;
import com.cycle30.prov.resource.sps.inquiry.TelephoneNumberInquiryResource;
import com.cycle30.prov.resource.sps.inquiry.ValidateSIMResource;

/**
 * Factory class for FX Web Enablement resource objects.
 *
 */
public class ProvisioningResourceFactory {

	// Instantiate singleton
	private static final ProvisioningResourceFactory self = new ProvisioningResourceFactory();

	private static final Logger log = Logger.getLogger(ProvisioningResourceFactory.class);

	private static final String EXCEPTION_SOURCE = "RESOURCE-FACTORY";


	//-------------------------------------------------------------------------
	/** Private constructor. 
	 */
	private ProvisioningResourceFactory() {
		super();
	}


	//-------------------------------------------------------------------------
	/**
	 * Return instance of singleton class
	 * 
	 * @return ResourceRequestFactory instance.
	 */
	public static ProvisioningResourceFactory getInstance() {
		return self;
	}




	/**Instantiate correct resource interface based on the resource name
	 * 
	 * @param resourceType
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public IProvisioningResource getResourceImpl(String resourceType) throws C30ProvConnectorException  {

		if (resourceType.startsWith(Constants.REQUEST_IMSI_VERIFICATION)) {
			return new GetIMSIResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_VALIDATE_SIM)) {
			return new ValidateSIMResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_TELEPHONE_NUMBER_VERIFICATION)) {
			return new TelephoneNumberInquiryResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_SUBSCRIBER_INQUIRY)) {
			return new SubscriberInquiryResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_SPS_REQUEST_HISTORY_INQUIRY)) {
			return new SPSRequestHistoryResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_SPS_REQUEST_STATUS_BY_REQUEST_ID)) {
			return new SPSRequestStatusResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_SPS_REQUEST_STATUS_BY_TRANSACTION_ID)) {
			return new SPSRequestStatusResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_BALANCE_INQUIRY)) {
			return new GetPrepaidBalanceResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_GCI_MULEPOS_SESSION_ID_INQUIRY)) {
			return new GetGCISessionIdResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_GCI_PHONE_INVENTORY_INQUIRY)) {
			return new GetGCIPhoneInventoryResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_GCI_PHONE_INVENTORY_UPADTE)) {
			return new GetGCIPhoneInventoryResource();
		}
		if (resourceType.equalsIgnoreCase(Constants.REQUEST_ACS_PHONE_INVENTORY_INQUIRY)) {
			return new GetACSPhoneInventoryResource();			
		}
		if (resourceType.equalsIgnoreCase(Constants.REQUEST_ACS_PHONE_INVENTORY_UPADTE)) {
			return new GetACSPhoneInventoryResource();			
		}
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_VOUCHER_STATUS)) {
			return new PrepaidVoucherResource();
		}	
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_BALANCE_UNITS_CACHE)) {
			return new GetPrepaidBalanceTypesResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_DETAILS)) {
			return new GetPrepaidSubcriberDetailsResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_OCS_ACCOUNT_HISTORY) || resourceType.startsWith(Constants.REQUEST_OCS_ACCOUNT_SEARCH)) {
			return new PrepaidAccountResource();
		}	
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER) || resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_UPDATE_SUBSCRIBER)) {
			return new SubscriberResource();
		}	
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_PROFILE)) {
			return new SubscriberProfileResource();
		}	
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_ORDER_BAL_UPDATE)) {
			return new SubscriberBalanceResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_OCS_PREPAID_PROFILES_CACHE)) {
			return new GetPrepaidProfilesResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_INCOMM_PREPAID_VOUCHER_STATUS)) {
			return new IncommVoucherResource();
		}
		if (resourceType.startsWith(Constants.REQUEST_BLACK_HAWK_PREPAID_VOUCHER_STATUS)) {
			return new BlackHawkVoucherResource();
		}

		
		// If not found, 
		String error = "Unable to instantiate correct resource type based on resource type " + resourceType;
		log.error(error);

		throw new C30ProvConnectorException ("PROV-005");


	}

	/** Instantiate correctResource interface based on the resource class
	 * 
	 * @param resourceClass
	 * @return
	 * @throws C30ProvConnectorException
	 */
	public IProvisioningResource getResourceImpl(Class resourceClass) throws C30ProvConnectorException  {

		try{
			return (IProvisioningResource) resourceClass.newInstance();
		}
		catch(Exception e)
		{

			// If not found, 
			String error = "Unable to instantiate correct resource type  based on resource type " + resourceClass.toString();
			log.error(error);
			
			throw new C30ProvConnectorException ( e, "PROV-005");
		}

	}
}
