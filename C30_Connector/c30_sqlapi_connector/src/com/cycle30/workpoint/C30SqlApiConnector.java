package com.cycle30.workpoint;

public class C30SqlApiConnector {

	public static void main(String[] args) { 

		System.out.println("");
		System.out.println("**********************************************************************");
		System.out.println("C30_SqlApi_Connector Build Version : " + C30SqlApiBuildVersion.LATEST_BUILD_VERSION);
		System.out.println("@2012. www.Cycle30.com. All rights Reserved");
		System.out.println("**********************************************************************");
		System.out.println("");
		System.out.println("c30_sqlapi_connector.jar has to be used from the C30 BIL not for direct use by other modules.");
		
	}
}
