
package com.cycle30.workpoint.sqlapi;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;

/** This script works on the Get Plan Info API
 * 
 * @author Umesh
 *  - Initial Version
 *  
 *  
 */

public class C30WPGetPlanInfoApi {
	
	private static Logger log = Logger.getLogger(C30WPGetPlanInfoApi.class);
	
	private C30JDBCDataSource dataSource=null;	

	public C30WPGetPlanInfoApi()
	{
		log.info("Default constructor .....................C30WPGetPlanInfoApi");
	}

	@SuppressWarnings("unchecked")
	public C30WPGetPlanInfoApi( Connection conn,
			String transactionId,
			String acctSegId,
			String instanceId
			) throws C30SDKToolkitException
	{
	    log.info("In C30WPGetPlanInfoApi class...");
		try
		{
			dataSource = new C30JDBCDataSource(conn);

			C30ExternalCallDef call = new C30ExternalCallDef(C30WPSQLConstants.C30_GET_PLAN_INFO);

			call.addParam(new C30CustomParamDef("v_transaction_id",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_instance_id",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("c30getPlanInfo_cv",10,  2000, 2));
			//OUT Parameter
			call.addParam(new C30CustomParamDef("d_catalog_id",2,  2000, 2));	

			log.info("Total Number of Add Plan Input parameters : "+ call.getInputParams().size() );

			//Setting up the value
			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------
			if (transactionId!=null ) paramValues.add(new String(transactionId)); else paramValues.add(null);
			if (acctSegId!=null ) paramValues.add(new Integer(acctSegId)); else paramValues.add(null);
			if (instanceId!=null ) paramValues.add(new String(instanceId)); else paramValues.add(null);
			
			log.info("Total Number of Get Plan Info Input ParamValues : "+ paramValues.size() );
			log.info("START - Get Plan Info for Instance Id : " +instanceId);
			//Making data base Query
			dataSource.queryData(call, paramValues, 2);
			log.info("END - Get Plan Info for Instance Id : " +instanceId);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30WPSQLConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30WPSQLConstants.SQL_ERROR_CODE_LENGTH+1);
			log.error(e.getMessage());
			throw new C30SDKToolkitException(e.getMessage(), e);
		}

	}

	public C30JDBCDataSource getDataSource()
	{
		return dataSource;
	}
	
	public void setDataSource(C30JDBCDataSource dataSource)
	{
		this.dataSource = dataSource;
	}

}
