package com.cycle30.workpoint.sqlapi;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;

/** This script works on the Update Account API
 * 
 * @author Umesh
 * - Initial Version
 * - Added the TransactionId Features
 * - Modified to align with the SQL-API created for wholesale project
 *
 */

public class C30WPUpdateAccountApi {
	private static Logger log = Logger.getLogger(C30WPUpdateAccountApi.class);
	
	private C30JDBCDataSource dataSource=null;

	public C30WPUpdateAccountApi()
	{
		log.info("Default constructor .....................C30WPUpdateAccountApi");
	}
	@SuppressWarnings("unchecked")
	public C30WPUpdateAccountApi(Connection conn, String transactionId, String acctSegId, 
			String acctId, String actionDate, 
			String actionWho, String noBill, String parentId,
			String accountCategory, String accountType,
			String houseNumber, String houseNumberSuffix, String prefixDirectional,
			String streetName, String streetSuffix, String postfixDirectional,
			String unitType, String unitNumber,
			String address1,
			String address2, String address3, String city, String county,
			String state, 
			String postalCode, String extenPostalCode,
			String countryCode, String company,
			String namePre, String fName, String mInit, String lName,
			String nameGeneration, String billPeriod, 
			String collectionIndicator, String collectionStatus,
			String languageCode, String billDispMeth, String billFmtOpt,
			String insertGrpId, String msgGrpId, String mktCode,
			String rateClass, String regulatoryId,
			String revRcvCostCtr, String franchiseTaxCode, String vertexGeoCode,  String vipCode,
			String billingInquiryCenter, String serviceInquiryCenter,
			String reemittanceCenter, String returnMailCenter, C30ExtendedDataList extDatList) throws C30SDKToolkitException
			{

		try
		{

			dataSource = new C30JDBCDataSource(conn);

			C30ExternalCallDef call = new C30ExternalCallDef(C30WPSQLConstants.C30_UPDATE_ACCOUNT_API_SQL_NAME);

			call.addParam(new C30CustomParamDef("v_transaction_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_acct_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_date",13,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_who",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_no_bill",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_parent_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_account_category",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_account_type",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_house_number",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_house_number_suffix",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_prefix_directional",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_street_name",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_street_suffix",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_postfix_directional",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_unit_type",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_unit_no",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_address1",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_address2",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_address3",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_city",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_county",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_state",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_postal_code",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_extended_postal_code",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_country_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_company",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_name_pre",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_fname",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_minit",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_lname",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_name_generation",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_bill_period",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_collection_indicator",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_collection_status",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_language_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_bill_disp_meth",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_bill_fmt_opt",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_insert_grp_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_msg_grp_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_mkt_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_rate_class",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_regulatory_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_rev_rcv_cost_ctr",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_franchise_tax_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_vertex_geocode",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_vip_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_billing_inquiry_center",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_service_inquiry_center",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_remittance_center",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_return_mail_center",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_ext_data",10,  2000, 1));

			log.info("Total Number of Update Account Input parameters : "+ call.getInputParams().size() );

			//Setting up the value

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------

			if (transactionId!=null ) paramValues.add(new String(transactionId)); else paramValues.add(null);			
			if (acctSegId!=null ) paramValues.add(new Integer(acctSegId)); else paramValues.add(null);
			if (acctId!=null ) paramValues.add(new String(acctId)); else paramValues.add(null);
			if (actionDate!=null ) paramValues.add(C30SqlApiUtils.getDateFromString(actionDate)); else paramValues.add(new Date());
			if (actionWho!=null ) paramValues.add(new String(actionWho)); else paramValues.add(null);
			if (noBill!=null ) paramValues.add(new Integer(noBill)); else paramValues.add(null);
			if (parentId!=null ) paramValues.add(new Integer(parentId)); else paramValues.add(null);
			if (accountCategory!=null ) paramValues.add(new Integer(accountCategory)); else paramValues.add(null);
			if (accountType!=null ) paramValues.add(new Integer(accountType)); else paramValues.add(null);
			if (houseNumber!=null ) paramValues.add(new String(houseNumber)); else paramValues.add(null);
			if (houseNumberSuffix!=null ) paramValues.add(new String(houseNumberSuffix)); else paramValues.add(null);
			if (prefixDirectional!=null ) paramValues.add(new String(prefixDirectional)); else paramValues.add(null);
			if (streetName!=null ) paramValues.add(new String(streetName)); else paramValues.add(null);
			if (streetSuffix!=null ) paramValues.add(new String(streetSuffix)); else paramValues.add(null);
			if (postfixDirectional!=null ) paramValues.add(new String(postfixDirectional)); else paramValues.add(null);
			if (unitType!=null ) paramValues.add(new String(unitType)); else paramValues.add(null);
			if (unitNumber!=null ) paramValues.add(new String(unitNumber)); else paramValues.add(null);			
			if (address1!=null ) paramValues.add(new String(address1)); else paramValues.add(null);
			if (address2!=null ) paramValues.add(new String(address2)); else paramValues.add(null);
			if (address3!=null ) paramValues.add(new String(address3)); else paramValues.add(null);
			if (city!=null ) paramValues.add(new String(city)); else paramValues.add(null);
			if (county!=null ) paramValues.add(new String(county)); else paramValues.add(null);
			if (state!=null ) paramValues.add(new String(state)); else paramValues.add(null);
			if (postalCode!=null ) paramValues.add(new String(postalCode)); else paramValues.add(null);
			if (extenPostalCode!=null ) paramValues.add(new String(extenPostalCode)); else paramValues.add(null);
			if (countryCode!=null ) paramValues.add(new Integer(countryCode)); else paramValues.add(null);
			if (company!=null ) paramValues.add(new String(company)); else paramValues.add(null);
			if (namePre!=null ) paramValues.add(new String(namePre)); else paramValues.add(null);
			if (fName!=null ) paramValues.add(new String(fName)); else paramValues.add(null);
			if (mInit!=null ) paramValues.add(new String(mInit)); else paramValues.add(null);
			if (lName!=null ) paramValues.add(new String(lName)); else paramValues.add(null);
			if (nameGeneration!=null ) paramValues.add(new String(nameGeneration)); else paramValues.add(null);
			if (billPeriod!=null ) paramValues.add(new String(billPeriod)); else paramValues.add(null);
			if (collectionIndicator!=null ) paramValues.add(new Integer(collectionIndicator)); else paramValues.add(null);
			if (collectionStatus!=null ) paramValues.add(new Integer(collectionStatus)); else paramValues.add(null);
			if (languageCode!=null ) paramValues.add(new Integer(languageCode)); else paramValues.add(null);
			if (billDispMeth!=null ) paramValues.add(new Integer(billDispMeth)); else paramValues.add(null);
			if (billFmtOpt!=null ) paramValues.add(new Integer(billFmtOpt)); else paramValues.add(null);
			if (insertGrpId!=null ) paramValues.add(new Integer(insertGrpId)); else paramValues.add(null);
			if (msgGrpId!=null ) paramValues.add(new Integer(msgGrpId)); else paramValues.add(null);
			if (mktCode!=null ) paramValues.add(new Integer(mktCode)); else paramValues.add(null);
			if (rateClass!=null ) paramValues.add(new Integer(rateClass)); else paramValues.add(null);
			if (regulatoryId!=null ) paramValues.add(new Integer(regulatoryId)); else paramValues.add(null);
			if (revRcvCostCtr!=null ) paramValues.add(new Integer(revRcvCostCtr)); else paramValues.add(null);
			if (franchiseTaxCode!=null ) paramValues.add(new Integer(franchiseTaxCode)); else paramValues.add(null);
			if (vertexGeoCode!=null ) paramValues.add(new String(vertexGeoCode)); else paramValues.add(null);
			if (vipCode!=null ) paramValues.add(new Integer(vipCode)); else paramValues.add(null);
			if (billingInquiryCenter!=null ) paramValues.add(new Integer(billingInquiryCenter)); else paramValues.add(null);
			if (serviceInquiryCenter!=null ) paramValues.add(new Integer(serviceInquiryCenter)); else paramValues.add(null);
			if (reemittanceCenter!=null ) paramValues.add(new Integer(reemittanceCenter)); else paramValues.add(null);
			if (returnMailCenter!=null ) paramValues.add(new Integer(returnMailCenter)); else paramValues.add(null);
			paramValues.add(C30SqlApiUtils.getExtendedDataList(extDatList, conn));

			log.info("Total Number of Update Account Input ParamValues : "+ paramValues.size() );
			
			log.info("START - Invoking UPDATE Account SQL-API for AccountId : "+acctId + " orderDesiredDate : " + actionDate);
			//Making data base Query
			dataSource.queryData(call, paramValues, 2);
			log.info("END - Invoking UPDATE Account SQL-API for AccountId : "+acctId + " orderDesiredDate : " + actionDate);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30WPSQLConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30WPSQLConstants.SQL_ERROR_CODE_LENGTH+1);
			log.error(e.getMessage());
			throw new C30SDKToolkitException(e.getMessage(), e);
		}

	}
	
	public C30JDBCDataSource getDataSource()
	{
		return dataSource;
	}
	
	public void setDataSource(C30JDBCDataSource dataSource)
	{
		this.dataSource = dataSource;
	}

}
