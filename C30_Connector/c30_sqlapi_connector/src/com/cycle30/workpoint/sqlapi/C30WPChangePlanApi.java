
package com.cycle30.workpoint.sqlapi;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;

/** This script works on the Change Plan API
 * 
 * @author Umesh
 * - Initial Version
 * - Added the TransactionId Features
 * - Modified to align with the SQL-API created for wholesale project 
 *
 */

public class C30WPChangePlanApi {
	
	private static Logger log = Logger.getLogger(C30WPChangePlanApi.class);
	
	private C30JDBCDataSource dataSource=null;	

	public C30WPChangePlanApi()
	{
		log.info("Default constructor .....................C30WPChangePlanApi");
	}

	@SuppressWarnings("unchecked")
	public C30WPChangePlanApi( Connection conn,
			String transactionId,
			String acctSegId,
			String oldinstanceId,
			String catalogId,
			String newinstanceId,
			String actionDate,
			String actionWho) throws C30SDKToolkitException
	{
		try
		{
			dataSource = new C30JDBCDataSource(conn);

			C30ExternalCallDef call = new C30ExternalCallDef(C30WPSQLConstants.C30_CHANGE_PLAN_API_SQL_NAME);

			call.addParam(new C30CustomParamDef("v_transaction_id",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_old_instance_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_catalog_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_new_instance_id",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_action_date",13,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_who",2,  2000, 1));
			call.addParam(new C30CustomParamDef("c30changePlan_cv",10,  2000, 2));
			
			log.info("Total Number of Change Plan Input parameters : "+ call.getInputParams().size() );

			//Setting up the value

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------

			if (transactionId!=null ) paramValues.add(new String(transactionId)); else paramValues.add(null);			
			if (acctSegId!=null ) paramValues.add(new Integer(acctSegId)); else paramValues.add(null);
			if (oldinstanceId!=null ) paramValues.add(new String(oldinstanceId)); else paramValues.add(null);
			if (catalogId!=null ) paramValues.add(new String(catalogId)); else paramValues.add(null);			
			if (newinstanceId!=null ) paramValues.add(new String(newinstanceId)); else paramValues.add(null);
			if (actionDate!=null ) paramValues.add(C30SqlApiUtils.getDateFromString(actionDate)); else paramValues.add(new Date());
			if (actionWho!=null ) paramValues.add(new String(actionWho)); else paramValues.add(null);
			
			log.info("Total Number of Change Plan Input ParamValues : "+ paramValues.size());
			
			log.info("START - Invoking CHANGE Plan SQL-API for Old InstanceId : "+oldinstanceId + " New InstanceId : "+newinstanceId +
					"catalogId : "+catalogId +"actionDate : "+ actionDate);
			//Making data base Query
			dataSource.queryData(call, paramValues, 2);
			log.info("END - Invoking CHANGE Plan SQL-API for Old InstanceId : "+oldinstanceId + " New InstanceId : "+newinstanceId +
					"catalogId : "+catalogId +"actionDate : "+ actionDate);

		} // End - try
		catch(Exception e)
		{
			e.printStackTrace();
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30WPSQLConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30WPSQLConstants.SQL_ERROR_CODE_LENGTH+1);
			log.error(e.getMessage());
			throw new C30SDKToolkitException(e.getMessage(), e);
		}// End - catch(Exception e)

	} //End - C30WPDiscPlanApi
	
	public C30JDBCDataSource getDataSource()
	{
		return dataSource;
	}
	
	public void setDataSource(C30JDBCDataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
} // End - Main C30WPDiscPlanApi
