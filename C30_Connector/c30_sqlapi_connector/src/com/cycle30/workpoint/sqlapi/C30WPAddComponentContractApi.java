package com.cycle30.workpoint.sqlapi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;

public class C30WPAddComponentContractApi {
	
	private static Logger log = Logger.getLogger(C30WPAddComponentContractApi.class);

	@SuppressWarnings("unchecked")
	public C30WPAddComponentContractApi(Connection conn,  Integer componentId,
	 Integer compInstId,
	 Integer compInstIdServ,
	 Integer packageId,
	 Integer packageInstId,
	 Integer packageInstIdServ,
	 Integer subscrNo,
	 Integer subscrNoResets,
	 Integer contractType,
	 Date actionDate,
	 Integer waiveNrc,
	 String actionWho) throws C30SDKToolkitException
	{
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			C30JDBCDataSource dataSource = new C30JDBCDataSource(conn);

			C30ExternalCallDef call = new C30ExternalCallDef(C30WPSQLConstants.C30_ADD_COMP_CTR_API_SQL_NAME);

			call.addParam(new C30CustomParamDef("v_component_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_comp_inst_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_comp_inst_id_serv",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_package_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_package_inst_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_package_inst_id_serv",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_subscr_no",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_subscr_no_resets",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_contract_type",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_date",3,  2000, 1));
			call.addParam(new C30CustomParamDef("v_waive_nrc",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_who",2,  2000, 1));

			call.addParam(new C30CustomParamDef("v_view_id",2,  2000, 2));

			log.info("Total Number of Input parameters : "+ call.getInputParams().size() );

			//Setting up the value

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------

			if (componentId!=null ) paramValues.add(new Integer(componentId)); else paramValues.add(null);
			if (compInstId!=null ) paramValues.add(new Integer(compInstId)); else paramValues.add(null);
			if (compInstIdServ!=null ) paramValues.add(new Integer(compInstIdServ)); else paramValues.add(null);
			if (packageId!=null ) paramValues.add(new Integer(packageId)); else paramValues.add(null);
			if (packageInstId!=null ) paramValues.add(new Integer(packageInstId)); else paramValues.add(null);
			if (packageInstIdServ!=null ) paramValues.add(new Integer(packageInstIdServ)); else paramValues.add(null);
			if (subscrNo!=null ) paramValues.add(new Integer(subscrNo)); else paramValues.add(null);
			if (subscrNoResets!=null ) paramValues.add(new Integer(subscrNoResets)); else paramValues.add(null);
			if (contractType!=null ) paramValues.add(new Integer(contractType)); else paramValues.add(null);
			if (actionDate!=null ) paramValues.add(new Date()); else paramValues.add(null);
			if (waiveNrc!=null ) paramValues.add(new Integer(waiveNrc)); else paramValues.add(null);
			if (actionWho!=null ) paramValues.add(new String(actionWho)); else paramValues.add(null);
			//if (viewId!=null ) paramValues.add(new String(viewId)); else paramValues.add(null);

			log.info("Total Number of Input ParamValues : "+ paramValues.size() );


			//Making data base Query
			dataSource.queryData(call, paramValues, 2);


		}
		catch(Exception e)
		{
			e.printStackTrace();
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30WPSQLConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30WPSQLConstants.SQL_ERROR_CODE_LENGTH+1);
			log.error(e.getMessage());
			throw new C30SDKToolkitException(e.getMessage(), e, errCode);
		}

	}
}
