package com.cycle30.workpoint.sqlapi.utils;

/** This script defines the SQL Constants used
 * 
 * Umesh - Added the Constants for Add Plan, Disconnect Plan,Add Feature, Disconnect Feature,
 *		   Add Identifer, Disconnect Identifer, Swap Plan
 *		 - Order Complete Date query added
 *		 - Removed Force References and replaced with Client
 *		 - Added the constants for Add Service, Disconnect Service, Update Service
 *
 */

public class C30WPSQLConstants {

	public static final int SQL_ERROR_CODE_LENGTH = 13;
	
	//ORDER ActionWho
	public static final String C30_ORDER_ACTION_WHO_TYPE_LCF_BATCH="LCF Batch";
	public static final String C30_ORDER_REQ_SYSTEM_ID="GCIPP";
	
	//ORDER Actions
	public static final String C30_ORDER_ACTION_TYPE_ADD="ADD";
	public static final String C30_ORDER_ACTION_TYPE_DISC="DISCONNECT";
	public static final String C30_ORDER_ACTION_TYPE_CHANGE="CHANGE";
	public static final String C30_ORDER_ACTION_TYPE_SWAP="SWAP";
	public static final String C30_ORDER_ACTION_TYPE_SUSPEND="SUSPEND";
	public static final String C30_ORDER_ACTION_TYPE_RESUME="RESUME";
	
	//Client Action Type Reason
	public static final String C30_ORDER_ACTION_REASON_TO_POSTPAID="Change to GSM Postpaid";
	public static final String C30_ORDER_ACTION_REASON_TO_LANDLINE="Change to GCI Landline";
	public static final String C30_ORDER_ACTION_REASON_TO_AGING="Disconnect to Aging";
	public static final String C30_ORDER_ACTION_REASON_TO_AVAILABLE="Disconnect to Available";
	public static final String C30_ORDER_ACTION_REASON_CODE_500="500";
	public static final String C30_ORDER_ACTION_REASON_CODE_7="7";
	public static final String C30_ORDER_ACTION_REASON_CODE_6="6";	
	
	//INVENTORY Actions
	public static final String C30_INV_STATUS_TYPE_ASSIGNED="Assigned";
	public static final String C30_INV_STATUS_TYPE_ASSIGNED_NORMAL="Assigned - Normal";	
	public static final String C30_INV_STATUS_TYPE_AVAILABLE="Available";
	public static final String C30_INV_STATUS_TYPE_AGING="Aging";	
	public static final String C30_INV_STATUS_TYPE_UPGRADE="Upgrade Pending Assigned";	
	public static final String C30_INV_ACTION_TYPE_UPDATE="UPDATE";	
    public static final String C30_INV_ATTRIB_ACTION_TYPE="ActionType";
    public static final String C30_INV_ATTRIB_PAYLOAD="Payload";
    public static final String C30_INV_STATUS_TYPE_PORTOUT="Ported Out";    

	//INVENTORY Sales Channel
	public static final String C30_INV_SALES_CHANNEL_GSM_PREPAID="404";
	public static final String C30_INV_SALES_CHANNEL_PORTED_OUT="48";	
    
	public static final String C30_EXT_DATA_SEND_FLAG_TRUE = "TRUE";
	public static final String C30_EXT_DATA_SEND_FLAG_FALSE = "FALSE";
	
	public static final String C30_ADD_ACCOUNT_API_SQL_NAME="c30addAccount";
	public static final String C30_ADD_PLAN_API_SQL_NAME="c30addPlan";
	public static final String C30_CHANGE_PLAN_API_SQL_NAME="c30changePlan";
	public static final String C30_CHANGE_PLAN_HOST_API_SQL_NAME="c30changePlanHost";
	public static final String C30_ADD_PLAN_MEMBER_API_SQL_NAME="c30addPlanMember";
	public static final String C30_ADD_SERVICE_API_SQL_NAME="c30addService";
	public static final String C30_UPDATE_SERVICE_API_SQL_NAME="c30updateService";
	public static final String C30_DISC_SERVICE_API_SQL_NAME="c30discService";
	public static final String C30_SUSPEND_SERVICE_API_SQL_NAME="c30suspendService";
	public static final String C30_RESUME_SERVICE_API_SQL_NAME="c30resumeService";
	public static final String C30_ADD_COMP_CTR_API_SQL_NAME="c30addCompContMem";
	public static final String C30_ADD_COMP_PROD_API_SQL_NAME="c30addCompProdMem";
	public static final String C30_DISC_PLAN_API_SQL_NAME="c30discPlan";
	public static final String C30_ADD_COMP_API_SQL_NAME="c30addComponent";	
	public static final String C30_DISC_COMP_API_SQL_NAME="c30discComponent";	
	public static final String C30_ADD_IDENTIFIER_API_SQL_NAME="c30addIdentifier";
	public static final String C30_DISC_IDENTIFIER_API_SQL_NAME="c30discIdentifier";
	public static final String C30_SWAP_IDENTIFIER_API_SQL_NAME="c30swapIdentifier";	
	public static final String C30_SWAP_PLAN_API_SQL_NAME="c30swapPlan";	
	public static final String C30_UPDATE_ACCOUNT_API_SQL_NAME="c30updateAccount";
	public static final String C30_UPDATE_PLAN_API_SQL_NAME="c30updatePlan";
	public static final String C30_GET_SERVICE_INFO="c30getServiceInfo";
	public static final String C30_GET_PLAN_INFO="c30getPlanInfo";	
	
	//Order
	public static final Integer ORD_STATUS_INITIATED = new Integer(0);
	public static final Integer ORD_STATUS_IN_PROGRESS = new Integer(10);
	public static final Integer ORD_STATUS_COMPLETED = new Integer(80);
	public static final Integer ORD_STATUS_CANCELLED = new Integer(90);
	public static final Integer ORD_STATUS_COMMITTED = new Integer(20);
	public static final Integer ORD_STATUS_IN_ERROR = new Integer(99);
	
	//Provisioning
	public static final String C30_ICCID="ICCID";
	public static final String C30_TELEPHONE_NUMBER="TelePhoneNumber";	
	public static final String C30_ICCID_VALUE="20008";
	public static final String C30_MSISDN="MSISDN";
	public static final String C30_MSISDN_VALUE="60";
	public static final String C30_IMSI="IMSI";
	public static final String C30_IMSI_VALUE="20015";
	public static final String C30_PHONE_NUMBER="PhoneNumber";
	public static final String C30_PROV_ATTRIB_PREPAID_SERVICE="PREPAID SERVICE";
	public static final String C30_PROV_ATTRIB_ACS_PREPAID_SERVICE="ACS PREPAID SERVICE";	
	public static final String C30_PROV_ATTRIB_PREPAID_SUSPEND="PREPAID SUSPEND";	
	public static final String C30_OCS_ATTRIB_PREPAID_SUSPEND="PREPAID_SUSPEND";	
	public static final String C30_PROV_ATTRIB_PREPAID_DATAONLY="PREPAID DATAONLY";
	public static final String C30_PROV_ATTRIB_ACS_PREPAID_DATAONLY="ACS PREPAID DATAONLY";	
	public static final String C30_PROV_ATTRIB_ICCID="ICCID";
	public static final String C30_PROV_ATTRIB_TELEPHONE_NUMBER="TELEPHONENUMBER";
	public static final String C30_PROV_ATTRIB_SERVICE = "SERVICE";
	public static final String C30_PROV_ATTRIB_INVENTORY = "INVENTORY";	
	public static final String C30_PROV_SPS_PREPAID_VALUE = "prepaid";	
	public static final String C30_PROV_SPS_PREPAID_TRUE = "TRUE";
	public static final String C30_PROV_SPS_RESPONSE_TELEPHONE_NO = "PortingPoolTN";	
	public static final String C30_PROV_SPS_TEMP_TELEPHONE_NO = "SPSTemporaryTN";	
	public static final String C30_PREPAID_DATA_ONLY = "_DO_";	
	public static final String C30_ACS_PREPAID_VOICE_VVM = "VVM_";
	public static final String C30_ACS_PREPAID_VOICE_VVM_TRUE = "TRUE";
	//public static final String C30_PROV_ATTRIB_ACS_VISUAL_VOICE_MAIL_CHANGE="ACS PREPAID VOICE MAIL CHANGE";
	
	
	
	//OCS
	public static final String PREPAID_CPNI_PIN="CPNI_PIN__c";
	public static final String PREPAID_PLAN_PROFILE="Subscriber_Default_Profile__c";
	public static final String PREPAID_AUTOPAY_FLAG="AutoPay__c";
	public static final String PREPAID_PLAN_RATE="Plan_Price__c";
	public static final String PREPAID_PLAN_EXPIRATION_DATE="Plan_Expiry_Date__c";
	public static final String PREPAID_PLAN_PRORATE_AMOUNT="Plan_Proration_Amount__c";	
	public static final String PREPAID_COMPONENET_RATE = "Component_Price__c";
	public static final String PREPAID_COMPONENET_EXPIRY_DATE = "Component_Expiry_Date__c";	
	public static final String PREPAID_COMPONENET_TARGET_SYSTEM = "Component_Target_System__c";	
	public static final String PREPAID_NOTIFICATION_PREFERENCE = "Notification_Preference__c";	
	public static final String PREPAID_BALANCE_VOLUME = "OCS_Balance_Volume__c";
	public static final String PREPAID_FORCE_ATTRIB_CHARGE_EVENT = "OCS_Charge_Event__c";
	public static final String PREPAID_OCS_SUBSCRIBER_LINE_ID = "Service_ClientId__c";
	public static final String PREPAID_METERED_PLAN = "_MT_";
	
	public static final String PREPAID_COMPONENET_TARGET_SYSTEM_OCS = "OCS";
	public static final String PREPAID_COMPONENET_TARGET_SYSTEM_SPS = "SPS";
	public static final String PREPAID_COMPONENET_TARGET_SYSTEM_OCS_SPS = "OCS_SPS";
	
	public static final String PREPAID_PORT_IN="PORT_IN";
	public static final String PREPAID_PORT_OUT="PORT_OUT";	
	public static final String PREPAID_PORT_OUT1="PORT-OUT";
	public static final String PREPAID_PORT_OUT2="PORTOUT";	
	public static final String PREPAID_AUTOPAY_VALUE="AUTOPAY_ON";
	public static final String PREPAID_SUBSCRIBER_PROFILE_LABEL="SubscriberProfileLabel";
	public static final String PREPAID_CHARGE_EVENT_LABEL="ChargeableEventLabel";
	public static final String PREPAID_CHARGE_EVENT_BALANCE_TYPE_LABEL="EventBalanceTypeLabel";	
	public static final String PREPAID_BALANCE_TYPE_LABEL="BalanceTypeLabel";
	public static final String PREPAID_CLI="CLI";
	public static final String PREPAID_SUBSCRIBER_CLI="SubscriberCLI";
	public static final String PREPAID_NUMBER_SUB_TYPE="NumberSubType";
	public static final String PREPAID_NUMBER_SUB_TYPE_MOBILE="MOBILE";
	public static final String PREPAID_PREFERRED_LANG="PreferredLang";
	public static final String PREPAID_PREFERRED_LANG_EN="en";
	public static final String PREPAID_SUBSCRIBER_ID="SubscriberId";
	public static final String PREPAID_SUBSCRIBER_LINE_ID="SubscriberLineId";
	public static final String PREPAID_REASON="Reason";
	public static final String PREPAID_IPTRUNK_NAME="IpTrunk";
	public static final String PREPAID_VALUE="Value";
	public static final String PREPAID_EXPIRATIONDATE = "ExpirationDate";
	public static final String PREPAID_BALANCE_EXPIRATION_DATE = "BalanceExpirationDate";	
	public static final String PREPAID_BALANCE_WARNING_SMS = "SendBalanceWarningsSms";
	public static final String PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS = "SendSubscriberExpiryWarningSms";	
	public static final String PREPAID_VOUCHER_PINCODE = "Pincode";
	public static final String PREPAID_OCS_NULL_VALUE = "<ex:nil/>";
	public static final String PREPAID_OCS_SUBSCRIBER_STATE_LABEL = "SubscriberStateLabel";	
	public static final String PREPAID_OCS_SUBSCRIBER_STATE_LABEL_VALUE = "ACTIVE";	
	public static final String PREPAID_PORT_IN_SUBSCRIBER_LINE_ID_KEY="PortInSubscriberLineId";

	
	//OCS DATA Constants
	public static final String PREPAID_BALANCE_TYPE_LABEL_COMMON="COMMON";
	public static final String PREPAID_BALANCE_REASON_WALLET_REFILL="WALLET_REFILL_";
	public static final String PREPAID_IPTRUNK_VALUE="192.168.1.2";
	public static final String PREPAID_ADD_SERVICE_REASON="ADD_BALANCE";
	public static final String PREPAID_DEFAULT_PROFILE="PLAN_NOT_SELECTED";
	public static final String GCI_PREPAID_DEFAULT_PROFILE="GCI_PLAN_NOT_SELECTED";	
	public static final String ACS_PREPAID_DEFAULT_PROFILE="ACS_PLAN_NOT_SELECTED";	
	public static final String PREPAID_ADJ_EXPIRY_DATE_REASON="ADJ_PLAN_EXPIRATION";	
	public static final String PREPAID_NOTIFICATION_PREFERENCE_SMS="SMS";
	public static final String PREPAID_NOTIFICATION_PREFERENCE_BOTH="BOTH";	
	public static final String PREPAID_OCS_TRUE_VALUE="1";
	public static final String PREPAID_OCS_FALSE_VALUE="0";	
	public static final String PREPAID_OCS_CLI_CHECK_FAULT_CODE="OCS-159";
	public static final String PREPAID_BALANCE_WALLET_REFILL="WALLET_REFILL";
	public static final String PREPAID_BALANCE_WALLETREFILL="WALLETREFILL";	
	public static final String PREPAID_BALANCE_BALANCE_REFILL="BALANCE_REFILL";	
	public static final String PREPAID_BALANCE_BALANCEREFILL="BALANCEREFILL";	
	public static final String PREPAID_BALANCE_VOUCHER_REDEEM="VOUCHER_REDEEM";	
	public static final String PREPAID_BALANCE_VOUCHERREDEEM="VOUCHERREDEEM";	
	

	
	
	//OCS Response
	public static final String OCS_XML_INT_NAME_TAG="int";
	public static final String OCS_RESPONSE_SUBSCRIBER_KEY="OCSSubscriberId";
	public static final String OCS_RESPONSE_SUBSCRIBER_LINE_ID_KEY="OCSSubscriberLineId";

	
	//Segmentation
	public static final String C30_SEGMENT_ACS="3";
	public static final String C30_SEGMENT_AWN="2";
	public static final String C30_SEGMENT_GCI_PREPAID="4";
	public static final String C30_SEGMENT_ACS_PREPAID="5";	
	
	public static final String CHANGE_ORDER_STATUS =                    
		"UPDATE C30_SDK_ORDER " +
		"SET IS_LOCKED=0, ORDER_STATUS_ID= ? " +
		"WHERE CLIENT_ORDER_ID = ? AND TRANSACTION_ID = ?";
	
	public static final String CHANGE_SUB_ORDER_STATUS =                    
		"UPDATE C30_SDK_SUB_ORDER " +
		"SET SUB_ORDER_STATUS_ID= ?, IS_CANCELLABLE=?, IS_RESUBMITTABLE=? " +
		"WHERE CLIENT_SUB_ORDER_ID = ? AND TRANSACTION_ID = ?";
	
	public static final String CHANGE_SUB_ORDER_STATUS_COMPLETE =                    
			"UPDATE C30_SDK_SUB_ORDER " +
			"SET SUB_ORDER_COMPLETE_DATE=SYSDATE, SUB_ORDER_STATUS_ID= ?, IS_CANCELLABLE=?, IS_RESUBMITTABLE=? " +
			"WHERE CLIENT_SUB_ORDER_ID = ? AND TRANSACTION_ID = ?";

	public static final String CHANGE_ORDER_STATUS_COMPLETE =                    
			"UPDATE C30_SDK_ORDER " +
			"SET ORDER_COMPLETE_DATE=SYSDATE, ORDER_STATUS_ID= ? , IS_CANCELLABLE=?, IS_RESUBMITTABLE=? " +
			"WHERE CLIENT_ORDER_ID = ? AND TRANSACTION_ID = ?";

	public static final String INSERT_INTO_SDK_TRANSACTION_LOG = 
		" INSERT INTO C30_SDK_ORDER_TRANS_LOG (CLIENT_ORDER_ID,CLIENT_SUB_ORDER_ID , LOG_DATE,LOG_LEVEL,LOG_MESSAGE,TRANSACTION_ID)" +
		" VALUES (?,?, SYSDATE,?,?,?) ";
	
	public static final String INSERT_INTO_SUBORDER_EXTENDED_DATA = 
			" INSERT INTO C30_SDK_SUB_ORDER_EXT_DATA (CLIENT_SUB_ORDER_ID, TRANSACTION_ID, ACCT_SEG_ID , KEY, VALUE, SEND_FLAG)" +
					" VALUES (?,?,?,?,?,?) ";

	public static final String FETCH_SUBORDER_EXTENDED_DATA = "SELECT KEY, VALUE " +
			" FROM C30_SDK_SUB_ORDER_EXT_DATA " +
			"WHERE CLIENT_SUB_ORDER_ID = ? AND TRANSACTION_ID = ? AND KEY = ? "; 

	public static final String FETCH_C30_BIL_PARAMETERS = "SELECT VALUE " +
			" FROM TEMP_PARAMETERS " +
			"WHERE MODULE = ? AND PARAMETER_NAME = ? "; 
	
	public static final String GET_ACCT_SEG_ID_WEBLOGIC_DATASOURCE_NAME = "SELECT ACCT_SEG_ID, C30_API_LOGIN_ID, C30_WEBLOGIC_DATASOURCE " +
			" FROM C30_SDK_ACCT_SEG_CONFIG"; 


}
