package com.cycle30.workpoint.sqlapi.utils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import com.cycle30.sdk.toolkit.C30PropertyFileManager;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.acctseg.C30SDKAcctSegWLDataSourceMap;
import com.cycle30.workpoint.acctseg.C30SDKAcctSegWLDataSourceMapImpl;

public class C30WPSqlApiConnector {
	// The one-and-only instance of the C30WPConnection.
	private static C30WPSqlApiConnector instance = null;

	//Logs all the information
	private static Logger log = Logger.getLogger(C30WPSqlApiConnector.class);

	//Connection variable
	Connection genericGlobalConnection = null;
	static HashMap acctSegmentConnections= null;

	static C30PropertyFileManager toolkitProperties = null;
	/**
	 * Returns the one-and-only instance of the C30WPSqlApiConnector.
	 *
	 *@return    The one-and-only instance of the C30WPSqlApiConnector.
	 * @throws C30SDKToolkitException 
	 */
	public static C30WPSqlApiConnector getInstance() throws C30SDKToolkitException {
		if (instance == null) 
		{
			if( log != null ) 
			{
				log.debug("Creating new instance of C30WPSqlApiConnector"); 
			}
			toolkitProperties = C30PropertyFileManager.getInstance();
			instance = new C30WPSqlApiConnector(toolkitProperties.getProperty("C30_SQLAPI_DATASOURCE"));
			acctSegmentConnections   = loadAcctSegmentConnections();
			
		}

		return instance;
	}

	/** This method returns connections to all the acct segments
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private static HashMap loadAcctSegmentConnections() throws C30SDKToolkitException {
		HashMap retMap = new HashMap();
		
		C30SDKAcctSegWLDataSourceMapImpl acctSegDataSourceListImpl = C30SDKAcctSegWLDataSourceMapImpl.getInstance();
		ArrayList<C30SDKAcctSegWLDataSourceMap> acctSegDataSourceList =  acctSegDataSourceListImpl.getAcctSegToDataSourceList();
		
		for (C30SDKAcctSegWLDataSourceMap acctSegDSMap : acctSegDataSourceList)
		{
			Integer acctSegId = acctSegDSMap.getAcctSegid();
			log.info("Working with account Segment Id : " + acctSegId);
			String dataSourceName  = acctSegDSMap.getWeblogicDataSource();
			retMap.put(acctSegId, getConnection(dataSourceName));
		}
		return retMap;
		
		
	}

	/**
	 * 
	 * @throws C30SDKToolkitException
	 */
	private C30WPSqlApiConnector(String dataSource) throws C30SDKToolkitException
	{
		genericGlobalConnection = getConnection(dataSource);
	}
	/** Retrieves the Connection from the Weblogic datasource for given datasource name
	 * 
	 * @throws C30SDKToolkitException
	 */
	private static Connection getConnection(String dataSource) throws C30SDKToolkitException
	{
		// Read the property file C30_WP Config.
		log.debug("Getting the datasource for : " +dataSource);
		Connection conn = null;
		try{
			
			Context ctx = null;
			Hashtable ht = new Hashtable();
			ht.put(Context.INITIAL_CONTEXT_FACTORY,toolkitProperties.getProperty(Context.INITIAL_CONTEXT_FACTORY));
			ht.put(Context.PROVIDER_URL,toolkitProperties.getProperty(Context.PROVIDER_URL));
			ht.put(Context.SECURITY_CREDENTIALS,toolkitProperties.getProperty(Context.SECURITY_CREDENTIALS));
			ht.put(Context.SECURITY_PRINCIPAL,toolkitProperties.getProperty(Context.SECURITY_PRINCIPAL));

			ctx = new InitialContext(ht);
			javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup (dataSource);
			
			conn = ds.getConnection();


		}
		catch (java.util.MissingResourceException ex) 
		{
			throw new C30SDKToolkitException("An Error occured while loading the property files. No resource found.", ex, "WP-CONN-001");			
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
			throw new C30SDKToolkitException("General Exception while making connections to billingDataSource.", ex, "WP-CONN-001");			
		} 
		return conn;
	}
	
	/** Returns the Generic Global Connection in case of no connection available.
	 * 
	 * @return
	 */
	public Connection getConnection()
	{
		return genericGlobalConnection;
	}
	
	/** This return the Connection for the given Acct Segment
	 * 
	 * @param acctSegId
	 * @return
	 */
	public Connection getConnection(Integer acctSegId)
	{
		return (Connection) acctSegmentConnections.get(acctSegId);
	}
}
