
package com.cycle30.workpoint.sqlapi;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;

/** This script works on the Add Service API
 * 
 * @author Umesh
 *  - Initial Version
 *  
 */

public class C30WPAddServiceApi {
	
	private static Logger log = Logger.getLogger(C30WPAddServiceApi.class);
	
	private C30JDBCDataSource dataSource=null;	

	public C30WPAddServiceApi()
	{
		log.info("Default constructor .....................C30WPAddServiceApi");
	}

	@SuppressWarnings("unchecked")
	public C30WPAddServiceApi( Connection conn,
			String transactionId,
			String acctSegId,
			String serviceId,
			String actionDate,
			String actionWho,
			String acctId,
			String emfConfigId,
			String rateClass,
			String revRcvCostCtr,
			String currencyCode,
			String timeZone,
			String franchiseTaxCode,
			String vertexGeoCode,
			String houseNumber,
			String houseNumberSuffix,
			String prefixDirectional,
			String streetName,
			String streetSuffix,
			String postfixDirectional,			
			String unitType,
			String unitNo,			
			String city,
			String county,
			String state,			
			String postalCode,
			String extendedPostalCode,
			String countryCode,
			String connectReason,
			C30ExtendedDataList extDatList
			) throws C30SDKToolkitException
	{
		log.info("In C30WPAddServiceApi class...");
		try
		{
			dataSource = new C30JDBCDataSource(conn);

			C30ExternalCallDef call = new C30ExternalCallDef(C30WPSQLConstants.C30_ADD_SERVICE_API_SQL_NAME);

			call.addParam(new C30CustomParamDef("v_transaction_id",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_service_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_date",13,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_who",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_acct_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_emf_config_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_rate_class",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_rev_rcv_cost_ctr",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_currency_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_timezone",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_franchise_tax_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_vertex_geocode",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_house_number",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_house_number_suffix",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_prefix_directional",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_street_name",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_street_suffix",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_postfix_directional",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_unit_type",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_unit_no",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_city",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_county",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_state",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_postal_code",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_extended_postal_code",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_country_code",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_connect_reason",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_ext_data",10,  2000, 1));
			call.addParam(new C30CustomParamDef("c30addService_cv",10,  2000, 2));	

			log.info("Total Number of Add Service Input parameters : "+ call.getInputParams().size() );

			//Setting up the value

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------
			if (transactionId!=null ) paramValues.add(new String(transactionId)); else paramValues.add(null);
			if (acctSegId!=null ) paramValues.add(new Integer(acctSegId)); else paramValues.add(null);
			if (serviceId!=null ) paramValues.add(new String(serviceId)); else paramValues.add(null);
			if (actionDate!=null ) paramValues.add(C30SqlApiUtils.getDateFromString(actionDate)); else paramValues.add(new Date());
			if (actionWho!=null ) paramValues.add(new String(actionWho)); else paramValues.add(null);
			if (acctId!=null ) paramValues.add(new String(acctId)); else paramValues.add(null);
			if (emfConfigId!=null ) paramValues.add(new String(emfConfigId)); else paramValues.add(null);
			if (rateClass!=null ) paramValues.add(new Integer(rateClass)); else paramValues.add(null);
			if (revRcvCostCtr!=null ) paramValues.add(new Integer(revRcvCostCtr)); else paramValues.add(null);
			if (currencyCode!=null ) paramValues.add(new Integer(currencyCode)); else paramValues.add(null);
			if (timeZone!=null ) paramValues.add(new Integer(timeZone)); else paramValues.add(null);
			if (franchiseTaxCode!=null ) paramValues.add(new Integer(franchiseTaxCode)); else paramValues.add(null);
			if (vertexGeoCode!=null ) paramValues.add(new String(vertexGeoCode)); else paramValues.add(null);			
			if (houseNumber!=null ) paramValues.add(new String(houseNumber)); else paramValues.add(null);
			if (houseNumberSuffix!=null ) paramValues.add(new String(houseNumberSuffix)); else paramValues.add(null);
			if (prefixDirectional!=null ) paramValues.add(new String(prefixDirectional)); else paramValues.add(null);
			if (streetName!=null ) paramValues.add(new String(streetName)); else paramValues.add(null);
			if (streetSuffix!=null ) paramValues.add(new String(streetSuffix)); else paramValues.add(null);
			if (postfixDirectional!=null ) paramValues.add(new String(postfixDirectional)); else paramValues.add(null);
			if (unitType!=null ) paramValues.add(new String(unitType)); else paramValues.add(null);
			if (unitNo!=null ) paramValues.add(new String(unitNo)); else paramValues.add(null);
			if (city!=null ) paramValues.add(new String(city)); else paramValues.add(null);
			if (county!=null ) paramValues.add(new String(county)); else paramValues.add(null);
			if (state!=null ) paramValues.add(new String(state)); else paramValues.add(null);
			if (postalCode!=null ) paramValues.add(new String(postalCode)); else paramValues.add(null);
			if (extendedPostalCode!=null ) paramValues.add(new String(extendedPostalCode)); else paramValues.add(null);			
			if (countryCode!=null ) paramValues.add(new Integer(countryCode)); else paramValues.add(null);
			if (connectReason!=null) paramValues.add(new String(connectReason)); else paramValues.add(null);
			paramValues.add(C30SqlApiUtils.getExtendedDataList(extDatList, conn));
			
			log.info("Total Number of Add Service Input ParamValues : "+ paramValues.size() );

			log.info("START - Invoking ADD Service SQL-API for emfConfigId : "+emfConfigId + "serviceId : "+serviceId+"actionDate : "+ actionDate);
			//Making data base Query
			dataSource.queryData(call, paramValues, 2);
			log.info("END - Invoking ADD Service SQL-API for emfConfigId : "+emfConfigId + "serviceId : "+serviceId+"actionDate : "+ actionDate);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30WPSQLConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30WPSQLConstants.SQL_ERROR_CODE_LENGTH+1);
			log.error(e.getMessage());
			throw new C30SDKToolkitException(e.getMessage(), e);
		}

	}

	public C30JDBCDataSource getDataSource()
	{
		return dataSource;
	}
	
	public void setDataSource(C30JDBCDataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
}
