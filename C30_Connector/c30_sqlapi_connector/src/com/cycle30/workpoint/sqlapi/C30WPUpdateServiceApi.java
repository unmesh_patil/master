
package com.cycle30.workpoint.sqlapi;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;

/** This script works on the Update Service API
 * 
 * @author Umesh
 *  - Initial Version
 *  
 */

public class C30WPUpdateServiceApi {
	
	private static Logger log = Logger.getLogger(C30WPUpdateServiceApi.class);
	
	private C30JDBCDataSource dataSource=null;	

	public C30WPUpdateServiceApi()
	{
		log.info("Default constructor .....................C30WPUpdateServiceApi");
	}

	@SuppressWarnings("unchecked")
	public C30WPUpdateServiceApi( Connection conn,
			String transactionId,
			String acctSegId,
			String serviceId,
			String actionDate,
			String actionWho,
			String rateClass,
			String revRcvCostCtr,
			C30ExtendedDataList extDatList
			) throws C30SDKToolkitException
	{
		log.info("In C30WPUpdateServiceApi class...");
		try
		{
			dataSource = new C30JDBCDataSource(conn);

			C30ExternalCallDef call = new C30ExternalCallDef(C30WPSQLConstants.C30_UPDATE_SERVICE_API_SQL_NAME);

			call.addParam(new C30CustomParamDef("v_transaction_id",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_service_id",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_date",13,  2000, 1));
			call.addParam(new C30CustomParamDef("v_action_who",2,  2000, 1));
			call.addParam(new C30CustomParamDef("v_rate_class",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_rev_rcv_cost_ctr",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_ext_data",10,  2000, 1));
			call.addParam(new C30CustomParamDef("c30updateService_cv",10,  2000, 2));	
			
			log.info("Total Number of Update Service Input parameters : "+ call.getInputParams().size() );

			//Setting up the value

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------
			if (transactionId!=null ) paramValues.add(new String(transactionId)); else paramValues.add(null);
			if (acctSegId!=null ) paramValues.add(new Integer(acctSegId)); else paramValues.add(null);
			if (serviceId!=null ) paramValues.add(new String(serviceId)); else paramValues.add(null);
			if (actionDate!=null ) paramValues.add(C30SqlApiUtils.getDateFromString(actionDate)); else paramValues.add(new Date());
			if (actionWho!=null ) paramValues.add(new String(actionWho)); else paramValues.add(null);
			if (rateClass!=null ) paramValues.add(new Integer(rateClass)); else paramValues.add(null);
			if (revRcvCostCtr!=null ) paramValues.add(new Integer(revRcvCostCtr)); else paramValues.add(null);
			paramValues.add(C30SqlApiUtils.getExtendedDataList(extDatList, conn));
			
			log.info("Total Number of Update Service Input ParamValues : "+ paramValues.size() );

			log.info("START - Invoking UPDATE Service SQL-API for Service Id : "+serviceId + " orderDesiredDate : " + actionDate);
			//Making data base Query
			dataSource.queryData(call, paramValues, 2);
			log.info("END - Invoking UPDATE Service SQL-API for Service Id : "+serviceId + " orderDesiredDate : " + actionDate);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30WPSQLConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30WPSQLConstants.SQL_ERROR_CODE_LENGTH+1);
			log.error(e.getMessage());
			throw new C30SDKToolkitException(e.getMessage(), e);
		}

	}

	public C30JDBCDataSource getDataSource()
	{
		return dataSource;
	}
	
	public void setDataSource(C30JDBCDataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
}
