
package com.cycle30.workpoint.sqlapi;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Struct;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/** This script works on the Get Service Info API
 * 
 * @author Umesh
 *  - Initial Version
 *  
 */

public class C30WPGetServiceInfoApi {

	private static Logger log = Logger.getLogger(C30WPGetServiceInfoApi.class);

	private C30JDBCDataSource dataSource=null;	

	public C30WPGetServiceInfoApi()
	{
		log.info("Default constructor .....................C30WPGetServiceInfoApi");
	}

	@SuppressWarnings("unchecked")
	public C30WPGetServiceInfoApi( Connection conn,
			String transactionId,
			String acctSegId,
			String serviceId
			) throws C30SDKToolkitException
			{
		log.info("In C30WPGetServiceInfoApi class...");
		try
		{
			dataSource = new C30JDBCDataSource(conn);

			C30ExternalCallDef call = new C30ExternalCallDef(C30WPSQLConstants.C30_GET_SERVICE_INFO);
			//IN Parameter
			call.addParam(new C30CustomParamDef("v_transaction_id",2,  2000, 1));			
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1,  2000, 1));
			call.addParam(new C30CustomParamDef("v_service_id",2,  2000, 1));
			//call.addParam(new C30CustomParamDef("c30getServiceInfo_cv",10,  2000, 2));
			//OUT Parameter
			call.addParam(new C30CustomParamDef("v_service_id",2,  2000, 2));
			call.addParam(new C30CustomParamDef("d_subscr_no",1,  2000, 2));
			call.addParam(new C30CustomParamDef("d_subscr_no_resets",1,  2000, 2));
            call.addParam(new C30CustomParamDef("d_status_id",2, 2000, 2));			
			call.addParam(new C30CustomParamDef("d_status_descr",2,  2000, 2));
			call.addParam(new C30CustomParamDef("d_active_dt",3,  2000, 2));
			call.addParam(new C30CustomParamDef("d_inactive_dt",3,  2000, 2));
			call.addParam(new C30CustomParamDef("d_host_service_id",2,  2000, 2));
			call.addParam(new C30CustomParamDef("d_is_host",1,  2000, 2));
			call.addParam(new C30CustomParamDef("d_partner_count",1,  2000, 2));			
			call.addParam(new C30CustomParamDef("d_identifier_col",10,  5000, 2));
			call.addParam(new C30CustomParamDef("d_plan_col",10,  2000, 2));			
			call.addParam(new C30CustomParamDef("d_comp_col",10,  2000, 2));
            
			log.info("Total Number of Get Service Info Input parameters : "+ call.getInputParams().size() );

			//Setting up the value

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------
			if (transactionId!=null ) paramValues.add(new String(transactionId)); else paramValues.add(null);
			if (acctSegId!=null ) paramValues.add(new Integer(acctSegId)); else paramValues.add(null);
			if (serviceId!=null ) paramValues.add(new String(serviceId)); else paramValues.add(null);

			log.info("Total Number of Get Service Info Input ParamValues : "+ paramValues.size() );

			//Making data base Query
			dataSource.queryData(call, paramValues, 2);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30WPSQLConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30WPSQLConstants.SQL_ERROR_CODE_LENGTH+1);
			log.error(e.getMessage());
			throw new C30SDKToolkitException(e.getMessage(), e);
		}

	}

	public C30JDBCDataSource getDataSource()
	{
		return dataSource;
	}

	public void setDataSource(C30JDBCDataSource dataSource)
	{
		this.dataSource = dataSource;
	}

}
