package com.cycle30.workpoint.prov.api;

import java.util.HashMap;
import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/**
 * 
 * @author Umesh
 *
 */
public class ChangeServiceStatus {
	private static Logger log = Logger.getLogger(ChangeServiceStatus.class);

	public ChangeServiceStatus() {
		log.info("Default constructor .....................ChangeServiceStatus");
	}	

	/**This method is responsible for Sending ChangeServiceStatus Details to Provisioning system
	 * 
	 * @param TelephoneNumber
	 * @param ICCID
	 * @param actionType
	 * @param provRequestData
	 * 
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	public ChangeServiceStatus(String TelephoneNumber, String ICCID, String actionType, String actionReason,
			HashMap<String, String> provRequestData,
			String acctSegId, String subOrderId, String transactionId) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Sending Service Status Change to Provisioning System...");
		
		try{
				log.info("ServiceId: " +TelephoneNumber);	
				provRequestData.put(Constants.TELEPHONE_NUMBER, TelephoneNumber);
				if (ICCID != null) {
					provRequestData.put(Constants.ICCID, ICCID);
				}
				provRequestData.put(Constants.ATTR_CATALOG_ID, C30WPSQLConstants.C30_PROV_ATTRIB_SERVICE);				
				provRequestData.put(Constants.ATTR_CATALOG_ACTION_ID, actionType);

				log.info("ExternalTransactionId: " +provRequestData.get("transactionId")+" C30TransactionId: " +provRequestData.get("internalAuditId") +
						" TelephoneNumber: "+TelephoneNumber+" ICCID: "+ICCID);				

				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Service Status Change RequestId : "+response.get("RequestId") +" Service Status Change ResponseCode: "+ response.get("ResponseCode"));
				
				if (actionReason.equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_OUT)) {
					//Get Telephone Number
					String telePhoneNumber = C30ProvConnectorUtils.getXmlStringElementValue(response.get("ResponseXML").toString(), 
							C30WPSQLConstants.C30_PROV_SPS_RESPONSE_TELEPHONE_NO);

					if (telePhoneNumber != null) {
						//Store Temporary TN returned by SPS in Sub Order Extended Data in the database.
						C30OrderExtendedDataLog logOrderExtData = new C30OrderExtendedDataLog(acctSegId, subOrderId, transactionId);
						logOrderExtData.writeToSubOrderExtendedData(C30WPSQLConstants.C30_PROV_SPS_TEMP_TELEPHONE_NO, telePhoneNumber, 
								C30WPSQLConstants.C30_EXT_DATA_SEND_FLAG_FALSE);			
					} 
				}
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR-Service Status Change Provisioning Request : "+e.getMessage());
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Service Status Change Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
		    log.error("ERROR-Service Status Change Provisioning Request : "+e.getMessage());	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Service Status Change Provisioning Request: "+e.getMessage(), e );			
		}
		
	}
	
}
