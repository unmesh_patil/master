package com.cycle30.workpoint.prov.api;

import java.util.HashMap;
import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/**
 * 
 * @author Umesh
 *
 */
public class ChangeSIM {
	private static Logger log = Logger.getLogger(ChangeSIM.class);

	public ChangeSIM() {
		log.info("Default constructor .....................ChangeSIM");
	}	

	/**This method is responsible for ChangeSIM Details to Provisioning system
	 * 
	 * @param TelephoneNumber
	 * @param ICCID
	 * @param newICCID
	 * @param provRequestData
	 * 
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	public ChangeSIM(String TelephoneNumber, String ICCID, String newICCID,
			HashMap<String, String> provRequestData) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Sending Change SIM to Provisioning System...");
		
		try{
				provRequestData.put(Constants.TELEPHONE_NUMBER, TelephoneNumber);
				provRequestData.put(Constants.NEW_ICCID, newICCID);
				provRequestData.put(Constants.ICCID, ICCID);
				provRequestData.put(Constants.ATTR_CATALOG_ID, C30WPSQLConstants.C30_PROV_ATTRIB_ICCID);				
				provRequestData.put(Constants.ATTR_CATALOG_ACTION_ID, C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP);

				log.info("ExternalTransactionId: " +provRequestData.get("transactionId")+" C30TransactionId: " +provRequestData.get("internalAuditId") +
						" TelephoneNumber: "+TelephoneNumber+" ICCID: "+ICCID);				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Change SIM RequestId : "+response.get("RequestId") +" Change SIM ResponseCode: "+ response.get("ResponseCode"));
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Change SIM Provisioning Request : "+e.getMessage());
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Change SIM Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
		    log.error("ERROR - Change SIM Provisioning Request : "+e.getMessage());	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Change SIM Provisioning Request: "+e.getMessage(), e );			
		}
		
	}
	
}
