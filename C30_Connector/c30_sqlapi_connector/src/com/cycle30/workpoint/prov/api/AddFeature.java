package com.cycle30.workpoint.prov.api;

import java.util.HashMap;
import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/**
 * 
 * @author Umesh
 *
 */
public class AddFeature {
	private static Logger log = Logger.getLogger(AddFeature.class);

	public AddFeature()	{
		log.info("Default constructor .....................AddFeature");
	}	

	/**This method is responsible for Sending a Feature Details to Provisioning system
	 * 
	 * @param catalogId
	 * @param TelephoneNumber
	 * @param ICCID
	 * @param provRequestData
	 * 
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	public AddFeature(String catalogId, String TelephoneNumber, String ICCID,
			HashMap<String, String> provRequestData) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Sending ADD feature to Provisioning System...");
		
		try{
				log.info("ServiceId: " +TelephoneNumber);	
				provRequestData.put(Constants.TELEPHONE_NUMBER, TelephoneNumber);
				provRequestData.put(Constants.ICCID, ICCID);
				provRequestData.put(Constants.ATTR_CATALOG_ID, catalogId);				
				provRequestData.put(Constants.ATTR_CATALOG_ACTION_ID, C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD);

				log.info("ExternalTransactionId: " +provRequestData.get("transactionId")+" C30TransactionId: " +provRequestData.get("internalAuditId") +
						" TelephoneNumber: "+TelephoneNumber+" ICCID: "+ICCID);				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Feature ADD RequestId : "+response.get("RequestId") +" Feature ADD ResponseCode: "+ response.get("ResponseCode"));
				
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - ADD feature Provisioning Request : "+e.getMessage());
			throw new C30SDKToolkitException(
					"EXCP: ORDER-ADD feature Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
		    log.error("ERROR - ADD feature Provisioning Request : "+e.getMessage());	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-ADD feature Provisioning Request: "+e.getMessage(), e );			
		}
		
	}
	
}
