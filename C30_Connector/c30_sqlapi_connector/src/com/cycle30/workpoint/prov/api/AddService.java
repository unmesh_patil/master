package com.cycle30.workpoint.prov.api;

import java.util.HashMap;
import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/**
 * 
 * @author Umesh
 *
 */
public class AddService {
	private static Logger log = Logger.getLogger(AddService.class);

	public AddService()	{
		log.info("Default constructor .....................AddService");
	}	

	/**This method is responsible for Sending a ADD Service Details to Provisioning system
	 * 
	 * @param catalogId
	 * @param TelephoneNumber
	 * @param ICCID
	 * @param provRequestData
	 * 
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	public AddService(String catalogId, String TelephoneNumber, String ICCID,
			HashMap<String, String> provRequestData, String connectReason,
			String acctSegId, String subOrderId, String transactionId) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Sending ADD service to Provisioning System...");
		
		try{
				log.info("ServiceId: " +TelephoneNumber);	
				provRequestData.put(Constants.TELEPHONE_NUMBER, TelephoneNumber);
				provRequestData.put(Constants.ICCID, ICCID);
				provRequestData.put(Constants.ATTR_CATALOG_ID, catalogId);				
				provRequestData.put(Constants.ATTR_CATALOG_ACTION_ID, C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD);

				log.info("ExternalTransactionId: " +provRequestData.get("ExternalTransactionId")+" C30TransactionId: " +provRequestData.get("internalAuditId") +
						" TelephoneNumber: "+TelephoneNumber+" ICCID: "+ICCID);				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Service ADD RequestId : "+response.get("RequestId") +" Service ADD ResponseCode: "+ response.get("ResponseCode"));
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR-ADD Service Provisioning Request : "+e.getMessage());
			throw new C30SDKToolkitException(
					"EXCP: ORDER-ADD Service Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
		    log.error("ERROR - ADD Service Provisioning Request : "+e.getMessage());	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-ADD Service Provisioning Request: "+e.getMessage(), e );			
		}
		
	}
	
}
