package com.cycle30.workpoint.prov.api;

import java.util.HashMap;
import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
//import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/**
 * 
 * @author Umesh
 *
 */
public class ChangeTelephoneNumber {
	private static Logger log = Logger.getLogger(ChangeTelephoneNumber.class);

	public ChangeTelephoneNumber() {
		log.info("Default constructor .....................ChangeTelephoneNumber");
	}	

	/**This method is responsible for Changing Telephone Details in Provisioning system
	 * 
	 * @param ICCID
	 * @param TelephoneNumber
	 * @param NewTelephoneNumber
	 * @param provRequestData
	 * 
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	public ChangeTelephoneNumber(String ICCID, String TelephoneNumber, 
			String NewTelephoneNumber, HashMap<String, String> provRequestData) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Change Telephone Number in Provisioning System...");
		
		try{
		    	log.info("oldTelephoneNumber : " + TelephoneNumber + "newTelephoneNumber : " + NewTelephoneNumber);	
				//Prepare Data For Provisioning
				provRequestData.put(Constants.TELEPHONE_NUMBER, TelephoneNumber);
				provRequestData.put(Constants.NEW_TELEPHONE_NUMBER, NewTelephoneNumber);
				if (ICCID != null) {
					provRequestData.put(Constants.ICCID, ICCID);
				}
				provRequestData.put(Constants.ATTR_CATALOG_ID, C30WPSQLConstants.C30_PROV_ATTRIB_TELEPHONE_NUMBER);			
				provRequestData.put(Constants.ATTR_CATALOG_ACTION_ID, C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP);

				log.info("Send Telephonenumber "+C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP+" Request for " +
						"TelephoneNumber: "+ TelephoneNumber + " to Provisioning ");	
				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Change TN RequestId : "+response.get("RequestId") +" Change TN ResponseCode: "+ response.get("ResponseCode"));
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Telephone Number Change Provisioning Request : "+e);
			throw new C30SDKToolkitException("EXCP: ORDER-Telephone Number Change Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Telephone Number Change Provisioning Request: "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Telephone Number Change Order Request Process: "+e, e );			
		}
		
	}	
	
}
