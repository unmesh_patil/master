package com.cycle30.workpoint.prov.api;

import java.util.HashMap;
import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/**
 * 
 * @author Umesh
 *
 */
public class SendFeature {
	private static Logger log = Logger.getLogger(SendFeature.class);

	public SendFeature()	{
		log.info("Default constructor .....................SendFeature");
	}	

	/**This method is responsible for Sending a Feature Details to Provisioning system
	 * 
	 * @param catalogId
	 * @param TelephoneNumber
	 * @param ICCID
	 * @param provRequestData
	 * 
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	public SendFeature(String catalogId, String TelephoneNumber, String ICCID,
			HashMap<String, String> provRequestData, String compactionType) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Sending ADD feature to Provisioning System...");
		
		try{
				if (catalogId.equalsIgnoreCase(C30WPSQLConstants.C30_OCS_ATTRIB_PREPAID_SUSPEND)) {
					catalogId = C30WPSQLConstants.C30_PROV_ATTRIB_PREPAID_SUSPEND;
				}
				log.info("ServiceId: " +TelephoneNumber);	
				provRequestData.put(Constants.TELEPHONE_NUMBER, TelephoneNumber);
				provRequestData.put(Constants.ICCID, ICCID);
				provRequestData.put(Constants.ATTR_CATALOG_ID, catalogId);				
				provRequestData.put(Constants.ATTR_CATALOG_ACTION_ID, compactionType.toUpperCase());

				log.info("ExternalTransactionId: " +provRequestData.get("transactionId")+" C30TransactionId: " +provRequestData.get("internalAuditId") +
						" TelephoneNumber: "+TelephoneNumber+" ICCID: "+ICCID);				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Feature RequestId : "+response.get("RequestId") +" Feature ResponseCode: "+ response.get("ResponseCode"));
				
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR-Feature Provisioning Request : "+e.getMessage());
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Feature Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
		    log.error("ERROR-Feature Provisioning Request : "+e.getMessage());	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Feature Provisioning Request: "+e.getMessage(), e );			
		}
		
	}
	
}
