package com.cycle30.workpoint.prov.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import org.apache.log4j.Logger;

import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.C30Integer;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;

/**
 * 
 * @author Umesh
 *
 */
public class C30ProvisioningUtils {
	private static Logger log = Logger.getLogger(C30ProvisioningUtils.class);

	/**
	 * This method used to convert the given JAXBElement to String.
	 * @param elem
	 * @return
	 */
	public static String convertJaxbElementToString(JAXBElement elem)
	{
		String returnValue=null;
		try{
		    
		        // Check base and superclass name for 'C30Integer'.  Complex types like 
		        // ComponentId may have superclass name of C30Integer after JAXB class
		        // generation.
                        String elementClassName = elem.getValue().getClass().toString();
                        String superClassName  = elem.getValue().getClass().getSuperclass().toString();
			if (elementClassName.equalsIgnoreCase("class com.cycle30.sdk.schema.jaxb.C30Integer") ||
			    superClassName.equalsIgnoreCase("class com.cycle30.sdk.schema.jaxb.C30Integer"))
			{
				Integer ret = new Integer(((C30Integer)elem.getValue()).getValue());
				returnValue = ret.toString();
			}
			if ((elem.getValue()).getClass().toString().equalsIgnoreCase("class java.lang.String"))
			{
				returnValue = elem.getValue().toString();
			}

		}
		catch(Exception e)
		{
			returnValue=null;
		}


		return returnValue;
	}
	public static String convertJaxbElementToString(C30Integer elem)
	{
		String returnValue=null;
		try{

			Integer ret = new Integer(elem.getValue());
			returnValue = ret.toString();

		}
		catch(Exception e)
		{
			returnValue=null;
		}


		return returnValue;
	}
	public static String convertJaxbElementToString(String elem)
	{
		String returnValue=null;
		try{
			returnValue = elem.toString();
		}
		catch(Exception e)
		{
			returnValue=null;
		}


		return returnValue;
	}
	public static String convertJaxbElementToString(XMLGregorianCalendar elem)
	{
		String returnValue=null;
		try{
			returnValue = elem.toGregorianCalendar().getTime().toString();
			//returnValue = elem.toGregorianCalendar().toString();
		}
		catch(Exception e)
		{
			returnValue=null;
		}
		return returnValue;
	}

	public static Date getDateFromString(String dateStr) throws ParseException{

		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		Date a = (Date)formatter.parse(dateStr);  
		return a;
	}
	
	/** Return the extendedData as ByteArray
	 * 
	 * @param extDatList
	 * @param genericGlobalConnection
	 * @return
	 * @throws SQLException
	 */
	public static oracle.sql.ARRAY getExtendedDataList(C30ExtendedDataList extDatList, Connection conn) throws SQLException
	{
		StructDescriptor bookStructDesc =
			StructDescriptor.createDescriptor("C30ARBOR.C30NAMEVALUE", conn);
		ArrayDescriptor arrayDescriptor = ArrayDescriptor.createDescriptor("C30ARBOR.C30NAMEVALUEPAIRS", conn);
		// then obtain an Array filled with the content below
		Object[] bArray =null;
		if (extDatList != null)
		{
			bArray = new Object[extDatList.getExtendedData().size()];
			List<C30ExtendedData> extdtList = extDatList.getExtendedData();
			int i =0;
			for(C30ExtendedData extData : extdtList)
			{

				STRUCT m1 = new STRUCT(bookStructDesc, conn, new Object[]{extData.getExtendedDataName().toString(), extData.getExtendedDataValue().toString()});
				log.info("ExtendedDataValue: "+extData.getExtendedDataValue().toString());
				log.info("ExtendedDataName: "+extData.getExtendedDataName().toString());
				bArray[i]=m1;
				i++;
			}
		}
		
		oracle.sql.ARRAY extListArray = new oracle.sql.ARRAY(arrayDescriptor, conn, bArray);

		return extListArray;
	}

	/**
	 * 
	 * @param xml
	 * @param elementName
	 * @return XML element value
	 */
	public static synchronized String getXmlStringElementValue (String xml, String elementName) {

		if (xml == null || xml.length()==0) {
			return "";
		}


		String beginElement = "<" + elementName + ">";
		int start = xml.indexOf(beginElement);

		String endElement = "</" + elementName + ">";
		int end   = xml.indexOf(endElement);

		if (start == -1 || end == -1) {
			return null;
		}

		// Get description w/i the <description> tags
		start += (elementName.length()) + 2; // start of data = start element (+ 2 for '<' and '>' chars)
		String value = xml.substring(start, end);

		return value;

	}	
}
