package com.cycle30.workpoint.prepaidapi;

import java.util.HashMap;
//import java.util.List;
import org.apache.log4j.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//import com.cycle30.sdk.schema.jaxb.C30AccountObject;
//import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
//import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
//import com.cycle30.sdk.schema.jaxb.C30BalanceList;
//import com.cycle30.sdk.schema.jaxb.C30PlanObject;
//import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
//Workpoint
//import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
//import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.data.Constants;
//import com.cycle30.prov.utils.C30ProvConnectorUtils;

/** This script works on the Service Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class SubscriberCredit {

	private static Logger log = Logger.getLogger(SubscriberCredit.class);

	public SubscriberCredit()
	{
		log.info("Default constructor .....................SubscriberCredit");
	}

	/** 
	 * 
	 * This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException
	 *  
	 */
	public SubscriberCredit(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
		
		// TODO Auto-generated method stub
		String ExpirationDate=null;

		//Add 30 days to expiration date and format it to according OCS request
		// Need to modify/remove this upon further discussion of handling it in BIL
//		Date currentdate = new Date();
//		currentdate.setTime(currentdate.getTime() + 30L * 24 * 60 * 60 * 1000);
//		SimpleDateFormat simpleCurrentDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//		ExpirationDate = simpleCurrentDate.format(currentdate);
//		log.info("ExpirationDate = " +ExpirationDate);
		
		try
		{
			requestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_CREDIT);
//			requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, ExpirationDate);		    
			
			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> responseXML = prepaidRequest.awnprepaidOrderRequest(requestHashMap);
			
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - SubscriberCredit: "+e);
			throw new C30SDKToolkitException(
					"EXCP:Request-SubscriberCredit: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - SubscriberCredit :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-SubscriberCredit: "+e, e);			
		}
	}
	
}
