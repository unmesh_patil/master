package com.cycle30.workpoint.prepaidapi;

import java.io.IOException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;

import org.xml.sax.SAXException;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
//Workpoint
import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
//import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.utils.C30ProvConnectorUtils;

/** This script works on the Service Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class SubscriberActivateByCLI {

	private static Logger log = Logger.getLogger(SubscriberActivateByCLI.class);

	public SubscriberActivateByCLI()
	{
		log.info("Default constructor .....................SubscriberActivateByCLI");
	}

	/** 
	 * 
	 * This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException
	 * @throws C30ProvConnectorException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 *  
	 */
	public SubscriberActivateByCLI(HashMap<String, String> requestHashMap, String acctSegId, 
				String subOrderId, String transactionId,String internalAuditId) throws C30SDKToolkitException, C30ProvConnectorException, ParserConfigurationException, SAXException, IOException {
		
		// TODO Auto-generated method stub
		
		try
		{		
			requestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_ACTIVATE_BY_CLI);
			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> responseXML = prepaidRequest.awnprepaidOrderRequest(requestHashMap);
			
			//Get the Prepaid Subscriber Id/Account No
			String SubscrbierLineId = C30ProvConnectorUtils.getOCSMemberValuefromResponse(responseXML.get("ResponseXML").toString(), Constants.PREPAID_SUBSCRIBER_LINE_ID); 

			//Store Sub Order Extended Data in the database.
			C30OrderExtendedDataLog logOrderExtData = new C30OrderExtendedDataLog(acctSegId, subOrderId, transactionId);
			logOrderExtData.writeToSubOrderExtendedData(C30WPSQLConstants.OCS_RESPONSE_SUBSCRIBER_LINE_ID_KEY, SubscrbierLineId,
					C30WPSQLConstants.C30_EXT_DATA_SEND_FLAG_TRUE);			
			
		}
	    catch (C30ProvConnectorException e) {
		     log.error("ERROR OCCURED- SubscriberActivateByCLI: "+e);
		     //PSST-99 code changes : If account is already Active call subscriberLine.getByCLI when SubsciberActivateBYCLI throws Exception 
		    if(e.getMessage().contains("Already active account"))
		    {	
		    	try{
		       	log.info("Account is already active : Get BY CLI called to get subscriberLineid");
		    	HashMap<String, String> ProvCLIInquiry = new HashMap<String, String>();
	            ProvCLIInquiry.put(C30WPSQLConstants.PREPAID_CLI,requestHashMap.get("CLI"));
	            String CLI = ProvCLIInquiry.get(Constants.PREPAID_CLI);
				log.info("SubscriberLine.GetByCLI: "+CLI);
				ProvCLIInquiry.put(Constants.ACCT_SEG_ID,acctSegId);
				ProvCLIInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
				ProvCLIInquiry.put(Constants.C30_TRANSACTION_ID,internalAuditId);
				ProvCLIInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);			
				ProvCLIInquiry.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBERLINE_GET_BYCLI);
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap<?, ?> response = request.ocsInquiryRequest(ProvCLIInquiry);
				log.info("GetBYCLI Response: "+response.get("ResponseXML"));
				String subscriberLineId = C30ProvConnectorUtils.getOCSMemberValuefromResponse(response.get("ResponseXML").toString(), C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID);
				log.info("subscriberLineId ="+subscriberLineId);
				//Store Sub Order Extended Data in the database.
				C30OrderExtendedDataLog logOrderExtData = new C30OrderExtendedDataLog(acctSegId, subOrderId, transactionId);
				logOrderExtData.writeToSubOrderExtendedData(C30WPSQLConstants.OCS_RESPONSE_SUBSCRIBER_LINE_ID_KEY, subscriberLineId,
						C30WPSQLConstants.C30_EXT_DATA_SEND_FLAG_TRUE);
		    	}
		    	catch(C30SDKToolkitException ex)
		    	{
		    		log.error("ERROR - SubscriberLineGetBYCLI:"+ex);
					ex.printStackTrace();
					throw new C30SDKToolkitException(
							"EXCP:Request-SubscriberLineGetBYCLI: "+ex, ex);
		    	}
		    }
		    else{
		    	log.error("ERROR - SubscriberActivateByCLI :"+e);
			throw new C30SDKToolkitException(
					"EXCP:Request-SubscriberActivateByCLI: "+e, e);
		    }
		
	    }
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - SubscriberActivateByCLI :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - SubscriberActivateByCLI :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-SubscriberActivateByCLI: "+e, e);			
		}
	}
	
}
