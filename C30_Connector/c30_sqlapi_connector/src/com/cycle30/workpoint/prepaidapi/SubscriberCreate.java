package com.cycle30.workpoint.prepaidapi;

import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
//Workpoint
import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.data.Constants;

/** This script works on the Account Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class SubscriberCreate {

	private static Logger log = Logger.getLogger(SubscriberCreate.class);

	public SubscriberCreate()
	{
		log.info("Default constructor .....................SubscriberCreate");
	}

	/** 
	 * 
	 * This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException
	 *  
	 */
	public SubscriberCreate(C30AccountObject account, HashMap<String, String> requestHashMap, String acctSegId, 
			String subOrderId, String transactionId) throws C30SDKToolkitException {
		
		// TODO Auto-generated method stub
		String		acctId=null;
		C30ExtendedDataList extDatList=null;			
		Integer segmentationId = Integer.valueOf(acctSegId);
		
		try
		{
			requestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_CREATE);
			if (account.getClientId() != null) { acctId = C30SqlApiUtils.convertJaxbElementToString(account.getClientId());}
			requestHashMap.put(Constants.PREPAID_FIRST_NAME, acctId);
			
			//Subscriber.Create
			if (account.getExtendedDataList() != null) {
				extDatList=account.getExtendedDataList().getValue();
				log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = extDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList)
				{
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_CPNI_PIN)) {
						if (extData.getExtendedDataValue().toString().isEmpty() || extData.getExtendedDataValue().toString() == null) {
							throw new C30SDKToolkitException(
									"EXCP: ORDER-addAccountInPrepaidSystem Request - CPNIPIN not found");
						} else {
							requestHashMap.put(Constants.PREPAID_PINCODE, extData.getExtendedDataValue().toString());								
						}
					}
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PLAN_PROFILE)) {
						if (extData.getExtendedDataValue().toString().isEmpty() || extData.getExtendedDataValue().toString() == null) {
							if (segmentationId.equals(Constants.ACCT_SEG_ID_4)) {
								requestHashMap.put(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL, C30WPSQLConstants.GCI_PREPAID_DEFAULT_PROFILE);								
							}
							if (segmentationId.equals(Constants.ACCT_SEG_ID_5)) {
								requestHashMap.put(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL, C30WPSQLConstants.ACS_PREPAID_DEFAULT_PROFILE);								
							}
						} else {
							requestHashMap.put(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL, extData.getExtendedDataValue().toString());								
						}
					} else {
						if (segmentationId.equals(Constants.ACCT_SEG_ID_4)) {
							requestHashMap.put(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL, C30WPSQLConstants.GCI_PREPAID_DEFAULT_PROFILE);								
						}
						if (segmentationId.equals(Constants.ACCT_SEG_ID_5)) {
							requestHashMap.put(Constants.PREPAID_SUBSCRIBER_PROFILE_LABEL, C30WPSQLConstants.ACS_PREPAID_DEFAULT_PROFILE);								
						}
					}
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_AUTOPAY_FLAG)) {
						if (extData.getExtendedDataValue().toString().equalsIgnoreCase("YES")) {
							requestHashMap.put(Constants.PREPAID_LAST_NAME, C30WPSQLConstants.PREPAID_AUTOPAY_VALUE);								
						}
					}
				}//END - for(C30ExtendedData extData : extdtList)
			}

			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> responseXML = prepaidRequest.awnprepaidOrderRequest(requestHashMap);
			
			//Get the Prepaid Subscriber Id/Account No
			String SubscrbierId = C30SqlApiUtils.getXmlStringElementValue(responseXML.get("ResponseXML").toString(), C30WPSQLConstants.OCS_XML_INT_NAME_TAG);

			//Store the Subscriber Id Response
			C30OrderExtendedDataLog logOrderExtData = new C30OrderExtendedDataLog(acctSegId, subOrderId, transactionId);
			logOrderExtData.writeToSubOrderExtendedData(C30WPSQLConstants.OCS_RESPONSE_SUBSCRIBER_KEY, SubscrbierId, 
					C30WPSQLConstants.C30_EXT_DATA_SEND_FLAG_TRUE);			
			
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Request-SubscriberCreate: "+e);
			throw new C30SDKToolkitException(
					"EXCP:Request-SubscriberCreate: "+e, e);
	    }
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Request-SubscriberCreate: "+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP:Request-SubscriberCreate: "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-SubscriberCreate: "+e, e);			
		}
	}
	
}
