package com.cycle30.workpoint.prepaidapi;

import java.util.HashMap;
//import java.util.List;
import org.apache.log4j.Logger;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
//Workpoint
import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
//import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.utils.C30ProvConnectorUtils;

/** This script works on the Service Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class SubscriberGetById {

	private static Logger log = Logger.getLogger(SubscriberGetById.class);

	public SubscriberGetById()
	{
		log.info("Default constructor .....................SubscriberGetById");
	}

	/** 
	 * 
	 * This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException
	 *  
	 */
	public SubscriberGetById(HashMap<String, String> requestHashMap, String acctSegId, 
				String subOrderId, String transactionId) throws C30SDKToolkitException {
		
		// TODO Auto-generated method stub
		
		try
		{		
			requestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_GET_BYID);
			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> responseXML = prepaidRequest.awnprepaidOrderRequest(requestHashMap);
			
			//Get the Prepaid Subscriber Id/Account No
			String SubscrbierLineId = C30ProvConnectorUtils.getOCSMemberValuefromResponse(responseXML.get("ResponseXML").toString(), Constants.PREPAID_SUBSCRIBER_LINE_ID); 
			log.info("SubscrbierLineId : " + SubscrbierLineId);
			
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - SubscriberActivateByCLI: "+e);
			throw new C30SDKToolkitException(
					"EXCP:Request-SubscriberActivateByCLI: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - SubscriberActivateByCLI :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-SubscriberActivateByCLI: "+e, e);			
		}
	}
	
}
