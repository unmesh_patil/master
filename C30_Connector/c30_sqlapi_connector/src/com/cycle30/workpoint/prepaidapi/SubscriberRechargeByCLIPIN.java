package com.cycle30.workpoint.prepaidapi;

import java.util.HashMap;
//import java.util.List;
import org.apache.log4j.Logger;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//import com.cycle30.sdk.schema.jaxb.C30AccountObject;
//import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
//import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
//import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
//import com.cycle30.sdk.schema.jaxb.C30PlanObject;
//import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
////Workpoint
//import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
//import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
//import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.data.Constants;

/** This script works on the SubscriberLine.Update API call
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class SubscriberRechargeByCLIPIN {

	private static Logger log = Logger.getLogger(SubscriberRechargeByCLIPIN.class);

	public SubscriberRechargeByCLIPIN()
	{
		log.info("Default constructor .....................SubscriberRechargeByCLIPIN");
	}

	/** 
	 * 
	 * This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException
	 *  
	 */
	public SubscriberRechargeByCLIPIN(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
		
		// TODO Auto-generated method stub
		
		try
		{	
			requestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_UPDATE_RECHARGE_VOUCHER);

			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> responseXML = prepaidRequest.awnprepaidOrderRequest(requestHashMap);
			
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Request-SubscriberRechargeByCLIPIN: "+e);
			throw new C30SDKToolkitException(
					"EXCP:Request-SubscriberRechargeByCLIPIN: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Request-SubscriberRechargeByCLIPIN: "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-SubscriberRechargeByCLIPIN: "+e, e);			
		}
	}
	
}
