package com.cycle30.workpoint.prepaidapi;

import java.util.HashMap;
//import java.util.List;
import org.apache.log4j.Logger;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;

//Workpoint
//import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
//import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
//import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.data.Constants;
//import com.cycle30.prov.utils.C30ProvConnectorUtils;

/** This script works on the Service Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class EventChargingDirectDebit {

	private static Logger log = Logger.getLogger(EventChargingDirectDebit.class);

	public EventChargingDirectDebit()
	{
		log.info("Default constructor .....................EventChargingDirectDebit");
	}

	/** 
	 * 
	 * This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException
	 *  
	 */
	public EventChargingDirectDebit(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
		
		// TODO Auto-generated method stub
//		String planName = null;
		
		try
		{		
			requestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT);
			
			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> responseXML = prepaidRequest.awnprepaidOrderRequest(requestHashMap);
			
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - EventChargingDirectDebit: "+e);
			throw new C30SDKToolkitException(
					"EXCP:Request-EventChargingDirectDebit: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - EventChargingDirectDebit :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-EventChargingDirectDebit: "+e, e);			
		}
	}
	
}
