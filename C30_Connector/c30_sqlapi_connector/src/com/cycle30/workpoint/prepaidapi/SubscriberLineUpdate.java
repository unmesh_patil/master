package com.cycle30.workpoint.prepaidapi;

import java.util.HashMap;
import org.apache.log4j.Logger;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//Provision
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.data.Constants;

/** This script works on the SubscriberLine.Update API call
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class SubscriberLineUpdate {

	private static Logger log = Logger.getLogger(SubscriberLineUpdate.class);

	public SubscriberLineUpdate()
	{
		log.info("Default constructor .....................SubscriberLineUpdate");
	}

	/** 
	 * 
	 * This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException
	 *  
	 */
	public SubscriberLineUpdate(HashMap<String, String> requestHashMap, String SubscriberId) throws C30SDKToolkitException {
		
		// TODO Auto-generated method stub
		try
		{	
			
			requestHashMap.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_LINE_UPDATE);

			//Create a Prepaid System Request Instance
			C30ProvisionRequest prepaidRequest = C30ProvisionRequest.getInstance();
			HashMap<?, ?> responseXML = prepaidRequest.awnprepaidOrderRequest(requestHashMap);
			
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Request-SubscriberLineUpdate: "+e);
			throw new C30SDKToolkitException(
					"EXCP:Request-SubscriberLineUpdate: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Request-SubscriberLineUpdate: "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-SubscriberLineUpdate: "+e, e);			
		}
	}
	
}
