package com.cycle30.workpoint;

public class C30SqlApiBuildVersion {
	/** This tag is used to identify the Build version of the C30_SqlApi_Connector, to verify the build
	 * dependencies and smoke testing few basic issues etc.,
	 * Also, we use the log.error so that this will not be ignored in the low log level
	 * 
	 * Also this need to be updated every time we request a build.
	 */
	public static String LATEST_BUILD_VERSION="2012-09-25T13:00:00";
	
	/**
	 * 9/10/2012 - Initial Version of the C30_Provisioning_Connector
	 * 
	 */
	public static String CHANGE_LOG="" ;
}
