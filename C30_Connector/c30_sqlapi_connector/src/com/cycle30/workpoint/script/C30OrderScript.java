package com.cycle30.workpoint.script;

import java.sql.Connection;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30DOMUtils;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30SDKConfigurationManager;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;


/** This script works on the Account Object in the given XML
 * 
 * @author rnelluri
 * 
 * @author Umesh 
 * - Updated the TransactionId Feature Modifications
 * - Accepting the request name for Prepaid
 *
 */
public abstract class C30OrderScript {

	//Logs all the information
	public static Logger log = Logger.getLogger(C30OrderScript.class);
	public static C30SDKConfigurationManager exceptionResourceBundle = C30SDKConfigurationManager.getInstance();
	public OrderRequest order;
	public Connection conn = null;
	
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the subOrderId
	 */
	public String getSubOrderId() {
		return subOrderId;
	}

	/**
	 * @param subOrderId the subOrderId to set
	 */
	public void setSubOrderId(String subOrderId) {
		this.subOrderId = subOrderId;
	}

	
	/**
	 * @return the acctSegId
	 */
	public String getAcctSegId() {
		return acctSegId;
	}

	/**
	 * @param acctSegId the acctSegId to set
	 */
	public void setAcctSegId(String acctSegId) {
		this.acctSegId = acctSegId;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the internalAuditId
	 */
	public String getInternalAuditId() {
		return internalAuditId;
	}

	/**
	 * @param internalAuditId the internalAuditId to set
	 */
	public void setInternalAuditId(String internalAuditId) {
		this.internalAuditId = internalAuditId;
	}

	/**
	 * @return the requestName
	 */
	public String getRequestName() {
		return requestName;
	}

	/**
	 * @param requestName the requestName to set
	 */
	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}
	

	public String orderId;
	public String subOrderId;
	public String acctSegId;
	public String transactionId;	
	public String internalAuditId;
	public String requestId;
	public String requestName;
	
	public C30OrderScript(String orderId) throws C30SDKToolkitException
	{
		try
		{
			this.orderId =orderId; 
			C30WPSqlApiConnector apiConnector = C30WPSqlApiConnector.getInstance();
			conn = apiConnector.getConnection();
			log.info("Able to extract genericGlobalConnection from billing Datasource");
		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}

	public C30OrderScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException
	{
		log.info("Constructor ...C30OrderScript");
		this.orderId=orderId;
		this.subOrderId=subOrderId;	
		this.acctSegId=acctSegId;
		this.transactionId= transactionId;
		this.internalAuditId= internalAuditId;
		
		try {
			//Get the data source for AWN segment only
			if (acctSegId.equalsIgnoreCase(C30WPSQLConstants.C30_SEGMENT_ACS)) {
				C30WPSqlApiConnector apiConnector = C30WPSqlApiConnector.getInstance();
				conn = apiConnector.getConnection(new Integer(acctSegId));
				log.info("Able to extract genericGlobalConnection from billing Datasource");
			}
			String xmlPayload = getOrderXMLFromDataBase(subOrderId,transactionId);
			processXmlPayload(xmlPayload, subOrderId, acctSegId);
		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}

	}


	public C30OrderScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId, String requestName) throws C30SDKToolkitException
	{
		log.info("Constructor ...C30OrderScript");
		this.orderId=orderId;
		this.subOrderId=subOrderId;	
		this.acctSegId=acctSegId;
		this.transactionId= transactionId;
		this.internalAuditId= internalAuditId;
		this.requestName=requestName;		
		
		try {
			String xmlPayload = getOrderXMLFromDataBase(subOrderId,transactionId);
			//Process Payload
			processXmlPayload(xmlPayload, subOrderId, acctSegId);
		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}

	}
	
	public C30OrderScript(String requestId, String requestName) throws C30SDKToolkitException
	{
		log.info("Constructor ...C30OrderScript");
		
		this.requestId=requestId;
		this.requestName=requestName;	
	}
	
	/** this method is responsible Processing the scripts
	 * 
	 * @throws C30SDKToolkitException
	 */
	public abstract void process() throws C30SDKToolkitException;


	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void processXmlPayload(String xmlPayload, String subOrderId, String acctSegId) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		//Check the inputXML against a schema.
		Document orderXMLDoc = C30DOMUtils.stringToDomAndValidate(xmlPayload, "/com/cycle30/sdk/schema/C30BillingOrder.xsd");

		// Serialize - Store the order XML in order SDK Database.
		try
		{
			//Use JAXB for the unmarshalling
			JAXBContext context = JAXBContext.newInstance(OrderRequest.class);
			Unmarshaller u = context.createUnmarshaller();

			order = (OrderRequest)u.unmarshal(orderXMLDoc);			
		}
		catch(Exception e)
		{	e.printStackTrace();
		throw new C30SDKToolkitException("EXCP", e, "GENERIC");
		}
	}


	private String getOrderXMLFromDataBase(String subOrderId, String transactionId) throws C30SDKToolkitException
	{
		String xmlPayload ="";

		log.info("Get the XML Payload from database/C30_SDK_SUB_ORDER for the given subOrderId");


		C30JDBCDataSource dataSource = null;
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   
			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_sub_order");

			call.addParam(new C30CustomParamDef("payload", 2, 300, 2));
			call.addParam(new C30CustomParamDef("v_client_sub_order_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_transaction_id", 2, 300, 1));
			

			ArrayList paramValues = new ArrayList();
			paramValues.add(subOrderId);
			paramValues.add(transactionId);

			dataSource.queryData(call, paramValues, 2);
			log.debug("Total records found = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{

				if(dataSource.getValueAt(0,0)!= null) 
				{
					byte[] bdata =((oracle.sql.BLOB)dataSource.getValueAt(0,0)).getBytes(1,(int) ((oracle.sql.BLOB)dataSource.getValueAt(0,0)).length() );
					//log.info("XML Payload :"+(( oracle.sql.BLOB)dataSource.getValueAt(0,0)).stringValue().toString());
					xmlPayload= new String (bdata);
					log.info("XML Payload :"+xmlPayload);
				}

			}

		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Exception during the database operation", e, "CONN-032");
		}
		finally {
			try 
			{
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30SDKToolkitException("Exception during the database operation", ex, "CONN-032");				
			}
		}

		log.info("returning the orderXMLPayload");

		return xmlPayload;

	}

}
