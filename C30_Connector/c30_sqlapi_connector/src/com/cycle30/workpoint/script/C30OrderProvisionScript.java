package com.cycle30.workpoint.script;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Struct;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.C30WPGetPlanInfoApi;
import com.cycle30.workpoint.sqlapi.C30WPGetServiceInfoApi;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;


/** This script works on the Order Object in the given XML for Provisioning
 * 
 * @author Umesh
 * - Initial Version
 * 
 * 
 */
public class C30OrderProvisionScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30OrderProvisionScript.class);

	private boolean outParam = false;
	
	public C30OrderProvisionScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30OrderProvisionScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException, C30ProvConnectorException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the Provisioning action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String orderDesiredDate = null;
			String actionWho = null;
			String identifierTypeName = null;
			String SIMIdentifierIdIn = null;	
			String SIMIdentifierIdOut = null;
	    	String identifierAction = null;		
	    	
			orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
			actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());
			
			if (orderDesiredDate == null) {
				orderDesiredDate = new Date().toString();
			}
			log.info("acctSegId: " + acctSegId + "orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);				
			
			//If AWN 
			if (acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_ACS)) {

				log.info("START - Provisioning Request for : " + " acctSegId: " + acctSegId);
				
				//Check the Service Object
				if (order.getServiceObjectList() != null)
				{
					List<C30ServiceObject> sList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
					log.info("Working on the Service Object.  Size of list is: "+ sList.size());
					for (C30ServiceObject service : sList)
					{
						
					    String serviceId = service.getClientId();
					    String svcactionType  = service.getClientActionType().toString();
			            log.info("Sub order id: '" + subOrderId + "'  Service id: '" + serviceId + "'  Action type: '" + svcactionType);	
			            
			            //Get the ICCID from a PAYLOAD
					    if (service.getIdentifierObjectList() != null)  {
					    	List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
					    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());					    	
					    	for (C30IdentifierObject identifier : ilList)  {
					    		//Get the Identifier Type and the action
								identifierTypeName = identifier.getIdentifierTypeName();
								identifierAction = identifier.getClientActionType().toString();

									//Get the values if Identifier Type is ICCID
									if ((identifierTypeName.equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) || 
											(identifierTypeName.equals(C30WPSQLConstants.C30_ICCID_VALUE))) {
											//ICCID In
											SIMIdentifierIdIn = identifier.getIdentifierIdIn();
											log.info("ICCID - SIMIdentifierIdIn : "+SIMIdentifierIdIn);
										if (identifier.getIdentifierIdOut() != null) {
											//ICCID Out
											SIMIdentifierIdOut = identifier.getIdentifierIdOut();
											log.info("ICCID - SIMIdentifierIdOut : "+SIMIdentifierIdOut);											
										}//End - if (identifier.getIdentifierIdOut() != null)
										
									} else if ((identifierTypeName.equalsIgnoreCase(C30WPSQLConstants.C30_MSISDN)) || 
											(identifierTypeName.equals(C30WPSQLConstants.C30_MSISDN_VALUE))) { 
										//Get the values if Identifier Type is MSISDN
										String svcId = null;
										svcId = identifier.getIdentifierIdOut();
								    	//Get the ICCID from Billing 
										C30WPGetServiceInfoApi getSvcInfo  = new C30WPGetServiceInfoApi(conn, transactionId, acctSegId, svcId);
										//The return list of Identifier collection is Array
										Array identiferList = (Array) getSvcInfo.getDataSource().getValueAt(0,10);
										Object[] objects = (Object[]) identiferList.getArray();
										Object[] attr = null;
										for (Object obj :objects)
										{
											attr = (Object[]) ((Struct)obj).getAttributes();
											if (attr[2].equals(C30WPSQLConstants.C30_ICCID)) {
												log.info("ICCID Retreived from Billing for SWAP TN : "+attr[0]);
												SIMIdentifierIdOut = (String)attr[0];
											}											
//											if (((BigDecimal)attr[1]).intValue()==4) {
//												log.info("ICCID Retreived from Billing for SWAP TN : "+attr[0]);
//												SIMIdentifierIdOut = (String)attr[0];
//											}
										}
									}
								
					    	}//End - for (C30IdentifierObject identifier : ilList)
					    	
					    }//End - if (service.getIdentifierObjectList() != null)
					    else {
						    	//Get the ICCID from Billing if the PAYLOAD does not have a Identifier Object
								C30WPGetServiceInfoApi getSvcInfo  = new C30WPGetServiceInfoApi(conn, transactionId, acctSegId, serviceId);
								//The return list of Identifier collection is Array
								Array identiferList = (Array) getSvcInfo.getDataSource().getValueAt(0,10);
								Object[] objects = (Object[]) identiferList.getArray();
								Object[] attr = null;
								for (Object obj :objects)
								{
									attr = (Object[]) ((Struct)obj).getAttributes();
									if (attr[2].equals(C30WPSQLConstants.C30_ICCID)) {
										log.info("ICCID Retreived from Billing : "+attr[0]);
										SIMIdentifierIdOut = (String)attr[0];
									}									
//									if (((BigDecimal)attr[1]).intValue()==4) {
//										log.info("ICCID Retreived from Billing : "+attr[0]);
//										SIMIdentifierIdOut = (String)attr[0];
//									}
								}
					    }//End - else if (service.getIdentifierObjectList() != null)
			            
					    
			            //Service and Plan
					    if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
					    	//ADD Service to SPS
					    	addServicetoProvisioningSystem(service, acctSegId, transactionId, internalAuditId, orderDesiredDate, actionWho, 
					    			serviceId, SIMIdentifierIdIn, svcactionType);
					    } else if ((svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) ||
					    		(svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SUSPEND)) ||
					    		(svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_RESUME))) {
					    	//DISCONNECT/SUSPEND/RESUME service in Provisioning
					    	changeServiceStatusInProvisioningSystem(service, acctSegId, transactionId, internalAuditId, orderDesiredDate, actionWho, 
					    			serviceId, SIMIdentifierIdOut, svcactionType);
					    } else if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
						    //Identifier - Change Telephone Number  
						    if (service.getIdentifierObjectList() != null)  {
						    	List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
						    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());					    	
						    	for (C30IdentifierObject identifier : ilList)  {
						    		//Get the Identifier Type and the action
									identifierTypeName = identifier.getIdentifierTypeName();
									identifierAction = identifier.getClientActionType().toString();
									if (identifierAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)){
										//Change Telephone Number
										if ((identifierTypeName.equalsIgnoreCase(C30WPSQLConstants.C30_MSISDN)) || 
												(identifierTypeName.equalsIgnoreCase(C30WPSQLConstants.C30_PHONE_NUMBER)) ||
												(identifierTypeName.equals(C30WPSQLConstants.C30_MSISDN_VALUE))) {
											changeTNInProvisioningSystem(identifier, acctSegId, transactionId, internalAuditId, orderDesiredDate, actionWho,
													serviceId, SIMIdentifierIdOut, identifierAction);
										}
									}//End - if (identifierAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))
									
						    	}//End - for (C30IdentifierObject identifier : ilList)
						    	
						    }//End - if (service.getIdentifierObjectList() != null)
					    }else if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {
							//Identifier - Change SIM
						    if (service.getIdentifierObjectList() != null)  {
						    	
						    	List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
						    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());					    	
						    	for (C30IdentifierObject identifier : ilList)  {
									identifierTypeName = identifier.getIdentifierTypeName();
									identifierAction = identifier.getClientActionType().toString();
									if (identifierAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)){
										//Change SIM 
										if ((identifierTypeName.equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) || 
												(identifierTypeName.equals(C30WPSQLConstants.C30_ICCID_VALUE))) {
											changeSIMInProvisioningSystem(identifier, acctSegId, transactionId, internalAuditId, orderDesiredDate, actionWho,
													serviceId, identifierAction);
										}
										
									}//End - if (identifierAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))
									
						    	}//End - for (C30IdentifierObject identifier : ilList)
						    	
						    }//End - if (service.getIdentifierObjectList() != null)
						    
							// CHANGE - Feature/Component
		                    if (service.getComponentObjectList() != null) {
							    List<C30ComponentObject> clList = (List<C30ComponentObject>) service.getComponentObjectList().getComponentObject();
						    	log.info("Working on the Component Object.  Size of list is " + clList.size());					    
							    for (C30ComponentObject comp : clList) {
							    	 String compactionType  = comp.getClientActionType().toString();
							    	 //Send Feature to Provisioning
							    	sendFeaturetoProvisioningSystem(comp, acctSegId, transactionId, internalAuditId, orderDesiredDate, actionWho, serviceId, 
							    			SIMIdentifierIdOut, compactionType);
							    }
		                    	
		                    }//End - if (service.getComponentObjectList() != null)
					    	
					    }
						
					}//End - for (C30ServiceObject service : sList)
					
				}//End - if (order.getServiceObjectList() != null)				
				
				log.info("END - Provisioning Request for : " + " acctSegId: " + acctSegId);				
				
			}//End - if (acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_ACS))
			else {
				log.info("Customer Segementation is NOT Valid for Provisioning, SegmentId : " +acctSegId);
			}
			
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Account Segment : " +acctSegId + " Provisioning Request : "+e);
			e.printStackTrace();
			throw e;
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Account Segment : " +acctSegId + " Provisioning Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Provisioning Request "+e, e);
	    }
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Provisioning Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Provisioning Request "+e, e);
		}
	}

	/**This method is responsible for Sending a Service Details to Provisioning system
	 * 
	 * @param service
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	private void addServicetoProvisioningSystem(C30ServiceObject service, String acctSegId, String transactionId, String internalAuditId, String orderDesiredDate, 
			String actionWho, String serviceId, String SIMIdentifierIdIn, String actionType) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Sending ADD service to Provisioning System...");
		
		try{
				HashMap<String, String> provRequestData = new HashMap<String, String>();	
				
			    String	planactionType = null;
				String	catalogId = null;
				String	instanceId = null;
				
				//Get Plan Catalog Id
				if (service.getPlanObject() != null) {
					//Check on the Plan Object
					C30PlanObject plan = service.getPlanObject().getValue();
					if (plan != null) {
					    planactionType  = plan.getClientActionType().toString();
					    log.info("Plan Action Type : " +planactionType);
					    
						if (plan.getPlanInstanceIdIn() != null)		{	instanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdIn());}						
						if (plan.getPlanName() != null) {
							catalogId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanName());}
						else {
								//Get the CatalogId for the given Catalog Instance Id from Billing
								C30WPGetPlanInfoApi getPlanInfo  = new C30WPGetPlanInfoApi(conn, transactionId, acctSegId, instanceId);
								if(getPlanInfo.getDataSource().getRowCount() > 0) {
									for(int i = 0; i < getPlanInfo.getDataSource().getRowCount(); i++)  {
										if(getPlanInfo.getDataSource().getValueAt(i,1)!= null)   {
											catalogId = (String)getPlanInfo.getDataSource().getValueAt(i,0);
											log.info("CatalogId: " + catalogId + " Retreived for the given Instance Id: " +instanceId);
										}
	
									}
								}//End - if(getPlanInfo.getDataSource().getRowCount() > 0)			
							}
					} //End - if (plan != null)
				}//End - if (service.getPlanObject() != null)
				else { 
					log.info("Plan object IS NOT available");	
				}
				
				log.info("ServiceId: " +serviceId+" catalogId: "+catalogId);	
				//Prepare Data For Provisioning
				provRequestData.put("ExternalTransactionId",transactionId);				
				provRequestData.put("C30TransactionId",internalAuditId);
				provRequestData.put("AcctSegId", acctSegId);
				provRequestData.put("TelephoneNumber", serviceId);
				provRequestData.put("CatalogId", catalogId);
				provRequestData.put("ICCID", SIMIdentifierIdIn);
				provRequestData.put("CatalogActionId", actionType);

				log.info("Send Service " +actionType+" Request for Provisioning");
				log.info("ExternalTransactionId: " +transactionId+" C30TransactionId: " +internalAuditId +
						" TelephoneNumber: "+serviceId+" CatalogId : "+catalogId+" ICCID: "+SIMIdentifierIdIn);				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Service ADD RequestId : "+response.get("RequestId") +" Service ADD ResponseCode: "+ response.get("ResponseCode"));
				
				//Feature/Component
                if (service.getComponentObjectList() != null) {
                	
				    List<C30ComponentObject> clList = (List<C30ComponentObject>) service.getComponentObjectList().getComponentObject();
			    	log.info("Working on the Component Object for Provisioning.  Size of list is " + clList.size());					    
				    for (C30ComponentObject comp : clList) {
				    	 String compactionType  = comp.getClientActionType().toString();
				    	sendFeaturetoProvisioningSystem(comp, acctSegId, transactionId, internalAuditId, orderDesiredDate, actionWho, serviceId, 
				    			SIMIdentifierIdIn, compactionType);
				    }
                }
				
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Account Segment : " +acctSegId + " Service Provisioning Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-ADD Service Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Service Provisioning : "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Service Order Request Process : "+e, e );			
		}
		
	}

	/**This method is responsible for Changing Telephone Details in Provisioning system
	 * 
	 * @param identifier
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	private void changeTNInProvisioningSystem(C30IdentifierObject identifier, String acctSegId, String transactionId, String internalAuditId, 
			String orderDesiredDate, String actionWho, String serviceId, String SIMIdentifierIdOut, 
			String actionType) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Change Telephone Number in Provisioning System...");
		
		try{
				HashMap<String, String> provRequestData = new HashMap<String, String>();
				String newTelephoneNumber = null;
				String oldTelephoneNumber = null;
				
				newTelephoneNumber = identifier.getIdentifierIdIn();
				oldTelephoneNumber = identifier.getIdentifierIdOut();
		    	log.info("oldTelephoneNumber : " + oldTelephoneNumber + "newTelephoneNumber : " + newTelephoneNumber);	
		    	
				//Prepare Data For Provisioning
				provRequestData.put("ExternalTransactionId", transactionId);			
				provRequestData.put("C30TransactionId",internalAuditId);
				provRequestData.put("AcctSegId", acctSegId);
				provRequestData.put("TelephoneNumber", oldTelephoneNumber);
				provRequestData.put("NewTelephoneNumber", newTelephoneNumber);		
				provRequestData.put("ICCID", SIMIdentifierIdOut); 				
				provRequestData.put("CatalogId", "TELEPHONENUMBER");			
				provRequestData.put("CatalogActionId", actionType);

				log.info("Send Telephonenumber "+actionType+" Request for Provisioning ");	
				log.info("ExternalTransactionId: " +transactionId+" C30TransactionId: " +internalAuditId +
						" TelephoneNumber: "+serviceId+" ICCID: "+SIMIdentifierIdOut);				
				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Change TN RequestId : "+response.get("RequestId") +" Change TN ResponseCode: "+ response.get("ResponseCode"));
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Account Segment : " +acctSegId + " Telephone Number Change Provisioning Request : "+e);
			throw new C30SDKToolkitException("EXCP: ORDER-Telephone Number Change Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Telephone Number Change Provisioning : "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Telephone Number Change Order Request Process : "+e, e );			
		}
		
	}	

	/**This method is responsible for Changing SIM Details in Provisioning system
	 * 
	 * @param identifier
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	private void changeSIMInProvisioningSystem(C30IdentifierObject identifier, String acctSegId, String transactionId, String internalAuditId, 
			String orderDesiredDate, String actionWho, String serviceId, String actionType) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Change SIM in Provisioning System...");
		
		try{
				HashMap<String, String> provRequestData = new HashMap<String, String>();	
	
				String	oldICCID = null;
				String	newICCID = null;
				
				oldICCID = identifier.getIdentifierIdOut();
				newICCID = identifier.getIdentifierIdIn();
		    	log.info("oldICCID : " + oldICCID + "newICCID : " + newICCID);				

				//Prepare Data For Provisioning
				provRequestData.put("ExternalTransactionId", transactionId);			
				provRequestData.put("C30TransactionId",internalAuditId);
				provRequestData.put("AcctSegId", acctSegId);
				provRequestData.put("TelephoneNumber", serviceId);
				provRequestData.put("NewICCID", newICCID);		
				provRequestData.put("ICCID", oldICCID);				
				provRequestData.put("CatalogId", "ICCID");			
				provRequestData.put("CatalogActionId", actionType);
				
				log.info("Send SIM "+actionType+" Request for Provisioning ");				
				log.info("ExternalTransactionId: " +transactionId+" C30TransactionId: " +internalAuditId +
						" TelephoneNumber: "+serviceId+" ICCID: "+oldICCID+ " NewICCID: "+newICCID);				
				
				//Provisioning Request
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap response = request.awnProvisioningRequest(provRequestData);
		
				log.info("Change SIM RequestId : "+response.get("RequestId") +" Change SIM ResponseCode: "+ response.get("ResponseCode"));
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Account Segment : " +acctSegId + " SIM Change Provisioning Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-SIM Change Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - SIM Change Provisioning : "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-SIM Change Order Request Process : "+e, e );			
		}
		
	}	


	/**This method is responsible for Sending a Component Details to Provisioning system
	 * 
	 * @param component
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	private void sendFeaturetoProvisioningSystem(C30ComponentObject component, String acctSegId, String transactionId, String internalAuditId, 
			String orderDesiredDate, String actionWho, String serviceId, String SIMIdentifierId, 
			String actionType) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Sending Feature to Provisioning System...");
		
		try{
			HashMap<String, String> provRequestData = new HashMap<String, String>();	
			String catalogId = null;
			String instanceId = null;
			
			if (component.getComponentName() != null)			{	catalogId = C30SqlApiUtils.convertJaxbElementToString(component.getComponentName());}
			if (component.getComponentInstanceId() != null)		{	instanceId = C30SqlApiUtils.convertJaxbElementToString(component.getComponentInstanceId());}
			
			log.info("Provisioning Request for Feature Instance Id: "+instanceId +" CatalogId: "+ catalogId);
			//Prepare Data For Provisioning
			provRequestData.put("ExternalTransactionId",transactionId);				
			provRequestData.put("C30TransactionId",internalAuditId);
			provRequestData.put("AcctSegId", acctSegId);
			provRequestData.put("TelephoneNumber", serviceId);
			provRequestData.put("CatalogId", catalogId);
			provRequestData.put("ICCID", SIMIdentifierId);
			provRequestData.put("CatalogActionId", actionType);
			
			log.info("Send "+actionType+" Feature Request for Provisioning");
			log.info("ExternalTransactionId: " +transactionId+" C30TransactionId: " +internalAuditId +
					" TelephoneNumber: "+serviceId+" ICCID: "+SIMIdentifierId);				
			
			//Provisioning Request
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap response = request.awnProvisioningRequest(provRequestData);
	
			log.info("Feature RequestId : "+response.get("RequestId") +" Feature ResponseCode: "+ response.get("ResponseCode"));
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Account Segment : " +acctSegId + " Feature Provisioning Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Feature Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Feature Provisioning : "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Feature Order Request Process : "+e, e );			
		}
		
	}	


	/**This method is responsible for Change Service Status in Provisioning system
	 * 
	 * @param service
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	private void changeServiceStatusInProvisioningSystem(C30ServiceObject service, String acctSegId, String transactionId, String internalAuditId, 
			String orderDesiredDate, String actionWho, String serviceId, String SIMIdentifierId, 
			String actionType) throws C30SDKToolkitException, C30ProvConnectorException {

		log.info("Change Service Status In Provisioning System...");
		
		try{
			
			//If DISCONNECT check if the given service id is a HOST
			if (actionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
				
				String	primaryServiceId=null;
				
				//Get the Service Information
				C30WPGetServiceInfoApi getSvcInfo  = new C30WPGetServiceInfoApi(conn, transactionId, acctSegId, serviceId);
				//Check the Value Returned
				if (getSvcInfo.getDataSource().getRowCount() > 0 ) {
					primaryServiceId = (String) getSvcInfo.getDataSource().getValueAt(0,0);
					log.info("primaryServiceId " + primaryServiceId);
					
					//Derive based on PARTNER Count
					if (getSvcInfo.getDataSource().getValueAt(0,9) != null) {
						BigDecimal isHost = (BigDecimal) getSvcInfo.getDataSource().getValueAt(0,9);
						log.info("primaryServiceId " + primaryServiceId + " isHost "+isHost);	
						//If Partner Cannot Continue
						if ((isHost.intValue() > 0)) {
							log.info("serviceId : " + primaryServiceId + " is a HOST service ID Cannot continue with DISCONNECT action ");
							String excp = "serviceId : " + primaryServiceId + " is a HOST service ID Cannot continue with DISCONNECT action ";
							throw new C30SDKToolkitException("EXCP: Provisioning Request: "+excp, excp);
						}						
					}
					else {
						log.info("Unable to determine the Telephone Number : " + primaryServiceId + " is a HOST or PARTNER Cannot continue with DISCONNECT action ");
						String excp = "Unable to determine the serviceId : " + primaryServiceId + " is a HOST or NOT Cannot continue with DISCONNECT action ";
						throw new C30SDKToolkitException("EXCP: Provisioning Request: "+excp, excp);
					}
				}
			}//End - if (actionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC))
			
			HashMap<String, String> provRequestData = new HashMap<String, String>();	
			
			log.info("Provisioning Request for Service Id : "+serviceId);
			//Prepare Data For Provisioning
			provRequestData.put("ExternalTransactionId",transactionId);				
			provRequestData.put("C30TransactionId",internalAuditId);
			provRequestData.put("AcctSegId", acctSegId);
			provRequestData.put("TelephoneNumber", serviceId);
			provRequestData.put("CatalogId", "SERVICE");
			provRequestData.put("ICCID", SIMIdentifierId);
			provRequestData.put("CatalogActionId", actionType);
			
			log.info("Send "+actionType+" Service Request for Provisioning");	
			log.info("ExternalTransactionId: " +transactionId+" C30TransactionId: " +internalAuditId +
					" TelephoneNumber: "+serviceId+" ICCID: "+SIMIdentifierId);				
			
			//Provisioning Request
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap response = request.awnProvisioningRequest(provRequestData);
	
			log.info("Service " + actionType +" RequestId : "+response.get("RequestId") +" Service " + actionType +" ResponseCode: "+ response.get("ResponseCode"));
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Account Segment : " +acctSegId + " Change Service Status Provisioning Request : "+e);
			throw new C30SDKToolkitException("EXCP: ORDER-Change Service Status Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Change Service Status Provisioning : "+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Change Service Status Order Request Process : "+e, e);			
		}
		
	}	
	
	public boolean isOutParam()
	{
		return outParam;
	}

	public void setOutParam(boolean outParam)
	{
		this.outParam = outParam;
	}
 
}
