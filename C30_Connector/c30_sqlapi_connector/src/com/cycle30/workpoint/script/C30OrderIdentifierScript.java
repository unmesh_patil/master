package com.cycle30.workpoint.script;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest.ServiceObjectList;
import com.cycle30.workpoint.sqlapi.C30WPAddIdentifierApi;
import com.cycle30.workpoint.sqlapi.C30WPDiscIdentifierApi;
import com.cycle30.workpoint.sqlapi.C30WPSwapIdentifierApi;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

//Provisioning Enquiry
import com.cycle30.prov.auth.ProvAuthClientDataSource;
import com.cycle30.prov.auth.ProvAuthClientObject;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;


/** This script works on the Identifier object in the given xml
 * 
 * @author Umesh
 * - Initial Version
 * - Added the TransactionId Features
 * - Modified to align with the new XSD created for wholesale project
 *
 */
public class C30OrderIdentifierScript  extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30OrderIdentifierScript.class);
	
	private boolean outParam = false;
	
	public C30OrderIdentifierScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30OrderIdentifierScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException, C30ProvConnectorException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {

            
	    try {	

	    		String	serviceId = null;
				String	orderDesiredDate = null;
				String	actionWho=null; 
				String 	identifierTypeName=null;
				String  IdentifierId=null;
//				String  oldIdentifierId=null;
				
				orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
				actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());
	            log.info("orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);				
	            
			    //Check if the service object is empty.
			    if (order.getServiceObjectList() != null) {		       
			    	
			    	List<C30ServiceObject> serviceobjList =  order.getServiceObjectList().getServiceObject();
			    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
			    	
			    	for (C30ServiceObject service : serviceobjList) {
	                    //Get Client Id from service
	                    serviceId = service.getClientId();
	                    log.info("Checking Identifiers for serviceId : " +serviceId);
	                    //If the Identifier Object is populated.
						if (service.getIdentifierObjectList() != null)  {
				        
							List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
					    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());
		
							for (C30IdentifierObject identifier : ilList)  {
								
								//Set the Identifier Id for Change TN
								if ((service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) &&
										(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))) {
										serviceId = identifier.getIdentifierIdOut();
								} 
////								else if ((service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) &&
////										(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))) {
////									oldIdentifierId = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierIdOut());
////								}
								
								//Get the Identifier Type
								identifierTypeName = identifier.getIdentifierTypeName();
								if (identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
									//ADD - Identifier information to Billing
								    addIdentifierInBillingSystem(identifier, acctSegId, serviceId, transactionId);	}
								else if(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
									//DISCONNECT - Identifier information in Billing									
								    discIdentifierInBillingSystem(identifier, acctSegId, serviceId, transactionId);	}
								else if(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
									//SWAP - Identifier information in Billing									
									swapIdentifierInBillingSystem(identifier, acctSegId, serviceId, transactionId);}

								
								//If AWN ICCID Identifier Request
								if ((acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_ACS)) && 
										((identifierTypeName.equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) || 
										(identifierTypeName.equals(C30WPSQLConstants.C30_ICCID_VALUE)))) {

									HashMap<String, String> ProvEnquiry = new HashMap<String, String>();
									String 	ICCIDIdentifierIdIn = null;
									String 	ICCIDIdentifierIdOut = null;									
									String	IMSIIdentifierIdIn = null;
									String	IMSIIdentifierIdOut = null;
									
									ICCIDIdentifierIdIn=identifier.getIdentifierIdIn();
									ProvEnquiry.put("AcctSegId",acctSegId);
									ProvEnquiry.put("ICCID",ICCIDIdentifierIdIn);
									ProvEnquiry.put("RequestName", "IMSIVerifcation");		
									log.info("Send IMSI Inquiry for ICCID : "+ICCIDIdentifierIdIn);									
									C30ProvisionRequest request = C30ProvisionRequest.getInstance();
									HashMap response = request.awnInquiryRequest(ProvEnquiry) ;
									IMSIIdentifierIdIn = response.get("IMSI").toString();
									log.info("IMSI Retreived - IdentifierIdIn : "+IMSIIdentifierIdIn);
									log.info("Response XML : "+response.get("ResponseXML"));									
									identifier.setIdentifierIdIn(IMSIIdentifierIdIn);									
									identifier.setIdentifierTypeName("IMSI");
									
									if (IMSIIdentifierIdIn != null) {
										//Action for the Retrieved IMSI
										if (identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
											//ADD - IMSI Identifier to Billing
										    addIdentifierInBillingSystem(identifier, acctSegId, serviceId, transactionId);	}
										else if(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
											//DISCONNECT - IMSI Identifier to Billing											
										    discIdentifierInBillingSystem(identifier, acctSegId, serviceId, transactionId);	}
									} else {
										
										log.info("Retreived IMSI IS NULL ");
									}

									if(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
										ProvEnquiry.clear();
										ICCIDIdentifierIdOut=identifier.getIdentifierIdOut();
										ProvEnquiry.put("AcctSegId",acctSegId);
										ProvEnquiry.put("ICCID",ICCIDIdentifierIdOut);
										ProvEnquiry.put("RequestName", "IMSIVerifcation");	
										log.info("Send IMSI Inquiry for ICCID : "+ICCIDIdentifierIdOut);										
										C30ProvisionRequest oldSIMrequest = C30ProvisionRequest.getInstance();
										HashMap oldSIMresponse = oldSIMrequest.awnInquiryRequest(ProvEnquiry) ;
										IMSIIdentifierIdOut = oldSIMresponse.get("IMSI").toString();
										log.info("IMSI - IdentifierIdOut : "+IMSIIdentifierIdOut);	
										log.info("Response XML : "+oldSIMresponse.get("ResponseXML"));										
										identifier.setIdentifierIdOut(IMSIIdentifierIdOut);										
										identifier.setIdentifierTypeName("IMSI");
										
										if (IMSIIdentifierIdOut != null) {
											//SWAP Identifier
											swapIdentifierInBillingSystem(identifier, acctSegId, serviceId, transactionId);	
										}else {
											
											log.info("Retreived IMSI IS NULL ");
										}
										
									}//End - if(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))
								}								
																
							} //End - for (C30IdentifierObject identifier : ilList)
							
						}//End - if (service.getIdentifierObjectList() != null)
					    else {
					    	log.info("ERROR : Identifier Object Empty/NOT found");
					    	return;
					    }
	                }//End - for (C30ServiceObject service : serviceSubOrderList)
			    	
			    }//End - if (order.getServiceObjectList() != null)
			    else {
			    	log.info("ERROR : Service Object Empty/NOT found");
			    	return;
			    }
		}// End - try
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Identifier Order :"+e);
			e.printStackTrace();
			throw e;
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR - Identifier Order Prov Enquiry:"+e);
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-IMSI Enquiry Request "+e, e);
	    }
		catch(Exception e) {
			log.error("EXCP - Identifier Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Identifier Order Request Process", e );			
		}
	}//End - void process()


	/**This method is responsible for adding a Identifier in the Billing system
	 * 
	 * @param identifier
	 * @throws C30SDKToolkitException 
	 */
	private void addIdentifierInBillingSystem(C30IdentifierObject identifier, String acctSegId, String serviceId, 
			String transactionId) throws C30SDKToolkitException {

		String     actionDate=null;
		String     actionWho=null;		
		String     IdentifierId=null;
		String     IdentifierIdType=null;

		try {
			if (order.getOrderDesiredDate() != null)		{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)				{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
			if (identifier.getIdentifierIdIn() != null)  	{ IdentifierId = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierIdIn());}
			if (identifier.getIdentifierTypeName() != null)	{ IdentifierIdType = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierTypeName());}
	
			log.info("START - Invoking ADD Identifier SQL-API for ClientId : " + serviceId + " Identifier : " + IdentifierId + " Identifer Type : " + IdentifierIdType);
			
			//Add Identifier API
			C30WPAddIdentifierApi addIdentifier  = new C30WPAddIdentifierApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate, 		
					actionWho, IdentifierId, IdentifierIdType);
			
			log.info("END - Invoking ADD Identifier SQL-API for ClientId : " + serviceId + " Identifier : " + IdentifierId + " Identifer Type : " + IdentifierIdType);
			
			//Check the Output			
			if ((addIdentifier.getDataSource() != null) && (addIdentifier.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = addIdentifier.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Add Identifier OUT PARAM1 : " + outparam.keySet() + " Add Identifier OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Add Identifier OUT PARAM=true");					
					}
				}
			}//End - if ((addIdentifier.getDataSource() != null) && (addIdentifier.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("ERROR - Add Identifier : Data Source/Result Map IS NULL ");
			}//End - Else if ((addIdentifier.getDataSource() != null) && (addIdentifier.getDataSource().getResultMap() != null))

		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - addIdentifierInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - addIdentifierInBillingSystem(C30IdentifierObject identifier, String acctSegId)

	/**This method is responsible for disconnecting a Identifier in the Billing system
	 * 
	 * @param identifier
	 * @throws C30SDKToolkitException 
	 */
	private void discIdentifierInBillingSystem(C30IdentifierObject identifier, String acctSegId, String serviceId, 
			String transactionId) throws C30SDKToolkitException {

		String     actionDate=null;
		String     actionWho=null;		
		String     IdentifierId=null;
		String     IdentifierIdType=null;

		try {
			if (order.getOrderDesiredDate() != null)		{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)				{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
			if (identifier.getIdentifierIdIn() != null)  	{ IdentifierId = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierIdIn());}
			if (identifier.getIdentifierTypeName() != null) { IdentifierIdType = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierTypeName());}
	
			log.info("START - Invoking DISCONNECT Identifier SQL-API for ClientId : " + serviceId + " Identifier : " + IdentifierId + " Identifer Type : " + IdentifierIdType);			
			
			//Disconnect Identifier API
			C30WPDiscIdentifierApi discIdentifier  = new C30WPDiscIdentifierApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate, 		
					actionWho, IdentifierId, IdentifierIdType);
			log.info("END - Invoking DISCONNECT Identifier SQL-API for ClientId : " + serviceId + " Identifier : " + IdentifierId + " Identifer Type : " + IdentifierIdType);
			
			//Check the Output			
			if ((discIdentifier.getDataSource() != null) && (discIdentifier.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = discIdentifier.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Disconnect Identifier OUT PARAM1 : " + outparam.keySet() + " Disconnect Identifier OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Disconnect Identifier OUT PARAM=true");					
					}
				}
			}//End - if (discIdentifier.getDataSource() != null) && (discIdentifier.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("ERROR - Disc Identifier : Data Source/Result Map IS NULL ");
			}//End - Else if (discIdentifier.getDataSource() != null) && (discIdentifier.getDataSource().getResultMap() != null))
			
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - discIdentifierInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - discIdentifierInBillingSystem(C30IdentifierObject identifier, String acctSegId)

	
	/**This method is responsible for adding a Identifier in the Billing system
	 * 
	 * @param identifier
	 * @throws C30SDKToolkitException 
	 */
	private void swapIdentifierInBillingSystem(C30IdentifierObject identifier, String acctSegId, String serviceId, 
			String transactionId) throws C30SDKToolkitException {

		String	actionDate=null;
		String	actionWho=null;		
		String	newIdentifierId=null;		
		String	IdentifierIdType=null;
		String	oldIdentifierId=null;
		
		try {
			if (order.getOrderDesiredDate() != null)		{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)				{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
			if (identifier.getIdentifierIdOut() != null)  	{ oldIdentifierId = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierIdOut());}			
			if (identifier.getIdentifierIdIn() != null)  	{ newIdentifierId = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierIdIn());}			
			if (identifier.getIdentifierTypeName() != null) { IdentifierIdType = C30SqlApiUtils.convertJaxbElementToString(identifier.getIdentifierTypeName());}

	
			log.info("START - Invoking SWAP Identifier SQL-API for ClientId : " + serviceId + " Old Identifier : " + oldIdentifierId + 
					" New Identifier : " + newIdentifierId + " Identifer Type : " + IdentifierIdType);
			
			//Add Identifier API
			C30WPSwapIdentifierApi swapIdentifier  = new C30WPSwapIdentifierApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate, 		
					actionWho, oldIdentifierId, newIdentifierId, IdentifierIdType);
			
			log.info("END - Invoking SWAP Identifier SQL-API for ClientId : " + serviceId + " Old Identifier : " + oldIdentifierId + 
					" New Identifier : " + newIdentifierId + " Identifer Type : " + IdentifierIdType);

			
			//Check the Output			
			if ((swapIdentifier.getDataSource() != null) && (swapIdentifier.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = swapIdentifier.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Swap Identifier OUT PARAM1 : " + outparam.keySet() + " Swap Identifier OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Swap Identifier OUT PARAM=true");					
					}
				}
			}//End - if ((swapIdentifier.getDataSource() != null) && (swapIdentifier.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("ERROR - Swap Identifier : Data Source/Result Map IS NULL ");
			}//End - Else if ((swapIdentifier.getDataSource() != null) && (swapIdentifier.getDataSource().getResultMap() != null))

		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - swapIdentifierInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - swapIdentifierInBillingSystem(C30IdentifierObject identifier, String acctSegId, String serviceId, String transactionId, String orderDesiredDate, String actionWho)
	
	
	public boolean isOutParam()
	{
		return outParam;
	}

	public void setOutParam(boolean outParam)
	{
		this.outParam = outParam;
	}

} // End - public class C30OrderIdentifierScript
