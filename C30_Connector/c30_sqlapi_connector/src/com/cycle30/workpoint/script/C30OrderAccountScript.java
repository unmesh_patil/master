package com.cycle30.workpoint.script;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30AddressData;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.C30WPAddAccountApi;
import com.cycle30.workpoint.sqlapi.C30WPUpdateAccountApi;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/** This script works on the Account Object in the given XML
 * 
 * @author rnelluri
 * 
 * @author Umesh
 * - Updated the Functionality of Account Change Order
 * - Added the TransactionId Features 
 * - Modified to align with the new XSD created for wholesale project 
 * 
 * 
 */
public class C30OrderAccountScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30OrderAccountScript.class);

	private boolean outParam = false;
	
	public C30OrderAccountScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30OrderAccountScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String orderDesiredDate = null;
			String actionWho = null;
			
			// If the Account Object is populated.
			if (order.getAccount() != null)
			{
				orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
				actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());
				log.info("orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);	
				
				//If Order Date is null set the System date
				if (orderDesiredDate == null) {
		        	Date sysdate = new Date();
		        	GregorianCalendar gCal = new GregorianCalendar(); 
		        	gCal.setTime(sysdate); 
		        	XMLGregorianCalendar xgOrderDesiredate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal); 
					order.setOrderDesiredDate(xgOrderDesiredate);
					orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());					
				}//End - if (orderDesiredDate == null)	
				
	            log.info("orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);				
				
				log.info("Working on the account object");
				if(order.getAccount().getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
				{
					addAccountInBillingSystem(order.getAccount(), acctSegId, transactionId);
				}
				else if(order.getAccount().getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE))
				{
					updateAccountInBillingSystem(order.getAccount(), acctSegId, transactionId);
				}
			}//End - if (order.getAccount() != null)
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Account Order :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - Account Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Account Order Request Process", e );			
		}
	}

	/**This method is responsible for adding an Account in the Billing system
	 * 
	 * @param account
	 * @throws C30SDKToolkitException 
	 */
	private void addAccountInBillingSystem(C30AccountObject account, String acctSegId, String transactionId) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		String		acctId=null;
		String		actionDate=null;
		String		actionWho=null;		
		String		accountNumber=null;
		String		parentId=null;
		String		accountCategory=null;
		String		accountType=null;
		String		houseNumber=null;
		String		houseNumberSuffix=null;
		String		prefixDirectional=null;
		String		streetName=null;
		String		streetSuffix=null;
		String		postfixDirectional=null;
		String		unitType=null;
		String		unitNo=null;
		String		address1=null;
		String		address2=null;
		String		address3=null;
		String		city=null;
		String		county=null;
		String		postalCode=null;
		String		extendedPostalCode=null;
		String		state=null;
		String		countryCode=null;
		String		company=null;
		String		namePre=null;
		String		fname=null;
		String		mname=null;
		String		lname=null;
		String		nameGeneration=null;
		String		billPeriod=null;
		String		currencyCode=null;
		String		collectionIndicator=null;
		String		collectionStatus=null;
		String		languageCode=null;
		String		billDispMeth=null;
		String		billFmtOpt=null;
		String		insertGrpId=null;
		String		msgGrpId=null;
		String		mktCode=null;
		String		owningCostCtr=null;
		String		rateClass=null;
		String		regulatoryId=null;
		String		revRcvCostCtr=null;
		String		franchiseTaxCode=null;
		String		vertexGeoCode=null;
		String		vipCode=null;
		String		billingInquiryCenter=null;
		String		serviceInquiryCenter=null;
		String		remittanceCenter=null;
		String		returnMailCenter=null;
		
		try{
			if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
			if (account.getClientId() != null)  		{ acctId = C30SqlApiUtils.convertJaxbElementToString(account.getClientId());}
			if (account.getAccountNumber() != null)  	{ accountNumber = C30SqlApiUtils.convertJaxbElementToString(account.getAccountNumber());}
			if (account.getParentId() != null)  		{ parentId = C30SqlApiUtils.convertJaxbElementToString(account.getParentId());}
			if (account.getAccountCategory() != null)	{ accountCategory = C30SqlApiUtils.convertJaxbElementToString(account.getAccountCategory());}
			if (account.getAccountType() != null)  		{ accountType = C30SqlApiUtils.convertJaxbElementToString(account.getAccountType());}
			
			if (account.getBillingAddress() != null){
			   C30AddressData billingAddress = account.getBillingAddress().getValue();
			   if (billingAddress != null) {
				if (billingAddress.getHouseNumber() != null)  		{ houseNumber = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getHouseNumber());}
				if (billingAddress.getHouseNumberSuffix() != null)  { houseNumberSuffix = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getHouseNumberSuffix());}
				if (billingAddress.getPrefixDirectional() != null)  { prefixDirectional = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getPrefixDirectional());}
				if (billingAddress.getStreetName() != null)  		{ streetName = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getStreetName());}
				if (billingAddress.getStreetSuffix() != null)  		{ streetSuffix = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getStreetSuffix());}
				if (billingAddress.getPostfixDirectional() != null)	{ postfixDirectional = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getPostfixDirectional());}
				if (billingAddress.getUnitType() != null)  			{ unitType = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getUnitType());}
				if (billingAddress.getUnitNo() != null)  			{ unitNo = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getUnitNo());}
				if (billingAddress.getAddress1() != null)  			{ address1 = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getAddress1());}
				if (billingAddress.getAddress2() != null)  			{ address2 = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getAddress2());}
				if (billingAddress.getAddress3() != null)  			{ address3 = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getAddress3());}
				if (billingAddress.getCity().toString() != null)  	{ city = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getCity().toString());}
				if (billingAddress.getCounty() != null)  			{ county = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getCounty());}
				if (billingAddress.getState() != null)  			{ state = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getState());}
				if (billingAddress.getPostalCode() != null)  		{ postalCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getPostalCode());}
				if (billingAddress.getExtendedPostalCode() != null)	{ extendedPostalCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getExtendedPostalCode());}
				if (billingAddress.getCountryCode() != null)  		{ countryCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getCountryCode());}
				if (billingAddress.getVertexGeoCode() != null) 		{ vertexGeoCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getVertexGeoCode());}
			   }//End - if (billingAddress != null)
			}//End - if (account.getBillingAddress() != null)		
			
			if (account.getCompany() != null)				{ company = C30SqlApiUtils.convertJaxbElementToString(account.getCompany());}
			if (account.getNamePre() != null)				{ namePre = C30SqlApiUtils.convertJaxbElementToString(account.getNamePre());}
			if (account.getFname() != null)  				{ fname = C30SqlApiUtils.convertJaxbElementToString(account.getFname());}
			if (account.getMname() != null)  				{ mname = C30SqlApiUtils.convertJaxbElementToString(account.getMname());}
			if (account.getLname() != null)  				{ lname = C30SqlApiUtils.convertJaxbElementToString(account.getLname());}
			if (account.getNameGeneration() != null)		{ nameGeneration = C30SqlApiUtils.convertJaxbElementToString(account.getNameGeneration());}
			if (account.getBillPeriod() != null)  			{ billPeriod = C30SqlApiUtils.convertJaxbElementToString(account.getBillPeriod());}
			if (account.getCurrencyCode() != null)  		{ currencyCode = C30SqlApiUtils.convertJaxbElementToString(account.getCurrencyCode());}
			if (account.getCollectionIndicator() != null)	{ collectionIndicator = C30SqlApiUtils.convertJaxbElementToString(account.getCollectionIndicator());}
			if (account.getCollectionStatus() != null)  	{ collectionStatus = C30SqlApiUtils.convertJaxbElementToString(account.getCollectionStatus());}
			if (account.getLanguageCode() != null)  		{ languageCode = C30SqlApiUtils.convertJaxbElementToString(account.getLanguageCode());}
			if (account.getBillDispMeth() != null)  		{ billDispMeth = C30SqlApiUtils.convertJaxbElementToString(account.getBillDispMeth());}
			if (account.getBillFmtOpt() != null)  			{ billFmtOpt = C30SqlApiUtils.convertJaxbElementToString(account.getBillFmtOpt());}
			if (account.getInsertGrpId() != null)  			{ insertGrpId = C30SqlApiUtils.convertJaxbElementToString(account.getInsertGrpId());}
			if (account.getMsgGrpId() != null)  			{ msgGrpId = C30SqlApiUtils.convertJaxbElementToString(account.getMsgGrpId());}
			if (account.getMktCode() != null)  				{ mktCode = C30SqlApiUtils.convertJaxbElementToString(account.getMktCode());}
			if (account.getOwningCostCtr() != null)  		{ owningCostCtr = C30SqlApiUtils.convertJaxbElementToString(account.getOwningCostCtr());}
			if (account.getRateClass() != null)  			{ rateClass = C30SqlApiUtils.convertJaxbElementToString(account.getRateClass());}
			if (account.getRegulatoryId() != null)  		{ regulatoryId = C30SqlApiUtils.convertJaxbElementToString(account.getRegulatoryId());}
			if (account.getRevRcvCostCtr() != null)  		{ revRcvCostCtr = C30SqlApiUtils.convertJaxbElementToString(account.getRevRcvCostCtr());}
			if (account.getFranchiseTaxCode() != null)  	{ franchiseTaxCode = C30SqlApiUtils.convertJaxbElementToString(account.getFranchiseTaxCode());}
			if (account.getVipCode() != null)  				{ vipCode = C30SqlApiUtils.convertJaxbElementToString(account.getVipCode());}
			if (account.getBillingInquiryCenter() != null)  { billingInquiryCenter = C30SqlApiUtils.convertJaxbElementToString(account.getBillingInquiryCenter());}
			if (account.getServiceInquiryCenter() != null)  { serviceInquiryCenter = C30SqlApiUtils.convertJaxbElementToString(account.getServiceInquiryCenter());}
			if (account.getRemittanceCenter() != null)  	{ remittanceCenter = C30SqlApiUtils.convertJaxbElementToString(account.getRemittanceCenter());}
			if (account.getReturnMailCenter() != null)  	{ returnMailCenter = C30SqlApiUtils.convertJaxbElementToString(account.getReturnMailCenter());}

			log.info("The following are the account fields");
			
			log.info(transactionId+ " , "+ acctSegId.toString()+ " , "+
					acctId+ " , " +			    actionDate+ " , " +			    actionWho+ " , " +
					accountNumber+ " , " +
					parentId+ " , " +			    accountCategory+ " , " +			    accountType+ " , " +			houseNumber+ " , " +			houseNumberSuffix+ " , " +
					prefixDirectional+ " , " +			streetName+ " , " +			streetSuffix+ " , " +			postfixDirectional+ " , " +			unitType+ " , " +
					unitNo+ " , " +			address1+ " , " +			address2+ " , " +			address3+ " , " +			city+ " , " +			county+ " , " +
					postalCode+ " , " +			extendedPostalCode+ " , " +			state+ " , " +			countryCode+ " , " +			company+ " , " +			    namePre+ " , " +
					fname+ " , " +			    mname+ " , " +			    lname+ " , " +			    nameGeneration+ " , " +			    billPeriod+ " , " +			    currencyCode+ " , " +
					collectionIndicator+ " , " +			    collectionStatus+ " , " +			    languageCode+ " , " +			    billDispMeth+ " , " +			    billFmtOpt+ " , " +
					insertGrpId+ " , " +			    msgGrpId+ " , " +			    mktCode+ " , " +			    owningCostCtr+ " , " +			    rateClass+ " , " +			    regulatoryId+ " , " +
					revRcvCostCtr+ " , " +			    franchiseTaxCode+ " , " +			vertexGeoCode+ " , " +    vipCode+ " , " +			    billingInquiryCenter+ " , " +			    serviceInquiryCenter+ " , " +
					remittanceCenter+ " , " +			    returnMailCenter+ " , " + " and ExtendedData");
	
			// The ActionDate and ActionDesiredDate are now being driven by OrderRequest/OrderDesiredDate to support consistent back-dating(explicit dating) - #380		
			log.info("START - Invoking ADD Account SQL-API for AccountId : "+acctId);		
			C30WPAddAccountApi addAct  = new C30WPAddAccountApi(conn, transactionId, acctSegId.toString(),
					acctId, actionDate, actionWho,
					parentId, accountCategory, accountType, houseNumber, houseNumberSuffix,
					prefixDirectional, streetName, streetSuffix, postfixDirectional, unitType,
					unitNo, address1, address2, address3, city, county,
					state, postalCode, extendedPostalCode, countryCode, company, namePre,
					fname, mname, lname, nameGeneration, billPeriod, currencyCode,
					collectionIndicator, collectionStatus, languageCode, billDispMeth, billFmtOpt,
					insertGrpId, msgGrpId, mktCode, owningCostCtr, rateClass, regulatoryId,
					revRcvCostCtr, franchiseTaxCode, vertexGeoCode, vipCode, billingInquiryCenter, serviceInquiryCenter,
					remittanceCenter, returnMailCenter, account.getExtendedDataList().getValue());
			log.info("END - Invoking ADD Account SQL-API for AccountId : "+acctId);

			//Check the Output			
			if ((addAct.getDataSource() != null) && (addAct.getDataSource().getResultMap() != null)) {			
				Iterator<?> itr = addAct.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Add ACT OUT PARAM1 : " + outparam.keySet() + " Add ACT OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Add ACT OUT PARAM=true");					
					}
				}
			}//End - if ((addAct.getDataSource() != null) && (addAct.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("ERROR - DataSource/Result Map IS NULL ");
			}//End - Else if ((addAct.getDataSource() != null) && (addAct.getDataSource().getResultMap() != null))
			
		}// End - Try
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - addAccountInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	}

	/**This method is responsible for updating an Account in the Billing system
	 * 
	 * @param account
	 * @throws C30SDKToolkitException 
	 */
	private void updateAccountInBillingSystem(C30AccountObject account, String acctSegId, String transactionId) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		String		actionDate=null;
		String		actionWho=null;		
		String		acctId=null;
		String		noBill=null;		
		String		parentId=null;
		String		accountCategory=null;
		String		accountType=null;
		String		houseNumber=null;
		String		houseNumberSuffix=null;
		String		prefixDirectional=null;
		String		streetName=null;
		String		streetSuffix=null;
		String		postfixDirectional=null;
		String		unitType=null;
		String		unitNo=null;
		String		address1=null;
		String		address2=null;
		String		address3=null;
		String		city=null;
		String		county=null;
		String		state=null;
		String		postalCode=null;
		String		extendedPostalCode=null;
		String		countryCode=null;
		String		company=null;
		String		namePre=null;
		String		fname=null;
		String		minit=null;
		String		lname=null;
		String		nameGeneration=null;
		String		billPeriod=null;
		String		collectionIndicator=null;
		String		collectionStatus=null;
		String		languageCode=null;
		String		billDispMeth=null;
		String		billFmtOpt=null;
		String		insertGrpId=null;
		String		msgGrpId=null;
		String		mktCode=null;
		String		rateClass=null;
		String		regulatoryId=null;
		String		revRcvCostCtr=null;
		String		franchiseTaxCode=null;
		String		vertexGeoCode=null;
		String		vipCode=null;
		String		billingInquiryCenter=null;
		String		serviceInquiryCenter=null;
		String		remittanceCenter=null;
		String		returnMailCenter=null;

		try{
			if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
			if (account.getClientId() != null)  		{ acctId = C30SqlApiUtils.convertJaxbElementToString(account.getClientId());}
			if (account.getParentId() != null)  		{ parentId = C30SqlApiUtils.convertJaxbElementToString(account.getParentId());}
			if (account.getAccountCategory() != null)  	{ accountCategory = C30SqlApiUtils.convertJaxbElementToString(account.getAccountCategory());}
			if (account.getAccountType() != null)  		{ accountType = C30SqlApiUtils.convertJaxbElementToString(account.getAccountType());}
			if(account.getBillingAddress() != null)
			{
				C30AddressData billingAddress = account.getBillingAddress().getValue();
				if (billingAddress.getHouseNumber() != null)  		{ houseNumber = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getHouseNumber());}
				if (billingAddress.getHouseNumberSuffix() != null)  { houseNumberSuffix = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getHouseNumberSuffix());}
				if (billingAddress.getPrefixDirectional() != null)  { prefixDirectional = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getPrefixDirectional());}
				if (billingAddress.getStreetName() != null)  		{ streetName = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getStreetName());}
				if (billingAddress.getStreetSuffix() != null)  		{ streetSuffix = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getStreetSuffix());}
				if (billingAddress.getPostfixDirectional() != null)	{ postfixDirectional = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getPostfixDirectional());}
				if (billingAddress.getUnitType() != null)  			{ unitType = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getUnitType());}
				if (billingAddress.getUnitNo() != null)  			{ unitNo = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getUnitNo());}
				if (billingAddress.getAddress1() != null)  			{ address1 = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getAddress1());}
				if (billingAddress.getAddress2() != null)  			{ address2 = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getAddress2());}
				if (billingAddress.getAddress3() != null)  			{ address3 = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getAddress3());}
				if (billingAddress.getCity().toString() != null)	{ city = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getCity().toString());}
				if (billingAddress.getCounty() != null)  			{ county = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getCounty());}
				if (billingAddress.getState() != null)  			{ state = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getState());}
				if (billingAddress.getPostalCode() != null)  		{ postalCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getPostalCode());}
				if (billingAddress.getExtendedPostalCode() != null)	{ extendedPostalCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getExtendedPostalCode());}
				if (billingAddress.getCountryCode() != null)  		{ countryCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getCountryCode());}
				if (billingAddress.getVertexGeoCode() != null)  	{ vertexGeoCode = C30SqlApiUtils.convertJaxbElementToString(billingAddress.getVertexGeoCode());}			
			}		
			if (account.getCompany() != null)  				{ company = C30SqlApiUtils.convertJaxbElementToString(account.getCompany());}
			if (account.getNamePre() != null)  				{ namePre = C30SqlApiUtils.convertJaxbElementToString(account.getNamePre());}
			if (account.getFname() != null)  				{ fname = C30SqlApiUtils.convertJaxbElementToString(account.getFname());}
			if (account.getMname() != null)  				{ minit = C30SqlApiUtils.convertJaxbElementToString(account.getMname());}
			if (account.getLname() != null)  				{ lname = C30SqlApiUtils.convertJaxbElementToString(account.getLname());}
			if (account.getNameGeneration() != null)  		{ nameGeneration = C30SqlApiUtils.convertJaxbElementToString(account.getNameGeneration());}
			if (account.getBillPeriod() != null)  			{ billPeriod = C30SqlApiUtils.convertJaxbElementToString(account.getBillPeriod());}
			if (account.getCollectionIndicator() != null)	{ collectionIndicator = C30SqlApiUtils.convertJaxbElementToString(account.getCollectionIndicator());}
			if (account.getCollectionStatus() != null)  	{ collectionStatus = C30SqlApiUtils.convertJaxbElementToString(account.getCollectionStatus());}
			if (account.getLanguageCode() != null)  		{ languageCode = C30SqlApiUtils.convertJaxbElementToString(account.getLanguageCode());}
			if (account.getBillDispMeth() != null)  		{ billDispMeth = C30SqlApiUtils.convertJaxbElementToString(account.getBillDispMeth());}
			if (account.getBillFmtOpt() != null)  			{ billFmtOpt = C30SqlApiUtils.convertJaxbElementToString(account.getBillFmtOpt());}
			if (account.getInsertGrpId() != null)  			{ insertGrpId = C30SqlApiUtils.convertJaxbElementToString(account.getInsertGrpId());}
			if (account.getMsgGrpId() != null)  			{ msgGrpId = C30SqlApiUtils.convertJaxbElementToString(account.getMsgGrpId());}
			if (account.getMktCode() != null)  				{ mktCode = C30SqlApiUtils.convertJaxbElementToString(account.getMktCode());}
			if (account.getRateClass() != null)  			{ rateClass = C30SqlApiUtils.convertJaxbElementToString(account.getRateClass());}
			if (account.getRegulatoryId() != null)  		{ regulatoryId = C30SqlApiUtils.convertJaxbElementToString(account.getRegulatoryId());}
			if (account.getRevRcvCostCtr() != null)  		{ revRcvCostCtr = C30SqlApiUtils.convertJaxbElementToString(account.getRevRcvCostCtr());}
			if (account.getFranchiseTaxCode() != null)  	{ franchiseTaxCode = C30SqlApiUtils.convertJaxbElementToString(account.getFranchiseTaxCode());}
			if (account.getVipCode() != null)  				{ vipCode = C30SqlApiUtils.convertJaxbElementToString(account.getVipCode());}
			if (account.getBillingInquiryCenter() != null)  { billingInquiryCenter = C30SqlApiUtils.convertJaxbElementToString(account.getBillingInquiryCenter());}
			if (account.getServiceInquiryCenter() != null)  { serviceInquiryCenter = C30SqlApiUtils.convertJaxbElementToString(account.getServiceInquiryCenter());}
			if (account.getRemittanceCenter() != null)  	{ remittanceCenter = C30SqlApiUtils.convertJaxbElementToString(account.getRemittanceCenter());}
			if (account.getReturnMailCenter() != null)  	{ returnMailCenter = C30SqlApiUtils.convertJaxbElementToString(account.getReturnMailCenter());}
			noBill = "0";
			
			log.info("The following are the update account fields");
			log.info(transactionId+ " , "  + acctSegId.toString()+ " , "  +
					acctId+ " , " +			    actionDate+ " , " +			    actionWho+ " , " +
					parentId+ " , " +			    accountCategory+ " , " +			    accountType+ " , " +			houseNumber+ " , " +			houseNumberSuffix+ " , " +
					prefixDirectional+ " , " +			streetName+ " , " +			streetSuffix+ " , " +			postfixDirectional+ " , " +			unitType+ " , " +
					unitNo+ " , " +			address1+ " , " +			address2+ " , " +			address3+ " , " +			city+ " , " +			county+ " , " +
					postalCode+ " , " +			extendedPostalCode+ " , " +			state+ " , " +			countryCode+ " , " +			company+ " , " +			    namePre+ " , " +
					fname+ " , " +			    minit+ " , " +			    lname+ " , " +			    nameGeneration+ " , " +			    billPeriod+ " , " +
					collectionIndicator+ " , " +			    collectionStatus+ " , " +			    languageCode+ " , " +			    billDispMeth+ " , " +			    billFmtOpt+ " , " +
					insertGrpId+ " , " +			    msgGrpId+ " , " +			    mktCode+ " , "  +			    rateClass+ " , " +			    regulatoryId+ " , " +
					revRcvCostCtr+ " , " +			    franchiseTaxCode+ " , " +			vertexGeoCode+ " , " +    vipCode+ " , " +			    billingInquiryCenter+ " , " +			    serviceInquiryCenter+ " , " +
					remittanceCenter+ " , " +			    returnMailCenter+ " , " + " and ExtendedData");
	
			log.info("START - Invoking UPDATE Account SQL-API for AccountId : "+acctId);		
			C30WPUpdateAccountApi updAcct  = new C30WPUpdateAccountApi(conn, transactionId, acctSegId.toString(), acctId, actionDate, actionWho, noBill,
					parentId, accountCategory, accountType, houseNumber, houseNumberSuffix,
					prefixDirectional, streetName, streetSuffix, postfixDirectional, unitType, unitNo, address1, address2, address3, city, county,
					state, postalCode, extendedPostalCode, countryCode, company, namePre, fname, minit, lname, nameGeneration, billPeriod,
					collectionIndicator, collectionStatus, languageCode, billDispMeth, billFmtOpt,
					insertGrpId, msgGrpId, mktCode, rateClass, regulatoryId,
					revRcvCostCtr, franchiseTaxCode, vertexGeoCode, vipCode, billingInquiryCenter, serviceInquiryCenter,
					remittanceCenter, returnMailCenter, account.getExtendedDataList().getValue());
			log.info("END - Invoking UPDATE Account SQL-API for AccountId : "+acctId);	

			//Check the Output			
			if ((updAcct.getDataSource() != null) && (updAcct.getDataSource().getResultMap() != null)) {			
				Iterator<?> itr = updAcct.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Update ACT OUT PARAM1 : " + outparam.keySet() + " Update ACT OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Update ACT OUT PARAM=true");					
					}
				}
			}//End - if ((updAcct.getDataSource() != null) && (updAcct.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("WARN - UPDATE ACT Data Source/Result Map IS NULL ");
			}//End - else if ((updAcct.getDataSource() != null) && (updAcct.getDataSource().getResultMap() != null))
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - updateAccountInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	
	}
	
	public boolean isOutParam()
	{
		return outParam;
	}

	public void setOutParam(boolean outParam)
	{
		this.outParam = outParam;
	}
 
}
