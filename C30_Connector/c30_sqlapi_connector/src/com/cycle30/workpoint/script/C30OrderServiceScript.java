package com.cycle30.workpoint.script;

import java.sql.Connection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import com.cycle30.sdk.schema.jaxb.C30AddressData;
import com.cycle30.sdk.schema.jaxb.C30EmfConfig;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest.ServiceObjectList;
import com.cycle30.workpoint.sqlapi.C30WPAddServiceApi;
import com.cycle30.workpoint.sqlapi.C30WPDiscServiceApi;
import com.cycle30.workpoint.sqlapi.C30WPUpdateServiceApi;
import com.cycle30.workpoint.sqlapi.C30WPSuspendServiceApi;
import com.cycle30.workpoint.sqlapi.C30WPResumeServiceApi;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/** This script works on the service object in the given xml
 * 
 * @author Umesh
 * - Created for Wholesale Project
 * 
 */
public class C30OrderServiceScript  extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30OrderServiceScript.class);
	
	private boolean outParam = false;	
	
	public C30OrderServiceScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30OrderServiceScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	/** This is used to process the Service Object of the Payload extracted from the 
	 * given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		 
		try  {	

				String	orderDesiredDate = null;
				String	actionWho=null; 
				
				orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
				actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());
	            log.info("orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);	
	            
				//If Order Date is null set the sysdate
				if (orderDesiredDate == null) {
		        	Date sysdate = new Date();
		        	GregorianCalendar gCal = new GregorianCalendar(); 
		        	gCal.setTime(sysdate); 
		        	XMLGregorianCalendar xgOrderDesiredate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal); 
					order.setOrderDesiredDate(xgOrderDesiredate);
					orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());					
				}				
	            log.info("orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);				
	            
			    //Check if the service object is empty.
			    if (order.getServiceObjectList() != null) {
				    
			    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
			    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
			    	for (C30ServiceObject service : serviceobjList)  {
					    
					    String clientId = service.getClientId();
					    String actionType = service.getClientActionType().toString();
				    
			            log.info("Sub order id: '" + subOrderId + "'  Service Client id: '" + clientId + "'  Action type: '" + actionType + "'");
						if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
							//ADD - Service in Billing System
						    addServiceInBillingSystem(service, acctSegId, transactionId);	}
						else if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
							//DISCONNECT - Service in Billing System
						    discServiceInBillingSystem(service, acctSegId, transactionId);	}
						else if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {
							//UPDATE - Service in Billing System
						    updateServiceInBillingSystem(service, acctSegId, transactionId);	}
						else if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SUSPEND)) {
							//SUSPEND - Service in Billing System
						    suspendServiceInBillingSystem(service, acctSegId, transactionId);	}
						else if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_RESUME)) {
							//RESUME - Service in Billing System
						    resumeServiceInBillingSystem(service, acctSegId, transactionId);	}
	
				    } // End - for (C30ServiceObject service : plList)
	
				} // End - if (order.getServiceObjectList() != null)
			    else {
			    	log.info("ERROR : Service Object Empty/NOT found");
		            return;
			    }
			} 
			catch(C30SDKToolkitException e) {
			    log.error("ERROR - Service Order :"+e);
				e.printStackTrace();
				throw e;
			}
			catch(Exception e) {
				log.error("EXCP - Service Order :"+e);	                
				e.printStackTrace();
				throw new C30SDKToolkitException("EXCP: ORDER-Service Order Request Process", e );			
			}
	}//End - void process()


	/**This method is responsible for Adding a Service in the Billing system
	 * 
	 * @param service
	 * @throws C30SDKToolkitException 
	 */
	private void addServiceInBillingSystem(C30ServiceObject service, String acctSegId, String transactionId) throws C30SDKToolkitException {

		log.info("Adding service in billing system...");
		String		serviceId=null;
		String     	actionDate=null;
		String	   	actionWho=null;	
		String     	acctId=null;
		String     	emfConfigId=null;
		String     	rateClass=null;
		String     	revRcvCostCtr=null;
		String     	currencyCode=null;
		String     	timeZone=null;
		String     	franchiseTaxCode=null;
		String     	vertexGeoCode=null;
		String     	houseNumber=null;
		String     	houseNumberSuffix=null;
		String     	prefixDirectional=null;
		String     	streetName=null;
		String     	streetSuffix=null;
		String     	postfixDirectional=null;
		String     	unitType=null;
		String     	unitNo=null;
		String     	city=null;
		String     	county=null;
		String     	state=null;
		String     	postalCode=null;
		String     	extendedPostalCode=null;
		String     	countryCode=null;
		String		connectReason=null;
		C30ExtendedDataList extDatList=null;
		
		try{
			if (service.getClientId() != null)           			{	serviceId = service.getClientId();}
			if (order.getOrderDesiredDate() != null)				{	actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)						{	actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}
			if (service.getClientAccountId() != null)    			{	acctId = C30SqlApiUtils.convertJaxbElementToString(service.getClientAccountId());}
			if (service.getEmfConfig() != null) {			
				if (service.getEmfConfig().getEmfConfigName() != null)	{	emfConfigId = C30SqlApiUtils.convertJaxbElementToString(service.getEmfConfig().getEmfConfigName());}				
			}
			if (service.getRateClass() != null)         			{	rateClass = C30SqlApiUtils.convertJaxbElementToString(service.getRateClass());}
			if (service.getRevRcvCostCtr() != null)     			{	revRcvCostCtr = C30SqlApiUtils.convertJaxbElementToString(service.getRevRcvCostCtr());}
			if (service.getCurrencyCode() != null)      			{	currencyCode = C30SqlApiUtils.convertJaxbElementToString(service.getCurrencyCode());}
			if (service.getTimeZone() != null)          			{	timeZone = C30SqlApiUtils.convertJaxbElementToString(service.getTimeZone());}
			if (service.getFranchiseTaxCode() != null)  			{	franchiseTaxCode = C30SqlApiUtils.convertJaxbElementToString(service.getFranchiseTaxCode());}
			if (service.getServiceAddress() != null)
			{
			   C30AddressData serviceAddress = service.getServiceAddress().getValue();
			   if (serviceAddress != null) {
				if (serviceAddress.getVertexGeoCode() != null)      {  vertexGeoCode = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getVertexGeoCode());}
				if (serviceAddress.getHouseNumber() != null)        {  houseNumber = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getHouseNumber());}
				if (serviceAddress.getHouseNumberSuffix() != null)  {  houseNumberSuffix = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getHouseNumberSuffix());}
				if (serviceAddress.getPrefixDirectional() != null)  {  prefixDirectional = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getPrefixDirectional());}
				if (serviceAddress.getStreetName() != null)         {  streetName = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getStreetName());}
				if (serviceAddress.getStreetSuffix() != null)       {  streetSuffix = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getStreetSuffix());}
				if (serviceAddress.getPostfixDirectional() != null) {  postfixDirectional = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getPostfixDirectional());}
				if (serviceAddress.getUnitType() != null)           {  unitType = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getUnitType());}
				if (serviceAddress.getUnitNo() != null)             {  unitNo = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getUnitNo());}
				if (serviceAddress.getCity() != null)    			{  city = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getCity());}
				if (serviceAddress.getCounty() != null)             {  county = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getCounty());}
				if (serviceAddress.getState() != null)              {  state = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getState());}
				if (serviceAddress.getPostalCode() != null)         {  postalCode = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getPostalCode());}
				if (serviceAddress.getExtendedPostalCode() != null) {  extendedPostalCode = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getExtendedPostalCode());}
				if (serviceAddress.getCountryCode() != null)        {  countryCode = C30SqlApiUtils.convertJaxbElementToString(serviceAddress.getCountryCode());}
			   }
			   
			}
			if (service.getClientActionTypeReason() != null)		{ connectReason = service.getClientActionTypeReason();}
			if (service.getExtendedDataList() != null) {
				extDatList=service.getExtendedDataList().getValue();
			}

			log.info("START - Invoking ADD Service SQL-API for ClientId : "+serviceId);		
			
			//Add Service API
			C30WPAddServiceApi addService = new C30WPAddServiceApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate, actionWho, acctId,
			emfConfigId, rateClass, revRcvCostCtr, currencyCode, timeZone, franchiseTaxCode, vertexGeoCode, houseNumber, houseNumberSuffix,
			prefixDirectional, streetName, streetSuffix, postfixDirectional, unitType, unitNo, city, county, state, postalCode,
			extendedPostalCode, countryCode, connectReason, extDatList);	
			
			log.info("END - Invoking ADD Service SQL-API for ClientId : "+serviceId);	
			
			//Check the Output			
			if ((addService.getDataSource() != null) && (addService.getDataSource().getResultMap() != null)) {			
				Iterator<?> itr = addService.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Add Service OUT PARAM1 : " + outparam.keySet() + " Add Service OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Add Service OUT PARAM=true");					
					}
				}
			}//End - if ((addService.getDataSource() != null) && (addService.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("ERROR - Add Service : Data Source/Result Map IS NULL ");
			}//End - Else if ((addService.getDataSource() != null) && (addService.getDataSource().getResultMap() != null))
			
		}//End - Try
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - addServiceInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - private void addServiceInBillingSystem(C30ServiceObject service, String acctSegId) throws C30SDKToolkitException

	/**This method is responsible for Disconnecting a Service in the Billing system
	 * 
	 * @param service
	 * @throws C30SDKToolkitException 
	 */
	private void discServiceInBillingSystem(C30ServiceObject service, String acctSegId, String transactionId) throws C30SDKToolkitException {

		log.info("Disconnecting service in billing system...");
		String		serviceId=null;
		String		actionDate=null;
		String		disconnectReason=null;
		String 		actionWho=null;
		C30ExtendedDataList extDatList=null;
		
		try {
				if (order.getOrderDesiredDate() != null)				{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
				if (order.getActionWho() != null)						{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}				
				if (service.getClientId() != null)           			{ serviceId = service.getClientId();}
				if (service.getClientActionTypeReason() != null)		{ disconnectReason = service.getClientActionTypeReason();}	
				if (service.getExtendedDataList() != null) {
					extDatList=service.getExtendedDataList().getValue();
				}
				
				
			   log.info("START - Invoking DISCONNECT Service SQL-API for ClientId : "+serviceId+" Disconnect Reason :"+disconnectReason);		
			   C30WPDiscServiceApi discService  = new C30WPDiscServiceApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate, actionWho, 
					   disconnectReason, extDatList);
			   log.info("END - Invoking DISCONNECT Service SQL-API for ClientId : "+serviceId+" Disconnect Reason :"+disconnectReason);	

				//Check the Output			
				if ((discService.getDataSource() != null) && (discService.getDataSource().getResultMap() != null)) {			   
					Iterator<?> itr = discService.getDataSource().getResultMap().values()
							.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("DISC Service OUT PARAM1 : " + outparam.keySet() + " DISC Service OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("DISC Service OUT PARAM=true");					
						}
					}
				}//End - if ((discService.getDataSource() != null) && (discService.getDataSource().getResultMap() != null))
				else { 
					this.outParam = false;
					log.info("ERROR - Disconnect Service : Data Source/Result Map IS NULL ");
				}//End - Else if ((discService.getDataSource() != null) && (discService.getDataSource().getResultMap() != null))

			}
			catch(C30SDKToolkitException e) {
		    log.error("ERROR - discServiceInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	
	}//End - void discServiceInBillingSystem(C30ServiceObject service, String acctSegId)

	/**This method is responsible for Suspending a Service in the Billing system
	 * 
	 * @param service
	 * @throws C30SDKToolkitException 
	 */
	private void suspendServiceInBillingSystem(C30ServiceObject service, String acctSegId, String transactionId) throws C30SDKToolkitException {

		log.info("Suspending service in billing system...");
		String		serviceId=null;
		String		actionDate=null;
		String		suspendReason=null;
		String 		actionWho=null;		

		try {
				if (order.getOrderDesiredDate() != null)				{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
				if (order.getActionWho() != null)						{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}				
				if (service.getClientId() != null)           			{ serviceId = service.getClientId();}
				if (service.getClientActionTypeReason() != null)		{ suspendReason = service.getClientActionTypeReason();}	
				
			   log.info("START - Invoking SUSPEND Service SQL-API for ClientId : "+serviceId);		
			   C30WPSuspendServiceApi suspService  = new C30WPSuspendServiceApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate, actionWho, suspendReason);
			   log.info("END - Invoking SUSPEND Service SQL-API for ClientId : "+serviceId);		

				//Check the Output			
				if ((suspService.getDataSource() != null) && (suspService.getDataSource().getResultMap() != null)) {			   
					Iterator<?> itr = suspService.getDataSource().getResultMap().values()
							.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("SUSPEND Service OUT PARAM1 : " + outparam.keySet() + " SUSPEND Service OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("SUSPEND Service OUT PARAM=true");					
						}
					}
				}//End - if ((suspService.getDataSource() != null) && (suspService.getDataSource().getResultMap() != null))
				else { 
					this.outParam = false;
					log.info("ERROR - Suspend Service : Data Source/Result Map IS NULL ");
				}//End - Else if ((suspService.getDataSource() != null) && (suspService.getDataSource().getResultMap() != null))

			}
			catch(C30SDKToolkitException e) {
		    log.error("ERROR - suspendServiceInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	
	}//End - void suspendServiceInBillingSystem(C30ServiceObject service, String acctSegId, String transactionId, String orderDesiredDate, String actionWho)

	
	/**This method is responsible for Resume a Service in the Billing system
	 * 
	 * @param service
	 * @throws C30SDKToolkitException 
	 */
	private void resumeServiceInBillingSystem(C30ServiceObject service, String acctSegId, String transactionId) throws C30SDKToolkitException {

		log.info("Resuming a service in billing system...");
		String		serviceId=null;
		String		actionDate=null;
		String		resumeReason=null;
		String 		actionWho=null;
		try {
				if (order.getOrderDesiredDate() != null)				{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
				if (order.getActionWho() != null)						{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}				
				if (service.getClientId() != null)           			{ serviceId = service.getClientId();}
				if (service.getClientActionTypeReason() != null)		{ resumeReason = service.getClientActionTypeReason();}
				
			   log.info("START - Invoking RESUME Service SQL-API for ClientId : "+serviceId);		
			   C30WPResumeServiceApi resumeService  = new C30WPResumeServiceApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate, actionWho, resumeReason);
			   log.info("END - Invoking RESUME Service SQL-API for ClientId : "+serviceId);		

				//Check the Output			
				if ((resumeService.getDataSource() != null) && (resumeService.getDataSource().getResultMap() != null)) {			   
					Iterator<?> itr = resumeService.getDataSource().getResultMap().values()
							.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("RESUME Service OUT PARAM1 : " + outparam.keySet() + " RESUME Service OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("RESUME Service OUT PARAM=true");					
						}
					}
				}//End - if ((resumeService.getDataSource() != null) && (resumeService.getDataSource().getResultMap() != null))
				else { 
					this.outParam = false;
					log.info("ERROR - RESUME Service : Data Source/Result Map IS NULL ");
				}//End - Else if ((resumeService.getDataSource() != null) && (resumeService.getDataSource().getResultMap() != null))

			}
			catch(C30SDKToolkitException e) {
		    log.error("ERROR - resumeServiceInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	
	}//End - void resumeServiceInBillingSystem(C30ServiceObject service, String acctSegId, String transactionId, String orderDesiredDate, String actionWho)

	
	/**This method is responsible for Update a Service in the Billing system
	 * 
	 * @param service
	 * @throws C30SDKToolkitException 
	 */
	private void updateServiceInBillingSystem(C30ServiceObject service, String acctSegId, String transactionId) throws C30SDKToolkitException {

		log.info("Updating service in billing system...");
		String     serviceId=null;
		String     actionDate=null;
		String     actionWho=null;
		String     rateClass=null;
		String     revRcvCostCtr=null;
		C30ExtendedDataList svcextDatList=null;

		try {
			if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}				
			if (service.getClientId() != null)          { serviceId = service.getClientId();}
			if (service.getRateClass() != null)         { rateClass = C30SqlApiUtils.convertJaxbElementToString(service.getRateClass());}
			if (service.getRevRcvCostCtr() != null)     { revRcvCostCtr = C30SqlApiUtils.convertJaxbElementToString(service.getRevRcvCostCtr());}
			if (service.getExtendedDataList() != null) {
				svcextDatList=service.getExtendedDataList().getValue();
			}
	
			
			log.info("START - Invoking UPDATE Service SQL-API for ServiceId : "+serviceId);
			//Update Service API
			C30WPUpdateServiceApi updService  = new C30WPUpdateServiceApi(conn, transactionId, acctSegId.toString(), serviceId, actionDate,		
					actionWho, rateClass, revRcvCostCtr, svcextDatList);
			log.info("END - Invoking UPDATE Service SQL-API for ServiceId : "+serviceId);

			
			//Check the Output			
			if ((updService.getDataSource() != null) && (updService.getDataSource().getResultMap() != null)) {			
				Iterator<?> itr = updService.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Update Service OUT PARAM1 : " + outparam.keySet() + " Update Service OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Update Service OUT PARAM=true");					
					}
				}
			}//End - if ((updService.getDataSource() != null) && (updService.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("ERROR - Update Service : Data Source/Result Map IS NULL ");
			}//End - Else if ((updService.getDataSource() != null) && (updService.getDataSource().getResultMap() != null))
			
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - updateServiceInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - void updateServiceInBillingSystem(C30ServiceObject service, String acctSegId)
	
	public boolean isOutParam()
	{
		return outParam;
	}

	public void setOutParam(boolean outParam)
	{
		this.outParam = outParam;
	}
	
} // End - public class C30OrderServiceScript
