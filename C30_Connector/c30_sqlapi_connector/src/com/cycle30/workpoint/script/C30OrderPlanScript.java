package com.cycle30.workpoint.script;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;

import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.C30WPAddPlanApi;
import com.cycle30.workpoint.sqlapi.C30WPAddPlanMemberApi;
import com.cycle30.workpoint.sqlapi.C30WPDiscPlanApi;
import com.cycle30.workpoint.sqlapi.C30WPChangePlanApi;
import com.cycle30.workpoint.sqlapi.C30WPChangePlanHostApi;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/** This script works on the plan object in the given xml
 * 
 * @author rnelluri
 *
 * @author Umesh
 * - Updated Add Plan and Disconnect Plan
 * - Added Swap Plan & Update Plan
 * - Added the TransactionId Features
 * - Removed ForceOrderItemId and SubOrderId comparison condition for plan object
 * - Modified to align with the new XSD created for wholesale project
 * 
 */
public class C30OrderPlanScript  extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30OrderPlanScript.class);
	
	private boolean outParam = false;	
	
	public C30OrderPlanScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30OrderPlanScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		 
		try  {	
				
		       	//If service object exists, get the Client id
	    		String	serviceId=null;
				String	orderDesiredDate=null;
				String	actionWho=null; 
				String 	planName=null;
				oracle.sql.ARRAY servIdListArray=null;	
				
				orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
				actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());
				log.info("orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);				

			    //Check if the service object is empty.
			    if (order.getServiceObjectList() != null) {
				    
	                List<C30ServiceObject> serviceobjList =  order.getServiceObjectList().getServiceObject();
	                log.info("Service Object List Size: " + serviceobjList.size());
	                for (C30ServiceObject service : serviceobjList) {

	                	//Get Client Id from service
	                    serviceId = service.getClientId();
	                    log.info("Checking Plan for serviceId : " +serviceId);
	                	
	                    if (service.getPlanObject() != null) {
	    					C30PlanObject plan = service.getPlanObject().getValue();
	    					
	    					//Check Plan Name
	    					planName=plan.getPlanName();
	    					String	PlanInstanceIDOut = plan.getPlanInstanceIdOut();
	    					String	PlanInstanceIdIn = plan.getPlanInstanceIdIn();
	    					servIdListArray = preparePlanServiceIdList(plan,serviceId, conn);
	    					String	planAction = plan.getClientActionType().toString();

	                    	if (((planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) && 
	                    			(planName == null) && 
	                    			(PlanInstanceIdIn != null) &&
	                    			(PlanInstanceIDOut == null)) 
	                    			|| ((planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) && 
	                    					(planName == null) &&
	                    					(PlanInstanceIdIn != null) &&
	                    					(PlanInstanceIDOut == null))) {
	                    		addPlanMemberInBillingSystem(plan, acctSegId, serviceId, transactionId, servIdListArray);
	                    	} else if (((planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) && 
	                    			(planName != null) && 
	                    			(PlanInstanceIdIn != null) &&
	                    			(PlanInstanceIDOut == null)) 
	                    			|| ((planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) && 
	                    					(planName != null) &&
	                    					(PlanInstanceIdIn != null) &&
	                    					(PlanInstanceIDOut == null))) {
	                    		addPlanInBillingSystem(plan, acctSegId, serviceId, transactionId, servIdListArray); }
							else if (planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
							    discPlanInBillingSystem(plan, acctSegId, transactionId); }
							else if (((planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) && 
	                    			(planName != null) && 
	                    			(PlanInstanceIdIn != null) &&
	                    			(PlanInstanceIDOut != null)) 
	                    			|| ((planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) && 
	                    					(planName != null) &&
	                    					(PlanInstanceIdIn != null) &&
	                    					(PlanInstanceIDOut != null))) {
								changePlanInBillingSystem(plan, acctSegId, transactionId); }
							else if ((planAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) &&
									(planName == null) &&
									(PlanInstanceIdIn != null) &&
									(PlanInstanceIDOut != null)) {
								changePlanHostInBillingSystem(plan, acctSegId, transactionId, serviceId); }
	                    	
	                    }// End - if (service.getPlanObject() != null)
	    			    else {
	    			    	log.info("ERROR : Plan Object Empty/NOT found");
	    		            return;
	    			    }
	                }// End - for (C30ServiceObject service : serviceobjList)
	                
				} // End - if (order.getServiceObjectList() != null)
			    else {
			    	log.info("ERROR : Service Object Empty/NOT found");
		            return;
			    }
			} 
			catch(C30SDKToolkitException e) {
			    log.error("ERROR - Plan Order :"+e);
				e.printStackTrace();
				throw e;
			}
			catch(Exception e) {
				log.error("EXCP - Plan Order :"+e);	                
				e.printStackTrace();
				throw new C30SDKToolkitException("EXCP: ORDER-Plan Order Request Process", e );			
			}
	}//End - void process()


	/**This method is responsible for Preparing Service Id List
	 * 
	 * @param plan
	 * @throws C30SDKToolkitException 
	 */	
	
	public static oracle.sql.ARRAY preparePlanServiceIdList(C30PlanObject plan, String serviceId, Connection conn) throws SQLException
	{
		StructDescriptor bookStructDesc =
		StructDescriptor.createDescriptor("C30ARBOR.C30STRING", conn);
		ArrayDescriptor arrayDescriptor = ArrayDescriptor.createDescriptor("C30ARBOR.C30STRINGARRAY", conn);
		log.info("Getting Service Id List");
		int i = 0;
		int size = 1;
		oracle.sql.ARRAY servIdListArray = null;
		Object[] bArray =null;

		if (plan.getPlanName() == null) {
				bArray = new Object[size];
				STRUCT m1 = new STRUCT(bookStructDesc, conn, new Object[]{serviceId});
				bArray[i]=m1;
			}
		log.info("servIdListArray before : " +servIdListArray);
		servIdListArray = new oracle.sql.ARRAY(arrayDescriptor, conn, bArray);
		log.info("servIdListArray After : " +servIdListArray);
		return servIdListArray;
	}
	
	/**This method is responsible for adding a Plan in the Billing system
	 * 
	 * @param plan
	 * @throws C30SDKToolkitException 
	 */
	private void addPlanInBillingSystem(C30PlanObject plan, String acctSegId, String serviceId, String transactionId, 
			oracle.sql.ARRAY servIdListArray) throws C30SDKToolkitException {

		log.info("Adding plan in billing system...");
		String		catalogId=null;
		String		instanceId=null;
		String		actionDate=null;
		String		actionWho=null;		
		
		try{
				if (order.getOrderDesiredDate() != null)		{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
				if (order.getActionWho() != null)				{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
				if (plan.getPlanName() != null)           		{ catalogId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanName());}
				if (plan.getPlanInstanceIdIn() != null)         { instanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdIn());}
			 
				log.info("START - Invoking ADD Plan SQL-API for Service Id : " +serviceId +" and CatalogId : "+catalogId+" instanceId : "+instanceId);		
				//Add Plan SQL-API 
				C30WPAddPlanApi addPlan  = new C30WPAddPlanApi(conn, transactionId, acctSegId.toString(), catalogId, instanceId, actionDate,
				actionWho, serviceId, servIdListArray);
				log.info("END - Invoking ADD Plan SQL-API for Service Id : " +serviceId +" and CatalogId : "+catalogId+" instanceId : "+instanceId);	

				//Check the Output			
				if ((addPlan.getDataSource() != null) && (addPlan.getDataSource().getResultMap() != null)) {
					Iterator<?> itr = addPlan.getDataSource().getResultMap().values()
							.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("Add Plan OUT PARAM1 : " + outparam.keySet() + " Add Plan OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("Add Plan OUT PARAM=true");					
						}
					}
				}//End - if ((addPlan.getDataSource() != null) && (addPlan.getDataSource().getResultMap() != null))
				else { 
					this.outParam = false;
					log.info("ERROR - Add Plan : Data Source/Result Map IS NULL ");
				}//End - Else if ((addPlan.getDataSource() != null) && (addPlan.getDataSource().getResultMap() != null))

		}//End - Try
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - addPlanInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - private void addPlanInBillingSystem(C30PlanObject plan, String acctSegId) throws C30SDKToolkitException

	/**This method is responsible for disconnecting a Plan in the Billing system
	 * 
	 * @param plan
	 * @throws C30SDKToolkitException 
	 */
	private void discPlanInBillingSystem(C30PlanObject plan, String acctSegId, String transactionId) throws C30SDKToolkitException {

		log.info("Disconnecting plan in billing system...");
		String		instanceId=null;
		String		actionDate=null;
		String 		actionWho=null;

		try{
				if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
				if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
				if (plan.getPlanInstanceIdIn() != null)		{	instanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdIn());}
		   
				log.info("START - Invoking DISCONNECT Plan SQL-API for InstanceId : "+instanceId + " actionDate : "+actionDate);		
				//Disconnect Plan API
				C30WPDiscPlanApi discPlan  = new C30WPDiscPlanApi(conn, transactionId, acctSegId.toString(), instanceId, actionDate, actionWho);
				log.info("END - Invoking DISCONNECT Plan SQL-API for InstanceId : "+instanceId + " actionDate : "+actionDate);		
			   
				//Check the Output			
				if ((discPlan.getDataSource() != null) && (discPlan.getDataSource().getResultMap() != null)) {
					Iterator<?> itr = discPlan.getDataSource().getResultMap().values()
							.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("Disc Plan OUT PARAM1 : " + outparam.keySet() + " Disc Plan OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("Disc Plan OUT PARAM=true");					
						}
					}
				}//End - if ((discPlan.getDataSource() != null) && (discPlan.getDataSource().getResultMap() != null))
				else { 
					this.outParam = false;
					log.info("ERROR - Disconnect Plan : Data Source/Result Map IS NULL ");
				}//End - Else if ((discPlan.getDataSource() != null) && (discPlan.getDataSource().getResultMap() != null))

			}
			catch(C30SDKToolkitException e) {
		    log.error("ERROR - discPlanInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	
	}//End - void discPlanInBillingSystem(C30PlanObject plan, String acctSegId)


	/**This method is responsible for Change a Plan in the Billing system
	 * 
	 * @param plan
	 * @throws C30SDKToolkitException 
	 */
	private void changePlanInBillingSystem(C30PlanObject plan, String acctSegId, String transactionId) throws C30SDKToolkitException {

		log.info("Change/Swap a plan in billing system...");
		String		oldinstanceId=null;
		String		actionDate=null;
		String		actionWho=null;		
		String		catalogId=null;
		String		newinstanceId=null;
		
		
		try{
				if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
				if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
				if (plan.getPlanInstanceIdOut() != null)	{ oldinstanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdOut());}
				if (plan.getPlanName() != null)				{ catalogId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanName());}
				if (plan.getPlanInstanceIdIn() != null)		{ newinstanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdIn());}
		   
				log.info("START - Invoking CHANGE Plan SQL-API for Old InstanceId : "+oldinstanceId + 
						" New InstanceId : "+newinstanceId+" catalogId : "+catalogId+" actionDate : "+actionDate);		
				//Change Plan API
				C30WPChangePlanApi changePlan  = new C30WPChangePlanApi(conn, transactionId, acctSegId.toString(), oldinstanceId, catalogId, 
						newinstanceId, actionDate, actionWho);
				log.info("END - Invoking CHANGE Plan SQL-API for Old InstanceId : "+oldinstanceId + 
						" New InstanceId : "+newinstanceId+" catalogId : "+catalogId+" actionDate : "+actionDate);	
			   
				//Check the Output			
				if ((changePlan.getDataSource() != null) && (changePlan.getDataSource().getResultMap() != null)) {
					Iterator<?> itr = changePlan.getDataSource().getResultMap().values()
							.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("Change Plan OUT PARAM1 : " + outparam.keySet() + " Change Plan OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("Change Plan OUT PARAM=true");					
						}
					}
				}//End - if ((changePlan.getDataSource() != null) && (changePlan.getDataSource().getResultMap() != null))
				else { 
					this.outParam = false;
					log.info("ERROR - Change Plan : Data Source/Result Map IS NULL ");
				}//End - Else if ((changePlan.getDataSource() != null) && (changePlan.getDataSource().getResultMap() != null))

			}
			catch(C30SDKToolkitException e) {
		    log.error("ERROR - changePlanInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	
	}//End - void changePlanInBillingSystem(C30PlanObject plan, String acctSegId, String transactionId, String orderDesiredDate, String actionWho)


	/**This method is responsible for Change a Plan HOST in the Billing system
	 * 
	 * @param plan
	 * @throws C30SDKToolkitException 
	 */
	private void changePlanHostInBillingSystem(C30PlanObject plan, String acctSegId, String transactionId, String serviceId) throws C30SDKToolkitException {

		log.info("Change plan HOST in billing system...");
		String		oldinstanceId=null;
		String		actionDate=null;
		String		actionWho=null;		
		String		newinstanceId=null;
		
		try{
				if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
				if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
				if (plan.getPlanInstanceIdOut() != null)	{ oldinstanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdOut());}
				if (plan.getPlanInstanceIdIn() != null)		{ newinstanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdIn());}
		   
				log.info("START - Invoking CHANGE Plan Host SQL-API for Old InstanceId : "+oldinstanceId + " New InstanceId : "+newinstanceId+
						" Service Id : "+serviceId+" actionDate : "+actionDate);		
				//Change Plan HOST API
				C30WPChangePlanHostApi changePlanHost  = new C30WPChangePlanHostApi(conn, transactionId, acctSegId.toString(), oldinstanceId, 
						newinstanceId, serviceId, actionDate, actionWho);
				log.info("END - Invoking CHANGE Plan Host SQL-API for Old InstanceId : "+oldinstanceId + " New InstanceId : "+newinstanceId+
						" Service Id : "+serviceId+" actionDate : "+actionDate);		
			   
				//Check the Output			
				if ((changePlanHost.getDataSource() != null) && (changePlanHost.getDataSource().getResultMap() != null)) {
					Iterator<?> itr = changePlanHost.getDataSource().getResultMap().values()
							.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("Change Plan Host OUT PARAM1 : " + outparam.keySet() + " Change Plan Host OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("Change Plan Host OUT PARAM=true");					
						}
					}
				}//End - if ((changePlanHost.getDataSource() != null) && (changePlanHost.getDataSource().getResultMap() != null))
				else { 
					this.outParam = false;
					log.info("ERROR - Change Plan Host : Data Source/Result Map IS NULL ");
				}//End - Else if ((changePlanHost.getDataSource() != null) && (changePlanHost.getDataSource().getResultMap() != null))

			}
			catch(C30SDKToolkitException e) {
		    log.error("ERROR - changePlanHostInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}
	
	}//End - void changePlanHostInBillingSystem(C30PlanObject plan, String acctSegId, String transactionId, String orderDesiredDate, String actionWho)

	
	/**This method is responsible for adding a Plan Member in the Billing system
	 * 
	 * @param plan
	 * @throws C30SDKToolkitException 
	 */
	private void addPlanMemberInBillingSystem(C30PlanObject plan, String acctSegId, String serviceId, String transactionId, 
			oracle.sql.ARRAY servIdListArray) throws C30SDKToolkitException {

		log.info("Adding plan member in billing system...");
		String		instanceId=null;
		String		actionDate=null;
		String		actionWho=null;		
		
		try{
			if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
			if (plan.getPlanInstanceIdIn() != null)     { instanceId = C30SqlApiUtils.convertJaxbElementToString(plan.getPlanInstanceIdIn());}

			log.info("START - Invoking ADD Plan Member SQL-API for Instance Id : " +instanceId+" actionDate : "+actionDate);	
			
			//Add Plan Member API  
			C30WPAddPlanMemberApi addPlanMember  = new C30WPAddPlanMemberApi(conn, transactionId, acctSegId.toString(), instanceId, actionDate,
			actionWho, servIdListArray);
			
			log.info("END - Invoking ADD Plan Member SQL-API for Instance Id : " +instanceId+" actionDate : "+actionDate);

			//Check the Output			
			if ((addPlanMember.getDataSource() != null) && (addPlanMember.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = addPlanMember.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Add Plan Member OUT PARAM1 : " + outparam.keySet() + " Add Plan Member OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Add Plan Member OUT PARAM=true");					
					}
				}
			}//End - if ((addPlanMember.getDataSource() != null) && (addPlanMember.getDataSource().getResultMap() != null))
			else { 
				this.outParam = false;
				log.info("ERROR - Add Plan Member: Data Source/Result Map IS NULL ");
			}//End - Else if ((addPlanMember.getDataSource() != null) && (addPlanMember.getDataSource().getResultMap() != null))

		}//End - Try
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - addPlanMemberInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - private void addPlanMemberInBillingSystem(C30PlanObject plan, String acctSegId) throws C30SDKToolkitException
	

	public boolean isOutParam()
	{
		return outParam;
	}

	public void setOutParam(boolean outParam)
	{
		this.outParam = outParam;
	}
	
} // End - public class C30OrderPlanScript
