package com.cycle30.workpoint.script;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30SDKConfigurationManager;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/** this is used to notify the status of the Order to the C30SDK
 * So, the update is going to C30_SDK_ORDER and C30_SDK_SUB_ORDER table
 * @author rnelluri
 *
 */
public class C30OrderStatusNotification {

	//Logs all the information
	private static Logger log = Logger.getLogger(C30OrderStatusNotification.class);
	public static C30SDKConfigurationManager exceptionResourceBundle = C30SDKConfigurationManager.getInstance();


	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the subOrderId
	 */
	public String getSubOrderId() {
		return subOrderId;
	}

	/**
	 * @param subOrderId the subOrderId to set
	 */
	public void setSubOrderId(String subOrderId) {
		this.subOrderId = subOrderId;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	String orderId;
	String subOrderId;
	String transactionId;
	int isCancellable=0;
	int isResubmittable=0;
	

	public C30OrderStatusNotification(String orderId, String subOrderId, String transactionId, int isCancellable,
			int isResubmittable) throws C30SDKToolkitException
	{
		log.debug("Constuctor - C30OrderStatusNotification");
		this.orderId=orderId;
		this.subOrderId=subOrderId;
		this.transactionId=transactionId;
		this.isCancellable = isCancellable;
		this.isResubmittable = isResubmittable;
	}

	public C30OrderStatusNotification(String orderId, String subOrderId, String transactionId) throws C30SDKToolkitException
	{
		log.debug("Constuctor - C30OrderStatusNotification");
		this.orderId=orderId;
		this.subOrderId=subOrderId;
		this.transactionId=transactionId;
	}
	
	/** Update the order status to Go To Error
	 * 
	 * @throws C30SDKToolkitException
	 */
	public void updateOrderStatusToError() throws C30SDKToolkitException
	{
		if ((orderId != null) && (transactionId != null))
		{
			updateOrderStatus(C30WPSQLConstants.ORD_STATUS_IN_ERROR);
		}
		if ((subOrderId != null) && (transactionId != null))
		{
			updateSubOrderStatus(C30WPSQLConstants.ORD_STATUS_IN_ERROR);
		}
	}
	
	/** Update the order status to Go To Completed
	 * 
	 * @throws C30SDKToolkitException
	 */
	public void updateOrderStatusToComplete() throws C30SDKToolkitException
	{
		if ((orderId != null) && (transactionId != null))
		{
			updateOrderStatus(C30WPSQLConstants.ORD_STATUS_COMPLETED);
		}
		if ((subOrderId != null) && (transactionId != null))
		{
			updateSubOrderStatus(C30WPSQLConstants.ORD_STATUS_COMPLETED);
		}
	}
	/** This method is used to update the Status of the order.
	 * 
	 * @param newOrderSatusId
	 * @throws C30SDKToolkitException
	 */
	public void updateOrderStatus(Integer newOrderSatusId) throws C30SDKToolkitException
	{

		String query = null;
		C30JDBCDataSource dataSource = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;		
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   
			if ((newOrderSatusId == C30WPSQLConstants.ORD_STATUS_COMPLETED) || (newOrderSatusId == C30WPSQLConstants.ORD_STATUS_IN_ERROR)) {
				query = C30WPSQLConstants.CHANGE_ORDER_STATUS_COMPLETE;
				ps = dataSource.getPreparedStatement(query);
				ps.setInt(1,newOrderSatusId);
				ps.setInt(2,isCancellable);
				ps.setInt(3,isResubmittable);
				ps.setString(4,(String)orderId);
				ps.setString(5,(String)transactionId);
			}
			else {
				query = C30WPSQLConstants.CHANGE_ORDER_STATUS;				
				ps = dataSource.getPreparedStatement(query);
				ps.setInt(1,newOrderSatusId);
				ps.setString(2,(String)orderId);
				ps.setString(3,(String)transactionId);
			}
		
			resultSet = ps.executeQuery();

		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Error While updating the order status",e,"ERROR");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			} catch (Exception ex){
				log.error("updateOrderStatus - Exception: "+ex.getMessage());
				throw new C30SDKToolkitException("Error While updating the order status",ex,"ERROR");				
			}
		}
	}
	/** This method is used to update the Status of the SubOrder.
	 * 
	 * @param newOrderSatusId
	 * @throws C30SDKToolkitException
	 */
	public void updateSubOrderStatus(Integer newOrderSatusId) throws C30SDKToolkitException
	{

		String query = null;
		C30JDBCDataSource dataSource = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;		
		
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();
			if ((newOrderSatusId == C30WPSQLConstants.ORD_STATUS_COMPLETED) || (newOrderSatusId == C30WPSQLConstants.ORD_STATUS_IN_ERROR)) {
				query = C30WPSQLConstants.CHANGE_SUB_ORDER_STATUS_COMPLETE;				
			}
			else {
				query = C30WPSQLConstants.CHANGE_SUB_ORDER_STATUS;
			}
			ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,newOrderSatusId);
			ps.setInt(2,isCancellable);
			ps.setInt(3,isResubmittable);
			ps.setString(4,(String)subOrderId);
			ps.setString(5,(String)transactionId);			

			resultSet = ps.executeQuery();

		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Error While updating the order status",e,"ERROR");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30SDKToolkitException("Error While updating the order status",ex,"ERROR");				
			}
		}
	}
	
}
