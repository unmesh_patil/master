package com.cycle30.workpoint.script.awnprepaid;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
//SDK
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
//import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//Workpoint
import com.cycle30.workpoint.script.C30OrderScript;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.script.awnprepaid.utils.C30AWNPrepaidUtils;
//Provision
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;



/** This script works on the Order Object in the given XML for Provisioning
 * 
 * @author Umesh
 * - Initial Version
 * 
 * 
 */
public class C30PrepaidOrderInventoryScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30PrepaidOrderInventoryScript.class);


	public C30PrepaidOrderInventoryScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidOrderInventoryScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException, C30ProvConnectorException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the Provisioning action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String orderDesiredDate = null;
			String actionWho = null;
			String actionReason = null;
			String sessionId = null;
			String salesChannelId = null;
            String disconnectReason = "";  //disconnectReason and reqSystemId added by shahshai for PSST-55
            String reqSystemId = "";
			orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
			actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());

			if (orderDesiredDate == null) {
				orderDesiredDate = new Date().toString();
			}
			log.info("acctSegId: " + acctSegId + "orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);				
			log.info("START - Inventory Request for : " + " acctSegId: " + acctSegId);

			HashMap<String, String> provRequestData = new HashMap<String, String>();
			//Prepare Data For Inventory
			provRequestData.put(Constants.ACCT_SEG_ID,acctSegId);
			provRequestData.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			provRequestData.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			provRequestData.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_PHONE_INVENTORY_INQUIRY);

			//Check the Service Object
			if (order.getServiceObjectList() != null)
			{
				List<C30ServiceObject> sList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
				log.info("Working on the Service Object.  Size of list is: "+ sList.size());
				for (C30ServiceObject service : sList)
				{
					String svcactionType  = service.getClientActionType().toString();
					String inventoryRequestAction = C30WPSQLConstants.C30_INV_ACTION_TYPE_UPDATE;
					actionReason = service.getClientActionTypeReason();				    
					log.info("actionReason = " +actionReason);

					//ADD Service
					if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
						log.info("ADD SERVICE");
						String telephoneNumber = service.getClientServicePrimaryIdentifier();
						//Get the Session Id
						if (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()))
							sessionId = getSessionId(transactionId);

						log.info("SessionId = " +sessionId);
						//Get the SalesChannel Id
						if((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString()))){
							log.info("GOING FOR SALES CHANNEL ID");
							salesChannelId = getInvSalesChannelId(sessionId, telephoneNumber, transactionId, acctSegId);
						}
						log.info("salesChannelId = " +salesChannelId);
						//Update Inventory Status
						if(/*sessionId!=null && !("".equals(sessionId)) && */((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString()))) 
								&& (inventoryRequestAction !=null)
								) {
							if (actionReason == "" || actionReason.isEmpty() || actionReason == null) {
								updatePhoneInventory(sessionId, telephoneNumber, C30WPSQLConstants.C30_INV_STATUS_TYPE_ASSIGNED, 
										inventoryRequestAction, salesChannelId ,acctSegId, disconnectReason, reqSystemId); 
							} else {
								updatePhoneInventory(sessionId, telephoneNumber, C30WPSQLConstants.C30_INV_STATUS_TYPE_ASSIGNED, 
										inventoryRequestAction, salesChannelId, acctSegId, disconnectReason, reqSystemId); 
							}
						}   			            
					}


					//Change Telephone Number
					if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {
						//Check Identifier Object
						if (service.getIdentifierObjectList() != null)  {
							List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
							log.info("Working on the Identifier Object.  Size of list is " + ilList.size());
							for (C30IdentifierObject identifier : ilList)  {
								if(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
									if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_TELEPHONE_NUMBER)) {
										String oldtelephoneNumber = identifier.getIdentifierIdOut();
										String newtelephoneNumber = identifier.getIdentifierIdIn();	
										//Get the Session Id
										if (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()))
											sessionId = getSessionId(transactionId);
										//Get the SalesChannel Id
										if((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString())))
											salesChannelId = getInvSalesChannelId(sessionId, newtelephoneNumber, transactionId, acctSegId);
										//Update Inventory Status
										if(/*sessionId!=null && !("".equals(sessionId)) && */((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString()))) 
												&& (inventoryRequestAction !=null)
												) {
											updatePhoneInventory(sessionId, newtelephoneNumber, C30WPSQLConstants.C30_INV_STATUS_TYPE_ASSIGNED, 
													inventoryRequestAction, salesChannelId, acctSegId, disconnectReason, reqSystemId); 
										}   			            

										//Get the Session Id
										if (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()))
											sessionId = getSessionId(transactionId);
										//Get the SalesChannel Id
										if((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString())))
											salesChannelId = getInvSalesChannelId(sessionId, oldtelephoneNumber, transactionId, acctSegId);
										//Update Inventory Status
										if(/*sessionId!=null && !("".equals(sessionId)) && */((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString()))) 
												&& (inventoryRequestAction !=null)
												) {
											//Added by vijaya PW-82
											disconnectReason = C30WPSQLConstants.C30_ORDER_ACTION_REASON_CODE_7;
											reqSystemId = C30WPSQLConstants.C30_ORDER_REQ_SYSTEM_ID;
											updatePhoneInventory(sessionId, oldtelephoneNumber, C30WPSQLConstants.C30_INV_STATUS_TYPE_AGING, 
													inventoryRequestAction, salesChannelId, acctSegId, disconnectReason, reqSystemId); 
										}   	
									}
								}
							}
						}
					}

					//DISCONNECT Service
					if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
						String telephoneNumber = service.getClientServicePrimaryIdentifier();
						String InvStatus = null;
						if ((actionReason.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_REASON_TO_POSTPAID))
								|| (actionReason.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_REASON_TO_LANDLINE))) {
							//InvStatus commented and added by sundar for PW-206
							//InvStatus = C30WPSQLConstants.C30_INV_STATUS_TYPE_UPGRADE;
							InvStatus = C30WPSQLConstants.C30_INV_STATUS_TYPE_AVAILABLE;
							//disconnect reason code and reqSystemId added by sundar for PW-196
							disconnectReason = C30WPSQLConstants.C30_ORDER_ACTION_REASON_CODE_6;
							reqSystemId = C30WPSQLConstants.C30_ORDER_REQ_SYSTEM_ID;
						} else if (actionReason.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_REASON_TO_AGING)) {
							InvStatus = C30WPSQLConstants.C30_INV_STATUS_TYPE_AGING;
							reqSystemId = C30WPSQLConstants.C30_ORDER_REQ_SYSTEM_ID;
							if(actionWho.equals(C30WPSQLConstants.C30_ORDER_ACTION_WHO_TYPE_LCF_BATCH)){        //added by shahshai for PSST-55
								disconnectReason = C30WPSQLConstants.C30_ORDER_ACTION_REASON_CODE_500;
							}
							else{
							disconnectReason = C30WPSQLConstants.C30_ORDER_ACTION_REASON_CODE_7;
							}
						} else {
							InvStatus = C30WPSQLConstants.C30_INV_STATUS_TYPE_AVAILABLE;
						}
						//Get the Session Id
						if (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()))
							sessionId = getSessionId(transactionId);
						//Get the SalesChannel Id
						if((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString())))
							salesChannelId = getInvSalesChannelId(sessionId, telephoneNumber, transactionId, acctSegId);
						//If Port Out Disconnect and sales channel "GSM Prepaid" change to "Wireless Ported Out"
						if ((actionReason.equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_OUT)) ||
								(actionReason.equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_OUT1)) ||
								(actionReason.equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_OUT2))) {
							if(acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString())){        //added by shahshai for PSST-55
							disconnectReason = C30WPSQLConstants.C30_ORDER_ACTION_REASON_CODE_6;
							reqSystemId = C30WPSQLConstants.C30_ORDER_REQ_SYSTEM_ID;							
							}
							if (salesChannelId.equals(C30WPSQLConstants.C30_INV_SALES_CHANNEL_GSM_PREPAID)) {
								salesChannelId = C30WPSQLConstants.C30_INV_SALES_CHANNEL_PORTED_OUT;
							}
							if(acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString())){
								InvStatus = C30WPSQLConstants.C30_INV_STATUS_TYPE_PORTOUT;
							} 
						}
						//Update Inventory Status			            
						if(/*sessionId!=null && !("".equals(sessionId)) && */((acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString()) && sessionId!=null && !("".equals(sessionId)) ) || (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString()))) 
								&& (inventoryRequestAction !=null)
								) {
							updatePhoneInventory(sessionId, telephoneNumber, InvStatus, 
									inventoryRequestAction, salesChannelId, acctSegId, disconnectReason, reqSystemId); 
						}   			            
					}

				}//End - for (C30ServiceObject service : sList)

			}//End - if (order.getServiceObjectList() != null)				

			log.info("END - Invetory Request for : " + " acctSegId: " + acctSegId);				
		} catch (C30ProvConnectorException e) {
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid Inventory Request : "+e);
			throw new C30SDKToolkitException("EXCP:ORDER-Prepaid Inventory Request "+e, e);
		}  catch (Exception e) {
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid Inventory Request : "+e);
			throw new C30SDKToolkitException("EXCP:ORDER-Prepaid Inventory Request "+e, e);
		} 
	}


	/** Get the Session Id from Kenan System for sending in the further Transactions to GCI MulePOS
	 * 
	 * @param transactionId
	 * @return SessionId
	 * @throws C30ProvConnectorException 
	 */
	public static String getSessionId(String transactionId) throws C30ProvConnectorException {

		HashMap<String, String> PrepaidValidateSession = new HashMap<String, String>();
		PrepaidValidateSession.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);			
		PrepaidValidateSession.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_MULEPOS_SESSION_ID_INQUIRY);
		C30ProvisionRequest request = C30ProvisionRequest.getInstance();
		HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryRequest(PrepaidValidateSession);
		String  responseXML = (String) response.get("ResponseXML");
		log.info("GCI SessionId responseXML = "+responseXML);
		String sessionId = C30AWNPrepaidUtils.parseResponseForSessionId(responseXML);

		return sessionId;
	}


	/**
	 * This method will take care to retrieveTN Inventory details.
	 * @param acctSegId 
	 * @param size 
	 * @param salesChannelId 
	 * @param invStatus 
	 * @param community 
	 * @throws C30SDKException 
	 * @throws Exception 
	 */
	public static String getInvSalesChannelId(String gciSessionId, String telephoneNumber,
			String transId, String acctSegId) throws C30SDKToolkitException, C30ProvConnectorException {

		String resp = null;
		try{

			HashMap<String, String> PrepaidPhoneNumberInquiry = new HashMap<String, String>();

			PrepaidPhoneNumberInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transId);
			PrepaidPhoneNumberInquiry.put(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID, gciSessionId);
			PrepaidPhoneNumberInquiry.put(Constants.ACCT_SEG_ID,acctSegId);

			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			if(acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString())){
				//For GCI
				PrepaidPhoneNumberInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_PHONE_INVENTORY_INQUIRY);
				PrepaidPhoneNumberInquiry.put(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER,telephoneNumber);
				HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryRequest(PrepaidPhoneNumberInquiry);
				System.out.println("response - "+response.toString());
				resp = response.get(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID).toString();
			}
			else if (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString()))
			{
				//For ACS
				PrepaidPhoneNumberInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_ACS_PHONE_INVENTORY_INQUIRY);
				PrepaidPhoneNumberInquiry.put(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER,telephoneNumber);				
				HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryACSRequest(PrepaidPhoneNumberInquiry);	
				System.out.println("response - "+response.toString());
				resp = response.get(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID).toString();
			}
		}
		catch(C30ProvConnectorException e)
		{
			log.info("getInvSalesChannelId - "+e);
			throw new C30SDKToolkitException("EXCP:ORDER-getInvSalesChannelId: "+e.getMessage(), e );
		}

		return resp;
	}	


	/**
	 * Method to update Inventory Status 
	 * new param disconnectReason is added by shahshai for PSST-55
	 * @param gciSessionId 
	 * @param telephoneNumber 
	 * @param inventoryRequestAction 
	 * @param salesChannelId
	 * @param acctSegId 
	 * @param disconnectReason
	 * @throws C30ProvConnectorException 
	 *  
	 * 
	 */
	public static boolean updatePhoneInventory(String gciSessionId, String telephoneNumber,
			String invStatus, String inventoryRequestAction, String salesChannelId, String acctSegId, String disconnectReason, String reqSystemId) throws C30SDKToolkitException, C30ProvConnectorException {

		try{
			HashMap<String, String> PrepaidPhoneInventoryUpdate = new HashMap<String, String>();

			PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID, gciSessionId);
			PrepaidPhoneInventoryUpdate.put(Constants.ACCT_SEG_ID,acctSegId);
			
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			if(acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_4.toString())){
				//For GCI
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBUTE_TELEPHONE_NUMBER,telephoneNumber);
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBUTE_INV_STATUS,invStatus);
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBUTE_SALES_CHANNEL_ID,salesChannelId);
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBUTE_DISCONNECT_REASON,disconnectReason);
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBUTE_REQ_SYSTEM_ID,reqSystemId);
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBURE_ACTION_TYPE,inventoryRequestAction);
				PrepaidPhoneInventoryUpdate.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_PHONE_INVENTORY_INQUIRY);				
				log.info("CALLING THE HANDLER FOR GCI");
				HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryRequest(PrepaidPhoneInventoryUpdate);
				log.info("updatePhoneInventory response - "+response.toString());
			}else if (acctSegId.equalsIgnoreCase(Constants.ACCT_SEG_ID_5.toString()))
			{
				//For ACS
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_ACS_ATTRIBUTE_TELEPHONE_NUMBER,telephoneNumber);
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_ACS_ATTRIBUTE_INV_STATUS,invStatus);
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_ACS_ATTRIBUTE_SALES_CHANNEL_ID,salesChannelId);			
				PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_ACS_ATTRIBURE_ACTION_TYPE,inventoryRequestAction);
				PrepaidPhoneInventoryUpdate.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_ACS_PHONE_INVENTORY_INQUIRY);				
				log.info("CALLING THE HANDLER FOR ACS");
				HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryACSRequest(PrepaidPhoneInventoryUpdate);
				log.info("updatePhoneInventory response - "+response.toString());
			}
		}
		catch(C30ProvConnectorException e)
		{
			log.info("updatePhoneInventory - "+e);
			throw new C30SDKToolkitException("EXCP:ORDER-updatePhoneInventory: "+e.getMessage(), e );
		}
		return true;
	}

}
