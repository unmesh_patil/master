package com.cycle30.workpoint.script.awnprepaid;

//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//import com.cycle30.sdk.schema.jaxb.C30AccountObject;
//import com.cycle30.sdk.schema.jaxb.C30BalanceList;
//import com.cycle30.sdk.schema.jaxb.C30BalanceObject;
import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
//import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
//import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
//Workpoint
import com.cycle30.workpoint.script.C30OrderScript;
//import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.script.awnprepaid.utils.C30AWNPrepaidUtils;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.data.Constants;
//Prepaid APIs
//import com.cycle30.workpoint.prepaidapi.SubscriberUpdate;
//import com.cycle30.workpoint.prepaidapi.SubscriberActivateByCLI;
import com.cycle30.workpoint.prepaidapi.SubscriberCreditSet;
import com.cycle30.workpoint.prepaidapi.SubscriberCredit;
import com.cycle30.workpoint.prepaidapi.EventChargingDirectDebit;
//import com.cycle30.workpoint.prepaidapi.SubscriberRechargeByCLIPIN;


/** This script works on the Service Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class C30PrepaidOrderComponentScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30PrepaidOrderComponentScript.class);

	public C30PrepaidOrderComponentScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidOrderComponentScript(String orderId, String subOrderId, String acctSegId, String transactionId, 
			String internalAuditId, String requestName) throws C30SDKToolkitException
			{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId, requestName);		
			}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String SubscriberId = null;
			String ExtdatKey = Constants.OCS_SUBSCRIBER_ID;

			HashMap<String, String> requestHashMap = new HashMap<String, String>();			
			requestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			requestHashMap.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			requestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		

			//Component Order
			//Check if the service object is empty.
			if (order.getServiceObjectList() != null) {

				List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
				log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
				for (C30ServiceObject service : serviceobjList)  {

					String clientId = service.getClientId();
					String actionType = service.getClientActionType().toString();

					if ((service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))) {
						log.info("Check for the Account Number");
						SubscriberId = C30AWNPrepaidUtils.getSubscriberNumber(order, subOrderId, transactionId, ExtdatKey, acctSegId);
			            if (SubscriberId == null) {
			            	throw new C30SDKToolkitException("Couldn't find SubscriberId for transactionId = "+transactionId+" subOrderId = "+subOrderId);
			            }				
					} else {
						
						// If the Account Object is populated.
						SubscriberId = C30AWNPrepaidUtils.getSubscriberNumber(order);
						if (SubscriberId == null) {
							throw new C30SDKToolkitException("Couldn't find SubscriberId for transactionId = "+transactionId+" subOrderId = "+subOrderId);
						}		
					}

					log.info("Sub order id: '" + subOrderId + "'  Service Client id: '" + clientId + "'  Action type: '" + actionType + "'");
					if ((service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) 
							|| (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE))) {

						String subscriberCLI = null;
						if (service.getClientServicePrimaryIdentifier() != null) 
						{ subscriberCLI = C30SqlApiUtils.convertJaxbElementToString(service.getClientServicePrimaryIdentifier());}
						//Add CLI to request
						requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_CLI, subscriberCLI);

						//Get the Event Charge Label if the Component Object is populated.
						if (service.getComponentObjectList() != null) {
							List<C30ComponentObject> clList = (List<C30ComponentObject>) service.getComponentObjectList().getComponentObject();
							log.info("Working on the Component Object.  Size of list is " + clList.size());					    
							for (C30ComponentObject comp : clList) {

								if (comp.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
									log.info("ComponentInstanceId = " +comp.getComponentInstanceId().toString());
									String targetSystem = getComponentTargetSystem(service);
									log.info("targetSystem = " +targetSystem);
									if ((targetSystem.equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_OCS)) ||
											(targetSystem.equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_OCS_SPS))) {

										requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);
//										requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);									
										log.info("requestName : "+requestName);
										//EventCharging.DirectDebit
										if (requestName.equals(Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT)) {
											log.info("EventCharging.DirectDebit");
											requestHashMap = C30AWNPrepaidUtils.getEventChargingInputData(requestHashMap, null, comp);
											//Call API EventCharging.DirectDebit
											new EventChargingDirectDebit(requestHashMap);								
										}
										//Subscriber.CreditSet
										if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDITSET)) {
											log.info("Subscriber.CreditSet");								
											requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
											//Get the INPUT Data for API call						
											requestHashMap = C30AWNPrepaidUtils.getSubscriberCreditSetInputData(requestHashMap, null, comp);
											//Call API Subscriber.CreditSet
											new SubscriberCreditSet(requestHashMap);								
										}
										//Subscriber.Credit
										if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDIT)) {
											log.info("Subscriber.Credit");								
											requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
											//Get the INPUT Data for API call
											requestHashMap = C30AWNPrepaidUtils.getSubscriberCreditInputData(requestHashMap, null, comp);						
											//Call API SubscriberCredit
											new SubscriberCredit(requestHashMap);								
										}			
									}
								}
							}
						}
					}
				}
			}
		}
		catch(C30SDKToolkitException e) {
			log.error("ERROR - Prepaid Component Order :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - Prepaid Component Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:AddComponentinBillingSystem Request-RequestName: "+requestName +" Message: "+e, e );			
		}
	}

	
	/**This method is responsible for Identifying Component Target System
	 * 
	 * @param service
	 * @throws C30SDKToolkitException
	 * 
	 *  
	 */
	private String getComponentTargetSystem(C30ServiceObject service) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		String targetSystem = null;
		C30ExtendedDataList compExtDatList=null;		

		try{
			List<C30ComponentObject> clList = (List<C30ComponentObject>) service.getComponentObjectList().getComponentObject();
			log.info("Working on the Component Object.  Size of list is " + clList.size());					    
			for (C30ComponentObject comp : clList) {
				compExtDatList=comp.getExtendedDataList().getValue();
				log.info(" Total Extended Data found : " + compExtDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = compExtDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList){
					if (extData.getExtendedDataName().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM)) {
						targetSystem = extData.getExtendedDataValue();
						log.info("targetSystem = "+targetSystem);
					}
				}			    	
			}
			return targetSystem;
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR- getComponentTargetSystem: "+e);
			throw new C30SDKToolkitException("EXCP: getComponentTargetSystem: "+e, e);
		}		
	}		

}
