package com.cycle30.workpoint.script.awnprepaid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.script.C30OrderScript;
import com.cycle30.workpoint.script.awnprepaid.utils.C30AWNPrepaidUtils;
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.schema.jaxb.C30BalanceObject;
import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.sdk.schema.jaxb.C30BalanceList;
import com.cycle30.prov.exception.C30ProvConnectorException;

public class C30PrepaidCheckOrderData extends C30OrderScript
{

	private static Logger log = Logger.getLogger(C30PrepaidCheckOrderData.class);

	public C30PrepaidCheckOrderData(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidCheckOrderData(String orderId, String subOrderId,
			String acctSegId, String transactionId, String internalAuditId, String requestName)
			throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId, requestName);
	}

	public void process() throws C30SDKToolkitException
	{
		// TODO Auto-generated method stub
		try
		{
			log.info("Process Check Prepaid Order Data");
		}
		catch (Exception e)
		{
			log.error("EXCP - Check Prepaid Order Data :" + e);
			e.printStackTrace();
			throw new C30SDKToolkitException(
					"EXCP: PREPAID ORDER-Check Order Request Process", e);
		}
	}
    
	// This method provide the old signature without a validateTN flag, it defaults to true
	public Map<String, Boolean> chkPrepaidOrderData() throws C30SDKToolkitException {
		return chkPrepaidOrderData(true);
	}

	public Map<String, Boolean> chkPrepaidOrderData(boolean validateTN) throws C30SDKToolkitException
	{
		// TODO Auto-generated method stub
		Map<String, Boolean> orderData = new HashMap<String, Boolean>();

		try
		{
			//Check on the Account Object and Identify the Action Status
			if (order.getAccount() != null)
			{
				C30ExtendedDataList extDatList=null;
				log.info("setting boolean value for Account object");
				if (order
						.getAccount()
						.getClientActionType()
						.toString()
						.equalsIgnoreCase(
								C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
				{
					validateInputDataForAddAccount();
					orderData.put("AddAct", true);
					log.info("boolean value for AddAct object : "
							+ orderData.get("AddAct"));
					
					//Get the Balance Details
					if (order.getAccount().getBalanceList() != null) {
						//Get Balance Object Data
						C30BalanceList balanceList = order.getAccount().getBalanceList().getValue();
						List<C30BalanceObject> balanceobjList = balanceList.getBalance();
						log.info("Get the Wallet Refill Data ");				
						for(C30BalanceObject balance : balanceobjList)
						{
							log.info("Balance Action : " +balance.getBalanceAction().toString());
							//Check for Wallet Refill
							if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLETREFILL)) ||
									(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLET_REFILL))) {
								if (balance.getBalanceUnits()!=null) {
									orderData.put("UpdateBalance", true);
									log.info("boolean value for Change account object  : "
											+ orderData.get("UpdateBalance"));
				    			}
				    		}
							
							//Check for Balance Refill
							if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCE_REFILL)) ||					
					    			(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCEREFILL))) {	
				    			if (balance.getBalanceUnits()!=null) {
										orderData.put("UpdateBalance", true);
										log.info("boolean value for Change account object  : "
												+ orderData.get("UpdateBalance"));
				    			}
				    		}				    		
							//Check for Wallet Redeem
				    		if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHER_REDEEM)) ||
				    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHERREDEEM))) {
								orderData.put("DebitPlanFee", true);				    			
								orderData.put("VoucherRedeem", true);
								log.info("boolean value for Change account object  : "
										+ orderData.get("VoucherRedeem"));
				    		}
						}
					}					
					
				} else if (order
						.getAccount()
						.getClientActionType()
						.toString()
						.equalsIgnoreCase(
								C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE))
				{
					if (order.getServiceObjectList() == null) {
						//Get the Balance Details
						if (order.getAccount().getBalanceList() != null) {
							//Get Balance Object Data
							C30BalanceList balanceList = order.getAccount().getBalanceList().getValue();
							List<C30BalanceObject> balanceobjList = balanceList.getBalance();
							log.info("Get the Wallet Refill Data ");				
							for(C30BalanceObject balance : balanceobjList)
							{
								log.info("Balance Action : " +balance.getBalanceAction().toString());
								if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLETREFILL)) ||
										(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLET_REFILL)) ||
										(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCE_REFILL)) ||					
					    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCEREFILL))) {	
									orderData.put("UpdateBalance", true);
									log.info("boolean value for Change account object  : "
											+ orderData.get("UpdateBalance"));
					    		}
					    		if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHER_REDEEM)) ||
					    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHERREDEEM))	) {
									orderData.put("VoucherRedeem", true);
									log.info("boolean value for Change account object  : "
											+ orderData.get("VoucherRedeem"));
					    		}
							}
						} else {
							if (order.getAccount().getExtendedDataList() != null) {
								extDatList=order.getAccount().getExtendedDataList().getValue();
								log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
								if (extDatList.getExtendedData().size() > 0) {
									orderData.put("UpdateAct", true);
									log.info("boolean value for Change account object  : "
											+ orderData.get("UpdateAct"));
								} else {
									throw new C30SDKToolkitException("Account data missing");							 
								}
							} else {
								throw new C30SDKToolkitException("Account data missing");
							}
						}
					} else {
						if (order.getAccount().getExtendedDataList() != null) {
							extDatList=order.getAccount().getExtendedDataList().getValue();
							log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
							if (extDatList.getExtendedData().size() > 0) {
								orderData.put("ActExtData", true);
								log.info("boolean value for Change account object  : "
										+ orderData.get("ActExtData"));
							} else {
								throw new C30SDKToolkitException("Account data missing");							 
							}
						}  else {
							throw new C30SDKToolkitException("Account data missing");
						}
					}
					
				}
			}

			//Check on the ServiceObject list and Identify the Action Status 
			if (order.getServiceObjectList() != null)
			{
				List<C30ServiceObject> sList = (List<C30ServiceObject>) order
						.getServiceObjectList().getServiceObject();
				log.info("Working on the Service Object.  Size of list is: "
						+ sList.size());
				for (C30ServiceObject service : sList)
				{
					HashMap<String, String> ProvSIMInquiry = new HashMap<String, String>();
					HashMap<String, String> ProvTNInquiry = new HashMap<String, String>();
					HashMap<String, String> ProvCLIInquiry = new HashMap<String, String>();
					
					String clientId = null;
					String actionType = null;
					String clientAcctId = null;
					String planExpiryDate = null;
					String compExpiryDate = null;					
					
					if (service.getClientAccountId() != null)	{clientAcctId = service.getClientAccountId().toString();}
					if (service.getClientActionType() != null)	{actionType = service.getClientActionType().toString();}
					if (service.getClientActionType() != null)	{clientId = service.getClientId();}
					
					log.info("service Client Account Id : '"
							+ clientAcctId
							+ "'  Service Client id: '" + clientId
							+ "'  Action type: '" + actionType + "'");

					if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
						if (service.getClientActionTypeReason() != null) {
							//PORT IN
							if (service.getClientActionTypeReason().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_IN)) {
								orderData.put("PortIn", true);
							}
						}
						if (service.getClientServicePrimaryIdentifier()!=null) {
							orderData.put("AddService", true);
							log.info("boolean value for AddService object: "+ orderData.get("AddService"));
							ProvTNInquiry.put(Constants.TELEPHONE_NUMBER, service.getClientServicePrimaryIdentifier());
							ProvCLIInquiry.put(Constants.PREPAID_CLI, service.getClientServicePrimaryIdentifier());							
						} else {
							throw new C30SDKToolkitException("Telephone Number couldn't find in the payload cannot continue");							
						}
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SUSPEND))
					{
						orderData.put("SuspendService", true);
						log.info("boolean value for SuspendService object: "
								+ orderData.get("SuspendService"));
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_RESUME))
					{
						orderData.put("ResumeService", true);
						log.info("boolean value for ResumeService object: "
								+ orderData.get("ResumeService"));
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC))
					{
						//PORT OUT
						if (service.getClientActionTypeReason().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_OUT)) {
							orderData.put("PortOut", true);
						}
						orderData.put("DisconnectService", true);
						log.info("boolean value for DiscService object: "
								+ orderData.get("DisconnectService"));
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE))
					{
						if ((service.getComponentObjectList() != null) || (service.getPlanObject() != null))							
						{
							log.info("Set boolean value for Update Account object");
							//Account Update order with swap plan/add component
							if (orderData.containsKey("ActExtData")) {
								log.info("Found Account Extended Data");
								if (service.getPlanObject() != null) {
									if (service.getPlanObject().getValue().getClientActionType().toString().equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
										orderData.put("UpdateService", true);
										log.info("boolean value for UpdateService object: "
											+ orderData.get("UpdateService"));
									}
								}
								if (service.getComponentObjectList() != null) {
									orderData.put("UpdateService", true);
									log.info("boolean value for UpdateService object: "
										+ orderData.get("UpdateService"));
								}
							}
						}
					} // End 

					//Check on the PlanObject list and Identify the Action Status					
					if (service.getPlanObject() != null) {
						C30ExtendedDataList extDatList=null;	
						//Check on the Plan Object
						C30PlanObject plan = service.getPlanObject().getValue();
						if (plan != null) {
							if (plan.getClientActionType().toString().equalsIgnoreCase(
									C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
								orderData.put("AddPlan", true);
								log.info("boolean value for AddPlan object: "
										+ orderData.get("AddPlan"));
								//If Plan Object has Extended Data
								if (plan.getExtendedDataList() != null) {
									extDatList=plan.getExtendedDataList().getValue();
									log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
									List<C30ExtendedData> extdtList = extDatList.getExtendedData();
									for(C30ExtendedData extData : extdtList) {
										if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PLAN_EXPIRATION_DATE)) {
											planExpiryDate = extData.getExtendedDataValue().toString();
										}							
									}	
								}								

							} else if (plan.getClientActionType().toString().equalsIgnoreCase(
									C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {
								//If Plan Object has Extended Data
								if (plan.getExtendedDataList() != null) {
									extDatList=plan.getExtendedDataList().getValue();
									log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
									List<C30ExtendedData> extdtList = extDatList.getExtendedData();
									for(C30ExtendedData extData : extdtList) {
										if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PLAN_RATE)) {
											log.info("Plan Rate = " +Integer.valueOf(extData.getExtendedDataValue().toString()));
											if(Integer.valueOf(extData.getExtendedDataValue().toString()) > 0) {	
												orderData.put("PlanRecharge", true);
											} else if(Integer.valueOf(extData.getExtendedDataValue().toString()) == 0){
												orderData.put("ExtendExpiryDate", true);
											}
										}							
									}	
								}
							} else if (plan.getClientActionType().toString().equalsIgnoreCase(
									C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
								//SWAP Plan without Account Update
								orderData.put("SwapPlan", true);
								log.info("boolean value for SwapPlan object: "
										+ orderData.get("SwapPlan"));
								if (plan.getPlanName().contains(C30WPSQLConstants.PREPAID_METERED_PLAN)) {
									orderData.put("MeterPlan", true);
									log.info("boolean value for SwapPlan object: "
											+ orderData.get("MeterPlan"));
								}
							} else if (plan.getClientActionType().toString().equalsIgnoreCase(
									C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
								orderData.put("DiscPlan", true);
								log.info("boolean value for DiscPlan object: "
										+ orderData.get("DiscPlan"));
							}//End - Check on the Plan Object
						}
						else { 
							log.info("Plan object IS Null");	
						}
					}

					//Check on the ComponentObject list and Identify the Action Status
					if (service.getComponentObjectList() != null)
					{
						C30ExtendedDataList extDatList=null;
						List<C30ComponentObject> clList = (List<C30ComponentObject>) service
								.getComponentObjectList().getComponentObject();
						log.info("Working on the Component Object.  Size of list is "
								+ clList.size());
						for (C30ComponentObject comp : clList)
						{
							int compPrice = 0;
							if (comp.getClientActionType()
									.toString()
									.equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
							{ // Add
								orderData.put("AddComp", true);
								log.info("boolean value for AddComp object: "
										+ orderData.get("AddComp"));
								//If Plan Object has Extended Data
								if (comp.getExtendedDataList() != null) {
									extDatList=comp.getExtendedDataList().getValue();
									log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
									List<C30ExtendedData> extdtList = extDatList.getExtendedData();
									for(C30ExtendedData extData : extdtList) {
										//Get the Component Expiry Date
										if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_EXPIRY_DATE)) {
											compExpiryDate = extData.getExtendedDataValue().toString();
										}	
										//Get the Component Price
										if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_RATE)) {
											compPrice = Integer.valueOf(extData.getExtendedDataValue().toString());
										}	
									}	
								}										
							} else if (comp
									.getClientActionType()
									.toString()
									.equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
								// Disconnect Component
								orderData.put("DiscComp", true);
								log.info("boolean value for DiscComp object: "
										+ orderData.get("DiscComp"));
							}
							String targetSystem = getComponentTargetSystem(service);
							if (targetSystem != null) {
								if (targetSystem.equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_SPS) ||
										targetSystem.equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_OCS_SPS)) {
									orderData.put("ProvisionRequired", true);
								}
								if (targetSystem.equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_OCS) ||
										targetSystem.equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_OCS_SPS)) {
									orderData.put("OCSComp", true);
									if (compPrice > 0) {
										orderData.put("DebitAddonPkgFee", true);
									}
								}
								if (targetSystem.equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_SPS)) {
									orderData.put("SPSComp", true);
								}
							} else {
								throw new C30SDKToolkitException("Couldn't find Component Target System in the payload cannot continue");									
							}
							
						}// End-for (C30ComponentObject comp : clList)

					} // End - if (service.getComponentObjectList() != null)

					//Check on the IdentifierObject list and Identify the Action Status					
					if (service.getIdentifierObjectList() != null)
					{
						List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
						log.info("Working on the Identifier Object.  Size of list is "+ ilList.size());
						String idnAction=null;
						boolean iccidValue=false;						
						int iccidCnt = 0;
						int telphoneNumberCnt = 0;						
						for (C30IdentifierObject identifier : ilList)
						{
							//ADD
							if (identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
								idnAction=C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD;								
								if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) {
									if (iccidCnt > 1) {
										throw new C30SDKToolkitException("ICCID cannot be more than ONE for a service, cannot continue");
									} else {
										iccidValue=true;
										orderData.put("AddIdn", true);
										log.info("boolean value for AddIdn object: "+ orderData.get("AddIdn"));
										ProvSIMInquiry.put(C30WPSQLConstants.C30_ICCID, identifier.getIdentifierIdIn());
									}
									iccidCnt++;
								} 
							}
							//DISCONNECT
							if (identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
								idnAction=C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC;									
								orderData.put("DiscIdn", true);
								log.info("boolean value for DiscIdn object: "+ orderData.get("DiscIdn"));
							} 
							//CHANGE and SWAP
							if ((service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) 
									&& (identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))) {

								idnAction=C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP;
								//Check for CHANGE SIM
								if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) {
									if (iccidCnt > 1) {
										throw new C30SDKToolkitException("ICCID cannot be more than ONE for a service, cannot continue");
									} else {
										iccidValue=true;
										orderData.put("ChangeSIM", true);
										log.info("boolean value for ChangeSIM object: " + orderData.get("ChangeSIM"));
									}
									iccidCnt++;
								} 
								//Check for CHANGE TN
								if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_TELEPHONE_NUMBER)) {
									if (telphoneNumberCnt > 1) {
										throw new C30SDKToolkitException("TELEPHONE NUMBER cannot be more than ONE for a service, cannot continue");
									} else {
										//Check if the NEW TN is already ACTIVE in OCS
										ProvCLIInquiry.put(Constants.PREPAID_CLI, identifier.getIdentifierIdIn());
										ValidateCLIinOCS(ProvCLIInquiry, service, orderData);
										orderData.put("ChangeTN", true);
										log.info("boolean value for ChangeTN object: " + orderData.get("ChangeTN"));
									}
									telphoneNumberCnt++;
									log.info("telphoneNumberCnt =  " + telphoneNumberCnt);
								} 
							}
							
						} // End - for (C30IdentifierObject identifier :ilList)

						if (idnAction.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
							if (!iccidValue){
								throw new C30SDKToolkitException("ICCID couldn't find in the payload cannot continue");
							}
						}
						
					}// End-if (service.getIdentifierObjectList() != null)

					//Validate SIM and TN for Add Orders
					if (validateTN == true) {
						if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
							ValidateSIM(ProvSIMInquiry);
							ValidateTN(ProvTNInquiry);
							ValidateCLIinOCS(ProvCLIInquiry, service, orderData);
						}
					}
					//Debit Plan Fee Status
					if (planExpiryDate != null) {
			            Calendar calExpiryDate = Calendar.getInstance();
			             DateFormat dfZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			             calExpiryDate.setTime(dfZ.parse(planExpiryDate));
			           	 Calendar calnow = Calendar.getInstance();
			        	 java.util.Date currentDate = calnow.getTime();
			        	 calnow.setTime(dfZ.parse(dfZ.format(currentDate).toString()));
			             if (calnow.before(calExpiryDate)) {
			            	 orderData.put("DebitPlanFee", true);
			             }
					} else if ((service.getPlanObject() != null) && (planExpiryDate == null) && (orderData.containsKey("AddPlan"))) {
						throw new C30SDKToolkitException("Plan Expiry Date is " + planExpiryDate +" Cannot Continue");
					}

					//Add on Package Debit Plan Fee Status
					if (service.getComponentObjectList() != null && compExpiryDate == null && orderData.containsKey("OCSComp")) {
						throw new C30SDKToolkitException("Component Expiry Date is " + compExpiryDate +" Cannot Continue");
					} 
				} // End - for (C30ServiceObject service : sList)
				
			}// End-if(order.getServiceObjectList()!=null)
		}
		catch (Exception e)
		{
			log.error("EXCP - Check Prepaid Order Data :" + e);
			e.printStackTrace();
			throw new C30SDKToolkitException("ORDER-Check Prepaid Order Request Process: "+e.getMessage(), e);
		}
		log.info("Prepaid Order Data details :" + orderData);
		return orderData;
	}
	
	/** Validate all the input parameters For ADD Account Order
	 * ClientId, CPNIPin, OCSSubscriberProfileId validated
	 * 
	 *   
	 */
	private void validateInputDataForAddAccount() throws C30SDKToolkitException 
	{
		//Validate if Service Object Exists
		if (order.getServiceObjectList() != null) {
			boolean cpni = false;
			boolean profile = true;
			C30ExtendedDataList actExtDatList=null;			
			if (order.getAccount().getClientId() == null) {
				throw new C30SDKToolkitException("Could not find Required Data in the payload, Cannot continue - ClientId not found");
			}
			if (order.getAccount().getExtendedDataList() != null) {
				actExtDatList=order.getAccount().getExtendedDataList().getValue();
				List<C30ExtendedData> extdtList = actExtDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList) {
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_CPNI_PIN)) {
						cpni = true;
					} else if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PLAN_PROFILE)) {
						profile = true;
					}		
				}
				if (cpni == false || profile == false ) {
					throw new C30SDKToolkitException("Could not find Required Data in the Extended Data List, Cannot continue");					
				}
			}
			else {
				throw new C30SDKToolkitException("Could not find Extended Data in the Payload, Cannot continue");
			}
		}
		
	}	

	/**This method is responsible for Validating SIM for ADD orders
	 * 
	 * @param ProvSIMInquiry
	 * @throws C30SDKToolkitException
	 * 
	 *  
	 */
	private void ValidateSIM(HashMap<String, String> ProvSIMInquiry) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try{
			log.info("Send Validate SIM Inquiry for ICCID : "+ProvSIMInquiry.get(Constants.ICCID));			
			ProvSIMInquiry.put(Constants.ACCT_SEG_ID,acctSegId);
			ProvSIMInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			ProvSIMInquiry.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			ProvSIMInquiry.put("RequestName", Constants.REQUEST_VALIDATE_SIM);		
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.awnInquiryRequest(ProvSIMInquiry) ;
			log.info("Response XML : "+response.get("ResponseXML"));
			log.info("IMSI Retreived: "+response.get(Constants.IMSI).toString());
			//Get SIM Status
			String retString = C30ProvConnectorUtils.getXmlStringElementValue(response.get("ResponseXML").toString(), C30WPSQLConstants.C30_PROV_SPS_PREPAID_VALUE);
			log.info("retString = "+retString);
			if (!retString.equalsIgnoreCase(C30WPSQLConstants.C30_PROV_SPS_PREPAID_TRUE)){
				log.info(" Return Value from SPS is not TRUE");
				throw new C30SDKToolkitException("Prepaid Return Value from SPS is "+retString + " Cannot Continue");
			}
	
		}// End - Try
		catch (C30ProvConnectorException e)
		{
			log.error("ERROR - Account Segment: " + ProvSIMInquiry.get(Constants.ACCT_SEG_ID).toString() + " ValidateSIM : "+e);
			throw new C30SDKToolkitException("EXCP: ValidateSIM: "+e, e);			
		}		
		catch (Exception e)
		{
			log.error("ERROR - Account Segment: " + ProvSIMInquiry.get(Constants.ACCT_SEG_ID).toString() + " ValidateSIM : "+e);
			throw new C30SDKToolkitException("EXCP: ValidateSIM: "+e, e);
		}		
	}

	/**This method is responsible for Validating TN for ADD orders
	 * 
	 * @param ProvTNInquiry
	 * @throws C30SDKToolkitException
	 * 
	 *  
	 */
	private void ValidateTN(HashMap<String, String> ProvTNInquiry) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		
		try{
			log.info("Send Validate TN Inquiry for Telephonenumber : "+ProvTNInquiry.get(Constants.TELEPHONE_NUMBER));			
			ProvTNInquiry.put(Constants.ACCT_SEG_ID,acctSegId);
			ProvTNInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			ProvTNInquiry.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			ProvTNInquiry.put("RequestName", Constants.REQUEST_TELEPHONE_NUMBER_VERIFICATION);		
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.awnInquiryRequest(ProvTNInquiry) ;
			log.info("Response XML : "+response.get("ResponseXML"));
			log.info("TN Validated : "+response.get(Constants.TELEPHONE_NUMBER).toString());
	
		}// End - Try
		catch (C30ProvConnectorException e)
		{
			log.error("ERROR - Account Segment: " + ProvTNInquiry.get(Constants.ACCT_SEG_ID).toString() + " ValidateTN: "+e);
			throw new C30SDKToolkitException("EXCP: ValidateTN: "+e, e);
		}
		catch (Exception e)
		{
			log.error("ERROR - Account Segment: " + ProvTNInquiry.get(Constants.ACCT_SEG_ID).toString() + " ValidateTN: "+e);
			throw new C30SDKToolkitException("EXCP: ValidateTN: "+e, e);
		}		
	}	
	
	/**This method is responsible for Validating TN for ADD orders
	 * 
	 * @param ProvTNInquiry
	 * @throws C30SDKToolkitException
	 * 
	 *  
	 */
	private void ValidateCLIinOCS(HashMap<String, String> ProvCLIInquiry, C30ServiceObject Service, Map<String, Boolean> orderData) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		
		try{
			String CLI = ProvCLIInquiry.get(Constants.PREPAID_CLI);
			log.info("Send Subscriber.GetByCLI Inquiry for Telephonenumber : "+CLI);
			ProvCLIInquiry.put(Constants.ACCT_SEG_ID,acctSegId);
			ProvCLIInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			ProvCLIInquiry.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			ProvCLIInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);			
			ProvCLIInquiry.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_GET_BYCLI);			
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.ocsInquiryRequest(ProvCLIInquiry);
			log.info("Response XML : "+response.get("ResponseXML"));
			
			//Get CLI Status
			String retString = C30ProvConnectorUtils.getOCSMemberValuefromResponse(response.get("ResponseXML").toString(), 
					C30WPSQLConstants.PREPAID_OCS_SUBSCRIBER_STATE_LABEL);
			String respCode = response.get("ResponseCode").toString();
			log.info("retString = "+retString);
			log.info("respCode = "+respCode);
			
			if (Service.getClientActionTypeReason() != null) {
				if (Service.getClientActionTypeReason().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_IN)) {
					log.info("PORT IN");
					if (!respCode.equals(C30WPSQLConstants.PREPAID_OCS_CLI_CHECK_FAULT_CODE)) {
						log.info("OCS FAULT CODE");
						if (retString.equalsIgnoreCase(C30WPSQLConstants.PREPAID_OCS_SUBSCRIBER_STATE_LABEL_VALUE)){
							HashMap<String, String> ProvCLISubLineInquiry = new HashMap<String, String>();
							String subscriberLineId = null;
							//Get the Subscriber Line Id
							subscriberLineId = C30AWNPrepaidUtils.getSubscriberLinefromOCS(ProvCLISubLineInquiry, Service, orderData, acctSegId, subOrderId, 
									transactionId, internalAuditId);
							if (subscriberLineId != null) {
								//Store Sub Order Extended Data in the database.
								C30OrderExtendedDataLog logOrderExtData = new C30OrderExtendedDataLog(acctSegId, subOrderId, transactionId);
								logOrderExtData.writeToSubOrderExtendedData(C30WPSQLConstants.PREPAID_PORT_IN_SUBSCRIBER_LINE_ID_KEY, subscriberLineId, 
										C30WPSQLConstants.C30_EXT_DATA_SEND_FLAG_FALSE);	
							} else {
								throw new C30SDKToolkitException("Couldn't find SubscriberLine Id in OCS Cannot Continue");
							}
							orderData.put("ChangeTN", true);
							log.info("TN is ACTIVE in OCS");
						}
					}
				} else if (retString != null) {
					if (retString.equalsIgnoreCase(C30WPSQLConstants.PREPAID_OCS_SUBSCRIBER_STATE_LABEL_VALUE)){
						throw new C30SDKToolkitException("CLI " + CLI+ " is "+retString + " in OCS Cannot Continue");
					}
				}
			} else if (retString != null) {
				if (retString.equalsIgnoreCase(C30WPSQLConstants.PREPAID_OCS_SUBSCRIBER_STATE_LABEL_VALUE)){
					throw new C30SDKToolkitException("CLI " + CLI+ " is "+retString + " in OCS Cannot Continue");
				}
			}
			
		}// End - Try
		catch (C30ProvConnectorException e)
		{
			log.error("ERROR - Account Segment: " + ProvCLIInquiry.get(Constants.ACCT_SEG_ID).toString() + " Subscriber.GetByCLI: "+e);
				throw new C30SDKToolkitException("EXCP:ValidateCLIinOCS:"+e, e);
		}
		catch (Exception e)
		{
			log.error("ERROR - Account Segment: " + ProvCLIInquiry.get(Constants.ACCT_SEG_ID).toString() + " Subscriber.GetByCLI: "+e);
				throw new C30SDKToolkitException("EXCP:ValidateCLIinOCS: "+e, e);
		}		
	}	

	
	/**This method is responsible for Identifying Component Target System
	 * 
	 * @param service
	 * @throws C30SDKToolkitException
	 * 
	 *  
	 */
	private String getComponentTargetSystem(C30ServiceObject service) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		String targetSystem = null;
		C30ExtendedDataList compExtDatList=null;		
		
		try{
		    List<C30ComponentObject> clList = (List<C30ComponentObject>) service.getComponentObjectList().getComponentObject();
	    	log.info("Working on the Component Object.  Size of list is " + clList.size());					    
		    for (C30ComponentObject comp : clList) {
		    	compExtDatList=comp.getExtendedDataList().getValue();
				log.info(" Total Extended Data found : " + compExtDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = compExtDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList){
					if (extData.getExtendedDataName().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM)) {
						targetSystem = extData.getExtendedDataValue();
						log.info("targetSystem = "+targetSystem);
					}
				}			    	
		    }
		    return targetSystem;
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR- getComponentTargetSystem: "+e);
			throw new C30SDKToolkitException("EXCP: getComponentTargetSystem: "+e, e);
		}		
	}		
}
