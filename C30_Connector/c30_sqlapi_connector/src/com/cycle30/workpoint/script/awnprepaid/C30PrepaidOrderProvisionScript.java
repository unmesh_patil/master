package com.cycle30.workpoint.script.awnprepaid;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
//SDK
import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
//import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
//Workpoint
import com.cycle30.workpoint.script.C30OrderScript;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.prov.api.AddService;
import com.cycle30.workpoint.prov.api.SendFeature;
import com.cycle30.workpoint.prov.api.ChangeSIM;
import com.cycle30.workpoint.prov.api.ChangeTelephoneNumber;
import com.cycle30.workpoint.prov.api.ChangeServiceStatus;

//Provision
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;


/** This script works on the Order Object in the given XML for Provisioning
 * 
 * @author Umesh
 * - Initial Version
 * 
 * 
 */
public class C30PrepaidOrderProvisionScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30PrepaidOrderProvisionScript.class);
	private Map<String, String> outParam = null;
	private static String requestId = null;
	AddService addSvc = null;
	ChangeSIM chgSIM = null;
	ChangeTelephoneNumber chgTN = null;
	ChangeServiceStatus chgSvcStatus = null;
	
	public String getRequestId() {
		return requestId;
	}

	public static void setRequestId(String requestid) {
		requestId = requestid;
	}
	
	public C30PrepaidOrderProvisionScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidOrderProvisionScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException, C30ProvConnectorException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	public C30PrepaidOrderProvisionScript(String requestId, String requestName) throws C30SDKToolkitException 
	{
		super(requestId, requestName);
	}
	

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the Provisioning action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String orderDesiredDate = null;
			String actionWho = null;
	    	
			orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
			actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());
			
			if (orderDesiredDate == null) {
				orderDesiredDate = new Date().toString();
			}
			log.info("acctSegId: " + acctSegId + "orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);				
			log.info("START - Provisioning Request for : " + " acctSegId: " + acctSegId);
			
			HashMap<String, String> provRequestData = new HashMap<String, String>();
			//Prepare Data For Provisioning
			provRequestData.put(Constants.ACCT_SEG_ID,acctSegId);
			provRequestData.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			provRequestData.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			
			//Check the Service Object
			if (order.getServiceObjectList() != null)
			{
				List<C30ServiceObject> sList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
				log.info("Working on the Service Object.  Size of list is: "+ sList.size());
				for (C30ServiceObject service : sList)
				{
					String serviceId = null;
					String SIMIdentifierIdIn = null;
					String clientActionReason = null;
					String CatalogId = null;
				    String svcactionType  = service.getClientActionType().toString();
				    
					//ADD Service
					if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
					    serviceId = service.getClientServicePrimaryIdentifier();	
					    clientActionReason = service.getClientActionTypeReason().toString(); 
			            //Get the ICCID from a PAYLOAD
					    if (service.getIdentifierObjectList() != null)  {
					    	List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
					    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());					    	
					    	for (C30IdentifierObject identifier : ilList)  {
					    		if (identifier.getIdentifierTypeName().equalsIgnoreCase(Constants.ICCID)) {
						    		SIMIdentifierIdIn = identifier.getIdentifierIdIn();					    			
					    		}
					    	}
					    }
			            log.info("Sub order id: '" + subOrderId + "'  Service id: '" + serviceId + "'  Action type: '" + service.getClientActionType().toString());
			            
						//Get the Subscriber Profile Label if the Plan Object is populated.
						if (service.getPlanObject() != null)  {
							C30PlanObject plan = service.getPlanObject().getValue();
							
							if (plan.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {	
								String SPSCatalogId = plan.getPlanName();
								if (SPSCatalogId.contains(C30WPSQLConstants.C30_PREPAID_DATA_ONLY)) {
									//If GCI or ACS 
									if (acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_GCI_PREPAID)) {
										CatalogId = C30WPSQLConstants.C30_PROV_ATTRIB_PREPAID_DATAONLY;										
									} else if (acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_ACS_PREPAID)) {
										CatalogId = C30WPSQLConstants.C30_PROV_ATTRIB_ACS_PREPAID_DATAONLY;
									}
								} else {
									//If GCI or ACS 
									if (acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_GCI_PREPAID)) {
										CatalogId = C30WPSQLConstants.C30_PROV_ATTRIB_PREPAID_SERVICE;										
									} else if (acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_ACS_PREPAID)) {
										CatalogId = C30WPSQLConstants.C30_PROV_ATTRIB_ACS_PREPAID_SERVICE;
									}
								}
							}
							
							
							
							if (acctSegId.equals(C30WPSQLConstants.C30_SEGMENT_ACS_PREPAID) && plan.getPlanName().contains(C30WPSQLConstants.C30_ACS_PREPAID_VOICE_VVM)) {
								provRequestData.put(C30WPSQLConstants.C30_ACS_PREPAID_VOICE_VVM,C30WPSQLConstants.C30_ACS_PREPAID_VOICE_VVM_TRUE);
								log.info( "Add Service for VVM     "  +provRequestData.toString());
							}
							//ADD Service to Provisioning System
							addSvc = new AddService(CatalogId, serviceId, SIMIdentifierIdIn, provRequestData, clientActionReason,
				            		acctSegId, subOrderId, transactionId);
						}
			            
						// SEND - Feature/Component
	                    if (service.getComponentObjectList() != null) {
				            //Send Feature to Provisioning System
				            sendFeaturetoProvisioningSystem(service, provRequestData);
	                    }//End - if (service.getComponentObjectList() != null)

					}

					//CHANGE Service/SIM
					if (svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {
					    serviceId = service.getClientServicePrimaryIdentifier();						
			            log.info("Sub order id: '" + subOrderId + "'  Service id: '" + serviceId + "'  Action type: '" + service.getClientActionType().toString());
			            //Check the Identifier Action Type
					    if (service.getIdentifierObjectList() != null)  {
					    	List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
					    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());					    	
					    	for (C30IdentifierObject identifier : ilList)  {
					    		if (identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
					    			if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) {
							            //CHANGE SIM in Provisioning System
					    				chgSIM = new ChangeSIM(serviceId, identifier.getIdentifierIdOut(), identifier.getIdentifierIdIn(), provRequestData);
					    			} else if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_TELEPHONE_NUMBER)) {
							            //CHANGE Telephone Number in Provisioning System
						    			new ChangeTelephoneNumber(null, identifier.getIdentifierIdOut(), identifier.getIdentifierIdIn(), provRequestData);
					    			}
					    		}
					    	}
					    }	
					    
						// SEND - Feature/Component
	                    if (service.getComponentObjectList() != null) {
				            //Send Feature to Provisioning System
				            sendFeaturetoProvisioningSystem(service, provRequestData);
	                    }//End - if (service.getComponentObjectList() != null)

					}
					
					//DISCONNECT/SUSPEND/RESUME Service in Provisioning System
					if ((svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) ||
				    		(svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SUSPEND)) ||
				    		(svcactionType.equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_RESUME))) {
					    serviceId = service.getClientServicePrimaryIdentifier();
					    clientActionReason = service.getClientActionTypeReason().toString(); 
			            log.info("Sub order id: '" + subOrderId + "'  Service id: '" + serviceId + "'  Action type: '" + service.getClientActionType().toString());
			            //Change Service Status in Provisioning
			            chgSvcStatus = new ChangeServiceStatus(serviceId, null, svcactionType, clientActionReason, provRequestData, 
			            		acctSegId, subOrderId, transactionId);
					}
					
				}//End - for (C30ServiceObject service : sList)
				
			}//End - if (order.getServiceObjectList() != null)				
			
			log.info("END - Provisioning Request for : " + " acctSegId: " + acctSegId);				
		}
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid Provisioning Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Prepaid Provisioning Request "+e, e);
		}
	}

	
	/**Send the response parameters
	 * 
	 * 
	 */	
	public Map<String, String> getOutParam() {
		return outParam;
	}
	
	/**This method is responsible for Sending a Service Details to Provisioning system
	 * 
	 * @param service
	 * @param provRequestData
	 * 
	 * @throws C30SDKToolkitException , C30ProvConnectorException
	 */
	private void sendFeaturetoProvisioningSystem(C30ServiceObject service, HashMap<String, String> provRequestData) 
			throws C30SDKToolkitException, C30ProvConnectorException {
		
		// TODO Auto-generated method stub
		C30ExtendedDataList compExtDatList=null;		
		String serviceId = null;
		String compactionType  = null;
		
		log.info("sendFeaturetoProvisioningSystem for ServiceId: "+serviceId);
		try{
		    serviceId = service.getClientServicePrimaryIdentifier();

		    List<C30ComponentObject> clList = (List<C30ComponentObject>) service.getComponentObjectList().getComponentObject();
		    for (C30ComponentObject comp : clList) {
		    	compactionType=comp.getClientActionType().toString();
		    	log.info(" compactionType : " + compactionType);
		    	compExtDatList=comp.getExtendedDataList().getValue();
				List<C30ExtendedData> extdtList = compExtDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList){
					if (extData.getExtendedDataName().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM)) {
						if(!extData.getExtendedDataValue().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_TARGET_SYSTEM_OCS)) {
							new SendFeature(comp.getComponentName(), serviceId, null, provRequestData, compactionType); 
						}
					}
				}			    	
		    }
		}
	    catch (C30ProvConnectorException e) {
		    log.error("ERROR-Prepaid Feature Provisioning Request : "+e.getMessage());
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Prepaid Feature Provisioning Request: "+e, e);
	    }
		catch(Exception e) {
		    log.error("ERROR-Prepaid Feature Provisioning Request : "+e.getMessage());	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Prepaid Feature Provisioning Request: "+e.getMessage(), e );			
		}
	}
	
}
