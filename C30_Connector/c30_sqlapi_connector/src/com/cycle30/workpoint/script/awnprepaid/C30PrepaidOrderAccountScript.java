package com.cycle30.workpoint.script.awnprepaid;

//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

//import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.script.C30OrderScript;
import com.cycle30.workpoint.script.awnprepaid.utils.C30AWNPrepaidUtils;
import com.cycle30.workpoint.prepaidapi.SubscriberCreate;
import com.cycle30.workpoint.prepaidapi.SubscriberCredit;
import com.cycle30.workpoint.prepaidapi.SubscriberRechargeByCLIPIN;
import com.cycle30.workpoint.prepaidapi.SubscriberUpdate;
import com.cycle30.workpoint.prepaidapi.SubscriberRecharge;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.prov.data.Constants;
//import com.cycle30.prov.exception.C30ProvConnectorException;
//import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30BalanceList;
import com.cycle30.sdk.schema.jaxb.C30BalanceObject;
//import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
//import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;


/** This script works on the Account Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class C30PrepaidOrderAccountScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30PrepaidOrderAccountScript.class);

	
	public C30PrepaidOrderAccountScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidOrderAccountScript(String orderId, String subOrderId, String acctSegId, String transactionId, 
			String internalAuditId, String requestName) throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId, requestName);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String orderDesiredDate = null;
			String actionWho = null;
			String serviceId = null;
			String SubscriberId = null;
			
			HashMap<String, String> requestHashMap = new HashMap<String, String>();
			requestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			requestHashMap.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			requestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		
			requestHashMap.put(Constants.TELEPHONE_NUMBER, serviceId);
			requestHashMap.put(Constants.ATTR_CATALOG_ID, requestName);

			
			// If the Account Object is populated.
			if (order.getAccount() != null)
			{
	            
				orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
				actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());
				log.info("orderDesiredDate: '" + orderDesiredDate + "'  actionWho: '" + actionWho);	
				
				//If Order Date is null set the System date
				if (orderDesiredDate == null) {
		        	Date sysdate = new Date();
		        	GregorianCalendar gCal = new GregorianCalendar(); 
		        	gCal.setTime(sysdate); 
		        	XMLGregorianCalendar xgOrderDesiredate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal); 
					order.setOrderDesiredDate(xgOrderDesiredate);
					orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());					
				}//End - if (orderDesiredDate == null)	
				
				log.info("Working on the prepaid account object");

				//Account ADD Orders
				if(order.getAccount().getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
				{
					requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);
					
					//Process ADD Orders
					AddAccountInPrepaidSystem(requestName,  requestHashMap);
	
				}
				
				//Account CHANGE Orders
				if(order.getAccount().getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE))
				{
					log.info("Check for the Account Number");
					SubscriberId = C30AWNPrepaidUtils.getSubscriberNumber(order);
		            
		            if (SubscriberId == null) {
		            	throw new C30SDKToolkitException("Couldn't find SubscriberId for transactionId = "+transactionId+" subOrderId = "+subOrderId);
		            } else {
		    			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
		            }

		            //Process CHANGE ORDERS
		            ChangeAccountInPrepaidSystem(requestName,  requestHashMap);		            
				}
			}//End - if (order.getAccount() != null)
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Prepaid Account Order :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - Prepaid Account Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Prepaid Account Order Request Process", e );			
		}
	}


	/**This method is responsible for getting the input data required to update Account in the Prepaid system
	 * 
	 * @param account
	 * @param requestHashMap
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void AddAccountInPrepaidSystem(String requestName,  HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
		
		String ExtdatKey = Constants.OCS_SUBSCRIBER_ID;		
		
		//Subscriber.Create
		if (requestName.equals(Constants.OCS_SUBSCRIBER_CREATE)) {
			//Call API Subscriber.Create
			new SubscriberCreate(order.getAccount(), requestHashMap, acctSegId, subOrderId, transactionId);					
		}

		//Subscriber.Credit
		if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDIT)) {
			log.info("Subscriber.Credit - Check for the Account Number");
			String SubscriberId = C30AWNPrepaidUtils.getSubscriberNumber(order, subOrderId, transactionId, ExtdatKey, acctSegId);
            if (SubscriberId == null) {
            	throw new C30SDKToolkitException("Couldn't find SubscriberId for transactionId = "+transactionId+" subOrderId = "+subOrderId);
            } else {
				requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);			            	
            }
            
			requestHashMap  = C30AWNPrepaidUtils.getSubscriberCreditInputData(requestHashMap, order.getAccount());	
			log.info("requestHashMap = "+requestHashMap);
			//Call API Subscriber.Credit
			new SubscriberCredit(requestHashMap);
			
		}		
	}
	
	
	/**This method is responsible for getting the input data required to update Account in the Prepaid system
	 * 
	 * @param account
	 * @param requestHashMap
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void ChangeAccountInPrepaidSystem(String requestName,  HashMap<String, String> requestHashMap) throws C30SDKToolkitException {

        //Subscriber.Credit
		if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDIT)) {
			requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);						
			//Get the required data from Payload
			requestHashMap  = C30AWNPrepaidUtils.getSubscriberCreditInputData(requestHashMap, order.getAccount());	
			log.info("Subscriber.Credit - requestHashMap = "+requestHashMap);
			//Call API Subscriber.Credit
			new SubscriberCredit(requestHashMap);
		}

		//Subscriber.Update
		if (requestName.equals(Constants.OCS_SUBSCRIBER_UPDATE)) {
            log.info("Subscriber.Update");								
			requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_UPDATE_SUBSCRIBER);
			//Get the required data from Payload
			requestHashMap  = C30AWNPrepaidUtils.getSubscriberUpdateInputData(order.getAccount(), requestHashMap);	
			log.info("Subscriber.Update - requestHashMap = "+requestHashMap);			
			//Call API Subscriber.Update
			new SubscriberUpdate(requestHashMap);
		}				
		
		//Subscriber.Recharge
		if (requestName.equals(Constants.OCS_SUBSCRIBER_RECHARGE)) {
            log.info("Subscriber.Recharge");								
			requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_ORDER_BAL_UPDATE);
			//Get the required data from Payload
			requestHashMap  = C30AWNPrepaidUtils.getSubscriberRechargeInputData(order.getAccount(), requestHashMap);	
			log.info("Subscriber.Update - requestHashMap = "+requestHashMap);			
			//Call API Subscriber.Recharge
			new SubscriberRecharge(requestHashMap);
		}				

		//Subscriber.RechargeByCLIPIN
		if (requestName.equals(Constants.OCS_SUBSCRIBER_RECHARGE_BY_CLI_PIN)) {
            log.info("Subscriber.RechargeByCLIPIN");
			requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_ORDER_BAL_UPDATE);
//			requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, C30WPSQLConstants.PREPAID_ADD_SERVICE_REASON);
			//Get the INPUT Data for API call						
			getSubscriberRechargeByCLIPINInputData(order.getAccount(), requestHashMap);
			//Call API Subscriber.RechargeByCLIPIN
			new SubscriberRechargeByCLIPIN(requestHashMap);								
			
		}				
		
	}
	
	
	/**This method is responsible for getting the input data required for EventCharging.DirectDebit API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getSubscriberRechargeByCLIPINInputData(C30AccountObject account, HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		C30BalanceList balanceList=null;
		
		try{

			//Get the Balance Details
			if (account.getBalanceList() != null) {
				//Get Balance Object Data
				balanceList=account.getBalanceList().getValue();
				List<C30BalanceObject> balanceobjList = balanceList.getBalance();
				log.info("Get the Wallet Refill Data ");				
				for(C30BalanceObject balance : balanceobjList)
				{
					if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHERREDEEM)) ||
		    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHER_REDEEM))) {					
						if (balance.getVoucherPINCode()!= null) {requestHashMap.put(C30WPSQLConstants.PREPAID_VOUCHER_PINCODE, 
								C30SqlApiUtils.convertJaxbElementToString(balance.getVoucherPINCode()));}
		    		}
				}
			}
			
			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
	    	
			//Get the Subscriber Profile Label	    	
	    	for (C30ServiceObject service : serviceobjList)  {
			    String subscriberCLI = null;
	    		if (service.getClientServicePrimaryIdentifier() != null) 
	    			{ subscriberCLI = C30SqlApiUtils.convertJaxbElementToString(service.getClientServicePrimaryIdentifier());}
	    		//Add CLI to request
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_CLI, subscriberCLI);
			}	

		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getEventChargingDirectDebitInputData Request "+e, e);
		}		
	}	
	
}
