package com.cycle30.workpoint.script.awnprepaid;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
//Workpoint
import com.cycle30.workpoint.script.C30OrderScript;
import com.cycle30.workpoint.script.awnprepaid.utils.C30AWNPrepaidUtils;
//import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.data.Constants;
//Prepaid APIs
import com.cycle30.workpoint.prepaidapi.SubscriberLineUpdate;

/** This script works on the Identifier Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class C30PrepaidOrderIdentifierScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30PrepaidOrderIdentifierScript.class);

	public C30PrepaidOrderIdentifierScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidOrderIdentifierScript(String orderId, String subOrderId, String acctSegId, String transactionId, 
			String internalAuditId, String requestName) throws C30SDKToolkitException
			{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId, requestName);		
			}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String SubscriberId = null;
			String SubscribeLineId = null;
			String clientActionTypeReason = null;
			String temporaryTN = null;
			
			HashMap<String, String> requestHashMap = new HashMap<String, String>();			
			requestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			requestHashMap.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			requestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		

			//Check if the service object is empty.
			if (order.getServiceObjectList() != null) {

				List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
				log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
				for (C30ServiceObject service : serviceobjList)  {

					log.info("Sub order id: " + subOrderId + " transactionId: " + transactionId + " Action type: " + service.getClientActionType().toString());
					
					//PORT-IN
					if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
						clientActionTypeReason = service.getClientActionTypeReason();						
						if (clientActionTypeReason.equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_IN)) {
							SubscribeLineId = C30AWNPrepaidUtils.getSubOrderExtData(order, subOrderId, transactionId, 
									C30WPSQLConstants.PREPAID_PORT_IN_SUBSCRIBER_LINE_ID_KEY, acctSegId);
							temporaryTN = C30AWNPrepaidUtils.getSubOrderExtData(order, subOrderId, transactionId, 
									C30WPSQLConstants.C30_PROV_SPS_TEMP_TELEPHONE_NO, acctSegId);
						}
						if (SubscribeLineId != null && temporaryTN!= null) {
							//SWAP - TN information in OCS									
							swapTNinOCS(temporaryTN, requestHashMap, SubscriberId, SubscribeLineId);
						} else {
							throw new C30SDKToolkitException(" Couldn't Retreive SubscribeLineId/TemporaryTN information, Cannot Continue");	
						}
					}

					//IMSI/TN Swap
					if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {
						String SubscriberLineId = service.getClientId();
						log.info("Check for the Account Number");
						SubscriberId = C30AWNPrepaidUtils.getSubscriberNumber(order);
						//Check Identifier Object
						if (service.getIdentifierObjectList() != null)  {
							List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
							log.info("Working on the Identifier Object.  Size of list is " + ilList.size());
							for (C30IdentifierObject identifier : ilList)  {
								if(identifier.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
									if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_TELEPHONE_NUMBER)) {
										String telephoneNumber = identifier.getIdentifierIdIn();
										//SWAP TN information in OCS									
										swapTNinOCS(telephoneNumber, requestHashMap, SubscriberId, SubscriberLineId);
										} else if (identifier.getIdentifierTypeName().equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) {
										//SWAP IMSI information in OCS									
										swapIMSIinOCS(identifier, requestHashMap, SubscriberId, SubscriberLineId);
									}	
								} 							
							}
						}
					}
				}
			}
		}
		catch(C30SDKToolkitException e) {
			log.error("ERROR - Prepaid Identifier Order :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - Prepaid Identifier Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:Request-RequestName: "+requestName +" Message: "+e, e );			
		}
	}

	/**This method is responsible for changing SIM in the Prepaid system
	 * 
	 * @param account
	 * @param requestName
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void swapIMSIinOCS(C30IdentifierObject identifier, HashMap<String, String> requestHashMap, 
			String SubscriberId, String SubscriberLineId) throws C30SDKToolkitException {

		// TODO Auto-generated method stub

		try{
			requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);			
			//Get SubscriberId
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
			//Get SubscriberLineId
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID, SubscriberLineId);
			//Get the ICCID
			requestHashMap.put(Constants.ICCID, identifier.getIdentifierIdIn());
			//Call API SubscriberLine.Update
			new SubscriberLineUpdate(requestHashMap, SubscriberId);
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-swapIMSIinOCS Request "+e, e);
		}		

	}		

	/**This method is responsible for changing SIM in the Prepaid system
	 * 
	 * @param identifier
	 * @param requestHashMap
	 * @param SubscriberId
	 * @param SubscriberLineId
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void swapTNinOCS(String telephoneNumber, HashMap<String, String> requestHashMap, 
			String SubscriberId, String SubscriberLineId) throws C30SDKToolkitException {

		// TODO Auto-generated method stub

		try{
			requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);			
			//Get SubscriberLineId
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID, SubscriberLineId);
			//Get the CLI
			requestHashMap.put(Constants.PREPAID_CLI, telephoneNumber);
			//Call API SubscriberLine.Update
			new SubscriberLineUpdate(requestHashMap, SubscriberId);

		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-swapTNinOCS Request "+e, e);
		}		

	}	

}
