package com.cycle30.workpoint.script.awnprepaid;

//import java.util.ArrayList;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;







//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30BalanceList;
import com.cycle30.sdk.schema.jaxb.C30BalanceObject;
import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
//Workpoint
import com.cycle30.workpoint.script.C30OrderScript;
//import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.script.awnprepaid.utils.C30AWNPrepaidUtils;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.workpoint.prepaidapi.SubscriberDelete;
//Prepaid APIs
//import com.cycle30.workpoint.prepaidapi.SubscriberCreate;
import com.cycle30.workpoint.prepaidapi.SubscriberUpdate;
import com.cycle30.workpoint.prepaidapi.SubscriberActivateByCLI;
import com.cycle30.workpoint.prepaidapi.SubscriberCreditSet;
import com.cycle30.workpoint.prepaidapi.SubscriberCredit;
import com.cycle30.workpoint.prepaidapi.EventChargingDirectDebit;
import com.cycle30.workpoint.prepaidapi.SubscriberRechargeByCLIPIN;
import com.cycle30.workpoint.prepaidapi.SubscriberLineDelete;


/** This script works on the Service Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class C30PrepaidOrderServiceScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30PrepaidOrderServiceScript.class);

	public C30PrepaidOrderServiceScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidOrderServiceScript(String orderId, String subOrderId, String acctSegId, String transactionId, 
			String internalAuditId, String requestName) throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId, requestName);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String SubscriberId = null;
			String ExtdatKey = Constants.OCS_SUBSCRIBER_ID;
			
			HashMap<String, String> requestHashMap = new HashMap<String, String>();			
			requestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			requestHashMap.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			requestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		
			
            //ADD Order
		    //Check if the service object is empty.
		    if (order.getServiceObjectList() != null) {
			    
		    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
		    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
		    	for (C30ServiceObject service : serviceobjList)  {
				    
				    String clientId = service.getClientId();
				    String clientAccountId=service.getClientAccountId(); //PW-43 added by vijaya.
				    String actionType = service.getClientActionType().toString();
			    
		            log.info("Sub order id: '" + subOrderId + "'  Service Client id: '" + clientId + "'  Action type: '" + actionType + "'");
		            
		            //SERVICE ADD Orders
					if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {

						log.info("Check for the Account Number");
						SubscriberId = C30AWNPrepaidUtils.getSubscriberNumber(order, subOrderId, transactionId, ExtdatKey, acctSegId);
				        
			            if (SubscriberId == null) {
			            	throw new C30SDKToolkitException("Couldn't find SubscriberId for transactionId = "+transactionId+" subOrderId = "+subOrderId);
			            }				

						requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);
			            log.info("requestName : "+requestName);	
			            //adding ICCID and IMSI to the request hashmap from payload
			            if (service.getIdentifierObjectList() != null)  {
					    	List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
					    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());					    	
					    	for (C30IdentifierObject identifier : ilList)  {
					    		if (identifier.getIdentifierTypeName().equalsIgnoreCase(Constants.IMSI)) {
					    			String imsi=identifier.getIdentifierIdIn();
					    			log.info("IMSI="+imsi);
					    			requestHashMap.put(Constants.IMSI,imsi);
					    		}
					    		if (identifier.getIdentifierTypeName().equalsIgnoreCase(Constants.ICCID)) {
					    			String iccid=identifier.getIdentifierIdIn();
					    			log.info("ICCID="+iccid);
					    			requestHashMap.put(Constants.ICCID,iccid);
					    		}
					    	}
					    }
						//Process ADD Orders
						AddServiceInPrepaidSystem(requestName,  requestHashMap, SubscriberId);
					}
					
		            //SERVICE DISCONNECT Orders
					if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
						requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);						
						if (requestName.equals(Constants.OCS_SUBSCRIBER_LINE_DELETE)) {
				            log.info("SubscriberLine.Delete");
				            //If PORT-OUT disconnect
							if (service.getClientActionTypeReason().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PORT_OUT)) {
								log.info("Received Port out disconnect");
								HashMap<String, String> ProvCLISubLineInquiry = new HashMap<String, String>();
								HashMap<String, String> ProvCLISubscriberInquiry = new HashMap<String, String>();
								//Get the Subscriber Line Id
								clientId = C30AWNPrepaidUtils.getSubscriberLinefromOCS(ProvCLISubLineInquiry, service, null, acctSegId, subOrderId, 
										transactionId, internalAuditId);
								requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID, clientId);	
								//PW-43 added by vijaya
								//Get subscriber id
					           /* clientAccountId=C30AWNPrepaidUtils.getSubscriberfromOCS(ProvCLISubscriberInquiry, service, null, acctSegId, subOrderId, transactionId, internalAuditId);
					            log.info("subscriberId == "+clientAccountId);
					            requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, clientAccountId);*/
							} else {
								log.info("Not Port Out Disconnect Order");
								requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID, clientId);
								//requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, clientAccountId);
							}
							
							//Call API Subscriber.CreditSet
							new SubscriberLineDelete(requestHashMap);
							//Call API Susbscriber.Delete PW -43
							//new SubscriberDelete(requestHashMap);
							//PW-169 code changes
							// Call Subscriber.update for updating expiration date to 60 days after disconnect.
							Calendar c = Calendar.getInstance();
					        c.add(Calendar.DATE, 60);
					       	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							String ExpirationDate=df.format(c.getTime());
							log.info("Disconnect ExpirationDate*"+ExpirationDate);
							requestHashMap.put(Constants.PREPAID_EXPIRATIONDATE, ExpirationDate);
							log.info("Subscriber Id="+clientAccountId);
							requestHashMap.put(Constants.PREPAID_SUBSCRIBERID, clientAccountId);
							requestHashMap.remove(C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID);
							new SubscriberUpdate(requestHashMap);
							
						}
					}
					
		    	}
		    }
				
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Prepaid Account Order :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - Prepaid Account Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:addAccountInPrepaidSystem Request-RequestName: "+requestName +" Message: "+e, e );			
		}
	}

	
	/**This method is responsible for getting the input data required to update Account in the Prepaid system
	 * 
	 * @param account
	 * @param requestHashMap
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void AddServiceInPrepaidSystem(String requestName,  HashMap<String, String> requestHashMap, String SubscriberId)throws C30SDKToolkitException, C30ProvConnectorException, ParserConfigurationException, SAXException, IOException {
		

		//Subscriber.Update
		if (requestName.equals(Constants.OCS_SUBSCRIBER_UPDATE)) {
            log.info("Subscriber.Update");
			if (order.getAccount() != null && order.getServiceObjectList() != null) {
				getSubscriberUpdateInputData(order.getAccount(), requestHashMap, SubscriberId);
				//Call API Subscriber.Update
				new SubscriberUpdate(requestHashMap);
			}
		}

		//Subscriber.ActivateByCLI
		if (requestName.equals(Constants.OCS_SUBSCRIBER_ACTIVATE_BY_CLI)) {
            log.info("Subscriber.ActivateByCLI");
			getSubscriberActivatYyCLIInputData(requestHashMap);
			//Call API Subscriber.ActivateByCLI
			new SubscriberActivateByCLI(requestHashMap,acctSegId, subOrderId, transactionId,internalAuditId);								
		}
		
		//Subscriber.Credit
		if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDIT)) {
            log.info("Subscriber.Credit");								
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
			//Get the INPUT Data for API call
			getSubscriberCreditInputData(requestHashMap);						
			//Call API SubscriberCredit
			new SubscriberCredit(requestHashMap);								
		}

		//Subscriber.RechargeByCLIPIN
		if (requestName.equals(Constants.OCS_SUBSCRIBER_UPDATE_RECHARGE_VOUCHER)) {
            log.info("Subscriber.RechargeByCLIPIN");								
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
//			requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, C30WPSQLConstants.PREPAID_ADD_SERVICE_REASON);
			//Get the INPUT Data for API call						
			getSubscriberRechargeByCLIPINInputData(order.getAccount(), requestHashMap);
			//Call API Subscriber.RechargeByCLIPIN
			new SubscriberRechargeByCLIPIN(requestHashMap);								
		}
		
		//EventCharging.DirectDebit
		if (requestName.equals(Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT)) {
            log.info("EventCharging.DirectDebit");
			//Get the INPUT Data for API call						
			getEventChargingDirectDebitInputData(requestHashMap);
			//Call API EventCharging.DirectDebit
			new EventChargingDirectDebit(requestHashMap);								
		}
		
		//Subscriber.CreditSet
		if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDITSET)) {
            log.info("Subscriber.CreditSet");								
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
//			requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, C30WPSQLConstants.PREPAID_ADD_SERVICE_REASON);
			//Get the INPUT Data for API call						
			getSubscriberCreditSetInputData(requestHashMap);
			//Call API Subscriber.CreditSet
			new SubscriberCreditSet(requestHashMap);								
		}		
	
	}
	
	/**This method is responsible for changing an Account in the Prepaid system
	 * 
	 * @param account
	 * @param requestName
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getSubscriberUpdateInputData(C30AccountObject account,  HashMap<String, String> requestHashMap, 
			String SubscriberId) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		C30ExtendedDataList actExtDatList=null;				
		try{
			//Get SubscriberId
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);
			//Get the NumberSubtype and PreferredLang
			if (account.getExtendedDataList() != null) {
				actExtDatList=account.getExtendedDataList().getValue();
				log.info(" Total Extended Data found : " + actExtDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = actExtDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList)
				{
					
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE, extData.getExtendedDataValue().toString());							
					}
					else{
						requestHashMap.put(C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE, C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE_MOBILE);
					}
					
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PREFERRED_LANG)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_PREFERRED_LANG, extData.getExtendedDataValue().toString());							
					}
					else {
						requestHashMap.put(C30WPSQLConstants.PREPAID_PREFERRED_LANG, C30WPSQLConstants.PREPAID_PREFERRED_LANG_EN);
					}
					
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NOTIFICATION_PREFERENCE)) {
						if ((extData.getExtendedDataValue().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NOTIFICATION_PREFERENCE_SMS)) ||
								(extData.getExtendedDataValue().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NOTIFICATION_PREFERENCE_BOTH))) {
							requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_TRUE_VALUE);							
							requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_TRUE_VALUE);						
						} else {
							requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_FALSE_VALUE);							
							requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_FALSE_VALUE);						
						}
					}
				}
			} else {
				requestHashMap.put(C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE, C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE_MOBILE);
				requestHashMap.put(C30WPSQLConstants.PREPAID_PREFERRED_LANG, C30WPSQLConstants.PREPAID_PREFERRED_LANG_EN);
			}
			
			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
	    	for (C30ServiceObject service : serviceobjList)  {
			    String subscriberCLI = null;
	    		if (service.getClientServicePrimaryIdentifier() != null) 
	    			{ subscriberCLI = C30SqlApiUtils.convertJaxbElementToString(service.getClientServicePrimaryIdentifier());}
	    		//Add CLI to request
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_CLI, subscriberCLI);
	    		
	            //If the Identifier Object is populated.
				if (service.getIdentifierObjectList() != null)  {
					List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service.getIdentifierObjectList().getIdentifierObject();
			    	log.info("Working on the Identifier Object.  Size of list is " + ilList.size());
					for (C30IdentifierObject identifier : ilList)  {
						String	IdentifierTypeName = null;
						//Get the Identifier Type
						IdentifierTypeName = identifier.getIdentifierTypeName();
						if (IdentifierTypeName.equalsIgnoreCase(C30WPSQLConstants.C30_ICCID)) {
							requestHashMap.put(C30WPSQLConstants.C30_ICCID, identifier.getIdentifierIdIn());
						}
					}
				}
				
				//Get the Subscriber Profile Label if the Plan Object is populated.
				if (service.getPlanObject() != null)  {
					C30PlanObject plan = service.getPlanObject().getValue();
					//Get Plan Name
					String planName=plan.getPlanName();						
		    		requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_PROFILE_LABEL, planName);
				}
	    	}			
		
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-addAccountInPrepaidSystem Request "+e, e);
		}		
		
	}		

	
	/**This method is responsible for preparing input Data for Subscriber.ActivatByCLI from payload
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getSubscriberActivatYyCLIInputData(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		try{
			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
	    	for (C30ServiceObject service : serviceobjList)  {
			    String subscriberCLI = null;
	    		if (service.getClientServicePrimaryIdentifier() != null) 
	    			{ subscriberCLI = C30SqlApiUtils.convertJaxbElementToString(service.getClientServicePrimaryIdentifier());}
	    		//Add CLI to request
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_CLI, subscriberCLI);
	    	}			
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getSubscriberActivatYyCLIInputData: "+e, e);
		}		
		
		
	}		

	/**This method is responsible for preparing input Data for Subscriber.Credit from payload
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getSubscriberCreditInputData(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		String	reason=null;		
		try{
			
			//Get the Balance Details
			if (order.getAccount().getBalanceList() != null) {
				C30BalanceList balanceList=null;
				//Get Balance Object Data
				balanceList=order.getAccount().getBalanceList().getValue();
				List<C30BalanceObject> balanceobjList = balanceList.getBalance();
				log.info("Get the Wallet Refill/Balance Refill Data ");				
				for(C30BalanceObject balance : balanceobjList)
				{
					if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLETREFILL)) ||
						(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLET_REFILL)) ||
						(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCE_REFILL)) ||					
		    			(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCEREFILL))) {	
							if (balance.getBalanceTypeLabel()!=null) 
								{requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_TYPE_LABEL, balance.getBalanceTypeLabel());}
							if (balance.getBalanceUnits()!=null) {requestHashMap.put(C30WPSQLConstants.PREPAID_VALUE, 
									C30SqlApiUtils.convertJaxbElementToString(balance.getBalanceUnits().getValue()));}
							if (balance.getExpirationDate()!= null) {requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, balance.getExpirationDate());}
							log.info("Order - Expiration Date: " +balance.getExpirationDate());
							if (balance.getBalanceReason()!= null) {reason = balance.getBalanceReason();}
							else {
								reason = balance.getBalanceAction().toString().toUpperCase()+'_'+C30SqlApiUtils.
										convertJaxbElementToString(balance.getBalanceUnits().getValue()).toString();}
		    		}
				}
			}

			requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, reason);	    	
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getSubscriberCreditInputData: "+e, e);
		}		
		
		
	}	
	
	/**This method is responsible for getting the input data required for EventCharging.DirectDebit API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getSubscriberRechargeByCLIPINInputData(C30AccountObject account, HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		C30BalanceList balanceList=null;
		
		try{

			//Get the Balance Details
			if (account.getBalanceList() != null) {
				//Get Balance Object Data
				balanceList=account.getBalanceList().getValue();
				List<C30BalanceObject> balanceobjList = balanceList.getBalance();
				log.info("Get the Wallet Refill Data ");				
				for(C30BalanceObject balance : balanceobjList)
				{
					if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHERREDEEM)) ||
		    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOUCHER_REDEEM))) {					
						if (balance.getVoucherPINCode()!= null) {requestHashMap.put(C30WPSQLConstants.PREPAID_VOUCHER_PINCODE, 
								C30SqlApiUtils.convertJaxbElementToString(balance.getVoucherPINCode()));}
		    		}
				}
			}
			
			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
	    	
			//Get the Subscriber Profile Label	    	
	    	for (C30ServiceObject service : serviceobjList)  {
			    String subscriberCLI = null;
	    		if (service.getClientServicePrimaryIdentifier() != null) 
	    			{ subscriberCLI = C30SqlApiUtils.convertJaxbElementToString(service.getClientServicePrimaryIdentifier());}
	    		//Add CLI to request
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_CLI, subscriberCLI);
			}	

		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getEventChargingDirectDebitInputData Request "+e, e);
		}		
	}	
	
	/**This method is responsible for getting the input data required for Subscriber.CreditSet API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getSubscriberCreditSetInputData(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		C30ExtendedDataList extDatList=null;		
		String	planName=null;		
		String	reason=null;		
		String	balValue=null;
		long balValueLong = 0;
        
		try{

			//Get the Balance Details
			if (order.getAccount().getBalanceList() != null) {
				C30BalanceList balanceList=null;
				//Get Balance Object Data
				balanceList=order.getAccount().getBalanceList().getValue();
				List<C30BalanceObject> balanceobjList = balanceList.getBalance();
				log.info("Get the Wallet Refill/Balance Refill Data ");				
				for(C30BalanceObject balance : balanceobjList)
				{
					if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLETREFILL)) ||
						(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLET_REFILL))) {	
							if (balance.getBalanceTypeLabel()!=null) 
								{requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_TYPE_LABEL, balance.getBalanceTypeLabel());}
							if (balance.getBalanceUnits()!=null) 
								{balValue = C30SqlApiUtils.convertJaxbElementToString(balance.getBalanceUnits().getValue());
								balValueLong = balValueLong+Long.parseLong(balValue);}
							if (balance.getExpirationDate()!= null) 
								{requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_EXPIRATION_DATE, balance.getExpirationDate());}
							log.info("Order - Expiration Date: " +balance.getExpirationDate());
							if (balance.getBalanceReason()!= null) {reason = balance.getBalanceReason();}
							else {
								reason = C30WPSQLConstants.PREPAID_ADD_SERVICE_REASON;
//								reason = balance.getBalanceAction().toString().toUpperCase()+'_'+C30SqlApiUtils.
//										convertJaxbElementToString(balance.getBalanceUnits().getValue()).toString();
								}
		    		}
				}
			}
			balValue = String.valueOf(balValueLong);
			log.info("Wallet Refill balValue = " +balValue);
			requestHashMap.put(C30WPSQLConstants.PREPAID_VALUE, balValue);			
			requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, reason);	    	
			
			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
			//Get the Subscriber Profile Label	    	
	    	for (C30ServiceObject service : serviceobjList)  {   		
	            //If the Plan Object is populated.
				if (service.getPlanObject() != null)  {
					C30PlanObject plan = service.getPlanObject().getValue();
					//Get Plan Name
					planName=plan.getPlanName();	
					//If Plan Object has Extended Data
					if (plan.getExtendedDataList() != null) {
						extDatList=plan.getExtendedDataList().getValue();
						log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
						List<C30ExtendedData> extdtList = extDatList.getExtendedData();
						for(C30ExtendedData extData : extdtList)
						{
							if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PLAN_EXPIRATION_DATE)) {
								requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, extData.getExtendedDataValue().toString());
							}
						}							
					}					
				}
	    	}		
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_PROFILE_LABEL, planName);	   	
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getSubscriberCreditSetInputData: "+e, e);
		}		
		
	}	
	
	/**This method is responsible for getting the input data required for EventCharging.DirectDebit API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getEventChargingDirectDebitInputData(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		String	planName=null;		
		try{

			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
	    	
			//Get the Subscriber Profile Label	    	
	    	for (C30ServiceObject service : serviceobjList)  {
			    String subscriberCLI = null;
	    		if (service.getClientServicePrimaryIdentifier() != null) 
	    			{ subscriberCLI = C30SqlApiUtils.convertJaxbElementToString(service.getClientServicePrimaryIdentifier());}
	    		//Add CLI to request
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_CLI, subscriberCLI);
	    		
				//Get the Charging Event Label	-- This need to be changed once OCS setup the Charging Event				
				if (service.getPlanObject() != null)  {
					C30PlanObject plan = service.getPlanObject().getValue();
					//Get Plan Name
					planName=plan.getPlanName();						
		    		requestHashMap.put(C30WPSQLConstants.PREPAID_CHARGE_EVENT_LABEL, planName);
					
				}
			}	
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getEventChargingDirectDebitInputData Request "+e, e);
		}		
	}	

	
}
