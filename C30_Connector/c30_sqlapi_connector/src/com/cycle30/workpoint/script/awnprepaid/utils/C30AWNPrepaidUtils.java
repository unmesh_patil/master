package com.cycle30.workpoint.script.awnprepaid.utils;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;
import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30BalanceList;
import com.cycle30.sdk.schema.jaxb.C30BalanceObject;
import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.prepaidapi.SubscriberCredit;
import com.cycle30.workpoint.script.C30OrderExtendedDataLog;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

public class C30AWNPrepaidUtils {

	private static Logger log = Logger.getLogger(C30AWNPrepaidUtils.class);
	

	/**This method is responsible to get SubscriberId from database/payload
	 * 
	 * @param order
	 * @param subOrderId
	 * @param transactionId
	 * @param ExtdatKey
	 * @param acctSegId 
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */	
	
	public static String getSubscriberNumber( OrderRequest order, String subOrderId, String transactionId, String ExtdatKey, String acctSegId) throws C30SDKToolkitException
	{
		
		String SubscriberId = null;

		if (ExtdatKey != null)
		{
			//Fetch SubscriberId from Sub Order Extended Data.
			C30OrderExtendedDataLog logOrderExtData = new C30OrderExtendedDataLog(acctSegId, subOrderId, transactionId);
			SubscriberId = logOrderExtData.fetchSubOrderExtendedData(subOrderId, transactionId,ExtdatKey);
		}
		else if (order.getAccount()!=null && order.getAccount().getClientId() != null) {
			SubscriberId = C30SqlApiUtils.convertJaxbElementToString(order.getAccount().getClientId());
		}
		else if (order.getServiceObjectList() != null )
		{
			List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	for (C30ServiceObject service : serviceobjList)  {
	    		SubscriberId = service.getClientAccountId();
	    	}
		}
		
		return SubscriberId;
	}
	
	/**This method is responsible to get SubscriberId from payload
	 * 
	 * @param order
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */	
	public static String getSubscriberNumber( OrderRequest order)
	{
		
		String SubscriberId = null;
		if (order.getAccount()!=null && order.getAccount().getClientId() != null) {
			SubscriberId = C30SqlApiUtils.convertJaxbElementToString(order.getAccount().getClientId());
		}
		else if (order.getServiceObjectList() != null )
		{
			List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	for (C30ServiceObject service : serviceobjList)  {
	    		SubscriberId = service.getClientAccountId();
	    	}
		}
		
		return SubscriberId;
	}
	
	/**This method is responsible to get SubscriberLineId from database
	 * 
	 * @param order
	 * @param subOrderId
	 * @param transactionId
	 * @param ExtdatKey
	 * @param acctSegId 
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */	
	
	public static String getSubOrderExtData( OrderRequest order, String subOrderId, String transactionId, String ExtdatKey, String acctSegId) throws C30SDKToolkitException
	{
		
		String extDataValue = null;

		if (ExtdatKey != null)
		{
			//Fetch SubscriberId from Sub Order Extended Data.
			C30OrderExtendedDataLog logOrderExtData = new C30OrderExtendedDataLog(acctSegId, subOrderId, transactionId);
			extDataValue = logOrderExtData.fetchSubOrderExtendedData(subOrderId, transactionId,ExtdatKey);
		}
		
		return extDataValue;
	}
	
	/**This method is responsible for preparing input Data for Subscriber.Credit from payload
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	public static HashMap<String, String> getSubscriberCreditInputData(HashMap<String, String> requestHashMap,
			C30AccountObject account) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		C30BalanceList balanceList=null;		
		String	reason=null;	
		String	balExpirydate=null;		
		try{
			
			
			//Get the Balance Details
			if (account.getBalanceList() != null) {

				//Get Balance Object Data
				balanceList=account.getBalanceList().getValue();
				List<C30BalanceObject> balanceobjList = balanceList.getBalance();
				log.info("Get the Wallet Refill/Balance Refill Data ");				
				for(C30BalanceObject balance : balanceobjList)
				{
					log.info("balance = " +balance.getBalanceAction().toString());
					//Get the INPUT Data for API call
					if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLETREFILL)) ||
							(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLET_REFILL)) ||
							(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCE_REFILL)) ||					
		    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCEREFILL))) {	
						//Get Balance Type Label
						if (balance.getBalanceTypeLabel()!=null) 
							{requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_TYPE_LABEL, balance.getBalanceTypeLabel());}
						//Get Balance Value
						if (balance.getBalanceUnits()!=null) {requestHashMap.put(C30WPSQLConstants.PREPAID_VALUE, 
								C30SqlApiUtils.convertJaxbElementToString(balance.getBalanceUnits().getValue()));}
						//Get Expiration Date
						if (balance.getExpirationDate()!= null) {balExpirydate = balance.getExpirationDate();}
						log.info("Order - Expiration Date: " +balance.getExpirationDate());
						//Get the Balance reason
						if (balance.getBalanceReason()!= null) {
							reason =  balance.getBalanceReason();
						} else {
							reason = balance.getBalanceAction().toString().toUpperCase()+'_'+C30SqlApiUtils.
									convertJaxbElementToString(balance.getBalanceUnits().getValue()).toString();
						}
		    		}
					//Get ExpiryDate
					if(balExpirydate != null) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, balExpirydate);
					} else if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCE_REFILL)) ||					
		    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCEREFILL)))  {
						String NoofDays = "365";
			            Calendar cal = Calendar.getInstance();
			            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 23, 59, 59);
			            cal.add(Calendar.DATE, Integer.valueOf(NoofDays));
			            java.util.Date CalExpirationDate = cal.getTime();
			            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			            balExpirydate = df.format(CalExpirationDate).toString();
						log.info("system compExpirydate : " +balExpirydate);	            
			            requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, balExpirydate);				
					}					
				}
			}
			requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, reason);
			log.info("getSubscriberCreditInputData - requestHashMap = " +requestHashMap.toString());
			
			return requestHashMap;
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Prepaid System Request-getSubscriberCreditInputData : "+e);
			throw new C30SDKToolkitException("EXCP: ORDER-getSubscriberCreditInputData: "+e, e);
		}		
		
	}	
	
	
	/**This method is responsible for getting the input data required to update Account in the Prepaid system
	 * 
	 * @param account
	 * @param requestHashMap
	 * @throws C30SDKToolkitException 
	 * 
	 */
	public static HashMap<String, String> getSubscriberUpdateInputData(C30AccountObject account,  HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		C30ExtendedDataList actExtDatList=null;		
		try{
			//Get the NumberSubtype and PreferredLang
			if (account.getExtendedDataList() != null) {
				actExtDatList=account.getExtendedDataList().getValue();
				log.info(" Total Extended Data found : " + actExtDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = actExtDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList)
				{
					//Get NumberSubType
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_NUMBER_SUB_TYPE, extData.getExtendedDataValue().toString());							
					}
					//Get Preferred Language
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PREFERRED_LANG)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_PREFERRED_LANG, extData.getExtendedDataValue().toString());							
					}
					//Get CPNI Pin
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_CPNI_PIN)) {
						requestHashMap.put(Constants.PREPAID_PINCODE, extData.getExtendedDataValue().toString());								
					}
					//Get AUTOPay Flag
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_AUTOPAY_FLAG)) {
						if (extData.getExtendedDataValue().toString().equalsIgnoreCase("YES")) {
							requestHashMap.put(Constants.PREPAID_LAST_NAME, C30WPSQLConstants.PREPAID_AUTOPAY_VALUE);								
						}
						if (extData.getExtendedDataValue().toString().equalsIgnoreCase("NO")) {
							requestHashMap.put(Constants.PREPAID_LAST_NAME, C30WPSQLConstants.PREPAID_OCS_NULL_VALUE);								
						}
					}
					//Get SubscriberLineId
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_OCS_SUBSCRIBER_LINE_ID)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID, extData.getExtendedDataValue().toString());								
					}		
					//Get Notification Preference
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NOTIFICATION_PREFERENCE)) {
						if ((extData.getExtendedDataValue().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NOTIFICATION_PREFERENCE_SMS)) ||
								(extData.getExtendedDataValue().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_NOTIFICATION_PREFERENCE_BOTH))) {
							requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_TRUE_VALUE);							
							requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_TRUE_VALUE);						
						} else {
							requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_FALSE_VALUE);							
							requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_EXPIRY_WARNING_SMS, C30WPSQLConstants.PREPAID_OCS_FALSE_VALUE);						
						}
					}					
				}
			}
			
			return requestHashMap;
			
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR-Prepaid Order getSubscriberUpdateInputData: "+e);
			throw new C30SDKToolkitException(
					"EXCP: PREPAID ORDER-getSubscriberUpdateInputData: "+e, e);
		}		
		
	}		
	
	/**This method is responsible for getting the input data required to update balance in the Prepaid system
	 * 
	 * @param account
	 * @param requestName
	 * @throws C30SDKToolkitException 
	 * 
	 */
	public static HashMap<String, String> getSubscriberRechargeInputData(C30AccountObject account,  
			HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		C30BalanceList balanceList=null;
		String reason=null;
		try{
			//Get the Balance Details
			if (account.getBalanceList() != null) {
				//Get Balance Object Data
				balanceList=account.getBalanceList().getValue();
				List<C30BalanceObject> balanceobjList = balanceList.getBalance();
				log.info("Get the Wallet/Balance Refill Data ");				
				for(C30BalanceObject balance : balanceobjList)
				{
					//WALLET REFILL and BALANCE REFILL
					if((balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLETREFILL)) ||
							(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_WALLET_REFILL)) ||
							(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCE_REFILL)) ||					
		    				(balance.getBalanceAction().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_BALANCEREFILL))) {					
						//Get Balance Type Label
						if (balance.getBalanceTypeLabel()!=null) {
							requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_TYPE_LABEL, balance.getBalanceTypeLabel());}
						//Get Balance Value
						if (balance.getBalanceUnits()!=null) {
							requestHashMap.put(C30WPSQLConstants.PREPAID_VALUE, C30SqlApiUtils.convertJaxbElementToString(balance.getBalanceUnits().getValue()));}
						//Get Expiration Date
						if (balance.getExpirationDate()!= null) {
							requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, balance.getExpirationDate());}
						//Get the Balance reason
						if (balance.getBalanceReason()!= null) {
							reason =  balance.getBalanceReason();}
						else {
							reason = balance.getBalanceAction().toString().toUpperCase()+'_'+C30SqlApiUtils.
									convertJaxbElementToString(balance.getBalanceUnits().getValue()).toString();}
						//Get Reason
						requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, reason);
		    		}
				}
			}
			log.info("requestHashMap =  "+requestHashMap.toString());
			return requestHashMap;
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - PREPAID ORDER - getSubscriberRechargeInputData: "+e);
			throw new C30SDKToolkitException(
					"EXCP: PREPAID ORDER-getSubscriberRechargeInputData: "+e, e);
		}		
	}	
	
	/**This method is responsible for getting the input data required for EventCharging.DirectDebit API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	public static HashMap<String, String> getEventChargingInputData(HashMap<String, String> requestHashMap, C30ServiceObject service,
			C30ComponentObject comp) throws C30SDKToolkitException {

		// TODO Auto-generated method stub
		C30ExtendedDataList extDatList=null;		
		try{		
			//If Comp Object has Extended Data
			if (comp.getExtendedDataList() != null) {
				extDatList=comp.getExtendedDataList().getValue();
				log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = extDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList) {
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_FORCE_ATTRIB_CHARGE_EVENT)) {
						//Get the Charge Event
						requestHashMap.put(C30WPSQLConstants.PREPAID_CHARGE_EVENT_LABEL, extData.getExtendedDataValue().toString());	
					}							
				}	
			}		
			log.info("requestHashMap =  "+requestHashMap.toString());
			return requestHashMap;
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR-Prepaid Request-getEventChargingInputData " +e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Prepaid Request-getEventChargingInputData: "+e, e);
		}		

	}
	
	/**This method is responsible for getting the input data required for Subscriber.CreditSet API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	public static HashMap<String, String> getSubscriberCreditSetInputData(HashMap<String, String> requestHashMap, C30ServiceObject service, 
			C30ComponentObject comp) throws C30SDKToolkitException {

		// TODO Auto-generated method stub
		C30ExtendedDataList extDatList=null;		
		String	compName=null;		
		try{

			compName = comp.getComponentName();		
			//If Component Object has Extended Data
			if (comp.getExtendedDataList() != null) {
				extDatList=comp.getExtendedDataList().getValue();
				log.info(" Total Component Extended Data found : " + extDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = extDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList)
				{
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_EXPIRY_DATE)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, extData.getExtendedDataValue().toString());
					}
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOLUME)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_VALUE, extData.getExtendedDataValue().toString());
					}
				}							
			}	

			requestHashMap.put(C30WPSQLConstants.PREPAID_CHARGE_EVENT_BALANCE_TYPE_LABEL, compName);	  
			log.info("requestHashMap =  "+requestHashMap.toString());
			return requestHashMap;
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR-Prepaid Request-getSubscriberCreditSetInputData :"+e);
			throw new C30SDKToolkitException(
					"EXCP:ORDER-Prepaid Request-getSubscriberCreditSetInputData: "+e, e);
		}		

	}		
	

	/**This method is responsible for getting the input data required for Subscriber.CreditSet API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	public static HashMap<String, String> getSubscriberCreditInputData(HashMap<String, String> requestHashMap, C30ServiceObject service,
			C30ComponentObject comp) throws C30SDKToolkitException {

		// TODO Auto-generated method stub
		C30ExtendedDataList extDatList=null;		
		String	compName=null;	
		String	compExpirydate=null;

		try{

			compName = comp.getComponentName();		
			//If Component Object has Extended Data
			if (comp.getExtendedDataList() != null) {
				extDatList=comp.getExtendedDataList().getValue();
				log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
				List<C30ExtendedData> extdtList = extDatList.getExtendedData();
				for(C30ExtendedData extData : extdtList)
				{	
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_COMPONENET_EXPIRY_DATE)) {
						compExpirydate = extData.getExtendedDataValue().toString();
						log.info("compExpirydate : " +compExpirydate);						
					}
					if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_BALANCE_VOLUME)) {
						requestHashMap.put(C30WPSQLConstants.PREPAID_VALUE, extData.getExtendedDataValue().toString());
						log.info("Value : " +extData.getExtendedDataValue().toString());
					}
				}							
			}	

			if(compExpirydate != null) {
				requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, compExpirydate);
			} else {
				String NoofDays = "365";
				Calendar cal = Calendar.getInstance();
				cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 23, 59, 59);
				cal.add(Calendar.DATE, Integer.valueOf(NoofDays));
				java.util.Date CalExpirationDate = cal.getTime();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				compExpirydate = df.format(CalExpirationDate).toString();
				log.info("system compExpirydate : " +compExpirydate);	            
				requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, compExpirydate);				
			}
			requestHashMap.put(C30WPSQLConstants.PREPAID_BALANCE_TYPE_LABEL, compName);	   	
		
			log.info("requestHashMap =  "+requestHashMap.toString());
			return requestHashMap;			
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR-Prepaid Request-getSubscriberCreditInputData: "+e);
			throw new C30SDKToolkitException(
					"EXCP:ORDER-Prepaid Request-getSubscriberCreditInputData: "+e, e);
		}		
	}
	
	
	/**This method is responsible for getting a Subscriber Line Id from OCS
	 * 
	 * @param ProvCLISubLineInquiry
	 * @param Service
	 * @param orderData
	 * @param acctSegId
	 * @param subOrderId
	 * @param transactionId
	 * @param internalAuditId
	 * @throws C30SDKToolkitException
	 * 
	 *  
	 */
	public static String getSubscriberLinefromOCS(HashMap<String, String> ProvCLISubLineInquiry, C30ServiceObject Service, Map<String, Boolean> orderData,
			String acctSegId, String subOrderId, String transactionId, String internalAuditId) throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		
		try{
			String subscriberLineId = null;
			ProvCLISubLineInquiry.put(Constants.PREPAID_CLI, Service.getClientServicePrimaryIdentifier());
			ProvCLISubLineInquiry.put(Constants.ACCT_SEG_ID,acctSegId);
			ProvCLISubLineInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			ProvCLISubLineInquiry.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			ProvCLISubLineInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);			
			ProvCLISubLineInquiry.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_LINE_GET_BYCLI);			

			log.info("Send SubscriberLine.GetByCLI Inquiry for Telephonenumber : "+ProvCLISubLineInquiry.get(Constants.PREPAID_CLI));
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.ocsInquiryRequest(ProvCLISubLineInquiry);
			log.info("Response XML : "+response.get("ResponseXML"));
			
			//Get subscriberLineId
			subscriberLineId = C30ProvConnectorUtils.getOCSMemberValuefromResponse(response.get("ResponseXML").toString(), 
					C30WPSQLConstants.PREPAID_SUBSCRIBER_LINE_ID);
			log.info("subscriberLineId = "+subscriberLineId);

			return subscriberLineId;
		}// End - Try
		catch (C30ProvConnectorException e)
		{
			log.error("ERROR - Account Segment: " + ProvCLISubLineInquiry.get(Constants.ACCT_SEG_ID).toString() + " SubscriberLine.GetByCLI: "+e);
				throw new C30SDKToolkitException("getSubscriberLinefromOCS: "+e, e);
		}
		catch (Exception e)
		{
			log.error("ERROR - Account Segment: " + ProvCLISubLineInquiry.get(Constants.ACCT_SEG_ID).toString() + " SubscriberLine.GetByCLI: "+e);
				throw new C30SDKToolkitException("getSubscriberLinefromOCS: "+e, e);
		}		
	}
	
	/**Method to parse and get the MULE POS SessionId
	 * 
	 * @param response
	 * @throws C30SDKToolkitException
	 * 
	 *  
	 */	
	public static String parseResponseForSessionId(String response)
	{
		String sessionId=null;
		System.out.println("Parsing the response content for session ID.");
		try
		{
			Document doc = getDocument(response);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("AuthenticationResponse");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE)
				{

					Element eElement = (Element) nNode;
					sessionId = getTagValue("SessionId", eElement);
					System.out.println("SessionId : " + sessionId);
				}
			}
			return sessionId;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	

	/**Method to get the Tag Value
	 * 
	 * @param TagName
	 * @param Element
	 *  
	 *  
	 */		
	private static String getTagValue(String sTag, Element eElement)
	{
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
				.getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}
	
	/**
	 * Method for getting a DOM Document from a xmlString
	 * 
	 * @param xmlString
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	
	public static Document getDocument(String xmlString)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlString));
        return docBuilder.parse(is);
    }	
	
		//Added by vijaya as part of PW-43
	
		/**This method is responsible for getting a Subscriber Id from OCS
		 * 
		 * @param ProvCLISubscriberInquiry
		 * @param Service
		 * @param orderData
		 * @param acctSegId
		 * @param subOrderId
		 * @param transactionId
		 * @param internalAuditId
		 * @throws C30SDKToolkitException
		 * 
		 *  
		 */
		public static String getSubscriberfromOCS(HashMap<String, String> ProvCLISubscriberInquiry, C30ServiceObject Service, Map<String, Boolean> orderData,
				String acctSegId, String subOrderId, String transactionId, String internalAuditId) throws C30SDKToolkitException {
			// TODO Auto-generated method stub
			
			try{
				String subscriberId = null;
				ProvCLISubscriberInquiry.put(Constants.PREPAID_CLI, Service.getClientServicePrimaryIdentifier());
				ProvCLISubscriberInquiry.put(Constants.ACCT_SEG_ID,acctSegId);
				ProvCLISubscriberInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
				ProvCLISubscriberInquiry.put(Constants.C30_TRANSACTION_ID,internalAuditId);
				ProvCLISubscriberInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER);			
				ProvCLISubscriberInquiry.put(Constants.ATTR_OCS_METHOD_NAME, Constants.OCS_SUBSCRIBER_GET_BYCLI);			

				log.info("Sending Subscriber.GetByCLI Inquiry for Telephonenumber : "+ProvCLISubscriberInquiry.get(Constants.PREPAID_CLI));
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				HashMap<?, ?> response = request.ocsInquiryRequest(ProvCLISubscriberInquiry);
				log.info("Response XML : "+response.get("ResponseXML"));
				
				//Get subscriberLineId
				subscriberId = C30ProvConnectorUtils.getOCSMemberValuefromResponse(response.get("ResponseXML").toString(), 
						C30WPSQLConstants.PREPAID_SUBSCRIBER_ID);
				log.info("subscriber Id = "+subscriberId);

				return subscriberId;
			}// End - Try
			catch (C30ProvConnectorException e)
			{
				log.error("ERROR - Account Segment: " + ProvCLISubscriberInquiry.get(Constants.ACCT_SEG_ID).toString() + " Subscriber.GetByCLI: "+e);
					throw new C30SDKToolkitException("getSubscriberfromOCS: "+e, e);
			}
			catch (Exception e)
			{
				log.error("ERROR - Account Segment: " + ProvCLISubscriberInquiry.get(Constants.ACCT_SEG_ID).toString() + " Subscriber.GetByCLI: "+e);
					throw new C30SDKToolkitException("getSubscriberfromOCS: "+e, e);
			}		
		}
	
}
