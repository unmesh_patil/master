package com.cycle30.workpoint.script.awnprepaid;

import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

//SDK
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.schema.jaxb.C30ExtendedData;
import com.cycle30.sdk.schema.jaxb.C30ExtendedDataList;
import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
//Workpoint
import com.cycle30.workpoint.script.C30OrderScript;
import com.cycle30.workpoint.script.awnprepaid.utils.C30AWNPrepaidUtils;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
//Provision
import com.cycle30.prov.data.Constants;
//Prepaid APIs
import com.cycle30.workpoint.prepaidapi.EventChargingDirectDebit;
import com.cycle30.workpoint.prepaidapi.SubscriberUpdate;
import com.cycle30.workpoint.prepaidapi.SubscriberCreditSet;
import com.cycle30.workpoint.prepaidapi.UpdateBalances;
import com.cycle30.workpoint.prov.api.SendFeature;


/** This script works on the Service Object in the given XML
 * 
 * 
 * @author Umesh
 * 
 * 
 */
public class C30PrepaidOrderPlanScript extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30PrepaidOrderPlanScript.class);

	public C30PrepaidOrderPlanScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30PrepaidOrderPlanScript(String orderId, String subOrderId, String acctSegId, String transactionId, 
			String internalAuditId, String requestName) throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId, requestName);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {
		// TODO Auto-generated method stub
		try
		{		
			String SubscriberId = null;
//			String ExtdatKey = Constants.OCS_SUBSCRIBER_ID;
			
			HashMap<String, String> requestHashMap = new HashMap<String, String>();			
			requestHashMap.put(Constants.EXTERNAL_TRANSACTION_ID,transactionId);				
			requestHashMap.put(Constants.C30_TRANSACTION_ID,internalAuditId);
			requestHashMap.put(Constants.ACCT_SEG_ID, acctSegId);		
			
			// If the Account Object is populated.
				log.info("Check for the Account Number");
	            SubscriberId = C30AWNPrepaidUtils.getSubscriberNumber(order);
	            
	            //Plan Order
			    //Check if the service object is empty.
			    if (order.getServiceObjectList() != null) {
				    
			    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
			    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
			    	for (C30ServiceObject service : serviceobjList)  {
					    
					    String clientId = service.getClientId();
					    String actionType = service.getClientActionType().toString();
					    String serviceId = service.getClientServicePrimaryIdentifier();
				    
			            log.info("Sub order id: '" + subOrderId + "'  Service Client id: '" + clientId + "'  Action type: '" + actionType + "'");
						if (service.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {

							//Get the Subscriber Profile Label if the Plan Object is populated.
							if (service.getPlanObject() != null)  {
								C30ExtendedDataList extDatList=null;
								C30PlanObject plan = service.getPlanObject().getValue();
								requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_ID, SubscriberId);									
								
								if (plan.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE)) {
									requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_PROFILE);
									
						            log.info("requestName : "+requestName);
									if (plan.getExtendedDataList() != null) {
										extDatList=plan.getExtendedDataList().getValue();
										log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
										List<C30ExtendedData> extdtList = extDatList.getExtendedData();
										for(C30ExtendedData extData : extdtList) {
											if(extData.getExtendedDataName().toString().equals(C30WPSQLConstants.PREPAID_PLAN_RATE)) {
												log.info("Plan Rate = " +Integer.valueOf(extData.getExtendedDataValue().toString()));
												if(Integer.valueOf(extData.getExtendedDataValue().toString()) > 0) {	
													requestHashMap.put(Constants.ATTR_PREAPID_ACTION, Constants.PREPAID_PLAN_RECHARGE_RESERVE);		
													planRechargeinPrepaidSystem(requestHashMap, plan);
												} else if(Integer.valueOf(extData.getExtendedDataValue().toString()) == 0){
													requestHashMap.put(Constants.ATTR_PREAPID_ACTION, Constants.PREPAID_EXTEND_EXPIRYDATE);													
													extendPlanExpiryDateinPrepaidSystem(requestHashMap, plan);
												}
											}							
										}	
									}
								} else if (plan.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP)) {
									requestHashMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_PROFILE);
									requestHashMap.put(Constants.ATTR_PREAPID_ACTION, Constants.PREPAID_PLAN_CHANGE);									
						            swapPlaninPrepaidSystem(requestHashMap, plan);
						            
						           /** The Below block is commented as to avoid CHANGE VM Call to all the PLAN SWAP Actions, the decision would be taken 
						            * about when to uncomment or remove this code would be based on results of SPS on VVM activation testing on PROD as on 4/11/2014.
						            */
						            
						          /*  if(requestHashMap.get(Constants.ACCT_SEG_ID).equals(C30WPSQLConstants.C30_SEGMENT_ACS_PREPAID)){
						            	log.info("Preparing to send the Voice Mail feature Change Request ");
						            	String CatalogId = C30WPSQLConstants.C30_PROV_ATTRIB_ACS_VISUAL_VOICE_MAIL_CHANGE;
						            	log.info("Catalog Id for Voice Mail Change as per configuration "  + CatalogId );
						            	if(plan.getPlanName().contains(C30WPSQLConstants.C30_ACS_PREPAID_VOICE_VVM)){
						            		requestHashMap.put(C30WPSQLConstants.C30_ACS_PREPAID_VOICE_VVM,C30WPSQLConstants.C30_ACS_PREPAID_VOICE_VVM_TRUE);
						            		log.info("Plan Need Visual Voice Mail Box ");
						            	} else{
						            		log.info("Plan Needs Just Voice Mail Box but not the Visual Voice Mail Box");
						            	}
						            	new SendFeature(CatalogId, serviceId, null, requestHashMap,  C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE);
						           } */
								}
							}
						}
			    	}
			    }
				

		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Prepaid Account Order :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - Prepaid Account Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP:addAccountInPrepaidSystem Request-RequestName: "+requestName +" Message: "+e, e );			
		}
	}

	/**This method is responsible for the OCS API calls to SWAP Plan 
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void swapPlaninPrepaidSystem(HashMap<String, String> requestHashMap, C30PlanObject plan) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		String	planName=null;		
		try{

            //Subscriber.Update
            if (requestName.equals(Constants.OCS_SUBSCRIBER_UPDATE)) {
				//Get Plan Name
            	planName=plan.getPlanName();						
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_PROFILE_LABEL, planName);
				if ( order.getServiceObjectList() != null) {
					//Call API Subscriber.Update
					new SubscriberUpdate(requestHashMap);
				}
			}
			//Subscriber.CreditSet
			if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDITSET)) {
	            log.info("Subscriber.CreditSet");								
//				requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, C30WPSQLConstants.PREPAID_ADD_SERVICE_REASON);
				//Get the INPUT Data for API call						
				getSubscriberCreditSetInputData(requestHashMap);
				//Call API Subscriber.CreditSet
				new SubscriberCreditSet(requestHashMap);								
			}
			//Subscriber.GetBalances and Subscriber.RemoveBalance
			if (requestName.equals(Constants.OCS_SUBSCRIBER_UPDATEBALANCE)) {
	            log.info("Update Balance");								
            	planName=plan.getPlanName();						
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_PROFILE_LABEL, planName);
	            //Get the INPUT Data for API call
				new UpdateBalances(requestHashMap);						
			}
			//EventCharging.DirectDebit
			if (requestName.equals(Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT)) {
	            log.info("EventCharging.DirectDebit");
				//Get the INPUT Data for API call						
				getEventChargingDirectDebitInputData(requestHashMap);
				//Call API EventCharging.DirectDebit
				new EventChargingDirectDebit(requestHashMap);								
			}			
			
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-swapPlaninPrepaidSystem: "+e, e);
		}		
		
	}	
	

	/**This method is responsible for the OCS API calls to Add/Subtract Plan Expiration Date 
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void extendPlanExpiryDateinPrepaidSystem(HashMap<String, String> requestHashMap, C30PlanObject plan) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		try{

			//Subscriber.CreditSet
			if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDITSET)) {
	            log.info("Subscriber.CreditSet");								
//				requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, C30WPSQLConstants.PREPAID_ADD_SERVICE_REASON);
				//Get the INPUT Data for API call						
				getSubscriberCreditSetInputData(requestHashMap);
				//Call API Subscriber.CreditSet
				new SubscriberCreditSet(requestHashMap);								
			}
			
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-swapPlaninPrepaidSystem: "+e, e);
		}		
		
	}	
	
	/**This method is responsible for the OCS API calls to Recharge Plan 
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void planRechargeinPrepaidSystem(HashMap<String, String> requestHashMap, C30PlanObject plan) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
	
		try{

            //EventCharging.DirectDebit
			if (requestName.equals(Constants.OCS_EVENT_CHARGING_DIRECT_DEBIT)) {
	            log.info("EventCharging.DirectDebit");
				//Get the INPUT Data for API call						
				getEventChargingDirectDebitInputData(requestHashMap);
				//Call API EventCharging.DirectDebit
				new EventChargingDirectDebit(requestHashMap);								
			}
			//Subscriber.CreditSet
			if (requestName.equals(Constants.OCS_SUBSCRIBER_CREDITSET)) {
	            log.info("Subscriber.CreditSet");								
				//Get the INPUT Data for API call						
				getSubscriberCreditSetInputData(requestHashMap);
				//Call API Subscriber.CreditSet
				new SubscriberCreditSet(requestHashMap);								
			}
			
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-planRechargeinPrepaidSystem: "+e, e);
		}		
		
	}	
	
	/**This method is responsible for getting the input data required for Subscriber.CreditSet API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getSubscriberCreditSetInputData(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		String	planName=null;	
		String clientId=null;		
		String reason=null;
		C30ExtendedDataList extDatList=null;		
		try{

			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
			//Get the Subscriber Profile Label	    	
	    	for (C30ServiceObject service : serviceobjList)  {  

	    		reason=service.getClientActionTypeReason();
	            //If the Plan Object is populated.
				if (service.getPlanObject() != null)  {
					C30PlanObject plan = service.getPlanObject().getValue();
					//Get Plan Name
					planName=plan.getPlanName();
					//Check Extended Data
					if (plan.getExtendedDataList() != null) {
						extDatList=plan.getExtendedDataList().getValue();
						log.info(" Total Extended Data found : " + extDatList.getExtendedData().size());
						List<C30ExtendedData> extdtList = extDatList.getExtendedData();
						for(C30ExtendedData extData : extdtList) {
							if(extData.getExtendedDataName().toString().equalsIgnoreCase(C30WPSQLConstants.PREPAID_PLAN_EXPIRATION_DATE)) {
								requestHashMap.put(C30WPSQLConstants.PREPAID_EXPIRATIONDATE, extData.getExtendedDataValue().toString());
							}
						}	
					}
				}
	    	}		
	    	if (reason!=null) {
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_REASON, reason);
	    	}
			requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_PROFILE_LABEL, planName);	   	
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getSubscriberCreditSetInputData: "+e, e);
		}		
	}	

	/**This method is responsible for getting the input data required for EventCharging.DirectDebit API Call
	 * 
	 * @param requestHashMap
	 * 
	 * @throws C30SDKToolkitException 
	 * 
	 */
	private void getEventChargingDirectDebitInputData(HashMap<String, String> requestHashMap) throws C30SDKToolkitException {
	
		// TODO Auto-generated method stub
		String	planName=null;		
		try{

			//Get Service Object Data
	    	List<C30ServiceObject> serviceobjList = (List<C30ServiceObject>) order.getServiceObjectList().getServiceObject();
	    	log.info("Working on the Service Object.  Size of list is " + serviceobjList.size());
	    	
			//Get the Subscriber Profile Label	    	
	    	for (C30ServiceObject service : serviceobjList)  {
			    String subscriberCLI = null;
	    		if (service.getClientServicePrimaryIdentifier() != null) 
	    			{ subscriberCLI = C30SqlApiUtils.convertJaxbElementToString(service.getClientServicePrimaryIdentifier());}
	    		//Add CLI to request
	    		requestHashMap.put(C30WPSQLConstants.PREPAID_SUBSCRIBER_CLI, subscriberCLI);
	    		
				//Get the Charging Event Label	-- This need to be changed once OCS setup the Charging Event				
				if (service.getPlanObject() != null)  {
					C30PlanObject plan = service.getPlanObject().getValue();
					//Get Plan Name
					planName=plan.getPlanName();						
		    		requestHashMap.put(C30WPSQLConstants.PREPAID_CHARGE_EVENT_LABEL, planName);
				}
			}	
		}// End - Try
		catch (Exception e)
		{
			log.error("ERROR - Account Segment : " +acctSegId + " Prepaid System Request : "+e);
			throw new C30SDKToolkitException(
					"EXCP: ORDER-getEventChargingDirectDebitInputData Request "+e, e);
		}		
	}		
	
}
