package com.cycle30.workpoint.script;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import org.apache.log4j.Logger;

import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.schema.jaxb.OrderRequest.ServiceObjectList;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.workpoint.sqlapi.C30WPAddComponentApi;
import com.cycle30.workpoint.sqlapi.C30WPDiscComponentApi;
import com.cycle30.workpoint.sqlapi.C30WPSwapPlanApi;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;


/** This script works on the Component object in the given xml
 * 
 * @author Umesh
 * - Initial Version
 * - Added the TransactionId Features
 * - Modified to align with the new XSD created for wholesale project 
 *
 */
public class C30OrderComponentScript  extends C30OrderScript{

	private static Logger log = Logger.getLogger(C30OrderComponentScript.class);
	
	private boolean outParam = false;	
	
	public C30OrderComponentScript(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30OrderComponentScript(String orderId, String subOrderId, String acctSegId, String transactionId, String internalAuditId) throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);		
	}

	/** This is used to process the input XML and extracts the block related to
	 * the given subOrderId and performs the given action.
	 * @param xmlPayload
	 * @throws C30SDKToolkitException 
	 */
	public void process() throws C30SDKToolkitException {

		try  {		
 
		       	//If service object exists, get the Client id
	    		String	serviceId = null;
				String	orderDesiredDate = null;
				String 	compInstId = null;
				oracle.sql.ARRAY servIdListArray=null;	
				
				orderDesiredDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());
				log.info("orderDesiredDate: '" + orderDesiredDate);				
				
		    
			    if (order.getServiceObjectList() != null) {			    
	                List<C30ServiceObject> serviceobjList =  order.getServiceObjectList().getServiceObject();
	                log.info("Service Object List Size: " + serviceobjList.size());
	                
	                for (C30ServiceObject service : serviceobjList) {
	                    
	                	//Get Client Id from service
	                    serviceId = service.getClientId();
	                    log.info("Checking Component/Feature for serviceId : " +serviceId);

	                    if (service.getComponentObjectList() != null) {
						    List<C30ComponentObject> clList = (List<C30ComponentObject>) service.getComponentObjectList().getComponentObject();
					    	log.info("Working on the Component Object.  Size of list is " + clList.size());					    
						    for (C30ComponentObject comp : clList) {
						    	compInstId = comp.getComponentInstanceId();
						    	servIdListArray = prepareCompServiceIdList(comp, serviceId, conn);	 						    	
							    if(comp.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
							    	//Add Component
							    	addCompInBillingSystem(comp, acctSegId, serviceId, compInstId, transactionId, servIdListArray);	}
							    else if (comp.getClientActionType().toString().equalsIgnoreCase(C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC)) {
							    	//Disconnect Component
							    	discCompInBillingSystem(comp, acctSegId, compInstId, transactionId); }
						    } // End - for (C30ComponentObject comp : clList)
						}// End - if (service.getComponentObjectList() != null)
	                }
	                
			    }//End - if (order.getServiceObjectList() != null)
			    else {
			    	log.info("ERROR : Service Object Empty/NOT found");
			    	return;
			    }
		}// End - try
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - Component Order :"+e);
			e.printStackTrace();
			throw e;
		}
		catch(Exception e) {
			log.error("EXCP - Component Order :"+e);	                
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Component Order Request Process", e );			
		}
	}//End - void process()

	/** Return the ServiceId as ByteArray
	 * 
	 * @param ComponentObject
	 * @param serviceId
	 * @param genericGlobalConnection
	 * @return
	 * @throws SQLException
	 */	
	public static oracle.sql.ARRAY prepareCompServiceIdList(C30ComponentObject comp, String serviceId, Connection conn) throws SQLException
	{
		StructDescriptor bookStructDesc =
		StructDescriptor.createDescriptor("C30ARBOR.C30STRING", conn);
		ArrayDescriptor arrayDescriptor = ArrayDescriptor.createDescriptor("C30ARBOR.C30STRINGARRAY", conn);
		log.info("Getting Service Id List");
		int i = 0;
		int size = 1;
		oracle.sql.ARRAY servIdListArray = null;
		Object[] bArray =null;
		//Service Ids having Component Name==NULL are considered as Shared and Service Ids having Component Name!=NULL are considered as Standalone
		if (comp.getComponentName() == null) {
				bArray = new Object[size];
				STRUCT m1 = new STRUCT(bookStructDesc, conn, new Object[]{serviceId});
				bArray[i]=m1;
			}
		log.info("servIdListArray before : " +servIdListArray);
		servIdListArray = new oracle.sql.ARRAY(arrayDescriptor, conn, bArray);
		log.info("servIdListArray After : " +servIdListArray);
		return servIdListArray;
	}
	
	
	/**This method is responsible for adding a Component in the Billing system
	 * 
	 * @param comp
	 * @throws C30SDKToolkitException 
	 */
	private void addCompInBillingSystem(C30ComponentObject comp, String acctSegId, String serviceId, String compInstId, String transactionId,
			oracle.sql.ARRAY servIdListArray) throws C30SDKToolkitException {

		String     actionDate=null;
		String     actionWho=null;
		String     componentId=null;
		
		try {
			if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}				
			if (comp.getComponentName() != null)  		{ componentId = C30SqlApiUtils.convertJaxbElementToString(comp.getComponentName());}

			log.info("START - Invoking ADD Component SQL-API for Component Id : "+ componentId + " Client Id : " + serviceId + " CompInstanceId : " + compInstId + " orderDesiredDate : " + actionDate);
		
			//Add Component API
			C30WPAddComponentApi addComp  = new C30WPAddComponentApi(conn, transactionId, acctSegId.toString(), componentId, compInstId, actionDate, 		
					actionWho, serviceId, servIdListArray);
			
			log.info("END - Invoking ADD Component SQL-API for Component Id : "+ componentId + " Client Id : " + serviceId + " CompInstanceId : " + compInstId + " orderDesiredDate : " + actionDate);

			//Check the Output			
			if ((addComp.getDataSource() != null) && (addComp.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = addComp.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					log.info("Add Comp OUT PARAM1 : " + outparam.keySet() + " Add Comp OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						this.outParam = true;
						log.info("Add Comp OUT PARAM=true");
					}
				}
			}// END - if ((addComp.getDataSource() != null) && (addComp.getDataSource().getResultMap() != null))
			else { 
				log.info("ERROR - DataSource/Result Map IS NULL ");
			}//End - Else if ((addComp.getDataSource() != null) && (addComp.getDataSource().getResultMap() != null))
			
		}//End - Try
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - addCompInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - addCompInBillingSystem(C30ComponentObject comp, String acctSegId)

	/**This method is responsible for disconnecting a Component in the Billing system
	 * 
	 * @param comp
	 * @throws C30SDKToolkitException 
	 */
	private void discCompInBillingSystem(C30ComponentObject comp, String acctSegId, String compInstId, String transactionId) throws C30SDKToolkitException {

		String     actionDate=null;
		String     actionWho=null;
		String     componentId=null;
		
		try {
			if (order.getOrderDesiredDate() != null)	{ actionDate = C30SqlApiUtils.convertJaxbElementToString(order.getOrderDesiredDate());}
			if (order.getActionWho() != null)			{ actionWho = C30SqlApiUtils.convertJaxbElementToString(order.getActionWho());}	
			if (comp.getComponentName() != null)  		{ componentId = C30SqlApiUtils.convertJaxbElementToString(comp.getComponentName());}		
			
			log.info("START - Invoking DISCONNECT Component SQL-API for Component : "+componentId+ " and Instance Id : "+compInstId +
					" orderDesiredDate : " + actionDate);
			//Disconnect Component API
			C30WPDiscComponentApi discComp  = new C30WPDiscComponentApi(conn, transactionId, acctSegId.toString(), componentId, compInstId, actionDate, actionWho);
			log.info("END - Invoking DISCONNECT Component SQL-API for Component : "+componentId+ " and Instance Id : "+compInstId +
					" orderDesiredDate : " + actionDate);

			//Check the Output			
			if ((discComp.getDataSource() != null) && (discComp.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = discComp.getDataSource().getResultMap().values()
						.iterator();
					while (itr.hasNext())
					{
						Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
						log.info("Disc Comp OUT PARAM1 : " + outparam.keySet() + " Disc Comp OUT PARAM2 :"
								+ outparam.values());
						if (outparam.values()!=null)
						{
							this.outParam = true;
							log.info("Disc Comp OUT PARAM=true");
						}
					}
			}// END - if ((discComp.getDataSource() != null) && (discComp.getDataSource().getResultMap() != null))
			else { 
				log.info("ERROR - DataSource/Result Map IS NULL ");
			}//End - Else if ((discComp.getDataSource() != null) && (discComp.getDataSource().getResultMap() != null))
				
		}
		catch(C30SDKToolkitException e) {
		    log.error("ERROR - discCompInBillingSystem :"+e);
			e.printStackTrace();
			throw e;
		}

	}//End - discCompInBillingSystem(C30ComponentObject comp, String acctSegId)

	public boolean isOutParam()
	{
		return outParam;
	}

	public void setOutParam(boolean outParam)
	{
		this.outParam = outParam;
	}

} // End - public class C30OrderComponentScript

