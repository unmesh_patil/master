package com.cycle30.workpoint.script;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30SDKConfigurationManager;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/** this is used to write the error / Major debug information   to the transaction 
 * Log in case of Error/Failure
 * @author rnelluri
 *
 */
public class C30OrderTransactionLog {

	
	//Logs all the information
	private static Logger log = Logger.getLogger(C30OrderTransactionLog.class);
	public static C30SDKConfigurationManager exceptionResourceBundle = C30SDKConfigurationManager.getInstance();


	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the subOrderId
	 */
	public String getSubOrderId() {
		return subOrderId;
	}

	/**
	 * @param subOrderId the subOrderId to set
	 */
	public void setSubOrderId(String subOrderId) {
		this.subOrderId = subOrderId;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	String orderId;
	String subOrderId;
	String transactionId;


	public C30OrderTransactionLog(String orderId, String subOrderId, String transactionId) throws C30SDKToolkitException
	{
		log.debug("Constuctor - C30OrderTransactionLog");
		this.orderId=orderId;
		this.subOrderId=subOrderId;	
		this.transactionId=transactionId;
	}
	
	/** Writing the log information to SDK 
	 * 
	 * @throws C30SDKToolkitException
	 */
	public void writeToSDKTransactionLog(String logLevel, String logMessage) throws C30SDKToolkitException
	{
		String query = null;
		C30JDBCDataSource dataSource = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;		
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();  
			query = C30WPSQLConstants.INSERT_INTO_SDK_TRANSACTION_LOG;
			
			String ExcpSDKstr = "com.cycle30.sdk.toolkit.exception.C30SDKToolkitException:";
			String ExcpProvstr = "com.cycle30.prov.exception.C30ProvConnectorException:";

			if (logMessage.contains(ExcpSDKstr)){
				logMessage = logMessage.replaceAll(ExcpSDKstr, "");
				logMessage = logMessage.trim();				
			}
			
			if (logMessage.contains(ExcpProvstr)){
				logMessage = logMessage.replaceAll(ExcpProvstr, "");
				logMessage = logMessage.trim();
			}
			log.info("logLevel : " +logLevel);
			log.info("logMessage : " +logMessage);
			log.info("Trimmed off unwanted message");
			ps = dataSource.getPreparedStatement(query);
			ps.setString(1,(String)orderId);
			ps.setString(2,(String)subOrderId);
			ps.setString(3,(String)logLevel);
			ps.setString(4,(String)logMessage);
			ps.setString(5,(String)transactionId);
			
			resultSet = ps.executeQuery();			

		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Error While writing to Log",e,"ERROR");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30SDKToolkitException("Error While writing to Log",ex,"ERROR");				
			}
		}
	}
	
		
}
