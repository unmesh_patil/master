package com.cycle30.workpoint.script;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30SDKConfigurationManager;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

/** this is used to write the order extended data information 
 * Log in case of value need to be returned to client
 * @author Umesh
 *
 */
public class C30OrderExtendedDataLog {


	//Logs all the information
	private static Logger log = Logger.getLogger(C30OrderExtendedDataLog.class);
	public static C30SDKConfigurationManager exceptionResourceBundle = C30SDKConfigurationManager.getInstance();


	/**
	 * @return the acctSegId
	 */
	public String getAcctSegId() {
		return acctSegId;
	}

	/**
	 * @param acctSegId the acctSegId to set
	 */
	public void setAcctSegId(String acctSegId) {
		this.acctSegId = acctSegId;
	}

	/**
	 * @return the subOrderId
	 */
	public String getSubOrderId() {
		return subOrderId;
	}

	/**
	 * @param subOrderId the subOrderId to set
	 */
	public void setSubOrderId(String subOrderId) {
		this.subOrderId = subOrderId;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	String acctSegId;
	String subOrderId;
	String transactionId;

	public C30OrderExtendedDataLog() {

	}

	public C30OrderExtendedDataLog(String acctSegId, String subOrderId, String transactionId) throws C30SDKToolkitException
	{
		log.debug("Constuctor - C30OrderExtendedDataLog");
		this.acctSegId=acctSegId;
		this.subOrderId=subOrderId;	
		this.transactionId=transactionId;
	}

	/** Writing the sub order extended data information to SDK 
	 * 
	 * @throws C30SDKToolkitException
	 */
	public void writeToSubOrderExtendedData(String key, String value, String sendFlag) throws C30SDKToolkitException
	{
		String query = null;
		C30JDBCDataSource dataSource = null;
		PreparedStatement ps = null;
		ResultSet resultSet  = null;		
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   
			query = C30WPSQLConstants.INSERT_INTO_SUBORDER_EXTENDED_DATA;

			ps = dataSource.getPreparedStatement(query);
			ps.setString(1,(String)subOrderId);
			ps.setString(2,(String)transactionId);
			ps.setInt(3, Integer.valueOf(acctSegId));
			ps.setString(4,(String)key);
			ps.setString(5,(String)value);
			ps.setString(6,(String)sendFlag);			

			resultSet  = ps.executeQuery();	
		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Error While writing to SubOrder Extended Data",e,"ERROR");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30SDKToolkitException("Error While writing to SubOrder Extended Data",ex,"ERROR");				
			}
		}
	}

	/** Fetch the sub order extended data information from SDK 
	 * 
	 * @throws C30SDKToolkitException
	 */
	public String fetchSubOrderExtendedData(String subOrderId, String transactionId, String ExtdatKey) throws C30SDKToolkitException
	{
		String query = null;
		C30JDBCDataSource dataSource = null;
		PreparedStatement ps = null;
		ResultSet resultSet  = null;
		String Value = null;
		String Key = null;

		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   
			query = C30WPSQLConstants.FETCH_SUBORDER_EXTENDED_DATA;
			log.info("Fetcing Subrder Extended Data");

			ps = dataSource.getPreparedStatement(query);
			ps.setString(1,(String)subOrderId);
			ps.setString(2,(String)transactionId);
			ps.setString(3,(String)ExtdatKey);

			resultSet  = ps.executeQuery();	

			while (resultSet.next()) {
				Key    = resultSet.getString(1);
				Value  = resultSet.getString(2);
			}			

			return Value;

		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Error While Fetching SubOrder Extended Data",e,"ERROR");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30SDKToolkitException("Error While Fetching SubOrder Extended Data",ex,"ERROR");				
			}
		}
	}	

	/** Fetch the sub order extended data information from SDK 
	 * 
	 * @throws C30SDKToolkitException
	 */
	public String fetchParameters(String module, String parameterName) throws C30SDKToolkitException
	{
		String query = null;
		C30JDBCDataSource dataSource = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		String Value = null;

		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   
			query = C30WPSQLConstants.FETCH_C30_BIL_PARAMETERS;

			ps = dataSource.getPreparedStatement(query);
			ps.setString(1,(String)module);
			ps.setString(2,(String)parameterName);

			resultSet  = ps.executeQuery();	

			while (resultSet.next()) {
				Value  = resultSet.getString(1);
			}			

			return Value;

		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Error While Fetching Parameters",e,"ERROR");
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}				
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
				throw new C30SDKToolkitException("Error While Fetching Parameters",ex,"ERROR");				
			}
		}
	}	

}
