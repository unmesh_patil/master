package com.cycle30.workpoint.script;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

//import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ComponentObject;
import com.cycle30.sdk.schema.jaxb.C30IdentifierObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30SqlApiUtils;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;

//Provisioning
//import com.cycle30.prov.auth.ProvAuthClientDataSource;
//import com.cycle30.prov.auth.ProvAuthClientObject;
//import com.cycle30.prov.data.Constants;
//import com.cycle30.prov.exception.C30ProvConnectorException;
//import com.cycle30.prov.request.C30ProvisionRequest;


public class C30CheckOrderData extends C30OrderScript
{

	private static Logger log = Logger.getLogger(C30CheckOrderData.class);

	public C30CheckOrderData(String orderId) throws C30SDKToolkitException
	{
		super(orderId);
	}

	public C30CheckOrderData(String orderId, String subOrderId,
			String acctSegId, String transactionId, String internalAuditId)
			throws C30SDKToolkitException
	{
		super(orderId, subOrderId, acctSegId, transactionId, internalAuditId);
	}

	public void process() throws C30SDKToolkitException
	{
		// TODO Auto-generated method stub
		try
		{
			log.info("Process Check Order Data");
		}
		catch (Exception e)
		{
			log.error("EXCP - Check Order Data :" + e);
			e.printStackTrace();
			throw new C30SDKToolkitException(
					"EXCP: ORDER-Check Order Request Process", e);
		}
	}

	public Map<String, Boolean> chkOrderData() throws C30SDKToolkitException
	{
		// TODO Auto-generated method stub
		Map<String, Boolean> orderData = new HashMap<String, Boolean>();
		try
		{
			//Check on the Account Object and Identify the Action Status
			if (order.getAccount() != null)
			{
				log.info("setting boolean value for Account object");
				if (order
						.getAccount()
						.getClientActionType()
						.toString()
						.equalsIgnoreCase(
								C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
				{
					orderData.put("AddAct", true);
					log.info("boolean value for AddAct object : "
							+ orderData.get("AddAct"));
				} else if (order
						.getAccount()
						.getClientActionType()
						.toString()
						.equalsIgnoreCase(
								C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE))
				{
					orderData.put("UpdateAct", true);
					log.info("boolean value for Change account object  : "
							+ orderData.get("UpdateAct"));
				}
			}

			//Check on the ServiceObject list and Identify the Action Status 
			if (order.getServiceObjectList() != null)
			{
				List<C30ServiceObject> sList = (List<C30ServiceObject>) order
						.getServiceObjectList().getServiceObject();
				log.info("Working on the Service Object.  Size of list is: "
						+ sList.size());
				for (C30ServiceObject service : sList)
				{

					String clientId = null;
					String actionType = null;
					String clientAcctId = null;
					
					if (service.getClientAccountId() != null)	{clientAcctId = service.getClientAccountId().toString();}
					if (service.getClientActionType() != null)	{actionType = service.getClientActionType().toString();}
					if (service.getClientActionType() != null)	{clientId = service.getClientId();}
					
					log.info("service Client Account Id : '"
							+ clientAcctId
							+ "'  Service Client id: '" + clientId
							+ "'  Action type: '" + actionType + "'");

					if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
					{
						orderData.put("AddService", true);
						log.info("boolean value for AddService object: "
								+ orderData.get("AddService"));
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SUSPEND))
					{
						orderData.put("SuspendService", true);
						log.info("boolean value for SuspendService object: "
								+ orderData.get("SuspendService"));
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_RESUME))
					{
						orderData.put("ResumeService", true);
						log.info("boolean value for ResumeService object: "
								+ orderData.get("ResumeService"));
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC))
					{
						orderData.put("DisconnectService", true);
						log.info("boolean value for DiscService object: "
								+ orderData.get("DisconnectService"));
					} else if (service.getClientActionType().toString().equalsIgnoreCase(
							C30WPSQLConstants.C30_ORDER_ACTION_TYPE_CHANGE))
					{

						if ((service.getComponentObjectList() == null) && (service.getIdentifierObjectList() == null) && (service.getPlanObject() == null))
						{
							orderData.put("UpdateService", true);
							log.info("boolean value for UpdateService object: "
								+ orderData.get("UpdateService"));
						}
					} // End 

					//Check on the PlanObject list and Identify the Action Status					
					if (service.getPlanObject() != null) {
						//Check on the Plan Object
						C30PlanObject plan = service.getPlanObject().getValue();
						if (plan != null) {

							if (plan.getClientActionType().toString().equalsIgnoreCase(
									C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD)) {
								orderData.put("AddPlan", true);
								log.info("boolean value for AddPlan object: "
										+ orderData.get("AddPlan"));
							} else if (plan.getClientActionType().toString().equalsIgnoreCase(
									C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))
							{
								orderData.put("ChangePlan", true);
								log.info("boolean value for DiscPlan object: "
										+ orderData.get("ChangePlan"));
							} else if (plan.getClientActionType().toString().equalsIgnoreCase(
									C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC))
							{
								orderData.put("DiscPlan", true);
								log.info("boolean value for DiscPlan object: "
										+ orderData.get("DiscPlan"));
							}//End - Check on the Plan Object
							
						}
						else { 
							log.info("Plan object IS Null");	
						}
					}

					//Check on the ComponentObject list and Identify the Action Status
					if (service.getComponentObjectList() != null)
					{

						List<C30ComponentObject> clList = (List<C30ComponentObject>) service
								.getComponentObjectList().getComponentObject();
						log.info("Working on the Component Object.  Size of list is "
								+ clList.size());
						for (C30ComponentObject comp : clList)
						{
							if (comp.getClientActionType()
									.toString()
									.equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
							{ // Add
								orderData.put("AddComp", true);
								log.info("boolean value for AddComp object: "
										+ orderData.get("AddComp"));
							} else if (comp
									.getClientActionType()
									.toString()
									.equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC))
							{
								// Disconnect Component
								orderData.put("DiscComp", true);
								log.info("boolean value for DiscComp object: "
										+ orderData.get("DiscComp"));
							}
						}// End-for (C30ComponentObject comp : clList)

					} // End - if (service.getComponentObjectList() != null)

					//Check on the IdentifierObject list and Identify the Action Status					
					if (service.getIdentifierObjectList() != null)
					{

						List<C30IdentifierObject> ilList = (List<C30IdentifierObject>) service
								.getIdentifierObjectList()
								.getIdentifierObject();
						log.info("Working on the Identifier Object.  Size of list is "
								+ ilList.size());

						for (C30IdentifierObject identifier : ilList)
						{

							if (identifier
									.getClientActionType()
									.toString()
									.equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_ADD))
							{
								orderData.put("AddIdn", true);
								log.info("boolean value for AddIdn object: "
										+ orderData.get("AddIdn"));
							} else if (identifier
									.getClientActionType()
									.toString()
									.equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_DISC))
							{
								orderData.put("DiscIdn", true);
								log.info("boolean value for DiscIdn object: "
										+ orderData.get("DiscIdn"));
							} else if (identifier
									.getClientActionType()
									.toString()
									.equalsIgnoreCase(
											C30WPSQLConstants.C30_ORDER_ACTION_TYPE_SWAP))
							{
								orderData.put("SwapIdn", true);
								log.info("boolean value for SwapIdn object: "
										+ orderData.get("SwapIdn"));
							}

						} // End - for (C30IdentifierObject identifier :ilList)
					}// End-if (service.getIdentifierObjectList() != null)
				} // End - for (C30ServiceObject service : sList)
			}// End-if(order.getServiceObjectList()!=null)
		}
		catch (Exception e)
		{
			log.error("EXCP - Check Order Data :" + e);
			e.printStackTrace();
			throw new C30SDKToolkitException("EXCP: ORDER-Check Order Request Process", e);
		}
		log.info("Order Data details :" + orderData);
		return orderData;
	}
}
