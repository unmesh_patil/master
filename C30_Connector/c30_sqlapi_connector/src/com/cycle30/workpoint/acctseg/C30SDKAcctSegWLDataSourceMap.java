package com.cycle30.workpoint.acctseg;

/** This class represents the mapping for the AcctSegId and the Weblogic data source.
 * 
 * @author rnelluri
 *
 */
public class C30SDKAcctSegWLDataSourceMap {
	/**
	 * @return the acctSegid
	 */
	public Integer getAcctSegid() {
		return acctSegid;
	}
	/**
	 * @param acctSegid the acctSegid to set
	 */
	public void setAcctSegid(Integer acctSegid) {
		this.acctSegid = acctSegid;
	}
	/**
	 * @return the weblogicDataSource
	 */
	public String getWeblogicDataSource() {
		return weblogicDataSource;
	}
	/**
	 * @param weblogicDataSource the weblogicDataSource to set
	 */
	public void setWeblogicDataSource(String weblogicDataSource) {
		this.weblogicDataSource = weblogicDataSource;
	}
	
	public String toString()
	{
		return "AcctSegId:"+acctSegid.toString()+",DataSourceName:"+weblogicDataSource;
	}
	/**Stores the acct Segment id */
	private Integer acctSegid;
	/**Stores the datasource name for the given acct segment for authentication.*/
	private String weblogicDataSource;
	
}
