package com.cycle30.workpoint.acctseg;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.workpoint.sqlapi.utils.C30WPSQLConstants;
import com.cycle30.workpoint.sqlapi.utils.C30WPSqlApiConnector;

public class C30SDKAcctSegWLDataSourceMapImpl {

	private static C30SDKAcctSegWLDataSourceMapImpl instance= null;
	private static HashMap acctSegToDataSourceMapping = null;
	private static ArrayList<C30SDKAcctSegWLDataSourceMap> acctSegToDataSourceList = new ArrayList();
	
	//Logs all the information
	private static Logger log = Logger.getLogger(C30SDKAcctSegWLDataSourceMapImpl.class);

	
	/** Constructor to get the data from the database
	 * @throws C30SDKToolkitException 
	 * 
	 */
	C30SDKAcctSegWLDataSourceMapImpl() throws C30SDKToolkitException
	{
		acctSegToDataSourceMapping = getAcctSegWeblogicDataMap();
	}
	
	/**
	 * Returns the one-and-only instance of the C30SDKAcctSegWLDataSourceMapImpl.
	 *
	 *@return    The one-and-only instance of the C30SDKAcctSegWLDataSourceMapImpl.
	 * @throws C30SDKToolkitException 
	 */
	public static C30SDKAcctSegWLDataSourceMapImpl getInstance() throws C30SDKToolkitException {
		if (instance == null) 
		{
			if( log != null ) 
			{
				log.debug("Creating new instance of C30SDKAcctSegWLDataSourceMapImpl"); 
			}
			instance = new C30SDKAcctSegWLDataSourceMapImpl();
		}

		return instance;
	}

	/** Writing the log information to SDK 
	 * 
	 * @throws C30SDKToolkitException
	 */
	public HashMap getAcctSegWeblogicDataMap() throws C30SDKToolkitException
	{
		String query = null;
		C30JDBCDataSource dataSource = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		HashMap acctSegDatamapList = new HashMap();
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   
			query = C30WPSQLConstants.GET_ACCT_SEG_ID_WEBLOGIC_DATASOURCE_NAME;
			log.info("Query used for getting AcctSegment vs dataSource mapping : " +query);
			ps = dataSource.getPreparedStatement(query);
			resultSet  = ps.executeQuery();			
			while (resultSet.next()) {
				C30SDKAcctSegWLDataSourceMap  c30acctSegWLData = new C30SDKAcctSegWLDataSourceMap();
				c30acctSegWLData.setAcctSegid(resultSet.getInt("ACCT_SEG_ID"));
				c30acctSegWLData.setWeblogicDataSource(resultSet.getString("C30_WEBLOGIC_DATASOURCE"));
				acctSegToDataSourceList.add(c30acctSegWLData);
				acctSegDatamapList.put(resultSet.getInt("ACCT_SEG_ID"), c30acctSegWLData);
				log.info("Daa from database - " +resultSet.getInt("ACCT_SEG_ID") +" , " + resultSet.getString("C30_WEBLOGIC_DATASOURCE")+ "; "+c30acctSegWLData.toString());
			}
		}
		catch(Exception e)
		{
			throw new C30SDKToolkitException("Error While getting the datasource names for acctSegments",e,"ERROR");
		}
		finally{
			try{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				//Return the connection back to pool
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
			}
			catch(Exception e)
			{
				throw new C30SDKToolkitException("Error While freeing the datasource names for acctSegments",e,"ERROR");
			}
		}
		
		return acctSegDatamapList;
	}

	/** This method returns the dataSourceName for the given AcctSegid as HashMap
	 * 
	 * @param acctSegId
	 * @return
	 */
	public C30SDKAcctSegWLDataSourceMap getDataSourceForGivenAcctSegment(Integer acctSegId)
	{
		C30SDKAcctSegWLDataSourceMap retValue = null;
		if (acctSegToDataSourceMapping !=null)
		{
			retValue = (C30SDKAcctSegWLDataSourceMap) acctSegToDataSourceMapping.get(acctSegId);
		}
		return retValue;
	}
	
	/** This returns the Mapping as ArrayList
	 * 
	 * @return
	 */
	public  ArrayList<C30SDKAcctSegWLDataSourceMap> getAcctSegToDataSourceList()
	{
		return  acctSegToDataSourceList;
	}
}
