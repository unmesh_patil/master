package com.cycle30.workpoint.sqlapi;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Hashtable;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;

public class C30WPDiscComponentApiTest {

	Connection conn = null;
	String url = "jdbc:oracle:thin:@c3b1devoradb1.cycle30.com:1521:C3BDV1";
	String user = "arbor";
	String pwd = "arbor123";
	
	@Before
	public  void oneTimeSetUp() throws Exception {
		conn =
	         DriverManager.getConnection(url,user, pwd);
		System.out.println("Connection secured");

	}

	
	@Test
	public void testC30WPDiscComponentApiConnectionStringS() {

		try {
			
			String	v_transaction_id		="100010";
			String	v_acct_seg_id			="3";
			String  v_catalog_id			="INTLROAM";
			String  v_instance_id			="98116242999INTLROAM20120522";
			String	v_action_date			="Thu Sep 06 01:09:15 AKST 2012";
			String	v_action_who			="TestUser";
			
			C30WPDiscComponentApi discComp  = new C30WPDiscComponentApi(conn, v_transaction_id, v_acct_seg_id, v_catalog_id, v_instance_id, 
					v_action_date, v_action_who);
			

			//Check the Output			
			if ((discComp.getDataSource() != null) && (discComp.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = discComp.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					System.out.println("Disc Component OUT PARAM1 : " + outparam.keySet() + " Disc Component OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						System.out.println("Disc Component OUT PARAM=true");					
					}
				}
			}//End - if (a.getDataSource().getResultMap() != null)
			else { 
				System.out.println("ERROR - DataSource/Result Map IS NULL ");
			}//End - Else if (a.getDataSource().getResultMap() != null)
			
		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

}
