package com.cycle30.workpoint.sqlapi;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;

public class C30WPResumeServiceApiTest {

	Connection conn = null;
	String url = "jdbc:oracle:thin:@c3b1devoradb1.cycle30.com:1521:C3BDV1";
	String user = "arbor";
	String pwd = "arbor123";
	
	@Before
	public  void oneTimeSetUp() throws Exception {
		conn =
	         DriverManager.getConnection(url,user, pwd);
		System.out.println("Connection secured");

	}
	
	@Test
	public void testC30WPResumeServiceApi() {
	
		try {
			
			String	v_transaction_id		="100013";
			String	v_acct_seg_id			="3";
			String	v_service_id			="98113422999";
			String	v_action_date			="Fri Jul 22 06:09:15 AKST 2012";
			String	v_action_who			="rnelluri";
			String	v_resume_reason			="1";
	
			C30WPResumeServiceApi resumeService  = new C30WPResumeServiceApi(conn, v_transaction_id, v_acct_seg_id, v_service_id, v_action_date, 
					 v_action_who, v_resume_reason);			

			//Check the Output			
			if ((resumeService.getDataSource() != null) && (resumeService.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = resumeService.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					System.out.println("Resume Service OUT PARAM1 : " + outparam.keySet() + " Resume Service OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						System.out.println("Resume Service OUT PARAM=true");					
					}
				}
			}//End - if (a.getDataSource().getResultMap() != null)
			else { 
				System.out.println("ERROR - DataSource/Result Map IS NULL ");
			}//End - Else if (a.getDataSource().getResultMap() != null)

		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		fail("Not yet implemented");
	}

}
