package com.cycle30.workpoint.sqlapi;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;

public class C30WPAddComponentApiTest {

	Connection conn = null;
	String url = "jdbc:oracle:thin:@c3b1devoradb1.cycle30.com:1521:C3BDV1";
	String user = "arbor";
	String pwd = "arbor123";
	
	@Before
	public  void oneTimeSetUp() throws Exception {
		conn =
	         DriverManager.getConnection(url,user, pwd);
		System.out.println("Connection secured");

	}
	
	@Test
	public void testC30WPAddComponentApi() {
	
		try {
			
			String	v_transaction_id		="79678";
			String	v_acct_seg_id			="3";
			String	v_catalog_id			="MSG";
			String  v_instance_id			="MSG90742076722012090715";
			String	v_action_date			="Fri Sep 07 04:30:15 AKST 2012";
			String	v_action_who			="UserABC";
			String	v_primary_service_id	="9074207672";
			oracle.sql.ARRAY	v_service_id_array		=null;
			
			try {
			StructDescriptor serviceStructDesc =
					StructDescriptor.createDescriptor("C30ARBOR.C30STRING", conn);
					ArrayDescriptor arrayDescriptor = ArrayDescriptor.createDescriptor("C30ARBOR.C30STRINGARRAY", conn);
					int i = 0;
					int size = 1;
					Object[] bArray =null;

					if (v_catalog_id == null) {
							bArray = new Object[size];
							STRUCT m1 = new STRUCT(serviceStructDesc, conn, new Object[]{v_primary_service_id});
							bArray[i]=m1;
						}
					System.out.println("servIdListArray before : " +v_service_id_array);
					v_service_id_array = new oracle.sql.ARRAY(arrayDescriptor, conn, bArray);
					System.out.println("servIdListArray After : " +v_service_id_array);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			C30WPAddComponentApi addComp  = new C30WPAddComponentApi(conn, v_transaction_id, v_acct_seg_id, v_catalog_id, v_instance_id, 
					v_action_date, v_action_who, v_primary_service_id, v_service_id_array);
			

			//Check the Output			
			if ((addComp.getDataSource() != null) && (addComp.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = addComp.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					System.out.println("Add Component OUT PARAM1 : " + outparam.keySet() + " Add Component OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						System.out.println("Add Component OUT PARAM=true");					
					}
				}
			}//End - if (a.getDataSource().getResultMap() != null)
			else { 
				System.out.println("ERROR - DataSource/Result Map IS NULL ");
			}//End - Else if (a.getDataSource().getResultMap() != null)

		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		fail("Not yet implemented");
	}

}
