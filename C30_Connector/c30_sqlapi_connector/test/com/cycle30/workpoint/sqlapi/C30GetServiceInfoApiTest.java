package com.cycle30.workpoint.sqlapi;

import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.util.Hashtable;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

//import com.cycle30.sdk.object.cycle30.order.C30SDKOrderManager;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;

public class C30GetServiceInfoApiTest {

	Connection conn = null;
	String url = "jdbc:oracle:thin:@c3b1devoradb1.cycle30.com:1521:C3BDV1";
	String user = "arbor";
	String pwd = "arbor123";

	@Before
	public  void oneTimeSetUp() throws Exception {
		conn =
				DriverManager.getConnection(url,user, pwd);
		System.out.println("Connection secured");

	}

	@Test
	public void testC30GetServiceInfoApi() throws SQLException {

		try {

			String	v_transaction_id		="100014";
			String	v_acct_seg_id			="3";
			String	v_service_id			="98112042999";


			C30WPGetServiceInfoApi getSvcInfo  = new C30WPGetServiceInfoApi(conn, v_transaction_id, v_acct_seg_id, v_service_id);
			int count = getSvcInfo.getDataSource().getRowCount();
			System.out.println("row count : "+getSvcInfo.getDataSource().getRowCount());
			System.out.println("column count : "+getSvcInfo.getDataSource().getColumnCount());


			//The return list of identifer col is Array
			Array identiferList = (Array) getSvcInfo.getDataSource().getValueAt(0,8);
			Object[] objects = (Object[]) identiferList.getArray();
			Object[] attr = null;
			for (Object obj :objects)
			{
				attr = (Object[]) ((Struct)obj).getAttributes();
				System.out.println(attr[0] +" - " + attr[1]);
			}
			/*
			if(getSvcInfo.getDataSource().getRowCount() > 0) {
				for(int i = 0; i < getSvcInfo.getDataSource().getRowCount(); i++)  {

					if(getSvcInfo.getDataSource().getValueAt(i,1)!= null)   {
					System.out.println("Output :"+(String)getSvcInfo.getDataSource().getObjectAt(0,8));	
					System.out.println("Output :"+(String)getSvcInfo.getDataSource().getValueAt(8,0));
					}

				}
			}			
			 */
			//Check the Output			
			if ((getSvcInfo.getDataSource() != null) && (getSvcInfo.getDataSource().getResultMap() != null)) {
				Iterator<?> itr = getSvcInfo.getDataSource().getResultMap().values()
						.iterator();
				while (itr.hasNext())
				{
					Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
					System.out.println("Get Service Info OUT PARAM1 : " + outparam.keySet() + " Get Service Info OUT PARAM2 :"
							+ outparam.values());
					if (outparam.values()!=null)
					{
						System.out.println("Suspend Service OUT PARAM=true");					
					}
				}
			}//End - if 
			else { 
				System.out.println("ERROR - DataSource/Result Map IS NULL ");
			}//End - Else if

		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fail("Not yet implemented");
	}

}
