package com.cycle30.workpoint.sqlapi;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;

public class C30WPAddAccountApiTest {

	Connection conn = null;
	String url = "jdbc:oracle:thin:@c3b1devoradb1.cycle30.com:1521:C3BDV1";
	String user = "arbor";
	String pwd = "arbor123";
	
	@Before
	public  void oneTimeSetUp() throws Exception {
		conn =
	         DriverManager.getConnection(url,user, pwd);
		System.out.println("Connection secured");

	}
	
	@Test
	public void testC30WPAddAccountApi() {
	
		try {
	
			C30WPAddAccountApi a  = new C30WPAddAccountApi(conn, "1167", "1", "ActAdd1167", "Thu Feb 09 01:09:15 AKST 2012", "UserABC", 
					null,"1", "1", "112", "AA", "NE", "Bell St", "ALY", "NE", "APT", "12", "Villa1", "vl2", "vl3", "Anchorage", "ACounty", "Anchorage",
					"99503", "1234", "840", "Abc", "as", "asd", "k", "gosh", "prd", "M01", "1", "0", "0", "1", "1", "1",
					"1", "1", "1", "1", "1", "1", "1", "1", "112341234", "1", "1", "1", "1","1", null );

			if (a.getDataSource().getResultMap().values().isEmpty()) {
				System.out.println("ResultMap EMPTY : " +a.getDataSource().getResultMap().values().size());
			}
			else{
				System.out.println("ResultMap NOT EMPTY  : " +a.getDataSource().getResultMap().values().size());				
			}

			Iterator<?> itr = a.getDataSource().getResultMap().values()
					.iterator();
			while (itr.hasNext())
			{
				Hashtable<?, ?> outparam = (Hashtable<?, ?>) itr.next();
				System.out.println("Update ACT OUT PARAM1 : " + outparam.keySet() + " Update ACT OUT PARAM2 :" + outparam.values());
			}
		
			
		} catch (C30SDKToolkitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		fail("Not yet implemented");
	}

}
