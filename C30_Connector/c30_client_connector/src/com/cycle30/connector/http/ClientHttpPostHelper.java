package com.cycle30.connector.http;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.connector.data.ClientProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectorConstants;
import com.cycle30.connector.util.ConnectorUtil;
import com.cycle30.connector.util.DomUtils;


/**
 * 
 * Class for authenticating, and invoking web service via HTTP POST 
 *
 */
public class ClientHttpPostHelper {

    private static final Logger log = Logger.getLogger(ClientHttpPostHelper.class);
    
    /**
     * 
     * @param xmlPayload
     */
    public String post(ClientProperties clientProperties, String xmlPayload, String callbackType)
                                          throws ConnectorException 
    {
        String postResponse = null;
        HashMap<String, String>oAuthProps = new HashMap<String, String>();
        
        // Get existing oAuth response properties. If it doesn't exist, need to authenticate
        oAuthProps = ClientAuthenticator.getOauthResponse(clientProperties.getOrgId());
        if (oAuthProps == null) {
           oAuthProps = ClientAuthenticator.authenticate(clientProperties);
        }
        
        // Get the token returned in the Map of authorization properties
        String authToken = oAuthProps.get("access_token");
        log.info("authToken:"+authToken);
        // Get the instance URL returned. This is base domain where to send the 
        String instanceUrl = oAuthProps.get("instance_url");
        log.info("instanceUrl:"+instanceUrl); 
        if (instanceUrl == null) {
            throw new ConnectorException("Unable to resolve 'instance_url' from authentication response");
        }
        
        String updatePath = clientProperties.getUrlByCallbackType(clientProperties.getOrgId(), callbackType);
        log.info("updatePath:"+updatePath); 
        // Tag on the actual orderupdate path. This is an org-specific property derived
        // from the CLIENT db table.
        if (updatePath == null || updatePath.length()==0) {
            throw new ConnectorException("No STATUS_CALLBACK_URL specified in Client database table.");
        }
        if (updatePath == null) {
            throw new ConnectorException("Unable to resolve order update path");
        }
        
        String orderUpdateURL = instanceUrl + updatePath;
        log.info("orderUpdateURL:"+orderUpdateURL); 
        
        // Invoke web service via generic HTTP POST class.
        HashMap<String, String> authHeaders = new HashMap<String, String>();    
        //authHeaders.put("Content-encoding:", "application/xml");
        authHeaders.put("Content-type", "application/xml");
        authHeaders.put("Accept", "application/xml");
        authHeaders.put("Authorization", "OAuth " + authToken);
        
        
        int retries = 0;
        while (retries <= ConnectorConstants.MAX_SESSION_RETRIES) {
        	log.info("orderUpdateURL:"+orderUpdateURL);
        	log.info("xmlPayload:"+xmlPayload);
        	log.info("posting messages to URL :"+orderUpdateURL);
        	try{
            postResponse = HttpPoster.post(orderUpdateURL, xmlPayload, authHeaders); 
        	}catch(Exception e){
        		log.error("Exceptin Occured......"+e);
				log.error(e);
					e.printStackTrace();		
        	}catch (Throwable he) {
				log.error("Throwable......"+he);
				log.error(he);
					he.printStackTrace();							
			}
            log.info("post  Order Response:"+postResponse);
            //postResponse = HttpPoster.post(orderUpdateURL, xmlPayload, authHeaders);
    //PW-245 vijaya
			if (postResponse != null && !("".equalsIgnoreCase(postResponse))) {/*
				// If the session hasn't expired, return the response
				if (ClientAuthenticator.sessionExpired(postResponse) == false) {
					return postResponse;
				} else {
					// Need to re-authenticate, reset oAuth header... and try
					// again
					log.info("oAuth token expired. Retrieving new token...");
					oAuthProps = ClientAuthenticator
							.authenticate(clientProperties);
					authToken = oAuthProps.get("access_token");
					authHeaders.put("Authorization", "OAuth " + authToken);
					log.info("Retrieved new token: " + authToken);

					retries++;
				}

			*/				// If the session hasn't expired, return the response				
				if(postResponse.contains("500 Internal Server")||postResponse.contains("403")||postResponse.contains("404")||postResponse.contains("405")){
					log.info("Internal Server due to ROR");
				break;	
				}
				
				if (ClientAuthenticator.sessionExpired(postResponse) == false) {
					return postResponse;
				} else {
					// Need to re-authenticate, reset oAuth header... and try
					// again
					log.info("oAuth token expired. Retrieving new token...");
					oAuthProps = ClientAuthenticator.authenticate(clientProperties);
					authToken = oAuthProps.get("access_token");
					authHeaders.put("Authorization", "OAuth " + authToken);
					log.info("Retrieved new token: " + authToken);
					retries++;
				} 
				// else part for internal error
			} else {
				log.info("Sucessfully posted");
				postResponse = "Sucessfully Posted the callback";
				break;
			}
		}
        
        return postResponse;
    }
}
