package com.cycle30.connector.http;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.connector.data.ClientProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectorUtil;
import com.cycle30.connector.util.DomUtils;

/**
 * 
 * Class for authenticating against client site
 *
 */
public class ClientAuthenticator {

    
    private static final Logger log = Logger.getLogger(ClientAuthenticator.class);
    
    // Keep list of oAuth responses
    private static HashMap<String,String> oAuthResponses = new HashMap<String, String>();
    
    
    
    /**
     * Return oAuth response in form of a set of Map properties.  An oAuth response will
     * be returned/stored in following format:
     * 
     * <?xml version="1.0" encoding="utf-8"?>
     * <OAuth>
     *   <id>https://login.salesforce.com/id/00DU0000000J354MAC/005U0000000dLKuIAM</id>
     *   <issued_at>1319834530276</issued_at>
     *   <instance_url>https://na12.salesforce.com</instance_url>
     *   <signature>vjTdp/SalmcA+fSIPw9f8Ej+Bq90FftITIa3OpP+EI=</signature>
     *   <access_token>00DU0000000J354!AR0AQDiE36egAd78xrFdO9MbQwS9MaZlPVn8HMCvFzGvGIN1fJNUx0ZnYziMVm.Fczp7HHaTwLBYiYkmkyN1Qax7mRidzYkv</access_token>
     * </OAuth>
     * 
     * 
     * Authentication errors will be returned with error/description:
     * 
     * <?xml version="1.0" encoding="utf-8"?>
     * <OAuth>
     *   <error>invalid_client</error>
     *   <error_description>invalid client credentials</error_description>
     * </OAuth>
     * 
     * 
     * @param orgId
     * @return map of oAuth response properties
     */
    public static HashMap<String, String> getOauthResponse(String orgId) throws ConnectorException {       
        
        String authResponse = oAuthResponses.get(orgId);  
        
        if (authResponse == null) {
            return null;
        }      
        
        // The response is in XML format.  Convert to DOM.
        Document dom = DomUtils.stringToDom(authResponse);
        
        // Check for authentication error.
        String error = DomUtils.getElementValue(dom, "error");
        if (error != null) {
            String errorDescr = DomUtils.getElementValue(dom, "error_description");
            throw new ConnectorException("Error authenticating in Force. Error: " + error + ". Description: " + errorDescr);
        }
        
        // Then extract all properties and place in HashMap
        HashMap<String, String> responseProps = new HashMap<String, String>();
        
        responseProps.put("id",           DomUtils.getElementValue(dom, "id"));
        responseProps.put("issued_at",    DomUtils.getElementValue(dom, "issued_at"));
        responseProps.put("instance_url", DomUtils.getElementValue(dom, "instance_url"));
        responseProps.put("signature",    DomUtils.getElementValue(dom, "signature"));
        responseProps.put("access_token", DomUtils.getElementValue(dom, "access_token"));
        
        return responseProps;
    }
    
    
    /**
     * 
     * @param orgId
     * @param authResponse
     */
    public static void setOauthResponse(String orgId, String authResponse) {       
        
        oAuthResponses.put(orgId, authResponse);        
        
    } 
        
    
    /**
     * '
     * @param clientProperties
     * @return Map of client properties
     */
    public static synchronized HashMap<String, String> authenticate(ClientProperties clientProperties) throws ConnectorException {
      
    	  String orgId = clientProperties.getOrgId();
        String authPayload = buildAuthPayload(clientProperties);
        log.info("authpayload...:"+authPayload); 
        try {
        String oAuthResponse = postAuthenticateRequest(authPayload);
        log.info("oAuthResponse......:"+oAuthResponse);	    
        if(oAuthResponse.contains("<respond_to?:to_xml/>")){
            try{ 
           //setting OauthResponse for ROR	 
       	 // log.info("second if else loop inside");  
           String oAuthResp= oAuthResponse.substring(0, oAuthResponse.indexOf("<respond_to?:to_xml/>"));
           log.info("oAuthResp after splitting ROR extra Tags:"+oAuthResp);
           	setOauthResponse(orgId, oAuthResp);	                	
                }catch(Exception e){
           	   log.info("Exception occured when remove extra tag <respond_to?:to_xml/> from ROR"+"Exception:"+e);   
           	 }
       	}
        else{
        // Cache the entire response
          setOauthResponse(orgId, oAuthResponse);
        }
    }
        catch(Exception e){
        	log.error("Exceptin Occured.....in Authenticate"+e);
        	log.error(e);
        	e.printStackTrace();		
        }catch (Throwable ex) {
        	log.error("Throwable.....in Authenticate"+ex);
        	log.error(ex);
        	ex.printStackTrace();							
     }
        // Return map of oAuth properties
        //return null;
        return getOauthResponse(orgId);
        
    }    
    
    
    /**
     * 
     * @param clientProperties
     * @param currentToken
     */
    public static synchronized void refreshToken(ClientProperties clientProperties, 
                                                     String currentToken) throws ConnectorException {

        String refreshPayload = buildRefreshPayload(clientProperties, currentToken);
        
        String refreshResponse = postAuthenticateRequest(refreshPayload);
  
        log.info("Refresh token response: " + refreshResponse);
  
    }
    
    
    
    private static synchronized String postAuthenticateRequest(String authPayload)  throws ConnectorException {
        
        String url = ConnectorUtil.getConnectorProperty("auth.url");
        HashMap<String, String> authHeaders = new HashMap<String, String>();
        
        authHeaders.put("Accept", "application/xml");
        authHeaders.put("Content-Type", "application/x-www-form-urlencoded");
        
        String response = HttpPoster.post(url, authPayload, authHeaders);
        
        return response;
    }
    
    
    /**
     * 
     * @param clientProperties
     * @return
     */
    private static synchronized String buildAuthPayload(ClientProperties clientProperties) {
        
        StringBuffer sb = new StringBuffer();
        
        String userName     = clientProperties.getUsername();
        String clientSecret = clientProperties.getClientSecret();
        String password     = clientProperties.getPassword();
        String orgToken     = clientProperties.getClientOrgToken();
        String clientId     = clientProperties.getClientConsumerKey();
        
        
        sb.append("username=" + userName + "&");
        sb.append("client_secret=" + clientSecret + "&");
        
        // password is the org password + org token for 'autonamous' oAuth
//        sb.append("grant_type=" + "password" + "&");
//        sb.append("client_id="  + clientId);
        sb.append("password="   + password + orgToken + "&");
        sb.append("grant_type=" + "password" + "&");
        sb.append("client_id="  + clientId);

        
        String payload = sb.toString();

        return payload;
    }
    
    
    /**
     * 
     * @param clientProperties
     * @return
     */
    private static synchronized String buildRefreshPayload(ClientProperties clientProperties,
                                                               String currentToken) {
        
        StringBuffer sb = new StringBuffer();
        ;
        String clientSecret = clientProperties.getClientSecret();
        
        sb.append("grant_type="    + "refresh_token" + "&");
        sb.append("client_secret=" + clientSecret + "&");
        sb.append("refresh_token=" + currentToken);
        
        String payload = sb.toString();
        
        return payload;
    }

    
    
    
    /**
     * 
     * @param response
     * @return true if session expired
     */
    public static synchronized boolean sessionExpired(String response) throws ConnectorException {

         Document responseDom = DomUtils.stringToDom(response);
         
         String errorMessage = DomUtils.getElementValue(responseDom, "message");
         if (errorMessage != null && errorMessage.startsWith("Session expired")) {
             return true;
         }
         
         return false;
    }
    
}
