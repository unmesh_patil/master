package com.cycle30.connector.http;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;


/** 
 * 
 * General purpose class for sending HTTP POST messages
 *
 */
public class HttpPoster {

    
    private static final Logger log = Logger.getLogger(HttpPoster.class);
    
    /**
     * 
     * @param url
     * @param payLoad
     * @param headers
     * @return response 
     */
    public static synchronized String post(String url, 
                                              String payLoad, 
                                              HashMap<String, String> headers) throws ConnectorException  {
        
        String postResponse = null;
        
        HttpPost post = new HttpPost(url);
        
        // get/set headers
        Iterator i = headers.keySet().iterator();
        while (i.hasNext()) {
            String key   = (String)i.next();
            String value = (String)headers.get(key);
            post.setHeader(key, value);  
        }
        
        try {
            
            log.info("Posting request to URL: " + url);
            log.info("Posting request using payload: " + payLoad);
            HttpEntity entity = new StringEntity(payLoad);
            post.setEntity(entity);
            
            
            // get the response entity value.
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse resp = client.execute(post);
            
            HttpEntity respEntity = resp.getEntity();
            InputStream is = respEntity.getContent();
            int status = resp.getStatusLine().getStatusCode();
            
            
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();    
            String line;
            while ((line = br.readLine()) != null) {
                    sb.append(line);
            } 
            
            postResponse = sb.toString();
            log.info("POST response HTTP status: " + status);
            log.info("Got POST response: " + postResponse);
        
        } catch (Exception e) {
             throw new ConnectorException (e);
        }
        

        return postResponse;
        
    }
}
