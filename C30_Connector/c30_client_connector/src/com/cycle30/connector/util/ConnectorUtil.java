package com.cycle30.connector.util;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate.encryptor.HibernatePBEEncryptorRegistry;

import com.cycle30.connector.exception.ConnectorException;


/**
 * 
 * Connector general-purpose utility class
 *
 */
public class ConnectorUtil {

    
    private final static String  DB_PROPERTIES_FILE_NAME = "com.cycle30.connector.c30clientconnector";
    private static ResourceBundle DB_PROPERTIES   = null;
    private static final Logger log = Logger.getLogger(ConnectorUtil.class);
    
    
    // System environment variable (set in script).
    private static final String ENCRYPTION_KEY_ENV_VAR = "C30APPENCKEY";

    
    
    /**
     * 
     * @param propertyName
     * @return property string
     * @throws ConnectorException
     */
    public static String getConnectorProperty(String propertyName)  throws ConnectorException {
        
        if (DB_PROPERTIES == null) {
            try {
                Locale locale = Locale.getDefault();
                DB_PROPERTIES = ResourceBundle.getBundle(DB_PROPERTIES_FILE_NAME, locale);
                
            } catch (Exception e) {
                log.error(e);
                throw new ConnectorException("Error opening " + DB_PROPERTIES_FILE_NAME + ".properties file.", e);
            }
        }
        
        String propertyValue = DB_PROPERTIES.getString(propertyName);
        
        return propertyValue;
    }
    
    
    
    /**
     * 
     * @return decrypted password
     */
    public static String decryptPassword(String password) throws ConnectorException {
        
        // decrypt the password.
        String decryptedPwd = null;
        
        try {
                      
            String decryptionPassword = System.getenv(ENCRYPTION_KEY_ENV_VAR);
            if (decryptionPassword == null) {
                throw new ConnectorException(ENCRYPTION_KEY_ENV_VAR + " env variable not found!");
            }
            
            StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
            strongEncryptor.setAlgorithm("PBEWithMD5AndDES");
            strongEncryptor.setPassword(decryptionPassword);
            HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
            registry.registerPBEStringEncryptor("configurationHibernateEncryptor", strongEncryptor);
            decryptedPwd = strongEncryptor.decrypt(password);

        } 
        catch (Exception e) {
            log.error(e);
            throw new ConnectorException("Error decrypting db password.", e);
        }
        
        return decryptedPwd;
        
    }
    
    
    /**
     * 
     * @param xml
     * @param elementName
     * @return XML element value
     */
    public static synchronized String getXmlStringElementValue (String xml, String elementName) {
        
        if (xml == null || xml.length()==0) {
            return "";
        }
        
        
        String beginElement = "<" + elementName + ">";
        int start = xml.indexOf(beginElement);
        
        String endElement = "</" + elementName + ">";
        int end   = xml.indexOf(endElement);
        
        if (start == -1 || end == -1) {
            return null;
        }
        
        // Get description w/i the <description> tags
        start += (elementName.length()) + 2; // start of data = start element (+ 2 for '<' and '>' chars)
        String value = xml.substring(start, end);
        
        return value;
        
    }
    
    /** Convert the Util date to Sql Date
	 * 
	 * @param inputDate
	 * @return
	 * @throws ParseException
	 */
	public static java.sql.Date getSQLDateFromUtilDate(Date inputDate) throws ParseException{
		if (inputDate != null) 
		{
			java.sql.Date sqlDate = new java.sql.Date(((Date)inputDate).getTime());
			return sqlDate;
		}
		else
			return null;
	}

    
}
