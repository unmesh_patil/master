package com.cycle30.connector.util;

 
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.cycle30.connector.exception.ConnectorException;



public class DomUtils {

    private static final Logger log = Logger.getLogger(DomUtils.class);
    
    //--------------------------------------------------------------------------
    /** Get the child node list from underneath the parent element name passed.
     *  Recursively traverse DOM tree attempting to find it.
     * 
     * @param currentList Current node list to process.
     * @return parentElement The parent element name we're looking for.
     */
    public static synchronized NodeList getChildNodes (NodeList currentList,
                                             String parentElement) {
        
        
        //  iterate through list looking for element name.
        int i = currentList.getLength();
        for (int j = 0; j < currentList.getLength(); j++) {
            
            Node childNode = currentList.item(j);
            if (childNode == null) {
                return null;
            }
            
            // we only care about element nodes.
            if (childNode.getNodeType() != Node.ELEMENT_NODE)
                continue;
            
            // if we have a node name match, return the nodes child list
            String childNodeName = childNode.getNodeName();
            if (childNodeName != null && childNodeName.equalsIgnoreCase(parentElement)) {
                return childNode.getChildNodes();
            }
            
            // this child element name doesn't match, so recursively invoke method  
            // with it's child nodes.
            NodeList list = getChildNodes(childNode.getChildNodes(), parentElement);
            if (list != null) {
                return list;
            }

        }
        
        return null;
        
    }
    
    
    //---------------------------------------------------------------------------
    /** Get all values associated with an element name 
     * 
     * @param domDocument
     * @param elementName
     * 
     */
    public static synchronized ArrayList getElementValues(Document domDocument, String elementName)  {
       
       ArrayList elementValues = new ArrayList();
       
       // Get all nodes associated with the element name.
       NodeList nodelist = domDocument.getElementsByTagName(elementName);           
       if (null == nodelist.item(0)) {
               return elementValues;    
       }
       
       int count = nodelist.getLength();
       
       // Keep getting values until we run out
       for (int i = 0; i < nodelist.getLength(); i++) {

           Node node = nodelist.item(i);
           if (node == null) {
               continue;
           }
           Node child = node.getFirstChild();
           if (child == null) {
               continue;
           }
           String nodeValue = child.getNodeValue();
           elementValues.add(nodeValue);
            
       }  
       
       return elementValues;
       
   }
    
    
    //---------------------------------------------------------------------------
    /** Get single value associated with an element name 
     * 
     * @param dom
     * @param elementName
     * 
     */
    public static synchronized String getElementValue (Document dom, String elementName) {
        
        NodeList nodeList = dom.getElementsByTagName(elementName);
        if (nodeList == null) {
            return null;
        }
        
        Node node = nodeList.item(0);
        if (node == null) {
            return null;
        }
        
        String value = "";
        Node n = node.getFirstChild();
        if (n != null) {
            value = n.getNodeValue();
        }

        return value;
    }
    
    
    /**
     * 
     * @param dom
     * @return XML string
     */
    public static synchronized String domToXtring (Document dom) throws Exception {
        
        String xml = "";
        
        if (dom == null) {
            return "";
        }
        
        try {
            
            // transform DOM response to string
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(dom);
            
            trans.transform(source, result);
            xml = sw.toString(); 
        
        }  catch (Exception e) { 
            throw e;
        }
        
        return xml;
    }
    
    
    /**
     * 
     * @param xml
     * @return DOM document
     */
    public static synchronized Document stringToDom(String xml) throws ConnectorException {
        
        Document domDocument = null;
        
        
        if (xml == null) {
            return null;
        }
        
        try {
            
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder        = factory.newDocumentBuilder();
            domDocument                    = builder.parse(new InputSource(new StringReader(xml)));
            
          } catch (Exception e) {
             log.error(e);
             throw new ConnectorException("Unable to convert xml string " + xml + " to a DOM document", e);
          }
          
          return domDocument;
    }

    
}
