package com.cycle30.connector.util;

import java.util.HashMap;

/**
 * 
 * Connector project constants
 *
 */
public class ConnectorConstants {
    
    public static final int MAX_SESSION_RETRIES = 1;
    
    public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    
    // Update constants (defined in SDK)
    public static final String ORD_STATUS_INITIATED   = "0";
    public static final String ORD_STATUS_IN_PROGRESS = "10";
    public static final String ORD_STATUS_COMMITTED   = "20";
    public static final String ORD_STATUS_COMPLETED   = "80";
    public static final String ORD_STATUS_CANCELLED   = "90";
    public static final String ORD_STATUS_IN_ERROR    = "99";
    public static final String ORD_STATUS_COMPLETED_STRING   = "Completed";
    public static final String ORD_STATUS_COMMITTED_STRING   = "Committed";
    public static final String ORD_STATUS_FAILED_STRING   = "Failed";
    public static final String ORD_STATUS_ERROR_STRING   = "ERROR";
    
    public static final String ORDER_CALL_BACK_TYPE="Order";
    
    
    // Order status map
    public final static HashMap<String, String> ORDER_STATUS_MAP = new HashMap<String,String>();
    static
    {
        ORDER_STATUS_MAP.put("0",  "Initiated");
        ORDER_STATUS_MAP.put("10", "In Progress");
        ORDER_STATUS_MAP.put("20", "Committed");
        ORDER_STATUS_MAP.put("80", "Completed");
        ORDER_STATUS_MAP.put("90", "Canceled");
        ORDER_STATUS_MAP.put("99", "Failed");
    }
}
