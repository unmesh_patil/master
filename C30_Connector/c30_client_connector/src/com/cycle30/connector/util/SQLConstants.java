package com.cycle30.connector.util;

public class SQLConstants {
	public static final String CLIENT_CONN_TRANS_INST = 
			" insert into c30sdk.C30_CLIENT_CONN_TRANS_INST " +
					" (request_id, ACCT_SEG_ID ,ORG_ID, ORDER_ID, request_date, " +
					"  REQUEST_XML,response_xml,response_code,CALL_BACK_URL_TYPE,  " +
					" status,transaction_id ) " +" values (?,?,?,?,sysdate,?,?,?,?,?,?)";
	
	public static final String CLIENT_CONN_TRAN_REQUEST_SEQUENCE_INFO = "select c30sdk.C30_CLIENT_CONN_REQUEST_SEQ.NEXTVAL REQUEST_ID from dual";

	public static final String CLIENT_QUERY_UPDATE_RESPONSE_TRANS_INST = "UPDATE c30sdk.C30_CLIENT_CONN_TRANS_INST SET response_date=sysdate,STATUS = ?,RESPONSE_XML =?, RESPONSE_CODE = ?  WHERE REQUEST_ID = ?";

	public static final String CLIENT_QUERY_UPDATE_TRANS_STATUS = "UPDATE c30sdk.C30_CLIENT_CONN_TRANS_INST SET STATUS = ?  WHERE REQUEST_ID = ?";

}
