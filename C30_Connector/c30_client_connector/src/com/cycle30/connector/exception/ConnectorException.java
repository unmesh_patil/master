package com.cycle30.connector.exception;


import org.apache.log4j.Logger;


/** 
 * General purpose exception handler for Cycle30 Audit logging
 * 
 *
 */
public class ConnectorException extends Exception {
    
    
    private static final Logger log = Logger.getLogger(ConnectorException.class);


    /**
     * Constructor
     * 
     * @param str String exception message.
     */
    public ConnectorException(String str, Exception e) {
        super(str);
        log.error(str, e);    
    }



    /**
     * Constructor
     * 
     * @param str String exception message.
     */
    public ConnectorException(String str) {
        super(str);
        log.error(str);
    }
    

    /**
     * Constructor
     */
    public ConnectorException(Exception e) {
        super(e);
        log.error(e);;  
    }




}