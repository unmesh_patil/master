package com.cycle30.connector.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;
import com.cycle30.connector.util.ConnectorUtil;
import com.cycle30.connector.util.SQLConstants;




/** this is to store the data in the C30_CLIENT_CONN_TRANS_INST table
 * 
 * 
 *
 */
public class C30ClientConnectorTransaction {

	 private static Logger log = Logger.getLogger(C30ClientConnectorTransaction.class);  
	 
	private Integer acctSegId;
	private String orgId;
	private String callBackUrlType;
	private String requestXml;
	private String responseXml;
	private Date requestDate;
	private Date responseDate;
	private String orderId;
	private String responseCode;
	private String status;
	private Integer requestId;
	private String transactionId;
	
	public C30ClientConnectorTransaction(){}

	public C30ClientConnectorTransaction(Integer acctSegId, String orgId,
			String callBackUrlType, String requestXml, String responseXml,
			Date requestDate, Date responseDate,
			String orderId, String responseCode,
			String status, Integer requestId, String transactionId) {
		super();
		this.acctSegId = acctSegId;
		this.orgId = orgId;
		this.callBackUrlType = callBackUrlType;
		this.requestXml = requestXml;
		this.responseXml = responseXml;
		this.requestDate = requestDate;
		this.responseDate=responseDate;
		this.orderId = orderId;
		this.responseCode = responseCode;
		this.status = status;
		this.requestId = requestId;
		this.transactionId=transactionId;
	}



	
	public Integer getAcctSegId() {
		return acctSegId;
	}

	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getCallBackUrlType() {
		return callBackUrlType;
	}

	public void setCallBackUrlType(String callBackUrlType) {
		this.callBackUrlType = callBackUrlType;
	}

	public String getRequestXml() {
		return requestXml;
	}

	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}

	public String getResponseXml() {
		return responseXml;
	}

	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	
	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("C30ClientConnectorTransaction [acctSegId=");
		builder.append(acctSegId);
		builder.append(", orgId=");
		builder.append(orgId);
		builder.append(", callBackUrlType=");
		builder.append(callBackUrlType);
		builder.append(", requestXml=");
		builder.append(requestXml);
		builder.append(", responseXml=");
		builder.append(responseXml);
		builder.append(", requestDate=");
		builder.append(requestDate);
		builder.append(", responseDate=");
		builder.append(responseDate);
		builder.append(", orderId=");
		builder.append(orderId);
		builder.append(", responseCode=");
		builder.append(responseCode);
		builder.append(", transactionId=");
		builder.append(transactionId);		
		builder.append(", status=");
		builder.append(status);
		builder.append(", requestId=");
		builder.append(requestId);
		builder.append("]");
		return builder.toString();
	}

	/** Inserts the data into the database.
	 * @throws ConnectorException 
	 * 
	 */
	public synchronized void dbProcess() throws ConnectorException 
	{
		// Store the data in the database

		log.info("Storing the data in the database with the Transaction Information");
		String query = SQLConstants.CLIENT_CONN_TRANS_INST;
		log.debug("Query : "+ query);
		PreparedStatement ps = null;
		Connection conn=null;
		
		try
		{
			
			 conn = ConnectionManager.getInstance().getConnection("c30sdk");

			ps = conn.prepareStatement(query);
			 if(!conn.getAutoCommit()){
				 conn.setAutoCommit(true);
			 }
			
			if (requestId!= null ) ps.setInt(1, requestId); else ps.setNull(1, java.sql.Types.INTEGER); 
			ps.setInt(2, acctSegId);
			ps.setString(3, orgId);
			ps.setString(4, orderId);
			ps.setString(5, requestXml);
			ps.setString(6, responseXml);
			ps.setString(7, responseCode);
			ps.setString(8,callBackUrlType);
			ps.setString(9,status);
			ps.setString(10,transactionId);
			
			int count  = ps.executeUpdate();
			conn.commit();
			  
			log.info("Row count " + count);
			
			log.debug(this.toString());
			
		}//END TRY
		catch(Exception e)
		{
			log.debug("Exception Message while Inserting the Client Request  "+ e.getMessage());
			log.error("Exception Message while Inserting the Client Request  "+ e.getMessage());
			//throw new ConnectorException(e);
		}
		finally {
			try 
			{
				if (ps != null) {
					ps.close();
				}
				ConnectionManager.getInstance().releaseConnectiontoCache("c30sdk", conn);
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}		
	}
	
	

	// Store the data in the database
	public synchronized void dbUpdateProcess() throws ConnectorException 
	{
	log.info("Storing the data in the database with the Transaction Information");
	String query = SQLConstants.CLIENT_QUERY_UPDATE_RESPONSE_TRANS_INST;
	log.debug("Query : "+ query);
	PreparedStatement ps = null;
	Connection conn=null;
	
	try
	{
		
		 conn = ConnectionManager.getInstance().getConnection("c30sdk");
		 if(!conn.getAutoCommit()){
			 conn.setAutoCommit(true);
		 }
		
		ps = conn.prepareStatement(query);
		ps.setString(1,status);
		ps.setString(2, responseXml);
		ps.setString(3, responseCode);		
		ps.setInt(4, requestId);
		
		
		int count  = ps.executeUpdate();
		conn.commit();
		
		log.debug(this.toString());
		
	}//END TRY
	catch(Exception e)
	{
		log.debug("Exception Message while Updating the Client Response  "+ e.getMessage());
		log.error("Exception Message while Updating the Client Response  "+ e.getMessage());
		//throw new ConnectorException(e);
	}
	finally {
		try 
		{
			if (ps != null) {
				ps.close();
			}
			ConnectionManager.getInstance().releaseConnectiontoCache("c30sdk", conn);
		} 
		catch (Exception ex){
			log.error("Exception: "+ex.getMessage());
		}
	}		
}
	
	
	public synchronized Integer getClientConnectorRequestId() throws ConnectorException  
	{

		log.info("Storing the data in the database with the Transaction Information");
		String query = SQLConstants.CLIENT_CONN_TRAN_REQUEST_SEQUENCE_INFO;
		log.debug("Query : "+ query);
		ResultSet resultSet  = null;
		PreparedStatement ps = null;
		Connection conn=null;
		Integer requestId = null;
		try
		{

			conn = ConnectionManager.getInstance().getConnection("c30sdk");

			ps = conn.prepareStatement(query);

			resultSet  = ps.executeQuery();

			if (resultSet.next()) 
			{
				requestId= resultSet.getInt("REQUEST_ID");
			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			log.debug("Exception Message while retriveing the Sequence for the Request_id  "+ e.getMessage());
			log.error("Exception Message while retriveing the Sequence for the Request_id  "+ e.getMessage());
			//throw new ConnectorException(e);
		}
		finally {
			try 
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {
					ps.close();
				}
				ConnectionManager.getInstance().releaseConnectiontoCache("c30sdk", conn);
			} 
			catch (Exception ex)
			{
				log.error("Exception: "+ex.getMessage());
			}
		}
		return requestId;	

	}

	
}
