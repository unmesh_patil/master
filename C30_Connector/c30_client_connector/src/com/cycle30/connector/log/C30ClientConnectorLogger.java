package com.cycle30.connector.log;



import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;





/** This class maintains the request and response logging Objects to DB
 * 
 * @author moharif
 *
 */
public class C30ClientConnectorLogger  {
	
	private C30ClientConnectorLogger(){}
	
	private static Logger log = Logger.getLogger(C30ClientConnectorLogger.class);
	private static C30ClientConnectorLogger c30clientLoggerInstance = null;
	
	public static C30ClientConnectorLogger getInstance() throws ConnectorException
	{
		if(c30clientLoggerInstance == null)
			c30clientLoggerInstance = new C30ClientConnectorLogger();
		return c30clientLoggerInstance;
	}

	
	
/** Logging the Client Request to the DB
 * 
 * @param clientConnectorprop
 * @throws ConnectorException
 */
	public synchronized void saveRequest(ClientConnectorRequestProperties clientConnectorprop) throws ConnectorException {
		
		C30ClientConnectorTransaction clientTranRequest = new C30ClientConnectorTransaction();
		clientTranRequest.setAcctSegId(clientConnectorprop.getAcctSegId());
		clientTranRequest.setOrgId(clientConnectorprop.getOrgId());
		clientTranRequest.setOrderId(clientConnectorprop.getOrderId());
		clientTranRequest.setCallBackUrlType(clientConnectorprop.getCallBackUrlType());
		clientTranRequest.setRequestId(clientConnectorprop.getRequestId());
		clientTranRequest.setRequestXml(clientConnectorprop.getRequestXml());
		clientTranRequest.setStatus(clientConnectorprop.getStatus());
		clientTranRequest.setTransactionId(clientConnectorprop.getC30TransactionId());
		
		clientTranRequest.dbProcess();
	}
	/** Logging the Client Response to the DB
	 * 
	 * @param clientConResponse
	 * @throws ConnectorException
	 */
	public synchronized void  saveResponse(ClientConnectorRequestProperties clientConResponse) throws ConnectorException {
		
		C30ClientConnectorTransaction clientTranResponse = new C30ClientConnectorTransaction();
		clientTranResponse.setStatus(clientConResponse.getStatus());
		clientTranResponse.setResponseXml(clientConResponse.getResponseXml());
		clientTranResponse.setResponseCode(clientConResponse.getResponseCode());		
		clientTranResponse.setRequestId(clientConResponse.getRequestId());
		
		clientTranResponse.dbUpdateProcess();
	}
	
}
