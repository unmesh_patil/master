package com.cycle30.connector.log;

import java.util.Date;

import org.apache.log4j.Logger;

public class ClientConnectorRequestProperties {
	
	private static Logger log = Logger.getLogger(ClientConnectorRequestProperties.class);

	private Integer acctSegId;
	private String orgId;
	private String callBackUrlType;
	private String requestXml;
	private String responseXml;
	private Date requestDate;
	private Date responseDate;
	private String orderId;
	private String responseCode;
	private String c30TransactionId;
	private String status;
	private Integer requestId;
	
	public ClientConnectorRequestProperties(){}
	
	public ClientConnectorRequestProperties(Integer acctSegId, String orgId, String callBackUrlType ){
		this.acctSegId=acctSegId;
		this.orgId=orgId;
		this.callBackUrlType=callBackUrlType;
	}

	public Integer getAcctSegId() {
		return acctSegId;
	}

	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getCallBackUrlType() {
		return callBackUrlType;
	}

	public void setCallBackUrlType(String callBackUrlType) {
		this.callBackUrlType = callBackUrlType;
	}

	public String getRequestXml() {
		return requestXml;
	}

	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}

	public String getResponseXml() {
		return responseXml;
	}

	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getC30TransactionId() {
		return c30TransactionId;
	}

	public void setC30TransactionId(String c30TransactionId) {
		this.c30TransactionId = c30TransactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	
	
	
	
	
}
