package com.cycle30.connector.service;

import org.apache.log4j.Logger;

import com.cycle30.connector.aq.QueueManager;
import com.cycle30.connector.exception.ConnectorException;


/** 
 * Driver for updating client from queued status updates.
 */
public class ClientNonOrderUpdater {


	private static final Logger log = Logger.getLogger(ClientNonOrderUpdater.class);


	/**
	 * 
	 * @param xmlMessage
	 * @throws ConnectorException
	 */
	public static void processMessage(String xmlMessage, QueueManager qm) {

		try {

			/** Actual payload look like this: 
			 *  "Payment-- <XML Message> ; delimited by "--"
			 */
			// Split the Message to obtain the Call Back Type
			String   delimiter = "--";
			String[]  payload = xmlMessage.split(delimiter);
			if(payload.length == 3 )
			{

				//String payloadwithxmlHeader =  "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+payload[2];
				String payloadwithxmlHeader = payload[2];
				String callbackType = payload[0];
				String c30AccountSegmentId = payload[1];
				/** Get the exact URL and Config for the given callback type*/

				ClientNonOrderUpdaterImpl nonOrderfcu = new ClientNonOrderUpdaterImpl(callbackType,payloadwithxmlHeader);
				nonOrderfcu.setC30AccountSegmentId(c30AccountSegmentId);
				nonOrderfcu.process();
			}else{
				new ConnectorException("Invalid Call Back Type");
			}

		} catch (ConnectorException ce) {

			// Place payload into error queue 
			qm.createErrorQueueMessage(xmlMessage);  
			log.error(ce);
		}

	}
}
