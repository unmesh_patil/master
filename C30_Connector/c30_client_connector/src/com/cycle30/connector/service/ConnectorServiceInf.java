package com.cycle30.connector.service;

import com.cycle30.connector.exception.ConnectorException;

/**
 * 
 *
 */
public interface ConnectorServiceInf {


    public void process() throws ConnectorException;
    
}
