package com.cycle30.connector.service;


import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.connector.data.ClientProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.http.ClientHttpPostHelper;
import com.cycle30.connector.log.C30ClientConnectorLogger;
import com.cycle30.connector.log.C30ClientConnectorTransaction;
import com.cycle30.connector.log.ClientConnectorRequestProperties;
import com.cycle30.connector.util.DomUtils;


/**
 * Class to implement behavior for updating client for non order detail such as Payment Log, Life Cycle file etc. 
 *
 */
public class ClientNonOrderUpdaterImpl implements ConnectorServiceInf {

    
    private String callbackType=null;
    private String orgId=null;
    private String c30AccountSegmentId = null;
	private String payload=null;
    private ClientProperties clientProperties=null;
    private Integer request_id=null;
    ClientConnectorRequestProperties clientProps=null;
    C30ClientConnectorLogger messageLogger=null;
 
    
    private static final Logger log = Logger.getLogger(ClientNonOrderUpdaterImpl.class);
    

    // Sole constructor - instantiated via ConnectorServiceFactory
    public ClientNonOrderUpdaterImpl(String callbackType, String payload) 
    {
        this.callbackType = callbackType;
        this.payload=payload;
    }
    
    
    /**
     * Process the request to update client order
     */
    public void process() throws ConnectorException {
        
        // Get client properties.  Check cache for existing, and
        // if not there, create/get new instance.
       
        /** Now get the orgId from Client Properties, should be a default OrgId
         * 
         */
    	
        clientProperties = (new ClientProperties()).getDefaultClientByCallbackType(callbackType, c30AccountSegmentId);
        if (clientProperties == null) {
           clientProperties = new ClientProperties(callbackType, c30AccountSegmentId);
           this.orgId=clientProperties.getOrgId();
         // --TODO : we should throw or handle this exception
        }else{
        	this.orgId=clientProperties.getOrgId();
        }

     
        
        //Log the Request to the DB
        this.clientProps = new ClientConnectorRequestProperties();
        this.request_id=new C30ClientConnectorTransaction().getClientConnectorRequestId();
        this.messageLogger = C30ClientConnectorLogger.getInstance();
      /**
       *  Call to Log the Request
       */
        messageLogger.saveRequest(getClientConnectorProperties());
        
        
        ClientHttpPostHelper fhc = new ClientHttpPostHelper();     
        
        try{
        
        String response = fhc.post(clientProperties, payload, callbackType );
        
        /**
         *  get the ClientProperties required for the response logging
         */
        ClientConnectorRequestProperties clientProps = getClientConnectorProperties();  
        if(response !=null ){
        clientProps.setStatus("Processed");
        }else{
        	clientProps.setStatus("Failed");
        }
        clientProps.setResponseXml(response);
        
        //Log the Response to the DB
          
        messageLogger.saveResponse(clientProps);
        //Pw-245 vijaya
        if(!("Sucessfully Posted the callback".equalsIgnoreCase(response))){
			checkUpdateError(response);
          }
                        
        } catch (ConnectorException ce) {
        	
        	clientProps.setResponseXml(ce.getMessage());
        	clientProps.setStatus("Failed");
      	  messageLogger.saveResponse(clientProps);
      	  
      	  throw new ConnectorException(ce);
        	
        }
        
    }
    
    /**
     *  Request Properties for Logging to DB, which is populated from the client request and response .
     * @return
     * @throws ConnectorException
     */
    
    public ClientConnectorRequestProperties getClientConnectorProperties() throws ConnectorException{
        
        clientProps.setAcctSegId(new Integer(c30AccountSegmentId));
        clientProps.setRequestId(request_id);
        clientProps.setOrgId(this.orgId);
        clientProps.setCallBackUrlType(callbackType);
        clientProps.setOrderId("");
        clientProps.setRequestXml(payload);
       // clientProps.setStatus("Processed");
        return clientProps;
    }


    
    /**
     * See if an error occurred during the order update.
     * 
     * @param response
     */
    private void checkUpdateError(String response) throws ConnectorException {

        Document responseDom = DomUtils.stringToDom(response);
        
        String errorCode = DomUtils.getElementValue(responseDom, "errorCode");
        if (errorCode != null && !errorCode.equals("0")) {
            String message = DomUtils.getElementValue(responseDom, "message");
            throw new ConnectorException("Error occurred when updating client order status. Error code: " + errorCode + 
                                          "  Message: " + message);        
        }

    }
    
    
    public String getCallbackType() {
 		return callbackType;
 	}


 	public void setCallbackType(String callbackType) {
 		this.callbackType = callbackType;
 	}


 	public String getOrgId() {
 		return orgId;
 	}


 	public void setOrgId(String orgId) {
 		this.orgId = orgId;
 	}


 	public String getC30AccountSegmentId() {
 		return c30AccountSegmentId;
 	}


 	public void setC30AccountSegmentId(String c30AccountSegmentId) {
 		this.c30AccountSegmentId = c30AccountSegmentId;
 	}


 	public String getPayload() {
 		return payload;
 	}


 	public void setPayload(String payload) {
 		this.payload = payload;
 	}

}
