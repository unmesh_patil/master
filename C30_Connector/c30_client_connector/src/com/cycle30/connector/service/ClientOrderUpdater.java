package com.cycle30.connector.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.cycle30.connector.aq.QueueManager;
import com.cycle30.connector.data.WorkpointJob;
import com.cycle30.connector.data.WorkpointProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;
import com.cycle30.connector.util.ConnectorConstants;
import com.cycle30.connector.util.ConnectorUtil;


/** 
 * Driver for updating client from queued status updates.
 */
public class ClientOrderUpdater {

    private static final String CLIENT_ACTION = "update";
    private static final Logger log = Logger.getLogger(ClientOrderUpdater.class);
    
    private static final String ERROR_MSG_QUERY = 
         " select log_message, log_level " +
         "   from c30_sdk_order_trans_log " +
         "  where client_order_id = ? " + 
         "    and client_sub_order_id = ? " +
         "    and transaction_id = ? " +         
         "  order by log_date asc";

    
    private static QueueManager qm = null;
    private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    private static long totalMessages;
    
    
    
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        String msg = "";

        Date date = new Date();          
        log.info("\n=======================================================\n" +
                 "   Started ClientOrderUpdater at: " + sdf.format(date) + 
                 "\n=======================================================");
        
        // Get value of SLEEP_TIME environment variable.
        String sleepVar = System.getenv("SLEEP_TIME");
        if (sleepVar == null) {
            log.error("SLEEP_TIME env variable not found!");
            System.exit(1);
        }
        long sleepTime = Long.parseLong(sleepVar);
        
        
        // Set up AQ queue receiver
        try {                
            qm = QueueManager.getInstance();
        } catch (ConnectorException e) {
            log.error(e);
            System.exit(1);
        }
        
        
        // Intercept a JVM shutdown signal (ctrl/c, kill, etc). 
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                shutdown();
            }
         });
        
        
        // Keep getting/processing messages that are queued, until no more exist.
        // When none exist, sleep for pre-determined time.
        while (true) {
        	
        	processNonOrderMessage();    
            try {          
                
                msg = qm.getNextOrderStatusQueueMessage();               

                // If no message, go to sleep...
                if (msg == null) {
                    log.info("No messages queued.  Sleeping for " + sleepTime + " milliseconds...");
                    Thread.sleep(sleepTime);
                    continue;
                }
                log.info("totalMessages " + totalMessages);
                totalMessages++;
                processMessage(msg, qm);
                           
            }   // if interrupted, close connections, etc.
                catch (InterruptedException ie) {
                log.error(ie);
                shutdown();
                
            }
            catch (ConnectorException ce) {
                log.error(ce);
            }

        } // end while(true) infinite loop

            
    }
    
    /**
     * Process the Non Order Messages that are queued up in the NON ORDER QUEUE
     */
    private static void processNonOrderMessage() {
    	
    	// Keep getting/processing messages that are queued, until no more exist.
		// When none exist, sleep for pre-determined time.
		
           try {          

			String	msg = qm.getNonOrderMessage();               

                log.info("Processing the non order queue");
				if (msg != null) {				
				log.info("totalMessages " + totalMessages);
				totalMessages++;
				ClientNonOrderUpdater.processMessage(msg, qm);
				}

			}   // if interrupted, close connections, etc.
				catch (ConnectorException ce) {
				log.error(ce);
			}
	}

	/**
     * 
     * @param xmlMessage
     * @throws ConnectorException
     */
    private static void processMessage(String xmlMessage, QueueManager qm) {
        
        try {
            
            // Parse Worpoint properties from XML message
            WorkpointProperties wp = getWorkpointProperties(xmlMessage);
            
    
            // Set up the job id. Data structure is set up to accept
            // multiple jobs in ArrayList, but this process actually 
            // processes one job at a time (from sub_order queued message)
            WorkpointJob wpJob = new WorkpointJob();
            
            String jobId = ConnectorUtil.getXmlStringElementValue(xmlMessage, "WpJobId");
            wpJob.setJobId(jobId);
            String jobStatus    = wp.getOrderStatus();
            String wpStatusCode = wp.getStatusCode();
            String wpStatusMsg  = wp.getStatusMessage();
            
            if (wpStatusCode.equals("1")) {
                jobStatus = "Failed";
            }
            wpJob.setJobStatus(jobStatus);
            
            // Set same code (0 or 1) and message as above in main WP properties
            wpJob.setCode(wpStatusCode);         
            wpJob.setJobMessage(wpStatusMsg);     
    
            // Add job details to WP properties
            wp.getJobList().add(wpJob);
            
    
            ClientOrderUpdaterImpl fcu = new ClientOrderUpdaterImpl(wp);
            fcu.process();
            
        } catch (ConnectorException ce) {
               
            // Place payload into error queue 
            qm.createErrorQueueMessage(xmlMessage);  
            log.error(ce);
        }
        
    }


    /**
     * Get/set main Workpoint properties.
     * 
     * @param xmlMessage
     * @return
     */
    private static WorkpointProperties getWorkpointProperties(String xmlMessage) 
                                              throws ConnectorException {
        
        WorkpointProperties wp = new WorkpointProperties();
        
        wp.setAction(CLIENT_ACTION);
        
        // Get basic WP job data elements from XML
        String orgId       		= ConnectorUtil.getXmlStringElementValue(xmlMessage, "OrgId");
        String orderId     		= ConnectorUtil.getXmlStringElementValue(xmlMessage, "OrderId");
        String subOrderId  		= ConnectorUtil.getXmlStringElementValue(xmlMessage, "SubOrderId");
        String statusId    		= ConnectorUtil.getXmlStringElementValue(xmlMessage, "SubOrderStatusId");
        String transactionId    = ConnectorUtil.getXmlStringElementValue(xmlMessage, "TransactionId");        

        String orderStatus = ConnectorConstants.ORDER_STATUS_MAP.get(statusId);
        if (orderStatus == null) {
            orderStatus = "Unknown";
        }
        
        wp.setOrgId(orgId);
        wp.setOrderId(orderId);
        wp.setOrderStatus(orderStatus);
        wp.setSubOrderId(subOrderId);
        wp.setTransactionId(transactionId);
        
        // Check for error code 99.  If it exists, get related message.
        String statusCode = "0";  // default to 'ok'
        String errorType = "INFO"; //default to INFO        
        String message    = orderStatus;

        if (ConnectorUtil.getXmlStringElementValue(xmlMessage, "IsCancellable") !=null && 
        		ConnectorUtil.getXmlStringElementValue(xmlMessage, "IsCancellable").equalsIgnoreCase("1"))
        	wp.setIsCancellable("Yes");
        if (ConnectorUtil.getXmlStringElementValue(xmlMessage, "IsResubmittable") !=null &&
        		ConnectorUtil.getXmlStringElementValue(xmlMessage, "IsResubmittable").equalsIgnoreCase("1"))
        	wp.setIsResubmittable("Yes");

        
        if (statusId.equals(ConnectorConstants.ORD_STATUS_IN_ERROR)) {
            statusCode = "1";
            String[] messageDetails = getErrorMessage(orderId, subOrderId, transactionId);
            message = messageDetails[0];
            errorType = messageDetails[1];
        }
        
        wp.setStatusCode(statusCode);
        wp.setStatusMessage(message);
        wp.setErrorType(errorType);

        // Create empty list
        ArrayList jobList = new ArrayList();
        wp.setJobList(jobList);
        
        return wp;
    }


    
    /**
     * Get error associated with Workpoint order/suborder
     * 
     * @param orderId
     * @param subOrderId
     * @return
     */
    private static String[] getErrorMessage(String orderId, String subOrderId, String transactionId) throws ConnectorException {   		
        
        String errorMsg = "";
        String errorType = "";        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet  = null;
        
        // Get C30SDK connection
        conn = ConnectionManager.getInstance().getConnection("c30sdk");

        try {   
            
            ps   = conn.prepareStatement(ERROR_MSG_QUERY);        
            ps.setString(1, orderId); 
            ps.setString(2, subOrderId);
            ps.setString(3, transactionId);
            
            resultSet  = ps.executeQuery();
            
            // Get the latest error (by date) associated with order/sub order
            while (resultSet.next()) {                      
                errorMsg = resultSet.getString(1);
                errorType = resultSet.getString(2);
            }
            
        } catch (SQLException se) {
                log.error("Error querying c30_sdk_order_trans_log table for order/suborder ids: " + orderId + "/" + subOrderId);
                throw new ConnectorException(se);
        } finally {
    		try 
    		{
    			if (resultSet != null) {
                    resultSet.close();
    			}
    			if (ps != null) {
    				ps.close();
    			}
    			ConnectionManager.getInstance().releaseConnectiontoCache("c30sdk", conn);
    		} 
    		catch (Exception ex){
    			log.error("Exception: "+ex.getMessage());
                throw new ConnectorException("ERROR - getErrorMessage : ", ex);    			
    		}
    	}	

        return new String[] {errorMsg, errorType};
    }


    // Do cleanup work sleep is interrupted or JVM shuts down.
    /**
     * 
     */
    private static void shutdown() {
        
        log.info("Shutdown signal encountered. Closing connections...");
        
        if (qm != null) {
            qm.closeConnections();
            qm = null;
        }
        
        ConnectionManager.getInstance().closeConnections();
            
        Date date = new Date();          
        log.info("\nEnded ClientOrderUpdater at: " + sdf.format(date));
        log.info("Total messages processed: " + totalMessages + "\n");

    }
}
