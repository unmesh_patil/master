package com.cycle30.connector.service;


import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.connector.data.ClientProperties;
import com.cycle30.connector.data.WorkpointJob;
import com.cycle30.connector.data.WorkpointProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.http.ClientHttpPostHelper;
import com.cycle30.connector.log.C30ClientConnectorLogger;
import com.cycle30.connector.log.C30ClientConnectorTransaction;
import com.cycle30.connector.log.ClientConnectorRequestProperties;
import com.cycle30.connector.util.ConnectorConstants;
import com.cycle30.connector.util.DomUtils;
import com.cycle30.connector.data.ClientOrdertExtendedData;


/**
 * Class to implement behavior for updating client order 
 *
 */
public class ClientOrderUpdaterImpl implements ConnectorServiceInf {

    
    private WorkpointProperties wpProperties;
    private ClientProperties clientProperties;
    String callbackType=ConnectorConstants.ORDER_CALL_BACK_TYPE;
    private Integer request_id=null;
    ClientConnectorRequestProperties clientProps=null;
    C30ClientConnectorLogger messageLogger=null;

    
    private static final Logger log = Logger.getLogger(ClientOrderUpdaterImpl.class);
    

    // Sole constructor - instantiated via ConnectorServiceFactory
    public ClientOrderUpdaterImpl(WorkpointProperties wpProps) {
        this.wpProperties = wpProps;
    }
    
    
    /**
     * Process the request to update client order
     */
    public void process() throws ConnectorException {
        
        // Get client properties.  Check cache for existing, and
        // if not there, create/get new instance.
        String orgId = wpProperties.getOrgId();
        
        clientProperties = ClientProperties.getExistingClient(orgId);
        if (clientProperties == null) {
           this.clientProperties = new ClientProperties(orgId);
        }

        // Create message payload for sending to client
        String payload = createOrderUpdatePayload();
        log.info("payload : "+payload);
        
        //Log the Request to the DB
        
        this.clientProps = new ClientConnectorRequestProperties();
        this.request_id=new C30ClientConnectorTransaction().getClientConnectorRequestId();
      /**
       *  Call to Log the Request
       */
        messageLogger=  C30ClientConnectorLogger.getInstance();
        messageLogger.saveRequest(getClientConnectorProperties(payload));  
        
        ClientHttpPostHelper fhc = new ClientHttpPostHelper();
        try{
        String response = fhc.post(clientProperties, payload, callbackType);
        /**
         *  get the ClientProperties required for the response logging
         */
        ClientConnectorRequestProperties clientProps = getClientConnectorProperties(payload);  
        if(response !=null ){
        clientProps.setStatus("Processed");
        }else{
        	clientProps.setStatus("Failed");
        }
        clientProps.setResponseXml(response);
        
        //Log the Response to the DB
        
        messageLogger.saveResponse(clientProps);        	

         if(!("Sucessfully Posted the callback".equalsIgnoreCase(response))){
			checkUpdateError(response);
          }
 
      } catch (ConnectorException ce) {
    	  
    	  clientProps.setResponseXml(ce.getMessage());
      	  clientProps.setStatus("Failed");    	  
    	  messageLogger.saveResponse(clientProps);
    	  
    	  throw new ConnectorException(ce);
          
     
      }
    }
    
    

    /**
     * Create client order updater HTTP POST payload from the Workpoint
     * properties.
     * 
     * @return
     */
    private String createOrderUpdatePayload() throws ConnectorException {

        StringBuffer payload = new StringBuffer();
        
        payload.append(
           "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + 
           "<request>" +
           "<Order>" + 
             "<OrderId>" + wpProperties.getOrderId()       + "</OrderId>" +
             "<Status>"  + wpProperties.getOrderStatus()   + "</Status>"  +
             "<Code>"    + wpProperties.getStatusCode()    + "</Code>"    +
             "<Message>" + wpProperties.getStatusMessage() + "</Message>"
        );
		payload.append("<ResubmitOrder>"+wpProperties.getIsResubmittable()+"</ResubmitOrder>");
		payload.append("<IsCancellable>"+wpProperties.getIsCancellable()+"</IsCancellable>"); 
        
        List jobList = wpProperties.getJobList();

        if (jobList == null) {
           throw new ConnectorException("No job list supplied in Workpoint properties.");
        }
        
        payload.append("<JobList size=\""+jobList.size()+"\">");
        
        Iterator i = jobList.iterator();
        while (i.hasNext()) {
            WorkpointJob wpJob = (WorkpointJob)i.next();
            payload.append("<Job>");
              payload.append("<JobId>"   + wpJob.getJobId()      + "</JobId>");
              payload.append("<Status>"  + wpJob.getJobStatus()  + "</Status>");
              payload.append("<Code>"    + wpJob.getCode()       + "</Code>");
              payload.append("<Message>" + wpJob.getJobMessage() + "</Message>");
            payload.append("</Job>");
        }
        
        payload.append("</JobList>");
        log.info("Get Attribute List");
        
        //Add Attribute List if there is any data to be sent back to client
        if (wpProperties.getOrderStatus().equalsIgnoreCase(ConnectorConstants.ORD_STATUS_COMPLETED_STRING)
        		|| wpProperties.getOrderStatus().equalsIgnoreCase(ConnectorConstants.ORD_STATUS_COMMITTED_STRING)
        		|| wpProperties.getOrderStatus().equalsIgnoreCase(ConnectorConstants.ORD_STATUS_FAILED_STRING)) {
            String attribResponse = new ClientOrdertExtendedData().getOrderExtendedData(wpProperties.getTransactionId().toString(), 
            		wpProperties.getSubOrderId().toString());
            if (!attribResponse.isEmpty() || attribResponse != null) {
            	payload.append(attribResponse);
            }
        }
        
        
        payload.append("</Order>");
        payload.append("</request>");
        
        return payload.toString();
    }
    
    
    /**
     * See if an error occurred during the order update.
     * 
     * @param response
     */
    private void checkUpdateError(String response) throws ConnectorException {

        Document responseDom = DomUtils.stringToDom(response);
        
        String errorCode = DomUtils.getElementValue(responseDom, "errorCode");
        if (errorCode != null && !errorCode.equals("0")) {
            String message = DomUtils.getElementValue(responseDom, "message");
            throw new ConnectorException("Error occurred when updating client order status. Error code: " + errorCode + 
                                          "  Message: " + message);        
        }

    }
    
    /**
     *  Request Properties for Logging to DB, which is populated from the client request and response .
     * @return
     * @throws ConnectorException
     */
    
    public ClientConnectorRequestProperties getClientConnectorProperties(String payload) throws ConnectorException{
        if(clientProperties.getC30SegmentationId() !=null){
        clientProps.setAcctSegId(new Integer(clientProperties.getC30SegmentationId().toString()));
        }
        clientProps.setOrgId(clientProperties.getOrgId());
        clientProps.setRequestId(request_id);
        clientProps.setCallBackUrlType(callbackType);
        if(wpProperties.getOrderId() !=null){
        clientProps.setOrderId(wpProperties.getOrderId());
        }
        if(wpProperties.getTransactionId() !=null){
        	clientProps.setC30TransactionId(wpProperties.getTransactionId().toString());
        }
        clientProps.setRequestXml(payload);
       // clientProps.setStatus("Processed");
        return clientProps;
    }

  
}
