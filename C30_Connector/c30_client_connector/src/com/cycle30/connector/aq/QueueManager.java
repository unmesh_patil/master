package com.cycle30.connector.aq;


import java.sql.Connection;
import java.sql.SQLException;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.jms.JMSException;

import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;

import oracle.AQ.AQDriverManager;
import oracle.AQ.AQOracleSession;
import oracle.AQ.AQSession;
import oracle.AQ.AQException;
import oracle.jms.AQjmsQueueConnectionFactory;
import oracle.jms.AQjmsSession;



//-----------------------------------------------------------------------------
/**
 * 
 * Singleton to manage Oracle AQ queue functionality
 *
 */
public class QueueManager {


	private static QueueReceiver orderStatusReceiverQueue;
	private static QueueSender orderStatusSenderQueue;

	private static QueueReceiver nonOrderReceiverQueue;
	private static QueueSender nonOrderSenderQueue;

	private static QueueReceiver errReceiverQueue;
	private static QueueSender errSenderQueue;

	private static AQSession orderStatusAqSession;
	private static AQSession nonOrderAqSession;
	private static Connection dbConn;
	private static Connection aqConn;
	private static Connection nonaqConn;
	private static Connection errAqConn;

	private static QueueSession orderStatusQueueSession;
	private static QueueSession nonOrderQueueSession;

	private static Queue orderStatusQueue;
	private static Queue errQueue;;
	private static Queue nonOrderQueue;

	private static QueueConnection qConn;
	private static QueueConnection nonqConn;
	private static QueueConnection errQConn;

	private final static String INPUT_QNAME_ENV_VAR ="INPUT_AQ_QUEUE_NAME"; // e.g.ORDER_STATUS_QUEUE
	private final static String QUEUE_USER_ID = "C30SDK";

	private final static String NON_ORDER_QUEUE_USER_ID = "C30SDK";


	// Error queue properties
	private static AQSession errAqSession;
	private static QueueSession errQueueSession;
	private final static String ERR_QUEUE_USER_ID = "C30SDK";
	private final static String ERR_QUEUE_NAME    = "ERROR_QUEUE";
	private final static String NON_ORDER_QUEUE_NAME    = "NON_ORDER_QUEUE";

	private static final Logger log = Logger.getLogger(QueueManager.class);

	// 
	private static QueueManager self = new QueueManager();


	//------------------------------------------------------------------
	/**
	 *  private (only) constructor
	 */
	private QueueManager() {   
	}



	//--------------------------------------------------------------------------
	/** Get singleton instance of the connection
	 */  
	public static synchronized QueueManager getInstance() throws ConnectorException {

		// Do initial AQ setup on first invocation
		initialize(); 

		return self;

	}

	/**
	 * Create a new Oracle AQ session on first call to singleton.
	 * 
	 * @throws ConnectorException
	 */
	public static void initialize() throws ConnectorException {


		// Just return if initialization already completed.
		if (dbConn != null) {
			return;
		}

		log.info("Initializing AQ manager...");

		try {

			// Get input queue name from env variable.  This allows for processing
			// the error queue as input if necessary.
			String inputQueueName = System.getenv(INPUT_QNAME_ENV_VAR);
			if (inputQueueName == null) {
				throw new ConnectorException(INPUT_QNAME_ENV_VAR + " env variable not found!");
			}

			// Get C30SDK connection
			dbConn = ConnectionManager.getInstance().getConnection("c30sdk");

			dbConn.setAutoCommit(false);

			// Load the Oracle AQ driver: 
			Class.forName("oracle.AQ.AQOracleDriver");

			// Create AQ sessions




			orderStatusAqSession    = AQDriverManager.createAQSession(dbConn);
			aqConn    = ((AQOracleSession)orderStatusAqSession).getDBConnection(); 
			qConn    = AQjmsQueueConnectionFactory.createQueueConnection(aqConn);
			qConn.start(); 
			orderStatusQueueSession    = qConn.createQueueSession(true, javax.jms.Session.CLIENT_ACKNOWLEDGE);
			orderStatusQueue    = ((AQjmsSession) orderStatusQueueSession).getQueue(QUEUE_USER_ID, inputQueueName);
			orderStatusReceiverQueue = orderStatusQueueSession.createReceiver(orderStatusQueue);
			orderStatusSenderQueue   = orderStatusQueueSession.createSender(orderStatusQueue);

			nonOrderAqSession    = AQDriverManager.createAQSession(dbConn);
			nonaqConn    = ((AQOracleSession)nonOrderAqSession).getDBConnection();   
			nonqConn    = AQjmsQueueConnectionFactory.createQueueConnection(nonaqConn);
			nonqConn.start();
			nonOrderQueueSession    = nonqConn.createQueueSession(true, javax.jms.Session.CLIENT_ACKNOWLEDGE);
			nonOrderQueue = ((AQjmsSession) nonOrderQueueSession).getQueue(NON_ORDER_QUEUE_USER_ID, NON_ORDER_QUEUE_NAME);
			nonOrderReceiverQueue = nonOrderQueueSession.createReceiver(nonOrderQueue);
			nonOrderSenderQueue=nonOrderQueueSession.createSender(nonOrderQueue);

			errAqSession = AQDriverManager.createAQSession(dbConn);
			errAqConn = ((AQOracleSession)errAqSession).getDBConnection();  
			errQConn = AQjmsQueueConnectionFactory.createQueueConnection(errAqConn);
			errQConn.start();
			errQueueSession = errQConn.createQueueSession(true, javax.jms.Session.CLIENT_ACKNOWLEDGE);
			errQueue = ((AQjmsSession) errQueueSession).getQueue(ERR_QUEUE_USER_ID, ERR_QUEUE_NAME);
			errReceiverQueue = errQueueSession.createReceiver(errQueue);
			errSenderQueue = errQueueSession.createSender(errQueue);


			log.info("Created new AQ sessions and receiver...");

		}
		catch (SQLException ex)
		{
			throw new ConnectorException("SQL error creating AQ session: ", ex);  
		}  
		catch (AQException ex)
		{
			throw new ConnectorException("Error creating AQ session: ", ex);  
		}
		catch (JMSException ex)
		{
			throw new ConnectorException("JMS error setting up AQ session: ", ex);  
		}
		catch (ClassNotFoundException ex)
		{
			throw new ConnectorException("Oracle AQ db driver not found: ", ex);  
		}


	}



	/**
	 * Get next AQ message on the queue.
	 *  
	 * @return AQ message text
	 * @throws ConnectorException
	 */
	public String getNextOrderStatusQueueMessage() throws ConnectorException {

		String xmlMessage = null;

		try   {

			Message message = (Message)orderStatusReceiverQueue.receive(10);
			if (message == null) { 
				return null;  // no message on queue
			}


			// Get message text (XML format)
			TextMessage text = (TextMessage)message;
			xmlMessage = text.getText();
			log.info("text: " + text);
			log.info("Retrieved AQ text message: " + xmlMessage);

			// Add XML header prefix to raw payload
			//xmlMessage = ConnectorConstants.XML_HEADER + msg;

			// commit to physically remove from queue
			orderStatusQueueSession.commit();
		} catch (JMSException ex) {
			ex.printStackTrace();
			resetOrderstatusQueueConn();
			throw new ConnectorException("JMS exception retrieving AQ message: ", ex);     

		}  

		return xmlMessage;
	}

	/** Resets the order status Queue to handle more connections..
	 * @throws JMSException 
	 * 
	 */
	private void resetOrderstatusQueueConn() {

		try
		{
			orderStatusQueueSession.close();
			qConn.stop();

			orderStatusAqSession    = AQDriverManager.createAQSession(dbConn);
			aqConn    = ((AQOracleSession)orderStatusAqSession).getDBConnection(); 
			qConn    = AQjmsQueueConnectionFactory.createQueueConnection(aqConn);
			qConn.start(); 			
			orderStatusQueueSession    = qConn.createQueueSession(true, javax.jms.Session.CLIENT_ACKNOWLEDGE);
			orderStatusQueue    = ((AQjmsSession) orderStatusQueueSession).getQueue(QUEUE_USER_ID, "ORDER_STATUS_QUEUE");
			orderStatusReceiverQueue = orderStatusQueueSession.createReceiver(orderStatusQueue);
			orderStatusSenderQueue   = orderStatusQueueSession.createSender(orderStatusQueue);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}


	}



	/**
	 * Get next AQ message on the queue.
	 *  
	 * @return AQ message text
	 * @throws ConnectorException
	 */
	public String getNonOrderMessage() throws ConnectorException {

		String xmlMessage = null;

		try   {

			Message message = (Message)nonOrderReceiverQueue.receive(10);
			if (message == null) { 
				return null;  // no message on queue
			}


			// Get message text (XML format)
			TextMessage text = (TextMessage)message;
			xmlMessage = text.getText();
			log.info("text: " + text);
			log.info("Retrieved AQ text message: " + xmlMessage);

			// Add XML header prefix to raw payload
			//xmlMessage = ConnectorConstants.XML_HEADER + msg;

			log.info("Before Commit");
			// commit to physically remove from queue
			nonOrderQueueSession.commit();
			log.info("After Commit");

		} catch (JMSException ex) {
			throw new ConnectorException("JMS exception retrieving AQ message: ", ex);     
		}  

		return xmlMessage;
	}

	/**
	 * 
	 * @param msg
	 */
	public void createErrorQueueMessage(String msg) {

		try {

			log.info("Requeuing message in error queue.");
			TextMessage tm = errQueueSession.createTextMessage();
			tm.setText(msg);
			errSenderQueue.send(tm);
			errQueueSession.commit();

		} catch (JMSException je) {
			log.error(je);
		}
	}


	/**
	 * 
	 * @param msg
	 */
	public void createNonOrderSenderQueueMessage(String msg) {

		try {

			log.info("Create NonOrder Sender Queue Message");
			TextMessage tm = nonOrderQueueSession.createTextMessage();
			tm.setText(msg);
			nonOrderSenderQueue.send(tm);
			nonOrderQueueSession.commit();

		} catch (JMSException je) {
			log.error(je);
		}
	}

	/**
	 * 
	 */
	public void closeConnections() {
		try {
			orderStatusQueueSession.close();
			aqConn.close();
			orderStatusAqSession.close();

			errQueueSession.close();
			errAqConn.close();
			errAqSession.close();

		} catch (JMSException e) {
			log.error(e);
		} catch (SQLException se) {
			log.error(se);
		}
	}


}