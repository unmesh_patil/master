package com.cycle30.connector.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;
import com.cycle30.connector.util.ConnectorUtil;


/** Class for getting order extended data 
 * @author Umesh
 *
 */
public class ClientOrdertExtendedData  {


    private String key;
    private String value;
    

    private static final Logger log = Logger.getLogger(ClientOrdertExtendedData.class);
    
    private static final String C30_SDK_USERID="c30sdk";
    
    private static final String CLIENT_ORDER_EXT_DATA_QUERY_SQL = 
            "select key, value " +
               "from c30_sdk_sub_order_ext_data " +
             " where transaction_id = ? and client_sub_order_id = ? and upper(send_flag) = 'TRUE'";
    
    public ClientOrdertExtendedData() {

    }


    /**
     *   Sole constructor.  Accepts transactionId, suborderId and fetch 
     *   order extended data from C30 database and build Attribute List payload.
     */
    public ClientOrdertExtendedData(String transactionId, String suborderId) throws ConnectorException {
    	getOrderExtendedData(transactionId, suborderId);
    }
    
    
    /**
     * 
     * @param transactionId
     * @param suborderId
     * 
     */
    public String getOrderExtendedData(String transactionId, String suborderId) throws ConnectorException {
        
        Connection conn=null;
        PreparedStatement ps = null;
        ResultSet resultSet  = null;

        try {
            conn = ConnectionManager.getInstance().getConnection(C30_SDK_USERID);            
            ps = conn.prepareStatement(CLIENT_ORDER_EXT_DATA_QUERY_SQL);
            ps.setString(1, transactionId);
            ps.setString(2, suborderId);
            
            resultSet  = ps.executeQuery();
            int totalRows = 0;
            StringBuffer attribPayload = new StringBuffer();
            StringBuffer attribPayloadBody = new StringBuffer();
            while (resultSet.next()) {
            	attribPayloadBody.append("<Attribute>");
                // Populate order extended data 
            	key    = resultSet.getString(1);
            	value  = resultSet.getString(2);
            	attribPayloadBody.append("<Key>" + key + "</Key>");
            	attribPayloadBody.append("<Value>" + value + "</Value>");
            	attribPayloadBody.append("</Attribute>"); 
            	totalRows++;
            }
            //Build the attribute list only if the data is available
            if (totalRows > 0) {
                attribPayload.append("<AttributeList size=\""+totalRows+"\">");
                attribPayload.append(attribPayloadBody);
                attribPayload.append("</AttributeList>");
            }
            
            return attribPayload.toString();
            
        } catch (SQLException e) {
            log.error(e);
            throw new ConnectorException("ERROR - Build Order Extended Data Reponse Payload : ", e);   
        } finally {
    		try 
    		{
    			if (resultSet != null) {
                    resultSet.close();
    			}
    			if (ps != null) {
    				ps.close();
    			}
    			ConnectionManager.getInstance().releaseConnectiontoCache(C30_SDK_USERID, conn);
    		} 
    		catch (Exception ex){
    			log.error("Exception: "+ex.getMessage());
                throw new ConnectorException("ERROR - Build Order Extended Data Reponse Payload : ", ex);    			
    		}
    	}		
    }
    
}
