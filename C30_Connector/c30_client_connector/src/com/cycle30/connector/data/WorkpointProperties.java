package com.cycle30.connector.data;

import java.util.List;

/** Class to contain Workpoint related properties */
public class WorkpointProperties {



    private String action; //  action to take - 'update', etc. from Workpoint
    
    private String orderId;
    private String suborderId;
    private String transactionId;
    private String orderStatus;
    private String statusCode;
    private String statusMessage;
    private String errorType;    
    private String orgId;
    private String customerId;
    private String isCancellable="No";
    private String isResubmittable="No";
    private List<WorkpointJob> jobList;
    
    /**
	 * @return the isCancellable
	 */
	public String getIsCancellable() {
		return isCancellable;
	}
	/**
	 * @param isCancellable the isCancellable to set
	 */
	public void setIsCancellable(String isCancellable) {
		this.isCancellable = isCancellable;
	}
	/**
	 * @return the isResubmittable
	 */
	public String getIsResubmittable() {
		return isResubmittable;
	}
	/**
	 * @param isResubmittable the isResubmittable to set
	 */
	public void setIsResubmittable(String isResubmittable) {
		this.isResubmittable = isResubmittable;
	}
	
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }
    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    /**
     * @return the errorType
     */
    public String getErrorType() {
        return errorType;
    }
    /**
     * @param errorType the errorType to set
     */
    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }
    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }
    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }    
    /**
     * @return the suborderId
     */
    public String getSubOrderId() {
        return suborderId;
    }
    /**
     * @param suborderId the suborderId to set
     */
    public void setSubOrderId(String suborderId) {
        this.suborderId = suborderId;
    }    
    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }
    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
    /**
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }
    /**
     * @param statusMessage the statusMessage to set
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }
    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
    /**
     * @return the customerId
     */
    public String geCustomerId() {
        return customerId;
    }
    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    /**
     * @return the jobList
     */
    public List getJobList() {
        return jobList;
    }
    /**
     * @param jobList the jobList to set
     */
    public void setJobList(List jobList) {
        this.jobList = jobList;
    }


    
}
