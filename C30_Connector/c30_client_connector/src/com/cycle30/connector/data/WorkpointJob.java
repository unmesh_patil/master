package com.cycle30.connector.data;

/**
 * 
 * Workpoint job properties
 *
 */
public class WorkpointJob {

    private String jobId;
    private String jobStatus;
    private String code;
    private String jobMessage;
    
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    
    /**
     * @return the jobId
     */
    public String getJobId() {
        return jobId;
    }
    /**
     * @param jobId the jobId to set
     */
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
    /**
     * @return the jobStatus
     */
    public String getJobStatus() {
        return jobStatus;
    }
    /**
     * @param jobStatus the jobStatus to set
     */
    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }
    /**
     * @return the jobMessage
     */
    public String getJobMessage() {
        return jobMessage;
    }
    /**
     * @param jobMessage the jobMessage to set
     */
    public void setJobMessage(String jobMessage) {
        this.jobMessage = jobMessage;
    }

}
