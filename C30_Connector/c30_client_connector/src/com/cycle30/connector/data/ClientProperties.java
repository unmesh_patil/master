package com.cycle30.connector.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;
import com.cycle30.connector.util.ConnectorConstants;
import com.cycle30.connector.util.ConnectorUtil;


/** Class for encapsulating client properties 
 *
 */
public class ClientProperties  {

	/* An OrgId, can only belong to one AccountSegmentId, 
	 * but one account segmentid can have more than one OrgId.*/
	private String orgId;

	private String c30SegmentationId;
	private String username;
	private String password;
	private String clientOrgToken;
	private String clientSecret;
	private String clientConsumerKey;
	private Date dateAdded;
	private Date dateChanged;
	private String changedBy;
	private String isDefault;




	/* This is callback URL Types */
	private  ArrayList<CallbackUrlType> callbackUrlList = new ArrayList();



	public ArrayList<CallbackUrlType> getCallbackUrlList() {
		return callbackUrlList;
	}


	public void setCallbackUrlList(ArrayList<CallbackUrlType> callbackUrlList) {
		this.callbackUrlList = callbackUrlList;
	}


	private static final Logger log = Logger.getLogger(ClientProperties.class);

	private static final String C30_API_USERID="c30api";
	private static final String IS_DEFAULT_YES="1";


	private static final String CLIENT_QUERY_SQL = 
			"select client.c30_segmentation_id, client.username, client.password, "+
					" client.org_token, client.consumer_secret, client.consumer_key,client_config.is_default,client_config.call_back_type, "+
					" client_config.call_back_url,client_config.env_name "+
					" from client client, client_call_back_config client_config  "+
					" where client.client_id =client_config.client_id  " +
					" and client.c30_segmentation_id = client_config.c30_segmentation_id "+
					" and client.client_id=?  " +
					" and client_config.call_back_type='Order'";
	
	private static final String CLIENT_NON_ORDER_QUERY_SQL = 
			"select client.c30_segmentation_id, client.username, client.password, "+
					" client.org_token, client.consumer_secret, client.consumer_key,client_config.is_default,client_config.call_back_type, "+
					" client_config.call_back_url,client_config.env_name,client.client_id "+
					" from client client, client_call_back_config client_config  "+
					" where client.client_id =client_config.client_id  " +
					" and client.c30_segmentation_id = client_config.c30_segmentation_id "+
					" and client_config.call_back_type=?  "+
					" and client_config.is_default=1  "+
					" and client_config.c30_segmentation_id=?  ";



	// Maintain cache of clients so a database lookup isn't required each time
	public static HashMap<String, ClientProperties>clientCache = new HashMap<String, ClientProperties>();
	public static HashMap<String, ArrayList<ClientProperties>> clientCacheByAccountSegment = new HashMap<String, ArrayList<ClientProperties>>();


	public ClientProperties(){}

	/**
	 *   Sole constructor.  Accepts client org id, and populates with
	 *   client properties from C30 database.
	 */
	public ClientProperties(String orgId) throws ConnectorException {
		getClientProperties(orgId,null,null);
	}

	
	public ClientProperties(String callbackType, String acctSegmentId) throws ConnectorException {
		getClientProperties(null,callbackType,acctSegmentId);
	}

	/**
	 *  Check static cache for existing properties, in some case we dont need AccountSegmentId.
	 *  
	 * @param orgId
	 * @return client properties
	 */
	public static ClientProperties getExistingClient(String orgId) {
		ClientProperties fcp = clientCache.get(orgId);
		return fcp;
	}



	/**
	 *  Check static cache for existing properties 
	 *  
	 * @param orgId
	 * @return client properties
	 * @throws ConnectorException 
	 */
	public  ClientProperties getDefaultClientByCallbackType( String callbackType, String acctSegmentId) throws ConnectorException 
	{
		String orgId = null;
		if(clientCacheByAccountSegment.get(acctSegmentId) !=null ){
		for (ClientProperties clientProp : clientCacheByAccountSegment.get(acctSegmentId))
		{
			if (clientProp.getIsDefault() != null && clientProp.getIsDefault().equalsIgnoreCase(IS_DEFAULT_YES)  )
			{
				orgId = clientProp.getOrgId();
				break;
			}else{
				continue;
			}
		}
		if(orgId==null){
			
			throw new ConnectorException("Unable to find the Default OrgId for the given Segmentaion id ");
		}
		ClientProperties fcp = clientCache.get(orgId+callbackType);
		this.orgId=orgId;
		return fcp;
	}
		else{
			return null;
		}
		
	}

	/**
	 *  Check Callback URL by Callback Type 
	 *  
	 * @param orgId
	 * @return client properties
	 */
	public String getUrlByCallbackType( String orgId, String callbackType) 
	{
		String url = null;
		ClientProperties fcp=null;
		
			if (ConnectorConstants.ORDER_CALL_BACK_TYPE.equalsIgnoreCase(callbackType)) {
				fcp = clientCache.get(orgId);
			} else {
				fcp = clientCache.get(orgId+callbackType);
			}

		for (CallbackUrlType callbackurlType  : fcp.getCallbackUrlList())
		{
			if (callbackurlType.getCallbackType() != null && callbackurlType.getCallbackType().equalsIgnoreCase(callbackType) )
			{
				url = callbackurlType.getCallbackUrl();
				return url;
			}
		}
		return url;
	}

	//---------------------------------
	//  Getters/setters
	//---------------------------------

	/**
	 * @return the OrgId
	 */
	public String getOrgId() {
		return orgId;
	}
	/**
	 * @param OrgId the orgId to set
	 */
	public void setC30ClientId(String orgId) {
		this.orgId = orgId;
	}
	/**
	 * @return the c30SegmentationId
	 */
	public String getC30SegmentationId() {
		return c30SegmentationId;
	}
	/**
	 * @param c30SegmentationId the c30SegmentationId to set
	 */
	public void setC30SegmentationId(String c30SegmentationId) {
		this.c30SegmentationId = c30SegmentationId;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	public String getIsDefault() {
		return isDefault;
	}


	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the Password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the clientOrgToken
	 */
	public String getClientOrgToken() {
		return clientOrgToken;
	}
	/**
	 * @param clientOrgToken the clientOrgToken to set
	 */
	public void setClientOrgToken(String clientOrgToken) {
		this.clientOrgToken = clientOrgToken;
	}
	/**
	 * @return the dateAdded
	 */
	public Date getDateAdded() {
		return dateAdded;
	}
	/**
	 * @param dateAdded the dateAdded to set
	 */
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
	/**
	 * @return the dateChanged
	 */
	public Date getDateChanged() {
		return dateChanged;
	}
	/**
	 * @param dateChanged the dateChanged to set
	 */
	public void setDateChanged(Date dateChanged) {
		this.dateChanged = dateChanged;
	}
	/**
	 * @return the changedBy
	 */
	public String getChangedBy() {
		return changedBy;
	}
	/**
	 * @param changedBy the changedBy to set
	 */
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}



	/**
	 * @return the clientSecret
	 */
	public String getClientSecret() {
		return clientSecret;
	}
	/**
	 * @param clientSecret the clientSecret to set
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	/**
	 * @return the clientConsumerKey
	 */
	public String getClientConsumerKey() {
		return clientConsumerKey;
	}
	/**
	 * @param clientConsumerKey the clientConsumerKey to set
	 */
	public void setClientConsumerKey(String clientConsumerKey) {
		this.clientConsumerKey = clientConsumerKey;
	}   





	//    public static void main(String[] args) {
	//        try {
	//         ClientProperties cp = new ClientProperties("ACSCF1");
	//        } catch (Exception e) {
	//            e.printStackTrace();
	//        }
	//    }

	/**
	 * 
	 * @param orgId
	 */
	private void getClientProperties(String clientOrgId, String callbackType, String acctSegmentId) throws ConnectorException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {

			conn = ConnectionManager.getInstance().getConnection(C30_API_USERID);
			
			if(clientOrgId !=null ){
			ps = conn.prepareStatement(CLIENT_QUERY_SQL);
			ps.setString(1, clientOrgId);
			log.info("Getting client properties using org id: " + clientOrgId);
			}else{
				ps = conn.prepareStatement(CLIENT_NON_ORDER_QUERY_SQL);
				ps.setString(1, callbackType);	
				ps.setString(2, acctSegmentId);
				log.info("Getting client properties using Segmentation Id   and Callback type : " + acctSegmentId + "--" + callbackType);
			}

			
			resultSet = ps.executeQuery();
			int totalRows = 0;
			
			while (resultSet.next()) {// should be only 1 result

				// Populate client properties
				if(clientOrgId !=null){
					orgId = clientOrgId;
				}else{
					orgId = resultSet.getString(11);
				}
				c30SegmentationId 	= resultSet.getString(1);
				username          	= resultSet.getString(2);
				password          	= resultSet.getString(3);
				clientOrgToken    	= resultSet.getString(4);
				clientSecret      	= resultSet.getString(5);
				clientConsumerKey	= resultSet.getString(6);
				isDefault =  resultSet.getString(7);
				
				
				CallbackUrlType cbType = new CallbackUrlType();
				cbType.setCallbackType( resultSet.getString(8));
				cbType.setCallbackUrl( resultSet.getString(9));
				cbType.setEnvName( resultSet.getString(10));
				callbackUrlList.add(cbType);
				// Decrypt the  password, and replace the client properties value.
				if (password != null && password.trim().length()>0) {
					String pwd = ConnectorUtil.decryptPassword(password);
					password = pwd;
				}
				
				if(clientOrgId !=null && (clientCache.get(clientOrgId)==null) ){
					clientCache.put(orgId, this);
				
				}else if(clientOrgId==null){
					clientCache.put(orgId+callbackType, this);
					
					// Add this to client Cache by AccountSeg.
					if (clientCacheByAccountSegment.get(c30SegmentationId) != null)
					{
						(clientCacheByAccountSegment.get(c30SegmentationId)).add(this);		
					
					}
					else
					{
						ArrayList<ClientProperties> clientPropList = new ArrayList();
						clientPropList.add(this);
						clientCacheByAccountSegment.put(c30SegmentationId, clientPropList);
					}

				}
				log.info("Retrieved properties... segid: " + c30SegmentationId + " username: " + username);

				totalRows++;
			}
			
			// Should have found only 1 result
			if (totalRows != 1) {
				String error = "Found " + totalRows +" result rows (instead of 1) for client id " + orgId + " in CLIENT table.";
				log.error(error);
				throw new ConnectorException(error);
			}

		} catch (SQLException e) {
			log.error(e);
			throw new ConnectorException("Error client properties from CLIENT table", e);   
		} finally {
    		try 
    		{
    			if (resultSet != null) {
                    resultSet.close();
    			}
    			if (ps != null) {
    				ps.close();
    			}
    			ConnectionManager.getInstance().releaseConnectiontoCache(C30_API_USERID, conn);
    		} 
    		catch (Exception ex){
    			log.error("Exception: "+ex.getMessage());
                throw new ConnectorException("ERROR - getClientProperties : ", ex);    			
    		}
    	}
	}


	/** This is a combination of the Callback URL  and Type based on the EnvName
	 * 
	 * @author rnelluri
	 *
	 */
	class CallbackUrlType
	{
		private String callbackUrl;
		private String callbackType;
		private String envName;

		public String getCallbackUrl() {
			return callbackUrl;
		}
		public void setCallbackUrl(String callbackUrl) {
			this.callbackUrl = callbackUrl;
		}
		public String getCallbackType() {
			return callbackType;
		}
		public void setCallbackType(String callbackType) {
			this.callbackType = callbackType;
		}

		public String getEnvName() {
			return envName;
		}
		public void setEnvName(String envName) {
			this.envName = envName;
		}

	}


}
