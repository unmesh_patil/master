
Client updater notes
--------------------

1. Deployed under 'muleapi' user ./client-updater directory:
   
    ./lib contains app-specific jars (log4j.props.jar, c30_client_connector.jar, aqapi.jar)
    ./bin/client-updater.sh used to start the process (run at the end of fxwebctl.sh)
    ./logs contains daily-rolling log files. Logging defined in log4j.props.jar (log4j.xml file)
             
2. Oracle AQ used for queueing messages via trigger on C30SDK.C30_SDK_SUB_ORDER table: 
   
   queue name: ORDER_STATUS_QUEUE     queue table: ORDER_STATUS_QTABLE
   queue name: ORDER_STATUS_ERR_QUEUE queue table: ORDER_STATUS_ERR_QTABLE
   
   Error queue used to store original payload when error occurs.
   
3. SQL scripts:
	(arbor)  022_AQ_grants_to_user_c30sdk.sql
	(c30sdk) 220_create_queues_ORDER_STATUS.sql
	(c30sdk) 224_create_trigger_ORDER_STATUS.sql
    
4. The updater runs as a daemon job, started by the ./bin/client-upater.sh script,
   which in turn is initially run at the end of ./bin/fxwebctl.sh.  The script
   kills any current client-updater processes, then restarts the daemon.
   
   The script can be run standalone if/when necessary via simple command line invocation:
       $ ./bin/client-updater.sh &
	   
   The processes (parent/child) associated with the update daemon can be determined by:
       $ ps -ef | grep 'client-updater' | grep -v grep | awk '{print $2}'
	   
   and stopped manually via:
       $ ps -ef | grep 'client-updater' | grep -v grep | awk '{print $2}' | xargs kill 2>/dev/null
	   
   -->  It's important to note that the processes should be stopped using 'kill' vs 'kill -9'.
   -->  This is so the JVM can be sent a terminate signal, which the Java daemon needs to 
   -->  intercept and close various connections.

	
Error queue recovery steps (client-updater.sh)
----------------------------------------------
   client-updater.sh script has an env variable 'INPUT_AQ_QUEUE_NAME' that contains the
   name of the AQ input queue name (e.g. 'ORDER_STATUS_QUEUE').  To process messages in 
   error queue, simply run the script 'process-error-queue.sh' that has the env 
   variable set to the error queue.
   
   !! Note !! The problem MUST be resolved before doing this, since the messages will 
              simply be put back on the error queue and processed infinitely.   

