#!/bin/bash

#--------------------------------------------------------------------------------
#  Script for updating Force orgs with Kenan order status changes that
#  have been sent to the Oracle AQ error-queue. This script is identical to
#  the force-updater.sh script, except that it simply has a different input
#  queue name value for INPUT_AQ_QUEUE_NAME
#--------------------------------------------------------------------------------

MULEHOME=/opt/app/kenanfx2/muleapi
UPDHOME=$MULEHOME/client-updater
#JAVABIN=/opt/jdk1.6.0_18/bin
JAVABIN=/opt/jdk1.8.0_92/bin


#-- compress previous day archived log file.
cd $UPDHOME/logs
dt=$(date --date="yesterday" "+%Y-%m-%d")
if [ -f client-updater.log.$dt ]
then
    gzip client-updater.log.$dt
fi

export INPUT_AQ_QUEUE_NAME=ORDER_STATUS_ERR_QUEUE

cd /opt/app/kenanfx2/muleapi/bin

export CP="$UPDHOME/lib/c30_client_connector.jar:$UPDHOME/lib/log4j.props.jar:$UPDHOME/lib/aqapi.jar:$UPDHOME/lib/log4j.props.jar"
CP="$CP:$UPDHOME/lib/log4j-1.2.16.jar:$UPDHOME/lib/commons-lang-2.4.jar:$UPDHOME/lib/ojdbc6.jar"
CP="$CP:$UPDHOME/lib/jasypt-1.6.jar:$UPDHOME/lib/geronimo-jms_1.1_spec-1.1-osgi.jar:$UPDHOME/lib/commons-codec-1.3-osgi.jar:$UPDHOME/lib/geronimo-jta_1.1_spec-1.1.1.jar"

#-- for HTTP support
CP="$CP:$UPDHOME/lib/httpcore-4.1.2.jar:$UPDHOME/lib/httpclient-4.1.2.jar"

#-- for logging support
CP="$CP:$UPDHOME/lib/mule-module-logging-3.3.1.jar"


# - Update Client status
$JAVABIN/java -cp $CP com.cycle30.connector.service.ClientOrderUpdater
