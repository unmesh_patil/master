#!/bin/bash

#--------------------------------------------------------------------------------
#  Script for updating client web service(s) with Kenan order status changes.
#  This process runs as a daemon...
#--------------------------------------------------------------------------------


UPDHOME=/opt/app/kenanfx2/muleapi/client-updater
#JAVABIN=/opt/jdk1.6.0_18/bin
JAVABIN=/opt/jdk1.8.0_92/bin

#-- compress previous day archived log file.
cd $UPDHOME/logs
dt=$(date --date="yesterday" "+%Y-%m-%d")
if [ -f client-updater.log.$dt ]
then
    gzip client-updater.log.$dt
fi

export INPUT_AQ_QUEUE_NAME=ORDER_STATUS_QUEUE

cd /opt/app/kenanfx2/muleapi/bin


#-- Refer all the Mule Jars from the lib folder under client-updater

export CP="$UPDHOME/lib/c30_client_connector.jar:$UPDHOME/lib/log4j.props.jar:$UPDHOME/lib/aqapi.jar:$UPDHOME/lib/log4j.props.jar"
CP="$CP:$UPDHOME/lib/log4j-1.2.16.jar:$UPDHOME/lib/commons-lang-2.4.jar:$UPDHOME/lib/ojdbc6.jar"
CP="$CP:$UPDHOME/lib/jasypt-1.6.jar:$UPDHOME/lib/geronimo-jms_1.1_spec-1.1-osgi.jar:$UPDHOME/lib/commons-codec-1.3-osgi.jar:$UPDHOME/lib/geronimo-jta_1.1_spec-1.1.1.jar"

#-- for HTTP support
CP="$CP:$UPDHOME/lib/httpcore-4.1.2.jar:$UPDHOME/lib/httpclient-4.1.2.jar"

#-- for logging support
CP="$CP:$UPDHOME/lib/mule-module-logging-3.3.1.jar"

# Sleep time in milliseconds when no messages exist in the queue.
export SLEEP_TIME=10000

# - wait a few seconds before starting again.
sleep 5

# - Start the client order status (runs as a daemon) 
# -Xdebug -Xrunjdwp:transport=dt_socket,address=6666,server=y,suspend=n
$JAVABIN/java -cp $CP com.cycle30.connector.service.ClientOrderUpdater