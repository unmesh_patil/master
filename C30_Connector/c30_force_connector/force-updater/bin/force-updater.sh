#!/bin/bash

#--------------------------------------------------------------------------------
#  Script for updating Force orgs with Kenan order status changes.
#--------------------------------------------------------------------------------

MULEHOME=/opt/app/kenanfx2/muleapi
UPDHOME=$MULEHOME/force-updater
JAVABIN=/opt/jdk1.6.0_18/bin


#-- compress previous day archived log file.
cd $UPDHOME/logs
dt=$(date --date="yesterday" "+%Y-%m-%d")
if [ -f force-updater.log.$dt ]
then
    gzip force-updater.log.$dt
fi

export INPUT_AQ_QUEUE_NAME=FORCE_STATUS_QUEUE

cd /opt/app/kenanfx2/muleapi/bin

export CP="$MULEHOME/lib/boot/log4j-1.2.14.jar:$MULEHOME/lib/opt/commons-lang-2.4.jar:$MULEHOME/apps/WebEnablement/lib/ojdbc6.jar"
CP="$CP:$MULEHOME/apps/WebEnablement/lib/jasypt-1.6.jar:$MULEHOME/lib/opt/geronimo-jms_1.1_spec-1.1-osgi.jar"
CP="$CP:$MULEHOME/lib/opt/commons-codec-1.3-osgi.jar:$MULEHOME/lib/opt/geronimo-jta_1.1_spec-1.1.1.jar"
CP="$CP:$UPDHOME/lib/c30_force_connector.jar:$UPDHOME/lib/log4j.props.jar:$UPDHOME/lib/aqapi.jar:$UPDHOME/lib/log4j.props.jar"

#-- for HTTP support
CP="$CP:$UPDHOME/lib/httpcore-4.1.2.jar:$UPDHOME/lib/httpclient-4.1.2.jar"

#-- for logging support
CP="$CP:$MULEHOME/lib/boot/mule-module-logging-3.2.0.jar"


# - Update Force status
$JAVABIN/java -cp $CP com.cycle30.connector.service.ForceOrderUpdater
