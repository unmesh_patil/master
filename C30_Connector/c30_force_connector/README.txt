
Force updater notes
-------------------

1. Deployed under 'muleapi' user ./force-updater directory:
   
    ./lib contains app-specific jars (log4j.props.jar, c30_force_connector.jar, aqapi.jar)
    ./bin/force-updater.sh executed via cron task
    ./logs contains daily-rolling log files. Logging defined in log4j.props.jar (log4j.xml file)
             
2. Oracle AQ used for queueing messages via trigger on C30SDK.C30_SDK_SUB_ORDER table: 
   
   queue name: ORDER_STATUS_QUEUE     queue table: FORCE_STATUS_QTBL
   queue name: ORDER_STATUS_ERR_QUEUE queue table: FORCE_STATUS_ERR_QTBL
   
   Error queue used to store original payload when error occurs.
   
3. SQL scripts:
	(arbor)  022_AQ_grants_to_user_c30sdk.sql
	(c30sdk) 220_create_ORDER_STATUS_queues.sql
	(c30sdk) 225_create_ORDER_STATUS_trigger.sql
    
3. Error queue recovery steps (force-updater.sh)

   force-updater.sh script has an env variable 'INPUT_AQ_QUEUE_NAME' that contains the
   name of the AQ input queue name (e.g. 'FORCE_STATUS_QTBL').  To process messages in 
   error queue, simply run the script 'process-error-queue.sh' that has the env 
   variable set to the error queue.
   
   !! Note !! The problem MUST be resolved before doing this, since the messages will 
              simply be put back on the error queue and processed infinitely.   

