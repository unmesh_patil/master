package com.cycle30.connector.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;


import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;


/** 
 * Generic connection manager singeleton for database.
 * 
 */
public class ConnectionManager {
    
    private static final Logger log = Logger.getLogger(ConnectionManager.class);
   

    // Instantiate singleton when class is loaded by JVM
    private static final ConnectionManager self = new ConnectionManager(); 

    private static String dbUrl;   
    private static String dbUser;
    private static String dbDriver;
    private static String password;

    
    // Map for cached connections.
    HashMap<String, Connection> connectionCache = new HashMap<String, Connection>();
    
    /**
     * @return the dbUrl
     */
    public String getDbUrl() {
        return dbUrl;
    }
    /**
     * @param dbUrl the dbUrl to set
     */
    public void setDbUrl(String dbUrl) {
        ConnectionManager.dbUrl = dbUrl;
    }
    /**
     * @return the dbUser
     */
    public String getDbUser() {
        return dbUser;
    }
    /**
     * @param dbUser the dbUser to set
     */
    public void setDbUser(String dbUser) {
        ConnectionManager.dbUser = dbUser;
    }
    /**
     * @return the dbDriver
     */
    public String getDbDriver() {
        return dbDriver;
    }
    /**
     * @param dbDriver the dbDriver to set
     */
    public void setDbDriver(String dbDriver) {
        ConnectionManager.dbDriver = dbDriver;
    }
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        ConnectionManager.password = password;
    }



    
    //-------------------------------------------------------------------------
    /** Private singleton constructor. 
     */
    private ConnectionManager() {
        super();
    }
    
   
    //-------------------------------------------------------------------------
    /**
     * Return instance of singleton class
     * 
     * @return ConnectionManager instance.
     */
    public synchronized static ConnectionManager getInstance() {
        return self;
    }
    
    
    
    /**
     * 
     * @return Connection
     * @throws AuditException
     */
    public Connection getConnection(String userid) throws ConnectorException  {
        
        
        // See if existing db connection already exists.
        Connection conn = connectionCache.get(userid);
        if (conn != null) {
            return conn;
        }      

        // Get/cache db connection
        conn = getDbConnection(userid);
        connectionCache.put(userid, conn);
        
        return conn;
        
    }
    

	
    
    /**
     * 
     * Get fresh db connection.
     * 
     * @param userKey - key to the .properties file (eg, 'c30api', 'c30sdk')
     * @return Connection
     * @throws AuditException
     */
    private Connection getDbConnection(String userKey) throws ConnectorException  {
               
        Connection conn = null;    
     
        try {
            
            
            // Get/set db properties
            String dbDriver = ConnectorUtil.getConnectorProperty(userKey + ".driver");
            String dbUrl    = ConnectorUtil.getConnectorProperty(userKey + ".url");
            String dbUser   = ConnectorUtil.getConnectorProperty(userKey + ".username");
            String password = ConnectorUtil.getConnectorProperty(userKey + ".password");
            
            Class.forName(dbDriver);
            String pwd = ConnectorUtil.decryptPassword(password);
            
            log.info("Getting new Oracle connection using url/user: " + dbUrl + "/" + dbUser);
            conn = DriverManager.getConnection(dbUrl, dbUser, pwd);
            
            conn.setAutoCommit(true);
         
            
        } catch (SQLException e) {            
            throw new ConnectorException(e);
        } catch (ClassNotFoundException cnfe) {
            throw new ConnectorException(cnfe);
        } catch (Exception e) {
            throw new ConnectorException(e);
        }
        
        return conn;
    }
    
    


    
    /**
     * Close all cached connections.
     */
    public void closeConnections() {
        
        try {
            Iterator i = connectionCache.keySet().iterator();
            while (i.hasNext()) {
                String key = (String)i.next();
                Connection c = connectionCache.get(key);
                c.close();
            }
        }
        catch (Exception e)  {}  // shouldn't happen, but nothing we do if so
    }
    
    
    


}
