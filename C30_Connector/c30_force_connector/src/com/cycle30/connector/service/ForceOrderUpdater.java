package com.cycle30.connector.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.connector.aq.QueueManager;
import com.cycle30.connector.data.WorkpointJob;
import com.cycle30.connector.data.WorkpointProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;
import com.cycle30.connector.util.ConnectorConstants;
import com.cycle30.connector.util.ConnectorUtil;


/** 
 * Driver for updating Force from queued status updates.
 */
public class ForceOrderUpdater {

    private static final String FORCE_ACTION = "update";
    private static final Logger log = Logger.getLogger(ForceOrderUpdater.class);
    
    private static final String ERROR_MSG_QUERY = 
         " select log_message " +
         "   from c30_sdk_order_trans_log " +
         "  where client_order_id = ? " + 
         "    and client_sub_order_id = ? " +
         "  order by log_date asc";


    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        
        QueueManager qm = null;
        int totalMessages = 0;
        String msg = "";
        
        
        Date date = new Date();          
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        log.info("\n=======================================================\n" +
                 "   Started ForceOrderUpdater at: " + sdf.format(date) + 
                 "\n=======================================================");
        
        // Set up AQ queue receiver
        try {
                    
            qm = QueueManager.getInstance();
            
        } catch (ConnectorException e) {
            log.error(e);
        }
        
        // Keep getting/processing messages that are queued, until no more exist
        while (msg != null) {

            try {
               msg = qm.getMessage();               
            } 
            catch (ConnectorException ce) {
                log.error(ce);
            }
            
            // If no message, end processing.
            if (msg == null) {
                continue;
            }
            
            totalMessages++;
            
            processMessage(msg, qm);
        }
        
        // Close connections.
        qm.closeConnections();
        ConnectionManager.getInstance().closeConnections();
        
        date = new Date();          
        log.info("\nEnded ForceOrderUpdater at: " + sdf.format(date));
        log.info("Total messages processed: " + totalMessages + "\n");
        
        System.exit(0);
            
    }
    
        

    
    /**
     * 
     * @param xmlMessage
     * @throws ConnectorException
     */
    private static void processMessage(String xmlMessage, QueueManager qm) {
        
        try {
            
            // Parse Worpoint properties from XML message
            WorkpointProperties wp = getWorkpointProperties(xmlMessage);
            
    
            // Set up the job id. Data structure is set up to accept
            // multiple jobs in ArrayList, but this process actually 
            // processes one job at a time (from sub_order queued message)
            WorkpointJob wpJob = new WorkpointJob();
            
            String jobId = ConnectorUtil.getXmlStringElementValue(xmlMessage, "WpJobId");
            wpJob.setJobId(jobId);
            String jobStatus    = wp.getOrderStatus();
            String wpStatusCode = wp.getStatusCode();
            String wpStatusMsg  = wp.getStatusMessage();
            
            if (wpStatusCode.equals("1")) {
                jobStatus = "Failed";
            }
            wpJob.setJobStatus(jobStatus);
            
            // Set same code (0 or 1) and message as above in main WP properties
            wpJob.setCode(wpStatusCode);         
            wpJob.setJobMessage(wpStatusMsg);     
    
            // Add job details to WP properties
            wp.getJobList().add(wpJob);
            
    
            ForceOrderUpdaterImpl fcu = new ForceOrderUpdaterImpl(wp);
            fcu.process();
            
        } catch (ConnectorException ce) {
            
            // TODO - implement more robust error handling!
            
            // Place payload into error queue 
            qm.createErrorQueueMessage(xmlMessage);
            
            log.error(ce);
        }
        
    }


    /**
     * Get/set main Workpoint properties.
     * 
     * @param xmlMessage
     * @return
     */
    private static WorkpointProperties getWorkpointProperties(String xmlMessage) 
                                              throws ConnectorException {
        
        WorkpointProperties wp = new WorkpointProperties();
        
        wp.setAction(FORCE_ACTION);
        
        // Get basic WP job data elements from XML
        String orgId       = ConnectorUtil.getXmlStringElementValue(xmlMessage, "ForceOrgId");
        String orderId     = ConnectorUtil.getXmlStringElementValue(xmlMessage, "ForceOrderId");
        String subOrderId  = ConnectorUtil.getXmlStringElementValue(xmlMessage, "ForceSubOrderId");
        String statusId    = ConnectorUtil.getXmlStringElementValue(xmlMessage, "SubOrderStatusId");

        String orderStatus = ConnectorConstants.ORDER_STATUS_MAP.get(statusId);
        if (orderStatus == null) {
            orderStatus = "Unknown";
        }
        
        wp.setForceOrgId(orgId);
        wp.setOrderId(orderId);
        wp.setOrderStatus(orderStatus);
        
        // Check for error code 99.  If it exists, get related message.
        String statusCode = "0";  // default to 'ok'
        String message    = orderStatus;
        
        if (statusId.equals(ConnectorConstants.ORD_STATUS_IN_ERROR)) {
            statusCode = "1";
            message = getErrorMessage(orderId, subOrderId);
        }
        
        wp.setStatusCode(statusCode);
        wp.setStatusMessage(message);

        // Create empty list
        ArrayList jobList = new ArrayList();
        wp.setJobList(jobList);
        
        return wp;
    }


    
    /**
     * Get error associated with Workpoint order/suborder
     * 
     * @param orderId
     * @param subOrderId
     * @return
     */
    private static String getErrorMessage(String orderId, String subOrderId) throws ConnectorException {
        
        String errorMsg = "";
        
        // Get C30SDK connection
        Connection conn = ConnectionManager.getInstance().getConnection("c30sdk");
        
        PreparedStatement ps = null;

        try {   
            
            ps   = conn.prepareStatement(ERROR_MSG_QUERY);        
            ps.setString(1, orderId); 
            ps.setString(2, subOrderId);
            
            ResultSet resultSet  = ps.executeQuery();
            
            // Get the latest error (by date) associated with order/sub order
            while (resultSet.next()) {                      
                errorMsg = resultSet.getString(1);
            }
        
        } catch (SQLException se) {
                log.error("Error querying c30_sdk_order_trans_log table for order/suborder ids: " + orderId + "/" + subOrderId);
                throw new ConnectorException(se);
        }

        return errorMsg;
        
    }


}
