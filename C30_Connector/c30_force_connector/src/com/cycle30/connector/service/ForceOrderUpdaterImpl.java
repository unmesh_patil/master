package com.cycle30.connector.service;


import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.connector.data.ForceClientProperties;
import com.cycle30.connector.data.WorkpointJob;
import com.cycle30.connector.data.WorkpointProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.http.ForceHttpPostClient;
import com.cycle30.connector.util.DomUtils;


/**
 * Class to implement behavior for updating Force.com order 
 *
 */
public class ForceOrderUpdaterImpl implements ConnectorServiceInf {

    
    private WorkpointProperties wpProperties;
    private ForceClientProperties clientProperties;
    
    private static final Logger log = Logger.getLogger(ForceOrderUpdaterImpl.class);
    
    

    // Sole constructor - instantiated via ConnectorServiceFactory
    public ForceOrderUpdaterImpl(WorkpointProperties wpProps) {
        this.wpProperties = wpProps;
    }
    
    
    
    /**
     * Process the request to update Force.com order
     */
    public void process() throws ConnectorException {
        
        // Get Force.com client properties.  Check cache for existing, and
        // if not there, create/get new instance.
        String orgId = wpProperties.getForceOrgId();
        clientProperties = ForceClientProperties.getExistingClient(orgId);
        if (clientProperties == null) {
           clientProperties = new ForceClientProperties(orgId);
        }
        
        // Create message payload for sending to Force.com
        String payload = createForceOrderUpdatePayload();
        
        ForceHttpPostClient fhc = new ForceHttpPostClient();
        String response = fhc.post(clientProperties, payload);
        
        checkUpdateError(response);
        
    }
    
    

    /**
     * Create Force order updater HTTP POST payload from the Workpoint
     * properties.
     * 
     * @return
     */
    private String createForceOrderUpdatePayload() throws ConnectorException {
        
        StringBuffer payload = new StringBuffer();
        payload.append(
           "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + 
           "<request>" +
           "<Order>" + 
             "<OrderId>" + wpProperties.getOrderId()       + "</OrderId>" +
             "<Status>"  + wpProperties.getOrderStatus()   + "</Status>"  +
             "<Code>"    + wpProperties.getStatusCode()    + "</Code>"    +
             "<Message>" + wpProperties.getStatusMessage() + "</Message>"
        );
        
        payload.append("<Jobs>");
                
        List jobList = wpProperties.getJobList();
        if (jobList == null) {
           throw new ConnectorException("No job list supplied in Workpoint properties.");
        }
        
        Iterator i = jobList.iterator();
        while (i.hasNext()) {
            WorkpointJob wpJob = (WorkpointJob)i.next();
            payload.append("<Job>");
              payload.append("<JobId>"   + wpJob.getJobId()      + "</JobId>");
              payload.append("<Status>"  + wpJob.getJobStatus()  + "</Status>");
              payload.append("<Code>"    + wpJob.getCode()       + "</Code>");
              payload.append("<Message>" + wpJob.getJobMessage() + "</Message>");
            payload.append("</Job>");
        }
        
        payload.append("</Jobs></Order>");
        payload.append("</request>");
        
        return payload.toString();
    }
    
    
    /**
     * See if an error occurred during the order update.
     * 
     * @param response
     */
    private void checkUpdateError(String response) throws ConnectorException {

        Document responseDom = DomUtils.stringToDom(response);
        
        String errorCode = DomUtils.getElementValue(responseDom, "errorCode");
        if (errorCode != null && !errorCode.equals("0")) {
            String message = DomUtils.getElementValue(responseDom, "message");
            throw new ConnectorException("Error occurred when updating Force order status. Error code: " + errorCode + 
                                          "  Message: " + message);        
        }

    }
}
