package com.cycle30.connector.service;

import org.apache.log4j.Logger;

import com.cycle30.connector.data.WorkpointProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectorConstants;


/**
 * 
 * Connector service factory.
 *
 */
public class ConnectorServiceFactory {



    // Instantiate singleton
    private static final ConnectorServiceFactory self = new ConnectorServiceFactory();
    
    private static final Logger log = Logger.getLogger(ConnectorServiceFactory.class);
    

   
    
    //-------------------------------------------------------------------------
    /** Private constructor. */
    private ConnectorServiceFactory() {
        super();
    }
    
    
   
    //-------------------------------------------------------------------------
    /**
     * Return instance of singleton class
     * 
     * @return ServiceFactory instance.
     */
    public static ConnectorServiceFactory getInstance() {
        return self;
    }
    
    
    
    //-------------------------------------------------------------------------
    // Instantiate correct connector service
    /**
     * @param wpProps
     * @return service instance
     */
    public ConnectorServiceInf getServiceImpl(WorkpointProperties wpProps) throws ConnectorException  {
        
        String orderId     = wpProps.getOrderId();
        String action      = wpProps.getAction();
        String statusMsg   = wpProps.getStatusMessage();
        String clientId    = wpProps.getForceOrgId();
        
        
        // Return the correct service type based on the action
        if (action.equalsIgnoreCase("update")) {
           return new ForceOrderUpdaterImpl(wpProps);
        }

        
        // If not found
        String error = "Unable to instantiate correct service type based on client " + 
                       clientId + "/" + action + "/" + statusMsg;
        log.error(error);

        throw new ConnectorException (error);
        }
}
