package com.cycle30.connector.data;

import java.util.List;

/** Class to contain Workpoint related properties */
public class WorkpointProperties {



    private String action; // Force action to take - 'update', etc. from Workpoint
    
    private String orderId;
    private String orderStatus;
    private String statusCode;
    private String statusMessage;
    private String forceOrgId;
    private String forceCustomerId;
    private List<WorkpointJob> jobList;
    

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }
    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }
    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }
    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
    /**
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }
    /**
     * @param statusMessage the statusMessage to set
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
    /**
     * @return the forceOrgId
     */
    public String getForceOrgId() {
        return forceOrgId;
    }
    /**
     * @param forceOrgId the forceOrgId to set
     */
    public void setForceOrgId(String forceOrgId) {
        this.forceOrgId = forceOrgId;
    }
    /**
     * @return the forceCustomerId
     */
    public String getForceCustomerId() {
        return forceCustomerId;
    }
    /**
     * @param forceCustomerId the forceCustomerId to set
     */
    public void setForceCustomerId(String forceCustomerId) {
        this.forceCustomerId = forceCustomerId;
    }
    /**
     * @return the jobList
     */
    public List getJobList() {
        return jobList;
    }
    /**
     * @param jobList the jobList to set
     */
    public void setJobList(List jobList) {
        this.jobList = jobList;
    }


    
}
