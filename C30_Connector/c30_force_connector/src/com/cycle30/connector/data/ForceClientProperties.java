package com.cycle30.connector.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;
import com.cycle30.connector.util.ConnectorUtil;


/** Class for encapsulating Force client properties 
 *
 */
public class ForceClientProperties  {


    private String forceOrgId;
    private String c30SegmentationId;
    private String forceUsername;
    private String forcePassword;
    private String forceOrgToken;
    private String forceConsumerSecret;
    private String forceConsumerKey;
    private Date dateAdded;
    private Date dateChanged;
    private String changedBy;
    private String callbackUrl;
    

    private static final Logger log = Logger.getLogger(ForceClientProperties.class);
    
    private static final String C30_API_USERID="c30api";
    
    private static final String FORCE_CLIENT_QUERY_SQL = 
        "select c30_segmentation_id, username, password," +
               " org_token, consumer_secret, consumer_key, status_callback_url " + 
          "from client " +
        " where client_id = ?";
    
    
    // Maintain cache of clients so a database lookup isn't required each time
    public static HashMap<String, ForceClientProperties>clientCache = new HashMap<String, ForceClientProperties>();
    
    
    
    
    /**
     *   Sole constructor.  Accepts Force org id, and populates with
     *   client properties from C30 database.
     */
    public ForceClientProperties(String orgId) throws ConnectorException {
        getClientProperties(orgId);
        clientCache.put(orgId, this);
    }
    
    
    /**
     *  Check static cache for existing properties 
     *  
     * @param orgId
     * @return client properties
     */
    public static ForceClientProperties getExistingClient(String orgId) {
        ForceClientProperties fcp = clientCache.get(orgId);
        return fcp;
    }
    
    
    
    //---------------------------------
    //  Getters/setters
    //---------------------------------

    /**
     * @return the forceOrgId
     */
    public String getForceOrgId() {
        return forceOrgId;
    }
    /**
     * @param forceOrgId the forceOrgId to set
     */
    public void setC30ClientId(String forceOrgId) {
        this.forceOrgId = forceOrgId;
    }
    /**
     * @return the c30SegmentationId
     */
    public String getC30SegmentationId() {
        return c30SegmentationId;
    }
    /**
     * @param c30SegmentationId the c30SegmentationId to set
     */
    public void setC30SegmentationId(String c30SegmentationId) {
        this.c30SegmentationId = c30SegmentationId;
    }
    /**
     * @return the forceUsername
     */
    public String getForceUsername() {
        return forceUsername;
    }
    /**
     * @param forceUsername the forceUsername to set
     */
    public void setForceUsername(String forceUsername) {
        this.forceUsername = forceUsername;
    }
    /**
     * @return the forcePassword
     */
    public String getForcePassword() {
        return forcePassword;
    }
    /**
     * @param forcePassword the forcePassword to set
     */
    public void setForcePassword(String forcePassword) {
        this.forcePassword = forcePassword;
    }
    /**
     * @return the forceOrgToken
     */
    public String getForceOrgToken() {
        return forceOrgToken;
    }
    /**
     * @param forceOrgToken the forceOrgToken to set
     */
    public void setForceOrgToken(String forceOrgToken) {
        this.forceOrgToken = forceOrgToken;
    }
    /**
     * @return the forceClientSecret
     */
    public String getForceClientSecret() {
        return forceConsumerSecret;
    }
    /**
     * @param forceClientSecret the forceClientSecret to set
     */
    public void setForceClientSecret(String forceClientSecret) {
        this.forceConsumerSecret = forceClientSecret;
    }
    /**
     * @return the forceConsumerKey
     */
    public String getForceConsumerKey() {
        return forceConsumerKey;
    }
    /**
     * @param forceConsumerKey the forceConsumerKey to set
     */
    public void setForceConsumerKey(String forceConsumerKey) {
        this.forceConsumerKey = forceConsumerKey;
    }
    /**
     * @return the dateAdded
     */
    public Date getDateAdded() {
        return dateAdded;
    }
    /**
     * @param dateAdded the dateAdded to set
     */
    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }
    /**
     * @return the dateChanged
     */
    public Date getDateChanged() {
        return dateChanged;
    }
    /**
     * @param dateChanged the dateChanged to set
     */
    public void setDateChanged(Date dateChanged) {
        this.dateChanged = dateChanged;
    }
    /**
     * @return the changedBy
     */
    public String getChangedBy() {
        return changedBy;
    }
    /**
     * @param changedBy the changedBy to set
     */
    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }
    
    /**
     * @return the callbackUrl
     */
    public String getCallbackUrl() {
        return callbackUrl;
    }

    /**
     * @param callbackUrl the callbackUrl to set
     */
    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }


    /**
     * 
     * @param orgId
     */
    private void getClientProperties(String orgId) throws ConnectorException {
        
        try {
            
            Connection conn = ConnectionManager.getInstance().getConnection(C30_API_USERID);
            
            PreparedStatement ps = null;
            ps = conn.prepareStatement(FORCE_CLIENT_QUERY_SQL);
            ps.setString(1, orgId);
            
            log.info("Getting Force client properties using Force org id: " + orgId);
            ResultSet resultSet  = ps.executeQuery();
            int totalRows = 0;
            
            while (resultSet.next()) {// should be only 1 result
            
                // Populate client properties
                forceOrgId = orgId;
                c30SegmentationId   = resultSet.getString(1);
                forceUsername       = resultSet.getString(2);
                forcePassword       = resultSet.getString(3);
                forceOrgToken       = resultSet.getString(4);
                forceConsumerSecret = resultSet.getString(5);
                forceConsumerKey    = resultSet.getString(6);
                callbackUrl         = resultSet.getString(7);
                totalRows++;
            }
            resultSet.close();
            ps.close();
            
            log.info("Retrieved Force properties... segid: " + c30SegmentationId + " username: " + forceUsername);
            
            // Should have found only 1 result
            if (totalRows != 1) {
                String error = "Found " + totalRows +" result rows (instead of 1) for client id " + orgId + " in FORCE_CLIENT table.";
                log.error(error);
                throw new ConnectorException(error);
            }
            
            // Decrypt the force password, and replace the client properties value.
            log.info("Decrypting Force password: " + forcePassword);
            String password = ConnectorUtil.decryptPassword(forcePassword);
            forcePassword = password;
        
        } catch (SQLException e) {
            log.error(e);
            throw new ConnectorException("Error Force client properties from FORCE_CLIENT table", e);   
        } 
    }
    
    
    
    
    
}
