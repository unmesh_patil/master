package com.cycle30.connector.aq;


import java.sql.Connection;
import java.sql.SQLException;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.jms.JMSException;

import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectionManager;

import oracle.AQ.AQDriverManager;
import oracle.AQ.AQOracleSession;
import oracle.AQ.AQSession;
import oracle.AQ.AQException;
import oracle.jms.AQjmsQueueConnectionFactory;
import oracle.jms.AQjmsSession;



//-----------------------------------------------------------------------------
/**
 * 
 * Singleton to manage Oracle AQ queue functionality
 *
 */
public class QueueManager {

    
    private static QueueReceiver receiverQueue;
    private static QueueSender senderQueue;
    
    private static AQSession aqSession;
    private static Connection dbConn;
    private static Connection aqConn;
    private static Connection errAqConn;
    private static QueueSession queueSession;
    private static Queue queue;
    private static Queue errQueue;
    
    private final static String INPUT_QNAME_ENV_VAR = "INPUT_AQ_QUEUE_NAME"; // e.g. 'ORDER_STATUS_QUEUE'
    private final static String QUEUE_USER_ID = "C30SDK";

    
    // Error queue properties
    private static AQSession errAqSession;
    private static QueueSession errQueueSession;
    private final static String ERR_QUEUE_USER_ID = "C30SDK";
    private final static String ERR_QUEUE_NAME    = "FORCE_STATUS_ERR_QUEUE";
    
    private static final Logger log = Logger.getLogger(QueueManager.class);

    // 
    private static QueueManager self = new QueueManager();
    

    //------------------------------------------------------------------
    /**
     *  private (only) constructor
     */
    private QueueManager() {   
    }
    
    
    
    //--------------------------------------------------------------------------
    /** Get singleton instance of the connection
     */  
    public static synchronized QueueManager getInstance() throws ConnectorException {
        
        // Do initial AQ setup on first invocation
        initialize(); 
        
        return self;

    }

    /**
     * Create a new Oracle AQ session on first call to singleton.
     * 
     * @throws ConnectorException
     */
    public static void initialize() throws ConnectorException {


        // Just return if initialization already completed.
        if (dbConn != null) {
            return;
        }
        
        log.info("Initializing AQ manager...");
        
        try {
               
            // Get input queue name from env variable.  This allows for processing
            // the error queue as input if necessary.
            String inputQueueName = System.getenv(INPUT_QNAME_ENV_VAR);
            if (inputQueueName == null) {
                throw new ConnectorException(INPUT_QNAME_ENV_VAR + " env variable not found!");
            }
            
            // Get C30SDK connection
           dbConn = ConnectionManager.getInstance().getConnection("c30sdk");
          
           dbConn.setAutoCommit(false);
                  
           // Load the Oracle AQ driver: 
           Class.forName("oracle.AQ.AQOracleDriver");

           // Create AQ sessions
           aqSession    = AQDriverManager.createAQSession(dbConn);
           errAqSession = AQDriverManager.createAQSession(dbConn);
          
           // Set up the queue connection/sessions
           aqConn    = ((AQOracleSession)aqSession).getDBConnection();   
           errAqConn = ((AQOracleSession)errAqSession).getDBConnection();  
           
           QueueConnection qConn    = AQjmsQueueConnectionFactory.createQueueConnection(aqConn);
           QueueConnection errQConn = AQjmsQueueConnectionFactory.createQueueConnection(errAqConn);
           qConn.start();  // start message flow
          
           queueSession    = qConn.createQueueSession(true, javax.jms.Session.CLIENT_ACKNOWLEDGE);
           errQueueSession = errQConn.createQueueSession(true, javax.jms.Session.CLIENT_ACKNOWLEDGE);
           
           queue    = ((AQjmsSession) queueSession).getQueue(QUEUE_USER_ID, inputQueueName);
           errQueue = ((AQjmsSession) errQueueSession).getQueue(ERR_QUEUE_USER_ID, ERR_QUEUE_NAME);
           
           // Create receiver (incoming) and sender (outgoing) queues           
           receiverQueue = queueSession.createReceiver(queue);
           senderQueue   = queueSession.createSender(errQueue);
           
           log.info("Created new AQ sessions and receiver...");
          
       }
       catch (SQLException ex)
       {
          throw new ConnectorException("SQL error creating AQ session: ", ex);  
       }  
       catch (AQException ex)
       {
          throw new ConnectorException("Error creating AQ session: ", ex);  
       }
       catch (JMSException ex)
       {
          throw new ConnectorException("JMS error setting up AQ session: ", ex);  
       }
       catch (ClassNotFoundException ex)
       {
          throw new ConnectorException("Oracle AQ db driver not found: ", ex);  
       }

       
    }
    
    

    /**
     * Get next AQ message on the queue.
     *  
     * @return AQ message text
     * @throws ConnectorException
     */
    public String getMessage() throws ConnectorException {
        
        String xmlMessage = null;

        try   {
    
           Message message = (Message)receiverQueue.receive(10);
           if (message == null) { 
               return null;  // no message on queue
           }


           // Get message text (XML format)
           TextMessage text = (TextMessage)message;
           xmlMessage = text.getText();

           log.info("Retrieved AQ text message: " + xmlMessage);
           
           // Add XML header prefix to raw payload
           //xmlMessage = ConnectorConstants.XML_HEADER + msg;
           
           // commit to physically remove from queue
           queueSession.commit();
           
          
        } catch (JMSException ex) {
           throw new ConnectorException("JMS exception retrieving AQ message: ", ex);     
        }  
       
        return xmlMessage;
    }
    
    
    
    /**
     * 
     * @param msg
     */
    public void createErrorQueueMessage(String msg) {
        
        try {
            
            log.info("Requeuing message in error queue.");
            TextMessage tm = queueSession.createTextMessage();
            tm.setText(msg);
            senderQueue.send(tm);
            errQueueSession.commit();
        
        } catch (JMSException je) {
            log.error(je);
        }
    }
    
    
    /**
     * 
     */
    public void closeConnections() {
        try {
            queueSession.close();
            aqConn.close();
            aqSession.close();
            
            errQueueSession.close();
            errAqConn.close();
            errAqSession.close();
            
        } catch (JMSException e) {
            log.error(e);
        } catch (SQLException se) {
            log.error(se);
        }
    }

    
}