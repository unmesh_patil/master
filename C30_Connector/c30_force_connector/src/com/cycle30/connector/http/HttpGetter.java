package com.cycle30.connector.http;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.cycle30.connector.exception.ConnectorException;



/** 
 * 
 * General purpose class for sending HTTP GET messages
 *
 */
public class HttpGetter {

    
    private static final Logger log = Logger.getLogger(HttpGetter.class);
    
    /**
     * 
     * @param url
     * @param headers
     * @return response
     */
    public static synchronized String get(String url, HashMap<String, String> headers) throws ConnectorException  {
        
        String getResponse = null;
        
        HttpGet get = new HttpGet(url);
        
        // get/set headers
        Iterator i = headers.keySet().iterator();
        while (i.hasNext()) {
            String key   = (String)i.next();
            String value = (String)headers.get(key);
            get.setHeader(key, value);  
        }
        
        try {
     
            // get the response entity value.
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse resp = client.execute(get);
            
            HttpEntity respEntity = resp.getEntity();
            InputStream is = respEntity.getContent();
            
            
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb  = new StringBuilder();    
            String line;
            while ((line = br.readLine()) != null) {
               sb.append(line);
            } 
            
            getResponse = sb.toString();
            log.info("Got Force GET response: " + getResponse);
        
        } catch (Exception e) {
             throw new ConnectorException (e);
        }
        

        return getResponse;
        
    }
}
