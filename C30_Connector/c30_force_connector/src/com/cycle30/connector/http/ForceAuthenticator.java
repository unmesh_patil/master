package com.cycle30.connector.http;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.connector.data.ForceClientProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectorUtil;
import com.cycle30.connector.util.DomUtils;

/**
 * 
 * Class for authenticating against Force.com to receive oAuth token
 *
 */
public class ForceAuthenticator {

    
    private static final Logger log = Logger.getLogger(ForceAuthenticator.class);
    
    // Keep list of oAuth responses
    private static HashMap<String,String> oAuthResponses = new HashMap<String, String>();
    
    
    
    /**
     * Return oAuth response in form of a set of Map properties.  An oAuth response will
     * be returned/stored in following format:
     * 
     * <?xml version="1.0" encoding="utf-8"?>
     * <OAuth>
     *   <id>https://login.salesforce.com/id/00DU0000000J354MAC/005U0000000dLKuIAM</id>
     *   <issued_at>1319834530276</issued_at>
     *   <instance_url>https://na12.salesforce.com</instance_url>
     *   <signature>vjTdp/SalmcA+fSIPw9f8Ej+Bq90FftITIa3OpP+EI=</signature>
     *   <access_token>00DU0000000J354!AR0AQDiE36egAd78xrFdO9MbQwS9MaZlPVn8HMCvFzGvGIN1fJNUx0ZnYziMVm.Fczp7HHaTwLBYiYkmkyN1Qax7mRidzYkv</access_token>
     * </OAuth>
     * 
     * 
     * Authentication errors will be returned with error/description:
     * 
     * <?xml version="1.0" encoding="utf-8"?>
     * <OAuth>
     *   <error>invalid_client</error>
     *   <error_description>invalid client credentials</error_description>
     * </OAuth>
     * 
     * 
     * @param orgId
     * @return map of oAuth response properties
     */
    public static HashMap<String, String> getOauthResponse(String orgId) throws ConnectorException {       
        
        String authResponse = oAuthResponses.get(orgId);  
        
        if (authResponse == null) {
            return null;
        }      
        
        // The response is in XML format.  Convert to DOM.
        Document dom = DomUtils.stringToDom(authResponse);
        
        // Check for authentication error.
        String error = DomUtils.getElementValue(dom, "error");
        if (error != null) {
            String errorDescr = DomUtils.getElementValue(dom, "error_description");
            throw new ConnectorException("Error authenticating in Force. Error: " + error + ". Description: " + errorDescr);
        }
        
        // Then extract all properties and place in HashMap
        HashMap<String, String> responseProps = new HashMap<String, String>();
        
        responseProps.put("id",           DomUtils.getElementValue(dom, "id"));
        responseProps.put("issued_at",    DomUtils.getElementValue(dom, "issued_at"));
        responseProps.put("instance_url", DomUtils.getElementValue(dom, "instance_url"));
        responseProps.put("signature",    DomUtils.getElementValue(dom, "signature"));
        responseProps.put("access_token", DomUtils.getElementValue(dom, "access_token"));
        
        return responseProps;
    }
    
    
    /**
     * 
     * @param orgId
     * @param authResponse
     */
    public static void setOauthResponse(String orgId, String authResponse) {       
        
        oAuthResponses.put(orgId, authResponse);        
        
    }
    
    
    /**
     * '
     * @param clientProperties
     * @return Map of client properties
     */
    public static synchronized HashMap<String, String> authenticate(ForceClientProperties clientProperties) throws ConnectorException {
       
        String authPayload = buildAuthPayload(clientProperties);
        
        String oAuthResponse = postAuthenticateRequest(authPayload);
        
        // Cache the entire response
        String orgId = clientProperties.getForceOrgId();
        setOauthResponse(orgId, oAuthResponse);
        
        // Return map of oAuth properties
        return getOauthResponse(orgId);
        
    }    
    
    
    /**
     * 
     * @param clientProperties
     * @param currentToken
     */
    public static synchronized void refreshToken(ForceClientProperties clientProperties, 
                                                     String currentToken) throws ConnectorException {

        String refreshPayload = buildRefreshPayload(clientProperties, currentToken);
        
        String refreshResponse = postAuthenticateRequest(refreshPayload);
  
        log.info("Refresh token response: " + refreshResponse);
  
    }
    
    
    
    private static synchronized String postAuthenticateRequest(String authPayload)  throws ConnectorException {
        
        String url = ConnectorUtil.getConnectorProperty("force.auth.url");
        HashMap<String, String> authHeaders = new HashMap<String, String>();
        
        authHeaders.put("Accept", "application/xml");
        authHeaders.put("Content-Type", "application/x-www-form-urlencoded");
        
        
        String response = HttpPoster.post(url, authPayload, authHeaders);
        
        return response;
    }
    
    
    /**
     * 
     * @param clientProperties
     * @return
     */
    private static synchronized String buildAuthPayload(ForceClientProperties clientProperties) {
        
        StringBuffer sb = new StringBuffer();
        
        String userName     = clientProperties.getForceUsername();
        String clientSecret = clientProperties.getForceClientSecret();
        String password     = clientProperties.getForcePassword();
        String orgToken     = clientProperties.getForceOrgToken();
        String clientId     = clientProperties.getForceConsumerKey();
        
        sb.append("username=" + userName + "&");
        sb.append("client_secret=" + clientSecret + "&");
        
        // password is the Force ord password + org token for 'autonamous' oAuth
        sb.append("password="   + password + orgToken + "&");
        sb.append("grant_type=" + "password" + "&");
        sb.append("client_id="  + clientId);
        
        String payload = sb.toString();
        
        return payload;
    }
    
    
    /**
     * 
     * @param clientProperties
     * @return
     */
    private static synchronized String buildRefreshPayload(ForceClientProperties clientProperties,
                                                               String currentToken) {
        
        StringBuffer sb = new StringBuffer();
        ;
        String clientSecret = clientProperties.getForceClientSecret();
        String clientId     = clientProperties.getForceConsumerKey();
        
        sb.append("grant_type="    + "refresh_token" + "&");
        sb.append("client_id="     + clientId + "&");
        sb.append("client_secret=" + clientSecret + "&");
        sb.append("refresh_token=" + currentToken);
        
        String payload = sb.toString();
        
        return payload;
    }

    
    
    
    /**
     * 
     * @param forceResponse
     * @return true if session expired
     */
    public static synchronized boolean sessionExpired(String forceResponse) throws ConnectorException {

         Document responseDom = DomUtils.stringToDom(forceResponse);
         
         String errorMessage = DomUtils.getElementValue(responseDom, "message");
         if (errorMessage != null && errorMessage.startsWith("Session expired")) {
             return true;
         }
         
         return false;
    }
    
}
