package com.cycle30.connector.http;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.connector.data.ForceClientProperties;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.connector.util.ConnectorConstants;
import com.cycle30.connector.util.ConnectorUtil;
import com.cycle30.connector.util.DomUtils;


/**
 * 
 * Class for authenticating, and invoking Force REST web service via HTTP POST 
 *
 */
public class ForceHttpPostClient {

    private static final Logger log = Logger.getLogger(ForceHttpPostClient.class);
    
    /**
     * 
     * @param xmlPayload
     */
    public String post(ForceClientProperties clientProperties, String xmlPayload)
                                          throws ConnectorException 
    {
        String postResponse = null;
        HashMap<String, String>oAuthProps = new HashMap<String, String>();
        
        
        // Get existing oAuth response properties. If it doesn't exist, need to authenticate
        oAuthProps = ForceAuthenticator.getOauthResponse(clientProperties.getForceOrgId());
        if (oAuthProps == null) {
            oAuthProps = ForceAuthenticator.authenticate(clientProperties);
        }
        

        // Get the token returned in the Map of authorization properties
        String authToken = oAuthProps.get("access_token");
        
        
        // Get the instance URL returned. This is base domain where to send the 
        // POST message, eg. "https://na12.salesforce.com"
        String instanceUrl = oAuthProps.get("instance_url");
        if (instanceUrl == null) {
            throw new ConnectorException("Unable to resolve 'instance_url' from Force authentication response");
        }
        
        // Tag on the actual orderupdate path. This is an org-specific property derived
        // from the FORCE_CLIENT db table.
        String updatePath = clientProperties.getCallbackUrl();
        if (updatePath == null || updatePath.length()==0) {
            throw new ConnectorException("No FORCE_CALLBACK_URL specified in Force client database table.");
        }
        if (updatePath == null) {
            throw new ConnectorException("Unable to resolve order update path");
        }
        
        String orderUpdateURL = instanceUrl + updatePath;
        
        // Invoke web service via generic HTTP POST class.
        HashMap<String, String> authHeaders = new HashMap<String, String>();    
        //authHeaders.put("Content-encoding:", "application/xml");
        authHeaders.put("Content-type", "application/xml");
        authHeaders.put("Accept", "application/xml");
        authHeaders.put("Authorization", "OAuth " + authToken);
        
        
        int retries = 0;
        while (retries <= ConnectorConstants.MAX_SESSION_RETRIES) {
            
            postResponse = HttpPoster.post(orderUpdateURL, xmlPayload, authHeaders);
    

            // If the Force session hasn't expired, return the response
            if (ForceAuthenticator.sessionExpired(postResponse) == false) {
                return postResponse;
            }
            
            // Need to re-authenticate, reset oAuth header... and try again
            log.info("oAuth token expired. Retrieving new token...");
            oAuthProps = ForceAuthenticator.authenticate(clientProperties);
            authToken = oAuthProps.get("access_token");
            authHeaders.put("Authorization", "OAuth " + authToken);
            log.info("Retrieved new token: " + authToken);
            
            retries++;

        }
        
        return postResponse;
    }
}
