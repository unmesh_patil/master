<?php

/**
 *
 * 
 *
 * Generated by <a href="http://enunciate.codehaus.org">Enunciate</a>.
 *
 */
 

  namespace Com\Ifyouwannabecool\Domain\Link {

    /**
     * A social group.
     */
    class SocialGroup  {
    
    
      /**
       * The id of the social group.
       */
      private $id;
      /**
       * The ids of the members in the social group.
       */
      private $memberIds;
      /**
       * The id of the group leader.
       */
      private $groupLeaderId;
      /**
       * Whether the group is exclusive.
       */
      private $exclusive;

      /**
       * Constructs a SocialGroup from a (parsed) JSON hash
       */
      public function __construct($o = null) {
        if( $o ) {
          $this->initFromArray($o);
        }
      }
      
      /**
       * The id of the social group.
       */
      public function getId() {
        return $this->id;
      }
      
      /**
       * The id of the social group.
       */
      public function setId($id) {
        $this->id = $id;
      }
      /**
       * The ids of the members in the social group.
       */
      public function getMemberIds() {
        return $this->memberIds;
      }
      
      /**
       * The ids of the members in the social group.
       */
      public function setMemberIds($memberIds) {
        $this->memberIds = $memberIds;
      }
      /**
       * The id of the group leader.
       */
      public function getGroupLeaderId() {
        return $this->groupLeaderId;
      }
      
      /**
       * The id of the group leader.
       */
      public function setGroupLeaderId($groupLeaderId) {
        $this->groupLeaderId = $groupLeaderId;
      }
      /**
       * Whether the group is exclusive.
       */
      public function getExclusive() {
        return $this->exclusive;
      }
      
      /**
       * Whether the group is exclusive.
       */
      public function setExclusive($exclusive) {
        $this->exclusive = $exclusive;
      }
      /**
       * Returns the associative array for this SocialGroup
       */
      public function toArray() {
        $a = array();
        if( $this->id ) {
          $a["id"] = $this->id;
        }
        if( $this->memberIds ) {
          $ab = array();
          foreach( $this->memberIds as $i => $x ) {
            $ab[$i] = $x;
          }
          $a['memberIds'] = $ab;
        }
        if( $this->groupLeaderId ) {
          $a["groupLeaderId"] = $this->groupLeaderId;
        }
        if( $this->exclusive ) {
          $a["exclusive"] = $this->exclusive;
        }
        return $a;
      }
      
      /**
       * Returns the JSON string for this SocialGroup
       */
      public function toJson() {
        return json_encode($this->toArray());
      }

      /**
       * Initializes this SocialGroup from an associative array
       */
      public function initFromArray($o) {
        if( isset($o['id']) ) {
          $this->id = $o["id"];
        }
        $this->memberIds = array();
        if( isset($o['memberIds']) ) {
          foreach( $o['memberIds'] as $i => $x ) {
            $this->memberIds[$i] = $x;
          }
        }
        if( isset($o['groupLeaderId']) ) {
          $this->groupLeaderId = $o["groupLeaderId"];
        }
        if( isset($o['exclusive']) ) {
          $this->exclusive = $o["exclusive"];
        }
      }
    
    }
    
  }


  namespace Com\Ifyouwannabecool\Domain\Persona {

    /**
     * 
     */
    class Persona  {
    
    
      /**
       * (no documentation provided)
       */
      private $id;
      /**
       * (no documentation provided)
       */
      private $email;
      /**
       * (no documentation provided)
       */
      private $alias;
      /**
       * (no documentation provided)
       */
      private $name;
      /**
       * (no documentation provided)
       */
      private $picture;

      /**
       * Constructs a Persona from a (parsed) JSON hash
       */
      public function __construct($o = null) {
        if( $o ) {
          $this->initFromArray($o);
        }
      }
      
      /**
       * (no documentation provided)
       */
      public function getId() {
        return $this->id;
      }
      
      /**
       * (no documentation provided)
       */
      public function setId($id) {
        $this->id = $id;
      }
      /**
       * (no documentation provided)
       */
      public function getEmail() {
        return $this->email;
      }
      
      /**
       * (no documentation provided)
       */
      public function setEmail($email) {
        $this->email = $email;
      }
      /**
       * (no documentation provided)
       */
      public function getAlias() {
        return $this->alias;
      }
      
      /**
       * (no documentation provided)
       */
      public function setAlias($alias) {
        $this->alias = $alias;
      }
      /**
       * (no documentation provided)
       */
      public function getName() {
        return $this->name;
      }
      
      /**
       * (no documentation provided)
       */
      public function setName($name) {
        $this->name = $name;
      }
      /**
       * (no documentation provided)
       */
      public function getPicture() {
        return $this->picture;
      }
      
      /**
       * (no documentation provided)
       */
      public function setPicture($picture) {
        $this->picture = $picture;
      }
      /**
       * Returns the associative array for this Persona
       */
      public function toArray() {
        $a = array();
        if( $this->id ) {
          $a["id"] = $this->id;
        }
        if( $this->email ) {
          $a["email"] = $this->email;
        }
        if( $this->alias ) {
          $a["alias"] = $this->alias;
        }
        if( $this->name ) {
          $a["name"] = $this->name->toArray();
        }
        if( $this->picture ) {
          $a["picture"] = $this->picture;
        }
        return $a;
      }
      
      /**
       * Returns the JSON string for this Persona
       */
      public function toJson() {
        return json_encode($this->toArray());
      }

      /**
       * Initializes this Persona from an associative array
       */
      public function initFromArray($o) {
        if( isset($o['id']) ) {
          $this->id = $o["id"];
        }
        if( isset($o['email']) ) {
          $this->email = $o["email"];
        }
        if( isset($o['alias']) ) {
          $this->alias = $o["alias"];
        }
        if( isset($o['name']) ) {
          $this->name = new \Com\Ifyouwannabecool\Domain\Persona\Name($o["name"]);
        }
        if( isset($o['picture']) ) {
          $this->picture = $o["picture"];
        }
      }
    
    }
    
  }


  namespace Com\Ifyouwannabecool\Domain\Persona {

    /**
     * A name of a persona.
     */
    class Name  {
    
    
      /**
       * The given name.
       */
      private $givenName;
      /**
       * The surname.
       */
      private $surname;

      /**
       * Constructs a Name from a (parsed) JSON hash
       */
      public function __construct($o = null) {
        if( $o ) {
          $this->initFromArray($o);
        }
      }
      
      /**
       * The given name.
       */
      public function getGivenName() {
        return $this->givenName;
      }
      
      /**
       * The given name.
       */
      public function setGivenName($givenName) {
        $this->givenName = $givenName;
      }
      /**
       * The surname.
       */
      public function getSurname() {
        return $this->surname;
      }
      
      /**
       * The surname.
       */
      public function setSurname($surname) {
        $this->surname = $surname;
      }
      /**
       * Returns the associative array for this Name
       */
      public function toArray() {
        $a = array();
        if( $this->givenName ) {
          $a["givenName"] = $this->givenName;
        }
        if( $this->surname ) {
          $a["surname"] = $this->surname;
        }
        return $a;
      }
      
      /**
       * Returns the JSON string for this Name
       */
      public function toJson() {
        return json_encode($this->toArray());
      }

      /**
       * Initializes this Name from an associative array
       */
      public function initFromArray($o) {
        if( isset($o['givenName']) ) {
          $this->givenName = $o["givenName"];
        }
        if( isset($o['surname']) ) {
          $this->surname = $o["surname"];
        }
      }
    
    }
    
  }

  
?>