rem	(c) 2006 BEA Systems, Inc. All Rights Reserved.

rem     THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
rem     BEA Systems, Inc.
rem     The copyright notice above does not evidence any
rem     actual or intended publication of such source code.

rem	(c) 2006 BEA Systems, Inc. All Rights Reserved.

SET TUXDIR=<full path of TUXEDO software>
rem SET WSNADDR=<address of the server; for .NET client, WSL must be configured>
set APPDIR=<full path of the application directory>
set PATH=%TUXDIR%\bin;%APPDIR%;%PATH%
rem SET TUXCONFIG=%APPDIR%\tuxconfig

set FIELDTBLS32=Fnext_flds
set FLDTBLDIR32=%APPDIR%
set VIEWFILES32=firstvw.VV
set VIEWDIR32=%APPDIR%

set FIELDTBLS=Fnext_flds16
set FLDTBLDIR=%APPDIR%
set VIEWFILES=firstvw16.VV
set VIEWDIR=%APPDIR%
