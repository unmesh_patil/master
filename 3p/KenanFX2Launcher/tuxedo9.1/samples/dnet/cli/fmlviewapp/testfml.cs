
using System;
using System.Text;
using System.Collections;

using System.Reflection;
using System.Runtime.InteropServices;

using Bea.Tuxedo.ATMI;
using Bea.Tuxedo.FML;
using Bea.Tuxedo.Autogen;



public class TestFML
{

	public static void Main(string[] args) {

		if (args.Length < 1) {
			Console.WriteLine("usage: testfml <16|32>");
			return;
		}

		try {
			if (args[0].Equals("16")) {
				TestFML16();
			} else if (args[0].Equals("32")) { 
				TestFML32();
			} else {
				Console.WriteLine("error: unknown option '{0}'", args[0] );
				return;
			}
		} catch (Exception e) {
			Console.WriteLine(e);
			Console.WriteLine("\n\n!!!  TypedFML/TypedFML32 test failed  !!!\n\n");
			return;
		} 

		Console.WriteLine("\n\n!!!  TypedFML/TypedFML32 test succeeded  !!!\n\n");
		
	}


		//	This function is used to test the TypedFML wrapper class for 16-bit FML
	public static void TestFML16() {

		TypedFML fml = new TypedFML(10000);
		First16 firstvw16 = new First16();
		


		Console.WriteLine("\nThe following code shows how to invoke static members functions of class TypedFML");
		Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

		TypedFML.Ferror = FException.FBADFLD;
		Console.WriteLine("TypedFML.Fstrerror(FException.FBADFLD) ==> {0}", TypedFML.Fstrerror(FException.FBADFLD) );
		TypedFML.F_error("TypedFML.F_error");

		Console.WriteLine("TypedFML.Fname(fnext_flds16.F_short) ==> {0}", TypedFML.Fname(fnext_flds16.F_short) );
		Console.WriteLine("TypedFML.Fldid(TypedFML.Fname(fnext_flds16.F_short)) ==> {0}", 
							TypedFML.Fldid(TypedFML.Fname(fnext_flds16.F_short)) );
		Console.WriteLine("TypedFML.Fldtype(fnext_flds16.F_short) ==> {0}", TypedFML.Fldtype(fnext_flds16.F_short) );
		Console.WriteLine("TypedFML.Fldno(fnext_flds16.F_short) ==> {0}", TypedFML.Fldno(fnext_flds16.F_short) );

		Console.WriteLine("TypedFML.Fidnm_unload()");
		TypedFML.Fidnm_unload();
		Console.WriteLine("TypedFML.Fnmid_unload()");
		TypedFML.Fnmid_unload();

		Console.WriteLine("TypedFML.Fmkfldid(FieldType.FLD_DOUBLE, 112) ==> {0}", 
							TypedFML.Fmkfldid(FieldType.FLD_DOUBLE, 112) );

		Console.WriteLine("TypedFML.Fneeded(50, 100) ==> {0}", TypedFML.Fneeded(50, 100 ) );
		Console.WriteLine("TypedFML.Ftype(fnext_flds16.F_short) ==> {0}", TypedFML.Ftype(fnext_flds16.F_short ) );

		Console.WriteLine("TypedFML.Fvneeded(\"First16\") ==> {0}", TypedFML.Fvneeded("First16") );

		Console.WriteLine("TypedFML.Fvnull(firstvw16, \"bb\", 0) ==> {0}", 
									TypedFML.Fvnull(firstvw16, "bb", 0) );
		TypedFML.Fvopt("bb", FMLC.F_BOTH, "First16");
		TypedFML.Fvrefresh();

		firstvw16.bb = 12345;
		firstvw16.cc = 123.456F;
		TypedFML.Fvselinit(firstvw16, "bb");	//	reset the firstvw16.bb to default value
		Console.WriteLine("TypedFML.Fvselinit(firstvw16, \"bb\") ==> firstvw16.bb={0}, firstvw16.cc={1}",
							firstvw16.bb, firstvw16.cc );
		TypedFML.Fvsinit(firstvw16);	//	reset all members of firstvw to default value
		Console.WriteLine("TypedFML.Fvsinit(firstvw16) ==> firstvw16.bb={0}, firstvw16.cc={1}",
							firstvw16.bb, firstvw16.cc );

		Console.WriteLine("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n"); 

		Console.WriteLine("\nThe following code shows how to invoke instance members of class TypedFML");
		Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

			//	add a field of type 'short'
		short nn = 123;
		fml.Fadd(fnext_flds16.F_short, nn);
		Console.WriteLine("adding a field, name={0}, id={1}, num={3}, type={2}",
							TypedFML.Fname(fnext_flds16.F_short), 
							TypedFML.Fldid(TypedFML.Fname(fnext_flds16.F_short)),
							TypedFML.Fldtype(fnext_flds16.F_short),
							TypedFML.Fldno(fnext_flds16.F_short) );

		fml.Fadds(fnext_flds16.F_short, "5555" );
		fml.Fadds(fnext_flds16.F_short, "7777" );

		fml.Fchg(fnext_flds16.F_short, 1, (short)6666);

		Console.WriteLine("fml.Ffindocc(fnext_flds16.F_short, (short)6666) ==> {0}", 
									fml.Ffindocc(fnext_flds16.F_short, (short)6666) );
		Console.WriteLine("fml.Flen(fnext_flds16.F_short, 1) ==> {0}", fml.Flen(fnext_flds16.F_short, 1) );

		Console.WriteLine("fml.Fnum() ==> {0}", fml.Fnum() );
		Console.WriteLine("fml.Foccur(fnext_flds16.F_short) ==> {0}", fml.Foccur(fnext_flds16.F_short) );

		Console.WriteLine("fml.Fprint() ==> ");
		fml.Fprint();

		Console.WriteLine("fml.Fsizeof() ==> {0}", fml.Fsizeof() );
		Console.WriteLine("fml.Fchksum() ==> {0}", fml.Fchksum() );

		TypedFML32 fml32 = new TypedFML32(10000);
		Console.WriteLine("fml32.FromTypedFML(fml) ==> ");
		fml32.FromTypedFML(fml);
		fml32.Fprint();


		TypedFML fml_two = new TypedFML(200);

		fml_two.Fadd(fnext_flds16.F_short, (short)2000);
		Console.WriteLine("fml.Fconcat(fml_two) ==> ");
		fml.Fconcat(fml_two);
		fml.Fprint();

		FLDOCC oc;
		Object value;
		fml.Fgetlast(fnext_flds16.F_short, out oc, out value);
		Console.WriteLine("fml.Fgetlast(fnext_flds16.F_short, ..) ==> oc={0}, value={1}", oc, value );

		fml.Findex(10);
		Console.WriteLine("fml.Fidxused() ==> {0}", oc, fml.Fidxused() );


		fml_two.Fadd(fnext_flds16.F_double, (double)12345678.99);

		Console.WriteLine("fml.Fojoin(fml_two) ==>" );
		fml.Fojoin(fml_two);

		fml.Fprint();

		Console.WriteLine("fml_two.Fpres(fnext_flds16.F_double, 0) ==> {0}",
									fml_two.Fpres(fnext_flds16.F_double, 0) );

		int idx_num = fml_two.Funindex();
		Console.WriteLine("fml_two.Funindex() ==> {0}", idx_num );
		Console.WriteLine("fml_two.Frstrindex(idx_num) ==>" );
		fml_two.Frstrindex(idx_num);

		Console.WriteLine("fml.Fupdate(fml_two) ==>" );
		fml.Fupdate(fml_two);
		fml.Fprint();



		Console.WriteLine("fml.Fdel(fnext_flds16.F_short, 1) ==>" );
		Console.WriteLine("fml.Fdelall(fnext_flds16.F_short) ==>" );
		fml.Fdel(fnext_flds16.F_short, 1);
		fml.Fdelall(fnext_flds16.F_short);


		FILE datfile = new FILE();

		datfile.fopen("fml_txt.dat", "w");
		fml.Ffprint(datfile);
		datfile.fclose();

		datfile.fopen("fml_bin.dat", "wb");
		fml.Fwrite(datfile);
		datfile.fclose();

		Console.WriteLine("fml.Finit(15000) ==>" );
		fml.Finit(15000);

		datfile.fopen("fml_bin.dat", "rb");
		Console.WriteLine("fml.Fread(datfile) ==>" );
		fml.Fread(datfile);
		datfile.fclose();
		fml.Fprint();

		Console.WriteLine("fml.Finit(15000) ==>" );
		fml.Finit(15000);

		fml_two = new TypedFML(fml.Size);

		fml_two.Fcpy(fml);
		Console.WriteLine("fml.Fcmp(fml_two) ==> {0}", fml.Fcmp(fml_two) );
		fml_two.Dispose();

			//	add a field of type 'double'
		double dd = 777.7777;
		fml.Fadd(fnext_flds16.F_double, dd);
		Console.WriteLine("adding a field, name={0}, id={1}, num={3}, type={2}",
							TypedFML.Fname(fnext_flds16.F_double), 
							TypedFML.Fldid(TypedFML.Fname(fnext_flds16.F_double)),
							TypedFML.Fldtype(fnext_flds16.F_double),
							TypedFML.Fldno(fnext_flds16.F_double) );

		Console.WriteLine("\n=======================================\n");

		Console.WriteLine("fml.Fchgs(fnext_flds16.F_double, 0, \"666.7777\") ==>");
		fml.Fchgs(fnext_flds16.F_double, 0, "666.7777");

		Console.WriteLine("fml.Ffinds(fnext_flds16.F_double, 0) ==> {0}", fml.Ffinds(fnext_flds16.F_double, 0) );

		Console.WriteLine("\nfml.Fnum() ==> {0}\n", (int)fml.Fnum() );

		fml.Fprint();

		Console.WriteLine("fml.Funused() ==> {0}", fml.Funused() );
		Console.WriteLine("fml.Fused() ==> {0}", fml.Fused() );


		fml.Resize(20000);
		

		int		rc;
		FLDID	fldid;
		FLDLEN	len;
		value = 0;

			//	travel around all the fields in fml
		for (rc=1, oc = 0, fldid=FMLC.FIRSTFLDID; rc == 1; ) {
			rc = fml.Fnext(ref fldid, ref oc, ref value, out len);
			if (rc == 1)
				Console.WriteLine("fldid = {0},  fldocc = {1},  value = {2},  fldlen = {3}",
							fldid, oc, value, len );
		}


		fml.Fget(fnext_flds16.F_double, 0, out value);
		Console.WriteLine("fml.Fget(fnext_flds16.F_double, 0, out value) ==> {0}\n", value);


		fml.Finit(15000);

		for (int i=0; i < firstvw16.aa.Length; i++)
			firstvw16.aa[i] = (short)(firstvw16.aa.Length-1-i);
		firstvw16.bb = 54321;
		firstvw16.cc = 888.777F;

		Console.WriteLine("fml.FromTypedView(firstvw16, FMLC.FUPDATE) ==>");
		Console.WriteLine("firstvw16.Subtype = {0}", firstvw16.Subtype );
		fml.FromTypedView(firstvw16, FMLC.FUPDATE);
		fml.Fprint();


		firstvw16.Dispose();
		fml.Dispose();

		return;
	}



		
	public static void TestFML32() {

		TypedFML32 fml32 = new TypedFML32(10000);
		First firstvw = new First();


		Console.WriteLine("\nThe following code shows how to invoke static members of class TypedFML32");
		Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

		TypedFML32.Ferror = FException.FBADFLD;
		Console.WriteLine("TypedFML32.Fstrerror(FException.FBADFLD) ==> {0}", TypedFML32.Fstrerror(FException.FBADFLD) );
		TypedFML32.F_error("TypedFML32.F_error");

		Console.WriteLine("TypedFML32.Fname(fnext_flds.F_view32) ==> {0}", TypedFML32.Fname(fnext_flds.F_view32) );
		Console.WriteLine("TypedFML32.Fldid(TypedFML32.Fname(fnext_flds.F_view32)) ==> {0}", 
							TypedFML32.Fldid(TypedFML32.Fname(fnext_flds.F_view32)) );
		Console.WriteLine("TypedFML32.Fldtype(fnext_flds.F_view32) ==> {0}", TypedFML32.Fldtype(fnext_flds.F_view32) );
		Console.WriteLine("TypedFML32.Fldno(fnext_flds.F_view32) ==> {0}", TypedFML32.Fldno(fnext_flds.F_view32) );

		Console.WriteLine("TypedFML32.Fidnm_unload()");
		TypedFML32.Fidnm_unload();
		Console.WriteLine("TypedFML32.Fnmid_unload()");
		TypedFML32.Fnmid_unload();

		Console.WriteLine("TypedFML32.Fmkfldid(FieldType.FLD_DOUBLE, 112) ==> {0}", 
							TypedFML32.Fmkfldid(FieldType.FLD_DOUBLE, 112) );

		Console.WriteLine("TypedFML32.Fneeded(50, 100) ==> {0}", TypedFML32.Fneeded(50, 100 ) );
		Console.WriteLine("TypedFML32.Ftype(fnext_flds.F_view32) ==> {0}", TypedFML32.Ftype(fnext_flds.F_view32 ) );

		Console.WriteLine("TypedFML32.Fvneeded(\"First\") ==> {0}", TypedFML32.Fvneeded("First") );

		Console.WriteLine("TypedFML32.Fvnull(firstvw, \"bb\", 0) ==> {0}", 
									TypedFML32.Fvnull(firstvw, "bb", 0) );
		TypedFML32.Fvopt("bb", FMLC.F_BOTH, "First");
		TypedFML32.Fvrefresh();

		firstvw.bb = 12345;
		firstvw.cc = 123.456F;
		TypedFML32.Fvselinit(firstvw, "bb");	//	reset the firstvw.bb to default value
		Console.WriteLine("TypedFML32.Fvselinit(firstvw, \"bb\") ==> firstvw.bb={0}, firstvw.cc={1}",
							firstvw.bb, firstvw.cc );
		TypedFML32.Fvsinit(firstvw);	//	reset all members of firstvw to default value
		Console.WriteLine("TypedFML32.Fvsinit(firstvw) ==> firstvw.bb={0}, firstvw.cc={1}",
							firstvw.bb, firstvw.cc );

		Console.WriteLine("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n"); 

		Console.WriteLine("\nThe following code shows how to invoke instance members of class TypedFML32");
		Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

			//	add a field of type 'short'
		short nn = 123;
		fml32.Fadd(fnext_flds.F_short, nn);
		Console.WriteLine("adding a field, name={0}, id={1}, num={3}, type={2}",
							TypedFML32.Fname(fnext_flds.F_short), 
							TypedFML32.Fldid(TypedFML32.Fname(fnext_flds.F_short)),
							TypedFML32.Fldtype(fnext_flds.F_short),
							TypedFML32.Fldno(fnext_flds.F_short) );

		fml32.Fadds(fnext_flds.F_short, "5555" );
		fml32.Fadds(fnext_flds.F_short, "7777" );

		fml32.Fchg(fnext_flds.F_short, 1, (short)6666);

		Console.WriteLine("fml32.Ffindocc(fnext_flds.F_short, (short)6666) ==> {0}", 
									fml32.Ffindocc(fnext_flds.F_short, (short)6666) );
		Console.WriteLine("fml32.Flen(fnext_flds.F_short, 1) ==> {0}", fml32.Flen(fnext_flds.F_short, 1) );

		Console.WriteLine("fml32.Fnum() ==> {0}", fml32.Fnum() );
		Console.WriteLine("fml32.Foccur(fnext_flds.F_short) ==> {0}", fml32.Foccur(fnext_flds.F_short) );

		Console.WriteLine("fml32.Fprint() ==> ");
		fml32.Fprint();

		Console.WriteLine("fml32.Fsizeof() ==> {0}", fml32.Fsizeof() );
		Console.WriteLine("fml32.Fchksum() ==> {0}", fml32.Fchksum() );

		TypedFML fml = new TypedFML(10000);
		Console.WriteLine("fml.FromTypedFML32(fml32) ==> ");
		fml.FromTypedFML32(fml32);
		fml.Fprint();


		TypedFML32 fml32_two = new TypedFML32(200);

		fml32_two.Fadd(fnext_flds.F_short, (short)2000);
		Console.WriteLine("fml32.Fconcat(fml32_two) ==> ");
		fml32.Fconcat(fml32_two);
		fml32.Fprint();

		FLDOCC oc;
		Object value;
		fml32.Fgetlast(fnext_flds.F_short, out oc, out value);
		Console.WriteLine("fml32.Fgetlast(fnext_flds.F_short, ..) ==> oc={0}, value={1}", oc, value );

		fml32.Findex(10);
		Console.WriteLine("fml32.Fidxused() ==> {0}", oc, fml32.Fidxused() );


		fml32_two.Fadd(fnext_flds.F_double, (double)12345678.99);

		Console.WriteLine("fml32.Fojoin(fml32_two) ==>" );
		fml32.Fojoin(fml32_two);

		fml32.Fprint();

		Console.WriteLine("fml32_two.Fpres(fnext_flds.F_double, 0) ==> {0}",
									fml32_two.Fpres(fnext_flds.F_double, 0) );

		int idx_num = fml32_two.Funindex();
		Console.WriteLine("fml32_two.Funindex() ==> {0}", idx_num );
		Console.WriteLine("fml32_two.Frstrindex(idx_num) ==>" );
		fml32_two.Frstrindex(idx_num);

		Console.WriteLine("fml32.Fupdate(fml32_two) ==>" );
		fml32.Fupdate(fml32_two);
		fml32.Fprint();



		Console.WriteLine("fml32.Fdel(fnext_flds.F_short, 1) ==>" );
		Console.WriteLine("fml32.Fdelall(fnext_flds.F_short) ==>" );
		fml32.Fdel(fnext_flds.F_short, 1);
		fml32.Fdelall(fnext_flds.F_short);


		FILE datfile = new FILE();

		datfile.fopen("fml32_txt.dat", "w");
		fml32.Ffprint(datfile);
		datfile.fclose();

		datfile.fopen("fml32_bin.dat", "wb");
		fml32.Fwrite(datfile);
		datfile.fclose();

		Console.WriteLine("fml32.Finit(15000) ==>" );
		fml32.Finit(15000);

		datfile.fopen("fml32_bin.dat", "rb");
		Console.WriteLine("fml32.Fread(datfile) ==>" );
		fml32.Fread(datfile);
		datfile.fclose();
		fml32.Fprint();

		Console.WriteLine("fml32.Finit(15000) ==>" );
		fml32.Finit(15000);

		fml32_two = new TypedFML32(fml32.Size);

		fml32_two.Fcpy(fml32);
		Console.WriteLine("fml32.Fcmp(fml32_two) ==> {0}", fml32.Fcmp(fml32_two) );
		fml32_two.Dispose();



			//	add a field of type 'view32'
		for (int i=0; i < firstvw.aa.Length; i++)
			firstvw.aa[i] = (short)i;
		firstvw.bb = 12345;
		firstvw.cc = 999.888F;
		fml32.Fadd(fnext_flds.F_view32, firstvw);
		Console.WriteLine("adding a field, name={0}, id={1}, num={3}, type={2}",
							TypedFML32.Fname(fnext_flds.F_view32), 
							TypedFML32.Fldid(TypedFML32.Fname(fnext_flds.F_view32)),
							TypedFML32.Fldtype(fnext_flds.F_view32),
							TypedFML32.Fldno(fnext_flds.F_view32) );

			//	add a field of type 'double'
		double dd = 777.7777;
		fml32.Fadd(fnext_flds.F_double, dd);
		Console.WriteLine("adding a field, name={0}, id={1}, num={3}, type={2}",
							TypedFML32.Fname(fnext_flds.F_double), 
							TypedFML32.Fldid(TypedFML32.Fname(fnext_flds.F_double)),
							TypedFML32.Fldtype(fnext_flds.F_double),
							TypedFML32.Fldno(fnext_flds.F_double) );

		Console.WriteLine("\n=======================================\n");

		Console.WriteLine("fml32.Fchgs(fnext_flds.F_double, 0, \"666.7777\") ==>");
		fml32.Fchgs(fnext_flds.F_double, 0, "666.7777");

		Console.WriteLine("fml32.Ffinds(fnext_flds.F_double, 0) ==> {0}", fml32.Ffinds(fnext_flds.F_double, 0) );

		Console.WriteLine("\nfml32.Fnum() ==> {0}\n", (int)fml32.Fnum() );

		fml32.Fprint();

		Console.WriteLine("fml32.Funused() ==> {0}", fml32.Funused() );
		Console.WriteLine("fml32.Fused() ==> {0}", fml32.Fused() );


		fml32.Resize(20000);
		

		int		rc;
		FLDID	fldid;
		FLDLEN	len;
		value = 0;

			//	travel around all the fields in fml32
		for (rc=1, oc = 0, fldid=FMLC.FIRSTFLDID; rc == 1; ) {
			rc = fml32.Fnext(ref fldid, ref oc, ref value, out len);
			if (rc == 1)
				Console.WriteLine("fldid = {0},  fldocc = {1},  value = {2},  fldlen = {3}",
							fldid, oc, value, len );
		}

			//	get the field of type view32
		fml32.Fget(fnext_flds.F_view32, 0, out value);
		firstvw = (First)value;

		for (int i=0; i < firstvw.aa.Length; i++)
			Console.WriteLine("aa[{0}] = {1}", i, firstvw.aa[i] );

		Console.WriteLine("bb = {0}\ncc = {1}\n", firstvw.bb,  firstvw.cc );


		fml32.Finit(15000);
		Console.WriteLine("fml32.FromTypedView32(firstvw, FMLC.FUPDATE) ==>");
		fml32.FromTypedView32(firstvw, FMLC.FUPDATE);
		fml32.Fprint();


		firstvw.Dispose();
		fml32.Dispose();

		return;

	}


}


