rem	(c) 2006 BEA Systems, Inc. All Rights Reserved.

rem     THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
rem     BEA Systems, Inc.
rem     The copyright notice above does not evidence any
rem     actual or intended publication of such source code.

rem	(c) 2006 BEA Systems, Inc. All Rights Reserved.

SET TUXDIR=<full path of TUXEDO software>
SET WSNADDR=<address of the server; for .NET client, WSL must be configured>
set APPDIR=<full path of the application directory>
set PATH=%TUXDIR%\bin;%APPDIR%;%PATH%
SET TUXCONFIG=%APPDIR%\tuxconfig
