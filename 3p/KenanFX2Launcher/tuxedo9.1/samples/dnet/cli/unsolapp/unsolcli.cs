/*
 *	(c) 2006 BEA Systems, Inc.
 *	All Rights Reserved.
 *
 *	THIS IS UNPUBLISHED PROPRIETARY
 *	SOURCE CODE OF BEA Systems, Inc.
 *	The copyright notice above does not
 *	evidence any actual or intended
 *	publication of such source code.
 */

using System;
using System.Threading;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;

using Bea.Tuxedo.ATMI;

public class UnsolTest
{
	int thread_index;
	int loop_times;

	static int max_retry = 10;  /* maximum retry times to wait fo all unsolicited messages */

	static int rcv_cnt = 0;   /* static counter for received unsolicited messages */
	static int exp_cnt = 0;   /* expected number of unsolicited messages of this program */

	public UnsolTest(int thread_index, int lt) {
		this.thread_index = thread_index;
		loop_times = lt;
	}


	public static void MyUMHandler(AppContext appctxt, TypedBuffer tb, long flags) {
		Console.WriteLine("MyUMHandler is being called,  {0}", appctxt.ToString() );
		string rcvstr_str = ((TypedString)tb).GetString(0, 1000);
		lock(typeof(UnsolTest))
			UnsolTest.rcv_cnt++;
		Console.WriteLine("MyUMHandler received No.{0} string = \"{1},  flags = {2:X}\"\n", UnsolTest.rcv_cnt, rcvstr_str, flags);
	}



	public void Run() {

		try {

			TypedString  sndstr = new TypedString(1000);
			TypedBuffer  rcvstr = new TypedString(1000);

			string s_sndstr = "thread " + thread_index.ToString() + ",  ";

			TypedTPINIT	tpinfo = new TypedTPINIT();
			tpinfo.flags = TypedTPINIT.TPMULTICONTEXTS | TypedTPINIT.TPU_THREAD;

			AppContext	ac = AppContext.tpinit(tpinfo);


			ac.tpsetunsol(new UnsolicitedMessageHandler(MyUMHandler) );

			for (int i=0; i < loop_times; i++) {

				string tmpstr = s_sndstr + "i = " + i.ToString();
				sndstr.PutString(0, tmpstr, tmpstr.Length);

				//	UNSOLSVC is a service in unsolsvr server
				//	it calls tpnotify to send unsolicited messages back to this client before it calls tpreturn 
				ac.tpcall("UNSOLSVC", sndstr, ref rcvstr, 0);

			}
            
			for (int i = 0; UnsolTest.rcv_cnt != UnsolTest.exp_cnt && i < UnsolTest.max_retry; Thread.Sleep(1000), i++) {
				Console.WriteLine("Thread {0}: Retry {1} times - Waiting for all unsolicite messages ......", thread_index, i+1);
			}

			Console.WriteLine("Thread {0}: exiting......", thread_index);
			ac.tpterm();

		} catch (ApplicationException e) {
			Console.WriteLine("**** ERROR ****,  e = {0}\n", e );
		}
	}	
		

	public static void Main(string[] args) {

		if (args.Length == 0 || args.Length > 2) {
			Console.WriteLine("Usage: unsolcli [[number_of_threads] loop_times]\n");
			return;
		}

		int number_of_threads;
		int loop_times;

		if (args.Length > 1) {
			number_of_threads = Convert.ToInt32(args[0]);
			loop_times = Convert.ToInt32(args[1]);
		} else {
			number_of_threads = 1;
			loop_times = Convert.ToInt32(args[0]);
		}

		if (number_of_threads < 1) {
			Console.WriteLine("ERROR: invalid number {0} for optional argument 'number_of_threads'", number_of_threads );
			return;
		}

		if (loop_times < 1) {
			Console.WriteLine("ERROR: invalid number {0} for argument 'loop_times'", loop_times );
			return;
		}

		/* calculate the number of expected unsolicited messages */
		UnsolTest.exp_cnt = loop_times * number_of_threads;

		for (int i=0; i < number_of_threads; i++) {

			Thread	bt = new Thread(new ThreadStart((new UnsolTest(i+1, loop_times)).Run));
			bt.Start();
		}

	}

}

