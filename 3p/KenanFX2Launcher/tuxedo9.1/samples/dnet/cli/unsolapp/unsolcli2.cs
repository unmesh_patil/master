/*
 *	(c) 2006 BEA Systems, Inc.
 *	All Rights Reserved.
 *
 *	THIS IS UNPUBLISHED PROPRIETARY
 *	SOURCE CODE OF BEA Systems, Inc.
 *	The copyright notice above does not
 *	evidence any actual or intended
 *	publication of such source code.
 */

using System;
using System.Threading;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;

using Bea.Tuxedo.ATMI;

public class MsgBroadcaster
{
	private int index;
	private int loop_times;

	public MsgBroadcaster(int idx, int lt) {
		index = idx;
		loop_times = lt;
	}


	public void run() {

		TypedTPINIT	tpinfo = new TypedTPINIT();
		tpinfo.flags = TypedTPINIT.TPMULTICONTEXTS;

		AppContext	ac = AppContext.tpinit(tpinfo);

		TypedString  sndstr = new TypedString(1000);

		for (int i=0; i < loop_times; i++) {

			string s_sndstr = "Broadcaster(index=" + index + "),  message NO = " + i;
			sndstr.PutString(0, s_sndstr, 1000);
			
			ac.tpbroadcast(null, null, null, sndstr, 0 );
			Console.WriteLine(s_sndstr);
		}
			//	the exit sign
		sndstr.PutString(0, "over", 1000);
		ac.tpbroadcast(null, null, null, sndstr, 0 );

		Console.WriteLine("*** this is the end of MsgBroadcaster ***");
		ac.tpterm();
	}
}




public class MsgReceiver 
{
	private bool over = false;
	private int index;
	
	public MsgReceiver(int idx) {
		index = idx;
	}


	public void MyUMHandler(AppContext appctxt, TypedBuffer tb, long flags) {
		Console.WriteLine("MyUMHandler is being called,  {0}", appctxt.ToString() );
		string rcvstr_str = ((TypedString)tb).GetString(0, 1000);
		if (rcvstr_str.Equals("over")) {
			over = true;
			Console.WriteLine("MsgReceiver(index={0}) exit.\n",  index );
		} else {
			Console.WriteLine("MyUMHandler received string = \"{0},  flags = {1:X}\"\n", rcvstr_str, flags );
		}

	}


	public void run() {

		TypedTPINIT	tpinfo = new TypedTPINIT();
		tpinfo.flags = TypedTPINIT.TPMULTICONTEXTS | TypedTPINIT.TPU_DIP;

		AppContext	ac = AppContext.tpinit(tpinfo);

		ac.tpsetunsol(new UnsolicitedMessageHandler(this.MyUMHandler) );

		for (int i=0; !over; Thread.Sleep(1000)) {
			Console.WriteLine("i = {0}",  i++ );
			ac.tpchkunsol();
		}

		Console.WriteLine("*** this is the end of MsgReceiver(index={0}) ***", index);

		ac.tpterm();
	}

}



public class UnsolTest2
{
	public static void Main(string[] args) {


		if (args.Length == 0 || args.Length > 3) {
			Console.WriteLine("Usage: unsolcli [[number_of_broadcasters  number_of_receivers] loop_times]\n");
			return;
		} else if (args.Length == 2) {
			Console.WriteLine("ERROR: incorrect syntex");
			return;

		} 

			//	default settings
		int number_of_broadcasters = 1, number_of_receivers = 1;
		int loop_times = 100;

		if (args.Length == 3) {
			number_of_broadcasters = Convert.ToInt32(args[0]);
			number_of_receivers = Convert.ToInt32(args[1]); 
			loop_times = Convert.ToInt32(args[2]);
		} else if (args.Length == 1) {
			loop_times = Convert.ToInt32(args[0]);
		} else {
			//	use default settings
		}

		if (number_of_broadcasters < 1) {
			Console.WriteLine("ERROR: invalid number {0} for optional argument 'number_of_broadcasters'", number_of_broadcasters );
			return;
		}

		if (number_of_receivers < 1) {
			Console.WriteLine("ERROR: invalid number {0} for optional argument 'number_of_receivers'", number_of_receivers );
			return;
		}

		if (loop_times < 1) {
			Console.WriteLine("ERROR: invalid number {0} for argument 'loop_times'", loop_times );
			return;
		}


		Console.WriteLine("Creating {0} receiver threads...", number_of_receivers );
		for (int i=0; i < number_of_receivers; i++) {
			Thread	bt = new Thread(new ThreadStart((new MsgReceiver(i+1)).run));
			bt.Start();
		}
		Thread.Sleep(1000);

		Console.WriteLine("Creating {0} broadcaster threads...", number_of_broadcasters );
		Thread.Sleep(2000);
		for (int i=0; i < number_of_broadcasters; i++) {
			Thread	bt = new Thread(new ThreadStart((new MsgBroadcaster(i+1, loop_times)).run));
			bt.Start();
		}

	}

}

