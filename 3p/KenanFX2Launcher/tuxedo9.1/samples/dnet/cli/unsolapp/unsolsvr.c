/*
  	(c) 2006 BEA Systems, Inc. All Rights Reserved.

  	THIS IS UNPUBLISHED PROPRIETARY
  	SOURCE CODE OF BEA Systems, Inc.
  	The copyright notice above does not
  	evidence any actual or intended
  	publication of such source code.
*/

#include <stdio.h>		/* UNIX */
#include <string.h>		/* UNIX */

#include <atmi.h>		/* TUXEDO */
#include <Uunix.h>		/* TUXEDO */
#include <userlog.h>		/* TUXEDO */

static char pgmname[1000];		/* program name = argv[0] */
static int number = 0;


int
#if defined(__STDC__) || defined(__cplusplus)
tpsvrinit(int argc, char **argv)
#else
tpsvrinit(argc,argv)
int     argc;
char    **argv;
#endif
{
	(void)strcpy(pgmname, argv[0]); /* program name */
	userlog("%s started", pgmname);

	return(0);
}


#define	LINE	1000

void
#if defined(__STDC__) || defined(__cplusplus)
UNSOLSVC(TPSVCINFO *svcinfo)
#else
UNSOLSVC(svcinfo)
TPSVCINFO *svcinfo;
#endif
{
	
	char c;				/* Option character */
	char svc_name[100];	/* service name */
	char hdr_type[100];	/* heading to appear on output */
	char *line;			/* tux buffer holding request	*/

	long len;			/* length of recv'd line	*/
	long revent;		/* events for send/recv */
	time_t	now;
	struct tm*	tm_now;

	/* allocate buffer to receive balance request.	*/

	if ((line = tpalloc("STRING","",LINE+1)) == (char *)NULL) {
		(void)userlog("%s: string tpalloc failed tperrno %d",
                        pgmname,tperrno);
		(void)tpreturn(TPFAIL,0,NULL,0,0);
	}

	sprintf(line, "Invocation SN = %6d", ++number);

	len = strlen(line);
	sprintf(&line[len], " >>> clientdata[0] = {%d},  [1] = {%d},  [2] = {%d},  [3] = {%d}",
				svcinfo->cltid.clientdata[0], svcinfo->cltid.clientdata[1], 
				svcinfo->cltid.clientdata[2], svcinfo->cltid.clientdata[3] );

	if (tpnotify(&svcinfo->cltid, line, strlen(line), 0 ) < 0) {
		(void)userlog("tpnotify failed.\n");
		(void)tpreturn(TPFAIL,0,NULL,0,0);
	}
	
	line[len] = 0;

	(void)tpreturn(TPSUCCESS, 0, line, len, 0);

}

