/*
 *	(c) 2006 BEA Systems, Inc.
 *	All Rights Reserved.
 *
 *	THIS IS UNPUBLISHED PROPRIETARY
 *	SOURCE CODE OF BEA Systems, Inc.
 *	The copyright notice above does not
 *	evidence any actual or intended
 *	publication of such source code.
 */

using System;
using System.Threading;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;

using Bea.Tuxedo.ATMI;


//	single thread
public class STClient
{
	int loop_times;

	public STClient(int lt) {
		loop_times = lt;
	}

	public void Run() {

		try {

			TypedString  sndstr = new TypedString(1000);
			TypedBuffer  rcvstr = new TypedString(1000);

			string s_sndstr = "single thread,  ";

			for (int i=0; i < loop_times; i++) {

				Console.WriteLine("i = {0}", i );

					//	use single application context
				AppContext	ac = AppContext.tpinit(null);

				string tmpstr = s_sndstr + "i = " + i.ToString();
				sndstr.PutString(0, tmpstr, tmpstr.Length);

					//	asynchronous sending/receiving
				AsyncCallDescriptor acd = ac.tpacall("TOUPPER", sndstr, 0);

				ac.tpgetrply(ref acd, ref rcvstr, 0);

				string rcvstr_str = ((TypedString)rcvstr).GetString(0, 1000);

				Console.WriteLine("tpgetrply >>>   rcvstr = {0}\n", rcvstr_str );


				ac.tpterm();
			}

		} catch (ApplicationException e) {
			Console.WriteLine("**** ERROR ****,  e = {0}\n", e );
		}
	}
}



//	multiple threads
public class MTClient
{
	private int thread_index;
	int loop_times;

	public MTClient(int thread_index, int lt) {
		this.thread_index = thread_index;
		loop_times = lt;
	}

	public void Run() {

		try {

			TypedString  sndstr = new TypedString(1000);
			TypedBuffer  rcvstr = new TypedString(1000);

			string s_sndstr = "thread " + thread_index.ToString() + ",  ";

			for (int i=0; i < loop_times; i++) {

				Console.WriteLine("i = {0}", i );

				TypedTPINIT	tpinfo = new TypedTPINIT();
				tpinfo.flags = TypedTPINIT.TPMULTICONTEXTS;

				AppContext	ac = AppContext.tpinit(tpinfo);


				string tmpstr = s_sndstr + "i = " + i.ToString();
				sndstr.PutString(0, tmpstr, tmpstr.Length);

				ac.tpcall("TOUPPER", sndstr, ref rcvstr, 0);

				string rcvstr_str = ((TypedString)rcvstr).GetString(0, 1000);
	
				Console.WriteLine("tpgetrply >>>   rcvstr = {0}\n", rcvstr_str );

				ac.tpterm();
			}

		} catch (ApplicationException e) {
			Console.WriteLine("**** ERROR ****,  e = {0}\n", e );
		}
	}
}



public class _Main
{
	public static void Main(string[] args) {

		if (args.Length == 0 || args.Length > 2) {
			Console.WriteLine("Usage: callcli [[number_of_threads] loop_times]\n");
			return;
		}
		
		int number_of_threads;
		int loop_times;

		if (args.Length > 1) {
			number_of_threads = Convert.ToInt32(args[0]);
			loop_times = Convert.ToInt32(args[1]);
		} else {
			number_of_threads = 1;
			loop_times = Convert.ToInt32(args[0]);
		}

		if (number_of_threads < 1) {
			Console.WriteLine("ERROR: invalid number {0} for optional argument 'number_of_threads'", number_of_threads );
			return;
		}

		if (loop_times < 1) {
			Console.WriteLine("ERROR: invalid number {0} for argument 'loop_times'", loop_times );
			return;
		}

		if (number_of_threads == 1) {
			Thread	bt = new Thread(new ThreadStart((new STClient(loop_times)).Run));
			bt.Start();
		} else {
			for (int i=0; i < number_of_threads; i++) {
				Thread	bt = new Thread(new ThreadStart((new MTClient(i, loop_times)).Run));
				bt.Start();
			}
		}

	}
	
}	

