      *
      *	Copyright (c) 1999 BEA Systems
      * All rights reserved
      *
      *	THIS IS UNPUBLISHED PROPRIETARY
      *	SOURCE CODE OF BEA Systems
      *	The copyright notice above does not
      *	evidence any actual or intended
      *	publication of such source code.
      *
      * #ident	"@(#) cobol/cobatmi/TPCONTEXTDEF.cbl	$Revision: 1.2 $" 
      * static	char	sccsid[] = "@(#) cobol/cobatmi/TPCONTEXTDEF.cbl	$Revision: 1.2 $";
      *
      *  TPCONTEXTDEF.cbl
      *
	05 CONTEXT		PIC S9(9) COMP-5.
