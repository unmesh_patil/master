      *
      *	Copyright (c) 1997 BEA Systems, Inc.
      * All rights reserved
      *
      *	THIS IS UNPUBLISHED PROPRIETARY
      *	SOURCE CODE OF BEA Systems, Inc.
      *	The copyright notice above does not
      *	evidence any actual or intended
      *	publication of such source code.
      *
      * #ident	"@(#) cobol/cobatmi/TPCONDEF.cbl	$Revision: 1.3 $"
      * static	char	sccsid[] = "@(#) cobol/cobatmi/TPCONDEF.cbl	$Revision: 1.3 $";
      *
      *  TPCONDEF.cbl	
      *
	05 CONTEXT 	PIC S9(9) COMP-5.
