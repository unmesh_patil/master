      *
      *	Copyright (c) 1999 BEA Systems, Inc.
      * All rights reserved
      *
      *	THIS IS UNPUBLISHED PROPRIETARY
      *	SOURCE CODE OF BEA Systems, Inc.
      *	The copyright notice above does not
      *	evidence any actual or intended
      *	publication of such source code.
      *
      * #ident	"@(#) cobol/cobatmi/TPKEYDEF.cbl	$Revision: 1.2 $" 
      * static	char	sccsid[] = "@(#) cobol/cobatmi/TPKEYDEF.cbl	$Revision: 1.2 $";
      *
      *  TPKEYDEF.cbl
      *
	05 KEY-HANDLE 	PIC S9(9) COMP-5.
	05 PRINCIPAL-NAME 	PIC X(256).
	05 LOCATION 	PIC X(1024).
	05 IDENTITY-PROOF 	PIC X(2048).
	05 PROOF-LEN 	PIC S9(9) COMP-5.
	05 SIGNATURE-FLAG 	PIC S9(9) COMP-5.
		88 TPKEY-NOSIGNATURE    VALUE 0.
		88 TPKEY-SIGNATURE      VALUE 1.
	05 DECRYPT-FLAG 	PIC S9(9) COMP-5.
		88 TPKEY-NODECRYPT      VALUE 0.
		88 TPKEY-DECRYPT        VALUE 1.
	05 ENCRYPT-FLAG 	PIC S9(9) COMP-5.
		88 TPKEY-NOENCRYPT      VALUE 0.
		88 TPKEY-ENCRYPT        VALUE 1.
	05 AUTOSIGN-FLAG 	PIC S9(9) COMP-5.
		88 TPKEY-NOAUTOSIGN     VALUE 0.
		88 TPKEY-AUTOSIGN       VALUE 1.
	05 AUTOENCRYPT-FLAG 	PIC S9(9) COMP-5.
		88 TPKEY-NOAUTOENCRYPT  VALUE 0.
		88 TPKEY-AUTOENCRYPT    VALUE 1.
	05 ATTRIBUTE-NAME 	PIC X(64).
	05 ATTRIBUTE-VALUE-LEN 	PIC S9(9) COMP-5.
      *
