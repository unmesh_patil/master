@echo off

rem
rem This batch file is used to execute the
rem FXLauncher application. The first thing
rem we check is whether or not a new version
rem of the launcher has been placed in the
rem current directory


rem *******************************
rem  Variables that can be changed
rem *******************************
rem Set this directory to whereever the jar directory is
set APPDIR=.
set JARDIR=lib
set AXISJARDIR=axis2_lib
set LANGDIR=LanguagePacks
set LOCALCLASSPATH=.
rem set LOCALCLASSPATH2=C:\KenanFX2\FXLauncher\Subsystems\CustomerCenter\5.0.20061226\2-FXCustomCode-2.0.jar



rem ******************************************************
rem    NO USER EDITABLE PARTS BELOW THIS LINE
rem ******************************************************

if exist FXLauncherDeploy.jar goto extractJar

goto keepGoing

:extractJar

rem
rem Extract the contents of the jar to the appropriate
rem directory
rem
java -jar %JARDIR%\utils\FXJarTools-1.0.0.jar FXLauncherDeploy.jar %JARDIR%

rem
rem Delete the jar file so we don't do this again
rem
del /q FXLauncherDeploy.jar

:keepGoing

mkdir "%USERPROFILE%\.config"
echo %USERPROFILE%\.config\FXFrameConfiguration.properties
if exist "%USERPROFILE%\.config\FXFrameConfiguration.properties" del /q "%USERPROFILE%\.config\FXFrameConfiguration.properties"
> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo tuxedo-host-port= gci1devkenap1.cycle30.com:32001
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo fx-configuration-file-count=3
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo fx-configuration-file.1=/config/FXLauncherConfiguration.properties
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo fx-configuration-file.2=/com/cycle30/resources/C30HTMLTemplateOverlayMAP.properties
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo fx-configuration-file.3=/com/cycle30/resources/C30INVTemplateOverlayMAP.properties
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo display-debug-button=true
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo c30-Scheduler-webservice-findWindowsCall=http://gci1devkenap1.cycle30.com:63083/service/cxf/v02/findWindows
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo c30-Scheduler-webservice-createWOCall=http://gci1devkenap1.cycle30.com:63083/service/cxf/v02/createWorkorder
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo c30-Scheduler-webservice-modifyWOCall=http://gci1devkenap1.cycle30.com:63083/service/cxf/v02/modifyWorkorder
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo #c30-Scheduler-webservice-debug-flag=L
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo #c30-Scheduler-webservice-debug-email=wfm-debug.cycle30.com
rem cr130
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo com.csgsystems.domain.arbor.businessobject.AccountLocateList.DisplayRowsPerPage=20
>> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo environment-name=DEV
rem >> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo Login.Application.Context.Name=powerclient_security_ldap
rem >> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo securityManager.login-mechanism=ldap
rem >> "%USERPROFILE%\.config\FXFrameConfiguration.properties" echo securityManager.password-encryption-enabled=false

rem ******************************************************************
rem  Call batch file to create the class path with every jar file in 
rem   the language pack directory and the lib directory.
rem ******************************************************************

for %%i in ("%LANGDIR%\*.jar") do call "%APPDIR%\lcp.bat" %%i
for %%i in ("%JARDIR%\*.jar") do call "%APPDIR%\lcp.bat" %%i
for %%i in ("%AXISJARDIR%\*.jar") do call "%APPDIR%\lcp.bat" %%i
rem *********************************************************
rem NOTE: The following commented out set commands show WHICH 
rem   environment variables need to be set.  Please note that
rem   these ABSOLUTELY POSITIVELY MUST BE SET IN THE WINDOWS
rem   environment AND NOT by this script.  The reason for this
rem   is so that they are available when running the GUI's 
rem   from inside GUI Designer.  If we only set them here GD
rem   wouldn't know about them and thus wouldn't be able to 
rem   run the GUI's.

set JDIC_DIR=C:\KenanFX2\FXLauncher\jdic-20061102-bin-win
set TUXDIR=C:\KenanFX2\tuxedo9.1
set TERRAPIN_DIR=C:\KenanFX2\FXLauncher\Terrapin
set FIELDTBLS32=bali.fml,ShieldWare.fml,tpadm,Usysflds
set FLDTBLDIR32=%TUXDIR%;%TUXDIR%\udataobj;%TERRAPIN_DIR%
set PATH=%TUXDIR%\bin;%TERRAPIN_DIR%;%JDIC_DIR%;%PATH%


start java -classpath %LOCALCLASSPATH% -Xms256M -Xmx512M -Xdebug -Xrunjdwp:transport=dt_socket,address=8010,server=y,suspend=n com.csgsystems.fxframe.main.FXSubsystemLauncher
rem start java -classpath %LOCALCLASSPATH% -Xms256M -Xmx512M com.csgsystems.fxframe.main.FXSubsystemLauncher
