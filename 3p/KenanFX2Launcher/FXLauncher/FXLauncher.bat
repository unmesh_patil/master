@echo off

rem
rem This batch file is used to execute the
rem FXLauncher application. The first thing
rem we check is whether or not a new version
rem of the launcher has been placed in the
rem current directory


rem *******************************
rem  Variables that can be changed
rem *******************************
rem Set this directory to whereever the jar directory is
set APPDIR=.
set JARDIR=lib
set LANGDIR=LanguagePacks
set LOCALCLASSPATH=.



rem ******************************************************
rem    NO USER EDITABLE PARTS BELOW THIS LINE
rem ******************************************************

if exist FXLauncherDeploy.jar goto extractJar

goto keepGoing

:extractJar

rem
rem Extract the contents of the jar to the appropriate
rem directory
rem
java -jar %JARDIR%\utils\FXJarTools-1.0.0.jar FXLauncherDeploy.jar %JARDIR%

rem
rem Delete the jar file so we don't do this again
rem
del /q FXLauncherDeploy.jar

:keepGoing


rem ******************************************************************
rem  Call batch file to create the class path with every jar file in 
rem   the language pack directory and the lib directory.
rem ******************************************************************
for %%i in ("%LANGDIR%\*.jar") do call "%APPDIR%\lcp.bat" %%i
for %%i in ("%JARDIR%\*.jar") do call "%APPDIR%\lcp.bat" %%i

rem *********************************************************
rem NOTE: The following commented out set commands show WHICH 
rem   environment variables need to be set.  Please note that
rem   these ABSOLUTELY POSITIVELY MUST BE SET IN THE WINDOWS
rem   environment AND NOT by this script.  The reason for this
rem   is so that they are available when running the GUI's 
rem   from inside GUI Designer.  If we only set them here GD
rem   wouldn't know about them and thus wouldn't be able to 
rem   run the GUI's.
rem *********************************************************
rem set JDIC_DIR=C:\KenanFX2\FXLauncher\jdic-20061102-bin-win
rem set TERRAPIN_DIR=C:\KenanFX2\FXLauncher\Terrapin
rem set FIELDTBLS32=bali.fml,ShieldWare.fml,tpadm,Usysflds
rem set FLDTBLDIR32=%TUXDIR%;%TUXDIR%\udataobj;%TERRAPIN_DIR%
rem set PATH=%TUXDIR%\bin;%TERRAPIN_DIR%;%JDIC_DIR%;%PATH%

start javaw -classpath %LOCALCLASSPATH% -Xms256M -Xmx512M com.csgsystems.fxframe.main.FXSubsystemLauncher
