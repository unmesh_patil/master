package com.cycle30.sdk.bulkJob.process;

import java.util.HashMap;

public interface ISubJobProcessResponse {

	 public HashMap getResponse();
	 public void setResponseMap(HashMap responseMap);
}
