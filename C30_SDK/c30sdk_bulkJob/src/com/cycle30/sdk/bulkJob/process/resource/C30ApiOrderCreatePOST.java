package com.cycle30.sdk.bulkJob.process.resource;

import java.util.HashMap;

import org.w3c.dom.Document;

import com.cycle30.sdk.bulkJob.C30BulkJobSqlUtils;
import com.cycle30.sdk.bulkJob.auth.C30BulkAuthManager;
import com.cycle30.sdk.bulkJob.auth.C30BulkAuthObject;
import com.cycle30.sdk.bulkJob.config.C30BulkConfigManager;
import com.cycle30.sdk.bulkJob.config.C30BulkJobTemplate;
import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessRequest;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResource;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResponse;
import com.cycle30.sdk.bulkJob.webservice.C30ApiRestHttpClient;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.utils.C30BulkJobUtils;

public class C30ApiOrderCreatePOST  implements ISubJobProcessResource{

	@Override
	public ISubJobProcessResponse handleRequest(ISubJobProcessRequest request) throws C30SDKException
	{

		try
		{
			String systemName = request.getRequestProperties().getSystemName();

			//Get the Auth Object for the corresponding Object.
			C30BulkAuthObject  authObject = C30BulkAuthManager.getInstance().getAuthObject(systemName);
			HashMap<String, C30BulkJobTemplate> templateMap = C30BulkConfigManager.getInstance().getBulkJobTemplateConfigMap();
			C30ApiRestHttpClient restApi = new C30ApiRestHttpClient();
			C30BulkJobTemplate jobTemplate = templateMap.get(request.getRequestProperties().getResourceName());
			
			
			Document doc = C30BulkJobUtils.populateDocument(request.getRequestProperties(), jobTemplate.getTemplatePayload());
			
			//Get Datasource connection from pool 
			C30JDBCDataSource datasource= C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();
			//Update the requestDoc in the database-
			C30BulkJobSqlUtils.changeSubJobStatus(request.getRequestProperties().getJobId(), request.getRequestProperties().getExternalTransactionId(),
					C30BulkJobValueConstants.JOB_STATUS_IN_PROGRESS , doc , null, datasource);
			//Freeup the connection and return pool
			C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(datasource.getConnection());
			
			restApi.postCall(doc, request.getRequestProperties().getExternalTransactionId(),
					"/"+ jobTemplate.getWebserviceMethodName(), authObject.toHashMap());

			
		}
		catch(Exception e)
		{
			throw new C30SDKException (e.getMessage(), e, "C30SDK-BULK-JOB-010");
		}

		return null;
	}

}
