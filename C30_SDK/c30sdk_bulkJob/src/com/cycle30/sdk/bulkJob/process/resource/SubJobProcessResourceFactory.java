package com.cycle30.sdk.bulkJob.process.resource;

import org.apache.log4j.Logger;

import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResource;
import com.cycle30.sdk.exception.C30SDKException;


/**
 * Factory class for  resource objects.
 *
 */
public class SubJobProcessResourceFactory {

	// Instantiate singleton
	private static final SubJobProcessResourceFactory self = new SubJobProcessResourceFactory();

	private static final Logger log = Logger.getLogger(SubJobProcessResourceFactory.class);

	private static final String EXCEPTION_SOURCE = "RESOURCE-FACTORY";


	//-------------------------------------------------------------------------
	/** Private constructor. 
	 */
	private SubJobProcessResourceFactory() {
		super();
	}


	//-------------------------------------------------------------------------
	/**
	 * Return instance of singleton class
	 * 
	 * @return ResourceRequestFactory instance.
	 */
	public static SubJobProcessResourceFactory getInstance() {
		return self;
	}




	/** Instantiate correct resource interface based on the resource nam
	 * 
	 * @param resourceType
	 * @return
	 * @throws C30SDKException
	 */
	public ISubJobProcessResource getResourceImpl(String resourceType) throws C30SDKException  {

		if (resourceType.startsWith(C30BulkJobValueConstants.RES_C30_ACCOUNT_FIND_GET)) {
			return new C30ApiAccountFindGET();
		}
		if (resourceType.startsWith(C30BulkJobValueConstants.RES_C30_ORDER_CREATE_POST)) {
			return new C30ApiOrderCreatePOST();
		}
		// If not found, 
		String error = "Unable to instantiate correct resource type based on resource type " + resourceType;
		log.error(error);

		throw new C30SDKException ("C30SDK-BULK-JOB-009");


	}
}
