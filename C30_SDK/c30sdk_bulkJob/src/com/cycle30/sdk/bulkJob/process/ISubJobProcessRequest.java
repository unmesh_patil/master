package com.cycle30.sdk.bulkJob.process;

import com.cycle30.sdk.bulkJob.process.request.SubJobProcessProperties;


/** This implementers the request related to different Vendors
 * 
 * @author rnelluri
 *
 */
public interface ISubJobProcessRequest {
	
	public void setRequestProperties(SubJobProcessProperties props);
    public SubJobProcessProperties getRequestProperties();

}

