package com.cycle30.sdk.bulkJob.process.resource;

import java.util.HashMap;

import javax.ws.rs.core.MultivaluedMap;

import com.cycle30.sdk.bulkJob.auth.C30BulkAuthManager;
import com.cycle30.sdk.bulkJob.auth.C30BulkAuthObject;
import com.cycle30.sdk.bulkJob.config.C30BulkConfigManager;
import com.cycle30.sdk.bulkJob.config.C30BulkJobTemplate;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessRequest;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResource;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResponse;
import com.cycle30.sdk.bulkJob.webservice.C30ApiRestHttpClient;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.utils.C30BulkJobUtils;

public class C30ApiAccountFindGET implements ISubJobProcessResource{

	@Override
	public ISubJobProcessResponse handleRequest(ISubJobProcessRequest request) throws C30SDKException
	{

		try
		{
			String systemName = request.getRequestProperties().getSystemName();

			//Get the Auth Object for the corresponding Object.
			C30BulkAuthObject  authObject = C30BulkAuthManager.getInstance().getAuthObject(systemName);
			HashMap<String, C30BulkJobTemplate> templateMap = C30BulkConfigManager.getInstance().getBulkJobTemplateConfigMap();
			C30ApiRestHttpClient restApi = new C30ApiRestHttpClient();
			C30BulkJobTemplate jobTemplate = templateMap.get(request.getRequestProperties().getResourceName());

			MultivaluedMap<String, String> params = C30BulkJobUtils.populateMultivaluedMap(request.getRequestProperties(), jobTemplate.getTemplatePayload()); 
			
			restApi.getCall(params, request.getRequestProperties().getExternalTransactionId(),
					"/"+ jobTemplate.getWebserviceMethodName(), authObject.toHashMap());
			
			//Update the response and status
			//TODO--
			
			
		}
		catch(Exception e)
		{
			throw new C30SDKException (e.getMessage(), e, "C30SDK-BULK-JOB-010");
		}

		return null;
	}

}
