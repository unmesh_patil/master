package com.cycle30.sdk.bulkJob.process.request;

import org.apache.log4j.Logger;

import com.cycle30.sdk.bulkJob.data.C30BulkJobSqlConstants;
import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessRequest;
import com.cycle30.sdk.exception.C30SDKException;



/**
 * 
 * Resource request factory.
 *
 */
public class SubJobProcessRequestFactory {

	// Instantiate singleton
	private static final SubJobProcessRequestFactory self = new SubJobProcessRequestFactory();

	private static final Logger log = Logger.getLogger(SubJobProcessRequestFactory.class);

	private static final String EXCEPTION_SOURCE = "REQUEST-FACTORY";


	//-------------------------------------------------------------------------
	/** Private constructor. */
	private SubJobProcessRequestFactory() {
		super();
	}


	//-------------------------------------------------------------------------
	/**
	 * Return instance of singleton class
	 * 
	 * @return ResourceRequestFactory instance.
	 */
	public static SubJobProcessRequestFactory getInstance() {
		return self;
	}


	/**Instantiate correct  request interface based on the resource name
	 * 
	 * @param resourceName
	 * @return
	 * @throws C30SDKException
	 */
	public ISubJobProcessRequest getRequestImpl(String resourceName) throws C30SDKException  {

		if (resourceName == null ) throw new C30SDKException ("C30SDK-BULK-JOB-008");

		if (resourceName.startsWith(C30BulkJobValueConstants.RES_C30_ACCOUNT_FIND_GET)) {
			return new C30APIProcessRequest();
		}
		if (resourceName.startsWith(C30BulkJobValueConstants.RES_C30_ORDER_CREATE_POST)) {
			return new C30APIProcessRequest();
		}
		// If not found
		String error = "Unable to instantiate correct request type based on resource name " + resourceName;
		log.error(error);

		throw new C30SDKException (error, "C30SDK-BULK-JOB-008");
	}

	/** Instantiate the correct Request based on the class supplied
	 * 
	 * @param resourceClass
	 * @return
	 * @throws C30SDKException
	 */
	public ISubJobProcessRequest getRequestImpl(Class resourceClass) throws C30SDKException  {
		try
		{
			return (ISubJobProcessRequest) resourceClass.newInstance();
		}
		catch(Exception e)
		{
			// If not found
			String error = "Unable to instantiate correct request type ("+resourceClass.toString()+") based on resource name " ;
			log.error(error);

			throw new C30SDKException (error, e, "PROV-005");
		}
	}
}
