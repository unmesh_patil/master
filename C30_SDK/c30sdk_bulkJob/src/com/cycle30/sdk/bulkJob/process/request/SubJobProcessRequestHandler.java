package com.cycle30.sdk.bulkJob.process.request;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessRequest;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResource;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResponse;
import com.cycle30.sdk.bulkJob.process.resource.SubJobProcessResourceFactory;
import com.cycle30.sdk.exception.C30SDKException;


public class SubJobProcessRequestHandler {

	private static final Logger log = Logger.getLogger(SubJobProcessRequestHandler.class);


	/**
	 * Return a JAX-RS 'Response' result from the client request.
	 * 
	 * @param resourceType
	 * @param headers
	 * @param ui
	 * @return response
	 */
	public ISubJobProcessResponse handleProvisionRequest (Integer systemId, HashMap inputRequest) throws C30SDKException {

		ISubJobProcessResponse response = null;

		//get Request Name
		String resourceType = (String) inputRequest.get(C30BulkJobValueConstants.ATTR_REQUEST_TEMPLATE_NAME);


		// Get the request associated with the type of resource requested
		ISubJobProcessRequest request = SubJobProcessRequestFactory.getInstance().getRequestImpl(resourceType);

		//SubJobProcessProperties requestProperties = setProperties(systemId, inputRequest);
		//request.setRequestProperties(requestProperties);

		// Get the associated resource instance
		ISubJobProcessResource resource = SubJobProcessResourceFactory.getInstance().getResourceImpl(resourceType);

		response = resource.handleRequest(request);

		return response;
	}

}
