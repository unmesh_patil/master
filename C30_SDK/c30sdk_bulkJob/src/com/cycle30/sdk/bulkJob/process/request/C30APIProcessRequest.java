package com.cycle30.sdk.bulkJob.process.request;

import com.cycle30.sdk.bulkJob.process.ISubJobProcessRequest;

public class C30APIProcessRequest implements ISubJobProcessRequest {

	SubJobProcessProperties prop;
	@Override
	public void setRequestProperties(SubJobProcessProperties props) {
		this.prop = props;
	}

	@Override
	public SubJobProcessProperties getRequestProperties() {
		return prop;
	}

}
