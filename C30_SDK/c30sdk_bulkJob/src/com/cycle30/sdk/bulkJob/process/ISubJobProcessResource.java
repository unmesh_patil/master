package com.cycle30.sdk.bulkJob.process;

import com.cycle30.sdk.exception.C30SDKException;


public interface ISubJobProcessResource {

	public ISubJobProcessResponse handleRequest(ISubJobProcessRequest request) throws C30SDKException;

}
