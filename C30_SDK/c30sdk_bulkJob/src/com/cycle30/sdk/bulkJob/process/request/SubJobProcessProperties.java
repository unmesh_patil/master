package com.cycle30.sdk.bulkJob.process.request;

import java.util.HashMap;

public class SubJobProcessProperties {

	private String systemName;
	private String resourceName;
	private HashMap requestMap;
	private Integer acctSegId;
	private String externalTransactionId;
	private String jobId;
	private String subJobDataRecord;
	
	/** Getters and Setters*/
	
	
	public String getSystemName() {
		return systemName;
	}
	public String getSubJobDataRecord() {
		return subJobDataRecord;
	}
	public void setSubJobDataRecord(String subJobDataRecord) {
		this.subJobDataRecord = subJobDataRecord;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public HashMap getRequestMap() {
		return requestMap;
	}
	public void setRequestMap(HashMap requestMap) {
		this.requestMap = requestMap;
	}
	public Integer getAcctSegId() {
		return acctSegId;
	}
	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}
	public String getExternalTransactionId() {
		return externalTransactionId;
	}
	public void setExternalTransactionId(String externalTransactionId) {
		this.externalTransactionId = externalTransactionId;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public SubJobProcessProperties(String systemName, 
			String resourceName, HashMap requestMap, Integer acctSegId,
			String externalTransactionId, String jobId, String subJobDataRecord) {
		super();
		this.systemName = systemName;
		this.resourceName = resourceName;
		this.requestMap = requestMap;
		this.acctSegId = acctSegId;
		this.externalTransactionId = externalTransactionId;
		this.jobId = jobId;
		this.subJobDataRecord = subJobDataRecord;
	}
	

}