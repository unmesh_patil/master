package com.cycle30.sdk.bulkJob.auth;

import org.apache.log4j.Logger;

import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;

public class C30BulkAuthManager {

	private static Logger log = Logger.getLogger(C30BulkAuthManager.class);
	private static C30BulkAuthManager instance = null;
	C30APIAuthObject c30ApiAuthObj = null;
	
	public static C30BulkAuthManager getInstance() {
		if (instance == null) {
			if( log != null ) { 
				log.info("Creating new instance of C30BulkAuthObject"); 
			}
			instance = new C30BulkAuthManager();
		}
		return instance;
	}

	/** Get the Auth Object based on the System Name
	 * 
	 * @param systemName
	 * @return
	 */
	public  C30BulkAuthObject getAuthObject(String systemName) {
		
		C30BulkAuthObject bulkAuthObject = null;
		if (systemName.equalsIgnoreCase(C30BulkJobValueConstants.SYSTEM_C30_API))
			return c30ApiAuthObj;
		
		return bulkAuthObject; 
	}
	
	/** Default manager that read the environment information and stores in the Auth object.
	 * 
	 */
	C30BulkAuthManager()
	{
		// This is to read the config and create the object correctly.
		try {
			C30SDKPropertyFileManager rb = C30SDKPropertyFileManager.getInstance();
			rb.addFromClassPath(C30BulkJobValueConstants.BULK_JOB_PROPERTIES_FILE_NAME);

			/** C30API Stuff
				C30API-EnvName=C3B1DEV
				C30API-SERVER_URL=https://ssp1devkenap1.cycle30.com:52010/C3B1/DEV
				C30API-X_ENV_ID=C3B1DEV
				C30API-X_ORG_ID=ACSTS1
				C30API-SecreteKey=5C6FC822A72436C19E97BEC61FA5E0BA
			*/
			String c30api_envName = rb.getString("C30API-EnvName");
			String c30api_serverUrl = rb.getString("C30API-SERVER_URL");
			String c30api_envId = rb.getString("C30API-X_ENV_ID");
			String c30api_orgId = rb.getString("C30API-X_ORG_ID");
			String c30api_secretKey = rb.getString("C30API-SecreteKey");
			
			c30ApiAuthObj = new C30APIAuthObject(c30api_envName, c30api_serverUrl, c30api_envId, c30api_orgId, c30api_secretKey);

			/** C30 Force.com stuff
			 * 
			 */
			
			
		} catch (java.util.MissingResourceException ex) {
			log.debug("BulkJob Manager Settings were not found; using defaults"); 
		} catch(Exception e) {
			log.error(e);
		}
	}
	
}
