package com.cycle30.sdk.bulkJob.auth;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/** this stores the authentication data for the C30API
 * 
 * @author rnelluri
 *
 */
public class C30APIAuthObject implements C30BulkAuthObject{
	/** 
	 * 
		C30API-EnvName=C3B1DEV
		C30API-SERVER_URL=https://ssp1devkenap1.cycle30.com:52010/C3B1/DEV
		C30API-X_ENV_ID=C3B1DEV
		C30API-X_ORG_ID=ACSTS1
		C30API-SecreteKey=5C6FC822A72436C19E97BEC61FA5E0BA
	 */
	String envName;
	String serverUrl;
	String envId;
	String orgId;
	String secretKey;
	String digest;
	String date;
	/** Getters and Setters*/
	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	public String getServerUrl() {
		return serverUrl;
	}
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}
	public String getEnvId() {
		return envId;
	}
	public void setEnvId(String envId) {
		this.envId = envId;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public C30APIAuthObject(String envName, String serverUrl, String envId,
			String orgId, String secretKey) {
		super();
		this.envName = envName;
		this.serverUrl = serverUrl;
		this.envId = envId;
		this.orgId = orgId;
		this.secretKey = secretKey;
		generateXDigestForApi();
	}

	/** Return the auth data as HashMap
	 * 
	 * @return
	 */
	public HashMap toHashMap()
	{
		HashMap authMap = new HashMap();
		//authMap.put("X-Trans-Id",);
		authMap.put("X-Env-Id",envId);
		authMap.put("X-Org-Id", orgId);
		authMap.put("Date",date);
		authMap.put("X-Digest",digest);
		authMap.put("URL",serverUrl);

		return authMap;
	}

	/** This method used to generate the XDigest.
	 * 
	 */
	private  void generateXDigestForApi() {
		try {

			String clientId = orgId;

			// Get client 'secret' hash value from CLIENT table cache.
			String secretHash = secretKey;

			// Get Date header value and parse into YYYYMMDD format
			SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
			Date d = new Date();
			date = formatter.format(d);
			formatter = new SimpleDateFormat("yyyyMMdd");
			String yyyymmdd = formatter.format(d);

			// Full 'salted' value:
			String saltValue = clientId + secretHash + yyyymmdd;


			// get hash value
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.reset();
			byte[] nameBytes = saltValue.getBytes();
			digest.update(nameBytes);
			byte[] hash = digest.digest();

			// convert hash bytes to hex string value
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xFF & hash[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}

			String md5Digest = hexString.toString();
			this.digest = md5Digest;

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
