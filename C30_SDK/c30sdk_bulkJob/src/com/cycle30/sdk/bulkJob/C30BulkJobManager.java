package com.cycle30.sdk.bulkJob;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.cycle30.sdk.bulkJob.data.C30BulkJobSqlConstants;
import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessRequest;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResource;
import com.cycle30.sdk.bulkJob.process.ISubJobProcessResponse;
import com.cycle30.sdk.bulkJob.process.request.SubJobProcessProperties;
import com.cycle30.sdk.bulkJob.process.request.SubJobProcessRequestFactory;
import com.cycle30.sdk.bulkJob.process.resource.SubJobProcessResourceFactory;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.utils.C30BulkJobUtils;

/** this class is responsible for managing all the bulk templates
 * 
 * @author rnelluri
 *
 */
public class C30BulkJobManager  implements Runnable{

	private static Logger log = Logger.getLogger(C30BulkJobManager.class);

	// The one-and-only instance of the BulkTemplate.
	private static C30BulkJobManager instance = null;

	public static C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();

	private static final String SLEEP_TIME   = "c30sdk-bulkjob-manager.sleep-time";
	private static final String START_OR_NOT = "c30sdk-bulkjob-manager.start";

	private C30JDBCDataSource dataSource = null;

	// The number of milliseconds to sleep between cache sweeps
	protected long sleepTime = 100000; // default in case property can't be found.

	// Flag indicating that the run-thread for the manager should stop. True
	// indicates that the run thread should die
	protected boolean die = false;

	//Flag indicating that the run-thread for the manager should start. True
	//indicates that the run thread should start
	protected boolean start = true;

	public static TreeSet<String> jobQueue = new TreeSet<String>();

	/**
	 * Returns the one-and-only instance of the BulkTemplate.
	 *
	 *@return    The one-and-only instance of the BulkTemplate.
	 */
	public static C30BulkJobManager getInstance() {
		if (instance == null) {
			if( log != null ) { 
				log.info("Creating new instance of BulkTemplate"); 
			}
			instance = new C30BulkJobManager();
		}
		return instance;
	}

	/**
	 * default constructor
	 */
	protected C30BulkJobManager() {

		try {
			C30SDKPropertyFileManager rb = C30SDKPropertyFileManager.getInstance();
			rb.addFromClassPath(C30BulkJobValueConstants.BULK_JOB_PROPERTIES_FILE_NAME);

			sleepTime = rb.getLong(SLEEP_TIME);
			start = rb.getBoolean(START_OR_NOT);
			
			dataSource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();   


		} catch (java.util.MissingResourceException ex) {
			log.debug("BulkJob Manager Settings were not found; using defaults"); 
		} catch(Exception e) {
			log.error(e);
		}

		//Start the thread.
		Thread t = new Thread(this);
		t.setPriority(Thread.MIN_PRIORITY);
		if (start) t.start();

	}


	//@Override
	public void run() {

		//Check the Jobs that are in Initiated and then start Processing them.
		while (!die) {

			try {
				Thread.sleep(sleepTime);
				log.debug("Find and Processing the New Bulk Jobs......"); 

				//Code to get the Orders and populated in the orderQueue
				C30BulkJobSqlUtils.getOpenJobs(dataSource);

				//Get the first openOrder and start processing
				while(jobQueue.size() != 0) {
					processJob(jobQueue.first());
					jobQueue.remove(jobQueue.first()); //Remove  first Element from orderQueue
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				log.error(ex);
			}

		}  // end while()

	}


	/**
	 * This is responsible for Processing the Order.
	 * @param jobKey concatenation of orderId + ":" + transactionId
	 * @throws C30SDKInvalidAttributeException 
	 * @throws C30SDKObjectConnectionException 
	 * @throws SQLException 
	 * @throws C30SDKToolkitException 
	 */
	private void processJob(String jobKey) throws C30SDKException, C30SDKInvalidAttributeException, SQLException, C30SDKToolkitException {

		//Change the status of the order to InProgress and Locked.
		int idx = jobKey.indexOf(":");
		String jobId = jobKey.substring(0, idx);		
		String transId = jobKey.substring(idx+1);
		log.debug("Working on the Job with Id = "+jobKey);
		C30BulkJobSqlUtils.changeJobStatus(jobId, transId, C30BulkJobValueConstants.JOB_STATUS_IN_PROGRESS, dataSource );

		// Start the jobs for each SubJob in the given Job.
		String query = C30BulkJobSqlConstants.GET_SUB_JOB_INFORMATION;
		ResultSet resultSet =null;
		
		try  {
			
			PreparedStatement ps = dataSource.getPreparedStatement(query);

			ps.setString(1, jobId);

			resultSet  = ps.executeQuery();

			while(resultSet.next()) {

				//String jobId     = resultSet.getString("JOB_ID");
				String subJobTemplateId      = resultSet.getString("JOB_TEMPLATE_ID");
				String acctSegId       = resultSet.getString("ACCT_SEG_ID");
				String clientOrgName   = resultSet.getString("CLIENT_ORG_NAME");
				String clientOrgId     = resultSet.getString("CLIENT_ORG_ID");
				String subJobtransactionId   = resultSet.getString("SUB_JOB_TRANSACTION_ID");
				String subJobDataRecord   = resultSet.getString("SUB_JOB_DATA_RECORD");
				String subJobClientId = resultSet.getString("CLIENT_ORDER_ID");
				java.sql.Timestamp subJobDesiredDate = resultSet.getTimestamp("SUB_JOB_DESIRED_DATE");

				String desiredDate =C30BulkJobUtils.getDateToString(new java.util.Date(subJobDesiredDate.getTime()));
				log.info("SubJob Id =" + subJobtransactionId +", Template Id = " + subJobTemplateId);

				// Change the status of SubJob to InProgress
				C30BulkJobSqlUtils.changeSubJobStatus(jobId, subJobtransactionId, C30BulkJobValueConstants.JOB_STATUS_IN_PROGRESS, dataSource );

				try {
					// Based on the Template determine the SubJob Type and create the Request C30API or Force or any other external System, based on the request.
					ISubJobProcessResponse response = null;
					// get the RequestName based on the Template
					ISubJobProcessRequest sjReq = SubJobProcessRequestFactory.getInstance().getRequestImpl(subJobTemplateId);
					//Set the properties
					SubJobProcessProperties props = setProperties(subJobTemplateId, acctSegId, clientOrgId, clientOrgName, subJobDataRecord, subJobtransactionId, subJobClientId, jobId);
					sjReq.setRequestProperties(props);
					sjReq.getRequestProperties().getRequestMap().put(C30BulkJobValueConstants.CLIENT_ORDER_ID, subJobClientId);
					sjReq.getRequestProperties().getRequestMap().put(C30BulkJobValueConstants.SUB_JOB_DESIRED_DATE, desiredDate);

					// Call the resource and make the call
					ISubJobProcessResource sjResource = SubJobProcessResourceFactory.getInstance().getResourceImpl(subJobTemplateId);
					response = sjResource.handleRequest(sjReq);
					
					C30BulkJobSqlUtils.changeSubJobStatus(jobId, subJobtransactionId, C30BulkJobValueConstants.JOB_STATUS_COMPLETED, dataSource );

				}
				catch( Exception e)
				{
					// Change the status of SubJob to Error and also update the request and response
					C30BulkJobSqlUtils.changeSubJobStatus(jobId, subJobtransactionId, C30BulkJobValueConstants.JOB_STATUS_IN_ERROR, dataSource );
					e.printStackTrace();
				}

				ps.close();
			}
		} catch(Exception e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-032"), e, "CONN-032");
		} 


	}

	/** Set the system Properties for the give request
	 * 
	 * @param subJobTemplateId
	 * @param acctSegId
	 * @param clientOrgId
	 * @param clientOrgName
	 * @param subJobDataRecord
	 * @param subJobtransactionId
	 * @param subJobClientId
	 * @param jobId
	 * @return
	 */
	private SubJobProcessProperties setProperties(String subJobTemplateId, String acctSegId, String clientOrgId,
			String clientOrgName, String subJobDataRecord, String subJobtransactionId, String subJobClientId, String jobId) {


		String systemName = "";
		if (subJobTemplateId.startsWith("C30API_"))
			systemName = C30BulkJobValueConstants.SYSTEM_C30_API;
		else
			systemName = "Other";

		SubJobProcessProperties sjProp = new SubJobProcessProperties(systemName, subJobTemplateId, new HashMap(),
				new Integer(acctSegId), subJobtransactionId, jobId, subJobDataRecord );
		return sjProp;
	}

	/** This method is used to load the default templates
	 * 
	 */
	private void loadSystemDefaultTemplates() {

	}


}
