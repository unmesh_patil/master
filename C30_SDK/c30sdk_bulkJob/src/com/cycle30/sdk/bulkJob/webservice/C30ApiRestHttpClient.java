package com.cycle30.sdk.bulkJob.webservice;

import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class C30ApiRestHttpClient {


	public static Logger log = Logger.getLogger(C30ApiRestHttpClient.class);

	public static Client client= Client.create();
	public static XPathFactory xpf = XPathFactory.newInstance();
	public static XPath xp = xpf.newXPath();

	/** Make GET Api Call
	 * 
	 * @param params
	 * @param transId
	 * @param path
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public static  String getCall(MultivaluedMap params, String transId, String path, HashMap headers) throws Exception
	{
		WebResource webResource = client.resource((String)headers.get("URL")); 
		WebResource.Builder wrb = webResource.path(path)
				.queryParams(params)
				.getRequestBuilder();

		wrb.header("X-Trans-Id", transId);
		wrb.header("X-Env-Id",  (String)headers.get("X-Env-Id"));
		wrb.header("X-Org-Id", (String)headers.get("X-Org-Id"));
		wrb.header("Date", (String)headers.get("Date"));
		wrb.header("X-Digest",(String)headers.get("X-Digest"));

		HostnameVerifier hv = new HostnameVerifier(){
			public boolean verify(String urlHostName, SSLSession session)
			{
				System.out.println("Warning: URL Host: " + urlHostName + " vs. "
						+ session.getPeerHost());
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		trustAllHttpsCertificates();


		String body =wrb.get(String.class);
		return body;
	}

	/** make POST http call
	 * 
	 * @param xmlRequest
	 * @param transId
	 * @param path
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public static  String postCall(Document xmlRequest, String transId, String path, HashMap headers) throws Exception {
		WebResource webResource = client.resource((String)headers.get("URL")); 
		WebResource.Builder wrb = webResource.path(path)
				.getRequestBuilder();

		wrb.header("X-Trans-Id", transId);
		wrb.header("X-Env-Id",  (String)headers.get("X-Env-Id"));
		wrb.header("X-Org-Id", (String)headers.get("X-Org-Id"));
		wrb.header("Date", (String)headers.get("Date"));
		wrb.header("X-Digest",(String)headers.get("X-Digest"));

		HostnameVerifier hv = new HostnameVerifier(){
			public boolean verify(String urlHostName, SSLSession session)
			{
				System.out.println("Warning: URL Host: " + urlHostName + " vs. "
						+ session.getPeerHost());
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		trustAllHttpsCertificates();

		ClientResponse clientResponse = wrb.post(ClientResponse.class, xmlRequest);

		int status = clientResponse.getStatus();
		String body = clientResponse.getEntity(String.class);
		log.info(body);
		return body;
	}

	private static void trustAllHttpsCertificates() throws Exception
	{
		//  Create a trust manager that does not validate certificate chains:
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc =javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	// Just add these two functions in your program 
	public static class  miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()
		{
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs)
		{
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs)
		{
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
						throws java.security.cert.CertificateException
						{
			return;
						}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
						throws java.security.cert.CertificateException
						{
			return;
						}
	}


}
