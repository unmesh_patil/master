package com.cycle30.sdk.bulkJob;

/** This holds the Template class
 * 
 * @author rnelluri
 *
 */
public class C30BulkTemplate {

	String templateId;
	String templateName;
	String templateType; // SystemDefault OR UserDefined
	String templateFileName;
	String jobType;
	Integer acctSegId;
	String templatePayload;
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTemplateType() {
		return templateType;
	}
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	public String getTemplateFileName() {
		return templateFileName;
	}
	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public Integer getAcctSegId() {
		return acctSegId;
	}
	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}
	public String getTemplatePayload() {
		return templatePayload;
	}
	public void setTemplatePayload(String templatePayload) {
		this.templatePayload = templatePayload;
	}
	@Override
	public String toString() {
		return "C30BulkTemplate [templateId=" + templateId + ", templateName="
				+ templateName + ", templateType=" + templateType
				+ ", templateFileName=" + templateFileName + ", jobType="
				+ jobType + ", acctSegId=" + acctSegId + ", templatePayload="
				+ templatePayload + "]";
	}
	
	public C30BulkTemplate(String templateId, String templateName,
			String templateType, String templateFileName, String jobType,
			Integer acctSegId, String templatePayload) {
		super();
		this.templateId = templateId;
		this.templateName = templateName;
		this.templateType = templateType;
		this.templateFileName = templateFileName;
		this.jobType = jobType;
		this.acctSegId = acctSegId;
		this.templatePayload = templatePayload;
	}
	
	/** This method is used to store the user defined templates in database.
	 * 
	 * @param templateId
	 * @param templateName
	 * @param templateType
	 * @param templateFileName
	 * @param jobType
	 * @param acctSegId
	 * @param templatePayload
	 */
	public void  storeInDatabase(String templateId, String templateName,
			String templateType, String templateFileName, String jobType,
			Integer acctSegId, String templatePayload) {
		
	}
	
}
