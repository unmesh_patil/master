package com.cycle30.sdk.bulkJob;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.sdk.bulkJob.data.C30BulkJobSqlConstants;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30DOMUtils;
import com.cycle30.sdk.util.C30ExternalCallDef;

public class C30BulkJobSqlUtils {

	private static Logger log = Logger.getLogger(C30BulkJobSqlUtils.class);
	public static C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();

	/**
	 * Update the orderstatus to a status and Locks the order.
	 * @param jobId
	 * @param transid
	 * @param jobStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void changeJobStatus(String jobId, 
			String transId,
			Integer jobStatusId, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
					{

		String query = C30BulkJobSqlConstants.CHANGE_JOB_STATUS;

		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,jobStatusId);
			ps.setString(2, jobId);
			ps.setString(3, transId);
			ps.executeQuery();
			ps.close();

		}  catch(Exception e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-002"),e,e.getMessage(),"ORD-002");
		} 
					}

	/**
	 * Update the Sub Job Status to a status and Locks the order.
	 * @param jobId
	 * @param transid
	 * @param subJobStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void changeSubJobStatus(String jobId, 
			String subJobTransId,
			Integer subJobStatusId, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
					{

		String query = C30BulkJobSqlConstants.CHANGE_SUB_JOB_STATUS;

		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,subJobStatusId);
			ps.setString(2, jobId);
			ps.setString(3, subJobTransId);
			ps.executeQuery();
			ps.close();

		}  catch(Exception e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-002"),e,e.getMessage(),"ORD-002");
		}  
					}

	/**
	 * Update the Sub Job Status to a status and Locks the order.
	 * @param jobId
	 * @param transid
	 * @param subJobStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void changeSubJobStatus(String jobId, 
			String subJobTransId,
			Integer subJobStatusId, Document inputDoc, Document outputDoc, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
					{

		String query = C30BulkJobSqlConstants.CHANGE_SUB_JOB_STATUS_AND_DOCS;

		try  {

			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,subJobStatusId);

			if (inputDoc != null ) 
				ps.setBlob(2, C30DOMUtils.documentToInputStream(inputDoc));
			else
				ps.setNull(2, java.sql.Types.BLOB);
			if (outputDoc != null ) 
				ps.setBlob(3, C30DOMUtils.documentToInputStream(outputDoc));
			else
				ps.setNull(3, java.sql.Types.BLOB);
			ps.setString(4, jobId);
			ps.setString(5, subJobTransId);

			ps.executeQuery();
			ps.close();

		}  catch(Exception e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-002"),e,e.getMessage(),"ORD-002");
		}  
					}

	/** To get the next JobId from the Sequence
	 * 
	 * @return
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static String getNextJobId(C30JDBCDataSource dataSource) 
			throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
			{

		String query = C30BulkJobSqlConstants.GET_NEXT_JOB_ID;
		String jobId ="";
		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ResultSet resultSet  =ps.executeQuery();

			if(resultSet.next())
			{
				jobId      = resultSet.getString("JOB_ID");
				ps.close();
			}
		}  
		catch(Exception e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("C30SDK-BULK-JOB-005"),e,e.getMessage(),"C30SDK-BULK-JOB-005");
		}  
		return jobId;
			}

	/** Get the Job Template Id from the sequence
	 * 
	 * @return
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static String getNextJobTemplateId(C30JDBCDataSource dataSource) 
			throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
			{

		String query = C30BulkJobSqlConstants.GET_NEXT_JOB_TEMPLATE_ID;
		String templateId ="";
		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ResultSet resultSet  =ps.executeQuery();

			if(resultSet.next()) {

				//String jobId     = resultSet.getString("JOB_ID");
				templateId      = resultSet.getString("TEMPLATE_ID");
			}
			ps.close();
		}  catch(Exception e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("C30SDK-BULK-JOB-005"),e,e.getMessage(),"C30SDK-BULK-JOB-005");
		} 
		return templateId;
			}


	/**
	 * This is used to get the open order information from database.
	 *  
	 * @throws C30SDKObjectConnectionException 
	 */
	public static void getOpenJobs(C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {

		try  {
			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_bulk_get_open_jobs");

			call.addParam(new C30CustomParamDef("job_id", 2, 300, 2));
			call.addParam(new C30CustomParamDef("transaction_id", 2, 300, 2));

			ArrayList paramValues = new ArrayList();

			dataSource.queryData(call, paramValues, 2);

			log.debug("Total open Jobs found = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0) {
				for(int i = 0; i < dataSource.getRowCount(); i++)  {

					if(dataSource.getValueAt(i,0) != null)   {  // order id

						// This populates the orderQueue with concatenation of
						// OrderIds + TransactionId
						String orderId  = (String)dataSource.getValueAt(i,0);
						String transId  = (String)dataSource.getValueAt(i,1);
						String queueKey = orderId + ":" + transId;
						C30BulkJobManager.jobQueue.add(queueKey);
					}
				}
			}

		}  catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-032"), e,e.getMessage(), "CONN-032");
		}  

	}

	/** Update Job Data
	 * 
	 * @param jobId
	 * @param transId
	 * @param jobDesiredStartDate
	 * @param isTestJob
	 * @param totalRecords
	 * @throws C30SDKObjectConnectionException
	 */
	public static void updateBulkJobDataInDateBase(String jobId,  String transId, Date jobDesiredStartDate, boolean isTestJob, 
			Integer totalRecords,C30JDBCDataSource dataSource ) throws C30SDKObjectConnectionException {

		log.debug("Update JobData in the database JobId- " + jobId);
		String query = C30BulkJobSqlConstants.UPDATE_JOB_STATUS;
		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			if (isTestJob)
				ps.setInt(1,1);
			else
				ps.setInt(1,0);

			ps.setTimestamp(2,  new java.sql.Timestamp(jobDesiredStartDate.getTime()));

			ps.setInt(3,totalRecords);
			ps.setString(4,jobId);
			ps.setString(5,transId);

			ps.executeQuery();
			ps.close();
		} catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("C30SDK-BULK-JOB-003"), e,e.getMessage(), "C30SDK-BULK-JOB-003");
		} 

	}

	/** Store the initial Job Data in database
	 * 
	 * @param orgId
	 * @param orgName
	 * @param order
	 * @param jobPayloadStream
	 * @param jobId
	 * @param jobType
	 * @param acctSegId
	 * @param transId
	 * @param auditId
	 * @throws C30SDKObjectConnectionException
	 */
	public static void storeBulkJobDataInDateBase(String orgId, String orgName,
			String jobPayloadStream, String jobId, String jobType,
			Integer acctSegId, String transId, String auditId, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {

		log.debug("Store JobData in the database JobId- " + jobId);
		String query = C30BulkJobSqlConstants.INSERT_INTO_BULK_JOB_DATA;
		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setString(1,jobId);
			ps.setString(2,jobId);
			ps.setString(3,jobType);
			ps.setInt(4,acctSegId);
			ps.setString(5,transId);
			ps.setString(6,auditId);
			ps.setString(7,orgName);
			ps.setString(8,orgId);

			InputStream is = new ByteArrayInputStream(jobPayloadStream.getBytes());
			ps.setBlob(9,is);

			ps.executeQuery();
			ps.close();
		} catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("C30SDK-BULK-JOB-003"), e,e.getMessage(), "C30SDK-BULK-JOB-003");
		} 

	}

	/** Create the SubJob records in database
	 * 
	 * @param jobId
	 * @param subJobClientOrderId
	 * @param templateName
	 * @param jobRecord
	 * @param acctSegId
	 * @param orgId
	 * @param orgName
	 * @param subJobTransactionId
	 * @param auditId
	 * @param jobDesiredStartDate
	 * @param isTestJob
	 * @throws C30SDKObjectConnectionException
	 */
	public static void createBulkJobSubRecords(String jobId,
			String subJobClientOrderId, String templateName, String jobRecord,
			Integer acctSegId, String orgId, String orgName,
			String subJobTransactionId, String auditId, Date jobDesiredStartDate, 
			boolean isTestJob, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException 
			{



		log.debug("Store  Sub JobData in the database JobId- " + jobId);
		String query = C30BulkJobSqlConstants.INSERT_INTO_JOB_RECORD_DATA;
		try  {

			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setString(1,jobId);
			ps.setString(2,subJobClientOrderId);
			ps.setString(3,templateName);
			ps.setInt(4,acctSegId);
			ps.setString(5,subJobTransactionId);
			ps.setString(6,auditId);
			ps.setTimestamp(7,  new java.sql.Timestamp(jobDesiredStartDate.getTime()));
			ps.setString(8,orgName);
			ps.setString(9,orgId);
			ps.setString(10,jobRecord);
			if (isTestJob)
				ps.setInt(11,1);
			else
				ps.setInt(11,0);

			ps.executeQuery();
			ps.close();
		} catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("C30SDK-BULK-JOB-004"), e,e.getMessage(), "C30SDK-BULK-JOB-004");
		} 

			}

	/** This is to update the Job Status and the complete date.
	 * 
	 * @param jobId
	 * @param jobType
	 * @param bulkJobData
	 * @param acctSegId
	 * @param orgId
	 * @param orgName
	 * @param jobTransactionId
	 * @param auditId
	 */
	public static void deleteBulkJob(String jobId, Integer acctSegId, String orgId,
			String orgName, String jobTransactionId, String auditId, Integer subJobStatusId, C30JDBCDataSource dataSource) throws C30SDKException{

		log.debug("Delete  the job with JobId- " + jobId);
		String query = C30BulkJobSqlConstants.UPDATE_JOB_STATUS;
		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,subJobStatusId);
			ps.setString(2,jobId);
			ps.setString(3,jobTransactionId);

			ps.executeQuery();
			ps.close();
		} catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("C30SDK-BULK-JOB-003"), e,e.getMessage(), "C30SDK-BULK-JOB-003");
		}

	}

}
