package com.cycle30.sdk.bulkJob;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.utils.C30BulkJobUtils;

/**
 * C30SDK C30BulkOrderJob 
 * @author Ranjith Nelluri
 */
public class C30BulkJob extends C30SDKObject implements Cloneable {

	private static Logger log = Logger.getLogger(C30BulkJob.class);

	String jobTransactionId="";
	String jobOrderIdPrefix="";
	boolean isTestJob=false;
	Integer totalRecords;
	Date jobDesiredStartDate = new Date();
	private C30JDBCDataSource c30BulkJobDataSource = null;
	
	/**
	 * Creates a new instance of C30BulkOrderJob.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30BulkJob(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30BulkJob(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30BulkJob.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30BulkOrderJob object.
	 * @return A copy of the C30BulkOrderJob object
	 */
	@Override
	public Object clone() {
		C30BulkJob copy = (C30BulkJob) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30BulkOrderJob.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject sdkObj = null;
		try {
			log.info("In the C30BulkOrderJob Process ()");
			setAttributesFromNameValuePairs();
			c30BulkJobDataSource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();   

			//This valdates the Input Validates the input attributes.
			validateAttributes();

			//Start Bulk Manager
			C30BulkJobManager.getInstance();

			//If the input request is to create the Order Job.
			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.C30BULKORDERJOB_CREATE)) {
				sdkObj = createBulkJob();
			}
			//If the input request is to create the Order Job.
			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.C30BULKORDERJOB_CANCEL)) {
				sdkObj = deleteBulkJob();
			}
			setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			//Freeing the used Datasource connection, so that it can return to pool.
			C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(c30BulkJobDataSource.getConnection());
		
		} catch (C30SDKException e) {
			log.error(e);
			sdkObj = this.createExceptionMessage(e);
		} catch (Exception e) {
			e.printStackTrace();
			sdkObj = this.createExceptionMessage(e);
		}

		// Return self-reference
		return  sdkObj;
	}

	/** Delete the BulkJob
	 * 
	 * @return
	 */
	private C30SDKObject deleteBulkJob()  throws C30SDKException {
		log.debug("Delete the Bulk Order Job in SDK...");

		Integer acctSegId = this.getAcctSegId();
		String jobId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_JOB_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String orgName       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ORG_NAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String orgId         = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ORG_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		jobTransactionId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String auditId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_AUDIT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		//Validate the Job if it is eligible for update
		//validate
		
		// Serialize - Store the order XML in order SDK Database.
		C30BulkJobSqlUtils.deleteBulkJob(jobId,  acctSegId, orgId, orgName, jobTransactionId, auditId, 
				C30BulkJobValueConstants.JOB_STATUS_DELETED_BY_USER, c30BulkJobDataSource);

		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_JOB_ID, jobId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		return this;
	}

	/** To create the Bulk Job
	 * 
	 * @return
	 * @throws C30SDKException
	 */
	private C30SDKObject createBulkJob() throws C30SDKException {
		log.debug("Creating the Bulk Order Job in SDK...");
		String bulkJobData = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_ORDER_JOB_DATA, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		Integer acctSegId = this.getAcctSegId();
		String jobType       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_JOB_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		//String jobId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_JOB_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String jobId = C30BulkJobSqlUtils.getNextJobId(c30BulkJobDataSource);
		String orgName       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ORG_NAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String orgId         = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ORG_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		jobTransactionId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String auditId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_AUDIT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		// Serialize - Store the order XML in order SDK Database.
		createBulkJob(jobId, jobType, bulkJobData, acctSegId, orgId, orgName, jobTransactionId, auditId);

		//Validate input job file...
		validateInputJobFile(bulkJobData);

		//Work on the Input File, validate and create the job rows in the sub job table.
		processJobRecords(jobId,  jobType, bulkJobData, acctSegId, orgId, orgName, jobTransactionId, auditId);

		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_JOB_ID, jobId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		return this;
	}

	/** Read through each and every job record and create them in the database
	 * 
	 * @param jobId
	 * @param jobType
	 * @param bulkJobData
	 * @param acctSegId
	 * @param orgId
	 * @param orgName
	 * @param transId
	 * @param auditId
	 * @throws C30SDKInvalidAttributeException 
	 */
	private void processJobRecords(String jobId, String jobType,
			String bulkJobData, Integer acctSegId, String orgId,
			String orgName, String transId, String auditId) throws C30SDKInvalidAttributeException {

		InputStream jobDataIS = new ByteArrayInputStream(bulkJobData.getBytes());
		java.io.DataInputStream din = new java.io.DataInputStream(jobDataIS);

		String jobRecord = "";

		//Use the transactionId and OrderId prefix from the given file and use them in the order calls..
		StringBuffer sb = new StringBuffer();
		try{
			while(( jobRecord = din.readLine()) != null)
			{
				if (jobRecord.startsWith("OrderIdPrefix"))
				{
					StringTokenizer st = new StringTokenizer(jobRecord,"=");
					st.nextToken();
					jobOrderIdPrefix= st.nextToken();
				}
				else if (jobRecord.startsWith("TotalRecords"))
				{
					StringTokenizer st = new StringTokenizer(jobRecord,"=");
					st.nextToken();
					totalRecords= new Integer(st.nextToken());
				}
				else if (jobRecord.startsWith("IsTestJob"))
				{
					StringTokenizer st = new StringTokenizer(jobRecord,"=");
					st.nextToken();
					if (st.nextToken().equalsIgnoreCase("TRUE")) 
						isTestJob = true;
					else
						isTestJob = false;
				}
				else if (jobRecord.startsWith("JobStartDate"))
				{
					StringTokenizer st = new StringTokenizer(jobRecord,"=");
					st.nextToken();
					String newDate = st.nextToken();
					jobDesiredStartDate = C30BulkJobUtils.getDateFromString(newDate);
				}
				else if (jobRecord.startsWith("#")) 
				{
					//Ignore this
				}
				else
				{
					//For all other records
					//Extract TemplateId, SerialNumber from the JobRecord
					StringTokenizer st = new StringTokenizer(jobRecord,",");
					String recordNumber = st.nextToken();
					String templateName = st.nextToken();
					String subJobClientOrderId = jobOrderIdPrefix+"_"+recordNumber;
					String subJobTransactionId = jobTransactionId+"_"+recordNumber;
					String updatedJobRecord = jobRecord.substring(recordNumber.length()+templateName.length()+2);

					C30BulkJobSqlUtils.createBulkJobSubRecords(jobId,subJobClientOrderId,  templateName, updatedJobRecord, acctSegId, orgId,
							orgName, subJobTransactionId, auditId, jobDesiredStartDate, isTestJob, c30BulkJobDataSource);

					//proRecord.process(transactionId+"-"+orderNumber,orderId+"-"+orderNumber,jobRecord);

				}
			}

			// Once all the jobs are processed then update the c30_sdk_bulk_job table with the status and desired date, total records.
			C30BulkJobSqlUtils.changeJobStatus(jobId, jobTransactionId, C30BulkJobValueConstants.JOB_STATUS_JOB_LOADED_IN_DB, c30BulkJobDataSource );
			C30BulkJobSqlUtils.updateBulkJobDataInDateBase(jobId, jobTransactionId,  jobDesiredStartDate, isTestJob, totalRecords, c30BulkJobDataSource );

		}
		catch(Exception ex){
			//If there is any exception, throw error and log the error in the table.

			log.error(ex);
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("C30SDK-BULK-JOB-002"),ex,"C30SDK-BULK-JOB-002");
		}
		finally{
			try{
				jobDataIS.close();
			}catch(Exception ex){}
		}



	}

	private void validateInputJobFile(String bulkJobData) {
		//Read the first record and last

	}

	/**
	 * This is to store the Order XML in the database. 
	 * @param jobPayloadStream
	 * @param orgName 
	 * @param orgId 
	 */
	private void createBulkJob(String jobId,  String jobType, String jobPayloadStream, 
			Integer acctSegId, 
			String orgId,
			String orgName, 
			String transId,
			String auditId) throws C30SDKException {

		try {
			log.info("Created Bulk order object in the database..");

			C30BulkJobSqlUtils.storeBulkJobDataInDateBase(orgId, orgName, jobPayloadStream, jobId, jobType, acctSegId, transId, auditId, c30BulkJobDataSource);
		}
		catch(C30SDKException e)
		{
			log.error(e);
			throw e;
		}
		catch(Exception e)
		{ 
			log.error(e);
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("C30SDK-BULK-JOB-001"),e,"C30SDK-BULK-JOB-001");
		}

	}



	/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 

		//Check if the JobId, JobType, JobData values present or not.
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_ORDER_JOB_DATA, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null )
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("C30SDK-BULK-JOB-503"), "C30SDK-BULK-JOB-503");
		//if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_JOB_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null )
		//	throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("C30SDK-BULK-JOB-502"), "C30SDK-BULK-JOB-502");
		//if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BULK_JOB_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null )
		//	throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("C30SDK-BULK-JOB-501"), "C30SDK-BULK-JOB-501");

	}


	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}