package com.cycle30.sdk.bulkJob.config;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.activation.DataSource;

import org.apache.log4j.Logger;

import com.cycle30.sdk.bulkJob.C30BulkJobSqlUtils;
import com.cycle30.sdk.bulkJob.data.C30BulkJobSqlConstants;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.utils.C30BulkJobUtils;

public class C30BulkConfigManager {
	private static Logger log = Logger.getLogger(C30BulkConfigManager.class);
	private static C30BulkConfigManager instance = null;
	private HashMap<String, C30BulkJobTemplate>  bulkJobTemplateConfigMap;

	public static C30BulkConfigManager getInstance() {
		if (instance == null) {
			if( log != null ) { 
				log.info("Creating new instance of C30BulkAuthObject"); 
			}
			instance = new C30BulkConfigManager();
		}
		return instance;
	}

	/** Default manager that read the environment information and stores in the Auth object.
	 * 
	 */
	C30BulkConfigManager()
	{
		// This is to read the config and create the object correctly.
		try {

			bulkJobTemplateConfigMap = getAuthenticateRequestHashMap();


		}  catch(Exception e) {
			log.error(e);
		}
	}

	/** This is used to get the Bulk Job Template Configuration from the database.
	 * 
	 * @return
	 * @throws C30SDKException
	 */
	private HashMap<String, C30BulkJobTemplate> getAuthenticateRequestHashMap() throws C30SDKException  {
		HashMap<String, C30BulkJobTemplate> response = new HashMap();

		log.info("Fetching all the TemplateJob data from database");
		String query = C30BulkJobSqlConstants.PROV_QUERY_GET_BULK_JOB_TEMPLATE_DATA;

		PreparedStatement ps = null;
		Connection conn = null;
		try
		{
			C30JDBCDataSource datasource  = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();
			conn = datasource.getConnection();

			ps = conn.prepareStatement(query);

			log.debug("Query : "+ query); 
			ResultSet resultSet  = ps.executeQuery();

			while (resultSet.next()) 
			{
				//Store the xmlTemplate into the file
				String xmlTemplate= "";
				if  (resultSet.getBlob("TEMPLATE_PAYLOAD") != null)
				{
					byte[] bdata = (resultSet.getBlob("TEMPLATE_PAYLOAD")).getBytes(1, (int) (resultSet.getBlob("TEMPLATE_PAYLOAD")).length());
					xmlTemplate= new String (bdata);

				}
				// If xml template is Empty then check read the template FileName
				if (xmlTemplate.equalsIgnoreCase(""))
				{
					String templateFileName = resultSet.getString("TEMPLATE_FILE_NAME");
					if (templateFileName != null && !templateFileName.equalsIgnoreCase("") && 
							!templateFileName.equalsIgnoreCase("null"))
					{
						// This is to read the config and create the object correctly.
						InputStream resourceAsStream = getClass().getResourceAsStream(templateFileName);
						xmlTemplate = C30BulkJobUtils.convertIStoString(resourceAsStream);
					}
				}

				// Constructor
				C30BulkJobTemplate dcJobTemplate = new C30BulkJobTemplate(new Integer(resultSet.getString("TEMPLATE_ID")), 
						resultSet.getString("TEMPLATE_NAME"), 
						resultSet.getString("TEMPLATE_TYPE"), 
						resultSet.getString("TEMPLATE_FILE_NAME"), 
						resultSet.getString("JOB_TYPE"),
						new Integer(resultSet.getString("ACCT_SEG_ID")),
						xmlTemplate,
						resultSet.getString("SYSTEM_ID"),
						resultSet.getString("WEBSERVICE_TYPE"),
						resultSet.getString("WEBSERVICE_TYPE_METHOD"),
						resultSet.getString("WEBSERVICE_METHOD_NAME") );

				response.put(resultSet.getString("TEMPLATE_NAME"), dcJobTemplate);
				log.debug(dcJobTemplate.toString());

			}//END WHILE

		}//END TRY
		catch(Exception e)
		{
			e.printStackTrace();
			throw new C30SDKException(e.getMessage(), e, "C30SDK-BULK-JOB-011");
		}
		finally {
			try 
			{
				ps.close();
				C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(conn);
			} 
			catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}		
		return response;
	}

	public HashMap<String, C30BulkJobTemplate> getBulkJobTemplateConfigMap() {
		return bulkJobTemplateConfigMap;
	}

	public void setBulkJobTemplateConfigMap(
			HashMap<String, C30BulkJobTemplate> bulkJobTemplateConfigMap) {
		this.bulkJobTemplateConfigMap = bulkJobTemplateConfigMap;
	}

}

