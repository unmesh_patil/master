package com.cycle30.sdk.bulkJob.config;

public class C30BulkJobTemplate {

	/**
	 * 
create table c30sdk.C30_SDK_BULK_JOB_TEMPLATE
(
  TEMPLATE_ID		VARCHAR2(144) NOT NULL,
  TEMPLATE_NAME		VARCHAR2(144),
  TEMPLATE_TYPE		VARCHAR2(80), -- user defined/ System Defined
  TEMPLATE_FILE_NAME		VARCHAR2(512),
  JOB_TYPE			VARCHAR2(40),  -- Order, AccountFind, etc.,
  ACCT_SEG_ID       NUMBER,
  TEMPLATE_PAYLOAD	BLOB, -- Input File (Text or CSV file)
  SYSTEM_ID VARCHAR2(100) , -- C30API, AWN, StreamWIDE, Force.com etc.,
  WEBSERVICE_TYPE VARCHAR2(100), -- REST, SOAP etc.,
  WEBSERVICE_TYPE_METHOD VARCHAR2(100), -- GET, POST, PUT, DELETE
  WEBSERVICE_METHOD_NAME VARCHAR2(100) -- AccountGet, Order POST etc.,  
);

	 */
	
	Integer templateId;
	String templateName;
	String templateType;
	String templateFileName;
	String jobType;
	Integer acctSegId;
	String templatePayload;
	String systemId;
	String webserviceType;
	String webserviceTypeMethod;
	String webserviceMethodName;
	public Integer getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTemplateType() {
		return templateType;
	}
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	public String getTemplateFileName() {
		return templateFileName;
	}
	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public Integer getAcctSegId() {
		return acctSegId;
	}
	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}
	public String getTemplatePayload() {
		return templatePayload;
	}
	public void setTemplatePayload(String templatePayload) {
		this.templatePayload = templatePayload;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getWebserviceType() {
		return webserviceType;
	}
	public void setWebserviceType(String webserviceType) {
		this.webserviceType = webserviceType;
	}
	public String getWebserviceTypeMethod() {
		return webserviceTypeMethod;
	}
	public void setWebserviceTypeMethod(String webserviceTypeMethod) {
		this.webserviceTypeMethod = webserviceTypeMethod;
	}
	public String getWebserviceMethodName() {
		return webserviceMethodName;
	}
	public void setWebserviceMethodName(String webserviceMethodName) {
		this.webserviceMethodName = webserviceMethodName;
	}
	
	/**Constructors */
	
	public C30BulkJobTemplate(Integer templateId, String templateName,
			String templateType, String templateFileName, String jobType,
			Integer acctSegId, String templatePayload, String systemId,
			String webserviceType, String webserviceTypeMethod,
			String webserviceMethodName) {
		super();
		this.templateId = templateId;
		this.templateName = templateName;
		this.templateType = templateType;
		this.templateFileName = templateFileName;
		this.jobType = jobType;
		this.acctSegId = acctSegId;
		this.templatePayload = templatePayload;
		this.systemId = systemId;
		this.webserviceType = webserviceType;
		this.webserviceTypeMethod = webserviceTypeMethod;
		this.webserviceMethodName = webserviceMethodName;
	}
	
	@Override
	public String toString() {
		return "C30BulkJobTemplate [templateId=" + templateId
				+ ", templateName=" + templateName + ", templateType="
				+ templateType + ", templateFileName=" + templateFileName
				+ ", jobType=" + jobType + ", acctSegId=" + acctSegId
				+ ", templatePayload=" + templatePayload + ", systemId="
				+ systemId + ", webserviceType=" + webserviceType
				+ ", webserviceTypeMethod=" + webserviceTypeMethod
				+ ", webserviceMethodName=" + webserviceMethodName + "]";
	}
	
	
}
