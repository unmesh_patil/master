package com.cycle30.sdk.bulkJob.data;

public class C30BulkJobValueConstants {

	public static final String BULK_JOB_PROPERTIES_FILE_NAME = "/C30SDK_BulkJob.properties";


	public static final Integer JOB_STATUS_INITIATED =   new Integer(0);
	public static final Integer JOB_STATUS_JOB_LOADING_IN_DB =   new Integer(1);
	public static final Integer JOB_STATUS_JOB_LOADED_IN_DB =   new Integer(2);
	public static final Integer JOB_STATUS_SUBJOB_LOADED_IN_DB =   new Integer(3);
	public static final Integer JOB_STATUS_IN_PROGRESS = new Integer(10);
	public static final Integer JOB_STATUS_COMPLETED =   new Integer(80);
	public static final Integer JOB_STATUS_CANCELLED =   new Integer(90);
	public static final Integer JOB_STATUS_DELETED_BY_USER =    new Integer(98);
	public static final Integer JOB_STATUS_IN_ERROR =    new Integer(99);
	
	public static final String RES_C30_ACCOUNT_FIND_GET =   "C30API_AccountFind_GET";
	public static final String RES_C30_ORDER_CREATE_POST =   "C30API_OrderCreate_POST";

	
	public static final String ATTR_REQUEST_TEMPLATE_NAME =   "RequestName";

	public static final String SYSTEM_C30_API =   "C30API";
	public static final String SYSTEM_FORCE_COM =   "FORCE.COM";

	public static final String SUB_JOB_DESIRED_DATE = "SubJobDesiredDate";
	public static final String CLIENT_ORDER_ID = "ClientOrderId";

	
}
