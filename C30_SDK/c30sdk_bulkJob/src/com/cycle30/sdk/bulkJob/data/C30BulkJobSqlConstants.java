package com.cycle30.sdk.bulkJob.data;

public class C30BulkJobSqlConstants {

	public static  String CHANGE_JOB_STATUS =                    
			"UPDATE C30_SDK_BULK_JOB " +
					"SET JOB_STATUS_ID= ? " +
					"WHERE JOB_ID = ? " +
					"AND TRANSACTION_ID = ?";

	public static  String CHANGE_SUB_JOB_STATUS =                    
			"UPDATE C30_SDK_BULK_SUB_JOB " +
					"SET SUB_JOB_STATUS_ID= ? " +
					"WHERE JOB_ID = ? " +
					"AND SUB_JOB_TRANSACTION_ID = ?";
	
	public static  String CHANGE_SUB_JOB_STATUS_AND_DOCS =                    
			"UPDATE C30_SDK_BULK_SUB_JOB " +
					"SET SUB_JOB_STATUS_ID= ?, " +
					"SUB_JOB_FINAL_REQUEST_DATA =?, " +
					" SUB_JOB_FINAL_RESPONSE_DATA =? " +
					"WHERE JOB_ID = ? " +
					"AND SUB_JOB_TRANSACTION_ID = ?";
	
	public static  String UPDATE_JOB_STATUS =                    
			"UPDATE C30_SDK_BULK_JOB " +
					"SET IS_TESTJOB= ?, JOB_DESIRED_DATE=?, JOB_TOTAL_RECORDS=? " +
					"WHERE JOB_ID = ? " +
					"AND TRANSACTION_ID = ?";
	
	public static  String UPDATE_JOB_STATUS_WITH_COMPLETE =                    
			"UPDATE C30_SDK_BULK_JOB " +
					"SET JOB_COMPLETE_DATE= sysdate, JOB_STATUS_ID= ? " +
					"WHERE JOB_ID = ? " +
					"AND TRANSACTION_ID = ?";

	
	public static  String GET_SUB_JOB_INFORMATION =                    
			"SELECT JOB_ID, JOB_TEMPLATE_ID, ACCT_SEG_ID,CLIENT_ORDER_ID, CLIENT_ORG_NAME, SUB_JOB_DATA_RECORD, CLIENT_ORG_ID, SUB_JOB_TRANSACTION_ID, SUB_JOB_DESIRED_DATE " +
					"FROM C30_SDK_BULK_SUB_JOB " +
					"WHERE JOB_ID = ? " +
					" AND SUB_JOB_STATUS_ID = 3" ;
	
	public static  String INSERT_INTO_BULK_JOB_DATA =  "insert into C30_SDK_BULK_JOB (JOB_ID, JOB_NAME, JOB_TYPE, ACCT_SEG_ID, IS_LOCKED, " +
			"TRANSACTION_ID,INTERNAL_AUDIT_ID ,  JOB_STATUS_ID,CLIENT_ORG_NAME, CLIENT_ORG_ID, PAYLOAD) " +
			"  values (?,?,?,?,0,?,?,0,?,?,?) ";
	
	public static  String INSERT_INTO_JOB_RECORD_DATA =  "insert into C30_SDK_BULK_SUB_JOB (JOB_ID, CLIENT_ORDER_ID, JOB_TEMPLATE_ID, ACCT_SEG_ID, IS_LOCKED, " +
			"SUB_JOB_TRANSACTION_ID,SUB_JOB_INTERNAL_AUDIT_ID , SUB_JOB_CREATE_DATE, SUB_JOB_DESIRED_DATE, SUB_JOB_STATUS_ID,CLIENT_ORG_NAME, CLIENT_ORG_ID, " +
			"SUB_JOB_DATA_RECORD, IS_TESTJOB) " +
			"  values (?,?,?,?,0,?,?, SYSDATE, ?,3,?,?,?,?) ";
	
	public static  String GET_NEXT_JOB_ID =                    
			"  select C30_SDK_BULK_JOB_ID_SEQ.nextval JOB_ID from DUAL";

	public static  String GET_NEXT_JOB_TEMPLATE_ID =                    
			"  select C30_SDK_BULK_TEMPLATE_ID_SEQ.nextval TEMPLATE_ID from DUAL";

	public static  String PROV_QUERY_GET_BULK_JOB_TEMPLATE_DATA =                    
			"  select TEMPLATE_ID, TEMPLATE_NAME, TEMPLATE_TYPE, TEMPLATE_FILE_NAME, " +
			" JOB_TYPE, ACCT_SEG_ID, SYSTEM_ID, WEBSERVICE_TYPE, WEBSERVICE_TYPE_METHOD, WEBSERVICE_METHOD_NAME, TEMPLATE_PAYLOAD " +
			" FROM C30SDK.c30_sdk_bulk_job_template";

	
}
