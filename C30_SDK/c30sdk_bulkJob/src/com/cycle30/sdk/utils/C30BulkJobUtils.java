package com.cycle30.sdk.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import javax.ws.rs.core.MultivaluedMap;

import org.w3c.dom.Document;

import com.cycle30.sdk.bulkJob.data.C30BulkJobValueConstants;
import com.cycle30.sdk.bulkJob.process.request.SubJobProcessProperties;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class C30BulkJobUtils {

	public static Date getDateFromString(String dateStr) throws ParseException{
		//2012-10-11T14:35:01
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		Date a = (Date)formatter.parse(dateStr);  
		return a;
	}
	
	public static String getDateToString(Date date) throws ParseException{
		//2012-10-11T14:35:01
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		String dateStr = formatter.format(date);
		return dateStr;
	}

	
	/** Convert to the String
	 * 
	 * @param ins
	 * @return
	 */
	public static String convertIStoString(InputStream ins)
	{
		String response="";
		InputStreamReader is=new InputStreamReader(ins);
		BufferedReader br=new BufferedReader(is);
		String read = null;
		StringBuffer sb = new StringBuffer();
		try {
			while((read = br.readLine()) != null) {
			sb.append(read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		response = sb.toString();
		return response;
	}

	
	/** Replace the template with the input string
	 * 
	 * @param subJobProcessProperties
	 * @param templatePayload
	 * @return
	 * @throws C30SDKToolkitException 
	 */
	public static Document populateDocument(SubJobProcessProperties subJobProcessProperties,
			String templatePayload) throws C30SDKToolkitException {

		// This method is responsible for replacing all the templates in the given JobData
		String inputData = subJobProcessProperties.getSubJobDataRecord();
		
		// Replace the OrderId and OrderName
		templatePayload = templatePayload.replace("temp_OrderId", (String) subJobProcessProperties.getRequestMap().get(C30BulkJobValueConstants.CLIENT_ORDER_ID));
		templatePayload = templatePayload.replace("temp_OrderName", (String) subJobProcessProperties.getRequestMap().get(C30BulkJobValueConstants.CLIENT_ORDER_ID));
		templatePayload = 	templatePayload.replace("OrderDesiredDt", (String) subJobProcessProperties.getRequestMap().get(C30BulkJobValueConstants.SUB_JOB_DESIRED_DATE));

		//Replace all the temp_data tokens in the template
		StringTokenizer st = new StringTokenizer(inputData,",");
		int idx=1;
		while(st.hasMoreTokens())
		{
			String tmpString = "temp_data_" + idx;
			templatePayload = templatePayload.replace(tmpString.trim(), st.nextToken().trim());
			idx++;
		}
		
		// tempatePayload is the final order String.
		Document returnDoc = C30DOMUtils.stringToDom(templatePayload);
		
		return returnDoc;
	}

	/** Populate the GET multivalued Map 
	 * 
	 * @param requestProperties
	 * @param templatePayload
	 * @return
	 */
	public static MultivaluedMap<String, String> populateMultivaluedMap(
			SubJobProcessProperties requestProperties, String templatePayload) {
		MultivaluedMap<String, String> params  = new MultivaluedMapImpl();
		// This method is responsible for replacing all the templates in the given JobData
		String inputData = requestProperties.getSubJobDataRecord();
	
		//Replace all the temp_data tokens in the template
		StringTokenizer inputTokens = new StringTokenizer(inputData,",");
		StringTokenizer templateTokens = new StringTokenizer(templatePayload,",");

		while(inputTokens.hasMoreTokens() && templateTokens.hasMoreTokens())
		{
			params.add(templateTokens.nextToken().trim(),  inputTokens.nextToken().trim());
		}
		
		return params;
	}
}
