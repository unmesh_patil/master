package com.cycle30.sdk.object.awnprepaid;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.utils.C30XMLResponseParser;

public class C30PrepaidPhoneInventoryUpdate extends C30SDKObject implements Cloneable {

	
	
	private static Logger log = Logger.getLogger(C30PrepaidPhoneInventoryUpdate.class);

	/**
	 * Creates a new instance of C30PrepaidPhoneInventoryUpdate.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidPhoneInventoryUpdate(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidPhoneInventoryUpdate(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30PrepaidAccountBalance.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	@Override
	public Object clone() {
		C30PrepaidPhoneInventoryUpdate copy = (C30PrepaidPhoneInventoryUpdate) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30AccountBalance.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
		try {
			setAttributesFromNameValuePairs();
			//	validateAttributes();
			String sessionId=null;
			String clientId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			Integer acctSegId = this.getAcctSegId();
			String transId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			
			String telephoneNumber = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String community= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_COMMUNITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String invStatus=(String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INV_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            Integer size=(Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String salesChannelId= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SALES_CHANNEL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            sessionId= getSessionId(clientId);
             if(sessionId!=null && !("".equals(sessionId))){
            String tnInventoryrXML=null;
			updatePhoneInventory(clientId, acctSegId, transId,sessionId,tnInventoryrXML );
			 setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             }
			lwo = this;

		} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
		} catch (C30ProvConnectorException e) {
			lwo = this.createExceptionMessage(e);
			e.printStackTrace();
		}

		return lwo;
	}

       /** Get the Session Id from Kenan System for sending in the further Transactions to GCI MulePOS
        * 
        * @param clientId
        * @return SessionId
     * @throws C30ProvConnectorException 
        */
		private String getSessionId(String clientId) throws C30ProvConnectorException {
			HashMap<String, String> PrepaidValidateSession = new HashMap<String, String>();
			PrepaidValidateSession.put(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,clientId);
			PrepaidValidateSession.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_MULEPOS_SESSION_ID_INQUIRY);
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryRequest(PrepaidValidateSession);
			String  sessionXML = (String) response.get("ResponseXML");
			String sessionId = C30XMLResponseParser.parseResponseForSessionId(sessionXML);
			
		return sessionId;
	}

		/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 
	}



	/**
	 * Internal method to get the outstanding account balance.  This method is 
	 * used when the account is to have their entire balance cleared.
	 * @param acctSegId 
	 * @param clientId 
	 * @param size 
	 * @param salesChannelId 
	 * @param invStatus 
	 * @param community 
	 * @throws C30SDKException 
	 * @throws Exception 
	 */
	private  void updatePhoneInventory(String clientId, Integer acctSegId, String transId,String gciSessionId, String tnInventoryXML) throws C30SDKException {

		   
		try{
			HashMap<String, String> PrepaidPhoneNumberUpdate = new HashMap<String, String>();
			
			PrepaidPhoneNumberUpdate.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctSegId.toString());
			PrepaidPhoneNumberUpdate.put("GCI-SessionId", gciSessionId);
			PrepaidPhoneNumberUpdate.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_PHONE_INVENTORY_UPADTE);
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryRequest(PrepaidPhoneNumberUpdate);
			
			//Still this config needs to be done.
		    this.setAttributeValue("C30SDKValueConstants.ATTRIB_PREPAID_PHONE_INVENTORY_UPDATE_XML", response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
		  
	}



	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}