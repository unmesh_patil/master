package com.cycle30.sdk.object.awnprepaid;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.utils.C30XMLResponseParser;

public class C30PrepaidVoucher extends C30SDKObject implements Cloneable {

	
	
	private static Logger log = Logger.getLogger(C30PrepaidVoucher.class);

	/**
	 * Creates a new instance of C30PrepaidVoucher.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK Voucher
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidVoucher(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidVoucher(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30PrepaidVoucher.class);
	}

	/**
	 * Initializes the base attributes.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30PrepaidVoucher object.
	 * @return A copy of the C30PrepaidVoucher object
	 */
	@Override
	public Object clone() {
		C30PrepaidVoucher copy = (C30PrepaidVoucher) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30PrepaidVoucher.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
//		String acctsegId = null;
		
		try {
			setAttributesFromNameValuePairs();
			
			String clientId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			String transId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String voucherPIN = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_VOUCHER_PIN, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String voucherAction = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_VOUCHER_ACTION, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			Integer acctSegId = this.getAcctSegId();
			
            HashMap<String, String> prepaidVoucherDetails = new HashMap<String, String>();
            prepaidVoucherDetails.put(Constants.VOUCHER_PIN,voucherPIN);
            prepaidVoucherDetails.put(Constants.EXTERNAL_TRANSACTION_ID,transId);
            prepaidVoucherDetails.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctSegId.toString());
            if(voucherPIN !=null && (voucherPIN.length()==12)){
            	log.info("OCS Voucher Details getting executed --- PIN "   + voucherPIN);
            	  //Get Card GUID
                prepaidVoucherDetails.put(Constants.VOUCHER_ACTION,voucherAction);
            	prepaidVoucherDetails.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_VOUCHER_STATUS);
                String cardGUID=getPrepaidVoucherStatus(prepaidVoucherDetails);
    			log.info("cardGUID = "+cardGUID);
                //NOT a Voucher STATUS action
                if(voucherAction!=null && !(Constants.VOUCHER_STATUS_ACTION.equalsIgnoreCase(voucherAction))){

                	//Voucher Block or UnBlock Action
                	if((Constants.VOUCHER_BLOCK_ACTION.equalsIgnoreCase(voucherAction) || Constants.VOUCHER_UNBLOCK_ACTION.equalsIgnoreCase(voucherAction)) && cardGUID !=null ){
                		prepaidVoucherDetails.put(Constants.VOUCHER_GUID,cardGUID);
                		blockUnBlockVoucher(prepaidVoucherDetails);
                	}
                	  
                }

            	
            }else if((voucherPIN !=null && (voucherPIN.length()==10)) && Constants.VOUCHER_STATUS_ACTION.equalsIgnoreCase(voucherAction) ){
            	log.info("Incomm Voucher getting executed --- PIN "   + voucherPIN);
                prepaidVoucherDetails.put(Constants.VOUCHER_ACTION,Constants.VOUCHER_STATUS_INCOMM_ACTION);
            	prepaidVoucherDetails.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_INCOMM_PREPAID_VOUCHER_STATUS);
             	getIncommVoucherStatus(prepaidVoucherDetails);
            } else if(voucherPIN !=null && (voucherPIN.length()==10) && (Constants.VOUCHER_BLOCK_ACTION.equalsIgnoreCase(voucherAction) || Constants.VOUCHER_UNBLOCK_ACTION.equalsIgnoreCase(voucherAction)) ){
            	String errMsg = "Incomm Voucher Can't be " +voucherAction;
    			String errCode = "INCOMM-300";
    			
    			log.error(errMsg);
    			throw new C30SDKException(errMsg, errMsg, errCode);	
            }else if((voucherPIN !=null && (voucherPIN.length()==11)) && Constants.VOUCHER_STATUS_ACTION.equalsIgnoreCase(voucherAction) ){
            	if(Constants.ACCT_SEG_ID_5.equals(acctSegId))throw  new C30SDKException("Blackhawk Voucher cannot be Used for this Tenant", "BLACKHAWK-400");
            	log.info("BlackHawk Voucher getting executed --- PIN "   + voucherPIN);
                prepaidVoucherDetails.put(Constants.VOUCHER_ACTION,Constants.VOUCHER_STATUS_BLACKHAWK_ACTION);
            	prepaidVoucherDetails.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_BLACK_HAWK_PREPAID_VOUCHER_STATUS);
            	getBlackHawkVoucherStatus(prepaidVoucherDetails);
            } else if(voucherPIN !=null && (voucherPIN.length()==11) && (Constants.VOUCHER_BLOCK_ACTION.equalsIgnoreCase(voucherAction) || Constants.VOUCHER_UNBLOCK_ACTION.equalsIgnoreCase(voucherAction)) ){
            	if(Constants.ACCT_SEG_ID_5.equals(acctSegId))throw  new C30SDKException("Blackhawk Voucher cannot be Used for this Tenant", "BLACKHAWK-400");
            	String errMsg = "BlackHawk Voucher Can't be " +voucherAction;
    			String errCode = "BLACKHAWK-300";
    			
    			log.error(errMsg);
    			throw new C30SDKException(errMsg, errMsg, errCode);	
            } else{
            	String errMsg = "Voucher with invalid digits length " ;
    			String errCode = "Invalid Voucher-300";    			
    			log.error(errMsg);
    			throw new C30SDKException(errMsg, errMsg, errCode);
            }
            
			lwo = this;

		} catch (C30SDKException e) {
			log.error(e);
			lwo = this.createExceptionMessage(e);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			log.error(e);
			lwo = this.createExceptionMessage(e);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			log.error(e);
			lwo = this.createExceptionMessage(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error(e);
			lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}



		/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 


	}



	/**
	 * Internal method to get the card GUID.   
	 * 
	 * @param PrepaidVoucherDetails 
	 * @return cardGUID
	 * @throws C30SDKException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws Exception 
	 */
	private  String getPrepaidVoucherStatus(HashMap<String, String> prePaidVoucherDetails) throws C30SDKException, ParserConfigurationException, SAXException, IOException {

		try{
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.ocsInquiryRequest(prePaidVoucherDetails);
			
			if(!(prePaidVoucherDetails.get(Constants.VOUCHER_ACTION).toString().equalsIgnoreCase(Constants.VOUCHER_STATUS_ACTION))){
				String  vocherDetailsXML = (String) response.get("ResponseXML");
				log.info("vocherDetailsXML = "+vocherDetailsXML);
				String cardGUID=null;
				HashMap<?, ?> ocsHashMap = C30XMLResponseParser.parseOCSResponse(vocherDetailsXML,"CardGUID");
				HashMap<?, ?> cardDetailsHashMap = (HashMap<?, ?>) ocsHashMap.get(0);
			
				if(cardDetailsHashMap.get("CardGUID")!=null){
					 cardGUID =  cardDetailsHashMap.get("CardGUID").toString();
					 log.info("cardGUID Got from the OCS " + cardGUID);
				}
				return cardGUID;
				
			} else {
			    this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_VOUCHER_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			    this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			    return "";
			}
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
	
		  
	}
	
	
	private void getIncommVoucherStatus(HashMap<String, String> prePaidVoucherDetails) throws C30SDKException{

		try{
			
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.incommInquiryRequest(prePaidVoucherDetails);		
			
			this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_VOUCHER_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
	
		  
	}
	
	
	private void getBlackHawkVoucherStatus(HashMap<String, String> prePaidVoucherDetails) throws C30SDKException{

		try{
			
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.blackHawkInquiryRequest(prePaidVoucherDetails);		
			
			this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_VOUCHER_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
	
		  
	}


	
	/**
	 * 
	 * @param prepaidVoucherDetails
	 * @throws C30SDKException
	 */
	 private void blockUnBlockVoucher(HashMap<String, String> prepaidVoucherDetails) throws C30SDKException { 
		
		try{
			
	        prepaidVoucherDetails.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_VOUCHER_STATUS);
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.ocsInquiryRequest(prepaidVoucherDetails);
		    this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_VOUCHER_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		    this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
	
}

	

	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}