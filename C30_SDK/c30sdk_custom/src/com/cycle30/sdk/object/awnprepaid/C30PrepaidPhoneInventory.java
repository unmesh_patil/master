package com.cycle30.sdk.object.awnprepaid;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.utils.C30XMLResponseParser;

public class C30PrepaidPhoneInventory extends C30SDKObject implements Cloneable {

	
	
	private static Logger log = Logger.getLogger(C30PrepaidPhoneInventory.class);

	/**
	 * Creates a new instance of C30PrepaidPhoneInventory.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidPhoneInventory(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidPhoneInventory(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30PrepaidAccountBalance.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	@Override
	public Object clone() {
		C30PrepaidPhoneInventory copy = (C30PrepaidPhoneInventory) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30AccountBalance.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
		try {
			setAttributesFromNameValuePairs();
			//	validateAttributes();
			String sessionId=null;
			String clientId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			Integer acctSegId = this.getAcctSegId();
			String transId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			
			String inventoryRequestAction= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_ACTION_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			
			
			String telephoneNumber = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String community= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_COMMUNITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String invStatus=(String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INV_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            Integer size=(Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String salesChannelId= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SALES_CHANNEL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String networkId= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NETWORK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String operatorId= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_OPERATOR_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String inventoryRequestPayload= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INVENTORY_PAYLOAD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            
            

            sessionId= getSessionId(clientId,transId,acctSegId);
             if(sessionId!=null && !("".equals(sessionId)) && (inventoryRequestAction==null)){
             getPhoneInventory(sessionId,acctSegId,transId,telephoneNumber,community,invStatus,salesChannelId,size,inventoryRequestAction );
			 setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             }else if(sessionId!=null && !("".equals(sessionId)) && (inventoryRequestAction !=null)){
            	 updatePhoneInventory(sessionId,acctSegId,transId,inventoryRequestPayload,telephoneNumber,invStatus,salesChannelId,networkId,operatorId,inventoryRequestAction); 
            	 setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             }
			lwo = this;

		} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
		} catch (C30ProvConnectorException e) {
			lwo = this.createExceptionMessage(e);
			e.printStackTrace();
		}

		return lwo;
	}

       /** Get the Session Id from Kenan System for sending in the further Transactions to GCI MulePOS
        * 
        * @param clientId
        * @return SessionId
     * @throws C30ProvConnectorException 
        */
		private String getSessionId(String clientId,String transId, Integer acctSegId ) throws C30ProvConnectorException {
			if(Constants.ACCT_SEG_ID_4.equals(acctSegId)){
			HashMap<String, String> PrepaidValidateSession = new HashMap<String, String>();
			PrepaidValidateSession.put(Constants.EXTERNAL_TRANSACTION_ID,transId);
			PrepaidValidateSession.put(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,clientId);
			PrepaidValidateSession.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_MULEPOS_SESSION_ID_INQUIRY);
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.prepaidPhoneInventoryInquiryRequest(PrepaidValidateSession);
			String  sessionXML = (String) response.get("ResponseXML");
			String sessionId = C30XMLResponseParser.parseResponseForSessionId(sessionXML);
			
		return sessionId;
			}else{
				return "SessionIdNotRequired";
			}
	}

		/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 
	}



	/**
	 * This method will take care to retrieveTN Inventory details.
	 * @param acctSegId 
	 * @param size 
	 * @param salesChannelId 
	 * @param invStatus 
	 * @param community 
	 * @throws C30SDKException 
	 * @throws Exception 
	 */
	private  void getPhoneInventory(String gciSessionId,Integer acctSegId, String transId, String telephoneNumber, String community, String invStatus, String salesChannelId, Integer size, String inventoryRequestAction ) throws C30SDKException {

		    if( (telephoneNumber !=null && community!=null) || telephoneNumber !=null ){
		    	size=1;
		    }
		    
		    if(size == null){
		    	size=5;
		    }
		
		try{
			HashMap<String, String> PrepaidPhoneNumberInquiry = new HashMap<String, String>();
			

			PrepaidPhoneNumberInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transId);
			PrepaidPhoneNumberInquiry.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctSegId.toString());
			
			//PrepaidPhoneNumberInquiry.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctSegId.toString());
			PrepaidPhoneNumberInquiry.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NUMBER,telephoneNumber);
			PrepaidPhoneNumberInquiry.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_COMMUNITY,community);
			PrepaidPhoneNumberInquiry.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SALES_CHANNEL_ID,salesChannelId);
			PrepaidPhoneNumberInquiry.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INV_STATUS,invStatus);
			PrepaidPhoneNumberInquiry.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SIZE,size.toString());
			PrepaidPhoneNumberInquiry.put(Constants.ACCT_SEG_ID,acctSegId.toString());
			HashMap<?, ?> response= new HashMap();
			if(Constants.ACCT_SEG_ID_4.equals(acctSegId)){			
			
			PrepaidPhoneNumberInquiry.put(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID, gciSessionId);
			PrepaidPhoneNumberInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_PHONE_INVENTORY_INQUIRY);
			log.info("Send Prepaid Phone Inquiry Parameters   : " +community   +"  "  +telephoneNumber  +"  "  +salesChannelId  +"  "  +invStatus  +"  "  +size.toString()  );	
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			response = request.prepaidPhoneInventoryInquiryRequest(PrepaidPhoneNumberInquiry);
			
			}else if(Constants.ACCT_SEG_ID_5.equals(acctSegId)) {
				PrepaidPhoneNumberInquiry.remove(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID);
				PrepaidPhoneNumberInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_ACS_PHONE_INVENTORY_INQUIRY);
				log.info("Send Prepaid Phone Inquiry Parameters   : " +community   +"  "  +telephoneNumber  +"  "  +salesChannelId  +"  "  +invStatus  +"  "  +size.toString()  );	
				C30ProvisionRequest request = C30ProvisionRequest.getInstance();
				response = request.prepaidPhoneInventoryInquiryACSRequest(PrepaidPhoneNumberInquiry);
			}
			
			this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_PHONE_INVENTORY_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);		    
			
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
		  
	}

	/**
	 *  This method will take care of 
	 * @param gciSessionId
	 * @param inventoryRequestPayload
	 * @param inventoryRequestAction 
	 * @param operatorId 
	 * @param networkId 
	 * @param salesChannelId 
	 * @param invStatus 
	 * @param telephoneNumber 
	 * @throws C30SDKException
	 */
	private  void updatePhoneInventory(String gciSessionId,Integer acctSegId, String transId, String inventoryRequestPayload, String telephoneNumber, String invStatus, String salesChannelId, String networkId, String operatorId, String inventoryRequestAction ) throws C30SDKException {

		
		try{
			HashMap<String, String> PrepaidPhoneInventoryUpdate = new HashMap<String, String>();
			
			PrepaidPhoneInventoryUpdate.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INVENTORY_PAYLOAD,inventoryRequestPayload);
			PrepaidPhoneInventoryUpdate.put(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID, gciSessionId);
			PrepaidPhoneInventoryUpdate.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_NUMBER,telephoneNumber);
			PrepaidPhoneInventoryUpdate.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_SALES_CHANNEL_ID,salesChannelId);
			PrepaidPhoneInventoryUpdate.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_INV_STATUS,invStatus);
			PrepaidPhoneInventoryUpdate.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_OPERATOR_ID,operatorId);
			PrepaidPhoneInventoryUpdate.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_ACTION_TYPE,inventoryRequestAction);
			PrepaidPhoneInventoryUpdate.put(C30SDKAttributeConstants.ATTRIB_KENAN_PREPAID_ACTION_TYPE,inventoryRequestAction);
			PrepaidPhoneInventoryUpdate.put(Constants.ACCT_SEG_ID,acctSegId.toString());
			
			log.info("Send Prepaid Phone Update -- InventoryRequestObject as Payload   : " +inventoryRequestPayload );
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = new HashMap();
			if(Constants.ACCT_SEG_ID_4.equals(acctSegId)){
				PrepaidPhoneInventoryUpdate.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_GCI_PHONE_INVENTORY_UPADTE);
				
			 response = request.prepaidPhoneInventoryInquiryRequest(PrepaidPhoneInventoryUpdate);
			}else if(Constants.ACCT_SEG_ID_5.equals(acctSegId)){
				PrepaidPhoneInventoryUpdate.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_ACS_PHONE_INVENTORY_UPADTE);
				PrepaidPhoneInventoryUpdate.remove(Constants.PREPAID_GCI_ATTRIBUTE_SESSION_ID);
				response = request.prepaidPhoneInventoryInquiryACSRequest(PrepaidPhoneInventoryUpdate);
			}
		    this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_PHONE_INVENTORY_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
		  
	}



	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}