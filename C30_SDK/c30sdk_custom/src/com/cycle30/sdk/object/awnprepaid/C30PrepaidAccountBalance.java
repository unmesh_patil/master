package com.cycle30.sdk.object.awnprepaid;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

public class C30PrepaidAccountBalance extends C30SDKObject implements Cloneable {

	
	
	private static Logger log = Logger.getLogger(C30PrepaidAccountBalance.class);

	/**
	 * Creates a new instance of C30Payment.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidAccountBalance(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidAccountBalance(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30PrepaidAccountBalance.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	@Override
	public Object clone() {
		C30PrepaidAccountBalance copy = (C30PrepaidAccountBalance) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30AccountBalance.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
		try {
			setAttributesFromNameValuePairs();
			//	validateAttributes();
            HashMap<String, String> balanceObject = new HashMap<String, String>();
			String clientId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			Integer acctSegId = this.getAcctSegId();
			
			String transId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			String balanceAction= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_ACTION, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            String  subscriberId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_SUBSCRIBER_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

             if(balanceAction == null ){
            	 getPrepaidAccountBalance(acctSegId, transId,subscriberId );
			     setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             }else {
            	String balanceTypeLabel= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_LABEL_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    			String balanceReason= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_REASON, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    			String balanceValue= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_VALUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    			String balanceExpirationDate = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_EXPIRATION_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    			String voucherPIN = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_VOUCHER_PIN, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
      			String subscriberCLI = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_SUBSCRIBER_CLI, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    			String subscriberCurrentPlan = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_CHARGING_EVENT_LABEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    			String reversalRequestId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_REVERSAL_REQUEST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_ACTION,balanceAction);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_LABEL_TYPE,balanceTypeLabel);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_REASON,balanceReason);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_VALUE,balanceValue);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_EXPIRATION_DATE, balanceExpirationDate);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_VOUCHER_PIN, voucherPIN);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_SUBSCRIBER_CLI, subscriberCLI);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_OCS_CHARGING_EVENT_LABEL, subscriberCurrentPlan);
				balanceObject.put(C30SDKAttributeConstants.ATTRIB_REVERSAL_REQUEST_ID, reversalRequestId);
				//UPDATE Balance
				updatePrepaidBalance(acctSegId,transId,subscriberId,balanceAction, balanceObject);
            	 
           		setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            	}
            
			lwo = this;

		} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}



		/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 
	}



	/**
	 * This method to get the Balance of the given Subscriber Number.
	 * @param acctSegId 
	 * @param clientId 
	 * @return 
	 * @return the account balance
	 * @throws C30SDKException 
	 * @throws Exception 
	 */
	private  void getPrepaidAccountBalance(Integer acctSegId, String transId, String subscriberId) throws C30SDKException {

		try{
			HashMap<String, String> PrepaidBalInquiry = new HashMap<String, String>();
			PrepaidBalInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transId);
			PrepaidBalInquiry.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctSegId.toString());
			PrepaidBalInquiry.put(Constants.SUBSCRIBER_ID,subscriberId);
			PrepaidBalInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_BALANCE_INQUIRY);
			log.info("Send Prepaid Balance Inquiry for Subscriber  : "+subscriberId);	
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.ocsInquiryRequest(PrepaidBalInquiry);
		    this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_BALANCE_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
		  
	}

	
/**
 * This method will Update the Prepaid Balance Based on Action define in the input Balance Object
 * @param clientId
 * @param acctSegId
 * @param transId
 * @param subscriberId
 * @param balanceAction
 * @param balanceObject
 * @throws C30SDKException 
 */
	private void updatePrepaidBalance(Integer acctSegId,String transId,String subscriberId, String balanceAction,HashMap balanceObject) throws C30SDKException {
		
		boolean checkReversal=false;
		
		try{
			
			HashMap<String, String> PrepaidBalanceUpdate = new HashMap<String, String>();
			PrepaidBalanceUpdate.put(Constants.EXTERNAL_TRANSACTION_ID,transId);
			PrepaidBalanceUpdate.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctSegId.toString());
			PrepaidBalanceUpdate.put(Constants.SUBSCRIBER_ID,subscriberId);
			PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_ACTION,balanceAction);
			PrepaidBalanceUpdate.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_PREPAID_SUBSCRIBER_BALANCE_INQUIRY);
			
			if(Constants.PREPAID_WALLET_REFILL.equalsIgnoreCase(balanceAction)){
				
				if(Constants.PREPAID_WALLET_BALANCE_LABEL.equalsIgnoreCase((String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE))){
				
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_TYPE,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_REASON,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_REASON));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_Value,(String)balanceObject.get(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_VALUE));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_EXPIRATION_DATE,(String)balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE));
				}else{
					throw new C30ProvConnectorException(" Error Message : " + "Only COMMON Balance should be used for this action", "PROV-399");
				}
					
			}else if(Constants.PREPAID_VOUCHER_REDEEM.equalsIgnoreCase(balanceAction) && balanceObject.get(Constants.VOUCHER_PIN).toString().length()==12){
				PrepaidBalanceUpdate.put(Constants.VOUCHER_PIN,(String)balanceObject.get(Constants.VOUCHER_PIN));
				if(balanceObject.get(Constants.PREPAID_SUBSCRIBER_CLI)!= null){
					PrepaidBalanceUpdate.put(Constants.PREPAID_SUBSCRIBER_CLI,(String)balanceObject.get(Constants.PREPAID_SUBSCRIBER_CLI));
				}else{
					throw new C30ProvConnectorException(" Error Message from OCS : " + "No destination CLI provided", "OCS-399");
				}
				
			}else if(Constants.PREPAID_VOUCHER_REDEEM.equalsIgnoreCase(balanceAction) && balanceObject.get(Constants.VOUCHER_PIN).toString().length()==10){
				if(balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE)!=null){
					//REDEEM INCOMM VOUCHER
					redeemIncommVocher(PrepaidBalanceUpdate,balanceAction,balanceObject);
				}else{
					throw new C30ProvConnectorException(" Error Message from INCOMM : " + "Expiration is Required for Redeeming Incomm Voucher", "INCOMM-111");
				}
			}else if(Constants.PREPAID_VOUCHER_REDEEM.equalsIgnoreCase(balanceAction) && (balanceObject.get(Constants.VOUCHER_PIN).toString().length()==11)){
				if(Constants.ACCT_SEG_ID_5.equals(acctSegId))throw  new C30ProvConnectorException(" Error Message from Provisioning Connector  : " + "Blackhawk Voucher cannot be redeemed for this Tenant", "PROV-400");
					if(balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE)!=null && balanceObject.get(Constants.REVERSAL_REQUEST_ID)==null){
					//REDEEM BLACKHAWK VOUCHER
					redeemBlackHawkVoucher(PrepaidBalanceUpdate,balanceAction,balanceObject);
					}else if(balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE)!=null && balanceObject.get(Constants.REVERSAL_REQUEST_ID)!=null) {
						//REVERSAL BLACKHAWK VOUCHER
						checkReversal=reversalBlackHawkVoucher(PrepaidBalanceUpdate,balanceAction,balanceObject);
					}else{
						throw new C30ProvConnectorException(" Error Message from BLACKHAWK : " + "Expiration Date is Required for Redeeming BlackHawk Voucher", "BLACKHAWK-111");
					}
			
			}
			else if(Constants.PREPAID_BALANCE_REFILL.equalsIgnoreCase(balanceAction)){
				if(!(Constants.PREPAID_WALLET_BALANCE_LABEL.equalsIgnoreCase((String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE)))){
				
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_TYPE,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_REASON,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_REASON));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_Value,(String)balanceObject.get(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_VALUE));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_EXPIRATION_DATE,(String)balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE));
				}else{
					throw new C30ProvConnectorException(" Error Message : " + "COMMON Balance Cannot be used for this action", "PROV-399");
				}
			
			}else if(Constants.PREPAID_EXTEND_EXPIRYDATE.equalsIgnoreCase(balanceAction)){
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_TYPE,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_REASON,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_REASON));
				PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_EXPIRATION_DATE,(String)balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE));
			
			}else if(Constants.PREPAID_PLAN_RECHARGE_RESERVE.equalsIgnoreCase(balanceAction)){
//				if(Constants.PREPAID_WALLET_BALANCE_LABEL.equalsIgnoreCase((String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE))){
					PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_TYPE,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE));
					//Send the Plan Name
					PrepaidBalanceUpdate.put(Constants.PREPAID_CHARGE_EVENT_LABEL,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_TYPE));					
//					PrepaidBalanceUpdate.put(Constants.PREPAID_CHARGE_EVENT_LABEL,(String)balanceObject.get(Constants.PREPAID_BALANCE_CHARGING_EVENT_LABEL));
					PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_Value,(String)balanceObject.get(C30SDKAttributeConstants.ATTRIB_OCS_BALANCE_VALUE));
					PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_REASON,(String)balanceObject.get(Constants.PREPAID_BALANCE_BALANCE_REASON));
					PrepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_EXPIRATION_DATE,(String)balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE));
					if(balanceObject.get(Constants.PREPAID_SUBSCRIBER_CLI)!= null){
						PrepaidBalanceUpdate.put(Constants.PREPAID_SUBSCRIBER_CLI,(String)balanceObject.get(Constants.PREPAID_SUBSCRIBER_CLI));
					}else{
						throw new C30ProvConnectorException(" Error Message from OCS : " + "No destination CLI provided", "OCS-399");
					}
//				}else{
//					throw new C30ProvConnectorException(" Error Message : " + "Only COMMON Balance should be used for this action", "PROV-399");
//				}
	
			
			}
			 if(!checkReversal){
			log.info("Send Prepaid Balance Update for Subscriber  : "+subscriberId);
			log.info("PrepaidBalanceUpdate  : "+PrepaidBalanceUpdate.toString());			
			C30ProvisionRequest request = C30ProvisionRequest.getInstance();
			HashMap<?, ?> response = request.ocsInquiryRequest(PrepaidBalanceUpdate);
		    this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_BALANCE_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			 }else{
				 
			 }
		
		}
		catch(C30ProvConnectorException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
			
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}
}


	private void redeemBlackHawkVoucher(HashMap<String, String> prepaidBalanceUpdate,String balanceAction, HashMap<String, String> balanceObject) throws C30ProvConnectorException, C30SDKException {
	log.info("BalckHawk Vouchers Redemption Method");
	C30ProvisionRequest request = C30ProvisionRequest.getInstance();	
	HashMap<String, String> redeemBlackHawkMap = new HashMap<String, String>();
	redeemBlackHawkMap.put(Constants.EXTERNAL_TRANSACTION_ID,prepaidBalanceUpdate.get(Constants.EXTERNAL_TRANSACTION_ID));
	redeemBlackHawkMap.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,prepaidBalanceUpdate.get(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID));
	redeemBlackHawkMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_BLACK_HAWK_PREPAID_VOUCHER_STATUS);
	redeemBlackHawkMap.put(Constants.VOUCHER_PIN,(String) balanceObject.get(Constants.VOUCHER_PIN));
	redeemBlackHawkMap.put(Constants.REVERSAL_REQUEST_ID,(String) balanceObject.get(Constants.REVERSAL_REQUEST_ID));
	redeemBlackHawkMap.put(Constants.VOUCHER_ACTION,Constants.VOUCHER_STATUS_BLACKHAWK_ACTION);
	
	//Make  a call to BlackHawk system to first get the Voucher Status
	HashMap<?, ?> response = request.blackHawkInquiryRequest(redeemBlackHawkMap);
	//If response is not Null from BlackHawk Status call , then it is Active Card.
	//Make call to OCS to update the wallet Balance 
	balanceAction=Constants.PREPAID_WALLET_REFILL;
	prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_ACTION,balanceAction);
	prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_TYPE,Constants.PREPAID_BALANCE_TYPE_COMMON);
	prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_REASON,"BLACKHAWK_VOUCH_WALLET_REFILL_"+getBlackhawkVoucherValue((String) response.get("FaceValue")));
	prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_Value,getBlackhawkVoucherValue((String) response.get("FaceValue")));
	prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_EXPIRATION_DATE,(String)balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE));
	
	log.info("prepaidBalanceUpdate = "+prepaidBalanceUpdate.toString());

	if(response.get("ResponseXML") !=null){
		if(redeemBlackHawkMap!=null){
			redeemBlackHawkMap.remove(Constants.VOUCHER_ACTION);
			redeemBlackHawkMap.put(Constants.PREPAID_BALANCE_BALANCE_Value,response.get("FaceValue").toString());
			redeemBlackHawkMap.put(Constants.VOUCHER_ACTION,Constants.VOUCHER_REDEEM_BLACKHAWK_ACTION);
			response=null;
			//Make a call to BlackHawk to redeem the Voucher
			response = request.blackHawkInquiryRequest(redeemBlackHawkMap);				
		    this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_BALANCE_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		}
	}
}
	
	
	private boolean reversalBlackHawkVoucher(HashMap<String, String> prepaidBalanceUpdate,String balanceAction, HashMap<String, String> balanceObject) throws C30ProvConnectorException, C30SDKException {
		log.info("BlackHawk Vouchers Reversal Method");
		C30ProvisionRequest request = C30ProvisionRequest.getInstance();	
		HashMap<String, String> redeemBlackHawkMap = new HashMap<String, String>();
		redeemBlackHawkMap.put(Constants.EXTERNAL_TRANSACTION_ID,prepaidBalanceUpdate.get(Constants.EXTERNAL_TRANSACTION_ID));
		redeemBlackHawkMap.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,prepaidBalanceUpdate.get(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID));
		redeemBlackHawkMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_BLACK_HAWK_PREPAID_VOUCHER_STATUS);
		redeemBlackHawkMap.put(Constants.VOUCHER_PIN,(String) balanceObject.get(Constants.VOUCHER_PIN));
		redeemBlackHawkMap.put(Constants.REVERSAL_REQUEST_ID,(String) balanceObject.get(Constants.REVERSAL_REQUEST_ID));
		redeemBlackHawkMap.put(Constants.VOUCHER_ACTION,Constants.VOUCHER_REDEEM_BLACKHAWK_ACTION);
		HashMap<?, ?>	response = request.blackHawkInquiryRequest(redeemBlackHawkMap);				
		this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_BALANCE_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		return true;
	}

	
	
	
	private void redeemIncommVocher(HashMap<String, String> prepaidBalanceUpdate,String balanceAction, HashMap<String, String> balanceObject) throws C30ProvConnectorException, C30SDKException {
		log.info("Incomm Vocuhers Redemption Method");
		C30ProvisionRequest request = C30ProvisionRequest.getInstance();	
		HashMap<String, String> redeemIncommMap = new HashMap<String, String>();
		redeemIncommMap.put(Constants.EXTERNAL_TRANSACTION_ID,prepaidBalanceUpdate.get(Constants.EXTERNAL_TRANSACTION_ID));
		redeemIncommMap.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,prepaidBalanceUpdate.get(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID));
		redeemIncommMap.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_INCOMM_PREPAID_VOUCHER_STATUS);
		redeemIncommMap.put(Constants.VOUCHER_PIN,(String) balanceObject.get(Constants.VOUCHER_PIN));
		redeemIncommMap.put(Constants.VOUCHER_ACTION,Constants.VOUCHER_STATUS_INCOMM_ACTION);
		
		//Make  a call to Incomm system to first get the Voucher Status
		HashMap<?, ?> response = request.incommInquiryRequest(redeemIncommMap);

		
		//If response is not Null from Incomm Status call , then it is Active Card.
		//Make call to OCS to update the wallet Balance 
		balanceAction=Constants.PREPAID_WALLET_REFILL;
		prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_ACTION,balanceAction);
		prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_TYPE,Constants.PREPAID_BALANCE_TYPE_COMMON);
		prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_REASON,"INCOMM_VOUCH_WALLET_REFILL_"+(String)response.get("FaceValue"));
		prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_BALANCE_Value,(String)response.get("FaceValue"));
		prepaidBalanceUpdate.put(Constants.PREPAID_BALANCE_EXPIRATION_DATE,(String)balanceObject.get(Constants.PREPAID_BALANCE_EXPIRATION_DATE));
		
		log.info("prepaidBalanceUpdate = "+prepaidBalanceUpdate.toString());

		if(response.get("ResponseXML") !=null){
			if(redeemIncommMap!=null){
				redeemIncommMap.remove(Constants.VOUCHER_ACTION);
				redeemIncommMap.put(Constants.VOUCHER_ACTION,Constants.VOUCHER_REDEEM_INCOMM_ACTION);
				response=null;
				//Make a cal to Incomm to redeem the Voucher
				response = request.incommInquiryRequest(redeemIncommMap);
					
			    this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_BALANCE_XML, response.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}
		}
	}

	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}
	
	  /**
     * 
     * @param voucherValue
     * @return
     */
     private static String getBlackhawkVoucherValue(String voucherValue) {
		Double exactValue =  ((( new Double(voucherValue)/100))); 		
		return exactValue.toString();
	}
     

}