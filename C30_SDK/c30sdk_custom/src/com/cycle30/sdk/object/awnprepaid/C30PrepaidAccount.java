
package com.cycle30.sdk.object.awnprepaid;


import java.util.HashMap;
import org.apache.log4j.Logger;

//Provision Objects
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;

//SDK Objects
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;



/**
 * @author Umesh
 * This object is used to work on Account Search and History OCS calls.
 * 
 */

public class C30PrepaidAccount extends C30SDKObject implements Cloneable {

	private static Logger log = Logger.getLogger(C30PrepaidAccount.class);
	

	/**
	 * Creates a new instance of C30PrepaidAccount.
	 * @param factory Factory used to create C30PrepaidAccount objects
	 * @param objectType Type of C30SDK object
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 * @throws C30SDKInvalidConfigurationException if there is invalid configuration which causes this account to fail during initialization.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidAccount(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}


	@Override
	protected final void initializeAttributes()
	throws C30SDKInvalidAttributeException {

		setClassFieldConfiguration();

	}

    /**
     * Creates a copy of the C30PrepaidAccount object.
     * 
     * @return A copy of the C30PrepaidAccount object
     */
    @Override
    public Object clone() {
    	C30PrepaidAccount copy = (C30PrepaidAccount)super.clone();        
            return copy;        
    }
    
    
	@Override
	protected void loadCache() throws C30SDKCacheException {
		// TODO Auto-generated method stub

	}

	@Override
	public C30SDKObject process() throws C30SDKException 
	{
		C30SDKObject sdkObj = null;
		String acctsegId = null;
		String subscriberId = null;
		String transId = null;
		String accountProperty = null;
		
		try{
				log.info("Prepaid Account Details process initialized ");
		        acctsegId = this.getAcctSegId().toString();
		        log.info("acctsegId =  " +acctsegId);

	            if (acctsegId == null) {
	                throw new C30SDKException("PrepaidAccountDetails - Could not retrieve segmentation id from SDKObject ");
	            }
		        
		        transId   = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            subscriberId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_SUBSCRIBER_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);	            
	            accountProperty = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_ACCOUNT_ACTION, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            
	            log.info("transId =  " +transId + " subscriberId =  " +subscriberId + " accountProperty = " +accountProperty);	               

	            //GET PrepaidAccountDetails from OCS
	            sdkObj = getPrepaidAccountDetails(acctsegId, subscriberId, transId, accountProperty);
	            
				//if we got an PrepaidAccountHistory rather than an exception
				if (sdkObj instanceof C30PrepaidAccount) {
					log.info("Found the Instance of C30PrepaidAccount");
					this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					sdkObj = this;
				}
	            
		}
		catch(Exception e)
		{
		    log.error(e);
		    sdkObj = this.createExceptionMessage(e);
		}
		return sdkObj;
	}

    /**
     * Internal method to invoke OCS.  
     * 
     * @param acctsegId 
     * @param subscriberId 
     * @param transId
     * @throws Exception
     *  
     */
    	
	private C30SDKObject getPrepaidAccountDetails(String acctsegId, 
                String  subscriberId, String  transId, String accountProperty) throws C30SDKException {
                  
		HashMap<String, String> ProvInquiry = new HashMap<String, String>();
		
		C30PrepaidAccount ocssdkObj = this;
		
        try {
        	
				//Account process initialized.		        
	            log.info("Account " + accountProperty + "process initialized for " + "transId = " + transId + "acctsegId : "+acctsegId + "subscriberId : "+subscriberId);
	
				//Get the Account Details Response from OCS 
	            ProvInquiry.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctsegId);
	            ProvInquiry.put(Constants.SUBSCRIBER_ID,subscriberId);
	            log.error("subscriberId " +subscriberId);
	            if (accountProperty.equalsIgnoreCase("history")) {
		            log.error("Account History");
		            ProvInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_ACCOUNT_HISTORY);	            	
	            }
	            else if (accountProperty.equalsIgnoreCase("search")) {
		            log.error("Account Search");
		            ProvInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_OCS_ACCOUNT_SEARCH);	            	
	            }
	            log.error("Create PROV Instance");
				C30ProvisionRequest acctrequest = C30ProvisionRequest.getInstance();
				HashMap<?, ?>  acctHistoryresponse = acctrequest.ocsInquiryRequest(ProvInquiry);
	            log.error("acctHistoryresponse" +acctHistoryresponse);
				//Send Response XML.
				ocssdkObj.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_ACCOUNT_XML, acctHistoryresponse.get("ResponseXML") , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);					

				return ocssdkObj;
				
        }
        catch (C30ProvConnectorException e) {
        	log.error("PROV Exception - PrepaidAccountDetails : " +e);
            String errMsg = e.getMessage();
            String errCode = e.getExceptionCode();
            throw new C30SDKException(errMsg, e, errCode);
        }
        catch(Exception e) {
        	log.error("Exception - PrepaidAccountDetails : " +e);        	
            String errMsg = e.getMessage();
            String errCode = errMsg.substring(0, C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
            throw new C30SDKException(errMsg, e, errCode);
        }
       
    }

}
