package com.cycle30.sdk.object.extension.brePreValidation;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.extension.C30TransactionBREExtension;

public class PaymentCreateBrePreValidation  extends C30TransactionBREExtension 
{
	/**
	 * Validates that all the required Business Logic and dataAttributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
	 */
	public void brevalidate(C30SDKObject lwo) throws C30SDKException 
	{
		String tenderType = "";
		
		if (lwo.isAttribute(C30SDKAttributeConstants.ATTRIB_TENDER_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			tenderType = (String)lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TENDER_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	
		//throw new C30SDKBRERuleException(exceptionResourceBundle.getString("INVALID-ATTRIB-500") + tenderType,"INVALID-ATTRIB-500");
		
	}

}
