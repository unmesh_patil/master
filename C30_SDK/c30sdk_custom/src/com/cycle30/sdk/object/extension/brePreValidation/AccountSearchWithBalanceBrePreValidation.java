package com.cycle30.sdk.object.extension.brePreValidation;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKBRERuleException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.extension.C30TransactionBREExtension;

public class AccountSearchWithBalanceBrePreValidation  extends C30TransactionBREExtension 
{
	/*
	 * This is used to validate Account for AccountSearchWithBalance Summary find... 
	 */
	public void brevalidate(C30SDKObject lwo) throws C30SDKException
	{
		String externalId = (String) lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer externalIdType = (Integer)lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer accountInternalId = (Integer)lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billFname = (String)lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billLname = (String)lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billCompany = (String)lwo.getAttributeValue("BillCompany", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String phone =(String)lwo.getAttributeValue("CustPhone1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String zip  =(String)lwo.getAttributeValue("BillZip", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String city  =(String)lwo.getAttributeValue("BillCity", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billAddress1 = (String)lwo.getAttributeValue("BillAddress1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String state = (String)lwo.getAttributeValue("BillState", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Boolean includeInactiveAccount = (Boolean)lwo.getAttributeValue("IncludeInactiveAccounts", C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		
		//Test Validation so not being used.
		/*if (zip!=null || state!=null)
		{
			if (!(zip!=null && state!=null))
			{		
				throw new C30SDKInvalidAttributeException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-501"),"INVALID-ATTRIB-501");

			}
		}*/	
		
		//Validation -1: Implement a validation rule where wildcard searches with X number of characters in lastName/companyName result in a validation error.
		//X=3 seems like a reasonable minimum number of characters for wildcard searches on the aforementioned fields.

		if (billLname!=null && billLname.contains("%") && billLname.length()<4)
		{
			throw new C30SDKBRERuleException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-502"),"INVALID-ATTRIB-502");
		}
		if (billCompany!=null && billCompany.contains("%") && billCompany.length()<4)
		{
			throw new C30SDKBRERuleException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-503"),"INVALID-ATTRIB-503");
		}
		
		//Validation-2 : but searches with first name only (regardless of whether it is a wildcard search or specific name) 
		// should also result in a validation error. Firstname requires at least 1 other search criteria to be provided.
		if (billFname!=null && externalId==null  &&accountInternalId==null
				&& billLname==null && billCompany==null && phone==null && zip==null && city==null
				&& billAddress1==null && state==null)
		{
			throw new C30SDKBRERuleException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-504"),"INVALID-ATTRIB-504");
		}
		
		//Validation-3 : If state is provided, either city or zip are required.
		if (state!=null)
		{
			if(zip==null && city==null)
			{
				throw new C30SDKBRERuleException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-505"),"INVALID-ATTRIB-505");
			}
			if( !(billLname!=null || billCompany != null  || phone!=null || billFname!=null ))
			{
				throw new C30SDKBRERuleException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-507"),"INVALID-ATTRIB-507");
			}
			
		}
		//validation - 4: INVALID-ATTRIB-507=Cannot allow any single or more combination of city/state/zip only.
		if (city!=null)
		{
			if( !(billLname!=null || billCompany != null  || phone!=null || billFname!=null ))
			{
				throw new C30SDKBRERuleException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-507"),"INVALID-ATTRIB-507");
			}
		}
		if (zip!=null)
		{
			if( !(billLname!=null || billCompany != null  || phone!=null || billFname!=null ))
			{
				throw new C30SDKBRERuleException(lwo.exceptionResourceBundle.getString("INVALID-ATTRIB-507"),"INVALID-ATTRIB-507");
			}
		}
	}

}
