package com.cycle30.sdk.object.gci;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.http.HttpRequest;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30REST;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.object.utils.PropertyManager;
import com.cycle30.sdk.object.utils.C30XMLResponseParser;

public class GCIBillInvoice {
	private static Logger log = Logger.getLogger(GCIBillInvoice.class);
	C30SDKObject sdkObj;
	private Properties prop;

	public GCIBillInvoice() {
		prop = PropertyManager.getInstance();
	}

	public C30SDKObject process() throws C30SDKException {

		C30REST ob = new C30REST();
		HttpRequest request;
		try {

			request = postMethod(ob);
			String serverResponse = ob.execute((HttpUriRequest) request,
					prop.getProperty("END_POINT_REFERENCE_POST"));
			String sessionId = C30XMLResponseParser.parseResponse(serverResponse);
			request = getMethod(ob, sessionId);
			serverResponse = ob.execute((HttpUriRequest) request,
					prop.getProperty("GCIBILLINVOICE_END_POINT_REFERENCE_REQUEST_URL"));
			return sdkObj;
		} catch (UnsupportedEncodingException e) {
			log.error("Encoding Not supported :" + e.getMessage());
		}
		return null;
	}

	public HttpGet getMethod(C30REST ob, String sessionId) {
		ArrayList<NameValuePair> headers = ob.getHeaders();
		headers.add(new NameValuePair("X-Session-Id", sessionId));
		HttpGet request = new HttpGet(
				prop.getProperty("GCIBILLINVOICE_END_POINT_REFERENCE_GET"));
		return request;
	}

	public HttpPost postMethod(C30REST ob) throws UnsupportedEncodingException {
		ArrayList<NameValuePair> headers = ob.getHeaders();
		headers.add(new NameValuePair("X-Client-ID", prop
				.getProperty("GCIBILLINVOICE_X_Client_ID")));
		headers.add(new NameValuePair("X-User-ID", prop
				.getProperty("GCIBILLINVOICE_X_User_ID")));
		HttpPost request = new HttpPost(
				prop.getProperty("END_POINT_REFERENCE_POST"));
		setHeaders(request, headers);

		StringEntity enty = new StringEntity(
				prop.getProperty("AUTHENICATION_XML"));
		request.setEntity(enty);

		return request;

	}

	public void setHeaders(HttpUriRequest request,
			ArrayList<NameValuePair> headers) {
		// add headers
		for (NameValuePair h : headers) {
			request.addHeader(h.getName(), h.getValue());
		}
	}

	public static void main(String args[]) {
		GCIBillInvoice obj = new GCIBillInvoice();
		try {
			obj.process();
		} catch (C30SDKException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
