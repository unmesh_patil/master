package com.cycle30.sdk.object.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

public class PropertyManager extends Properties
{

	private static final long serialVersionUID = -5439024782805715936L;

	private static PropertyManager propertyManager = null;
	private boolean checkingForDuplicates = false;

	private static Logger log = Logger.getLogger(PropertyManager.class);

	public static PropertyManager getInstance()
	{
		if (propertyManager == null)
		{
			try
			{
				propertyManager = new PropertyManager();
			} catch (Exception e)
			{
				e.printStackTrace();
				log.warn("Can not load com.script.utils.PropertyManager class");
			}
		}

		return propertyManager;
	}

	public void setCheckingForDuplicates(boolean checkingForDuplicates)
	{
		log.info("Setting the checkingForDuplicates flag to "
				+ checkingForDuplicates);
		this.checkingForDuplicates = checkingForDuplicates;
	}

	public boolean isCheckingForDuplicates()
	{
		return checkingForDuplicates;
	}

	public void addFromClassPath(String resourceName)
			throws java.io.IOException, java.io.FileNotFoundException
	{
		log.info("Adding configuration resource " + resourceName
				+ " from class path");
		java.io.InputStream is = this.getClass().getResourceAsStream(
				resourceName);

		if (is != null)
		{
			addFromStream(is);
		} else
		{
			log.error("Couldn't find " + resourceName + " on the CLASSPATH",
					null);
			throw new java.io.FileNotFoundException("Couldn't find "
					+ resourceName + " on the CLASSPATH");
		}
	}

	public void addFromClassPathAsaResourceBundle(String resourceName)
			throws java.io.IOException, java.io.FileNotFoundException
	{

		log.info("Adding configuration resource " + resourceName
				+ " from class path");
		resourceName = "/" + resourceName + "_"
				+ Locale.getDefault().getLanguage() + ".properties";
		java.io.InputStream is = this.getClass().getResourceAsStream(
				resourceName);

		if (is != null)
		{
			addFromStream(is);
		} else
		{
			log.error("Couldn't find " + resourceName + " on the CLASSPATH",
					null);
			throw new java.io.FileNotFoundException("Couldn't find "
					+ resourceName + " on the CLASSPATH");
		}
	}

	public void addFromFile(java.io.File configFilePath)
			throws java.io.IOException, java.io.FileNotFoundException
	{
		log.info("Adding configuration resource " + configFilePath
				+ " from a file");
		java.io.InputStream is = null;
		try
		{
			is = new java.io.FileInputStream(configFilePath);
		} catch (java.io.FileNotFoundException ex)
		{
			log.error("Error getting resource", ex);
			throw ex;
		}

		addFromStream(is);
	}

	public Properties addFromFileAndReturnProperties(String resourceName)
			throws java.io.IOException, java.io.FileNotFoundException
	{
		log.info("Adding configuration resource " + resourceName
				+ " from class path");
		java.io.InputStream is = this.getClass().getResourceAsStream(
				resourceName);

		Properties newProperties = new Properties();
		newProperties.load(is);
		return newProperties;
	}

	public void addFromStream(java.io.InputStream inputSource)
			throws java.io.IOException
	{
		Properties newProperties = new Properties();
		newProperties.load(inputSource);

		addFromProperties(newProperties);
	}

	public void addFromProperties(Properties properties)
	{
		if (checkingForDuplicates)
		{
			Enumeration enumKeys = properties.propertyNames();
			while (enumKeys.hasMoreElements())
			{
				String key = (String) enumKeys.nextElement();
				if (containsKey(key))
				{
					log.warn("Properties file contains duplicate key: " + key,
							null);
					throw new IllegalArgumentException(
							"Properties file contains duplicate key: " + key);
				}
			}
		}

		putAll(properties);
	}

	public String getString(String key)
	{
		if (!containsKey(key))
		{

			log.info("Key '" + key + "' does not exist");
		}
		return getProperty(key);
	}

	public int getInteger(String key)
	{
		int retVal = -1;

		String value = getString(key);
		if (value != null)
		{
			retVal = Integer.parseInt(value);
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return retVal;
	}

	public float getFloat(String key)
	{
		float retVal = -1.0f;

		String value = getString(key);
		if (value != null)
		{
			retVal = Float.parseFloat(value);
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return retVal;
	}

	public double getDouble(String key)
	{
		double retVal = -1.0;

		String value = getString(key);
		if (value != null)
		{
			retVal = Double.parseDouble(value);
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return retVal;
	}

	public long getLong(String key)
	{
		long retVal = -1;

		String value = getString(key);
		if (value != null)
		{
			retVal = Long.parseLong(value);
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return retVal;
	}

	public short getShort(String key)
	{
		short retVal = -1;

		String value = getString(key);
		if (value != null)
		{
			retVal = Short.parseShort(value);
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return retVal;
	}

	public java.io.File getFile(String key)
	{
		java.io.File retVal = null;

		String value = getString(key);
		if (value != null)
		{
			retVal = new java.io.File(value);
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return retVal;
	}

	public boolean getBoolean(String key)
	{
		boolean boolVal = false;

		String value = getString(key);
		if (value != null)
		{
			boolVal = Boolean.valueOf(value).booleanValue();
		}

		return boolVal;
	}

	public java.net.URL getURL(String key)
			throws java.net.MalformedURLException
	{
		java.net.URL retVal = null;

		String value = getString(key);
		if (value != null)
		{
			retVal = new java.net.URL(value);
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return retVal;
	}

	public Class getRuntimeClass(String key) throws ClassNotFoundException
	{
		Class runtimeClass = null;

		String value = getString(key);
		if (value != null && value.length() > 0)
		{
			runtimeClass = Class.forName(value, true, getClass()
					.getClassLoader());
		} else
		{
			throw new IllegalArgumentException("Invalid key specified: " + key);
		}

		return runtimeClass;
	}

	public java.util.Locale getLocale(String key)
	{
		Locale locale = null;

		String value = getString(key);
		if (value != null && value.length() > 0)
		{
			StringTokenizer st = new StringTokenizer(value, "_");
			String languageCode = st.nextToken();
			String countryCode = null;
			String variant = null;
			if (st.hasMoreTokens())
			{
				countryCode = st.nextToken();
			}
			if (st.hasMoreTokens())
			{
				variant = st.nextToken();
			}
			if (languageCode != null && countryCode == null && variant == null)
			{
				locale = new Locale(languageCode);
			} else if (languageCode != null && countryCode != null
					&& variant == null)
			{
				locale = new Locale(languageCode, countryCode);
			} else if (languageCode != null && countryCode != null
					&& variant != null)
			{
				locale = new Locale(languageCode, countryCode, variant);
			}
		}

		return locale;
	}

	public static final String WEB_SERVICE_RESOURCE = "webservice";
	

	/**
	 * Constructs a new C30SDKConfigurationManager
	 * 
	 * @throws C30SDKToolkitException
	 */
	private PropertyManager() throws Exception
	{
		try
		{
			log.info("Creating new Property File Manager");
			addFromClassPathAsaResourceBundle(WEB_SERVICE_RESOURCE);
			//addFromClassPath(WEB_SERVICE_RESOURCE);

		} catch (java.util.MissingResourceException ex)
		{
			log.error("An Error occured while loading the property files. No resource found.");
		} catch (FileNotFoundException e)
		{
			log.error("An Error occured while loading the property files. No file found.");
		} catch (IOException e)
		{
			log.error("An Error occured while loading the property files. IO Exception found.");
		}
	}

	protected static File getPersonalizationPath(String relativeUri)
	{
		return new File(System.getProperty("user.home"), relativeUri);
	}

	@Override
	public String getProperty(String key, String defaultValue)
	{
		String value = super.getProperty(key);

		if (value == null)
		{
			value = defaultValue;
		}

		return value;
	}

	@Override
	public String getProperty(String key)
	{
		String value = super.getProperty(key);

		return value;
	}

	private void addPrefixedProperties(String keyPrefix,
			Properties prefixedProperties, Properties sourceProperties)
	{
		if (sourceProperties == null)
		{
			return;
		}

		Iterator itSrcProps = sourceProperties.entrySet().iterator();
		while (itSrcProps.hasNext())
		{
			Map.Entry me = (Map.Entry) itSrcProps.next();
			String key = (String) me.getKey();
			String value = (String) me.getValue();
			if (key.startsWith(keyPrefix))
			{
				prefixedProperties.setProperty(key, value);
			}
		}
	}

	public static Properties loadProperties(final String name)
	{
		if (SCRIPT_INPUT_FILE == null)
		{
			log.debug("Env Properties file path not set");
			SCRIPT_INPUT_FILE = System.getenv("PROP_FILE_PATH");
			SCRIPT_INPUT_FILE = SCRIPT_INPUT_FILE == null ? System
					.getProperty("SCRIPT_INPUT_FILE") : SCRIPT_INPUT_FILE;
			log.debug("Properties file path is :" + SCRIPT_INPUT_FILE);
		}

		return loadProperties(name, null);
	}

	public static Properties loadProperties(String name, ClassLoader loader)
	{
		if (properties == null)
		{
			try
			{
				properties = new PropertyManager();
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name == null)
			throw new IllegalArgumentException("The input file name is NULL");

		if (name.startsWith("/"))
			name = name.substring(1);

		if (name.endsWith(SUFFIX))
			name = name.substring(0, name.length() - SUFFIX.length());

		Properties result = null;

		try
		{
			name = name.replace('.', File.separator.charAt(0));
			if (!name.endsWith(SUFFIX))
				name = name.concat(SUFFIX);
			String propsFilePath = SCRIPT_INPUT_FILE + File.separator + name;
			log.debug("propsFilePath is :" + propsFilePath);
			properties.load(new FileInputStream(new File(propsFilePath)));
			if (properties.size() > 0)
				result = properties;

		} catch (Exception e)
		{
			log.error("Exception :" + e.getMessage());
			e.printStackTrace();
		}

		if (THROW_ON_LOAD_FAILURE && (result == null))
		{

			throw new IllegalArgumentException("could not load ["
					+ name
					+ "]"
					+ " in "
					+ (LOAD_AS_RESOURCE_BUNDLE ? "a resource bundle"
							: "Properties file path : " + SCRIPT_INPUT_FILE));

		}

		return result;
	}

	private static String SCRIPT_INPUT_FILE;
	private static final String SUFFIX = ".properties";
	private static boolean LOAD_AS_RESOURCE_BUNDLE = false;
	private static boolean THROW_ON_LOAD_FAILURE = true;
	private static Properties properties = null;
	
	public static void main(String args[])
	{
		PropertyManager.getInstance();
	}
}
