package com.cycle30.sdk.bulkorder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;



/** read input CSV file and process each order
 * 
 * @author rnelluri
 *
 */
public class OrderBulkTest {

	public static void main(String[] args) {
		try {
			// Read the input file
			File file = new File("BulkOrderTest.csv");
			BufferedReader bufRdr  = new BufferedReader(new FileReader(file));

			String orderRecord = "";
			ProcessOrderRecord proRecord = new ProcessOrderRecord();

			//Use the transactionId and OrderId prefix from the given file and use them in the order calls..
			String transactionId = "";
			String orderId="";

			int orderNumber =0;
			
			while(( orderRecord = bufRdr.readLine()) != null)
			{
				if (orderRecord.startsWith("TransactionIdPrefix"))
				{
					StringTokenizer st = new StringTokenizer(orderRecord,"=");
					st.nextToken();
					transactionId= st.nextToken();
				}
				else if (orderRecord.startsWith("OrderIdPrefix"))
				{
					StringTokenizer st = new StringTokenizer(orderRecord,"=");
					st.nextToken();
					orderId= st.nextToken();
				}
				else if (orderRecord.startsWith("#")) 
				{
					//Ignore this
				}
				else
				{
					orderNumber++;
					proRecord.process(transactionId+"-"+orderNumber,orderId+"-"+orderNumber,orderRecord);

				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

