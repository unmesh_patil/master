package com.cycle30.sdk.bulkorder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderXMLTemplate {


	/** Build this object
	 * 		<IdentifierObject ClientActionType="SWAP">
				<IdentifierIdIn>112500000240032</IdentifierIdIn>  <!-- NEW SIM -->
			   <IdentifierIdOut>114240000240032</IdentifierIdOut> <!-- Existing SIM -->
				<IdentifierTypeName>ICCID</IdentifierTypeName>  
			</IdentifierObject>	
	 */
	public static String getIdentifierObject(String clientActionType, String idIn, String idOut, String idTypeName)
	{
		String idXml = "";

		idXml = idXml + createXmlTag("IdentifierIdIn", idIn,true,false);
		idXml = idXml + createXmlTag("IdentifierIdOut", idOut,true,false);
		idXml = idXml + createXmlTag("IdentifierTypeName", idTypeName,true,false);

		idXml = "<IdentifierObject ClientActionType=\""+clientActionType.trim()+"\">\n"  +  idXml + "<IdentifierObject>\n";

		return idXml;

	}

	/** Get ComponentObject
	 * 
	 * @param clientActionType
	 * @param idIn
	 * @param idOut
	 * @param idTypeName
	 * @return
	 */
	public static String getComponentObject(String clientActionType, String cmpId, String cmpName, String cmpInstId)
	{
		String componentXml = "";

		componentXml = componentXml + createXmlTag("ComponentId", cmpId,true,false);
		componentXml = componentXml + createXmlTag("ComponentName", cmpName,true,false);
		componentXml = componentXml + createXmlTag("ComponentInstanceId", cmpInstId,true,false);

		componentXml = "<ComponentObject ClientActionType=\""+clientActionType.trim()+"\">\n"  +  componentXml + "<ComponentObject>\n";

		return componentXml;
	}

	/** Get PlanObject XML
	 * 
	 * @param clientId
	 * @param clientActionTypeReason
	 * @param clientActionType
	 * @param planId
	 * @param planName
	 * @param PlanInstanceIdIn
	 * @param PlanInstanceIdOut
	 * @return
	 */
	public static String getPlanObject(String clientId, String clientActionTypeReason, String clientActionType, String planId, String planName,
			String PlanInstanceIdIn, String PlanInstanceIdOut)
	{
		String planXml = "";

		planXml = planXml + createXmlTag("PlanId", planId,true,false);
		planXml = planXml + createXmlTag("PlanName", planName,true,false);
		planXml = planXml + createXmlTag("PlanInstanceIdIn", PlanInstanceIdIn,true,false);
		planXml = planXml + createXmlTag("PlanInstanceIdOut", PlanInstanceIdOut,true,false);

		planXml = "<PlanObject ClientActionType=\""+clientActionType.trim()+"\" "+ 
				" ClientId=\""+clientId.trim()+"\" "+ 
				" ClientActionTypeReason=\""+clientActionType.trim()+"\">\n"  +  planXml + "<PlanObject>\n";

		return planXml;

	}
	
	
	/** Get ServiceObject
	 * 
	 * @param clientId
	 * @param clientActionTypeReason
	 * @param clientActionType
	 * @param planId
	 * @param planName
	 * @param PlanInstanceIdIn
	 * @param PlanInstanceIdOut
	 * @return
	 */
	public static String getServiceObject(String clientId, String clientAccountId, String clientOrderItemName,String clientActionType)
	{
		String serviceHeadXml = "";

		serviceHeadXml = "<ServiceObject ClientAccountId=\""+clientAccountId.trim()+"\" "+ 
				" ClientId=\""+clientId.trim()+"\" "+ 
				" ClientOrderItemName=\""+clientOrderItemName.trim()+"\" "+ 
				" ClientActionType=\""+clientActionType.trim()+"\">\n";

		return serviceHeadXml;

	}

	/** Returns the proper XML tag for every request
	 * 
	 * @param tagName
	 * @param value
	 * @param includeNull
	 * @param includeEmptyString
	 * @return
	 */
	public static String createXmlTag(String tagName, String value, boolean includeNull, boolean includeEmptyString) {
		boolean buildTag = true;
		String xmlStr = "";

		if (value == null && !includeNull) {
			buildTag = false;
		}
		else if ("".equals(value) && !includeEmptyString) {
			buildTag = false;
		}
		if (buildTag) {
			xmlStr = "<" + tagName + ">" + value + "</" + tagName + ">\n";
		}

		if (xmlStr.length() <= 1  && includeNull)  xmlStr =  "<" + tagName + " xsi:nil=\"true\">\n";

		return xmlStr;
	}
	
	public static String getSysDate()
	{
		//2012-05-22T09:30:47.0Z
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.0'Z'");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
}
