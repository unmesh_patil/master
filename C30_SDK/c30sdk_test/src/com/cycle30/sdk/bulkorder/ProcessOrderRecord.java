package com.cycle30.sdk.bulkorder;

import java.util.StringTokenizer;

import org.apache.log4j.Logger;


public class ProcessOrderRecord {

	private static final Logger log = Logger.getLogger(ProcessOrderRecord.class);


	/** This is to process the input record and make call to order system or make a direct api call
	 * 
	 * @param orderRecord
	 */
	public void process(String transactionId, String orderId, String orderRecord) {
		// Read the first element and find the order type
		//ignore the lines that start with #

			log.debug("TransactionId =" +transactionId +"; OrderId = "+ orderId);
			log.debug("Working on record : "+ orderRecord);
			StringTokenizer st = new StringTokenizer(orderRecord,",");
			String orderType = st.nextToken();
			
			String orderXML ="<OrderRequest>\n" +
					"  <OrderDesiredDate>"+BulkJobXMLTemplate.getSysDate()+"</OrderDesiredDate>\n" +
					"  <ActionWho>bulkOrderTest</ActionWho>\n" +
					"  <ClientOrderId>"+orderId+"</ClientOrderId>\n" +
					"  <ClientOrderName>"+orderId+"</ClientOrderName>\n";
			
			if (orderType.equalsIgnoreCase("SIM_Swap"))
				orderXML= orderXML+ getSIMSwapXML(orderRecord);
			
			orderXML = orderXML +"</OrderRequest>";
			
			log.debug(orderXML);
		
	}

	/** Create SIM Swap XML
	 * 
	 * @param orderRecord
	 * @return
	 */
	private String getSIMSwapXML(String orderRecord) {
		//#Identifier_Swap,ServiceObjectClientId,ServiceObjectClientAccountId,ServiceObjectClientActionType,
		//ServiceObjectClientOrderItemName,IdentifierObjectListSize,IdnClientActionType,IdentifierIdIn,IdentifierIdOut,IdentifierIdTypeName
		
		//SIM_Swap,9810029383,,CHANGE,PO-SVC1,1,SWAP,112500000240032,112500000240031,ICCID
		String retString = "";
		StringTokenizer st = new StringTokenizer(orderRecord,",");
		st.nextToken();
		
		 String srvClientId = st.nextToken();
		 String srvClientAccountId = st.nextToken();
		 String srvclientActionType = st.nextToken();
		 String srvclientOrderItemName = st.nextToken();
		 
		 
		 retString = retString + "<ServiceObjectList Size=\"1\">\n";
		 
		 retString = retString + BulkJobXMLTemplate.getServiceObject(srvClientId,  srvClientAccountId,  srvclientOrderItemName, srvclientActionType);
		
		 //st.nextToken();
		 
		 retString = retString + "<IdentifierObjectList Size=\"1\">\n";
	
		 String IdnClientActionType = st.nextToken();
		 String  IdentifierIdIn= st.nextToken();
		 String IdentifierIdOut= st.nextToken();
		 String IdentifierIdTypeName= st.nextToken();
		 
		 retString = retString + BulkJobXMLTemplate.getIdentifierObject(IdnClientActionType,IdentifierIdIn,IdentifierIdOut,IdentifierIdTypeName);
		 
		 retString = retString + "</IdentifierObjectList>\n";
		 
		 retString = retString + "</ServiceObject>\n</ServiceObjectList>\n";
		return retString;
	}

}
