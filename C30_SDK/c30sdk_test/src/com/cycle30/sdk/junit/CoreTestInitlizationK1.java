package com.cycle30.sdk.junit;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKUser;


/** used as the intializatino class for all the test scenarios
 * 
 * @author rnelluri
 *
 */
public abstract class CoreTestInitlizationK1 
{
	public static Logger log = Logger.getLogger(CoreTestInitlizationK1.class);
	static C30SDKFrameworkFactory psFactory=null;
	static C30SDKUser user = null;

	public static void initialize() throws Exception {
		log.info("Setting up the Enviroment and LWO Factory Only once");
		user  = new C30SDKUser( "arborsv", "csgfx","CSGFx");
		psFactory = new C30SDKFrameworkFactory( "FX1.0/PS2.0", user);  // K1
	}

}
