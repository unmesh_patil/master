
package com.cycle30.sdk.junit;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({OrderCreateTest.class})
public class CoreTestSuite {
	
	
	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		CoreTestInitlizationK1.initialize();
	}
	
}
