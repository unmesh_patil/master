package com.cycle30.sdk.junit;

import static org.junit.Assert.assertTrue;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.util.C30DOMUtils;

public class OrderCreateTest  extends CoreTestInitlizationK1{
	
	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		CoreTestInitlizationK1.initialize();
	}
	@Test
	public void orderCreate1() throws C30SDKException, TransformerFactoryConfigurationError, TransformerException {
		String xmlDoc="Order1.xml";
		Document request = C30DOMUtils.getDocument(xmlDoc);
		StringWriter stw = new StringWriter(); 
		Transformer serializer = TransformerFactory.newInstance().newTransformer(); 
		serializer.transform(new DOMSource(request), new StreamResult(stw)); 
		String xmlRequest  = stw.toString();
		System.out.println(xmlRequest);
		
		C30SDKObject lwo = psFactory.createC30SDKObject("Core Order", 3);
		lwo.setNameValuePair("OrderXML",xmlRequest);
		lwo.setNameValuePair("ItemActionId","1");
		C30SDKObject returnsdk = lwo.process();		
		if(returnsdk instanceof C30SDKExceptionMessage)
		{
			assertTrue(false);
		}
		
	}
	
	
}
