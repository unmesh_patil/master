
package com.cycle30.sdk.test;

import java.io.FileInputStream;
import java.io.InputStream;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;

public class BulkJobCreateTest {


	public static void main(String[] args) {
		try {
			BulkJobCreateTest test = new BulkJobCreateTest();
			String xmlDoc="BulkOrderTest.csv";
			InputStream fis = new FileInputStream(xmlDoc);
			String xmlRequest  = test.parseISToString(fis);

			System.out.println(xmlRequest);

			C30SDKFrameworkFactory	psFactory = new C30SDKFrameworkFactory( "FX2.0","FX2.0/PS2.0");
			C30SDKObject lwo = psFactory.createC30SDKObject("Core Bulk Order Job Create", 2);
			lwo.setNameValuePair("BulkOrderJobData",xmlRequest);
			//lwo.setNameValuePair("BulkOrderJobId","3");
			//lwo.setNameValuePair("BulkOrderJobType","Test");
			lwo.setNameValuePair("ItemActionId","1");
			lwo.setNameValuePair("ClientOrgId","ACSDV1");
			lwo.setNameValuePair("TransactionId","T10000");
			lwo.setNameValuePair("ClientOrgName","ACSDV1-Name");
			C30SDKObject returnsdk = lwo.process();


			System.out.println(returnsdk.toString());

		} 
		catch (C30SDKException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		//System.exit(0);
	}

	public String parseISToString(java.io.InputStream is){
		java.io.DataInputStream din = new java.io.DataInputStream(is);
		StringBuffer sb = new StringBuffer();
		try{
			String line = null;
			while((line=din.readLine()) != null){
				sb.append(line+"\n");
			}
		}catch(Exception ex){
			ex.getMessage();
		}finally{
			try{
				is.close();
			}catch(Exception ex){}
		}
		return sb.toString();
	}
}
