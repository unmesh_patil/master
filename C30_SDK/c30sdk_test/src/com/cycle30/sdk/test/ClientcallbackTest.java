package com.cycle30.sdk.test;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.util.C30DOMUtils;

public class ClientcallbackTest {


	public static void main(String[] args) {
		try {

			String xmlDoc="Order1.xml";
			Document request = C30DOMUtils.getDocument(xmlDoc);
			StringWriter stw = new StringWriter(); 
			Transformer serializer = TransformerFactory.newInstance().newTransformer(); 
			serializer.transform(new DOMSource(request), new StreamResult(stw)); 
			String xmlRequest  = stw.toString();
			System.out.println(xmlRequest);
			
			C30SDKFrameworkFactory	psFactory = new C30SDKFrameworkFactory( "FX2.0","FX2.0/PS2.0");
			C30SDKObject lwo = psFactory.createC30SDKObject("Core ClientCallback", 4);
			lwo.setNameValuePair("Payload",xmlRequest);
			lwo.setNameValuePair("TransactionId","1");
			C30SDKObject returnsdk = lwo.process();

			
			System.out.println(returnsdk.toString());
			
		} 
		catch (C30SDKException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.exit(0);
	}
}
