package com.cycle30.sdk.test;	
	
	import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.framework.C30SDKUser;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.object.kenan.financial.C30Deposit;
import com.cycle30.sdk.object.kenan.invoice.C30BillInvoice;
import com.cycle30.sdk.object.kenan.ordering.C30Order;
	
	
	public class Evaluate {
	
	
		private static Logger log = Logger.getLogger(Evaluate.class);
		private static String inputTestFile = "/K1Test.txt";
		private static String fxVersion = "2.0";
		public static Properties collectionsProps = new Properties();
		public static final String xmlDocStart = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"; 
		public static final String xmlElementStart = "<";
		private static String CONNECT_DELIMITER = ",";
		public static final String STR_PROCESS_FLAG_PROCESSED = "PROCESSED";
		public static final String STR_PROCESS_FLAG_FAILED = "FAILED";
		public static String TestFileOutput = "";
		private static Logger logger = Logger.getLogger(Evaluate.class);
		private static boolean autoSubmit = false;
	
		public static class LwObjectType
		{
			public String lwObjType;
			public String requestKey;
			public Hashtable lwObjTransactions = new Hashtable();;
			public LwObjectType(String lot) {
				this.lwObjType = lot;
	
			}
		}
	
		public static void main(String[] args) { 
	
		/*	BSDMSettings settings = null;
			com.csgsystems.fx.security.SecurityManager sm = null;
			BSDMSessionContext context = null; 
		*/	// SessionContext sessionContext = null; 
			//  XmlConnection connection = null;   
			for (int i=0; i < args.length; i++){
				log.debug("arg[" + i + "]: " + args[i]);
			}
	
	
			logger.setLevel(Level.DEBUG);   
			/*
	    String accountExtId = "25";
	    String serviceExtId = "24.0";
	    String domainName = "KenanFx";
	    String userName = "arborsv";
	    String kenan_password = "kenanfx";
	    String lwoVersion = "FX1.0/PS2.0";
	    String reqKeyVal = "2010100514243400426";  // disconnect service
			 */
			// String reqKeyVal = "1";    // delete of multiple components - bulk Child permanent disconnect
			boolean xml_on = true;
	
	
	
	
			String xmlBillInvoiceFindLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Bill Invoice Find Test</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			//      "  <AccountNumber>001100030</AccountNumber> " +  
			"  <AccountId>2</AccountId> " +  
			//  "  <AccountNumber>172000135</AccountNumber> " +  // K2  no payments found
			//   "  <AccountNumber>172000180</AccountNumber> " +  // K2
			//  " <BillRefNo>11413</BillRefNo> " + // K2
			//   " <BillRefNo>2606218</BillRefNo> " + // K1
			//     " <BillRefResets>0</BillRefResets> " +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String xmlAccountServiceUpdateLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>LifeLine Service Suspend (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>172000643</AccountNumber> " +  // K2
			" <ServiceId>9073443579</ServiceId> " + 
			" <BillFname>Alphonso</BillFname> " +
			"  <KenanTelcoAccount>67676767</KenanTelcoAccount> " + 
			" <GLAccount>999999</GLAccount> "+
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
			String xmlAccountUpdateLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Account Update Test</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>001100030</AccountNumber> " +  // K1
			//   "  <AccountNumber>172000643</AccountNumber> " +  // K2
			//   " <ServiceId>9073443579</ServiceId> " + 
			" <BillFname>Aaron</BillFname> " +
			"  <KenanTelcoAccount>67676767</KenanTelcoAccount> " + // K2
			" <Bankruptcy>true</Bankruptcy> " +  //K1
			" <GLAccount>696969</GLAccount> "+
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
			String xmlCreateWirelessDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Wireless Service Create (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>172257400</AccountNumber> " + // K2
			" <FirstName>Freddy</FirstName> " +   
			" <LastName>Swaggertly</LastName> " +   
			" <ServiceId>9073443579</ServiceId> " + 
			" <IMEI>907367WhackaDrooldookie</IMEI> " +   
			" <SIM>wackaDoodle72</SIM> " +
			" <IMSI>YamIAm72</IMSI> " +    
			" <AddressId>255002</AddressId> " +  
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String xmlCreateCblModemDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Add CableModem</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			//  "  <AccountNumber>172000310</AccountNumber> " + // K2
			"  <AccountNumber>001487358</AccountNumber> " + // K1
			" <FirstName>Freddy</FirstName> " +   
			" <LastName>Krueger</LastName> " +   
			" <ServiceId>345EFG90697</ServiceId> " + 
			" <RateClass>3000</RateClass> " +   // K1
			// " <RateClass>1</RateClass> " +   // K2DEV
			" <AddressId>21002</AddressId> " +  // K2DEV
			// " <AddressId>259002</AddressId> " +  // K2DEMO
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String xmlAccountNoteLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Create Account Note</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"<AccountInternalId>120020714</AccountInternalId> " +   // K1
			//    "  <AccountNumber>172000310</AccountNumber> " + // K2
			//    " <AccountInternalId>172000310</AccountInternalId>" + // k2
			" <ACCT_SERVER_ID>4</ACCT_SERVER_ID> "+  // K2
			" <COMMENTS>MessedUp</COMMENTS> " +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
			String xmlCreatePaymentLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Cable Conversion Payment</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			// "  <ACCOUNTEXTERNALID>172000310</ACCOUNTEXTERNALID> " +   // K2 no payments found
			"  <ACCOUNTEXTERNALID>172000180</ACCOUNTEXTERNALID> " + // K2 Payments
			" <AMOUNT>10806</AMOUNT> " +  // K2
			"  <TRANSTYPE>990000</TRANSTYPE>" + // K2
			//    "  <ACCOUNTEXTERNALID>120319067</ACCOUNTEXTERNALID> " + // K1 Payments
			" <AMOUNT>10806</AMOUNT> " + 
			//  "  <TRANSTYPE>-9</TRANSTYPE>" +
			// " <BILLREFNO>33445937</BILLREFNO> " + // K1
			//   " <BILLREFRESETS>2</BILLREFRESETS> " +
			// "  <FIND>true</FIND>" +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
			String xmlAccountFindLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Account Find Test</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			//   "<AccountNumber>120020714</AccountNumber> " +   // K1
			"  <AccountNumber>172000126</AccountNumber> " +  // K2
			//"  <BillFname>Jesse</BillFname> " +  // K2
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
			String xmlAccountBalanceFindLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Account Balance Find Test</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"<AccountNumber>172000126</AccountNumber> " +   // K1
			//  "  <AccountNumber>172000310</AccountNumber> " +  // K2
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
	
			String xmlProdPkgFindLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Service Find (Order)</lw_object_type>" + 
			//  "  <lw_object_type>Product Package Find (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>172000310</AccountNumber> " + 
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
	
			String xmlServiceFindLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>Service Find (Order)</lw_object_type>" +
			//   "  <lw_object_type>Create Cable Service With Address Id (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			//  "<AccountNumber>001122355</AccountNumber> " +  // find services K1
			" <AccountInternalId>4</AccountInternalId> " + // service K1
			//    "<AccountNumber>172000310</AccountNumber> " +  // find services K2
			//  "<AccountInternalId>172000310</AccountInternalId> " +  // find services K2 with accountInternalId and accountServerId
			//   "  <AccountNumber>172000057</AccountNumber> " +  // for notes 
			// "  <AccountNumber>172000598</AccountNumber> " +  // for services
			// "  <AccountNumber>172000135</AccountNumber> " +    // for payments
	
			//"  <EmfConfigId>20001</EmfConfigId>" +
			//   "  <ServiceId>867.0</ServiceId> "+
			//   "  <ServiceExtIdType>30002</ServiceExtIdType> " +
			/*
	        "  <AccountNumber>172000643</AccountNumber> " +  // K2
	        " <ServiceId>9073443579</ServiceId> " + 
	        "  <ServiceExtIdType>60</ServiceExtIdType> " +
			 */
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
	
	
			String xmlResidentialDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <lw_object_type>Residential Account Create</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <reqKey>2008032619450000192</reqKey> " + 
			"  <idSwitch>GC01</idSwitch> " + 
			"  <numberOe>A0031092003</numberOe> " + 
			"  <idVoiceGateway>SADC1</idVoiceGateway> " + 
			"  <numberInterfaceGroup>01</numberInterfaceGroup> " + 
			"  <numberCrv>2003</numberCrv> " + 
			"  <numberEndPoint>1</numberEndPoint> " + 
			"  <numberTelephone>9073759473</numberTelephone> " + 
			"  <dtLastProvStateChange>2005-09-17 18:04:47</dtLastProvStateChange> " + 
			"  <errNumber>0</errNumber> " + 
			"  <errText>Success - No Error</errText> " + 
			"  <companyName>Unknown</companyName> " + 
			"  <customerName>LIRETTE, TODD</customerName> " + 
			"  <unitNum>Unknown</unitNum> " + 
			"  </lw_object_attributes>" + 
			" <lw_object_attributes>" + 
			"  <reqKey  name=\"reqKey\">2008032619450000198</reqKey> " + 
			"  <idSwitch name=\"idSwitch\">GC01</idSwitch> " + 
			"  <numberOe name=\"numberOe\">A0151301894</numberOe> " + 
			"  <idVoiceGateway name=\"idVoiceGateway\">SADC1</idVoiceGateway> " + 
			"  <numberInterfaceGroup name=\"numberInterfaceGroup\">04</numberInterfaceGroup> " + 
			"  <numberCrv name=\"numberCrv\">1894</numberCrv> " + 
			"  <numberEndPoint name=\"numberEndPoint\">2</numberEndPoint> " + 
			"  <numberTelephone name=\"numberTelephone\">9073759471</numberTelephone> " + 
			"  <dtLastProvStateChange name=\"dtLastProvStateChange\">2005-09-17 18:04:47</dtLastProvStateChange> " + 
			"  <errNumber name=\"errNumber\">0</errNumber> " + 
			"  <errText name=\"errText\">Success - No Error</errText> " + 
			"  <companyName name=\"companyName\">Unknown</companyName> " + 
			"  <customerName name=\"customerName\">LIRETTE, TODD</customerName> " + 
			"  <unitNum name=\"unitNum\">Unknown</unitNum> " + 
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
	
			String xmlLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006969</reqKey> " + 
			"  <lw_object_type>Cable Residential Account Create</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountExternalID>1000696969</AccountExternalID> " + 
			"  <BILLLNAME>THORNTON</BILLLNAME> " + 
			"  <BILLFNAME>BILLY BOB</BILLFNAME> " + 
			"  <BILLADDRESS1>18321  BARONOFF AVE</BILLADDRESS1> " + 
			"  <BillAddress2>APT 7</BillAddress2> " + 
			"  <BILLCITY>EAGLE RIVER</BILLCITY> " + 
			"  <BILLCOUNTY>ANCHORAGE BOROUGH</BILLCOUNTY> " + 
			"  <BILLSTATE>AK</BILLSTATE> " + 
			"  <BILLZIP>99577-8277</BILLZIP> " + 
			"  <DayPhone>9076943468</DayPhone> " + 
			"  <EveningPhone>9075614950</EveningPhone> " + 
			"  <AlternateAddress>18321 BARONOFF AVE</AlternateAddress> " + 
			"  <AlternateCity>EAGLE RIVER</AlternateCity> " + 
			"  <CUSTCOUNTY>ANCHORAGE BOROUGH</CUSTCOUNTY> " + 
			"  <AlternateZip>99577-8277</AlternateZip> " + 
			"  <REVRCVCOSTCTR>1</REVRCVCOSTCTR> " +
			"  <CREDITRATING>0</CREDITRATING> " + 
			"  <BULKPARENT>1</BULKPARENT> " + 
			"  <BANKRUPTCY>0</BANKRUPTCY> " + 
			"  <RETURNEDMAIL>1</RETURNEDMAIL> " + 
			"  <BILLDISPMETH>1</BILLDISPMETH> " + 
			"  <BillFormat>1</BillFormat> " + 
			"  <RateClass>1</RateClass> " + 
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
	
			String xmlEmailCreateLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006972</reqKey> " + 
			"  <lw_object_type>Add Email</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>1000696969</AccountNumber> " + 
			"  <EmailAddress>wackydoodledandy@mail.com</EmailAddress> " +
			"  <RateClass>1</RateClass> " + 
			"  <CostCenter>1</CostCenter> " + 
			"  <LastName>BALBOA</LastName> " + 
			"  <FirstName>ROCKY</FirstName> " + 
			"  <Address>1250 Pearl STREET</Address> " + 
			"  <City>Boulder</City> " + 
			"  <State>CO</State> " + 
			"  <Zip>80302</Zip> " + 
			"  <NoteCategory>PROV</NoteCategory> " +
			"  <NoteText>Creating a new whoopdi do note</NoteText> " +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String xmlAcctNoteCreateLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006974</reqKey> " + 
			"  <lw_object_type>Create Account Note Test</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>1000696969</AccountNumber> " + 
			"  <Comments>This is my latest account Note test for today</Comments> " +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String xmlServiceSuspendLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006975</reqKey> " + 
			"  <lw_object_type>Service Suspend (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>1000696969</AccountNumber> " + 
			"  <ServiceId>wackydoodledandy@mail.com</ServiceId> "+
			"  <ServiceExtIdType>51</ServiceExtIdType> " +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String xmlServiceResumeLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006976</reqKey> " + 
			"  <lw_object_type>General Service Resume(Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>1000696969</AccountNumber> " + 
			"  <ServiceId>wackydoodledandy@mail.com</ServiceId> "+
			"  <ServiceExtIdType>51</ServiceExtIdType> " +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
	
	
			String xmlAddPkgCompLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006978</reqKey> " + 
			"  <lw_object_type>Package/Component Add Test (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>1000696969</AccountNumber> " + 
			"  <ServiceId>wackydoodledandy@mail.com</ServiceId> "+
			"  <ServiceExtIdType>51</ServiceExtIdType> " +
			"  <ComponentId>402225</ComponentId> " +
			"  <PackageId>50222</PackageId> "  +
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String xmlAddNRCLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450007001</reqKey> " + 
			"  <lw_object_type>NRC Add Test (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>1000696969</AccountNumber> " + 
			//   "  <NrcId>201001</NrcId> "+
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
	
			String xmlServiceCreateLwoDoc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
			" <dataSet>" + 
			"  <reqKey>2008032619450006972</reqKey> " + 
			"  <lw_object_type>Service Create (Order)</lw_object_type>" + 
			" <LwoDataSet>" + 
			" <lw_object_attributes>" + 
			"  <AccountNumber>1000696969</AccountNumber> " + 
			"  <ServiceId>3035551000</ServiceId> " +
			"  <RateClass>1</RateClass> " + 
			"  <CostCenter>1</CostCenter> " + 
			"  <LastName>THECAR</LastName> " + 
			"  <FirstName>MYMOTHER</FirstName> " + 
			"  <Address>1250 Pearl STREET</Address> " + 
			"  <City>Boulder</City> " + 
			"  <State>CO</State> " + 
			"  <Zip>80302</Zip> " + 
			"  </lw_object_attributes>" + 
			"  </LwoDataSet>" + 
			"  </dataSet>";
	
	
			String FxDomDocument  = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " +
			//" <?xml version='1.0' encoding='UTF-8'?> " +
			"   <Request> " +
			"   <Header> " +
			"    <OperatorName e-dtype=\"string\">trainee</OperatorName>  " +
			"   <ApplicationName e-dtype=\"string\">DOMExample1.java</ApplicationName>  " +
			"    </Header>  " +
			"   <AccountLocateFind>  " +
			"    <AccountLocate>  " +
			"    <Fetch e-dtype=\"boolean\">true</Fetch>  " +
			"    <BillFname>  " +
			"    <Like e-dtype=\"string\">B%</Like>  " +
			"    </BillFname>   " +
			"     <Key>  " +
			"    <Fetch e-dtype=\"boolean\">true</Fetch>  " +
			"      </Key>  " +
			"    </AccountLocate>  " +
			"    </AccountLocateFind>  " +
			"    </Request>  " ;
	
	
			String processFlag = STR_PROCESS_FLAG_PROCESSED;
			String message = "";
			ArrayList xmlDocs = new ArrayList();
			xml_on = false;
			autoSubmit = false;
	
			try
			{   
	
	
				FileObject fo = new FileObject();
				if (autoSubmit){
	
					if (xml_on){
						if (args.length == 0 )
						{
							logger.error("EA9-Insufficient number of arguments supplied for cpniExtract.");
							System.exit(1);
						}
	
						if (args.length > 0) {
	
							inputTestFile = args[0];
	
							if (args.length < 2)
							{
								logger.error("EA9-Insufficient number of arguments supplied for Evaluatint LWO transactions.");
								System.exit(1);
							}
	
							fxVersion = args[1];
							if (fxVersion.indexOf("1.0") < 0 && fxVersion.indexOf("2.0") < 0)
							{
								logger.error("EA9-Insufficient number of arguments supplied for Evaluatint LWO transactions.");
								System.exit(1);
							}             
						}
					}
	
					RandomAccessFile f = fo.openReadFile("/"+inputTestFile);
	
					String LwoXmlDoc = "";
					String line = "";
					boolean done = false;
					int docCount = 0;
					while (!done)
					{
						while ((line = f.readLine()) != null)
						{
							line = line.trim();
							if (line.length() == 0 ||
									line.indexOf("//")== 0)
								continue;
							if (line.indexOf("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>") > -1) { 
								if (LwoXmlDoc.length() > 0)
									break;
							}
							LwoXmlDoc = LwoXmlDoc + " "+line;
							log.debug(line)  ;  
						}// end inner while
						if (line == null)
							done = true;
						LwObjectType nextlwObject = null;
						try{
							if (LwoXmlDoc.trim().length() > 0)
								nextlwObject = getXLMData(LwoXmlDoc.trim());
						} 
						catch(Exception ex)
						{}
						LwoXmlDoc = line;   
						if (nextlwObject != null){
							xmlDocs.add(docCount,nextlwObject);
							docCount++;
						}
	
	
					}// end outer while
	
					f.close();
				} // autoSubmit on
				else{
	
	
					  LwObjectType singleLwObject = getXLMData(xmlAccountBalanceFindLwoDoc);
	
					//  LwObjectType singleLwObject = getXLMData(xmlAccountNoteLwoDoc);
	
	
					//   LwObjectType singleLwObject = getXLMData(xmlBillInvoiceFindLwoDoc);
	
					//     LwObjectType singleLwObject = getXLMData(xmlAccountServiceUpdateLwoDoc);
	
	
					//    LwObjectType singleLwObject = getXLMData(xmlAccountUpdateLwoDoc);
	
					// LwObjectType singleLwObject = getXLMData(xmlCreateWirelessDoc);
	
					//  LwObjectType singleLwObject = getXLMData(xmlCreateCblModemDoc);
	
					//       LwObjectType singleLwObject = getXLMData(xmlAccountNoteLwoDoc);
	
	
				//	LwObjectType singleLwObject = getXLMData(xmlCreatePaymentLwoDoc);
	
					//   LwObjectType singleLwObject = getXLMData(xmlAccountFindLwoDoc);   // focus development
	
	
					//   LwObjectType singleLwObject = getXLMData(xmlServiceFindLwoDoc);   // focus development
	
					// LwObjectType singleLwObject = getXLMData(xmlLwoDoc); // Demo Create Account
	
					//    LwObjectType singleLwObject = getXLMData(xmlProdPkgFindLwoDoc);
	
					// LwObjectType singleLwObject = getXLMData(xmlServiceFindOrderLwoDoc);
	
					xmlDocs.add(0,singleLwObject);
				}  // autoSubmit off
	
	
				// set up access to get LWO Order, SO  and SO Items
				C30SDKObject lwo = null;
	
				C30SDKObject returnlwo = null; 
	
				//String userName = "jwood";
				//String userPassword = "csgfx";
	
				String userName = "lwocsr";
				String userPassword = "kenanfx";
				C30SDKUser lwUser = null;
				/*
	           for (int i=0; i<10; i++ ) {
	
	                  String sessionId = UUID.randomUUID().toString();
	
	                  sm = SecurityManagerFactory.createSecurityManager("KenanFx", "arborsv", sessionId, "kenanfx");
	
	                  sm.authenticate();
	
	                  SecurityManagerFactory.cleanupSecurityManager(sm);
	                  sm.logout();
	
	              }
				 */
	
	
	
	
				C30SDKFrameworkFactory psFactory;
				if (fxVersion.indexOf("1.0") > -1)
					psFactory = new C30SDKFrameworkFactory("CSGFx", "arborsv", "12345", "csgfx", "FX1.0/PS2.0");  // K1
				else
					psFactory = new C30SDKFrameworkFactory("KenanFx", "arborsv", "12345", "kenanfx", "FX2.0/PS2.0"); // K2
	
				/* lwUser =  psFactory.createLightweightUser("lwocsr", "kenanfx");
	        lwUser.authenticate();
				 */ 
	
				for(int i = 0; i < xmlDocs.size(); i++)
				{              
					// lets use the parsed xml document to build an LWO 
					LwObjectType lwObject = (LwObjectType)xmlDocs.get(i);  
					log.debug("lwObjectType: "+lwObject.lwObjType);
					lwo = psFactory.createC30SDKObject(lwObject.lwObjType);
					//  lwo = psFactory.createLightweightObject(lwObject.lwObjType,userName,userPassword,null);
					// lwo = psFactory.createLightweightObject(lwObject.lwObjType,lwUser);
					Enumeration la = lwObject.lwObjTransactions.elements();
					while (la.hasMoreElements())
					{  Hashtable attrData = (Hashtable)la.nextElement();
					Enumeration ad = attrData.keys();
					while (ad.hasMoreElements())
					{ 
						String externalName = (String)ad.nextElement();
						String attrValue = (String)attrData.get(externalName);
						log.debug("externalName: "+externalName+", Value: "+attrValue);
						lwo.setNameValuePair(externalName,attrValue);
					}
	
					}
	
	
	
	
					String lwoAttr = "";
					log.debug("REQUEST SUBMITTED FOR PROCESSING");
					log.debug(" ");
					printHashMap(lwo.toHashMap(false));
					returnlwo = lwo.process();
					log.debug("REQUEST RETURNED FROM PROCESSING");
					log.debug(" ");
					HashMap lwoResults = returnlwo.toHashMap(false);
					printHashMap(lwoResults);
					
					
					Iterator iter = lwoResults.entrySet().iterator(); 
	
					for (int j = 0; j < lwoResults.size(); j++)
					{ 
	
						HashMap classAttr = (HashMap)lwoResults.get(""+j);
						log.debug("LW_OBJECT LEVEL: "+j);
	
						Iterator mapIiter = classAttr.entrySet().iterator();    
						while (mapIiter.hasNext()) 
						{ 
							Map.Entry classEntry = (Map.Entry)mapIiter.next();      
							String attrKey = (String)classEntry.getKey();
							String attrValue = (String)classEntry.getValue();
							log.debug("Attr Name: "+attrKey+", Attr Value: "+attrValue);
	
						}
	
					}
	
	
	
	
					lwoAttr = returnlwo.toString();
					// log.debug(lwoAttr);
					//lwo.setNameValuePair("Account Number", LwOrder.getAccountExtId());
					message = "";
					String processInd = "Y";
					if(returnlwo instanceof C30Order)
						message = "Order ID: " + lwo.getAttributeValueAsString("OrderId");
					else
						if(returnlwo instanceof C30Account)
							message = "Internal ID: " + lwo.getAttributeValueAsString("AccountInternalId") + ", External ID: " + lwo.getAttributeValueAsString("AccountExternalId");
						else
							if(returnlwo instanceof C30BillInvoice)
								message = "Bill Ref No: " + lwo.getAttributeValueAsString("BillRefNo") + ", Bill Ref Resets: " + lwo.getAttributeValueAsString("BillRefResets");
							else
								if(returnlwo instanceof C30Deposit)
									message = "Tracking ID: " + lwo.getAttributeValueAsString("TrackingId") + ", Tracking ID Serv: " + lwo.getAttributeValueAsString("TrackingIdServ");
	
					//  else
					if(returnlwo instanceof C30SDKExceptionMessage)
					{
						processInd = "E";
						processFlag = STR_PROCESS_FLAG_FAILED+":" + returnlwo.getAttributeValueAsString("ExceptionCode");
						message =  " ERROR MESSAGE - " + returnlwo.getAttributeValue("MsgText") + "";
						message = message + "STACKTRACE - " + returnlwo.getAttributeValue("StackTrace") + "";
					}
					if (message.trim().length() > 0)                    
						log.debug("Result: "+ message);
					if (message.length() > 999 )
						message = message.substring(0,998);
	
				}// end of the loop to process the documents
			}
			catch (C30SDKException e)
			{
				log.debug("Exception Code: "+ e.getExceptionCode());
				log.debug("Exception: "+e.getMessage());
				e.printStackTrace();
				System.exit(0);
			}
			catch (Exception e)
			{
				e.printStackTrace();
	
				log.debug("Exception: "+e.getMessage());
				System.exit(0);
			}  
	
	
			System.exit(0);
	
	
		}
	
	
	
		public static String generateNextId()
		{
	
			SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMddHHmmss0SSSS");
			String ss = formatter.format(new java.util.Date());
			while (ss.equals(formatter.format(new java.util.Date()))) {}
			return formatter.format(new java.util.Date());
	
		}
	
	
	
		//-----------------------------------------------------------------------
		/**
		 *   Get data items from DOM document
		 *   
		 *   @param xmlDoc the xml document to be parsed
		 *   @return Hashtable containing a HashMap of item.
		 */
		public static LwObjectType getXLMData(String xmlDoc ) throws Exception
		{
	
	
			Hashtable lwoOjbCollection = new Hashtable();
			Hashtable lwoObjData =  new Hashtable();
			String numberEndPoint= "";
			String numberTelephone= "";
			String lw_object_type = "";
			Document dom;
			Vector elementData;
			LwObjectType lwoTransactions = null;
	
	
			try{ 
	
	
				/* xmlDoc is a String of xml */
				byte aByteArr [] = xmlDoc.getBytes();
				ByteArrayInputStream bais = new ByteArrayInputStream (aByteArr, 0, aByteArr.length);
				// domParser.parse(bais);
	
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				//Using factory get an instance of document builder
				DocumentBuilder db = dbf.newDocumentBuilder();
	
				//parse using builder to get DOM representation of the XML file
				dom = db.parse(bais);
	
				Element docEle = dom.getDocumentElement();
	
	
				lw_object_type = dom.getElementsByTagName("lw_object_type").item(0).getFirstChild().getNodeValue();
				if (lw_object_type == null)
				{
					log.debug("xmlDoc not functional.");
					Exception nex = new Exception("xmlDoc not functional.");
					throw nex;
				}
				lwoTransactions = new  LwObjectType(lw_object_type); 
				String reqKey = dom.getElementsByTagName("reqKey").item(0).getFirstChild().getNodeValue();
				if (reqKey == null)
				{
					log.debug("xmlDoc not functional.");
					Exception nex = new Exception("xmlDoc not functional.");
					throw nex;
				}
	
				lwoTransactions.requestKey = reqKey;
				// Get a list of all port elements in the document
				Element lwoObjects = dom.getDocumentElement();
				NodeList objlist = lwoObjects.getElementsByTagName("lw_object_attributes");
	
	
				for( int i=0; i<objlist.getLength(); i++ ) {
	
					lwoObjData =  new Hashtable();
					elementData = DomUtil.getElementNamesVector((Element)objlist.item(i));
					for (int j=0; j < elementData.size(); j++){
						String nameVal = (String)elementData.get(j);
						String value = DomUtil.getSimpleElementText((Element)objlist.item(i), nameVal );
						if (value != null)
							lwoObjData.put(nameVal ,value );
	
					}
	
					lwoTransactions.lwObjTransactions.put(""+i,lwoObjData);
	
	
				}
			}catch(ParserConfigurationException pce) {
				pce.printStackTrace();
			}catch(SAXException se) {
				se.printStackTrace();
			}catch(IOException ioe) {
				ioe.printStackTrace();
	
	
			} catch (Exception ex)
			{
				lwoObjData.put("ERROR","XML Parsing error for xmlDoc");
				lwoOjbCollection.put("ERROR",lwoObjData);
				log.debug( "getMtaPorts:  ERROR XML Parsing error for xmlDoc" );
	
			}
	
	
			return lwoTransactions;
			// return lwoOjbCollection;
	
		}
	
	
	
	
	
	
	
		public static class DomUtil
		{
			public static Element getFirstElement( Element element, String name ) {
				NodeList nl = element.getElementsByTagName( name );
				if ( nl.getLength() < 1 )
					throw new RuntimeException(
							"Element: "+element+" does not contain: "+name);
				return (Element)nl.item(0);
			}
	
			public  static String getSimpleElementText( Element node, String name ) throws Exception
			{
				Element namedElement = getFirstElement( node, name );
				return getSimpleElementText( namedElement );
			}
	
			public  static String getSimpleElementText( Element node ) 
			{
				StringBuffer sb = new StringBuffer();
				NodeList children = node.getChildNodes();
				for(int i=0; i<children.getLength(); i++) {
					Node child = children.item(i);
					if ( child instanceof Text )
						sb.append( child.getNodeValue() );
				}
				return sb.toString();
			}
	
	
			public  static Vector getElementNamesVector( Element node ) 
			{
				Vector elementNames = new Vector();
	
				NodeList children = node.getChildNodes();
				for(int i=0; i<children.getLength(); i++) {
					Node child = children.item(i);
					if (child.getNodeType() == 1)  // Deferred element impl
					elementNames.add(child.getNodeName());
	
				}
				return elementNames;
			}
	
	
	
		}
	
	
	
		public static class Parser
		{
			public static String parseStr = "";
	
			public static String parseit(String source, char delim){
				String target = "";
				int startpos = parseStr.length();
	
				for (int i = startpos; i< source.length(); i++){
					parseStr = parseStr + source.substring(i,i+1);
					if (source.charAt(i) != delim )
						target = target + source.substring(i,i+1);
					else
						break;
				}
				return target;
	
			}
	
			public static String parseit(String source, char delim,ParseStringObject parsedData){
				String target = "";
	
				int startpos = 0;
	
				if (parsedData == null)
					parsedData = new ParseStringObject();
	
				startpos = parsedData.length();
	
	
				for (int i = startpos; i< source.length(); i++){
					parsedData.add(source.substring(i,i+1));
	
					if (source.charAt(i) != delim )
						target = target + source.substring(i,i+1);
					else
						break;
				}
				return target;
	
			}
	
	
			public static String delimitData(String theData,String theChar){
	
				String replacementText = "";
				if (theData.lastIndexOf(theChar) <= 0)
					return theData;
				// replace all spaces with underscores
				for (int ii = 0; ii < theData.length(); ii++){
					if (theData.charAt(ii) == theChar.charAt(0))
						replacementText = replacementText + theChar+theChar;
					else
						replacementText = replacementText + theData.charAt(ii);
	
				}
				return replacementText;
			}
	
	
			public static String padStringDir(String direction,String aStrValue,int padToLen,String fillChar){
				if (direction.trim().length() == 0 || direction.trim().toUpperCase().equals("L"))
					return  padString(aStrValue,padToLen,fillChar);
				// fill to the right of the string  
				String strVal = aStrValue;
				int fillerWidth = fillChar.length();
				int fillLen = padToLen - strVal.length();
				if (fillLen <= 0)
					return strVal;
				if (fillerWidth + strVal.length() > padToLen)
					return strVal;
				while (strVal.length() < padToLen)
					strVal = strVal+fillChar;
				return strVal;
	
			}
	
			public static String padString(String aStrValue,int padToLen,String fillChar){
				String strVal = aStrValue;
				int fillerWidth = fillChar.length();
				int fillLen = padToLen - strVal.length();
				if (fillLen <= 0)
					return strVal;
				if (fillerWidth + strVal.length() > padToLen)
					return strVal;
				while (strVal.length() < padToLen)
					strVal = fillChar + strVal;
				return strVal;
	
			}
	
			public static int StringToInt (String aStr){
				int result = 0;
				int integer_value = 0;
				boolean negative = false;
				if (aStr != null)
					for (int i = 0; i < aStr.length() 
					&& (aStr.charAt(i) == '-' || (aStr.charAt(i) >= '0' && aStr.charAt(i) <= '9'))
					&& aStr.charAt(i) != ' '; i++){
						if (aStr.charAt(i) == '-')
							if (i == 0)
								negative = true;
							else
								return 0;
						else
						{
							integer_value = aStr.charAt(i) - '0';
							result = result * 10 + integer_value;
						}
					}
				if (negative && result > 0)
					result = result * -1;
	
				return result;
			}
	
			public static double StringToDouble (String aStr){
				double results;
				double translated_double = 0.0;
				try{
	
					results = Double.valueOf(aStr).doubleValue();
	
	
				}
				catch (Exception ex){
					results = -0.00009;
				}
				return results;
			}
	
			public static long StringTolong (String aStr){
				long result = 0;
				long integer_value = 0;
				boolean negative = false;
				if (aStr != null)
					for (int i = 0; i < aStr.length() 
					&& (aStr.charAt(i) == '-' || (aStr.charAt(i) >= '0' && aStr.charAt(i) <= '9'))
					&& aStr.charAt(i) != ' '; i++){
						if (aStr.charAt(i) == '-')
							if (i == 0)
								negative = true;
							else
								return 0;
						else
						{
							integer_value = aStr.charAt(i) - '0';
							result = result * 10 + integer_value;
						}
					}
				if (negative && result > 0)
					result = result * -1;
	
				return result;
			}
	
	
	
	
			public static String strReplace(String targetData,String searchStr,String replaceStr){
				String retval = targetData.trim();
				String temp1 = "";
				String temp2 = "";
				int replaceLen = replaceStr.length();
				int strIndx = 0;
	
				if (retval.equals(searchStr.trim()))
					return replaceStr;
	
				strIndx = retval.indexOf(searchStr);
				while (strIndx >= 0){
					temp1  = retval.substring(0,strIndx);
					temp2 = retval.substring(strIndx + searchStr.length(),retval.length());
					if (replaceLen == 0)
						retval = temp1 + temp2;
					else
						retval = temp1+replaceStr+temp2;
	
					strIndx = retval.indexOf(searchStr);
				}
				return retval;
			}
	
	
			public static String generateNextId()
			{
	
				SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMddHHmmss0SSSS");
				String ss = formatter.format(new java.util.Date());
				while (ss.equals(formatter.format(new java.util.Date()))) {}
				return formatter.format(new java.util.Date());
	
			}
	
	
	
			public static String replace(String str, String pattern, String replace) {
				int s = 0;
				int e = 0;
				StringBuffer result = new StringBuffer();
	
				while ((e = str.indexOf(pattern, s)) >= 0) {
					result.append(str.substring(s, e));
					result.append(replace);
					s = e+pattern.length();
				}
				result.append(str.substring(s));
				return result.toString();
			}
	
	
	
		} // end of Parser class
	
	
		// this class defines a single external Id and carries all items associated with it as
		// contained in a single order
		public static class ParseStringObject
		{
			private String dataString = "";
	
			public ParseStringObject(){}
	
	
	
			public ParseStringObject(String aDataString){
				this.dataString = aDataString;
			}
	
			public int length(){
				return dataString.length();
			}
	
			public void add(String aDataString){
				dataString = dataString + aDataString;
			}
	
			public void add(char aChar){
	
				dataString = dataString + aChar;
			}
	
			public int indexOf(String aCompareString){
				return dataString.indexOf(aCompareString);
			}
	
			public int lastIndexOf(String aCompareString){
				return dataString.lastIndexOf(aCompareString);
			}
			public String substring(int startPos,int endPos){
				return dataString.substring(startPos,endPos);
			}
			public String value(){
				return dataString;
			}
	
		} // end of ParseStringObject class
	
	
		public static class FileObject
		{
			/**
			 * this mehtod opens a file for reading
			 * @param strPath
			 * @return handle to file open for reading
			 * @throws java.io.IOException
			 */
			public FileObject(){}
	
			public  InputStream getStream(String strPath) throws IOException{
				URL fileHandle = this.getClass().getResource(strPath);
				return (this.getClass().getResourceAsStream(strPath));
	
	
			}
	
	
			/**
			 * this mehtod opens a file for reading
			 * @param strPath
			 * @return handle to file open for reading
			 * @throws java.io.IOException
			 */
			public RandomAccessFile openReadFile(String strPath) throws IOException{ 
				URL fileHandle = this.getClass().getResource(strPath);
				return (new RandomAccessFile(fileHandle.getFile(),"r"));
			}
	
			/**
			 * this method opens a file for writing
			 * @param strPath
			 * @return handle to a file open for writing
			 * @throws java.io.IOException
			 */
			public  RandomAccessFile openWriteFile(String strPath) throws IOException{  
				URL fileHandle = this.getClass().getResource(strPath);
				return ( new RandomAccessFile(fileHandle.getFile(),"rw"));
			}
	
	
	
			/**
			 * This method reads the next line in a file open for reading
			 * @param inStream
			 * @return 
			 * @throws java.io.IOException
			 */
			public  String readFile(RandomAccessFile inStream) throws IOException {
				String line = inStream.readLine();
				if (line.length() == 0){
					IOException ex = new IOException();
					throw ex;
				}
				else
	
					return line;
	
			}
	
	
	
			/**
			 * This writes to a file opened for output
			 * @param outStream
			 * @param rawText
			 * @throws java.io.IOException
			 */
			public  void writeFile(RandomAccessFile outStream,String rawText) throws IOException {
				// assume the output file has been open
				if (rawText == null){
					IOException newex = new IOException();
					throw newex;
				}
				rawText = rawText + "\n";
				outStream.writeBytes(rawText);
	
			}
	
			public  boolean isDir(String dirName) throws IOException
			{
				boolean retval = false;
				File dir = new File(dirName);
				if (dir.isDirectory())
					return true;
	
				return retval;
			}
	
			//InputStream is = getClass().getResourceAsStream(fileName);
	
			public  String findFile(String dirName, String fileExt) throws IOException
			{
				File dir = new File(dirName);
	
				String[] children = dir.list();
				if (children == null) {
					return "ERROR: "+ dirName+" as Directory not found." ;// Either dir does not exist or is not a directory
				} else {
					for (int i=0; i<children.length; i++) {
						// Get filename of file or directory
						String filename = children[i];
						if (filename.indexOf(fileExt) > -1)
							return filename;
					}
				}
				return "";
	
			}
		}// end FileObject class
	
		/**
		 * Utility method for printing HashMaps.
		 * 
		 * @param map HashMap to be printed.
		 */
		public static void printHashMap(Map map) {
			try {
				// Create a BufferedWriter
				BufferedWriter bw = new BufferedWriter(new PrintWriter(System.out));
	
				// Pass the map to the ServiceException object's print() method
			//	ServiceException.printMap(bw, map, 2);
	
				// Flush the BufferedWriter
				bw.flush();
			}
	
			// Handle input/output exceptions
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
