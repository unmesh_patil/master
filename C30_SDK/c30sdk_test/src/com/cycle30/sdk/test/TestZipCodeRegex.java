package com.cycle30.sdk.test;

import java.util.regex.Pattern;

public class TestZipCodeRegex
{

	static final String regex1 = "^\\d{5}(-\\d{4})?$";
	static final String regex2 = "^\\d{9}";
	public static void main(String args[]) 
	{


		String mixedZips[] = {"50266-234A", "502662342", "5026A-2344","5026A-234A", "50266", "230" };
		int index = 0;
		
		boolean isMatch = false;
		
		while (index < mixedZips.length) {
					isMatch = isAValidZipCode(mixedZips[index]);
					System.out.println("Zip " + mixedZips[index] + " - "+ (isMatch ? "Valid" : "Invalid"));
					index ++;
			}

	}

	private static boolean isAValidZipCode(String string) {
		// TODO Auto-generated method stub
		return Pattern.matches(regex1, string) ||  Pattern.matches(regex2, string);
	}
	

}