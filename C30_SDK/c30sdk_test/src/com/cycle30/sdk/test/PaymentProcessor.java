package com.cycle30.sdk.test;



import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectException;

/*
 * This is used to create the Dynamic Payment Structure 
 * Mostly used by the POS (Point of Sales) system for posting
 * Different Payment Tendors in Single Transaction
 */

public class PaymentProcessor {

	//public static final String PAYMENT_PROCESSOR_XSD = "com/cycle30/sdk/lightweight/config/PaymentProcessor.xsd";

	public static final String PAYMENT_PROCESSOR_XSD = "<?xml version=\"1.0\"?> " +
	"  <xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"" +
	"		 targetNamespace=\"www.cycle30.com\"" +
	"		              xmlns=\"www.cycle30.com\"" +
	"		              elementFormDefault=\"qualified\">" +
	"		  <xs:element name=\"PaymentProcessor\" type=\"PaymentProcessorType\" />" +
	"		  <xs:complexType name=\"PaymentProcessorType\">" +
	"	    <xs:sequence>" +
	"		  <xs:element name=\"RequestKey\" type=\"xs:integer\" minOccurs=\"1\" />" +
	"		  <xs:element name=\"AccountNumber\" type=\"xs:string\"  minOccurs=\"1\" />" +
	"		  <xs:element name=\"AdditionalNotes\" type=\"xs:string\"  minOccurs=\"1\" />" +
	"		  <xs:element name=\"Payments\">" +
	"		  <xs:complexType> " +
	"			<xs:sequence>" +
	"				<xs:element name=\"Payment\" type=\"PaymentType\" minOccurs=\"1\"  maxOccurs=\"unbounded\"/>" +
	"			</xs:sequence>" +
	"		  </xs:complexType>" +
	"		  </xs:element>" +
	"		</xs:sequence>" +
	"	  </xs:complexType>" +
	"	  <xs:complexType name=\"PaymentType\">" +
	"	    <xs:sequence>" +
	"			<xs:element name=\"TenderType\" type=\"xs:string\" minOccurs=\"1\" />" +
	"			<xs:element name=\"Amount\" type=\"xs:integer\"  minOccurs=\"1\" />" +
	"		</xs:sequence>" +
	"	  </xs:complexType>" +
	"	</xs:schema>";

	/**
	 * This parses the input xml and convert this into a HashMap.
	 * @param paymentXML - the paymentXML that will be validated against PaymentProcessor.xsd 
	 * @throws C30SDKException if there was a problem loading the object attribute defaults.
	 */
	
	public PaymentProcessor(HashMap paymentMap, C30SDKFrameworkFactory psFactory) throws C30SDKException
	{		
		//The recieved paymentMap will be in the format of 
		
	}

	public PaymentProcessor(String samplePaymentXML) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param paymentXML
	 * @param paymentProcessorXsd
	 * @throws C30SDKException
	 */
	private void parseXML(String paymentXML, String paymentProcessorXsd)  throws C30SDKException
	{
		/* paymentXML is a String of xml */
		byte aByteArr [] = paymentXML.getBytes();
		ByteArrayInputStream bais = new ByteArrayInputStream (aByteArr, 0, aByteArr.length);

		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser parser = null;
		spf.setNamespaceAware(true);
		try {
			SchemaFactory sf =
				SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
			spf.setSchema(sf.newSchema(new SAXSource(new InputSource(new StringReader(PAYMENT_PROCESSOR_XSD)))));
			spf.setValidating(false);
			parser = spf.newSAXParser();
			MySAXHandler handler = new MySAXHandler(); 

			parser.parse(new InputSource(new StringReader(paymentXML)), handler);
			

		}
		catch(SAXException e) {
			e.printStackTrace();
			String error = "An error occurred while processing PaymentProcessor XML. " + System.err;
			throw new C30SDKObjectException(error, e);

		} 
		catch(ParserConfigurationException e) {
			e.printStackTrace();
			String error = "An error occurred while processing PaymentProcessor XML. " + System.err;
			throw new C30SDKObjectException(error, e);

		}
		catch (IOException e) {
			e.printStackTrace();
			String error = "An error occurred while processing PaymentProcessor XML. " + System.err;
			throw new C30SDKObjectException(error, e);

		} 

	}

}
class MySAXHandler extends DefaultHandler {
	public void startDocument() {
		//System.out.println("Start document: ");
	}    
	public void endDocument()  {
		//System.out.println("End document: ");
		printHashMap(request);
	}

	HashMap paymentProc = new HashMap();
	HashMap request = new HashMap();
	HashMap[] payment = new HashMap[50];
	int paymentListIndex=0;
	boolean isreqKey = false;
	boolean isPaymentProcessor = false;
	boolean isAccountNumber = false;
	boolean isPayments = false;
	boolean isPayment = false;
	boolean isTendorType = false;
	boolean isAmount = false;
	public void startElement(String uri, String localName, String qname, 
			Attributes attr)
	{
		if (qname.equalsIgnoreCase("REQKEY"))
		{
			isreqKey = true;
		}
		if (qname.equalsIgnoreCase("PAYMENTPROCESSOR"))
		{
			isPaymentProcessor = true;
		}
		if (qname.equalsIgnoreCase("ACCOUNTNUMBER"))
		{
			isAccountNumber = true;
		}
		if (qname.equalsIgnoreCase("PAYMENTS"))
		{
			isPayments = true;
		}
		if (qname.equalsIgnoreCase("PAYMENT"))
		{
			isPayment = true;
		}
		if (qname.equalsIgnoreCase("TENDERTYPE"))
		{
			isTendorType = true;
		}
		if (qname.equalsIgnoreCase("AMOUNT"))
		{
			isAmount = true;
		}
		
		System.out.println("Start element: local name: " + localName + " qname: " 
				+ qname + " uri: "+uri);
		int attrCount = attr.getLength();
		if(attrCount>0) {
			System.out.println("Attributes:"); 
			for(int i = 0 ; i<attrCount ; i++) {
				/*System.out.println("  Name : " + attr.getQName(i)); 
				System.out.println("  Type : " + attr.getType(i)); 
				System.out.println("  Value: " + attr.getValue(i)); */
			}
		} 
	}

	public void endElement(String uri, String localName, String qname) {
		System.out.println("End element: local name: " + localName + " qname: "
				+ qname + " uri: "+uri);
		
	}

	public void characters(char[] ch, int start, int length) {
		System.out.println("Characters: " + new String(ch, start, length));
		if (isreqKey)
		{
			paymentProc.put("ReqKey", new String(ch, start, length));
			isreqKey=false;
		}
		if (isAccountNumber)
		{
			paymentProc.put("AccountNumber", new String(ch, start, length));
			isAccountNumber=false;
		}
		if (isPayments)
		{
			paymentProc.put("Payments", payment);
			isPayments=false;
		}
		if (isPayment)
		{
			payment[paymentListIndex] = new HashMap();
			paymentListIndex++;
			isPayments=false;
		}
		if(isPaymentProcessor)
		{
			request.put("PaymentProcessor",paymentProc);
			isPaymentProcessor=false;
		}
		
	}

	public void ignorableWhitespace(char[] ch, int start, int length) {
		System.out.println("Ignorable whitespace: " + new String(ch, start, length));
	}

	public void startPrefixMapping(String prefix, String uri) {
		System.out.println("Start \"" + prefix + "\" namespace scope. URI: " + uri); 
	}

	public void endPrefixMapping(String prefix) {
		System.out.println("End \"" + prefix + "\" namespace scope."); 
	}

	public void warning(SAXParseException spe) {
		System.out.println("Warning at line "+spe.getLineNumber());
		System.out.println(spe.getMessage());
	}

	public void fatalError(SAXParseException spe) throws SAXException {
		System.out.println("Fatal error at line "+spe.getLineNumber());
		System.out.println(spe.getMessage());
		throw spe;
	}

	/**
	 * Utility method for printing HashMaps.
	 * 
	 * @param map HashMap to be printed.
	 */
	public static void printHashMap(Map map) {
		try {
			// Create a BufferedWriter
			BufferedWriter bw = new BufferedWriter(new PrintWriter(System.out));

			// Pass the map to the ServiceException object's print() method
			//ServiceException.printMap(bw, map, 2);

			// Flush the BufferedWriter
			bw.flush();
		}

		// Handle input/output exceptions
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	

}
