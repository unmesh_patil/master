package com.cycle30.c30api.awnprepaid.junit;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.c30api.junit.ApiAuthenticationInit;
import com.cycle30.c30api.junit.HttpUtils;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiVoucher  extends ApiAuthenticationInit{
	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void voucherGet() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();

		String body = getVoucher(params, "96920843825961");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	private String getVoucher(MultivaluedMap params, String voucherId)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/voucher/"+voucherId+"/status");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}
	
}
