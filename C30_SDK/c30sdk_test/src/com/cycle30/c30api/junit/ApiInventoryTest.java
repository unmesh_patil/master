package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiInventoryTest extends ApiAuthenticationInit {

	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void getPhoneInventory1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		params.add("community", "ANCHORAGE");
		params.add("size", "5");
		String body = getPhoneInventory(params);
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	@Test
	public void getCoreInventory1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		params.add("InventoryType", "3");
		params.add("InventoryValue", "2061234568");
		String body = getCoreInventory(params);
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}
	
	@Test
	public void getSimInventory1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		//params.add("community", "ANCHORAGE");
		String body = getSimInventory(params,"1243567");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	private String getPhoneInventory(MultivaluedMap params)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/inventory/phones");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}
	private String getSimInventory(MultivaluedMap params, String iccid)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/inventory/sim/"+iccid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}
	
	private String getCoreInventory(MultivaluedMap params)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/inventory");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}
}
