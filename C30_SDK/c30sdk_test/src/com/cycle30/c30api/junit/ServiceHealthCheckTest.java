package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ServiceHealthCheckTest extends ApiAuthenticationInit {

	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void getServiceHealthCheckDetails1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		params.add("key", "BillingStatus");
		params.add("key", "NetworkStatus");
		params.add("includeHistory", "no");
		//params.add("historyDays", "90");//

		String telephoneNumber = "9077391624";
		
		String body = HttpUtils.getHttpRequest(params,"/service/"+telephoneNumber+"/healthcheck");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}


	

}
