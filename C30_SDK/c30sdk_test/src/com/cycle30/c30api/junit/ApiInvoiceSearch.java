package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiInvoiceSearch extends ApiAuthenticationInit {

	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void InvoiceFind1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		params.add("size", "10");
		params.add("start", "1");
		String body = getInvoiceById(params, "a08d000000Qz5GeAAJ");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	
	private String getInvoiceById(MultivaluedMap params, String acctNo)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/account/"+acctNo+"/invoices");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}


}
