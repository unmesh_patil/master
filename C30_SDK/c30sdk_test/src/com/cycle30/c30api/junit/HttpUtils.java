package com.cycle30.c30api.junit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.ws.rs.core.MultivaluedMap;

import org.w3c.dom.Document;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class HttpUtils  extends ApiAuthenticationInit {

	public static  String getHttpRequest(MultivaluedMap params, String url) throws Exception
	{
		WebResource webResource = client.resource(SERVER_URL); 
		WebResource.Builder wrb = webResource.path(url)
				.queryParams(params)
				.getRequestBuilder();

		wrb.header("X-Trans-Id", X_Trans_Id);
		wrb.header("X-Env-Id",  X_ENV_ID);
		wrb.header("X-Org-Id", X_ORG_ID);
		//wrb.header("X-Client-Id", X_ORG_ID);

		wrb.header("Date", DATE);
		wrb.header("X-Digest",X_Digest);
		log.info("Date = "+ DATE+"; Digest = "+ X_Digest);
		HostnameVerifier hv = new HostnameVerifier(){
			public boolean verify(String urlHostName, SSLSession session)
			{
				System.out.println("Warning: URL Host: " + urlHostName + " vs. "
						+ session.getPeerHost());
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		trustAllHttpsCertificates();
		String body ="";
		try{
		 body =wrb.get(String.class);
			
		}
		catch(com.sun.jersey.api.client.UniformInterfaceException e)
		{
			ClientResponse cr = e.getResponse();
			System.out.println(cr.getEntity(String.class));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println(body);
		return body;
	}

	public static  String postOrder(Document xmlRequest, String url) throws Exception {
		WebResource webResource = client.resource(SERVER_URL); 
		WebResource.Builder wrb = webResource.path(url)
				.getRequestBuilder();

		wrb.header("X-Trans-Id", X_Trans_Id);
		wrb.header("X-Env-Id",  X_ENV_ID);
		wrb.header("X-Org-Id", X_ORG_ID);
		wrb.header("Date", DATE);
		wrb.header("X-Digest",X_Digest);
		//wrb.header("X-Digest",X_Digest);
		log.info("Date = "+ DATE+"; Digest = "+ X_Digest);
		HostnameVerifier hv = new HostnameVerifier(){
			public boolean verify(String urlHostName, SSLSession session)
			{
				System.out.println("Warning: URL Host: " + urlHostName + " vs. "
						+ session.getPeerHost());
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		trustAllHttpsCertificates();

		ClientResponse clientResponse = wrb.post(ClientResponse.class, xmlRequest);

		int status = clientResponse.getStatus();
		String body = clientResponse.getEntity(String.class);
		log.info(body);
		System.out.println(body);
		return body;
	}

	private static void trustAllHttpsCertificates() throws Exception
	{
		//  Create a trust manager that does not validate certificate chains:
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc =javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	// Just add these two functions in your program 
	public static class  miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()
		{
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs)
		{
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs)
		{
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
						throws java.security.cert.CertificateException
						{
			return;
						}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
						throws java.security.cert.CertificateException
						{
			return;
						}
	}


}
