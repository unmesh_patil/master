package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiSystemHealthCheck extends ApiAuthenticationInit {

	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void healthCheck1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		//params.add("accountNumber", "a08d000000Qz5GeAAJ");
		String body = doHeathcheck(params);
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}



	private String doHeathcheck(MultivaluedMap params)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/systems/healthcheck");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}

}
