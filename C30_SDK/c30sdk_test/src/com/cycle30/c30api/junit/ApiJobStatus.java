package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiJobStatus extends ApiAuthenticationInit {

	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void getJobStatusTest() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		//params.add("franchiseCode", "1");
		String body = getJobStatus(params, "198301:WPDS");
		log.info(body);
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	private String getJobStatus(MultivaluedMap params, String jobId)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/jobstatus/"+jobId);
			byte[] base64Data = body.getBytes();
			byte[] decodedBytes = Base64.decodeBase64(base64Data);
			 

			/*BASE64Decoder decoder = new BASE64Decoder();
			byte[] decodedBytes = decoder.decodeBuffer(body);
*/
			FileOutputStream output = new FileOutputStream(new File("jobStatus.png"));

			output.write(decodedBytes);
			output.flush();
			output.close();
			
			 
	        /* BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedBytes));
	         if (image == null) {
	              log.error("Buffered Image is null");
	          }
	         File f = new File("jobStatus1.jpg");
	 
	         // write the image
	          ImageIO.write(image, "jpg", f);*/

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}

}
