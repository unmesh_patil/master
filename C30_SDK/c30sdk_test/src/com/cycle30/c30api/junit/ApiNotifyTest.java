package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.io.StringWriter;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ApiNotifyTest extends ApiAuthenticationInit {
	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void OrderCreate1() throws Exception {


		String xmlDoc="Order1.xml";
		Document request = C30DOMUtils.getDocument(xmlDoc);
		StringWriter stw = new StringWriter(); 
		Transformer serializer = TransformerFactory.newInstance().newTransformer(); 
		serializer.transform(new DOMSource(request), new StreamResult(stw)); 
		String xmlRequest  = stw.toString();
		System.out.println(xmlRequest);


		String body = HttpUtils.postOrder(request, "/orders");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	@Test
	public void OrderCreate2() throws Exception {


		String xmlDoc="Order2.xml";
		Document request = C30DOMUtils.getDocument(xmlDoc);
		StringWriter stw = new StringWriter(); 
		Transformer serializer = TransformerFactory.newInstance().newTransformer(); 
		serializer.transform(new DOMSource(request), new StreamResult(stw)); 
		String xmlRequest  = stw.toString();
		System.out.println(xmlRequest);


		String body = HttpUtils.postOrder(request, "/orders");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

}
