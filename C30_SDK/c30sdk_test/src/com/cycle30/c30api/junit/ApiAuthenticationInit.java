package com.cycle30.c30api.junit;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;



public class ApiAuthenticationInit {

	public static Logger log = Logger.getLogger(ApiAuthenticationInit.class);

	
	//public static final String SERVER_URL="https://ssp1devkenap1.cycle30.com:52010/C3B1/DEV";
	//public static final String SERVER_URL="https://ssp1cfgkenap1.cycle30.com:52010/C3B1/CFG";
	//public static final String SERVER_URL="https://c3b3tstkenap1.cycle30.com:52010/C3B3/TST";
	//public static final String SERVER_URL="https://c3b1uatkenap1.cycle30.com:52010/C3B1/UAT";

	public static final String SERVER_URL="https://c3b3devkenap1.cycle30.com:52010/C3B3/DEV";
	//public static final String SERVER_URL="https://192.168.72.238/ppsxml/prepaid.xml_rpc.server.php";
	public static String DATE="";
	public static String X_Digest="123412341234";
	
	/*public static final String X_ENV_ID="C3B1DEV";
	public static final String X_ORG_ID="ACSDV1";
	public static final String SecreteKey = "C969036AA1A37DBEF47B80CA1B555E36";
*/
	/*public static final String X_ENV_ID="C3B1CFG";
	public static final String X_ORG_ID="ACSCF1";
	public static final String SecreteKey = "CF6FD893210EBC5E300D28407E38A5F2";
*/
	/*public static final String X_ENV_ID="C3B3TST";
	public static final String X_ORG_ID="SWAWNGCITST";
	public static final String SecreteKey = "5C6FC822A72436C19E97BEC61FA5E123";
*/
	//public static final String X_ENV_ID="C3B3UAT";
	//public static final String X_ORG_ID="ACSUAT";
	//public static final String SecreteKey = "06F8AB788869D4EC15605B17DD27A0A7";
	
	//public static final String X_ENV_ID="C3B1PRD";
	//public static final String X_ORG_ID="ACSPRD";
	//public static final String SecreteKey = "E3AD97090CEF0252452DCF4918A43DB2";
	
/*	public static final String X_ENV_ID="C3B3DEV";
	public static final String X_ORG_ID="SWDEV";
	public static final String SecreteKey = "7A2397C5C3648BFC1625E4B783FBC123";
*/		
	public static final String X_ENV_ID="C3B3DEV";
	public static final String X_ORG_ID="SWAWNGCIDEV";
	public static final String SecreteKey = "7A2397C5C3648BFC1625E4B783FBC123";

	/*public static final String X_ENV_ID="C3B3DEV";
	//public static final String X_ORG_ID="00Di0000000HkOtEAK";
	public static final String X_ORG_ID="PREPAID";
	public static final String SecreteKey = "4529716699903004036";
	
	*/public static final String X_Trans_Id ="Ranjith-Test-"+new Date();

	public static Client client;
	public static XPathFactory xpf = XPathFactory.newInstance();
	public static XPath xp = xpf.newXPath();


	
	public static void initialize()
	{
		
		try{
			client = Client.create();  // do one time and reuse
			//java.lang.System.setProperty("javax.net.ssl.allowUnsafeRenegotiation", "true");
			//java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
			//java.lang.System.setProperty("javax.net.debug", "ssl");
			//java.lang.System.setProperty("javax.net.ssl.trustStore",  "C:\\Ranjith\\Cycle30\\Project\\keystore\\c3b1\\truststore.jks");
			//java.lang.System.setProperty("javax.net.ssl.trustStorePassword", "ranjith");
			//java.lang.System.setProperty("javax.net.ssl.keyStore",  "C:\\Ranjith\\Cycle30\\Project\\keystore\\keystore.jks");
			//java.lang.System.setProperty("javax.net.ssl.keyStorePassword", "ranjith");

			generateXDigestForApi();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/** This method used to generate the XDigest.
	 * 
	 */
	private static void generateXDigestForApi() {
		 try {

	            String clientId = X_ORG_ID;
	            
	            // Get client 'secret' hash value from CLIENT table cache.
	            String secretHash = SecreteKey;
	            
	            // Get Date header value and parse into YYYYMMDD format
	            SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
	            Date d = new Date();
	            DATE = formatter.format(d);
	            formatter = new SimpleDateFormat("yyyyMMdd");
	            String yyyymmdd = formatter.format(d);
	            
	            // Full 'salted' value:
	            String saltValue = clientId + secretHash + yyyymmdd;
	            

	            // get hash value
	            MessageDigest digest = MessageDigest.getInstance("MD5");
	            digest.reset();
	            byte[] nameBytes = saltValue.getBytes();
	            digest.update(nameBytes);
	            byte[] hash = digest.digest();

	            // convert hash bytes to hex string value
	            StringBuffer hexString = new StringBuffer();

	            for (int i = 0; i < hash.length; i++) {
	                String hex = Integer.toHexString(0xFF & hash[i]);
	                if (hex.length() == 1)
	                    hexString.append('0');
	                hexString.append(hex);
	            }

	            String md5Digest = hexString.toString();
	            X_Digest = md5Digest;
	           
	        } catch (Exception e) {
	            log.error(e);
	        }

	}

}
