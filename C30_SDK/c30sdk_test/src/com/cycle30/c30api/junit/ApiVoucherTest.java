package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiVoucherTest extends ApiAuthenticationInit {

	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void voucherStatus1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		String body = getVoucher(params, "84970530242229a");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	@Test
	public void voucherBlock1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		String body = blockVoucher(params, "84970530242229");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	@Test
	public void voucherUnBlock1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		String body = unblockVoucher(params, "84970530242229");
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	
	private String getVoucher(MultivaluedMap params, String voucherPin)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/voucher/"+voucherPin+"/status");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}

	private String blockVoucher(MultivaluedMap params, String voucherPin)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/voucher/block/"+voucherPin);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}
	
	private String unblockVoucher(MultivaluedMap params, String voucherPin)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/voucher/unblock/"+voucherPin);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}
}
