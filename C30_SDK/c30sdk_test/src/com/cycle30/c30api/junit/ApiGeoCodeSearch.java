package com.cycle30.c30api.junit;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import sun.misc.BASE64Decoder;

import com.cycle30.sdk.util.C30DOMUtils;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiGeoCodeSearch extends ApiAuthenticationInit {

	@BeforeClass
	public static void oneTimeSetUp() throws Exception {
		ApiAuthenticationInit.initialize();
	}

	@Test
	public void geoCodeFind1() throws Exception {
		MultivaluedMap<String, String> params = new MultivaluedMapImpl();
		params.add("city", "Anchorage");
		params.add("state", "AK");
		params.add("zip", "99503");
		//params.add("countryCode", "840");//
		//params.add("accountNumber", "172163755");
		//params.add("ignorePunctuation", "Yes");
		//params.add("franchiseCode", "1");
		String body = getGeoCode(params);
		Document bodyDoc = C30DOMUtils.stringToDom(body);
		log.info(body);
		String text = xp.evaluate("//AccountSearchResponse/Accounts/System[@id=\"K2\"]/Account/AccountNumber/text()",bodyDoc.getDocumentElement());
		System.out.println(text);
		if (text.equalsIgnoreCase("172163755"))
			assertTrue(true);
		else 
			assertTrue(false);
	}

	private String getGeoCode(MultivaluedMap params)
	{
		String body= "";
		try {
			body = HttpUtils.getHttpRequest(params, "/addresses/geocode");
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Response Body : "+body);
		return body;
	}

}
