/*
 * C30TableDataSource.java
 *
 * Created on March 29, 2006, 4:47 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.datasource;

import com.cycle30.sdk.swing.table.C30TableModel;
import com.cycle30.sdk.util.*;
import java.util.ArrayList;

/**
 *
 * @author reej04
 */
public class C30TableDataSource implements C30DataSource {
    protected C30TableModel tableModel;
    protected String procName;
    protected ArrayList inputParams;
    protected ArrayList outputParams;
    protected ArrayList paramValues;
    
    public static final int DISPLAY_VALUES = 1;
    public static final int PARAM_NAMES = 2;
    
    /** Creates a new instance of C30TableDataSource */
    public C30TableDataSource() {
        this(null);
    }
    
    public C30TableDataSource(C30TableModel tableModel) {
        if (tableModel == null) {
            this.tableModel = new C30TableModel();
        } else {
            this.tableModel = tableModel;
        }
    }
    
    public int getColumnCount() {
        return tableModel.getColumnCount();
    }
    
    public int getRowCount() {
        return tableModel.getRowCount();
    }
    
    public String getStringAt(int rowIndex, int columnIndex) {
        return tableModel.getValueAt(rowIndex, columnIndex).toString();
    }
    
    public Object getObjectAt(int rowIndex, int columnIndex) {
        return tableModel.getValueAt(rowIndex, columnIndex);
    }
    public C30TableModel getTableModel() {
        return tableModel;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        return tableModel.getValueAt(rowIndex, columnIndex);
    }
    
    public void queryData(C30ExternalCallDef externalCall, ArrayList paramValues, int columnSource) throws Exception {        
        this.tableModel.clearData();
        
        if (externalCall != null) {
            this.procName = externalCall.getCallName();
            this.inputParams = externalCall.getInputParams();
            this.outputParams = externalCall.getOutputParams();
            
            if (columnSource == DISPLAY_VALUES) {
                tableModel.setColumnNames(externalCall.getOutputParamDisplayValues());
            } else if (columnSource == PARAM_NAMES) {
                tableModel.setColumnNames(externalCall.getOutputParamNames());
            }
        }
        
        this.paramValues = paramValues;
        
    }
 
}
