/*
 * This class is responsbile for making connections to WorkPoint and 
 * resuing the same connection again. Also this is responsible for all the
 * Initiations to WorkPoint Jobs.
 */
package com.cycle30.sdk.datasource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import com.cycle30.sdk.toolkit.C30PropertyFileManager;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.workpoint.client.ClientContext;

/*
 * Managing the WP Client Connections 
 */
public class C30WPConnection {
	// The one-and-only instance of the C30WPConnection.
	private static C30WPConnection instance = null;

	//Logs all the information
	private static Logger log = Logger.getLogger(C30WPConnection.class);

	//Config file loading.
	public C30PropertyFileManager configFilesResourceBundle = C30PropertyFileManager.getInstance();

	//DataSource Name
	private String wpDSN;

	//WorkPoint Client context
	private ClientContext wpClientContext;

	//Workpoint Property file name.
	public static final String C30_WP_CONFIG_FILE_NAME = "C30WP_Config_en.properties";
	public static final String C30_WP_CONFIG_FILE_NAME_RESOURCE = "C30WP_Config";
	
	//WorkPoint Error Config files
	public static final String C30_WP_ERROR_FILE_NAME = "C30WP_Core_Error_Text_en.properties";
	public static final String C30_WP_ERROR_FILE_NAME_RESOURCE = "C30WP_Core_Error_Text";
	
	//workpoint-client.propeties files
	public static final String C30_WP_CLIENT_FILE_NAME = "/workpoint-client.properties";
	public static final String C30_WP_CLIENT_FILE_NAME_RESOURCE = "workpoint-client";
	
	/**
	 * Returns the one-and-only instance of the C30WPConnection.
	 *
	 *@return    The one-and-only instance of the C30WPConnection.
	 * @throws C30SDKToolkitException 
	 */
	public static C30WPConnection getInstance() throws C30SDKToolkitException {
		if (instance == null) 
		{
			if( log != null ) 
			{
				log.debug("Creating new instance of C30WPConnection"); 
			}
			instance = new C30WPConnection();
		}

		return instance;
	}
	/*
	 * Constructor for the C30WPConnection
	 */
	private C30WPConnection() throws C30SDKToolkitException
	{
	
		log.debug("Making the Workpoint Client context and populating it.");
		wpDSN = configFilesResourceBundle.getProperty("C30_WORKPOINT_DSN");

		try {

			wpClientContext = ClientContext.createContext();
			wpClientContext.open(wpDSN, null,null);
			log.debug("Context information : "+ wpClientContext.getDatabase());
		}
		catch (Exception e) 
		{
			throw new C30SDKToolkitException(configFilesResourceBundle.getProperty("WP-CONN-004"), e, "WP-CONN-004");
			
		}

	}
	public ClientContext getWpClientContext() {
		return wpClientContext;
	}
	public void setWpClientContext(ClientContext wpClientContext) {
		this.wpClientContext = wpClientContext;
	}
}
