package com.cycle30.sdk.datasource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate.encryptor.HibernatePBEEncryptorRegistry;

import com.cycle30.sdk.toolkit.C30PropertyFileManager;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;

public class C30SDKDataSourceUtils
{
	private final static String decryptionAlgorithm = "PBEWithMD5AndDES";
	private static String decryptionPassword = "";

	public static final String PROPERTIES_FILE_NAME = "c30sdk";
	public static final int MAX_JDBC_CONNECTIONS = 15;
	public static final long MAX_WAIT_FOR_CONNECTIONS = 60000;
	private static final String SERVER_DEFINITION_GET_QUEREY = "SELECT SERVER_ID, DS_DATABASE, HOSTNAME FROM SERVER_DEFINITION";
	private static final String CUST_SERVER_DEFINITION_GET_QUEREY = "SELECT COUNT(*) as count FROM SERVER_DEFINITION WHERE SERVER_TYPE = 3";
	private static final int SERVER_DEFINITION_GET_QUEREY_PARAM_COUNT = 3;
	private static final C30SDKDataSourceUtils self = new C30SDKDataSourceUtils();
	private static Properties KenanServersJdbcProperties = null;
	private static Properties SdkConfigDbJdbcProperties = null;

	private static Vector drivers = new Vector();
	private static Hashtable pools = new Hashtable();

	private static ResourceBundle sdkProperties;

	private static Logger log = Logger.getLogger(C30SDKDataSourceUtils.class);

	//public C30SDKDataSourceUtils(){ }


	private static void loadJDBCProperties() throws IOException, FileNotFoundException, Exception
	{
		//C30SDKConfigurationManager cManager = C30SDKConfigurationManager.getInstance();
		//SdkConfigDbJdbcProperties = cManager.getPropertiesFromClassPath(PROPERTIES_FILE_NAME);
		//C30Properties prop = C30Properties.getInstance();
		SdkConfigDbJdbcProperties = C30PropertyFileManager.getInstance();
		/*
		 * Reload the C30SDK Config database encrypt password into the property file
		 */

		String c30sdkpwd  = SdkConfigDbJdbcProperties.getProperty("c30sdkconfigdb.password");  
		log.info("Decrypting the password for 'c30sdkconfigdb.password' : "+ c30sdkpwd);
		if (c30sdkpwd == null)
		{
			String errorMessage = "No value set for the property 'c30sdkconfigdb.url'";
			//throws java.lang.Exception(errorMessage);
			throw new Exception(errorMessage);
		}
		SdkConfigDbJdbcProperties.setProperty("c30sdkconfigdb.password", decryptPassword(c30sdkpwd));


		/* Initially we load all the catalog database properties from the properties file.
		 Usually that will be in the following format.
			# Catalog DB Connection
			# Kenan Databases Information
			#	server.1.url=jdbc:oracle:thin:@gci1shroradb1.cycle30.com:1521:K2DEVCAT
			#	server.1.username=arbor
			#	server.1.password=ZG5iRBaos8Oi25dKcF1VsW0UssAKYomn
		 */
		KenanServersJdbcProperties = C30PropertyFileManager.getInstance();
		//KenanServersJdbcProperties = cManager.getPropertiesFromClassPath(PROPERTIES_FILE_NAME);
		String url = KenanServersJdbcProperties.getProperty("server.1.url") ; 
		String user = KenanServersJdbcProperties.getProperty("server.1.username");
		String pwd  = KenanServersJdbcProperties.getProperty("server.1.password");

		//Reading the default port number
		String portNumber  = KenanServersJdbcProperties.getProperty("DefaultDatabasePortNumber");
		String hostName  = KenanServersJdbcProperties.getProperty("DefaultDatabaseHostName");
		if (portNumber == null ) portNumber="1521";
		if (hostName == null ) hostName="";
		log.info("Decrypting the password for 'server.1.password' : "+ pwd);
		pwd=decryptPassword(pwd);

		C30JDBCDataSource dataSource = new C30JDBCDataSource("oracle.jdbc.driver.OracleDriver",url,user,pwd);

		// The above properties file only contain KenanCatalog Database information
		// So, we need to get the other database information from the 
		ResultSet rsServerDefinition = dataSource.executeQueryandReturnResultSet(SERVER_DEFINITION_GET_QUEREY);
		while (rsServerDefinition.next())
		{
			int serverid = rsServerDefinition.getInt(1);
			String dbName = rsServerDefinition.getString(2).trim();
			String hostname = rsServerDefinition.getString(3).trim();
			if ( KenanServersJdbcProperties.getProperty("server."+serverid+".url") != null)
			{
				log.info("Using the values from the file : "+ KenanServersJdbcProperties.getProperty("server."+serverid+".url"));
				KenanServersJdbcProperties.setProperty("server."+serverid+".username",user);
				KenanServersJdbcProperties.setProperty("server."+serverid+".password",pwd);
			}
			else
			{
				if (hostname.contains("."))
				{
					KenanServersJdbcProperties.setProperty("server."+serverid+".url","jdbc:oracle:thin:@"+hostname+":"+portNumber.trim()+":"+dbName );			
				}
				else
				{
					KenanServersJdbcProperties.setProperty("server."+serverid+".url","jdbc:oracle:thin:@"+hostname+"."+hostName.trim()+":"+portNumber.trim()+":"+dbName );

				}

				log.info("Generating the values from Server Definition : "+ KenanServersJdbcProperties.getProperty("server."+serverid+".url"));
				KenanServersJdbcProperties.setProperty("server."+serverid+".username",user);
				KenanServersJdbcProperties.setProperty("server."+serverid+".password",pwd);
			}

		}
		ResultSet rsCustServerDefinition = dataSource.executeQueryandReturnResultSet(CUST_SERVER_DEFINITION_GET_QUEREY);
		rsCustServerDefinition.next();
		int totalCustServers = rsCustServerDefinition.getInt(1);
		KenanServersJdbcProperties.setProperty("totalCustomerServers",new Integer(totalCustServers).toString());
		rsServerDefinition.close();
		rsCustServerDefinition.close();

	}

	private static void createSDKPools () throws FileNotFoundException, IOException, Exception
	{
		String url  = null;
		String user = null;
		String pwd  = null;

		/*
		 * Initialize the LWO Configuration Database
		 # lwoconfigdb.url=jdbc:oracle:thin:@gci1shroradb1.cycle30.com:1521:K2DEVCAT
		 # lwoconfigdb.username=C30ARBOR
		 # lwoconfigdb.password=U3dQIWMPHVpWVoRKCCVrJxwIyhA4yeP6
		 */

		if (SdkConfigDbJdbcProperties == null)
			loadJDBCProperties();
		url = SdkConfigDbJdbcProperties.getProperty("c30sdkconfigdb.url") ; 
		user = SdkConfigDbJdbcProperties.getProperty("c30sdkconfigdb.username");
		pwd  = SdkConfigDbJdbcProperties.getProperty("c30sdkconfigdb.password");  
		if (url == null)
		{
			String errorMessage = "No LWO Config server JDBC Url Sepcified. Please use properties file and specify value for property 'lwoconfigdb.url'";
			//throws java.lang.Exception(errorMessage);
			throw new Exception(errorMessage);
		}

		int nonorderMaxConnections =0;
		if (SdkConfigDbJdbcProperties.getProperty("sdk.nonorder.max.connections") !=null)
		{
			nonorderMaxConnections = (new Integer(SdkConfigDbJdbcProperties.getProperty("sdk.nonorder.max.connections"))).intValue();
		}
		else
		{
			nonorderMaxConnections = self.MAX_JDBC_CONNECTIONS;
		}


		int orderMaxConnections =0;
		if (SdkConfigDbJdbcProperties.getProperty("sdk.order.max.connections") !=null)
		{
			orderMaxConnections = (new Integer(SdkConfigDbJdbcProperties.getProperty("sdk.order.max.connections"))).intValue();
		}
		else
		{
			orderMaxConnections = self.MAX_JDBC_CONNECTIONS;
		}

		//Generic SDK Pool
		DBConnectionPool sdkPool =  self.new DBConnectionPool("SDK", url, user, pwd, nonorderMaxConnections);
		pools.put("SDK", sdkPool);
		log.debug("Initialized pool " + ": SDK");

		//Order SDK Pool which is used by Order Manager.
		DBConnectionPool orderPool =  self.new DBConnectionPool("ORDER_SDK", url, user, pwd, orderMaxConnections);
		pools.put("ORDER_SDK", orderPool);
		log.debug("Initialized pool " + ": ORDER_SDK");
	}

	public static C30JDBCDataSource getDataSourceFromSDKPool()
			throws Exception
			{

		C30JDBCDataSource dataSource = null;
		if (dataSource == null)
		{
			DBConnectionPool pool = (DBConnectionPool) pools.get("SDK");
			if (pool == null) {
				createSDKPools();
				pool = (DBConnectionPool) pools.get("SDK");
				//throw new Exception( "JDBC Pool Name: SDK not found");
			}
			long nonorderMaxWait = self.MAX_WAIT_FOR_CONNECTIONS; 
			if (SdkConfigDbJdbcProperties.getProperty("sdk.nonorder.max.wait") !=null)
			{
				nonorderMaxWait = Long.parseLong( SdkConfigDbJdbcProperties.getProperty("sdk.nonorder.max.wait"));
			}		

			dataSource =	    new C30JDBCDataSource(pool.getConnection(nonorderMaxWait));
			log.debug("Total checkedout Connections out of SDK Pool :" +pool.checkedOut);

		}

		return dataSource;
			}


	public static C30JDBCDataSource getDataSourceFromOrderSDKPool()
			throws java.lang.Exception
			{
		C30JDBCDataSource dataSource = null;
		if (dataSource == null)
		{
			DBConnectionPool pool = (DBConnectionPool) pools.get("ORDER_SDK");

			if (pool == null) {
				createSDKPools();
				pool = (DBConnectionPool) pools.get("ORDER_SDK");
				//throw new Exception( "JDBC Pool Name: SDK not found");
			}

			long orderMaxWait = self.MAX_WAIT_FOR_CONNECTIONS; 
			if (SdkConfigDbJdbcProperties.getProperty("sdk.order.max.wait") !=null)
			{
				orderMaxWait = Long.parseLong( SdkConfigDbJdbcProperties.getProperty("sdk.order.max.wait"));
			}	

			dataSource =	    new C30JDBCDataSource(pool.getConnection(orderMaxWait));
			log.debug("Total checkedout Connections out of ORDER_SDK Pool :" +pool.checkedOut);
		}

		return dataSource;
			}


	/** Freeing the connections from the Order SDK Pool.
	 * 
	 * @param conn
	 * @throws java.lang.Exception
	 */
	public static void freeConnectionFromOrderSDKPool(Connection conn)
			throws java.lang.Exception
			{

		DBConnectionPool pool = (DBConnectionPool) pools.get("ORDER_SDK");

		if (pool == null) {
			createSDKPools();
			pool = (DBConnectionPool) pools.get("ORDER_SDK");
			//throw new Exception( "JDBC Pool Name: SDK not found");
		}
		pool.freeConnection(conn);

			}
	
	/** Freeing the connections from the Order SDK Pool.
	 * 
	 * @param conn
	 * @throws java.lang.Exception
	 */
	public static void freeConnectionFromSDKPool(Connection conn)
			throws java.lang.Exception
			{

		DBConnectionPool pool = (DBConnectionPool) pools.get("SDK");

		if (pool == null) {
			createSDKPools();
			pool = (DBConnectionPool) pools.get("SDK");
			//throw new Exception( "JDBC Pool Name: SDK not found");
		}
		pool.freeConnection(conn);

			}
	
	/** Get the JDBC Connection source for the given serverId.
	 * 
	 * @param serverId
	 * @return
	 * @throws java.lang.Exception
	 */
	public static C30JDBCDataSource createJDBCDataSource(Integer serverId)
			throws java.lang.Exception
			{
		C30JDBCDataSource dataSource = null;
		String url  = null;
		String user = null;
		String pwd  = null;


		if (KenanServersJdbcProperties == null)
			loadJDBCProperties();
		url = KenanServersJdbcProperties.getProperty("server."+serverId+".url") ; 
		user = KenanServersJdbcProperties.getProperty("server."+serverId+".username");
		pwd  = KenanServersJdbcProperties.getProperty("server."+serverId+".password");  
		dataSource = new C30JDBCDataSource("oracle.jdbc.OracleDriver",url,user,pwd);

		return dataSource;
			}

	public static Integer getTotalCustomerServers()
			throws IOException, FileNotFoundException,java.lang.Exception
			{
		Integer totalCount;

		if (KenanServersJdbcProperties == null)
			loadJDBCProperties();

		totalCount = new Integer(KenanServersJdbcProperties.getProperty("totalCustomerServers")) ; 


		return totalCount;
			}


	//----------------------------------------------------------------------------
	/* Decrypt db password stored in Advanced Encryption Standard (AES) format.
	 * 
	 */
	public static String decryptPassword(String pwd) throws Exception {


		String decryptedPwd = "";
		// decrypt the password.
		try {

			if (decryptionPassword.trim().length() == 0){
				ResourceBundle rb = getC30SdkToolkitProperties();
				String decryptionUserKey = rb.getString("DBENCRYPTKEY");  
				decryptionPassword = System.getenv(decryptionUserKey);
			}
			StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
			strongEncryptor.setAlgorithm(decryptionAlgorithm);
			strongEncryptor.setPassword(decryptionPassword);
			HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
			registry.registerPBEStringEncryptor("configurationHibernateEncryptor", strongEncryptor);
			decryptedPwd = strongEncryptor.decrypt(pwd);

		} 
		catch (Exception e) {
			throw new Exception ("Error decrypting password: ", e);
		}

		return decryptedPwd;

	}

	/**
	 * 
	 * @return resource bundle
	 */
	public static ResourceBundle getC30SdkToolkitProperties() throws C30SDKToolkitException {

		if (sdkProperties != null) {
			return sdkProperties;
		}

		try {
			Locale locale = Locale.getDefault();
			sdkProperties = ResourceBundle.getBundle(PROPERTIES_FILE_NAME, locale);
		} catch (Exception e) {
			log.error(e);
			throw new C30SDKToolkitException(e.getMessage());
		}

		return sdkProperties;
	}


	/**
	 * This inner class represents a connection pool. It creates new
	 * connections on demand, up to a max number if specified.
	 * It also makes sure a connection is still open before it is
	 * returned to a client.
	 */
	public class DBConnectionPool {
		private int checkedOut;
		private Vector freeConnections = new Vector();
		private int maxConn;
		private String name;
		private String password;
		private String URL;
		private String user;

		/**
		 * Creates new connection pool.
		 *
		 * @param name The pool name
		 * @param URL The JDBC URL for the database
		 * @param user The database user, or null
		 * @param password The database user password, or null
		 * @param maxConn The maximal number of connections, or 0
		 *   for no limit
		 */
		public DBConnectionPool(String name, String URL, String user, String password, 
				int maxConn) {
			this.name = name;
			this.URL = URL;
			this.user = user;
			this.password = password;
			this.maxConn = maxConn;
		}

		/**
		 * Checks in a connection to the pool. Notify other Threads that
		 * may be waiting for a connection.
		 *
		 * @param con The connection to check in
		 */
		public synchronized void freeConnection(Connection con) {
			// Put the connection at the end of the Vector
			freeConnections.addElement(con);
			checkedOut--;
			notifyAll();
		}

		/**
		 * Checks out a connection from the pool. If no free connection
		 * is available, a new connection is created unless the max
		 * number of connections has been reached. If a free connection
		 * has been closed by the database, it's removed from the pool
		 * and this method is called again recursively.
		 */
		public synchronized Connection getConnection() {
			Connection con = null;
			if (freeConnections.size() > 0) {
				// Pick the first Connection in the Vector
				// to get round-robin usage
				con = (Connection) freeConnections.firstElement();
				freeConnections.removeElementAt(0);
				try {
					if (con.isClosed()) {
						log.debug("Removed bad connection from " + name);
						// Try again recursively
						con = getConnection();
					}
				} catch (SQLException e) {
					log.debug("Removed bad connection from " + name);
					// Try again recursively
					con = getConnection();
				}
			} else if (maxConn == 0 || checkedOut < maxConn) {
				con = newConnection();
			}

			if (con != null) {
				checkedOut++;
			}
			return con;
		}

		/**
		 * Checks out a connection from the pool. If no free connection
		 * is available, a new connection is created unless the max
		 * number of connections has been reached. If a free connection
		 * has been closed by the database, it's removed from the pool
		 * and this method is called again recursively.
		 * <P>
		 * If no connection is available and the max number has been 
		 * reached, this method waits the specified time for one to be
		 * checked in.
		 *
		 * @param timeout The timeout value in milliseconds
		 */
		public synchronized Connection getConnection(long timeout) {
			long startTime = new Date().getTime();
			Connection con;
			while ((con = getConnection()) == null) {
				try {
					wait(timeout);
				} catch (InterruptedException e) {
				}
				if ((new Date().getTime() - startTime) >= timeout) {
					// Timeout has expired
					return null;
				}
			}
			return con;
		}

		/**
		 * Closes all available connections.
		 */
		public synchronized void release() {
			Enumeration allConnections = freeConnections.elements();
			while (allConnections.hasMoreElements()) {
				Connection con = (Connection) allConnections.nextElement();
				try {
					con.close();
					log.debug("Closed connection for pool " + name);
				} catch (SQLException e) {
					log.debug("Can't close connection for pool " + name
							+ " Error:" + e.getLocalizedMessage());
				}
			}
			freeConnections.removeAllElements();
		}

		/**
		 * Creates a new connection, using a userid and password
		 * if specified.
		 */
		private Connection newConnection() {
			Connection con = null;
			try {
				if (user == null) {
					con = DriverManager.getConnection(URL);
				} else {
					con = DriverManager.getConnection(URL, user, password);
				}
				log.debug("Created a new connection in pool " + name);
			} catch (SQLException e) {
				log.error("Can't create a new connection for " + URL 
						+ " Error:" + e.getLocalizedMessage());
				return null;
			}
			return con;
		}
	}

}