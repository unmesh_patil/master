/*
 * C30FixedWidthFileDataSource.java
 *
 * Created on July 17, 2006, 1:35 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.datasource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import com.cycle30.sdk.swing.table.C30TableModel;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 *
 * @author reej04
 */
public class C30FixedWidthFileDataSource extends C30FileDataSource {
    
    /**
     * Creates a new instance of C30FixedWidthFileDataSource
     *
     * @param file the file containing the fixed width data
     */
    public C30FixedWidthFileDataSource(File file) {
        super(file);
    }
    
    /**
     * Converts the file to a table of data.
     *
     * @param externalCall call with input parameters matching the columns
     */
    protected C30TableModel parseFileToTable(C30ExternalCallDef externalCall) throws Exception {
        C30TableModel fileData = new C30TableModel();
        BufferedReader in = null;
        String line;

        in = new BufferedReader(new FileReader(file));    
        while ((line = in.readLine()) != null) {
            ArrayList row = new ArrayList();
            
            //-------------------------------------------
            // parse the data from the file based on the
            // start and end position of the parameters
            //--------------------------------------------
            for (int i = 0; i < externalCall.getNumInputParams(); i++) {
                
                C30CustomParamDef param = externalCall.getInputParam(i);
                int startPos = param.getSourceStartPosition();
                int endPos = param.getSourceEndPosition();
                
                //-----------------------------------------------------
                // make sure the start and end positions have been set
                //-----------------------------------------------------
                if (startPos == -1 || endPos == -1) {
                    String message = "Start and end positions have not been set for parameter "
                                     + param.getParamName();
                    throw new Exception(message);
                }
                
                String field = line.substring(param.getSourceStartPosition(), param.getSourceEndPosition()).trim();
                row.add(field);
            }
            
            fileData.addRow(row);
        }
    
        in.close();
    
        return fileData;
    }
    
}
