/*
 * C30DataSource.java
 *
 * Created on March 7, 2006, 1:49 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.datasource;

import com.cycle30.sdk.util.*;
import java.util.ArrayList;

/**
 * This interface defines the method any object that would like to be a source of data
 * for custom applications needs to implement.
 * @author John Reeves
 */
public interface C30DataSource {
    
    /**
     * Retrieves the data from the data source.
     * @param externalCall the external call to use when retrieving data
     * @param paramValues ArrayList of parameter values
     * @param columnSource flag to indicate if column headers should be populated with param names or param display names
     * @throws java.lang.Exception if the data can't be retrieved
     */
    public void queryData(C30ExternalCallDef externalCall, ArrayList paramValues, int columnSource) throws Exception;

}
