/*
 * C30KenanMWDataSource.java
 *
 * Created on January 13, 2006, 2:58 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.cycle30.sdk.datasource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.io.IOException;

import org.apache.log4j.Logger;

import terrapin.tuxedo.ATMI;
import terrapin.tuxedo.Fml32;
import terrapin.tuxedo.TuxError;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.Serializer;
import com.csgsystems.aruba.connection.ServiceException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30HashMapUtils;
import com.cycle30.sdk.util.C30VersioningUtils;
import com.csgsystems.fx.security.util.FxException;

/**
 *
 * @author morj02
 */
public class C30KenanMWDataSource extends C30TableDataSource implements C30DataSource {

    private Connection conn;
    private BSDMSessionContext context;
    private int callDataFmlId = Fml32.Fldid("sw_CallData");
    private int accountServerFmlId = Fml32.Fldid("AccountServer");
    private Fml32 fml = null;
    private static Logger log = Logger.getLogger(C30KenanMWDataSource.class);
    private Boolean borrowConnection = Boolean.FALSE;

    private Double APIVersion = C30VersioningUtils.API_VERSION_1_0;
    
    //private Double PSVersion = C30VersioningUtils.PS_VERSION_1_0;

    /**
     * @deprecated Use C30KenanMWDataSource(Connection, BSDMSessionContext, String) instead.
     * Constructor which is used to create a data source when using the API-TS interface.
     * @param conn the connection to use with the data source.
     * @param context the session context the calls are to be associated with.
     */
    public C30KenanMWDataSource(Connection conn, BSDMSessionContext context) {
        super();
        this.context = context;
        this.conn = conn;
    }

    /**
     * Constructor which is used to create a data source when using the API-TS interface.
     * @param conn the connection to use with the data source.
     * @param context the session context the calls are to be associated with.
     * @param fxVersion identifies the FX version being used by the application.
     */
    public C30KenanMWDataSource(Connection conn, BSDMSessionContext context, Double apiVersion) {
        super();
        this.APIVersion = apiVersion;
        this.context = context;
        this.conn = conn;
    }

    public BSDMSessionContext getContext() {
	return context;
    }

    public void setContext(BSDMSessionContext context) {
    	this.context = context;
    }

    /**
     * This constructor is used when we are borrowing an existing connection
     * that is already owned by a process.  The contructor is typically used
     * inside the PSFramework where a connection already exists.
     * @param fxVersion identifies the FX version being used by the application.
     */
    public C30KenanMWDataSource(Double apiVersion) {
        super();
        this.APIVersion = apiVersion;
        borrowConnection = Boolean.TRUE;
    	fml = new Fml32();
    	fml.Fchg(accountServerFmlId, 0, new Integer(3));
    }

    /**
     * @deprecated Use C30KenanMWDataSource(String) instead.
     * This constructor is used when we are borrowing an existing connection
     * that is already owned by a process.  The contructor is typically used
     * inside the PSFramework where a connection already exists.
     */
    public C30KenanMWDataSource() {
        super();
        borrowConnection = Boolean.TRUE;
    	fml = new Fml32();
    	fml.Fchg(accountServerFmlId, 0, new Integer(3));
    }

    /**
     * This method is typically used for all non-borrewed
     * @see com.cycle30.sdk.datasource.C30TableDataSource#queryData(com.cycle30.sdk.util.C30ExternalCallDef, java.util.ArrayList, int)
     */
   public void queryData(C30ExternalCallDef externalCall, ArrayList paramValues, int columnSource) throws Exception {
    	super.queryData(externalCall, paramValues, columnSource);
        HashMap request = createRequest();
  		log.debug("\n"+C30HashMapUtils.getHashMapPrintOut(request));

        try {
            //--------------------------------------
            // make the call and store the response
            //--------------------------------------
            String callName = "K_S2PxC3";
            Map callResponse = null;
            if (borrowConnection.equals(Boolean.FALSE)) {
            	callResponse = connectionCall(request, callName); //Make regular BSDM connection call
            } else {
            	callResponse = directCall(request, callName); //Make direct connection because we are using a borrowed connection
            }
      		log.debug("\n"+C30HashMapUtils.getHashMapPrintOut(callResponse));

            //--------------------------------
            // populate the C30TableModel data
            //--------------------------------
            Object [] results = (Object[]) callResponse.get("StoredProcedureResultList");

            for (int i = 0; i < results.length; i++) {
                ArrayList row = new ArrayList();
                for (int j = 0; j < outputParams.size(); j++) {
                    String key = ((C30CustomParamDef) outputParams.get(j)).getParamName();
                    row.add(j, ((HashMap) results[i]).get(key));
                }
                tableModel.addRow(row);
            }

            request.clear();

        } catch (Exception e) {
        	e.printStackTrace();
            String message = e.getMessage();
            if (message.indexOf("ORA-20") != -1) {
                int startPos = message.indexOf("ORA-20");
                int endPos = message.indexOf("\n", startPos);
                String msg = message.substring(startPos + 11, endPos);
                throw new Exception(msg);
            } else {
                throw e;
            }
        }
    }

    /**
     * This method allows the user to make hashmap requests given a hashmap request and a call name.
     * @param callname the call name of the function being requested.
     * @param request the request information to be used by the requested function.
     * @return the Map containing the resulting inforamtion returned by the function.
     * @throws IOException if an exception is thrown.
     * @throws FxException if an exception is thrown.
     * @throws ServiceException if an exception is thrown.
     */
    public Map queryData(String callname, Map request) throws IOException, FxException, ServiceException, Exception {
  		log.debug("\n"+C30HashMapUtils.getHashMapPrintOut(request));
        Map callResponse = null;
        //--------------------------------------
        // make the call and store the response
        //--------------------------------------
        if (borrowConnection.equals(Boolean.FALSE)) {
        	callResponse = connectionCall(request, callname);
        } else {
        	callResponse = directCall(request, callname);
        }

  		log.debug("\n"+C30HashMapUtils.getHashMapPrintOut(callResponse));
        return callResponse;
    }

    /**
     * @param request
     * @param callName
     * @return
     * @throws Exception 
     */
    private Map connectionCall(Map request, String callName) throws Exception {
    	Map result = null;

    	// this try/catch statement is to fix a bug with middleware where if the
    	// stored procedure has been recently updated an error is returned the first
    	// time it is used: __ORA-04068: existing state of packages has been discarded__
    	// so all we are doing is rerunning the request.
    	try {
    		result = conn.call(context, callName, request);
    	} catch (ServiceException e) {
    	
            String message = e.getMessage();
            if (message.indexOf("ORA-04068") != -1) {
            	result = conn.call(context, callName, request);
            } else {
            	throw e;
            }
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    		throw e;
    	}
    	return result;
    }

    /**
     * This private method makes a request to the middleware using an existing context.
     * @param request the HashMap style request to be sent to the middleware.
     * @param callName the call name being requested.
     * @return the response from the service in HashMap style.
     * @throws IOException if an exception is thrown.
     */
    private Map directCall(Map request, String callName) throws IOException {
        log.debug("Starting C30KenanMWDataSource.directCall(Call: " + callName + ")");
        long startTime = System.currentTimeMillis();

    	fml.Fchg("OperatorName", 0, "aruba");
        fml.Fchg("ApplicationName", 0, "C30KenanMWDataSource");

        //---------------------------------------------
        // Ensure that the ATMI object has the context
        //---------------------------------------------
        if (APIVersion.compareTo(C30VersioningUtils.API_VERSION_1_0) > 0) {                    // <- This only works for FX 2.0 and above.
        	ATMI.tpsetctxt(1, 0);
        }
        ATMI.tpgetctxt(0);

        //-------------------------------------------
        // Serialize the request for use by the fml.
        //-------------------------------------------
        Serializer serializer = new Serializer();
    	byte[] ba = serializer.serialize(request);
        fml.Fchg(callDataFmlId, 0, ba);

        //-------------------------------------------
        // Make the call
        //-------------------------------------------
        ATMI.tpcall(callName, fml, fml, 0);

        //--------------------------------------------
        // deserialize the response
        //--------------------------------------------
        ba = (byte[]) fml.Fget("sw_CallData", 0);
        Map callResponse = (Map) serializer.deserialize(ba);

        log.debug("C30KenanMWDataSource time to make call = "+(System.currentTimeMillis() - startTime));
    	log.debug("Finished C30KenanMWDataSource.directCall");
    	return callResponse;

    }

    private HashMap createRequest() throws Exception {

        HashMap request = new HashMap();
        String paramType = "";
        String paramName = "";

    	try {

	        //----------------
	        // add the header
	        //----------------
	        HashMap obj = new HashMap();
	        obj.put("StoredProcedureName", procName);
	        request.put("StoredProcedureCall", obj);

	        //--------------------------
	        // add the input parameters
	        //--------------------------
	        HashMap[] paramInputList = new HashMap[inputParams.size()];
	        for (int i = 0; i < inputParams.size(); i++) {
	            paramInputList[i] = new HashMap();
	            paramType = ((C30CustomParamDef) inputParams.get(i)).getParamTypeString();
	            paramName = ((C30CustomParamDef) inputParams.get(i)).getParamName();
	            paramInputList[i].put("Type", paramType);
	            paramInputList[i].put("Name", paramName);
	            if(((C30CustomParamDef) inputParams.get(i)).getParamType() == C30CustomParamDef.STRING) {
	                paramInputList[i].put("Length", new Integer(((C30CustomParamDef) inputParams.get(i)).getParamLength()));
	            }
	        }

	        obj.put("InputParameterList", paramInputList);

	        //---------------------------
	        // add the output parameters
	        //---------------------------
	        HashMap[] paramOutputList = new HashMap[outputParams.size()];
	        for (int i = 0; i < outputParams.size(); i++) {
	            paramOutputList[i] = new HashMap();
	            paramType = ((C30CustomParamDef) outputParams.get(i)).getParamTypeString();
	            paramName = ((C30CustomParamDef) outputParams.get(i)).getParamName();
	            paramOutputList[i].put("Type", paramType);
	            paramOutputList[i].put("Name", paramName);
	            paramOutputList[i].put("Length", new Integer(((C30CustomParamDef) outputParams.get(i)).getParamLength()));
	        }

	        obj.put("OutputParameterList", paramOutputList);

	        //----------------------
	        // add the input values
	        //----------------------
	        HashMap paramInputParameters = new HashMap();
	        for (int i = 0; i < paramValues.size(); i++) {
	            C30CustomParamDef param = (C30CustomParamDef) inputParams.get(i);
                paramInputParameters.put(param.getParamName(), paramValues.get(i));
	        }
	        obj.put ("InputParameters", paramInputParameters);
    	} catch (Exception e) {
    		String message = "An error occurred trying to create the stored procedure request.\n";
    		message = message + "Stored Procedure Name = "+procName+"\n";
    		message = message + "Attribute Name = "+paramName+"\n";
    		message = message + "Attribute Type = "+paramType+"\n";
    		log.error(message);
    		e.printStackTrace();
    		Exception ex = new Exception();
    		ex.initCause(e);
    		throw ex;
    	}

        return request;
    }
    
}