/*
 * C30XMLFileDataSource.java
 *
 * Created on June 1, 2008, 4:21 PM
 */

package com.cycle30.sdk.datasource;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.NodeList;

import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * Implementation an XML file as a source of data used in a custom application.
 * @author Tom Ansley
 */
public class C30XMLFileDataSource implements C30DataSource {

    private static Logger log = Logger.getLogger(C30XMLFileDataSource.class);

    /**
     * Microsoft Excel workbook
     */
    private Document xmlDocument;

    /**
     * Creates a new instance of C30XMLFileDataSource
     * @param file XML file containing the data
     */
    public C30XMLFileDataSource(File file) {

    	try {
    		//build xml document
    		xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
    	} catch (IOException ioe) {
    		ioe.printStackTrace();
    	} catch (SAXException saxex) {
    		saxex.printStackTrace();
    	} catch (ParserConfigurationException pce) {
    		pce.printStackTrace();
    	}

    }

    /**
     * Creates a new instance of C30XMLFileDataSource
     * @param xml XML string containing the data
     */
    public C30XMLFileDataSource(String xml) {

   		//build xml document
   		try {
			xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		}

    }

    public Node getDocumentRoot() {
    	return xmlDocument.getDocumentElement();
    }

    public NodeList getNodes(String name) {
    	return xmlDocument.getElementsByTagName(name);
    }

    public int getNodeCount(String name) {
    	return xmlDocument.getElementsByTagName(name).getLength();
    }

    public Node getNode(String name, int index) {
    	return xmlDocument.getElementsByTagName(name).item(index);
    }

    public NodeList getNodeChildren(String name, int index) {
    	return xmlDocument.getElementsByTagName(name).item(index).getChildNodes();
    }

    public HashMap getNodeAttributes(Node node) {

        Node attribute;
        HashMap attribs = new HashMap();

        //if the node has attributes
        if (node.getAttributes() != null) {

        	//go through the attributes of the node
	        for (int j = 0; j < node.getAttributes().getLength(); j++) {

	        	//get attribute
	        	attribute = (Node) node.getAttributes().item(j);

	        	//put attribute name/value into hashmap
	            attribs.put(attribute.getNodeName(), attribute.getNodeValue());

	        }
        }
        log.debug("Finished getNodeAttributes(Node) with size = "+attribs.size());
        return attribs;
    }

    public String getNodeAttributeValue(String name, int index, String attributeName) {
        Node attribute;
    	String attributeValue = null;

    	NamedNodeMap attributes = xmlDocument.getElementsByTagName(name).item(index).getAttributes();

        //go through the attributes of the node
        for (int j = 0; j < attributes.getLength(); j++) {
            attribute = (Node) attributes.item(j);

            if (attribute.getNodeName().equals(attributeName)) {
                attributeValue = attribute.getNodeValue();
                break;
            }

        }

        log.debug("Attribute Value = "+attributeValue);
    	return attributeValue;
    }

    /**
     * (non-Javadoc)
     * @see com.cycle30.C30DataSource.PSDataSource#queryData(com.cycle30.C30ExternalCallDef.PSExternalCallDef, java.util.ArrayList, int)
     */
    public void queryData(C30ExternalCallDef externalCall, ArrayList paramValues, int columnSource) throws Exception { }
}
