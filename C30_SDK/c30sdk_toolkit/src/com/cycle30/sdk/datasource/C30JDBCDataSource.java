
package com.cycle30.sdk.datasource;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

import com.cycle30.sdk.swing.table.C30TableModel;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

// Referenced classes of package com.cycle30.sdk.datasource:
//            C30TableDataSource, C30DataSource

public class C30JDBCDataSource extends C30TableDataSource
implements C30DataSource
{



	public C30JDBCDataSource(Connection conn)
	{
		this(conn, null);
	}

	public C30JDBCDataSource(Connection conn, C30TableModel tableModel)
	{
		super(tableModel);
		this.conn = conn;
	}

	public C30JDBCDataSource(String driverName, String url, String username, String password)
	throws Exception
	{
		Class jdbcDriver = Class.forName(driverName);
		DriverManager.registerDriver((Driver)jdbcDriver.newInstance());
		conn = DriverManager.getConnection(url, username, password);
	}

	public void queryData(C30ExternalCallDef externalCall, ArrayList paramValues, int columnSource)
	throws Exception
	{

		super.queryData(externalCall, paramValues, columnSource);
		stmt = conn.prepareCall(buildQueryString(true));
		try
		{
			setInputParams();
			numOutputParams = outputParams.size();
			((CallableStatement)stmt).registerOutParameter(inputParams.size() + 1, -10);
			executeStatement();
			ResultSet rs = (ResultSet)((CallableStatement)stmt).getObject(inputParams.size() + 1);
			storeResults(rs);
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			stmt.close();
		}
	}

	public ResultSet queryDataWithRS(C30ExternalCallDef externalCall, ArrayList paramValues, int columnSource)
	throws Exception
	{
		ResultSet rs =null;
		super.queryData(externalCall, paramValues, columnSource);
		stmt = conn.prepareCall(buildQueryString(true));
		try
		{
			setInputParams();
			numOutputParams = outputParams.size();
			((CallableStatement)stmt).registerOutParameter(inputParams.size() + 1, -10);
			executeStatement();
			rs = (ResultSet)((CallableStatement)stmt).getObject(inputParams.size() + 1);
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			//stmt.close();
		}
		return rs;
	}


	public void executeCallableStatement(C30ExternalCallDef externalCall, ArrayList paramValues, int ColumnSource)
	throws Exception
	{
		procName = externalCall.getCallName();
		inputParams = externalCall.getInputParams();
		outputParams = externalCall.getOutputParams();
		this.paramValues = paramValues;
		stmt = conn.prepareCall(buildQueryString(false));
		setInputParams();
		executeStatement();
		numOutputParams = outputParams.size();
		if(numOutputParams > 0)
		{
			ResultSet rs = stmt.getResultSet();
			storeResults(rs);
		}
		stmt.close();
	}

	public ResultSet executeQueryandReturnResultSet(String sql)
	throws Exception
	{
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs  = ps.executeQuery();
		return rs;
	}


	public PreparedStatement getPreparedStatement(String sql)
	throws Exception
	{
		PreparedStatement ps = conn.prepareStatement(sql);
		return ps;
	}

	public void executePreparedStatement(PreparedStatement ps)
	throws Exception
	{
		ResultSet rs  = ps.executeQuery();
		storeResults(rs);
	}

	public void executeQuery(String sql, int numOutputParams)
	throws Exception
	{
		this.numOutputParams = numOutputParams;
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		storeResults(rs);
	}

	public  void closeConnection() throws Exception
	{
		if (this.conn != null)
			this.conn.close();   
	}

	private void setInputParams()
	throws Exception
	{
		for(int i = 0; i < inputParams.size(); i++)
		{
			C30CustomParamDef param = (C30CustomParamDef)inputParams.get(i);
			Object paramValue = paramValues.get(i);
			if(param.getParamType() == 1)
			{
				if(paramValue == null || paramValue.equals(""))
					((CallableStatement)stmt).setNull(i + 1, 2);
				else
					((CallableStatement)stmt).setInt(i + 1, Integer.parseInt(paramValues.get(i).toString()));
				continue;
			}
			if(param.getParamType() == 2 || param.getParamType() == 8)
			{
				if(paramValue == null || paramValue.equals(""))
					((CallableStatement)stmt).setString(i + 1, null);
				else
					((CallableStatement)stmt).setString(i + 1, (String)paramValues.get(i));
				continue;
			}
			if(param.getParamType() == 3)
			{
				if(paramValue == null || paramValue.equals(""))
				{
					((CallableStatement)stmt).setDate(i + 1, null);
					continue;
				}
				if(paramValue instanceof Date)
					((CallableStatement)stmt).setDate(i + 1, new java.sql.Date(((Date)paramValue).getTime()));
				continue;
			}
			//TIMESTAMP to cater the time in the request.
			if(param.getParamType() == 13)
			{
				if(paramValue == null || paramValue.equals(""))
				{
					((CallableStatement)stmt).setDate(i + 1, null);
					continue;
				}
				if(paramValue instanceof Date)
					((CallableStatement)stmt).setTimestamp(i + 1, new java.sql.Timestamp(((Date)paramValue).getTime()));
				continue;
			}
			//ARRAY Type
			if(param.getParamType() == 10)
			{
				if(paramValue == null || paramValue.equals(""))
				{
					((CallableStatement)stmt).setArray(i + 1, null);
					continue;
				}
				// Change the null value to what gets from the paramValue
				if(paramValue instanceof Array)
					((CallableStatement)stmt).setArray(i + 1,(Array)paramValue);
				continue;
			}
			//BLOB Type
			if(param.getParamType() == 11)
			{
				if(paramValue == null || paramValue.equals(""))
				{
					((CallableStatement)stmt).setBlob(i+1,(InputStream) paramValue);
					continue;
				}
				// Change the null value to what gets from the paramValue
				if(paramValue instanceof InputStream)
					((CallableStatement)stmt).setBlob(i + 1,(InputStream)paramValue);
				continue;
			}
			if(param.getParamType() != 4 && param.getParamType() != 7)
				continue;
			if(paramValue == null || paramValue.equals(""))
				((CallableStatement)stmt).setBigDecimal(i + 1, null);
			if(paramValue instanceof BigDecimal)
				((CallableStatement)stmt).setBigDecimal(i + 1, (BigDecimal)paramValue);
		}

	}

	private boolean executeStatement()
	throws SQLException
	{
		try{
			return ((CallableStatement)stmt).execute();
		}
		catch (SQLException e){

			String message = e.getMessage();
			if(message.indexOf("ORA-20") != -1)
			{
				StringTokenizer st = new StringTokenizer(message, "\n");
				throw new SQLException(st.nextToken().substring(11));
			} else
			{
				throw e;
			}
		}
	}

	private void storeResults(ResultSet rs)
	throws SQLException
	{

		try
		{
			tableModel.clearData();
			callResults = new HashMap();

			ResultSetMetaData rmd = rs.getMetaData();
			for(int i = 0; rs.next(); i++)
			{
				ArrayList row = new ArrayList();
				Hashtable rowResults = new Hashtable();
				for(int j = 0; j < numOutputParams; j++)
				{
					Object value = rs.getObject(j + 1);
					String colName = rmd.getColumnName(j + 1);
					row.add(j, value);
					if (value != null)
						rowResults.put(colName,value);

				}
				callResults.put(i,rowResults);
				tableModel.addRow(row);
			}

		}
		catch(SQLException e)
		{
			throw e;
		}
		finally
		{
			rs.close();
		}

	}

	private String buildQueryString(boolean addOutputParam)
	{
		String query = "{ call " + procName + "(";
		int numParams = inputParams.size();
		if(addOutputParam)
			numParams++;
		for(int i = 0; i < numParams; i++)
		{
			query = query + "?";
			if(i != numParams - 1)
				query = query + ",";
		}

		query = query + ") }";
		return query;
	}


	public Map getResultMap(){
		return callResults;
	}
	// jcw added: Lwo Enhancement
	private Map callResults;

	// end jcw added: Lwo Enhancement

	public Connection getConnection()
	{
		return conn;
	}
	private Connection conn;
	private Statement stmt;
	private int numOutputParams;
}