/*
 * C30DelimitedFileDataSource.java
 *
 * Created on July 17, 2006, 1:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.datasource;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.cycle30.sdk.swing.table.C30TableModel;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30StringUtils;

/**
 * Implementation of a delimited file as a source of data used in a
 * custom application.
 * @author John Reeves
 */
public class C30DelimitedFileDataSource extends C30FileDataSource {
    
    /**
     * The default delimiter if no delimiter is specified.
     */
    public static final String DEFAULT_DELIMITER = ",";
    
    /**
     * maps the column name to the zero-based column index
     */
    private HashMap columnNameToIndex = new HashMap();
    
    /**
     * zero-based index of row containing the column names
     */
    private int columnNameRowIndex;
    
    /**
     * The delimiter to separate the fields of data in the file
     */
    private String delimiter;
    private File outputFile;
    private BufferedWriter fileWriter;
    /**
     * Creates a new instance of C30DelimitedFileDataSource
     * @param file the file containing the delimited data
     */
    public C30DelimitedFileDataSource(File file) {
        this(file, DEFAULT_DELIMITER);
    }
    
    /**
     * Creates a new instance of C30DelimitedFileDataSource
     * @param file the file containing the delimited data
     * @param delimiter the delimiter used to seperate the data
     */
    public C30DelimitedFileDataSource(File file, String delimiter) {
        super(file);
        this.delimiter = delimiter;
    }
    
    /**
     * Returns the delimiter used te seperate the data.
     * @return the delimiter
     */
    public String getDelimiter() {
        return delimiter;
    }
    
    /**
     * Gets the index of the row where the column names are defined.
     * @return zero-based index of the row where the column names are defined.
     */
    public int getRowIndexOfColumnNames() {
        return columnNameRowIndex;
    }
    
    /**
     * Sets the row index of the row in the Excel spreadsheet that
     * has the column names.
     * @param rowIndex the zero-based row index that has the column headers
     */
    public void setRowIndexOfColumnNames(int rowIndex) {        
        columnNameToIndex.clear();
        columnNameRowIndex = rowIndex;
    }
    
    /**
     * Converts the file to a table of data.
     * @param externalCall call with input parameters matching the columns
     * @throws java.lang.Exception if the data can't be retrieved from the file
     * @return C30TableModel of data from the file
     */    
    protected C30TableModel parseFileToTable(C30ExternalCallDef externalCall) throws Exception {
    	C30TableModel fileData = new C30TableModel();
        BufferedReader in = null;
        String line;

        try {
            in = new BufferedReader(new FileReader(file));    
            
            while ((line = in.readLine()) != null) {
                ArrayList tokens = new ArrayList();
                
                //----------------------------------------------------
                // parse the line in the file into individual columns
                //----------------------------------------------------
                String correctedLine = C30StringUtils.correctDelimitedLine(line, delimiter);
                StringTokenizer st = new StringTokenizer(correctedLine, delimiter);
                while (st.hasMoreTokens()) {
                    String token = st.nextToken().trim();
                    if (token.equals("NULL")) {
                        tokens.add("");
                    } else {
                        tokens.add(token);
                    }
                }
                
                fileData.addRow(tokens);
            }
            
        } catch (Exception e) {
        	e.printStackTrace();
            throw e;
        } finally {        
            in.close();
        }
        
        if (columnNameRowIndex >= 0 && columnNameToIndex.size() == 0) {
        	ArrayList columnNames = new ArrayList();
        	for (int i = 0; i < fileData.getColumnCount(); i++) {
        		columnNames.add((String) fileData.getValueAt(columnNameRowIndex, i)); 
        	}
            fileData.setColumnNames(columnNames);
        }
        
        return fileData;
    }
    
    /**
     * Method to open up the output data file.
     * @param filePrefix The file prefix that will be concatenated on to the original data file
     */
    public void openOutputFile(String filePrefix) {
        String fileName = file.getName();
        String filePath = file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(fileName));
    	outputFile = new File(filePath + filePrefix + "-" + fileName);
    }
    
    public void postToOutputFile(String text) throws Exception {
	    
    	if (fileWriter == null) {
    		fileWriter = new BufferedWriter(new FileWriter(outputFile));
    	}
    	
	    fileWriter.write(text);
    }
 
    /**
     * Method to close the output file.
     * @throws Exception if there was an input/output error while closing the file.
     */
    public void closeOutputFile() throws Exception {
    	fileWriter.close(); 
    }
}
