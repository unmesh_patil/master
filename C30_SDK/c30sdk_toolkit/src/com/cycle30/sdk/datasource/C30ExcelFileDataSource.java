/*
 * C30ExcelFileDataSource.java
 *
 * Created on March 31, 2006, 4:21 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.datasource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import jxl.write.WritableWorkbook;
import jxl.write.WritableSheet;
import jxl.write.WritableCell;
import jxl.write.Label;


import com.cycle30.sdk.swing.table.C30TableModel;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * Implementation a Microsoft Excel file as a source of data used in a
 * custom application.
 * @author John Reeves
 */
public class C30ExcelFileDataSource extends C30FileDataSource {
    /**
     * zero-based index of worksheet in Microsoft Excel file
     */
    private int worksheetIndex;
    
    /**
     * zero-based index of row containing the column names
     */
    private int columnNameRowIndex;
    
    /**
     * maps the column name to the zero-based column index
     */
    private HashMap columnNameToIndex;
    
    /**
     * Microsoft Excel workbook
     */
    private Workbook workbook;
    private WritableWorkbook outputWorkbook;
    
    /**
     * worksheet in Microsoft Excel file
     */
    private Sheet sheet;
    private WritableSheet outputSheet;
    
    /**
     * Creates a new instance of C30ExcelFileDataSource
     * @param file Microsoft Excel file containing the data
     */
    public C30ExcelFileDataSource(File file) {
        this(file, 0);
    }

    /**
     * Creates a new instance of C30ExcelFileDataSource
     * @param file Microsoft Excel file containing the data
     * @param worksheetIndex the zero-based index of worksheet in Excel file
     */
    public C30ExcelFileDataSource(File file, int worksheetIndex) {
        super(file);
        this.worksheetIndex = worksheetIndex;
        columnNameRowIndex = -1;
        columnNameToIndex = new HashMap();
    }
    
    /**
     * Gets the index of the row where the column names are defined.
     * @return zero-based index of the row where the column names are defined.
     */
    public int getRowIndexOfColumnNames() {
        return columnNameRowIndex;
    }
    
    /**
     * Gets the index of the Excel worksheet.
     * @return zero-based index of Excel worksheet.
     */
    public int getWorksheetIndex() {
        return worksheetIndex;
    }
    
    /**
     * Sets the row index of the row in the Excel spreadsheet that
     * has the column names.
     * @param rowIndex the zero-based row index that has the column headers
     */
    public void setRowIndexOfColumnNames(int rowIndex) {        
        columnNameToIndex.clear();
        columnNameRowIndex = rowIndex;
    }
    
    /**
     * Sets the index of the Excel worksheet.
     * @param worksheetIndex zero-based index of worksheet in Excel file
     */
    public void setWorksheetIndex(int worksheetIndex) {
        this.worksheetIndex = worksheetIndex;
    }
    
    /**
     * Converts the file to a table of data.
     * @param externalCall call with input parameters matching the columns
     * @return C30TableModel of data from the Microsoft Excel file
     * @throws java.lang.Exception if the data can't be retrieved from the Excel file
     */
    protected C30TableModel parseFileToTable(C30ExternalCallDef externalCall) throws Exception {        
        C30TableModel fileData = new C30TableModel();
        
        try {
            loadWorksheet();
            
            if (columnNameRowIndex >= 0 && columnNameToIndex.size() == 0) {
                ArrayList columnNames = readColumnNames();
                fileData.setColumnNames(columnNames);
            }

            //--------------------------------------------
            // loop through all the rows in the worksheet
            //--------------------------------------------
            for (int i = 0; i < sheet.getRows(); i++) {
                if (i != this.columnNameRowIndex) {
                    ArrayList row = new ArrayList();
                    for (int j = 0; j < sheet.getColumns(); j++) {
                        row.add(getCellValue(i, j));
                    }
                    fileData.addRow(row);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        
        return fileData;
    }
    
    /**
     * Loads the excel file and validates the worksheet index
     * @throws java.lang.Exception if the Excel worksheet can't be loaded
     */
    private void loadWorksheet() throws Exception {
        try {
            //-----------------------------------------------------
            // open the workbook and load the configured worksheet
            //-----------------------------------------------------
            workbook = Workbook.getWorkbook(file);
                                
            if (workbook.getNumberOfSheets() < worksheetIndex + 1) {
                String message = "Worksheet #" + String.valueOf(worksheetIndex + 1) +
                                 " is not found in the Excel file \"" + file.getName() + "\"";
                throw new Exception(message);
            } else {
                sheet = workbook.getSheet(worksheetIndex);                
            }
            
        } catch (BiffException e) {
            if (e.getMessage().equals("Unable to recognize OLE stream")) {
                String message = "File \"" + file.getName() +
                                 "\" is not a valid Excel file";
                throw new Exception(message);
            }
        }       
    }
    
    /**
     * Reads the column names from the specified row in the spreadsheet.
     * @return An ArrayList containing the column names.
     * @throws java.lang.Exception if all columns in the row are not populated with a name
     */
    private ArrayList readColumnNames() throws Exception {
        ArrayList columnNames = new ArrayList();
        
        for (int i = 0; i < sheet.getColumns(); i++) {
            String columnName = (String) getCellValue(columnNameRowIndex, i);
            if (columnName != null && !columnName.equals("")) {
                columnNameToIndex.put(columnName, new Integer(i));
                columnNames.add(columnName);
            } else {
                String message = "All columns in Column Name row must be populated";
                throw new Exception(message);
            }         
        }
        
        return columnNames;
    }
    
    /**
     * Gets the value of a cell.
     * @param rowIndex the zero-based row index of the cell to read.
     * @param columnIndex the zero-based column index of the cell to read.
     * @return the string value in the cell
     */
    private Object getCellValue(int rowIndex, int columnIndex) {
        Cell cell;
        Object cellValue;
        
        cell = sheet.getCell(columnIndex, rowIndex);
        
        if (cell instanceof NumberCell) {
            cellValue = String.valueOf(((NumberCell) cell).getValue()).trim();
            if ( ((String) cellValue).endsWith(".0") || ((String) cellValue).indexOf("E") > 0) {
                cellValue = cell.getContents().trim();
            }
        } if (cell instanceof DateCell) {
        	Calendar cal = new GregorianCalendar();
        	cal.setTime(((DateCell) cell).getDate());
        	//cal.setTimeZone(TimeZone.)
        	cal.add(Calendar.MINUTE, ((DateCell) cell).getDate().getTimezoneOffset());
        	cellValue = cal.getTime();
        } else {
            cellValue = cell.getContents().trim();
        }
        
        return cellValue;
    }      
    
    /**
     * Method to open up the output data file.
     * @param filePrefix The file prefix that will be concatenated on to the original data file
     * @throws Exception If an OLE stream was unrecognizable.
     */
    public void openOutputFile(String filePrefix) throws Exception {
            
        try {
            //-----------------------------------------------------
            // open the workbook and load the configured worksheet
            //-----------------------------------------------------
            workbook = Workbook.getWorkbook(file);
            
            if (workbook.getNumberOfSheets() < worksheetIndex + 1) {
                String message = "Worksheet #" + String.valueOf(worksheetIndex + 1) +
                                 " is not found in the Excel file \"" + file.getName() + "\"";
                throw new Exception(message);
            } 
                
            String fileName = file.getName();
            String filePath = file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(fileName));
            
            outputWorkbook = Workbook.createWorkbook(new File(filePath + filePrefix + "-" + fileName), workbook);
            outputSheet = outputWorkbook.getSheet(worksheetIndex);
            workbook.close();
            
         } catch (BiffException e) {
            if (e.getMessage().equals("Unable to recognize OLE stream")) {
                String message = "File \"" + file.getName() +
                                 "\" is not a valid Excel file";
                throw new Exception(message);
            }
        }   
    }
    
    public void postToOutputFile(String text, int column, int row) throws Exception {
        WritableCell cell = outputSheet.getWritableCell(column,row);
        
        if (cell.getType().equals(CellType.LABEL)) {
            Label label = (Label) cell;
            label.setString(text);            
        } else if (cell.getType().equals(CellType.EMPTY)) {
            Label label = new Label(column, row, text);
            outputSheet.addCell(label);
        }
    }
 
    /**
     * Method to close the output file.
     * @throws IOException if there was an input/output error while closing the file.
     */
    public void closeOutputFile() throws Exception {
        outputWorkbook.write();
        outputWorkbook.close(); 
    }
}
