/*
 * C30FileDataSource.java
 *
 * Created on March 31, 2006, 4:19 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.datasource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.cycle30.sdk.swing.table.C30TableModel;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * This abstract class defines a file as a source of data used in a custom application.
 * All classes that inherit from this class must implement the parseFileToTable method
 * @author reej04
 */
public abstract class C30FileDataSource extends C30TableDataSource implements C30DataSource {
    
    /**
     * The file containing the data.
     */
    protected File file;    
    
    /**
     * The number of header rows to ignore when loading the data from the file.
     */
    protected int headerRowOffset;
    
    /**
     * The number of trailer rows to ignore when loading the data from the file.
     */
    protected int trailerRowOffset;
    
    /**
     * The zero-based index of a column that represents a comment.
     */
    protected int commentColumn;
    
    /**
     * The string value
     */
    protected String commentString;
    
    /**
     * The number of rows commented out.
     */
    protected int numComments;
    
    /**
     * Creates a new instance of C30FileDataSource
     * @param file the file containing the data
     */
    public C30FileDataSource(File file) {
        super();
        this.file = file;
        headerRowOffset = 0;
        trailerRowOffset = 0;
        commentColumn = -1;
    }
    
    /**
     * Gets the index of the comment column.
     * @return zero-based index of the comment column
     */
    public int getCommentColumn() {
        return commentColumn;
    }
    
    /**
     * Gets the string that indicates
     * @return The String
     */
    public String getCommentString() {
        return commentString;
    }
    
    /**
     * Gets the number of rows being ignored at the beginning of the file.
     * @return the number of header rows being ignored
     */
    public int getHeaderRowOffset() {
        return headerRowOffset;
    }
    
    /**
     * Gets the number of rows being ignored at the end of the file.
     * @return the number of trailer rows being ignored
     */
    public int getTrailerRowOffset() {
        return trailerRowOffset;
    }
    
    /**
     * Sets the index of the comment column.
     * @param commentColumn zero-based index of the comment column
     */
    public void setCommentColumn(int commentColumn) {
        this.commentColumn = commentColumn;
    }
    
    /**
     * Sets the string to indicate a row is commented out.
     * Used in conjunction with the commentIndex.
     * @param commentString String to indicate a row is commented out
     */
    public void setCommentString(String commentString) {
        this.commentString = commentString;
    }
    
    /**
     * Sets the number of rows to ignore at the beginning of the file.
     * @param headerRowOffset the number of header rows to ignore
     */
    public void setHeaderRowOffset(int headerRowOffset) {
        this.headerRowOffset = headerRowOffset;
    }
    
    /**
     * Sets the number of rows to ignore at the end of the file.
     * @param trailerRowOffset the number of trailer rows to ignore
     */
    public void setTrailerRowOffset(int trailerRowOffset) {
        this.trailerRowOffset = trailerRowOffset;
    }
    
    /**
     * This method is a convenience method to be able to get a row of unformatted data from the data source
     * @param rowIndex the row being returned.  Starting from zero.
     * @return the line of data from the file as a string.
     */
    public String getRawDataRow(int rowIndex) {
    	String data = "";
        BufferedReader in = null;
        int fileIndex = 0;
        try {
        	in = new BufferedReader(new FileReader(file));    
        	while (fileIndex <= rowIndex) {
        		data = in.readLine();
        		fileIndex++;
        	}
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        if (data == null) {
        	data = "";
        }
        
        return data;
    }
    
    /**
     * 
     * @param externalCall 
     * @throws java.lang.Exception 
     * @return void
     */
    protected abstract C30TableModel parseFileToTable(C30ExternalCallDef externalCall) throws Exception;
    
    public void queryData() throws Exception {
        this.queryData(null, null, -1);
    }
    
    public void queryData(C30ExternalCallDef externalCall, ArrayList paramValues, int columnSource) throws Exception {
        super.queryData(externalCall, paramValues, columnSource);
        
        //--------------------------------------
        // load the entire contents of the file
        //--------------------------------------
        C30TableModel fileData = parseFileToTable(externalCall);
        if (this.tableModel.getColumnCount() == 0) {
            this.tableModel.setColumnNames(fileData.getColumnNames());
        }        
        
        //-------------------------
        // discard the header rows
        //-------------------------
        for (int i = 0; i < headerRowOffset; i++) {
            fileData.removeRow(0);
        }
        
        //--------------------------
        // discard the trailer rows
        //--------------------------
        for (int i = 0; i < trailerRowOffset; i++) {
            fileData.removeRow(tableModel.getRowCount() - 1);
        }
        
        //---------------------------------------------------------------
        // clean up data by removing rows commented out and filtered out
        //---------------------------------------------------------------
        for (int i = 0; i < fileData.getRowCount(); i++) {
            
            //----------------------------------------------
            // first discard the row if it is commented out
            //-----------------------------------------------
            if (commentColumn >= 0 && commentColumn < fileData.getColumnCount()) {
                    String value = tableModel.getValueAt(i, commentColumn).toString();
                    if (value != null && value.equals(commentString)) {
                        continue;
                    }                
            }
            
            //--------------------------------------------------------------
            // next check to see if row is filtered out by the input params
            //--------------------------------------------------------------
            boolean filteredOut = false;
            if (externalCall != null) {
                for (int j = 0; j < paramValues.size(); j++) {
                    if (paramValues.get(j) != null) {

                        int columnIndex = externalCall.getInputParam(j).getSourcePosition();
                        if (columnIndex == -1) columnIndex = j;

                        if (columnIndex >= 0 && columnIndex < fileData.getColumnCount()) {
                            String columnValue = fileData.getValueAt(i, columnIndex).toString();
                            if (!paramValues.get(j).toString().equals(columnValue)) {
                                filteredOut = true;
                                continue;
                            }
                        } else {
                            String message = "Invalid source position " + String.valueOf(columnIndex) +
                                             " configured for param \"" + externalCall.getInputParam(i).getParamName() +
                                             "\"\nusing import record \"" + fileData.getRowAt(i).toString() + "\".";
                            throw new Exception(message);                        
                        }
                    }
                }
            }
            
            //--------------------------------------------------------
            // row is not filtered out, so add the data to the result
            // set based on the output parameters that are defined
            //--------------------------------------------------------
            if (!filteredOut) {
                ArrayList row = new ArrayList();
                if (externalCall != null) {
                    for (int j = 0; j < externalCall.getNumOutputParams(); j++) {

                        //----------------------------------------
                        // make sure the source position is valid
                        //----------------------------------------
                        int columnIndex = externalCall.getOutputParam(j).getSourcePosition();
                        if (columnIndex == -1) columnIndex = j;

                        if (columnIndex >= 0 && columnIndex < fileData.getColumnCount()) {
                            String columnValue = fileData.getValueAt(i, columnIndex).toString();
                            switch(externalCall.getOutputParam(j).getParamType()) {
                                case C30CustomParamDef.BIG_DECIMAL:
                                    row.add(new BigDecimal(columnValue));
                                    break;
                                case C30CustomParamDef.INT:
                                    row.add(new Integer(new BigDecimal(columnValue).intValue()));
                                    break;
                                case C30CustomParamDef.STRING:
                                    row.add(columnValue);
                                    break;
                            }
                        } else {
                            String message = "Invalid source position " + String.valueOf(columnIndex) +
                                             " configured for param \"" + externalCall.getInputParam(i).getParamName() +
                                             "\"\nusing import record \"" + fileData.getRowAt(i).toString() + "\".";
                            throw new Exception(message);
                        }  
                    }
                    
                    tableModel.addRow(row);
                    
                } else {
                    tableModel.addRow(fileData.getRowAt(i));
                }
            }               
        }
    }
    
}
