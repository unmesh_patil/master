/**
 * Class containing a number of utility methods used
 * by the DOM examples.
 */
package com.cycle30.sdk.util;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;


public class C30DOMUtils {
	/*public static void main(String[] args) 
	{
		C30DOMUtils du = new C30DOMUtils();
		Document d ;
		d = du.getDocument("asdf");

	}
	 */
	/**
	 * Utility method for getting a DOM Document from a file path.
	 * 
	 * @param filePath Path for the XML file.
	 * @return DOM Document based on that file.
	 */
	public static Document getDocument(String filePath) {
		try {
			// Create DOM document builder
			DocumentBuilderFactory domFactory =
				DocumentBuilderFactory.newInstance();
			DocumentBuilder domBuilder = domFactory.newDocumentBuilder();

			// Convert XML file into a DOM Document
			Document domDoc = domBuilder.parse(new FileInputStream(filePath));

			// Return the DOM Document
			return domDoc;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void writeXmlFile(Document doc, String filename) {
		try {
			// Prepare the DOM document for writing
			Source source = new DOMSource(doc);

			// Prepare the output file
			File file = new File(filename);
			Result result = new StreamResult(file);

			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
		} catch (TransformerException e) {
		}
	}



	/**
	 * Utility method for printing DOMs.
	 * 
	 * @param doc DOM Document to be printed.
	 */
	public static void printDOM(Document doc) {
		try {
			OutputFormat format = new OutputFormat();
			format.setIndent(2);
			XMLSerializer xs = new XMLSerializer(format);
			xs.setOutputByteStream(System.out);
			xs.serialize(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Utility method for replacing a value at a particular
	 * location in a DOM.
	 * 
	 * @param DOMin DOM in which replacement should occur.
	 * @param elementPath Path to target element, delimited by '/'.
	 * @param replaceWith String to be inserted into target element.
	 * @return 0 (if successful) or -1 (if failure).
	 */
	public static int replaceElement(
			Document DOMin,
			String elementPath,
			String replaceWith) {

		// Set success flag to fail by default
		int success = -1;

		/* 
		 * Parse elementPath String into a Vector
		 */

		// Variables used by parsing loop
		int currentPos = 0;
		int newPos = 0;
		String elementName;
		Vector path = new Vector();

		// Get location of first "/"
		newPos = elementPath.indexOf("/", currentPos);

		// Loop until last "/" has been found
		while (newPos > -1) {
			// Get current element name and add it to path Vector
			elementName = elementPath.substring(currentPos, newPos);
			path.addElement(elementName);

			// Get location of next "/"
			currentPos = newPos + 1;
			newPos = elementPath.indexOf("/", currentPos);
		}

		// Get last element name & add it to path Vector
		elementName = elementPath.substring(currentPos);
		path.addElement(elementName);

		/*
		 * Use path Vector to navigate through DOM & do replacement
		 */
		try {
			// Declare variables used by navigation loop
			//int elementPos = 0;
			int lastElement = (path.size() - 1);
			String pathElementName = "";

			// Set thisNode to root element of document
			Element thisElement = DOMin.getDocumentElement();

			// If thisNode's name matches the root element of elementPath
			if (thisElement.getTagName().equals(path.get(0))) {

				// Loop through path Vector, starting at the second element
				for (int i = 1; i <= lastElement; i++) {
					// Get name of this element from elementPath
					pathElementName = (String) path.get(i);

					// Get list of thisElement's child elements named pathElementName
					NodeList matchingChildren =
						thisElement.getElementsByTagName(pathElementName);

					// If pathElementName exists as a child of thisElement
					if (matchingChildren.getLength() > 0) {
						// Store the first matching child element as thisElement
						thisElement = (Element) matchingChildren.item(0);
					}
				}

				/* 
				 * At this point, thisElement should be the element that needs
				 * to have its contents replaced by the replaceWith value
				 */

				// Convert thisElement to a Node
				Node thisNode = (Node) thisElement;

				// If the node has a child text node, replace it;
				// if not, the method fails
				if (thisNode.hasChildNodes()) {
					Node thisNodeContents = thisNode.getFirstChild();
					thisNodeContents.setNodeValue(replaceWith);
					success = 0;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}


	/**
	 * Creates the Document from the given XML String.
	 * @param xml
	 * @return
	 * @throws C30SDKToolkitException
	 */
	public static synchronized Document stringToDom(String xml) throws C30SDKToolkitException {

		Document domDocument = null;


		if (xml == null) {
			return null;
		}

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder        = factory.newDocumentBuilder();
			domDocument                    = builder.parse(new InputSource(new StringReader(xml)));

		} catch (Exception e) {
			log.error(e);
			throw new C30SDKToolkitException( "Unable to convert xml string " + xml + " to a DOM document", e, "XML_ERROR");
		}

		return domDocument;
	}



	/**
	 * 
	 * @param xml - Input XML
	 * @param xsd - SchmeaToValidateAgainst
	 * @return
	 * @throws C30SDKToolkitException
	 */
	public  synchronized static Document stringToDomAndValidate(String xml, String[] xsdList) throws C30SDKToolkitException {

		Document domDocument = null;


		if (xml == null) {
			return null;
		}

		try {

			log.debug("Input XML : " +xml);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			factory.setValidating(false);
			factory.setNamespaceAware(true);

			javax.xml.validation.SchemaFactory schemaFactory = 
				SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");


			String  schemaString= convertStreamToString((BufferedInputStream)(C30DOMUtils.class.getResourceAsStream(xsdList[0])));
			log.info("InputSchema : " +schemaString);
				
			/*factory.setSchema(schemaFactory.newSchema(
					new Source[]{(Source)(new StringReader(schemaString))} ));

			factory.setSchema(schemaFactory.newSchema(
					new Source[] {new StreamSource(C30DOMUtils.class.getResourceAsStream(xsd))}));
			 */
			StreamSource[] streamSrc = new StreamSource[xsdList.length];
			for (int i=0;i<xsdList.length;i++)
			{
				streamSrc[i]= new StreamSource(C30DOMUtils.class.getResourceAsStream(xsdList[i]));
			}
			factory.setSchema(schemaFactory.newSchema(streamSrc));
			//factory.setXIncludeAware(true);
			DocumentBuilder builder        = factory.newDocumentBuilder();

			ErrorHandler handler = new ErrorHandler() {
				public void warning(SAXParseException e) throws SAXException {
					log.warn("[warning] "+e.getMessage());
				}
				public void error(SAXParseException e) throws SAXException {
					log.warn("[error] "+e.getMessage());
					throw e;
				}
				public void fatalError(SAXParseException e) throws SAXException {
					log.warn("[fatal error] "+e.getMessage());
					throw e;
				}
			};

			builder.setErrorHandler(handler);
			domDocument = builder.parse(new InputSource(new StringReader(xml)));

		} catch (Exception e) {
			log.error(e);
			throw new C30SDKToolkitException( "Unable to convert xml string  xml to a DOM document", e,e.getMessage(), "XML_ERROR");
		}

		return domDocument;
	}

	/**
	 * This converts the Input XML into 
	 * @param xml - Input XML
	 * @param xsd - SchmeaToValidateAgainst
	 * @return
	 * @throws C30SDKToolkitException
	 */
	public  synchronized static Document stringToDomAndValidate(String xml, String xsd) throws C30SDKToolkitException {

		Document domDocument = null;


		if (xml == null) {
			return null;
		}

		try {

			//log.debug("Input XML : " +xml);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			factory.setValidating(false);
			factory.setNamespaceAware(true);

			javax.xml.validation.SchemaFactory  schemaFactory = 
				javax.xml.validation.SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");


		//	String  schemaString= convertStreamToString((BufferedInputStream)(C30DOMUtils.class.getResourceAsStream(xsd)));
			//log.info("InputSchema : " +schemaString);
				
			/*factory.setSchema(schemaFactory.newSchema(
					new Source[]{(Source)(new StringReader(schemaString))} ));
 */
			java.io.InputStream ins = com.cycle30.sdk.util.C30DOMUtils.class.getResourceAsStream(xsd);
			log.info("Getting input schema as a resource : "+xsd);
			/*String  schemaString= convertStreamToString((java.io.BufferedInputStream)(ins).);
			log.info("InputSchema : " +schemaString);
			ins = com.cycle30.sdk.util.C30DOMUtils.class.getResourceAsStream(xsd);*/
			factory.setSchema(schemaFactory.newSchema(
					new Source[] {new StreamSource(ins)}));
			//factory.setSchema(schemaFactory.newSchema(streamSrc));
			factory.setXIncludeAware(false);
			DocumentBuilder builder        = factory.newDocumentBuilder();

			ErrorHandler handler = new ErrorHandler() {
				public void warning(SAXParseException e) throws SAXException {
					log.warn("[warning] "+e.getMessage());
				}
				public void error(SAXParseException e) throws SAXException {
					log.warn("[error] "+e.getMessage());
					throw e;
				}
				public void fatalError(SAXParseException e) throws SAXException {
					log.warn("[fatal error] "+e.getMessage());
					throw e;
				}
			};
			

			builder.setErrorHandler(handler);
			domDocument = builder.parse(new InputSource(new StringReader(xml)));

		}
		catch (SAXParseException e) {
			String info = "URI=" + e.getSystemId() +
			" Line=" + e.getLineNumber() +
			": " + e.getMessage();
			log.error(e);
			throw new C30SDKToolkitException( "Unable to convert xml string  xml to a DOM document", e, info, "XML_ERROR");
		}
		catch (Exception e) {
			log.error(e);
			throw new C30SDKToolkitException( "Unable to convert xml string  xml to a DOM document", e,e.getMessage(), "XML_ERROR");
		}

		return domDocument;
	}


	/**
	 * Converts the given inputStream to String.
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String convertStreamToString(InputStream is)
	throws IOException {
		/*
		 * To convert the InputStream to String we use the
		 * Reader.read(char[] buffer) method. We iterate until the
		 * Reader return -1 which means there's no more data to
		 * read. We use the StringWriter class to produce the string.
		 */
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(
						new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {        
			return "";
		}
	}
	

	/**
	 * 
	 * @param doc
	 * @return
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 * @throws TransformerFactoryConfigurationError
	 */
	public static InputStream documentToInputStream(Document doc) throws TransformerConfigurationException, TransformerException, TransformerFactoryConfigurationError 
	{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		Source xmlSource = new DOMSource(doc);
		Result outputTarget = new StreamResult(outputStream);
		TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
		InputStream is = new ByteArrayInputStream(outputStream.toByteArray());
		return is;
	}

	/**
	 * Convert a w3c dom node to a InputStream
	 * @param node
	 * @return
	 * @throws TransformerException
	 */
	public static InputStream nodeToInputStream(Node node) throws TransformerException
	{
	        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	        Result outputTarget = new StreamResult(outputStream);
	        Transformer t = TransformerFactory.newInstance().newTransformer();
	        t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	        t.transform(new DOMSource(node), outputTarget);
	        return new ByteArrayInputStream(outputStream.toByteArray());
	}
	private static final Logger log = Logger.getLogger(C30DOMUtils.class);

}
