/*
 * C30CustomParamComparator.java
 *
 * Created on June 12, 2006, 4:17 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.util;

import java.util.Comparator;

/**
 *
 * @author reej04
 */
public class C30CustomParamComparator implements Comparator {
    
    public static final int PARAM_ORDER = 1;
    public static final int DISPLAY_ORDER = 2;
    
    public int compareField;
    
    /** Creates a new instance of C30CustomParamComparator */
    public C30CustomParamComparator() {
        compareField = PARAM_ORDER;
    }
    
    public void setCompareField(int compareField) {
        this.compareField = compareField;
    }
    
    public int compare(Object o1, Object o2) throws ClassCastException {
        if (!(o1 instanceof C30CustomParamDef) || !(o2 instanceof C30CustomParamDef)) {
            throw new ClassCastException("Objects must be of type C30CustomParamDef");
        }
        
        int result = 0;
        C30CustomParamDef param1 = (C30CustomParamDef) o1;
        C30CustomParamDef param2 = (C30CustomParamDef) o1;
        
        switch(compareField) {
            case PARAM_ORDER:
                result = param1.getParamOrder().compareTo(param2.getParamOrder());
                break;
                
            case DISPLAY_ORDER:
                result = param1.getDisplayOrder().compareTo(param2.getDisplayOrder());
                break;
        }
        
        return result;
    }
    
}
