/*
 * C30CustomParamRetriever.java
 *
 * Created on March 8, 2006, 11:36 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.util;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30TableDataSource;

/**
 *
 * @author reej04
 */
public class C30CustomParamRetriever {
    
	Connection conn;
    String module;
    int languageCode;
    HashMap moduleParams;
    
    /** Creates a new instance of C30CustomParamRetriever */    
    public C30CustomParamRetriever(Connection conn, String module) {
        this(conn, module, 1);
    }
    
    public C30CustomParamRetriever(Connection conn, String module, int languageCode) {
        this.conn = conn;
        this.module = module;
        this.languageCode = languageCode;
        moduleParams = new HashMap();          
    }
    
    public C30ExternalCallDef getExternalCallDef(int entityType) throws Exception {
        return getExternalCallDef(entityType, languageCode);
    }
    
    public C30ExternalCallDef getExternalCallDef(int entityType, int languageCode) throws Exception {        
        //--------------------------------------------------
        // load the params if they have not been loaded yet
        //--------------------------------------------------
        if (moduleParams.size() == 0) {
            loadParams();
        }
        
        //----------------------------------------------------
        // make sure the entity type is valid for this module
        //----------------------------------------------------
        if (!moduleParams.containsKey(new Integer(entityType))) {
            String message = "Entity type '" + entityType + "' is not configured for module '" + module + "'";
            throw new Exception(message);
        }
        
        return (C30ExternalCallDef) moduleParams.get(new Integer(entityType));
    }
    
    private HashMap loadParams() throws Exception {
        getParams();

        //----------------------------------------------------------------
        // for each parameter, load the param value choices if applicable
        //----------------------------------------------------------------
        for (Iterator i = moduleParams.keySet().iterator(); i.hasNext(); ) {
            C30ExternalCallDef call = (C30ExternalCallDef) moduleParams.get(i.next());
            ArrayList inputParams = call.getInputParams();
            for (int j = 0; j < inputParams.size(); j++) {
                C30CustomParamDef param = (C30CustomParamDef) inputParams.get(j);
                if (param.getEnumId() != 0) {
                    int paramId = param.getParamId();
                    int entityType = param.getEntityType();
                    int wildcardFlag = param.getWildcardFlag();
                    param.setParamValueChoices(getDisplayValues(module, entityType, paramId, languageCode, wildcardFlag));
                }
            }
        }
        
        return moduleParams;
    }
    
    private void getParams() throws Exception {
        C30ExternalCallDef proc = new C30ExternalCallDef("get_custom_param_ref");
        
        // set the input parameters
        proc.addParam(new C30CustomParamDef("module", C30CustomParamDef.STRING, 100, C30CustomParamDef.INPUT));
        proc.addParam(new C30CustomParamDef("language_code", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
        
        // set the output parameters
        proc.addParam(new C30CustomParamDef("entity_type_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("param_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("param_name", C30CustomParamDef.STRING, 240, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("param_length", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("param_datatype", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("param_mode", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("assoc_enumeration_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("display_order", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("param_order", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("source_position", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("display_value", C30CustomParamDef.STRING, 240, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("allow_wildcard", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
        
        ArrayList paramValues = new ArrayList();
        paramValues.add(module);
        paramValues.add(String.valueOf(languageCode));
        
        //TODO: replace with data source factory to create data source
        C30JDBCDataSource dataSource = new C30JDBCDataSource(conn);
        dataSource.queryData(proc, paramValues, C30TableDataSource.PARAM_NAMES);
            
        for (int i = 0; i < dataSource.getRowCount(); i++) {
            C30ExternalCallDef callDef;
            
            Number entity = (Number) dataSource.getValueAt(i, 0);
            Integer paramEntity = new Integer(entity.intValue());
            
            if (moduleParams.containsKey(paramEntity)) {
                callDef = (C30ExternalCallDef) moduleParams.get(paramEntity);
            } else {
                callDef = new C30ExternalCallDef();
                moduleParams.put(paramEntity, callDef);
            }

            int entityType = ((Number) dataSource.getValueAt(i, 0)).intValue();
            int id = ((Number) dataSource.getValueAt(i, 1)).intValue();
            String name = (String) dataSource.getValueAt(i, 2);
            int length = ((Number) dataSource.getValueAt(i, 3)).intValue();
            int type = ((Number) dataSource.getValueAt(i, 4)).intValue();
            int mode = ((Number) dataSource.getValueAt(i, 5)).intValue();
            int enumId = 0;
            if (dataSource.getValueAt(i, 6) != null) {
                enumId = ((Number) dataSource.getValueAt(i, 6)).intValue();
            }
            int displayOrder = ((Number) dataSource.getValueAt(i, 7)).intValue();
            int paramOrder = ((Number) dataSource.getValueAt(i, 8)).intValue();
            int sourcePosition;
            if (dataSource.getValueAt(i, 9) == null) {
                sourcePosition = -1;
            } else {
                sourcePosition = ((Number) dataSource.getValueAt(i, 9)).intValue();
            }
            String displayValue = (String) dataSource.getValueAt(i, 10);
            int wildcardFlag = ((Number) dataSource.getValueAt(i, 11)).intValue();
            C30CustomParamDef param = new C30CustomParamDef(entityType, id, name, length, type, mode, enumId, displayOrder, paramOrder, sourcePosition, displayValue, wildcardFlag);

            callDef.addParam(param); 
        }        
    }
    
    private ArrayList getDisplayValues(String module, int entityType, int paramId, int languageCode, int wildcardFlag) throws Exception {
        
        C30ExternalCallDef proc = new C30ExternalCallDef("get_custom_param_enum");
        
        // set the input params
        proc.addParam(new C30CustomParamDef("module", C30CustomParamDef.STRING, 100, C30CustomParamDef.INPUT));
        proc.addParam(new C30CustomParamDef("entity_type_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
        proc.addParam(new C30CustomParamDef("param_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
        proc.addParam(new C30CustomParamDef("language_code", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
        
        // set the output params
        proc.addParam(new C30CustomParamDef("value", C30CustomParamDef.STRING, 10, C30CustomParamDef.OUTPUT));
        proc.addParam(new C30CustomParamDef("display_value", C30CustomParamDef.STRING, 240, C30CustomParamDef.OUTPUT));
        
        ArrayList paramValues = new ArrayList();
        paramValues.add(module);
        paramValues.add(String.valueOf(entityType));
        paramValues.add(String.valueOf(paramId));
        paramValues.add(String.valueOf(languageCode));       
        
        //TODO: replace with data source factory to create data source
        C30JDBCDataSource dataSource = new C30JDBCDataSource(conn);
        dataSource.queryData(proc, paramValues, C30TableDataSource.PARAM_NAMES);
        
        ArrayList paramValueChoices = new ArrayList();
        
        // Add in wildcard "(All)" option if this is a wildcard allow field           
            if (wildcardFlag == 1) {                
                paramValueChoices.add(new C30NameValuePair(new String("-1"), new String("(All)")));            
            }

        for (int i = 0; i < dataSource.getRowCount(); i++) {
            Object name = dataSource.getValueAt(i, 0);
            Object value = dataSource.getValueAt(i, 1);
            paramValueChoices.add(new C30NameValuePair(name, value));
        }
       
        return paramValueChoices;
    }
    
}
