package com.cycle30.sdk.util;

import java.io.*;
import java.util.Properties;

public class C30SDKConfigurationManager
{

    public static C30SDKConfigurationManager getInstance()
    {
        if(c30SDKConfigurationManager == null)
            c30SDKConfigurationManager = new C30SDKConfigurationManager();
        return c30SDKConfigurationManager;
    }

    public Properties getPropertiesFromClassPath(String fileName)
        throws IOException, FileNotFoundException
    {
        InputStream is = getClass().getResourceAsStream(fileName);
        if(is != null)
            return getPropertiesFromStream(is);
        else
            throw new FileNotFoundException((new StringBuilder()).append("Could not find the file '").append(fileName).append("' on the CLASSPATH").toString());
    }

    public String getProperty(String property, String fileName)
        throws IOException, FileNotFoundException
    {
        InputStream is = getClass().getResourceAsStream(fileName);
        if(is != null)
        {
            Properties props = getPropertiesFromStream(is);
            if(props != null)
                return props.getProperty(property);
            else
                return null;
        } else
        {
            throw new FileNotFoundException((new StringBuilder()).append("Could not find the file '").append(fileName).append("' on the CLASSPATH").toString());
        }
    }

    public Properties getPropertiesFromFile(File fileLoc)
        throws IOException, FileNotFoundException
    {
        InputStream is = null;
        try
        {
            is = new FileInputStream(fileLoc);
        }
        catch(FileNotFoundException ex)
        {
            throw ex;
        }
        return getPropertiesFromStream(is);
    }

    public synchronized Properties getPropertiesFromStream(InputStream inputStream)
        throws IOException
    {
        Properties newProps = new Properties();
        try
        {
            newProps.load(inputStream);
            inputStream.close();
        }
        catch(Exception x)
        {
            System.err.println((new StringBuilder()).append("Could not close inputStream - ").append(x).toString());
            x.printStackTrace();
        }
        return newProps;
    }

    private C30SDKConfigurationManager()
    {
    }

    public boolean getBoolean(String propName)
    {
        if(propName == null)
            return false;
        else
            return Boolean.valueOf(propName).booleanValue();
    }

    public int getInt(String propName)
    {
        if(propName == null)
            return -1;
        else
            return (new Integer(propName)).intValue();
    }

    public long getLong(String propName)
    {
        if(propName == null)
            return -1L;
        else
            return (new Long(propName)).longValue();
    }

    private static C30SDKConfigurationManager c30SDKConfigurationManager = null;

}