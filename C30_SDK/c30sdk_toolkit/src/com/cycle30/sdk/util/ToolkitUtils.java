package com.cycle30.sdk.util;

public class ToolkitUtils {

    
    /**
     * 
     * @param xml
     * @param elementName
     * @return XML element value
     */
    public static synchronized String getXmlStringElementValue (String xml, String elementName) {
        
        if (xml == null || xml.length()==0) {
            return "";
        }
        
        
        String beginElement = "<" + elementName + ">";
        int start = xml.indexOf(beginElement);
        
        String endElement = "</" + elementName + ">";
        int end   = xml.indexOf(endElement);
        
        if (start == -1 || end == -1) {
            return null;
        }
        
        // Get description w/i the <description> tags
        start += (elementName.length()) + 2; // start of data = start element (+ 2 for '<' and '>' chars)
        String value = xml.substring(start, end);
        
        return value;
        
    }
}
