/*
 * C30CustomParamDef.java
 *
 * Created on January 13, 2006, 9:30 AM
 */
package com.cycle30.sdk.util;

import java.util.ArrayList;
import java.util.List;
import com.cycle30.sdk.util.C30NameValuePair;

/**
 *
 * @author reej04
 */
public class C30CustomParamDef {
    
    private int entityType;
    private int paramId;
    private String displayName;
    private String paramName;
    private List paramValueChoices;
    private int paramType;
    private int paramLength;
    private int paramMode;
    private int enumId;
    private int displayOrder;
    private int paramOrder;
    private int sourcePosition;
    private int startPosition;
    private int endPosition;
    private int wildcardFlag;
    
    public static final int INT = 1;
    public static final int STRING = 2;
    public static final int DATE = 3;
    public static final int CURRENCY = 4;
    public static final int BOOLEAN = 5;
    public static final int FUTURE_USE = 6;
    public static final int BIG_DECIMAL = 7;
    public static final int PHONE_NUMBER = 8;
    public static final int NUMERIC = 9;
    public static final int ARRAY=10;
    public static final int STRUCT=11;
    public static final int BLOB=12;
    public static final int TIMESTAMP=13;
    
    
    public static final int INPUT = 1;
    public static final int OUTPUT = 2;
    
    /*
     * Creates a new instance of ReportParameterDef 
     */
    public C30CustomParamDef(String name, int type, int mode) {
        this(name, type, -1, mode);
    }
    
    public C30CustomParamDef(String name, int type, int length, int mode) {
        this(-1, -1, name, length, type, mode, -1, -1, -1, -1, null, -1);
    }
    
    public C30CustomParamDef(String name, int type, int length, int mode, int sourcePosition) {
        this(-1, -1, name, length, type, mode, -1, -1, -1, sourcePosition, null, -1);
    }
    
    public C30CustomParamDef(String name, String displayValue, int type, int length, int mode) {
        this(-1, -1, name, length, type, mode, -1, -1, -1, -1, displayValue, -1);
    }
    
    public C30CustomParamDef(int entityType, int id, String name, int length, int type,
                            int mode, int enumId, int displayOrder, int paramOrder,
                            int sourcePosition, String displayValue, int wildcardFlag) {
        this.entityType = entityType;
        this.paramId = id;
        this.paramName = name;
        this.paramLength = length;
        this.paramType = type;
        this.paramMode = mode;
        this.enumId = enumId;
        this.displayOrder = displayOrder;
        this.paramOrder = paramOrder;
        this.sourcePosition = sourcePosition;
        this.displayName = displayValue;
        this.wildcardFlag = wildcardFlag;
        
        startPosition = -1;
        endPosition = -1;
        
        this.paramValueChoices = new ArrayList();   
    }
    
    public int getEntityType() {
        return entityType;
    }
    
    public int getWildcardFlag() {
        return wildcardFlag;
    }
    
    public void setWildcardFlag(int wildcardFlag) {
    	this.wildcardFlag = wildcardFlag;
    }
    
    public int getEnumId() {
        return enumId;
    }
    
    public void setEnumId(int enumId) {
    	this.enumId = enumId;
    }
    
    public int getNumParamValueChoices() {
        return paramValueChoices.size();
    }
    
    public int getParamId() {
        return paramId;
    }
    
    public void setParamId(int paramId) {
    	this.paramId = paramId;
    }
    
    public String getParamName() {
        return paramName;
    }
    
    public int getParamType() {
        return paramType;
    }
    
    public String getParamTypeString() {
        String paramTypeString;
        switch(paramType) {
            case INT: paramTypeString = "INT32"; break;
            case STRING: paramTypeString = "STRING"; break;
            case DATE: paramTypeString = "DATETIME"; break;
            case CURRENCY: paramTypeString = "DECIMAL"; break;
            case BOOLEAN: paramTypeString = "BOOLEAN"; break;
            case FUTURE_USE: paramTypeString = "RESERVED FOR FUTURE USE"; break;
            case NUMERIC: paramTypeString = "NUMERIC"; break;
            case ARRAY: paramTypeString = "ARRAY"; break;
            case STRUCT: paramTypeString = "STRUCT"; break;
            case TIMESTAMP: paramTypeString = "DATETIME"; break; 
            default: paramTypeString = "UNDEFINED";
        }
        return paramTypeString;
    }
    
    public int getParamMode() {
        return paramMode;
    }
    
    public void setParamMode(int paramMode) {
    	this.paramMode = paramMode;
    }
    
    public Integer getParamOrder() {
        return new Integer(paramOrder);
    }
    
    public void setParamOrder(int paramOrder) {
    	this.paramOrder = paramOrder;
    }
    
    public int getParamLength() {
        return paramLength;
    }
    
    public void setParamLength(int paramLength) {
    	this.paramLength = paramLength;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    
    public void setDisplayName(String displayName) {
    	this.displayName = displayName;
    }
    
    public Integer getDisplayOrder() {
        return new Integer(displayOrder);
    }
    
    public void setDisplayOrder(int displayOrder) {
    	this.displayOrder = displayOrder;
    }
    
    /**
     * @deprecated use addParamValueChoice(Object, Object) instead.
     * @param id
     * @param value
     */
    public void addParamValueChoice(Integer id, String value) {
        C30NameValuePair pair = new C30NameValuePair(id, value);
        paramValueChoices.add(pair);
    }
    
    public void addParamValueChoice(Object id, Object value) {
        C30NameValuePair pair = new C30NameValuePair(id, value);
        paramValueChoices.add(pair);
    }
    
    public List getParamValueChoices() {
        return paramValueChoices;
    }
    
    public int getSourcePosition() {
        return sourcePosition;
    }
    
    public void setSourcePosition(int sourcePosition) {
    	this.sourcePosition = sourcePosition;
    }
    
    /**
     * Method which is used to identify the position in a fixed 
     * width file for where the parameter value is to be located.
     * @return the starting position of where the parameter value is located.
     */
    public int getSourceStartPosition() {
        return startPosition;
    }
    
    /**
     * Method which is used to identify the position in a fixed 
     * width file for where the parameter value is to be located.
     * @return the ending position of where the parameter value is located.
     */
    public int getSourceEndPosition() {
        return endPosition;
    }
    
    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }
    
    public void setParamValueChoices(List choices) {
        paramValueChoices = choices;
    }
    
    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }
    
    
 
}

