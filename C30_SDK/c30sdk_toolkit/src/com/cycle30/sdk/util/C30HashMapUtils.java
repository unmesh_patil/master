/*
 * C30HashMapUtils.java
 *
 * Created on January 16, 2008, 4:34 PM
 */

package com.cycle30.sdk.util;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Vector;


/**
 * Utility class that helps when working with hashmaps.
 * @author tomansley
 */
public class C30HashMapUtils {

	/**
	 * Utility method for printing HashMaps.  This is made available purely for debugging purposes
	 * @param map HashMap to be printed.
	 * @param level the tab level that the map is at.  This is for pretty printing.
	 */
	public static String getHashMapPrintOut(Map map) {
		return getHashMapPrintOut(map, 0);
	}

	/**
	 * Internal method which is the same as the above method except it exposes the 
	 * level that the hashmap is at to be printed.  The reason it is exposed is because
	 * the method uses itself and needs to know what level its at.  This is made 
	 * available purely for debugging purposes
	 * @param map HashMap to be printed.
	 * @param level the tab level that the map is at.  This is for pretty printing.
	 */
	private static String getHashMapPrintOut(Map map, int level) {
	
		StringBuffer mapString = new StringBuffer();
		Object key = null;
		
		//----------------------------------------
		//this sorts the keys so that we can find 
		// what we are looking for much easier.
		//----------------------------------------
		java.util.List keys = new java.util.ArrayList(map.keySet());
		java.util.Collections.sort(keys);

		//------------------------------------------------
		// This is purely for aesthetics.  Nice tabbing 
		// so that things can be viewed better on output.
		//------------------------------------------------
		String tabs = "";
		for(int j = 0; j <= level; j++) {
			tabs = tabs + "   ";
		}
		
		//Iterator i = keys.iterator();
		for (int i = 0; i < keys.size(); i++) {
			
			key = keys.get(i);
			
			Object o = map.get(key);
			if (o instanceof HashMap) {
				mapString.append( tabs + "   " + key + " = {\n");
				mapString.append(C30HashMapUtils.getHashMapPrintOut((Map) o, ++level));
				level--;
			} else if (o instanceof Object[]) {
				mapString.append( tabs + "   " + key + " = {\n");
				for (int j = 0; j < ((Object[]) o).length; j++ ) {
					if (((Object[]) o)[j] instanceof HashMap) {
						mapString.append( tabs + "      " + key + " = {\n");
						++level;
						mapString.append(C30HashMapUtils.getHashMapPrintOut((Map) ((Object[]) o)[j], ++level));
						level = level -2;
					}
				}
				mapString.append( tabs  + "   " + "}\n");
			} else {
				if (o != null) {
					mapString.append( tabs + "   " + key + " = '" + o + "' (" + o.getClass().getName() + ")" + "\n");
				} else {
					mapString.append( tabs + "   " + key + " = 'null' (null)" + "\n");
				}
			}
						
		}
		mapString.append( tabs + "}\n");
		return mapString.toString();
	}
	
	/**
	 * Method which takes an array of objects and creates a new array which is one object 
	 * bigger and places the given object into the array at the end
	 * @param array the array to put the object into
	 * @param object the object being placed into the array
	 * @return a new object array which is bigger than the input array by one object.  This array contains the input object.
	 */
	public static Object[] addObjectToArray(Object[] array, Object object) {
		
    	int currentLength = array.length;
    	Object[] newDataParams = null;
    	
    	//if the last position is NULL then don't increase the size of the array
		if (array[array.length-1] == null) {
			newDataParams = new Object[currentLength];
			
		//if we need to add a new position then add one
		} else {
			newDataParams = new Object[currentLength+1];
	    	for (int i = 0; i < currentLength; i++) {
	    		newDataParams[i] = array[i];
	    	}
	    	
		}
		newDataParams[newDataParams.length-1] = object;
		return newDataParams;
	}

/**
	 * Method which takes an array of HashMaps and creates a new array which is one HashMap
	 * bigger and places the given object into the array at the end if this aboject was
     * not already in the original array, comparing the key. If it was already inserted,
     * simply update its values.
	 * @param array the array to put the object into
	 * @param object the object being placed into the array
     *
	 * @return a new object array which is bigger than the input array by one object.  This array contains the input object.
	 */
	public static Object[] addUniqueHashMapToArray(Object[] array, HashMap newHashMap, String key) {

        if (array == null) {
            array = new Object[1];
        }

    	int currentLength = array.length;

        // try update existing HashMap first
        HashMap currentHashMap;
        for (int i = 0; i < currentLength; i++) {
            currentHashMap = (HashMap) array[i];
            if ( currentHashMap.get(key).equals(newHashMap.get(key)) ) {
                array[i] = newHashMap;
                return array;
            }
        }

        // new HashMap is not already a member of the array
        Object[] newDataParams = null;

    	//if the last position is NULL then don't increase the size of the array
		if (array[array.length-1] == null) {
			newDataParams = new Object[currentLength];

		//if we need to add a new position then add one
		} else {
			newDataParams = new Object[currentLength+1];
	    	for (int i = 0; i < currentLength; i++) {
	    		newDataParams[i] = array[i];
	    	}

		}
		newDataParams[newDataParams.length-1] = newHashMap;
		return newDataParams;
	}


    /**
	 * Method which takes a HashMap object the name of the key
	 * which contains an array of HashMaps and a sub key to
     * identify which values must be unique in those HashMaps.
     * The original HashMap will have all duplicates removed from
     * that array of sub HashMaps.
	 * @param array the original HashMap which needs to be cleaned up
	 * @param key the name of the key used to identify the array of objects inside the original HashMap (ex.: "ExtendedData")
     * @param subKey the sub HashMap key that will be made unique (ex.: "ParamId")
	 * @return void
     */
    public static void removeDuplicatesFromArray(Map map, String key, String subKey) {
        
    	if (map.get(key) != null) {
	        Object [] objArray = (Object[]) map.get(key);
	
	        HashSet uniqueSubKeys = new HashSet();
	        Vector newCollection = new Vector();
	        HashMap current;
	        Object currentObjValue;
	        for (int i = 0; i < objArray.length ; i++) {
	            current = (HashMap) objArray[i];
	            currentObjValue = current.get(subKey);
	            if (!uniqueSubKeys.contains(currentObjValue)) {
	                uniqueSubKeys.add(currentObjValue);
	                current = new HashMap((HashMap) objArray[i]);
	                newCollection.add(current);
	            }
	        } // for
	
	        Object [] newObjArray = newCollection.toArray();
	
	        map.put(key, newObjArray);
    	}
    }


}
