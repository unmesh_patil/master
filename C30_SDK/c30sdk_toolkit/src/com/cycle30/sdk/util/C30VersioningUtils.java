/*
 * C30HashMapUtils.java
 *
 * Created on January 16, 2008, 4:34 PM
 */

package com.cycle30.sdk.util;

/**
 * Utility class that helps when working with hashmaps.
 * @author tomansley
 */
public class C30VersioningUtils {

    public static Double API_VERSION_1_0 = new Double("1.0");
    public static Double API_VERSION_2_0 = new Double("2.0");
    public static Double API_VERSION_3_0 = new Double("3.0");

    public static Double PS_VERSION_1_0 = new Double("1.0");
    public static Double PS_VERSION_2_0 = new Double("2.0");
    public static Double PS_VERSION_3_0 = new Double("3.0");

    public static String API_VER_1_0 = new String("1.0");
    public static String API_VER_2_0 = new String("2.0");
    public static String API_VER_3_0 = new String("3.0");


    
}
