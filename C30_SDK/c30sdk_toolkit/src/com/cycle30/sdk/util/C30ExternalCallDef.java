/*
 * C30ExternalCallDef.java
 *
 * Created on March 7, 2006, 2:20 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.util;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author reej04
 */
public class C30ExternalCallDef {
    
    String callName;
    ArrayList inputParams;
    ArrayList outputParams;
    C30CustomParamComparator comparator;
    
    /**
     * Creates a new instance of C30ExternalCallDef 
     */
    public C30ExternalCallDef() {
        this(null);
    }
    
    public C30ExternalCallDef(String callName) {
        this.callName = callName;
        inputParams = new ArrayList();
        outputParams = new ArrayList();
        comparator = new C30CustomParamComparator();
    }
    
    public void addParam(C30CustomParamDef param) throws Exception {
        if (param.getParamMode() == C30CustomParamDef.INPUT) {
            inputParams.add(param);
        } else if (param.getParamMode() == C30CustomParamDef.OUTPUT) {
            outputParams.add(param);
        } else {
            throw new Exception("Invalid param mode");
        }
    }
    
    public String getCallName() {
        return callName;
    }
    
    public C30CustomParamDef getInputParam(int index) {
        return (C30CustomParamDef) inputParams.get(index);
    }
    
    public ArrayList getInputParams() {
        comparator.setCompareField(C30CustomParamComparator.PARAM_ORDER);
        return inputParams;
    }
    
    public ArrayList getInputParamsByDisplayOrder() {
        comparator.setCompareField(C30CustomParamComparator.DISPLAY_ORDER);
        Collections.sort(inputParams, comparator);
        return inputParams;
    }
    
    public ArrayList getInputParamDisplayValues() {
        ArrayList inputDisplayValues = new ArrayList();
        for (int i = 0; i < inputParams.size(); i++) {
            inputDisplayValues.add(((C30CustomParamDef) inputParams.get(i)).getDisplayName());
        }
        return inputDisplayValues;
    }
    
    public ArrayList getInputParamNames() {
        ArrayList inputNames = new ArrayList();
        for (int i = 0; i < inputParams.size(); i++) {
            inputNames.add(((C30CustomParamDef) inputParams.get(i)).getParamName());
        }
        return inputNames;    
    }
    
    public int getNumInputParams() {
        return inputParams.size();
    }
    
    public int getNumOutputParams() {
        return outputParams.size();
    }
    
    public C30CustomParamDef getOutputParam(int index) {
        return (C30CustomParamDef) outputParams.get(index);
    }
    
    public ArrayList getOutputParams() {
        return outputParams;
    }
    
    public ArrayList getOutputParamDisplayValues() {
        ArrayList outputDisplayValues = new ArrayList();
        for (int i = 0; i < outputParams.size(); i++) {
            outputDisplayValues.add(((C30CustomParamDef) outputParams.get(i)).getDisplayName());
        }
        return outputDisplayValues;
    }
    
    public String getOutputParamName(String outputDisplayName) {
        String paramName = null;
        for (int i = 0; i < outputParams.size(); i++) {
            C30CustomParamDef param = (C30CustomParamDef) outputParams.get(i);
            if (param.getDisplayName().equals(outputDisplayName)) {
                paramName = param.getParamName();
            }
        }
        return paramName;
    }
    
    public ArrayList getOutputParamNames() {
        ArrayList outputNames = new ArrayList();
        for (int i = 0; i < outputParams.size(); i++) {
            outputNames.add(((C30CustomParamDef) outputParams.get(i)).getParamName());
        }
        return outputNames;    
    }
    
    public void removeParam(C30CustomParamDef param) throws Exception {
        if (param.getParamMode() == C30CustomParamDef.INPUT) {
            inputParams.remove(param);
        } else if (param.getParamMode() == C30CustomParamDef.OUTPUT) {
            outputParams.remove(param);
        } else {
            throw new Exception("Invalid param mode");
        }
    }
    
    public void setCallName(String callName) {
        this.callName = callName;
    }
    
    public void setInputParams(ArrayList inputParams) {
        this.inputParams = inputParams;
    }
    
    public void setOutputParams(ArrayList outputParams) {
        this.outputParams = outputParams;
    }
    
}
