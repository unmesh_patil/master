/*
 * C30PropertyValidator.java
 *
 * Created on July 19, 2006, 2:40 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.util;

import java.io.File;
import java.util.Properties;

/**
 *
 * @author reej04
 */
public class C30PropertyValidator {
    
    /** Creates a new instance of C30PropertyValidator */
    public C30PropertyValidator() {
    }
    
    /**
     * Validates that the property is defined in the properties file.
     *
     * @param properties properties file to read the property from
     * @param property   property to validate
     */
    public static String validateString(Properties properties, String property) throws Exception {
        String value = properties.getProperty(property);
        
        if (value == null) {
            String message = "Property \"" + property + "\" is not defined in the properties file.";
            throw new Exception(message);
        }
        
        return value;
    }
    
    /**
     * Validates that the property is a valid filename.
     *
     * @param properties properties file to read the property from
     * @param property   property to validate
     */
    public File validateFile(Properties properties, String property) throws Exception {
        String value = validateString(properties, property);
        File file = null;
        
        try {
            file = new File(property);
        } catch (Exception e) {
            String message = "Invalid file \"" + value + "\"";
            throw new Exception(message);
        }
        
        return file;
    }
    
}