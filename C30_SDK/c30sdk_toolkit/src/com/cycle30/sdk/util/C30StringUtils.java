/*
 * C30StringUtils.java
 *
 * Created on June 5, 2006, 4:34 PM
 */

package com.cycle30.sdk.util;

import java.io.StringWriter;
import java.io.PrintWriter;

/**
 *
 * @author reej04
 */
public class C30StringUtils {
    
    /** Creates a new instance of C30StringUtils */
    public C30StringUtils() {
    }
    
    public static String correctDelimitedLine(String line, String delimiter) {
        //----------------------------------------------------------
        // the next three steps are a workaround to fix issues with
        // StringTokenizer ignoring two delimeters in a row
        //----------------------------------------------------------
        StringBuffer correctedLine = new StringBuffer(line);

        //------------------------------------------------------------------
        // 1) if the first string is the delimiter, add a NULL at the front
        //------------------------------------------------------------------
        if (correctedLine.indexOf(delimiter) == 0) {
            correctedLine.replace(0, 1, "NULL" + delimiter);
        }

        //----------------------------------------------------------------
        // 2) if there are two delimiters in a row, add a NULL in between
        //----------------------------------------------------------------
        while (correctedLine.indexOf(delimiter + delimiter) >= 0) {
            int index = correctedLine.indexOf(delimiter + delimiter);
            correctedLine.replace(index, index + 1, delimiter + "NULL");
        }

        //---------------------------------------------------------------
        // 3) if the last string is the delimiter, add a NULL at the end
        //---------------------------------------------------------------
        int lastPos = correctedLine.length() - delimiter.length();
        if (correctedLine.indexOf(delimiter, lastPos) == lastPos) {
            correctedLine.append("NULL");
        }
        
        return correctedLine.toString();
    }
    
    /**
     * Method to take an exception and return its stack trace as a string
     * @param exception the exception we are getting the stack trace from.
     * @return a string representation of the stack trace.
     */
    public static String getStackTrace(Throwable exception) {
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw, true);
    	exception.printStackTrace(pw);
    	pw.flush();
    	sw.flush();
    	return sw.toString();
    }
    
    /**
     * Method to take an exception and recursively return all readable messages
     * that are propogated up through the causes within exception stack trace. This method is 
     * used so that the readable messages can be obtains indepedently from their location 
     * in the stack.  
     * @param exception the exception we are getting the stack trace from.
     * @return a string representation of the stack trace.
     */
    public static String getNestedExceptionMessage(Throwable exception) {
    	String message = exception.getMessage();
        
        if(exception.getCause() != null) {
            message = message + getNestedExceptionMessage(exception.getCause());
        }

        return message;
    }
    
    public static boolean validPhoneNumber(String phoneNumber, int minLength, int maxLength) {
        boolean result = true;
        String validCharacters = "0123456789%*";
        
        if (phoneNumber.length() == 0) {
            result = true;
        } else if (phoneNumber.length() > maxLength) {
            result = false;
        } else if (phoneNumber.length() < minLength && phoneNumber.indexOf('*') == -1 && phoneNumber.indexOf('%') == -1) {
            result = false;
        } else {
            for (int i = 0; i < phoneNumber.length(); i++) {
                if (validCharacters.indexOf(phoneNumber.charAt(i)) == -1) {
                    result = false;
                    break;
                }
            }
        }
        
        return result;
    }
    
    
    /**
     * this method transforms a SQL column name to a Kenan API attriubute name
     * following the convention that the first letter and all the letter characters that
     * immediately succeeding the underscore are uppercase while all other letter characters are lower case.
     * Numeric characters stay but underscores are removed.
     * @param sqlName
     * @return
     */
    public static String SqlToKenanName(String sqlName)
       
    {
        String sBuf = "";
        if (sqlName == null || sqlName.length() == 0)
           return sqlName;
           
        sBuf = sqlName.toLowerCase();
        StringBuffer buf = new StringBuffer( sBuf );
        
         char underScore = '_';
         int targetChar;
         targetChar = (int)sBuf.charAt(0);
         if (targetChar >= 97 && targetChar <= 122)
             buf.setCharAt(0, (char)(targetChar - 32));
         
         for (int i = 0; i < sBuf.length()-1; i++)
         {
             
             if (sBuf.charAt(i) == underScore)
             {
               targetChar = (int)sBuf.charAt(i+1);
               if (targetChar >= 97 && targetChar <= 122)
                   buf.setCharAt( i+1, (char)(targetChar - 32));
               
             }
             
         }
         sBuf = buf.toString();
         sBuf = sBuf.replaceAll("_","");
        return sBuf;
    }
}
