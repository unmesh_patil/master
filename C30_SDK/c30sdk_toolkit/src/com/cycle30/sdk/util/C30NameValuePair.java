/*
 * C30NameValuePair.java
 *
 * Created on January 30, 2006, 6:44 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.cycle30.sdk.util;

/**
 *
 * @author reej04
 */
public class C30NameValuePair {
    private Object name;
    private Object value;
    
    /**
     * Creates a new instance of C30NameValuePair 
     */
    public C30NameValuePair(Object name, Object value) {
        this.name = name;
        this.value = value;
    }
    
    public Object getName() {
        return name;
    }
    
    public Object getValue() {
        return value;
    }
    
    public String toString() {
        return value.toString();
    }
    
}
