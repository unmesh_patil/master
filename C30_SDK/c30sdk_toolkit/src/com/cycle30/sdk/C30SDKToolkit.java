package com.cycle30.sdk;

public class C30SDKToolkit {

	public static void main(String[] args) { 

		System.out.println("");
		System.out.println("**********************************************************************");
		System.out.println("C30sdk_toolkit Build Version : " + C30SDKToolkitLatestBuildVersion.LATEST_BUILD_VERSION);
		System.out.println("@2012. www.Cycle30.com. All rights Reserved");
		System.out.println("**********************************************************************");
		System.out.println("");
		System.out.println("C30sdk_toolkit.jar has to be used from the C30 BIL not for direct use by other modules.");
		
	}
}
