package com.cycle30.sdk.toolkit.exception;

import java.io.PrintStream;
import java.io.PrintWriter;


/** This Class is Responsible for the All C30SDK Exceptions.
 *All the C30SDK Exception classes extends this class
 * This is developed as part of the C30SDK Development Framework.
 * @author Ranjith Kumar Nelluri, Tom
 *
 */
public class C30SDKToolkitException extends Exception {

	private String exceptionCode = "";
	private String shortDescr="";
	private String AdditionalErrorText ="";

	/**
	 * 
	 */
	private static final long serialVersionUID = -1776631507072582049L;

	/**
	 * Hidden constructor for creating a C30SDK exception.
	 */
	protected C30SDKToolkitException() {
	}
	
	/**
	 * Constructor which sets a message into the exception.
	 * @param message the message being set.
	 */
	public C30SDKToolkitException(final String message) {
		super(message);
	}
	
	public String getAdditionalErrorText() {
		return AdditionalErrorText;
	}

	public void setAdditionalErrorText(String additionalErrorText) {
		AdditionalErrorText = additionalErrorText;
	}
	/**
	 * Constructor which sets a message and exception code into the exception.
	 * @param message the message being set.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKToolkitException(final String message, final String exceptionCode) {
		super(message);
		this.exceptionCode = exceptionCode;
	}
	
	/**
	 * Constructor which sets a message and a thrown exception into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 */
	public C30SDKToolkitException(final String message, final Throwable exception) {
		super(message);
		this.initCause(exception);
	}
	
	/**
	 * Constructor which sets a message, a thrown exception and an exception code into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKToolkitException(final String message, final Throwable exception, final String exceptionCode) {
		super(message, exception);
		//this.initCause(exception);
		this.exceptionCode = exceptionCode;
		
	}
	/**
	 * Constructor which sets a message and exception code into the exception.
	 * @param message the message being set.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKToolkitException(final String message, final String AdditionalErrorText, final String exceptionCode) {
		super(message);
		this.exceptionCode = exceptionCode;
		this.AdditionalErrorText = AdditionalErrorText;
	}
	/**
	 * Constructor which sets a message, a thrown exception and an exception code into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 * @param shortdescr Short Descirption of the Exception.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKToolkitException(final String message,  final Throwable exception,final String AdditionalErrorText, final String exceptionCode) {
		super(message, exception);
		//this.initCause(exception);
		this.exceptionCode = exceptionCode;
		this.AdditionalErrorText = AdditionalErrorText;
	}
	/**
	 * Constructor which sets a message, a thrown exception and an exception code into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 * @param shortdescr Short Descirption of the Exception.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKToolkitException(final String message, final String shortdescr, final Throwable exception, final String exceptionCode) {
		super(message, exception);
		//this.initCause(exception);
		this.exceptionCode = exceptionCode;
		this.shortDescr= shortdescr;
		
	}
	
	/**
	 * Method to get the exception code associated with this exception.
	 * @return the exception code
	 */
	public final String getExceptionCode() {
		return exceptionCode;
	}
	
	/**
	 * Method to set the exception code associated with this exception.
	 * @param exceptionCode the exception code the exception should be associated with.
	 */
	public final void setExceptionCode(final String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	/** 
	 * (non-Javadoc).
	 * @param ps the print stream the stack trace should be sent to.
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintStream)
	 */
	@Override
	public final void printStackTrace(final PrintStream ps) {
		super.printStackTrace(ps);
	}

	/** 
	 * (non-Javadoc).
	 * @param pw the print writer the stack trace should be sent to.
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintWriter)
	 */
	@Override
	public final void printStackTrace(final PrintWriter pw) {
		super.printStackTrace(pw);
	}

	/**
	 * (non-Javadoc).
	 * @see java.lang.Throwable#printStackTrace()
	 */
	@Override
	public final void printStackTrace() {		
		super.printStackTrace();
	}
	
	public String getShortDescr() {
		return shortDescr;
	}

	public void setShortDescr(String shortDescr) {
		this.shortDescr = shortDescr;
	}


}
