/*
 * C30SDKSwingUtils.java
 *
 * Created on June 6, 2006, 6:37 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing.util;

import javax.swing.JTable;
import javax.swing.table.TableColumn;

/**
 * This class contains utility methods used with Java Swing objects.
 * @author John Reeves
 */
public class C30SDKSwingUtils {
    
    /**
     * Creates a new instance of C30SDKSwingUtils
     */
    public C30SDKSwingUtils() {
    }
    
    /**
     * Converts a visible column index to a column index in the model.
     * @param table the table containing the columns
     * @param vColIndex the index of the visible column 
     * @return the index of the column in the model or -1 if the index does not exist
     */
    public static int toModel(JTable table, int vColIndex) {
        if (vColIndex >= table.getColumnCount()) {
            return -1;
        }
        return table.getColumnModel().getColumn(vColIndex).getModelIndex();
    }
    
    /**
     * Converts a column index in the model to a visible column index.
     * @param table the table containing the columns
     * @param mColIndex the index of the model column
     * @return the index of the visible column or -1 if the index does not exist
     */
    public static int toView(JTable table, int mColIndex) {
        for (int c=0; c<table.getColumnCount(); c++) {
            TableColumn col = table.getColumnModel().getColumn(c);
            if (col.getModelIndex() == mColIndex) {
                return c;
            }
        }
        return -1;
    }

    
}
