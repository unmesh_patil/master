/*
 * C30PhoneNumberFormattedTextField.java
 *
 * Created on August 21, 2006, 11:55 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing;

import java.text.ParseException;
import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

/**
 * This class represents a JFormattedTextField that contains phone
 * number data.  This allows the JFormattedTextField to easily
 * format the output for phone numbers by simply instantiating
 * this object.
 * @author John Reeves
 */
public class C30PhoneNumberFormattedTextField extends JFormattedTextField {
    
    /**
     * Default mask to use if no mask is supplied.
     */
    private static final String DEFAULT_MASK = "(***) ***-****";
    
    /**
     * Creates a new instance of C30PhoneNumberFormattedTextField
     * @throws java.lang.Exception if the phone number mask can not be parsed
     */
    public C30PhoneNumberFormattedTextField() throws Exception {
        this(DEFAULT_MASK);
    }
    
    /**
     * Creates a new instance of C30PhoneNumberFormattedTextField
     * @param mask the mask to use for the phone number format
     * @throws java.lang.Exception if the phone number mask can not be parsed
     */
    public C30PhoneNumberFormattedTextField(String mask) throws Exception {
        super(createFormatter(mask));
    }
    
    /**
     * Creates a MaskFormatter object from the mask supplied.  This method
     * is static because the formatter is used to instantiate the super
     * JFormattedTextField object.
     * @param mask the mask to use for the phone number format
     * @throws java.lang.Exception if the phone number mask can not be parsed
     * @return the MaskFormatter that was created
     */
    public static MaskFormatter createFormatter(String mask) throws Exception {
        MaskFormatter formatter = new MaskFormatter(mask);
        setPlaceholderCharacter(formatter);
        formatter.setValidCharacters("0123456789*%");
        formatter.setAllowsInvalid(false);
        formatter.setValueContainsLiteralCharacters(false);
        return formatter;
    }
    
    /**
     * Sets the mask of the MaskFormatter.  This method can be used if the
     * C30PhoneNumberFormattedTextField has already been instantiated with
     * a mask and it needs to be changed.
     * @param mask the mask to use for the phone number format
     * @throws java.lang.Exception if the phone number mask can not be parsed
     */
    public void setMask(String mask) throws Exception {
        MaskFormatter formatter = (MaskFormatter) getFormatter();
        formatter.setMask(mask);
        setPlaceholderCharacter(formatter);
        
        //---------------------------------------------------
        // this is a hack to get the JFormattedTextField to
        // redraw the updated mask in the the text field
        //---------------------------------------------------
        setValue(getValue());
    }
    
    /**
     * Sets the placeholder character of the MaskFormatter.  If the
     * mask contains literal characters (such as "(", ")", or "-"),
     * then the placeholder character will be set to an underscore
     * for easier readibility.
     * @param formatter the MaskFormatter to set the placeholder character of
     */
    private static void setPlaceholderCharacter(MaskFormatter formatter) {
        //---------------------------------------------------------
        // if the mask has literal characters (ex - (***) ***-****)
        // then set the placeholder character so it displays better
        //----------------------------------------------------------
        if (formatter.getMask().indexOf("-") > 0) {
            formatter.setPlaceholderCharacter('_');
        } else {
            formatter.setPlaceholderCharacter(' ');
        }     
    }
    
    /**
     * Overwritten method to expand wildcards if needed to the remaining positions
     * in the phone number before the value is committed by the parent class.
     * This allows the user to enter a wildcard value like "303*", which will
     * then be expanded to "303*******" so the value meets the required number
     * of digits defined in the input mask.
     * 
     * (A better way to do this would be to get the input mask to use regular expressions)
     * @throws java.text.ParseException if the AbstractFormatter is not able to format the current value
     */
    public void commitEdit() throws ParseException {
        String text = getText().trim();
        MaskFormatter formatter = (MaskFormatter) getFormatter();
        char lastChar = text.charAt(text.length() - 1);
        
        //--------------------------------------------------------------------
        // if the last character is a wildcard, repeat the wildcard character
        // until the text is the proper length expected by the mask
        //--------------------------------------------------------------------
        if ((lastChar == '*' || lastChar == '%') && formatter.getValidCharacters().indexOf(lastChar) > 0)  {
            StringBuffer wildcardText = new StringBuffer(text);
            String mask = ((MaskFormatter) getFormatter()).getMask();
            while (wildcardText.length() < mask.length()) {
                wildcardText.append(lastChar);
            }
            setText(wildcardText.toString());
        }
        
        super.commitEdit();
    }
    
}
