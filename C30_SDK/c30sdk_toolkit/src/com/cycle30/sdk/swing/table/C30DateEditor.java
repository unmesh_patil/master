/*
 * C30DateEditor.java
 *
 * Created on March 4, 2006, 6:01 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.swing.table;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.util.Date;
import java.text.*;
import java.awt.event.*;
import java.awt.*;

/**
 *
 * @author reej04
 */
public class C30DateEditor extends DefaultCellEditor implements TableCellEditor {
    protected JFormattedTextField ftf;
    protected SimpleDateFormat dateFormat;
    //Date currentDate;

    /** Creates a new instance of C30DateEditor */
    public C30DateEditor(SimpleDateFormat dateFormat) {     
        super(new JFormattedTextField());
        this.ftf = (JFormattedTextField) this.getComponent();
        this.dateFormat = dateFormat;
        //this.currentDate = new Date();
        
        DateFormatter dateFormatter = new DateFormatter(dateFormat);
        ftf.setFormatterFactory(new DefaultFormatterFactory(dateFormatter));
        //ftf.setValue(currentDate);
        ftf.setHorizontalAlignment(JTextField.TRAILING);
        ftf.setFocusLostBehavior(JFormattedTextField.PERSIST);

        ftf.getInputMap().put(KeyStroke.getKeyStroke(
                                        KeyEvent.VK_ENTER, 0),
                                        "check");
        ftf.getActionMap().put("check", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
		if (!ftf.isEditValid()) { //The text is invalid.
                    if (userSaysRevert()) { //reverted
		        ftf.postActionEvent(); //inform the editor
		    }
                } else try {              //The text is valid,
                    ftf.commitEdit();     //so use it.
                    ftf.postActionEvent(); //stop editing
                } catch (java.text.ParseException exc) { }
            }
        });

    }
    
    public Component getTableCellEditorComponent(JTable table, Object value,
						 boolean isSelected,
						 int row, int column) {
        
        ftf = (JFormattedTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);
        if (value != null) {
            ftf.setValue(value);
        }
        ftf.setCaretPosition(ftf.getText().length());
        ftf.setFont(ftf.getFont().deriveFont(11f));
	return ftf;
    }
    
    public Object getCellEditorValue() {
        Object value = ftf.getValue();
        if (value instanceof Date || value == null) {
            return value;
        } else {
            try {
                return dateFormat.parseObject(value.toString());
            } catch (ParseException exc) {
                System.err.println("getCellEditorValue: can't parse value: " + value);
                return null;
            }
        }
    }
    
    public boolean stopCellEditing() {
        if (ftf.isEditValid()) {
            try {
                ftf.commitEdit();
            } catch (java.text.ParseException exc) { }
	    
        } else { //text is invalid
            if (!userSaysRevert()) { //user wants to edit
	        return false; //don't let the editor go away
	    } 
        }
        return super.stopCellEditing();
    }

    protected boolean userSaysRevert() {
        Toolkit.getDefaultToolkit().beep();
        ftf.selectAll();
        Object[] options = {"Edit",
                            "Revert"};
        int answer = JOptionPane.showOptionDialog(
            SwingUtilities.getWindowAncestor(ftf),
            "Date must be in "
            + dateFormat.toPattern().toLowerCase() + " format.\n"
            + "You can either continue editing "
            + "or revert to the last valid value.",
            "Invalid Date",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.ERROR_MESSAGE,
            null,
            options,
            options[1]);
	    
        if (answer == 1) { //Revert!
            ftf.setValue(ftf.getValue());
	    return true;
        }
	return false;
    }


}
