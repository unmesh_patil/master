/*
 * C30TableModel.java
 *
 * Created on January 13, 2006, 2:40 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.cycle30.sdk.swing.table;

import java.util.*;
import java.lang.Object;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author morj02
 */
public class C30TableModel extends AbstractTableModel {
    
	private static final long serialVersionUID = 1L;
	protected ArrayList columnNames;
    protected ArrayList data;
    
    /**
     * Creates a new instance of C30TableModel 
     */
    public C30TableModel() {
        columnNames = new ArrayList();
        data = new ArrayList();
    }
    
    // copy constructor
    public C30TableModel(C30TableModel tableModel) {
        // creates a shallow copy of the columns and data
        // if a deep copy is needed, it should be implemented
        // in a subclass of the C30TableModel that knows how
        // to create copys of the data.
        columnNames = (ArrayList) tableModel.getColumnNames().clone();
        data = new ArrayList();
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            data.add(tableModel.getRowAt(i).clone());
        }        
    }
    
    public C30TableModel(ArrayList columnNames) {
        this.columnNames = columnNames;
        data = new ArrayList();
    }

    public void add(C30TableModel dataSource) {
        for (int i = 0; i < dataSource.getRowCount(); i++) {
            data.add(dataSource.getRowAt(i));
        }
        
        if (columnNames.size() == 0) {
            for (int i = 0; i < dataSource.columnNames.size(); i++) {
                columnNames.add(dataSource.columnNames.get(i));
            }
        }
   
        //this.fireTableRowsInserted(data.size() - dataSource.getRowCount() -1, data.size() - 1);
        this.fireTableDataChanged();
    }
    
    public void addColumn(String columnName, Object value) {
        columnNames.add(columnName);
        for (int i = 0; i < data.size(); i++) {
            ((ArrayList) data.get(i)).add(value);
        }
        this.fireTableStructureChanged();
    }
    
    /*public void addColumn(String columnName, ArrayList column) {
        //TBD
    }*/
    
    public void addRow(ArrayList row) {
        int rowIndex = data.size();
        data.add(row);
        this.fireTableRowsInserted(rowIndex, rowIndex);
    }
    
    public void addRow(ArrayList row, ArrayList columnNames) {
        data.add(row);
        this.columnNames = columnNames;
        this.fireTableDataChanged();
    }
    
    public void clear() {
        columnNames.clear();
        clearData();
    }
    
    public void clearData() {
        int rowCount = this.getRowCount();
        if (rowCount > 0) {
            data.clear();
            this.fireTableRowsDeleted(0, rowCount-1);
        }
    }
    
    public ArrayList findRowIndexes(ArrayList row) {
        return findRowIndexes(row, 0, row.size());
    }
    
    public ArrayList findRowIndexes(ArrayList row, int beginColumnIndex, int endColumnIndex) {
        ArrayList rowIndexes = new ArrayList();
        
        for (int i = 0; i < data.size(); i++) {
            boolean match = true;
            for (int j = 0; j < endColumnIndex - beginColumnIndex; j++) {
                Object value = getValueAt(i, j + beginColumnIndex);
                if (value == null && row.get(j) != null) {
                    match = false;
                    break;
                } else if (!this.getValueAt(i, j + beginColumnIndex).equals(row.get(j))) {
                    match = false;
                    break;
                }
            }
            if (match) {
                rowIndexes.add(new Integer(i));
            }
        }
        
        return rowIndexes;        
    }
    
    public ArrayList findRowIndexes(String row, int beginIndex, int endIndex) {
        ArrayList rowIndexes = new ArrayList();
        ArrayList compareArray = new ArrayList();
        
        for (int i = 0; i < getRowCount(); i++) {
            compareArray.clear();
            for (int j = beginIndex; j < endIndex; j++) {
                compareArray.add(getValueAt(i,j));
            }
            
            if (row.equals(compareArray.toString())) {
                rowIndexes.add(new Integer(i));
            }            
        } 
       
        return rowIndexes;
    }
    
    public C30TableModel findRows(ArrayList row, int offset) {
        return findRows(row, offset, false, null);
    }
    
    public C30TableModel findRows(ArrayList row, int offset, boolean useWildcard, Object wildcardObject) {
        C30TableModel results = new C30TableModel();
        results.setColumnNames((ArrayList) this.getColumnNames().clone());
        
        for (int i = 0; i < data.size(); i++) {
            boolean match = true;
            for (int j = 0; j < row.size(); j++) {
                Object value = getValueAt(i, j + offset);
                
                if (!useWildcard ||
                    (useWildcard && wildcardObject == null && row.get(j) != null) ||
                    (useWildcard && wildcardObject != null && row.get(j) != null && !wildcardObject.equals(row.get(j)))) {
                    
                    if (value == null && row.get(j) != null) {
                        match = false;
                        break;
                    } else if (value != null && row.get(j) == null) {
                        match = false;
                        break;
                    } else if (value != null && !value.equals(row.get(j))) {
                        match = false;
                        break;
                    }
                }
            }
            if (match) {
                results.addRow((ArrayList) this.getRowAt(i).clone());
            }
        }
        
        return results;
    }
    
    public int getColumnCount() {
        return (data.size() > 0) ? ((ArrayList) data.get(0)).size() : 0;
    }
    
    public int getColumnIndex(String columnName) {
        return columnNames.indexOf(columnName);
    }

    public String getColumnName(int columnIndex) {
	return (String) columnNames.get(columnIndex);
    }
    
    public ArrayList getColumnNames() {
        return columnNames;
    }

    public int getRowCount() {
        return data.size();
    }
    
    public ArrayList getRowAt(int rowIndex) {
        return (ArrayList) data.get(rowIndex);
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return ((ArrayList) data.get(rowIndex)).get(columnIndex);
    }
    
    public ArrayList removeRow(int rowIndex) {
        ArrayList removed = (ArrayList) data.remove(rowIndex);
        this.fireTableRowsDeleted(rowIndex, rowIndex);
        return removed;
    }
    
    public Class getColumnClass(int columnIndex) {
        Class columnClass = super.getColumnClass(columnIndex);
        Object value = getValueAt(0, columnIndex);
        if (value != null) {
            columnClass = value.getClass();
        }
        return columnClass;
    }
    
    public void setColumnNames(ArrayList columnNames) {
        this.columnNames.clear();
        this.columnNames = columnNames;
        this.fireTableStructureChanged();
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ((ArrayList) data.get(rowIndex)).set(columnIndex, aValue);
        this.fireTableCellUpdated(rowIndex, columnIndex);
    }
    
}

    

