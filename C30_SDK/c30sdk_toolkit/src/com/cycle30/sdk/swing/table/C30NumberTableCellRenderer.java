/*
 * C30NumberTableCellRenderer.java
 *
 * Created on March 22, 2006, 10:31 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.swing.table;

import java.text.NumberFormat;

import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author reej04
 */
public class C30NumberTableCellRenderer extends DefaultTableCellRenderer {
    
    private NumberFormat numberFormat;
    
    /**
     * Creates a new instance of C30NumberTableCellRenderer 
     */
    public C30NumberTableCellRenderer(NumberFormat numberFormat) {
        super();
        this.numberFormat = numberFormat;
    }
    
    public void setValue(Object value) {     
        super.setValue(value);
        if (value instanceof Number) {
            setText((value == null) ? "" : numberFormat.format(value));
        }
    }
    
}
