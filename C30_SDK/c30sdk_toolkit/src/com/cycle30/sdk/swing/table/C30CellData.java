/*
 * C30CellData.java
 *
 * Created on March 4, 2006, 12:48 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.swing.table;

/**
 *
 * @author reej04
 */
public class C30CellData {
    private String value;
    private String previousValue;
    
    /** Creates a new instance of C30CellData */
    public C30CellData(String value) {
        this.value = value;
        previousValue = new String();
    }
    
    public C30CellData(String value, String previousValue) {
        this.value = value;
        this.previousValue = previousValue;
    }
    
    public String toString() {
        return value;
    }
    
    public String getPreviousValue() {
        return previousValue;
    }
    
    public void setPriviousValue(String previousValue) {
        this.previousValue = previousValue;
    }
    
}
