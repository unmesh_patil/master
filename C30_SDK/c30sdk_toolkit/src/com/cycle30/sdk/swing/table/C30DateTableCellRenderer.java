/*
 * C30DateTableCellRenderer.java
 *
 * Created on March 21, 2006, 11:04 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.swing.table;

import java.text.DateFormat;
import java.util.Date;

import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author reej04
 */
public class C30DateTableCellRenderer extends DefaultTableCellRenderer {
    
    private DateFormat dateFormat;
    
    /** Creates a new instance of C30DateTableCellRenderer */
    public C30DateTableCellRenderer(DateFormat dateFormat) {
        super();
        this.dateFormat = dateFormat;
    }
    
    public void setValue(Object value) {     
        super.setValue(value);
        if (value instanceof Date) {
            setText((value == null) ? "" : dateFormat.format(value));
        }
    }
    
}
