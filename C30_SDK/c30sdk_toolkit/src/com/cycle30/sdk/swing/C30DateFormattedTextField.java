/*
 * C30DateFormattedTextField.java
 *
 * Created on March 5, 2006, 5:56 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.cycle30.sdk.swing;

import java.text.SimpleDateFormat;
import javax.swing.JFormattedTextField;

/**
 * This class represents a JFormattedTextField that contains a date.
 * @author John Reeves
 */
public class C30DateFormattedTextField extends JFormattedTextField {
    
    /**
     * The default date format to use if one is not supplied.
     */
    private static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";
    
    /** Creates a new instance of C30DateFormattedTextField */
    public C30DateFormattedTextField() {
        this(DEFAULT_DATE_FORMAT);
    }
    
    /**
     * Creates a new instance of C30DateFormattedTextField
     * @param dateFormat the date format to use for the JFormattedTextField
     */
    public C30DateFormattedTextField(String dateFormat) {
        super(new SimpleDateFormat(dateFormat));
        setToolTipText(dateFormat.toLowerCase());
    }
}
