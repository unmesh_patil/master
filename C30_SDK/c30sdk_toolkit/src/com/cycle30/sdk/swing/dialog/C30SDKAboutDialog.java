/*
 * C30SDKAboutDialog.java
 *
 * Created on April 21, 2006, 11:14 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing.dialog;

import java.awt.Component;
import java.awt.Font;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * A class representing an about dialog used by custom GUI applications
 * @author John Reeves
 */
public class C30SDKAboutDialog {
    
    /**
     * Creates a new instance of C30SDKAboutDialog
     */
    private C30SDKAboutDialog() {}
    
    /**
     * Displays an about dialog that shows the application name, version
     * and build number as well as the version of the PSCustomUtils library.
     * @param parentComponent the parent component for the about dialog
     * @param applicationName name of application displaying the about dialog
     * @param version version of application displaying the about dialog
     * @param build build number of application displaying the about dialog
     */
    public static void showAboutDialog(Component parentComponent, String applicationName, String version, String build) {
        ImageIcon icon = new ImageIcon(ClassLoader.getSystemResource("com/comverse/cons/images/comverse_logo.gif"));

        JLabel[] labels = new JLabel[4];
        
        //------------------------
        // application name label
        //------------------------
        labels[0] = new JLabel();
        labels[0].setText(applicationName);
        labels[0].setFont(labels[0].getFont().deriveFont(Font.BOLD, 14f));
        
        //-------------------------
        // version and build label
        //-------------------------
        labels[1] = new JLabel();
        labels[1].setText("Version: " + version + "   Patch: " + build);
        labels[1].setFont(labels[1].getFont().deriveFont(12f));
        
        //---------------------
        // PSCustomUtils label
        //---------------------
        labels[2] = new JLabel();
        labels[2].setText("PSCustomUtils v1.3");
        labels[2].setFont(labels[1].getFont().deriveFont(9f));
        
        //-----------------
        // copyright label
        //-----------------
        labels[3] = new JLabel();
        labels[3].setText("Copyright, Comverse \u00a9 2006");
        labels[3].setFont(labels[1].getFont().deriveFont(9f));
        
        //--------------------------
        // display the about dialog
        //--------------------------
        JOptionPane.showMessageDialog(parentComponent, labels, "About " + applicationName, JOptionPane.INFORMATION_MESSAGE, icon);        
    }
    
}
