/*
 * C30SDKLoginDialog.java
 *
 * Created on April 10, 2006, 2:45 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing.dialog;

import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * A class representing a login dialog used by custom GUI applications
 * @author John Reeves
 */
public class C30SDKLoginDialog {
    /**
     * string values for login buttons
     */
    static String[] ConnectOptionNames = { "Login", "Cancel" };
    
    /**
     * title of login dialog
     */
    private String title;
    
    /**
     * driver for JDBC connection
     */
    private String databaseDriver;
    
    /**
     * URL for JDBC connection
     */
    private String databaseUrl;
    
    /**
     * database username
     */
    private String username;
    
    /**
     * database password
     */
    private String password;
    
    /**
     * Creates a new instance of C30SDKLoginDialog
     * @param title title of login dialog
     * @param databaseDriver driver for JDBC connection
     * @param databaseUrl URL for JDBC connection
     * @param username database username
     * @param password database password
     */
    public C30SDKLoginDialog(String title, String databaseDriver, String databaseUrl, String username, String password) {
        this.title = title;
        this.databaseDriver = databaseDriver;
        this.databaseUrl = databaseUrl;
        this.username = username;
        this.password = password;
        
        showDialog();
    }
        

    /**
     * modal dialog to get user ID and password
     */
    void showDialog() {
        JPanel loginPanel;

        //-----------------------------------
 	// create the labels and text fields
        //-----------------------------------
        JLabel labelDriver = new JLabel("Driver: ");
        JLabel labelDatabase = new JLabel("Database: ");
        JLabel labelUsername = new JLabel("Username: ");
        JLabel labelPassword = new JLabel("Password: ");
        JTextField textDriver = new JTextField(databaseDriver);
        JTextField textDatabaseUrl = new JTextField(databaseUrl);
        JTextField textUsername = new JTextField(username);
        JPasswordField textPassword = new JPasswordField(password);

	loginPanel = new JPanel(false);
	loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.X_AXIS));
	
        JPanel labelPanel = new JPanel(false);
	labelPanel.setLayout(new GridLayout(0, 1));
	labelPanel.add(labelUsername);
	labelPanel.add(labelPassword);
        labelPanel.add(labelDatabase);
	labelPanel.add(labelDriver);
        
	JPanel textPanel = new JPanel(false);
	textPanel.setLayout(new GridLayout(0, 1));
	textPanel.add(textUsername);
	textPanel.add(textPassword);
        textPanel.add(textDatabaseUrl);
        textPanel.add(textDriver);
	
        loginPanel.add(labelPanel);
	loginPanel.add(textPanel);

        //-----------------
        // connect or quit
        //-----------------
	if(JOptionPane.showOptionDialog(null, loginPanel, 
                                        title,
                                        JOptionPane.OK_CANCEL_OPTION, 
                                        JOptionPane.INFORMATION_MESSAGE,
                                        null, ConnectOptionNames, 
                                        ConnectOptionNames[0]) != 0) {
	    System.exit(0);
	} else {
            databaseDriver = textDriver.getText();
            databaseUrl = textDatabaseUrl.getText();
            username = textUsername.getText();
            password = new String(textPassword.getPassword());
        }
    }
    
    /**
     * gets the JDBC driver
     * @return driver for JDBC connection
     */
    public String getDatabaseDriver() {
        return databaseDriver;
    }
    
    /**
     * gets the JDBC URL
     * @return URL for JDBC connection
     */
    public String getDatabaseUrl() {
        return databaseUrl;
    }
    
    /**
     * gets the database password
     * @return database password
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * gets the database username
     * @return database username
     */
    public String getUsername() {
        return username;
    }
}

