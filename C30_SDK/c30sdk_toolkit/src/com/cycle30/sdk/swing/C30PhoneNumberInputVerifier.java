/*
 * C30PhoneNumberInputVerifier.java
 *
 * Created on August 22, 2006, 2:25 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing;

import java.awt.Toolkit;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;

import com.cycle30.sdk.util.C30StringUtils;

/**
 * This class is an InputValidator that validates that the input is a
 * valid phone number. The actual validation occurs in the C30StringUtils.validPhoneNumber()
 * method because the phone number validation is used by other objects
 * that initiate phone number validation (ex - C30PhoneNumberDocument)
 * @author John Reeves
 */
public class C30PhoneNumberInputVerifier extends InputVerifier {
    
    /** Creates a new instance of C30PhoneNumberInputVerifier */
    public C30PhoneNumberInputVerifier() {
    }
    
    /**
     * Checks whether the JComponent's input is a valid phone number.
     * It returns a boolean indicating the status of the argument's input.
     * @param input the JComponent to verify 
     * @return true when valid, false when invalid
     */
    public boolean verify(JComponent input) {
        boolean result = false;
        
        if (input instanceof JTextComponent) {
            result = C30StringUtils.validPhoneNumber(((JTextComponent) input).getText(), 10, 10);
            if (!result) Toolkit.getDefaultToolkit().beep();
        }
        
        return result;
    }
}
