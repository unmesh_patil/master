/*
 * C30SDKSearchPanel.java
 *
 * Created on May 23, 2006, 10:07 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing.panel;

import java.awt.Cursor;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

import com.cycle30.sdk.swing.C30PhoneNumberInputVerifier;
import com.cycle30.sdk.swing.event.C30SearchPanelEvent;
import com.cycle30.sdk.swing.event.C30SearchPanelListener;
import com.cycle30.sdk.swing.table.C30TableModel;
import com.cycle30.sdk.swing.table.C30TableSorter;
import com.cycle30.sdk.swing.text.C30PhoneNumberDocument;
import com.cycle30.sdk.swing.util.C30SDKSwingUtils;
import com.cycle30.sdk.datasource.C30TableDataSource;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30NameValuePair;

/**
 * A generic search panel that can be added to custom GUI applications.
 * The search panel dynamically populates the input fields based on
 * the input parameters in the PSExternalCall.
 * @author John Reeves
 */
public class C30SDKSearchPanel extends javax.swing.JPanel implements ListSelectionListener {
    
    /**
     * The default date format for date fields on the search panel.
     */
    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    
    /**
     * Name of search panel instance.  This can be used if a listener is
     * listening to multiple search panels to distinguish which search panel
     * generated the event.
     */
    private String panelName;
    
    /**
     * The data source to search the data from.
     */
    private C30TableDataSource dataSource;
    
    /**
     * The external call used to search the data.
     */
    private C30ExternalCallDef externalCall;
    
    /**
     * The panel containing all of the search input fields.
     */
    private JPanel panelInputFields;
    
    /**
     * The number of columns of input fields.
     */
    private int numColumns;
    
    /**
     * The width of the input fields.
     */
    private int inputFieldWidth;
    
    /**
     * The bitmask that indicates which input fields should be
     * displayed on the search panel. <br>
     * 1 = display the first field <br>
     * 2 = display the second field <br>
     * 4 = display the third field <br>
     * 8 = display the fourth field <br>
     * 11 = display the first, second and fourth fields <br>
     */
    private int showInputFieldBitmap;
    
    /**
     * Boolean to indicate if at least one input parameter is required for the search.
     */
    private boolean requireInputParams;
    
    /**
     * List of labels for the input fields.
     */
    private ArrayList inputLabels;
    
    /**
     * List of input field components.
     */
    private ArrayList inputFields;
    private JButton buttonSearch;
    
    /**
     * The bitmask that indicates which output fields should be
     * displayed in the result table. <br>
     * 1 = display the first field <br>
     * 2 = display the second field <br>
     * 4 = display the third field <br>
     * 8 = display the fourth field <br>
     * 11 = display the first, second and fourth fields <br>
     */    
    private int showOutputColumnBitmap;
    
    /**
     * The table of results from the search.
     */
    private JTable tableResults;
    
    /**
     * Sorting object used to sort the results table.
     */
    private C30TableSorter c30TableSorter;
    
    /**
     * The JPanel of action buttons at the bottom of the search panel.
     */
    private JPanel panelActionButtons;
     
    /**
     * The bitmask that indicates which action buttons should be
     * disabled in the action button panel. <br>
     * 1 = disable the first button <br>
     * 2 = disable the second button <br>
     * 4 = disable the third button <br>
     * 8 = disable the fourth button <br>
     * 11 = disable the first, second and fourth buttons <br>
     */ 
    private int disableButtonBitmap;
    
    /**
     * List of the names of the action buttons.
     */
    private String[] actionButtonNames;
    
    /**
     * List of references to the action buttons.
     */
    private ArrayList actionButtons;
    
    /**
     * List of listeners to notify when a search panel event occurs.
     */
    protected EventListenerList listenerList;
    
    /**
     * Creates a new instance of C30SDKSearchPanel
     * @param dataSource the data source to search the data from
     * @param externalCall the external call used to search the data
     */
    public C30SDKSearchPanel(C30TableDataSource dataSource, C30ExternalCallDef externalCall) {
        this(dataSource, externalCall, Integer.MAX_VALUE);
    }
    
    /**
     * Creates a new instance of C30SDKSearchPanel
     * @param dataSource the data source to search the data from
     * @param externalCall the external call used to search the data
     * @param showInputFieldBitmap bitmap indicating which input fields to display
     */
    public C30SDKSearchPanel(C30TableDataSource dataSource, C30ExternalCallDef externalCall, int showInputFieldBitmap) {
        this(dataSource, externalCall, new String[] { "OK", "Cancel" }, showInputFieldBitmap );
    }
    
    /**
     * Creates a new instance of C30SDKSearchPanel
     * @param dataSource the data source to search the data from
     * @param externalCall the external call used to search the data
     * @param actionButtonNames list of names of action buttons at bottom of search panel
     */
    public C30SDKSearchPanel(C30TableDataSource dataSource, C30ExternalCallDef externalCall, String[] actionButtonNames) {
        this(dataSource, externalCall, actionButtonNames, Integer.MAX_VALUE);
    }
    
    /**
     * Creates a new instance of C30SDKSearchPanel
     * @param dataSource the data source to search the data from
     * @param externalCall the external call used to search the data
     * @param actionButtonNames list of names of action buttons at bottom of search panel
     * @param showInputFieldBitmap bitmap indicating which input fields to display
     */
    public C30SDKSearchPanel(C30TableDataSource dataSource, C30ExternalCallDef externalCall, String[] actionButtonNames, int showInputFieldBitmap) {
        this.dataSource = dataSource;
        this.externalCall = externalCall;
        this.actionButtonNames = actionButtonNames;
        this.showInputFieldBitmap = showInputFieldBitmap;
        
        inputLabels = new ArrayList();
        inputFields = new ArrayList();
        actionButtons = new ArrayList();
        listenerList = new EventListenerList();
        
        requireInputParams = false;
        
        //-------------------------------------------------
        // default is to show all output columns and only
        // enable action buttons when a result is selected
        //-------------------------------------------------
        showOutputColumnBitmap = Integer.MAX_VALUE;
        disableButtonBitmap = Integer.MAX_VALUE;
        
        inputFieldWidth = 125;
        numColumns = 2;
        initComponents();
    }
    
    /**
     * Adds a listener to the list that's notified each time a change to the
     * search panel occurs.
     * @param l the C30SearchPanelListener
     */
    public void addSearchPanelListener(C30SearchPanelListener l) {
	listenerList.add(C30SearchPanelListener.class, l);
    }
    
    /**
     * Removes a listener from the list that's notified each time a change to the
     * search panel occurs.
     * @param l the C30SearchPanelListener
     */
    public void removeRateChangeListener(C30SearchPanelListener l) {
	listenerList.remove(C30SearchPanelListener.class, l);
    }
    
    /**
     * Notifies all listeners that a search panel has changed.
     * @param e the event to be forwarded
     */
    public void fireSearchPanelChanged(C30SearchPanelEvent e) {
	//---------------------------------------
        // Guaranteed to return a non-null array
        //---------------------------------------
	Object[] listeners = listenerList.getListenerList();
	
        //------------------------------------------------
        // Process the listeners last to first, notifying
	// those that are interested in this event
        //------------------------------------------------
	for (int i = listeners.length-2; i>=0; i-=2) {
	    if (listeners[i]==C30SearchPanelListener.class) {
		((C30SearchPanelListener) listeners[i+1]).searchPanelChanged(e);
	    }
	}
    }

    /**
     * Called when the results table changes.
     * @param e the event that characterizes the change
     */
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {
            if (((ListSelectionModel) e.getSource()).getMinSelectionIndex() == -1) {
                //----------------------------------------------------------
                // alert all of the listeners that the results were cleared
                //----------------------------------------------------------
                disableActionButtons();
                fireSearchPanelChanged(new C30SearchPanelEvent(this, C30SearchPanelEvent.RESULTS_CLEARED));                                               
            } else {
                //-------------------------------------------------------
                // alert all of the listeners that a result was selected
                //-------------------------------------------------------
                enableActionButtons();
                fireSearchPanelChanged(new C30SearchPanelEvent(this, C30SearchPanelEvent.RESULT_SELECTED));                
            }
        }
    }
    
    /**
     * Clears the result table and resets the input fields.
     */
    public void clearResults() {
        //---------------------------------------------------------
        // loop through all the input fields and clear the results
        //---------------------------------------------------------
        for (int i = 0; i < inputFields.size(); i++) {
            JComponent inputField = (JComponent) inputFields.get(i);
            
            if ((inputField) instanceof JFormattedTextField) {
                ((JFormattedTextField) inputField).setValue(null);
            } else if (inputField instanceof JTextField) {
                ((JTextField) inputField).setText(null);
            } else if (inputField instanceof JComboBox) {
                ((JComboBox) inputField).setSelectedIndex(0);
            }
        }
        
        //-------------------------
        // clears the result table
        //-------------------------
        if (tableResults.getModel().getRowCount() > 0) {
            ((C30TableModel) ((C30TableSorter) tableResults.getModel()).getTableModel()).clear();
        }        
    }
    
    /**
     * Disables all the action buttons at the bottom of the search panel.
     */
    public void disableActionButtons() {
        //------------------------------------------------------
        // loop through all the action buttons and disable them
        //-------------------------------------------------------
        for (int i = 0; i < actionButtons.size(); i++) {
            if ( (disableButtonBitmap & (int) Math.pow(2, i)) == (int) Math.pow(2, i) &&
                 !((JButton) actionButtons.get(i)).getActionCommand().equals("Cancel") ) {
                ((JButton) actionButtons.get(i)).setEnabled(false);
            }
        }
    }
    
    /**
     * Enables all the action buttons at the bottom of the search panel.
     */
    public void enableActionButtons() {
        //-----------------------------------------------------
        // loop through all the action buttons and enable them
        //-----------------------------------------------------
        for (int i = 0; i < actionButtons.size(); i++) {
            ((JButton) actionButtons.get(i)).setEnabled(true);
        }
    }
    
    /**
     * Returns the name of the panel.
     * @return the name of the panel
     */
    public String getPanelName() {
        return panelName;
    }
    
    /**
     * Returns the data in the selected row or null if no row is selected.
     * @return the array list of the row that is selected
     */
    public ArrayList getSelectedRow() {
        ArrayList selectedRow;        

        //----------------------------------------
        // get the view index of the selected row
        //----------------------------------------
        int viewIndex = tableResults.getSelectedRow();
        if (viewIndex == -1) {
            selectedRow = null;
        } else {
            //------------------------------------------------------------------
            // convert the view index to the model index to account for sorting
            //------------------------------------------------------------------
            int modelIndex = c30TableSorter.modelIndex(viewIndex);
            selectedRow = ((C30TableModel) ((C30TableSorter) tableResults.getModel()).getTableModel()).getRowAt(modelIndex);
        }
        
        return selectedRow;
    }
   
    /**
     * Repaints the results table.
     */
    public void repaintResults() {
        tableResults.repaint();
    }
    
    /**
     * Perform the search by using the parameters from the input fields on the
     * search panel.  It will only use the parameters from the input fields that
     * are displayed, which is controlled by the <CODE>showInputFieldBitmap</CODE>
     * variable.
     * @throws java.lang.Exception if the search could not be completed successfully
     */
    public void search() throws Exception {
        int paramValueCount = 0;
        int inputFieldIndex = 0;
        ArrayList paramValues = new ArrayList();

        for (int i = 0; i < externalCall.getInputParams().size(); i++) {
            //-----------------------------------------------------------------------
            // if the input parameter is displayed, get the value from the component
            //-----------------------------------------------------------------------
            if ((showInputFieldBitmap & (int) Math.pow(2, i)) == (int) Math.pow(2, i)) {
                Object paramValue = null;
                
                JComponent inputField = (JComponent) inputFields.get(inputFieldIndex++);
                
                if (inputField instanceof JFormattedTextField) {
                    paramValue = ((JFormattedTextField) inputField).getValue();
                
                } else if (inputField instanceof JTextField) {
                    if (externalCall.getInputParam(i).getParamType() == C30CustomParamDef.PHONE_NUMBER) {
                        //------------------------------------------------------------------
                        // verify that the phone number entered is a valid phone number
                        //
                        // Note - The inputVerifier is instantiated here and not associated
                        // with the JTextField using the setInputVerifier() method when the
                        // JTextField was instantiated because of the issues InputVerifier
                        // has with menus and combo boxes.  It will transfer focus to these
                        // components even if the input is not valid.  (see
                        // http://java.sun.com/developer/JDCTechTips/2001/tt1120.html for
                        // more information.)  So, we are just going to validate the inpt
                        // at the end when the search button is pressed and throw up a
                        // message box if the phone number is not valid.
                        //------------------------------------------------------------------
                        C30PhoneNumberInputVerifier inputVerifier = new C30PhoneNumberInputVerifier();
                        if (!inputVerifier.verify(inputField)) {
                            throw new Exception("Invalid Phone Number: " +
                                                ((JTextField) inputField).getText() +
                                                "\nPhone Number must be 10 digits or contain a wildcard.");
                        }
                    }
                    paramValue = ((JTextField) inputField).getText();
                
                } else if (inputField instanceof JComboBox) {
                    C30NameValuePair pair = (C30NameValuePair) ((JComboBox) inputField).getSelectedItem();
                    if (pair.getName().equals("-1")) {
                        paramValue = null;
                    } else {
                        paramValue = ((String) pair.getName());
                    }
                }
                paramValues.add(paramValue);
                if (paramValue != null && !paramValue.equals("")) {
                    paramValueCount++;
                }
            } else {
                paramValues.add(null);
            }
        }

        //--------------------------------------------
        // make sure at least one input parameter is
        // set if the panel requires input parameters
        //--------------------------------------------
        if (paramValueCount == 0 && requireInputParams) {
            String message = "You must enter at least one search parameter.";
            JOptionPane.showMessageDialog(null, message, "Input Required", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        //---------------------------------------------
        // confirm that the search should be performed
        // with no input parameters
        //---------------------------------------------
        if (paramValueCount == 0) {
            String message = "No search parameters were entered.  Do you want to continue?";
            if (JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(null, message,
                                                "Warning", JOptionPane.YES_NO_OPTION,
                                                JOptionPane.WARNING_MESSAGE)) {
                return;
            }
        }
        
        //---------------------------------------
        // make the external call with the param
        // values supplied from the input fields
        //---------------------------------------
        search(paramValues);
            
    }
    
    /**
     * Perform the search by using the input parameters passed to the function.
     * @param paramValues the input parameter values for the search
     * @throws java.lang.Exception if the search could not be completed successfully
     */
    public void search(Properties paramValues) throws Exception {        
        ArrayList params = new ArrayList();
        for (int i = 0; i < externalCall.getNumInputParams(); i++) {
            params.add(paramValues.getProperty(externalCall.getInputParam(i).getParamName()));
        }
        
        search(params);
    }
    
    /**
     * Perform the search by using the input parameters passed to the function.
     * @param paramValues the input parameter values for the search
     * @throws java.lang.Exception if the search could not be completed successfully
     */
    public void search(ArrayList paramValues) throws Exception {        
        dataSource.queryData(externalCall, paramValues, C30TableDataSource.DISPLAY_VALUES);
        c30TableSorter.setTableModel(dataSource.getTableModel());

        //----------------------------------------------------------------
        // only display the columns indicated in the output column bitmap
        //----------------------------------------------------------------
        for (int i = 0; i < externalCall.getOutputParams().size(); i++) {
            if (!((showOutputColumnBitmap & (int) Math.pow(2, i)) == (int) Math.pow(2, i))) {
                int columnIndex = C30SDKSwingUtils.toView(tableResults, i);
                TableColumn removedColumn = tableResults.getColumnModel().getColumn(columnIndex);
                tableResults.getColumnModel().removeColumn(removedColumn);
            }
        }

        //-----------------------------------------
        // if only one result was found, select it
        //-----------------------------------------
        if (tableResults.getModel().getRowCount() == 1) {
            tableResults.getSelectionModel().setSelectionInterval(0, 0);
        }        
    }
    
    /**
     * Sets if the panel with the action buttons is displayed on the search panel.
     * @param actionButtonPanelVisible boolean to indicate if the action button panel is visible
     */
    public void setActionButtonPanelVisible(boolean actionButtonPanelVisible) {
        panelActionButtons.setVisible(actionButtonPanelVisible);
    }
    
    /**
     * Sets the bitmask that indicates which output fields should be
     * displayed in the result table. <br>
     * (ex - 1 = display the first field) <br>
     * (ex - 2 = display the second field) <br>
     * (ex - 4 = display the third field) <br>
     * (ex - 8 = display the fourth field) <br>
     * (ex - 11 = display the first, second and fourth fields) <br>
     * (etc.)
     * @param bitmap the bitmap to indicate which output columns are displayed
     */   
    public void setDisplayOutputColumnBitmap(int bitmap) {
        showOutputColumnBitmap = bitmap;
    }

    /**
     * Sets the bitmask that indicates which action buttons should be
     * disabled in the action button panel. <br>
     * (ex - 1 = disable the first button) <br>
     * (ex - 2 = disable the second button) <br>
     * (ex - 4 = disable the third button) <br>
     * (ex - 8 = disable the fourth button) <br>
     * (ex - 11 = disable the first, second and fourth buttons) <br>
     * (etc.)
     * @param bitmap the bitmap to indicate which output columns are displayed
     */   
    public void setDisableButtonBitmap(int bitmap) {
        disableButtonBitmap = bitmap;
        
        //----------------------------------------------
        // reset the action buttons with the new bitmap
        //----------------------------------------------
        enableActionButtons();
        disableActionButtons();
    }
    
    /**
     * Sets if the panel with the input fields is displayed on the search panel.
     * @param inputFieldsPanelVisible boolean to indicate if the panel should be displayed
     */
    public void setInputFieldsPanelVisible(boolean inputFieldsPanelVisible) {
        panelInputFields.setVisible(inputFieldsPanelVisible);
    }
    
    /**
     * Sets the width of the input fields.
     * @param width the width of the input fields in pixels
     */
    public void setInputWidth(int width) {
        inputFieldWidth = width;
        for (int i = 0; i < inputFields.size(); i++) {
            ((JComponent) inputFields.get(i)).setPreferredSize(new Dimension(inputFieldWidth, 21));
        }
    }
    
    /**
     * Sets the name of search panel instance.  This can be used if a listener is
     * listening to multiple search panels to distinguish which search panel
     * generated the event.
     * @param panelName the name of the search panel
     */
    public void setPanelName(String panelName) {
        this.panelName = panelName;
    }
    
    /*public void setPhoneNumberMask(String mask) throws Exception {
        //--------------------------------------------------
        // loop through all the input fields and change the
        // mask for every C30PhoneNumberFormattedTextField
        //--------------------------------------------------
        for (int i = 0; i < inputFields.size(); i++) {
            Object inputField = inputFields.get(i);
            if (inputField instanceof C30PhoneNumberFormattedTextField) {
                ((C30PhoneNumberFormattedTextField) inputField).setMask(mask);
            }
        }
    }*/
    
    /**
     * Sets the boolean to indicate if at least one input parameter is required for
     * the search.
     * @param requireInputParams boolean to indicate if at least one input parameter is required
     */
    public void setRequireInputParams(boolean requireInputParams) {
        this.requireInputParams = requireInputParams;
    }
    
    public void setSearchButtonAsDefault() {
        if (getRootPane() != null) {
            getRootPane().setDefaultButton(buttonSearch);        
        }
    }
        
    /**
     * Initializes the components on the different panels of the search screen.
     */
    private void initComponents() {
        setLayout(new BorderLayout());
        ((BorderLayout) this.getLayout()).setVgap(10);
        
        setBorder(new EmptyBorder(1, 5, 5, 5));
        
        createInputPanel();
        createResultsPanel();
        createActionButtonsPanel();   
    }
    
    /**
     * Creates the panel that contains the input fields, search button
     * and reset button.
     */
    private void createInputPanel() {
        ArrayList panelColumns = new ArrayList();
    
        panelInputFields = new JPanel();
        panelInputFields.setLayout(new BorderLayout());
        panelInputFields.setBorder(new TitledBorder("Search Criteria"));
        
        JPanel panelSearchFields = new JPanel();
        panelSearchFields.setLayout(new BoxLayout(panelSearchFields, BoxLayout.X_AXIS));
        
        //------------------------------------------
        // add the input fields to the search panel
        //------------------------------------------
        int numParams = externalCall.getNumInputParams();
        JPanel panelLabelColumn = null;
        JPanel panelInputColumn = null;
        
        for (int i = 0; i < numParams; i++) {
            //--------------------------------------------------------------------
            // only add input fields that are specified in the input field bitmap
            //--------------------------------------------------------------------
            if ((showInputFieldBitmap & (int) Math.pow(2, i)) == (int) Math.pow(2, i)) {
                
                //----------------------------------
                // add a new panel column if needed
                //----------------------------------
                if (i % ((numParams / numColumns) + (numParams % numColumns)) == 0) {
                    JPanel panelColumn = new JPanel();
                    panelColumn.setAlignmentY(Component.TOP_ALIGNMENT);

                    //------------------------------------
                    // add the panel for the field labels
                    //------------------------------------
                    panelLabelColumn = new JPanel();
                    panelLabelColumn.setLayout(new BoxLayout(panelLabelColumn, BoxLayout.Y_AXIS));
                    panelColumn.add(panelLabelColumn);

                    //------------------------------------
                    // add the panel for the input fields
                    //------------------------------------
                    panelInputColumn = new JPanel();
                    panelInputColumn.setLayout(new BoxLayout(panelInputColumn, BoxLayout.Y_AXIS));
                    panelColumn.add(panelInputColumn);

                    panelSearchFields.add(panelColumn);               
                    panelColumns.add(panelColumn);
                }

                C30CustomParamDef param = externalCall.getInputParam(i);
                
                //-----------------------------------------
                // add the label to the appropriate column
                //-----------------------------------------
                JLabel label = new JLabel(param.getDisplayName() + ":");
                inputLabels.add(label);
                panelLabelColumn.add(label);
                panelLabelColumn.add(Box.createRigidArea(new Dimension(0, 13)));

                //-----------------------------------------------------------
                // add the text field or combo box to the appropriate column
                //-----------------------------------------------------------
                JComponent inputField = null;
                if (param.getParamValueChoices().size() == 0) {
                    if (param.getParamType() == C30CustomParamDef.PHONE_NUMBER) {
                        inputField = new JTextField();
                        ((JTextField) inputField).setDocument(new C30PhoneNumberDocument());
                    } else 
                    	
                    if (param.getParamType() == C30CustomParamDef.DATE) {
                        inputField = new JFormattedTextField(DEFAULT_DATE_FORMAT);
                        inputField.setToolTipText(DEFAULT_DATE_FORMAT.toPattern());
                    } else {
                        inputField = new JTextField();
                    }
                } else {
                    inputField = new JComboBox(param.getParamValueChoices().toArray());
                }
                
                inputFields.add(inputField);
                inputField.setPreferredSize(new Dimension(inputFieldWidth, 21));
                panelInputColumn.add(inputField);
                panelInputColumn.add(Box.createRigidArea(new Dimension(0, 7)));  
            }
        }
        
        panelInputFields.add(panelSearchFields, BorderLayout.CENTER);
        
        //---------------------------------------------------
        // add the search buttons to the bottom of the panel
        //---------------------------------------------------
        JPanel panelSearchButtons = new JPanel();
        buttonSearch = new JButton("Search");
        buttonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSearchActionPerformed(evt);
            }
        });
        
        JButton buttonReset = new JButton("Reset");
        buttonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonResetActionPerformed(evt);
            }
        });
        
        panelSearchButtons.add(buttonSearch);
        panelSearchButtons.add(buttonReset);
        panelInputFields.add(panelSearchButtons, BorderLayout.SOUTH);
               
        add(panelInputFields, BorderLayout.NORTH); 
    }
    
    /**
     * Creates the panel that contains the search results.
     */
    private void createResultsPanel() {
        //--------------------------
        // create the results table
        //--------------------------
        JScrollPane scrollPaneResults = new JScrollPane();
        tableResults = new JTable();
        tableResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableResults.getSelectionModel().addListSelectionListener(this);
        scrollPaneResults.setViewportView(tableResults);
        
        //----------------------------------
        // add sorting to the results table
        //----------------------------------
        c30TableSorter = new C30TableSorter();
        tableResults.setModel(c30TableSorter);
        c30TableSorter.setTableHeader(tableResults.getTableHeader());
                       
        add(scrollPaneResults, BorderLayout.CENTER);
    }
    
    /**
     * Creates the panel that contains the action buttons at the bottom of
     * the search panel.
     */
    private void createActionButtonsPanel() {
        panelActionButtons = new JPanel();
        
        for (int i = 0; i < actionButtonNames.length; i++) {
            JButton actionButton = new JButton(actionButtonNames[i]);
            actionButton.setActionCommand(actionButtonNames[i]);
            actionButton.setPreferredSize(new Dimension(75, 25));
            actionButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    buttonActionPerformed(evt);
                }
            });
            
            panelActionButtons.add(actionButton);
            actionButtons.add(actionButton);
        }
        
        disableActionButtons();
        
        add(panelActionButtons, BorderLayout.SOUTH);         
    }
    
    /**
     * Method called when the search button is pressed.  This method will call the
     * <CODE>search()</CODE> method to perform the actual search.
     * @param evt ActionEvent generated from pressing the search button
     */
    private void buttonSearchActionPerformed(java.awt.event.ActionEvent evt) {                                             
        try {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            search();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }
    
    /**
     * Method called when the reset button is pressed.
     * @param evt ActionEvent generated from pressing the reset button
     */
    private void buttonResetActionPerformed(java.awt.event.ActionEvent evt) {
        clearResults();
    }
    
    /**
     * Method called when an action button is pressed.
     * @param evt ActionEvent generated from pressing on of the action buttons
     */
    private void buttonActionPerformed(java.awt.event.ActionEvent evt) {
        C30SearchPanelEvent event = new C30SearchPanelEvent(this, C30SearchPanelEvent.ACTION_REQUESTED, evt.getActionCommand());
        fireSearchPanelChanged(event);    
    }
    

    
}
