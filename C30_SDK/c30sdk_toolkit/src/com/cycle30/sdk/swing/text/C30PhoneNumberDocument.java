/*
 * C30PhoneNumberDocument.java
 *
 * Created on August 22, 2006, 3:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing.text;

import java.awt.Toolkit;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.cycle30.sdk.util.C30StringUtils;

/**
 * This is a subclass of the PlainDocument (used to hold a JTextComponent's text
 * data) designed specifically for phone numbers.  If a JTextComponent (such as
 * a JTextField) uses an C30PhoneNumberDocument instead of the standard PlainDocument,
 * it will validate that the phone number is a valid number when the text data is
 * entered.  This class also accomodates cut and paste operations into the
 * JTextComponent.
 * @author John Reeves
 */
public class C30PhoneNumberDocument extends PlainDocument {
    
    /** Creates a new instance of C30PhoneNumberDocument */
    public C30PhoneNumberDocument() {
    }
    
    /**
     * Overwritten method to insert the phone number into the document.  Before
     * the data is written to the document by the super class, the phone number
     * is checked to see if it is valid or else the document is not changed.
     * @param offset the starting offset >= 0
     * @param string the string to insert; does nothing with null/empty strings
     * @param attributes the attributes for the inserted content 
     * @throws javax.swing.text.BadLocationException the given insert position is not a valid position within the document
     */
    public void insertString(int offset, String string, AttributeSet attributes) throws BadLocationException {
        if (string == null) {
            return;
        } else {
            String newValue;
            int length = getLength();
            
            if (length == 0) {
                newValue = string;
            } else {
                //--------------------------------------------------------------------
                // determine the complete new string including what was already there
                //--------------------------------------------------------------------
                String currentContent = getText(0, length);
                StringBuffer currentBuffer = new StringBuffer(currentContent);
                currentBuffer.insert(offset, string);
                newValue = currentBuffer.toString();
            }
            
            //-----------------------------------------------------------
            // make sure the phone number is valid before it is inserted
            //-----------------------------------------------------------
            if (C30StringUtils.validPhoneNumber(newValue, 0, 10)) {
                super.insertString(offset, string, attributes);
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        }
    }

    
}
