/*
 * C30SearchPanelListener.java
 *
 * Created on March 17, 2006, 10:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing.event;

/**
 * C30SearchPanelListener defines the interface for an object that listens to changes in an C30SDKSearchPanel.
 * @author John Reeves
 */
public interface C30SearchPanelListener extends java.util.EventListener {
    /**
     * Called whenever the search panel changes to notify the listeners.
     * @param e the event that characterizes the change
     */
    public void searchPanelChanged(C30SearchPanelEvent e);
}
