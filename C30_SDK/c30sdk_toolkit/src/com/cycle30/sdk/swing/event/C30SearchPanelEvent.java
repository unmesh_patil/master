/*
 * C30SearchPanelEvent.java
 *
 * Created on March 17, 2006, 11:21 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.cycle30.sdk.swing.event;

import com.cycle30.sdk.swing.panel.C30SDKSearchPanel;

/**
 * C30SearchPanelEvent is used to notify listeners that a search panel has
 * changed.  Changes to the search panel include:<br>
 *  - A result from the search has been selected<br>
 *  - The result set has been cleared<br>
 *  - An action button at the bottom of the screen has been pressed<br>
 * @author John Reeves
 */
public class C30SearchPanelEvent extends java.util.EventObject {
    
    /**
     * The event type when a result is selected.
     */
    public final static int RESULT_SELECTED = 1;
    
    /**
     * The event type when the results list is cleared.
     */
    public final static int RESULTS_CLEARED = 2;
    
    /**
     * The event type when an action button is pressed.
     */
    public final static int ACTION_REQUESTED = 3;
    
    /**
     * The type of event.
     */
    protected int type;
    
    /**
     * The JButton's action command from the action button pressed.
     */
    protected String actionCommand;
    
    /**
     * Creates a new instance of C30SearchPanelEvent
     * @param source the C30SDKSearchPanel that created the event
     * @param type the type of search panel event
     */    
    public C30SearchPanelEvent(C30SDKSearchPanel source, int type) {
        this(source, type, null);
    }
    
    /**
     * Creates a new instance of C30SearchPanelEvent
     * @param source the C30SDKSearchPanel that created the event
     * @param type the type of search panel event
     * @param actionCommand the action command from the action button that was selected
     */
    public C30SearchPanelEvent(C30SDKSearchPanel source, int type, String actionCommand) {
        super(source);
        this.type = type;
        this.actionCommand = actionCommand;
    }
    
    /**
     * Returns the JButton.getActionCommand() result from the action button
     * pressed.
     * @return the action command from the action button pressed on the C30SDKSearchPanel
     */
    public String getActionCommand() {
        return actionCommand;
    }
    
    /**
     * Returns the type of event
     * @return the type of event
     */
    public int getType() {
        return type;
    }
 
}
