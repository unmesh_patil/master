/*
 * C30SDKFrameworkFactory.java
 *
 * Created on December 7, 2006, 10:21 AM
 *
 */

package com.cycle30.sdk.core.framework;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;


import com.cycle30.sdk.C30SDKToolkitLatestBuildVersion;
import com.cycle30.sdk.cache.C30SDKCache;
import com.cycle30.sdk.cache.C30SDKCacheManager;
import com.cycle30.sdk.cache.C30SDKCacheableWrapper;
import com.cycle30.sdk.config.C30KenanConfigManager;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.attribute.C30SDKAttributeControl;
import com.cycle30.sdk.core.attribute.C30SDKEnumeration;
import com.cycle30.sdk.core.attribute.C30SDKLoadOrderComparator;
import com.cycle30.sdk.core.attribute.C30SDKNameComparator;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30KenanMWDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKSqlConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.cycle30.order.C30SDKOrderManager;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30VersioningUtils;

// API-TS imports
import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.BSDMSettings;
import com.csgsystems.aruba.connection.Connection;
import com.csgsystems.aruba.connection.ConnectionFactory;
import com.csgsystems.aruba.connection.ServiceException;
import com.csgsystems.bali.connection.ApiMappings;
import com.csgsystems.fx.security.SecurityManager;
import com.csgsystems.fx.security.SecurityManagerFactory;
import com.csgsystems.fx.security.util.AuthenticationException;
import com.csgsystems.fx.security.util.FxException;



/**
 * The factory class used to create any C30SDK objects.  There is only ever one factory instantiated
 * per application that is creating C30SDK object.
 *
 * The typical steps to building an application that uses C30SDK objects are as follows:
 * <BR><BR>
 * <B>Step 1:</B> Instantiate a C30SDKFrameworkFactory<BR>
 * <CODE>C30SDKFrameworkFactory factory = new C30SDKFrameworkFactory("CSGFx", "arborsv", "12345", "password", "FX2.0/PS2.0");
 * <BR><BR>    <B>OR</B><BR> <BR>
 * C30SDKUser user  = new C30SDKUser( "arborsv", "kenanfx","KenanFx"); <br>
 * C30SDKFrameworkFactory factory = new C30SDKFrameworkFactory( "FX2.0/PS2.0", user);  <br>
 </CODE><BR><BR>
 * <B>Step 2:</B> Create a C30SDK object using the factory<BR>
 * <CODE>
 *     C30SDKObject lwo = factory.createC30SDKObject("New Wireless Service");<BR><BR>
 * </CODE>
 * <B>Step 3:</B> Set the required attributes.<BR>
 * <CODE>
 * lwo.setNameValuePair("Account Number", "120010470");<BR>
 * lwo.setNameValuePair("Component", "54002");<BR>
 * order.setNameValuePair("Rate Class", "3001");<BR><BR>
 * </CODE>
 * <B>Step 4:</B> Process the object.<BR>
 * <CODE>
 * C30SDKObject response = lwo.process();<BR>
 * System.out.println("New Order ID = "+response.getAttribute(C30Order.ATTRIB_ORDER_ID));<BR><BR>
 * </CODE>
 * @author John Reeves, Ranjith Nelluri
 */
public class C30SDKFrameworkFactory {

	/**
	 * The logging variable.
	 */
	private static Logger log = Logger.getLogger(C30SDKFrameworkFactory.class);
	public C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	C30SDKPropertyFileManager KenanExceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	private Integer connectionMode = C30SDKValueConstants.CONNECTION_MODE_NEW;


	// API-TS properties
	private ConnectionFactory kenanFactory;
	private Connection kenanConnection;
	private BSDMSessionContext kenanContext;
	private BSDMSettings kenanBsdmSettings;
	//

	private Integer kenanServerId;
	private SecurityManager kenanSecMgr;
	private String kenanSecRealm;


	//==================================================
	// All the following variables deal with profiling
	// within the framework.
	//==================================================
	private Map profileAttributes = new HashMap();
	private String version = "FX1.0/PS1.0";
	/**
	 * This is used to know as if the reconnection attempt to KenanFX is alreay made or not. 
	 */
	private int reConnectAttempt=0;
	private HashMap c30SdkTypes = new HashMap();

	/** Object which holds raw hashmap data from extended data assoc list get request. */
	private Object[] extDataAssocList;

	/** Object which holds raw hashmap data from extended data params get request. */
	private Object[] extDataParamsList;

	/** Object which holds light weight attributes for all extended data.  The hashmap stores lists of
	 * attributes based on the table and type of extended data. So, for instance:
	 * Key = PRODUCT_VIEW_18025 would return all extended data attributes for products with type = 18025
	 * Key = ORD_SERVICE_ORDER_20 would return all extended data attributes for service order with type = 20 */
	private HashMap extDataList = new HashMap();

	/** Object which holds all database object configuration.  The hashmap stores the associated information about
	 * each object configuration as well as the attributes that the object may be preconfigured with.
	 */
	private HashMap objectConfiguration = new HashMap();

	/** Variable which holds the UI controls that have been configured in the database which could be used while
	 * creating light weight objects. */
	private List controls = new ArrayList();

	/** Object which holds all database object attribute configuration.  The hashmap stores the input and output attributes
	 * that the object may be preconfigured with.
	 */
	private HashMap classAttribConfiguration = new HashMap();

	/** Object which holds all database object attribute filtering configuration.  The hashmap stores the output attribute
	 * filtering that is applied to an object when it is instantiated.
	 */
	private HashMap objectFieldOutputFilter = new HashMap();

	/** Object which holds all enumeration configuration.  The hashmap stores the 
	 * Enumeration config.
	 */
	private HashMap objectEnumeration = new HashMap();
	public HashMap classIdConfig = new HashMap();
	public ArrayList<OrderWorkPointMap> orderWorkPointMapList = new ArrayList();

	/**
	 * Stores the external payment/Tender Type to Kenan/Comverse Payment mapping. 
	 */
	private HashMap paymentConfig = new HashMap();

	/**
	 * Username of Kenan Security Server used for Authentication
	 */
	private C30SDKUser c30sdkUser;
	/**
	 * Stores the cache related to c30SDK Framework.
	 */
	public C30SDKCache <String, C30SDKCacheableWrapper<String, Object>> c30SdkFactoryCache ;


	/**
	 * Creates a new instance of C30SDKFrameworkFactory. This constructor is intended for use with
	 * applications that already have a MW connection.  In this scenario, the MW connection is
	 * borrowed.  Thus no new connection is established.  An example of an application that may
	 * use this constructor is Customer Center.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware
	 * @throws C30SDKObjectException if there was a problem initializing the factory
	 * @throws C30SDKInvalidConfigurationException 
	 * @throws C30SDKInvalidAttributeException 
	 * @throws C30SDKKenanFxCoreException 
	 */

	public C30SDKFrameworkFactory(final Double fxVersion, final Double psVersion)
			throws C30SDKException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {
		populateConfigFiles();
		loadInitCache();
		loadOrderManager();
		this.version = fxVersion.toString()+"/"+psVersion.toString();

		// If a TUX connection is being borrowed, we indicate this by
		// instantiating a MW Datasource with no input params.
		this.connectionMode = C30SDKValueConstants.CONNECTION_MODE_BORROW;

		//========================================================================
		// The next thing we do is to initialize the factory attributes given the
		// input version that is to be used for this factory instance. The profile
		// includes versioning which tells the framework what functionality should
		// be processed.
		//========================================================================
		initializeAttributes("");
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION, fxVersion);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PS_VERSION, psVersion);

		//===========================================================================
		// Then we go out and get all extended data that can be associated to any
		// API-TS object.  Each time an object is instantiated it should perform the
		// this.loadExtendedDataAttributes(String tableBaseName, Integer entityType)
		// method.  This will ensure that all extended data values can be correctly
		// populated as necessary.
		//===========================================================================
		populateClassIdConfiguration();
		//populateExtendedDataAttributes();
		populateExtendedDataAttributesJDBC();
		populateObjectGenericEnumerations();
		populateObjectEnumerationInits();
		populateObjectEnumerations();
		populateOrderWorkPointMap();
		populatePaymentConfiguration(((Double)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION)).toString());

	}

	/*
	 * This adds all the property files to the C30SDKPropertyFileManager.
	 */
	private void populateConfigFiles() 
	{

		try
		{
			exceptionResourceBundle.addFromClassPathAsaResourceBundle(C30SDKValueConstants.RESOURCE_C30_ERROR_TEXT_FILE_NAME);
			exceptionResourceBundle.addFromClassPathAsaResourceBundle(C30SDKValueConstants.RESOURCE_C30_KENAN_ERROR_TEXT_FILE_NAME);
		} 
		catch (java.util.MissingResourceException ex) 
		{
			if( log != null ) 
			{ 
				log.debug("Error during the Property File Loading."); 
			}
		} 
		catch (FileNotFoundException e) 
		{
			log.debug("Error during the Property File Loading.");
			log.error(e);
		} 
		catch (IOException e) 
		{
			log.debug("Error during the Property File Loading.");
			log.error(e);
		}
	}


	/**
	 * Constructor to create new connections.  
	 * 
	 * @param userID
	 * @param ver
	 * @throws C30SDKException
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidConfigurationException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKKenanFxCoreException
	 */
	public C30SDKFrameworkFactory( String userID, String ver)
			throws C30SDKException,C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException
			{
		log.error("Latest C30SDK Build Version : "+C30SDKToolkitLatestBuildVersion.LATEST_BUILD_VERSION + " and C30SDK Version : " + ver);

		connectionMode = C30SDKValueConstants.CONNECTION_MODE_NEW;
		profileAttributes = new HashMap();
		version = "FX1.0/PS1.0";
		c30SdkTypes = new HashMap();
		extDataList = new HashMap();
		objectConfiguration = new HashMap();
		classAttribConfiguration = new HashMap();
		objectFieldOutputFilter = new HashMap();
		controls = new ArrayList();
		version = ver;

		// Create Kenan APITS/Tuxedo connection
		kenanBsdmSettings = BSDMSettings.getDefault();
		kenanContext = BSDMSessionContext.getDefaultContext();

		try  {
			kenanFactory = ConnectionFactory.instance();
			kenanConnection = kenanFactory.createConnection(kenanBsdmSettings);

			ResourceBundle sdkProperties = C30SDKDataSourceUtils.getC30SdkToolkitProperties();
			String user = sdkProperties.getString("kenan.userid");
			this.kenanSecRealm = sdkProperties.getString("kenan.realm");
			String pwd = sdkProperties.getString("kenan.passwd");

			// Decrypt the password
			String password = C30SDKDataSourceUtils.decryptPassword(pwd);


			log.debug("Authenticating Kenan user" + user);
			SecurityManager sm = authenticateUser(user, password, null);
			kenanContext.setSecurityContext(sm);

			log.debug("=====================================================================");
			log.debug("API-TS Connection successful!");
			log.debug("=====================================================================");
			kenanContext.setBlockSize(10000);

		}
		catch(Exception e)
		{
			log.error(e);
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-001"), e, "CONN-001");
		}

		populateConfigFiles();
		loadInitCache();
		loadOrderManager();
		populateClassIdConfiguration();
		populateC30SDKClassAttribConfiguration();        
		populateObjectConfiguration();
		populateObjectFieldOutputFilter();
		initializeAttributes(userID);
		populateExtendedDataAttributesJDBC();
		//populateExtendedDataAttributes();
		populateObjectGenericEnumerations();
		populateObjectEnumerationInits();
		populateObjectEnumerations();
		populateOrderWorkPointMap();
		populatePaymentConfiguration(((Double)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION)).toString());

			}



	/**
	 * Creates a new instance of C30SDKFrameworkFactory.  This is the most common constructor used outside
	 * of a container that holds middleware sessions.  For small, simple applications, this is the
	 * constructor that should be used.
	 * @param userRealm Kenan/FX middleware user realm
	 * @param userID Kenan/FX middleware user id
	 * @param sessionID Kenan/FX middleware session id
	 * @param password Kenan/FX middleware password
	 * @param ver the pre-configured LWO version that the factory should use. (Please ask the configuration team what this value should be)
	 * @throws C30SDKObjectException if there was a problem initializing the factory
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware
	 * @throws C30SDKInvalidConfigurationException if there was a problem loading the factory configuration during construction.
	 * @throws C30SDKInvalidAttributeException if there was invalid attribute information loading the factory configuration during construction.
	 * @throws C30SDKKenanFxCoreException 
	 */
	public C30SDKFrameworkFactory(final String userRealm, final String userID, final String sessionID, final String password, final String ver) 
			throws C30SDKException,C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
		log.error("Latest C30SDK Build Version : "+C30SDKToolkitLatestBuildVersion.LATEST_BUILD_VERSION + " and C30SDK Version : " + ver);


		this.version = ver;
		this.kenanSecRealm = userRealm;

		populateConfigFiles();
		loadInitCache();
		loadOrderManager();
		populateClassIdConfiguration();
		//=============================================================
		// The first thing we need to do after setting up the connection
		// factory is get the object configuration.  By getting the
		// configuration up front it means we do not need to make a call
		// to the database for every object.
		//=============================================================
		populateObjectConfiguration();


		//=============================================================
		// The next we need to load any C30SDK object 
		// attribute configuration.  Not every C30SDK object will have
		// this attribute configuration.
		//=============================================================
		populateC30SDKClassAttribConfiguration();


		//=============================================================
		// The next we need to load any C30SDK object  
		// attribute filtering configuration.  Not every C30SDK object will have
		// this filtering configuration.
		//=============================================================
		populateObjectFieldOutputFilter();


		//========================================================================
		// The next thing we do is to initialize the factory attributes given the
		// input version that is to be used for this factory instance. The profile
		// includes versioning which tells the framework what functionality should
		// be processed.
		//========================================================================
		initializeAttributes(userID);

		//===========================================================================
		// Then we go out and get all extended data that can be associated to any
		// API-TS object.  Each time an object is instantiated it should perform the
		// this.loadExtendedDataAttributes(String tableBaseName, Integer entityType)
		// method.  This will ensure that all extended data values can be correctly
		// populated as necessary.
		//===========================================================================
		//populateExtendedDataAttributes();
		populateExtendedDataAttributesJDBC();

		//===================================================================
		//Populating the Enumeratoin Data.
		//		
		//===================================================================
		populateObjectGenericEnumerations();
		populateObjectEnumerationInits();
		populateObjectEnumerations();
		populateOrderWorkPointMap();
		populatePaymentConfiguration(((Double)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION)).toString());

		if (((Double) getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION)).compareTo(C30VersioningUtils.API_VERSION_1_0) > 0) {
			populateUIControls();
		}
	}

	/**
	 * Creates a new instance of C30SDKFrameworkFactory.  This is the most common constructor used outside
	 * of a container that holds middleware sessions.  For small, simple applications, this is the
	 * constructor that should be used.
	 *
	 * @param user Kenan/FX middleware user id
	 * @param ver the pre-configured LWO version that the factory should use. (Please ask the configuration team what this value should be)
	 * @throws C30SDKObjectException if there was a problem initializing the factory
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware
	 * @throws C30SDKInvalidConfigurationException if there was a problem loading the factory configuration during construction.
	 * @throws C30SDKInvalidAttributeException if there was invalid attribute information loading the factory configuration during construction.
	 * @throws C30SDKKenanFxCoreException 
	 */
	public C30SDKFrameworkFactory( final String ver, C30SDKUser user) throws C30SDKException,C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

		log.error("Latest C30SDK Build Version : "+C30SDKToolkitLatestBuildVersion.LATEST_BUILD_VERSION + " and C30SDK Version : " + ver);

		this.version = ver;
		this.kenanSecRealm = user.getUserRealm();
		//===================================================================
		// The first thing we need to do is get the BSDM default settings
		// and then create the context using those settings.  These settings
		// typically come from configuration files in the classpath.
		//===================================================================

		// Tuxedo/Security server settings
		kenanBsdmSettings = BSDMSettings.getDefault();
		kenanContext = BSDMSessionContext.getDefaultContext();

		this.c30sdkUser = user;
		populateConfigFiles();
		loadInitCache();
		loadOrderManager();
		populateClassIdConfiguration();

		//=============================================================
		// The first thing we need to do after setting up the connection
		// factory is get the object configuration.  By getting the
		// configuration up front it means we do not need to make a call
		// to the database for every object.
		//=============================================================
		populateObjectConfiguration();


		//=============================================================
		// The next we need to load any C30SDK object 
		// attribute configuration.  Not every C30SDK object will have
		// this attribute configuration.
		//=============================================================
		populateC30SDKClassAttribConfiguration();


		//=============================================================
		// The next we need to load any C30SDK object  
		// attribute filtering configuration.  Not every C30SDK object will have
		// this filtering configuration.
		//=============================================================
		populateObjectFieldOutputFilter();


		//========================================================================
		// The next thing we do is to initialize the factory attributes given the
		// input version that is to be used for this factory instance. The profile
		// includes versioning which tells the framework what functionality should
		// be processed.
		//========================================================================
		initializeAttributes(user.getUserId());

		//===========================================================================
		// Then we go out and get all extended data that can be associated to any
		// API-TS object.  Each time an object is instantiated it should perform the
		// this.loadExtendedDataAttributes(String tableBaseName, Integer entityType)
		// method.  This will ensure that all extended data values can be correctly
		// populated as necessary.
		//===========================================================================
		//populateExtendedDataAttributes();
		populateExtendedDataAttributesJDBC();


		//===================================================================
		//Populating the Enumeratoin Data.		
		//===================================================================
		populateObjectGenericEnumerations();
		populateObjectEnumerationInits();
		populateObjectEnumerations();
		populateOrderWorkPointMap();
		populatePaymentConfiguration(((Double)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION)).toString());

		if (((Double) getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION)).compareTo(C30VersioningUtils.API_VERSION_1_0) > 0) {
			populateUIControls();
		}
	}

	/**
	 * This loads the cache manager and manages all the cache files.
	 * @throws C30SDKException
	 */
	private void loadInitCache() throws C30SDKException 
	{
		C30SDKCacheManager cacheManager=C30SDKCacheManager.getInstance();
		c30SdkFactoryCache= cacheManager.createCache();
		//cacheManager.createCache();
		C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>("Cache1","Object1");
		c30SdkFactoryCache.set(cacheWrapper);

		//Removing Tuxedo and security server from C30SDK Framework.
		//Kenan Config manager will be used later..
		C30KenanConfigManager kenanConfig =C30KenanConfigManager.getInstance(getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID));
	}

	private void loadOrderManager() throws C30SDKException 
	{
		C30SDKOrderManager orderManager=C30SDKOrderManager.getInstance();
	}

	/*
	 * Populating the object Enumerations
	 */
	private void populateObjectEnumerations()  throws C30SDKObjectConnectionException, C30SDKObjectException {
		// Populate all the objects from the GenericEnumerations using Generuc_enumeration_Ref and values.
		// Using Enumeration Find call.

		log.debug("Populating Defaults from C30_SDK_OBJECT_ENUMERATION ...");

		C30JDBCDataSource dataSource = getFrameworkDataSourceConnectionFromSDKPool();    
		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_enum_data");

		String resultArr[] = new String[8];

		try
		{
			call.addParam(new C30CustomParamDef("enumeration_id", 1, 7, 2));
			call.addParam(new C30CustomParamDef("enumeration_name", 2, 30, 2));
			call.addParam(new C30CustomParamDef("attribute_name", 2, 30, 2));
			call.addParam(new C30CustomParamDef("attribute_key_column", 2, 30, 2));
			call.addParam(new C30CustomParamDef("attribute_value_column", 2, 30, 2));
			call.addParam(new C30CustomParamDef("table_name", 2, 30, 2));
			call.addParam(new C30CustomParamDef("apits_object_name", 2, 30, 2));
			call.addParam(new C30CustomParamDef("dbms_query", 2, 2000, 2));


			dataSource.queryData(call, new ArrayList(), 2);
			log.debug("Total row count enumerations = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{
				ObjectConfiguration object = null;
				String previousObject = "";
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{


					for (int j = 0; j < resultArr.length; j++ )
						if (dataSource.getValueAt(i,j)!= null)
							resultArr[j]= dataSource.getValueAt(i,j).toString();
						else
							resultArr[j]= "";

					// Removing references to APITS/Tuxedo
					//String apiTSObjectName =  resultArr[6].trim();
					String dbmsQuery =  resultArr[7].trim();


					try {
						//--------------------------------------
						// Fetch all Extended Data Associations
						//--------------------------------------
						/*String callName = apiTSObjectName +"Find";
						C30KenanMWDataSource dataSource1 = getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
						HashMap dataFilter = new HashMap();
						dataFilter.put("Fetch", new Boolean(true));

						Map request = new HashMap();
						request.put(apiTSObjectName, dataFilter);

						Map callResponse = dataSource1.queryData(ApiMappings.getCallName(callName), request);

						Object enumList[] = (Object[])(Object[]) callResponse.get(apiTSObjectName+"List");
						for (int j = 0; j < enumList.length; j++)
						{ 
							HashMap enumData = (HashMap)enumList[j] ; 
							String enumValue = (String)enumData.get(resultArr[4].trim());
							String enumKey = resultArr[2].trim();
							HashMap key = (HashMap)enumData.get("Key");
							Object enumId = (Object)key.get(resultArr[3].trim());

							C30SDKEnumeration lwe = new C30SDKEnumeration(enumKey, enumValue,0, enumId.toString());
							this.objectEnumeration.put(enumKey+"_"+enumId.toString(),lwe);
						}
						 */


						C30JDBCDataSource dbmsDataSource =getJDBCDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
						//Get the Enumeration Values Count

						PreparedStatement ps = dbmsDataSource.getPreparedStatement(dbmsQuery);
						log.debug("Executing the enumeration query : " + dbmsQuery);
						ResultSet resultSet  = ps.executeQuery();

						while (resultSet.next()) {
							String enumValue =  resultSet.getString("DisplayValue");
							String enumKey =  resultArr[2].trim();
							Object enumId = (Object) resultSet.getString(resultArr[3].trim());

							C30SDKEnumeration lwe = new C30SDKEnumeration(enumKey, enumValue,0, enumId.toString());
							this.objectEnumeration.put(enumKey+"_"+enumId.toString(),lwe);
						}
						resultSet.close();

					} 
					/*catch (ServiceException e) {
						String error="";
						if (e.item instanceof HashMap) {
							error = error + ((HashMap) e.item).get("BSD_APPNAME");
							error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
						} else {
							error = error + e.item + "\n";
						}
						throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());

					} catch (FxException e) {
						throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
					} catch (IOException e) {
						throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
					} catch (terrapin.tuxedo.TuxError e) {
						String message = e.getMessage();
						String code[]=message.split("-");
						throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
					}*/
					catch (Exception e) {
						log.error(e);
						throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-009"), e, "CONN-027");
					}


				}     
			}


		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-010"), e, "CONN-010");
		}
		log.debug("Successfully populated Enumeration data from C30_SDK_OBJECT_ENUMERATION!");

	}


	/**
	 * 
	 * @param fxversion
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 */
	private void populatePaymentConfiguration(String fxversion)  throws C30SDKObjectConnectionException, C30SDKObjectException {
		// Populate all the objects from the GenericEnumerations using Generuc_enumeration_Ref and values.
		// Using Enumeration Find call.

		log.debug("Populating Defaults from c30_sdk_payment_config...");

		C30JDBCDataSource dataSource = getFrameworkDataSourceConnectionFromSDKPool();    
		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_payment_config");
		//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_objects");


		String resultArr[] = new String[2];

		try
		{
			call.addParam(new C30CustomParamDef("tender_type", 2, 300, 2));
			call.addParam(new C30CustomParamDef("bmf_trans_type", 1, 30, 2));
			call.addParam(new C30CustomParamDef("fxversion", 2, 200, 1));

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------
			paramValues.add(fxversion);

			dataSource.queryData(call, paramValues, 2);
			log.debug("Total row count payment configurations = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{
				ObjectConfiguration object = null;
				String previousObject = "";
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{	
					this.paymentConfig.put(dataSource.getValueAt(i,0).toString().toUpperCase(),dataSource.getValueAt(i,1).toString());
				}     
			}


		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-010"), e, "CONN-010");
		}
		log.debug("Successfully populated Enumeration data from c30_sdk_payment_config!");

	}

	/**
	 * This loads the WorkPointProcess and Order Type mapping.
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 */
	private void populateOrderWorkPointMap()  throws C30SDKObjectConnectionException, C30SDKObjectException {
		// Populate the Workpoint and Order Mapping.

		log.debug("Populating Defaults from c30_sdk_order_wp_map...");

		C30JDBCDataSource dataSource = getFrameworkDataSourceConnectionFromSDKPool();    
		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_order_wp_map");

		try
		{
			call.addParam(new C30CustomParamDef("order_type", 2, 300, 2));
			call.addParam(new C30CustomParamDef("acct_seg_id", 1, 30, 2));
			call.addParam(new C30CustomParamDef("is_service_level", 1, 30, 2));
			call.addParam(new C30CustomParamDef("emf_config_id", 1, 30, 2));
			call.addParam(new C30CustomParamDef("account_category", 1, 30, 2));
			call.addParam(new C30CustomParamDef("item_id", 2, 300, 2));
			call.addParam(new C30CustomParamDef("item_type", 2, 300, 2));
			call.addParam(new C30CustomParamDef("wp_process_id", 2, 300, 2));
			call.addParam(new C30CustomParamDef("active_date", 3, 30, 2));
			call.addParam(new C30CustomParamDef("inactive_date", 3, 30, 2));


			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values - NONE
			//--------------------------------

			dataSource.queryData(call, paramValues, 2);
			log.debug("Total row count Order Workpoint Map = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{	
					Integer acctSegId = null;
					if(dataSource.getValueAt(i,1)!= null) 
						acctSegId = ((BigDecimal)dataSource.getValueAt(i,1)).intValue();

					Integer isServiceLevel = null;
					if(dataSource.getValueAt(i,2)!= null) 
						isServiceLevel = ((BigDecimal)dataSource.getValueAt(i,2)).intValue();

					Integer emfConfigId = null;
					if(dataSource.getValueAt(i,3)!= null) 
						emfConfigId = ((BigDecimal)dataSource.getValueAt(i,3)).intValue();

					Integer accountCatrgory = null;
					if(dataSource.getValueAt(i,4)!= null) 
						accountCatrgory = ((BigDecimal)dataSource.getValueAt(i,4)).intValue();

					OrderWorkPointMap orderWpMap = new OrderWorkPointMap((String)dataSource.getValueAt(i,0),
							acctSegId,
							isServiceLevel,
							emfConfigId,
							accountCatrgory,
							(String)dataSource.getValueAt(i,5),
							(String)dataSource.getValueAt(i,6),
							(String)dataSource.getValueAt(i,7),
							(Date)dataSource.getValueAt(i,8),
							(Date)dataSource.getValueAt(i,9));
					orderWorkPointMapList.add(orderWpMap);
				}     
			}
		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-034"), e, "CONN-034");
		}
		
		log.debug("Successfully populated Enumeration data from c30_sdk_payment_config!");

	}
	/**
	 * Populating the ClassId Configuration
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 */
	private void populateClassIdConfiguration()  throws C30SDKObjectConnectionException, C30SDKObjectException {
		// Populate all the objects from the GenericEnumerations using Generuc_enumeration_Ref and values.
		// Using Enumeration Find call.

		log.debug("Populating Classid and ClassName  from c30_sdk_class_ref...");

		C30JDBCDataSource dataSource = getFrameworkDataSourceConnectionFromSDKPool();    
		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_class_id");



		String resultArr[] = new String[2];

		try
		{
			call.addParam(new C30CustomParamDef("class_name", 2, 300, 2));
			call.addParam(new C30CustomParamDef("class_id", 1, 30, 2));

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------

			dataSource.queryData(call, paramValues, 2);
			log.debug("Total row count class id config= " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{
				ObjectConfiguration object = null;
				String previousObject = "";
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{	
					this.classIdConfig.put(dataSource.getValueAt(i,1),dataSource.getValueAt(i,0).toString());
				}     
			}


		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-010"), e, "CONN-010");
		}
		log.debug("Successfully populated Enumeration data from c30_sdk_class_ref!");

	}
	/*
	 * Populating the object Enumerations
	 */
	private void populateObjectEnumerationInits()  throws C30SDKObjectConnectionException, C30SDKObjectException {
		// Populate all the objects from the GenericEnumerations using Generuc_enumeration_Ref and values.
		// Using Enumeration Find call.

		log.debug("Populating Defaults from c30_sdk_object_enum_inits...");

		C30JDBCDataSource dataSource = getFrameworkDataSourceConnectionFromSDKPool();    
		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_enum_inits");
		//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_objects");


		String resultArr[] = new String[3];

		try
		{
			call.addParam(new C30CustomParamDef("enum_key", 2, 30, 2));
			call.addParam(new C30CustomParamDef("enum_id", 2, 300, 2));
			call.addParam(new C30CustomParamDef("enum_display_value", 2, 2000, 2));

			dataSource.queryData(call, new ArrayList(), 2);
			log.debug("Total row count enum inits = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{
				ObjectConfiguration object = null;
				String previousObject = "";
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{


					for (int j = 0; j < resultArr.length; j++ )
						if (dataSource.getValueAt(i,j)!= null)
							resultArr[j]= dataSource.getValueAt(i,j).toString();
						else
							resultArr[j]= "";
					C30SDKEnumeration lwe = new C30SDKEnumeration(resultArr[0], resultArr[2],0, resultArr[1]);
					//this.objectEnumeration.add(lwe);
					this.objectEnumeration.put(resultArr[0]+"_"+resultArr[1].toString(),lwe);
				}     
			}


		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-010"), e, "CONN-010");
		}
		log.debug("Successfully populated Enumeration data from c30_sdk_object_enum_inits!");

	}

	/*
	 * Populating the object Enumerations
	 */
	private void populateObjectGenericEnumerations()  throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKKenanFxCoreException {
		// Populate all the objects from the GenericEnumerations using Generuc_enumeration_Ref and values.
		// Using Enumeration Find call.

		log.debug("Populating Defaults from Generic Enumerations Table ...");
		try {
			//--------------------------------------
			// Fetch all Enumerations Using Kenan MW DataConnection
			//--------------------------------------
			/*	C30KenanMWDataSource dataSource = getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
			HashMap dataFilter = new HashMap();
			dataFilter.put("Fetch", new Boolean(true));

			Map request = new HashMap();
			request.put("GenericEnumeration", dataFilter);

			Map callResponse = dataSource.queryData(ApiMappings.getCallName("GenericEnumerationFind"), request);

			Object enumList[] = (Object[])(Object[]) callResponse.get("GenericEnumerationList");
			for (int j = 0; j < enumList.length; j++)
			{ 
				HashMap enumData = (HashMap)enumList[j] ; 
				String enumValue = (String)enumData.get("DisplayValue");
				HashMap key = (HashMap)enumData.get("Key");
				String enumKey =  (String)key.get("EnumerationKey");
				String enumId =  (String)key.get("Value");
				C30SDKEnumeration lwe = new C30SDKEnumeration(enumKey, enumValue,0, enumId);
				this.objectEnumeration.put(enumKey+"_"+enumId.toString(),lwe);

			}
			 */
			//--------------------------------------
			// Fetch Data using Enumerations using JDBC
			//--------------------------------------

			C30JDBCDataSource jdbcdataSource =getJDBCDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
			//Get the ExtendedDataList Count
			String extendedDataCountQuery = C30SDKSqlConstants.QUERY_GET_GENERICENUMERAION_FIND_LIST;
			PreparedStatement ps = jdbcdataSource.getPreparedStatement(extendedDataCountQuery);
			ResultSet resultSet  = ps.executeQuery();

			while (resultSet.next()) {

				String enumValue =  resultSet.getString("DisplayValue");
				String enumKey = resultSet.getString("EnumerationKey"); 
				String enumId = resultSet.getString("Value");
				C30SDKEnumeration lwe = new C30SDKEnumeration(enumKey, enumValue,0, enumId);
				this.objectEnumeration.put(enumKey+"_"+enumId.toString(),lwe);
			}

			resultSet.close();


		} 
		/*catch (ServiceException e) {
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			//throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			if (e.getErrorCategory().equalsIgnoreCase("BakcEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}
			else if (e.getErrorCategory().equalsIgnoreCase("FrontEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorName()),e,e.getErrorName());	
			}
			else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}

		} catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
		} catch (IOException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		} catch (terrapin.tuxedo.TuxError e) {
			String message = e.getMessage();
			String code[]=message.split("-");
			if (code[0].trim().startsWith("TPE"))
			{
				//Reconnect and make the same call again.
				reconnectToMWDataSource();
				populateObjectGenericEnumerations();
			}else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
			}
		}
		 */
		catch (Exception e) {
			log.error(e);
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-009"), e, "CONN-027");
		}

		log.debug("Successfully populated Enumeration data from Generic Enumeration Ref/Values!");

	}

	/*
	 * This is used to Reconeect to Tuxedo incase of any TPE Exceptions or TuxErrors.
	 */
	public void reconnectToMWDataSource() throws C30SDKKenanFxCoreException {

		try {
			log.debug("Attempting Reconnection to Middleware");
			kenanBsdmSettings=BSDMSettings.getDefault();
			kenanFactory = ConnectionFactory.instance();
			kenanContext = BSDMSessionContext.getDefaultContext();
			kenanConnection = kenanFactory.createConnection(kenanBsdmSettings);

			// Get authentication details from .properties
			ResourceBundle sdkProperties = C30SDKDataSourceUtils.getC30SdkToolkitProperties();
			String user = sdkProperties.getString("kenan.userid");
			this.kenanSecRealm = sdkProperties.getString("kenan.realm");
			String pwd = sdkProperties.getString("kenan.passwd");

			// Decrypt the password
			String password = C30SDKDataSourceUtils.decryptPassword(pwd);

			log.debug("Authenticating Kenan user" + user);
			SecurityManager sm = authenticateUser(user, pwd, null);
			kenanContext.setSecurityContext(sm);

		}
		catch (ServiceException e) {
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			log.error(e);
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());

		}
		catch (terrapin.tuxedo.TuxError e) {
			log.error(e);
			String message = e.getMessage();
			String code[]=message.split("-");
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
		}
		catch (C30SDKObjectConnectionException e) {
			log.error(e);
			String message = e.getMessage();
			String code[]=message.split("-");
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
		}
		catch (Exception e) {
			log.error(e);
			String message = e.getMessage();
			throw new C30SDKKenanFxCoreException(message,e, "Error reconnecting to DataSource");
		}
	}

	/**
	 * this method creates a Security Manager, authenticates and returns that SecurityManager object.
	 * @param userID
	 * @param password
	 * @param sessionID
	 * @return
	 * @throws C30SDKKenanFxCoreException 
	 */
	private final SecurityManager authenticateUser(final String userID, final String password, String sessionID)
			throws  C30SDKObjectConnectionException, C30SDKKenanFxCoreException {
		SecurityManager userSM = null;

		try {
			if (sessionID == null || sessionID.trim().length() == 0) {
				sessionID = userID+generateUniqueSessionId();
			}

			//========================================================
			// authenticate the user with the middleware server.  Once authentication is done we
			// can create a security context and create a connection.
			//========================================================
			userSM = SecurityManagerFactory.createSecurityManager(this.kenanSecRealm, userID, sessionID, password);
			userSM.authenticate();  // enforced authentication
			SecurityManagerFactory.registerSecurityManager(userSM);  

		}
		catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()),e,e.getErrorCode());
		} 
		catch (Exception e) {
			String error = "Username: "+userID + "\n";
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-001") + error, e, "CONN-001");
		}
		return userSM;
	}




	/**
	 * Creates a new C30SDK user object containing an authenticated SecurityManager. 
	 * @param userId
	 * @param password
	 * @return
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKInvalidConfigurationException
	 */
	/*
	public final synchronized C30SDKUser createC30SDKUser(final String userId,final String password) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
		C30SDKUser lwUser = new C30SDKUser(userId,password);
		SecurityManager lwoSM = null;
		try{

			UUID sessionUUID = UUID.randomUUID();         
			lwoSM = SecurityManagerFactory.createSecurityManager(this.realm,userId, sessionUUID.toString(), password);       
			lwUser.setSecurityManager(lwoSM);
		}    
		catch (Exception e) 
		{
			String error = "Username: "+userId + "\n";
			//throw new  AuthenticationException(e.getMessage(), error);
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-002") + error, e, "CONN-002");
		}
		return lwUser;

	}
	 */


	private static String generateUniqueSessionId()
	{

		SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMddHHmmss0SSSS");
		String ss = formatter.format(new java.util.Date());
		while (ss.equals(formatter.format(new java.util.Date()))) {}
		return formatter.format(new java.util.Date());

	} 

	/**
	 * Method to get the data source created by the factory and used by the framework.  This method is for use
	 * by any objects that extend the C30SDKObject class and need to obtain information from middleware
	 * but are getting that data using a stored procedure call.
	 * @return the data source
	 */
	public final C30KenanMWDataSource getMwDataSource(Integer serverId) {

		C30KenanMWDataSource dataSource = null;
		Double apiVersion = null;
		try {
			apiVersion = ((Double) getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION));
		} catch (Exception e) {
			apiVersion = new Double("1.0");
		}

		//kenanContext.setServerId(serverId);
		kenanServerId = serverId;
		if (this.connectionMode.equals(C30SDKValueConstants.CONNECTION_MODE_NEW)) {
			dataSource = new C30KenanMWDataSource(kenanConnection, kenanContext, apiVersion);
		} else if (this.connectionMode.equals(C30SDKValueConstants.CONNECTION_MODE_BORROW)) {
			dataSource = new C30KenanMWDataSource(apiVersion);
		}
		return dataSource;
	}

	/**
	 * 
	 * @return bsdm context
	 */
	public BSDMSessionContext getContext() {
		return this.kenanContext;
	}

	/**
	 *    
	 * Method to get the data source created by the factory and used by the framework.  This method is for use
	 * by any objects that extend the C30SDKObject class and needs to obtain information from not available from the 
	 * middleware and not in the arbor schema,  getting that data using a stored procedure call or straight sql query.
	 * @return the data source
	 */
	public final C30JDBCDataSource getJDBCDataSource(Integer serverId) throws C30SDKObjectConnectionException
	{
		C30JDBCDataSource dataSource = null;
		// need to construct the connection and store it for re-use. 
		try{ 

			dataSource = C30SDKDataSourceUtils.createJDBCDataSource(serverId);

		}
		catch(Exception ex) {
			log.error("Exception: "+ex.getMessage());
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-004"), ex, "CONN-004");
		}
		return dataSource;
	}

	/**
	 *    
	 * Method to get the data source created by the factory and used by the framework.  This method is for use
	 * by any objects that extend the C30SDKObject class and needs to obtain information from not available from the 
	 * middleware and not in the arbor schema,  getting that data using a stored procedure call or straight sql query.
	 * @return the data source
	 */
	public final Integer getTotalCustomerServers() throws C30SDKObjectConnectionException
	{
		Integer totalCustomerServersCount;
		// need to construct the connection and store it for re-use. 
		try{ 

			totalCustomerServersCount = C30SDKDataSourceUtils.getTotalCustomerServers();

		}
		catch(Exception ex) {
			log.error("Exception: "+ex.getMessage());
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-004"), ex, "CONN-004");
		}
		return totalCustomerServersCount;
	}



	/**
	 *    
	 * Method to get the data source created by the factory and used by the framework.  This method is for use
	 * by any objects that extend the C30SDKObject class and needs to obtain information from not available from the 
	 * middleware and not in the arbor schema,  getting that data using a stored procedure call or straight sql query.
	 * This Returns the JDBC Connection Source to the LWO Config Database, Not the Kenan Databases
	 * @return the data source
	 */
	C30JDBCDataSource framworkDataSource = null;

	public final C30JDBCDataSource getFrameworkDataSourceConnectionFromSDKPool() throws C30SDKObjectConnectionException
	{
		// need to construct the connection and store it for re-use. 
		try{ 

			if (framworkDataSource == null)
				framworkDataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();

		}
		catch(Exception ex) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-004"), ex, "CONN-004");
		}
		return framworkDataSource;
	}
	
	/** Free Framework connection and return it to pool
	 * 
	 * @return
	 * @throws C30SDKObjectConnectionException
	 */
	public final void freeFrameworkDataSourceConnectionFromSDKPool() throws C30SDKObjectConnectionException
	{
		// need to construct the connection and store it for re-use. 
		try{ 

			if (framworkDataSource != null)
				C30SDKDataSourceUtils.freeConnectionFromSDKPool(framworkDataSource.getConnection());

		}
		catch(Exception ex) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-004"), ex, "CONN-004");
		}
	}

	/**
	 * expose the FX connection that starts the Factory.
	 * @return  Connection
	 */
	public  final Connection getConnection(){
		if (this.connectionMode.equals(C30SDKValueConstants.CONNECTION_MODE_NEW))
			return kenanConnection;
		else
			return null;
	}


	/**
	 * 
	 * @return server id
	 */
	public  final Integer getContextServerId(){
		return kenanContext.getServerId();
	}



	/**
	 * Creates a new C30SDK object given its object type.  The object type is that name given to the object
	 * in the database configuration. This is the most commonly used method to create C30SDK objects.  The
	 * factory configures the object using the predefined configuration that is associated with the given object type.
	 * 
	 * @param c30sdkObjectType Name of the type of C30SDK transaction to create
	 * @param userName
	 * @return the light weight object specified to be created.
	 * @throws C30SDKObjectException if there was a problem initializing the transaction
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the transaction
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the transaction.
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the object
	 */
	public final synchronized C30SDKObject createC30SDKObject(final String c30sdkObjectType, String userName) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
		//kenanContext.setOperatorName(userName);   
		return (createC30SDKObject(c30sdkObjectType));  
	}

	public final synchronized C30SDKObject createC30SDKObject( String c30sdkObjectType, Integer acctSegId) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
		C30SDKObject lwo = null;
		String className = "";
		String acctSegIdStr="";
		try {
			String orig_c30sdkObjectType =c30sdkObjectType;
			if (acctSegId==null) acctSegIdStr=""; else acctSegIdStr=acctSegId.toString();
			//Apend the Account Segment Id.
			c30sdkObjectType = c30sdkObjectType+acctSegIdStr;
			log.debug("Working on Object Name : "+ c30sdkObjectType);
			if (objectConfiguration.get(c30sdkObjectType) != null) {

				//get the objects class name using configuration
				className = ((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getClassName();

				//create the arguments for the object using the ID.
				Object[] args = {c30sdkObjectType};

				//create the class of the object using configuration.
				Class objClass = Class.forName(className);

				//create the configured object
				lwo = createC30SDKObject(objClass, args);
				//create the objects children.
				this.loadChildrenC30SDKObjects(lwo);

				lwo.setPreValidateClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getPreValidationClassName());
				lwo.setPostEnforcerClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getPostEnforcerClassName());
				lwo.setBrePostEnforcerClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getBrePostEnforcerClassName());
				lwo.setBrePreValidateClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getBrePreValidationClassName());
				lwo.setAcctSegId(acctSegId);
				//if we don't have a configuration with the Account SegmentId then 
				// Try with the defauly ane,
			} else if (objectConfiguration.get(orig_c30sdkObjectType) != null) {

				//get the objects class name using configuration
				className = ((ObjectConfiguration) objectConfiguration.get(orig_c30sdkObjectType)).getClassName();

				//create the arguments for the object using the ID.
				Object[] args = {orig_c30sdkObjectType};

				//create the class of the object using configuration.
				Class objClass = Class.forName(className);

				//create the configured object
				lwo = createC30SDKObject(objClass, args);
				//create the objects children.
				this.loadChildrenC30SDKObjects(lwo);

				lwo.setPreValidateClassId(((ObjectConfiguration) objectConfiguration.get(orig_c30sdkObjectType)).getPreValidationClassName());
				lwo.setPostEnforcerClassId(((ObjectConfiguration) objectConfiguration.get(orig_c30sdkObjectType)).getPostEnforcerClassName());
				lwo.setBrePostEnforcerClassId(((ObjectConfiguration) objectConfiguration.get(orig_c30sdkObjectType)).getBrePostEnforcerClassName());
				lwo.setBrePreValidateClassId(((ObjectConfiguration) objectConfiguration.get(orig_c30sdkObjectType)).getBrePreValidationClassName());
				//lwo.setAcctSegId(acctSegId);
				//if we don't have a configuration then throw an exception
			} 
			else {
				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-001") + c30sdkObjectType , "INVALID-CONF-001");
			}

		} catch (ClassNotFoundException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-002") + c30sdkObjectType +" / "+ className, "INVALID-CONF-002");
		}
		//lwo.setContext(context);
		//lwo.setUser(lwUser,kenanContext);
		return lwo;
	}



	public final synchronized C30SDKObject createC30SDKObject( String c30sdkObjectType, C30SDKUser lwUser, Integer acctSegId) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
		C30SDKObject lwo = null;
		String className = "";
		String contextUser = "";
		String acctSegIdStr="";
		try {

			contextUser = lwUser.getUserId();

			if (acctSegId==null) acctSegIdStr=""; else acctSegIdStr=acctSegId.toString();
			//Apend the Account Segment Id.
			c30sdkObjectType = c30sdkObjectType+acctSegIdStr;
			log.debug("Working on Object Name : "+ c30sdkObjectType);
			if (objectConfiguration.get(c30sdkObjectType) != null) {

				//get the objects class name using configuration
				className = ((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getClassName();

				//create the arguments for the object using the ID.
				Object[] args = {c30sdkObjectType};

				//create the class of the object using configuration.
				Class objClass = Class.forName(className);

				//create the configured object
				lwo = createC30SDKObject(objClass, args);
				//create the objects children.
				this.loadChildrenC30SDKObjects(lwo);

				lwo.setPreValidateClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getPreValidationClassName());
				lwo.setPostEnforcerClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getPostEnforcerClassName());
				lwo.setBrePostEnforcerClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getBrePostEnforcerClassName());
				lwo.setBrePreValidateClassId(((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getBrePreValidationClassName());

				//if we don't have a configuration then throw an exception
			} else {
				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-001") + c30sdkObjectType , "INVALID-CONF-001");
			}

		} catch (ClassNotFoundException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-002") + c30sdkObjectType +" / "+ className, "INVALID-CONF-002");
		}
		//lwo.setContext(context);
		//lwo.setUser(lwUser,kenanContext);
		return lwo;
	}



	/**
	 * Creates a new C30SDK object given its object type.  The object type is that name given to the object
	 * in the database configuration. This is the most commonly used method to create C30SDK objects.  The
	 * factory configures the object using the predefined configuration that is associated with the given object type.
	 * @param C30SDKObjectType Name of the type of C30SDK transaction to create
	 * @return the light weight object specified to be created.
	 * @throws C30SDKObjectException if there was a problem initializing the transaction
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the transaction
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the transaction.
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the object
	 */
	/* public final synchronized C30SDKObject createC30SDKObject(final String c30sdkObjectType, SecurityManager aSecurityManager) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
		C30SDKObject lwo = null;
		String className = "";
		String contextUser = "";
		try {

			if (aSecurityManager != null){
				// have requested this transaction with a own security manager so set the context accordingly
				kenanContext.setSecurityContext(aSecurityManager);
				log.debug("SecurityManger.ContextUser: "+aSecurityManager.getContextUser());
				contextUser = aSecurityManager.getContextUser().toString();
			}
			else{
				kenanContext.setSecurityContext(aSecurityManager);
				contextUser = this.kenanSecMgr.getContextUser().toString();
			}

			// set or re-set the context OperatorName
			if (contextUser.indexOf("@") > -1){
				String operatorName = contextUser.substring(0,contextUser.indexOf("@"));
				//kenanContext.setOperatorName(operatorName);
			}
			//log.debug("Context.OperatorName: "+kenanContext.getOperatorName());
			//if we have a configuration that matches the type
			if (objectConfiguration.get(c30sdkObjectType) != null) {

				//get the objects class name using configuration
				className = ((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getClassName();

				//create the arguments for the object using the ID.
				Object[] args = {c30sdkObjectType};

				//create the class of the object using configuration.
				Class objClass = Class.forName(className);

				//create the configured object
				lwo = createC30SDKObject(objClass, args);

				//create the objects children.
				this.loadChildrenC30SDKObjects(lwo);

				//if we don't have a configuration then throw an exception
			} else {
				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-001") + c30sdkObjectType , "INVALID-CONF-001");
			}

		} catch (ClassNotFoundException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-002") + c30sdkObjectType +" / "+ className, "INVALID-CONF-002");
		}

		return lwo;
	}

	 */


	/**
	 * Creates a new C30SDK object given its object type.  The object type is that name given to the object
	 * in the database configuration. This is the most commonly used method to create C30SDK objects.  The
	 * factory configures the object using the predefined configuration that is associated with the given object type.
	 * @param c30sdkObjectType Name of the type of C30SDK transaction to create
	 * @return the light weight object specified to be created.
	 * @throws C30SDKObjectException if there was a problem initializing the transaction
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the transaction
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the transaction.
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the object
	 */
	public final synchronized C30SDKObject createC30SDKObject(final String c30sdkObjectType) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
		C30SDKObject lwo = null;
		String className = "";
		try {

			// ensure that the Security Manager which established the Factory is set
			//context.setSecurityContext(this.sm);
			//if we have a configuration that matches the type
			if (objectConfiguration.get(c30sdkObjectType) != null) {

				//get the objects class name using configuration
				className = ((ObjectConfiguration) objectConfiguration.get(c30sdkObjectType)).getClassName();

				//create the arguments for the object using the ID.
				Object[] args = {c30sdkObjectType};

				//create the class of the object using configuration.
				Class objClass = Class.forName(className);

				//create the configured object
				lwo = createC30SDKObject(objClass, args);

				//create the objects children.
				this.loadChildrenC30SDKObjects(lwo);

				//if we don't have a configuration then throw an exception
			} else {
				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-001") + c30sdkObjectType , "INVALID-CONF-001");
			}

		} catch (ClassNotFoundException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-002") + c30sdkObjectType +" / "+ className, "INVALID-CONF-002");
		}

		return lwo;
	}

	/**
	 * Creates a new instance of a C30SDKObject object given the class of object to create and any arguments to be
	 * used in the constructor of the object.  This method operates by taking the arguments in order and tries to find
	 * a constructor that matches those exact arguments.  An exception is thrown if no constructors are found that match
	 * the given arguments.
	 * @param objectClass the class name of the object being created.
	 * @param args the arguments used to create the object
	 * @return A new instance of a C30SDKObject object
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the object
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 */
	public final synchronized C30SDKObject createC30SDKObject(final Class objectClass, final Object[] args) throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException {
		log.debug("Starting createC30SDKObject with Class = "+objectClass.getName());
		C30SDKObject lwObject = null;

		//-----------------------------------------------
		//If the object is in cache then use it instead.
		//-----------------------------------------------
		if (args.length == 1 && args[0].getClass().equals(BigInteger.class)
				&& c30SdkTypes.containsKey(objectClass.getName()+args[0])) {
			lwObject = (C30SDKObject) c30SdkTypes.get(objectClass.getName()+args[0]);

			//------------------------
			//Else create the object.
			//------------------------
		} else {                
			Object[] newArgs = new Object[args.length+1];
			newArgs[0] = this;
			for (int i = 0; i < args.length; i++) {
				newArgs[i+1] = args[i];
			}

			try {

				//get correct constructor
				Constructor[] cons = objectClass.getConstructors();
				Constructor constructor = null;
				Class[] types = null;
				boolean isCons = false;
				//go through all constructors
				for (int i = 0; i < cons.length; i++) {
					types = cons[i].getParameterTypes();
					//if constructor args length = argument length
					if (types.length == newArgs.length) {
						for (int j = 0; j < newArgs.length; j++) {
							if (!newArgs[j].getClass().equals(types[j])) {
								isCons = false;
								break;
							} else {
								isCons = true;
							}
						}
						if (isCons) {
							constructor = cons[i];
							break;
						}
					}
				}

				if (constructor == null) {
					String error = "There are no constructors for the object "+objectClass.getName()+" that match\n";
					error = error + "the given arguments [";
					for (int i = 0; i < newArgs.length-1; i++) {
						error = error + newArgs[i].getClass().getName() + ", ";
					}
					error = error + newArgs[newArgs.length-1].getClass().getName() + "]";
					//throw new C30SDKInvalidConfigurationException(error,Constants.EXCEPTION_INVALID_CONFIGURATION );
					//throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-003"), "INVALID-CONF-003");
					throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-003")+error, "INVALID-CONF-003");
				}

				//Create LWO with the given C30SDK object type.
				lwObject = (C30SDKObject) constructor.newInstance(newArgs);

			} catch (InvocationTargetException e) {
				String error = "There was an InvocationTargetException while constructing the object "+objectClass.getName()+" with\n";
				error = error + "the given arguments [";
				for (int i = 0; i < newArgs.length-1; i++) {
					error = error + newArgs[i].getClass().getName() + ", ";
				}
				error = error + newArgs[newArgs.length-1].getClass().getName() + "]";
				//throw new C30SDKInvalidConfigurationException(error, e, Constants.EXCEPTION_INVALID_CONFIGURATION );
				log.error(e);
				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-004")+error,e, "INVALID-CONF-004");
			} catch (IllegalAccessException e) {
				String error = "There was an IllegalAccessException while constructing the object "+objectClass.getName()+" with\n";
				error = error + "the given arguments [";
				for (int i = 0; i < newArgs.length-1; i++) {
					error = error + newArgs[i].getClass().getName() + ", ";
				}
				error = error + newArgs[newArgs.length-1].getClass().getName() + "]";
				//throw new C30SDKInvalidConfigurationException(error, e, Constants.EXCEPTION_INVALID_CONFIGURATION );
				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-005")+error,e,  "INVALID-CONF-005");
			} catch (InstantiationException e) {
				String error = "There was an InstantiationException while constructing the object "+objectClass.getName()+" with\n";
				error = error + "the given arguments [";
				for (int i = 0; i < newArgs.length-1; i++) {
					error = error + newArgs[i].getClass().getName() + ", ";
				}
				error = error + newArgs[newArgs.length-1].getClass().getName() + "]";
				//throw new C30SDKInvalidConfigurationException(error, e, Constants.EXCEPTION_INVALID_CONFIGURATION );
				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("INVALID-CONF-006")+error,e, "INVALID-CONF-006");
			}

			//put the LWO into the types list.
			//this.C30SDKTypes.put(objectClass.getName()+C30SDKObjectType, lwObject);
		}

		return lwObject;
	}







	/**
	 * Logs out of the SecurityManager.  Once this method has been called the factory can no longer be
	 * used to create C30SDK objects.  It is required that a new factory be instantiated.
	 */
	public final void logout() {
		if (kenanSecMgr != null) {
			try {
				//kenanSecMgr.logout();
				//SecurityManagerFactory.cleanupSecurityManager(kenanSecMgr);
			} catch (Exception e) {
				String error = "There was an error while trying to logout the security manager.\n";
				error = error + "Clean up of the security manager may not have been completed.";
				log.error(e);
			}
		}
	}

	/**
	 * Method to return the version of API-TS that is being used by any object instantiated from this factory object.
	 * @return the API-TS version
	 */
	public final Double getAPIVersion() {
		return ((Double) getAttributeValue(C30SDKAttributeConstants.ATTRIB_FX_VERSION));
	}

	/**
	 * Method to return the version of the C30SDK framework being used by any object instantiated from this factory object.
	 * @return the API-TS version
	 */
	protected final Double getFrameworkVersion() {
		return ((Double) getAttributeValue(C30SDKAttributeConstants.ATTRIB_PS_VERSION));
	}

	/**
	 * Method to return the version of the C30SDK framework being used by any object instantiated from this factory object.
	 * @return the API-TS version
	 */
	public final String getUserID() {
		return ((String) getAttributeValue(C30SDKAttributeConstants.ATTRIB_USER_ID));
	}

	/**
	 * Method which is used by all classes which extend C30SDKObject to get the extended data attributes
	 * specific to the object in Kenan.
	 * @param tableBaseName table base name which specifies the table that the object information comes from.  An example
	 * of this would be ORD_ORDER which specifies the ORDER table.  This is the table base name that would be used by
	 * the C30Order class.
	 * @param entityType the entity type which more finely tunes what extended attributes are available.  An example of this
	 * is the SERVICE table where different services allow for different extended data to be set.
	 * @return a hashmap of the extended data fields.
	 */
	protected final HashMap getExtendedDataAttributes(final String tableBaseName, final Integer entityType) {
		log.debug("Starting getExtendedDataAttributes. Table Base Name = " + tableBaseName + ", Entity Type = "+entityType);

		HashMap attributes = new HashMap();

		if (!extDataList.containsKey(tableBaseName+"_"+entityType)) {
			log.debug("No Extended Data hashmap exists.  Creating one for table and type");
			HashMap extData = null;
			Integer paramID = null;
			Integer tmpParamID = null;
			Integer paramDataType = null;
			Integer tmpEntityType = null;
			String tmpBaseTable = null;
			C30SDKAttribute lwoAttribute = null;
			Class attributeType = null;

			for (int i = 0; i < extDataAssocList.length; i++) {

				//------------------------------------------------
				// Get the param assoc information for each param
				//------------------------------------------------
				extData = (HashMap) extDataAssocList[i];
				paramID = (Integer) ((HashMap) ((HashMap) extData).get("Key")).get("ParamId");
				tmpEntityType = (Integer) ((HashMap) ((HashMap) extData).get("Key")).get("EntityType");
				tmpBaseTable = (String) ((HashMap) ((HashMap) extData).get("Key")).get("BaseTable");

				//----------------------------------------------
				// IF base table = criteria (tableBaseName)
				// AND entity type = criteria (entityType) OR 0
				// get data type and create attribute
				//----------------------------------------------
				if (tableBaseName.equals(tmpBaseTable) &&
						(entityType.equals(tmpEntityType) || tmpEntityType.equals(new Integer(0)))) {

					for (int j = 0; j < extDataParamsList.length; j++) {
						extData = (HashMap) extDataParamsList[j];
						tmpParamID = (Integer) ((HashMap) ((HashMap) extData).get("Key")).get("ParamId");
						if (tmpParamID.equals(paramID)) {
							paramDataType = (Integer) extData.get("ParamDatatype");

							if (paramDataType.equals(new Integer(1))) {
								attributeType = Integer.class;
							} else if (paramDataType.equals(new Integer(2))) {
								attributeType = String.class;
							} else if (paramDataType.equals(new Integer(3))) {
								attributeType = Date.class;
							} else if (paramDataType.equals(new Integer(4))) {
								attributeType = Integer.class;
							} else if (paramDataType.equals(new Integer(5))) {
								attributeType = Boolean.class;
							}
							break;
						}
					}

					if (paramID != null && attributeType != null) {
						lwoAttribute = new C30SDKAttribute(paramID.toString(), attributeType);
						// added enhancement jcw- all extended data attributes are not internal
						lwoAttribute.setIsInternal(false);
						attributes.put(lwoAttribute.getName(), lwoAttribute);
					}
				}

			}
			extDataList.put(tableBaseName+"_"+entityType, attributes);
		} else {
			log.debug("Extended Data hashmap already exists.  Using existing one for table and type");
			attributes = (HashMap) extDataList.get(tableBaseName+"_"+entityType);
		}
		log.debug("extDataList size = "+extDataList.size());
		log.debug("Finished getExtendedDataAttributes and size = "+attributes.size());
		return attributes;
	}

	/**
	 * Method which is used by all classes which extend C30SDKObject to get the extended data attributes
	 * specific to the object in Kenan.
	 * @param tableBaseName table base name which specifies the table that the object information comes from.  An example
	 * of this would be ORD_ORDER which specifies the ORDER table.  This is the table base name that would be used by
	 * the C30Order class.
	 * @param entityType the entity type which more finely tunes what extended attributes are available.  An example of this
	 * is the SERVICE table where different services allow for different extended data to be set.
	 * @return a hashmap of the extended data fields.
	 */
	protected final HashMap getAccountExtendedDataAttributes(final String tableBaseName, final Integer entityType) {
		log.debug("Starting getAccountExtendedDataAttributes. Table Base Name = " + tableBaseName + ", Entity Type = "+entityType);
		HashMap attributes = new HashMap();

		if (!extDataList.containsKey(tableBaseName+"_"+entityType)) {
			log.debug("No Extended Data hashmap exists.  Creating one for table and type");
			HashMap extData = null;
			Integer paramID = null;
			String  paramName = null;
			Integer tmpParamID = null;
			Integer paramDataType = null;
			Integer tmpEntityType = null;
			String tmpBaseTable = null;
			C30SDKAttribute lwoAttribute = null;
			Class attributeType = null;

			for (int i = 0; i < extDataAssocList.length; i++)
			{
				//------------------------------------------------
				// Get the param assoc information for each param
				//------------------------------------------------
				extData = (HashMap) extDataAssocList[i];
				paramID = (Integer) ((HashMap) ((HashMap) extData).get("Key")).get("ParamId");
				tmpEntityType = (Integer) ((HashMap) ((HashMap) extData).get("Key")).get("EntityType");
				tmpBaseTable = (String) ((HashMap) ((HashMap) extData).get("Key")).get("BaseTable");

				//----------------------------------------------
				// IF base table = criteria (tableBaseName)
				// AND entity type = criteria (entityType) OR 0
				// get data type and create attribute
				//----------------------------------------------
				if (tableBaseName.equals(tmpBaseTable) &&
						(entityType.equals(tmpEntityType) || entityType.equals(new Integer(0))))
				{
					for (int j = 0; j < extDataParamsList.length; j++)
					{
						extData = (HashMap) extDataParamsList[j];
						paramName = (String)  ((HashMap) extData).get("ParamName");
						tmpParamID = (Integer) ((HashMap) ((HashMap) extData).get("Key")).get("ParamId");
						if (tmpParamID.equals(paramID))
						{
							paramDataType = (Integer) extData.get("ParamDatatype");

							if (paramDataType.equals(new Integer(1))) {
								attributeType = Integer.class;
							} else if (paramDataType.equals(new Integer(2))) {
								attributeType = String.class;
							} else if (paramDataType.equals(new Integer(3))) {
								attributeType = Date.class;
							} else if (paramDataType.equals(new Integer(4))) {
								attributeType = Integer.class;
							} else if (paramDataType.equals(new Integer(5))) {
								attributeType = Boolean.class;
							}
							break;
						}
					}

					if (paramID != null && attributeType != null) {
						lwoAttribute = new C30SDKAttribute(paramID.toString(), attributeType);
						// enhancement jcw - no extended data is an internal attribute.
						lwoAttribute.setIsInternal(false);
						if (paramName != null){
							lwoAttribute.setAliasName(paramName);
							paramName = null;
						}
						attributes.put(lwoAttribute.getName(), lwoAttribute);
					}
				}
			}
			extDataList.put(tableBaseName+"_"+entityType, attributes);
		} else {
			log.debug("Extended Data hashmap already exists.  Using existing one for table and type");
			attributes = (HashMap) extDataList.get(tableBaseName+"_"+entityType);
		}
		log.debug("extDataList size = "+extDataList.size());
		log.debug("Finished getExtendedDataAttributes and size = "+attributes.size());
		printExtendedDataHashMap(attributes);
		return attributes;
	}

	private void printExtendedDataHashMap(HashMap myHMap)
	{
		if (myHMap == null)
			return;

		if (myHMap.size() < 1)
			return;

		Iterator it = myHMap.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			//C30SDKAttribute attr = (C30SDKAttribute) myHMap.get(key);
			//String value= attr.getValue().toString();
			log.debug("Name: " + key );
		}
	}

	/**
	 * Method which retrieves all extended data attributes which can be associated to domain objects.
	 * This information is then stored in the factory and used each time a domain object is created
	 * and contains extended data attributes.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while populating the extended data
	 * @throws C30SDKObjectException if there was a general problem populating the extended data attributes
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void populateExtendedDataAttributesJDBC() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKKenanFxCoreException {
		log.debug("Populating extended data attributes ...");
		try {
			//--------------------------------------
			// Fetch all Extended Data Associations
			//--------------------------------------
			//C30KenanMWDataSource dataSource = getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
			C30JDBCDataSource dataSource =getJDBCDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
			//Get the ExtendedDataList Count
			String extendedDataCountQuery = C30SDKSqlConstants.QUERY_GET_EXTENDEDDATA_PARAM_COUNT;
			PreparedStatement ps = dataSource.getPreparedStatement(extendedDataCountQuery);
			ResultSet resultSetCount  = ps.executeQuery();
			resultSetCount.next();
			int  totalCount = resultSetCount.getInt("TotalCount");
			resultSetCount.close();

			//Get the ExtendedDataList and populate it as part of the HashMap Array
			String extendedDataQuery = C30SDKSqlConstants.QUERY_GET_EXTENDEDDATA_PARAM_LIST;
			ps = dataSource.getPreparedStatement(extendedDataQuery);
			ResultSet resultSet  = ps.executeQuery();
			HashMap[] extededDataList = new HashMap[totalCount];
			int i =0;
			while (resultSet.next()) {
				HashMap extData =new HashMap(); 
				HashMap key = new HashMap();
				key.put("ParamId", resultSet.getInt("ParamId"));
				extData.put("Key", key);
				extData.put("DisplayValue", resultSet.getString("DisplayValue"));
				extData.put("ParamDataType", resultSet.getInt("ParamDataType"));
				extData.put("ParamName", resultSet.getString("ParamName"));
				extededDataList[i] = extData;
				i++;
			}
			resultSet.close();
			//Storing the extendedData List
			this.extDataAssocList = extededDataList;

			//Get the ExtendedDataAssocList Count
			String extendedDataAssocCountQuery = C30SDKSqlConstants.QUERY_GET_EXTENDEDDATA_ASSOC_COUNT;
			ps = dataSource.getPreparedStatement(extendedDataAssocCountQuery);
			ResultSet rsCount  = ps.executeQuery();
			rsCount.next();
			totalCount = rsCount.getInt("TotalCount");
			rsCount.close();

			//Get the ExtendedDataAssocList and populate it as part of the HashMap Array
			extendedDataAssocCountQuery = C30SDKSqlConstants.QUERY_GET_EXTENDEDDATA_ASSOC_LIST;
			ps = dataSource.getPreparedStatement(extendedDataAssocCountQuery);
			ResultSet rsAssocList  = ps.executeQuery();
			HashMap[] extededDataAssocList = new HashMap[totalCount];
			i =0;
			while (rsAssocList.next()) {
				HashMap extData =new HashMap(); 
				extData.put("IsRequired", rsAssocList.getInt("IsRequired"));
				HashMap key = new HashMap();
				key.put("BaseTable", rsAssocList.getString("BaseTable"));
				key.put("EntityType", rsAssocList.getString("EntityType"));
				key.put("ParamId", rsAssocList.getInt("ParamId"));
				extData.put("Key", key);
				extededDataAssocList[i] = extData;
				i++;
			}


			this.extDataParamsList = extededDataAssocList;
		}
		/*		catch (ServiceException e) {
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());

		} catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
		} catch (IOException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		} catch (terrapin.tuxedo.TuxError e) {
			String message = e.getMessage();
			String code[]=message.split("-");
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
		}*/
		catch (Exception e) {
			log.error(e);
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-009"), e, "CONN-009");
		}
		log.debug("Successfully populated extended data attribute!");
	}


	/**
	 * Method which retrieves all extended data attributes which can be associated to domain objects.
	 * This information is then stored in the factory and used each time a domain object is created
	 * and contains extended data attributes.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while populating the extended data
	 * @throws C30SDKObjectException if there was a general problem populating the extended data attributes
	 * @throws C30SDKKenanFxCoreException 
	 */
	/*private void populateExtendedDataAttributes() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKKenanFxCoreException {
		log.debug("Populating extended data attributes ...");
		try {
			//--------------------------------------
			// Fetch all Extended Data Associations
			//--------------------------------------
			C30KenanMWDataSource dataSource = getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
			HashMap dataFilter = new HashMap();
			dataFilter.put("Fetch", new Boolean(true));

			Map request = new HashMap();
			request.put("ExtendedDataAssociation", dataFilter);

			Map callResponse = dataSource.queryData(ApiMappings.getCallName("ExtendedDataAssociationFind"), request);
			this.extDataAssocList = (Object[]) callResponse.get("ExtendedDataAssociationList");

			//--------------------------------------
			// Fetch all Param Data
			//--------------------------------------
			dataSource = getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
			request = new HashMap();
			request.put("ExtendedDataParam", dataFilter);

			callResponse = dataSource.queryData(ApiMappings.getCallName("ExtendedDataParamFind"), request);
			this.extDataParamsList = (Object[]) callResponse.get("ExtendedDataParamList");
		}catch (ServiceException e) {
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());

		} catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
		} catch (IOException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		} catch (terrapin.tuxedo.TuxError e) {
			String message = e.getMessage();
			String code[]=message.split("-");
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
		}catch (Exception e) {
			log.error(e);
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-009"), e, "CONN-009");
		}
		log.debug("Successfully populated extended data attribute!");
	}

	 */

	/**
	 * Internal method which initializes all attributes for the factory.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during processing.
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the attributes
	 * @throws C30SDKObjectException if there was a problem initializing the attributes
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the factory.
	 * @throws C30SDKObjectConnectionException if there was a middleware error while initializing the attributes of the factory.
	 */
	private void initializeAttributes(String user) throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException {

		C30SDKAttribute attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FX_VERSION, Double.class);
		attribute.setOverrideable(false);
		this.profileAttributes.put(C30SDKAttributeConstants.ATTRIB_FX_VERSION, attribute);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PS_VERSION, Double.class);
		attribute.setOverrideable(false);
		this.profileAttributes.put(C30SDKAttributeConstants.ATTRIB_PS_VERSION, attribute);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COMPONENT_MEMBER_MAX_SIZE, Integer.class);
		attribute.setOverrideable(false);
		attribute.setValue(new Integer(1000));
		this.profileAttributes.put(C30SDKAttributeConstants.ATTRIB_COMPONENT_MEMBER_MAX_SIZE, attribute);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COMPONENT_MEMBER_FORCE_GET, Boolean.class);
		attribute.setOverrideable(false);
		attribute.setValue(Boolean.FALSE);
		this.profileAttributes.put(C30SDKAttributeConstants.ATTRIB_COMPONENT_MEMBER_FORCE_GET, attribute);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USER_ID, String.class);
		attribute.setOverrideable(false);
		attribute.setValue(user);
		this.profileAttributes.put(C30SDKAttributeConstants.ATTRIB_USER_ID, attribute);

		populateProfile();

	}

	/**
	 * Method which gets a factory attribute in a similar way to getting it from a C30SDK object.
	 * @param attribName the name of the attribute who's value is being retrieved.
	 * @return the value being retrieved.
	 */
	public Object getAttributeValue(final String attribName) {
		return ((C30SDKAttribute) profileAttributes.get(attribName)).getValue();
	}

	/**
	 * Method which gets a factory attribute in a similar way to getting it from a C30SDK object.
	 * @param attribName the name of the attribute who's value is being set.
	 * @param value the value of the attribute that is being set.
	 * @throws C30SDKInvalidAttributeException if an attribute was invalid while being set.
	 */
	private void setAttributeValue(final String attribName, final Object value) throws C30SDKInvalidAttributeException {
		((C30SDKAttribute) profileAttributes.get(attribName)).setValue(value);
	}

	/**
	 * Method which retrieves the profile that the framework should use.  The profile must be configured in the database.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while populating the attributes
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the attributes
	 * @throws C30SDKObjectException if there was a problem initializing the attributes
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the factory.
	 */
	private void populateProfile() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException {

		if (objectConfiguration.size() > 0) {

			String attribName = null;
			String attribValue = null;
			log.info("Getting the object config for version : " + version);
			ObjectConfiguration config = (ObjectConfiguration) objectConfiguration.get(version);

			if (config == null || config.getAttributeSize() < 1) {
				throw new C30SDKInvalidConfigurationException(exceptionResourceBundle.getString("INVALID-CONF-007"), "INVALID-CONF-007");
			}

			try {
				for (int i = 0; i < config.getAttributeSize(); i++) {
					attribName = config.getAttributeValue(i, "name");
					attribValue = config.getAttributeValue(i, "value");
					if (attribName.equals(C30SDKAttributeConstants.ATTRIB_FX_VERSION)) {
						this.setAttributeValue(attribName, new Double(attribValue));
					}
					if (attribName.equals(C30SDKAttributeConstants.ATTRIB_PS_VERSION)) {
						this.setAttributeValue(attribName, new Double(attribValue));
					}
					if (attribName.equals(C30SDKAttributeConstants.ATTRIB_COMPONENT_MEMBER_MAX_SIZE)) {
						this.setAttributeValue(attribName, new Integer(attribValue));
					}
					if (attribName.equals(C30SDKAttributeConstants.ATTRIB_COMPONENT_MEMBER_FORCE_GET)) {
						this.setAttributeValue(attribName, new Boolean(attribValue));
					}
					log.debug("Factory attribute " + attribName + " = " + attribValue);
				}
			} catch (NumberFormatException e) {
				throw new C30SDKInvalidConfigurationException(exceptionResourceBundle.getString("INVALID-ATTRIB-001"), "INVALID-ATTRIB-001");
			}

		}
	}

	/**
	 * Method which retrieves all parameters which are used as part of the defaults either required or not to retrieve the values from the user.
	 * The parameter information includes display information for how the information is to be retrieved from the user.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while populating the extended data
	 */
	private void populateUIControls() throws C30SDKObjectConnectionException {
		/*
    	C30KenanMWDataSource dataSource = getMwDataSource();

        C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_params"); //THIS STORED PROCEDURE HAS NOT BEEN CREATED YET.

        try {
        	//=============================================
        	// First thing we do is get all the parameters
        	//=============================================

            //------------------------------------------
            // set up the stored procedure to be called
            //------------------------------------------
            call.addParam(new C30CustomParamDef("lw_param_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("param_name", C30CustomParamDef.STRING, 32, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("param_length", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("param_datatype", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("param_ui_control", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("param_group", C30CustomParamDef.STRING, 100, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("param_order", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("enum_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("enum_value", C30CustomParamDef.STRING, 32, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("enum_display_value", C30CustomParamDef.STRING, 240, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("enum_sort_order", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));

            //---------------------------------------
            // make the call and process the results
            //---------------------------------------
            dataSource.queryData(call, new ArrayList(), C30TableDataSource.PARAM_NAMES);

            log.debug("Total row count = "+dataSource.getRowCount());

            if (dataSource.getRowCount() > 0) {

	            C30SDKAttributeControl uiControl = null;
	            C30SDKEnumeration enumeration = null;

	            for (int i = 0; i < dataSource.getRowCount(); i++) {

	            	//if its the first time or its a row with a new uiControl
	            	if (uiControl == null || ((Integer) dataSource.getValueAt(i,0)).intValue() != uiControl.getParamId()) {
	            		uiControl = new C30SDKAttributeControl(dataSource.getValueAt(i,1).toString(), ((Integer) dataSource.getValueAt(i,3)).intValue());
	            		uiControl.setParamId(((Integer) dataSource.getValueAt(i,0)).intValue());
	            		uiControl.setParamLength(((Integer) dataSource.getValueAt(i,2)).intValue());
	            		uiControl.setUIControl(((Integer) dataSource.getValueAt(i,4)).intValue());
	            		uiControl.setParamGroup(dataSource.getValueAt(i,5).toString());
	            		uiControl.setParamOrder(((Integer) dataSource.getValueAt(i,6)).intValue());
                        controls.add(uiControl);
                		log.debug("New Parameter: " + uiControl.toString());
	            	}

	            	//if we have an enumeration ID then create an enumeration
	            	if (!((Integer) dataSource.getValueAt(i,7)).equals(new Integer(0))) {
	            		enumeration = new C30SDKEnumeration();
	            		enumeration.setId( (Integer) dataSource.getValueAt(i,7));
	            		enumeration.setValue(dataSource.getValueAt(i,8).toString());
	            		enumeration.setDisplayValue(dataSource.getValueAt(i,9).toString());
	            		enumeration.setSortOrder((Integer) dataSource.getValueAt(i,10));
	            		uiControl.setEnumeration(enumeration);
	            		log.debug("     New Enumeration: "+enumeration.toString());
	            	}

	            }
            }

        } catch (Exception e) {
    		String error = "An error occurred trying to populate the UI Controls while constructing the factory.\n";
    		error = error + "The reasons for this error include:\n";
    		error = error + "1.  The parameter functionality is not included in this release.  Please ensure that\n";
    		error = error + "    the factory has the FX Version is set corectly.\n";
    		error = error + "2.  The parameters are not correctly initialized in the database.  Please ensure that\n";
    		error = error + "    all parameter data is correctly inserted into the database\n";
    		error = error + "Call Name = " + call.getCallName();
        	throw new C30SDKObjectConnectionException(error, e);
        }
		 */
	}


	/**
	 *  jcw: added for Class Attribute Configuration
	 * @throws C30SDKObjectConnectionException
	 */
	private void populateC30SDKClassAttribConfiguration()
			throws C30SDKObjectConnectionException
			{
		C30JDBCDataSource dataSource = null;
		try
		{
			//dataSource = getJDBCDataSource(C30SDKObject.C30_CATALOG_SERVER_ID);
			dataSource = getFrameworkDataSourceConnectionFromSDKPool();
			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_class_attrib");
			//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_class_attrib");

			call.addParam(new C30CustomParamDef("CLASS_NAME", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("ATTRIBUTE_NAME", 2, 250, 2));
			call.addParam(new C30CustomParamDef("ATTRIBUTE_JAVA_TYPE", 2, 250, 2));
			call.addParam(new C30CustomParamDef("INPUT_OUTPUT", 2, 10, 2));
			call.addParam(new C30CustomParamDef("FX_VERSION", 2, 150, 2));
			call.addParam(new C30CustomParamDef("REQUIRED", 1, 1, 2));
			call.addParam(new C30CustomParamDef("ATTRIBUTE_ALIAS", 2, 250, 2));
			call.addParam(new C30CustomParamDef("DEFAULT_VALUE", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("FORMAT", 2, 250, 2));
			call.addParam(new C30CustomParamDef("IS_PERMANENT", 2, 1, 2));
			call.addParam(new C30CustomParamDef("LOAD_ORDER", 2, 10,2));
			call.addParam(new C30CustomParamDef("IS_OVERRIDEABLE", 2, 1, 2));
			call.addParam(new C30CustomParamDef("IS_INTERNAL", 1, 1, 2));

			ArrayList paramValues = new ArrayList();
			dataSource.queryData(call, paramValues, 2);
			log.debug("Total row count class attrib config = " + dataSource.getRowCount());
			ClassAttribConfiguration object = null;
			String previousObject = "";
			if(dataSource.getRowCount() > 0)
			{        
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{
					String CLASS_NAME = "";
					String ATTRIBUTE_NAME = "";
					String ATTRIBUTE_JAVA_TYPE = "";
					String INPUT_OUTPUT = "";
					String FX_VERSION = "";
					String REQUIRED = "";
					String ATTRIBUTE_ALIAS = "";
					String DEFAULT_VALUE = "";
					String FORMAT = "";
					String IS_PERMANENT = "";
					String LOAD_ORDER = "";
					String IS_OVERRIDEABLE = "";
					String IS_INTERNAL = "";


					if (dataSource.getValueAt(i, 0) != null)
						CLASS_NAME =  dataSource.getValueAt(i, 0).toString();
					if (dataSource.getValueAt(i, 1) != null)
						ATTRIBUTE_NAME = dataSource.getValueAt(i, 1).toString();
					if (dataSource.getValueAt(i, 2) != null)
						ATTRIBUTE_JAVA_TYPE = dataSource.getValueAt(i, 2).toString();
					if (dataSource.getValueAt(i, 3) != null)
						INPUT_OUTPUT =  dataSource.getValueAt(i, 3).toString();
					if (dataSource.getValueAt(i, 4) != null)
						FX_VERSION =  dataSource.getValueAt(i, 4).toString();
					if (dataSource.getValueAt(i, 5) != null)
						REQUIRED = dataSource.getValueAt(i, 5).toString();
					if (dataSource.getValueAt(i, 6) != null)
						ATTRIBUTE_ALIAS = dataSource.getValueAt(i, 6).toString();
					if (dataSource.getValueAt(i, 7) != null)
						DEFAULT_VALUE = dataSource.getValueAt(i, 7).toString();
					if (dataSource.getValueAt(i, 8) != null)
						FORMAT = dataSource.getValueAt(i, 8).toString();
					if (dataSource.getValueAt(i, 9) != null)
						IS_PERMANENT = dataSource.getValueAt(i, 9).toString();
					if (dataSource.getValueAt(i, 10) != null)
						LOAD_ORDER = dataSource.getValueAt(i, 10).toString();
					if (dataSource.getValueAt(i, 11) != null)
						IS_OVERRIDEABLE = dataSource.getValueAt(i, 11).toString();
					if (dataSource.getValueAt(i, 12) != null)
						IS_INTERNAL = dataSource.getValueAt(i, 12).toString();

					if(object == null || !previousObject.equals(CLASS_NAME))
					{
						log.debug("For "+CLASS_NAME+", AttrName: "+ATTRIBUTE_NAME+", INPUT_OUTPUT: "+INPUT_OUTPUT+", ATTRIBUTE_JAVA_TYPE: "+ATTRIBUTE_JAVA_TYPE+", IS_OVERRIDEABLE: "+IS_OVERRIDEABLE+", IS_INTERNAL"+IS_INTERNAL);
						object = new ClassAttribConfiguration(CLASS_NAME);
						if(!ATTRIBUTE_NAME.equals(""))    
							object.addAttribute(ATTRIBUTE_NAME, ATTRIBUTE_JAVA_TYPE, REQUIRED+"", INPUT_OUTPUT, FX_VERSION, ATTRIBUTE_ALIAS, DEFAULT_VALUE, FORMAT, IS_PERMANENT, LOAD_ORDER, IS_OVERRIDEABLE,IS_INTERNAL);
						classAttribConfiguration.put(CLASS_NAME, object);
						previousObject = CLASS_NAME;
					} else
					{
						//if (CLASS_NAME.equals("com.cycle30.sdk.object.kenan.invoice.C30BillInvoice"))
						log.debug("For "+CLASS_NAME+", AttrName: "+ATTRIBUTE_NAME+", INPUT_OUTPUT: "+INPUT_OUTPUT+", ATTRIBUTE_JAVA_TYPE: "+ATTRIBUTE_JAVA_TYPE+", IS_OVERRIDEABLE: "+IS_OVERRIDEABLE);
						object.addAttribute(ATTRIBUTE_NAME, ATTRIBUTE_JAVA_TYPE, REQUIRED+"", INPUT_OUTPUT, FX_VERSION,  ATTRIBUTE_ALIAS, DEFAULT_VALUE, FORMAT, IS_PERMANENT, LOAD_ORDER, IS_OVERRIDEABLE,IS_INTERNAL);
					}
				}
			}

		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-010"), e, "CONN-010");
		}
		
			}

	/**
	 *  jcw: added for Class Attribute Configuration
	 * @throws C30SDKObjectConnectionException
	 */
	private void populateObjectFieldOutputFilter()
			throws C30SDKObjectConnectionException
			{
		C30JDBCDataSource dataSource = null;
		try
		{

			HashMap  classFieldMap = null;
			HashMap attrList = null;
			HashMap attrMap = null;
			String previousObject = "";
			String previousClass = "";
			String previousAttr = "";

			//dataSource = getJDBCDataSource(C30SDKObject.C30_CATALOG_SERVER_ID);
			//Connecting to C30SDK/Lwo Configuration Database.
			dataSource = getFrameworkDataSourceConnectionFromSDKPool();

			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_field_filter");
			//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_field_filter");

			call.addParam(new C30CustomParamDef("LW_OBJECT_TYPE", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("CLASS_NAME", 2, 250, 2));
			call.addParam(new C30CustomParamDef("ATTRIBUTE_NAME", 2, 250, 2));


			ArrayList paramValues = new ArrayList();
			dataSource.queryData(call, paramValues, 2);
			log.debug("Total row count field output filter= " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{          
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{
					String lwObjectType = "";
					String className = "";
					String attributeName = "";

					if (dataSource.getValueAt(i, 0) != null)
						lwObjectType =  dataSource.getValueAt(i, 0).toString();
					if (dataSource.getValueAt(i, 1) != null)
						className = dataSource.getValueAt(i, 1).toString();
					if (dataSource.getValueAt(i, 2) != null)
						attributeName = dataSource.getValueAt(i, 2).toString();

					log.debug(lwObjectType+", "+className+" -> "+attributeName);

					if( previousObject.trim().length() > 0 && !previousObject.equals(lwObjectType))
					{
						if (objectFieldOutputFilter.containsKey(previousObject)){
							log.info("Previous Object in filter :"+previousObject);
							Hashtable classAttrs = (Hashtable)objectFieldOutputFilter.get(previousObject);
							classAttrs.put(previousClass,attrList);
							objectFieldOutputFilter.put(previousObject,classAttrs);

						}
						else{
							classFieldMap = new HashMap();
							classFieldMap.put(previousClass,attrList);
							objectFieldOutputFilter.put(previousObject,classFieldMap);
						}

						//attrList = new ArrayList();
						attrList = new HashMap();
						if(!attributeName.equals("")) {
							// attrList.add(ATTRIBUTE_NAME);
							attrMap = new HashMap();
							attrMap.put("suppressWhenNull","0");
							attrList.put(attributeName,attrMap);

						}
						previousObject = lwObjectType;
						previousClass = className;
					} else
					{
						if(previousClass.trim().length() > 0 && !previousClass.equals(className)){

							if (objectFieldOutputFilter.containsKey(previousObject)){

								HashMap classAttrs = (HashMap)objectFieldOutputFilter.get(previousObject);
								classAttrs.put(previousClass,attrList);
								objectFieldOutputFilter.put(previousObject,classAttrs);

							}
							else{
								classFieldMap = new HashMap();
								classFieldMap.put(previousClass,attrList);
								objectFieldOutputFilter.put(previousObject,classFieldMap);
							}

							//attrList = new ArrayList();
							attrList = new HashMap();
							if(!attributeName.equals("")) {
								attrMap = new HashMap();
								attrMap.put("suppressWhenNull","0");
								attrList.put(attributeName,attrMap);
								//attrList.add(ATTRIBUTE_NAME);
							}
						}
						else
						{
							if (attrList == null)
								attrList = new HashMap();
							// attrList = new ArrayList();
							attrMap = new HashMap();
							attrMap.put("suppressWhenNull","0");
							attrList.put(attributeName,attrMap);   
							//attrList.add(ATTRIBUTE_NAME);
						}
						previousObject = lwObjectType;
						previousClass = className;
						previousAttr = attributeName;
					}


				}// end for


			}// end if data



			if (attrList != null && previousClass.trim().length() > 0)
				if (classFieldMap != null)
					classFieldMap.put(previousClass, attrList);
			if (objectFieldOutputFilter.containsKey(previousObject)){
				// Hashtable classAttrs = (Hashtable)objectFieldOutputFilter.get(previousObject);
				HashMap classAttrs = (HashMap)objectFieldOutputFilter.get(previousObject);
				classAttrs.put(previousClass,attrList);
				objectFieldOutputFilter.put(previousObject,classAttrs);

			}


		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-010"), e, "CONN-010");
		}
			}


	/**
	 * Method which retrieves all object configuration which is used to create C30SDK objects as they are requested.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while populating the configuration.
	 */
	private void populateObjectConfiguration()
			throws C30SDKObjectConnectionException
			{

		C30JDBCDataSource dataSource = getFrameworkDataSourceConnectionFromSDKPool();    
		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_objects");
		//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_objects");


		String resultArr[] = new String[16];

		try
		{
			call.addParam(new C30CustomParamDef("lw_object_type", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("class_id", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("attribute_name", 2, 250, 2));
			call.addParam(new C30CustomParamDef("attribute_value", 2, 250, 2));
			call.addParam(new C30CustomParamDef("attribute_type", 2, 5, 2));
			call.addParam(new C30CustomParamDef("is_required", 1, 1, 2));
			call.addParam(new C30CustomParamDef("allow_overrides", 1, 1, 2));
			call.addParam(new C30CustomParamDef("control_id", 1, 10, 2));
			call.addParam(new C30CustomParamDef("external_name", 2, 100, 2));
			call.addParam(new C30CustomParamDef("enumeration_id", 1, 10, 2));
			call.addParam(new C30CustomParamDef("pre_validate_class_id", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("bre_prevalidate_class_id", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("post_enforcer_class_id", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("bre_post_enforcer_class_id", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("keep_at_root", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("acct_seg_id", 1, 2000, 2));

			dataSource.queryData(call, new ArrayList(), 2);
			log.debug("Total row count object config= " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{
				ObjectConfiguration object = null;
				String previousObject = "";
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{


					for (int j = 0; j < resultArr.length; j++ )
					{
						if (dataSource.getValueAt(i,j)!= null)
						{
							//Populate the class_name for the given class_id

							if(j>=10 && j<14)
								resultArr[j]= (String)this.classIdConfig.get(dataSource.getValueAt(i,j));
							else 
								if(j==1) resultArr[j]= (String)this.classIdConfig.get(dataSource.getValueAt(i,j));
								else
									resultArr[j]= dataSource.getValueAt(i,j).toString();
						}
						else
							resultArr[j]= "";
					}
					//  log.debug(resultArr[0]+", "+resultArr[2]+", "+resultArr[8]);      
					Integer acctSegId=0;

					if(object == null || !previousObject.equals(resultArr[0].trim()))
					{
						if (resultArr[15] != null && !resultArr[15].trim().equalsIgnoreCase("") )
							acctSegId = new Integer(resultArr[15]);
						else
							acctSegId =0;

						object = new ObjectConfiguration(resultArr[0].trim(), resultArr[1],acctSegId);
						if(resultArr[2].length() > 0)
							object.addAttribute(resultArr[2], resultArr[3], resultArr[4], resultArr[5], resultArr[6], resultArr[7], resultArr[8], resultArr[9],resultArr[10],resultArr[11],resultArr[12],resultArr[13],resultArr[14]);
						objectConfiguration.put(resultArr[0].trim(), object);
						previousObject = resultArr[0].trim();
						log.info("Adding the configuration for the New object : " + previousObject +" External Name: " + resultArr[8]);
					} else
					{
						object.addAttribute(resultArr[2], resultArr[3], resultArr[4], resultArr[5], resultArr[6], resultArr[7], resultArr[8], resultArr[9],resultArr[10],resultArr[11],resultArr[12],resultArr[13],resultArr[14]);
						log.info("Adding the configuration for the previous object : " + previousObject +" External Name: " + resultArr[8]);
					}


				}     
			}

			call = new C30ExternalCallDef("c30_sdk_get_lw_object_map");
			//call = new C30ExternalCallDef("ps_get_lw_object_map");
			call.addParam(new C30CustomParamDef("parent_lw_object_type", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("child_lw_object_type", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("parent_acct_seg_id", 1, 2000, 2));
			call.addParam(new C30CustomParamDef("child_acct_seg_id", 1, 2000, 2));
			dataSource.queryData(call, new ArrayList(), 2);
			log.debug("Total row count object map = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{
				ObjectConfiguration parent = null;
				for(int i = 0; i < dataSource.getRowCount(); i++)
				{
					parent = (ObjectConfiguration)objectConfiguration.get(dataSource.getValueAt(i, 0).toString()+dataSource.getValueAt(i, 2).toString());
					parent.addChild(dataSource.getValueAt(i, 1).toString()+dataSource.getValueAt(i, 3).toString());
				}

			}
		}
		catch(Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-010"), e, "CONN-010");
		}

			}


	/**
	 * Method that, given a control ID will return the assocaited control with that ID.
	 * @param controlId the ID of the control being retrieved.
	 * @return the C30SDK control.
	 * @throws C30SDKInvalidAttributeException if there was a problem getting the control from the attribute it is associated with.
	 */
	private C30SDKAttributeControl getControl(final Integer controlId) throws C30SDKInvalidAttributeException {
		C30SDKAttributeControl control = null;
		for (int i = 0; i < controls.size(); i++) {
			if ( ((C30SDKAttributeControl) controls.get(i)).getParamId() == controlId.intValue()) {
				control = (C30SDKAttributeControl) controls.get(i);
				break;
			}
		}
		return control;
	}

	/**
	 * Method which populates an object type with the default attribute values that are configured for it.
	 * @param lwo the C30SDK object who's default values are being set.
	 * @throws C30SDKInvalidAttributeException if there was a problem loading the object attribute defaults.
	 */
	protected final void loadAttributeDefaults(final C30SDKObject lwo) throws C30SDKInvalidAttributeException {

		//What needs to be done.
		//1. Get the object configuration for the given C30SDK object
		//2. Loop through all attribute types.
		//3. Get list of attributes in LOAD ORDER. (getting them in load order means we can ensure that any specific business logic will
		//   be performed on the object as soon as the default value is set.  This is important because business logic could include the
		//   creation of new attributes which need to be populated and have default values)
		//4. loop through attributes list setting defaults if available.
		//5. get list of NEW attributes from first pass.
		//6. Once we have made a first pass at defaulting values we need
		//   to make a second pass since new attributes may have been added.
		//   i.e. extended data due to member_id having been set.

		//1. Get the object configuration for the given C30SDK object
		String objectType = lwo.getAttribute(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue().toString();
		ObjectConfiguration objectConfig = (ObjectConfiguration) objectConfiguration.get(objectType);

		//make sure there are attributes to default.  This may come back as null when an object
		//is being instantiated that has no attribute defaults. i.e. C30SDKExceptionMessage
		if (objectConfig != null) {

			Set attributes = null;
			Set newAttributes = null;

			//2.  Loop through all attribute types.
			for (int i = 0; i < C30SDKObject.ATTRIB_TYPES.length; i++) {

				//3.  Get list of attributes in LOAD ORDER.
				attributes = new TreeSet(new C30SDKLoadOrderComparator());
				attributes.addAll(lwo.getAttributes(C30SDKObject.ATTRIB_TYPES[i]).values());

				//4. loop through attributes list setting defaults if available.
				this.loadAttributeDefaults(lwo, attributes, C30SDKObject.ATTRIB_TYPES[i], objectConfig);

				//5. get list of NEW attributes from first pass.
				newAttributes = new TreeSet(new C30SDKNameComparator());
				newAttributes.addAll(lwo.getAttributes(C30SDKObject.ATTRIB_TYPES[i]).values());
				newAttributes.removeAll(attributes);

				//6. loop through NEW attributes, setting defaults if available.
				this.loadAttributeDefaults(lwo, newAttributes, C30SDKObject.ATTRIB_TYPES[i], objectConfig);

			}

		} else {
			log.warn("Warning: Object of type: " + objectType + " has no configuration associated with it.");
		}
	}

	/**
	 * Method which, given a set of attributes sets their default values if they exist.
	 * @param lwo the C30SDK object who's values are being set.  It is important that the C30SDK object be passed through.  The
	 * reason is because we must perform a setAttributeValue on the object itself when setting the value.  The reason is because each LWO
	 * object might have specific business logic associated with the attribute being set which exists in the overridden method.  So if we
	 * were to simply set the attributes value we would not be performing the business logic associated with the object itself.
	 * @param attributes the set of attributes we are trying to set values for.
	 * @param attributeType the type of the attribute. i.e. input, output or system.  This value must also be passed in because it is not
	 * a value which can be retrieved from the attribute itself.
	 * @param objectConfig the object configuration which will be quizzed for default values.
	 * @throws C30SDKInvalidAttributeException if there was a problem loading the object attribute defaults.
	 */
	private void loadAttributeDefaults(final C30SDKObject lwo, final Set attributes, final Integer attributeType, final ObjectConfiguration objectConfig) throws C30SDKInvalidAttributeException {

		String attribName = "";
		Integer attribType = null;
		C30SDKAttribute attribute = null;

		// 2. loop through attributes list.
		Iterator iter = attributes.iterator();
		while (iter.hasNext()) {

			attribute = (C30SDKAttribute) iter.next();
			log.debug("Getting details for attribute : "+ attribute.getName());
			if (attribute.getName().equalsIgnoreCase("ItemActionId"))
				log.info("Working on Item Action Id");
			//loop through configured attributes looking for any configuration
			for (int i = 0; i < objectConfig.getAttributeSize(); i++) {
				attribName = objectConfig.getAttributeValue(i, "name");
				attribType = new Integer(objectConfig.getAttributeValue(i, "type"));
				// Set External Name values on OUTPUT ATTRIBUTES from INPUT ATTRIBUTES
				//if(attribute.getName().equals(attribName) && attribType.equals(C30SDKObject.ATTRIB_TYPE_INPUT)&&  attributeType.equals(C30SDKObject.ATTRIB_TYPE_OUTPUT))
				log.debug("Attribute.getName : "+attribute.getName() + " attribName : "+attribName);
				if(attribute.getName().equals(attribName))  
				{
					attribute.setExternalName(objectConfig.getAttributeValue(i, "externalName"));
					if (!objectConfig.getAttributeValue(i, "enumerationId").equalsIgnoreCase(""))
					{
						attribute.setEnumerationId(new Integer(objectConfig.getAttributeValue(i, "enumerationId")));
						if (!objectConfig.getAttributeValue(i, "keepatRoot").equalsIgnoreCase(""))
							attribute.setKeepatRoot(new Integer(objectConfig.getAttributeValue(i, "keepatRoot")));
					}

				}

				// 3. Try get configured value.
				if (attribute.getName().equals(attribName) && attributeType.equals(attribType)) {
					log.debug("ObjectConfig Attribute Name: "+attribName+", Attribute.Name = "+attribute.getName()+", Attribute value: "+objectConfig.getAttributeValue(i, "value").toString());
					//we must perform a setAttributeValue here.  The reason is because each LWO object might have specific
					// business logic associated with the attribute being set which exists in the overridden method.
					lwo.setAttributeValue(attribName, objectConfig.getAttributeValue(i, "value"), attributeType);

					if (objectConfig.getAttributeValue(i, "isRequired").equals("1")) {
						attribute.setRequired(Boolean.TRUE.booleanValue());
					}
					attribute.setOverrideable(new Boolean(objectConfig.getAttributeValue(i, "allowOverrides")).booleanValue());
					attribute.setExternalName(objectConfig.getAttributeValue(i, "externalName"));
					if (!objectConfig.getAttributeValue(i, "enumerationId").equalsIgnoreCase(""))
					{
						attribute.setEnumerationId(new Integer(objectConfig.getAttributeValue(i, "enumerationId")));
						if (!objectConfig.getAttributeValue(i, "keepatRoot").equalsIgnoreCase(""))
							attribute.setKeepatRoot(new Integer(objectConfig.getAttributeValue(i, "keepatRoot")));
					}
					//	attribute.setEnumerationId(new Integer(objectConfig.getAttributeValue(i, "enumerationId")));

					//if the control ID is NOT zero then get the control
					if (!objectConfig.getAttributeValue(i, "controlId").equals("0")) {
						C30SDKAttributeControl control = this.getControl(new Integer(objectConfig.getAttributeValue(i, "controlId")));
						if (control != null) {
							attribute.setUIControl(control);
						} else {
							throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-002") +attribName ,"INVALID-ATTRIB-002");
						}
					}
					break;
				}
			}
		}

	}

	/**
	 * Method which loads all children C30SDK objects that have been configured for this parent.
	 * @param parent the parent object that is to be populated with its children.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while loading child objects.
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while loading child objects.
	 * @throws C30SDKObjectException if there was a problem loading child objects.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes while loading child objects.
	 */
	private void loadChildrenC30SDKObjects(final C30SDKObject parent) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
		ObjectConfiguration objectConfig = (ObjectConfiguration) objectConfiguration.get(parent.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE).toString());

		//make sure there are attributes to default.  This may come back as null when an object
		//is being instantiated that has no attribute defaults. i.e. C30SDKExceptionMessage
		if (objectConfig != null) {
			C30SDKObject child = null;
			for (int i = 0; i < objectConfig.getChildren().size(); i++) {
				child = this.createC30SDKObject((String) objectConfig.getChildren().get(i));
				parent.addC30SDKObject(child);
			}
		}

	}


	/**
	 * The method to load any configurate attributes and output filtering for a C30SDK Object
	 * @param lwo
	 * @param className
	 * @throws C30SDKInvalidAttributeException
	 */
	public final void setClassFieldConfiguration(C30SDKObject lwo, String className)
			throws  C30SDKInvalidAttributeException
			{

		String loadVersion = getAPIVersion().toString();
		ClassAttribConfiguration  fieldConfig = null;
		if (className == null || className.trim().length() == 0)
			className  = lwo.getClass().getName();

		ArrayList attrOutputFilterList = new ArrayList();
		HashMap  attrOutputFilterMap = new HashMap();
		String lwObjType = (String)lwo.getAttributeValue("ObjectType");
		// need to retrieve any fields that can be exposed at output time - get the entry for the the objectType
		if (objectFieldOutputFilter.containsKey(lwObjType)){
			HashMap classAttrMap = (HashMap)objectFieldOutputFilter.get(lwObjType);    
			// get the ArrayList for the specific lwo class attributes
			if (classAttrMap.containsKey(className))
				attrOutputFilterMap = (HashMap)classAttrMap.get(className); 
			//attrOutputFilterList = (ArrayList)classAttrMap.get(className);    
		}

		if (classAttribConfiguration.containsKey(className))
			fieldConfig = (ClassAttribConfiguration) classAttribConfiguration.get(className);    


		try{    
			for(int i = 0; i < fieldConfig.getAttributeSize() ; i++)
			{
				HashMap attrFilterValues = new HashMap();

				C30SDKAttribute attribute = null;

				String attrClass = fieldConfig.getAttributeValue(i, "attributeJavaType") ;
				//log.debug("attrClass :"+attrClass);
				Class attribClass = Class.forName(attrClass);

				String attrName = fieldConfig.getAttributeValue(i, "attributeName") ;
				//log.debug("attrValue :"+attrName);

				if (attrOutputFilterMap.containsKey(attrName))
					attrFilterValues = (HashMap)attrOutputFilterMap.get(attrName);

				String attrReq = fieldConfig.getAttributeValue(i, "required") ;
				String inputOutput = fieldConfig.getAttributeValue(i, "inputOutput");
				String fxVersion = fieldConfig.getAttributeValue(i, "fxVersion") ;
				String attributeAlias = fieldConfig.getAttributeValue(i, "attributeAlias") ;
				String defaultValue = fieldConfig.getAttributeValue(i, "defaultValue") ;
				String formatValue = fieldConfig.getAttributeValue(i, "format") ;
				String isInternal = fieldConfig.getAttributeValue(i,"isInternal");

				// check to see if the version we are running contains this attribute 
				if (loadVersion.equals(fxVersion) || fxVersion.equalsIgnoreCase("ALL") || fxVersion.indexOf(loadVersion) > -1){
					log.debug("attrClass :"+attrClass+", attrValue: "+attrName+", inputOutput: "+inputOutput+", Version: "+loadVersion);
					attribute = new C30SDKAttribute(attrName, attribClass);
					if (attrReq.equalsIgnoreCase("1"))
						attribute.setRequired(true);
					if (isInternal.equalsIgnoreCase("0"))
						attribute.setIsInternal(false);    
					if (defaultValue != null && defaultValue.trim().length() > 0)
					{
						if (attrClass.indexOf("Date") > -1 && defaultValue.equalsIgnoreCase("0L"))
							attribute.setValue(new Date(0L));
						else
							attribute.setValue(defaultValue);
					}
					if (formatValue != null && formatValue.trim().length() > 0) 
						attribute.setFormat(formatValue);
					if (attributeAlias != null && attributeAlias.trim().length() > 0) 
						attribute.setAliasName(attributeAlias);
					/* Setting filter output: if there is filtering on an object then exposing every attribute is turned off 
               // unless explicitly set.  In otherwords you will get everything if there is NO filter
               // If there is a filter, you will only get what you have set up in the filter
					 */
					if (attrOutputFilterMap.size() > 0) 
					{  
						attribute.setExposeOutput(false);
						if (attrFilterValues.size() > 0 ) 
						{
							if ( inputOutput.equalsIgnoreCase("OUTPUT"))
								attribute.setExposeOutput(true); 
						}
					}
					if (inputOutput.equalsIgnoreCase("INPUT"))      
						lwo.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					if (inputOutput.equalsIgnoreCase("OUTPUT"))      
						lwo.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				}

			}
		} 
		catch(Exception e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-003"), e,"INVALID-ATTRIB-003");
		}

			}

	/**
	 * the method used to remove Order-based attributes from a C30SDK order-based object when 
	 * it is being used directly to retreive data rather than build or create a Kenan object from within an 
	 * order context.
	 * @param lwo
	 * @param className
	 * @throws C30SDKInvalidAttributeException
	 */
	public final void removeClassFieldConfiguration(C30SDKObject lwo, String className)
			throws  C30SDKInvalidAttributeException
			{

		String loadVersion = getAPIVersion().toString();
		ClassAttribConfiguration  fieldConfig = null;
		if (className == null || className.trim().length() == 0)
			className  = lwo.getClass().getName();
		if (classAttribConfiguration.containsKey(className))
			fieldConfig = (ClassAttribConfiguration) classAttribConfiguration.get(className);


		try{    
			for(int i = 0; i < fieldConfig.getAttributeSize() ; i++)
			{
				C30SDKAttribute attribute = null;
				String attrName = fieldConfig.getAttributeValue(i, "attributeName") ;
				log.debug("attrValue :"+attrName);

				String inputOutput = fieldConfig.getAttributeValue(i, "inputOutput");
				log.debug("inputOutput :"+inputOutput);

				String isPermanent = fieldConfig.getAttributeValue(i, "isPermanent");
				log.debug("isPermanent :"+isPermanent);

				if (isPermanent.equalsIgnoreCase("1"))
					continue;
				if (inputOutput.equalsIgnoreCase("INPUT"))      
					lwo.removeAttribute(attrName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				if (inputOutput.equalsIgnoreCase("OUTPUT"))      
					lwo.removeAttribute(attrName, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

			}


		} 
		catch(Exception e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-003"), e,"INVALID-ATTRIB-003");
		}

			}


	/**
	 * This holds the Order and Workpoint Process Map
	 * @author rnelluri
	 *
	 */
	public class OrderWorkPointMap
	{
		String orderType="";
		Integer acctSegId;
		Integer isServiceLevel;
		Integer emfConfigId;
		Integer accountCategory;
		String itemId="";
		String itemType="";
		String wpProcessId="";
		Date activeDate;
		Date inactiveDate;
		/**
		 * @return the orderType
		 */
		public String getOrderType() {
			return orderType;
		}
		/**
		 * @param orderType the orderType to set
		 */
		public void setOrderType(String orderType) {
			this.orderType = orderType;
		}
		/**
		 * @return the acctSegId
		 */
		public Integer getAcctSegId() {
			return acctSegId;
		}
		/**
		 * @param acctSegId the acctSegId to set
		 */
		public void setAcctSegId(Integer acctSegId) {
			this.acctSegId = acctSegId;
		}
		/**
		 * @return the isServiceLevel
		 */
		public Integer getIsServiceLevel() {
			return isServiceLevel;
		}
		/**
		 * @param isServiceLevel the isServiceLevel to set
		 */
		public void setIsServiceLevel(Integer isServiceLevel) {
			this.isServiceLevel = isServiceLevel;
		}
		/**
		 * @return the emfConfigId
		 */
		public Integer getEmfConfigId() {
			return emfConfigId;
		}
		/**
		 * @param emfConfigId the emfConfigId to set
		 */
		public void setEmfConfigId(Integer emfConfigId) {
			this.emfConfigId = emfConfigId;
		}
		/**
		 * @return the accountCategory
		 */
		public Integer getAccountCategory() {
			return accountCategory;
		}
		/**
		 * @param accountCategory the accountCategory to set
		 */
		public void setAccountCategory(Integer accountCategory) {
			this.accountCategory = accountCategory;
		}
		/**
		 * @return the itemId
		 */
		public String getItemId() {
			return itemId;
		}
		/**
		 * @param itemId the itemId to set
		 */
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		/**
		 * @return the itemType
		 */
		public String getItemType() {
			return itemType;
		}
		/**
		 * @param itemType the itemType to set
		 */
		public void setItemType(String itemType) {
			this.itemType = itemType;
		}
		/**
		 * @return the wpProcessId
		 */
		public String getWpProcessId() {
			return wpProcessId;
		}
		/**
		 * @param wpProcessId the wpProcessId to set
		 */
		public void setWpProcessId(String wpProcessId) {
			this.wpProcessId = wpProcessId;
		}
		/**
		 * @return the activeDate
		 */
		public Date getActiveDate() {
			return activeDate;
		}
		/**
		 * @param activeDate the activeDate to set
		 */
		public void setActiveDate(Date activeDate) {
			this.activeDate = activeDate;
		}
		/**
		 * @return the inactiveDate
		 */
		public Date getInactiveDate() {
			return inactiveDate;
		}
		/**
		 * @param inactiveDate the inactiveDate to set
		 */
		public void setInactiveDate(Date inactiveDate) {
			this.inactiveDate = inactiveDate;
		}
		/**
		 * @param orderType
		 * @param acctSegId
		 * @param isServiceLevel
		 * @param emfConfigId
		 * @param accountCategory
		 * @param itemId
		 * @param itemType
		 * @param wpProcessId
		 * @param activeDate
		 * @param inactiveDate
		 */
		public OrderWorkPointMap(String orderType, Integer acctSegId,
				Integer isServiceLevel, Integer emfConfigId,
				Integer accountCategory, String itemId, String itemType,
				String wpProcessId, Date activeDate, Date inactiveDate) {
			this.orderType = orderType;
			this.acctSegId = acctSegId;
			this.isServiceLevel = isServiceLevel;
			this.emfConfigId = emfConfigId;
			this.accountCategory = accountCategory;
			this.itemId = itemId;
			this.itemType = itemType;
			this.wpProcessId = wpProcessId;
			this.activeDate = activeDate;
			this.inactiveDate = inactiveDate;
		}
		/**
		 * Returning the String value for the object.
		 */
		public String toString()
		{
			return (this.orderType+";"+
					this.acctSegId+";"+
					this.isServiceLevel+";"+
					this.emfConfigId+";"+
					this.accountCategory+";"+
					this.itemId+";"+
					this.itemType+";"+
					this.wpProcessId+";"+
					this.activeDate+";"+
					this.inactiveDate);			
		}
	}

	/**
	 * A private class that holds the C30SDK Object attribute configuration data that is loaded when the factory is first initialized.  This configuration
	 * could be considered the C30SDK object attribute configuration cache and is used to retrieve configuration information rather than retrieving
	 * information from the database.
	 */
	private class ClassAttribConfiguration
	{
		private class FieldAttribute
		{

			public String getValue(String name)
			{
				String value = "";

				if(name.equals("attributeName"))
					value = this.attributeName;
				else
					if(name.equals("attributeJavaType"))
						value = attributeJavaType;
					else
						if(name.equals("required"))
							value = required;
						else
							if(name.equals("inputOutput"))
								value = inputOutput;
							else
								if(name.equals("fxVersion"))
									value = fxVersion;
								else
									if(name.equals("attributeAlias"))
										value = attributeAlias;
									else
										if(name.equals("defaultValue"))
											value = defaultValue;
										else
											if(name.equals("format"))
												value = format;   
											else
												if(name.equals("isPermanent"))
													value = isPermanent;  
												else
													if(name.equals("isOverrideable"))
														value = isOverrideable; 
													else
														if(name.equals("loadOrder"))
															value = loadOrder;  
														else
															if(name.equals("isInternal"))
																value = isInternal;        
				return value;
			}




			private String attributeName;
			private String attributeJavaType;
			private String required;
			private String inputOutput;
			private String fxVersion;
			private String attributeAlias;
			private String defaultValue;
			private String format;
			private String isPermanent;
			private String loadOrder;
			private String isOverrideable;
			private String isInternal;

			public FieldAttribute(String attributeName, String attributeJavaType, String required, String inputOutput, String fxVersion, 
					String attributeAlias, String defaultValue,String format, String isPermanent,String loadOrder,String isOverrideable,String isInternal)
			{
				super();

				this.attributeName = "";
				this.attributeJavaType = "";
				this.required = "";
				this.inputOutput = "";
				this.fxVersion = "";
				this.attributeAlias = "";
				this.defaultValue = ""; 
				this.format = "";
				this.isPermanent = "";
				this.loadOrder = "";
				this.isOverrideable = "";
				this.isInternal = "";

				this.attributeName = attributeName;
				this.attributeJavaType = attributeJavaType;
				this.required = required;
				this.inputOutput = inputOutput;
				this.fxVersion = fxVersion;
				this.attributeAlias = attributeAlias;
				this.defaultValue = defaultValue;
				this.format = format;
				this.isPermanent = isPermanent;
				this.loadOrder = loadOrder;
				this.isOverrideable = isOverrideable;
				this.isInternal = isInternal;

			}
		}




		public String getClassName()
		{
			return className;
		}

		public String getAttributeValue(int index, String name)
		{
			return ((FieldAttribute)attributes.get(index)).getValue(name);
		}

		public int getAttributeSize()
		{
			return attributes.size();
		}

		public void addAttribute(String attributeName, String attributeJavaType, String required, String inputOutput, String fxVersion, 
				String attributeAlias, String defaultValue, String format, String isPermanent, String loadOrder, String isOverrideable, String isInternal)
		{
			FieldAttribute attr = new FieldAttribute(attributeName, attributeJavaType, required, inputOutput, fxVersion, 
					attributeAlias, defaultValue, format, isPermanent, loadOrder, isOverrideable, isInternal);
			attributes.add(attributes.size(), attr);
		}


		public String toString()
		{
			return "ClassConfiguration:  className = " + className + ", attributes = " + attributes.size();
		}


		private String className;
		private List attributes;


		public ClassAttribConfiguration(String className)
		{
			super();
			this.className = "";
			attributes = new ArrayList();
			this.className = className;
		}
	}
	// jcw: end added for Class-Field Configuration



	/**
	 * A private class that holds the configuration data that is loaded when the factory is first initialized.  This configuration
	 * could be considered the object configuration cache and is used to retrieve configuration information rather than retrieving
	 * information from the database.
	 */
	private class ObjectConfiguration {

		private String objectType = "";
		private String className = "";
		private Integer acctSegId;
		private String preValidationClassName = "";
		private String brePreValidationClassName = "";
		private String brePostEnforcerClassName = "";
		private String PostEnforcerClassName = "";
		private List attributes = new ArrayList();
		private List children = new ArrayList();

		public ObjectConfiguration(final String objectType, final String className,final Integer acctSegId) {
			this.objectType = objectType;
			this.className = className;
			this.acctSegId= acctSegId;
		}

		public String getObjectType() {
			return objectType;
		}

		public String getClassName() {
			return className;
		}
		public Integer getAcctSegId() {
			return acctSegId;
		}

		public String getPreValidationClassName() {
			return preValidationClassName;
		}

		public void setPreValidationClassName(String preValidationClassName) {
			this.preValidationClassName = preValidationClassName;
		}

		public String getBrePreValidationClassName() {
			return brePreValidationClassName;
		}

		public void setBrePreValidationClassName(String brePreValidationClassName) {
			this.brePreValidationClassName = brePreValidationClassName;
		}

		public String getBrePostEnforcerClassName() {
			return brePostEnforcerClassName;
		}

		public void setBrePostEnforcerClassName(String brePostEnforcerClassName) {
			this.brePostEnforcerClassName = brePostEnforcerClassName;
		}

		public String getPostEnforcerClassName() {
			return PostEnforcerClassName;
		}

		public void setPostEnforcerClassName(String postEnforcerClassName) {
			PostEnforcerClassName = postEnforcerClassName;
		}

		public String getAttributeValue(final int index, final String name) {
			return ((ObjectAttribute) attributes.get(index)).getValue(name);
		}

		public int getAttributeSize() {
			return attributes.size();
		}

		public void addAttribute(final String name, final String value, final String type, final String isRequired, final String allowOverrides, 
				final String controlId, final String externalName, final String enumerationId,
				final String preValidateClassId, final String brePreValidateClassId, final String postEnforcerClassId, final String brePostEnforcerClassId, 
				final String keepatRoot) {
			ObjectAttribute attr = new ObjectAttribute(name, value, type, isRequired, allowOverrides, controlId, externalName,enumerationId, keepatRoot);
			attributes.add(attributes.size(), attr);
			this.preValidationClassName=preValidateClassId;
			this.brePreValidationClassName=brePreValidateClassId;
			this.PostEnforcerClassName=postEnforcerClassId;
			this.brePostEnforcerClassName=brePostEnforcerClassId;
			this.acctSegId = acctSegId;


		}

		public void addChild(final String childType) {
			children.add(childType);
		}

		public List getChildren() {
			return children;
		}

		public String toString() {
			return "ObjectConfiguration: objectType = " + objectType + ", className = " + className + ", attributes = " + attributes.size() + ", children = " + children.size();
		}

		private class ObjectAttribute {

			private String name = "";
			private String value = "";
			private String type = "";
			private String isRequired = "";
			private String allowOverrides = "";
			private String controlId = "";
			private String externalName = "";
			private String enumerationId ="";
			private String keepatRoot="";


			public ObjectAttribute(final String name, final String value, final String type, final String isRequired, final String allowOverrides, final String controlId, 
					final String externalName, final String enumerationId, final String keepatRoot) {
				this.name = name;
				this.value = value;
				this.type = type;
				this.isRequired = isRequired;
				this.allowOverrides = allowOverrides;
				this.controlId = controlId;
				this.externalName = externalName;
				this.enumerationId= enumerationId;
				this.keepatRoot=keepatRoot;

			}

			public String getValue(final String name) {
				String value = "";
				if (name.equals("name")) {
					value = this.name;
				} else if (name.equals("value")) {
					value = this.value;
				} else if (name.equals("type")) {
					value = type;
				} else if (name.equals("isRequired")) {
					value = isRequired;
				} else if (name.equals("allowOverrides")) {
					value = allowOverrides;
				} else if (name.equals("controlId")) {
					value = controlId;
				} else if (name.equals("externalName")) {
					value = externalName;
				}else if (name.equals("enumerationId")) {
					value = enumerationId;
				}else if (name.equals("keepatRoot")) {
					value = keepatRoot;
				}
				return value;
			}

		}


	}
	public HashMap getPaymentObjectConfig() {
		return paymentConfig;
	}

	public HashMap getObjectEnumeration() {
		return objectEnumeration;
	}

}
