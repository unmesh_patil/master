/*
 * C30SDKCollection.java
 *
 * Created on January 3, 2007, 10:04 AM
 *
 */

package com.cycle30.sdk.core.framework;

import java.util.ArrayList;
import java.util.List;

import com.cycle30.sdk.core.message.C30SDKMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * A C30SDK object that contains a collection of other C30SDK objects.
 * Example - a C30Order contains a collection of C30SDKServiceOrders,
 * so it is a C30SDKCollection.
 * @author John Reeves
 */

public class C30SDKCollection extends C30SDKObject implements Cloneable {

    /**
     * Creates a new instance of C30ServiceServiceOrder.
     * @param factory the factory which is the parent of this object.
     * @param objectType Type of C30SDK service service order being constructed
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SDKCollection(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /** 
     * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
     * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
     * the framework to create objects.  It is also used by the BRE front end.
     * @param factory Factory used to create C30ServiceOrder objects
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SDKCollection(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30SDKCollection.class);
    }
    
    /**
     * Convenience method to create a List of the C30SDK objects in this collection.
     * @return the list containing the children of this collection.
     */
    public final List toList() {
    	List objects = new ArrayList(this.getC30SDKObjectCount());
    	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
    		objects.add(this.getC30SDKObject(i));
    	}
    	return objects;
    }
    
    /**
     * Method to get the n'th object of the given type.
     * @param lwoClass the class of object we are searching
     * @param index where the first element is the zero'th element
     * @return the object or NULL if not found.
     */
    public final C30SDKObject getNthObjectOfType(final Class lwoClass, final int index) {
    	C30SDKObject lwo = null;
    	int classIndex = 0;
    	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
    		if (this.getC30SDKObject(i).getClass().equals(lwoClass)) {
    			if (index == classIndex) {
    				lwo = this.getC30SDKObject(i);
    				break;
    			} else {
    				classIndex++;
    			}
    		}
    	}
    	return lwo;
    }

    /**
     * Creates a copy of the C30SDKCollection object.
     * @return A copy of the C30SDKCollection object
     */
    @Override
	public final Object clone() {
        C30SDKCollection copy = (C30SDKCollection) super.clone();

        //---------------------------------------------
        // clone the array list of C30SDK objects
        //---------------------------------------------
        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
            copy.addC30SDKObject((C30SDKObject) this.getC30SDKObject(i).clone());
        }

        return copy;
    }
    
    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {
    	
    }

    /**
     * Method that processes the C30SDK object.
     * @return the result of processing the C30SDK object
     */
    @Override
	public final C30SDKObject process() {

    	C30SDKObject lwo = null;
    	C30SDKObject tmpLwo = null;
    	try {
			lwo = this.getFactory().createC30SDKObject(C30SDKCollection.class, new Object[0]);
	    	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
	    		tmpLwo = this.getC30SDKObject(i).process();
	    		if (tmpLwo instanceof C30SDKMessage) {
	    			lwo = tmpLwo;
	    			break;
	    		} else {
	    			lwo.addC30SDKObject(tmpLwo);
	    		}
	    	}
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}
    	return lwo;
    }
    
    /**
     * Convenience method to add a list of C30SDK objects from one collection to this collection.
     * @param collection the collection being added to this collection.
     */
    public final void addCollection(final C30SDKCollection collection) {
    	for (int i = 0; i < collection.getC30SDKObjectCount(); i++) {
    		this.addC30SDKObject(collection.getC30SDKObject(i));
    	}
    }
    
    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
