package com.cycle30.sdk.core.framework;

import java.util.UUID;

import org.apache.log4j.Logger;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;

/**
 * This is the C30SDK user object that encapsulates a piece of "work" that is to be performed.
 * The work could be a single action like creating an order.  It could also have multiple actions
 * such as creating an order, adding a payment and emailing someone that the order is complete.
 * @author anst01
 */
public class C30SDKUser  {

	private static Logger log = Logger.getLogger(C30SDKUser.class);
	private SecurityManager lwSm = null;
	private String lwUserId;
	private String lwPassword;
	private String lwRealm;
	private String lwSessionId;

	C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	C30SDKPropertyFileManager KenanExceptionResourceBundle = C30SDKPropertyFileManager.getInstance();

	/**
	 *  Creates a new instance of a C30SDKUser.
	 * @param userId
	 * @param password
	 * @param realm
	 * @throws C30SDKKenanFxCoreException 
	 */
	public C30SDKUser(final String userId,final String password, final String realm) throws C30SDKKenanFxCoreException   {
		lwUserId = userId;
		lwPassword = password;
		lwRealm=realm;

		authenticate();
	}

	public String getUserId(){
		return lwUserId;
	}


	public String getUserRealm() {
		return lwRealm;
	}

	public String getSessionId() {
		return lwSessionId;
	}

	public String getContextUserString()
	{
		//return lwSm.getContextUser().toString();
		return lwUserId;
	}

	public void setSecurityManager(final SecurityManager sm){
		lwSm = sm;
	}

	public SecurityManager getSecurityManager(){
		return lwSm;
	}

	public void authenticate() throws  C30SDKKenanFxCoreException{
		lwSessionId=UUID.randomUUID().toString();
		lwSm=null;
		
		//Removing Security server authentication
		
		//	try {
		//	lwSessionId=UUID.randomUUID().toString();
		//lwSm=null;
		//lwSm = SecurityManagerFactory.createSecurityManager(lwRealm, lwUserId, lwSessionId, lwPassword);
		//lwSm.authenticate();
		//}  
		//catch (Exception e) {
		//	throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()),e,e.getErrorCode());
		//}
	}
	/**
	 * Logs out of the SecurityManager if this object has a security manager.  
	 */
	public final void logout() throws C30SDKObjectConnectionException{

		/*if (lwSm != null) {
			try{
				SecurityManagerFactory.cleanupSecurityManager(lwSm);  
				lwSm = null;
			}    catch (Exception e) {
				String error = "There was an error while trying to logout the security manager.\n";
				log.error(e);

				throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-016"),e,"CONN-016");
			}
		}*/
		lwSm = null;
	}


}
