package com.cycle30.sdk.core.framework;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.message.C30SDKMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * This is the  object that encapsulates a piece of "work" that is to be performed.
 * The work could be a single action like creating an order.  It could also have multiple actions
 * such as creating an order, adding a payment and emailing someone that the order is complete.
 * @author anst01
 */
public class C30SDKTransaction extends C30SDKObject {

    private static Logger log = Logger.getLogger(C30SDKTransaction.class);

    /**
     * Creates a new instance of C30SDKTransaction.
     * @param factory the factory which is the parent of this object.
     * @param objectType Type of  transaction being constructed
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SDKTransaction(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    

    /** 
     * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
     * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
     * the framework to create objects.  It is also used by the BRE front end.
     * @param factory Factory used to create C30ServiceOrder objects
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SDKTransaction(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30SDKTransaction.class);
    }
    
    /**
     * Method that processes the  transaction.
     * @return the result of processing the  object
     */
	@Override
	public final C30SDKObject process() {
    	log.debug("Starting C30SDKTransaction.process");
		C30SDKObject lwo = null;
		C30SDKObject tmpLwo = null;
		try {
			lwo = this.getFactory().createC30SDKObject(C30SDKCollection.class, new Object[0]);
			for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
				tmpLwo = (this.getC30SDKObject(i)).process();
	    		if (tmpLwo instanceof C30SDKMessage) {
	    			lwo = tmpLwo;
	    			break;
	    		} else {
	    			lwo.addC30SDKObject(tmpLwo);
	    		}
	    	}
    		lwo.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}
    	log.debug("Finished C30SDKTransaction.process");
    	return lwo;
	}

    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {
    	
    }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
