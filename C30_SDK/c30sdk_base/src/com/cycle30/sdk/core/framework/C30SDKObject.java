/*
 * C30SDKObject.java
 *
 * Created on December 12, 2006, 9:32 AM
 *
 */

package com.cycle30.sdk.core.framework;



import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.csgsystems.aruba.connection.ServiceException;
import com.csgsystems.bali.connection.ApiMappings;
import com.csgsystems.fx.security.util.FxException;
import com.cycle30.sdk.cache.C30SDKCacheableWrapper;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.attribute.C30SDKAttributeControl;
import com.cycle30.sdk.core.attribute.C30SDKAttributeControlComparator;
import com.cycle30.sdk.core.attribute.C30SDKEnumeration;
import com.cycle30.sdk.core.attribute.C30SDKSystemAttribute;
import com.cycle30.sdk.core.message.C30SDKBREMessage;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30KenanMWDataSource;
import com.cycle30.sdk.datasource.C30TableDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.extension.C30TransactionBREExtension;
import com.cycle30.sdk.object.extension.C30TransactionExtension;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * The abstract base class for all C30SDK objects.
 * @author John Reeves, Ranjith Nelluri
 */
public abstract class C30SDKObject implements Cloneable {

	private static Logger log = Logger.getLogger(C30SDKObject.class);

	protected static final Integer[] ATTRIB_TYPES = {C30SDKValueConstants.ATTRIB_TYPE_INPUT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT, C30SDKValueConstants.ATTRIB_TYPE_SYSTEM};
	public  final Integer[] CHANGEABLE_ATTRIB_TYPES = {C30SDKValueConstants.ATTRIB_TYPE_INPUT, C30SDKValueConstants.ATTRIB_TYPE_SYSTEM};


	private BSDMSessionContext userContextofLWObject;

	/** Collection of C30SDK objects contained in this object. */
	private ArrayList c30sdkObjects;

	/** Factory used to create the C30SDK objects in the collection. */
	private C30SDKFrameworkFactory factory;
	private C30SDKUser user;
	private Integer acctSegId ;
	

	/** Attributes of the data in the C30SDK object. */
	private HashMap inputAttributes;

	/**  for Account updates  */
	protected  HashMap updatedInputAttributes = new HashMap();

	/** System attributes of a C30SDK object. */
	private HashMap systemAttributes;

	/** Output attributes of a C30SDK object. */
	private HashMap outputAttributes;

	/** External names of a C30SDK object. */
	private Map nameValuePairs;
	private boolean hasExtData = false;

	public C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	public C30SDKPropertyFileManager KenanExceptionResourceBundle = C30SDKPropertyFileManager.getInstance();


	private static boolean firstReconnetAttempt=true;
	private String preValidateClassId="";
	private String brePreValidateClassId ="";
	private String postEnforcerClassId="";
	private String brePostEnforcerClassId="";

	//======================================================================================================================
	// C30SDK object constructors
	//======================================================================================================================

	/** Creates a new instance of C30SDKObject.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @param objectType the type of object being created by the factory.  This type indicates what configuration the object is to use.
	 * @throws C30SDKInvalidAttributeException if an exception is thrown while trying to initialize the objects attributes.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 * @throws C30SDKInvalidConfigurationException
	 */
	public C30SDKObject(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKInvalidAttributeException, C30SDKCacheException, C30SDKInvalidConfigurationException, C30SDKObjectException, C30SDKObjectConnectionException {
		this(factory);
		initializeObjectAttributes(objectType);
		loadAttributeDefaults();
		this.loadCache();
	}

	/**
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30SDKObject
	 * @param objectClass the class which will be used as the object type name for this object
	 * @throws C30SDKInvalidAttributeException if an exception is thrown while trying to initialize the objects attributes.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 * @throws C30SDKInvalidConfigurationException
	 */
	protected C30SDKObject(final C30SDKFrameworkFactory factory, final Class objectClass) throws C30SDKInvalidAttributeException, C30SDKCacheException, C30SDKInvalidConfigurationException, C30SDKObjectException, C30SDKObjectConnectionException {
		this(factory);

		//The following code takes the full class name and gets the final piece of the class name.
		// i.e. if the class name is "com.cycle30.sdk.C30SDK.object.kenan.C30SDKService" the type passed
		// through is "C30Service".  The reason we do this is because we are building the
		// object using a class name rather than an object type like the above constructor.
		String name = objectClass.getName();
		initializeObjectAttributes(name.substring(name.lastIndexOf(".")+1, name.length()));
		this.loadCache();
	}

	/**
	 * Base constructor which initializes most of the variables for the object.  This constructor is only ever called from within the
	 * C30SDKObject class and is therefore private and unavailable to users or developers.
	 * @param factory the factory that created the object.
	 * @throws C30SDKInvalidAttributeException if an exception is thrown while trying to initialize the objects attributes.
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 * @throws C30SDKInvalidConfigurationException
	 */
	private C30SDKObject(final C30SDKFrameworkFactory factory) throws C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKObjectException, C30SDKObjectConnectionException {
		this.factory = factory;
		this.inputAttributes = new HashMap();
		this.systemAttributes = new HashMap();
		this.outputAttributes = new HashMap();
		this.nameValuePairs = new HashMap();
		this.c30sdkObjects = new ArrayList();

	}

	/**
	 * Method counts the number of lwo associated with this lwo: minimum is 1 (itself)
	 * @return  count of associated lwos
	 */
	public int lwoCount() {
		return (1 + getC30SDKObjectCount());
	}

	/**
	 * Internal method which initializes all attributes including base C30SDK object attributes.  This method
	 * is called for every single instantiation of a C30SDK object.
	 * @param objectType the object type being initialized.  This value is always available to the outside world.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during processing.
	 */
	private void initializeObjectAttributes(final String objectType) throws C30SDKInvalidAttributeException {

		//===================
		// Data Attributes
		//===================
		C30SDKAttribute attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, String.class);
		attribute.setOverrideable(false);
		attribute.setRequired(true);
		attribute.setValue(objectType);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKObject.class);
		attribute.setExternalName(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.class);
		attribute.setExternalName(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED);
		attribute.setOverrideable(false);
		attribute.setValue(Boolean.FALSE);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, String.class);
		attribute.setValue("");
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE, Date.class);
		attribute.setValue(new Date());
		attribute.setFormat("MM/dd/yyyy HH:mm:ss");
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, String.class);
		attribute.setValue("");
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		//===================
		// Output Attributes
		//===================
		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, String.class);
		attribute.setValue(objectType);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, String.class);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE, String.class);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, String.class);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PERSIST_INTERNAL_ID, Integer.class);
		attribute.setValue(C30SDKValueConstants.IS_NOT_PERSISTED);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		initializeAttributes();
	}

	public C30SDKUser getUser() {
		return user;
	}

	public Integer getAcctSegId() {
		return acctSegId;
	}

	public void setAcctSegId(Integer acctSegId) {
		this.acctSegId = acctSegId;
	}
	/*public void setUser(C30SDKUser user,BSDMSessionContext context ) {
		this.user = user;
		this.userContextofLWObject = context;

	}*/

	/**
	 *  the method overloads clone method for internal generation of peer C30SDK objects,
	 *  designed specifically for retrieval operations.
	 * @param target
	 */
	public void clone(C30SDKObject target)
	{
		Object name = null;
		C30SDKObject copy = null;

		target.inputAttributes = (HashMap)inputAttributes.clone();
		for(Iterator i = inputAttributes.keySet().iterator(); i.hasNext(); target.inputAttributes.put(name, ((C30SDKAttribute)inputAttributes.get(name)).clone()))
			name = i.next();

		target.systemAttributes = (HashMap)systemAttributes.clone();
		for(Iterator i = systemAttributes.keySet().iterator(); i.hasNext(); target.systemAttributes.put(name, ((C30SDKSystemAttribute)systemAttributes.get(name)).clone()))
			name = i.next();

		target.outputAttributes = (HashMap)outputAttributes.clone();
		for(Iterator i = outputAttributes.keySet().iterator(); i.hasNext(); target.outputAttributes.put(name, ((C30SDKAttribute)outputAttributes.get(name)).clone()))
			name = i.next();

		//target.nameValuePairs = (HashMap)nameValuePairs.clone();
		target.nameValuePairs = (HashMap)inputAttributes.clone();
		for(Iterator i = nameValuePairs.keySet().iterator(); i.hasNext(); target.nameValuePairs.put(name, ((C30SDKAttribute)nameValuePairs.get(name)).clone()))
			name = i.next();

		target.c30sdkObjects = (ArrayList)c30sdkObjects.clone();
		for(int k = 0; k < getC30SDKObjectCount(); k++)
			target.c30sdkObjects.set(k, getC30SDKObject(k).clone());

	}

	/*	public BSDMSessionContext getUserContextofLWObject() {
		return userContextofLWObject;
	}

	public void setUserContextofLWObject(BSDMSessionContext userContextofLWObject) {
		this.userContextofLWObject = userContextofLWObject;
	}*/

	public String getPreValidateClassId() {
		return preValidateClassId;
	}

	public void setPreValidateClassId(String preValidateClassId) {
		this.preValidateClassId = preValidateClassId;
	}

	public String getBrePreValidateClassId() {
		return brePreValidateClassId;
	}

	public void setBrePreValidateClassId(String brePreValidateClassId) {
		this.brePreValidateClassId = brePreValidateClassId;
	}

	public String getPostEnforcerClassId() {
		return postEnforcerClassId;
	}

	public void setPostEnforcerClassId(String postEnforcerClassId) {
		this.postEnforcerClassId = postEnforcerClassId;
	}

	public String getBrePostEnforcerClassId() {
		return brePostEnforcerClassId;
	}

	public void setBrePostEnforcerClassId(String brePostEnforcerClassId) {
		this.brePostEnforcerClassId = brePostEnforcerClassId;
	}



	/**
	 * Creates a copy of the C30SDKObject object.
	 * @return A copy of the C30SDKObject object
	 */
	public Object clone() {
		Object name = null;
		C30SDKObject copy = null;
		try {
			copy = (C30SDKObject) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}

		//---------------------------
		// Clone the DATA attributes
		//---------------------------
		copy.inputAttributes = (HashMap) this.inputAttributes.clone();
		Iterator i = inputAttributes.keySet().iterator();
		while (i.hasNext()) { name = i.next(); copy.inputAttributes.put(name, ((C30SDKAttribute) inputAttributes.get(name)).clone()); }

		//-----------------------------
		// Clone the SYSTEM attributes
		//-----------------------------
		copy.systemAttributes = (HashMap) this.systemAttributes.clone();
		i = this.systemAttributes.keySet().iterator();
		while (i.hasNext()) { name = i.next(); copy.systemAttributes.put(name, ((C30SDKSystemAttribute) this.systemAttributes.get(name)).clone()); }

		//-----------------------------
		// Clone the OUTPUT attributes
		//-----------------------------
		copy.outputAttributes = (HashMap) this.outputAttributes.clone();
		i = this.outputAttributes.keySet().iterator();
		while (i.hasNext()) { name = i.next(); copy.outputAttributes.put(name, ((C30SDKAttribute) this.outputAttributes.get(name)).clone()); }

		//----------------------
		// clone the PARAMETERS
		//----------------------
		copy.nameValuePairs = (HashMap) this.inputAttributes.clone();
		i = this.nameValuePairs.keySet().iterator();
		while (i.hasNext()) { name = i.next(); copy.nameValuePairs.put(name, ((C30SDKAttribute) this.nameValuePairs.get(name)).clone()); }

		//---------------------------------------------------
		// clone the array list of child C30SDK objects
		//---------------------------------------------------
		copy.c30sdkObjects = (ArrayList) ((ArrayList) this.c30sdkObjects).clone();
		for (int k = 0; k < this.getC30SDKObjectCount(); k++) {
			copy.c30sdkObjects.set(k, ((C30SDKObject) this.getC30SDKObject(k)).clone());
		}

		return copy;
	}

	//======================================================================================================================
	// Starting all parameter methods
	//======================================================================================================================

	//======================================================================================================================
	// Methods below are all for ALL attributes of C30SDK objects
	//======================================================================================================================


	/**
	 * Loads the attribute defaults of the C30SDK objet from the database.  This includes all
	 * data, system, search and output types.
	 * @throws C30SDKInvalidAttributeException (1003) if the attributes can't be loaded.
	 */
	private void loadAttributeDefaults() throws C30SDKInvalidAttributeException {
		factory.loadAttributeDefaults(this);
	}

	//Loads the configurated Class Field Configuration to set Field Attributes and Filtering
	protected final void setClassFieldConfiguration()
	throws  C30SDKInvalidAttributeException
	{
		factory.setClassFieldConfiguration(this,null);
	}

	// jcw: added for LWO Find Enhancements : for removing Order Type attributes
	protected final void removeAttribute(String attributeName, Integer type)
	throws C30SDKInvalidAttributeException
	{
		if (getAttributes(type).containsKey(attributeName))
			getAttributes(type).remove(attributeName);

	}

	// jcw: added for LWO Find Enhancements : for removing Order Type attributes
	public final boolean isAttribute(String attributeName, Integer type)
	throws C30SDKInvalidAttributeException
	{

		if (getAttributes(type).containsKey(attributeName))
			return true;
		else
			return false;

	}

	/**
	 * Adds a C30SDK attribute to the objects attributes.
	 * @param type the type of attribute being processed.
	 * @param attribute the C30SDK attribute to add
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	protected final void addAttribute(C30SDKAttribute attribute, final Integer type) throws C30SDKInvalidAttributeException  {
		this.getAttributes(type).put(attribute.getName(), attribute);
		//if (attribute.getExternalName() != null && !attribute.getExternalName().equals("")) {
		//    this.addParameter(attribute.getExternalName(), "");
		//}

		// if the attribute is an OUTPUT attribute then
		// do not allow it to be overridden
		if (type.equals(C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) {
			attribute.setOverrideable(false);
		}
	}

	/**
	 * Adds a group of C30SDK dataAttributes to the map of dataAttributes.
	 * @param attributeMap the C30SDK dataAttributes to add
	 * @param type the type of attribute being processed.
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	protected final void addAttributes(final HashMap attributeMap, final Integer type) throws C30SDKInvalidAttributeException {
		this.getAttributes(type).putAll(attributeMap);
	}



	/**
	 * Gets the C30SDK attribute corresponding to the attribute name.
	 * @param attributeName name of the C30SDK attribute to retrieve
	 * @param type the type of attribute being processed.
	 * @throws C30SDKInvalidAttributeException if the attribute name is not defined for this object
	 * @return the C30SDK attribute corresponding to the attribute name
	 */
	public final C30SDKAttribute getAttribute(final String attributeName, final Integer type) throws C30SDKInvalidAttributeException {
		C30SDKAttribute attribute = null;
		if (this.getAttributes(type).containsKey(attributeName)) {
			attribute = (C30SDKAttribute) this.getAttributes(type).get(attributeName);
		} else {
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-004")+ " " + attributeName + " " + type,"INVALID-ATTRIB-004");
		}

		return attribute;
	}

	/**
	 * Gets the value of the C30SDK attribute given its type.
	 * @param attributeName name of the C30SDK attribute to retrieve
	 * @param type the type of attribute being processed.
	 * @throws C30SDKInvalidAttributeException if the attribute name is not defined for this object
	 * @return the value of the C30SDK attribute corresponding to the attribute name
	 */
	public Object getAttributeValue(final String attributeName, final Integer type) throws C30SDKInvalidAttributeException {
		Object value = null;
		if (this.getAttributes(type).containsKey(attributeName)) {
			value = this.getAttribute(attributeName, type).getValue();
		}
		return value;
	}

	/**
	 * Gets the value of the C30SDK attribute.  Note that this method can only be used AFTER
	 * the object has been processed. i.e. the process() method has been applied to the object.
	 * @param attributeName name of the C30SDK attribute to retrieve
	 * @throws C30SDKInvalidAttributeException if the attribute name is not defined for this object
	 * @return the value of the C30SDK attribute corresponding to the attribute name
	 */
	public Object getAttributeValue(final String attributeName) throws C30SDKInvalidAttributeException {
		Object value = null;
		boolean hasBeenSet = false;

		//object type should always be available.
		if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE)) {
			value = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		} else {

			//if we have persisted an object then make some attributes available.
			if (!this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).equals(C30SDKValueConstants.IS_NOT_PERSISTED)) {

				if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP) || attributeName.equals(C30SDKAttributeConstants.ATTRIB_PERSIST_INTERNAL_ID)
						|| attributeName.equals(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID) || attributeName.equals(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE)) {
					value = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					hasBeenSet = true;
				}
			}

			//if we still have not set the value
			if (!hasBeenSet) {

				//if we have processed an object then make all attributes available.
				if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(Boolean.FALSE)) {
					throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-005") + attributeName,"INVALID-ATTRIB-005");
				} else {

					if (this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).containsKey(attributeName)) {
						value = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					}

				}

			}

		}

		return value;
	}

	/**
	 * Method to get an attribute based on the attribute name of the value being retrieved as well as the class the attribute is associated to.
	 * This method is a convenience method so that the object tree does not need to be traversed in code in order to retrieve attribute values.
	 * @param clazz the class that holds the attribute value.
	 * @param attributeName the attribute name of the value being retrieved.
	 * @param type whether the attribute is an input or output attribute type.
	 * @return the attribute value or NULL if no value could be retrieved.
	 * @throws C30SDKInvalidAttributeException if an exception is thrown while trying to retrieve the attributes value.
	 */
	protected Object getAttributeValue(final Class clazz, final String attributeName, final Integer type) throws C30SDKInvalidAttributeException {
		Object attribValue = null;
		C30SDKObject objectParent = (C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		//make sure we are not at the top of the tree
		if (objectParent != null) {

			if (objectParent.getClass().isAssignableFrom(clazz) || objectParent.getClass().getSuperclass().equals(clazz)) {
				attribValue = objectParent.getAttributeValue(attributeName, type);
			} else {
				attribValue = objectParent.getAttributeValue(clazz, attributeName, type);
			}
		}

		return attribValue;
	}

	/**
	 * Gets the string value of the C30SDK attribute.  Note that this method can only be used
	 * AFTER the object has been processed. i.e. the process() method has been applied to the object.
	 * @param attributeName name of the C30SDK attribute to retrieve
	 * @throws C30SDKInvalidAttributeException if the attribute name is not defined for this object
	 * @return the string value of the C30SDK attribute corresponding to the attribute name
	 */
	public final String getAttributeValueAsString(final String attributeName) throws C30SDKInvalidAttributeException {
		String value = null;
		if (this.getAttributeValue(attributeName) != null) {
			value = this.getAttributeValue(attributeName).toString();
		}
		return value;
	}

	/**
	 * Method to get the output parameters for the C30SDK object.  This method would usually be called
	 * after the object has been processed (lwo.process()) and the results have been populated and returned
	 * @return a map containing attribute keys and their values.
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	public final Map getAttributes() throws C30SDKInvalidAttributeException {
		HashMap outputParams = new HashMap();

		//check to make sure that object has been processed.
		if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue().equals(Boolean.FALSE)) {
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-005") ,"INVALID-ATTRIB-005");
		} else {

			Map childOutputParams = null;
			Iterator iter = outputAttributes.keySet().iterator();
			String key = "";

			String className = this.getClass().getName();

			outputParams.put("C30SDKObjectClass", className.substring(className.lastIndexOf(".")+1));
			while (iter.hasNext()) {
				key = (String) iter.next();
				outputParams.put(key, this.getAttributeValue(key));
			}
			for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
				childOutputParams = this.getC30SDKObject(i).getAttributes();
				outputParams.put("C30SDKObjectChild-"+i, childOutputParams);
			}

		}
		return outputParams;
	}

	/**
	 * Indicates if an attribute value has been set.
	 * @param attributeName name of C30SDK attribute
	 * @param type the type of attribute being processed.
	 * @return boolean indicating if the attribute value has been set
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	public final boolean isAttributeValueSet(final String attributeName, final Integer type) throws C30SDKInvalidAttributeException {
		C30SDKAttribute attribute = (C30SDKAttribute) this.getAttributes(type).get(attributeName);
		boolean isSet = false;
		if (attribute.getValue() != null) {
			isSet = true;
		}
		return isSet;
	}

	/**
	 * Sets the value of an attribute.
	 * @param attributeName the attribute name who's value is to be set.
	 * @param attributeValue  the value the attribute is to be set to.
	 * @param type the type of attribute being processed.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the service order.
	 */
	public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
		if (this.getAttributes(type).containsKey(attributeName)) {
			C30SDKAttribute attribute = (C30SDKAttribute) this.getAttributes(type).get(attributeName);
			attribute.setValue(attributeValue);
		} else {
			String attrType = "";
			if (type.equals(C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
				attrType = "input";
			} else if (type.equals(C30SDKValueConstants.ATTRIB_TYPE_SYSTEM)) {
				attrType = "system";
			} else if (type.equals(C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) {
				attrType = "output";
			}

			String error = "The " + attrType + " attribute '" + attributeName + "' is not defined for this object.\n";
			error = error + "Ensure that the attribute is available given the factory version. (" + factory.getAPIVersion() + ")";
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-006")+ attrType + "/"+ attributeName,"INVALID-ATTRIB-006");

		}
	}

	/**
	 * This method takes all parameters that have been set and sets the corresponding attributes with the parameters value.
	 * @throws C30SDKInvalidAttributeException if there was a problem setting the parameter information for the attributes.
	 */
	protected final void setAttributesFromNameValuePairs() throws C30SDKInvalidAttributeException {
		String paramName, pairName, paramExtName = null;
		Object pairValue = null;
		Iterator pairIterator = null;
		Iterator paramIterator = null;
		for (int i = 0; i < CHANGEABLE_ATTRIB_TYPES.length; i++) {
			paramIterator = this.getAttributes(CHANGEABLE_ATTRIB_TYPES[i]).keySet().iterator();
			while (paramIterator.hasNext()) {
				paramName = (String) paramIterator.next();
				C30SDKAttribute attr = (C30SDKAttribute)this.getAttributes(CHANGEABLE_ATTRIB_TYPES[i]).get(paramName);
				paramExtName = attr.getExternalName();
				pairIterator = this.nameValuePairs.keySet().iterator();
				while (pairIterator.hasNext()) {
					pairName = (String) pairIterator.next();
					if(pairName.equals(paramExtName)) {
						pairValue = ((C30SDKAttribute) this.nameValuePairs.get(pairName)).getValue();
						this.setAttributeValue(paramName, pairValue, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
						this.updatedInputAttributes.put(paramName,getAttribute(paramName,C30SDKValueConstants.ATTRIB_TYPE_INPUT));
					}
				}
			}
		}
	}


	/**
	 * the method validates Find activities:
	 *  Find request must have either Account External Id and Account External Id Type
	 *  or Account Internal Id  or  AccountServerId and Account Internal Id available as
	 *  input attributes.
	 * @throws C30SDKInvalidAttributeException
	 */
	protected void validateFindAttributes()
	throws C30SDKInvalidAttributeException
	{
		String  acctExternalId = null;
		Integer acctExternalIdType = null;
		Integer acctInternalId = null;


		if (isAttribute("Find",C30SDKValueConstants.ATTRIB_TYPE_INPUT))
			if((Boolean)getAttributeValue("Find", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
			{  
				if (isAttribute("AccountExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
					acctExternalId = (String)getAttributeValue("AccountExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				if (isAttribute("AccountExternalIdType",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
					acctExternalIdType = (Integer)getAttributeValue("AccountExternalIdType",C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				if (isAttribute("AccountInternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
					acctInternalId = (Integer)getAttributeValue("AccountInternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT); 

				if ((acctExternalId == null || acctExternalIdType == null) && acctInternalId == null) 
				{
					throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-007"),"INVALID-ATTRIB-007");
				}      
			}       
	}

	/**
	 * Validates that all the required dataAttributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
	 * @throws C30SDKKenanFxCoreException 
	 */
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
		log.debug("BEGIN C30SDKObject.validateAttributes ...");
		int errorCount = 0;
		String errorAttributes = "";
		String attributeName = null;
		C30SDKAttribute attribute = null;
		Iterator iterator = null;

		for (int i = 0; i < CHANGEABLE_ATTRIB_TYPES.length; i++) {
			iterator = this.getAttributes(CHANGEABLE_ATTRIB_TYPES[i]).keySet().iterator();
			while (iterator.hasNext()) {
				attributeName = (String) iterator.next();
				attribute = (C30SDKAttribute) this.getAttributes(CHANGEABLE_ATTRIB_TYPES[i]).get(attributeName);
				if (attribute.isRequired() && attribute.getValue() == null) {
					if (errorCount == 0) {
						errorAttributes = errorAttributes + attributeName + " (" + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ")";
					} else {
						errorAttributes = errorAttributes + ", " + attributeName + " (" + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ")";
					}
					errorCount++;
				}
			}
			if (errorCount > 0) {
				throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-008") + errorAttributes,"INVALID-ATTRIB-008");
			}
		}
		log.debug("END C30SDKObject.validateAttributes ...");
	}

	/**
	 * This method is only used for retrieving a list of Services.
	 * @param totalRecordsFound
	 * @param lastRecordReturned
	 * @throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException
	 */
	protected void setProcessingAggregateTotalsAttributes(C30SDKObject lwo, int totalRecordsFound, int lastRecordReturned, int totalRecordsReturned ) 
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException
	{
		boolean setTotalCount = false;
		boolean setLastRecordReturned = false;
		boolean setTotalReturned = false;
		if (lwo == null) 
			return;

		// for each record returned, set the values for total records found and last record returned.  
		if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_LAST_RECORD_RETURNED_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)){
			lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_LAST_RECORD_RETURNED_NUMBER,lastRecordReturned, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			setLastRecordReturned = true;
		}
		if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_COUNT,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)){
			lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_COUNT,totalRecordsFound, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			setTotalCount = true;
		}

		if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_RETURNED,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)){
			lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_RETURNED,totalRecordsReturned, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			setTotalReturned = true;
		}

		for (int i = 0; i < lwo.getC30SDKObjectCount(); i++) {
			if (setLastRecordReturned)
				lwo.getC30SDKObject(i).setAttributeValue(C30SDKValueConstants.ATTRIB_LAST_RECORD_RETURNED_NUMBER,lastRecordReturned, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT); 
			if (setTotalCount)    
				lwo.getC30SDKObject(i).setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_COUNT,totalRecordsFound, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);   
			if (setTotalReturned)    
				lwo.getC30SDKObject(i).setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_RETURNED,totalRecordsReturned, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);   
		}


	}


	/**
	 * Method to get the hashmap of attributes given the attribute type.
	 * @param type the type of attribute being retrieved.
	 * @return the attributes matching the type given.
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	public HashMap getAttributes(final Integer type) throws C30SDKInvalidAttributeException {
		HashMap attribs = null;
		if (type.equals(C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
			attribs = inputAttributes;
		} else if (type.equals(C30SDKValueConstants.ATTRIB_TYPE_SYSTEM)) {
			attribs = systemAttributes;
		} else if (type.equals(C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) {
			attribs = outputAttributes;
		} else {

			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-009") + type,"INVALID-ATTRIB-009");

		}
		return attribs;
	}

	//======================================================================================================================
	// Methods below are all for the INPUT attributes of C30SDK objects
	//======================================================================================================================

	/**
	 * Loads the extended data attributes associated with this object.  In real terms this method is
	 * performing a SQL JOIN operation on the EXT_DATA_TYPE_ASSOC and PARAM_DEF tables in the database.
	 * This is so that the correct parameter type is associated with each extended data parameter.
	 * @param tableBaseName base name of table (ex - ORD_ORDER, CMF etc)
	 * @param entityType the entity type of the object. This is important, since different objects can
	 * be of different types and these types help dictate what extended data attributes are available.
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	protected final void loadExtendedDataAttributes(final String tableBaseName, final Integer entityType) throws C30SDKInvalidAttributeException {
		HashMap attributes = factory.getExtendedDataAttributes(tableBaseName, entityType);
		this.addAttributes(attributes, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttributes(attributes, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		if (attributes.size() > 0) {
			hasExtData = true;
		}
	}


	/**
	 * Loads the extended data attributes associated with this object.  In real terms this method is
	 * performing a SQL JOIN operation on the EXT_DATA_TYPE_ASSOC and PARAM_DEF tables in the database.
	 * This is so that the correct parameter type is associated with each extended data parameter.
	 * @param tableBaseName base name of table (ex - ORD_ORDER, CMF etc)
	 * @param entityType the entity type of the object. This is important, since different objects can
	 * be of different types and these types help dictate what extended data attributes are available.
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	protected final void loadAccountExtendedDataAttributes(final String tableBaseName, final Integer entityType) throws C30SDKInvalidAttributeException {
		HashMap attributes = factory.getAccountExtendedDataAttributes(tableBaseName, entityType);
		this.addAttributes(attributes, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttributes(attributes, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		if (attributes.size() > 0) {
			hasExtData = true;
		}
	}


	/**
	 * Method which determines whether the object being tested has extended data attributes.  Note that
	 * this method does NOT check the children of the object, only the object itself.
	 * @return whether the object has extended data attributes.  True = yes, False = no.
	 */
	protected final boolean hasExtendedDataAttributes() {
		return hasExtData;
	}

	//======================================================================================================================
	// Methods below are all for the NAME/VALUE PAIRS of all C30SDK objects
	//======================================================================================================================

	/**
	 * MEDIUM LEVEL DATA INPUT
	 * Set a name/value pair that can be used to initialize lw object attribute values.
	 * @param name the name of the name/value pair being set
	 * @param value the value of the name/value pair being set.
	 * @throws C30SDKInvalidAttributeException if there was a problem adding the name/value pair.
	 */
	public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {

		//There are a few things that happen when a name/value pair is added.
		//1.  The name/value pair is stored in the "parameter" list.  A list is required
		//    because if any new C30SDK objects are created during processing
		//    then those objects will also have a chance to set name/value pairs.
		//2.  All attributes that correspond to the name/value pair are set.
		//3.  The name/value pair is propagated through the children of this objects and
		//    all corresponding values are set.

		//1.  Name/value pair is stored.
		if (this.nameValuePairs.containsKey(name)) {
			this.nameValuePairs.remove(name);
		}

		C30SDKAttribute pair = new C30SDKAttribute(name, String.class);
		pair.setValue(value);
		this.nameValuePairs.put(pair.getName(), pair);

		//2.  Corresponding attributes of this object are set.
		Iterator keyIter = inputAttributes.keySet().iterator();
		C30SDKAttribute attribute = null;
		while (keyIter.hasNext()) {
			attribute = (C30SDKAttribute) inputAttributes.get(keyIter.next());
			if (attribute.getExternalName() != null
					&& attribute.getExternalName().equals(name)) {
				attribute.setValue(value);
				//  	break;
			}

		}

		//3.  Name/value pair is propagated through this objects children.
		// Any attributes designed specifically for C30SDKObject should NOT
		// be propagated to all children. It is just designed for this object.
		if (!name.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT)
				&& !name.equals(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED)
				&& !name.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE)) {

			//set the name/value pairs for each of the objects children
			for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
				this.getC30SDKObject(i).setNameValuePair(name, value);
			}
		}
	}



	/**
	 * MEDIUM LEVEL DATA INPUT
	 * Set a name/value pair that can be used to initialize lw object attribute values.
	 * @param name the name of the name/value pair being set
	 * @param value the value of the name/value pair being set.
	 * @param indices a string in "x,y,z" format where each number denotes an index that is to be traversed for setting the value.
	 * @throws C30SDKInvalidAttributeException if there was a problem adding the name/value pair.
	 */
	public void setNameValuePair(final String name, final Object value, final int[] indices) throws C30SDKInvalidAttributeException {

	}

	/**
	 * Set name value pairs that can be used to initialize lw object attribute values.
	 * @param nameValuePairMap the attribute name/value pairs that need to be set for this object
	 * @throws C30SDKInvalidAttributeException
	 */
	public final void setNameValuePairs(final Map nameValuePairMap) throws C30SDKInvalidAttributeException {
		this.nameValuePairs = nameValuePairMap;
	}




	/**
	 * Method to get the name/value pair mapping.
	 * @return the name/value pair map.
	 */
	protected final Map getNameValuePairMap() {
		return nameValuePairs;
	}

	/**
	 * MEDIUM LEVEL DATA INPUT
	 * Returns a list of all name/value pair that must be set in order to fulfill this LW request.
	 * This method is generally used by back end or server side applications.  UI applications
	 * should use the getNameValuePairControls() method instead. The returned set uses a comparator
	 * which returns the name/value pairs in load order.
	 * @return a set of light weight attributes in sort order.
	 * @throws C30SDKInvalidAttributeException if there was a problem setting the name/value pair information for the attributes.
	 */
	public final Map getNameValuePairs() throws C30SDKInvalidAttributeException {

		//first get a list of attributes
		Set attribSet = getNameValueAttributes();
		Iterator iter = attribSet.iterator();
		C30SDKAttribute attribute = null;
		Map attributes = new LinkedHashMap();

		//then get the name and value and put into a hashmap
		while (iter.hasNext()) {
			attribute = (C30SDKAttribute) iter.next();
			attributes.put(attribute.getExternalName(), attribute.getValue());
		}

		return attributes;
	}

	/**
	 * This is an internal method which returns all attributes that have a name/value pair.
	 * @return
	 * @throws C30SDKInvalidAttributeException
	 */
	private final Set getNameValueAttributes()throws C30SDKInvalidAttributeException {
		Set attributes = new TreeSet();

		// Add name/value pairs for all children objects
		C30SDKObject lwo = null;
		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			lwo = this.getC30SDKObject(i);
			attributes.addAll(lwo.getNameValueAttributes());
		}

		Iterator paramIterator = null;
		C30SDKAttribute attribute = null;
		String paramName = null;
		String paramExtValue = null;

		// Add the external name parameters to Hashmap for the current object
		for (int i = 0; i < CHANGEABLE_ATTRIB_TYPES.length; i++) {
			paramIterator = this.getAttributes(CHANGEABLE_ATTRIB_TYPES[i]).keySet().iterator();
			while (paramIterator.hasNext()) {
				paramName = (String) paramIterator.next();
				attribute = (C30SDKAttribute) this.getAttributes(CHANGEABLE_ATTRIB_TYPES[i]).get(paramName);
				paramExtValue = attribute.getExternalName();

				//make sure we have an external name and its not ObjectParent or IsProcessed
				if (paramExtValue != null && !paramExtValue.equals("")
						&& !paramExtValue.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT) && !paramExtValue.equals(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED)) {
					attributes.add(attribute);
				}
			}
		}

		return attributes;
	}


	public final Map getOutputNameValuePairs() throws C30SDKObjectException, C30SDKInvalidAttributeException {
		Map outputAttribs = new HashMap();
		this.getOutputNameValuePairs(outputAttribs);
		return outputAttribs;
	}

	/**
	 * MEDIUM/HIGH LEVEL DATA OUTPUT
	 * Returns a list of all name/value pairs that have been configured.  Note that in order for this 
	 * method to return information the object must have been processed.
	 * @return a map of name/value pairs which have been populated through processing.
	 * @throws C30SDKInvalidAttributeException if there was a problem getting the name/value pair information from the attributes.
	 */
	protected final Map getOutputNameValuePairs(Map outputAttribs) throws C30SDKObjectException, C30SDKInvalidAttributeException {


		// Get output attributes for all child objects
		C30SDKObject lwo;
		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			lwo = this.getC30SDKObject(i);
			outputAttribs.putAll(lwo.getOutputNameValuePairs());
		}

		//make sure the object has been processed.
		if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue().equals(Boolean.FALSE)) {
			throw new C30SDKObjectException(this.getFactory().exceptionResourceBundle.getString("LW-OBJ-001"),"LW-OBJ-001");
		} else {
			Iterator iter = outputAttributes.keySet().iterator();
			C30SDKAttribute attribute = null;
			String attribName = null;
			//go through each output attribute
			while (iter.hasNext()) {
				attribName = (String) iter.next();
				attribute = (C30SDKAttribute) outputAttributes.get(attribName);

				//if the attribute has an external name then it should be returned.
				if (attribute.getExternalName() != null) {
					outputAttribs.put(attribute.getExternalName(), attribute.getValue());
				}
			}
		}

		return outputAttribs;
	}

	/**
	 * HIGH LEVEL DATA INPUT.
	 * Returns a set of UI control information for all name/value pairs that must be set in order to
	 * fulfill this LW request.  This method is only used by front end UI applications.  Back end or
	 * server side applications must use the getNameValuePairs() method instead. The returned set uses
	 * a comparator which returns the attributes controls in sort order.
	 * @return a set of light weight parameters in sort order.
	 * @throws C30SDKInvalidAttributeException if there was a problem setting the parameter attributes.
	 */
	public final Set getNameValuePairControls() throws C30SDKInvalidAttributeException {
		Set parameters = new TreeSet(new C30SDKAttributeControlComparator());

		// Add external name parameters to input hashmap for all child objects
		C30SDKObject lwo;
		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			lwo = this.getC30SDKObject(i);
			parameters.addAll(lwo.getNameValuePairControls());
		}

		Iterator attribIterator = null;
		C30SDKAttributeControl parameter = null;
		// Add the attribute control to the set if it exists.
		for (int i = 0; i < CHANGEABLE_ATTRIB_TYPES.length; i++) {
			attribIterator = this.getAttributes(CHANGEABLE_ATTRIB_TYPES[i]).values().iterator();
			while (attribIterator.hasNext()) {
				parameter = ((C30SDKAttributeControl) attribIterator.next());
				if (parameter != null) {
					parameters.add(parameter);
				}
			}
		}

		return parameters;

	}

	//======================================================================================================================
	// Finished all name/value pair methods
	//======================================================================================================================

	/**
	 * Method that processes the C30SDK object.  This method is called to actually perform
	 * the function (or work) that the object has been created to do.  Before this method is
	 * called only configuration of the object can take place.  After the method is called
	 * attributes of the object can be retrieved using #link getAttributeValue(String).
	 * @return a C30SDK object containing output attributes returned after processing.
	 * @throws C30SDKInvalidAttributeException if an exception is thrown while trying to process the objects attributes.
	 * @throws C30SDKException 
	 */
	public abstract C30SDKObject process() throws C30SDKException;

	/**
	 * Abstract method that creates the C30SDK object.  This exists so that all
	 * C30SDK objects can adhere to the same interface so they fit into the Kenan
	 * Search architecture.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during processing.
	 */
	protected abstract void initializeAttributes() throws C30SDKInvalidAttributeException;

	/**
	 * Abstract method which allows objects to get and set cached information into the framework
	 * for later use if the data is required.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	protected abstract void loadCache() throws C30SDKCacheException;



	/**
	 * the method determines if the Account ServerId Cache can be set directly from 
	 * using the input attributes rather than retriving an Account object. Lwo Enhancement
	 * @return true if can be cached.
	 * @throws C30SDKInvalidAttributeException
	 */
	protected boolean canServerIdCacheBeSetLocally()   
	throws C30SDKInvalidAttributeException
	{
		if ((isAttribute("AccountServerId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue("AccountServerId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)  && 
				( isAttribute("AccountInternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue("AccountInternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)  )
			return true;

		return false;
	}

	/**
	 * Method to set Account Server Id, either from previous call to getAccount or directly from 
	 * input attributes
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	protected void setAccountServerId()
	throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException
	{
		// set the Account Server Id for this object
		Integer accountServerId = null;
		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
		{ 
			accountServerId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}
		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){

			if (accountServerId == null)
				accountServerId = getAccountServerId();
			if (accountServerId != null)
				setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,accountServerId,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		} 
	}

	/**
	 * Method to set the AccountServerIdCache directly rather than as a result of retrieving Account object.
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	protected void setAccountServerIdCache()
	throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException
	{
		// set the Account Server Id for this object
		Integer accountServerId = null;
		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
		{ 
			accountServerId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}

		if (accountServerId != null)
		{
			C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>("ACCOUNT_SERVER_ID_CACHE", accountServerId);
			this.factory.c30SdkFactoryCache.set(cacheWrapper);	
		}


		if (accountServerId != null){
			if (!isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
				addAttribute( new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,accountServerId,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		}

	}






	/**
	 * Method which allows C30SDK objects to create light weight messages which are themselves
	 * exceptions.  This is to allow a cleaner return to the user in case of error.
	 * @param exception the exception that is being thrown by the framework.
	 * @return an LWO object that contains the exception that must be resolved.
	 */
	protected final C30SDKExceptionMessage createExceptionMessage(final Throwable exception) {

		C30SDKExceptionMessage lwo = null;
		try {
			//create the C30SDK message
			lwo = (C30SDKExceptionMessage) factory.createC30SDKObject(C30SDKExceptionMessage.class, new Object[0]);

			//set the exception into the message
			lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_EXCEPTION, exception, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			lwo.process();

		} catch (Exception e) {
			log.error(e);
		}

		return lwo;
	}

	/**
	 * <p>Provides business rule validation of a C30SDK object using the BRE architecture.</p>
	 *
	 * The following is a rundown of what the process is when validation occurs:
	 * <ol>
	 * <li>The object being validated along with all its child objects are serialized.
	 *     This is done using the serializeObject() method.
	 * <li>The serialized string is then broken down into payloads.  This is done using the
	 *     chunkValidationRequest() method.  Payloads can only be 2000 characters long and
	 *     the last payload per request must contain entire objects.  So, the requirement is
	 *     that the last payload should be shorter, rather than contain an incomplete object.
	 *     This means that every 16th payload may be slightly shorter than 2000 characters.
	 * <li>Once the payloads are created they are sent to the DB.  Due to the fact that it
	 *     is possible that more payloads exist than can be sent down to the stored procedure
	 *     in one call two stored proc calls are available.  The first simply sends payloads
	 *     to the DB, the second sends a payload to the database as well as requests a
	 *     validation.  In order for a validation to take place the second stored proc must
	 *     be called at least once for each validation request.
	 * <li>When the request is validated if there are any messages in the response an LWO
	 *     collection of BREMessageRule objects are created.
	 * </ol>
	 * @param majorStepId the validation major step ID.  The step ID is used to identify what validations need to take place.
	 * @param minorStepId the validation minor step ID.
	 * @param userId the user making the validation request.
	 * @return a LWO which is a C30SDKCollection holding C30SDKObjects which are BusinessRuleMsg objects.
	 * @throws Exception 
	 */
	public final C30SDKObject validate(final Integer majorStepId, final Integer minorStepId, final String userId) throws Exception {
		log.debug("Starting validate");


		//---------------------------------------------------------------------------
		//0.  We need to set the name/value pairs before we can serialize the object
		//---------------------------------------------------------------------------
		setAttributesFromNameValuePairs();

		//-------------------------
		//1.  serialize the object
		//-------------------------
		String serializedObject = this.serializeObject("1", "1").toString();

		//-----------------------------------------
		//2.  chunk the serialization as necessary
		//-----------------------------------------
		List payloads = this.chunkValidationRequest(serializedObject);

		//-------------------------------------------------------------------------------
		//3.  create request and validate
		//    if there are LESS than 16 payloads then send just one validation.
		//    if there are MORE than 16 payloads then we need to send multiple requests.
		//-------------------------------------------------------------------------------
		C30KenanMWDataSource dataSource = null;
		if (payloads.size() < 16) {
			log.debug("About to perform single request validation");
			//dataSource = singleRequestValidation(majorStepId, minorStepId, userId, payloads);
		} else {
			log.debug("About to perform multiple request validation");
			//dataSource = multipleRequestValidation(majorStepId, minorStepId, userId, payloads);
		}

		//----------------------------
		//4.  create the LWO messages
		//----------------------------
		C30SDKCollection msgList = createValidationResponse(dataSource);
		return msgList;
	}

	public String getSerializeType() throws C30SDKInvalidAttributeException {
		String objType = this.getClass().getName();
		return "\"" + objType.substring(objType.lastIndexOf("C30SDK")+11, objType.length()) + "\"";
	}

	/**
	 * Method to create the validation payload which is used by the rules engine to validate the object.
	 * @param objectId the object ID that is used to uniquely identify this object.
	 * @param parentObjectId the object ID of the parent of this object
	 * @return a quasi-xml based string which describes the object.
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */
	protected StringBuffer serializeObject(final String objectId, final String parentObjectId) throws C30SDKInvalidAttributeException {

		String objType = getSerializeType();

		//---------------------------
		// Create the payload header
		//---------------------------
		StringBuffer payload = new StringBuffer();
		payload.append("<OBJ Type=");
		payload.append(objType);
		payload.append(" ObjId=\"");
		payload.append(objectId.toString());
		payload.append("\" ParId=\"");
		payload.append(parentObjectId.toString());
		payload.append("\">\n");

		//serializedStrings.add(payload);

		//-----------------------------------
		// Add all attributes to the payload
		//-----------------------------------
		String attributeName = null;
		Object attributeValue = null;
		StringBuffer attribString = null;
		Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
		while (iterator.hasNext()) {
			attributeName = (String) iterator.next();
			attributeValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			if (!(attributeValue instanceof C30SDKObject)) {
				attribString = new StringBuffer();

				if (attributeValue != null) {
					attribString.append("<ATR Name=\"");
					attribString.append(attributeName);
					attribString.append("\" Val=\"");
					attribString.append(attributeValue);
					attribString.append("\"/>\n");

				}
				payload.append(attribString);
			}

		}

		payload.append("</OBJ>\n");

		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			payload.append(this.getC30SDKObject(i).serializeObject(objectId + "." + new Integer(i+1).toString(), objectId));
		}

		return payload;
	}

	/**
	 * Method that takes the payload that represents the request used for validating the object and chunks it up
	 * into smaller pieces for use when validating the object.  The reason it is chunked up is because the stored
	 * procedure used for validation can only take 2000 byte parameters as input.  And, because we send the whole
	 * object in one payload to save network traffic we have to chunk the request up.
	 *
	 * The method also ensures that payloads are chunked up so that when requests are sent to the server that whole
	 * objects are sent.  It is not possible to send a single object in more than one request.  The method does this
	 * by taking 30000 character chunks, finding the last index of the tag and chunking the remainder up. So, if there
	 * are 50 payloads that need to be sent then we can be sure that the 15th and 45th payloads end with the tag.
	 * @param entirePayload the entire object string that is to be chunked up.
	 * @return the list of strings which together hold the entire object represented as a string.
	 */
	private ArrayList chunkValidationRequest(String entirePayload) {

		ArrayList payloads = new ArrayList();
		String singlePayload = "";

		log.debug("*******************************************************");
		log.debug("***            Validation Payload:                *****");
		log.debug("***            Payload Length: " + entirePayload.length() + "                 *****");
		log.debug("*******************************************************\n\n");
		log.debug(entirePayload + "\n\n");
		log.debug("*******************************************************");
		log.debug("***              End Validation Payload:          *****");
		log.debug("*******************************************************\n");

		do {

			//if entire payload is greater than 30000 characters
			if (entirePayload.length() > 30000) {

				//get the first 30000 characters.
				singlePayload = entirePayload.substring(0, 30000);

				//then remove everything after the last </OBJ> tag
				singlePayload = singlePayload.substring(0, singlePayload.lastIndexOf("</OBJ>")+6);

				//then remove what is in singlePayload from entirePayload.
				entirePayload = entirePayload.substring(singlePayload.length(), entirePayload.length());

				//if entire payload will fit in one request. i.e. < 30000
			} else {
				singlePayload = entirePayload;
				entirePayload = "";
			}
			log.debug("Single Payload Length = "+singlePayload.length());
			log.debug("Entire Payload Length = "+entirePayload.length());

			//now take singlePayload and break into 2000 character payload chunks.
			for (int i = 0; i * 2000 <= singlePayload.length() ; i++) {
				if((i+1) * 2000 >= singlePayload.length())  {
					log.debug("Payload Substring Start: " + i*2000);
					log.debug("Payload Substring End: " + singlePayload.length());
					payloads.add(singlePayload.substring(i*2000, singlePayload.length()));
				} else {
					log.debug("Payload Substring Start: " + i*2000);
					log.debug("Payload Substring End: " + (i+1)*2000);
					payloads.add(singlePayload.substring(i*2000, (i+1)*2000));
				}
			}

		} while (entirePayload.length() > 0);

		for (int i = 0; i < payloads.size() ; i++) {
			log.debug("*******************************");
			log.debug("**  Payload " + i + ":");
			log.debug("******************************");
			log.debug("\n" + payloads.get(i));
			log.debug("*******************************\n");
		}

		return payloads;
	}

	/**
	 * Method that actually creates the validation request to be sent to middleware and returns the response.  This method is used when small
	 * payloads are to be sent to the database.  This situation will happen probably 95% of the time.  Large payloads should use the other
	 * method called multipleRequestValidation(Integer, Integer, String, List)
	 * @param majorStepId the major step ID being validated against.
	 * @param minorStepId the minor step ID being validated against.
	 * @param userId the user performing the operation.
	 * @param payloads the payloads that are to be sent with the request.  This list should NOT be greater than 16 in size.
	 * @return the datasource containing any messages from the validation request.
	 * @throws Exception 
	 */
	/*private C30KenanMWDataSource singleRequestValidation(final Integer majorStepId, final Integer minorStepId, final String userId, final List payloads) throws Exception {

		C30ExternalCallDef ruleValidationCall = new C30ExternalCallDef("bre_validate_rules");

		//------------------------------------------
		// set up the stored procedure to be called
		//------------------------------------------
		try {
			ruleValidationCall.addParam(new C30CustomParamDef("major_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("minor_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("user_id", C30CustomParamDef.STRING, 100, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_a", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_b", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_c", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_d", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_e", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_f", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_g", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_h", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_i", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_j", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_k", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_l", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_m", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_n", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_o", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));

			ruleValidationCall.addParam(new C30CustomParamDef("msg_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("msg_severity", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("msg_severity_descr", C30CustomParamDef.STRING, 2000, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("msg_severity_short_descr", C30CustomParamDef.STRING, 2000, C30CustomParamDef.OUTPUT));

			ruleValidationCall.addParam(new C30CustomParamDef("display_value", C30CustomParamDef.STRING, 2000, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("major_rule_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("minor_rule_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("validation_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("major_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("minor_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("step_name", C30CustomParamDef.STRING, 240, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("step_action", C30CustomParamDef.STRING, 30, C30CustomParamDef.OUTPUT));
		} catch (Exception e) {
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-009"),e,"INVALID-ATTRIB-009");
		}

		//--------------------------------------
		// Add all parameter values for request
		//--------------------------------------
		ArrayList paramValues = new ArrayList();
		paramValues.add(majorStepId);
		paramValues.add(minorStepId);
		paramValues.add(userId);

		for (int i = 0; i < 15 ; i++) {
			if(i < payloads.size()) {
				paramValues.add(payloads.get(i));
			} else {
				paramValues.add("");
			}
		}

		//---------------------------------------
		// make the call and process the results
		//---------------------------------------
		C30KenanMWDataSource dataSource = this.queryData(ruleValidationCall, paramValues, C30TableDataSource.PARAM_NAMES, getAccountServerId());

		return dataSource;
	}*/

	/**
	 * Method that actually creates the validation request to be sent to middleware and returns the response.  This method is used when large
	 * payloads are to be sent to the database.  This situation will happen probably 5% of the time.  Small payloads should use the other
	 * method called singleRequestValidation(Integer, Integer, String, List).  This method is required for large orders where the stored
	 * procedure cannot handle more than 30000 characters per transaction.  The method sends all payloads to the database before being returned
	 * a validation result.
	 * @param majorStepId the major step ID being validated against.
	 * @param minorStepId the minor step ID being validated against.
	 * @param userId the user performing the operation.
	 * @param payloads the payloads that are to be sent with the request.  This list should NOT be greater than 16 in size.
	 * @return the datasource containing any messages from the validation request.
	 * @throws Exception 
	 */
	private C30KenanMWDataSource multipleRequestValidation(final Integer majorStepId, final Integer minorStepId, final String userId, final List payloads) throws Exception  {
		C30ExternalCallDef ruleValidationCall = new C30ExternalCallDef("bre_create_payload");

		C30KenanMWDataSource dataSource = null;
		Integer validationId = new Integer(0);
		Integer excludeUser = new Integer(0);

		//------------------------------------------
		// set up the stored procedure to be called
		//------------------------------------------
		try {
			ruleValidationCall.addParam(new C30CustomParamDef("major_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("minor_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("user_id", C30CustomParamDef.STRING, 100, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("validation_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_a", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_b", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_c", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_d", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_e", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_f", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_g", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_h", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_i", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_j", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_k", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_l", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_m", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_n", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("payload_o", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("validation_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("exclude_user", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("persist_validation", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
		} catch (Exception e) {
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-010"),e,"INVALID-ATTRIB-010");
		}

		//====================================================================================================
		// This first part only sends payloads down to the server. Only 15 payloads can go at a time.  The
		// first time around a request is made the validation ID passed down is 0.  That first request will
		// have a response which includes a validation ID which must be used for all future requests for this
		// validation.
		//====================================================================================================

		//while there are payloads that have not been sent
		while (payloads.size() > 0 && excludeUser.equals(new Integer(0))) {

			//--------------------------------------
			// Add all parameter values for request
			//--------------------------------------
			ArrayList paramValues = new ArrayList();
			paramValues.add(majorStepId);
			paramValues.add(minorStepId);
			paramValues.add(userId);
			paramValues.add(validationId.toString());

			for (int i = 0; i < 15 ; i++) {
				//if(i < payloads.size()) {
				if(payloads.size() > 0) {
					String temp = payloads.remove(0).toString();
					log.debug("**********************************************");
					log.debug("** ADDING PAYLOAD STRING: *" + i);
					log.debug("**********************************************");
					log.debug(temp);
					log.debug("**********************************************");
					log.debug("** FINISHED ADDING PAYLOAD STRING: *" + i);
					log.debug("**********************************************");

					paramValues.add(temp);

					//paramValues.add(payloads.remove(0)); //REMOVE the first payload in the list.  This creates a STACK style list. i.e. FILO (first in last out)
					//paramValues.add(payloads.get(i));
				} else {
					paramValues.add("");
				}
			}

			//----------------------------------------------
			// make the call and retrieve the validation ID
			//----------------------------------------------
			log.debug("Multiple request validation: about to query data");
			//dataSource = this.queryData(ruleValidationCall, paramValues, C30TableDataSource.PARAM_NAMES, getAccountServerId());

			validationId = new Integer(dataSource.getValueAt(0, 0).toString());
			excludeUser = new Integer(dataSource.getValueAt(0, 1).toString());

		}

		//===================================================================================================
		// This second part requests a validation to be performed on the payload information that was passed
		// in from the above requests.
		//===================================================================================================

		ruleValidationCall = new C30ExternalCallDef("bre_validate_payload");

		//------------------------------------------
		// set up the stored procedure to be called
		//------------------------------------------
		try {
			ruleValidationCall.addParam(new C30CustomParamDef("validation_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("user_id", C30CustomParamDef.STRING, 100, C30CustomParamDef.INPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("msg_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("msg_severity", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("msg_severity_descr", C30CustomParamDef.STRING, 2000, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("msg_severity_short_descr", C30CustomParamDef.STRING, 2000, C30CustomParamDef.OUTPUT));

			ruleValidationCall.addParam(new C30CustomParamDef("display_value", C30CustomParamDef.STRING, 2000, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("major_rule_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("minor_rule_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("validation_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("major_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("minor_step_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("step_name", C30CustomParamDef.STRING, 240, C30CustomParamDef.OUTPUT));
			ruleValidationCall.addParam(new C30CustomParamDef("step_action", C30CustomParamDef.STRING, 30, C30CustomParamDef.OUTPUT));
		} catch (Exception e) {
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-010"),e,"INVALID-ATTRIB-010");
		}

		//--------------------------------------
		// Add all parameter values for request
		//--------------------------------------
		ArrayList paramValues = new ArrayList();
		paramValues.add(validationId);
		paramValues.add(userId);

		//---------------
		// make the call
		//---------------
		//	dataSource = this.queryData(ruleValidationCall, paramValues, C30TableDataSource.PARAM_NAMES, getAccountServerId());

		return dataSource;

	}

	/**
	 * Method to get a response from a validation request.
	 * @param dataSource the data source form which the response should be received.
	 * @return a collection of messages that is returned from the validation
	 * @throws C30SDKException 
	 */
	private C30SDKCollection createValidationResponse(final C30KenanMWDataSource dataSource) throws C30SDKException {
		C30SDKCollection msgList = (C30SDKCollection) factory.createC30SDKObject(C30SDKCollection.class, new Object[0]);
		C30SDKObject lwobj = null;
		log.debug("<VALIDATION_OUTPUT>");
		for (int i = 0; i < dataSource.getRowCount(); i++) {
			if (this.factory != null) {

				lwobj = factory.createC30SDKObject(C30SDKBREMessage.class, new Object[0]);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID, dataSource.getValueAt(i, 0), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY, dataSource.getValueAt(i, 1), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_DESCR, dataSource.getValueAt(i, 2), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_SHORT_DESCR, dataSource.getValueAt(i, 3), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_TEXT, dataSource.getValueAt(i, 4), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_VALIDATION_ID, dataSource.getValueAt(i, 7), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_RULE_ID, dataSource.getValueAt(i, 5), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MINOR_RULE_ID, dataSource.getValueAt(i, 6), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_STEP_ID, dataSource.getValueAt(i, 8), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MINOR_STEP_ID, dataSource.getValueAt(i, 9), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_STEP_NAME, dataSource.getValueAt(i, 10), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwobj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_STEP_ACTION, dataSource.getValueAt(i, 11), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

				lwobj.process();
				msgList.addC30SDKObject(lwobj);

				log.debug(msgList.serializeObject("1", "1").toString());
			}
		}

		log.debug("</VALIDATION_OUTPUT>");
		return msgList;
	}

	/**
	 * Method to create an XML string from the name/value pairs of the object.
	 * @return the XML string representation.
	 * @throws C30SDKInvalidAttributeException
	 */
	protected String createXML() throws C30SDKInvalidAttributeException {
		StringBuffer xml = new StringBuffer();
		String attributeName;
		String attributeValue;

		// add XML body
		xml.append("<Transaction>" + "\n");
		xml.append("    <TransUser>" + factory.getUserID()  + "</TransUser>\n");

		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, "", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}

		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, "", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}

		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE, new Date(), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}

		xml.append("    <TransExternalId>" + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)  + "</TransExternalId>\n");
		xml.append("    <TransRequestApp>" + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, C30SDKValueConstants.ATTRIB_TYPE_INPUT)  + "</TransRequestApp>\n");
		SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		xml.append("    <TransRequestDate>" + date.format(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) + "</TransRequestDate>\n");
		xml.append("	<TransName>" + this.getAttributeValueAsString(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE) + "</TransName>\n");

		// add XML elements
		Iterator it = this.getNameValuePairMap().keySet().iterator();
		it = this.getNameValuePairMap().values().iterator();
		C30SDKAttribute currAttribute;
		int order = 0;
		while (it.hasNext()) {
			currAttribute = (C30SDKAttribute) it.next();
			attributeName = currAttribute.getName();

			//ObjectParent is the only attribute that has an external name set internally.
			if (!attributeName.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT)) {
				attributeValue = currAttribute.getValue().toString();

				xml.append("    <Attribute>\n");
				xml.append("        <Name>" + attributeName + "</Name>\n");
				xml.append("        <Value>" + attributeValue + "</Value>\n");
				xml.append("        <Order>" + order + "</Order>\n");
				xml.append("    </Attribute>\n");

				order++;
			}

		}

		// add XML tail
		xml.append("</Transaction>" + "\n");
		log.debug("xml = '" + xml + "'");

		return xml.toString();
	}

	/**
	 * Persists all name/value pairs set by the user and makes it available to be called at another time.
	 * @throws Exception 
	 */
	public final void persist() throws Exception {
		log.debug("Starting persist()");

		if (((Integer) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)).equals(C30SDKValueConstants.IS_NOT_PERSISTED)) {

			//get the object as xml
			String xml = createXML();

			C30ExternalCallDef persistObjectCall = new C30ExternalCallDef("ps_lw_persist");

			try {

				//------------------------------------------
				// set up the stored procedure to be called
				//------------------------------------------
				persistObjectCall.addParam(new C30CustomParamDef("payload", C30CustomParamDef.STRING, 2000, C30CustomParamDef.INPUT));
				persistObjectCall.addParam(new C30CustomParamDef("internal_id", C30CustomParamDef.INT, C30CustomParamDef.OUTPUT));
			} catch (Exception e) {

				throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("INVALID-ATTRIB-011"),e,"INVALID-ATTRIB-011");
			}

			//--------------------------------
			// Add input parameters for the stored proc call.
			//--------------------------------
			ArrayList paramValues = new ArrayList();
			paramValues.add(xml);

			//---------------------------------------
			// make the call and process the results
			//---------------------------------------
			//C30KenanMWDataSource dataSource = this.queryData(persistObjectCall, paramValues, C30TableDataSource.PARAM_NAMES, getAccountServerId());

			//this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_INTERNAL_ID, dataSource.getValueAt(0, 0), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		}
	}

	/**
	 * Method to get a collection of all the C30SDK objects associated with this object.
	 * @return the list of "child" objects
	 * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the object
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 */
	protected final C30SDKCollection getC30SDKObjects() throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException {
		C30SDKCollection objects = (C30SDKCollection) factory.createC30SDKObject(C30SDKCollection.class, new Object[0]);
		for (int i = 0; i < c30sdkObjects.size(); i++) {
			objects.addC30SDKObject((C30SDKObject) c30sdkObjects.get(i));
		}
		return objects;
	}

	/**
	 * Method to set a collection of C30SDK children into the object.  NOTE: This method clears the current children before adding any new children.
	 * @param objects the children being added to the object
	 */
	protected final void setC30SDKObjects(final C30SDKCollection objects) {
		c30sdkObjects.clear();
		for (int i = 0; i < objects.getC30SDKObjectCount(); i++) {
			c30sdkObjects.add(objects.getC30SDKObject(i));
		}
	}

	/**
	 * Gets the child C30SDK object in the collection by index.
	 * @param index zero-based index of C30SDK object to get
	 * @return C30SDKObject in the collection at the index
	 */
	public final C30SDKObject getC30SDKObject(final int index) {
		return (C30SDKObject) c30sdkObjects.get(index);
	}

	/**
	 * Removes the child C30SDK object in the collection by index. Lwo Enhancement.
	 * @param index zero-based index of C30SDK object to get
	 * 
	 */ 
	public final void removeC30SDKObject(int index)
	{
		c30sdkObjects.remove(index);
	}

	/**
	 * Get the child C30SDK object in the collection that matches the given type and instance.
	 * @param objType Type of C30SDK object (ex - lw_order_type, lw_service_order_type)
	 * @param index Zero based index of the instance of C30SDK object in the collection to return
	 * @return C30SDKObject in the collection matching the given type
	 * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
	 */

	public final C30SDKObject getC30SDKObject(final String objType, final int index) throws C30SDKInvalidAttributeException {
		C30SDKObject lwo = null;
		int match = 0;

		//--------------------------------------------------------------------------------
		// loop through the collection of C30SDKObjects and return the one matching
		// the type passed in or throw an exception if multiple C30SDKObjects
		// match the criteria
		//--------------------------------------------------------------------------------
		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			if (((C30SDKObject) this.getC30SDKObject(i)).getAttribute(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue().equals(objType)) {
				if (++match == index) {
					lwo = (C30SDKObject) this.getC30SDKObject(i);
				}
			}
		}
		return lwo;
	}

	/**
	 * Method to add a child object at a specific entry.
	 * @param object the child object being added.
	 */
	public void addC30SDKObject(int i,C30SDKObject object)
	{
		try
		{
			if(object.getAttributeValue("ObjectParent", C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null)
				object.setNameValuePair("ObjectParent", this);
		}
		catch(C30SDKInvalidAttributeException e)
		{
			log.error(e);
		}
		c30sdkObjects.add(i,object);
	}



	/**
	 * Method to add a child object.
	 * @param object the child object being added.
	 */
	public void addC30SDKObject(final C30SDKObject object){
		try {

			if (object.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
				object.setNameValuePair(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, this);
			}

			//this will never happen because all objects have the ATTRIB_OBJECT_PARENT attribute
		} catch (C30SDKInvalidAttributeException e) {
			log.error(e);
		}
		c30sdkObjects.add(object);
	}

	/**
	 * Gets the count of child C30SDK objects in the collection.
	 * @return the count of C30SDK objects in the collection
	 */
	public final int getC30SDKObjectCount() {
		return c30sdkObjects.size();
	}

	protected final void clearC30SDKObjects() {
		c30sdkObjects = new ArrayList();
	}

	/**
	 * Method to get the factory of this C30SDK object.
	 * @return the C30SDK factory associated with this object.
	 */
	public final C30SDKFrameworkFactory getFactory() {
		return factory;
	}

	/**
	 * Start a transaction using explicit transaction control 
	 * @return boolean indicating transaction has started
	 * @throws ServiceException
	 */
	/*	protected final boolean beginTransaction()throws ServiceException {
		boolean transactionStarted = false;
		factory.getConnection().beginTransaction (30, 0);
		return transactionStarted;
	}

	 *//**
	 * commit a pending transaction 
	 * @throws ServiceException
	 *//*
	protected final void endTransaction() throws ServiceException {
		factory.getConnection().endTransaction(0);
	}

	  *//**
	  *  Roll back a pending transaction using explicit transaction control.
	  * @throws ServiceException
	  *//*
	protected final void abortTransaction() throws ServiceException {
		factory.getConnection().abortTransaction(0);
	}*/

	/**
	 * Method which queries data from Kenan DB given an object name, the object query data and
	 * the verb which specifies the action that is to take place.
	 * An example of the verb might be Commit, Get, Find etc.
	 * @param objectName the business domain object that is being queried for.
	 * @param verb the action of the query. i.e. Get, Find, Commit.  This verb is used to create
	 * the call name supplied to the ApiMappings.
	 * @param objectData the mapping used in the request.
	 * @return the map containing the information returned from the server.
	 * @throws C30SDKObjectConnectionException if an exception is thrown
	 * @throws C30SDKKenanFxCoreException 
	 */
	protected final Map queryData(final String objectName, final String verb, final Map objectData, final Integer serverId) throws C30SDKObjectConnectionException, C30SDKKenanFxCoreException {

                BSDMSessionContext context =  this.getFactory().getContext();
                this.setContext(context);
            
		C30KenanMWDataSource dataSource = factory.getMwDataSource(serverId);
		dataSource.setContext(userContextofLWObject);
                
		Map request = new HashMap();
		Map callResponse = null;
		request.put(objectName, objectData);
		
		log.debug("Call Name = "+objectName + verb + "(" + ApiMappings.getCallName(objectName + verb) + ")");

		try {
			callResponse = dataSource.queryData(ApiMappings.getCallName(objectName + verb), request);

		} catch (ServiceException e) {
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			if (e.getErrorCategory().equalsIgnoreCase("BakcEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}
			else if (e.getErrorCategory().equalsIgnoreCase("FrontEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorName()),e,e.getErrorName());	
			}
			else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}


		} catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
		} catch (IOException e) {
			throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		} catch (terrapin.tuxedo.TuxError e) {
			String message = e.getMessage();
			String code[]=message.split("-");
			//throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
			if (code[0].trim().startsWith("TPE") && firstReconnetAttempt)
			{
				firstReconnetAttempt=false;
				//Reconnect and make the same call again.
				factory.reconnectToMWDataSource();
				try {
					callResponse = dataSource.queryData(ApiMappings.getCallName(objectName + verb), request);
				}  catch (Exception e1) {
					throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-030") ,e1,"CONN-030");
				}
			}else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
			}
		}
		catch (Exception e)
		{
			throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		}

		return callResponse;

	}


	/**
	 * Method that requests data from middleware given the calling name and data.
	 * @param callName the middleware service to be called.
	 * @param objectData the data to be passed into the service being called.
	 * @return the returned data from the service call.
	 * @throws C30SDKObjectConnectionException if an exception is thrown during a call to middleware
	 * @throws C30SDKKenanFxCoreException 
	 */
	protected final Map queryData(final String callName, final Map objectData, final Integer serverId) throws C30SDKObjectConnectionException, C30SDKKenanFxCoreException {

	        BSDMSessionContext context =  this.getFactory().getContext();
	        this.setContext(context);
	     
		C30KenanMWDataSource dataSource = factory.getMwDataSource(serverId);
		dataSource.setContext(userContextofLWObject);

		Map callResponse = null;
		
		log.debug("Call Name = "+ApiMappings.getCallName(callName)+" ("+callName+")");

		try {

			callResponse = dataSource.queryData(ApiMappings.getCallName(callName), objectData);

		} catch (ServiceException e) {
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			log.error(e);
			//throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			if (e.getErrorCategory().equalsIgnoreCase("BakcEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}
			else if (e.getErrorCategory().equalsIgnoreCase("FrontEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorName()),e,e.getErrorName());	
			}
			else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}


		} catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
		} catch (IOException e) {
			throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		} catch (terrapin.tuxedo.TuxError e) {
		    
			String message = e.getMessage();
			String code[]=message.split("-");
			//throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
			if (code[0].trim().startsWith("TPE") && firstReconnetAttempt)
			{
				firstReconnetAttempt=false;
				//Reconnect and make the same call again.
				factory.reconnectToMWDataSource();
				try {
					callResponse = dataSource.queryData(ApiMappings.getCallName(callName), objectData);
				}  catch (Exception e1) {
					throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-030") ,e1,"CONN-030");
				}
			}else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
			}
		}
		catch (Exception e)
		{
			throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		}
		return callResponse;

	}

	/**
	 * Method which queries data from Kenan DB given the call definition, parameters and the source.  This
	 * method is usually used when calling a stored procedure through middleware rather than calling a
	 * regular middleware API-TS call.
	 * @param call the call definition which includes the name of the stored procedure
	 * @param params the parameter values passed in with the call
	 * @param source whether the source of the information is DISPLAY_VALUES or PARAM_NAMES
	 * @return the data source containing the information returned from the server.
	 * @throws Exception 
	 */
	/*	protected final C30KenanMWDataSource queryData(final C30ExternalCallDef call, final ArrayList params, final int source, final Integer serverId) throws C30SDKException {

		C30KenanMWDataSource dataSource = factory.getMwDataSource(serverId);
		dataSource.setContext(userContextofLWObject);
		log.debug("About to Query data from PSMWDatasource");
		try{
			dataSource.queryData(call, params, source);
		} catch (ServiceException e) {
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			//throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			if (e.getErrorCategory().equalsIgnoreCase("BakcEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}
			else if (e.getErrorCategory().equalsIgnoreCase("FrontEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorName()),e,e.getErrorName());	
			}
			else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}


		} catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
		} catch (IOException e) {
			throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		} catch (terrapin.tuxedo.TuxError e) {
			String message = e.getMessage();
			String code[]=message.split("-");
			if (code[0].trim().startsWith("TPE") && firstReconnetAttempt)
			{
				firstReconnetAttempt=false;
				//Reconnect and make the same call again.
				factory.reconnectToMWDataSource();
				try {
					dataSource.queryData(call, params, source);
				}  catch (Exception e1) {
					throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-030") ,e1,"CONN-030");
				}
			}else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
			}
		}
		catch (Exception e)
		{
			throw new C30SDKObjectConnectionException(this.getFactory().exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		}
		return dataSource;

	}
	 */


	/**
	 * Method to get the current server that the account is located on.  This value is initialized
	 * to 3 (default) but is set once an account is found.
	 * @return the server ID that the customer information is located on.
	 */
	protected final Integer getAccountServerId() {

		Integer serverId = null;

		if (this.factory.c30SdkFactoryCache.get("ACCOUNT_SERVER_ID_CACHE")!= null) {
			serverId = (Integer) (this.factory.c30SdkFactoryCache.get(C30SDKAttributeConstants.ACCOUNT_SERVER_ID_CACHE)).getValue();
		} else {
			serverId = C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID;
		}

		return serverId;
	}

	/**
	 * Method which outputs HashMap tree of C30SDK objects and their attributes in a Hashmap of Hashmaps.
	 * The default setting is to include all values that are eligible for display including nulls, represented as
	 * the string "null".
	 * @return a HashMap representation of this object and all associated C30SDK Objects.
	 */
	public HashMap <String, HashMap> formattedOutput()
	{  
		return toHashMap(Boolean.TRUE);
	}

	/**
	 * Method which outputs the tree of C30SDK objects and their attributes in a Hashmap of Hashmaps.
	 * @param incNulls indicates whether attributes with null values should be displayed.
	 * @return a String representation of this object and all associated C30SDK Objects.
	 */
	public String toXML(Boolean incNulls)
	{
		String returnXML = "";
		HashMap map = toHashMap(incNulls);

		returnXML = printXML(map,2);
		return returnXML;		
	}

	private static String printXML(Map map, int indent)
	{
		String returnXML="";
		try{
			if (map == null) return "";
			Iterator iter = map.entrySet (). iterator ();
			String ObjectName =(String)map.get("#NAME");
			String ObjectType = (String)map.get("ObjectType");
			if (ObjectName!=null && ObjectType!=null)
			{
				for (int i = 0; i < indent; i++) returnXML=returnXML+" ";
				returnXML=returnXML+ "<"+ObjectName+" ObjectType=\""+ObjectType+"\">\n";
				indent=indent+2;
			}
			while (iter.hasNext ())
			{
				Map.Entry entry = (Map.Entry) iter.next();
				for (int i = 0; i < indent; i++) returnXML=returnXML+" ";
				returnXML=returnXML+ "<"+entry.getKey ()+">"; 
				returnXML=returnXML+printValue (entry.getValue (), indent+2);
				returnXML=returnXML+ "</"+entry.getKey ()+">\n";

			}
			if (ObjectName!=null && ObjectType!=null)
			{
				for (int i = 0; i < indent-2; i++) returnXML=returnXML+" ";
				returnXML=returnXML+ "<"+ObjectName+">\n";
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error(e);
		}

		return returnXML;		
	}

	/** print an object's value
	 * 
	 * @param val the Object to print
	 * @param indent the number of spaces to indent the contents
	 * @throws IOException if an IO error is encountered
	 */
	public static String printValue (Object val, int indent) throws IOException
	{
		String returnXML="";
		if (val == null)
		{
			return("null");
		}
		else if (val instanceof Map)
		{
			returnXML = returnXML+ "\n"+printXML((Map)val,indent+2);
			for (int i = 0; i < indent; i++) returnXML=returnXML+" ";

		}
		else if (val.getClass ().isArray () && !(val instanceof byte[]))
		{
			returnXML = returnXML+"[";
			Object[] objs = (Object[]) val;
			for (int i = 0; i < objs.length; i++)
			{
				returnXML = returnXML+printValue ( objs[i], indent + 2);
			}
			returnXML = returnXML+"]";
		}
		else if (val instanceof List)
		{
			returnXML = returnXML+"[";
			Iterator iter = ((List) val).iterator ();
			while (iter.hasNext ())
			{
				returnXML = returnXML+printValue ( iter.next (), indent + 2);
			}
			returnXML = returnXML+"]\n";
		}
		else
		{
			returnXML = returnXML+val;
		}
		return returnXML;
	}


	/**
	 * Method which outputs the tree of C30SDK objects and their attributes in a Hashmap of Hashmaps.
	 * @param incNulls indicates whether attributes with null values should be displayed.
	 * @return a HashMap representation of this object and all associated C30SDK Objects.
	 */
	public HashMap <String, HashMap> toHashMap(Boolean incNulls)
	{
		HashMap object = new HashMap<String, HashMap>(); 
		HashMap level= new HashMap<String, String>();
		Integer idx = 0;
		try
		{
			// if not processed then we have an empty object 
			if (!(Boolean)getAttributeValue("IsProcessed", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
				return object;

			level = toHashMap(this, incNulls);
			object.put(idx.toString(),level);
			// now get all linked lwos
			for(int i = 0; i < getC30SDKObjectCount(); i++){
				level = toHashMap(getC30SDKObject(i), incNulls);
				if (level.size() > 0){
					idx++;
					object.put(idx.toString(),level);
				}
			}

		}
		catch(Exception e)
		{
			log.error(e);
		}
		return object;
	}

	/**
	 * Method which transforms a C30SDK objects and their attributes to a Hashmap of attributes and values.
	 * Using this method indicates that all attributes and objects will be output according to attribute filtering
	 * including those with a value of null depending on the parameter setting of incNulls. 
	 * @param lwo  The C30SDK Object to be analyzed.
	 * @param incNulls indicates whether attributes with null values should be displayed.
	 * @return a HashMap representation of this object.
	 */
	protected HashMap <String, String> toHashMap(C30SDKObject lwo, Boolean incNulls)
	{
		HashMap object = new HashMap();
		try
		{
			List outputKeys = new ArrayList(lwo.outputAttributes.keySet());
			Collections.sort(outputKeys);
			C30SDKAttribute attribute = null;
			ArrayList externalNames = new ArrayList();
			Object extendedDataList[]=null;
			Object value = null;
			HashMap eumerationObject = this.getFactory().getObjectEnumeration();
			ArrayList<HashMap> enumerationList = new ArrayList();
			// list the lwo class name 
			String className = lwo.getClass().getName();
			if (className.lastIndexOf(".") < className.length())
				className = className.substring(className.lastIndexOf(".")+1);
			object.put("#NAME",className);
			boolean dontshowExtendDataList=false;
			boolean dontshowEnumerationList = false;
			for(int i = 0; i < outputKeys.size(); i++)
			{
				boolean requireEnumeration=false;
				boolean keepEnumatRoot=false;
				attribute = lwo.getAttribute((String)outputKeys.get(i), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				// Suppress output for internally generated attributes (starting with string Persist)
				if (attribute.getName().indexOf("Persist") > -1)
					continue;
				if (attribute.getName().trim().equalsIgnoreCase("EnumerationDataList"))
				{
					if(!attribute.isOutputExposed()) dontshowEnumerationList=true;
				}

				if (attribute.getName().trim().equalsIgnoreCase("ExtendedDataList"))
				{
					value = attribute.getValue();
					extendedDataList=(Object[])(Object[])value;		
					if(!attribute.isOutputExposed()) dontshowExtendDataList=true;
				}
				if (attribute.getName().trim().equalsIgnoreCase("Balances"))
				{
					value = attribute.getValue();
					//extendedDataList=(Object[])(Object[])value;	
					object.put("Balances",value);
				}
				if (attribute.getName().trim().equalsIgnoreCase("TotalCount"))
				{
					value = attribute.getValue();
					//extendedDataList=(Object[])(Object[])value;	
					if (value != null)object.put("TotalCount",value.toString());
				}
				if(attribute.getAttributeClass().toString().equalsIgnoreCase("class java.io.ByteArrayOutputStream") )
				{
					log.info("Class ="+ attribute.getAttributeClass().toString());
					log.info("Not converting ByteArray to String");
					object.put( attribute.getName(),attribute.getValue());
					continue;
				}
				
	                        if(attribute.getAttributeClass().toString().equalsIgnoreCase("class java.util.ArrayList") )
	                        {
	                                log.info("Class ="+ attribute.getAttributeClass().toString());
	                                log.info("Not converting ArrayList to String");
	                                object.put( attribute.getName(), attribute.getValue());
	                                continue;
	                        }
	                        
				if (!incNulls && attribute.getValue() == null)   
					continue;
				value = attribute.getValue();
				if (attribute.getName() == null)
					continue;
				String attrName = attribute.getName();
				String origattrName = attribute.getName();
				if (attribute.getExternalName() != null && attribute.getExternalName().trim().length() > 0 )
				{
					attrName = attribute.getExternalName().trim();
					if(isNumeric(attribute.getName()))
						externalNames.add(attribute.getName());					
				}
				else 
				{
					// Dont want to use the alias for the external ids.
					if (isNumeric(attrName))
						continue;
					else
						if (attribute.getAliasName() != null && attribute.getAliasName().trim().length() > 0 )
							attrName = attribute.getAliasName().trim();  

				}
				if (attribute.getEnumerationId()==1 )
				{
					requireEnumeration=true;
					if (attribute.getKeepatRoot()==1) 
						keepEnumatRoot=true;
				}
				if (!(lwo instanceof C30SDKExceptionMessage) )        
					if(!attribute.isOutputExposed() )
						continue;

				if(value instanceof C30SDKObject)
					object.put(attrName,"value=\"" + value.getClass().getName() + "\" ExternalName=\"" + attribute.getExternalName() + "\" class=\"" + attribute.getAttributeClass().getName() + "\" type=\"" + attribute.getAttributeType() + "\" IsOverrideable=\"" + attribute.isOverrideable() + "\" IsRequired=\"" + attribute.isRequired() + "\" LoadOrder=\"" + attribute.getLoadOrder() + "\"/>\n");
				else{
					String strValue = ""+value;//lwo.getAttributeValueAsString(attribute.getName()); 
					log.debug("Attr Name: "+attrName+", Attr Value: "+strValue);
					//Check if there are any enumerations

					if (requireEnumeration)
					{
						C30SDKEnumeration lwenum = (C30SDKEnumeration)eumerationObject.get(origattrName+"_"+strValue);
						if ( lwenum != null)
						{
							log.debug("This has Enumeration : " +origattrName+"_"+strValue + "  : " + lwenum.getDisplayValue() );
							HashMap tempEnumData = new HashMap();
						  	tempEnumData.put("Id", strValue);
							tempEnumData.put("Value",  lwenum.getDisplayValue());
							tempEnumData.put("Name", origattrName);
							tempEnumData.put("#NAME", "EunmerationData");
							if (!keepEnumatRoot)
							{
								enumerationList.add(tempEnumData);
							}
							else
							{
								strValue=(String)tempEnumData.get("Value");
								object.put(attrName,""+strValue);
							}
						}
					}else
					{
						object.put(attrName,""+strValue);
					}

				}
			}
			//Clearing all the extendeddata that has the External Name from the Extended DataList
			ArrayList<HashMap> newExtendedDataList = new ArrayList(); 
			//HashMap newExtendedDataList[] = new HashMap[extendedDataList.length-externalNames.size()];

			if (extendedDataList != null)
			{
				for (int j = 0; j < extendedDataList.length; j++)
				{ 

					HashMap extData = (HashMap)extendedDataList[j] ; 
					Integer AttrKey = (Integer)extData.get("ParamId");
					//Check if this is part of the externalNames arraylist.
					if (!externalNames.contains(AttrKey.toString()))
					{
						newExtendedDataList.add(extData);					
					}
				}

			}
			//Updating the extendedData list 
			if (newExtendedDataList != null && !dontshowExtendDataList) object.put("ExtendedDataList", newExtendedDataList);

			//Update for enumerations
			if (enumerationList != null && !dontshowEnumerationList)  object.put("EnumerationDataList", enumerationList);


		}
		catch(Exception e)
		{
			log.error(e);
		}
		return object;
	}


	/**
	 * Method which prints out the tree of C30SDK objects and their attributes.  Using this method, rather
	 * than the toString(String, Boolean) method, indicates that all attributes and objects will be printed
	 * including those with a value of null.
	 * @return a string representation of this object in semi-XML format.
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String object = "";
		try {
			object = toString(0, new Integer("0"), Boolean.TRUE);
		} catch (Exception e) {
			log.error(e);
		}
		return object;
	}

	/**
	 * Method which prints out the tree of C30SDK objects.  The type indicates what level
	 * the objects should be output as. i.e. whether the object attributes should be printed out as well.
	 * @param type the level the objects should be output as. Values are the strings ALL or OBJECT.
	 * @param incNulls indicates whether attributes with null values should be output.
	 * @return a string representation of the C30SDK object being printed.
	 */
	public final String toString(final Integer type, final Boolean incNulls) {
		String object = "";
		try {
			object = toString(0, type, incNulls);
		} catch (Exception e) {
			log.error(e);
		}
		return object;
	}

	/**
	 * Method which prints out the tree of C30SDK objects.  The type indicates what level
	 * the objects should be output as. i.e. whether the object attributes should be printed out as well.
	 * @param inputLevel this is used for pretty printing to determine what depth the object is at in the tree.
	 * @param type whether attributes should be printed out as well.
	 * @param incNulls indicates whether attributes with null values should be displayed.
	 * @return a string representation of the C30SDK object being printed.
	 * @throws C30SDKInvalidAttributeException if an exception is thrown while trying to retrieve the object attributes.
	 */
	protected final String toString(final int inputLevel, final Integer type, final Boolean incNulls) throws C30SDKInvalidAttributeException {
		StringBuffer object = new StringBuffer();
		int level = inputLevel;

		//------------------------------------------------
		// This is purely for aesthetics.  Nice tabbing
		// so that things can be viewed better on output.
		//------------------------------------------------
		String tabs = "";
		for(int j = 0; j <= level; j++) {
			tabs = tabs + "   ";
		}

		//----------------------------------------
		//this sorts the keys so that we can find
		// what we are looking for much easier.
		//----------------------------------------
		List dataKeys = new ArrayList(inputAttributes.keySet());
		Collections.sort(dataKeys);
		List outputKeys = new ArrayList(outputAttributes.keySet());
		Collections.sort(outputKeys);
		List systemKeys = new ArrayList(systemAttributes.keySet());
		Collections.sort(systemKeys);

		object.append("\n" + tabs + "<C30SDKObject class=\"" + this.getClass().getName().substring(this.getClass().getName().lastIndexOf(".")+1) + "\" type=\"" + this.getAttribute(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() + "\">\n");

		C30SDKAttribute attribute = null;
		Object value = null;
		if (type.equals(new Integer("0")) || type.equals(C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
			for  (int i = 0; i < dataKeys.size(); i++) {
				attribute = this.getAttribute( (String) dataKeys.get(i), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				value = attribute.getValue();
				if (incNulls.booleanValue() || (!incNulls.booleanValue() && value != null)) {
					if (value != null && attribute.getName().equals(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT)) {
						value = value.getClass().getName();
					}
					if (value instanceof C30SDKObject) {
						object.append(tabs + "   " + "<InputAttr name=\"" + attribute.getName() + "\" value=\"" + value.getClass().getName() + "\" ExternalName=\"" + attribute.getExternalName() + "\" class=\"" + attribute.getAttributeClass().getName() + "\" type=\"" + attribute.getAttributeType() + "\" IsOverrideable=\"" + attribute.isOverrideable() + "\" IsRequired=\"" + attribute.isRequired() + "\" LoadOrder=\"" + attribute.getLoadOrder() + "\"/>\n");
					} else {
						object.append(tabs + "   " + "<InputAttr name=\"" + attribute.getName() + "\" value=\"" + value + "\" ExternalName=\"" + attribute.getExternalName() + "\" class=\"" + attribute.getAttributeClass().getName() + "\" type=\"" + attribute.getAttributeType() + "\" IsOverrideable=\"" + attribute.isOverrideable() + "\" IsRequired=\"" + attribute.isRequired() + "\" LoadOrder=\"" + attribute.getLoadOrder() + "\"/>\n");
					}
				}
			}
		}

		if (type.equals(new Integer("0")) || type.equals(C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) {
			for  (int i = 0; i < outputKeys.size(); i++) {
				attribute = this.getAttribute( (String) outputKeys.get(i), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				value = attribute.getValue();
				if (incNulls.booleanValue() || (!incNulls.booleanValue() && value != null)) {
					if (value instanceof C30SDKObject) {
						object.append(tabs + "   " + "<OutputAttr name=\"" + attribute.getName() + "\" value=\"" + value.getClass().getName() + "\" ExternalName=\"" + attribute.getExternalName() + "\" class=\"" + attribute.getAttributeClass().getName() + "\" type=\"" + attribute.getAttributeType() + "\" IsOverrideable=\"" + attribute.isOverrideable() + "\" IsRequired=\"" + attribute.isRequired() + "\" LoadOrder=\"" + attribute.getLoadOrder() + "\"/>\n");
					} else {
						object.append(tabs + "   " + "<OutputAttr name=\"" + attribute.getName() + "\" value=\"" + value + "\" ExternalName=\"" + attribute.getExternalName() + "\" class=\"" + attribute.getAttributeClass().getName() + "\" type=\"" + attribute.getAttributeType() + "\" IsOverrideable=\"" + attribute.isOverrideable() + "\" IsRequired=\"" + attribute.isRequired() + "\" LoadOrder=\"" + attribute.getLoadOrder() + "\"/>\n");
					}
				}
			}
		}

		if (type.equals(new Integer("0")) || type.equals(C30SDKValueConstants.ATTRIB_TYPE_SYSTEM)) {
			for  (int i = 0; i < systemKeys.size(); i++) {
				attribute = this.getAttribute( (String) systemKeys.get(i), C30SDKValueConstants.ATTRIB_TYPE_SYSTEM);
				value = attribute.getValue();
				if (incNulls.booleanValue() || (!incNulls.booleanValue() && value != null)) {
					if (value instanceof C30SDKObject) {
						object.append(tabs + "   " + "<SystemAttr name=\"" + attribute.getName() + "\" value=\"" + value.getClass().getName() + "\" ExternalName=\"" + attribute.getExternalName() + "\" class=\"" + attribute.getAttributeClass().getName() + "\" type=\"" + attribute.getAttributeType() + "\" IsOverrideable=\"" + attribute.isOverrideable() + "\" IsRequired=\"" + attribute.isRequired() + "\" LoadOrder=\"" + attribute.getLoadOrder() + "\"/>\n");
					} else {
						object.append(tabs + "   " + "<SystemAttr name=\"" + attribute.getName() + "\" value=\"" + value + "\" ExternalName=\"" + attribute.getExternalName() + "\" class=\"" + attribute.getAttributeClass().getName() + "\" type=\"" + attribute.getAttributeType() + "\" IsOverrideable=\"" + attribute.isOverrideable() + "\" IsRequired=\"" + attribute.isRequired() + "\" LoadOrder=\"" + attribute.getLoadOrder() + "\"/>\n");
					}
				}
			}
		}

		object.append(tabs + "   <Children>");

		level = level + 2;

		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			object.append(this.getC30SDKObject(i).toString(level, type, incNulls));
		}
		if (this.getC30SDKObjectCount() > 0) {
			object.append(tabs + "   </Children>\n");
		} else {
			object.append("\n" + tabs + "   </Children>\n");
		}

		object.append(tabs + "</C30SDKObject>\n");

		return object.toString();

	}

	private boolean isNumeric(String input)
	{
		try
		{
			Integer.parseInt(input);
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

	public void setContext(BSDMSessionContext context) {
		// TODO Auto-generated method stub
		this.userContextofLWObject=context;
	}

	public void prevalidate() throws C30SDKException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		C30TransactionExtension lwt= null;
		C30TransactionBREExtension brelwt= null;

		// TODO Auto-generated method stub
		//Validating the input objects...
		Class validateClassName ;
		if (this.getPreValidateClassId() !=null &&  !this.getPreValidateClassId().equalsIgnoreCase(""))
		{
			 validateClassName = Class.forName(this.getPreValidateClassId());
			lwt = (C30TransactionExtension)validateClassName.newInstance();
			lwt.validate(this);
		}
		//Validating the input objects for Business Rules...
		if (this.getBrePreValidateClassId() !=null &&  !this.getBrePreValidateClassId().equalsIgnoreCase(""))
		{
			validateClassName = Class.forName(this.getBrePreValidateClassId());
			brelwt = (C30TransactionBREExtension)validateClassName.newInstance();
			brelwt.brevalidate(this);
		}
	}
}
