/*
 * BusinessRuleMsg.java
 *
 * Created on September 13, 2007, 9:38 AM
 *
 */

package com.cycle30.sdk.core.message;


import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30StringUtils;

/**
 * C30SDK object that encapsulates an exception message returned by the framework. This
 * object is used throughout the framework and is the transport mechanism for returning errors
 * that are created while using the framework.
 * @author Tom Ansley
 */
public class C30SDKExceptionMessage extends C30SDKMessage {

	/**String value of attributeName to set the stack trace object attribute.*/
	public static final String ATTRIB_EXCEPTION_CODE = "ExceptionCode";
	/**String value of attributeName to set the stack trace object attribute.*/
	public static final String ATTRIB_EXCEPTION = "Exception";
	/**String value of attributeName to set the stack trace object attribute.*/
	public static final String ATTRIB_STACK_TRACE = "StackTrace";
	/**String value of attributeName to set the Message Severity object attribute.*/
	public static final String ATTRIB_MSG_DESCR = "MsgDescr";
	/**String value of attributeName to set the Message Severity object attribute.*/
	public static final String ATTRIB_MSG_SHORT_DESCR = "MsgShortDescr";
	/**String value of attributeName to set the Message Text object attribute.*/
	public static final String ATTRIB_MSG_TEXT = "MsgText";
	public static final String ATTRIB_ADDT_ERROR_TEXT = "AdditionalErrorText";
	
	private static Logger log = Logger.getLogger(C30SDKExceptionMessage.class);

	/**
	 * Creates a new instance of C30SDKExceptionMessage.
	 * @param factory Factory used to create C30SDKMessage objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30SDKExceptionMessage(final C30SDKFrameworkFactory factory) throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30SDKExceptionMessage.class);
		log.error("=========================================================================");
		log.error("A C30SDK exception has been thrown and a C30SDK object of type");
		log.error("C30SDKExceptionMessage has been returned.  This object contains the");
		log.error("java error message and can be retrieved using the following code:");
		log.error("object.getAttribute(C30SDKExceptionMessage.ATTRIB_MSG_TEXT)");
		log.error("=========================================================================");
	}

	/**
	 * Creates a new isntance of C30SDKExceptionMessage.
	 * @param factory Factory used to create the object.
	 * @param objectType the object type which indicates the configuration to be used to populate the object.
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30SDKExceptionMessage(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/**
	 * Method to initialize the attributes of this object.
	 * @throws C30SDKInvalidAttributeException if an attribute could not be initialized correctly.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {

		C30SDKAttribute exceptionAttribute;
		this.addAttribute(new C30SDKAttribute(ATTRIB_EXCEPTION, Throwable.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_EXCEPTION_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_STACK_TRACE, String.class, 30000), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_MSG_DESCR, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_MSG_SHORT_DESCR, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_MSG_TEXT, String.class, 30000), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_EXCEPTION_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ADDT_ERROR_TEXT, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		
		exceptionAttribute = getAttribute(ATTRIB_MSG_DESCR, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		exceptionAttribute.setExposeOutput(true);


		exceptionAttribute = getAttribute(ATTRIB_MSG_SHORT_DESCR, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		exceptionAttribute.setExposeOutput(true);

		exceptionAttribute = getAttribute(ATTRIB_MSG_TEXT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		exceptionAttribute.setExposeOutput(true);

		exceptionAttribute = getAttribute(ATTRIB_ADDT_ERROR_TEXT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		exceptionAttribute.setExposeOutput(true);
		
		exceptionAttribute = getAttribute(ATTRIB_EXCEPTION_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		exceptionAttribute.setExposeOutput(true);

	}

	/**
	 * (non-Javadoc).
	 * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Integer)
	 */
	@Override
	public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {

		if (attributeName.equals(ATTRIB_MSG_TEXT)) {
			C30SDKAttribute attribute = this.getAttribute(ATTRIB_MSG_TEXT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			String newValue = "";

			//if original message was null then remove it.  This could happen if Java throws the original error.
			if (attribute.getValue() == null || ((String) attribute.getValue()).toString().equals("null")) {
				newValue = (String) attributeValue;
			} else {
				newValue = attribute.getValue() + "\n" + attributeValue;
			}
			attribute.setValue(newValue);
		} else {
			super.setAttributeValue(attributeName, attributeValue, type);    		
		}

	}

	/**
	 * @return the result of processing the C30SDK object
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public final C30SDKObject process() {
		try {

			//get the thrown exception
			Throwable exception = (Throwable) this.getAttributeValue(ATTRIB_EXCEPTION, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			//=========================================================
			// Depending on what exception is thrown will dictate what
			// information we are going to put into the returned LWO.
			//=========================================================

			//-------------------------------
			// LIGHT WEIGHT EXCEPTION
			//-------------------------------
			this.setAttributeValue(ATTRIB_STACK_TRACE, C30StringUtils.getStackTrace(exception), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);    		

			if (exception instanceof C30SDKException) {
				//this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException) exception).getExceptionCode().toString(), Constants.ATTRIB_TYPE_OUTPUT);     
				this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_TEXT, exception.getMessage(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (((C30SDKException)exception).getAdditionalErrorText() != null)
					this.setAttributeValue(ATTRIB_ADDT_ERROR_TEXT, ((C30SDKException)exception).getAdditionalErrorText(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}
			
			//-------------------------------
			// LIGHT WEIGHT OBJECT EXCEPTION
			//-------------------------------
			else if (exception instanceof C30SDKObjectException) {
				this.setAttributeValue(ATTRIB_MSG_DESCR, "C30SDK Object Exception", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_SHORT_DESCR, "OBJ EXCP", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (((C30SDKException)exception).getAdditionalErrorText() != null)
					this.setAttributeValue(ATTRIB_ADDT_ERROR_TEXT, ((C30SDKException)exception).getAdditionalErrorText(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

				//------------------------------------------
				// LIGHT WEIGHT OBJECT CONNECTION EXCEPTION
				//------------------------------------------
			} else if (exception instanceof C30SDKObjectConnectionException) {
				this.setAttributeValue(ATTRIB_MSG_DESCR, "C30SDK Connection Exception", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_SHORT_DESCR, "CONN EXCP", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (((C30SDKException)exception).getAdditionalErrorText() != null)
					this.setAttributeValue(ATTRIB_ADDT_ERROR_TEXT, ((C30SDKException)exception).getAdditionalErrorText(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

				//------------------------------------------
				// INVALID ATTRIBUTE EXCEPTION
				//------------------------------------------
			} else if (exception instanceof C30SDKInvalidAttributeException) {
				this.setAttributeValue(ATTRIB_MSG_DESCR, "Invalid Attribute Exception", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_SHORT_DESCR, "ATTR EXCP", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (((C30SDKException)exception).getAdditionalErrorText() != null)
					this.setAttributeValue(ATTRIB_ADDT_ERROR_TEXT, ((C30SDKException)exception).getAdditionalErrorText(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

				//------------------------------------------
				// INVALID CONFIGURATION EXCEPTION
				//------------------------------------------
			} else if (exception instanceof C30SDKInvalidConfigurationException) {
				this.setAttributeValue(ATTRIB_MSG_DESCR, "Invalid Configuration Exception", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_SHORT_DESCR, "CONF EXCP", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (((C30SDKException)exception).getAdditionalErrorText() != null)
					this.setAttributeValue(ATTRIB_ADDT_ERROR_TEXT, ((C30SDKException)exception).getAdditionalErrorText(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

				//------------------------------------------
				// REGULAR KENANs EXCEPTION
				//------------------------------------------
			}else if (exception instanceof C30SDKKenanFxCoreException) {
				this.setAttributeValue(ATTRIB_MSG_DESCR, ((C30SDKException)exception).getMessage() , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_SHORT_DESCR, ((C30SDKException)exception).getShortDescr() , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (((C30SDKException)exception).getAdditionalErrorText() != null)
					this.setAttributeValue(ATTRIB_ADDT_ERROR_TEXT, ((C30SDKException)exception).getAdditionalErrorText(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

				//------------------------------------------
				// REGULAR JAVA EXCEPTION
				//------------------------------------------
			}else if (exception instanceof C30SDKToolkitException) {
				this.setAttributeValue(ATTRIB_MSG_DESCR, ((C30SDKToolkitException)exception).getMessage() , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKToolkitException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_SHORT_DESCR, ((C30SDKToolkitException)exception).getShortDescr() , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (((C30SDKToolkitException)exception).getAdditionalErrorText() != null)
					this.setAttributeValue(ATTRIB_ADDT_ERROR_TEXT, ((C30SDKToolkitException)exception).getAdditionalErrorText(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

				//------------------------------------------
				// REGULAR JAVA EXCEPTION
				//------------------------------------------
			} else {
				String error = "A non-C30SDK framework exception was thrown.  Please review the stacktrace\n";
				error = error + "in the returned C30SDKExceptionMessage to understand what the problem is.\n";
				if (((C30SDKException)exception).getMessage()!= null || ((C30SDKException)exception).getMessage().equalsIgnoreCase(""))
				{	
					this.setAttributeValue(ATTRIB_MSG_DESCR, ((C30SDKException)exception).getMessage(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					this.setAttributeValue(ATTRIB_MSG_TEXT, error, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				}
				else{
					this.setAttributeValue(ATTRIB_MSG_DESCR, "Java Exception", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					this.setAttributeValue(ATTRIB_MSG_TEXT, error, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				}
				
				if (((C30SDKException)exception).getExceptionCode()!= null || ((C30SDKException)exception).getExceptionCode().equalsIgnoreCase(""))
					this.setAttributeValue(ATTRIB_EXCEPTION_CODE, ((C30SDKException)exception).getExceptionCode(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				else
					this.setAttributeValue(ATTRIB_EXCEPTION_CODE, "JAVA_EXCP", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

				//this.setAttributeValue(ATTRIB_MSG_DESCR, "Java Exception", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				//this.setAttributeValue(ATTRIB_MSG_SHORT_DESCR, "JAVA EXCP", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				//this.setAttributeValue(ATTRIB_EXCEPTION_CODE, "Generic Java Exception", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(ATTRIB_MSG_TEXT, error, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);		

			}

			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		} catch (Exception e) {
			log.error(e);
		}
		return this;
	}

	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}
