/*
 * C30SDKMessage.java
 *
 * Created on September 13, 2007, 9:38 AM
 *
 */

package com.cycle30.sdk.core.message;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;

/**
 * An abstract class encapsulating a message which is used by the framework.  All messages that
 * are created should be based off of this abstract class.
 * @author John Reeves
 */
public abstract class C30SDKMessage extends C30SDKObject {

    /**
     * Creates a new instance of C30SDKMessage.
     * @param factory Factory used to create C30SDKMessage objects
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     */
    //public C30SDKMessage(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {
    //    super(factory);
    //}

    /**
     * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
     * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
     * the framework to create objects.  It is also used by the BRE front end.
     * @param factory Factory used to create C30ServiceOrder objects
     * @param objectClass the class of the object being created.
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SDKMessage(final C30SDKFrameworkFactory factory, final Class objectClass) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectClass);

    }

    /**
     * Creates a new instance of C30SDKMessage.  This constructor is used for objects that are typically derived from a configuration
     * in the database.
     * @param factory Factory used to create C30ServiceOrder objects
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SDKMessage(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
}
