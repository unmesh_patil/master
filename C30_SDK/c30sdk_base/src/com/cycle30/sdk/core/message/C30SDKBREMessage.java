/*
 * C30SDKBREMessage.java
 *
 * Created on September 13, 2007, 9:38 AM
 *
 */

package com.cycle30.sdk.core.message;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Wrapper class encapsulating a Kenan/FX Business Rules Engine(BRE) message.  This object is 
 * used for passing messages returned by the BRE engine back to the application making the BRE
 * request.
 * @author Tom Ansley, Ranjith Nelluri
 */
public class C30SDKBREMessage extends C30SDKMessage {

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always 
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework 
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SDKBREMessage(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30SDKBREMessage.class);
    }
    
    /**
     * Method to initialize the attributes of this object.
     * @throws C30SDKInvalidAttributeException if an attribute could not be initialized correctly.
     */
    @Override
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException {

        //=================
        // Input Attributes
        //=================
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_DESCR, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_SHORT_DESCR, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_TEXT, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_VALIDATION_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_STEP_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MINOR_STEP_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_STEP_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_STEP_ACTION, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_RULE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MINOR_RULE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //===================
        // Output Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_DESCR, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_SHORT_DESCR, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_TEXT, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_VALIDATION_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_STEP_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MINOR_STEP_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_STEP_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_STEP_ACTION, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_RULE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BR_MINOR_RULE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }

    /**
     * Method to populate the output attributes with all available data.
     * @throws C30SDKInvalidAttributeException if there was an exception while trying to either get an input attribute
     * used to populate an output attribute or while setting an output attribute.
     */
    private void populateBREMessage() throws C30SDKInvalidAttributeException {
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_DESCR, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_DESCR, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_SHORT_DESCR, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_SEVERITY_SHORT_DESCR, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_TEXT, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_TEXT, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_VALIDATION_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_VALIDATION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_STEP_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_STEP_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MINOR_STEP_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MINOR_STEP_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_STEP_NAME, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_STEP_NAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_STEP_ACTION, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_STEP_ACTION, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_RULE_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MAJOR_RULE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MINOR_RULE_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BR_MINOR_RULE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }

    /**
     * @return the processed BRE message.
     * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
     */
    @Override
	public final C30SDKObject process() {
    	C30SDKObject lwo = null;
    	try {
    		populateBREMessage();
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			lwo = this;
		} catch (C30SDKInvalidAttributeException e) {
			lwo = this.createExceptionMessage(e);
		}
    	return lwo;
    }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
