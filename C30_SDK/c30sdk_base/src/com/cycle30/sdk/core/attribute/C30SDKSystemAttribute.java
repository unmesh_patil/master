package com.cycle30.sdk.core.attribute;

import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;

public class C30SDKSystemAttribute extends C30SDKAttribute {

    public C30SDKSystemAttribute(final String attribName, final Class attribType) {
		super(attribName, attribType);
	}

    /**
     * Gets the system value of the attribute.
     * @return value of the attribute
     */
    @Override
	public final Object getValue() {
        return super.getValue();
    }

    /**
     * Sets the system value of the attribute.
     * @param attribSystemValue value of the attribute
     * @throws C30SDKInvalidAttributeException if the attribute value can't be set
     */
    public final void setValue(final String attribSystemValue) throws C30SDKInvalidAttributeException {
    	this.setValue(createObjectFromString(attribSystemValue));

    }

}
