package com.cycle30.sdk.core.attribute;

import java.util.Comparator;

public class C30SDKNameComparator implements Comparator {

	public final int compare(final Object o1, final Object o2) {
		return ((C30SDKAttribute) o1).getName().compareTo(((C30SDKAttribute) o2).getName());
	}

}
