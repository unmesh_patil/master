/*
 * C30SDKAttribute.java
 *
 * Created on December 13, 2006, 11:36 AM
 *
 */

package com.cycle30.sdk.core.attribute;

/**
 * A single enumeration which is used in the GUI attributes.
 * @author Tom, Ranjith Kumar Nelluri
 */
public class C30SDKEnumeration implements Cloneable {

	//The following variable used to define the enumerations 
	/*@id Id of an attribute*/ 
	private String id="";
	private String key = "";
	private String displayValue = "";
	private Integer sortOrder;
	
    /**
     * Creates a new instance of C30SDKAttribute.
     */
    public C30SDKEnumeration() {    }

    public C30SDKEnumeration(final String key, final String displayValue, final Integer sortOrder, final String id) {
    	
    	setKey(key);
    	setDisplayValue(displayValue);
    	setSortOrder(sortOrder);
    	setId(id);
    }

    @Override
	public final Object clone() {
        C30SDKEnumeration copy = null;
        try {
            copy = (C30SDKEnumeration) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }

        return copy;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
 }
