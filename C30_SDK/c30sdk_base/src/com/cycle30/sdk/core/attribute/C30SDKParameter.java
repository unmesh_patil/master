/*
 * C30SDKAttribute.java
 *
 * Created on December 13, 2006, 11:36 AM
 *
 */

package com.cycle30.sdk.core.attribute;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.cycle30.sdk.util.C30CustomParamDef;

/**
 * A parameter holds information pertinent to GUI applications that are interested in displaying
 * the required attributes so that users can enter the values.  This requires that the C30SDKAttribute
 * as well as GUI level information is available to the users of the parameter object.
 * @author Tom Ansley
 */
public class C30SDKParameter implements Cloneable {

	private C30CustomParamDef parameter;
	private List attributes;
	private int guiControl;
	private String paramGroup;
	
    /**
     * Creates a new instance of C30SDKAttribute.
     * @param attribName the name of the attribute being set
     * @param attribType the class type that the attribute should exist as.
     */
    public C30SDKParameter(final String attribName, final int attribType) {
    	parameter = new C30CustomParamDef(attribName, attribType, C30CustomParamDef.INPUT);
    	attributes = new ArrayList();
    }

    /**
     * Creates a new instance of C30SDKAttribute.
     * @param attribName the name of the attribute being set
     * @param attribType the class type that the attribute should exist as.
     * @param maxLen the maximum length that the attribute can be in characters.
     */
    public C30SDKParameter(final String attribName, final int attribType, final int maxLen) {
    	parameter = new C30CustomParamDef(attribName, attribType, maxLen, C30CustomParamDef.INPUT);
    	attributes = new ArrayList();
    }

    /**
     * Creates a copy of the C30SDKAttribute object.
     * @return A copy of the C30SDKAttribute object
     */
    @Override
	public final Object clone() {
        C30SDKParameter copy = null;
        try {
            copy = (C30SDKParameter) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }

        return copy;
    }

    public final int getEntityType() {
        return parameter.getEntityType();
    }
    
    public final int getWildcardFlag() {
        return parameter.getWildcardFlag();
    }
    
    public final int getEnumerationCount() {
        return parameter.getNumParamValueChoices();
    }
    
    public final int getAttributeCount() {
        return attributes.size();
    }
    
    public final int getParamId() {
        return parameter.getParamId();
    }
    
    public final void setParamId(final int paramId) {
        parameter.setParamId(paramId);
    }
    
    public final String getParamName() {
        return parameter.getParamName();
    }
    
    public final int getParamType() {
        return parameter.getParamType();
    }
    
    public final int getParamMode() {
        return parameter.getParamMode();
    }
    
    public final Integer getParamOrder() {
        return parameter.getParamOrder();
    }
    
    public final void setParamOrder(final int paramOrder) {
        parameter.setParamOrder(paramOrder);
    }
    
    public final int getParamLength() {
        return parameter.getParamLength();
    }
    
    public final String getDisplayName() {
        return parameter.getDisplayName();
    }
    
    public final Integer getDisplayOrder() {
        return parameter.getDisplayOrder();
    }
    
    /**
     * Method to get the list of enumerations that have been associated with this parameter.
     * @return a list of C30SDKEnumerations.
     */
	public final Set getEnumerations() {
        
    	//-------------------------------------------------------------------------------------
    	// Internally this class uses C30CustomParamDef.  This means we use the C30NameValuePair 
    	// object to store enumerations.  But, we do not want to expose that class to the user 
    	// so we only give them the C30SDKEnumeration objects back.
    	//-------------------------------------------------------------------------------------
    	List nameValuePairs = parameter.getParamValueChoices();
        Set enumerations = new TreeSet(new C30SDKEnumerationComparator());
        for (int i = 0; i < nameValuePairs.size(); i++) {
        	enumerations.add( ((C30SDKEnumeration) nameValuePairs.get(i)).getValue()); 
        }
        return enumerations;
        
    }
    
    public final String getEnumerationValue(final int index) {
    	return ((C30SDKEnumeration) new ArrayList(getEnumerations()).get(index)).getValue();
    }
    
    /**
     * Method to add a C30SDKEnumeration into the parameters enumeration list.
     * @param enumeration
     */
    public final void setEnumeration(final C30SDKEnumeration enumeration) {
    	parameter.addParamValueChoice(enumeration.getValue(), enumeration);
    }
    
    public final List getAttributes() {
        return attributes;
    }
    
    public final int getSourcePosition() {
        return parameter.getSourcePosition();
    }
    
    public final int getSourceStartPosition() {
        return parameter.getSourceStartPosition();
    }
    
    public final int getSourceEndPosition() {
        return parameter.getSourceEndPosition();
    }
    
    public final void setEndPosition(final int endPosition) {
        parameter.setEndPosition(endPosition);
    }
    
    public final void setEnumerations(final ArrayList enumerations) {
        parameter.setParamValueChoices(enumerations);
    }
    
    public final void setAttributes(final List attributes) {
        this.attributes = attributes;
    }
    
    public final void setParamLength(final int length) {
    	parameter.setParamLength(length);
    }
    
    public final void setStartPosition(final int startPosition) {
        parameter.setStartPosition(startPosition);
    }
    
    public final void setAttribute(final C30SDKAttribute attribute) {
    	attributes.add(attribute);
    }
    
    public final void setUIControl(final int uiControl) {
    	guiControl = uiControl;
    }
    
    public final int getUIControl() {
    	return guiControl;
    }

    public final void setParamGroup(final String paramGroup) {
    	this.paramGroup = paramGroup;
    }
    
    public final String getParamGroup() {
    	return paramGroup;
    }
    
    @Override
	public final String toString() {
    	return "Display Name = " + this.getDisplayName() + ", Attribute Count = " + this.getAttributeCount() + ", Entity Type = " + this.getEntityType() + ", Enumeration Count = " 
    		+ this.getEnumerationCount() + ", Param Group = " + this.getParamGroup() + ", Param ID = " + this.getParamId() + ", Param Length = " + this.getParamLength() 
    		+ ", Param Mode = " + this.getParamMode() + ", Param Name = " + this.getParamName() + ", UI Control = " + this.getUIControl() + ", Display Order = " + this.getDisplayOrder() 
    		+ ", Param Order = " + this.getParamOrder() + ", Param Type = " + this.getParamType();
    }

}
