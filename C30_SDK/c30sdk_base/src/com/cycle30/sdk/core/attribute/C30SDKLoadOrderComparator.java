package com.cycle30.sdk.core.attribute;

import java.util.Comparator;

public class C30SDKLoadOrderComparator implements Comparator {

	public final int compare(final Object o1, final Object o2) {
		if (((C30SDKAttribute) o1).getLoadOrder().compareTo(((C30SDKAttribute) o2).getLoadOrder()) < 0) {
			return -1;
		} else {
			return 1;
		}
		//return ((C30SDKAttribute) o1).getLoadOrder().compareTo(((C30SDKAttribute) o2).getLoadOrder());
	}

}
