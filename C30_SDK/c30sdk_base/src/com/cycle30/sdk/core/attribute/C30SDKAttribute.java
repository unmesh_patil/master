/*
 * C30SDKAttribute.java
 *
 * Created on December 13, 2006, 11:36 AM
 *
 */

package com.cycle30.sdk.core.attribute;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cycle30.sdk.config.C30KenanConfigManager;
import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * A attribute of a C30SDK object.
 * Example - Order attributes, Service Order attributes, etc.
 * @author John Reeves, Ranjith Kumar Nelluri
 */
public class C30SDKAttribute implements Cloneable, Comparable {

        private static Logger log = Logger.getLogger(C30SDKAttribute.class);
    
	/*protected static final ResourceBundle exceptionResourceBundle =
		ResourceBundle.getBundle(Constants.C30_ERROR_TEXT_FILE_NAME);
	 */
	C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	C30SDKPropertyFileManager KenanExceptionResourceBundle = C30SDKPropertyFileManager.getInstance();

	private String attributeName;
	private Class attributeClass;
	private Integer attributeType;
	private Object attributeValue;
	private int maxLength;
	private boolean required;
	private boolean allowOverrides;
	private boolean appendSequence;
	private boolean exposeOutput;
	private String sequenceName;
	private String externalName;
	private String aliasName;
	private Format format;
	private Integer loadOrder;
	private boolean isInternal;
	private Integer enumerationId;
	private Integer keepatRoot;
	private C30SDKAttributeControl control;

	/**
	 * Gets the enumration Id for  that this attribute .
	 * @return a value of enumrationId.
	 */
	public Integer getEnumerationId() {
		return enumerationId;
	}

	public void setEnumerationId(Integer enumerationId) {
		this.enumerationId = enumerationId;
	}

	public Integer getKeepatRoot() {
		return keepatRoot;
	}

	public void setKeepatRoot(Integer keepatRoot) {
		this.keepatRoot = keepatRoot;
	}

	
	/**
	 * Creates a new instance of C30SDKAttribute.
	 * @param attribName the name of the attribute being set
	 * @param attribClass the class that the attribute should exist as.
	 */
	public C30SDKAttribute(final String attribName, final Class attribClass) {
		this(attribName, attribClass, C30SDKValueConstants.MAX_STRING_LENGTH);
	}

	/**
	 * Creates a new instance of C30SDKAttribute.
	 * @param attribName the name of the attribute being set
	 * @param attribClass the class that the attribute should exist as.
	 * @param maxLen the maximum length that the attribute can be in characters.
	 */
	public C30SDKAttribute(final String attribName, final Class attribClass, final int maxLen) {
		this.attributeName = attribName;
		this.attributeClass = attribClass;
		this.maxLength = maxLen;
		this.required = false;
		this.allowOverrides = true;
		this.appendSequence = false;
		this.exposeOutput = true;
		this.isInternal = true;
		this.loadOrder = new Integer(0);
		this.enumerationId=new Integer(0);
		this.keepatRoot= new Integer(0);
		this.setFormat(C30SDKValueConstants.DEFAULT_DATE_FORMAT);
	}

	/**
	 * Creates a copy of the C30SDKAttribute object.
	 * @return A copy of the C30SDKAttribute object
	 */
	@Override
	public final Object clone(){
		C30SDKAttribute copy = null;
		try {
			copy = (C30SDKAttribute) super.clone();
			copy.setAppendSequence(appendSequence);
			copy.setUIControl(control);
			copy.setExternalName(externalName);
			copy.setFormat(format);
			copy.setMaxLength(maxLength);
			copy.setName(attributeName);
			copy.setOverrideable(allowOverrides);
			copy.setRequired(required);
			copy.setSequenceName(sequenceName);
			copy.setAliasName(aliasName);
			copy.setExposeOutput(exposeOutput);
			copy.setAttributeClass(attributeClass);
			copy.setAttributeType(attributeType);
			copy.setLoadOrder(loadOrder);
			copy.setEnumerationId(enumerationId);
			copy.setKeepatRoot(keepatRoot);
			copy.setIsInternal(isInternal);
			if (attributeValue instanceof C30SDKObject && !attributeName.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT)) {
				copy.setValue(((C30SDKObject) attributeValue).clone());
			} else {
				copy.setValue(attributeValue);
			}

		} catch (Exception e) {
			//String error = "There was an attribute exception when trying to clone the following attribute:\n";
			String error =  " Attribute Name: "+attributeName + "\n";
			error = error + "Attribute Type: "+attributeType + "\n";
			error = error + "Attribute Class: "+attributeClass + "\n";
			error = error + "Attribute Value: "+attributeValue + "\n\n";
			//throw new InternalError(error + e.toString());
			try {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-041")+ error,e,"INVALID-ATTRIB-041");
			} catch (C30SDKInvalidAttributeException e1) {
				log.error(e1);

			}
		}

		return copy;
	}

	/**
	 * Gets the load order that this attribute .
	 * @return a value indicating when the attribute must have its value loaded during object construction.
	 */
	public final Integer getLoadOrder() {
		return this.loadOrder;
	}

	/**
	 * @param loadOrder
	 */
	public final void setLoadOrder(final Integer loadOrder) {
		this.loadOrder = loadOrder;
	}

	/**
	 * Gets the maximum length of the attribute value.
	 * @return maximum length of the attribute value
	 */
	public final int getMaxLength() {
		return this.maxLength;
	}

	/**
	 * Gets the name of the attribute.
	 * @return name of the attribute
	 */
	public final String getName() {
		return this.attributeName;
	}

	/**
	 * Sets the name of the attribute.
	 * @param attribName name of the attribute
	 */
	public final void setName(final String attribName) {
		this.attributeName = attribName;
	}

	/**
	 * Gets the Oracle sequence that will be appended to the end of the attribute value.
	 * @return Oracle sequence that will be appended to the end of the attribute value
	 */
	public final String getSequenceName() {
		return this.sequenceName;
	}

	/**
	 * Gets the external name that will used to identify the field from a client application.
	 * @return  Name used to represent the parameter from an external application
	 */
	public final String getExternalName() {
		return this.externalName;
	}

	/**
	 * Method to get the UI control of the attribute.
	 * @return the user interface control associated to the attribute.
	 */
	public final C30SDKAttributeControl getUIControl() {
		return control;
	}

	/**
	 * Gets the class representing the type of the attribute.
	 * @return class representing the type of the attribute
	 */
	public final Class getAttributeClass() {
		return this.attributeClass;
	}

	/**
	 * Gets the type of the attribute.
	 * @return the type of the attribute
	 */
	public final Integer getAttributeType() {
		return this.attributeType;
	}
	
	/**
	 * Gets the value of the attribute.
	 * @return value of the attribute
	 */
	public Object getValue() {
		return this.attributeValue;
	}

	/**
	 * Indicates if the attribute can be overwritten.
	 * @return boolean indicating if the attribute can be overwritten
	 */
	public final boolean isOverrideable() {
		return this.allowOverrides;
	}

	/**
	 * Indicates if the attribute is required.
	 * @return boolean indicating if the attribute is required
	 */
	public final boolean isRequired() {
		return this.required;
	}

	/**
	 * Indicates if an Oracle sequence should be appended to the attribute value
	 * in order to create a unique value.
	 * @return boolean indicating if an Oracle sequence should be appended to the
	 * attribute value.
	 */
	public final boolean isSequenceAppended() {
		return this.appendSequence;
	}

	/**
	 *     
	 * Indicates if the attribute value is eligible to be exposed in the HashMap output 
	 * @return boolean indicating if the attribute value is eligible to be exposed
	 * in the HashMap output 
	 */
	public final boolean isOutputExposed()
	{  
		return exposeOutput;
	}

	/**
	 *     
	 * Indicates if the attribute value is an internally defined attribute used
	 * for processing of C30SDK objects
	 * @return boolean indicating if the attribute value is lwo internal
	 * in the HashMap output 
	 */
	public final boolean isInternal()
	{  
		return isInternal;
	}

	/**
	 * gets the alias name for the attribute for output to the HashMap
	 * @return String alias name for the attribute for output to the HashMap
	 */
	public final String getAliasName()
	{
		return aliasName;
	}

	/**
	 * Sets the alias name for the attribute for output to the HashMap
	 * @param alias
	 */
	public final void setAliasName(String alias)
	{
		aliasName = alias;
	}

	/**
	 * Sets the indicator if attribute value is eligible to be exposed in the HashMap output 
	 * @param exp
	 */
	public final void setExposeOutput(boolean exp) 
	{
		exposeOutput = exp;
	}

	/**
	 * Sets the indicator if an Oracle sequence should be appended to the attribute
	 * value in order to create a unique value.
	 * @param appendSeq boolean indicating if an Oracle sequence should be appended
	 * to the attribute value.
	 */
	public final void setAppendSequence(final boolean appendSeq) {
		this.appendSequence = appendSeq;
	}

	/**
	 * Sets the date and time format to use when converting string
	 * values to Date objects.
	 * @param pattern the pattern describing the date and time format
	 */
	public final void setFormat(final String pattern) {
		if (attributeClass.equals(Date.class)) {
			this.format = new SimpleDateFormat(pattern);

		} else if (attributeClass.equals(Integer.class)) {
			this.format = new DecimalFormat(pattern);

		} else if (attributeClass.equals(Short.class)) {
			this.format = new DecimalFormat(pattern);
		}

	}

	/**
	 * This is to allow the cloning method to be able
	 * to get the format object. 
	 * @param format the format being set.
	 */
	public final void setFormat(final Format format) {
		this.format = format;
	}

	/**
	 * Gets the format associated with this attribute.
	 * @return the format of the object
	 */
	public final Format getFormat() {
		return format;
	}

	/**
	 * Sets the indicator if the attribute can be overwritten.
	 * @param overrideable boolean indicating if the attribute can be overwritten
	 */
	public final void setOverrideable(final boolean overrideable) {
		this.allowOverrides = overrideable;
	}

	/**
	 * Sets the maximum length of the attribute value.
	 * @param maxLen maximum length of the attribute value
	 */
	public final void setMaxLength(final int maxLen) {
		this.maxLength = maxLen;
	}

	/**
	 * Sets if the attribute is requried or not.
	 * @param req boolean indicating if the attribute is required
	 */
	public final void setRequired(final boolean req) {
		this.required = req;
	}

	/**
	 * Sets if the attribute is internal to application or not.
	 * @param isInternal boolean indicating if the attribute is internal to lwo
	 */
	public final void setIsInternal(final boolean isInternal) {
		this.isInternal = isInternal;
	}

	/**
	 * Sets the Oracle sequence that will be appended to the end of the attribute value.
	 * @param seqName Oracle sequence that will be appended to the end of the attribute value
	 */
	public final void setSequenceName(final String seqName) {
		this.sequenceName = seqName;
	}

	/**
	 * Sets the external name that will used to identify the field from a client application.
	 * @param extName Name used to represent the parameter from an external application
	 */
	public final void setExternalName(final String extName) {
		this.externalName = extName;
	}

	/**
	 * Method to set the UI control of the attribute.
	 * @param uiControl the user interface control associated to the attribute.
	 */
	public final void setUIControl(final C30SDKAttributeControl uiControl) {
		this.control = uiControl;
	}

	/**
	 * Sets the class of the attribute.
	 * @param attribClass class of the attribute
	 */
	public final void setAttributeClass(final Class attribClass) {
		this.attributeClass = attribClass;
	}

	/**
	 * Sets the type of the attribute.
	 * @param attribType int representing the type of the attribute
	 */
	public final void setAttributeType(final Integer attribType) {
		this.attributeType = attribType;
	}

	/**
	 * Sets the value of the attribute.
	 * @param attribValue value of the attribute
	 * @throws C30SDKInvalidAttributeException if the attribute value can't be set
	 */
	public final void setValue(final Object attribValue) throws C30SDKInvalidAttributeException {
		if (attribValue instanceof String) {
			this.attributeValue = createObjectFromString((String)attribValue);
		} else {
			this.attributeValue = attribValue;
		}

	}

	/**
	 * @param inputAttribValue the value being converted to an object
	 * @return the object created from the attribute value.
	 * @throws C30SDKInvalidAttributeException if there was an exception trying to convert the value
	 */
	protected final Object createObjectFromString(final String inputAttribValue) throws C30SDKInvalidAttributeException {

		String attribValue = inputAttribValue;

		Object value = null;

		String error = "Unable to set attribute value for attribute '" + this.getName()
		+ "' - value '" + attribValue + "' ";

		if (attribValue != null && !attribValue.equals("")) {

			if (this.getAttributeClass().equals(Boolean.class)) {

				//--------------------------------------
				// Boolean values must be TRUE or FALSE
				//--------------------------------------
				if (attribValue.equals("0")) {
					attribValue = Boolean.FALSE.toString();
				} else if (attribValue.equals("1")) {
					attribValue = Boolean.TRUE.toString();
				}

				if (!attribValue.toLowerCase().equals(Boolean.FALSE.toString())
						&& !attribValue.toLowerCase().equals(Boolean.TRUE.toString())) {
					error += "must be 'true' or 'false'";
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-042"),"INVALID-ATTRIB-042");
				} else {
					value = new Boolean(attribValue);
				}

			} else if (this.getAttributeClass().equals(Date.class)) {

				//----------------------------------------
				// Date values must match the date format
				//----------------------------------------
				try {
					value = ((SimpleDateFormat) this.getFormat()).parse(attribValue);
				} catch (ParseException e) {
					error += "does not match the pattern " + ((SimpleDateFormat) this.getFormat()).toPattern();
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-043"),e,"INVALID-ATTRIB-043");
				}

			} else if (this.getAttributeClass().equals(Integer.class)) {

				//---------------------------------------
				// Integer values must be valid integers
				//---------------------------------------
				try {
					value = new Integer(attribValue);
				} catch (NumberFormatException e) {
					error += "is not a valid integer";
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-044"),e,"INVALID-ATTRIB-044");
				}

			} else if (this.getAttributeClass().equals(BigInteger.class)) {

				//---------------------------------------
				// Integer values must be valid integers
				//---------------------------------------
				try {
					value = new BigInteger(attribValue);
				} catch (NumberFormatException e) {
					error += "is not a valid big integer";
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-045"),e, "INVALID-ATTRIB-045");
				}

			} else if (this.getAttributeClass().equals(Short.class)) {

				//----------------------------------
				// Short values must be valid shorts
				//-----------------------------------
				try {
					value = new Short(attribValue);
				} catch (NumberFormatException e) {
					error += "is not a valid short";
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-046"),e, "INVALID-ATTRIB-046");
				}

			} else if (this.getAttributeClass().equals(String.class)) {

				//----------------------------------------------------
				// String values must be less than the maximum length
				//----------------------------------------------------
				if (attribValue.length() > this.getMaxLength()) {
					error += "must be " + this.getMaxLength() + " characters or less";
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-047"),"INVALID-ATTRIB-047");
				} else {
					value = attribValue;
				}

			}
		}

		return value;
	}

	/**
	 * This is the method that allows the attributes to be ordered.  
	 * The ordering is done by comparing the LOADORDER.
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object arg0) {

		int result = 0;

		if (arg0 instanceof C30SDKAttribute) {
			if (this.getLoadOrder().compareTo( ((C30SDKAttribute) arg0).getLoadOrder()) == 0) {
				result = 1;
			} else {
				result = this.getLoadOrder().compareTo( ((C30SDKAttribute) arg0).getLoadOrder());
			}
		} else {
			result = -1;
		}
		return result;
	}

}
