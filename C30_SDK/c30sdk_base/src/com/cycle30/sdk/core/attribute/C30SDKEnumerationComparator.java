package com.cycle30.sdk.core.attribute;

import java.util.Comparator;

public class C30SDKEnumerationComparator implements Comparator {

	public final int compare(final Object o1, final Object o2) {
		return ((C30SDKEnumeration) o1).getSortOrder().compareTo(((C30SDKEnumeration) o2).getSortOrder());
	}

}
