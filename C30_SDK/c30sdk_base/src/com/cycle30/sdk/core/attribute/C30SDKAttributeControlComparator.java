package com.cycle30.sdk.core.attribute;

import java.util.Comparator;

public class C30SDKAttributeControlComparator implements Comparator {

	public final int compare(final Object o1, final Object o2) {
		return ((C30SDKAttributeControl) o1).getParamOrder().compareTo(((C30SDKAttributeControl) o2).getParamOrder());
	}

}
