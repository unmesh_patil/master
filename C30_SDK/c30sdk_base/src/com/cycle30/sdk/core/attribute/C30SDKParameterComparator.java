package com.cycle30.sdk.core.attribute;

import java.util.Comparator;

public class C30SDKParameterComparator implements Comparator {

	public final int compare(final Object o1, final Object o2) {
		return new Integer( ((C30SDKParameter) o1).getParamId()).compareTo(new Integer(((C30SDKParameter) o2).getParamId()));
	}

}
