package com.cycle30.sdk.datasource.kenan.output;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;

/**
 * A data source that uses an xml file datasource to create C30SDK transactions.
 * @author anst01
 */
public class C30tXMLOutputDataSource implements C30OutputDataSource {

    private static Logger log = Logger.getLogger(C30tXMLOutputDataSource.class);
    private C30SDKObject data;
    
	/**
	 * Constructor which creates an xml file using data from a C30SDK object.
	 */
	public C30tXMLOutputDataSource() {
	}

	/**
	 * Method which will populate the XML file with the data retrieved from the C30SDK object input as data.
	 * @param data the C30SDK object that should be processed.
	 * @return the file containing the XML.
	 * @exception C30SDKInvalidAttributeException if an attribute is in an invalid state while processing the output.
	 */
	public final Object processOutput(final C30SDKObject data) throws C30SDKInvalidAttributeException {

		//------------------
		// get the XML data
		//------------------
		this.data = data;
		Map outputMap = this.data.getAttributes();
		StringBuffer output = processMap(outputMap, 0);
		
		log.debug("\n" + output);
		
		return output.toString();
	}
	
	/**
	 * Method to process the given Map object.  For this concrete class processing includes creating
	 * a semi-XML string representation using the maps name/value attributes.
	 * @param data the Map object containing the name/value pair attribute values.
	 * @param inputLevel the level in the map at which processing should start.
	 * @return the semi-XML string based representation.
	 */
	private StringBuffer processMap(final Map data, final int inputLevel) {
		StringBuffer output = new StringBuffer();
		//Iterator iter = data.keySet().iterator();
		String key = "";
		Object value = null;
		int level = inputLevel;
		
		List l = new ArrayList(data.keySet());
		Collections.sort(l);

		//------------------------------------------------
		// This is purely for aesthetics.  Nice tabbing 
		// so that things can be viewed better on output.
		//------------------------------------------------
		String tabs = "";
		for(int j = 0; j <= level; j++) {
			tabs = tabs + "   ";
		}
		
		output.append(tabs.substring(3) + "<" + data.get("C30SDKObjectClass") + ">\n"); 
		
		for (int i = 0; i < l.size(); i++) {
			key = (String) l.get(i);
			value = data.get(key);
			
			if (value instanceof String) {
				if (!key.equals("C30SDKObjectClass") ) {
					output.append(tabs + "<" + key + ">" + (String) value + "</" + key + ">\n"); 
				}
			} else if (value instanceof Integer) {
				output.append(tabs + "<" + key + ">" + value + "</" + key + ">\n"); 
			} else if (value instanceof BigInteger) {
				output.append(tabs + "<" + key + ">" + value + "</" + key + ">\n"); 
			} else if (value instanceof Short) {
				output.append(tabs + "<" + key + ">" + value + "</" + key + ">\n"); 
			} else if (value instanceof Date) {
				output.append(tabs + "<" + key + ">" + value + "</" + key + ">\n"); 
			} else if (value instanceof Boolean) {
				output.append(tabs + "<" + key + ">" + ((Boolean) value).booleanValue() + "</" + key + ">\n"); 
			} else if (value instanceof Map) {
				output.append(processMap( (Map) value, ++level));
			} else if (value == null) {
				output.append(tabs + "<" + key + ">NULL</" + key + ">\n"); 
			} else {
				output.append(tabs + "<" + key + ">" + value + "</" + key + ">\n");
			}
		}
		
		output.append(tabs.substring(3) + "</" + data.get("C30SDKObjectClass") + ">\n"); 
		return output;
	}
	
}
