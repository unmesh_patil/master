package com.cycle30.sdk.datasource.kenan.input;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cycle30.sdk.core.framework.C30SDKCollection;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30XMLFileDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * A data source that uses an xml file datasource to create C30SDK transactions.
 * @author Tom Ansley
 */
public class C30XMLDataSource implements C30DataSource {

    private static Logger log = Logger.getLogger(C30XMLDataSource.class);
	private C30XMLFileDataSource dataSource;
	private C30SDKFrameworkFactory factory;

	
	/**
	 * Constructor which creates a data source using an xml file for use in creating transactions.
	 * @param xmlFile xml file containing transaction information.
	 */
	public C30XMLDataSource(final File xmlFile) {
		dataSource = new C30XMLFileDataSource(xmlFile);
		factory = null;
	}

	/**
	 * Constructor which creates a data source using a string which represents a file location for use in creating transactions.
	 * @param data either the xml file location containing transaction information or the actual xml data document.
	 */
	public C30XMLDataSource(final String data) {
    	File xmlFile = new File(data);
    	if (xmlFile.canRead()) {
    		dataSource = new C30XMLFileDataSource(xmlFile);
    	} else {
    		dataSource = new C30XMLFileDataSource(data);
    	}

		factory = null;
	}

	/**
	 * Constructor which creates a data source using an xml file for use in creating transactions.
	 * @param xmlFile xml file containing transaction information.
	 * @param lwoFactory the factory used to create the transactions.
	 */
	public C30XMLDataSource(final File xmlFile, final C30SDKFrameworkFactory lwoFactory) {
		dataSource = new C30XMLFileDataSource(xmlFile);
		factory = lwoFactory;
	}

	/**
	 * Constructor which creates a data source using a string which represents a file location for use in creating transactions.
	 * @param xmlFileLocation the xml file location containing transaction information.
	 * @param lwoFactory the factory used to create the transactions.
	 */
	public C30XMLDataSource(final String xmlFileLocation, final C30SDKFrameworkFactory lwoFactory) {
    	File xmlFile = new File(xmlFileLocation);
		dataSource = new C30XMLFileDataSource(xmlFile);
		factory = lwoFactory;
	}

	public C30SDKFrameworkFactory getC30SDKFactory() {
		return factory;
	}

	/**
	 * This method uses the input XML string or file to create the transactions stored in the XML.  If
	 * necessary the C30SDK factory is also created and made available.
	 * @return the list of transactions
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKInvalidConfigurationException
	 */
	public final C30SDKObject process() throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException {

		NodeList attributes = null;
		C30SDKObject requests = null;
		C30SDKObject responses = null;
		String action = C30SDKAttributeConstants.ACTION_PROCESS;
		String requestApp = "";
		String requestDate = "";
		String externalId = "";

		try {

			//=============================================================
			// if we do not have a factory then try and get factory making
			// credentials from xml in order to be able to create one.
			//=============================================================
			if (factory == null) {

				String username = null;
				String password = null;
				String userRealm = null;
				String version = null;

				//get the main nodes children and process
				attributes = dataSource.getNodeChildren(C30SDKAttributeConstants.XML_TAG_ROOT, 0);
				String nodeName = null;
				String nodeValue = null;
				for (int i = 0; i < attributes.getLength(); i++) {
					if (attributes.item(i).getNodeType() == Node.ELEMENT_NODE) {
						nodeName = attributes.item(i).getNodeName();
						nodeValue = attributes.item(i).getFirstChild().getNodeValue();
						if (nodeName.equals(C30SDKAttributeConstants.XML_FACTORY_ATTRIB_USERNAME)) {
							username = nodeValue;
							log.debug("Username = "+username);
						} else if (nodeName.equals(C30SDKAttributeConstants.XML_FACTORY_ATTRIB_PASSWORD)) {
							password = nodeValue;
						} else if (nodeName.equals(C30SDKAttributeConstants.XML_FACTORY_ATTRIB_USER_REALM)) {
							userRealm = nodeValue;
							log.debug("User Realm = " + userRealm);
						} else if (nodeName.equals(C30SDKAttributeConstants.XML_FACTORY_ATTRIB_VERSION)) {
							version = nodeValue;
							log.debug("Factory Version = "+version);
						} else if (nodeName.equals(C30SDKAttributeConstants.XML_ATTRIB_TRANS_ACTION)) {
							action = nodeValue;
							log.debug("Trans Action = "+action);
						} else if (nodeName.equals(C30SDKAttributeConstants.XML_ATTRIB_REQUEST_APP)) {
							requestApp = nodeValue;
							log.debug("Request App = "+requestApp);
						} else if (nodeName.equals(C30SDKAttributeConstants.XML_ATTRIB_REQUEST_DATE)) {
							requestDate = nodeValue;
							log.debug("Request Date = "+requestDate);
						} else if (nodeName.equals(C30SDKAttributeConstants.XML_ATTRIB_EXTERNAL_ID)) {
							externalId = nodeValue;
							log.debug("External ID = "+externalId);
						}
					}
				}

				if (username == null || password == null || userRealm == null || version == null) {
					String message = "The data source either did not have a C30SDKFrameworkFactory passed into its constructor\n";
					message = message + "or the XML being parsed did not have the correct credentials associated to it.  Please\n";
					message = message + "see the credentials below for the values given in the XML file\n";
					message = message + "<TransUser>"+username+"</TransUser>\n";
					message = message + "<TransPassword>"+password+"</TransPassword>\n";
					message = message + "<TransUserRealm>"+userRealm+"</TransUserRealm>\n";
					message = message + "<TransFactoryVersion>"+version+"</TransFactoryVersion>\n";
					throw new C30SDKObjectConnectionException(message);
				}

				String sessionID = username + userRealm + version + new Date().getTime();
				log.debug("Session ID = "+sessionID);

				factory = new C30SDKFrameworkFactory(userRealm, username, sessionID, password, version);

			}

			HashMap attribs = null;
			String transactionName = null;
			C30SDKObject transaction = null;

			//===============================================================
			//create a container to hold all the transactions and responses.
			//===============================================================
			requests = factory.createC30SDKObject(C30SDKCollection.class, new Object[0]);
			responses = factory.createC30SDKObject(C30SDKCollection.class, new Object[0]);

			//=========================================
			//loop through transactions creating LWO's
			//=========================================
			for (int i = 0; i < dataSource.getNodeCount(C30SDKAttributeConstants.XML_TAG_TRANSACTION); i++) {

				//get transaction name
				transactionName = dataSource.getNodeAttributeValue(C30SDKAttributeConstants.XML_TAG_TRANSACTION, i, C30SDKAttributeConstants.XML_TAG_TRANSACTION_NAME);

				//create transaction from configuration
				transaction = factory.createC30SDKObject(transactionName);

				//get transaction attributes to be set
				attributes = dataSource.getNodeChildren(C30SDKAttributeConstants.XML_TAG_TRANSACTION, i);

				//loop through attributes
				for (int j = 0; j < attributes.getLength(); j++) {
					if (attributes.item(j).getNodeType() == Node.ELEMENT_NODE) {
						attribs = dataSource.getNodeAttributes(attributes.item(j));
						log.debug("Adding name/value pair = " + attribs.get(C30SDKAttributeConstants.XML_TAG_ATTR_NAME) + "/" + attribs.get(C30SDKAttributeConstants.XML_TAG_ATTR_VALUE));
						transaction.setNameValuePair( (String) attribs.get(C30SDKAttributeConstants.XML_TAG_ATTR_NAME), attribs.get(C30SDKAttributeConstants.XML_TAG_ATTR_VALUE));
					}
				}

				if (requestApp != null) {
					transaction.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_APP, requestApp, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				if (requestDate != null) {
					transaction.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE, requestDate, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				if (externalId != null) {
					transaction.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PERSIST_EXTERNAL_ID, externalId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}

				requests.addC30SDKObject(transaction);

			}

			//==========================================================
			//go through each object either processing or persisting it
			//==========================================================
			C30SDKObject response = null;
			for (int i = 0; i < requests.getC30SDKObjectCount(); i++) {
				transaction = requests.getC30SDKObject(i);
				if (action.equals(C30SDKAttributeConstants.ACTION_PROCESS)) {
					response = transaction.process();
				} else {
					transaction.persist();
					response = transaction;
				}
				responses.addC30SDKObject(response);
			}

		} catch (C30SDKObjectException lwoe) {
		    log.error(lwoe);
		} catch (C30SDKObjectConnectionException lwce) {
		    log.error(lwce);
		} catch (C30SDKInvalidAttributeException iae) {
		    log.error(iae);
		} catch (Exception e) {
		    log.error(e);
		}
		return responses;
	}

}
