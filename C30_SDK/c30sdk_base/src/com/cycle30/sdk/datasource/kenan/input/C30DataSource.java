/*
 * C30DataSource.java
 *
 * Created on June 3rd, 2008, 1:49 PM
 */
 package com.cycle30.sdk.datasource.kenan.input;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;

/**
 * This interface defines the method any object that would like to be a source of data
 * for a C30SDK custom application needs to implement.
 * @author Tom Ansley
 */
public interface C30DataSource {

    /**
     * Retrieves all C30SDK transactions from the data source.
	 * @return a collection of C30SDK transactions
	 */
	C30SDKObject process() throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException;

}
