/*
 * C30DataSource.java
 *
 * Created on June 3rd, 2008, 1:49 PM
 */

 package com.cycle30.sdk.datasource.kenan.output;


import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;

/**
 * This interface defines the method any object that would like to be a source of data
 * for a C30SDK custom application needs to implement.
 * @author John Reeves
 */
public interface C30OutputDataSource {

	/**
	 * Method which will populate the XML file with the data retrieved from the C30SDK object input as data.
	 * @param data the C30SDK object that should be processed.
	 * @return the file containing the XML.
	 * @exception C30SDKInvalidAttributeException if an attribute is in an invalid state while processing the output.
	 */
	Object processOutput(C30SDKObject data) throws C30SDKInvalidAttributeException;

}
