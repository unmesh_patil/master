package com.cycle30.sdk;

/** This tag is used to identify the Build version of the FX
 * 
 * @author rnelluri
 *
 */
public class C30SDKBaseLatestBuildVersion {

	/** This tag is used to identify the Build version of the FX, to verify the build
	 * dependencies and smoke testing few basic issues etc.,
	 * Also, we use the log.error so that this will not be ignored in the low log level
	 * 
	 * Also this need to be updated every time we request a build.
	 */
	public static String LATEST_BUILD_VERSION="2012-10-04T10:19:00";
}
