package com.cycle30.sdk.object;

import java.util.HashMap;

/*Pakcage that stores all the Constants.
 * 
 */
public class C30SDKValueConstants {

	//This defines the Kenan Database ServerIds only.
	public static final Integer C30_KENAN_CATALOG_SERVER_ID = new Integer(1);
	public static final Integer C30_KENAN_ADMIN_SERVER_ID = new Integer(2);
	public static final Integer C30_KENAN_DEFAULT_CUST_SERVER_ID = new Integer(3);
	public static  Integer C30_KENAN_TOTAL_CUST_SERVERS = new Integer(1);
	public static final Integer IS_NOT_PERSISTED = new Integer("0");
	public static final Integer GLOBAL_LANGUAGE_CODE = new Integer(1);

	/** Default date format used to convert strings (MM/dd/yyyy). */
	public static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";

	/** Maximum length for a string. */
	public static final int MAX_STRING_LENGTH = 1000000;

	// Error Message Resource file name
	public static final String C30_ERROR_TEXT_FILE_NAME = "/C30SDK_Core_Error_Text_en.properties";
	public static final String C30_KENAN_ERROR_TEXT_FILE_NAME = "/C30SDK_Kenan_Error_Text_en.properties";
	public static final String KENAN_CONFIG_LIST_FILE = "/C30SDK_Kenan_Config.properties";
	public static final String CACHE_PROPERTIES_FILE_NAME = "/C30SDK_Cache.properties";
	public static final String ORDER_PROPERTIES_FILE_NAME = "/C30SDK_Order.properties";

	//Resourcenames
	public static final String RESOURCE_C30_ERROR_TEXT_FILE_NAME = "C30SDK_Core_Error_Text";
	public static final String RESOURCE_C30_KENAN_ERROR_TEXT_FILE_NAME = "C30SDK_Kenan_Error_Text";
	public static final String RESOURCE_KENAN_CONFIG_LIST_FILE = "C30SDK_Kenan_Config";
	public static final String RESOURCE_CACHE_PROPERTIES_FILE_NAME = "C30SDK_Cache";
	public static final String RESOURCE_ORDER_PROPERTIES_FILE_NAME = "C30SDK_Order";

	// ActionTypes
	public static final String C30_ACTION_ADD ="Add";
	public static final String C30_ACTION_CHANGE ="Change";
	public static final String C30_ACTION_DISCONNECT ="Disconnect";
	public static final String C30_ACTION_SWAP ="Swap";	

	public static final String C30_ORDER_SCHEMA = "/com/cycle30/sdk/schema/C30BillingOrder.xsd";
	public static final String C30_PLAN_SCHEMA = "/com/cycle30/sdk/schema/C30BillingPlan.xsd";
	public static final String C30_ACCOUNT_SCHEMA = "/com/cycle30/sdk/schema/C30BillingAccount.xsd";

	//UI Control Constatnts
	public static final String C30_ACCOUNT_API_PROC_NAME="C30_Account_Api";


	public static final int UI_TYPE_TEXTFIELD = 0;
	public static final int UI_TYPE_CHECKBOX = 1;
	public static final int UI_TYPE_OPTION = 2;
	public static final int UI_TYPE_COMBOBOX = 3;
	public static final int UI_TYPE_DATE = 4;


	//C30SDKObject
	public static final Integer ATTRIB_TYPE_INPUT = new Integer(1);
	public static final Integer ATTRIB_TYPE_SYSTEM = new Integer(2);
	public static final Integer ATTRIB_TYPE_OUTPUT = new Integer(4);


	// Item actions
	public static final Integer ITEM_ACTION_ID_CREATE = new Integer(1);
	public static final Integer ITEM_ACTION_ID_FIND = new Integer(2);
	public static final Integer SERVICE_ITEM_ACTION_ID_FIND_ALL = new Integer(4);


	// Account Note related Constants
	public static final Integer NOTE_SOURCE_MANUAL = new Integer(1);
	public static final Integer NOTE_SOURCE_SYSTEM_GENERATED = new Integer(0);
	public static final Integer PARENT_CODE = new Integer(1);
	public static final Integer NOTE_SOURCE = new Integer(1);
	public static final Integer NOTE_PARENT_CODE = new Integer(0); // default is Account (CMF)
	public static final Integer NOTE_TEMPLATE_ID = new Integer(81);  // free form
	public static final Integer CIT_REASON_ID_DEFAULT = new Integer(1);
	public static final Integer CIT_CHANNEL_ID_DEFAULT = new Integer(1);
	public static final Integer CIT_IMPORTANCE_ID_MEDIUM = new Integer(2);



	// Search attributes
	public static final String ATTRIB_SEARCH_START_RECORD = "StartRecord";
	public static final String ATTRIB_SEARCH_RETURN_SIZE = "ReturnSize";
	public static final String ATTRIB_SEARCH_TOTAL_COUNT = "TotalCount";
	public static final String ATTRIB_SEARCH_TOTAL_RETURNED = "TotalReturned";
	public static final String ATTRIB_SEARCH_RECORD_NUMBER = "RecordNumber";
	public static final String ATTRIB_LAST_RECORD_RETURNED_NUMBER = "LastRecordReturned";
	
	//Prepaid OUTPUT Attributes
	public static final String ATTRIB_PREPAID_BALANCE_XML = "PrepaidBalanceXML";
	public static final String ATTRIB_PREPAID_VOUCHER_XML = "PrepaidVoucherDetailsXML";
	public static final String ATTRIB_PREPAID_ACCOUNT_XML = "PrepaidAccountDetailsXML";
	public static final String ATTRIB_PREPAID_PAYMENT_XML = "PrepaidPaymentXML";
	public static final String ATTRIB_RESPONSE = "Response";	
	
	//Prepaid Payment Actions
	public final static String PREPAID_PAYMENT_REGISTER_ACTION_TYPE="REGISTER";
	public final static String PREPAID_PAYMENT_ACTION_TYPE="PAYMENT";
	
	public final static String PREPAID_PAYMENT_CALL_BACK_TYPE="Payment";
	public final static String LIFECYCLE_CALL_BACK_TYPE="LifeCycleOrder";

	//Prepaid Phone Inventory OUTPUT Attributes 
	public static final String ATTRIB_PREPAID_PHONE_INVENTORY_XML = "PrepaidPhoneInventoryXML";

	//Prepaid Account History OUTPUT Attributes 
	public static final String ATTRIB_PREPAID_ACCOUNT_HISTORY_XML = "PrepaidAccountHistoryXML";
	
	public static final String C30_PROV_SPS_PREPAID_VALUE = "prepaid";
	public static final String C30_PROV_SPS_PREPAID_TRUE = "TRUE";
	
	//Attributes
	public static final Integer ITEM_ACTION_ID_PAYMENT_CREATE = new Integer(1);
	public static final Integer ITEM_ACTION_ID_PAYMENT_FIND = new Integer(2);
	public static final Integer ITEM_ACTION_ID_PAYMENT_DELETE = new Integer(3);
	public static final Integer ITEM_ACTION_ID_PAYMENT_REVERSAL = new Integer(4);
	public static final Integer ITEM_ACTION_ID_PAYMENT_CREATE_WITH_ACCT_NOTES = new Integer(5);

	//Item Types
	public static final String ITEM_TYPE_ACCOUNT = "Account";
	public static final String ITEM_TYPE_PLAN = "Plan";
	public static final String ITEM_TYPE_SERVICE = "Service";
	public static final String ITEM_TYPE_COMPONENT = "Component";
	public static final String ITEM_TYPE_IDENTIFIER = "Identifier";
	
	//Exceptions
	/*	public static final Integer EXCEPTION_NO_ACCOUNT_FOUND = new Integer(11101);
	public static final Integer EXCEPTION_MULTIPLE_ACCOUNTS_FOUND = new Integer(11102);
	public static final Integer EXCEPTION_NO_BILL_GEOCODE_FOUND = new Integer(11103);
	public static final Integer EXCEPTION_CUSTOMER_SERVER_ID_ERROR = new Integer(11104);
	public static final Integer EXCEPTION_INVALID_NOTE_CODE = new Integer(10301);
	public static final Integer EXCEPTION_INVALID_EQUIP_EXTERNAL_ID = new Integer(10302);
	public static final Integer EXCEPTION_INVALID_NOTE_SOURCE = new Integer(10303);
	public static final Integer EXCEPTION_NO_USAGE = new Integer(10001);
	public static final Integer EXCEPTION_MULTIPLE_BILLED_USAGE = new Integer(10002);
	public static final Integer EXCEPTION_MULTIPLE_UNBILLED_USAGE = new Integer(10003);
	public static final Integer EXCEPTION_MULTIPLE_ADJUSTMENT_TYPES = new Integer(10004);
	public static final Integer EXCEPTION_AMOUNT_EXCEEDS_CHARGE = new Integer(10005);
	public static final Integer EXCEPTION_MIU_STORED_PROC_ERROR = new Integer(10006);
	public static final Integer EXCEPTION_INVALID_ITEM_ACTION_ID = new Integer(11000);
	public static final Integer EXCEPTION_NO_ACTIVE_SERVICE_FOUND = new Integer(11001);
	public static final Integer EXCEPTION_MULTIPLE_ACTIVE_SERVICE_FOUND = new Integer(11002);
	public static final Integer EXCEPTION_ADDRESS_CHANGE_UNAVAILABLE = new Integer(11003);
	public static final Integer EXCEPTION_ADDRESS_NOT_SERVICEABLE = new Integer(11004);
	public static final Integer EXCEPTION_SERVICEABILITY_CHECK = new Integer(11005);
	public static final Integer EXCEPTION_INVALID_ASSOCIATION_STATUS = new Integer(21003);
	public static final Integer EXCEPTION_NO_SERVICE_ADDRESS_FOUND = new Integer(31001);
	public static final Integer EXCEPTION_MULTIPLE_SERVICE_ADDRESS_FOUND = new Integer(31002);
	public static final Integer EXCEPTION_INVALID_DEPOSIT_TYPE = new Integer(10201);
	public static final Integer EXCEPTION_INVALID_DEPOSIT_AMOUNT = new Integer(10202);
	public static final Integer EXCEPTION_INSTANTIATION_ERROR = new Integer(1002);
	public static final Integer EXCEPTION_DEFAULT_ATTR_LOAD_ERROR = new Integer(1003);
	public static final Integer EXCEPTION_ATTR_VALIDATION_ERROR = new Integer(1000);     
	public static final Integer EXCEPTION_INVALID_FIND_ACTION = new Integer(1006);
	public static final Integer EXCEPTION_INVALID_CREATE_ACTION = new Integer(1007);
	public static final Integer EXCEPTION_INVALID_PAYMENT_TYPE = new Integer(10101);
	public static final Integer EXCEPTION_INVALID_TRANS_AMOUNT = new Integer(10102);
	public static final Integer EXCEPTION_INVALID_CONFIGURATION = new Integer(50001);
	public static final Integer EXCEPTION_CONNECTION_ERROR = new Integer(50002);
	public static final Integer EXCEPTION_AUTHENTICATION_ERROR = new Integer(50003);
	 */
	//Account Actions
	public static final Integer ACCOUNT_ITEM_ACTION_ID_CREATE = new Integer(1);
	public static final Integer ACCOUNT_ITEM_ACTION_ID_FIND = new Integer(2);
	public static final Integer ACCOUNT_ITEM_ACTION_ID_UPDATE = new Integer(3);
	public static final Integer ACCOUNT_ITEM_ACTION_ID_FIND_ALL = new Integer(4);
	public static final Integer ACCOUNT_ITEM_ACTION_ID_FIND_WITH_BALANCE_SUMMARY = new Integer(5);

	//WP Job Actions
	public static final Integer WORKPOINT_GET_JOB_IMAGE = new Integer(1);
	public static final Integer WORKPOINT_CREATE_JOB = new Integer(2);

	//Cycle30.C30ORDER
	public static final Integer C30ORDER_CREATE = new Integer(1);
	public static final Integer C30ORDER_CANCEL = new Integer(2);

	//Cycle30.C30BULKORDERJOB
	public static final Integer C30BULKORDERJOB_CREATE = new Integer(1);
	public static final Integer C30BULKORDERJOB_CANCEL = new Integer(2);

	//Address
	public static final Integer ASSOC_STATUS_PENDING = new Integer(1);
	public static final Integer ASSOC_STATUS_CURRENT = new Integer(2);
	public static final Integer ASSOC_STATUS_OLD = new Integer(3);
	public static final Integer ASSOC_STATUS_CANCELLED = new Integer(4);
	public static final Integer ADDR_CATEGORY_SERVICE = new Integer(1);
	public static final Integer ADDR_CATEGORY_B_END = new Integer(2);
	public static final Integer ADDR_CATEGORY_SHIPPING = new Integer(3);
	public static final Integer ADDR_LOCATION_MASTER = new Integer(1);
	public static final Integer ADDR_LOCATION_LOCAL = new Integer(2);
	public static final Integer ADDR_TYPE_FREE_FORM = new Integer(2);
	public static final Integer ADDR_TYPE_NON_FREE_FORM = new Integer(1);
	public static final Integer ADDR_TYPE_TEMP_ADDRESS = new Integer(3);

	//Components
	public static final Integer COMPONENT_INACTIVE_STATUS = new Integer(0);
	public static final Integer COMP_ELEMENT_PRODUCT_ASSOC_TYPE = new Integer(1);
	public static final Integer COMP_ELEMENT_CONTRACT_ASSOC_TYPE = new Integer(2);
	public static final Integer COMPONENT_STATUS_ACTIVE = new Integer(1);

	//Contracts        
	public static final Integer CONTRACT_SERVICE_LEVEL = new Integer(3);
	public static final Integer CONTRACT_ACCOUNT_LEVEL = new Integer(1);
	public static final Integer CONTRACT_TYPE_DURATION_UNITS_DAYS = new Integer(140);
	public static final Integer CONTRACT_TYPE_DURATION_UNITS_WEEKS = new Integer(150);
	public static final Integer CONTRACT_TYPE_DURATION_UNITS_MONTHS = new Integer(160);
	public static final Integer CONTRACT_TYPE_DURATION_UNITS_QUARTERS = new Integer(170);
	public static final Integer CONTRACT_TYPE_DURATION_UNITS_YEARS = new Integer(180);
	public static final Integer CONTRACT_TYPE_DURATION_UNITS_BILL_CYCLE = new Integer(-3);
	public static final Integer CONTRACT_TYPE_DURATION_UNITS_UNSPECIFIED = new Integer(-1);
	public static final Integer CONTRACT_TYPE_STANDARD_PROMOTION = new Integer(6);
	public static final Integer CONTRACT_TYPE_COMMITMENT = new Integer(7);
	public static final Integer CONTRACT_TYPE_FREE_USAGE = new Integer(8);
	public static final Integer CONTRACT_TYPE_HQ_CONTRACT = new Integer(9);
	public static final Integer CONTRACT_TYPE_HIERARCHY_BRANCH = new Integer(10);
	public static final Integer CONTRACT_TYPE_GLOBAL = new Integer(11);
	public static final Integer CONTRACT_TYPE_HISTORIC_DISCOUNT = new Integer(12);
	public static final Integer CONTRACT_TYPE_ROLLOVER = new Integer(13);
	public static final Integer CONTRACT_TYPE_HISTORIC_PAYBACK = new Integer(14);
	public static final Integer CONTRACT_TYPE_PAYG_UNIT_CREDIT = new Integer(15);
	public static final Integer CONTRACT_TYPE_PAYG_ROLLOVER_UC = new Integer(16);
	public static final Integer ASSOC_TYPE_NONE = new Integer(0);
	public static final Integer ASSOC_TYPE_RC = new Integer(4);
	public static final Integer ASSOC_TYPE_COM_REF = new Integer(5);
	public static final Integer ASSOC_TYPE_INV = new Integer(6);

	//Order
	public static final Integer ORD_STATUS_INITIATED =   new Integer(0);
	public static final Integer ORD_STATUS_IN_PROGRESS = new Integer(10);
	public static final Integer ORD_STATUS_COMMITTED =   new Integer(20);
	public static final Integer ORD_STATUS_COMPLETED =   new Integer(80);
	public static final Integer ORD_STATUS_CANCELLED =   new Integer(90);
	public static final Integer ORD_STATUS_IN_ERROR =    new Integer(99);
	public static final Integer ORD_STATUS_IS_CANCELLABLE =    new Integer(1);
	public static final Integer ORD_STATUS_IS_RESUBMITTABLE =    new Integer(1);	
	public static final HashMap<String, String> orderStatusMap = new HashMap<String, String>();
	static {
		orderStatusMap.put("0",  "Order initiated");
		orderStatusMap.put("10", "Order in progress");
		orderStatusMap.put("20", "Order committed");
		orderStatusMap.put("80", "Order completed");
		orderStatusMap.put("90", "Order cancelled");
		orderStatusMap.put("99", "Order failed");
	}

	public static final Integer ORD_INSERT_SINGLE = new Integer(0);
	public static final Integer ORD_INSERT_MULTI = new Integer(1);  


	// provisioning
	public static final String SVC_PROVISION_SUCCESS = "80";
	public static final String SVC_PROVISION_FAILURE = "99";

	//Order
	public static final Integer ORD_LOCKED = new Integer(1);
	public static final Integer ORD_NOT_LOCKED = new Integer(0);

	/** Order Item member types. **/
	public static final Integer ORD_ITEM_PRODUCT_MEMBER_TYPE = new Integer(10);
	public static final Integer ORD_ITEM_NRC_MEMBER_TYPE = new Integer(20);
	public static final Integer ORD_ITEM_CONTRACT_MEMBER_TYPE = new Integer(30);
	public static final Integer ORD_ITEM_COMPONENT_MEMBER_TYPE = new Integer(40);
	public static final Integer ORD_ITEM_PACKAGE_MEMBER_TYPE = new Integer(50);
	public static final Integer ORD_ITEM_SERVICE_EXT_ID_MEMBER_TYPE = new Integer(60);
	public static final Integer ORD_ITEM_INVENTORY_MEMBER_TYPE = new Integer(70);
	public static final Integer ORD_ITEM_ADDRESS_ASSOC_MEMBER_TYPE = new Integer(78);
	public static final Integer ORD_ITEM_SERVICE_MEMBER_TYPE = new Integer(80);

	/** LWO special member types. **/
	public static final Integer ORD_ITEM_CONTAINER_MEMBER_TYPE = new Integer(-100);
	public static final Integer ORD_ITEM_PRODUCT_OVERRIDE_MEMBER_TYPE = new Integer(-10);
	public static final Integer ORD_ITEM_SERVICE_ORDER_NOTE_MEMBER_TYPE = new Integer(-20);

	/** Integer Values for Item Action Ids. **/
	public static final Integer ORD_ITEM_CONNECT_ITEM_ACTION_ID = new Integer(10);
	public static final Integer ORD_ITEM_CHANGE_ITEM_ACTION_ID = new Integer(20);
	public static final Integer ORD_ITEM_DISCONNECT_ITEM_ACTION_ID = new Integer(30);
	public static final Integer ORD_ITEM_FIND_ITEM_ACTION_ID = new Integer(-10);

	/** Package Value Constants. */
	public static final Integer PRODUCT_PKG_INACTIVE_STATUS = new Integer(0);
	public static final Integer PRODUCT_PKG_ACTIVE_STATUS = new Integer(1);
	public static final Integer PRODUCT_PKG_DISCONNECTED_STATUS = new Integer(2);

	//ServiceOrder
	/** Status indicating that the service order is in progress. */
	public static final Integer SERV_ORD_STATUS_IN_PROGRESS = new Integer(10);

	/** Level independent Service Order Type value strings. */
	public static final Integer SERV_ORD_TYPE_CONNECT = new Integer(10);
	/** Level independent Service Order Type value strings. */
	public static final Integer SERV_ORD_TYPE_CHANGE = new Integer(20);

	/** Service Order Level 1 = Account. */
	public static final Integer SERV_ORD_ACCOUNT_LEVEL = new Integer(1);
	/** Service Order Level 2 = Service. */
	public static final Integer SERV_ORD_SERVICE_LEVEL = new Integer(2);

	/** Kenan service move in service order type value.  */
	public static final Integer SERV_ORD_TYPE_SERVICE_MOVE_OUT = new Integer(54);
	/** Kenan service move out service order type value.  */
	public static final Integer SERV_ORD_TYPE_SERVICE_MOVE_IN = new Integer(56);
	/** Kenan service disconnect service order type value.  */
	public static final Integer SERV_ORD_TYPE_SERVICE_DISCONNECT = new Integer(50);
	/** Kenan service resume service order type value.  */
	public static final Integer SERV_ORD_TYPE_RESUME = new Integer(70);
	/** Kenan service suspend service order type value.  */
	public static final Integer SERV_ORD_TYPE_SUSPEND = new Integer(80);
	/** Kenan service transfer disconnect service order type value.  */
	public static final Integer SERV_ORD_TYPE_TRANSFER_DISCONNECT = new Integer(100);

	/** Kenan service instance active status. */
	public static final Integer SERVICE_STATUS_ACTIVE = new Integer(1);
	/** Kenan service instance disconnected status. */
	public static final Integer SERVICE_STATUS_DISCONNECTED = new Integer(2);
	/** Kenan service instance suspended status. */
	public static final Integer SERVICE_STATUS_SUSPENDED = new Integer(3);
	/** Kenan service instance transferred out status. */
	public static final Integer SERVICE_STATUS_TRANSFERRED_OUT = new Integer(4);
	/** Kenan service instance transferred in status. */
	public static final Integer SERVICE_STATUS_TRANSFERRED_IN = new Integer(5);


	//Proc Names 
	public static final String GET_NOTE_OBJECTS_SP_CALL = "c30_sdk_get_acct_notes";

	public static final Integer EXCEPTION_NO_USAGE = new Integer(10001);

	public static final Integer EXCEPTION_MULTIPLE_BILLED_USAGE = new Integer(10002);

	public static final Integer EXCEPTION_MULTIPLE_UNBILLED_USAGE = new Integer(10003);

	public static final Integer EXCEPTION_MULTIPLE_ADJUSTMENT_TYPES = new Integer(10004);

	public static final Integer EXCEPTION_AMOUNT_EXCEEDS_CHARGE = new Integer(10005);

	public static final Integer EXCEPTION_MIU_STORED_PROC_ERROR = new Integer(10006);

	/**
	 * Variable indicating that the middleware connection for the factory is BORROWED
	 * from whatever container the factory is running in.
	 */
	public final static Integer CONNECTION_MODE_BORROW = new Integer(1);
	/**
	 * Variable indicating that the middleware connection for the factory is CREATED
	 * by the factory itself.
	 */
	public static final Integer CONNECTION_MODE_NEW = new Integer(2);

	public static final int SQL_ERROR_CODE_LENGTH = 12;

}
