package com.cycle30.sdk.object.cycle30.invoice;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * C30SDK core Invoice object 
 */
public class C30Invoice extends C30SDKObject implements Cloneable {

        private static Logger log = Logger.getLogger(C30Invoice.class);

        /**
         * Creates a new instance of C30Invoice.
         * 
         * @param factory the factory which is the parent of this object.
         * @param objectType Type of C30SDK payment
         * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
         * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
         * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
         * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
         * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
         */
        public C30Invoice(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
                super(factory, objectType);
        }



        /**
         * Initializes the base attributes 
         * 
         * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
         */
        @Override
        protected void initializeAttributes() throws C30SDKInvalidAttributeException{
            setClassFieldConfiguration();
        }

        
        
        /**
         * Creates a copy of the C30Invoice object.
         * 
         * @return A copy of the C30Invoice object
         */
        @Override
        public Object clone() {
                C30Invoice copy = (C30Invoice)super.clone();        
                return copy;        
        }

        
        /**
         * Method to process the C30Invoice.
         * 
         * @return C30SDKObject 
         * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
         */
        @Override
        public C30SDKObject process() {
            
            C30SDKObject c30Object = null;
            
            try {
               setAttributesFromNameValuePairs();

               String clientId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               Integer acctSegId = this.getAcctSegId();
               Integer size    = (Integer)getAttributeValue("Size",  C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               Integer start   = (Integer)getAttributeValue("Start", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               String  transId = (String)getAttributeValue("TransactionId", C30SDKValueConstants.ATTRIB_TYPE_INPUT);

               getInvoices(clientId, acctSegId, size, start, transId);
               setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               
               // Set return object reference to this instance
               c30Object = this;

            } catch (C30SDKException e) {
                c30Object = this.createExceptionMessage(e);
            }

            return c30Object;
        }

        

        /**
         * Internal method to get the set of invoices.  
         * 
         * @param acctSegId 
         * @param clientId 
         * @throws C30SDKException 
         * @throws Exception 
         */
        private void getInvoices(String clientId, 
                                   Integer acctSegId, 
                                   Integer returnSize, 
                                   Integer returnStart,
                                   String  transId) throws C30SDKException {

            try {
                
                C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId()); 
                C30ExternalCallDef call = new C30ExternalCallDef("c30getInvoice");

                // Input params
                call.addParam(new C30CustomParamDef("v_transaction_id", 2, 2000, 1));
                call.addParam(new C30CustomParamDef("v_acct_seg_id",    1, 2000, 1));
                call.addParam(new C30CustomParamDef("v_client_id",      2, 2000, 1));


                // Output params (mapped to output names in stored proc).
                call.addParam(new C30CustomParamDef("invoice_number",   1, 2000, 2));
                call.addParam(new C30CustomParamDef("statement_date",   2, 2000, 2));
                call.addParam(new C30CustomParamDef("new_charges",      2, 2000, 2));
                call.addParam(new C30CustomParamDef("balance_due",      2, 2000, 2));
                call.addParam(new C30CustomParamDef("payment_due_date", 2, 2000, 2));
                call.addParam(new C30CustomParamDef("closed_date",      2, 2000, 2));
                call.addParam(new C30CustomParamDef("dispute_amount",   2, 2000, 2));

                // Make db call using required params.
                ArrayList<Object> paramValues = new ArrayList<Object>();
                paramValues.add(transId);
                paramValues.add(acctSegId);
                paramValues.add(clientId);

                
                
                dataSource.queryData(call, paramValues, 2);
                
                int rowCount = dataSource.getRowCount();
                log.debug("Total row count = " + rowCount);
                
                int start = returnStart.intValue()-1;  // row #'s start at zero
                int size  = returnSize.intValue();
                
                // if no records found, throw 'not found' exception
                if (rowCount == 0) {
                    throw new Exception("C30INVCNOTFND No invoice data found");
                }
                
                // Populate 'this' object with first row, then add all others to the
                // 'c30sdkObjects' ArrayList via cloning this instance.
                if (rowCount > 0) {    
                    
                    int currCount = 0;
                    int startIdx = 0;
                    int endIdx   = rowCount;
                    
                    // Set paging params
                    if (rowCount >= start) {
                        startIdx = start;
                    }
                    if ( (startIdx + size) < rowCount) {
                        endIdx = (startIdx + size);
                    }
                    
                    for (int i=startIdx; i<endIdx; i++) {
                    
                        C30Invoice sdkObj = this;
                        
                        if (currCount != 0) {
                            sdkObj = (C30Invoice)super.clone();
                        }
                        
                        sdkObj.setAttributeValue("InvoiceNumber",  dataSource.getValueAt(i, 0), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("StatementDate",  dataSource.getValueAt(i, 1), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("NewCharges",     dataSource.getValueAt(i, 2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("BalanceDue",     dataSource.getValueAt(i, 3), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("PaymentDueDate", dataSource.getValueAt(i, 4), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("ClosedDate",     dataSource.getValueAt(i, 5), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("DisputeAmount",  dataSource.getValueAt(i, 6), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("ClientId",       clientId,                    C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("TotalCount",     rowCount,                    C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

                        // Add all rows after 1st row to collection of objects.  This is so toHashMap() can
                        // unmarshall the list of objects.
                        if (currCount > 0) {
                            addC30SDKObject(sdkObj);
                        }
                        currCount++;
                        
                    }
                }
            }
            catch(Exception e) {
                String errMsg = e.getMessage();
                String errCode = errMsg.substring(0, C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
                errMsg = errMsg.substring(C30SDKValueConstants.SQL_ERROR_CODE_LENGTH+1);
                log.error(errMsg);
                throw new C30SDKException(errMsg, e, errCode);
            }
                  
        }


        /**
         * Method which gets any data to be cached for further use by the object.
         */
        @Override
        protected void loadCache() {

        }

}
