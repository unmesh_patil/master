package com.cycle30.sdk.object.cycle30.service;


import java.math.BigDecimal;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


import oracle.sql.ArrayDescriptor;
import oracle.sql.StructDescriptor;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKValueConstants;

import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.ToolkitUtils;

/**
 * C30SDK core Service object 
 */
public class C30Service extends C30SDKObject implements Cloneable {

        private static Logger log = Logger.getLogger(C30Service.class);

        /**
         * Creates a new instance of C30Service.
         * 
         * @param factory the factory which is the parent of this object.
         * @param objectType Type of C30SDK payment
         * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
         * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
         * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
         * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
         * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
         */
        public C30Service(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
                super(factory, objectType);
        }



        /**
         * Initializes the base attributes 
         * 
         * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
         */
        @Override
        protected void initializeAttributes() throws C30SDKInvalidAttributeException{
            setClassFieldConfiguration();
        }

        
        
        /**
         * Creates a copy of the C30Service object.
         * 
         * @return A copy of the C30Service object
         */
        @Override
        public Object clone() {
            C30Service copy = (C30Service)super.clone();        
                return copy;        
        }

        
        /**
         * Method to process the C30Service.
         * 
         * @return C30SDKObject 
         * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
         */
        @Override
        public C30SDKObject process() {
            
            C30SDKObject c30Object = null;
            
            try {
               setAttributesFromNameValuePairs();

               Integer acctSegId = this.getAcctSegId();

               String  transId   = (String)getAttributeValue("TransactionId",     C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               String  serviceId = (String)getAttributeValue("ServiceExternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               
               getServiceData(acctSegId, transId, serviceId);
               
               setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               
               // Set return object reference to this instance
               c30Object = this;

            } catch (C30SDKException e) {
                c30Object = this.createExceptionMessage(e);
            }

            return c30Object;
        }



        /**
         * Internal method to get the set service data.  
         * 
         * @param acctSegId 
         * @param clientId 
         * @param serviceId
         * @throws C30SDKException 
         * @throws Exception 
         */
        private void getServiceData(Integer acctSegId, 
                                      String  transId,
                                      String  serviceId) throws C30SDKException {
                      
            ResultSet rs = null;
            C30JDBCDataSource dataSource = null;
            try {
                
                dataSource = this.getFactory().getJDBCDataSource(getAccountServerId()); 
                C30ExternalCallDef call = new C30ExternalCallDef("c30getServiceInfo");

                // Input params
                call.addParam(new C30CustomParamDef("v_transaction_id", 2, 2000, 1));
                call.addParam(new C30CustomParamDef("v_acct_seg_id",    1, 2000, 1));
                call.addParam(new C30CustomParamDef("v_service_id",     2, 2000, 1));


                // Output params (mapped to output names in stored proc).
                call.addParam(new C30CustomParamDef("v_service_id",       2, 2000, 2));
                call.addParam(new C30CustomParamDef("d_subscr_no",        1, 2000, 2));
                call.addParam(new C30CustomParamDef("d_subscr_no_resets", 1, 2000, 2));
                call.addParam(new C30CustomParamDef("d_status_id",        2, 2000, 2));
                call.addParam(new C30CustomParamDef("d_status_descr",     2, 2000, 2));
                call.addParam(new C30CustomParamDef("d_active_dt",        3, 2000, 2));
                call.addParam(new C30CustomParamDef("d_inactive_dt",      3, 2000, 2));
                call.addParam(new C30CustomParamDef("d_host_service_id",  2, 2000, 2));
                call.addParam(new C30CustomParamDef("d_is_host",          1, 2000, 2));
                call.addParam(new C30CustomParamDef("d_partner_count",    1, 2000, 2));
                call.addParam(new C30CustomParamDef("d_identifier_col",  11, 2000, 2));  // STRUCT type
                call.addParam(new C30CustomParamDef("d_plan_col",        11, 2000, 2));  // STRUCT type
                call.addParam(new C30CustomParamDef("d_comp_col",        11, 2000, 2));  // STRUCT type
                
                // Make db call using required params, and get a result set to work with
                ArrayList<Object> paramValues = new ArrayList<Object>();
                paramValues.add(transId);
                paramValues.add(acctSegId);
                paramValues.add(serviceId);
                
                rs = dataSource.queryDataWithRS(call, paramValues, 2);

                C30Service sdkObj = this;

                rs.next();
                String serviceExtId       =  rs.getString(1);
                BigDecimal subscrNo       =  rs.getBigDecimal(2);
                BigDecimal subscrNoResets =  rs.getBigDecimal(3);
                BigDecimal statusId       =  rs.getBigDecimal(4);
                String statusDescr        =  rs.getString(5);
                Timestamp activeDate      =  rs.getTimestamp(6);
                Timestamp inactiveDate    =  rs.getTimestamp(7);
                String hostServiceId      =  rs.getString(8);
                BigDecimal isHost         =  rs.getBigDecimal(9);
                BigDecimal partnerCount   =  rs.getBigDecimal(10);
                
                // The result set contains C30 custom Oracle data types for identifiers, 
                // plans and components which need to be broken down into lists of 
                // Java equivalent values.
                Array identifierCollection = rs.getArray(11);
                Array planCollection       = rs.getArray(12);
                Array componentCollection  = rs.getArray(13);
                
                // Get raw Object array of the actual data values. 
                Object identifiers[] = (Object[])identifierCollection.getArray();
                Object plans[]       = (Object[])planCollection.getArray();
                Object components[]  = (Object[])componentCollection.getArray();

                
                // Create list of each type for attaching as object attribute
                ArrayList identifierList = (ArrayList)IdentifierInstance.createList(identifiers);
                ArrayList planList       = (ArrayList)CatalogInstance.createList(plans);
                ArrayList compList       = (ArrayList)CatalogInstance.createList(components);
                
                
                // Do some data cleanup before setting as attributes.
                String activeDateStr = "";
                String inactiveDateStr = "";
                if (activeDate != null) {
                    activeDateStr = new SimpleDateFormat("yyyyMMdd").format(activeDate);
                }
                if (inactiveDate != null) {
                    inactiveDateStr = new SimpleDateFormat("yyyyMMdd").format(inactiveDate);
                }

                if (hostServiceId == null || hostServiceId.equalsIgnoreCase("null")) {
                    hostServiceId = "";
                }
                
                // Set output attributes.
                sdkObj.setAttributeValue("ServiceId",            serviceExtId,   C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("SubscrNo",             subscrNo,       C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("SubscrNoResets",       subscrNoResets, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("StatusId",             statusId,       C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("StatusDescr",          statusDescr,    C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("ActiveDate",           activeDateStr,  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("InactiveDate",         inactiveDateStr,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("HostServiceId",        hostServiceId,  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("IdentifierCollection", identifierList, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("PlanCollection",       planList,       C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                sdkObj.setAttributeValue("ComponentCollection",  compList,       C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                
                
            } catch(Exception e) {
                String errMsg = e.getMessage();
                String errCode = errMsg.substring(0, C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
                errMsg = errMsg.substring(C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
                log.error(errMsg);
                throw new C30SDKException(errMsg, e, errCode);
            }

            finally {
                if (rs != null) {
                    try {
                        rs.close();
                        dataSource.closeConnection();
                    } catch (Exception e) {
                        throw new C30SDKException(e.getMessage(), "Could not close result set when doing service inquiry!");
                    }
                }
            }
                  
        }


        /**
         * Method which gets any data to be cached for further use by the object.
         */
        @Override
        protected void loadCache() {

        }

        

}
