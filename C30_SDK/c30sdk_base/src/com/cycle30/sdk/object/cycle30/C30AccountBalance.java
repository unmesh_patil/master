package com.cycle30.sdk.object.cycle30;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * C30SDK AccountBalance 
 * @author jwood
 */
public class C30AccountBalance extends C30SDKObject implements Cloneable {

	private C30SDKObject account = null;
	protected Map balance = null;
	private static Logger log = Logger.getLogger(C30AccountBalance.class);

	/**
	 * Creates a new instance of C30Payment.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30AccountBalance(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30AccountBalance(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30AccountBalance.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	@Override
	public Object clone() {
		C30AccountBalance copy = (C30AccountBalance) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30AccountBalance.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
		try {
			setAttributesFromNameValuePairs();
			//	validateAttributes();

			String clientId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			Integer acctSegId = this.getAcctSegId();
			String transId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);


			getAccountBalance(clientId, acctSegId, transId);
			setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			lwo = this;

		} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}


	/**
	 * Method to ensure all attributes that are visible to the calling application are populated.  This 
	 * method is usually called just before the object returns objects using the process() method.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */
	private void populateBalanceOutput() throws C30SDKException{
		for  ( Iterator it=this.balance.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
			Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
			String key = (String)entry.getKey (  ) ; 
			Object value = this.balance.get(key) ;
			if (value != null)
			{
				if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
				{
					setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

					log.debug(  key+" " +  value +"   SET") ;                            
				}
				else
					if (value != null){
						log.debug(  key+" " +  value ) ; 
					}

			}
		}

	}

	/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 




	}



	/**
	 * Internal method to get the outstanding account balance.  This method is 
	 * used when the account is to have their entire balance cleared.
	 * @param acctSegId 
	 * @param clientId 
	 * @return the account balance
	 * @throws C30SDKException 
	 * @throws Exception 
	 */
	private void getAccountBalance(String clientId, Integer acctSegId, String transId) throws C30SDKException {

		//getWriteOffBalance();


		try{
			//Write code to get the accountBalance using stored Procedure call.
			C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId()); 
			C30ExternalCallDef call = new C30ExternalCallDef("c30getAccountBalance");

			// Input params
			call.addParam(new C30CustomParamDef("v_transaction_id", 2, 2000, 1));
			call.addParam(new C30CustomParamDef("v_acct_seg_id", 1, 2000, 1));
			call.addParam(new C30CustomParamDef("v_client_id", 2, 2000, 1));

			// Output purams
			call.addParam(new C30CustomParamDef("balance_current", 1, 2000, 2));
			call.addParam(new C30CustomParamDef("balance_30", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("balance_60", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("balance_90", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("balance_120", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("balance_greater_120", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("total_balance", 2, 2000, 2));

			ArrayList paramValues = new ArrayList();

			paramValues.add(transId);
			paramValues.add(acctSegId);
			paramValues.add(clientId);


			dataSource.queryData(call, paramValues, 2);
			log.debug("Total row count = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0)
			{          
				//Populating output attributes.
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_BAL_CURRENT,dataSource.getValueAt(0, 0),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_BAL_BALANCE30,dataSource.getValueAt(0, 1),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_BAL_BALANCE60,dataSource.getValueAt(0, 2),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_BAL_BALANCE90,dataSource.getValueAt(0, 3),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_BAL_BALANCE120,dataSource.getValueAt(0, 4),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_BAL_BALANCE_GT_120,dataSource.getValueAt(0, 5),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_BAL_TOTAL,dataSource.getValueAt(0, 6),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID ,clientId,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}
		}
		catch(Exception e)
		{
			String errMsg = e.getMessage();
			String errCode = errMsg.substring(0, C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
			errMsg = errMsg.substring(C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		}

	}



	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}