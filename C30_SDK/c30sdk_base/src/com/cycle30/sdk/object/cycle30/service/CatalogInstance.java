package com.cycle30.sdk.object.cycle30.service;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Class that presents a single Identifier instance that maps back
 * to custom Oracle data type in C30ARBOR database called C30CATALOGCOL
 *
 */
public class CatalogInstance {

    private String catalogId;
    private String externalInstId;
    private String componentId;
    private String description;
    
    
    
    /**
     * @return the catalogId
     */
    public String getCatalogId() {
        return catalogId;
    }
    /**
     * @param catalogId the catalogId to set
     */
    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }
    /**
     * @return the externalInstId
     */
    public String getExternalInstId() {
        return externalInstId;
    }
    /**
     * @param externalInstId the externalInstId to set
     */
    public void setExternalInstId(String externalInstId) {
        this.externalInstId = externalInstId;
    }
    /**
     * @return the componentId
     */
    public String getComponentId() {
        return componentId;
    }
    /**
     * @param componentId the componentId to set
     */
    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
    //-------------------------------------------------------------------------
    /**
     * Create a list of CatalogInstance instances from raw Object list retrieved
     * from the database.
     * 
     * @param objects identifiers
     * @return List of catalog instances
     */
    public static List createList(Object[] objects) throws SQLException {
            
       ArrayList objectList = new ArrayList();
       
       for (int i=0; i<objects.length; i++) {
           
           try {
               
               // Convert object to underlying Oracle data STRUCT type
               oracle.sql.STRUCT struct = (oracle.sql.STRUCT)objects[i];
               
               Object atts[] = struct.getAttributes();
               CatalogInstance inst = new CatalogInstance();
               inst.setCatalogId((String)atts[0]);
               inst.setExternalInstId((String)atts[1]);
               
               BigDecimal bd = (BigDecimal)atts[2];
               String s = bd.toString();
               inst.setComponentId(s);
               
               inst.setDescription((String)atts[3]); 
               
               objectList.add(inst);
               
           }
           catch (SQLException se) {
               throw se;
           }
           
       }
       
       
       return objectList;
       
    }
}
