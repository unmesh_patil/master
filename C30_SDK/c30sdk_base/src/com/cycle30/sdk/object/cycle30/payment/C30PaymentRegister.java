package com.cycle30.sdk.object.cycle30.payment;

import org.apache.log4j.Logger;

import com.cycle30.connector.aq.QueueManager;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

public class C30PaymentRegister extends C30SDKObject implements Cloneable {

	
	
	private static Logger log = Logger.getLogger(C30PaymentRegister.class);

	/**
	 * Creates a new instance of C30PrepaidPayment.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PaymentRegister(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PaymentRegister(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30PaymentRegister.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	@Override
	public Object clone() {
		C30PaymentRegister copy = (C30PaymentRegister) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30PrepaidPaymentRegister
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
		try {
			setAttributesFromNameValuePairs();
			//	validateAttributes();
 
			String  orgId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			Integer acctSegId = this.getAcctSegId();
			String  transId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
			String paymentAction= (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_PAYEMNT_ACTION, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					
  

             if(C30SDKValueConstants.PREPAID_PAYMENT_REGISTER_ACTION_TYPE.equalsIgnoreCase(paymentAction) ){
            	 registerPrepaidPaymentEntry(acctSegId.toString());
			     setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
    
             lwo = this;
             
		} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
		} catch (ConnectorException e) {
			lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}

   
	private  void registerPrepaidPaymentEntry(String acctSegId) throws C30SDKException, ConnectorException {

		try{
			
	        QueueManager queueManager =  QueueManager.getInstance();
        
	      //  Create message payload for sending to Non Order Status Queue
	        String payload =createPaymentRegisterPayload(acctSegId);
	        queueManager.createNonOrderSenderQueueMessage(payload);
	        log.info("Non Order Payment Register Payload : "+payload);
	        String forceResponse="SUCESS";
	        this.setAttributeValue(C30SDKValueConstants.ATTRIB_PREPAID_PAYMENT_XML, forceResponse , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		}
		catch(C30SDKException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
		
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		} 
		  
	}
	

		/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 
	}

	
	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

	
	private String createPaymentRegisterPayload(String acctSegId) throws  C30SDKInvalidAttributeException {

	        StringBuffer payload = new StringBuffer();
	        
	        payload.append(C30SDKValueConstants.PREPAID_PAYMENT_CALL_BACK_TYPE);
	        payload.append("--");  
	        payload.append(acctSegId);
	        payload.append("--");
	       // payload.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>" + 
	        payload.append("<CreatePaymentLogRequest>" +
	                       "<TotalCount>1</TotalCount>"+
	                       "<Payments>" +  
	        					"<Payment>");  
	        		payload.append("<Amount>"+(String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_PAYEMNT_AMOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)     + "</Amount>" );
	        		payload.append("<Status>Processed</Status>");
	        		payload.append("<PaymentType>"+getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_PAYEMNT_TENDER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)     + "</PaymentType>" );
	        		payload.append("<TransactionDate>"+getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_PAYEMNT_TRANSACTION_DATE_TIME, C30SDKValueConstants.ATTRIB_TYPE_INPUT)     + "</TransactionDate>" );
	        		payload.append("<PaymentSource>"+getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_PAYEMNT_CHANNEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT)     + "</PaymentSource>" );
	        		payload.append("<Account>"+getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_PAYEMNT_ACCOUNT_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT)     + "</Account>" );
	        		payload.append("<GatewayTransactionID>"+getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_PAYEMNT_EXTCORRELATIONID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)     + "</GatewayTransactionID>" );
	        		payload.append("<PaymentOption>Payment Gateway</PaymentOption>");
	        		payload.append("<PaymentComments>"+ getAttributeValue(C30SDKAttributeConstants.ATTRIB_OCS_PAYEMNT_NOTE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)    + "</PaymentComments>" );
	        		payload.append("</Payment>" );
	        		payload.append("</Payments>" );
	        payload.append("</CreatePaymentLogRequest>");
	         
	        return payload.toString();
	 }
	

}