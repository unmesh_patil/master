package com.cycle30.sdk.object.cycle30.service;


/**
 * 
 * Class for encapsulating custom Oracle C30NAMEVALUE data type
 *
 */
public class ExtendedDataInstance {
    
    String name;
    String value;
    
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    
    

}
