package com.cycle30.sdk.object.cycle30.order;

import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.datasource.C30WPConnection;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKSqlConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.schema.jaxb.C30OrderActionType;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30DOMUtils;
import com.workpoint.client.Job;
import com.workpoint.server.ejb.WorkPointEJBException;

/**
 * This method used for checking and processing open Orders.
 * @author rnelluri
 *
 */
public class C30SDKOrderManager  implements Runnable {

	private static Logger log = Logger.getLogger(C30SDKOrderManager.class);
	public static C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();

	// The one-and-only instance of the C30SDKOrderManager.
	private static C30SDKOrderManager instance = null;
	private static final String SLEEP_TIME   = "c30sdkorder-manager.sleep-time";
	private static final String START_OR_NOT = "c30sdkorder-manager.start";
	C30JDBCDataSource ordeMgrSdkdataSource = null;

	// The number of milliseconds to sleep between cache sweeps
	protected long sleepTime = 100000; // default in case property can't be found.

	// Flag indicating that the run-thread for the manager should stop. True
	// indicates that the run thread should die
	protected boolean die = false;

	//Flag indicating that the run-thread for the manager should start. True
	//indicates that the run thread should start
	protected boolean start = true;

	public static TreeSet<String> orderQueue = new TreeSet<String>();


	/**
	 * Returns the one-and-only instance of the C30SDKOrderManager.
	 *
	 *@return    The one-and-only instance of the C30SDKOrderManager.
	 */
	public static C30SDKOrderManager getInstance() {
		if (instance == null) {
			if( log != null ) { 
				log.info("Creating new instance of C30SDKOrderManager"); 
			}
			instance = new C30SDKOrderManager();
		}
		return instance;
	}




	/**
	 * default constructor
	 */
	protected C30SDKOrderManager() {

		try {
			C30SDKPropertyFileManager rb = C30SDKPropertyFileManager.getInstance();
			rb.addFromClassPath(C30SDKValueConstants.ORDER_PROPERTIES_FILE_NAME);
			sleepTime = rb.getLong(SLEEP_TIME);
			start = rb.getBoolean(START_OR_NOT);
			ordeMgrSdkdataSource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();

		} catch (java.util.MissingResourceException ex) {
			log.debug("Order Manager Settings were not found; using defaults"); 
		} catch(Exception e) {
			log.error(e);
		}

		//Start the thread.
		Thread t = new Thread(this);
		t.setPriority(Thread.MIN_PRIORITY);
		if (start) t.start();

	}



	//@Override
	public void run() {

		//Check the orders that are in Initiated and then start Processing them.
		while (!die) {

			try {
				Thread.sleep(sleepTime);
				log.debug("Find and Processing the New Orders......"); 

				//Code to get the Orders and populated in the orderQueue
				C30OrderSqlUtils.getOpenOrders(ordeMgrSdkdataSource);

				//Get the first openOrder and start processing
				while(orderQueue.size() != 0) {
					processOrder(orderQueue.first());
					orderQueue.remove(orderQueue.first()); //Remove  first Element from orderQueue
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				log.error(ex);
			}

		}  // end while()

	}


	/**
	 * This is responsible for Processing the Order.
	 * @param orderKey concatatenation of orderId + ":" + transactionId
	 * @throws C30SDKInvalidAttributeException 
	 * @throws C30SDKObjectConnectionException 
	 * @throws SQLException 
	 * @throws C30SDKToolkitException 
	 */
	private void processOrder(String orderKey) throws C30SDKException, C30SDKInvalidAttributeException, SQLException, C30SDKToolkitException {

		//Change the status of the order to InProgress and Locked.
		int idx = orderKey.indexOf(":");
		String orderId = orderKey.substring(0, idx);		
		String transId = orderKey.substring(idx+1);
		
		log.debug("Checking the Dependency among the orders for the Order - " +orderKey);
		if (checkOrderDependency(orderId, transId) ) return;
		log.debug("No Dependent Orders Found. So, Processing the Order with Order Id =" + orderKey);
		
		C30OrderSqlUtils.changeOrderStatus(orderId, transId, C30SDKValueConstants.ORD_STATUS_IN_PROGRESS, ordeMgrSdkdataSource );

		// Start the jobs for each subOrder in the given order.
		String query = C30SDKSqlConstants.GET_SUB_ORDER_INFORMATION;
		ResultSet resultSet =null;

		try  {

			PreparedStatement ps = ordeMgrSdkdataSource.getPreparedStatement(query);

			ps.setString(1, orderId);
			ps.setString(2, transId);
			resultSet  = ps.executeQuery();

			while(resultSet.next()) {

				String wpProcessId     = resultSet.getString("WP_PROCESS_ID");
				String subOrderid      = resultSet.getString("CLIENT_SUB_ORDER_ID");
				String acctSegId       = resultSet.getString("ACCT_SEG_ID");
				String clientOrgName   = resultSet.getString("CLIENT_ORG_NAME");
				String clientOrgId     = resultSet.getString("CLIENT_ORG_ID");
				String transactionId   = resultSet.getString("TRANSACTION_ID");
				String internalAuditId = resultSet.getString("INTERNAL_AUDIT_ID");

				log.info("Working on SubOrder ("+subOrderid+") of Order ("+orderId+") with transaction id:" + transactionId);

				//Kickoff the WPJobs for the given processId
				Job job;
				try {
					//C30OrderSqlUtils.changeSubOrderStatus(orderId, subOrderid, C30SDKValueConstants.ORD_STATUS_IN_PROGRESS);
					C30WPConnection wpConn = C30WPConnection.getInstance();

					job = Job.createJobFromProcess(wpConn.getWpClientContext(), null, wpProcessId, subOrderid,null);

					job.setBulkUserData(C30SDKAttributeConstants.ATTRIB_C30ORDER_ORDERID, orderId);
					job.setBulkUserData(C30SDKAttributeConstants.ATTRIB_C30ORDER_SUBORDER_ID, subOrderid);
					job.setBulkUserData(C30SDKAttributeConstants.ATTRIB_C30ORDER_ACCOUNT_SEG_ID, acctSegId);
					job.setBulkUserData(C30SDKAttributeConstants.ATTRIB_C30ORDER_TRANSACTION_ID, transactionId);
					job.setBulkUserData(C30SDKAttributeConstants.ATTRIB_C30ORDER_INTERNAL_ID, internalAuditId);

					if (clientOrgName != null) job.setBulkUserData(C30SDKAttributeConstants.ATTRIB_C30ORDER_CLIENT_ORGNAME, clientOrgName);
					if (clientOrgId != null) job.setBulkUserData(C30SDKAttributeConstants.ATTRIB_C30ORDER_CLIENT_ORDID, clientOrgId);

					job.save();

					log.info("Workpoint job created with ID : " + job.getJobID());
					C30OrderSqlUtils.changeSubOrderStatus(orderId, subOrderid, transactionId, C30SDKValueConstants.ORD_STATUS_IN_PROGRESS, job.getJobID().toString(), ordeMgrSdkdataSource);

				} catch (C30SDKToolkitException e) {
					log.error(e);
				} catch (WorkPointEJBException e) {
					log.error(e);
				} catch (RemoteException e) {
					log.error(e);				
				}
				
				
			}
			ps.close();
		} catch(Exception e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-032"), e, "CONN-032");
		} 


	}



	/** if the given order is still waiting for the dependent orders to complete then we return true.
	 * 
	 * @param orderId
	 * @return
	 * @throws C30SDKObjectConnectionException 
	 * @throws SQLException 
	 * @throws C30SDKInvalidAttributeException 
	 * @throws C30SDKToolkitException 
	 */
	private boolean checkOrderDependency(String orderId, String transId) throws C30SDKObjectConnectionException, SQLException, C30SDKInvalidAttributeException, C30SDKToolkitException {

		boolean isOrderStillDependent = false;

		//Get if the order is dependent or not from the is_dependent and dependent_order_id fields in the C30_SDK_ORDER table.
		HashMap dependencyInfo = C30OrderSqlUtils.getOrderDependencyData(orderId, ordeMgrSdkdataSource);
		if (dependencyInfo == null )  return false;
		if (dependencyInfo.get("IsDependent") !=null )
		{
			log.debug("IsDependent for the given order id = " +orderId +" is = "+ dependencyInfo.get("IsDependent"));
			if (((Integer)dependencyInfo.get("IsDependent")).intValue() == 0)
			{
				log.debug("IsDependent for the given order id = " +orderId +" is = 0");
				return false;
			}
			else
			{
				log.debug("IsDependent for the given order id = " +orderId +" is Non Zero");
				String dependentOrderId = null;
				if (dependencyInfo.get("DependentOrderId") != null )
					dependentOrderId  =  (String)dependencyInfo.get("DependentOrderId");

				//In this case we have an dependent orderId to handle. So, we check the status of this order first before
				// we proceed to the next check.
				if (dependentOrderId != null && !dependentOrderId.equalsIgnoreCase(""))
				{
					log.debug("Dependent OrderId for the given order id = " +orderId +" is =" + dependentOrderId);
					//Check the status of the given DependentOrderId. If this is completed then we will return false. else true.
					Integer orderStatusId = C30OrderSqlUtils.getOrderStatusByOrderId(orderId, ordeMgrSdkdataSource);
					if (orderStatusId != null && orderStatusId.equals(C30SDKValueConstants.ORD_STATUS_COMPLETED))
					{
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					//In this case we don't have any dependentOrderId given. So, we have to drill down the order request and find what 
					// order is dependent on the given one based on the telephone number.
					// Currently we are reading the payload into a String, but it is not good always.
					// We need to change it to InputStream / or File like a proper blob data.
					log.debug("No Dependent OrderId given for the given order id = " +orderId);

					// Get the orderPayload.
					//InputStream payloadIS = (InputStream)dependencyInfo.get("Payload");
					String payloadIS = (String) dependencyInfo.get("Payload");
					Document orderXMLDoc = C30DOMUtils.stringToDomAndValidate(payloadIS,C30SDKValueConstants.C30_ORDER_SCHEMA);
					
					OrderRequest order;  
					try {
						//Use JAXB for the unmarshalling
						JAXBContext context = JAXBContext.newInstance(OrderRequest.class);
						Unmarshaller u = context.createUnmarshaller();

						order = (OrderRequest)u.unmarshal(orderXMLDoc);

						// Check if there is a ServiceObjectList in the give Order XML.
						if (order.getServiceObjectList() != null && order.getServiceObjectList().getServiceObject()!= null)
						{
							List<C30ServiceObject> serviceObjectList = order.getServiceObjectList().getServiceObject();
							for (C30ServiceObject serviceObj : serviceObjectList )
							{
								// This is the telephone Number, in this case we have clientId usually stored in the C30_SDK_SUB_ORDER table
								// and see if we can find the 
								String serviceClientId ="";
								if (serviceObj.getClientId() != null)
								{
									serviceClientId = serviceObj.getClientId();
									C30OrderActionType serviceClientActionType  = serviceObj.getClientActionType();
									// For the given serviceClientId, check if there are any orders with the pending status.
									// If the current action is "Change" check if there are any other pending "Add", "Change", "Disconnect"
									//For now we will be checking if there are any Pending (Irrespective ot types)
									if (! serviceClientActionType.equals(C30OrderActionType.ADD))
									{
										isOrderStillDependent = C30OrderSqlUtils.isOrderDependent(transId, orderId, serviceClientId, serviceClientActionType.value(), ordeMgrSdkdataSource);
									}
								}
							}
						}

					}
					catch(Exception e)
					{ 
						log.error(e);
						throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("CONN-031"),e,"CONN-031");
					}

				}
			}

		}

		return isOrderStillDependent;

	}

}
