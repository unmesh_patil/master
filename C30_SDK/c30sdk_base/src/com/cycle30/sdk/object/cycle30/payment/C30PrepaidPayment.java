package com.cycle30.sdk.object.cycle30.payment;

import org.apache.log4j.Logger;

import com.cycle30.connector.aq.QueueManager;
import com.cycle30.connector.exception.ConnectorException;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

public class C30PrepaidPayment extends C30SDKObject implements Cloneable {

	
	
	private static Logger log = Logger.getLogger(C30PrepaidPayment.class);

	/**
	 * Creates a new instance of C30PrepaidPayment.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidPayment(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30PrepaidPayment(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30PrepaidPayment.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	@Override
	public Object clone() {
		C30PrepaidPayment copy = (C30PrepaidPayment) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30PrepaidPaymentRegister
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
		String payLoad="";		
		try {
			setAttributesFromNameValuePairs();
 
			String  orgId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			Integer acctSegId = this.getAcctSegId();
			String  transId    = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
			payLoad = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_PAYLOAD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);					
	        log.info("Register Payment with Payload : "+payLoad);
			registerPrepaidPaymentEntry(acctSegId.toString(), payLoad);
	        log.info("Payment Registered with Payload : "+payLoad);			
		    setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    
            lwo = this;
             
		} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
		} catch (ConnectorException e) {
			lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}

	/**
	 * Method to process the payment request for FORCE placing it in a Non-Order Queue
	 * @param acctSegId
	 * @param payLoad
	 * @return
	 * 
	 */   
	private  void registerPrepaidPaymentEntry(String acctSegId, String payLoad) throws C30SDKException, ConnectorException {

		try{
			
			StringBuffer payloadwithKey = new StringBuffer();
			
			payloadwithKey.append(C30SDKValueConstants.PREPAID_PAYMENT_CALL_BACK_TYPE);
			payloadwithKey.append("--");
			payloadwithKey.append(acctSegId);
			payloadwithKey.append("--");
			payloadwithKey.append(payLoad);
			
	        log.info("Non Order Payment Register Payload to Queue Manager : "+payloadwithKey);
	        QueueManager queueManager =  QueueManager.getInstance();
        
	        //Create message payload for sending to Non Order Status Queue
	        queueManager.createNonOrderSenderQueueMessage(payloadwithKey.toString());
	        log.info("Non Order Payment Register Payload : "+payloadwithKey.toString());
	        String forceResponse="SUCESS";
	        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_CALLBACK_RESPONSE, forceResponse , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        
		}
		catch(C30SDKException e)
		{
			String errMsg = e.getMessage();
			String errCode = e.getExceptionCode();
		
			log.error(errMsg);
			throw new C30SDKException(errMsg, e, errCode);
		} 
		  
	}
	

		/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 
	}

	
	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}