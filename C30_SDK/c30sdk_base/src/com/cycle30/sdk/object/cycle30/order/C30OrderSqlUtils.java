package com.cycle30.sdk.object.cycle30.order;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory.OrderWorkPointMap;
import com.cycle30.sdk.datasource.C30DataSource;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.object.C30SDKSqlConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30EmfConfig;
import com.cycle30.sdk.schema.jaxb.C30Integer;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.schema.jaxb.OrderRequest.ServiceObjectList;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.ToolkitUtils;

/** 
 * 
 * @author Umesh
 * - Added Component & Identifier Sub order/WP JobId insert/update.
 * - Added "ItemType" Validation for Account and Plan JobId update
 * 
 */

public class C30OrderSqlUtils {

	private static Logger log = Logger.getLogger(C30OrderSqlUtils.class);
	public static C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();


	/**
	 * Method to store the Account SubOrder Information in database.
	 * @param hm HashMap of sub order data 
	 * @throws C30SDKObjectConnectionException 
	 */
	public static void storeAccountSubOrderInDataBase(HashMap<String, Object>hm, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {


		String orgId =          (String)hm.get("orgId"); 
		String orgName =        (String)hm.get("orgName"); 
		OrderRequest order =    (OrderRequest)hm.get("order"); 
		InputStream payload =   (InputStream)hm.get("payload");
		ArrayList wpList =      (ArrayList)hm.get("wpList");
		Integer acctSegId =     (Integer)hm.get("acctSegId");
		String transactionId =  (String)hm.get("transId");
		String auditId =        (String)hm.get("auditId");

		log.debug("Storing the Account Sub Order Information in to C30_SDK_SUB_ORDER");


		C30AccountObject account = order.getAccount();

		try  {
			String itemType=C30SDKValueConstants.ITEM_TYPE_ACCOUNT;			

			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_insert_sub_order");

			call.addParam(new C30CustomParamDef("v_client_order_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_sub_order_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_sub_order_name", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_type", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_action_type", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_action_reason", 2, 2048, 1)); //new
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1, 300, 1));
			call.addParam(new C30CustomParamDef("v_payload", 11, 300, 1));
			call.addParam(new C30CustomParamDef("v_billing_acct_number", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_emf_config_id", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_wp_process_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_status_id", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_create_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_desired_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_complete_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_level", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_org_name", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_org_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_transaction_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_internal_audit_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_client_id", 2, 512, 1)); //new
			call.addParam(new C30CustomParamDef("v_is_cancellable", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_is_resubmittable", 1, 300, 1));			
			

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------
			// Add suffix to match service/plan suffix pattern 
			String orderId = order.getClientOrderId().getValue();
			paramValues.add(orderId);
			// Add suffix to match service/plan suffix pattern
			paramValues.add(orderId + "-1");

			paramValues.add(order.getClientOrderName());
			paramValues.add(C30SDKValueConstants.ITEM_TYPE_ACCOUNT);
			paramValues.add(account.getClientActionType().toString());
			paramValues.add(null); // account.getClientActionTypeReason() does not exist
			paramValues.add(acctSegId);
			paramValues.add(payload);
			paramValues.add(null);
			paramValues.add(null);
			//get the correct WpJobId
			String wpJob = "";
			//			if (account.getAccountCategory().getValue() != null)
			//				wpJob= getWorkPointProcessId(wpList, acctSegId,0,null,
			//						(account.getAccountCategory()).getValue().getValue(),null,itemType, account.getClientActionType().toString());
			//			else
			wpJob= getWorkPointProcessId(wpList, acctSegId,0,null,
					null,null,itemType, account.getClientActionType().toString());
			log.info("Workpoint job id for the account :"+wpJob);
			paramValues.add(wpJob);
			paramValues.add(C30SDKValueConstants.ORD_STATUS_INITIATED); // Status Initiated
			paramValues.add(new Date());

			Date orderDesireDate = null;
			if (order.getOrderDesiredDate() != null) {
				orderDesireDate = order.getOrderDesiredDate().toGregorianCalendar().getTime();
			}

			paramValues.add(orderDesireDate);

			paramValues.add(null);		
			paramValues.add(null);		
			paramValues.add(orgName);
			paramValues.add(orgId);
			paramValues.add(transactionId);
			paramValues.add(auditId);
			paramValues.add(null); // not sure where to get ClientId for Account orders
			paramValues.add(C30SDKValueConstants.ORD_STATUS_IS_CANCELLABLE); 
			paramValues.add(C30SDKValueConstants.ORD_STATUS_IS_RESUBMITTABLE);

			dataSource.queryData(call, paramValues, 2);

		} catch(Exception e)  {
			log.error(e);
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-033"), e,e.getMessage(), "CONN-033");
		} 

		log.debug("Successfully populated Account data into C30_SDK_SUB_ORDER");

	}



	/**
	 * Get the Workpoint ProcessId based on the type and acctSegid etc., 
	 * @param wpList
	 * @return
	 * public static String getWorkPointProcessId(ArrayList wpList, Integer acctSegId, Integer isServiceLevel, 
			Integer emfConfigId, Integer accountCategory,
			String itemId, String itemType, String action)
	 */
	public static String getWorkPointProcessId(ArrayList wpList, 
			int acctSegId,  
			int isServiceLevel, 
			Integer emfConfigId, 
			Integer accountCategory,
			String itemId, 
			String itemType, 
			String action)  {

		String wpProcId="";

		//If need to find the account Level Process information.
		if (isServiceLevel==0 && itemType.equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_ACCOUNT))  {

			//AccountLevel Process Id
			//Required to consider the AccountCategory and AcctSegId.
			for(int i=0; i<wpList.size();i++) {
				OrderWorkPointMap ordWpMap = (OrderWorkPointMap)wpList.get(i);

				if(ordWpMap.getItemType().equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_ACCOUNT) && ordWpMap.getOrderType().equalsIgnoreCase(action)) { 
					if (ordWpMap.getAccountCategory()==accountCategory && ordWpMap.getAcctSegId()==acctSegId)  {
						wpProcId=ordWpMap.getWpProcessId();
						break;
					}
				}
			}
		}
		else if (itemType.equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_COMPONENT))  {
			//Component - Process Id
			//If the accountSegId and Service Level 
			for(int i=0; i<wpList.size();i++)  {
				OrderWorkPointMap ordWpMap = (OrderWorkPointMap)wpList.get(i);

				if(ordWpMap.getItemType().equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_COMPONENT) && ordWpMap.getOrderType().equalsIgnoreCase(action)) { 
					if (ordWpMap.getIsServiceLevel()==isServiceLevel && ordWpMap.getAcctSegId()==acctSegId)  {
						wpProcId=ordWpMap.getWpProcessId();
						break;
					}
				}
			}
		} //End - else if (itemType.equalsIgnoreCase(C30SDKValueConstants.COMPONENT_ITEM_TYPE))
		else if (itemType.equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_IDENTIFIER))  {
			//Identifier - Process Id
			//If the accountSegId and Service Level 
			for(int i=0; i<wpList.size();i++) {
				OrderWorkPointMap ordWpMap = (OrderWorkPointMap)wpList.get(i);

				if(ordWpMap.getItemType().equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_IDENTIFIER) && ordWpMap.getOrderType().equalsIgnoreCase(action))  { 
					if (ordWpMap.getIsServiceLevel()==isServiceLevel && ordWpMap.getAcctSegId()==acctSegId) {
						wpProcId=ordWpMap.getWpProcessId();
						break;
					}
				}
			}
		} //End - else if (itemType.equalsIgnoreCase(C30SDKValueConstants.IDENTIFIER_ITEM_TYPE))
		else if (itemType.equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_SERVICE))  {
			//Plan Level
			for(int i=0; i<wpList.size();i++)  {
				OrderWorkPointMap ordWpMap = (OrderWorkPointMap)wpList.get(i);

				if(ordWpMap.getItemType().equalsIgnoreCase(C30SDKValueConstants.ITEM_TYPE_SERVICE) && ordWpMap.getOrderType().equalsIgnoreCase(action))  { 
					if (ordWpMap.getEmfConfigId() == null) {
						if (ordWpMap.getIsServiceLevel()==isServiceLevel && ordWpMap.getAcctSegId()==acctSegId) {
							wpProcId=ordWpMap.getWpProcessId();
						}
					}
					else {

						if (ordWpMap.getEmfConfigId() == emfConfigId && ordWpMap.getIsServiceLevel()==isServiceLevel && ordWpMap.getAcctSegId()==acctSegId)  {
							wpProcId=ordWpMap.getWpProcessId();
							break;
						}
					}
				} 
			}
		}

		return wpProcId;
	}



	/**
	 * @param hm HashMap of sub order data elements
	 * @throws C30SDKObjectConnectionException
	 */
	public static void storeServiceSubOrderInDataBase(HashMap<String, Object>hm, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {

		String orgId =             (String)hm.get("orgId"); 
		String orgName =           (String)hm.get("orgName"); 
		C30ServiceObject service = (C30ServiceObject)hm.get("service"); 
		int serviceIndex = Integer.valueOf((String)hm.get("serviceIdx"));
		OrderRequest order =       (OrderRequest)hm.get("order");
		InputStream payload =      (InputStream)hm.get("payload");
		ArrayList wpList =         (ArrayList)hm.get("wpList");
		Integer acctSegId =        (Integer)hm.get("acctSegId");
		String transactionId =     (String)hm.get("transId");
		String auditId =           (String)hm.get("auditId");
		
		//Insert the Order data into C30_SDK_SUB_ORDER
		log.debug("Storing the Service Order  Information in to C30_SDK_SUB_ORDER");

		try  {
			Integer emfConfigId =null;
			String itemType=C30SDKValueConstants.ITEM_TYPE_SERVICE;


			C30EmfConfig emfConfig = service.getEmfConfig();
			if (emfConfig != null) {
				C30Integer emfConfigIint = emfConfig.getEmfConfigId().getValue();
				if (emfConfigIint != null) {
					emfConfigId =   emfConfigIint.getValue();
				}
			}

			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_insert_sub_order");

			call.addParam(new C30CustomParamDef("v_client_order_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_sub_order_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_sub_order_name", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_type", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_action_type", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_action_reason", 2, 2048, 1));
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1, 300, 1));
			call.addParam(new C30CustomParamDef("v_payload", 11, 300, 1));
			call.addParam(new C30CustomParamDef("v_billing_acct_number", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_emf_config_id", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_wp_process_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_status_id", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_create_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_desired_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_complete_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_sub_order_level", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_org_name", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_org_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_transaction_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_internal_audit_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_client_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_is_cancellable", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_is_resubmittable", 1, 300, 1));			

			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------
			String orderId = order.getClientOrderId().getValue();
			paramValues.add(orderId);
			paramValues.add( orderId + "-" + serviceIndex);  
			paramValues.add(order.getClientOrderName());
			paramValues.add(C30SDKValueConstants.ITEM_TYPE_SERVICE);

			paramValues.add(service.getClientActionType().toString());
			paramValues.add(service.getClientActionTypeReason());

			paramValues.add(acctSegId);
			paramValues.add(payload);
			paramValues.add(service.getClientAccountId());

			paramValues.add(emfConfigId);
			//get the correct WpJobId
			String wpJob = "";
			wpJob= getWorkPointProcessId(wpList, acctSegId,1,
					emfConfigId,
					null,null,C30SDKValueConstants.ITEM_TYPE_SERVICE, service.getClientActionType().toString());
			paramValues.add(wpJob);
			paramValues.add(C30SDKValueConstants.ORD_STATUS_INITIATED); // Status Initiated
			paramValues.add(new Date());

			Date orderDesireDate = null;
			if (order.getOrderDesiredDate() != null) {
				orderDesireDate = order.getOrderDesiredDate().toGregorianCalendar().getTime();
			}

			paramValues.add(orderDesireDate);


			paramValues.add(null);		
			paramValues.add(null);		
			paramValues.add(orgName);
			paramValues.add(orgId);
			paramValues.add(transactionId);
			paramValues.add(auditId);
			if (service.getClientId() != null )
				paramValues.add(service.getClientId());
			else
				paramValues.add(null);
			paramValues.add(C30SDKValueConstants.ORD_STATUS_IS_CANCELLABLE); 
			paramValues.add(C30SDKValueConstants.ORD_STATUS_IS_RESUBMITTABLE);
			dataSource.queryData(call, paramValues, 2);

		} catch(Exception e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-033"), e,e.getMessage(), "CONN-033");
		}

		log.debug("Successfully populated Order data into C30_SDK_SUB_ORDER");

	}





	/**
	 * This is used to store the Order Object in the database.
	 * @param orgId 
	 * @param orgName 
	 * @param order
	 * @param payload
	 * @param acctSegId
	 * @param transactionId
	 * @throws C30SDKObjectConnectionException 
	 */
	public static void storeOrderObjectInDateBase(String orgId, 
			String orgName, 
			OrderRequest order, 
			InputStream payload, 
			Integer acctSegId,
			String transactionId,
			String auditId, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {

		log.info("Storing the Order Information in to C30_SDK_ORDER");


		try  {
			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_insert_order_data");

			call.addParam(new C30CustomParamDef("v_client_order_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_order_name", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_order_type", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_acct_seg_id",1, 300, 1));
			call.addParam(new C30CustomParamDef("v_payload", 11, 300, 1));
			call.addParam(new C30CustomParamDef("v_is_locked", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_wp_process_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_order_status_id", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_order_create_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_order_desired_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_order_complete_date", 13, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_org_name", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_client_org_id", 2, 300, 1));
			call.addParam(new C30CustomParamDef("v_transaction_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_internal_audit_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_is_dependent", 1, 512, 1));
			call.addParam(new C30CustomParamDef("v_dependent_order_id", 2, 512, 1));
			call.addParam(new C30CustomParamDef("v_is_cancellable", 1, 300, 1));
			call.addParam(new C30CustomParamDef("v_is_resubmittable", 1, 300, 1));			
			

			ArrayList paramValues = new ArrayList();

			//--------------------------------
			// set the input parameter values
			//--------------------------------
			String orderId = order.getClientOrderId().getValue();
			paramValues.add(orderId);
			paramValues.add(order.getClientOrderName());

			if (order.getAccount() != null) {
				//If account object is not null then read the data from the account object.
				paramValues.add(order.getAccount().getClientId());
				paramValues.add(order.getAccount().getClientActionType().toString());
			}
			else if(order.getServiceObjectList() != null)  {
				//If account object is null then check a service Object for an account Number
				ServiceObjectList serviceList = order.getServiceObjectList();
				C30ServiceObject service = serviceList.getServiceObject().get(0);
				if (service != null) {
					paramValues.add(service.getClientAccountId());
					// if there is a Service in the Order means it would be a Account Change. So, the order would be a change order.
					paramValues.add(C30SDKValueConstants.C30_ACTION_CHANGE);
				}
			}

			paramValues.add(acctSegId);
			paramValues.add(payload);
			paramValues.add(C30SDKValueConstants.ORD_LOCKED);
			paramValues.add(null);
			paramValues.add(C30SDKValueConstants.ORD_STATUS_INITIATED); // Status Initiated
			paramValues.add(new Date());


			Date orderDesireDate = null;
			if (order.getOrderDesiredDate() != null) {
				orderDesireDate = order.getOrderDesiredDate().toGregorianCalendar().getTime();
			}

			paramValues.add(orderDesireDate);

			paramValues.add(null);
			paramValues.add(orgName);
			paramValues.add(orgId);
			paramValues.add(transactionId);
			paramValues.add(auditId);


			// Get the dependency values
			if (order.getIsDependent() != null && 
					order.getIsDependent().getValue() != null)
			{
				if (order.getIsDependent().getValue())
					paramValues.add(1);
				else
					paramValues.add(0);
			}
			else
			{
				paramValues.add(0);
			}
			if (order.getDependentOrderId() != null && 
					order.getDependentOrderId().getValue() != null)
			{
				paramValues.add(order.getDependentOrderId().getValue());
			}
			else
			{
				paramValues.add(null);
			}
			paramValues.add(C30SDKValueConstants.ORD_STATUS_IS_CANCELLABLE); 
			paramValues.add(C30SDKValueConstants.ORD_STATUS_IS_RESUBMITTABLE);

			dataSource.queryData(call, paramValues, 2);

		}
		catch(Exception e)  {
			log.error(e);
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("ORD-004"), e, e.getMessage(),"ORD-004");
		}

		log.debug("Successfully populated Plan data into C30_SDK_ORDER");

	}




	/**
	 * This is used to get the open order information from database.
	 *  
	 * @throws C30SDKObjectConnectionException 
	 */
	public static void getOpenOrders(C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {

		try  {
			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_open_order_data");

			call.addParam(new C30CustomParamDef("client_order_id", 2, 300, 2));
			call.addParam(new C30CustomParamDef("client_order_name", 2, 300, 2));
			call.addParam(new C30CustomParamDef("transaction_id", 2, 300, 2));

			ArrayList paramValues = new ArrayList();

			dataSource.queryData(call, paramValues, 2);

			log.debug("Total open orders found = " + dataSource.getRowCount());
			if(dataSource.getRowCount() > 0) {
				for(int i = 0; i < dataSource.getRowCount(); i++)  {

					if(dataSource.getValueAt(i,0) != null)   {  // order id

						// This populates the orderQueue with concatenation of
						// OrderIds + TransactionId
						String orderId  = (String)dataSource.getValueAt(i,0);
						String transId  = (String)dataSource.getValueAt(i,2);
						String queueKey = orderId + ":" + transId;
						C30SDKOrderManager.orderQueue.add(queueKey);
					}
				}
			}

		}  catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-032"), e,e.getMessage(), "CONN-032");
		}

	}




	/**
	 * Update the orderstatus to a status and Locks the sub order.
	 * @param orderId
	 * @param orderStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void changeSubOrderStatus(String orderId, 
			String subOrderId, 
			String transactionId, 
			Integer orderStatusId, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException {

		String query = C30SDKSqlConstants.CHANGE_SUBORDER_STATUS;
		PreparedStatement ps = null;
		ResultSet resultSet =null;		
		try  {
			ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,orderStatusId);
			ps.setString(2, orderId);
			ps.setString(3, subOrderId);
			ps.setString(4, transactionId);
			resultSet  = ps.executeQuery();

		}  catch(Exception e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-003"),e,e.getMessage(),"ORD-003");
		}  finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null)
					ps.close();
			} catch (Exception ex) {
				log.error("changeSubOrderStatus - Exception: "+ex.getMessage());
			}
		}
		
	}



	/**
	 * Update the orderstatus to a status and Locks the sub order.
	 * @param orderId
	 * @param orderStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void changeSubOrderStatus(String orderId, 
			String subOrderId, 
			String transactionId,
			Integer orderStatusId,
			String jobId,C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
			{
				PreparedStatement ps = null;
				ResultSet resultSet = null;				
				String query = C30SDKSqlConstants.CHANGE_SUBORDER_STATUS_UPDATE_JOB_ID;
		
				try  {
					ps = dataSource.getPreparedStatement(query);
					ps.setInt(1,orderStatusId);
					ps.setString(2,jobId);
					ps.setString(3, orderId);
					ps.setString(4, subOrderId);
					ps.setString(5, transactionId);
					resultSet  = ps.executeQuery();

				} catch(Exception e) {
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-003"),e,e.getMessage(),"ORD-003");
				} finally {
					try {
						if (resultSet != null) {
							resultSet.close();
						}
						if (ps != null) {
							ps.close();
						}
					} catch (Exception ex) {
						log.error("changeSubOrderStatus - Exception: "+ex.getMessage());
					}
				} 
					
			}




	/**
	 * Update the orderstatus to a status and Locks the sub order.
	 * @param orderId
	 * @param lockUnlock 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void updateOrderLockUnlockStatus(String orderId, 
			String transactionId, 
			Integer lockUnlock, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException   {

		String query = C30SDKSqlConstants.UPDATE_ORDER_LOCK_UNLOCK;
		PreparedStatement ps = null;
		ResultSet resultSet = null;		
		try  {

			ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,lockUnlock);
			ps.setString(2, orderId);
			ps.setString(3, transactionId);
			resultSet  = ps.executeQuery();

		} catch(Exception e)  {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-001"),e,e.getMessage(),"ORD-001");
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ps != null) {				
					ps.close();
				}
			} catch (Exception ex) {
				log.error("updateOrderLockUnlockStatus - Exception: "+ex.getMessage());
			}
		} 
	}



	/**
	 * Update the orderstatus to a status and Locks the order.
	 * @param orderId
	 * @param transid
	 * @param orderStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void changeOrderStatus(String orderId, 
			String transId,
			Integer orderStatusId, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
		{
			PreparedStatement ps = null;
			ResultSet resultSet = null;			
			String query = C30SDKSqlConstants.CHANGE_ORDER_STATUS;
	
			try  {
	
				ps = dataSource.getPreparedStatement(query);
				ps.setInt(1,orderStatusId);
				ps.setString(2, orderId);
				ps.setString(3, transId);
				resultSet  = ps.executeQuery();
	
			}  catch(Exception e) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-002"),e,e.getMessage(),"ORD-002");
			} finally {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ex) {
					log.error("changeOrderStatus - Exception: "+ex.getMessage());
				}
			} 
		}



	/**
	 * Get the Dependency order information for the orderI
	 * @param orderId
	 * @throws C30SDKObjectConnectionException
	 */
	public static HashMap getOrderDependencyData(String orderId, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {


		log.debug("Get the Dependency order information for the orderId - " + orderId);
		HashMap dependencyInfo =null;
		String query = C30SDKSqlConstants.GET_ORDER_DEPENDENCY_INFORMATION;
		ResultSet resultSet = null;
		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setString(1,(String)orderId);
			resultSet  = ps.executeQuery();
			if (resultSet.next())
			{
				dependencyInfo = new HashMap();
				dependencyInfo.put("OrderId", orderId);
				dependencyInfo.put("IsDependent", resultSet.getInt("IS_DEPENDENT"));
				dependencyInfo.put("DependentOrderId", resultSet.getString("DEPENDENT_ORDER_ID"));
				// Get the orderPayload.
				byte[] bdata = resultSet.getBytes("PAYLOAD");
				String xmlPayload= new String (bdata);
				dependencyInfo.put("Payload",xmlPayload);
			}
			ps.close();

		} catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-032"), e,e.getMessage(), "CONN-032");
		} 

		return dependencyInfo;
	} 


	/** Return the order status by OrderId
	 * 
	 * @param orderId
	 * @param dataSource
	 * @return
	 * @throws C30SDKObjectConnectionException
	 */
	public static Integer getOrderStatusByOrderId(String orderId, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {
		Integer orderStatusId = null;
		log.debug("Get the order status by  orderId - " + orderId);
		String query = C30SDKSqlConstants.GET_ORDER_STATUS_BY_ORDER_ID;
		ResultSet resultSet = null;
		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setString(1,(String)orderId);
			resultSet  = ps.executeQuery();
			if (resultSet.next())
			{
				orderStatusId = new Integer(resultSet.getInt("ORDER_STATUS_ID"));
			}
			ps.close();
		} catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-032"), e,e.getMessage(), "CONN-032");
		} 
		return orderStatusId;
	} 

	

	/**
	 * Update the orderstatus to a status and Locks the order.
	 * @param orderId
	 * @param transid
	 * @param orderStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	
	public static void changeOrderStatus(String orderId, 
			Integer orderStatusId) throws Exception  {
		C30JDBCDataSource dataSource =null;
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();
			//Change the Order Status
			C30OrderSqlUtils.changeOrderStatus(orderId, orderStatusId, dataSource);
			//Change the SubOrder Status
			C30OrderSqlUtils.changeSubOrderStatus(orderId, orderStatusId, dataSource);
		}
		catch(Exception e )
		{
			log.error("changeOrderStatus = "+e);
			throw e;
		}
		finally
		{
			try {
				C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(dataSource.getConnection());
			} catch (Exception e) {
				log.error("ERROR - changeOrderStatus = " +e);
				throw e;
			}
		}
	}
	
	/**
	 * Update the order status to a status and unLocks the order.
	 * @param orderId
	 * @param orderStatusId
	 * @param dataSource 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */	
	public static void changeOrderStatus(String orderId, 
			Integer orderStatusId, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
		{
			PreparedStatement ps = null;
			ResultSet resultSet = null;			
			String query = C30SDKSqlConstants.CHANGE_ORDER_STATUS_CANCEL;
	
			try  {
				ps = dataSource.getPreparedStatement(query);
				ps.setInt(1,orderStatusId);
				ps.setString(2, orderId);
				resultSet  = ps.executeQuery();
	
			}  catch(Exception e) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-002"),e,e.getMessage(),"ORD-002");
			}  finally {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ex) {
					log.error("changeOrderStatus - Exception: "+ex.getMessage());
				}
			} 
			
		}

	/**
	 * Update the SubOrder status to a status and unLocks the order.
	 * @param orderId
	 * @param orderStatusId
	 * @param dataSource 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */	
	public static void changeSubOrderStatus(String orderId, 
			Integer orderStatusId, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
		{
			PreparedStatement ps = null;
			ResultSet resultSet = null;			
			String query = C30SDKSqlConstants.CHANGE_SUBORDER_STATUS_CANCEL;
	
			try  {
				ps = dataSource.getPreparedStatement(query);
				ps.setInt(1,orderStatusId);
				ps.setString(2, orderId);
				resultSet  = ps.executeQuery();
	
			}  catch(Exception e) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-002"),e,e.getMessage(),"ORD-002");
			}  finally {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ex) {
					log.error("changeSubOrderStatus - Exception: "+ex.getMessage());
				}
			} 
			
		}
	
	/**
	 * Get the order status.
	 * @param orderId
	 * @param transid
	 * @param orderStatusId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	
	public static void getOrderStatus(String transactionId) throws Exception  {
		C30JDBCDataSource dataSource =null;
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();
			C30OrderSqlUtils.getOrderStatus(transactionId, dataSource);
		}
		catch(Exception e )
		{
			log.error("getOrderStatus = "+e);
			throw e;
		}
		finally
		{
			try {
				C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(dataSource.getConnection());
			} catch (Exception e) {
				log.error("ERROR - getOrderStatus = " +e);
				throw e;
			}
		}
	}
	
	public static void getOrderStatus(String transactionId, C30JDBCDataSource dataSource) 
					throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException 
		{
			PreparedStatement ps = null;
			ResultSet resultSet = null;			
			String query = C30SDKSqlConstants.GET_ORDER_STATUS_BY_TRANSACTION_ID;
	
			try  {
	
				ps = dataSource.getPreparedStatement(query);
				ps.setString(1, transactionId);
				resultSet  = ps.executeQuery();
	
			}  catch(Exception e) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-002"),e,e.getMessage(),"ORD-002");
			}  finally {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ex) {
					log.error("getOrderStatus -Exception: "+ex.getMessage());
				}
			} 
			
		}

	/**
	 * Get the status of an order, based on a request id 
	 * which was originally supplied as 'X-Trans-Id' HTTP header value.
	 * 
	 * @param requestId
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static HashMap getRequestStatus(String requestId) throws Exception  {

		C30JDBCDataSource dataSource =null;
		HashMap result =null;
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();
	
			result  = C30OrderSqlUtils.getRequestStatus(requestId, dataSource);
		}
		catch(Exception e)
		{
			log.error("getRequestStatus = "+e);
			throw e;
		}
		finally
		{
			try {
				C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(dataSource.getConnection());
			} catch (Exception e) {
				log.error("ERROR - getRequestStatus = " +e);
				throw e;
			}
		}
		return result;
	}
	
	public static HashMap getRequestStatus(String requestId, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException, 
	C30SDKInvalidAttributeException  {

		String query = C30SDKSqlConstants.GET_REQUEST_STATUS;

		HashMap<String, HashMap> results = new HashMap<String, HashMap>();

		try  {
			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setString(1,requestId);
			ResultSet rs = ps.executeQuery();

			int idx = 0;
			while (rs.next()) {

				String requestDate  = rs.getString(1);
				String featureId    = rs.getString(2);
				String featureName  = rs.getString(3);
				String subRequestId = rs.getString(4);
				String statusId     = rs.getString(5);
				String statusName   = rs.getString(6);
				String responseDate = rs.getString(7);
				String logMessage   = rs.getString(8);

				HashMap<String, String> hm = new HashMap<String, String>();
				hm.put("RequestDate",  requestDate);
				hm.put("FeatureId",    featureId);
				hm.put("FeatureName",  featureName);
				hm.put("SubRequestId", subRequestId);
				hm.put("StatusId",     statusId);
				hm.put("StatusName",   statusName);
				hm.put("ResponseDate", responseDate);
				hm.put("LogMessage",   logMessage);

				// Return a HashMap of HashMaps, with indexs 0, 1, 2...N
				String key = Integer.toString(idx++);
				results.put(key, hm);
			}

			//  If no provisioning information found, check order table.
			if (results.size() == 0) {

				ps = dataSource.getPreparedStatement(C30SDKSqlConstants.GET_ORDER_STATUS_BY_TRANSACTION_ID);
				ps.setString(1,requestId);
				rs = ps.executeQuery();

				while (rs.next()) {

					String requestDate  = rs.getString(1);
					String statusId     = rs.getString(2);

					String statusName = C30SDKValueConstants.orderStatusMap.get(statusId);
					if (statusName == null) {
						statusName = "Unknown status id: " + statusId;
					}

					// Return a HashMap of HashMaps, with indexs 0, 1, 2...N
					HashMap<String, String> hm = new HashMap<String, String>();
					hm.put("RequestDate",  requestDate);
					hm.put("FeatureName",  "");
					hm.put("StatusId",     statusId);
					hm.put("StatusName",   statusName);
					hm.put("ResponseDate", "");

					results.put("0", hm);
				}
			}
			ps.close();
			return results;
		}
		catch(Exception e)  {
			throw new C30SDKInvalidAttributeException("Error executing status inquiry SQL",e,e.getMessage(),"ORD-002");
		}
		
	}

	/**
	 * Insert the Sub order Extended Data.
	 * 
	 * @param requestId
	 * @param key
	 * @param value
	 * @param sendFlag
	 * @param acctSegId
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 * @throws SQLException
	 * @throws Exception
	 * 
	 */	
	public static void insertExtendedData(String requestId, String key, String value, String sendFlag, String acctSegId) 
			throws C30SDKObjectConnectionException, 
			C30SDKInvalidAttributeException, 
			SQLException,
			Exception 
			{
				log.info("Inserting Extended Data for Request Id :"+ requestId);
				C30JDBCDataSource datasource =  null;
				try
				{
					datasource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();
					
					C30OrderSqlUtils.writeToSubOrderExtendedData(requestId, key, value, sendFlag, acctSegId, (C30JDBCDataSource) datasource);
				}
				catch(Exception e)
				{
					log.error("insertExtendedData "+e);
					throw e;
				}
				finally{
					try {
						C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(datasource.getConnection());
					} catch (Exception e) {
						log.error("ERROR - insertExtendedData : " +e);
						throw e;
					}
				}
			}
	
	/**
	 * Insert the Sub order Extended Data.
	 * 
	 * @param requestId
	 * @param key
	 * @param value
	 * @param sendFlag
	 * @param acctSegId
	 * @param dataSource
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void writeToSubOrderExtendedData(String requestId, String key, String value, String sendFlag,
			String acctSegId, C30JDBCDataSource dataSource) 
			throws C30SDKObjectConnectionException, 
			C30SDKInvalidAttributeException, 
			SQLException,
			Exception 
			{
				String suborderId = null;
				String transactionId = null;
				ResultSet resultSet  = null;				
				PreparedStatement ps = null;

				// Verify the request exists.
				try {
					ps = dataSource.getPreparedStatement(C30SDKSqlConstants.QUERY_SUBORDER_FROM_REQUEST_ID);
					ps.setString(1, requestId);
					resultSet  = ps.executeQuery();
			
					while (resultSet.next()) {
						transactionId = resultSet.getString(1); 
						suborderId    = resultSet.getString(2); 
					}
					// if transaction id not found, just return.
					if (transactionId == null) {
						return;
					}
					
					C30OrderSqlUtils.insertSubOrderExtendedData(transactionId, suborderId, key, value, sendFlag, acctSegId, dataSource);
					
				} 
				catch (SQLException se) {
					log.error(se);
					throw se;
				}     
				finally {
					try {
						if (resultSet != null) {
							resultSet.close();
						}
						if (ps != null) {
							ps.close();							
						}
					} catch (Exception ex) {
						log.error("Exception-writeToSubOrderExtendedData: "+ex.getMessage());
					}
				}
		
			}
	
	/**
	 * Insert SubOrder Extended Data
	 * 
	 * @param statusId
	 * @param xml
	 * @param subOrderId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void insertSubOrderExtendedData(String transactionId, String suborderId, String key, String value, String sendFlag,
			String acctSegId, C30JDBCDataSource dataSource) 
			throws C30SDKObjectConnectionException, 
			C30SDKInvalidAttributeException, 
			SQLException,
			Exception 
			{
				PreparedStatement ps = null;
				ResultSet resultSet = null;
				try {
					
					String query = C30SDKSqlConstants.INSERT_INTO_SUBORDER_EXTENDED_DATA;

					ps = dataSource.getPreparedStatement(query);
					ps.setString(1,(String)suborderId);
					ps.setString(2,(String)transactionId);
					ps.setInt(3, Integer.valueOf(acctSegId));
					ps.setString(4,(String)key);
					ps.setString(5,(String)value);
					ps.setString(6,(String)sendFlag);			
					resultSet  = ps.executeQuery();						
		
				}
				catch (SQLException se) {
					log.error(se);
					throw se;
				}
				catch(Exception e) {
					log.error(e);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-009"),e,e.getMessage(),"ORD-009");
				}
				finally {
					try {
						if (resultSet != null) {
							resultSet.close();
						}
						if (ps != null) {
							ps.close();
						}
					} catch (Exception ex) {
						log.error("insertSubOrderExtendedData - Exception: "+ex.getMessage());
					}
				}		
			}
	
	public static void updateProvisioningStatus(String statusId, String xml, String requestId) 
			throws C30SDKObjectConnectionException, 
			C30SDKInvalidAttributeException, 
			SQLException,
			Exception 
			{
				log.info("Updating the status " + statusId + " for Request Id :"+ requestId);
				C30JDBCDataSource datasource =  null;
				try
				{
					datasource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();

					C30OrderSqlUtils.updateProvisioningStatus(statusId, xml,requestId, (C30JDBCDataSource) datasource);
				}
				catch(Exception e)
				{
					log.error("updateProvisioningStatus = "+e);
					throw e;
				}
				finally{
					try {
						C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(datasource.getConnection());
					} catch (Exception e) {
						log.error("ERROR - updateProvisioningStatus = " +e);
						throw e;
					}
				}
			}

	/**
	 * Update the provision status.
	 * 
	 * @param statusId
	 * @param xml
	 * @param subOrderId 
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 */
	public static void updateProvisioningStatus(String statusId, String xml, String requestId, C30JDBCDataSource dataSource) 
			throws C30SDKObjectConnectionException, 
			C30SDKInvalidAttributeException, 
			SQLException,
			Exception 
			{
				ResultSet resultSet = null;
				PreparedStatement ps = null;
				// Verify the request exists.
				try {

					ps = dataSource.getPreparedStatement(C30SDKSqlConstants.QUERY_PROV_INSTANCE_STATUS);
					ps.setString(1, requestId);
					resultSet  = ps.executeQuery();
					if (resultSet.next() == false) {
						throw new C30SDKInvalidAttributeException("Request id " + requestId + " not found.");
					}   
				} catch (SQLException se) {
					log.error(se);
					throw se;
				} catch(Exception e) {
					log.error(e);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-007"),e,e.getMessage(),"ORD-007");
				} finally {
					try {
						if (resultSet != null) {
							resultSet.close();
						}
						if (ps != null) {
							ps.close();							
						}
					} catch (Exception ex) {
						log.error("Exception: "+ex.getMessage());
					}
				}

				
				//Update Provision Instance Status
				try {
					String query = C30SDKSqlConstants.UPDATE_PROV_INSTANCE_STATUS;
					int status = Integer.valueOf(statusId);

					ps = dataSource.getPreparedStatement(query);
					ps.setInt(1, status);
					ps.setString(2, xml);
					ps.setString(3, requestId);
					resultSet  = ps.executeQuery();
				} catch (SQLException se) {
					log.error(se);
					throw se;
				}
				catch(Exception e) {
					log.error(e);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-007"),e,e.getMessage(),"ORD-007");
				} finally {
					try {
						if (resultSet != null) {
							resultSet.close();
						}
						if (ps != null) {
							ps.close();							
						}
					} catch (Exception ex) {
						log.error("Exception: "+ex.getMessage());
					}
				}
				
				// If no problem occurred, attempt to update the parent catalog data.
				try {

					int status = Integer.valueOf(statusId);					
					ps = dataSource.getPreparedStatement(C30SDKSqlConstants.UPDATE_PROV_CATALOG_STATUS);
					ps.setInt(1, status);
					ps.setString(2, requestId);
					ps.setString(3, requestId);
					ps.setInt(4, status);
					resultSet  = ps.executeQuery();
		
					// If this was a failure notification, update the C30_SDK_ORDER_TRANS_LOG
					// with the details so it can be researched and reported in inquiry services.
					if (statusId.equalsIgnoreCase(C30SDKValueConstants.SVC_PROVISION_FAILURE)) {
						updateOrderTransactionLog(requestId, xml, dataSource);  
					}

				}
				catch (SQLException se) {
					log.error(se);
					throw se;
				}
				catch(Exception e) {
					log.error(e);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-007"),e,e.getMessage(),"ORD-007");
				} finally {
					try {
						if (resultSet != null) {
							resultSet.close();
						}
						if (ps != null) {
							ps.close();							
						}
					} catch (Exception ex) {
						log.error("Exception: "+ex.getMessage());
					}
				}
		
			}



	/**
	 * Update the SDK order transaction log when a callback provisioning status
	 * is an error condition.
	 * 
	 * @param requestId
	 * @param xml
	 */
	private static void updateOrderTransactionLog(String requestId, 
			String xml,
			C30JDBCDataSource dataSource)  
					throws C30SDKInvalidAttributeException   {

		PreparedStatement ps = null;
		ResultSet rs = null;
		String orderId = null;
		String transactionId = null;
		int count = 0;

		try {

			// Get the original order id and transaction id from
			// the original callback request id, eg. 'ACS.34_1'
			ps = dataSource.getPreparedStatement(C30SDKSqlConstants.QUERY_ORDER_FROM_REQUEST_ID);
			ps.setString(1,requestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				transactionId = rs.getString(1); 
				orderId       = rs.getString(2); 
			}
			// if transaction id not found, just return. In theory... this shouldn't happen.
			if (transactionId == null) {
				return;
			}

		} catch (SQLException se) {
			log.error(se);
		} catch(Exception e) {
			log.error(e);
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-008"),e,e.getMessage(),"ORD-008");
		}  finally {
			try {
				if (rs != null) {
					rs.close();
				}				
				if (ps != null) {
					ps.close();							
				}
			} catch (Exception ex) {
				log.error("Exception-updateOrderTransactionLog: "+ex.getMessage());
			}
		}
		
		// See if an existing transaction log entry exists.		
		try {

			ps = dataSource.getPreparedStatement(C30SDKSqlConstants.QUERY_TRANSACTION_LOG);
			ps.setString(1,transactionId);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt(1);

		} catch (SQLException se) {
			log.error(se);
		} catch(Exception e) {
			log.error(e);
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-008"),e,e.getMessage(),"ORD-008");
		}  finally {
			try {
				if (rs != null) {
					rs.close();
				}				
				if (ps != null) {
					ps.close();							
				}
			} catch (Exception ex) {
				log.error("Exception:-updateOrderTransactionLog "+ex.getMessage());
			}
		}

		// If log entry doesn't exist, insert new one.
		try {

			String errorMsg = ToolkitUtils.getXmlStringElementValue(xml, "Error");
			String logMsg   = "Provisioning callback error: " + errorMsg;
			// limit message to 2048 chars max.
			String logMessage = logMsg;
			if (logMsg.length() > 2048) {
				logMessage = logMsg.substring(0, 2048);
			}

			if (count == 0) {
				ps = dataSource.getPreparedStatement(C30SDKSqlConstants.INSERT_PROV_TRANSACTION_LOG);
				ps.setString(1, orderId);
				ps.setString(2, requestId);
				ps.setString(3, transactionId);
				ps.setString(4, logMessage);
				ps.execute();
			}
			else {
				ps = dataSource.getPreparedStatement(C30SDKSqlConstants.UPDATE_PROV_TRANSACTION_LOG);
				ps.setString(1, logMessage);
				ps.setString(2, transactionId);
				ps.execute();  
			}
			
		} catch (SQLException se) {
			log.error(se);
		} catch(Exception e) {
			log.error(e);
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("ORD-008"),e,e.getMessage(),"ORD-008");
		}  finally {
			try {
				if (ps != null) {
					ps.close();							
				}
			} catch (Exception ex) {
				log.error("Exception:-updateOrderTransactionLog "+ex.getMessage());
			}
		}
	}


	/** This method will return if there are any dependent SubOrders for the given clientId
	 * 
	 * @param serviceClientId
	 * @param clietnActionType
	 * @return
	 * @throws C30SDKObjectConnectionException 
	 */
	public static boolean isOrderDependent(String transId,String orderId,  String serviceClientId, String clietnActionType, C30JDBCDataSource dataSource) throws C30SDKObjectConnectionException {
		boolean retValue  = false;
		log.debug("Get the dependent open orders for the given clientid - " + serviceClientId);
		String query = C30SDKSqlConstants.GET_IS_DEPENDENT_ORDER_EXISTS;
		ResultSet resultSet = null;
		PreparedStatement ps = null;
		try  {

			ps = dataSource.getPreparedStatement(query);
			ps.setString(1,serviceClientId);
			ps.setString(2,transId);
			ps.setString(3,orderId);
			
			resultSet  = ps.executeQuery();
			if (resultSet.next())
			{
				int count = resultSet.getInt("COUNT");
				log.debug("Count of the dependent open orders for the given clientid - " + serviceClientId + " is ="+count);
				if (count > 0) retValue = true;
			}

		}  catch(Exception e)  {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-032"), e,e.getMessage(), "CONN-032");
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}				
				if (ps != null) {
					ps.close();							
				}
			} catch (Exception ex) {
				log.error("Exception:-isOrderDependent "+ex.getMessage());
			}
		}

		return retValue;
	}

}
