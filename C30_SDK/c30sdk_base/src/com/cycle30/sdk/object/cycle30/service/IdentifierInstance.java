package com.cycle30.sdk.object.cycle30.service;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/** 
 * Class that presents a single Identifier instance that maps back
 * to custom Oracle data type in C30ARBOR database called C30IDENTIFIERCOL
 *  
 * @author mark
 *
 */
public class IdentifierInstance {

    private String externalId;
    private String externalIdType;
    private String description;
    
    /**
     * @return the externalId
     */
    public String getExternalId() {
        return externalId;
    }
    /**
     * @param externalId the externalId to set
     */
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
    /**
     * @return the externalIdType
     */
    public String getExternalIdType() {
        return externalIdType;
    }
    /**
     * @param externalIdType the externalIdType to set
     */
    public void setExternalIdType(String externalIdType) {
        this.externalIdType = externalIdType;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
    //-------------------------------------------------------------------------
    /**
     * Create a list of Identifier instances from raw Object list retrieved
     * from the database.
     * 
     * @param identifiers
     * @return List of Identifier instances
     */
    public static List createList(Object[] identifiers) throws SQLException {
            
       ArrayList identifierList = new ArrayList();
       
       for (int i=0; i<identifiers.length; i++) {
           
           try {
               
               // Convert object to underlying Oracle data STRUCT type
               oracle.sql.STRUCT struct = (oracle.sql.STRUCT)identifiers[i];
               
               Object atts[] = struct.getAttributes();
               IdentifierInstance ident = new IdentifierInstance();
               ident.setExternalId((String)atts[0]);
               BigDecimal bd = (BigDecimal)atts[1];
               String s = bd.toString();
               ident.setExternalIdType(s);
               ident.setDescription((String)atts[2]); 
               
               identifierList.add(ident);
               
           }
           catch (SQLException se) {
               throw se;
           }
           
       }
       
       
       return identifierList;
       
    }

}
