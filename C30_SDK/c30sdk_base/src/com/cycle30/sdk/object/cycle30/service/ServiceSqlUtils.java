package com.cycle30.sdk.object.cycle30.service;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;



/**
 * SQL utility class for Service related queries.
 * 
 */
public class ServiceSqlUtils {

    
    private static Logger log = Logger.getLogger(ServiceSqlUtils.class);
    public static C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
    
    

    // TO-DO - optize (outer joins, etc)
    
    private static final String CATALOG_TRANS_QUERY = 
            "select request_id, c30_transaction_id " + 
            "     from C30_PROV_CATALOG_TRANS " + 
            "    where telephone_number = ?";
    
    private static final String REQUEST_AUDIT__QUERY = 
            "select message_date, payload_received " +
            "  from c30audit.request_audit  " +
            " where message_id = ?"; 
    
    private static final String RESPONSE_AUDIT__QUERY = 
            "select message_date " +
            "  from c30audit.response_audit  " +
            " where message_id = ?"; 

    private static final String PROVISIONING_INSTANCE_QUERY = 
            "select a.sub_request_id, a.request_date, a.response_date, a.request_xml, b.status_name " + 
            "from c30_prov_trans_inst a, c30_prov_status_def b " + 
            "where a.request_id =? " + 
            "   and b.status_id = a.status_id";
    
    
    
    //-------------------------------------------------------------------------
    /**
     * Query the provision status.
     * 
     * @param serviceId

     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     */
    public synchronized static HashMap queryProvisioningStatus(String serviceId) 
                                          throws C30SDKObjectConnectionException, 
                                                  C30SDKInvalidAttributeException, 
                                                  SQLException,
                                                  Exception 
    {

        C30JDBCDataSource dataSource = null;

        HashMap<String, HashMap> results = new HashMap<String, HashMap>();
        
                  
        // Get the catalog transaction instance(s) that belong to the service id.
        String requestId  = "";
        String internalId = "";
        PreparedStatement ps = null;
        ResultSet resultSet  = null;        
        
        try {
            
            dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   
            ps = dataSource.getPreparedStatement(CATALOG_TRANS_QUERY);
            ps.setString(1, serviceId);
            resultSet  = ps.executeQuery();
            
            boolean serviceExists = false;
            int resultIndex = 0;
            
            // For each catalog instance, create a map of provisioning results.
            while (resultSet.next()) {
                
                serviceExists = true;
                requestId  = resultSet.getString(1);
                internalId = resultSet.getString(2);
                
                HashMap thisCatalogInst = new HashMap();
                thisCatalogInst.put("RequestId",  requestId);
                thisCatalogInst.put("InternalId", internalId);
                
                getProvisioningDetails(dataSource, thisCatalogInst);
                
                // Place this set of results in result map with 'N' key
                String key = Integer.toString(resultIndex++);
                results.put (key, thisCatalogInst);
                
            }
            
            // If nothing found, return exception
            if (serviceExists == false) {
                HashMap hm = new HashMap();
                hm.put("ExceptionCode", "CONN-017"); // Generic SDK not-found error code.
                hm.put("MsgDescr", "Service id " + serviceId + " not found in provisioning history tables.");
                results.put("0", hm);
            }  

    		
        } catch (SQLException se) {
           log.error(se);
           throw se;
        } catch(Exception e) {
			log.error("queryProvisioningStatus = "+e);
			throw e;
		}
		finally{
			try {
				if(resultSet != null) {
					resultSet.close();					
				}
				if(ps != null) {
					ps.close();					
				}
	            //return the connection to pool
	            C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(dataSource.getConnection());
			} catch (Exception e) {
				log.error("ERROR - queryProvisioningStatus = " +e);
				throw e;
			}
		}
    
        return results;
    }
    
    
    
    
    /**
     * 
     * @param catalogInst
     */
    private synchronized static void getProvisioningDetails(C30JDBCDataSource dataSource, 
                                                                HashMap catalogInst) 
                                               throws SQLException, Exception {
        
        
        String requestId  = (String)catalogInst.get("RequestId");
        String internalId = (String)catalogInst.get("InternalId");

        
        // For each catalog transaction, get the audit request/response and related 
        // provisioning instance sets of data
        
        String originalPayload = "";
        Date   orderRequestDate = null;
        Date   orderResponseDate = null;

            
        // Get request date/original payload
        PreparedStatement ps = dataSource.getPreparedStatement(REQUEST_AUDIT__QUERY);
        ps.setString(1, internalId);
        ResultSet resultSet  = ps.executeQuery();
        if (resultSet.next() != false) {
            orderRequestDate = resultSet.getDate(1);
            originalPayload = resultSet.getString(2);
        }  
        
        // get response date
        ps = dataSource.getPreparedStatement(REQUEST_AUDIT__QUERY);
        ps.setString(1, internalId);
        resultSet  = ps.executeQuery();
        if (resultSet.next() != false) {
            orderResponseDate = resultSet.getDate(1);
        } 
        
        // Convert dates
        String reqDate = "";
        String resDate = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        if (orderRequestDate != null) {
            reqDate = format.format(orderRequestDate);
        }
        if (orderResponseDate != null) {
            resDate = format.format(orderResponseDate);
        }
        
        // Store original request info in result map
        String requestXml = ""; 
        if (originalPayload != null) {
            requestXml = removeXmlHeader(originalPayload);
        }
        catalogInst.put("OriginalRequest",   requestXml);
        catalogInst.put("OrderRequestDate",  reqDate);
        catalogInst.put("OrderResponseDate", resDate);


        // Get all provisioning instance(s) associated with the request
        ps = dataSource.getPreparedStatement(PROVISIONING_INSTANCE_QUERY);
        ps.setString(1, requestId);
        resultSet  = ps.executeQuery();
        
        // For each instance found, populate a map with details using 
        // subrequest id as key. Add each to another HashMap with 
        // "ProvRequests" key.
        HashMap provRequests = new HashMap();
        while (resultSet.next()) {
            
            String subReqId    = resultSet.getString(1);
            Date   provReqDate = resultSet.getDate(2);
            Date   provResDate = resultSet.getDate(3);
            String provXml     = resultSet.getString(4);
            String status      = resultSet.getString(5);
            
            // Convert dates
            reqDate = "";
            resDate = "";
            format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            if (provReqDate != null) {
                reqDate = format.format(provReqDate);
            }
            if (provResDate != null) {
                resDate = format.format(provResDate);
            }
            
            requestXml = ""; 
            if (provXml != null) {
                requestXml = removeXmlHeader(provXml);
            }
            
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("SubRequestId",     subReqId);
            hm.put("ProvRequestDate",  reqDate);
            hm.put("ProvResponseDate", resDate);
            hm.put("ProvRequestXml",   requestXml);
            hm.put("ProvStatus",       status);
           
            String key = subReqId;
            provRequests.put(key, hm);
            
        }  
        
        // When finished, add prov requests to catalog request map.
        catalogInst.put("ProvRequests", provRequests);
        
        ps.close();
    }

    
    /**
     * Remove the XML header '<?xml version="1.0" encoding="utf-8"?>'
     * 
     * @param xmlPayload
     * @return modified payload
     */
    public static String removeXmlHeader(String xmlPayload) {
        
        String response = xmlPayload;
        
        if (xmlPayload == null) {
            return null;
        }
        
        int startIdx = xmlPayload.indexOf("<?");
        if (startIdx == -1) {
            return xmlPayload;
        }
        
        int endIdx = xmlPayload.indexOf(">", startIdx);
        if (endIdx == -1) {
            return xmlPayload;
        }
        
        String tmp = xmlPayload.substring(endIdx+1);
        
        return tmp;
    }
    
    

    

}