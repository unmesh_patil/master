package com.cycle30.sdk.object.cycle30.invoice;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * C30SDK core Invoice object 
 */
public class C30InvoiceCharge extends C30SDKObject implements Cloneable {

        private static Logger log = Logger.getLogger(C30InvoiceCharge.class);

        /**
         * Creates a new instance of C30InvoiceChargw.
         * 
         * @param factory the factory which is the parent of this object.
         * @param objectType Type of C30SDK payment ???
         * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
         * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
         * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
         * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
         * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
         */
        public C30InvoiceCharge(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
                super(factory, objectType);
        }



        /**
         * Initializes the base attributes 
         * 
         * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
         */
        @Override
        protected void initializeAttributes() throws C30SDKInvalidAttributeException{
            setClassFieldConfiguration();
        }

        
        
        /**
         * Creates a copy of the C30InvoiceCharge object.
         * 
         * @return A copy of the C30InvoiceCharge object
         */
        @Override
        public Object clone() {
                C30InvoiceCharge copy = (C30InvoiceCharge)super.clone();        
                return copy;        
        }

        
        /**
         * Method to process the C30InvoiceCharge.
         * 
         * @return C30SDKObject 
         * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
         */
        @Override
        public C30SDKObject process() {
            
            C30SDKObject c30Object = null;
            
            try {
               setAttributesFromNameValuePairs();

               String forceId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               Integer acctSegId = this.getAcctSegId();
               String  transId = (String)getAttributeValue("TransactionId", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               Integer  invoiceNumber = (Integer)getAttributeValue("InvoiceNumber", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              
               getInvoiceCharges(forceId, acctSegId, invoiceNumber ,transId);
               setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               
               // Set return object reference to this instance
               c30Object = this;

            } catch (C30SDKException e) {
                c30Object = this.createExceptionMessage(e);
            }

            return c30Object;
        }

        

        /**
         * Internal method to get the set of invoice charge.  
         * 
         * @param acctSegId 
         * @param forceId 
         * @throws C30SDKException 
         * @throws Exception 
         */
        private void getInvoiceCharges(String forceId, 
                                   Integer acctSegId, 
                                   Integer  invoiceNumber,
                                   String  transId) throws C30SDKException {

            try {
                
                C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId()); 
                C30ExternalCallDef call = new C30ExternalCallDef("c30getInvoiceCharges");

                // Input params
                call.addParam(new C30CustomParamDef("v_transaction_id", 2, 2000, 1));
                call.addParam(new C30CustomParamDef("v_acct_seg_id",    1, 2000, 1));
                call.addParam(new C30CustomParamDef("v_invoice_number", 1, 2000, 1));


                // Output params (mapped to output names in stored proc).
                call.addParam(new C30CustomParamDef("tracking_id",    1, 2000, 2));
                call.addParam(new C30CustomParamDef("charge_type",    2, 2000, 2));
                call.addParam(new C30CustomParamDef("charge_descr",   2, 2000, 2));
                call.addParam(new C30CustomParamDef("total_amount",   2, 2000, 2));
                call.addParam(new C30CustomParamDef("federal_amount", 2, 2000, 2));
                call.addParam(new C30CustomParamDef("state_amount",   2, 2000, 2));
                call.addParam(new C30CustomParamDef("county_amount",  2, 2000, 2));
                call.addParam(new C30CustomParamDef("city_amount",    2, 2000, 2));
                call.addParam(new C30CustomParamDef("other_amount",   2, 2000, 2));
                call.addParam(new C30CustomParamDef("units",          2, 2000, 2));
                call.addParam(new C30CustomParamDef("trans_date",     2, 2000, 2));
                call.addParam(new C30CustomParamDef("from_date",      2, 2000, 2));
                call.addParam(new C30CustomParamDef("to_date",        2, 2000, 2));
                 
                // Make db call using required params.
                ArrayList<Object> paramValues = new ArrayList<Object>();
                paramValues.add(transId);
                paramValues.add(acctSegId);
                //paramValues.add(forceId);
                paramValues.add(invoiceNumber);
                
                dataSource.queryData(call, paramValues, 2);
                
                int rowCount = dataSource.getRowCount();
                log.debug("Total row count = " + rowCount);
                
                // if no records found, throw 'not found' exception
                if (rowCount == 0) {
                    throw new Exception("C30INVCHRGCNOTFND No invoice data found");
                }
                
                // Populate 'this' object with first row, then add all others to the
                // 'c30sdkObjects' ArrayList via cloning this instance.
                if (rowCount > 0) {    
                    
                    int currCount = 0;
                    int startIdx = 0;
                    int endIdx   = rowCount;
                    
                    /*
                    // Set paging params
                    if (rowCount >= start) {
                        startIdx = start;
                    }
                    if ( (startIdx + size) < rowCount) {
                        endIdx = (startIdx + size);
                    }*/
                    
                    for (int i=startIdx; i<endIdx; i++) {
                    
                        C30InvoiceCharge sdkObj = this;
                        
                        if (currCount != 0) {
                            sdkObj = (C30InvoiceCharge)super.clone();
                        }
                        
                        sdkObj.setAttributeValue("TrackingId",         dataSource.getValueAt(i, 0),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("ChargeType",         dataSource.getValueAt(i, 1),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("ChargeDescription",  dataSource.getValueAt(i, 2),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("TotalAmount",        dataSource.getValueAt(i, 3),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("FederalAmount",      dataSource.getValueAt(i, 4),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("StateAmount",        dataSource.getValueAt(i, 5),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("CountyAmount",       dataSource.getValueAt(i, 6),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("CityAmount",         dataSource.getValueAt(i, 7),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("OtherAmount",        dataSource.getValueAt(i, 8),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("Units",              dataSource.getValueAt(i, 9),  C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("TransactionDate",    dataSource.getValueAt(i, 10), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("FromDate",           dataSource.getValueAt(i, 11), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("ToDate",             dataSource.getValueAt(i, 12), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("ForceId",            forceId,                      C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        sdkObj.setAttributeValue("TotalCount",         rowCount,                     C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

                        
                        // Add all rows after 1st row to collection of objects.  This is so toHashMap() can
                        // unmarshall the list of objects.
                        if (currCount > 0) {
                            addC30SDKObject(sdkObj);
                        }
                        currCount++;
                        
                    }
                }
            }
            catch(Exception e) {
                String errMsg = e.getMessage();
                String errCode = errMsg.substring(0, C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
                errMsg = errMsg.substring(C30SDKValueConstants.SQL_ERROR_CODE_LENGTH+1);
                log.error(errMsg);
                throw new C30SDKException(errMsg, e, errCode);
            }
                  
        }


        /**
         * Method which gets any data to be cached for further use by the object.
         */
        @Override
        protected void loadCache() {

        }

}
