package com.cycle30.sdk.object.cycle30.order;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.schema.jaxb.C30AccountObject;
import com.cycle30.sdk.schema.jaxb.C30OrderActionType;
import com.cycle30.sdk.schema.jaxb.C30PlanObject;
import com.cycle30.sdk.schema.jaxb.C30ServiceObject;
import com.cycle30.sdk.schema.jaxb.OrderRequest;
import com.cycle30.sdk.schema.jaxb.OrderRequest.ServiceObjectList;
import com.cycle30.sdk.toolkit.exception.C30SDKToolkitException;
import com.cycle30.sdk.util.C30DOMUtils;

/**
 * C30SDK C30Order 
 * @author Ranjith Nelluri
 */
public class C30Order extends C30SDKObject implements Cloneable {

	private C30SDKObject account = null;
	protected Map balance = null;
	private static Logger log = Logger.getLogger(C30Order.class);
	C30JDBCDataSource c30OrderDataSource = null;
	/**
	 * Creates a new instance of C30Order.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Order(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Order(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30Order.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Order object.
	 * @return A copy of the C30Order object
	 */
	@Override
	public Object clone() {
		C30Order copy = (C30Order) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30Order.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject sdkObj = null;
		try {
			log.info("In the C30Order Process ()");
			setAttributesFromNameValuePairs();

			c30OrderDataSource = C30SDKDataSourceUtils.getDataSourceFromOrderSDKPool();
			//This valdates the Input Validates the input attributes.
			validateAttributes();

			//If the input request is to create the Order Job.
			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.C30ORDER_CREATE)) {

				log.debug("Creating the Order in SDK...");
				String orderXmlString = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ORDER_XML, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				Integer acctSegId = this.getAcctSegId();
				//String[] schemaList = new String[]{C30SDKValueConstants.C30_ORDER_SCHEMA, C30SDKValueConstants.C30_PLAN_SCHEMA, C30SDKValueConstants.C30_ACCOUNT_SCHEMA};

				//Validatethe Order XML against the schema.
				//Document orderXMLDoc = C30DOMUtils.stringToDomAndValidate(orderXmlString,schmeaList);
				Document orderXMLDoc = C30DOMUtils.stringToDomAndValidate(orderXmlString,C30SDKValueConstants.C30_ORDER_SCHEMA);
				String orgName       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ORG_NAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				String orgId         = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLIENT_ORG_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				String transId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				String auditId       = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_AUDIT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

				// Serialize - Store the order XML in order SDK Database.
				serializeOrderXml(orderXMLDoc, acctSegId, orgId, orgName, transId, auditId);

				//Update the Order Dependency
				checkDependencyAmongPlansAndSubOrders();
				sdkObj = this;
			}

			setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		} catch (C30SDKException e) {
			log.error(e);
			sdkObj = this.createExceptionMessage(e);
		}
		catch (C30SDKToolkitException e) {

			log.error(e);
			sdkObj = this.createExceptionMessage(e);
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sdkObj = this.createExceptionMessage(e);
		}
		finally
		{
			//Close the data source and return the connection back to pool
			try {
				C30SDKDataSourceUtils.freeConnectionFromOrderSDKPool(c30OrderDataSource.getConnection());
			} catch (Exception e) {
				e.printStackTrace();
				log.error("ERROR - Order Processing = " +e);
				sdkObj = this.createExceptionMessage(e);
			}
		}		

		// Return self-reference
		return  sdkObj;
	}



	private void checkDependencyAmongPlansAndSubOrders() {
		// TODO Auto-generated method stub

	}

	/**
	 * This is to store the Order XML in the database. 
	 * @param orderXMLDoc
	 * @param orgName 
	 * @param orgId 
	 */
	private void serializeOrderXml(Document orderXMLDoc, 
			Integer acctSegId, 
			String orgId, 
			String orgName, 
			String transId,
			String auditId) throws C30SDKException {

		OrderRequest order;  
		try {
			//Use JAXB for the unmarshalling
			JAXBContext context = JAXBContext.newInstance(OrderRequest.class);
			Unmarshaller u = context.createUnmarshaller();

			order = (OrderRequest)u.unmarshal(orderXMLDoc);
			
			
			C30OrderSqlUtils.storeOrderObjectInDateBase(orgId, orgName,order, C30DOMUtils.documentToInputStream(orderXMLDoc), acctSegId, transId, auditId, c30OrderDataSource);
			log.info("Created order object in the database..");
			log.info("Org Name : "+ orgName);

			//Get the OrderWorkFlowMap
			ArrayList wpList = this.getFactory().orderWorkPointMapList;


			//---------------------------------------------------------------------
			// Rules for inserting sub-order rows (to satisfy Workpoint):
			//
			// 1. Only one row will ever be inserted.
			// 2. If adding account, create account add row
			// 3. If changing account w/o service, add account change row.
			// 4. If adding/changing service to existing account, add service row. 
			//---------------------------------------------------------------------


			// Put necessary properties in map for cleaner interface
			HashMap hm = new HashMap<String, Object>();

			hm.put("orgId",     orgId);
			hm.put("orgName",   orgName);
			hm.put("order",     order);
			hm.put("payload",   C30DOMUtils.documentToInputStream(orderXMLDoc));
			hm.put("wpList",    wpList);
			hm.put("acctSegId", acctSegId);
			hm.put("transId",   transId);
			hm.put("auditId",   auditId);


			// If the Account Object is populated then store the SubOrder in Database.
			boolean acctRowInserted = false;
			if (order.getAccount() != null  && insertAcctSubOrderRow(order)==true) {
				log.debug("Storing account suborder object into suborder table");
				C30OrderSqlUtils.storeAccountSubOrderInDataBase(hm, c30OrderDataSource);
				acctRowInserted = true;
			}


			if (order.getServiceObjectList()!=null && acctRowInserted==false)  {

				log.info(order.getServiceObjectList().getSize());

				if ((order.getServiceObjectList().getSize()).compareTo(new BigInteger("0")) > 0) {

					log.info("Working on the Service Objects");

					//Insert the first service in the list (just one service per order necessary) into the suborder table.
					List<C30ServiceObject> svcSubOrderList =   order.getServiceObjectList().getServiceObject();

					log.info("Total Number of Service Objects :" + svcSubOrderList.size());

					int idx=0;	
					for (C30ServiceObject service : svcSubOrderList) { 

						idx++;
						hm.put("service", service); 
						hm.put("serviceIdx", Integer.toString(idx));
						C30OrderSqlUtils.storeServiceSubOrderInDataBase(hm, c30OrderDataSource);
						break;

					} 

				}
			}

			log.info("Completed creating order and sub orders..");

			//Unlock the order, so it can picked up by Order Manager
			log.info("Unlock the order to pickup by Order Manager..");
			String orderId = order.getClientOrderId().getValue();
			C30OrderSqlUtils.updateOrderLockUnlockStatus(orderId, transId, C30SDKValueConstants.ORD_NOT_LOCKED, c30OrderDataSource);

		}
		catch(C30SDKException e)
		{
			log.error(e);
			throw e;
		}
		catch(Exception e)
		{ 
			log.error(e);
			throw new C30SDKInvalidAttributeException(this.getFactory().exceptionResourceBundle.getString("CONN-031"),e,"CONN-031");
		}

	}


	/**
	 * 
	 * @param order
	 * @return true if need to insert an Account sub-order row
	 */
	private boolean insertAcctSubOrderRow(OrderRequest order) {


		//---------------------------------------------------------------------
		// Rules for inserting account sub-order rows
		//
		// If adding account, create account add row
		// 3. If changing account w/o service, add account change row.
		// 4. If adding/changing service to existing account, add service row.
		//---------------------------------------------------------------------

		C30AccountObject account = order.getAccount();
		if (account == null) {
			return false;
		}

		// Always create account row if adding new account
		C30OrderActionType action = account.getClientActionType();
		if (action.equals(C30OrderActionType.ADD) ) {
			return true;
		}

		// If changing account w/o service, add account row
		ServiceObjectList serviceList = order.getServiceObjectList();
		if (serviceList == null || serviceList.getSize().intValue() == 0){
			return true;
		}

		return false;

	}



	/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 

	}


	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}