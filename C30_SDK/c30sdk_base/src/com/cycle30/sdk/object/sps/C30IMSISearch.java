
package com.cycle30.sdk.object.sps;

import java.io.IOException;
import java.util.HashMap;
import org.apache.log4j.Logger;

//Provision Objects
import com.cycle30.prov.data.Constants;
import com.cycle30.prov.exception.C30ProvConnectorException;
import com.cycle30.prov.request.C30ProvisionRequest;
import com.cycle30.prov.utils.C30ProvConnectorUtils;

//SDK Objects
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;



/**
 * @author Umesh
 * This object is used to work on SPS calls.
 * 
 */

public class C30IMSISearch extends C30SDKObject implements Cloneable {

	private static Logger log = Logger.getLogger(C30IMSISearch.class);
	

	/**
	 * Creates a new instance of C30IMSISearch.
	 * @param factory Factory used to create C30IMSISearch objects
	 * @param objectType Type of C30SDK object
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 * @throws C30SDKInvalidConfigurationException if there is invalid configuration which causes this account to fail during initialization.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30IMSISearch(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}


	@Override
	protected final void initializeAttributes()
	throws C30SDKInvalidAttributeException {

		setClassFieldConfiguration();

	}

    /**
     * Creates a copy of the C30IMSISearch object.
     * 
     * @return A copy of the C30IMSISearch object
     */
    @Override
    public Object clone() {
    	C30IMSISearch copy = (C30IMSISearch)super.clone();        
            return copy;        
    }
    
    
	@Override
	protected void loadCache() throws C30SDKCacheException {
		// TODO Auto-generated method stub

	}

	@Override
	public C30SDKObject process() throws C30SDKException 
	{
		C30SDKObject sdkObj = null;
		String acctsegId = null;
		String serialNumber = null;
		String transId = null;
		
		try{
				log.info("IMSI search process initialized ");
		        acctsegId = this.getAcctSegId().toString();
		        log.info("acctsegId =  " +acctsegId);

	            if (acctsegId == null) {
	                throw new C30SDKException("SIMInventory - Could not retrieve segmentation id from SDKObject ");
	            }
		        
		        transId   = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_SPS_TRANSACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            serialNumber = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERIAL_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            log.info("transId =  " +transId + "serialNumber =  " +serialNumber);	               

	            //GET NetworkId from SPS
	            sdkObj = getIMSI(acctsegId, serialNumber, transId);
	            
				//if we got an IMSI rather than an exception
				if (sdkObj instanceof C30IMSISearch) {
					log.info("Found the Instance of C30IMSISearch");
					this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				sdkObj = this;	            
		} catch (C30SDKException e) {
		    log.error(e);
			sdkObj = this.createExceptionMessage(e);
		} catch (Exception e) {
		    log.error(e);
		    sdkObj = this.createExceptionMessage(e);
		}
		return sdkObj;
	}

    /**
     * Internal method to invoke SPS.  
     * 
     * @param acctsegId 
     * @param serialNumber 
     * @param transId
     * @throws Exception
     *  
     */
    	
	private C30SDKObject getIMSI(String acctsegId, 
                String  serialNumber, String  transId) throws C30SDKException {
                  
		HashMap<String, String> ProvInquiry = new HashMap<String, String>();
		
		String ntwrkid = null;		
		C30IMSISearch spssdkObj = this;
		
        try {
        	
				//IMSI search process initialized.		        
	            log.info("IMSI search process initialized for " + "transId = " + transId + "acctsegId : "+acctsegId + "serialNumber : "+serialNumber);
	
				//Get the IMSI response from SPS 
				ProvInquiry.put(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID,acctsegId);
				ProvInquiry.put(Constants.ICCID,serialNumber);
				ProvInquiry.put(Constants.EXTERNAL_TRANSACTION_ID,transId);				
				ProvInquiry.put(Constants.ATTR_REQUEST_NAME, Constants.REQUEST_VALIDATE_SIM);		
				
				C30ProvisionRequest IMSIrequest = C30ProvisionRequest.getInstance();
				HashMap IMSIresponse = IMSIrequest.awnInquiryRequest(ProvInquiry) ;
				
				log.info("Response XML: "+IMSIresponse.get("ResponseXML").toString());
				log.info("IMSI Retreived: "+IMSIresponse.get(Constants.IMSI).toString());
				
				//Get SIM Status
				String retString = C30ProvConnectorUtils.getXmlStringElementValue(IMSIresponse.get("ResponseXML").toString(), 
						C30SDKValueConstants.C30_PROV_SPS_PREPAID_VALUE);
				if (!retString.equalsIgnoreCase(C30SDKValueConstants.C30_PROV_SPS_PREPAID_TRUE)){
	            	String errMsg = "Prepaid Return Value from SPS is "+retString + " Cannot Continue";
	    			throw new C30SDKException(errMsg, "SDK-SPS-001");	
				}
				
				// See if NO error occurred.
				ntwrkid = IMSIresponse.get("IMSI").toString();
				spssdkObj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERIAL_NUMBER, serialNumber, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				spssdkObj.setAttributeValue(C30SDKAttributeConstants.ATTRIB_NETWORK_ID, ntwrkid, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);					

				return spssdkObj;
				
        }
        catch (C30ProvConnectorException e) {
        	log.error("PROV Exception - IMSI Search : " +e);
            throw new C30SDKException(e.getMessage(), e, e.getExceptionCode());
        }
        catch(C30SDKException e) {
        	log.error("SDK Exception - IMSI Search : " +e);        	
            throw new C30SDKException(e.getMessage(), e, e.getExceptionCode());
        }        
        catch(Exception e) {
        	log.error("Exception - IMSI Search : " +e);        	
            String errMsg = e.getMessage();
            String errCode = errMsg.substring(0, C30SDKValueConstants.SQL_ERROR_CODE_LENGTH);
            throw new C30SDKException(errMsg, e, errCode);
        }
       
    }

}
