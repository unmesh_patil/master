package com.cycle30.sdk.object;

public class C30SDKAttributeConstants {


	/** Attributes of the C30SDK object class. */
	public static final String ATTRIB_OBJECT_TYPE = "ObjectType";
	public static final String ATTRIB_OBJECT_PARENT = "ObjectParent";
	public static final String ATTRIB_IS_PROCESSED = "IsProcessed";
	public static final String ATTRIB_PERSIST_REQUEST_APP = "PersistRequestApp";
	public static final String ATTRIB_PERSIST_REQUEST_DATE = "PersistRequestDate";
	public static final String ATTRIB_PERSIST_EXTERNAL_ID = "PersistExternalId";
	public static final String ATTRIB_PERSIST_INTERNAL_ID = "PersistInternalId";
	public static final String ACCOUNT_SERVER_ID_CACHE = "ACCOUNT_SERVER_ID_CACHE";


	public static final String ATTRIB_FX_VERSION = "FXVersion";
	public static final String ATTRIB_PS_VERSION = "PSVersion";
	public static final String ATTRIB_SDKAPI_VERSION = "C30SdkApiVersion";	
	public static final String ATTRIB_USER_ID = "UserId";
	public static final String ATTRIB_COMPONENT_MEMBER_MAX_SIZE = "ComponentMemberMaxSize";
	public static final String ATTRIB_COMPONENT_MEMBER_FORCE_GET = "ComponentMemberForceGet";
	
	

	//APITS GeoCode Object
	public static final String ATTRIB_GEOCODE_CITY = "City";
	public static final String ATTRIB_GEOCODE_COUNTY = "County";
	public static final String ATTRIB_GEOCODE_STATE = "State";
	public static final String ATTRIB_GEOCODE_GEOCODE = "Geocode";
	public static final String ATTRIB_GEOCODE_GEOCODE_LIST = "GeoCodeList";
	public static final String ATTRIB_GEOCODE_TOTALCOUNT = "TotalCount";
	public static final String ATTRIB_GEOCODE_ZIP = "Zip";
	public static final String ATTRIB_GEOCODE_COUNTRYCODE = "CountryCode";
	public static final String ATTRIB_GEOCODE_FRANCHISECODE = "FranchiseCode";
	
	
	
	//APITS Account Object
	public static final String ATTRIB_SERVER_CATEGORY = "ServerCategory";
	public static final String ATTRIB_BILLING_SERVICE_CENTER_ID = "BillingServiceCenterId";
	public static final String ATTRIB_REMIT_SERVICE_CENTER_ID = "RemitServiceCenterId";
	public static final String ATTRIB_INQUIRY_SERVICE_CENTER_ID = "InquiryServiceCenterId";
	public static final String ATTRIB_COLLECTION_SERVICE_CENTER_ID = "CollectionServiceCenterId";
	public static final String ATTRIB_PRINT_SERVICE_CENTER_ID = "PrintServiceCenterId";
	public static final String ATTRIB_PARENT_ID = "ParentId";
	public static final String ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID = "ParentAccountExternalId";
	public static final String ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID_TYPE = "ParentAccountExternalIdType";
	public static final String ATTRIB_ACCT_SEG_ID = "AcctSegId";
	public static final String ATTRIB_BILL_COMPANY = "BillCompany";
	public static final String ATTRIB_BILL_FNAME = "BillFname";
	public static final String ATTRIB_BILL_LNAME = "BillLname";
	public static final String ATTRIB_BILL_ADDRESS_1 = "BillAddress1";
	public static final String ATTRIB_BILL_CITY = "BillCity";
	public static final String ATTRIB_BILL_COUNTY = "BillCounty";
	public static final String ATTRIB_BILL_STATE = "BillState";
	public static final String ATTRIB_BILL_GEOCODE = "BillGeocode";
	public static final String ATTRIB_RATE_CLASS_DEFAULT = "RateClassDefault";
	public static final String ATTRIB_LANGUAGE_CODE = "LanguageCode";
	

	//Extended Data Attributes
	public static final String EXT_DATA_BASE_TABLE = "CMF";
	public static final String ATTRIB_EXTENDED_DATA = "ExtendedData";
	public static final String ATTRIB_PARAM_ID = "ParamId";
	public static final String ATTRIB_PARAM_VALUE = "ParamValue";

	//XML Datasource Attributes
	public static final String XML_TAG_ROOT = "Transactions";
	public static final String XML_TAG_TRANSACTION = "Transaction";
	public static final String XML_TAG_TRANSACTION_NAME = "name";
	public static final String XML_TAG_ATTR_NAME = "name";
	public static final String XML_TAG_ATTR_VALUE = "value";
	public static final String XML_FACTORY_ATTRIB_USERNAME = "TransUser";
	public static final String XML_FACTORY_ATTRIB_PASSWORD = "TransPassword";
	public static final String XML_FACTORY_ATTRIB_USER_REALM = "TransUserRealm";
	public static final String XML_FACTORY_ATTRIB_VERSION = "TransFactoryVersion";
	public static final String XML_ATTRIB_TRANS_ACTION = "TransAction";
	public static final String XML_ATTRIB_REQUEST_APP = "TransRequestApp";
	public static final String XML_ATTRIB_REQUEST_DATE = "TransRequestDate";
	public static final String XML_ATTRIB_EXTERNAL_ID = "TransExternalId";
	public static final String ACTION_PROCESS = "process";
	
	//AccountNote Attribute Names
	public static final String ATTRIB_ACCT_NOTE_CHG_DATE = "ChgDate";
	public static final String ATTRIB_ACCT_NOTE_CHG_WHO = "ChgWho";
	private static final String ATTRIB_ACCT_NOTE_CODE = "Code";
	public static final String ATTRIB_ACCT_NOTE_COMMENTS = "Comments";
	public static final String ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID = "EquipExternalId";
	public static final String ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID_FIND = "EquipExternalIdFind";
	public static final String ATTRIB_ACCT_NOTE_LANGUAGE_CODE = "LanguageCode";
	public static final String ATTRIB_ACCT_NOTE_NOTE_CODE = "NoteCode";
	public static final String ATTRIB_ACCT_NOTE_NOTE_API_CODE = "Code";
	public static final String ATTRIB_ACCT_NOTE_NOTE_SOURCE = "NoteSource";
	public static final String ATTRIB_ACCT_NOTE_NOTE_TEXT = "NoteText";
	public static final String ATTRIB_ACCT_NOTE_PERMANENT_FLAG = "PermanentFlag";
	
	public static final String ATTRIB_ACCOUNT = "Account";
	public static final String ATTRIB_ACCOUNT_SERVER_ID = "AccountServerId";
	public static final String ATTRIB_ACCOUNT_INTERNAL_ID = "AccountInternalId";
	public static final String ATTRIB_ACCOUNT_EXTERNAL_ID = "AccountExternalId";
	public static final String ATTRIB_ACCOUNT_BILL_FNAME = "BillFname";
	public static final String ATTRIB_ACCOUNT_BILL_LNAME = "BillLname";
	public static final String ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE = "AccountExternalIdType";
	public static final String ATTRIB_PARENT_ACCOUNT_INTERNAL_ID = "ParentAccountInternalId";
	public static final String ATTRIB_SERVICE_INTERNAL_ID = "ServiceInternalId";
	public static final String ATTRIB_SERVICE_INTERNAL_ID_RESETS = "ServiceInternalIdResets";
	public static final String ATTRIB_SERVICE_EXTERNAL_ID = "ServiceExternalId";
	public static final String ATTRIB_SERVICE_EXTERNAL_ID_TYPE = "ServiceExternalIdType";
    public static final String ATTRIB_ACTIVE_DATE = "ActiveDate";    
    public static final String ATTRIB_NEW_ACCOUNT_EXTERNAL_ID = "NewAccountExternalId";
    public static final String ATTRIB_NEW_ACCOUNT_EXTERNAL_ID_TYPE = "NewAccountExternalIdType";
    public static final String ATTRIB_TENDER_TYPE = "TENDERTYPE";


    public static final String ATTRIB_CLIENT_ID = "ClientId";
    public static final String ATTRIB_WP_JOB_ID = "WpJobId";
    public static final String ATTRIB_WP_JOB_IMAGE = "JobImage";
	public static final String ATTRIB_BULK_ORDER_JOB_DATA = "BulkOrderJobData";
    public static final String ATTRIB_BULK_JOB_ID = "BulkOrderJobId";
    public static final String ATTRIB_BULK_JOB_TYPE = "BulkOrderJobType";

    
	public static final String ATTRIB_FIND = "Find";
	public static final String ATTRIB_ITEM_ACTION_ID = "ItemActionId";
	public static final String ATTRIB_ORDER_XML = "OrderXML";
	public static final String ATTRIB_CLIENT_ORG_NAME = "ClientOrgName";
	public static final String ATTRIB_CLIENT_ORG_ID = "ClientOrgId";
	public static final String ATTRIB_TRANSACTION_ID = "TransactionId";
	public static final String ATTRIB_PAYLOAD = "Payload";
	public static final String ATTRIB_AUDIT_ID = "InternalAuditId";
	       
	public static final String ATTRIB_ACTION_CODE = "ActionCode";
	public static final String ATTRIB_ARCH_FLAG = "ArchFlag";
	public static final String ATTRIB_ALT_BANK_ACC_NUM = "AltBankAccNum";
	public static final String ATTRIB_AVS_ADDRESS_ID = "AvsAddressId";
	public static final String ATTRIB_AVS_RESPONSE_CODE = "AvsResponseCode";
	public static final String ATTRIB_BANK_ACCOUNT_TYPE = "BankAccountType";
	public static final String ATTRIB_BANK_AGENCY_CODE = "BankAgencyCode";
	public static final String ATTRIB_BANK_AGENCY_NAME = "BankAgencyName";
	public static final String ATTRIB_BANK_CODE = "BankCode";
	public static final String ATTRIB_BATCH_ID = "BatchId";
	public static final String ATTRIB_BATCH_ID_SERV = "BatchIdServ";
	public static final String ATTRIB_BILL_ORDER_NUMBER = "BillOrderNumber";
	public static final String ATTRIB_BILL_REF_NO = "BillRefNo";
	public static final String ATTRIB_BILL_REF_RESETS = "BillRefResets";
	public static final String ATTRIB_BILL_COMPANY_TAX_ID = "BillCompanyTaxId";
	public static final String ATTRIB_BRANCH_CODE = "BranchCode";
	public static final String ATTRIB_BRANCH_NAME = "BranchName";
	public static final String ATTRIB_CARD_ACCOUNT = "CardAccount";
	public static final String ATTRIB_CARD_CARRIER = "CardCarrier";
	public static final String ATTRIB_CARD_EXPIRE = "CardExpire";
	public static final String ATTRIB_CCARD_ACCOUNT = "CcardAccount";
	public static final String ATTRIB_CCARD_CARRIER = "CcardCarrier";
	public static final String ATTRIB_CCARD_EXPIRE = "CcardExpire";
	public static final String ATTRIB_CCARD_ID_SERV = "CcardIdServ";
	public static final String ATTRIB_CCARD_OWNR_NAME = "CcardOwnrName";
	public static final String ATTRIB_CHG_DATE = "ChgDate";
	public static final String ATTRIB_CLEAR_ACCOUNT_BALANCE = "ClearAccountBalance";
	public static final String ATTRIB_CLEARING_HOUSE_ID = "ClearingHouseId";
	public static final String ATTRIB_CR_NOTE_BILL_REF_NO = "CrNoteBillRefNo";
	public static final String ATTRIB_CR_NOTE_BILL_REF_RESETS = "CrNoteBillRefResets";
	public static final String ATTRIB_CUST_BANK_ACC_NUM = "CustBankAccNum";
	public static final String ATTRIB_CUST_BANK_ACCT_TYPE = "CustBankAcctType";
	public static final String ATTRIB_CUST_BANK_SORT_CODE = "CustBankSortCode";
	public static final String ATTRIB_CUST_BILL_ADDRESS = "CustBillAddress";
	public static final String ATTRIB_CUST_BILL_CITY = "CustBillCity";
	public static final String ATTRIB_CUST_BILL_COUNTRY_CODE = "CustBillCountryCode";
	public static final String ATTRIB_CUST_BILL_STATE = "CustBillState";
	public static final String ATTRIB_CUST_COMPANY_NAME = "CustCompanyName";
	public static final String ATTRIB_CUST_BILL_ZIP = "CustBillZip";
	public static final String ATTRIB_CUST_EMAIL = "CustEmail";
	public static final String ATTRIB_CUST_PHONE = "CustPhone";
	public static final String ATTRIB_CYCLICAL_BILL_USED = "CyclicalBillUsed";
	public static final String ATTRIB_CURRENCY_CODE = "CurrencyCode";
	public static final String ATTRIB_DISCOUNT_ID = "DiscountId";
	public static final String ATTRIB_DISTRIBUTED_AMOUNT = "DistributedAmount";
	public static final String ATTRIB_DISTRIBUTED_GL_AMOUNT = "DistributedGlAmount";
	public static final String ATTRIB_DRIVER_LICENSE_NUM = "DriverLicenseNum";
	public static final String ATTRIB_DRIVER_LICENSE_STATE = "DriverLicenseState";
	public static final String ATTRIB_EPG_TRANSACTION_ID = "EpgTransactionId";
	public static final String ATTRIB_EXTERNAL_AMOUNT = "ExternalAmount";
	public static final String ATTRIB_EXTERNAL_CURRENCY = "ExternalCurrency";
	public static final String ATTRIB_FILE_ID = "FileId";
	public static final String ATTRIB_GL_AMOUNT = "GlAmount";
	public static final String ATTRIB_INACTIVE_DATE = "InactiveDate";
	public static final String ATTRIB_IS_CURRENT = "IsCurrent";
	public static final String ATTRIB_IS_DEFAULT = "IsDefault";
	public static final String ATTRIB_IS_TEMPORARY = "IsTemporary";
	public static final String ATTRIB_MANUAL_CCAUTH_CODE = "ManualCcauthCode";
	public static final String ATTRIB_MANUAL_CCAUTH_DATE = "ManualCcauthDate";
	public static final String ATTRIB_MICR_BANK_ID = "MicrBankId";
	public static final String ATTRIB_MICR_CHECK_NUM = "MicrCheckNum";
	public static final String ATTRIB_MICR_DBA_NUM = "MicrDdaNum";
	public static final String ATTRIB_NEW_CUST_BANK_SORT_CODE = "NewCustBankSortCode";
	public static final String ATTRIB_NON_REALTIME_ONLY = "NonRealtimeOnly";
	public static final String ATTRIB_NO_BILL = "NoBill";
	public static final String ATTRIB_OPEN_ITEM_ID = "OpenItemId";
	public static final String ATTRIB_ORIG_BILL_REF_NO = "OrigBillRefNo";
	public static final String ATTRIB_ORIG_BILL_REF_RESETS = "OrigBillRefResets";
	public static final String ATTRIB_ORIG_TRACKING_ID = "OrigTrackingId";
	public static final String ATTRIB_ORIG_TRACKING_ID_SERV = "OrigTrackingIdServ";
	public static final String ATTRIB_OWNR_FNAME = "OwnrFname";
	public static final String ATTRIB_OWNR_LNAME = "OwnrLname";
	public static final String ATTRIB_PAYMENT_PROFILE_ID = "PaymentProfileId";
	public static final String ATTRIB_PAY_METHOD = "PayMethod";
	public static final String ATTRIB_POST_DATE = "PostDate";
	public static final String ATTRIB_REALTIME_INDICATOR = "RealtimeIndicator";
	public static final String ATTRIB_RESPONSE_CODE = "ResponseCode";
	public static final String ATTRIB_SOURCE_ID = "SourceId";
	public static final String ATTRIB_SOURCE_ID_SERV = "SourceIdServ";
	public static final String ATTRIB_SOURCE_TYPE = "SourceType";
	public static final String ATTRIB_STATUS = "Status";
	public static final String ATTRIB_TAX_TYPE_CODE = "TaxTypeCode";
	public static final String ATTRIB_TRACKING_ID = "TrackingId";
	public static final String ATTRIB_TRACKING_ID_SERV = "TrackingIdServ";
	public static final String ATTRIB_TRANS_AMOUNT = "TransAmount";
	public static final String ATTRIB_TRANS_CATEGORY = "TransCategory";
	public static final String ATTRIB_TRANS_DATE = "TransDate";
	public static final String ATTRIB_TRANS_FLAG = "TransFlag";
	public static final String ATTRIB_TRANS_SOURCE = "TransSource";
	public static final String ATTRIB_TRANS_SUBMITTER = "TransSubmitter";
	public static final String ATTRIB_TRANS_TYPE = "TransType";
	public static final String ATTRIB_TRANS_TYPE_CREDIT = "TransTypeCredit";
	public static final String ATTRIB_TRANS_TYPE_DEBIT = "TransTypeDebit";

	//Account Balance 
	public static final String ATTRIB_WRITEOFF_BALANCE = "WriteOffBalance";

	// Deposit
	public static final String ATTRIB_DATE_RECEIVED = "DateReceived";
	public static final String ATTRIB_DATE_RETURNED = "DateReturned";
	public static final String ATTRIB_DEPOSIT_AMOUNT = "DepositAmount";
	public static final String ATTRIB_DEPOSIT_TYPE = "DepositType";
	public static final String ATTRIB_INCOME_TAX_AMOUNT = "IncomeTaxAmount";
	public static final String ATTRIB_INTEREST_AMOUNT = "InterestAmount";
	public static final String ATTRIB_REASON_CODE = "ReasonCode";
	public static final String ATTRIB_REFUND_TYPE = "RefundType";
	public static final String ATTRIB_SERVER_ID = "ServerId";
	
	// These are the attributes for the ADJUSTMENT portion of the adjustment.
	public static final String ATTRIB_ADJ_REASON_CODE = "AdjReasonCode";
	public static final String ATTRIB_AMOUNT_ADJUSTMENT = "AmountAdjustment";
	public static final String ATTRIB_CHG_WHO = "ChgWho";
	public static final String ATTRIB_AMOUNT_USAGE = "AmountUsage";
	public static final String ATTRIB_ANNOTATION_ADJUSTMENT = "AnnotationAdjustment";
	public static final String ATTRIB_EFFECTIVE_DATE = "EffectiveDate";
	public static final String ATTRIB_FRAUD_INDICATOR = "FraudIndicator";
	public static final String ATTRIB_REQUEST_STATUS = "RequestStatus";
	public static final String ATTRIB_PRIMARY_UNITS_TYPE = "PrimaryUnitsType";
	public static final String ATTRIB_PRIMARY_UNITS_ADJUSTMENT = "PrimaryUnitsAdjustment";
	public static final String ATTRIB_TRANS_CODE = "TransCode";
	public static final String ATTRIB_COMB_TAX_RATE = "CombTaxRate";
	public static final String ATTRIB_LAST_REVIEWED_NAME = "LastReviewedName";
	public static final String ATTRIB_RATED_FLAG = "RatedFlag";
	public static final String ATTRIB_TRACKING_ID_COUNTER = "TrackingIdCounter";
	public static final String ATTRIB_SUPERVISOR_NAME = "SupervisorName";
	public static final String ATTRIB_PENDING_FLAG = "PendingFlag";
	public static final String ATTRIB_ORIG_MSG_ID = "OrigMsgId";
	public static final String ATTRIB_DISCOUNT_AMT = "DiscountAmt";
	public static final String ATTRIB_ORIG_MSG_ID2 = "OrigMsgId2";
	public static final String ATTRIB_ORIG_TYPE = "OrigType";
	public static final String ATTRIB_ORIG_SUBTYPE = "OrigSubtype";
	public static final String ATTRIB_ORIG_MSG_ID_SERV = "OrigMsgIdServ";
	public static final String ATTRIB_ORIG_TRANS_CODE = "OrigTransCode";
	public static final String ATTRIB_CURRENT_FLAG = "CurrentFlag";
	public static final String ATTRIB_TOTAL_AMT = "TotalAmt";
	public static final String ATTRIB_TRANSACT_DATE = "TransactDate";
	public static final String ATTRIB_REVIEW_DATE = "ReviewDate";
	public static final String ATTRIB_TAX_PKG_INST_ID = "TaxPkgInstId";
	public static final String ATTRIB_ORIG_BILL_INVOICE_ROW = "OrigBillInvoiceRow";
	public static final String ATTRIB_ORIG_SUBMITTER_NAME = "OrigSubmitterName";
	public static final String ATTRIB_ORIG_SPLIT_ROW_NUM = "OrigSplitRowNum";
	public static final String ATTRIB_ORIG_PROVIDER_ID = "OrigProviderId";

	// These are the attributes for the KEY part of the BILLED/UNBILLED USAGE portion of the adjustment.
	public static final String ATTRIB_MSG_ID = "MsgId";
	public static final String ATTRIB_MSG_ID2 = "MsgId2";
	public static final String ATTRIB_MSG_ID_SERV = "MsgIdServ";
	public static final String ATTRIB_SPLIT_ROW_NUM = "SplitRowNum";
	public static final String ATTRIB_SERVICE = "Service";
	
	// These are the attributes for the UNBILLED/BILLED USAGE portion of the adjustment.
	public static final String ATTRIB_ACCESS_REGION_ORIGIN = "AccessRegionOrigin";
	public static final String ATTRIB_ACCESS_REGION_TARGET = "AccessRegionTarget";
	public static final String ATTRIB_ADD_IMPLIED_DECIMAL = "AddImpliedDecimal";
	public static final String ATTRIB_AGGR_USAGE_ID = "AggrUsageId";
	public static final String ATTRIB_AMOUNT_REDUCTION = "AmountReduction";
	public static final String ATTRIB_AMOUNT_REDUCTION_ID = "AmountReductionId";
	public static final String ATTRIB_ANNOTATION_USAGE = "AnnotationUsage";
	public static final String ATTRIB_BASE_AMT = "BaseAmt";
	public static final String ATTRIB_BILL_AGGR_LEVEL = "BillAggrLevel";
	public static final String ATTRIB_BILL_CLASS = "BillClass";
	public static final String ATTRIB_BILLING_UNITS_TYPE = "BillingUnitsType";
	public static final String ATTRIB_CCARD_ID = "CcardId";
	public static final String ATTRIB_CDR_DATA_PARTITION_KEY = "CdrDataPartitionKey";
	public static final String ATTRIB_CDR_STATUS = "CdrStatus";
	public static final String ATTRIB_CELL_ID_ORIGIN = "CellIdOrigin";
	public static final String ATTRIB_COMP_STATUS = "CompStatus";
	public static final String ATTRIB_COMPONENT_ID = "ComponentId";
	public static final String ATTRIB_CONSOLIDATE_USAGE = "ConsolidateUsage";
	public static final String ATTRIB_CORRIDOR_PLAN_ID = "CorridorPlanId";
	public static final String ATTRIB_COUNTRY_CODE_ORIGIN = "CountryCodeOrigin";
	public static final String ATTRIB_COUNTRY_CODE_TARGET = "CountryCodeTarget";
	public static final String ATTRIB_COUNTRY_DIAL_CODE_ORIGIN = "CountryDialCodeOrigin";
	public static final String ATTRIB_CUSTOMER_TAG = "CustomerTag";
	public static final String ATTRIB_DERIVE_DISTANCE_UNITS = "DeriveDistanceUnits";
	public static final String ATTRIB_DERIVE_JURISDICTION = "DeriveJurisdiction";
	public static final String ATTRIB_DESCRIPTION_CODE_USAGE = "DescriptionCodeUsage";
	public static final String ATTRIB_DISTANCE_UNITS_INDICATOR = "DistanceUnitsIndicator";
	public static final String ATTRIB_DURATION_FLAG = "DurationFlag";
	public static final String ATTRIB_ELEMENT_ID = "ElementId";
	public static final String ATTRIB_EQUIP_CLASS_CODE = "EquipClassCode";
	public static final String ATTRIB_EXT_TRACKING_ID = "ExtTrackingId";
	public static final String ATTRIB_EXTERNAL_ID = "ExternalId";
	public static final String ATTRIB_EXTERNAL_ID_TYPE = "ExternalIdType";
	public static final String ATTRIB_FILE_ID_SERV = "FileIdServ";
	public static final String ATTRIB_FOREIGN_AMOUNT = "ForeignAmount";
	public static final String ATTRIB_FREE_USG = "FreeUsg";
	public static final String ATTRIB_GEOCODE = "Geocode";
	public static final String ATTRIB_GUIDE_TO = "GuideTo";
	public static final String ATTRIB_GUIDE_TO_PROVIDER = "GuideToProvider";
	public static final String ATTRIB_IS_PRERATED = "IsPrerated";
	public static final String ATTRIB_JURISDICTION = "Jurisdiction";
	public static final String ATTRIB_KEEP_RUNNING_TOTAL = "KeepRunningTotal";
	public static final String ATTRIB_MIN_BILLING_UNITS = "MinBillingUnits";
	public static final String ATTRIB_NETWORK_DELAY_BEFORE = "NetworkDelayBefore";
	public static final String ATTRIB_NETWORK_DELAY_AFTER = "NetworkDelayAfter";
	public static final String ATTRIB_NUM_RECORDS = "NumRecords";
	public static final String ATTRIB_ORIG_TYPE_ID_USG = "OrigTypeIdUsg";
	public static final String ATTRIB_ORIGIN_COUNTRY_DIAL_CODE_REQ = "OriginCountryDialCodeReq";
	public static final String ATTRIB_POINT_CATEGORY = "PointCategory";
	public static final String ATTRIB_POINT_CLASS_ORIGIN = "PointClassOrigin";
	public static final String ATTRIB_POINT_CLASS_TARGET = "PointClassTarget";
	public static final String ATTRIB_POINT_ID_ORIGIN = "PointIdOrigin";
	public static final String ATTRIB_POINT_ID_TARGET = "PointIdTarget";
	public static final String ATTRIB_POINT_ORIGIN = "PointOrigin";
	public static final String ATTRIB_POINT_TARGET = "PointTarget";
	public static final String ATTRIB_POINT_TAX_CODE_ORIGIN = "PointTaxCodeOrigin";
	public static final String ATTRIB_POINT_TAX_CODE_TARGET = "PointTaxCodeTarget";
	public static final String ATTRIB_POINT_TAX_CODE_TYPE_ORIGIN = "PointTaxCodeTypeOrigin";
	public static final String ATTRIB_POINT_TAX_CODE_TYPE_TARGET = "PointTaxCodeTypeTarget";
	public static final String ATTRIB_PRIMARY_UNITS_USAGE = "PrimaryUnitsUsage";
	public static final String ATTRIB_PRODUCT_LINE_ID = "ProductLineId";
	public static final String ATTRIB_PROVIDER_ID = "ProviderId";
	public static final String ATTRIB_RATABLE_UNIT_CLASS = "RatableUnitClass";
	public static final String ATTRIB_RATE_CLASS = "RateClass";
	public static final String ATTRIB_RATE_CURRENCY_CODE = "RateCurrencyCode";
	public static final String ATTRIB_RATE_CURRENCY_LOCATION = "RateCurrencyLocation";
	public static final String ATTRIB_RATE_DT = "RateDt";
	public static final String ATTRIB_RATE_MINIMUM_DURATION = "RateMinimumDuration";
	public static final String ATTRIB_RATE_PERIOD = "RatePeriod";
	public static final String ATTRIB_RATE_PERIOD_ROUNDING = "RatePeriodRounding";
	public static final String ATTRIB_RATED_UNITS = "RatedUnits";
	public static final String ATTRIB_RATING_METHOD = "RatingMethod";
	public static final String ATTRIB_RAW_UNITS = "RawUnits";
	public static final String ATTRIB_RAW_UNITS_ROUNDED = "RawUnitsRounded";
	public static final String ATTRIB_RAW_UNITS_TYPE = "RawUnitsType";
	public static final String ATTRIB_REV_RCV_COST_CTR = "RevRcvCostCtr";
	public static final String ATTRIB_ROUNDING_METHOD = "RoundingMethod";
	public static final String ATTRIB_SECOND_DT = "SecondDt";
	public static final String ATTRIB_SECOND_UNITS = "SecondUnits";
	public static final String ATTRIB_SEQNUM_RATE_USAGE = "SeqnumRateUsage";
	public static final String ATTRIB_SEQNUM_RATE_USAGE_OVERRIDES = "SeqnumRateUsageOverrides";
	public static final String ATTRIB_TAX_CLASS = "TaxClass";
	public static final String ATTRIB_TAX_CODE_ORIGIN_REQ = "TaxCodeOriginReq";
	public static final String ATTRIB_TAX_CODE_TARGET_REQ = "TaxCodeTargetReq";
	public static final String ATTRIB_TAX_LOCATION_OUTCOLLECT = "TaxLocationOutcollect";
	public static final String ATTRIB_TAX_LOCATION_USG = "TaxLocationUsg";
	public static final String ATTRIB_TAX_PKG_COUNT = "TaxPkgCount";
	public static final String ATTRIB_TAX_RATE_ACTIVE_DT = "TaxRateActiveDt";
	public static final String ATTRIB_TAX_RATE_INACTIVE_DT = "TaxRateInactiveDt";
	public static final String ATTRIB_THIRD_UNITS = "ThirdUnits";
	public static final String ATTRIB_TIMEZONE = "Timezone";
	public static final String ATTRIB_TRANS_DT = "TransDt";
	public static final String ATTRIB_TRANS_ID = "TransId";
	public static final String ATTRIB_TRANS_ID_USG = "TypeIdUsg";
	public static final String ATTRIB_UNITS_CURRENCY_CODE = "UnitsCurrencyCode";
	public static final String ATTRIB_UNITS_INDICATOR = "UnitsIndicator";
	public static final String ATTRIB_UNROUNDED_AMOUNT = "UnroundedAmount";
	public static final String ATTRIB_USE_BILL_CLASS = "UseBillClass";
	public static final String ATTRIB_USE_CLASS_OF_SERVICE_CODE = "UseClassOfServiceCode";
	public static final String ATTRIB_USE_COMPONENT_ID = "UseComponentId";
	public static final String ATTRIB_USE_DEFAULT_RATE_TYPE = "UseDefaultRateType";
	public static final String ATTRIB_USE_DISTANCE_BAND_ID = "UseDistanceBandId";
	public static final String ATTRIB_USE_ELEMENT_ID = "UseElementId";
	public static final String ATTRIB_USE_EQUIP_CLASS_CODE = "UseEquipClassCode";
	public static final String ATTRIB_USE_EQUIP_TYPE_CODE = "UseEquipTypeCode";
	public static final String ATTRIB_USE_JURISDICTION = "UseJurisdiction";
	public static final String ATTRIB_USE_POINT_CLASS_ORIGIN = "UsePointClassOrigin";
	public static final String ATTRIB_USE_POINT_CLASS_TARGET = "UsePointClassTarget";
	public static final String ATTRIB_USE_PROVIDER_CLASS = "UseProviderClass";
	public static final String ATTRIB_USE_RATE_CLASS = "UseRateClass";
	public static final String ATTRIB_USE_RATE_PERIOD = "UseRatePeriod";
	public static final String ATTRIB_USE_ZONE_CLASS = "UseZoneClass";
	public static final String ATTRIB_USG_CLASS = "UsgClass";
	public static final String ATTRIB_VH_MINOR_THRESHOLD = "VhMinorThreshold";
	public static final String ATTRIB_ZONE_CLASS = "ZoneClass";
	// These are the attributes of the BILLED USAGE portion of the adjustment.
	public static final String ATTRIB_ACCOUNT_NO = "AccountNo";
	public static final String ATTRIB_RATED_AMOUNT = "RatedAmount";
	public static final String ATTRIB_AMOUNT_CREDITED = "AmountCredited";
	public static final String ATTRIB_RATED_AMOUNT_REDUCTION = "RatedAmountReduction";
	public static final String ATTRIB_AUX_TAX_INFO = "AuxTaxInfo";
	public static final String ATTRIB_BILL_INVOICE_ROW = "BillInvoiceRow";
	public static final String ATTRIB_CITY_TAX = "CityTax";
	public static final String ATTRIB_COUNTY_TAX = "CountyTax";
	public static final String ATTRIB_FEDERAL_TAX = "FederalTax";
	public static final String ATTRIB_OTHER_TAX = "OtherTax";
	public static final String ATTRIB_STATE_TAX = "StateTax";
	public static final String ATTRIB_DESCRIPTION_GROUP_USAGE = "DescriptionGroupUsage";
	public static final String ATTRIB_DESCRIPTION_TEXT_USAGE = "DescriptionTextUsage";
	public static final String ATTRIB_DISCOUNT = "Discount";
	public static final String ATTRIB_IS_LATE_FEE_EXEMPT_USAGE = "IsLateFeeExemptUsage";
	public static final String ATTRIB_PROCESS_NUM = "ProcessNum";
	public static final String ATTRIB_UNITS = "Units";
	public static final String ATTRIB_REFUND = "RefundProrationFactor";
	public static final String ATTRIB_SHORT_DESCRIPTION_TEXT = "ShortDescriptionText";
	public static final String ATTRIB_THRESHOLD_PRORATION_FACTOR = "ThresholdProrationFactor";
	public static final String ATTRIB_TYPE_ID_USG = "TypeIdUsg";
	public static final String ATTRIB_UNIT_CR_ID = "UnitCrId";
	public static final String ATTRIB_UNITS_CREDITED = "UnitsCredited";
	// These are attributes of the ADJUSTMENT TYPE portion of the usage adjustment.
	public static final String ATTRIB_TRANS_SIGN = "TransSign";
	public static final String ATTRIB_TRANS_TARGET_TYPE = "TransTargetType";
	public static final String ATTRIB_TRANS_TARGET_ID = "TransTargetId";
	public static final String ATTRIB_IS_DISCONNECT_CREDIT = "IsDisconnectCredit";
	public static final String ATTRIB_BILLING_CATEGORY = "BillingCategory";
	public static final String ATTRIB_DESCRIPTION_CODE_ADJUSTMENT_TYPE = "DescriptionCodeAdjustmentType";
	public static final String ATTRIB_DESCRIPTION_TEXT_ADJUSTMENT_TYPE = "DescriptionTextAdjustmentType";
	public static final String ATTRIB_IS_LATE_FEE_EXEMPT_ADJUSTMENT_TYPE = "IsLateFeeExemptAdjustmentType";
	public static final String ATTRIB_DESCRIPTION_GROUP_ADJUSTMENT_TYPE = "DescriptionGroupAdjustmentType";
	public static final String ATTRIB_IS_ADJUSTABLE = "IsAdjustable";
	public static final String ATTRIB_TAX_ON_INVOICE = "TaxOnInvoice";
	public static final String ATTRIB_IS_JOURNALABLE = "IsJournalable";
	public static final String ATTRIB_IS_REFINANCE = "IsRefinance";
	public static final String ATTRIB_IS_NEGATIVE_BILL_ADJ = "IsNegativeBillAdj";
	public static final String ATTRIB_IS_VIEWABLE = "IsViewable";
	public static final String ATTRIB_ALLOW_INTERIM_BILL = "AllowInterimBill";
	public static final String ATTRIB_IS_DISPLAYED_ON_BILL = "IsDisplayedOnbill";
	public static final String ATTRIB_BILLING_LEVEL = "BillingLevel";
	public static final String ATTRIB_IS_MODIFIABLE = "IsModifiable";
	// These are attributes for the MIU portion of the usage adjustment
	public static final String ATTRIB_IS_MIU_ADJUSTED = "IsMIUAdjusted";
	public static final String ATTRIB_MIU_ANNOTATION = "MIUAnnotation";
	public static final String ATTRIB_USAGE_TYPE_ADJUSTED = "UsageTypeAdjusted";
	/** Initialize string values for all attributes that can be set by a client. **/    
	
	// Due to more than one object using the same 
	public static final String ATTRIB_ANNOTATION = "Annotation";
	public static final String ATTRIB_PRIMARY_UNITS = "PrimaryUnits";
	public static final String ATTRIB_AMOUNT = "Amount";
	public static final String ATTRIB_DESCRIPTION_CODE = "DescriptionCode";
	public static final String ATTRIB_DESCRIPTION_TEXT = "DescriptionText";
	public static final String ATTRIB_DESCRIPTION_GROUP = "DescriptionGroup";
	public static final String ATTRIB_IS_LATE_FEE_EXEMPT = "IsLateFeeExempt";

	/**String value of attributeName to set the Message Severity "Business Rule" object attribute.*/
	public static final String ATTRIB_MSG_SEVERITY = "MsgSeverity";
	/**String value of attributeName to set the Message Severity "Business Rule" object attribute.*/
	public static final String ATTRIB_MSG_SEVERITY_DESCR = "MsgSeverityDescr";
	/**String value of attributeName to set the Message Severity "Business Rule" object attribute.*/
	public static final String ATTRIB_MSG_SEVERITY_SHORT_DESCR = "MsgSeverityShortDescr";
	/**String value of attributeName to set the Message Text "Business Rule" object attribute.*/
	public static final String ATTRIB_MSG_TEXT = "MsgText";
	/**String value of attributeName to set the Validation Id "Business Rule" object attribute.*/
	public static final String ATTRIB_VALIDATION_ID = "ValidationId";
	/**String value of attributeName to set the Major Step "Business Rule" object attribute.*/
	public static final String ATTRIB_BR_MAJOR_STEP_ID = "BrMajorStepId";
	/**String value of attributeName to set the Minor Step "Business Rule" object attribute.*/
	public static final String ATTRIB_BR_MINOR_STEP_ID = "BrMinorStepId";
	/**String value of attributeName to set the Step Display_value "Business Rule" object attribute.*/
	public static final String ATTRIB_BR_STEP_NAME = "StepName";
	/**String value of attributeName to set the Step Display_value "Business Rule" object attribute.*/
	public static final String ATTRIB_BR_STEP_ACTION = "StepAction";
	/**String value of attributeName to set the Major Rule "Business Rule" object attribute.*/
	public static final String ATTRIB_BR_MAJOR_RULE_ID = "BrMajorRuleId";
	/**String value of attributeName to set the Minor Rule "Business Rule" object attribute.*/
	public static final String ATTRIB_BR_MINOR_RULE_ID = "BrMinorRuleId";
	

	//Cycle30 Account Balance Lookup
	public static final String ATTRIB_ACCT_BAL_WRITEOFF = "WriteOffBalance";
	public static final String ATTRIB_ACCT_BAL_BALANCE30="Balance30";
	public static final String ATTRIB_ACCT_BAL_BALANCE60="Balance60";
	public static final String ATTRIB_ACCT_BAL_BALANCE90="Balance90";
	public static final String ATTRIB_ACCT_BAL_BALANCE120="Balance120";
	public static final String ATTRIB_ACCT_BAL_BALANCE_GT_120="BalanceGreater120";
	public static final String ATTRIB_ACCT_BAL_CURRENT="CurrentBalance";
	public static final String ATTRIB_ACCT_BAL_TOTAL="TotalBalance";
	
	//C30 Order Attributes.
	public static final String ATTRIB_C30ORDER_ORDERID = "OrderId";
	public static final String ATTRIB_C30ORDER_SUBORDER_ID = "SubOrderId";
	public static final String ATTRIB_C30ORDER_ACCOUNT_SEG_ID="AcctSegId";
	public static final String ATTRIB_C30ORDER_TRANSACTION_ID ="TransactionId";
	public static final String ATTRIB_C30ORDER_CLIENT_ORGNAME ="ClientOrgName";
	public static final String ATTRIB_C30ORDER_CLIENT_ORDID = "ClientOrgId";
	public static final String ATTRIB_C30ORDER_INTERNAL_ID = "InternalId";

	//SPS Attribute Names
	public static final String ATTRIB_SPS_TRANSACTION_ID ="TransactionId";	
    public static final String ATTRIB_SERIAL_NUMBER = "SerialNumber";    
    public static final String ATTRIB_NETWORK_ID = "NetworkId";

	//Cycle30 OCS Prepaid Account Balance Lookup
    public static final String ATTRIB_OCS_SUBSCRIBER_NUMBER="SubscriberId";
     
    //Cycle 30 OCS Prepaid Voucher 
    public static final String ATTRIB_OCS_VOUCHER_PIN="VoucherPIN";
    public static final String ATTRIB_OCS_VOUCHER_ACTION="VoucherAction";
    public static final String ATTRIB_OCS_ACCOUNT_ACTION="PrepaidAccountAction";
    
    //Cycle30 OCS Prepaid Balance Create Update 
    public static final String ATTRIB_OCS_BALANCE_ACTION="BalanceAction";
    public static final String ATTRIB_OCS_BALANCE_LABEL_TYPE="BalanceTypeLabel";
    public static final String ATTRIB_OCS_BALANCE_REASON= "BalanceReason";
    public static final String ATTRIB_OCS_BALANCE_VALUE="Value";
    public static final String ATTRIB_OCS_BALANCE_EXPIRATION_DATE="ExpirationDate";
    public static final String ATTRIB_OCS_BALANCE_VOUCHER_ACCESS_PHONE_CODE="AccessPhone";
    public static final String ATTRIB_OCS_SUBSCRIBER_CLI="SubscriberCLI";
    public static final String ATTRIB_OCS_CHARGING_EVENT_LABEL="ChargeableEventLabel";
    public static final String ATTRIB_REVERSAL_REQUEST_ID="ReversalRequestId";
    
    //Cycle30 OCS Prepaid Payment Register
    public static final String ATTRIB_PAYEMNT_ACTION="ActionType";
    public static final String ATTRIB_OCS_PAYEMNT_NOTE="Note";
    public static final String ATTRIB_OCS_PAYEMNT_CHANNEL="PaymentSalesChannel";
    public static final String ATTRIB_OCS_PAYEMNT_EXTCORRELATIONID="ExtCorrelationId";
    public static final String ATTRIB_OCS_PAYEMNT_CHECK_NUMBER="CheckNumber";
    public static final String ATTRIB_OCS_PAYEMNT_TRANSACTION_DATE_TIME="TransactionDateTime";
    public static final String ATTRIB_OCS_PAYEMNT_TENDER_TYPE="TenderType";
    public static final String ATTRIB_OCS_PAYEMNT_CURRENCY="Currency";
    public static final String ATTRIB_OCS_PAYEMNT_AMOUNT="Amount";
    public static final String ATTRIB_OCS_PAYEMNT_ACCOUNT_NUMBER="AccountNumber";
    
    
    
    // Cycle30 Prepaid Phone Number Inventory Lookup
    
    public static final String ATTRIB_KENAN_PREPAID_NUMBER="TelephoneNumber";
    public static final String ATTRIB_KENAN_PREPAID_COMMUNITY="Community";
    public static final String ATTRIB_KENAN_PREPAID_SALES_CHANNEL_ID="SalesChannelId";
    public static final String ATTRIB_KENAN_PREPAID_SIZE="Size";
    public static final String ATTRIB_KENAN_PREPAID_INV_STATUS="InvStatus";
    public static final String ATTRIB_KENAN_PREPAID_NETWORK_ID="NetworkId";
    public static final String ATTRIB_KENAN_PREPAID_OPERATOR_ID="OperatorId";
    public static final String ATTRIB_KENAN_PREPAID_ACTION_TYPE="ActionType";
    public static final String ATTRIB_KENAN_PREPAID_INVENTORY_PAYLOAD="Payload";
    
    
    
    //Core Clientcallbacks
    public static final String ATTRIB_CLIENT_CALLBACK_RESPONSE  = "Response";
    
}
