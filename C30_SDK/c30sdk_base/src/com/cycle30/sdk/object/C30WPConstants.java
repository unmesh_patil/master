/*
 * Used to store all the constants in the project.
 */
package com.cycle30.sdk.object;

public class C30WPConstants {

	/** Attributes of the C30WP object class. */
	
	//Workpoint Property file name.
	public static final String C30_WP_CONFIG_FILE_NAME = "C30WP_Config_en.properties";
	public static final String C30_WP_CONFIG_FILE_NAME_RESOURCE = "C30WP_Config";
	
	//WorkPoint Error Config files
	public static final String C30_WP_ERROR_FILE_NAME = "C30WP_Core_Error_Text_en.properties";
	public static final String C30_WP_ERROR_FILE_NAME_RESOURCE = "C30WP_Core_Error_Text";
	
}
