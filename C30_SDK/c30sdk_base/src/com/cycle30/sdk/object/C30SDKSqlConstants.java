package com.cycle30.sdk.object;

/*
 * Class that stores all the SQL Constants.
 */
public class C30SDKSqlConstants {



	public static String QUERY_FIND_ACCOUNT_DATA_USING_ACCOUNT_LIST = 
			"SELECT  CMF.bill_hold_code BillHoldCode ,CMF.remark Remark, CMF.cust_faxno CustFaxno,CMF.cust_city CustCity," +
					"CMF.exrate_class ExrateClass ,CMF.sic_code SicCode,CMF.account_type AccountType,CMF.cust_address3 CustAddress3,TO_CHAR(" +
					"CMF.date_active,'YYYY-MM-DD HH24:MI:SS') DateActive,CMF.cust_phone2 CustPhone2,CMF.vip_code VipCode," +
					"CMF.disconnect_reason DisconnectReason,CMF.plan_id_credit PlanIdCredit,CMF.bill_name_pre BillNamePre,CMF.cust_county  CustCounty," +
					"CMF.cust_state CustState,CMF.converted Converted,CMF.insert_grp_id InsertGrpId,CMF.bill_period BillPeriod," +
					"CMF.statement_to_email StatementToEmail,CMF.dept Dept,CMF.no_bill NoBill,CMF.contact2_name ContactName2," +
					"CMF.prev_balance_ref_resets,CMF.rate_class_special RateClassSpecial,CMF.cust_address1 CustAddress1," +
					"CMF.cyclical_threshold,CMF.parent_id,CMF.bill_fname BillFname,CMF.child_count ChildCount," +
					"CMF.bill_address1,CMF.rev_rcv_cost_ctr,CMF.collection_history,CMF.alt_lname," +
					"CMF.ssn,CMF.threshold,CMF.bill_county,CMF.bill_title,CMF.bill_zip," +
					"CMF.contact2_phone,CMF.account_no,CMF.credit_thresh,CMF.msg_grp_id," +
					"CMF.usg_amt_limit,CMF.cred_status,CMF.bill_company,CMF.title," +
					"CMF.prev_bill_refno,CMF.bill_lname,CMF.plan_id_discount,CMF.cust_country_code," +
					"CMF.cust_phone1,CMF.bill_address3,CMF.bill_country_code,TO_CHAR(" +
					"CMF.date_inactive,'YYYY-MM-DD HH24:MI:SS'),CMF.alt_fname,CMF.cust_address2," +
					"CMF.collection_indicator,TO_CHAR(CMF.account_status_dt,'YYYY-MM-DD HH24:MI:SS')," +
					"CMF.bill_fmt_opt,CMF.payment_profile_id,CMF.cust_geocode,CMF.alt_company_name," +
					"CMF.contact1_phone,CMF.billing_frequency,CMF.codeword,CMF.credit_rating," +
					"CMF.account_category,TO_CHAR(CMF.date_created,'YYYY-MM-DD HH24:MI:SS')," +
					"CMF.chg_who,CMF.currency_code,CMF.bill_address2,CMF.bill_geocode," +
					"CMF.global_contract_status,CMF.tie_code,CMF.collection_status,CMF.cust_zip," +
					"CMF.gender,TO_CHAR(CMF.prev_bill_date,'YYYY-MM-DD HH24:MI:SS'),TO_CHAR(" +
					"CMF.prev_cutoff_date,'YYYY-MM-DD HH24:MI:SS'),CMF.bill_city," +
					"CMF.statement_to_faxno,CMF.usg_units_limit,TO_CHAR(CMF.chg_date," +
					"'YYYY-MM-DD HH24:MI:SS'),CMF.bill_state,TO_CHAR(CMF.next_bill_date," +
					"'YYYY-MM-DD HH24:MI:SS'),CMF.in_use,CMF.contact1_name,CMF.cust_email," +
					"CMF.mkt_code,CMF.regulatory_id,CMF.hierarchy_id,CMF.bill_franchise_tax_code," +
					"CMF.rate_class_default,CMF.purchase_order,CMF.owning_cost_ctr,CMF.distr_chan," +
					"CMF.bill_minit,CMF.language_code,CMF.sales_code,CMF.charge_threshold," +
					"CMF.disc_rcv_opt,CMF.cust_franchise_tax_code,CMF.bill_name_generation," +
					"CMF.prev_balance_refno,CMF.prev_bill_ref_resets,CMF.thresh_ref," +
					"CMF.bill_sequence_num,CMF.account_status,CMF.acct_seg_id,CMF.bill_disp_meth," +
					"CIAM.account_no,TO_CHAR(CIAM.inactive_date,'YYYY-MM-DD HH24:MI:SS')," +
					"CIAM.external_id,CIAM.is_current,CIAM.external_id_type,TO_CHAR(CIAM.active_date," +
					"'YYYY-MM-DD HH24:MI:SS'),CIAM_H.account_no,TO_CHAR(CIAM_H.inactive_date," +
					"'YYYY-MM-DD HH24:MI:SS'),CIAM_H.external_id,CIAM_H.is_current," +
					"CIAM_H.external_id_type,TO_CHAR(CIAM_H.active_date,'YYYY-MM-DD HH24:MI:SS')," +
					"CIAM_P.account_no,TO_CHAR(CIAM_P.inactive_date,'YYYY-MM-DD HH24:MI:SS')," +
					"CIAM_P.external_id,CIAM_P.is_current,CIAM_P.external_id_type,TO_CHAR(" +
					"CIAM_P.active_date,'YYYY-MM-DD HH24:MI:SS') FROM CMF,CUSTOMER_ID_ACCT_MAP CIAM," +
					"CUSTOMER_ID_ACCT_MAP CIAM_H,CUSTOMER_ID_ACCT_MAP CIAM_P,EXTERNAL_ID_TYPE_REF" +
					" WHERE CMF.account_no = CIAM.account_no AND CIAM.is_current = 1 AND" +
					" CMF.hierarchy_id = CIAM_H.account_no AND CIAM_H.is_current = 1 AND" +
					" CMF.parent_id = CIAM_P.account_no (+) AND CIAM_P.is_current (+) = 1 AND " +
					"CIAM.external_id_type = EXTERNAL_ID_TYPE_REF.external_id_type AND " +
					" CIAM_H.external_id_type = EXTERNAL_ID_TYPE_REF.external_id_type AND " +
					" EXTERNAL_ID_TYPE_REF.is_default = 1 AND  " +
					"CIAM_P.external_id_type (+) = ? " +
					" AND CMF.account_no IN  ";


	public static String QUERY_GET_ACCOUNT_INTERNAL_ID_FROM_EXTERNAL_ID ="SELECT ACCOUNT_NO AccountInternalId FROM CUSTOMER_ID_ACCT_MAP " +
			"WHERE EXTERNAL_ID = ? AND EXTERNAL_ID_TYPE =? AND IS_CURRENT=1";

	public static  String QUERY_WRITEOFF_BALANCE =                    
			"select  account_no, sum(case when writeoff_type_code = 2 then amount " + 
					" when writeoff_type_code = 3 then (-1*amount) " +  
					" when writeoff_type_code = 4 then amount " + 
					" when writeoff_type_code = 5 then (-1*amount) " + 
					" end * 1) TRAN_AMT " + 
					" from jnl_writeoff where  account_no =  ? group by account_no" ;  

	public static  String QUERY_WRITEOFF_BALANCE_IN_LIST =                    
			"select  account_no, sum(case when writeoff_type_code = 2 then amount " + 
					" when writeoff_type_code = 3 then (-1*amount) " +  
					" when writeoff_type_code = 4 then amount " + 
					" when writeoff_type_code = 5 then (-1*amount) " + 
					" end * 1) TRAN_AMT " + 
					" from jnl_writeoff where  account_no in " ;  

	public static String QUERY_GET_EXTENDEDDATA_PARAM_LIST=
			"SELECT  PARAM_DEF.param_external_id PARAM_EXT_ID,PARAM_DEF.param_id ParamId,PARAM_DEF.param_name ParamName," +
					"		PARAM_DEF.param_datatype ParamDataType,PARAM_DEF.assoc_enumeration_id," +
					"PARAM_DEF.is_addressable,PARAM_DEF.validation_rules,PARAM_VALUES.language_code," +
					"PARAM_VALUES.display_value DisplayValue,PARAM_VALUES.validation_rules_desc," +
					"PARAM_VALUES.param_id,PARAM_VALUES.short_display  FROM PARAM_DEF,PARAM_VALUES " +
					"WHERE PARAM_DEF.param_id = PARAM_VALUES.param_id";

	public static String QUERY_GET_EXTENDEDDATA_PARAM_COUNT=
			"SELECT  COUNT(*) TotalCount FROM PARAM_DEF,PARAM_VALUES " +
					"WHERE PARAM_DEF.param_id = PARAM_VALUES.param_id";

	public static String QUERY_GET_EXTENDEDDATA_ASSOC_LIST = 
			"SELECT  EXT_PARAM_TYPE_ASSOC.grouping_id,EXT_PARAM_TYPE_ASSOC.base_table BaseTable," +
					"EXT_PARAM_TYPE_ASSOC.is_required IsRequired,EXT_PARAM_TYPE_ASSOC.attribute_display_order," +
					"EXT_PARAM_TYPE_ASSOC.param_id ParamId,EXT_PARAM_TYPE_ASSOC.entity_type EntityType," +
					"EXT_PARAM_TYPE_ASSOC.default_value FROM EXT_PARAM_TYPE_ASSOC";

	public static String QUERY_GET_EXTENDEDDATA_ASSOC_COUNT = 
			"SELECT  COUNT(*) TotalCount FROM EXT_PARAM_TYPE_ASSOC";

	public static String QUERY_GET_GENERICENUMERAION_FIND_COUNT=
			"SELECT  COUNT(*) FROM GENERIC_ENUMERATION_REF," +
					"	GENERIC_ENUMERATION_VALUES WHERE GENERIC_ENUMERATION_REF.enumeration_key =" +
					"	GENERIC_ENUMERATION_VALUES.enumeration_key AND GENERIC_ENUMERATION_REF.value = GENERIC_ENUMERATION_VALUES.value";

	public static String QUERY_GET_GENERICENUMERAION_FIND_LIST=
			"SELECT  GENERIC_ENUMERATION_REF.is_internal IsInternal," +
					"	GENERIC_ENUMERATION_REF.enumeration_key EnumerationKey,GENERIC_ENUMERATION_REF.value Value," +
					"	GENERIC_ENUMERATION_REF.is_default IsDefault,GENERIC_ENUMERATION_REF.sort_index," +
					"	GENERIC_ENUMERATION_VALUES.language_code LanguageCode," +
					"	GENERIC_ENUMERATION_VALUES.enumeration_key EnumerationKey," +
					"	GENERIC_ENUMERATION_VALUES.short_display,GENERIC_ENUMERATION_VALUES.value," +
					"	GENERIC_ENUMERATION_VALUES.display_value DisplayValue " +
					"	FROM GENERIC_ENUMERATION_REF," +
					"	GENERIC_ENUMERATION_VALUES " +
					"	WHERE GENERIC_ENUMERATION_REF.enumeration_key =" +
					"	GENERIC_ENUMERATION_VALUES.enumeration_key AND GENERIC_ENUMERATION_REF.value = GENERIC_ENUMERATION_VALUES.value";

	public static  String CHANGE_ORDER_STATUS =                    
			"UPDATE C30_SDK_ORDER " +
					"SET IS_LOCKED=1, ORDER_MANAGER_PICK_DATE = SYSDATE, ORDER_STATUS_ID= ? " +
					"WHERE CLIENT_ORDER_ID = ? " +
					"AND TRANSACTION_ID = ?";

	public static  String CHANGE_ORDER_STATUS_CANCEL =                    
			"UPDATE C30_SDK_ORDER " +
					"SET IS_LOCKED=0,  ORDER_STATUS_ID= ? " +
					"WHERE CLIENT_ORDER_ID = ? AND ORDER_STATUS_ID NOT IN(20,80)" ;	

	public static  String CHANGE_SUBORDER_STATUS_CANCEL =                    
			"UPDATE C30_SDK_SUB_ORDER " +
					"SET SUB_ORDER_STATUS_ID= ? " +
					"WHERE CLIENT_ORDER_ID = ? AND SUB_ORDER_STATUS_ID NOT IN(20,80)" ;

	public static  String GET_ORDER_STATUS_BY_ORDER_ID =                    
			"SELECT ORDER_STATUS_ID FROM C30_SDK_ORDER " +
					"WHERE CLIENT_ORDER_ID = ? ORDER BY ORDER_CREATE_DATE DESC";
	
	public static  String GET_IS_DEPENDENT_ORDER_EXISTS =  "select count(*)  COUNT from c30_sdk_order ord, c30_sdk_sub_order so " +
			" where ord.client_order_id = so.client_order_id  " +
			" and ord.order_status_id not in (80, 90, 99) " +
			" and ord.order_create_date >  sysdate - 3 " +
			" and so.client_id = ?" +
			" and ord.order_create_date < ( select a.order_create_date from c30_sdk_order a where a.transaction_id = ? and a.client_order_id = ? )";

	public static  String CHANGE_SUBORDER_STATUS =                    
			"UPDATE C30_SDK_SUB_ORDER " +
					"SET SUB_ORDER_STATUS_ID= ? " +
					"WHERE CLIENT_ORDER_ID = ? " +
					"AND CLIENT_SUB_ORDER_ID = ? " + 
					"AND TRANSACTION_ID = ?";

	public static final String CHANGE_SUBORDER_STATUS_UPDATE_JOB_ID = 	"UPDATE C30_SDK_SUB_ORDER " +
			"SET SUB_ORDER_STATUS_ID= ? ," +
			" WP_JOB_ID = ?" +
			"WHERE CLIENT_ORDER_ID = ? " +
			"AND CLIENT_SUB_ORDER_ID = ? " +
			"AND TRANSACTION_ID = ?";


	public static  String UPDATE_ORDER_LOCK_UNLOCK =                    
			"UPDATE C30_SDK_ORDER " +
					"SET IS_LOCKED= ? " +
					"WHERE CLIENT_ORDER_ID = ? " +
					"AND TRANSACTION_ID = ?";

	public static  String GET_SUB_ORDER_INFORMATION =                    
			"SELECT CLIENT_SUB_ORDER_ID, WP_PROCESS_ID, ACCT_SEG_ID, CLIENT_ORG_NAME, CLIENT_ORG_ID, TRANSACTION_ID, INTERNAL_AUDIT_ID " +
					"FROM C30_SDK_SUB_ORDER " +
					"WHERE CLIENT_ORDER_ID = ?" +
					"AND TRANSACTION_ID = ?";

	//Get the dependency order information
	public static  String GET_ORDER_DEPENDENCY_INFORMATION =                    
			"SELECT  CLIENT_ORDER_ID, IS_DEPENDENT, DEPENDENT_ORDER_ID, PAYLOAD " +
					"FROM C30_SDK_ORDER " +
					"WHERE CLIENT_ORDER_ID = ?";

	public static  String FIND_GEO_CODE_FROM_LOCGEO =                    
			"SELECT GEOCODE, COUNTYNAME FROM c30vertex.LOCGEO " +
					" WHERE  CITYNAME = ? " +
					" AND POSTALSTART <= ? " +
					" AND POSTALEND >=  ? " +
					" AND STATENAME = ?";


	// Get request status - do left outer join on item name/status name/log message since they
	// may not exist in the provisioning tables.
	public static  String GET_REQUEST_STATUS =                    
			"select a.request_start_date, b.feature_id, c.feature_item_name, " +
					"b.sub_request_id, b.status_id, d.status_name, b.response_date, " +
					" e.log_message "                       +
					"from c30_prov_catalog_trans     a,  " +
					"c30_prov_trans_inst       b,  " +
					"c30_prov_feature_item_def c,  " +
					"c30_prov_status_def       d,  " +
					"c30_sdk_order_trans_log   e   " +
					"where a.external_transaction_id = ? " +
					"and b.request_id = a.request_id   " +
					"and c.feature_item_id(+) = b.feature_item_id " +
					"and d.status_id(+) = b.status_id "  +
					"and (e.transaction_id(+) = a.external_transaction_id " +
					"     and e.log_level(+)='ERROR') "  +
					"order by sub_request_id";


	// Query order table for basic status based on transaction id
	public static  String GET_ORDER_STATUS_BY_TRANSACTION_ID =   
			"Select order_create_date, order_status_id " +
					"from c30_sdk_order " +
					"where transaction_id=?";

	// Query provisioning transaction instance table for matching sub_request id
	public static  String QUERY_PROV_INSTANCE_STATUS =                    
			"Select sub_request_id from c30_prov_trans_inst " +
					"where sub_request_id=?";

	//Query to Insert the Extended Data Values of TransactionId/SubOrderId
	public static final String INSERT_INTO_SUBORDER_EXTENDED_DATA = 
			" INSERT INTO C30_SDK_SUB_ORDER_EXT_DATA (CLIENT_SUB_ORDER_ID, TRANSACTION_ID, ACCT_SEG_ID , KEY, VALUE, SEND_FLAG)" +
					" VALUES (?,?,?,?,?,?) ";
	
	// Update provisioning status tables using request id provided by provisioning system.
	public static  String UPDATE_PROV_INSTANCE_STATUS =                    
			"update c30_prov_trans_inst " +
					"set status_id=?, response_date=SYSDATE, response_xml=? " +
					"where sub_request_id=?";

	// See if all provisioning instances are in specific state, and update the single
	// c30_prov_catalog_trans row when all instances with the same request
	// id are are the same (eg '80', '99', etc)
	public static String UPDATE_PROV_CATALOG_STATUS =
			"update c30_prov_catalog_trans " +
					"set request_complete_date=sysdate, status_id=? " +
					"where request_id  in  (select request_id from c30_prov_trans_inst where sub_request_id=?) " +
					"and " +
					"(select count(*) from c30_prov_trans_inst a, c30_prov_trans_inst b " +
					"where  a.request_id=b.request_id " +
					"and  b.sub_request_id=? " +
					"and  a.status_id<>? " +
					") = 0";

	// Get transaction id from provisioning callback request id, eg. ACS.23_1
	public static String QUERY_ORDER_FROM_REQUEST_ID =
			"select b.external_transaction_id, c.client_order_id " +
					"from c30_prov_trans_inst a, c30_prov_catalog_trans b, c30_sdk_order c " +
					"where a.sub_request_id=? " +
					"and b.request_id=a.request_id " +
					"and c.transaction_id = b.external_transaction_id ";

	// Get transaction id and sub order id from provisioning callback request id, eg. ACS.23_1
	public static String QUERY_SUBORDER_FROM_REQUEST_ID =
			"select b.external_transaction_id, c.client_sub_order_id " +
					"from c30_prov_trans_inst a, c30_prov_catalog_trans b, c30_sdk_sub_order c " +
					"where a.sub_request_id=? " +
					"and b.request_id=a.request_id " +
					"and c.transaction_id = b.external_transaction_id ";


	public static String QUERY_TRANSACTION_LOG =
			"select count(*) from c30_sdk_order_trans_log where transaction_id = ?";


	public static String INSERT_PROV_TRANSACTION_LOG =
			"insert into c30_sdk_order_trans_log " +
					"(client_order_id,  "      +
					" client_sub_order_id,  "  +
					"transaction_id,  "       +
					"log_date,  "             +
					"log_level,  "            +
					"log_message)"            + 
					"values (?, ?, ?, sysdate, 'ERROR', ?)";  


	public static String UPDATE_PROV_TRANSACTION_LOG =
			"update c30_sdk_order_trans_log set log_date=SYSDATE, log_level='ERROR', log_message=? " + 
					" where transaction_id=?";


	
}

