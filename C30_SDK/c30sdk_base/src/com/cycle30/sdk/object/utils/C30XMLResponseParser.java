package com.cycle30.sdk.object.utils;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class C30XMLResponseParser
{
	private static Logger log=Logger.getLogger(C30XMLResponseParser.class);
	static String sessionId;

	public static String parseResponseForSessionId(String response)
	{
		log.debug("Parsing the response content for session ID.");
		try
		{
			Document doc = getDocument(response);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("AuthenticationResponse");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE)
				{

					Element eElement = (Element) nNode;
					sessionId = getTagValue("SessionId", eElement);
					log.info("SessionId : " + sessionId);
				}
			}
			return sessionId;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private static String getTagValue(String sTag, Element eElement)
	{
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
				.getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}
	public static Document getDocument(String xmlString)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlString));
        return docBuilder.parse(is);
    }

	public static HashMap parseOCSResponse(String OCSresponse,
			String CardGUID) throws ParserConfigurationException, SAXException, IOException {
		Document doc = getDocument(OCSresponse);
		doc.getDocumentElement().normalize();
		NodeList structList = doc.getElementsByTagName("struct");
		HashMap<Integer, HashMap<String, String>> bundleResult = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> balanceResultMap = null;
		for (int i = 0; i < structList.getLength(); i++) {

			NodeList memberList = ((Element) structList.item(i))
					.getElementsByTagName("member");
			balanceResultMap = new HashMap<String, String>();
			for (int j = 0; j < memberList.getLength(); j++) {

				String Name = (String) ((Element) memberList.item(j))
						.getElementsByTagName("name").item(0)
						.getChildNodes().item(0).getNodeValue();
				String Value = (String) ((Element) memberList.item(j))
						.getElementsByTagName("value").item(0)
						.getChildNodes().item(0).getTextContent();

				if (CardGUID.equalsIgnoreCase(Name)) {
					balanceResultMap.put(Name, Value);
					break;
				} 
			}
			bundleResult.put(i, balanceResultMap);
		}
		return bundleResult;
	}
	public static String parseResponse(String response)
	{
		log.debug("Parsing the response content for session ID.");
		try
		{
			Document doc = getDocument(response);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("AuthenticationResponse");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE)
				{

					Element eElement = (Element) nNode;
					sessionId = getTagValue("SessionId", eElement);
					log.info("SessionId : " + sessionId);
				}
			}
			return sessionId;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}


}
