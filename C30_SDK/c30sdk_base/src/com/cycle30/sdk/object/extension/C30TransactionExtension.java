package com.cycle30.sdk.object.extension;

import java.util.Iterator;

import org.apache.log4j.Logger;

import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKValidationException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * @author rnelluri
 * This is responsible for all the Pre and Post process implementaitons 
 * for the LW_OBJECT
 */
public abstract class C30TransactionExtension 
{
	private static Logger log = Logger.getLogger(C30TransactionExtension.class);
	public C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	public C30SDKPropertyFileManager KenanExceptionResourceBundle = C30SDKPropertyFileManager.getInstance();




	/**
	 * Validates that all the required dataAttributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
	 */
	public void validate(C30SDKObject c30sdkObj) throws C30SDKException {
		log.debug("BEGIN C30SDKObject.validateAttributes ...");
		int errorCount = 0;
		String errorAttributes = "";
		String attributeName = null;
		C30SDKAttribute attribute = null;
		Iterator iterator = null;

		for (int i = 0; i < c30sdkObj.CHANGEABLE_ATTRIB_TYPES.length; i++) {
			iterator = c30sdkObj.getAttributes(c30sdkObj.CHANGEABLE_ATTRIB_TYPES[i]).keySet().iterator();
			while (iterator.hasNext()) {
				attributeName = (String) iterator.next();
				attribute = (C30SDKAttribute) c30sdkObj.getAttributes(c30sdkObj.CHANGEABLE_ATTRIB_TYPES[i]).get(attributeName);
				if (attribute.isRequired() && attribute.getValue() == null) {
					if (errorCount == 0) {
						errorAttributes = errorAttributes + attributeName + " (" + c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ")";
					} else {
						errorAttributes = errorAttributes + ", " + attributeName + " (" + c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ")";
					}
					errorCount++;
				}
			}
			if (errorCount > 0) {
				throw new C30SDKValidationException(exceptionResourceBundle.getString("INVALID-ATTRIB-008") + errorAttributes,"INVALID-ATTRIB-008");
			}
		}
		log.debug("END C30SDKObject.validateAttributes ...");
	}
	
	
}
