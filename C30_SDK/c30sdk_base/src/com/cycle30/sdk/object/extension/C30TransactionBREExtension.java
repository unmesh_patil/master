package com.cycle30.sdk.object.extension;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;

public abstract class C30TransactionBREExtension {
	/**
	 * Validates that all the required Business Logic and dataAttributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
	 */
	public abstract void brevalidate(C30SDKObject c30sdkObj) throws C30SDKException ;
}
