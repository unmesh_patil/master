package com.cycle30.sdk.object.extension.prevalidation;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKValidationException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.extension.C30TransactionExtension;

public class PaymentDeletePreValidation extends C30TransactionExtension 
{
	/*
	 * This is used to validate Payment Object
	 */
	@Override
	public void validate(C30SDKObject c30sdkObj) throws C30SDKException
	{
		String acctExternalId = null;
		Integer acctExternalIdType = null;
		Integer acctInternalId = null;
		Integer accountServerId = null;
		Integer trackingId=null;
		Integer trackingIdServ = null;

		if (c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctExternalId = (String)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		if (c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctExternalIdType = (Integer)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		if (c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctInternalId = (Integer)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
		if (c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			trackingId = (Integer)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRACKING_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
		if (c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			trackingIdServ = (Integer)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
		if (c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			accountServerId = (Integer)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 

		if(trackingId == null || trackingIdServ==null)
		{
			throw new C30SDKValidationException(c30sdkObj.exceptionResourceBundle.getString("INVALID-ATTRIB-052"),"INVALID-ATTRIB-052");
		}
		if (((acctExternalId == null || acctExternalIdType == null) && acctInternalId == null)  && accountServerId == null ) 
		{

			throw new C30SDKValidationException(c30sdkObj.exceptionResourceBundle.getString("INVALID-ATTRIB-020"),"INVALID-ATTRIB-020");
		}

		// validate values for create payment or clear balance transaction    
		if  (! isFindTransaction(c30sdkObj))
		{
			super.validate(c30sdkObj);

			// for create transactions must have Account External id and Account Ext Id Type
			if (acctExternalId == null || acctExternalIdType == null)
			{
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-020"),"INVALID-ATTRIB-020");
			}

		/*	//check to ensure either the payment amount is set or the payment is set to clear the account balance.
			if (lwo.getAttribute(Attributes.ATTRIB_TRANS_AMOUNT, Constants.ATTRIB_TYPE_INPUT).getValue() == null
					&& !((Boolean) lwo.getAttributeValue(Attributes.ATTRIB_CLEAR_ACCOUNT_BALANCE, Constants.ATTRIB_TYPE_INPUT)).booleanValue()) {
				throw new C30SDKValidationException(exceptionResourceBundle.getString("INVALID-ATTRIB-021"),"INVALID-ATTRIB-021");
			}
			if (lwo.getAttribute(Attributes.ATTRIB_TRANS_AMOUNT, Constants.ATTRIB_TYPE_INPUT).getValue() != null &&
					((BigInteger) lwo.getAttributeValue(Attributes.ATTRIB_TRANS_AMOUNT, Constants.ATTRIB_TYPE_INPUT)).equals(BigInteger.ZERO)) {
				throw new C30SDKValidationException(exceptionResourceBundle.getString("INVALID-ATTRIB-022"),"INVALID-ATTRIB-022");
			}

*/
		}
		else // is a Find transaction, check billRefNo and Resets dependencies
		{
			if (c30sdkObj.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null &&
					c30sdkObj.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() != null ){

				throw new C30SDKValidationException(exceptionResourceBundle.getString("INVALID-ATTRIB-023"),"INVALID-ATTRIB-023");
			}
			if (c30sdkObj.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() != null &&
					c30sdkObj.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null ){
				throw new C30SDKValidationException(exceptionResourceBundle.getString("INVALID-ATTRIB-023"),"INVALID-ATTRIB-023");
			}
		}
	}



	private boolean isFindTransaction(C30SDKObject c30sdkObj)throws C30SDKInvalidAttributeException 
	{   boolean retval = false; 
	if  ((c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
			c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
			c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND) ||
			(  c30sdkObj.isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
					(Boolean)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT))   )) 
		return true;

	return retval;       

	}

}

