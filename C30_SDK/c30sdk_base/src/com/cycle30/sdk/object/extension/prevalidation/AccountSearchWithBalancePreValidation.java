package com.cycle30.sdk.object.extension.prevalidation;

import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.extension.C30TransactionExtension;

public class AccountSearchWithBalancePreValidation  extends C30TransactionExtension 
{
	/*
	 * This is used to validate Account for AccountSearchWithBalance Summary find... 
	 */
	@Override
	public void validate(C30SDKObject c30sdkObj) throws C30SDKInvalidAttributeException
	{
		String externalId = (String) c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer externalIdType = (Integer)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer accountInternalId = (Integer)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billFname = (String)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billLname = (String)c30sdkObj.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billCompany = (String)c30sdkObj.getAttributeValue("BillCompany", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String phone =(String)c30sdkObj.getAttributeValue("CustPhone1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String zip  =(String)c30sdkObj.getAttributeValue("BillZip", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String city  =(String)c30sdkObj.getAttributeValue("BillCity", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billAddress1 = (String)c30sdkObj.getAttributeValue("BillAddress1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String state = (String)c30sdkObj.getAttributeValue("BillState", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Boolean includeInactiveAccount = (Boolean)c30sdkObj.getAttributeValue("IncludeInactiveAccounts", C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		// Do any required validations on the input...
		//TO-DO
	}

}
