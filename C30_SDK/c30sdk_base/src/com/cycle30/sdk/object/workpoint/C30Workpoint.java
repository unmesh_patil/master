
package com.cycle30.sdk.object.workpoint;

import java.io.ByteArrayOutputStream;
import java.rmi.RemoteException;

import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30WPConnection;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.workpoint.client.ClientContext;
import com.workpoint.client.Job;
import com.workpoint.common.util.TableID;
import com.workpoint.gui.designer.AbstractModel;
import com.workpoint.gui.designer.JobModel;
import com.workpoint.gui.designer.MenuManager;
import com.workpoint.gui.external.WpJobModel;
import com.workpoint.server.ejb.WorkPointEJBException;


/**
 * @author rnelluri
 * This object is used to work on all the WorkPoint Processes and Jobs.
 * For all the Order Creation information etc.,
 */

public class C30Workpoint extends C30SDKObject {

	private static Logger log = Logger.getLogger(C30Workpoint.class);
	private JPanel panelVar = null;
	private AbstractModel model = null;
	private ClientContext clientContext = null;
	private boolean bConnectionFound = false;
	private MenuManager menuManager = null;
	C30WPConnection wpConn =null;

	/**
	 * Creates a new instance of C30Workpoint.
	 * @param factory Factory used to create C30Workpoint objects
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 * @throws C30SDKInvalidConfigurationException if there is invalid configuration which causes this account to fail during initialization.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Workpoint(final C30SDKFrameworkFactory factory) throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30Workpoint.class);
	}
	/**
	 * Creates a new instance of C30Workpoint.
	 * @param factory Factory used to create C30Workpoint objects
	 * @param objectType Type of C30SDK object
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 * @throws C30SDKInvalidConfigurationException if there is invalid configuration which causes this account to fail during initialization.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Workpoint(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}


	@Override
	protected final void initializeAttributes()
	throws C30SDKInvalidAttributeException {
		log.debug("Starting C30Workpoint.initializeAttributes().");
		setClassFieldConfiguration();
		log.debug("Finished C30Workpoint.initializeAttributes().");

	}


	@Override
	protected void loadCache() throws C30SDKCacheException {
		// TODO Auto-generated method stub

	}

	@Override
	public C30SDKObject process() throws C30SDKException 
	{
		C30SDKObject sdkObj = null;
		
		try{
			//Workpoint process initilized.
			log.info("Workpoint job process initialized");
			
			//Read the action and determine the task.
			log.info("Input Item Action Id : "+this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			log.info("Working on  WP Job Id : "+this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_WP_JOB_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

			//If ItemActionId =1 ,  Then perform the Save Job As Image and return
			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.WORKPOINT_GET_JOB_IMAGE)) {
				log.debug("Getting workpoint job image...");
				TableID jobId = new TableID((String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_WP_JOB_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				//showJob(jobId);
				ByteArrayOutputStream jobImage = getJobAsImage(jobId);

				log.info("Job is shown correctly..");
			
				populateWorkPointJob(jobImage, jobId, sdkObj);
				sdkObj = this;
			}


			setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		}
		catch(Exception e)
		{
			//throw new C30SDKException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),e,"INVALID-ATTRIB-013");
		    log.error(e);
			sdkObj = this.createExceptionMessage(e);
		}
		return sdkObj;
	}

	/**
	 * @param jobImage
	 * @param jobId
	 * @param sdkObject
	 * @throws C30SDKInvalidAttributeException
	 * This is used to populate the output Object with all the information from the WPJob. 
	 */
	private void populateWorkPointJob(ByteArrayOutputStream jobImage,
			TableID jobId, C30SDKObject sdkObject) throws C30SDKInvalidAttributeException {

		setAttributeValue(C30SDKAttributeConstants.ATTRIB_WP_JOB_ID, jobId.toString(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		setAttributeValue(C30SDKAttributeConstants.ATTRIB_WP_JOB_IMAGE, jobImage, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

	}

	/**
	 * @param jobID
	 * @return
	 * @throws C30SDKException
	 * This returns the image snapshot from the Workpoint job and wirtes the job image to a file/ByteArrayOutPutStream
	 */
	private ByteArrayOutputStream getJobAsImage( TableID jobID ) throws C30SDKException
	{
		ByteArrayOutputStream imageByteArray = new ByteArrayOutputStream();
		com.workpoint.gui.license.LicenseKeys.verifyJIDELicense();
		//LookAndFeelFactory.installJideExtension(LookAndFeelFactory.OFFICE2003_STYLE);

		//Initializing the panel for the images
		if ( panelVar == null )
			panelVar = new JPanel();
		else
			removeCurrentModel();

		try
		{

			if( !bConnectionFound )
				getConnection();
			if( clientContext == null )
				getConnection();
			if( clientContext != null )
			{
				Job job = null;
				job = Job.queryByID(clientContext, jobID, true);
				if( job != null )
				{
					//panelVar.getContentPane().setLayout(new BorderLayout(10,10));

					WpJobModel model = new WpJobModel( JobModel.getTitle( job ),
							clientContext,
							panelVar,
							job);
					log.info("Creating the Image of Job : "+JobModel.getTitle( job ));
					try
					{
					imageByteArray = model.createJPEGImage("jpg");
					}
					catch(Exception e)
					{
						throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-007"),"WP-JOB-007");
					}
				}
				else
				{
					throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-001"),"WP-JOB-001");
				}
			}
			else
			{
				throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-002"),"WP-JOB-002");
			}
		}
		catch( WorkPointEJBException e )
		{
			//JOptionPane.showMessageDialog(this, ResourceManager.getString("CC-4-42") );
			log.debug( "WorkPointEJBException during show job " );
			throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-003"),e,"WP-JOB-003");
		}
		catch( RemoteException ex )
		{
			//JOptionPane.showMessageDialog(this, ResourceManager.getString("CC-4-42") );
			log.debug( "Remote Exception " );
			throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-004"),ex,"WP-JOB-004");
		}
		catch( Exception ex )
		{
			log.debug( "UNKNOW ERROR" );
			throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-005"),ex,"WP-JOB-005");
		}
		return imageByteArray;
	}

	private void removeCurrentModel()
	{
		if( panelVar != null && model != null )
		{
			//panelVar.getContentPane().remove(model);
			panelVar.invalidate();
			panelVar.repaint();
			//panelVar.dispose();
		}
	}

	/**
	 * Gets the connection from existing WpConnection.
	 * @return
	 * @throws C30SDKException
	 */
	private boolean getConnection() throws C30SDKException
	{
		boolean bSucess = false;
		try
		{
			wpConn = C30WPConnection.getInstance();

			log.debug( "Try to connect WorkPoint with this parameter");

			clientContext = wpConn.getWpClientContext();
			if( clientContext == null )
			{
				log.debug( "Client Context is null" );
				throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-002"),"WP-JOB-002");
			}
		}
		catch( Exception ex )
		{
			log.debug( "UNKNOW ERROR" );
			throw new C30SDKException(exceptionResourceBundle.getString("WP-JOB-005"),ex,"WP-JOB-005");
		}
		return bSucess;
	}

}
