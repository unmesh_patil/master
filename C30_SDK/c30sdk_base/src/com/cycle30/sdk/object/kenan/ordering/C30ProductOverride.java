/*
 * C30ProductOverride.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30VersioningUtils;


/**
 * Wrapper class encapsulating a Kenan/FX product rate override object.  This object has no corresponding order item record in kenan
 * because it is a billing entity, not an ordering entity.
 * @author Joe Morales
 */
public class C30ProductOverride extends C30OrderItem {

    /**String value of attributeName to set the CurrencyCode "Lightweight Product Override" order item attribute.*/
    public static final String ATTRIB_ACTIVE_DT = "ActiveDt";
    /**String value of attributeName to set the CurrencyCode "Lightweight Product Override" order item attribute.*/
    public static final String ATTRIB_CURRENCY_CODE = "CurrencyCode";
    /**String value of attributeName to set the CurrencyCode "Lightweight Product Override" order item attribute.*/
    public static final String ATTRIB_INACTIVE_DT = "InactiveDt";
    /**String value of attributeName to set the OverrideRate "Lightweight Product Override" order item attribute.*/
    public static final String ATTRIB_OVERRIDE_RATE = "OverrideRate";
    /**String value of attributeName to set the Tracking Id "Lightweight Product Override" order item attribute.*/
    public static final String ATTRIB_TRACKING_ID = "TrackingId";
    /**String value of attributeName to set the TrackingIdServ "Lightweight Product Override" order item attribute.*/
    public static final String ATTRIB_TRACKING_ID_SERV = "TrackingIdServ";

    /**
     * Creates a new instance of C30ProductOverride.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ProductOverride(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ProductOverride(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30ProductOverride.class);
    }

    /**
     * Validates the item action id of the order item.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        
    	super.validateAttributes();
        
    	if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)){

            //String error = "Invalid item action id: (" + super.getItemActionId().toString() +
            //        ").  Item action id 10 must be used for a Product Override";
        }
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     */
    @Override
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException {

    	super.initializeAttributes();

    	//=================
    	// Data Attributes
    	//=================
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OVERRIDE_RATE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKAttribute attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_PRODUCT_OVERRIDE_MEMBER_TYPE);

    	//===================
    	// Output Attributes
    	//===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OVERRIDE_RATE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

    }

    /**
     * Creates the product override in Kenan FX using API TS.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the service order note
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected final C30SDKObject createFXObject() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	C30SDKObject lwo = null;
    	
        //create hashmap shell
        Map key = new HashMap();
        Map overrideRateData = new HashMap();
        overrideRateData.put("Key", key);

        //----------------------------------------------------------------------
        // Set the FX fields based on LWO attributes that have been configured
        // or explicitly set into the shell
        //----------------------------------------------------------------------
        setFXProductAttributes(overrideRateData);

        Map callResponse = this.queryData("ProductRateOverride", "Create", overrideRateData, getAccountServerId());
        Map productOverride = (HashMap) callResponse.get("ProductRateOverride");
        populateProductOverride(productOverride);

        lwo = this;
        
        return lwo;
	   
    }

    private void populateProductOverride(final Map productOverride) throws C30SDKInvalidAttributeException {
    	this.setAttributeValue(ATTRIB_ACTIVE_DT, ((HashMap) productOverride.get("Key")).get(ATTRIB_ACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRACKING_ID, ((HashMap) productOverride.get("Key")).get(ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TRACKING_ID_SERV, ((HashMap) productOverride.get("Key")).get(ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_OVERRIDE_RATE, productOverride.get(ATTRIB_OVERRIDE_RATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CURRENCY_CODE, productOverride.get(ATTRIB_CURRENCY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_INACTIVE_DT, productOverride.get(ATTRIB_INACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }

    /**
     * Method to set the override rate attributes into the override rate hashmap.
     * @param overrideRate the hashmap that will hold the override rate information
     * @throws C30SDKInvalidAttributeException
     */
    private void setFXProductAttributes(final Map overrideRate) throws C30SDKInvalidAttributeException {

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	((HashMap) overrideRate.get("Key")).put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	((HashMap) overrideRate.get("Key")).put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if(attributeName.equals(ATTRIB_TRACKING_ID) && attributeValue != null) {
            	((HashMap) overrideRate.get("Key")).put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_TRACKING_ID_SERV) && attributeValue != null) {
            	((HashMap) overrideRate.get("Key")).put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_OVERRIDE_RATE) && attributeValue != null) {
                overrideRate.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CURRENCY_CODE) && attributeValue != null) {
                overrideRate.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_INACTIVE_DT) && attributeValue != null) {
                overrideRate.put(attributeName, attributeValue);
            }
        }
    }

    /**
     * LW Product Overrides do not currently support child order items.  This is a placeholder
     * for potential future expansion of the LWO framework.
     * @return C30SDKObject the returned child order items.
     */
    @Override
	protected final C30SDKObject createChildOrderItems() { return null; }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {

    }

}
