/*
 * C30Address.java
 *
 * Created on December 8, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Wrapper class encapsulating a Kenan/FX Address object and its corresponding order item.
 * @author Tom Ansley
 */
public class C30Address extends C30SDKObject {

    private static Logger log = Logger.getLogger(C30Address.class);

    public static final String ATTRIB_ADDRESS1 = "Address1";
    public static final String ATTRIB_ADDRESS2 = "Address2";
    public static final String ATTRIB_ADDRESS3 = "Address3";
    public static final String ATTRIB_ADDRESS4 = "Address4";
    public static final String ATTRIB_ADDRESS_ID = "AddressId";
    public static final String ATTRIB_ADDRESS_LOCATION = "AddressLocation";
    public static final String ATTRIB_ADDRESS_TYPE_ID = "AddressTypeId";
    public static final String ATTRIB_CHANGE_DT = "ChangeDt";
    public static final String ATTRIB_CHANGE_WHO = "ChangeWho";
    public static final String ATTRIB_CITY = "City";
    public static final String ATTRIB_COUNTRY_CODE = "CountryCode";
    public static final String ATTRIB_COUNTY = "County";
    public static final String ATTRIB_EXTENDED_POSTAL_CODE = "ExtendedPostalCode";
    public static final String ATTRIB_EXTERNAL_ADDRESS_ID = "ExternalAddressId";
    public static final String ATTRIB_FRANCHISE_TAX_CODE = "FranchiseTaxCode";
    public static final String ATTRIB_FX_GEOCODE = "FxGeocode";
    public static final String ATTRIB_HOUSE_NUMBER = "HouseNumber";
    public static final String ATTRIB_HOUSE_NUMBER_SUFFIX = "HouseNumberSuffix";
    public static final String ATTRIB_IS_ACTIVE = "IsActive";
    public static final String ATTRIB_KEY_LINE_CODE = "KeyLineCode";
    public static final String ATTRIB_NEAREST_CROSSSTREET = "NearestCrossstreet";
    public static final String ATTRIB_OPTIONAL_ENDORSEMENT_LINE = "OptionalEndorsementLine";
    public static final String ATTRIB_POSTAL_CODE = "PostalCode";
    public static final String ATTRIB_POSTFIX_DIRECTIONAL = "PostfixDirectional";
    public static final String ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE = "PostnetAddressBlockBarcode";
    public static final String ATTRIB_POSTNET_BARCODE = "PostnetBarcode";
    public static final String ATTRIB_PREFIX_DIRECTIONAL = "PrefixDirectional";
    public static final String ATTRIB_STATE = "State";
    public static final String ATTRIB_STREET_NAME = "StreetName";
    public static final String ATTRIB_STREET_SUFFIX = "StreetSuffix";
    public static final String ATTRIB_UNIT_NO = "UnitNo";
    public static final String ATTRIB_UNIT_TYPE = "UnitType";
    public static final String ATTRIB_VERTEX_GEOCODE = "VertexGeocode";
    
    public static final Integer EXCEPTION_NO_SERVICE_ADDRESS_FOUND = new Integer(31001);
    public static final Integer EXCEPTION_MULTIPLE_SERVICE_ADDRESS_FOUND = new Integer(31002);
    
    public static final Integer ASSOC_STATUS_PENDING = new Integer(1);
    public static final Integer ASSOC_STATUS_CURRENT = new Integer(2);
    public static final Integer ASSOC_STATUS_OLD = new Integer(3);
    public static final Integer ASSOC_STATUS_CANCELLED = new Integer(4);
    
    public static final Integer ADDR_CATEGORY_SERVICE = new Integer(1);
    public static final Integer ADDR_CATEGORY_B_END = new Integer(2);
    public static final Integer ADDR_CATEGORY_SHIPPING = new Integer(3);

    public static final Integer ADDR_LOCATION_MASTER = new Integer(1);
    public static final Integer ADDR_LOCATION_LOCAL = new Integer(2);

    public static final Integer ADDR_TYPE_FREE_FORM = new Integer(2);
    public static final Integer ADDR_TYPE_NON_FREE_FORM = new Integer(1);
    public static final Integer ADDR_TYPE_TEMP_ADDRESS = new Integer(3);
    /**
     * Creates a new instance of C30ServiceAddressAssoc.  This order item and object is only available through
     * the framework.  It is used in conjunction with the C30Service object when moving services.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Address(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * Creates a new instance of C30ServiceAddressAssoc.  This order item and object is only available through
     * the framework.  It is used in conjunction with the C30Service object when moving services.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Address(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30Service.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Address Association Order Item.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is invalid.
     */
    @Override
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException{

        //=================
        // Input Attributes
        //=================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS1, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS2, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS3, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS4, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_LOCATION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_TYPE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CHANGE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CHANGE_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CITY, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COUNTRY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COUNTY, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EXTENDED_POSTAL_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EXTERNAL_ADDRESS_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_FRANCHISE_TAX_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_FX_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HOUSE_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HOUSE_NUMBER_SUFFIX, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_ACTIVE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_KEY_LINE_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEAREST_CROSSSTREET, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OPTIONAL_ENDORSEMENT_LINE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTAL_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTFIX_DIRECTIONAL, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTNET_BARCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREFIX_DIRECTIONAL, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_STATE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_STREET_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_STREET_SUFFIX, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_UNIT_NO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_UNIT_TYPE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VERTEX_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //===================
        // Output Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS1, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS2, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS3, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS4, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_LOCATION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_TYPE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CHANGE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CHANGE_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CITY, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COUNTRY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COUNTY, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EXTENDED_POSTAL_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EXTERNAL_ADDRESS_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_FRANCHISE_TAX_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_FX_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HOUSE_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HOUSE_NUMBER_SUFFIX, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_ACTIVE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_KEY_LINE_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEAREST_CROSSSTREET, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OPTIONAL_ENDORSEMENT_LINE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTAL_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTFIX_DIRECTIONAL, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_POSTNET_BARCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREFIX_DIRECTIONAL, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_STATE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_STREET_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_STREET_SUFFIX, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_UNIT_NO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_UNIT_TYPE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VERTEX_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }
    
    /** (non-Javadoc).
     * @return C30SDKObject the returned service that was processed.
     * @throws C30SDKObjectException 
     * @throws C30SDKObjectConnectionException 
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#process()
     */
    @Override
	public final C30SDKObject process() throws C30SDKInvalidAttributeException {
        C30SDKObject lwo = null;

        try {
        	this.findAddress();
    		lwo = this;
    	} catch (Exception e) {	
            C30SDKException er = new C30SDKException(exceptionResourceBundle.getString("LW-OBJ-010"),e, "LW-OBJ-010");
    		lwo = this.createExceptionMessage(er);
        }
    	
    	return lwo;

    }
    
    private void findAddress() throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKKenanFxCoreException {
    	
    	//Create a HashMap to contain the address filter's info
    	Map addressFilter = new HashMap();
    	Map key = new HashMap();
    	addressFilter.put("Key", key);
    	
    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	addressFilter.put("Fetch", new Boolean(true));

        HashMap attrFilter = null;
    	Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            
        	String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();
	    	
            if (!attributeName.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT) &&
            		!attributeName.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE) &&
            		!attributeName.equals(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE) &&
            		attributeValue != null) {
	            attrFilter = new HashMap();
		    	attrFilter.put("Equal", attributeValue);
		    	addressFilter.put(attributeName, attrFilter);
            }
        }
        Map callResponse = this.queryData("AdrAddress", "Find", addressFilter, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
        	String error = "No service addresses were found with the given information";
            throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-011"), "LW-OBJ-011");
        } else if (count > 1) {
        	String error = "Multiple service addresses were found with the given information";
        	throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-012"), "LW-OBJ-012");
        }

        Map address = (HashMap) ((Object[]) callResponse.get("AdrAddressList"))[0];
    	populateServiceAddress(address);
    }

    /**
     * Method which takes the returned hashmap from the API-TS call and populates the C30SDK object.
     * @throws C30SDKInvalidAttributeException thrown if an invalid attribute exists.
     */
    private void populateServiceAddress(Map address) throws C30SDKInvalidAttributeException {
        this.setAttributeValue(ATTRIB_ADDRESS1, address.get(ATTRIB_ADDRESS1), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ADDRESS2, address.get(ATTRIB_ADDRESS2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ADDRESS3, address.get(ATTRIB_ADDRESS3), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ADDRESS4, address.get(ATTRIB_ADDRESS4), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ADDRESS_ID, ((HashMap) address.get("Key")).get(ATTRIB_ADDRESS_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ADDRESS_LOCATION, address.get(ATTRIB_ADDRESS_LOCATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ADDRESS_TYPE_ID, address.get(ATTRIB_ADDRESS_TYPE_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CHANGE_DT, address.get(ATTRIB_CHANGE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CHANGE_WHO, address.get(ATTRIB_CHANGE_WHO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CITY, address.get(ATTRIB_CITY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COUNTRY_CODE, address.get(ATTRIB_COUNTRY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COUNTY, address.get(ATTRIB_COUNTY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_EXTENDED_POSTAL_CODE, address.get(ATTRIB_EXTENDED_POSTAL_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_EXTERNAL_ADDRESS_ID, address.get(ATTRIB_EXTERNAL_ADDRESS_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_FRANCHISE_TAX_CODE, address.get(ATTRIB_FRANCHISE_TAX_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_FX_GEOCODE, address.get(ATTRIB_FX_GEOCODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_HOUSE_NUMBER, address.get(ATTRIB_HOUSE_NUMBER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_HOUSE_NUMBER_SUFFIX, address.get(ATTRIB_HOUSE_NUMBER_SUFFIX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_IS_ACTIVE, address.get(ATTRIB_IS_ACTIVE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_KEY_LINE_CODE, address.get(ATTRIB_KEY_LINE_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_NEAREST_CROSSSTREET, address.get(ATTRIB_NEAREST_CROSSSTREET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_OPTIONAL_ENDORSEMENT_LINE, address.get(ATTRIB_OPTIONAL_ENDORSEMENT_LINE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_POSTAL_CODE, address.get(ATTRIB_POSTAL_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_POSTFIX_DIRECTIONAL, address.get(ATTRIB_POSTFIX_DIRECTIONAL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE, address.get(ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_POSTNET_BARCODE, address.get(ATTRIB_POSTNET_BARCODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PREFIX_DIRECTIONAL, address.get(ATTRIB_PREFIX_DIRECTIONAL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_STATE, address.get(ATTRIB_STATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_STREET_NAME, address.get(ATTRIB_STREET_NAME), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_STREET_SUFFIX, address.get(ATTRIB_STREET_SUFFIX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_UNIT_NO, address.get(ATTRIB_UNIT_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_UNIT_TYPE, address.get(ATTRIB_UNIT_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_VERTEX_GEOCODE, address.get(ATTRIB_VERTEX_GEOCODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }

    /**
     * Method which gets any data to be cached for further use by the object.
     * @throws C30SDKCacheException if there was an error while trying to populate the service cache.
     */
    @Override
	protected void loadCache() throws C30SDKCacheException {}

}

