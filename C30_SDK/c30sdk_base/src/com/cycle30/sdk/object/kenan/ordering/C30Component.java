/*
 * C30Component.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.cycle30.sdk.cache.C30SDKCacheableWrapper;
import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKCollection;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX Component object and its corresponding order item.
 * @author Joe Morales
 */
public class C30Component extends C30OrderItem {

    private static Logger log = Logger.getLogger(C30Component.class);

    //Component Attributes
    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID = "AccountExternalId";
    private static final String ATTRIB_ACTIVE_DT = "ActiveDt";
    public static final String ATTRIB_CONNECT_REASON = "ConnectReason";
    private static final String ATTRIB_COMPONENT_INST_ID = "ComponentInstId";
    private static final String ATTRIB_COMPONENT_INST_ID_SERV = "ComponentInstIdServ";
    public static final String ATTRIB_COMPONENT_ID = "ComponentId";
    public static final String ATTRIB_COMPONENT_OVERRIDE_RATE = "ComponentOverrideRate";
    private static final String ATTRIB_COMPONENT_STATUS = "ComponentStatus";
    public static final String ATTRIB_INACTIVE_DT = "InactiveDt";
    public static final String ATTRIB_PACKAGE_INST_ID = "PackageInstId";
    public static final String ATTRIB_PACKAGE_INST_ID_SERV = "PackageInstIdServ";
    public static final String ATTRIB_PACKAGE_ID = "PackageId";
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID = "ParentServiceInternalId";
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS = "ParentServiceInternalIdResets";
    public static final String ATTRIB_SERVICE_EXTERNAL_ID = "ServiceExternalId";
    private static final String ATTRIB_SERVICE_INTERNAL_ID = "ServiceInternalId";
    private static final String ATTRIB_SERVICE_INTERNAL_ID_SERV = "ServiceInternalIdServ";

    //Component Search Attributes
    public static final String ATTRIB_SEARCH_COMPONENT_ID = "SearchComponentId";
    public static final String ATTRIB_SEARCH_COMPONENT_INST_ID = "SearchComponentInstId";
    public static final String ATTRIB_SEARCH_COMPONENT_INST_ID_SERV = "SearchComponentInstIdServ";
    public static final String ATTRIB_SEARCH_CONNECT_REASON = "SearchConnectReason";
    public static final String ATTRIB_SEARCH_PACKAGE_INST_ID = "SearchPackageInstId";
    public static final String ATTRIB_SEARCH_PACKAGE_INST_ID_SERV = "SearchPackageInstIdServ";

    //Component Element Attributes
    private static final String ATTRIB_ASSOCIATION_ID = "AssociationId";
    private static final String ATTRIB_ASSOCIATION_ID_SERV = "AssociationIdServ";
    private static final String ATTRIB_ASSOCIATION_TYPE = "AssociationType";
    private static final String ATTRIB_COMPONENT_ELEMENT_STATUS = "ComponentElementStatus";

    public static final Integer COMPONENT_INACTIVE_STATUS = new Integer(0);

    public static final Integer COMP_ELEMENT_PRODUCT_ASSOC_TYPE = new Integer(1);
    public static final Integer COMP_ELEMENT_CONTRACT_ASSOC_TYPE = new Integer(2);
    public static final Integer COMPONENT_STATUS_ACTIVE = new Integer(1);

    private static final String COMPONENT_CACHE_SIGNATURE = "COMPONENT_MEMBER_CACHE";
    
    /* used for Searches */
    private C30SDKObject account = null;
    private static Integer startRecord = 1;
    private static Integer returnSetSize = 50;

    /**
     * Creates a new instance of C30OrderItem.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Component(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Component(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30Component.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the component.
     */
    protected final void initializeAttributes() throws C30SDKInvalidAttributeException {

        super.initializeAttributes();

        //===================
        // Data Attributes
        //===================
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_OVERRIDE_RATE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKAttribute attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_COMPONENT_MEMBER_TYPE);

        //===================
        // Search Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_COMPONENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_COMPONENT_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_COMPONENT_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PACKAGE_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PACKAGE_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //===================
        // Output Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        //===========================
        // Search Attributes
        //===========================
         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, java.lang.Integer.class);
         attribute.setValue(this.startRecord);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         
         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, java.lang.Integer.class);
         attribute.setValue(this.returnSetSize);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    
         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_COUNT, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_RETURNED, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_LAST_RECORD_RETURNED_NUMBER, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         
         if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, java.lang.Integer.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }
         
         if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, java.lang.Integer.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }
         
         if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }
         
         if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) 
              if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
                  attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, java.lang.Integer.class);
                  addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                  addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
              }
              
         if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, java.lang.String.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }
         
         if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, java.lang.Integer.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }

        
    }

    private void initializeComponentDisconnectAttributes() throws C30SDKInvalidAttributeException {
        C30SDKAttribute attribute = this.getAttribute(ATTRIB_CONNECT_REASON, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_COMPONENT_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_COMPONENT_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PACKAGE_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PACKAGE_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PACKAGE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);

    	//if we are setting the action ID
    	if (name.equals(this.getAttribute(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
    		if (value.equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
    			initializeComponentDisconnectAttributes();
    		}
    	}

    	if (name.equals(ATTRIB_SEARCH_COMPONENT_ID)) {
    		this.getAttribute(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
    	}
    	
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Integer)
     */
    public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
    	super.setAttributeValue(attributeName, attributeValue, type);

    	//if we are setting the action ID
    	if (attributeName.equals(ATTRIB_ITEM_ACTION_ID)) {
    		if (attributeValue.equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
    			initializeComponentDisconnectAttributes();
    		}
    	}

    	if (attributeName.equals(ATTRIB_SEARCH_COMPONENT_ID)) {
    		this.getAttribute(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
    	}
    	
    }

    /**
     * Method to validate the component.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        
    	super.validateAttributes();
        
    	if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID) &&
            !this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)){

            String error = "Invalid item action id: (" + this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ").  Item action id 10 or 30 must be used for a Component";
            throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-030"),"INVALID-ATTRIB-030");
        }
    }
    
 
          
    /**
     *   method to locate and use an account object during search for Component(s)
     * @return C30SDKObject
     * @throws C30SDKException 
     */
    private C30SDKObject getAssociatedAccount()
        throws C30SDKException
    {
        C30SDKObject anAccount = null;
        // add the attributes here - not previously part of the order-service_order processing only for the find 
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT, com.cycle30.sdk.core.framework.C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if(!isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            if(isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            {
                anAccount = getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30Account.class, new Object[0]);
                anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                anAccount = anAccount.process();
                if(anAccount instanceof C30Account)
                {
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, anAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, anAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                    if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) 
                         if  (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                             setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, anAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, anAccount, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                }
            } else
            {
               
                throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-013"),"LW-OBJ-013");
            }
        return anAccount;
    }
    
    
    private void validateComponentFindAttributes()
        throws C30SDKInvalidAttributeException
    {
        Integer SearchComponentInstId = null;
        Integer SearchComponentInstIdServ = null;
        Integer SearchPackageInstId = null;
        Integer SearchPackageInstIdServ = null;
        Integer ServiceInternalIdResets = null;
        Integer ServiceInternalId = null;

        
        
        super.validateFindAttributes();
       
  
        if (isAttribute(ATTRIB_SEARCH_COMPONENT_INST_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
               SearchComponentInstId = (Integer)getAttributeValue(ATTRIB_SEARCH_COMPONENT_INST_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        if (isAttribute(ATTRIB_SEARCH_COMPONENT_INST_ID_SERV,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
               SearchComponentInstIdServ = (Integer)getAttributeValue(ATTRIB_SEARCH_COMPONENT_INST_ID_SERV,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               
        if (isAttribute(ATTRIB_SEARCH_PACKAGE_INST_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
               SearchPackageInstId = (Integer)getAttributeValue(ATTRIB_SEARCH_PACKAGE_INST_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
        if (isAttribute(ATTRIB_SEARCH_PACKAGE_INST_ID_SERV,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
               SearchPackageInstIdServ = (Integer)getAttributeValue(ATTRIB_SEARCH_PACKAGE_INST_ID_SERV,C30SDKValueConstants.ATTRIB_TYPE_INPUT);                
        if (isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
           ServiceInternalId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
        if (isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
           ServiceInternalIdResets = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 

                      
       if ( SearchComponentInstId == null && SearchPackageInstId == null  && ServiceInternalId == null) 
       {
            throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-031"),"INVALID-ATTRIB-031");
       }   
       if ( ServiceInternalId != null && ServiceInternalIdResets == null) 
       {
             String error = "Invalid find action requested: Component Search criteria is missing, ServiceInternalIdResets";
             throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-031"),"INVALID-ATTRIB-031");
       }   
       if ( SearchComponentInstId != null && SearchComponentInstIdServ == null) 
        {
              String error = "Invalid find action requested: Component Search criteria is missing, SearchComponentInstIdServ";
              throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-031"),"INVALID-ATTRIB-031");
        }         
        if ( SearchPackageInstId != null && SearchPackageInstIdServ == null) 
        {
               String error = "Invalid find action requested: Component Search criteria is missing, SearchPackageInstIdServ";
               throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-031"),"INVALID-ATTRIB-031");
        }         

               
    }
    
    public C30SDKObject process() throws C30SDKInvalidAttributeException {
        log.debug("Starting C30Component.process()");

        //in case there is an error.
        String error = "creating C30SDK object: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKObject lwo = null;
        try {
            if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND)) 
            {
                setAttributesFromNameValuePairs();
                this.validateComponentFindAttributes();
                super.clearOrderAttributes();
                 if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
                    isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
                     getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
                 {

                        // check to see if we need to explicitly get an account
                        if (super.canServerIdCacheBeSetLocally())
                             super.setAccountServerIdCache();
                         else{
                                 if (this.account == null){
                                     account = getAssociatedAccount();
                                     if(account instanceof C30SDKExceptionMessage)
                                       return account;
                                 } 
                         }
                        super.setAccountServerId();
                }
                
                seachForComponents();
                setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                lwo = this;
            }
            else  // this is processed as an orderitem part of a service order
               lwo = super.process();
           
        } catch (C30SDKException e) {
                        lwo = this.createExceptionMessage(e);
                lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "Error " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                }
        log.debug("Finished C30Component.process()");
        return lwo;
    }

    private void populateComponent(final Map component) throws C30SDKInvalidAttributeException {
        this.setAttributeValue(ATTRIB_COMPONENT_INST_ID, ((HashMap) component.get("Key")).get(ATTRIB_COMPONENT_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_COMPONENT_INST_ID_SERV, ((HashMap) component.get("Key")).get(ATTRIB_COMPONENT_INST_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_ID, component.get(ATTRIB_PACKAGE_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_INST_ID, component.get(ATTRIB_PACKAGE_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_INST_ID_SERV, component.get(ATTRIB_PACKAGE_INST_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_VIEW_ID, new BigInteger("0"), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_ID, component.get(ATTRIB_COMPONENT_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, this.getAttributeValue(ATTRIB_COMPONENT_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        this.setAttributeValue(ATTRIB_COMPONENT_INST_ID, ((HashMap) component.get("Key")).get(ATTRIB_COMPONENT_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMPONENT_INST_ID_SERV, ((HashMap) component.get("Key")).get(ATTRIB_COMPONENT_INST_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ACTIVE_DT, component.get(ATTRIB_ACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMPONENT_ID, component.get(ATTRIB_COMPONENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMPONENT_STATUS, component.get(ATTRIB_COMPONENT_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONNECT_REASON, component.get(ATTRIB_CONNECT_REASON), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_INACTIVE_DT, component.get(ATTRIB_INACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_ID, component.get(ATTRIB_PACKAGE_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_INST_ID, component.get(ATTRIB_PACKAGE_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_INST_ID_SERV, component.get(ATTRIB_PACKAGE_INST_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, component.get(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID, component.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, component.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	this.setAttributeValue(ATTRIB_MEMBER_INST_ID, this.getAttributeValue(ATTRIB_COMPONENT_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, new BigInteger( ((Integer) this.getAttributeValue(ATTRIB_COMPONENT_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)).toString()), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            //this.setAttributeValue(ATTRIB_MEMBER_INST_ID, this.getAttributeValue(ATTRIB_COMPONENT_INST_ID, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_INPUT);
        }

    }
    
    /**
     * 
     * @param componentMap
     * @throws C30SDKInvalidAttributeException
     */
    private void populateComponentMap(Map componentMap)
                 throws C30SDKInvalidAttributeException
             {
             // jcw added: LWO Enhancements
                 for  ( Iterator it=componentMap.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                          Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                          String key = (String)entry.getKey (  ) ;     
                             Object value = entry.getValue (  ) ;
                             if (value != null)
                                 if (key.equalsIgnoreCase("KEY"))
                                 {    
                                   HashMap keyValue = (HashMap)entry.getValue (  ) ; 
                                   if (keyValue.containsKey(ATTRIB_COMPONENT_INST_ID)){
                                       if (isAttribute(ATTRIB_COMPONENT_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                           setAttributeValue(ATTRIB_COMPONENT_INST_ID, ((HashMap)componentMap.get("Key")).get(ATTRIB_COMPONENT_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                       if (isAttribute(ATTRIB_COMPONENT_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                           setAttributeValue(ATTRIB_COMPONENT_INST_ID_SERV, ((HashMap)componentMap.get("Key")).get(ATTRIB_COMPONENT_INST_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);    
                                   }
                                 }
                                 else
                                    if (key.equalsIgnoreCase(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID) ){
                                        if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                          setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                    }
                                    else 
                                        if (key.equalsIgnoreCase(ATTRIB_PARENT_SERVICE_INTERNAL_ID) )
                                            setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                     else 
                                         if (key.equalsIgnoreCase(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS) )
                                             setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);    
                                     else    
                                         if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                             setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);           
                                             if (value != null)
                                                 log.debug(  ( String ) key+" " +  value ) ; 
                         
                 }
                 
                 log.debug("Done populating component response!");
   }
   
   /**
   * Method to find a component.
   * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while finding the component
   * @throws C30SDKObjectException if there was a problem finding the component
   * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the component.
 * @throws C30SDKKenanFxCoreException 
   */
    private void seachForComponents() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {
                 log.debug("Inside C30Component.findComponent");
        int lastRecordReturned = 0; 
        //Create a HashMap to contain the CIEM filter's info
                 HashMap componentFilter = new HashMap();
                 Map attrFilter = new HashMap();
                 Map key = new HashMap();
                 componentFilter.put("Key", key);

                 //Set the Fetch flag at the root level,
                 //so the call will return all fields for found objects
                 componentFilter.put("Fetch", Boolean.TRUE);

                 //-------------------------------------------
                 // Only get services that are still active
                 //-------------------------------------------
                 attrFilter = new HashMap();
                 attrFilter.put("IsNull", Boolean.TRUE);
                 componentFilter.put(ATTRIB_INACTIVE_DT, attrFilter);

                 // Set Component Status Filter
                 attrFilter = new HashMap();
                 attrFilter.put("Equal", COMPONENT_STATUS_ACTIVE);
                 componentFilter.put(ATTRIB_COMPONENT_STATUS, attrFilter);

                 // Set Account Filter
                 attrFilter = new HashMap();
                 attrFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                 componentFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, attrFilter);

                 if  ((isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                          getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ) &
                    (isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                              getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )) { 
                         attrFilter = new HashMap();
                         attrFilter.put("Equal", (Integer) getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                         componentFilter.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, attrFilter);
                         
                         attrFilter = new HashMap();
                         attrFilter.put("Equal", (Integer) getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                         componentFilter.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, attrFilter);
                 }


                 //--------------------------------------------
                 // Construct Filter elements from SEARCH
                 // attributes that have been set or defaulted
                 //--------------------------------------------
                 Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
                 String attributeName = null;
                 Object attributeSearchValue = null;
                 while (iter.hasNext()) {
                     attributeName = (String) iter.next();

                     if (attributeName.indexOf("Search") != -1) {
                             attributeSearchValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

                             if (attributeSearchValue != null) {

                             attrFilter = new HashMap();
                             attrFilter.put("Equal",attributeSearchValue);

                                 //if we are searching on the key
                                 if (attributeName.equals(ATTRIB_SEARCH_COMPONENT_INST_ID) || attributeName.equals(ATTRIB_SEARCH_COMPONENT_INST_ID_SERV)) {
                                     key.put(attributeName.replaceFirst("Search", ""), attrFilter);
                                     componentFilter.put("Key", key);
                                 //if we are not searching on the key.
                                     } else {
                                     componentFilter.put(attributeName.replaceFirst("Search", ""), attrFilter);
                                     }

                             }
                     }
                 }

                 log.debug(componentFilter.toString());
                 Map callResponse = this.queryData("Component", "Find", componentFilter, getAccountServerId());

                 int count = ( (Integer) callResponse.get("Count")).intValue();

                 if(count == 0)
                 {
                   setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                   return;
                 }
                 
                 
                 Map components = (HashMap) ((Object[]) callResponse.get("ComponentList"))[0];
                 
                 int recordsProcessed = 0;
                 Integer lastPackageInstId = -1;
                 if (startRecord == 1)
                 {
                     populateComponentMap(components);
                     lastPackageInstId = (Integer)components.get(ATTRIB_PACKAGE_INST_ID);
                     if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                         setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                     lastRecordReturned = 1;
                     recordsProcessed++;
                 }

                 
                 // at this point we have more than a single account to load
                  C30Component cloneLwo = (C30Component)super.clone();
                  Integer aPackageInstId = -1;
                  if (startRecord > 1)
                    startRecord--;
                   for (int i=startRecord; i < count; i++)
                   {
                       if (recordsProcessed >= returnSetSize )
                         break;
                      // productPackage = (HashMap)((Object[])(Object[])callResponse.get("ProductPackageList"))[i]; 
                       components = (HashMap)((Object[])(Object[])callResponse.get("ComponentList"))[i]; 
                       aPackageInstId  = (Integer)components.get(ATTRIB_PACKAGE_INST_ID);
                       if (aPackageInstId.equals(lastPackageInstId))
                          continue;
                       lastPackageInstId = aPackageInstId;
                       // is this the first account we are processing?
                       if (recordsProcessed == 0){
                           populateComponentMap(components);
                         if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                               this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

                         this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                       }
                       else
                       {
                           C30Component lwo = (C30Component)cloneLwo.clone();
                           lwo.populateComponentMap(components);
                          // add to our list of peer objects
                           if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                 lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                           lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                           super.addC30SDKObject(lwo);
                           }
                       recordsProcessed++;
                       lastRecordReturned =  i + 1;
                   }
                 super.setProcessingAggregateTotalsAttributes(this, count, lastRecordReturned, recordsProcessed );
                 
                 
             }

    /**
     * Method to find a component.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while finding the component
     * @throws C30SDKObjectException if there was a problem finding the component
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the component.
     * @throws C30SDKKenanFxCoreException 
     */
    private void findComponent() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {
    	log.debug("Inside C30Component.findComponent");

    	//Create a HashMap to contain the CIEM filter's info
    	HashMap componentFilter = new HashMap();
    	Map attrFilter = new HashMap();
    	Map key = new HashMap();
    	componentFilter.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	componentFilter.put("Fetch", Boolean.TRUE);

    	//-------------------------------------------
    	// Only get services that are still active
    	//-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("IsNull", Boolean.TRUE);
    	componentFilter.put(ATTRIB_INACTIVE_DT, attrFilter);

    	// Set Component Status Filter
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", COMPONENT_STATUS_ACTIVE);
    	componentFilter.put(ATTRIB_COMPONENT_STATUS, attrFilter);

    	// Set Account Filter
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	componentFilter.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, attrFilter);

    	if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL))  {

    		//---------------------
    		// service internal id
    		//---------------------
    		attrFilter = new HashMap();
    		attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    		componentFilter.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, attrFilter);

    		//----------------------------
    		// service internal ID resets
    		//----------------------------
    		attrFilter = new HashMap();
    		attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    		componentFilter.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, attrFilter);
    	}

    	C30PackageInstanceList packageList = (C30PackageInstanceList) this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    	if(packageList.getLightweightPackageInstId( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) != null) {
    		attrFilter = new HashMap();
    		attrFilter.put("Equal", packageList.getLightweightPackageInstId( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)));
    		componentFilter.put(ATTRIB_SERVICE_INTERNAL_ID, attrFilter);
    	}

    	if(packageList.getLightweightPackageInstId( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) != null) {
    		attrFilter = new HashMap();
    		attrFilter.put("Equal", packageList.getLightweightPackageInstIdServ( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)));
    		componentFilter.put(ATTRIB_SERVICE_INTERNAL_ID_SERV, attrFilter);
    	}

        //--------------------------------------------
        // Construct Filter elements from SEARCH
        // attributes that have been set or defaulted
        //--------------------------------------------
        Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeSearchValue = null;
        while (iter.hasNext()) {
            attributeName = (String) iter.next();

            if (attributeName.indexOf("Search") != -1) {
	            attributeSearchValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	            if (attributeSearchValue != null) {

                    attrFilter = new HashMap();
                    attrFilter.put("Equal",attributeSearchValue);

	            	//if we are searching on the key
	            	if (attributeName.equals(ATTRIB_SEARCH_COMPONENT_INST_ID) || attributeName.equals(ATTRIB_SEARCH_COMPONENT_INST_ID_SERV)) {
	                    key.put(attributeName.replaceFirst("Search", ""), attrFilter);
	                    componentFilter.put("Key", key);
	            	//if we are not searching on the key.
		            } else {
	                    componentFilter.put(attributeName.replaceFirst("Search", ""), attrFilter);
		            }

	            }
            }
        }

    	Map callResponse = this.queryData("Component", "Find", componentFilter, getAccountServerId());

    	//get the count
    	int count = ( (Integer) callResponse.get("Count")).intValue();

    	if (count == 0) {
    		String error = "No active components match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    		if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
    			error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
    			      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		}
    		throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-014"),"LW-OBJ-014");

    	} else if (count > 1) {

    		String error = "Multiple components match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    		if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL))  {
    			error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
    				  + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		}

    	}

		Map component = (HashMap) ((Object[]) callResponse.get("ComponentList"))[0];
		populateComponent(component);
		this.setAttributeValue(ATTRIB_SEARCH_COMPONENT_ID, this.getAttributeValue(ATTRIB_COMPONENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	log.debug("Finished C30Component.findComponent");
    }

    /**
     * Creates the package component using the Kenan FX APIs.
     * @throws C30SDKInvalidConfigurationException if there was a configuration error while inititalizing the component
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the component
     * @throws C30SDKObjectException if there was a problem initializing the component
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the component.
     * @throws C30SDKKenanFxCoreException 
     */
    protected final C30SDKObject createFXObject() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {
        C30SDKObject lwo = null;
        
    	// Reload child order items if the component id has been overriden.
        Integer componentId = (Integer)((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(ATTRIB_COMPONENT_ID)).getValue();

        if (componentId != null && !componentId.equals(new Integer(-1))) {
            this.setAttributeValue(ATTRIB_SEARCH_COMPONENT_ID, componentId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        } else {
            if (this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
            		!this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(new Integer(-1))) {
                this.setAttributeValue(ATTRIB_SEARCH_COMPONENT_ID, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                this.setAttributeValue(ATTRIB_COMPONENT_ID, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
        }

        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {
            this.connectComponent();
        } else if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
            this.findComponent();
        }
        lwo = this;
        
        return lwo;
    }

    /**
     * Method to connect a component.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the component
     * @throws C30SDKKenanFxCoreException 
     */
    private void connectComponent() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        Map componentData = setFXComponentAttributes();
        Map callResponse = this.queryData("Component", "Create", componentData, getAccountServerId());
        Map component = (HashMap) callResponse.get("Component");
        populateComponent(component);

    }

    /**
     * Creates child order items for the Lightweight Component.  These are products or contracts that are members of
     * the package component.  The component element is also created to link the products and contracts to the component.
     * @return C30SDKObject the returned child order items.
     * @throws C30SDKException 
     */
    protected final C30SDKObject createChildOrderItems() throws C30SDKException {

    	//==================================================================================
    	//We need to meld the LWO and core information together.  So we do the following:
    	//1. If we have LWO child configuration then the child would already be created.
    	//   If this is the case then we remove that child from the child temp map.
    	//2. Once we have gone through all children we will have a map of those children
    	//   that need to be created by the framework.  Create the children.
    	//3. Once all children have been created for this component we process them.
    	//4. If we are disconnecting then we get all component element information
    	//5. If we are connecting then we create the component element information.
    	//==================================================================================

    	//get potential member information from the factory
    	Set members = this.getComponentMemberData( (Integer) this.getAttributeValue(ATTRIB_COMPONENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        Map memberMap = null;
        C30SDKObject memberLWO = null;
        C30SDKObject child = null;
        Iterator membersIter = members.iterator();

        //go through children and remove any mappings that already exist
        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {

        	child = this.getC30SDKObject(i);

        	//go through core configured members
        	while (membersIter.hasNext()) {

        		memberMap = (HashMap) membersIter.next();

        		//if we have a product
    			if (((HashMap) memberMap.get("Key")).get(C30Component.ATTRIB_MEMBER_TYPE).equals(new Integer(1))
    					&& child instanceof C30ProductElement) {

    				if (((HashMap) memberMap.get("Key")).get(C30Component.ATTRIB_MEMBER_ID).equals(child.getAttributeValue(C30ProductElement.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
    					&& child instanceof C30ProductElement) {
    					members.remove(memberMap);
    				}

        		//if we have a contract
    			} else if (((HashMap) memberMap.get("Key")).get(C30Component.ATTRIB_MEMBER_TYPE).equals(new Integer(2))
    					&& child instanceof C30ProductElement) {

    				if (((HashMap) memberMap.get("Key")).get(C30Component.ATTRIB_MEMBER_ID).equals(child.getAttributeValue(C30ProductElement.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
        					&& child instanceof C30ProductElement) {
        					members.remove(memberMap);
        				}


    			//if we have something else configured then throw an error.
    			} else {
    				throw new C30SDKInvalidConfigurationException(exceptionResourceBundle.getString("INVALID-CONF-008"),"INVALID-CONF-008");
    			}

        	}

        }

        membersIter = members.iterator();
        while (membersIter.hasNext()) {
        	memberMap = (HashMap) membersIter.next();

			//if we have a PRODUCT
			if (((HashMap) memberMap.get("Key")).get(C30Component.ATTRIB_MEMBER_TYPE).equals(new Integer(1))) {
				memberLWO = this.getFactory().createC30SDKObject(C30ProductElement.class, new Object[0]);
				memberLWO.setAttributeValue(C30ProductElement.ATTRIB_COMPONENT_ID, ((HashMap) memberMap.get("Key")).get(C30Component.ATTRIB_MEMBER_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				memberLWO.setAttributeValue(C30ProductElement.ATTRIB_MEMBER_TYPE, C30OrderItem.ORD_ITEM_PRODUCT_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			//if we have a CONTRACT
			} else {
				memberLWO = this.getFactory().createC30SDKObject(C30CustomerContract.class, new Object[0]);
				//memberLWO.setAttributeValue(C30CustomerContract.ATTRIB_CONTRACT_ID, ((HashMap) memberMap.get("Key")).get(C30Component.ATTRIB_MEMBER_ID), C30SDKObject.Constants.ATTRIB_TYPE_INPUT);
				memberLWO.setAttributeValue(C30ProductElement.ATTRIB_MEMBER_TYPE, C30OrderItem.ORD_ITEM_CONTRACT_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			}
			memberLWO.setAttributeValue(ATTRIB_ITEM_ACTION_ID, this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			memberLWO.setAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, ((HashMap) memberMap.get("Key")).get(C30OrderItem.ATTRIB_MEMBER_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			this.addC30SDKObject(memberLWO);

    	}

    	C30SDKObject children = this.getFactory().createC30SDKObject(C30SDKCollection.class, new Object[0]);
        //--------------------------------------------------------------------------------
        // If this is a component disconnect, get the component elements so the proper
        // contracts and products are found so they can be disconnected.
        //--------------------------------------------------------------------------------
        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
           this.getComponentElementInfo();
        }

        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
            C30SDKObject lwoi = (C30OrderItem) this.getC30SDKObject(i);
            lwoi.setAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            //-------------------------------------------------------------------------------
            // If this is a product or contract connect set the necesary fields to tie the
            // product or contract to the component (rating purposes)
            //-------------------------------------------------------------------------------
            if (lwoi.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {

                if (lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_PRODUCT_MEMBER_TYPE)) {
                        Integer componentId = (Integer)((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(ATTRIB_COMPONENT_ID)).getValue();
                        if (componentId != null) {
                            lwoi.setAttributeValue(C30ProductElement.ATTRIB_COMPONENT_ID, componentId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                        } else {
                            lwoi.setAttributeValue(C30ProductElement.ATTRIB_COMPONENT_ID, this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                        }
                        BigInteger rateOverride = (BigInteger)((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(ATTRIB_COMPONENT_OVERRIDE_RATE)).getValue();
                        if(rateOverride!=null) {
                            lwoi.setAttributeValue(C30ProductElement.ATTRIB_OVERRIDE_RATE, rateOverride, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                        }
                    lwoi.setAttributeValue(C30ProductElement.ATTRIB_IS_PART_OF_COMPONENT, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                } else if (lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONTRACT_MEMBER_TYPE)) {
                    lwoi.setAttributeValue(C30CustomerContract.ATTRIB_IS_PART_OF_COMPONENT, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                }
            }
            lwoi.setNameValuePairs(this.getNameValuePairMap());

            lwoi = lwoi.process();

            //make sure process() doesn't return an exception
            if (lwoi instanceof C30SDKExceptionMessage) {
				children = lwoi;
				break;
            } else {

            	children.addC30SDKObject(lwoi);

            	//------------------------------------------------------------------
                // Component elements are only added when adding new ones... For
            	// disconnects they are updated later in the order complete process.
                //-------------------------------------------------------------------
                if (lwoi.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {
                    this.createComponentElement(lwoi);
                }
            }
        }

        return children;
    }

    /**
     * Method which creates the component element in the database based on the lightweight order item.
     * @param lwoi the lightweight order item containing the element information
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the component element
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void createComponentElement(final C30SDKObject lwoi) throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        //create hashmap shell
        Map key = new HashMap();
        Map componentElementData = new HashMap();
        componentElementData.put("Key", key);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            key.put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            key.put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }

        if(lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_PRODUCT_MEMBER_TYPE)) {
            key.put(ATTRIB_ASSOCIATION_TYPE, COMP_ELEMENT_PRODUCT_ASSOC_TYPE);
        } else if(lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONTRACT_MEMBER_TYPE)) {
            key.put(ATTRIB_ASSOCIATION_TYPE, COMP_ELEMENT_CONTRACT_ASSOC_TYPE);
        }

        key.put(ATTRIB_ASSOCIATION_ID_SERV, lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_INST_ID2, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        componentElementData.put(ATTRIB_COMPONENT_ELEMENT_STATUS, new Integer(0));
        componentElementData.put(ATTRIB_COMPONENT_ID, (Integer)((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(ATTRIB_COMPONENT_ID)).getValue());
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            key.put(ATTRIB_ASSOCIATION_ID, lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	componentElementData.put(ATTRIB_COMPONENT_INST_ID, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            key.put(ATTRIB_ASSOCIATION_ID, new Integer(lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString()));
        	componentElementData.put(ATTRIB_COMPONENT_INST_ID, new Integer(this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString()));
        }
        
        componentElementData.put(ATTRIB_COMPONENT_INST_ID_SERV, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_INST_ID2, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        componentElementData.put(ATTRIB_PACKAGE_ID, this.getAttributeValue(ATTRIB_PACKAGE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        componentElementData.put(ATTRIB_PACKAGE_INST_ID, this.getAttributeValue(ATTRIB_PACKAGE_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        componentElementData.put(ATTRIB_PACKAGE_INST_ID_SERV, this.getAttributeValue(ATTRIB_PACKAGE_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        this.queryData("ComponentElement", "Create", componentElementData, getAccountServerId());
    }

    /**
     * Method to get component elements for this component.  This method is used to get all INSTANCE ID's
     * of the children.  This method is used when disconnecting the component and its children.
     * @throws C30SDKException 
     */
    private void getComponentElementInfo() throws C30SDKException {

        C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_lw_comp_elements");
		//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_comp_elements");
        try {

            //------------------------------------------
            // set up the stored procedure to be called
            //------------------------------------------
            call.addParam(new C30CustomParamDef("component_inst_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("component_inst_id_serv", C30CustomParamDef.INT, 3, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("member_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("association_type", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("association_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("association_id_serv", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
        } catch (Exception e) {
            
            throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-032"),"INVALID-ATTRIB-032");
        }

        //--------------------------------
        // set the input parameter values
        //--------------------------------
        ArrayList paramValues = new ArrayList();
        paramValues.add(new Integer(this.getAttributeValue(ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString()));
        paramValues.add(this.getAttributeValue(ATTRIB_MEMBER_INST_ID2, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        log.debug("COMP_INST_ID: " + this.getAttributeValue(ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        log.debug("COMP_INST_ID_SERV: " + this.getAttributeValue(ATTRIB_MEMBER_INST_ID2, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        //---------------------------------------
        // make the call and process the results
        //---------------------------------------
      //  C30KenanMWDataSource dataSource = this.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES, getAccountServerId());

        /*for (int i = 0; i < dataSource.getRowCount(); i++) {
            if (this.getFactory() != null) {
                Integer memberId = new Integer(dataSource.getValueAt(i,0).toString());
                Integer associationType = new Integer(dataSource.getValueAt(i,1).toString());
                Integer associationId = new Integer(dataSource.getValueAt(i,2).toString());
                Integer associationIdServ = new Integer(dataSource.getValueAt(i,3).toString());

                for (int j = 0; j < this.getC30SDKObjectCount(); j++) {
                    C30OrderItem lwoi = (C30OrderItem) this.getC30SDKObject(j);

                    if (lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_PRODUCT_MEMBER_TYPE)
                    		&& (associationType.equals(C30Component.COMP_ELEMENT_PRODUCT_ASSOC_TYPE)) &&
                       (lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(memberId))) {
                            lwoi.setAttributeValue(C30ProductElement.ATTRIB_SEARCH_IS_PART_OF_COMPONENT, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                            lwoi.setAttributeValue(C30ProductElement.ATTRIB_SEARCH_TRACKING_ID, associationId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                            lwoi.setAttributeValue(C30ProductElement.ATTRIB_SEARCH_TRACKING_ID_SERV, associationIdServ, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    } else if (lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONTRACT_MEMBER_TYPE) &&
                       (associationType.equals(C30Component.COMP_ELEMENT_CONTRACT_ASSOC_TYPE)) &&
                       (lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(memberId))) {
                            lwoi.setAttributeValue(C30CustomerContract.ATTRIB_SEARCH_IS_PART_OF_COMPONENT, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                            lwoi.setAttributeValue(C30CustomerContract.ATTRIB_SEARCH_TRACKING_ID, associationId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                            lwoi.setAttributeValue(C30CustomerContract.ATTRIB_SEARCH_TRACKING_ID_SERV, associationIdServ, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    }
                }
            }
        }
*/    }

    /**
     * Method to set the component attributes into the component hashmap.
     * @return Map the hashmap that will be used when making the API-TS call for connecting or disconnecting the component.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while trying to get package information
     * @throws C30SDKKenanFxCoreException 
     */
    private Map setFXComponentAttributes() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	Map componentData = new HashMap();

    	//=============================================================
    	// If we are doing a CONNECT then set the following attributes
    	//=============================================================
    	if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {

	        componentData.put(ATTRIB_COMPONENT_ID, (Integer)((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(ATTRIB_COMPONENT_ID)).getValue());
	        componentData.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

	        if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
	                componentData.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	                componentData.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }

	    	C30PackageInstanceList packageList = (C30PackageInstanceList) this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	    	if (packageList.getLightweightPackageId( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) != null ){
	            componentData.put(ATTRIB_PACKAGE_ID, packageList.getLightweightPackageId( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)));
	            componentData.put(ATTRIB_PACKAGE_INST_ID, packageList.getLightweightPackageInstId( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)));
	            componentData.put(ATTRIB_PACKAGE_INST_ID_SERV, packageList.getLightweightPackageInstIdServ( (Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)));
	        }

	        componentData.put(ATTRIB_COMPONENT_STATUS, COMPONENT_INACTIVE_STATUS);
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
		        componentData.put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
		        componentData.put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }
    	}

    	//=================================================================
    	// Override any attributes from configuration and name/value pairs
    	//=================================================================
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();
            if(attributeName.equals(ATTRIB_CONNECT_REASON) && attributeValue != null) {
                componentData.put(ATTRIB_CONNECT_REASON, (Integer) attributeValue);
            } else if(attributeName.equals(ATTRIB_PACKAGE_INST_ID) && attributeValue != null) {
                componentData.put(ATTRIB_PACKAGE_INST_ID, (Integer) attributeValue);
            } else if(attributeName.equals(ATTRIB_PACKAGE_INST_ID_SERV) && attributeValue != null) {
                componentData.put(ATTRIB_PACKAGE_INST_ID_SERV, (Integer) attributeValue);
            } else if(attributeName.equals(ATTRIB_PACKAGE_ID) && attributeValue != null) {
                componentData.put(ATTRIB_PACKAGE_ID, (Integer) attributeValue);
            }
        }

        //=================================================================================
        // If the package ID is not available then we need to get it using the package
        // instance ID.  This should really be done by API-TS automatically but it doesn't
        //=================================================================================
        if (componentData.get(ATTRIB_PACKAGE_ID) == null) {

        	HashMap tmpComponentData = new HashMap();
            Map key = new HashMap();
            tmpComponentData.put("Key", key);
            key.put(ATTRIB_PACKAGE_INST_ID, componentData.get(ATTRIB_PACKAGE_INST_ID));
            key.put(ATTRIB_PACKAGE_INST_ID_SERV, componentData.get(ATTRIB_PACKAGE_INST_ID_SERV));

            Map callResponse = this.queryData("ProductPackage", "Get", tmpComponentData, getAccountServerId());
            tmpComponentData = (HashMap) callResponse.get("ProductPackage");

            componentData.put(ATTRIB_PACKAGE_ID, tmpComponentData.get(ATTRIB_PACKAGE_ID));
        }

        return componentData;
    }

    /**
     * Method to get the component member data associated with the given component ID.  If there are no members found
     * then a call to middleware is made to ensure no members exist for the component ID.
     * @param componentId the component ID who's members are to be returned.
     * @return a list of Map data of component members associated with the component ID.
     * @throws C30SDKInvalidConfigurationException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKKenanFxCoreException 
     */
    private Set getComponentMemberData(final Integer componentId) throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKKenanFxCoreException {

    	Set members = new TreeSet();
    	Map packageComponentMember = null;

    	if ((this.getFactory().c30SdkFactoryCache.get(COMPONENT_CACHE_SIGNATURE)).getValue()!=null) {

	    	//Iterator i = ((Set) LightweightCache.getCache(COMPONENT_CACHE_SIGNATURE)).iterator();
	    	Iterator i = ((Set) (this.getFactory().c30SdkFactoryCache.get(COMPONENT_CACHE_SIGNATURE)).getValue()).iterator();

	    	while (i.hasNext()) {
	    		packageComponentMember = (HashMap) i.next();

	    		//if we have a member
	    		if (((HashMap) packageComponentMember.get("Key")).get(C30Component.ATTRIB_COMPONENT_ID).equals(componentId)) {
	    			members.add(packageComponentMember);
	    		}
	    	}

    	}

    	//if there are no members in the configuration then get them from middleware
    	if (members.size() == 0) {
    		members = this.getComponentMembers(componentId);
    	}

    	return members;
    }

    /**
     * Method that retrieves component member configuration which is used when creating components.  This method can be
     * used where the componentId is NULL (which then retrieves all component members), or with a value (which then
     * retrieves all component members that match the criteria).
     * @param componentId the component ID wh's members will be returned.
     * @return Set the returned set of component members.
     * @throws C30SDKObjectConnectionException if there was an exception while trying to connect to middleware.
     * @throws C30SDKInvalidAttributeException if there was an attribute exception while getting the component members.
     * @throws C30SDKKenanFxCoreException 
     */
    private Set getComponentMembers(final Integer componentId) throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
    	log.debug("Starting populateComponentDefinitions");

    	Map callResponse = null;
    	//Set componentMembersList = (Set) LightweightCache.getCache(COMPONENT_CACHE_SIGNATURE);
    	Set componentMembersList = (Set)(this.getFactory().c30SdkFactoryCache.get(COMPONENT_CACHE_SIGNATURE)).getValue();
	    Set newComponentMembersList = new HashSet();

    	//Create a HashMap that holds the info
	    HashMap packageComponentMemberFilter = new HashMap();
      	packageComponentMemberFilter.put("Fetch", Boolean.TRUE);

        Map key = new HashMap();
        packageComponentMemberFilter.put("Key", key);

    	HashMap componentIdFilter = new HashMap();
    	componentIdFilter.put("Equal", componentId);
    	((Map) packageComponentMemberFilter.get("Key")).put(C30Component.ATTRIB_COMPONENT_ID, componentIdFilter);

    	Map request = new HashMap();
        request.put("PackageComponentMember", packageComponentMemberFilter);

		//callResponse = this.queryData(ApiMappings.getCallName("PackageComponentMemberFind"), request, getAccountServerId());

		Object[] list = (Object[]) callResponse.get("PackageComponentMemberList");

		//create a set out of the list of members
		for (int i = 0; i < list.length; i++) {
			newComponentMembersList.add(list[i]);
		}

		//add the new members to the cached list as well.
		componentMembersList.addAll(newComponentMembersList);

    	log.debug("Finished populateComponentDefinitions");
    	return newComponentMembersList;
    }

    /**
     * Method which gets any data to be cached for further use by the object.
     * @throws C30SDKCacheException if an exception was thrown while trying to load the cache.  This exception
     * is very generic because this method allows the developer to load the cache using any mechanism they choose.
     */
    protected void loadCache() throws C30SDKCacheException {
    	try {
    		if ( (this.getFactory().c30SdkFactoryCache.get(COMPONENT_CACHE_SIGNATURE)).getValue()==null) {
    			//LightweightCache.setCache(COMPONENT_CACHE_SIGNATURE, new HashSet());

    			C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(COMPONENT_CACHE_SIGNATURE, new HashSet());
    			this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
    		}
    	} catch (Exception e) {
    		String error = "An error occurred while trying to load the component member cache\n";
        	throw new C30SDKCacheException(exceptionResourceBundle.getString("LW-CACHE-002"),e,"LW-CACHE-002");
    	}
    }

}
