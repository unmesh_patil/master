/*
 * C30CustomerContract.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKCollection;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30HashMapUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX customer contract object and its corresponding order item.
 * @author Joe Morales
 */
public class C30CustomerContract extends C30OrderItem {

    private static Logger log = Logger.getLogger(C30CustomerContract.class);

    /** These are all input/output attributes. */
    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID = "AccountExternalId";
    public static final String ATTRIB_ARCH_FLAG = "ArchFlag";
    public static final String ATTRIB_AVAIL_PERIODS = "AvailPeriods";
    public static final String ATTRIB_BILL_PERIOD = "BillPeriod";
    public static final String ATTRIB_COMMITMENT_AMOUNT = "CommitmentAmount";
    public static final String ATTRIB_COMMITMENT_CURRENCY_CODE = "CommitmentCurrencyCode";
    public static final String ATTRIB_COMMITMENT_REFERENCE = "CommitmentReference";
    public static final String ATTRIB_CONTRACT_ID = "ContractId";
    public static final String ATTRIB_CONTRACT_LEVEL = "ContractLevel";
    public static final String ATTRIB_CONTRACT_TYPE = "ContractType";
    public static final String ATTRIB_DISTRIBUTE_PAYBACK = "DistributePayback";
    public static final String ATTRIB_DURATION_OVERRIDE = "DurationOverride";
    public static final String ATTRIB_ELEMENT_ID = "ElementId";
    public static final String ATTRIB_END_DT = "EndDt";
    private static final String ATTRIB_EXTENDED_DATA = "ExtendedData";
    public static final String ATTRIB_FIRST_USE_DT = "FirstUseDt";
    public static final String ATTRIB_GRACE_PERIOD = "GracePeriod";
    public static final String ATTRIB_IN_USE = "InUse";
    public static final String ATTRIB_INTENDED_VIEW_EFFECTIVE_DT = "IntendedViewEffectiveDt";
    public static final String ATTRIB_IS_PART_OF_COMPONENT = "IsPartOfComponent";
    public static final String ATTRIB_IS_CUSTOM = "IsCustom";
    public static final String ATTRIB_LAST_USE_DT = "LastUseDt";
    public static final String ATTRIB_MIN_DURATION_DATE = "MinDurationDate";
    public static final String ATTRIB_MIN_NOTICE_DATE = "MinNoticeDate";
    public static final String ATTRIB_NEXT_EVAL_DATE = "NextEvalDate";
    public static final String ATTRIB_NOTICE_END_DT = "ProrateTgtRebatesOverride";
    public static final String ATTRIB_NOTICE_OVERRIDE = "NoticeOverride";
    public static final String ATTRIB_OWNING_ACCOUNT_INTERNAL_ID = "OwningAccountInternalId";
    private static final String ATTRIB_PARAM_ID = "ParamId";
    private static final String ATTRIB_PARAM_VALUE = "ParamValue";
    public static final String ATTRIB_PARENT_GROUP_NO = "ParentGroupNo";
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID = "ParentServiceInternalId";
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS = "ParentServiceInternalIdResets";
    public static final String ATTRIB_PREV_END_DT = "PrevEndDt";
    public static final String ATTRIB_PREV_EVAL_DATE = "PrevEvalDate";
    public static final String ATTRIB_PREV_VIEW_ID = "PrevViewId";
    public static final String ATTRIB_PRORATE_ACCOUNT_END_OVERRIDE = "ProrateAccountEndOverride";
    public static final String ATTRIB_PRORATE_ACCOUNT_START_OVERRIDE = "ProrateAccountStartOverride";
    public static final String ATTRIB_PRORATE_CONTRACT_END_OVERRIDE = "ProrateContractEndOverride";
    public static final String ATTRIB_PRORATE_CONTRACT_STRT_OVERRIDE = "ProrateContractStrtOverride";
    public static final String ATTRIB_PRORATE_INTERIM_BILL_OVERRIDE = "ProrateInterimBillOverride";
    public static final String ATTRIB_PRORATE_PERIOD_CHANGE_OVERRIDE = "ProratePeriodChangeOverride";
    public static final String ATTRIB_PRORATE_TGT_REBATES_OVERRIDE = "ProrateTgtRebatesOverride";
    public static final String ATTRIB_PRORATE_TGT_THRESHOLD_OVERRIDE = "ProrateTgtThresholdOverride";
    public static final String ATTRIB_ROLLOVER_PERIOD = "RolloverPeriod";
    public static final String ATTRIB_ROLLOVER_PERIOD_START = "RolloverPeriodStart";
    public static final String ATTRIB_SALES_CHANNEL_ID = "SalesChannelId";
    public static final String ATTRIB_SERVICE_EXTERNAL_ID = "ServiceExternalId";
    public static final String ATTRIB_START_DT = "StartDt";
    public static final String ATTRIB_START_BY_DT = "StartByDt";
    public static final String ATTRIB_TOTAL_ELEMENT_AMOUNT = "TotalElementAmount";
    public static final String ATTRIB_TOTAL_PERIODS = "TotalPeriods";
    public static final String ATTRIB_TOTAL_SAVINGS_AMOUNT = "TotalSavingsAmount";
    public static final String ATTRIB_TRACK_TOTAL_AMOUNTS = "TrackTotalAmounts";
    public static final String ATTRIB_TRACKING_ID = "TrackingId";
    public static final String ATTRIB_TRACKING_ID_SERV = "TrackingIdServ";
    public static final String ATTRIB_VIEW_CREATED_DT = "ViewCreatedDt";
    public static final String ATTRIB_VIEW_EFFECTIVE_DT = "ViewEffectiveDt";
    public static final String ATTRIB_VIEW_ID = "ViewId";
    public static final String ATTRIB_VIEW_STATUS = "ViewStatus";

    /** These are all contract type attributes. */
    private static final String ATTRIB_ACTIVATION_TYPE_ID_NRC = "ActivationTypeIdNrc";
    private static final String ATTRIB_LANGUAGE_CODE = "LanguageCode";
    private static final String ATTRIB_DURATION = "Duration";
    private static final String ATTRIB_DURATION_UNITS = "DurationUnits";

    /** These are all search attributes. */
    public static final String ATTRIB_SEARCH_BILL_PERIOD = "SearchBillPeriod";
    public static final String ATTRIB_SEARCH_COMMITMENT_REFERENCE = "SearchCommitmentReference";
    public static final String ATTRIB_SEARCH_CONTRACT_ID = "SearchContractId";
    public static final String ATTRIB_SEARCH_DISTRIBUTE_PAYBACK = "SearchDistributePayback";
    public static final String ATTRIB_SEARCH_DURATION_OVERRIDE = "SearchDurationOverride";
    public static final String ATTRIB_SEARCH_ELEMENT_ID = "SearchElementId";
    public static final String ATTRIB_SEARCH_GRACE_PERIOD = "SearchGracePeriod";
    public static final String ATTRIB_SEARCH_IS_CUSTOM = "SearchIsCustom";
    public static final String ATTRIB_SEARCH_IS_PART_OF_COMPONENT = "SearchIsPartOfComponent";
    public static final String ATTRIB_SEARCH_MIN_DURATION_DATE = "SearchMinDurationDate";
    public static final String ATTRIB_SEARCH_MIN_NOTICE_DATE = "SearchMinNoticeDate";
    public static final String ATTRIB_SEARCH_NOTICE_END_DT = "SearchNoticeEndDt";
    public static final String ATTRIB_SEARCH_NOTICE_OVERRIDE = "SearchNoticeOverride";
    public static final String ATTRIB_SEARCH_PARENT_GROUP_NO = "SearchParentGroupNo";
    public static final String ATTRIB_SEARCH_PREV_END_DT = "SearchPrevEndDt";
    public static final String ATTRIB_SEARCH_PRORATE_CONTRACT_STRT_OVERRIDE = "SearchProrateContractStrtOverride";
    public static final String ATTRIB_SEARCH_PRORATE_CONTRACT_END_OVERRIDE = "SearchProrateContractEndOverride";
    public static final String ATTRIB_SEARCH_PRORATE_INTERIM_BILL_OVERRIDE = "SearchProrateInterimBillOverride";
    public static final String ATTRIB_SEARCH_PRORATE_PERIOD_CHANGE_OVERRIDE = "SearchProratePeriodChangeOverride";
    public static final String ATTRIB_SEARCH_PRORATE_ACCOUNT_START_OVERRIDE = "SearchProrateAccountStartOverride";
    public static final String ATTRIB_SEARCH_PRORATE_ACCOUNT_END_OVERRIDE = "SearchProrateAccountEndOverride";
    public static final String ATTRIB_SEARCH_PRORATE_TGT_THRESHOLD_OVERRIDE = "SearchProrateTgtThresholdOverride";
    public static final String ATTRIB_SEARCH_PRORATE_TGT_REBATES_OVERRIDE = "SearchProrateTgtRebatesOverride";
    public static final String ATTRIB_SEARCH_ROLLOVER_PERIOD = "SearchRolloverPeriod";
    public static final String ATTRIB_SEARCH_ROLLOVER_PERIOD_START = "SearchRolloverPeriodStart";
    public static final String ATTRIB_SEARCH_SALES_CHANNEL_ID = "SearchSalesChannelId";
    public static final String ATTRIB_SEARCH_START_BY_DT = "SearchStartByDt";
    public static final String ATTRIB_SEARCH_TOTAL_PERIODS = "SearchTotalPeriods";
    public static final String ATTRIB_SEARCH_TRACK_TOTAL_AMOUNTS = "SearchTrackTotalAmounts";
    public static final String ATTRIB_SEARCH_TRACKING_ID = "SearchTrackingId";
    public static final String ATTRIB_SEARCH_TRACKING_ID_SERV = "SearchTrackingIdServ";
    public static final String ATTRIB_SEARCH_VIEW_ID = "SearchViewId";

    public static final Integer CONTRACT_SERVICE_LEVEL = new Integer(3);
    public static final Integer CONTRACT_ACCOUNT_LEVEL = new Integer(1);

    public static final Integer CONTRACT_TYPE_DURATION_UNITS_DAYS = new Integer(140);
    public static final Integer CONTRACT_TYPE_DURATION_UNITS_WEEKS = new Integer(150);
    public static final Integer CONTRACT_TYPE_DURATION_UNITS_MONTHS = new Integer(160);
    public static final Integer CONTRACT_TYPE_DURATION_UNITS_QUARTERS = new Integer(170);
    public static final Integer CONTRACT_TYPE_DURATION_UNITS_YEARS = new Integer(180);
    public static final Integer CONTRACT_TYPE_DURATION_UNITS_BILL_CYCLE = new Integer(-3);
    public static final Integer CONTRACT_TYPE_DURATION_UNITS_UNSPECIFIED = new Integer(-1);

    public static final Integer CONTRACT_TYPE_STANDARD_PROMOTION = new Integer(6);
    public static final Integer CONTRACT_TYPE_COMMITMENT = new Integer(7);
    public static final Integer CONTRACT_TYPE_FREE_USAGE = new Integer(8);
    public static final Integer CONTRACT_TYPE_HQ_CONTRACT = new Integer(9);
    public static final Integer CONTRACT_TYPE_HIERARCHY_BRANCH = new Integer(10);
    public static final Integer CONTRACT_TYPE_GLOBAL = new Integer(11);
    public static final Integer CONTRACT_TYPE_HISTORIC_DISCOUNT = new Integer(12);
    public static final Integer CONTRACT_TYPE_ROLLOVER = new Integer(13);
    public static final Integer CONTRACT_TYPE_HISTORIC_PAYBACK = new Integer(14);
    public static final Integer CONTRACT_TYPE_PAYG_UNIT_CREDIT = new Integer(15);
    public static final Integer CONTRACT_TYPE_PAYG_ROLLOVER_UC = new Integer(16);

    public static final Integer ASSOC_TYPE_NONE = new Integer(0);
    public static final Integer ASSOC_TYPE_RC = new Integer(4);
    public static final Integer ASSOC_TYPE_COM_REF = new Integer(5);
    public static final Integer ASSOC_TYPE_INV = new Integer(6);

    private Map contractType;
    private Map contract;

    /**
     * Creates a new instance of C30CustomerContract.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30CustomerContract(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30CustomerContract(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30CustomerContract.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the customer contract.
     */
    protected final void initializeAttributes() throws C30SDKInvalidAttributeException{

    	super.initializeAttributes();

        this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_PERIOD, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMMITMENT_REFERENCE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_DISTRIBUTE_PAYBACK, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_DURATION_OVERRIDE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_GRACE_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_PART_OF_COMPONENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NOTICE_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NOTICE_OVERRIDE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_GROUP_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_CONTRACT_STRT_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_CONTRACT_END_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_INTERIM_BILL_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_PERIOD_CHANGE_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_ACCOUNT_START_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_ACCOUNT_END_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_TGT_THRESHOLD_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_TGT_REBATES_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ROLLOVER_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ROLLOVER_PERIOD_START, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_START_BY_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TOTAL_PERIODS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACK_TOTAL_AMOUNTS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKAttribute attribute = new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class);
        attribute.setValue(null);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class);
        attribute.setValue(null);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(ATTRIB_VIEW_ID, BigInteger.class);
        attribute.setValue(null);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_CONTRACT_MEMBER_TYPE);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.addAttribute(new C30SDKAttribute(ATTRIB_IS_CUSTOM, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            this.addAttribute(new C30SDKAttribute(ATTRIB_MIN_DURATION_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            this.addAttribute(new C30SDKAttribute(ATTRIB_MIN_NOTICE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        //===================
        // Search Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILL_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_COMMITMENT_REFERENCE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONTRACT_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_DISTRIBUTE_PAYBACK, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_DURATION_OVERRIDE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_GRACE_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_IS_PART_OF_COMPONENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_MIN_DURATION_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_MIN_NOTICE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PREV_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_NOTICE_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_NOTICE_OVERRIDE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_GROUP_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PREV_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_CONTRACT_STRT_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_CONTRACT_END_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_INTERIM_BILL_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_PERIOD_CHANGE_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_ACCOUNT_START_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_ACCOUNT_END_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_TGT_THRESHOLD_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRORATE_TGT_REBATES_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ROLLOVER_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ROLLOVER_PERIOD_START, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_START_BY_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TOTAL_PERIODS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRACK_TOTAL_AMOUNTS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //===================
        // Output Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ARCH_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_AVAIL_PERIODS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_PERIOD, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMMITMENT_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMMITMENT_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMMITMENT_REFERENCE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_LEVEL, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_DISTRIBUTE_PAYBACK, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_DURATION_OVERRIDE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_FIRST_USE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_GRACE_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IN_USE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_PART_OF_COMPONENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_LAST_USE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEXT_EVAL_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NOTICE_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NOTICE_OVERRIDE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OWNING_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_GROUP_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_END_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_EVAL_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_ACCOUNT_END_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_ACCOUNT_START_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_CONTRACT_END_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_CONTRACT_STRT_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_INTERIM_BILL_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_PERIOD_CHANGE_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_TGT_REBATES_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PRORATE_TGT_THRESHOLD_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ROLLOVER_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ROLLOVER_PERIOD_START, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_START_BY_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_START_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TOTAL_ELEMENT_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TOTAL_PERIODS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TOTAL_SAVINGS_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACK_TOTAL_AMOUNTS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_CREATED_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.addAttribute(new C30SDKAttribute(ATTRIB_IS_CUSTOM, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.addAttribute(new C30SDKAttribute(ATTRIB_MIN_DURATION_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.addAttribute(new C30SDKAttribute(ATTRIB_MIN_NOTICE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
    }

    /**
     * This method gets called if the action ID of the customer contract is ORD_ITEM_CHANGE_ITEM_ACTION_ID.
     * @throws C30SDKInvalidAttributeException if an error occurred updating the attributes.
     */
    private void initializeCustomerContractChangeAttributes() throws C30SDKInvalidAttributeException {

    	C30SDKAttribute attribute = this.getAttribute(ATTRIB_IS_PART_OF_COMPONENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

    }

    /**
     * This method gets called if the action ID of the customer contract is ORD_ITEM_DISCONNECT_ITEM_ACTION_ID.
     * @throws C30SDKInvalidAttributeException if an error occurred updating the attributes.
     */
    private void initializeCustomerContractDisconnectAttributes() throws C30SDKInvalidAttributeException {
        C30SDKAttribute attribute = this.getAttribute(ATTRIB_CONTRACT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PARENT_GROUP_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PREV_END_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_GRACE_PERIOD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_TOTAL_PERIODS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_SALES_CHANNEL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_TRACK_TOTAL_AMOUNTS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_ROLLOVER_PERIOD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_ROLLOVER_PERIOD_START, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_BILL_PERIOD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_DISTRIBUTE_PAYBACK, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PRORATE_CONTRACT_STRT_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PRORATE_CONTRACT_END_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PRORATE_INTERIM_BILL_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PRORATE_ACCOUNT_START_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PRORATE_ACCOUNT_END_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PRORATE_TGT_THRESHOLD_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_PRORATE_TGT_REBATES_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_START_BY_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_NOTICE_END_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_DURATION_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_NOTICE_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_ELEMENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_COMMITMENT_REFERENCE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_IS_PART_OF_COMPONENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Integer)
     */
    public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
    	super.setAttributeValue(attributeName, attributeValue, type);

    	//if we are setting the action ID
    	if (attributeName.equals(ATTRIB_ITEM_ACTION_ID)) {
    		if (attributeValue.equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
    			initializeCustomerContractDisconnectAttributes();
    		} else if (attributeValue.equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID)) {
    			initializeCustomerContractChangeAttributes();
    		}
    	}

    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);

    	//if we are setting the member ID then get the extended data.
    	if (name.equals(this.getAttribute(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
    		super.loadExtendedDataAttributes("CUSTOMER_CONTRACT_VIEW", (Integer) value);

    	//if we are setting the action ID
    	} else if (name.equals(this.getAttribute(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
    		if (value.equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
    			initializeCustomerContractDisconnectAttributes();
    		} else if (value.equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID)) {
    			initializeCustomerContractChangeAttributes();
    		}
    	}

    }

    /**
     * Validates the item action id of the order item.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        super.validateAttributes();

        //----------------------------------------------------------
        // Make sure we only create child order items on a CONNECT.
        //----------------------------------------------------------
        if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {

        	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
        		if (this.getC30SDKObject(i) instanceof C30OrderItem) {
                    String error = "Lightweight Customer Contract can only have child order items when connecting new contracts.";
                    error = error + " Error on contract with ContractType = " + this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    throw new C30SDKInvalidAttributeException(error);
        		}
        	}
        }

        if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID) &&
            !this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID) &&
            !this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)){

            //String error = "Invalid item action id: (" + super.getItemActionId().toString() +
            //        ").  Item action id 10, 20 or 30 must be used for a Customer Contract";
        }
    }

    /**
     * Private method which gets the contract associated with the service order.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the customer contract
     * @throws C30SDKObjectException if there was a problem initializing the customer contract
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void findContract() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        //Create a HashMap to contain the CIEM filter's info
    	HashMap contractFilter = new HashMap();
    	Map attrFilter = new HashMap();
        Map key = new HashMap();
        contractFilter.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	contractFilter.put("Fetch", Boolean.TRUE);

        //-----------------------------
        // Only get the current view
        //-----------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(2));
    	contractFilter.put(ATTRIB_VIEW_STATUS, attrFilter);

        //-----------------------------
        // Set the contract type that is specified for the order item
        //-----------------------------
        //---------------------------------------------------
        // Set the Element ID that is specified for contract
        //---------------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	contractFilter.put(ATTRIB_CONTRACT_TYPE, attrFilter);

        //-------------------------------------------
        // only get contracts owned by the parent SI if it is service level.
        //-------------------------------------------
        if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL))  {
            //---------------------
            // service internal id
            //---------------------
        	attrFilter = new HashMap();
        	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	contractFilter.put("ServiceInternalId", attrFilter);

            //----------------------------
            // service internal ID resets
            //----------------------------
        	attrFilter = new HashMap();
        	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	contractFilter.put("ServiceInternalIdResets", attrFilter);
        }

        //--------------------------------------------
        // Construct Filter elements from SEARCH
        // attributes that have been set or defaulted
        //--------------------------------------------
        Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeSearchValue = null;
        while (iter.hasNext()) {
            attributeName = (String) iter.next();

            if (attributeName.indexOf("Search") != -1) {
	            attributeSearchValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	            if (attributeSearchValue != null) {

                    attrFilter = new HashMap();
                    attrFilter.put("Equal",attributeSearchValue);

	            	//if we are searching on the key
	            	if (attributeName.equals(ATTRIB_SEARCH_VIEW_ID)) {
	                    key.put(attributeName.replaceFirst("Search", ""), attrFilter);
	                    contractFilter.put("Key", key);
	            	//if we are not searching on the key.
		            } else {
	                    contractFilter.put(attributeName.replaceFirst("Search", ""), attrFilter);
		            }

	            }
            }
        }

	    Map callResponse = this.queryData("CustomerContract", "FindWithExtendedData", contractFilter, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
            String error = "No active contracts match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
                error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
                      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            throw new C30SDKObjectException(error);

        } else if (count > 1) {
            String error = "Multiple contracts match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL))  {
                error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
                      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            throw new C30SDKObjectException(error);
        }
        contract = (HashMap) ((Object[]) callResponse.get("CustomerContractList"))[0];
        populateContract();
    }

    private void populateContract() throws C30SDKInvalidAttributeException {
    	this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, contract.get(ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.setAttributeValue(ATTRIB_TRACKING_ID, contract.get(ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.setAttributeValue(ATTRIB_TRACKING_ID_SERV, contract.get(ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) contract.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        this.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, contract.get(ATTRIB_ACCOUNT_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ARCH_FLAG, contract.get(ATTRIB_ARCH_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_AVAIL_PERIODS, contract.get(ATTRIB_AVAIL_PERIODS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_BILL_PERIOD, contract.get(ATTRIB_BILL_PERIOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMMITMENT_AMOUNT, contract.get(ATTRIB_COMMITMENT_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMMITMENT_CURRENCY_CODE, contract.get(ATTRIB_COMMITMENT_CURRENCY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMMITMENT_REFERENCE, contract.get(ATTRIB_COMMITMENT_REFERENCE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTRACT_ID, contract.get(ATTRIB_CONTRACT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTRACT_LEVEL, contract.get(ATTRIB_CONTRACT_LEVEL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTRACT_TYPE, contract.get(ATTRIB_CONTRACT_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CREATE_DT, contract.get(ATTRIB_CREATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_DISTRIBUTE_PAYBACK, contract.get(ATTRIB_DISTRIBUTE_PAYBACK), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_DURATION_OVERRIDE, contract.get(ATTRIB_DURATION_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ELEMENT_ID, contract.get(ATTRIB_ELEMENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_END_DT, contract.get(ATTRIB_END_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_FIRST_USE_DT, contract.get(ATTRIB_FIRST_USE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_GRACE_PERIOD, contract.get(ATTRIB_GRACE_PERIOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_IN_USE, contract.get(ATTRIB_IN_USE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, contract.get(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_IS_PART_OF_COMPONENT, contract.get(ATTRIB_IS_PART_OF_COMPONENT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_LAST_USE_DT, contract.get(ATTRIB_LAST_USE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_NEXT_EVAL_DATE, contract.get(ATTRIB_NEXT_EVAL_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_NOTICE_END_DT, contract.get(ATTRIB_NOTICE_END_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_NOTICE_OVERRIDE, contract.get(ATTRIB_NOTICE_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_OWNING_ACCOUNT_INTERNAL_ID, contract.get(ATTRIB_OWNING_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, contract.get(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_GROUP_NO, contract.get(ATTRIB_PARENT_GROUP_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID, contract.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, contract.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PREV_END_DT, contract.get(ATTRIB_PREV_END_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PREV_EVAL_DATE, contract.get(ATTRIB_PREV_EVAL_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PREV_VIEW_ID, contract.get(ATTRIB_PREV_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_ACCOUNT_END_OVERRIDE, contract.get(ATTRIB_PRORATE_ACCOUNT_END_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_ACCOUNT_START_OVERRIDE, contract.get(ATTRIB_PRORATE_ACCOUNT_START_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_CONTRACT_END_OVERRIDE, contract.get(ATTRIB_PRORATE_CONTRACT_END_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_CONTRACT_STRT_OVERRIDE, contract.get(ATTRIB_PRORATE_CONTRACT_STRT_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_INTERIM_BILL_OVERRIDE, contract.get(ATTRIB_PRORATE_INTERIM_BILL_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_PERIOD_CHANGE_OVERRIDE, contract.get(ATTRIB_PRORATE_PERIOD_CHANGE_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_TGT_REBATES_OVERRIDE, contract.get(ATTRIB_PRORATE_TGT_REBATES_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PRORATE_TGT_THRESHOLD_OVERRIDE, contract.get(ATTRIB_PRORATE_TGT_THRESHOLD_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ROLLOVER_PERIOD, contract.get(ATTRIB_ROLLOVER_PERIOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ROLLOVER_PERIOD_START, contract.get(ATTRIB_ROLLOVER_PERIOD_START), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_SALES_CHANNEL_ID, contract.get(ATTRIB_SALES_CHANNEL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_START_BY_DT, contract.get(ATTRIB_START_BY_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_START_DT, contract.get(ATTRIB_START_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TOTAL_ELEMENT_AMOUNT, contract.get(ATTRIB_TOTAL_ELEMENT_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TOTAL_PERIODS, contract.get(ATTRIB_TOTAL_PERIODS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TOTAL_SAVINGS_AMOUNT, contract.get(ATTRIB_TOTAL_SAVINGS_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TRACK_TOTAL_AMOUNTS, contract.get(ATTRIB_TRACK_TOTAL_AMOUNTS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TRACKING_ID, contract.get(ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TRACKING_ID_SERV, contract.get(ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_VIEW_CREATED_DT, contract.get(ATTRIB_VIEW_CREATED_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_VIEW_EFFECTIVE_DT, contract.get(ATTRIB_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) contract.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_VIEW_STATUS, contract.get(ATTRIB_VIEW_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	this.setAttributeValue(ATTRIB_MEMBER_INST_ID, contract.get(ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, new BigInteger( ((Integer) this.getAttributeValue(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)).toString()), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            this.setAttributeValue(ATTRIB_IS_CUSTOM, contract.get(ATTRIB_IS_CUSTOM), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_MIN_DURATION_DATE, contract.get(ATTRIB_MIN_DURATION_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_MIN_NOTICE_DATE, contract.get(ATTRIB_MIN_NOTICE_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
    }

    /**
     * Creates child order items for the LW Contract.  LW Contracts support order items
     * to represent activation NRCs and products.
     * @return C30SDKObject the returned child order items.
     * @throws C30SDKException 
     */
    protected final C30SDKObject createChildOrderItems() throws C30SDKException {
        //-----------------------------------------------
        // To find out what NRC and products we need we
        // have to look at the contract type configured.
        //-----------------------------------------------
    	if (contractType == null) {
    		getContractType();
    	}

        //--------------------------------------------------------------------------------------------------------
        // To meld the CORE configured NRC's/Products with the LWO configured items we need to do a few things:
        // 1.  Check that if an NRC is configured in LWO that a configuration exists in the contract type.
        // 2.  Check that if a product is configured in LWO that a configuration exists in the contract type.
        // 3.  Fail the configuration if any LWO configuration exists for any other order item type.
        // 4.  Create the NRC's/Products based on both the LWO and the CORE configuration.
        // 5.  Regardless of whether anything is configured in LWO, ensure all core configured items are created.
        //--------------------------------------------------------------------------------------------------------
        C30SDKObject lwoi = null;
        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {

        	lwoi = this.getC30SDKObject(i);

        	//there could be other stuff in the list that we don't want to look at.
        	if (lwoi instanceof C30OrderItem) {

	        	//-------------------------
	            //If LWO member type = NRC
	            //-------------------------
	            if (lwoi.getAttributeValue(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_NRC_MEMBER_TYPE)) {
	            	//If there is a CORE configured NRC
	            	if (contractType.get(ATTRIB_ACTIVATION_TYPE_ID_NRC) != null &&
	            			lwoi.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals( (Integer) contractType.get(ATTRIB_ACTIVATION_TYPE_ID_NRC))) {

	                    lwoi.setAttributeValue(C30ProductElement.ATTRIB_CONTRACT_ASSOCIATION_TYPE, ASSOC_TYPE_RC.toString(), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	                //if there is no CORE configured NRC then throw an error.
	            	} else {
	                    String error = "Lightweight Customer Contract does not have an NRC member configured for it that can be configured or " +
	                    			   "the member ID of the LWO configured NRC does not match that of the configured contract.";
	                    error = error + " ContractType = " + contractType.get("CommitmentReference") + ", Member ID = " + this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	                    throw new C30SDKInvalidConfigurationException(error);
	            	}

	            //-----------------------------
	            //if LWO member type = Product
	            //-----------------------------
	            } else if (lwoi.getAttributeValue(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_PRODUCT_MEMBER_TYPE)) {
	            	//If there is a CORE configured product
	                if (contractType.get(ATTRIB_COMMITMENT_REFERENCE) != null &&
	                		lwoi.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals( (Integer) contractType.get(ATTRIB_COMMITMENT_REFERENCE))) {

	                    lwoi.setAttributeValue(C30ProductElement.ATTRIB_CONTRACT_ASSOCIATION_TYPE, ASSOC_TYPE_COM_REF, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	                //if there is no CORE configured product then throw an error.
	                } else {
	                    String error = "Lightweight Customer Contract does not have a product member configured for it that can be configured or " +
	     			   				   "the member ID of the LWO configured product does not match that of the configured contract.";
	                    error = error + " ContractType = " + contractType.get("CommitmentReference") + ", Member ID = " + this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	                    throw new C30SDKInvalidConfigurationException(error);
	                }

	            //--------------------------
	            // If LWO member is invalid
	            //--------------------------
	            } else {
	                String error = "The customer contract cannot have a child order item of the type configured.";
	                error = error + " Member Type = " + lwoi.getAttributeValue(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	                throw new C30SDKInvalidConfigurationException(error);
	            }
        	}

        }

        //-------------------------------------------------------------------
        // Now that we have checked the items that have been configured in
        // LWO we need to create those items that are not configured in LWO.
        //-------------------------------------------------------------------
        boolean hasItem = false;
        //------------------
        // Check for an NRC
        //------------------
        if (contractType.get(ATTRIB_ACTIVATION_TYPE_ID_NRC) != null) {
	        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
	        	if (this.getC30SDKObject(i) instanceof C30Nrc) {
	        		hasItem = true;
	        		break;
	        	}
	        }

	        if (!hasItem) {
	        	lwoi = this.getFactory().createC30SDKObject(C30Nrc.class, new Object[0]);
                lwoi.setAttributeValue(ATTRIB_ITEM_ACTION_ID, C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                lwoi.setAttributeValue(ATTRIB_MEMBER_ID, (Integer) contractType.get(ATTRIB_ACTIVATION_TYPE_ID_NRC), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                this.addC30SDKObject(lwoi);
	        }

        }

	    //---------------------
	    // Check for a product
	    //---------------------
        hasItem = false;
        if (contractType.get(ATTRIB_COMMITMENT_REFERENCE) != null) {
        	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
	        	if (this.getC30SDKObject(i) instanceof C30ProductElement) {
	        		hasItem = true;
	        		break;
	        	}
	        }

	        if (!hasItem) {
	        	lwoi = this.getFactory().createC30SDKObject(C30ProductElement.class, new Object[0]);
                lwoi.setAttributeValue(ATTRIB_ITEM_ACTION_ID, C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                lwoi.setAttributeValue(ATTRIB_MEMBER_ID, (Integer) contractType.get(ATTRIB_COMMITMENT_REFERENCE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                this.addC30SDKObject(lwoi);
	        }

        }

        C30SDKObject children = this.getFactory().createC30SDKObject(C30SDKCollection.class, new Object[0]);

        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
            lwoi = (C30OrderItem) this.getC30SDKObject(i);

            lwoi.setAttributeValue(C30ProductElement.ATTRIB_CONTRACT_TRACKING_ID, this.getAttributeValue(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setAttributeValue(C30ProductElement.ATTRIB_CONTRACT_TRACKING_ID_SERV, this.getAttributeValue(ATTRIB_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setNameValuePairs(this.getNameValuePairMap());
            lwoi = lwoi.process();

            //make sure process() doesn't return an exception
            if (lwoi instanceof C30SDKExceptionMessage) {
				children = lwoi;
				break;
            } else {
            	children.addC30SDKObject(lwoi);
            }
        }
        return children;
    }

    /**
     * Creates the Kenan FX Product view record using API TS.
     * @throws C30SDKException 
     */
    protected final C30SDKObject createFXObject() throws C30SDKException {
    	C30SDKObject lwo = null;
    	
        //----------------------------------------------------------------------
        // Products can be used in "Add", "Change" or "Disconnect" order item
        // action ids.
        //----------------------------------------------------------------------
        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {

        	//---------------------------------------------
        	// First thing we do is get the contract type.
        	//---------------------------------------------
        	getContractType();
        	
        	contract = new HashMap();

        	//create hashmap shell
            Map key = new HashMap();
            contract.put("Key", key);

            //----------------------------------------------------------------------
            // Set the following key product fields:
            // View Status: Pending
            // View Effective Date: Now
            // View Create Date: Now
            // Service External Id Type: based on member_id of the parent order item
            //----------------------------------------------------------------------
            contract.put(ATTRIB_VIEW_STATUS, C30OrderItem.FX_PENDING_VIEW_STATUS);
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
	            contract.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
	            contract.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }
            contract.put(ATTRIB_VIEW_CREATED_DT, new Date());
            contract.put(ATTRIB_CONTRACT_TYPE, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            contract.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            contract.put(ATTRIB_OWNING_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

            //----------------------------------------------------------------------
            // If the contact is service level, set the subscr_no/resets
            // otherwise set the account no.  Also perform product level validation
            //----------------------------------------------------------------------
            if (contractType.get("AllowServInst").equals(new Integer(1))) {
            //if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, Constants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
                contract.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                contract.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                contract.put(ATTRIB_CONTRACT_LEVEL, CONTRACT_SERVICE_LEVEL);
            }// else {
            if (contractType.get("AllowAccount").equals(new Integer(1))) {
                contract.put(ATTRIB_CONTRACT_LEVEL, CONTRACT_ACCOUNT_LEVEL);
            }

            this.connectContract();

        } else if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID) ||
        	this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CHANGE_ITEM_ACTION_ID)) {

            this.updateContract();

        }

        //---------------------------------------------------------------------------------------
        // Get the contract end date by finding the contract type if it's not a disconnect action.
        // In case of disconnect, end_dt is set in updateConnect method.
		// CBS00093090 
        //---------------------------------------------------------------------------------------
        if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID))
            setContractEndDate();

        Map callResponse = this.queryData("CustomerContract", "Create", contract, getAccountServerId());
        contract = (HashMap) callResponse.get("CustomerContract");

        //remove duplicate extended data - this is a hack to fix a core API-TS bug
        C30HashMapUtils.removeDuplicatesFromArray(contract, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

        //-------------------------------------------------
        // Now that we have the data, populate this object
        //-------------------------------------------------
        populateContract();

        //--------------------------------------------------
        // And finally create its children if there are any
        //--------------------------------------------------
        this.createChildOrderItems();

        lwo = this;
        
        return lwo;
        
    }

    /**
     * Method to connect the contract.
     * @throws C30SDKInvalidAttributeException
     */
    private void connectContract() throws C30SDKInvalidAttributeException {
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            contract.put(ATTRIB_START_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            contract.put(ATTRIB_START_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        setFXContractAttributes();
    }

    /**
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while updating the customer contract
     * @throws C30SDKObjectException if there was a problem initializing the customer contract during update
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void updateContract()  throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        this.findContract();
        contract.put(ATTRIB_PREV_VIEW_ID, this.getAttributeValue(ATTRIB_VIEW_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        contract.put(ATTRIB_TRACKING_ID, this.getAttributeValue(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        contract.put(ATTRIB_TRACKING_ID_SERV, this.getAttributeValue(ATTRIB_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        contract.put(ATTRIB_VIEW_STATUS, C30OrderItem.FX_PENDING_VIEW_STATUS);
        if(this.getAttributeValue(ATTRIB_EXTENDED_DATA, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ) {
            contract.put(ATTRIB_EXTENDED_DATA, this.getAttributeValue(ATTRIB_EXTENDED_DATA, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        
        // Set other contract attributes
        setFXContractAttributes();

        // Set Inactive Date if it is a product Disconnect Order Item
        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
                contract.put(C30CustomerContract.ATTRIB_END_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            }
            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
                contract.put(C30CustomerContract.ATTRIB_END_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            }
        }
    }

    /**
     * Method to set the attributes of the contract.
     * @throws C30SDKInvalidAttributeException if an invalid attribute exists
     */
    private void setFXContractAttributes() throws C30SDKInvalidAttributeException {
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if(attributeName.equals(ATTRIB_CONTRACT_ID) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_ARCH_FLAG) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_AVAIL_PERIODS) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_COMMITMENT_AMOUNT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_COMMITMENT_CURRENCY_CODE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_COMMITMENT_REFERENCE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CONTRACT_LEVEL) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CREATE_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_END_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_FIRST_USE_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_IN_USE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_IS_PART_OF_COMPONENT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_LAST_USE_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_NEXT_EVAL_DATE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_OWNING_ACCOUNT_INTERNAL_ID) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PREV_EVAL_DATE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_START_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_TOTAL_ELEMENT_AMOUNT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PARENT_GROUP_NO) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PREV_END_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_GRACE_PERIOD) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_TOTAL_PERIODS) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_SALES_CHANNEL_ID) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_TRACK_TOTAL_AMOUNTS) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_ROLLOVER_PERIOD) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_ROLLOVER_PERIOD_START) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_BILL_PERIOD) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_DISTRIBUTE_PAYBACK) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_CONTRACT_STRT_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_CONTRACT_END_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_INTERIM_BILL_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_PERIOD_CHANGE_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_ACCOUNT_START_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_ACCOUNT_END_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_TGT_THRESHOLD_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PRORATE_TGT_REBATES_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_START_BY_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_NOTICE_END_DT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_DURATION_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_NOTICE_OVERRIDE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_ELEMENT_ID) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_COMMITMENT_REFERENCE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_IS_PART_OF_COMPONENT) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_IS_CUSTOM) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_MIN_DURATION_DATE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_MIN_NOTICE_DATE) && attributeValue != null) {
                contract.put(attributeName, attributeValue);
            } else {
                //------------------------------------------------------
                // check if the attribute is an extended data parameter
                //------------------------------------------------------
                try {
                    if (attributeValue != null) {

                    	//check if the attribute is extended data
                        Integer paramId = new Integer(attributeName);

                        //if we do not already have extended data attributes then create an array
                        if (contract.get(ATTRIB_EXTENDED_DATA) == null) {
                            contract.put(ATTRIB_EXTENDED_DATA, new Object[1]);

	                        //create the extended data attribute
	                        HashMap extData = new HashMap();
	                        extData.put(ATTRIB_PARAM_ID, paramId);
	                        extData.put(ATTRIB_PARAM_VALUE, attributeValue);

	                        //add it to the array
	                        Object[] newDataParams = C30HashMapUtils.addObjectToArray( (Object[]) contract.get(ATTRIB_EXTENDED_DATA), extData);

	                        //put the array into the contract data.
	                        contract.put(ATTRIB_EXTENDED_DATA, newDataParams);

                        } else {
                            C30HashMapUtils.removeDuplicatesFromArray(contract, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

                            Object [] extendedData = (Object[]) contract.get(ATTRIB_EXTENDED_DATA);
                            HashMap xdHashMap = new HashMap(((HashMap) extendedData[0]));

                            // set the extended data attributes
                            xdHashMap.put(ATTRIB_PARAM_ID, paramId);
                            xdHashMap.put(ATTRIB_PARAM_VALUE, attributeValue);

                            // add it uniquely to contract
                            Object [] newDataParams = C30HashMapUtils.addUniqueHashMapToArray(extendedData, xdHashMap, ATTRIB_PARAM_ID);
                            contract.put(ATTRIB_EXTENDED_DATA, newDataParams);
                        }

                    }
                } catch (NumberFormatException e) {
                    // not a valid param id
                }
            }
        }
    }

    /**
     * Method that gets the contract type associated with the contract.
     * @return the contract type hashmap.
     * @throws C30SDKObjectConnectionException if there is a problem connecting to middleware.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void getContractType() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
    	
    	Integer contractTypeId = (Integer) this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    	//Create a HashMap to contain the products filter info
    	HashMap contractTypeFilter = new HashMap();
        Map key = new HashMap();
        contractTypeFilter.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found object
    	contractTypeFilter.put("Fetch", Boolean.TRUE);

        // Set Contract Type Filter
    	HashMap typeFilter = new HashMap();
    	typeFilter.put("Equal", contractTypeId);
    	key.put(C30CustomerContract.ATTRIB_CONTRACT_TYPE, typeFilter);
        
    	// Set Language Code Filter
    	typeFilter = new HashMap();
    	typeFilter.put("Equal", new Integer(1));
    	key.put(C30CustomerContract.ATTRIB_LANGUAGE_CODE, typeFilter);
    	
        Map callResponse = this.queryData("ContractType", "Find", contractTypeFilter, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
            String error = "No contract types match given contract type ID: " + contractTypeId;
            error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
                error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
                      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            throw new C30SDKObjectException(error);

        }

	    contractType = (HashMap) ((Object[]) callResponse.get("ContractTypeList"))[0];

    }
    
    /**
     * Method that sets the end date of the contract based on the start date and the contract type.
     * The possible values for the duration units are:
     *  -3 = bill cycles in which contract was used - (LWO not touching because set by BIP on first run)
     *  -2 = terminate on first charge reduction (obsolete) - (LWO not touching since it's obsolete)
     *  -1 = unspecified duration (no time limit) - (LWO leaves end date unset)
     *  140 = days - (LWO sets end date)
     *  150 = weeks - (LWO sets end date)
     *  160 = months - (LWO sets end date)
     *  170 = quarters - (LWO sets end date.  Quarter means 3 months)
     *  180 = years  - (LWO sets end date)
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while setting the end date of the contract
     * @throws C30SDKObjectException if there was a problem initializing the customer contract while setting the end date
     * @throws C30SDKInvalidAttributeException
     */
    private void setContractEndDate() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException {
    	log.debug("Starting setContractEndDate");

        //-------------------------------------------------------
        // Now that we have the contract type we need to set
        // the end date based on the duration and duration units
        //-------------------------------------------------------

    	Integer contractTypeDuration = null;
    	Integer contractTypeDurationUnits = null;
    	Date startDate = (Date) contract.get(ATTRIB_START_DT);
    	
	    //get the duration and durations units
	    contractTypeDuration = (Integer) contractType.get(C30CustomerContract.ATTRIB_DURATION);
	    contractTypeDurationUnits = (Integer) contractType.get(C30CustomerContract.ATTRIB_DURATION_UNITS);

	    //if the contract doesn't have unspecified durations then calculate the end date.
	    if (!contractTypeDuration.equals(new Integer(-1))
	    		&& !contractTypeDurationUnits.equals(CONTRACT_TYPE_DURATION_UNITS_UNSPECIFIED)) {

	    	//create calendar and set the date to the start date of contract
	    	GregorianCalendar calendar = new GregorianCalendar();
	    	calendar.setTime(startDate);

	    	if (contractTypeDurationUnits.equals(CONTRACT_TYPE_DURATION_UNITS_YEARS)) {
	    		calendar.add(Calendar.YEAR, contractTypeDuration.intValue());
	    	} else if (contractTypeDurationUnits.equals(CONTRACT_TYPE_DURATION_UNITS_WEEKS)) {
	    		calendar.add(Calendar.DATE, (contractTypeDuration.intValue()*7));
	    	} else if (contractTypeDurationUnits.equals(CONTRACT_TYPE_DURATION_UNITS_DAYS)) {
	    		calendar.add(Calendar.DATE, contractTypeDuration.intValue());
	    	} else if (contractTypeDurationUnits.equals(CONTRACT_TYPE_DURATION_UNITS_MONTHS)) {
	    		calendar.add(Calendar.MONTH, contractTypeDuration.intValue());
	    	} else if (contractTypeDurationUnits.equals(CONTRACT_TYPE_DURATION_UNITS_QUARTERS)) {
	    		calendar.add(Calendar.MONTH, (contractTypeDuration.intValue()*3));
	    	}

	    	contract.put(C30CustomerContract.ATTRIB_END_DT, calendar.getTime());
	    }

    	log.debug("Finished setContractEndDate");
    }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    protected void loadCache() {

    }

}
