
/*
 * C30AccountBalance.java
 *
 * Created on May 17, 2011
 *
 */

package com.cycle30.sdk.object.kenan.financial;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKSqlConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * C30SDK AccountBalance 
 * @author jwood
 */
public class C30AccountBalance extends C30SDKObject implements Cloneable {

	private C30SDKObject account = null;
	protected Map balance = null;
	private static Logger log = Logger.getLogger(C30AccountBalance.class);

	/**
	 * Creates a new instance of C30Payment.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30AccountBalance(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30AccountBalance(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30AccountBalance.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	@Override
	public Object clone() {
		C30AccountBalance copy = (C30AccountBalance) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30AccountBalance.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
		C30SDKObject lwo = null;
		try {
			setAttributesFromNameValuePairs();
			validateAttributes();
			if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
					isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
					getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
			{

				// check to see if we need to explicitly get an account
				if (super.canServerIdCacheBeSetLocally())
					super.setAccountServerIdCache();
				else{
					if (this.account == null){
						account = getAccount();
						if(account instanceof C30SDKExceptionMessage)
							return account;
					} 
				}
				super.setAccountServerId();
			}
			if  (isFindTransaction()) {
				// getSumAndBalanceDue();
				getAccountBalance();
			}
			// non-null balance structure indicates success create or find transaction
			if (this.balance != null)
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			lwo = this;

		} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}

	private boolean isFindTransaction()throws C30SDKInvalidAttributeException 
	{   boolean retval = false; 
	if  ((isAttribute(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
			getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
			getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND) ||
			(  isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
					(Boolean)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT))   )) 
		return true;

	return retval;       

	}



	private C30SDKObject getAccount() throws C30SDKException {
		log.debug("Starting C30AccountBalance.getAccount");
		C30SDKObject theAccount = null;
		//try get account from object children
		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			if (this.getC30SDKObject(i) instanceof C30Account) {
				theAccount = this.getC30SDKObject(i);
			}
		}

		//if no account in children then get account from middleware
		if (theAccount == null) {
			theAccount = this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);
			theAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			theAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			theAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			theAccount = theAccount.process();
		}

		//if the processed account produced an exception.
		if (!(theAccount instanceof C30SDKExceptionMessage)) {
			this.setAccountLevelAttributes(theAccount);
		}
		log.debug("Finished C30AccountBalance.getAccount");
		return theAccount;
	}



	/**
	 *      
	 * Method which sets all account data to be usee by the object.
	 * @param account
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKObjectException
	 */
	private void setAccountLevelAttributes(C30SDKObject theAccount)
	throws C30SDKInvalidAttributeException, C30SDKObjectException
	{

		if(theAccount instanceof C30Account)
			if (theAccount != null)
			{
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null)
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) 
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))     
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))       
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}
	}


	/**
	 * Method to ensure all attributes that are visible to the calling application are populated.  This 
	 * method is usually called just before the object returns objects using the process() method.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */
	private void populateBalanceOutput() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {



		for  ( Iterator it=this.balance.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
			Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
			String key = (String)entry.getKey (  ) ; 
			Object value = this.balance.get(key) ;
			if (value != null)
			{
				if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
				{
					setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

					log.debug(  key+" " +  value +"   SET") ;                            
				}
				else
					if (value != null){
						log.debug(  key+" " +  value ) ; 
					}

			}
		}

	}

	/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 

		String acctExternalId = null;
		Integer acctExternalIdType = null;
		Integer acctInternalId = null;
		Integer accountServerId = null;

		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctExternalId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctExternalIdType = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctInternalId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 


		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			accountServerId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 


		if (((acctExternalId == null || acctExternalIdType == null) && acctInternalId == null)  && accountServerId == null ) 
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-017"),"INVALID-ATTRIB-017");
		}



	}



	/**
	 * Internal method to get the outstanding account balance.  This method is 
	 * used when the account is to have their entire balance cleared.
	 * @return the account balance
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void getAccountBalance() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

		getWriteOffBalance();

		//Create a HashMap to contain the account filter info
		HashMap accountMap = new HashMap();

		//create the key hashmap
		Map key = new HashMap();
		accountMap.put("Key", key);

		//put the ID into the key
		key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		//make the call
		Map callResponse = this.queryData("Account", "BalanceSummary", accountMap, getAccountServerId());
		this.balance = callResponse;
		populateBalanceOutput();  
	}


	/**
	 * Method to find Kenan FX 1.0 Bill Invoices using C30JDBCDataSource invoking StoredProcedure in the Customer database.
	 * FX 1.0 is a Single Account Server Database so all catalog, admin and customer data is in a single database.
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidConfigurationException
	 */
	private int getWriteOffBalance()
	throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException
	{
		log.debug("In getWriteOffBalance");
		int writeOffBalance = 0;

		String query = C30SDKSqlConstants.QUERY_WRITEOFF_BALANCE;

		C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId()); 
		try
		{

			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,(Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			ResultSet resultSet  = ps.executeQuery();
			if (resultSet.next()) {
				writeOffBalance = resultSet.getInt("TRAN_AMT"); 
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_WRITEOFF_BALANCE, writeOffBalance, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}

		}
		catch(Exception e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-018"),"INVALID-ATTRIB-018");
		}
		finally {
			try 
			{
				dataSource.closeConnection();
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}
		return writeOffBalance;

	}


	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}