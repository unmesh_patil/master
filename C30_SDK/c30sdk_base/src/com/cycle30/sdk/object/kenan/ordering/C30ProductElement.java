/*
 * C30ProductElement.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKCollection;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30HashMapUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX Product Element object and its corresponding order item.
 * @author Joe Morales
 */
public class C30ProductElement extends C30OrderItem {

    public static final String ATTRIB_ARCH_FLAG = "ArchFlag";
    public static final String ATTRIB_AUTO_ACTIVATION = "AutoActivation";
    public static final String ATTRIB_BILLING_ACCOUNT_INTERNAL_ID = "BillingAccountInternalId";
    public static final String ATTRIB_BILLING_ACTIVE_DT = "BillingActiveDt";
    public static final String ATTRIB_BILLING_INACTIVE_DT = "BillingInactiveDt";
    public static final String ATTRIB_BILL_PERIOD = "BillPeriod";
    public static final String ATTRIB_CHARGE_ORDER = "ChargeOrder";
    public static final String ATTRIB_CHG_DT = "ChgDt";
    public static final String ATTRIB_CHG_WHO = "ChgWho";
    public static final String ATTRIB_CONVERTED = "Converted";
    public static final String ATTRIB_CONNECT_REASON = "ConnectReason";
    public static final String ATTRIB_COMPONENT_ID = "ComponentId";
    public static final String ATTRIB_CONTRACT_ASSOCIATION_TYPE = "ContractAssociationType";
    public static final String ATTRIB_CONTRACT_TRACKING_ID = "ContractTrackingId";
    public static final String ATTRIB_CONTRACT_TRACKING_ID_SERV = "ContractTrackingIdServ";
    public static final String ATTRIB_DISCONNECT_REASON = "DisconnectReason";
    public static final String ATTRIB_ELEMENT_ID = "ElementId";
    public static final String ATTRIB_EXTENDED_DATA = "ExtendedData";
    public static final String ATTRIB_HAS_OVERRIDES = "HasOverrides";
    public static final String ATTRIB_HAS_PRODUCT_KEYS = "HasProductKeys";
    private static final String ATTRIB_INTENDED_VIEW_EFFECTIVE_DT = "IntendedViewEffectiveDt";
    public static final String ATTRIB_IN_ARREARS_OVERRIDE = "InArrearsOverride";
    public static final String ATTRIB_IS_PART_OF_COMPONENT = "IsPartOfComponent";
    private static final String ATTRIB_LANGUAGE_CODE = "LanguageCode";
    public static final String ATTRIB_NO_BILL = "NoBill";
    public static final String ATTRIB_OPEN_ITEM_ID = "OpenItemId";
    public static final String ATTRIB_ORDER_NUMBER = "OrderNumber";
    public static final String ATTRIB_OVERRIDE_RATE = "OverrideRate";
    public static final String ATTRIB_PARAM_ID = "ParamId";
    public static final String ATTRIB_PARAM_VALUE = "ParamValue";
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID = "ParentServiceInternalId";
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS = "ParentServiceInternalIdResets";
    public static final String ATTRIB_PREV_PRODUCT_INACTIVE_DT = "PrevProductInactiveDt";
    public static final String ATTRIB_PREV_VIEW_ID = "PrevViewId";
    public static final String ATTRIB_PRODUCT_INACTIVE_DT = "ProductInactiveDt";
    public static final String ATTRIB_PRODUCT_ACTIVE_DT = "ProductActiveDt";
    public static final String ATTRIB_SALES_CHANNEL_ID = "SalesChannelId";
    public static final String ATTRIB_SERIAL_NUMBER = "SerialNumber";
    public static final String ATTRIB_TRACKING_ID = "TrackingId";
    public static final String ATTRIB_TRACKING_ID_SERV = "TrackingIdServ";
    public static final String ATTRIB_VIEW_CREATED_DT = "ViewCreatedDt";
    public static final String ATTRIB_VIEW_EFFECTIVE_DT = "ViewEffectiveDt";
    public static final String ATTRIB_VIEW_STATUS = "ViewStatus";

    public static final String ATTRIB_SEARCH_ACCOUNT_EXTERNAL_ID = "SearchAccountExternalId";
    public static final String ATTRIB_SEARCH_ARCH_FLAG = "SearchArchFlag";
    public static final String ATTRIB_SEARCH_AUTO_ACTIVATION = "SearchAutoActivation";
    public static final String ATTRIB_SEARCH_BILL_PERIOD = "SearchBillPeriod";
    public static final String ATTRIB_SEARCH_BILLING_ACCOUNT_INTERNAL_ID = "SearchBillingAccountInternalId";
    public static final String ATTRIB_SEARCH_BILLING_ACTIVE_DT = "SearchBillingActiveDt";
    public static final String ATTRIB_SEARCH_BILLING_INACTIVE_DT = "SearchBillingInactiveDt";
    public static final String ATTRIB_SEARCH_CHG_DT = "SearchChgDt";
    public static final String ATTRIB_SEARCH_CHG_WHO = "SearchChgWho";
    public static final String ATTRIB_SEARCH_CONVERTED = "SearchConverted";
    public static final String ATTRIB_SEARCH_CHARGE_ORDER = "SearchChargeOrder";
    public static final String ATTRIB_SEARCH_CONTRACT_TRACKING_ID_SERV = "SearchContractTrackingIdServ";
    public static final String ATTRIB_SEARCH_CONTRACT_TRACKING_ID = "SearchContractTrackingId";
    public static final String ATTRIB_SEARCH_CONTRACT_ASSOCIATION_TYPE = "SearchContractAssociationType";
    public static final String ATTRIB_SEARCH_COMPONENT_ID = "SearchComponentId";
    public static final String ATTRIB_SEARCH_CONNECT_REASON = "SearchConnectReason";
    public static final String ATTRIB_SEARCH_CREATE_DT = "SearchCreateDt";
    public static final String ATTRIB_SEARCH_DISCONNECT_REASON = "SearchDisconnectReason";
    public static final String ATTRIB_SEARCH_ELEMENT_ID = "SearchElementId";
    public static final String ATTRIB_SEARCH_HAS_OVERRIDES = "SearchHasOverride";
    public static final String ATTRIB_SEARCH_HAS_PRODUCT_KEYS = "SearchHasProductKeys";
    public static final String ATTRIB_SEARCH_IS_PART_OF_COMPONENT = "SearchIsPartOfComponent";
    public static final String ATTRIB_SEARCH_IN_ARREARS_OVERRIDE = "SearchInArrearsOverride";
    public static final String ATTRIB_SEARCH_INTENDED_VIEW_EFFECTIVE_DT = "SearchIntendedViewEffectiveDt";
    public static final String ATTRIB_SEARCH_NO_BILL = "SearchNoBill";
    public static final String ATTRIB_SEARCH_ORDER_NUMBER = "SearchOrderNumber";
    public static final String ATTRIB_SEARCH_OPEN_ITEM_ID = "SearchOpenItemId";
    public static final String ATTRIB_SEARCH_PARENT_ACCOUNT_INTERNAL_ID = "SearchParentAccountInternalId";
    public static final String ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID = "SearchParentServiceInternalId";
    public static final String ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID_RESETS = "SearchParentServiceInternalIdResets";
    public static final String ATTRIB_SEARCH_PREV_VIEW_ID = "SearchPrevViewId";
    public static final String ATTRIB_SEARCH_PREV_PRODUCT_INACTIVE_DT = "SearchPrevProductInactiveDt";
    public static final String ATTRIB_SEARCH_PRODUCT_ACTIVE_DT = "SearchProductActiveDt";
    public static final String ATTRIB_SEARCH_PRODUCT_INACTIVE_DT = "SearchProductInactiveDt";
    public static final String ATTRIB_SEARCH_SALES_CHANNEL_ID = "SearchSalesChannelId";
    public static final String ATTRIB_SEARCH_SERIAL_NUMBER = "SearchSerialNumber";
    public static final String ATTRIB_SEARCH_SERVICE_EXTERNAL_ID = "SearchServiceExternalId";
    public static final String ATTRIB_SEARCH_TRACKING_ID = "SearchTrackingId";
    public static final String ATTRIB_SEARCH_TRACKING_ID_SERV = "SearchTrackingIdServ";
    public static final String ATTRIB_SEARCH_VIEW_CREATED_DT = "SearchViewCreatedDt";
    public static final String ATTRIB_SEARCH_VIEW_EFFECTIVE_DT = "SearchViewEffectiveDt";
    public static final String ATTRIB_SEARCH_VIEW_ID = "SearchViewId";

    /** Constant representing service level product value. */
    public static final Integer PRODUCT_SERVICE_LEVEL = new Integer(2);
    /** Constant representing account level product value. */
    public static final Integer PRODUCT_ACCOUNT_LEVEL = new Integer(1);
    /** Constant representing flexible level product value. */
    public static final Integer PRODUCT_ANY_LEVEL = new Integer(0);

    private Map product;
    /* used for Searches */
    private C30SDKObject account = null;
    private static Integer startRecord = 1;
    private static Integer returnSetSize = 50;
    private static Logger log = Logger.getLogger(C30ProductElement.class);


    /**
     * Creates a new instance of C30OrderItem.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ProductElement(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ProductElement(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30ProductElement.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Product Element.
     */
    protected void initializeAttributes() throws C30SDKInvalidAttributeException{

    	super.initializeAttributes();

        //===================
        // Input Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ARCH_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_AUTO_ACTIVATION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_PERIOD, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_BILLING_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CHARGE_ORDER, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_ASSOCIATION_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONVERTED, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_DISCONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HAS_OVERRIDES, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HAS_PRODUCT_KEYS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IN_ARREARS_OVERRIDE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_PART_OF_COMPONENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NO_BILL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_PRODUCT_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERIAL_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OVERRIDE_RATE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKAttribute attribute = this.getAttribute(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = this.getAttribute(ATTRIB_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = this.getAttribute(ATTRIB_VIEW_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_PRODUCT_MEMBER_TYPE);

        //===================
        // Search Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ARCH_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_AUTO_ACTIVATION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILL_PERIOD, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILLING_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILLING_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILLING_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CHARGE_ORDER, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CHG_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_COMPONENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONTRACT_ASSOCIATION_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONTRACT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONTRACT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONVERTED, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_DISCONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_HAS_OVERRIDES, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_HAS_PRODUCT_KEYS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_IN_ARREARS_OVERRIDE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_INTENDED_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_IS_PART_OF_COMPONENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_NO_BILL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PREV_PRODUCT_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PREV_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRODUCT_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PRODUCT_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SERIAL_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SERVICE_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_CREATED_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //===================
        // Output Attributes
        //===================
    	this.addAttribute(new C30SDKAttribute(ATTRIB_ARCH_FLAG, Boolean.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_AUTO_ACTIVATION, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_PERIOD, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_BILLING_ACTIVE_DT, Date.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_BILLING_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CHARGE_ORDER, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CHG_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_COMPONENT_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CONNECT_REASON, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_ASSOCIATION_TYPE, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID_SERV, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CONVERTED, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_DISCONNECT_REASON, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_ELEMENT_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_HAS_OVERRIDES, Boolean.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_HAS_PRODUCT_KEYS, Boolean.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_IN_ARREARS_OVERRIDE, Boolean.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_IS_PART_OF_COMPONENT, Boolean.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_NO_BILL, Boolean.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_OPEN_ITEM_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_NUMBER, String.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_PRODUCT_INACTIVE_DT, Date.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_VIEW_ID, BigInteger.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PRODUCT_ACTIVE_DT, Date.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PRODUCT_INACTIVE_DT, Date.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SERIAL_NUMBER, String.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_CREATED_DT, Date.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_EFFECTIVE_DT, Date.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_STATUS, Integer.class) , C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        
        //===========================
        // Search Attributes
        //===========================
         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, java.lang.Integer.class);
         attribute.setValue(this.startRecord);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         
         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, java.lang.Integer.class);
         attribute.setValue(this.returnSetSize);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_COUNT, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_RETURNED, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

         attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_LAST_RECORD_RETURNED_NUMBER, java.lang.Integer.class);
         attribute.setIsInternal(true);
         addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         
          if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }
         
         if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) 
              if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
                  attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, java.lang.Integer.class);
                  addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                  addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
              }
              
         if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, java.lang.String.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }
         
         if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
             attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, java.lang.Integer.class);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         }


    }

    private void initializeProductElementChangeAttributes() throws C30SDKInvalidAttributeException {
        C30SDKAttribute attribute = this.getAttribute(ATTRIB_CONNECT_REASON, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_COMPONENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONTRACT_ASSOCIATION_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONTRACT_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONTRACT_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_IS_PART_OF_COMPONENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);
    }

    private void initializeProductElementDisconnectAttributes() throws C30SDKInvalidAttributeException {
        C30SDKAttribute attribute = this.getAttribute(ATTRIB_BILL_PERIOD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CHARGE_ORDER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONVERTED, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_HAS_OVERRIDES, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_HAS_PRODUCT_KEYS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_IN_ARREARS_OVERRIDE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_NO_BILL, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_OPEN_ITEM_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_ORDER_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_ORDER_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_SALES_CHANNEL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_SERIAL_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONNECT_REASON, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_COMPONENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONTRACT_ASSOCIATION_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONTRACT_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_CONTRACT_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

        attribute = this.getAttribute(ATTRIB_IS_PART_OF_COMPONENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setValue(null);
        attribute.setOverrideable(false);

    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Integer)
     */
    public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
    	super.setAttributeValue(attributeName, attributeValue, type);

    	//if we are setting the action ID
    	if (attributeName.equals(ATTRIB_ITEM_ACTION_ID)) {
    		if (attributeValue.equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
    			initializeProductElementDisconnectAttributes();
    		} else if (attributeValue.equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID)) {
    			initializeProductElementChangeAttributes();
    		}
    	}

    	if (attributeName.equals(ATTRIB_MEMBER_ID)) {
    		super.loadExtendedDataAttributes("PRODUCT_VIEW", (Integer) this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	}
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);
        boolean setOtherAttributes = true;
        
        if (this.isAttribute(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
    	   //if we are setting the member ID then get the extended data.
            if (name.equals(this.getAttribute(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
                   Integer intValue;
                   try {
                        intValue = new Integer(value.toString());
                   } catch (Exception ex){
                       intValue = (Integer)value;
                   }
                   
                  //  super.loadExtendedDataAttributes("PRODUCT_VIEW", (Integer) value);
                super.loadExtendedDataAttributes("PRODUCT_VIEW", intValue);
                setOtherAttributes = false;
            } 
        }
        if (setOtherAttributes)  //if we are setting the action ID
            if (name.equals(this.getAttribute(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
                            if (value.equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
                                    initializeProductElementDisconnectAttributes();
                            } else if (value.equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID)) {
                                    initializeProductElementChangeAttributes();
                            }
            }

    }

    
    /**
    *   method to locate and use an account object during search for Component(s)
    * @return C30SDKObject
     * @throws C30SDKException 
    */
    private C30SDKObject getAssociatedAccount()
    throws C30SDKException
    {
    C30SDKObject anAccount = null;
    // add the attributes here - not previously part of the order-service_order processing only for the find
    addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT, com.cycle30.sdk.core.framework.C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    
    if(!isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
      if(isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
      {
          anAccount = getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30Account.class, new Object[0]);
          anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
          anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
          anAccount = anAccount.process();
          if(anAccount instanceof C30Account)
          {
              setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, anAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, anAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
              if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) 
                   if  (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                       setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, anAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

              setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, anAccount, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
          }
      } else
      {
          String errorMsg = "The account internal id or external id and external id type must be set in order to create or fetch a Kenan/FX object";
          throw new C30SDKObjectException(errorMsg);
      }
    return anAccount;
    }
    
 
    /**
     * the method that executes for Find transactions only.  
     * @return SDK object
     * @throws C30SDKInvalidAttributeException
     */
     public C30SDKObject process() throws C30SDKInvalidAttributeException {
         log.debug("Starting C30ProductElement.process()");

         //in case there is an error.
         String error = "creating lightweight object: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

         C30SDKObject lwo = null;
         try {
             if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND)) {
                 setAttributesFromNameValuePairs();
                 super.clearOrderAttributes();
                  if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
                     isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
                      getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
                  {

                         // check to see if we need to explicitly get an account
                         if (super.canServerIdCacheBeSetLocally())
                              super.setAccountServerIdCache();
                          else{
                                  if (this.account == null){
                                      account = getAssociatedAccount();
                                      if(account instanceof C30SDKExceptionMessage)
                                        return account;
                                  } 
                          }
                         super.setAccountServerId();
                 }
                 
                 searchForProducts();
                 setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                 lwo = this;
             }
             else  // this is a request to process for internal orderitem processing
               lwo = super.process();
            
         } catch (Exception e) {
                         lwo = this.createExceptionMessage(e);
                 lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "Error " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                 }
         log.debug("Finished C30ProductElement.process()");
         return lwo;
     }


    private void searchForProducts() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        int lastRecordReturned = 0; 
        //Create a HashMap to contain the products filter info
        Map filters = new HashMap();
        HashMap key = new HashMap();
        filters.put("Key", key);

        //Set the Fetch flag at the root level,
        //so the call will return all fields for found objects
        filters.put("Fetch", Boolean.TRUE);

        //---------------------------------------------------------
        // Set the Element ID that is specified for the order item
        //---------------------------------------------------------
        Map attrFilter = new HashMap();
        /*
        if  (isAttribute(Constants.ATTRIB_SERVICE_INTERNAL_ID,Constants.ATTRIB_TYPE_INPUT) && 
                 getAttributeValue(Constants.ATTRIB_SERVICE_INTERNAL_ID, Constants.ATTRIB_TYPE_INPUT) != null ){

             attrFilter.put("Equal", this.getAttributeValue(ATTRIB_MEMBER_ID, Constants.ATTRIB_TYPE_INPUT));
            filters.put(ATTRIB_ELEMENT_ID, attrFilter);
        }
        */
        //-----------------------------
        // Only get the current view
        //-----------------------------
        attrFilter = new HashMap();
        attrFilter.put("Equal", new Integer(2));
        filters.put(ATTRIB_VIEW_STATUS, attrFilter);

        //---------------------
        // billing account internal id
        //---------------------         
        attrFilter = new HashMap();
        attrFilter.put("Equal", (Integer) getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        filters.put(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, attrFilter);

  
        //--------------------------------------------
        // Construct Filter elements from SEARCH
        // attributes that have been set or defaulted
        //--------------------------------------------
        Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeSearchValue = null;
        while (iter.hasNext()) {
            attributeName = (String) iter.next();

            if (attributeName.indexOf("Search") != -1) {
                    attributeSearchValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

                    if (attributeSearchValue != null) {

                    attrFilter = new HashMap();
                    attrFilter.put("Equal",attributeSearchValue);

                        //if we are searching on the key
                        if (attributeName.equals(ATTRIB_SEARCH_VIEW_ID)) {
                            key.put(attributeName.replaceFirst("Search", ""), attrFilter);
                            filters.put("Key", key);
                        //if we are not searching on the key.
                            } else {
                            filters.put(attributeName.replaceFirst("Search", ""), attrFilter);
                            }

                    }
            }
        }

       log.debug(filters.toString());
       
       Map callResponse = this.queryData("Product", "FindWithExtendedData", filters, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

            if(count == 0)
            {
              setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              return;
            }
            
            int recordsProcessed = 0;
           
            if (startRecord == 1)
            {
                product = (HashMap) ((Object[]) callResponse.get("ProductList"))[0];
                //remove duplicate extended data - this is a hack to fix a core API-TS bug
                C30HashMapUtils.removeDuplicatesFromArray(product, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);
                populateFoundProductMap();
                
                if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                    setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                lastRecordReturned = 1;
                recordsProcessed++;
            }

            
            // at this point we have more than a single account to load
             C30ProductElement cloneLwo = (C30ProductElement)this.clone();
             if (startRecord > 1)
               startRecord--;
              for (int i=startRecord; i < count; i++)
              {
                  if (recordsProcessed >= returnSetSize )
                    break;
                  product = (HashMap)((Object[])(Object[])callResponse.get("ProductList"))[i]; 
                  //remove duplicate extended data - this is a hack to fix a core API-TS bug
                  C30HashMapUtils.removeDuplicatesFromArray(product, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

                  if (recordsProcessed == 0){
                      this.populateFoundProductMap();
                    if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                          this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

                    this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                  }
                  else
                  {
                      C30ProductElement lwo = (C30ProductElement)cloneLwo.clone();
                      lwo.populateFoundProductMap();
                     // add to our list of peer objects
                      if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                            lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                      lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                      this.addC30SDKObject(lwo);
                      }
                  recordsProcessed++;
                  lastRecordReturned =  i + 1;
              }
            super.setProcessingAggregateTotalsAttributes(this, count, lastRecordReturned, recordsProcessed );
            
    }

   /**
     * this method set the attributes in the lightwieght object from the product element map found by the query.
     * @throws C30SDKInvalidAttributeException
     */
    private void populateFoundProductMap() throws C30SDKInvalidAttributeException {
    
        for  ( Iterator it=product.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                 Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                 String key = (String)entry.getKey (  ) ;     
                    Object value = entry.getValue (  ) ;
                    if (value != null)
                        if (key.equalsIgnoreCase("KEY"))
                        {    
                          HashMap keyValue = (HashMap)entry.getValue (  ) ; 
                          if (keyValue.containsKey(ATTRIB_VIEW_ID)){
                              if (isAttribute(ATTRIB_VIEW_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                  setAttributeValue(ATTRIB_VIEW_ID, ((HashMap)product.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                  }
                        }
                        else
                           if (key.equalsIgnoreCase(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID) ){
                               if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                 setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                           }
                           else 
                               if (key.equalsIgnoreCase(ATTRIB_CREATE_DT) ){
                                   if  (! isAttribute(ATTRIB_CREATE_DT,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                      addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                   setAttributeValue(ATTRIB_CREATE_DT, product.get(ATTRIB_CREATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                               }
                             else    
                                if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                    setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);           
                   if (value != null)
                     log.debug(  ( String ) key+" " +  value ) ; 
                
        }
        
        log.debug("Done populating product element response!"); 
    }
    

    private void findProduct() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	//Create a HashMap to contain the products filter info
        Map filters = new HashMap();
    	HashMap key = new HashMap();
    	filters.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	filters.put("Fetch", Boolean.TRUE);

        //---------------------------------------------------------
        // Set the Element ID that is specified for the order item
        //---------------------------------------------------------
        Map attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	filters.put(ATTRIB_ELEMENT_ID, attrFilter);

        //-----------------------------
        // Only get the current view
        //-----------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(2));
    	filters.put(ATTRIB_VIEW_STATUS, attrFilter);

        //-------------------------------------------
        // only get products owned by the parent SI.
        //-------------------------------------------
        if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
            //---------------------
            // service internal id
            //---------------------
        	attrFilter = new HashMap();
        	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	filters.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, attrFilter);

            //----------------------------
            // service internal ID resets
            //----------------------------
        	attrFilter = new HashMap();
        	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	filters.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, attrFilter);

        }

        //--------------------------------------------
        // Construct Filter elements from SEARCH
        // attributes that have been set or defaulted
        //--------------------------------------------
        Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeSearchValue = null;
        while (iter.hasNext()) {
            attributeName = (String) iter.next();

            if (attributeName.indexOf("Search") != -1) {
	            attributeSearchValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	            if (attributeSearchValue != null) {

                    attrFilter = new HashMap();
                    attrFilter.put("Equal",attributeSearchValue);

	            	//if we are searching on the key
	            	if (attributeName.equals(ATTRIB_SEARCH_VIEW_ID)) {
	                    key.put(attributeName.replaceFirst("Search", ""), attrFilter);
	                    filters.put("Key", key);
	            	//if we are not searching on the key.
		            } else {
	                    filters.put(attributeName.replaceFirst("Search", ""), attrFilter);
		            }

	            }
            }
        }

	    Map callResponse = this.queryData("Product", "FindWithExtendedData", filters, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
            String error = "No active product elements match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
                error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
                      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            throw new C30SDKObjectException(error);
        } else if (count > 1) {
            String error = "Multiple product elements match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL))  {
                error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
                      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            throw new C30SDKObjectException(error);
        }

        product = (HashMap) ((Object[]) callResponse.get("ProductList"))[0];

        //remove duplicate extended data - this is a hack to fix a core API-TS bug
        C30HashMapUtils.removeDuplicatesFromArray(product, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

        populateProduct();

    }

    private void populateProduct() throws C30SDKInvalidAttributeException {

    	this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) product.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    	this.setAttributeValue(ATTRIB_ARCH_FLAG, product.get(ATTRIB_ARCH_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_AUTO_ACTIVATION, product.get(ATTRIB_AUTO_ACTIVATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BILL_PERIOD, product.get(ATTRIB_BILL_PERIOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, product.get(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BILLING_ACTIVE_DT, product.get(ATTRIB_BILLING_ACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BILLING_INACTIVE_DT, product.get(ATTRIB_BILLING_INACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CHARGE_ORDER, product.get(ATTRIB_CHARGE_ORDER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CHG_DT, product.get(ATTRIB_CHG_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CHG_WHO, product.get(ATTRIB_CHG_WHO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_COMPONENT_ID, product.get(ATTRIB_COMPONENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CONNECT_REASON, product.get(ATTRIB_CONNECT_REASON), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CONTRACT_ASSOCIATION_TYPE, product.get(ATTRIB_CONTRACT_ASSOCIATION_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CONTRACT_TRACKING_ID, product.get(ATTRIB_CONTRACT_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CONTRACT_TRACKING_ID_SERV, product.get(ATTRIB_CONTRACT_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CONVERTED, product.get(ATTRIB_CONVERTED), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CREATE_DT, product.get(ATTRIB_CREATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_DISCONNECT_REASON, product.get(ATTRIB_DISCONNECT_REASON), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ELEMENT_ID, product.get(ATTRIB_ELEMENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_HAS_OVERRIDES, product.get(ATTRIB_HAS_OVERRIDES), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_HAS_PRODUCT_KEYS, product.get(ATTRIB_HAS_PRODUCT_KEYS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_IN_ARREARS_OVERRIDE, product.get(ATTRIB_IN_ARREARS_OVERRIDE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, product.get(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_IS_PART_OF_COMPONENT, product.get(ATTRIB_IS_PART_OF_COMPONENT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_NO_BILL, product.get(ATTRIB_NO_BILL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_OPEN_ITEM_ID, product.get(ATTRIB_OPEN_ITEM_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ORDER_NUMBER, product.get(ATTRIB_ORDER_NUMBER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, product.get(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID, product.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, product.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_PREV_PRODUCT_INACTIVE_DT, product.get(ATTRIB_PREV_PRODUCT_INACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_PREV_VIEW_ID, product.get(ATTRIB_PREV_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_PRODUCT_ACTIVE_DT, product.get(ATTRIB_PRODUCT_ACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_PRODUCT_INACTIVE_DT, product.get(ATTRIB_PRODUCT_INACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SALES_CHANNEL_ID, product.get(ATTRIB_SALES_CHANNEL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SERIAL_NUMBER, product.get(ATTRIB_SERIAL_NUMBER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRACKING_ID, product.get(ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRACKING_ID_SERV, product.get(ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_VIEW_CREATED_DT, product.get(ATTRIB_VIEW_CREATED_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_VIEW_EFFECTIVE_DT, product.get(ATTRIB_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_VIEW_STATUS, product.get(ATTRIB_VIEW_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) product.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_ID, product.get(ATTRIB_ELEMENT_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, product.get(ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, product.get(ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, new BigInteger(product.get(ATTRIB_TRACKING_ID).toString()), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
    }

    /**
     * Validates the item action id of the order item .
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        super.validateAttributes();

        //----------------------------------------------------------
        // Make sure we only create child order items on a CONNECT.
        //----------------------------------------------------------
        if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {

        	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
        		if (this.getC30SDKObject(i) instanceof C30OrderItem) {
                    String error = "Lightweight Product Elements can only have child order items when connecting new products.";
                    error = error + " Error on product with type = " + this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    throw new C30SDKInvalidAttributeException(error);
        		}
        	}
        }

        if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID) &&
            !this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID) &&
            !this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)){

            String error = "Invalid item action id: (" + this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                    ").  Item action id 10, 20 or 30 must be used for a Product";
            throw new C30SDKInvalidAttributeException(error);
        }
    }

    /**
     * Creates child order items for the Lightweight Product.  LW Products only support
     * LW Product Overrides as child order items.
     * @return C30SDKObject the returned child order items that were created.
     * @throws C30SDKException 
     */
    protected final C30SDKObject createChildOrderItems() throws C30SDKException {
    	C30SDKObject children = this.getFactory().createC30SDKObject(C30SDKCollection.class, new Object[0]);
        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
            C30SDKObject lwoi = (C30OrderItem) this.getC30SDKObject(i);

            lwoi.setAttributeValue(C30ProductOverride.ATTRIB_TRACKING_ID, this.getAttributeValue(ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setAttributeValue(C30ProductOverride.ATTRIB_TRACKING_ID_SERV, this.getAttributeValue(ATTRIB_MEMBER_INST_ID2, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setNameValuePairs(this.getNameValuePairMap());
            lwoi = lwoi.process();

            if (lwoi instanceof C30SDKExceptionMessage) {
            	children = lwoi;
            	break;
            } else {
            	children.addC30SDKObject(lwoi);
            }
        }
        return children;
    }
   
    /**
     * Creates the Kenan FX Product view record using API TS.
     * @throws C30SDKKenanFxCoreException 
     */
    protected C30SDKObject createFXObject() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

    	C30SDKObject lwo = null;
    	
        //----------------------------------------------------------------------
        // Products can be used in "Add", "Change" or "Disconnect" order item
        // action ids.
        //----------------------------------------------------------------------
        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {
	        
        	//create hashmap shell
	        product = new HashMap();

	        //----------------------------------------------------------------------
	        // Set the following key product fields:
	        // View Status: Pending
	        // View Effective Date: Now
	        // View Create Date: Now
	        //----------------------------------------------------------------------
	        product.put(ATTRIB_VIEW_STATUS, C30OrderItem.FX_PENDING_VIEW_STATUS);
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
		        product.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
		        product.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }
	        product.put(ATTRIB_VIEW_CREATED_DT, new Date());
	        product.put(ATTRIB_ELEMENT_ID, this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        product.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        product.put(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

	        //----------------------------------------------------------------------
	        // If the product is service level, set the subscr_no/resets
	        // otherwise set the account no.  Also perform product level validation
	        //----------------------------------------------------------------------
	        if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
	                product.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	                product.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        }

            this.connectProduct();

        } else if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID) ||
        		this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CHANGE_ITEM_ACTION_ID)) {

            this.updateProduct();

        }
        lwo = this;
        
        return lwo;
    }

    private void connectProduct() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            product.put(ATTRIB_PRODUCT_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            product.put(ATTRIB_BILLING_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            product.put(ATTRIB_PRODUCT_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            product.put(ATTRIB_BILLING_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }

        setFXProductAttributes();

        // Create the Product View Row

        Map callResponse = this.queryData("Product", "Create", product, getAccountServerId());

        product = (HashMap) callResponse.get("Product");
        populateProduct();

        if (this.getAttributeValue(ATTRIB_OVERRIDE_RATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null){
        	C30SDKObject lwoi = this.getFactory().createC30SDKObject(C30ProductOverride.class, new Object[0]);

        	lwoi.setAttributeValue(ATTRIB_ITEM_ACTION_ID, ORD_ITEM_CONNECT_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	lwoi.setAttributeValue(ATTRIB_MEMBER_ID, new Integer(-1), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setAttributeValue(C30ProductOverride.ATTRIB_OVERRIDE_RATE, this.getAttributeValue(ATTRIB_OVERRIDE_RATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setAttributeValue(C30ProductOverride.ATTRIB_CURRENCY_CODE, C30SDKValueConstants.GLOBAL_LANGUAGE_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            this.addC30SDKObject(lwoi);
        }

    }

    private void updateProduct() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        this.findProduct();
        product.put(ATTRIB_PREV_VIEW_ID, this.getAttributeValue(ATTRIB_VIEW_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        if(this.getAttributeValue(ATTRIB_ARCH_FLAG, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_ARCH_FLAG, this.getAttributeValue(ATTRIB_ARCH_FLAG, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_AUTO_ACTIVATION, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_AUTO_ACTIVATION, this.getAttributeValue(ATTRIB_AUTO_ACTIVATION, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, this.getAttributeValue(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_BILLING_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_BILLING_ACTIVE_DT, this.getAttributeValue(ATTRIB_BILLING_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_CREATE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_CREATE_DT, this.getAttributeValue(ATTRIB_CREATE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, this.getAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }

        if(this.getAttributeValue(ATTRIB_PRODUCT_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_PRODUCT_ACTIVE_DT, this.getAttributeValue(ATTRIB_PRODUCT_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_TRACKING_ID, this.getAttributeValue(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_TRACKING_ID_SERV, this.getAttributeValue(ATTRIB_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            product.put(ATTRIB_CHG_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            product.put(ATTRIB_CHG_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if(this.getAttributeValue(ATTRIB_EXTENDED_DATA, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            product.put(ATTRIB_EXTENDED_DATA, this.getAttributeValue(ATTRIB_EXTENDED_DATA, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }

        product.put(ATTRIB_VIEW_STATUS, C30OrderItem.FX_PENDING_VIEW_STATUS);

        // Set product attributes from the lw order item attributes
        setFXProductAttributes();

        // Set Inactive Date if it is a product Disconnect Order Item
        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            	product.put(ATTRIB_PRODUCT_INACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            }
            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            	product.put(ATTRIB_PRODUCT_INACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            }
        }

        Map callResponse = this.queryData("Product", "Create", product, getAccountServerId());

        product = (HashMap) callResponse.get("Product");
        populateProduct();

    }

    private void setFXProductAttributes() throws C30SDKInvalidAttributeException {

        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if(attributeName.equals(ATTRIB_CONNECT_REASON) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_COMPONENT_ID) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_ELEMENT_ID) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CONTRACT_ASSOCIATION_TYPE) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CONTRACT_TRACKING_ID) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CONTRACT_TRACKING_ID_SERV) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_IS_PART_OF_COMPONENT) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_BILL_PERIOD) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CHARGE_ORDER) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CONVERTED) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_HAS_OVERRIDES) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_HAS_PRODUCT_KEYS) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_IN_ARREARS_OVERRIDE) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_NO_BILL) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_OPEN_ITEM_ID) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_ORDER_NUMBER) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_SALES_CHANNEL_ID) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_SERIAL_NUMBER) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_CHG_WHO) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_DISCONNECT_REASON) && attributeValue != null) {
                product.put(attributeName, attributeValue);
            } else {
                //------------------------------------------------------
                // check if the attribute is an extended data parameter
                //------------------------------------------------------
                try {
                    if (attributeValue != null) {

                    	//check if the attribute is extended data
                        Integer paramId = new Integer(attributeName);

                        //if we do not already have extended data attributes then create an array
                        if (product.get(ATTRIB_EXTENDED_DATA) == null) {
                            product.put(ATTRIB_EXTENDED_DATA, new Object[1]);

	                        //create the extended data attribute
	                        HashMap extData = new HashMap();
	                        extData.put(ATTRIB_PARAM_ID, paramId);
	                        extData.put(ATTRIB_PARAM_VALUE, attributeValue);

	                        //add it to the array
	                        Object[] newDataParams = C30HashMapUtils.addObjectToArray( (Object[]) product.get(ATTRIB_EXTENDED_DATA), extData);

	                        //put the array into the service data.
	                        product.put(ATTRIB_EXTENDED_DATA, newDataParams);

                        } else {
                            C30HashMapUtils.removeDuplicatesFromArray(product, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

                            Object [] extendedData = (Object[]) product.get(ATTRIB_EXTENDED_DATA);
                            HashMap xdHashMap = new HashMap(((HashMap) extendedData[0]));

                            // set the extended data attributes
                            xdHashMap.put(ATTRIB_PARAM_ID, paramId);
                            xdHashMap.put(ATTRIB_PARAM_VALUE, attributeValue);

                            // add it uniquely to product
                            Object [] newDataParams = C30HashMapUtils.addUniqueHashMapToArray(extendedData, xdHashMap, ATTRIB_PARAM_ID);
                            product.put(ATTRIB_EXTENDED_DATA, newDataParams);
                        }

                    }
                } catch (NumberFormatException e) {
                    // not a valid param id
                }
            }
        }

    }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    protected void loadCache() {

    }

}
