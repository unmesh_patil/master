/*
 * C30Service.java
 *
 * Created on December 8, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;



import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.cache.C30SDKCacheableWrapper;
import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30KenanMWDataSource;
import com.cycle30.sdk.datasource.C30TableDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30HashMapUtils;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX Service object and its corresponding order item.
 * @author John Reeves
 */
public class C30Service extends C30OrderItem {

    private static Logger log = Logger.getLogger(C30Service.class);

    public static final String ATTRIB_ACTIVE_DT = "ActiveDt";
    private static final String ATTRIB_ADDRESS_ID = "AddressId";
    public static final String ATTRIB_ADDRESS1 = "Address1";
    public static final String ATTRIB_ADDRESS2 = "Address2";
    public static final String ATTRIB_ADDRESS3 = "Address3";
    public static final String ATTRIB_ADDRESS4 = "Address4";
    public static final String ATTRIB_ADDRESS_LOCATION = "AddressLocation";
    public static final String ATTRIB_ADDRESS_TYPE_ID = "AddressTypeId";
    private static final String ATTRIB_B_ADDRESS_ID = "BAddressId";
    public static final String ATTRIB_B_ADDRESS1 = "BAddress1";
    public static final String ATTRIB_B_ADDRESS2 = "BAddress2";
    public static final String ATTRIB_B_ADDRESS3 = "BAddress3";
    public static final String ATTRIB_B_ADDRESS4 = "BAddress4";
    public static final String ATTRIB_B_ADDRESS_LOCATION = "BAddressLocation";
    public static final String ATTRIB_B_ADDRESS_TYPE_ID = "BAddressTypeId";
    public static final String ATTRIB_B_CITY = "BCity";
    public static final String ATTRIB_B_COUNTRY_CODE = "BCountryCode";
    public static final String ATTRIB_B_COUNTY = "BCounty";
    public static final String ATTRIB_B_EXTENDED_POSTAL_CODE = "BExtendedPostalCode";
    public static final String ATTRIB_B_EXTERNAL_ADDRESS_ID = "BExternalAddressId";
    public static final String ATTRIB_B_FRANCHISE_TAX_CODE = "BFranchiseTaxCode";
    public static final String ATTRIB_B_FX_GEOCODE = "BFxGeocode";
    public static final String ATTRIB_B_HOUSE_NUMBER = "BHouseNumber";
    public static final String ATTRIB_B_HOUSE_NUMBER_SUFFIX = "BHouseNumberSuffix";
    public static final String ATTRIB_B_KEY_LINE_CODE = "BKeyLineCode";
    public static final String ATTRIB_B_NEAREST_CROSSSTREET = "BNearestCrossstreet";
    public static final String ATTRIB_B_OPTIONAL_ENDORSEMENT_LINE = "BOptionalEndorsementLine";
    public static final String ATTRIB_B_POSTAL_CODE = "BPostalCode";
    public static final String ATTRIB_B_POSTFIX_DIRECTIONAL = "BPostfixDirectional";
    public static final String ATTRIB_B_POSTNET_ADDRESS_BLOCK_BARCODE = "BPostnetAddressBlockBarcode";
    public static final String ATTRIB_B_POSTNET_BARCODE = "BPostnetBarcode";
    public static final String ATTRIB_B_PREFIX_DIRECTIONAL = "BPrefixDirectional";
    public static final String ATTRIB_B_SERVICE_ADDRESS_ASSOC_ID = "BServiceAddressAssocId";
    public static final String ATTRIB_B_STATE = "BState";
    public static final String ATTRIB_B_STREET_NAME = "BStreetName";
    public static final String ATTRIB_B_STREET_SUFFIX = "BStreetSuffix";
    public static final String ATTRIB_B_UNIT_NO = "BUnitNo";
    public static final String ATTRIB_B_UNIT_TYPE = "BUnitType";
    public static final String ATTRIB_B_VERTEX_GEOCODE = "BVertexGeocode";    
    public static final String ATTRIB_B_REV_RCV_COST_CTR = "BRevRcvCostCtr";
    public static final String ATTRIB_B_SERVICE_ADDRESS1 = "BServiceAddress1";
    public static final String ATTRIB_B_SERVICE_ADDRESS2 = "BServiceAddress2";
    public static final String ATTRIB_B_SERVICE_ADDRESS3 = "BServiceAddress3";
    public static final String ATTRIB_B_SERVICE_CITY = "BServiceCity";
    public static final String ATTRIB_B_SERVICE_COMPANY = "BServiceCompany";
    public static final String ATTRIB_B_SERVICE_COUNTRY_CODE = "BServiceCountryCode";
    public static final String ATTRIB_B_SERVICE_COUNTY = "BServiceCounty";
    public static final String ATTRIB_B_SERVICE_FNAME = "BServiceFname";
    public static final String ATTRIB_B_SERVICE_FRANCHISE_TAX_CODE = "BServiceFranchiseTaxCode";
    public static final String ATTRIB_B_SERVICE_GEOCODE = "BServiceGeocode";
    public static final String ATTRIB_B_SERVICE_LNAME = "BServiceLname";
    public static final String ATTRIB_B_SERVICE_MINIT = "BServiceMinit";
    public static final String ATTRIB_B_SERVICE_PHONE = "BServicePhone";
    public static final String ATTRIB_B_SERVICE_PHONE2 = "BServicePhone2";
    public static final String ATTRIB_B_SERVICE_STATE = "BServiceState";
    public static final String ATTRIB_B_SERVICE_ZIP = "BServiceZip";
    public static final String ATTRIB_CHG_DT = "ChgDt";
    public static final String ATTRIB_CHG_WHO = "ChgWho";
    public static final String ATTRIB_CITY = "City";
    public static final String ATTRIB_CODEWORD = "Codeword";
    public static final String ATTRIB_CONVERTED = "Converted";
    public static final String ATTRIB_COUNTRY_CODE = "CountryCode";
    public static final String ATTRIB_COUNTY = "County";
    public static final String ATTRIB_CREATE_DT = "CreateDt";
    public static final String ATTRIB_CURRENCY_CODE = "CurrencyCode";
    private static final String ATTRIB_DEFAULT_EXTERNAL_ID_TYPE = "DefaultExternalIdType";
    public static final String ATTRIB_DISPLAY_ADDRESS_ID = "DisplayAddressId";
    public static final String ATTRIB_DISPLAY_CIEM_VIEW_ID = "DisplayCiemViewId";
    private static final String ATTRIB_DISPLAY_EXTERNAL_ID_TYPE = "DisplayExternalIdType";
    public static final String ATTRIB_ELIG_KEY1 = "EligKey1";
    public static final String ATTRIB_ELIG_KEY2 = "EligKey2";
    public static final String ATTRIB_ELIG_KEY3 = "EligKey3";
    public static final String ATTRIB_EMF_CONFIG_ID = "EmfConfigId";
    public static final String ATTRIB_EXRATE_CLASS = "ExrateClass";
    private static final String ATTRIB_EXTENDED_DATA = "ExtendedData";
    public static final String ATTRIB_EXTENDED_POSTAL_CODE = "ExtendedPostalCode";
    public static final String ATTRIB_EXTERNAL_ADDRESS_ID = "ExternalAddressId";
    public static final String ATTRIB_FRANCHISE_TAX_CODE = "FranchiseTaxCode";
    public static final String ATTRIB_FX_GEOCODE = "FxGeocode";
    public static final String ATTRIB_HOUSE_NUMBER = "HouseNumber";
    public static final String ATTRIB_HOUSE_NUMBER_SUFFIX = "HouseNumberSuffix";
    public static final String ATTRIB_KEY_LINE_CODE = "KeyLineCode";
    public static final String ATTRIB_INACTIVE_DATE = "InactiveDate";
    public static final String ATTRIB_INTENDED_VIEW_EFFECTIVE_DT = "IntendedViewEffectiveDt";
    public static final String ATTRIB_IS_CURRENT = "IsCurrent";
    public static final String ATTRIB_IS_PREPAID = "IsPrepaid";
    public static final String ATTRIB_IXC_PROVIDER_ID = "IxcProviderId";
    private static final String ATTRIB_LANGUAGE_CODE = "LanguageCode";
    public static final String ATTRIB_LEC_PROVIDER_ID = "LecProviderId";
    public static final String ATTRIB_NEAREST_CROSSSTREET = "NearestCrossstreet";
    public static final String ATTRIB_NONPUB_NONLIST = "NonpubNonlist";
    public static final String ATTRIB_NO_BILL = "NoBill";
    private static final String ATTRIB_OPTIONAL_ENDORSEMENT_LINE = "OptionalEndorsementLine";
    private static final String ATTRIB_PARAM_ID = "ParamId";
    private static final String ATTRIB_PARAM_VALUE = "ParamValue";
    public static final String ATTRIB_PIC_DATE_ACTIVE = "PicDateActive";
    public static final String ATTRIB_POP_UNITS = "PopUnits";
    public static final String ATTRIB_POSTAL_CODE = "PostalCode";
    public static final String ATTRIB_POSTFIX_DIRECTIONAL = "PostfixDirectional";
    public static final String ATTRIB_PREV_VIEW_ID = "PrevViewId";
    public static final String ATTRIB_PREFIX_DIRECTIONAL = "PrefixDirectional";
    public static final String ATTRIB_PRIVACY_LEVEL = "PrivacyLevel";
    public static final String ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE = "PostnetAddressBlockBarcode";
    public static final String ATTRIB_POSTNET_BARCODE = "PostnetBarcode";
    public static final String ATTRIB_RATE_CLASS = "RateClass";
    public static final String ATTRIB_RESTRICTED_PIC = "RestrictedPic";
    public static final String ATTRIB_REV_RCV_COST_CTR = "RevRcvCostCtr";
    public static final String ATTRIB_SALES_CHANNEL_ID = "SalesChannelId";
    public static final String ATTRIB_SERVICE_ACTIVE_DT = "ServiceActiveDt";
    public static final String ATTRIB_SERVICE_ADDRESS_ASSOC_ID = "ServiceAddressAssocId";
    public static final String ATTRIB_SERVICE_ADDRESS1 = "ServiceAddress1";
    public static final String ATTRIB_SERVICE_ADDRESS2 = "ServiceAddress2";
    public static final String ATTRIB_SERVICE_ADDRESS3 = "ServiceAddress3";
    public static final String ATTRIB_SERVICE_CITY = "ServiceCity";
    public static final String ATTRIB_SERVICE_COMPANY = "ServiceCompany";
    public static final String ATTRIB_SERVICE_COUNTRY_CODE = "ServiceCountryCode";
    public static final String ATTRIB_SERVICE_COUNTY = "ServiceCounty";
    public static final String ATTRIB_SERVICE_EXTERNAL_ID = "ServiceExternalId";
    public static final String ATTRIB_SERVICE_EXTERNAL_ID_TYPE = "ServiceExternalIdType";
    public static final String ATTRIB_SERVICE_FNAME = "ServiceFname";
    public static final String ATTRIB_SERVICE_FRANCHISE_TAX_CODE = "ServiceFranchiseTaxCode";
    public static final String ATTRIB_SERVICE_GEOCODE = "ServiceGeocode";
    public static final String ATTRIB_SERVICE_INACTIVE_DT = "ServiceInactiveDt";
    public static final String ATTRIB_SERVICE_INTERNAL_ID = "ServiceInternalId";
    public static final String ATTRIB_SERVICE_INTERNAL_ID_RESETS = "ServiceInternalIdResets";
    public static final String ATTRIB_SERVICE_LNAME = "ServiceLname";
    public static final String ATTRIB_SERVICE_MINIT = "ServiceMinit";
    public static final String ATTRIB_SERVICE_NAME_GENERATION = "ServiceNameGeneration";
    public static final String ATTRIB_SERVICE_NAME_PRE = "ServiceNamePre";
    public static final String ATTRIB_SERVICE_PHONE = "ServicePhone";
    public static final String ATTRIB_SERVICE_PHONE2 = "ServicePhone2";
    public static final String ATTRIB_SERVICE_STATE = "ServiceState";
    public static final String ATTRIB_SERVICE_ZIP = "ServiceZip";
    public static final String ATTRIB_SIM_SERIAL_NUMBER = "SimSerialNumber";
    public static final String ATTRIB_STATE = "State";
    public static final String ATTRIB_STATUS_ID = "StatusId";
    public static final String ATTRIB_STATUS_REASON_ID = "StatusReasonId";
    public static final String ATTRIB_STREET_NAME = "StreetName";
    public static final String ATTRIB_STREET_SUFFIX = "StreetSuffix";
    public static final String ATTRIB_SWITCH_ID = "SwitchId";
    public static final String ATTRIB_TIMEZONE = "Timezone";
    public static final String ATTRIB_UNIT_NO = "UnitNo";
    public static final String ATTRIB_UNIT_TYPE = "UnitType";
    public static final String ATTRIB_VERTEX_GEOCODE = "VertexGeocode";
    public static final String ATTRIB_VIEW_CREATED_DT = "ViewCreatedDt";
    public static final String ATTRIB_VIEW_EFFECTIVE_DT = "ViewEffectiveDt";
    public static final String ATTRIB_VIEW_STATUS = "ViewStatus";
    
    private static final String SERVICEABILITY_MODULE = "SVBL";
    private static final String SERVICEABILITY_DISABLED_STR = "SERVICEABILITY_DISABLED";
    private static final Integer SERVICEABILITY_ENABLED = new Integer(0);
    private static final Integer SERVICEABILITY_DISABLED = new Integer(1);
    private static final String ATTRIB_MODULE = "Module";
    private static final String ATTRIB_PARAMETER_NAME = "ParameterName";
    private static final String ATTRIB_INT_VALUE = "IntValue";
    
    private static final String SERVICE_IS_EXT_ID_GENERATED_CACHE = "SERVICE_IS_EXT_ID_GENERATED_CACHE";
    private static final String SERVICE_IS_SERVICEABILITY_ENABLED_CACHE = "SERVICE_IS_SERVICEABILITY_ENABLED_CACHE";
    
    private static final String[] SERVICE_ADDRESS_ATTRIBS = {ATTRIB_ADDRESS1, ATTRIB_ADDRESS2, ATTRIB_ADDRESS3, ATTRIB_ADDRESS4, ATTRIB_ADDRESS_LOCATION, 
    														ATTRIB_ADDRESS_TYPE_ID, ATTRIB_CITY, ATTRIB_COUNTRY_CODE, ATTRIB_COUNTY, ATTRIB_DISPLAY_ADDRESS_ID, ATTRIB_EXTENDED_POSTAL_CODE, 
    														ATTRIB_EXTERNAL_ADDRESS_ID, ATTRIB_FRANCHISE_TAX_CODE, ATTRIB_FX_GEOCODE, ATTRIB_HOUSE_NUMBER, 
    														ATTRIB_HOUSE_NUMBER_SUFFIX, ATTRIB_KEY_LINE_CODE, ATTRIB_NEAREST_CROSSSTREET, ATTRIB_OPTIONAL_ENDORSEMENT_LINE, 
    														ATTRIB_POSTAL_CODE, ATTRIB_POSTFIX_DIRECTIONAL, ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE, ATTRIB_POSTNET_BARCODE, 
    														ATTRIB_PREFIX_DIRECTIONAL, ATTRIB_SERVICE_ADDRESS_ASSOC_ID, ATTRIB_STATE, ATTRIB_STREET_NAME, 
    														ATTRIB_STREET_SUFFIX, ATTRIB_UNIT_NO, ATTRIB_UNIT_TYPE, ATTRIB_VERTEX_GEOCODE};
    private static final String[] B_END_ADDRESS_ATTRIBS = {ATTRIB_B_ADDRESS1, ATTRIB_B_ADDRESS2, ATTRIB_B_ADDRESS3, ATTRIB_B_ADDRESS4, ATTRIB_B_ADDRESS_LOCATION, 
															ATTRIB_B_ADDRESS_TYPE_ID, ATTRIB_B_CITY, ATTRIB_B_COUNTRY_CODE, ATTRIB_B_COUNTY, ATTRIB_B_EXTENDED_POSTAL_CODE, 
															ATTRIB_B_EXTERNAL_ADDRESS_ID, ATTRIB_B_FRANCHISE_TAX_CODE, ATTRIB_B_FX_GEOCODE, ATTRIB_B_HOUSE_NUMBER, 
															ATTRIB_B_HOUSE_NUMBER_SUFFIX, ATTRIB_B_KEY_LINE_CODE, ATTRIB_B_NEAREST_CROSSSTREET, ATTRIB_B_OPTIONAL_ENDORSEMENT_LINE, 
															ATTRIB_B_POSTAL_CODE, ATTRIB_B_POSTFIX_DIRECTIONAL, ATTRIB_B_POSTNET_ADDRESS_BLOCK_BARCODE, ATTRIB_B_POSTNET_BARCODE, 
															ATTRIB_B_PREFIX_DIRECTIONAL, ATTRIB_B_SERVICE_ADDRESS_ASSOC_ID, ATTRIB_B_STATE, ATTRIB_B_STREET_NAME, 
															ATTRIB_B_STREET_SUFFIX, ATTRIB_B_UNIT_NO, ATTRIB_B_UNIT_TYPE, ATTRIB_B_VERTEX_GEOCODE};
    
    private Map service = null;
    private static Integer startRecord = 1;
    private static Integer returnSetSize = 50;

    /**
     * Creates a new instance of C30Service.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Service(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Service(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30Service.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is invalid.
     */
    protected final void initializeAttributes() throws C30SDKInvalidAttributeException{

    	super.initializeAttributes();
        super.setClassFieldConfiguration();
           
        C30SDKAttribute attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_SERVICE_MEMBER_TYPE);
    }


    /**
     * jcw added: LWO Enhancement must have either Account External Id and Account External Id Type
     *  or Account Internal Id  or  AccountServerId and Account Internal Id
     * @throws C30SDKInvalidAttributeException
     */
    protected final void validateFindAttributes()
        throws C30SDKInvalidAttributeException
    {
        String  acctExternalId = null;
        Integer acctExternalIdType = null;
        Integer acctInternalId = null;
        String  serviceExternalId = null;
        Integer serviceExternalIdType = null;
        Integer accountServerId = null;
       
        if (isAttribute("Find",C30SDKValueConstants.ATTRIB_TYPE_INPUT))
          if((Boolean)getAttributeValue("Find", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
           {  
              if (isAttribute("AccountExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
               acctExternalId = (String)getAttributeValue("AccountExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              if (isAttribute("AccountExternalIdType",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
                acctExternalIdType = (Integer)getAttributeValue("AccountExternalIdType",C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              if (isAttribute("AccountInternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
                 acctInternalId = (Integer)getAttributeValue("AccountInternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
             
              if (isAttribute("ServiceExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
                   serviceExternalId = (String)getAttributeValue("ServiceExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              if (isAttribute("ServiceExternalIdType",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
                    serviceExternalIdType = (Integer)getAttributeValue("ServiceExternalIdType",C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              
               if (isAttribute("AccountServerId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
                  accountServerId = (Integer)getAttributeValue("AccountServerId",C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
               
                         
              if (((acctExternalId == null || acctExternalIdType == null) && acctInternalId == null) &&
                  ((serviceExternalId == null || serviceExternalIdType == null) && accountServerId == null ) ) 
              {
                  throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-033"),"INVALID-ATTRIB-033");
              }
                  
           }       
    }
    

    /**
     * Validates the item action id of the order item.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is invalid.
     * @throws C30SDKKenanFxCoreException 
     */
    protected final void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        
    	super.validateAttributes();
        
    	if (!((Integer) super.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID) &&
            !((Integer) super.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID)){

           throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-034"),"INVALID-ATTRIB-034");
        }
    	
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        
        	// if we are doing a change service order and the address attributes are set to change
        	// then throw an exception that the address can only change with a MOVE request.
        	if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceServiceOrder.SERV_ORD_TYPE_CHANGE)) {
        		if (this.isServiceAddressChange()) {
                    String error = "All service address attributes can only be changed through a service order move\n";
                    error = error + "request (ServiceOrderTypeId = 56). Please check the following address attributes\n";
                    error = error + "(Address1, Address2, Address3, Address4, AddressLocation, AddressTypeId, City, CountryCode,\n";
                    error = error + "County, ExtendedPostalCode, ExternalAddressId, FranchiseTaxCode, FxGeocode, HouseNumber,\n";
                    error = error + "HouseNumberSuffix, KeyLineCode, NearestCrossstreet, OptionalEndorsementLine, PostalCode,\n";
                    error = error + "PostfixDirectional, ServiceAddressAssocId, State, StreetName, StreetSuffix, UnitNo, UnitType,\n";
                    error = error + "VertexGeocode)";
                    throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-035"),"INVALID-ATTRIB-035");
        		}
        		if (this.isBEndAddressChange()) {
                    String error = "All service address attributes can only be changed through a service order move\n";
                    error = error + "request (ServiceOrderTypeId = 56). Please check the following address attributes\n";
                    error = error + "(BAddress1, BAddress2, BAddress3, BAddress4, BAddressLocation, BAddressTypeId, BCity, BCountryCode,\n";
                    error = error + "BCounty, BExtendedPostalCode, BExternalAddressId, BFranchiseTaxCode, BFxGeocode, BHouseNumber,\n";
                    error = error + "BHouseNumberSuffix, BKeyLineCode, BNearestCrossstreet, BOptionalEndorsementLine, BPostalCode,\n";
                    error = error + "BPostfixDirectional, BServiceAddressAssocId, BState, BStreetName, BStreetSuffix, BUnitNo, BUnitType,\n";
                    error = error + "BVertexGeocode)";
                    throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-035"),"INVALID-ATTRIB-035");
        		}
        	}
        
        }
        
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);
        
        
        if (isAttribute(ATTRIB_MEMBER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            if (name.equals(this.getAttribute(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
                    super.loadExtendedDataAttributes("SERVICE_VIEW", (Integer) this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            }
     
        if (isAttribute(ATTRIB_DISPLAY_ADDRESS_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT))  
            if (name.equals(this.getAttribute(ATTRIB_DISPLAY_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
                    this.getAttribute(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
                    this.getAttribute(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
                    this.getAttribute(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
                    this.getAttribute(ATTRIB_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
            }
    	    	
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Integer)
     */
    public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
    	super.setAttributeValue(attributeName, attributeValue, type);

        if (isAttribute(ATTRIB_MEMBER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            if (attributeName.equals(ATTRIB_MEMBER_ID)) {
                    super.loadExtendedDataAttributes("SERVICE_VIEW", (Integer) this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            }
    	//if we are setting the action ID
    	if (attributeName.equals(ATTRIB_ITEM_ACTION_ID)) {
    		if (attributeValue.toString().equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID.toString())) {
    			initializeServiceCreateAttributes();
    		}
    	}
        
  
        if (isAttribute(ATTRIB_DISPLAY_ADDRESS_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT))  
            if (attributeName.equals(this.getAttribute(ATTRIB_DISPLAY_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
                    this.getAttribute(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
                    this.getAttribute(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
                    this.getAttribute(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
                    this.getAttribute(ATTRIB_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(false);
            }
    	    	
    }

    private C30SDKObject getAssociatedAccount()
        throws C30SDKException
    {
        C30SDKObject account = null;
        // add the attributes here - not previously part of the order-service_order processing only for the find 
        addAttribute(new C30SDKAttribute("AccountInternalId", java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        addAttribute(new C30SDKAttribute("Account", com.cycle30.sdk.core.framework.C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if(!isAttributeValueSet("AccountInternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            if(isAttributeValueSet("AccountExternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttributeValueSet("AccountExternalIdType", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            {
                account = getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30Account.class, new Object[0]);
                account.setAttributeValue("AccountExternalId", getAttributeValue("AccountExternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account.setAttributeValue("AccountExternalIdType", getAttributeValue("AccountExternalIdType", C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account = account.process();
                if(account instanceof C30Account)
                {
                    setAttributeValue("AccountInternalId", account.getAttributeValue("AccountInternalId", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    setAttributeValue("Account", account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                }
            } else
            {
                String errorMsg = "The account internal id or external id and external id type must be set in order to create or fetch a Kenan/FX object";
                throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-013"),"LW-OBJ-013");
            }
        return account;
    }

    /**
     * Method which sets specific attributes of the service if the service is being created.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is incorrectly set.
     */
    private void initializeServiceCreateAttributes() throws C30SDKInvalidAttributeException {
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
	    	C30SDKAttribute attribute = this.getAttribute(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        attribute.setRequired(true);
	        attribute = this.getAttribute(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        attribute.setRequired(true);
	        attribute = this.getAttribute(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        attribute.setRequired(true);
	        attribute = this.getAttribute(ATTRIB_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        attribute.setRequired(true);
        }
    }
    
    /**
     * Method which sets specific attributes of the service if the service is being created.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is incorrectly set.
     */
    protected void initializeServiceMoveAttributes() throws C30SDKInvalidAttributeException {
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
	    	
        	//if we are changing any service address values then set them to be required
        	if (this.getAttributeValue(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ||
        			this.getAttributeValue(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ||
        			this.getAttributeValue(ATTRIB_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ||
        			this.getAttributeValue(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	        	C30SDKAttribute attribute = this.getAttribute(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
		        attribute = this.getAttribute(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
		        attribute = this.getAttribute(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
		        attribute = this.getAttribute(ATTRIB_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
        	}
        	//if we are changing any b-end address values then set them to be required
        	if (this.getAttributeValue(ATTRIB_B_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ||
        			this.getAttributeValue(ATTRIB_B_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ||
        			this.getAttributeValue(ATTRIB_B_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ||
        			this.getAttributeValue(ATTRIB_B_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	        	C30SDKAttribute attribute = this.getAttribute(ATTRIB_B_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
		        attribute = this.getAttribute(ATTRIB_B_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
		        attribute = this.getAttribute(ATTRIB_B_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
		        attribute = this.getAttribute(ATTRIB_B_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        attribute.setRequired(true);
        	}
        }
        
    }
    
    private boolean isCacheSetLocally()   
          throws C30SDKInvalidAttributeException
    {
        if ((isAttribute("AccountServerId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue("AccountServerId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)  && 
           (( isAttribute("AccountInternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue("AccountInternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) ||
            ( isAttribute("AccountExternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue("AccountExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ) ||
            ( isAttribute("ServiceExternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue("ServiceExternalId",C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ) ) )
          return true;
          
          return false;
    }
  
    /** (non-Javadoc).
     * @return C30SDKObject the returned service that was processed.
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#process()
     */
    public final C30SDKObject process() throws C30SDKInvalidAttributeException {
        C30SDKObject lwo = null;
        Boolean lookupOnly = false;
    	//in case there is an error.
        String error = "Error processing service:\n\tLW Order Item Type: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        try {
        
    	    // jcw: added for FIND only transactions
    	     if (isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT))
    	       if((Boolean)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
    	            validateFindAttributes();
    	            super.clearOrderAttributes();
    	           lookupOnly = true;
    	       }  
       	        if (lookupOnly)
    	        {
    	           if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
    	                isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
    	                getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
    	            {    	            
                       // check to see if we need to explicitly get an account
                       if (isCacheSetLocally())
                           setAccountServerIdCache();
                       else{
                               if ((C30SDKObject)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null){
                                C30SDKObject account = getAssociatedAccount();
                                   if(account instanceof C30SDKExceptionMessage)
                                     return account;
                               }
                       }
                        setAccountServerId(); 
                    }
    	            // looking for one or more services
    	            if(isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) 
    	            {  
                       externalFindActiveService();
    	               setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	            }
                    else 
    	            if(isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) 
    	                {  
    	                   internalFindActiveService();
    	                   setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	                }
    	            else
    	              externalFindActiveServiceList();
    	            
    	            lwo = this;    
    	            
    	        } else
    	        {
        
	    	if (((Integer) this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(ORD_ITEM_FIND_ITEM_ACTION_ID)) {
	    		this.findService();
	    		lwo = this;
	    	} else {
	    		
	    		lwo = super.process();

	    		if (!(lwo instanceof C30SDKExceptionMessage)) {

	    			//if we are inside a service order CONNECT then check whether we need to create a special external ID.
	    			if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceOrder.SERV_ORD_TYPE_CONNECT)) {

	    				//if a customer is using Customer Center then we need to create a special external ID to allow them
			    		//to view usage records.  This is actually a bug but SE refuses to fix it.  The external ID for this
			    		//is set to SUBSCR_NO.SUBSCR_NO_RESETS
			        	//Integer externalIdType = (Integer) LightweightCache.getCache(SERVICE_IS_EXT_ID_GENERATED_CACHE);
			        	Integer externalIdType = (Integer)(this.getFactory().c30SdkFactoryCache.get(SERVICE_IS_EXT_ID_GENERATED_CACHE)).getValue();
			    		if (!externalIdType.equals(new Integer(-1))) {
			    			C30SDKObject lwExternalId = this.getFactory().createC30SDKObject(C30ServiceExternalId.class, new Object[0]);

			    	    	lwExternalId.setAttributeValue(ATTRIB_ITEM_ACTION_ID, C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			    	    	lwExternalId.setAttributeValue(ATTRIB_MEMBER_TYPE, C30ServiceExternalId.ORD_ITEM_SERVICE_EXT_ID_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			    	    	lwExternalId.setAttributeValue(ATTRIB_MEMBER_ID, externalIdType, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			                lwExternalId.setAttributeValue(C30ServiceExternalId.ATTRIB_SERVICE_EXTERNAL_ID, this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) + "." + this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			    	    	lwExternalId.setAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, this, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			                lwo = lwExternalId.process();
			    		}

	    			}

	    		}
	    	}
          }
    	} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
    		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}
      
    	return lwo;

    }

    private Integer getDisplayExternalIdType() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	//Hashmap to hold EMFConfigId data
    	HashMap emfConfigIdData = new HashMap();

    	//HashMap to hold EMFConfigId key
    	HashMap key = new HashMap();
    	key.put(ATTRIB_EMF_CONFIG_ID, this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	key.put(ATTRIB_LANGUAGE_CODE, C30SDKValueConstants.GLOBAL_LANGUAGE_CODE);

    	emfConfigIdData.put("Key", key);

        Map callResponse = this.queryData("EmfConfigId", "Get", emfConfigIdData, getAccountServerId());

        //get emf config ID response.
        HashMap emfConfigId = (HashMap) callResponse.get("EmfConfigId");

        this.setAttributeValue(ATTRIB_DISPLAY_EXTERNAL_ID_TYPE, (Integer) emfConfigId.get(ATTRIB_DEFAULT_EXTERNAL_ID_TYPE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        return (Integer) emfConfigId.get(ATTRIB_DEFAULT_EXTERNAL_ID_TYPE);
    }

    /**
     * Finds the Kenan/FX service based on external id and external id type.
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void externalFindActiveService() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	//==========================================================
    	// We have to do two things when finding a service using
    	// the external ID and type.  We have to find the customer
    	// id equip map and then from that we can find the service.
    	// the reason is because the ServiceExternalFind service
    	// returns a number of rows.
    	//==========================================================

    	//Create a HashMap to contain the CIEM filter's info
    	HashMap ciemFilter = new HashMap();

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	ciemFilter.put("Fetch", new Boolean(true));

        //-----------------------------
        // Only get the current view
        //-----------------------------
    	HashMap attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(2));
    	ciemFilter.put(ATTRIB_VIEW_STATUS, attrFilter);

        //-------------------------------------------
        // Only get services that are still active
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("IsNull", Boolean.TRUE);
    	ciemFilter.put(ATTRIB_INACTIVE_DATE, attrFilter);

        //-----------------------------
        // Set the External ID and type that is specified
        //-----------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	ciemFilter.put(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, attrFilter);

    	attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	ciemFilter.put(ATTRIB_SERVICE_EXTERNAL_ID, attrFilter);

        //-------------------------------------------
        // add is_current flag to the filter.
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", Boolean.TRUE);
    	ciemFilter.put(ATTRIB_IS_CURRENT, attrFilter);

        Map callResponse = this.queryData("CustomerIdEquipMap", "Find", ciemFilter, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
        	String error = "No active service found with external id = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", external id type = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-016"),"LW-OBJ-016");
        } else if (count > 1) {
        	String error = "Multiple services found with external id = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", external id type = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-017"),"LW-OBJ-017");
        }

        Map ciem = (HashMap) ((Object[]) callResponse.get("CustomerIdEquipMapList"))[0];

    	//Create a HashMap to contain the CIEM filter's info
    	HashMap serviceFilter = new HashMap();

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	serviceFilter.put("Fetch", new Boolean(true));

        //-----------------------------
        // Only get the current view
        //-----------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(2));
    	serviceFilter.put(ATTRIB_VIEW_STATUS, attrFilter);

        //-------------------------------------------
        // Only get services that are still active
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("IsNull", Boolean.TRUE);
    	serviceFilter.put(ATTRIB_INACTIVE_DATE, attrFilter);

        //-----------------------------
        // Set the External ID and type that is specified
        //-----------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", ciem.get(ATTRIB_SERVICE_INTERNAL_ID_RESETS));
    	serviceFilter.put(ATTRIB_SERVICE_INTERNAL_ID_RESETS, attrFilter);

    	attrFilter = new HashMap();
    	attrFilter.put("Equal", ciem.get(ATTRIB_SERVICE_INTERNAL_ID));
    	serviceFilter.put(ATTRIB_SERVICE_INTERNAL_ID, attrFilter);

        if (this.hasExtendedDataAttributes()) {
        	callResponse = this.queryData("Service", "Find", serviceFilter, getAccountServerId());
        } else {
        	callResponse = this.queryData("Service", "ExternalFind", serviceFilter, getAccountServerId());
        }

        //get the count
        count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
        	String error = "No active service found with external id = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", external id type = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-016"),"LW-OBJ-016");
        } else if (count > 1) {
        	String error = "Multiple services found with external id = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", external id type = " + this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-017"),"LW-OBJ-017");
        }

        service = (HashMap) ((Object[]) callResponse.get("ServiceList"))[0];


        populateService();

    }

    /**
     * Finds a list of Kenan/FX services based on external id and external id type.
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void externalFindActiveServiceList()
        throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException
    {
        int lastRecordReturned = 0;
        // set up start and returnSetSize constraints.
        if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
              getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
             startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
          
        if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                 getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
              returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        HashMap ciemFilter = new HashMap();
        ciemFilter.put("Fetch", new Boolean(true));
        HashMap attrFilter = new HashMap();
        attrFilter.put("Equal", new Integer(2));
        ciemFilter.put("ViewStatus", attrFilter);
        attrFilter = new HashMap();
        attrFilter.put("IsNull", Boolean.TRUE);
        ciemFilter.put("InactiveDate", attrFilter);
        attrFilter = new HashMap();
        if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
        {
           attrFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
           ciemFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, attrFilter);
        }
        else 
            if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
            {
                attrFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                ciemFilter.put("ParentAccountInternalId", attrFilter);
            }
        Map callResponse = queryData("Service", "ExternalFind", ciemFilter, getAccountServerId());
        int count = ((Integer)callResponse.get("Count")).intValue();
        if(count == 0)
        {
            setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            return;
        }
        HashMap aciem = (HashMap)((Object[])(Object[])callResponse.get("ServiceList"))[0];   
        int recordsProcessed = 0;
        if (startRecord == 1){
            this.service = aciem;
            this.populateService();
            this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            lastRecordReturned = 1;
            recordsProcessed++;
        }

        C30Service cloneLwo = (C30Service)super.clone();
        if (startRecord > 1)
          startRecord--;
         for (int i=startRecord; i < count; i++)
         {
             if (recordsProcessed >= returnSetSize )
               break;
            
                
             HashMap aCiem = (HashMap)((Object[])(Object[])callResponse.get("ServiceList"))[i];   
                
             // is this the first account we are processing?
             if (recordsProcessed == 0){
                 this.service = aCiem;
                 this.populateService();
               if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                     this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

               this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             }
             else
             {
                 C30Service lwo = (C30Service)cloneLwo.clone();
                 lwo.service = aCiem;
                 lwo.populateService();
                 // set values to pass back in result
                 if (lwo.isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) && (lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) != null ) )
                       lwo.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,lwo.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                 // add to our list of peer objects
                  if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                        lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                  lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                  super.addC30SDKObject(lwo);
                  }
                  recordsProcessed++;
                  lastRecordReturned = i + 1;
         }
        super.setProcessingAggregateTotalsAttributes(this, count, lastRecordReturned, recordsProcessed );
    }
    
 

    /**
     * Internal method which finds a service given either its external or internal ID's.
     * @throws C30SDKObjectConnectionException if there was a connection error while making middleware requests.
     * @throws C30SDKInvalidAttributeException if an invalid attribute exists.
     * @throws C30SDKObjectException thrown if neither the external or internal ID's are available for searching for the service.
     * @throws C30SDKKenanFxCoreException 
     */
    private void findService() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKKenanFxCoreException {

    	if (!this.isAttributeValueSet(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) ||
    			!this.isAttributeValueSet(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
    		if (this.isAttributeValueSet(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
    				this.isAttributeValueSet(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {

    			// Find the service using the EXTERNAL id.
    			this.externalFindActiveService();

    		} else {
    			String error = "Must set either external id/type or internal id/resets for the lightweight service order";
    			throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-018"),"LW-OBJ-018");
    		}

    	} else {

    		// Find the service using the INTERNAL id.
    		this.internalFindActiveService();

    	}

    }

    /**
     * Internal method which finds a service given its internal ID's.
     * @throws C30SDKObjectConnectionException if there was a connection error while making middleware requests.
     * @throws C30SDKInvalidAttributeException if an invalid attribute exists.
     * @throws C30SDKObjectException thrown if either no service or multiple services was found.
     * @throws C30SDKKenanFxCoreException 
     */
    private void internalFindActiveService() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	//Create a HashMap to contain the CIEM filter's info
    	HashMap serviceFilter = new HashMap();

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	serviceFilter.put("Fetch", new Boolean(true));

        //-----------------------------
        // Only get the current view
        //-----------------------------
    	HashMap attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(2));
    	serviceFilter.put(ATTRIB_VIEW_STATUS, attrFilter);

        //-------------------------------------------
        // Only get services that are still active
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("IsNull", Boolean.TRUE);
    	serviceFilter.put(ATTRIB_INACTIVE_DATE, attrFilter);


        //-------------------------------------------
        // add the service internal id to the filter
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString()));
    	serviceFilter.put(ATTRIB_SERVICE_INTERNAL_ID, attrFilter);

        //------------------------------------------------
        // add the service internal resets to the filter
        //------------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString()));
    	serviceFilter.put(ATTRIB_SERVICE_INTERNAL_ID_RESETS, attrFilter);

        Integer serverId = getAccountServerId();

        Map callResponse = null;
        if (this.hasExtendedDataAttributes()) {
        	callResponse = this.queryData("Service", "Find", serviceFilter, getAccountServerId());
        } else {
        	callResponse = this.queryData("Service", "ExternalFind", serviceFilter, getAccountServerId());
        }

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
        	String error = "No active service found with subscr no = " + this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", subscr no resets = " + this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-019"),"LW-OBJ-019");
        } else if (count > 1) {
        	String error = "Multiple services found with subscr_no = " + this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", subscr no resets = " + this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-020"),"LW-OBJ-020");
        }
        service = (HashMap) ((Object[]) callResponse.get("ServiceList"))[0];

        populateService();
    }

    /**
     * Method to create the service itself.
     * @throws C30SDKException 
     */
    protected final C30SDKObject createFXObject() throws C30SDKException {
    	log.debug("Starting C30Service.createFXObject");

    	C30SDKObject lwo = null;
    	
        //----------------------------------------------------------------------
        // If this is a change order we have to get the previous view id
        // so we can initialize all previous attribute values that are not
        // explicitly set in the LW0.  If it is a connect order do nothing. The
        // Service view row will have already been created.
        //----------------------------------------------------------------------
        if (((Integer) super.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(ORD_ITEM_CHANGE_ITEM_ACTION_ID)) {

        	//-----------------
        	//find the service
        	//-----------------

        	//lets first see if we have a service order we can get the internal ID's from.
        	if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
        		this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        		this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	}

        	if (service == null) {
        		this.internalFindActiveService();
        	}

            //remove duplicate extended data - this is a hack to fix a core API-TS bug
            C30HashMapUtils.removeDuplicatesFromArray(service, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

            //-------------------------------------------------------------------
            // The following values are not overrideable.  They are specially set
            // based on account and order attributes
            //-------------------------------------------------------------------

            //copy the view ID to the previous view ID
            HashMap key = (HashMap) service.get("Key");
            BigInteger viewId = (BigInteger) key.get("ViewId");
            service.put("PrevViewId", viewId);

            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
                service.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
            }
            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
                service.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
            }

            // Set the overrideable attributes and create the service
            this.setFXServiceAttributes(service);

	        Map callResponse = this.queryData("Service", "Create", service, getAccountServerId());
	        service = (HashMap) callResponse.get("Service");
	        populateService();
	        
	        // this is where we check if they are changing an address.  If an address is being changed
	        // and they are using serviceability then we need to make sure that the corresponding
	        // address association is created for the new address.
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {

	        	if (this.isServiceAddressChange()) {       		
	    			lwo = this.getAddress(C30Address.ADDR_CATEGORY_SERVICE);	    			
					if (!(lwo instanceof C30SDKExceptionMessage)) {
						this.addC30SDKObject(createAddressAssoc(C30Address.ADDR_CATEGORY_SERVICE));
					}
	        	}
	        	
				if (!(lwo instanceof C30SDKExceptionMessage)) {
		        	if (this.isBEndAddressChange()) {       		
						lwo = this.getAddress(C30Address.ADDR_CATEGORY_B_END);
						if (!(lwo instanceof C30SDKExceptionMessage)) {
							this.addC30SDKObject(createAddressAssoc(C30Address.ADDR_CATEGORY_B_END));
						}
		        	}
				}
				
	            // check whether serviceability is enabled.  If it is then we make a call to check whether
	            // the service which has been created is serviceable at that location.  Unfortunately it is
	            // not possible to check for serviceability before the service is created.  Makes no sense!!
            	//Integer isServiceabilityEnabled = (Integer) LightweightCache.getCache(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE);
            	Integer isServiceabilityEnabled = (Integer)(this.getFactory().c30SdkFactoryCache.get(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE)).getValue();
            	
            	if (isServiceabilityEnabled.equals(SERVICEABILITY_ENABLED) 
            			&& (this.isServiceAddressChange() || this.isBEndAddressChange())) {
            		
            		if (!this.isServiceServiceable()) {
                        String error = "The service is not serviceable at the given location.  Either ensure that the\n";
                        error = error + "address is correctly set or change the system parameter SERVICEABILITY_DISABLED to 1.";
                        throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-036"),"INVALID-ATTRIB-036");
            		}
            		
            	}
	            	
	        }
	        

        }
        
        if (!(lwo instanceof C30SDKExceptionMessage)) {
        	lwo = this;
        }
        return lwo;
    }

    /**
     * Calls the Kenan FX API to create a service connect view row for connect service orders.
     * @throws C30SDKException 
     */
    protected final C30SDKObject connectFXService() throws C30SDKException {
    	log.debug("Starting C30Service.connectFXService");
    	C30SDKObject lwo = null;
    	
		//we need to get the account internal ID
		//from the account associated to the order.
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30Order.class, C30Order.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //create hashmap shell
        Map key = new HashMap();
        Map serviceData = new HashMap();
        serviceData.put("Key", key);

        if (((Integer) super.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {
        	serviceData.put(ATTRIB_SERVICE_ACTIVE_DT, this.getAttributeValue(ATTRIB_SERVICE_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	serviceData.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	serviceData.put(ATTRIB_PRIVACY_LEVEL, new Integer(0));
        	serviceData.put(ATTRIB_DISPLAY_EXTERNAL_ID_TYPE, this.getDisplayExternalIdType());
        	serviceData.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        	this.setAttributesFromNameValuePairs();
        	
    		//retrieve the address.
            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {

    			if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceServiceOrder.SERV_ORD_TYPE_CONNECT) ||
    					this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceServiceOrder.SERV_ORD_TYPE_SERVICE_MOVE_IN)) {
    				lwo = this.getAddress(C30Address.ADDR_CATEGORY_SERVICE);
    				
    				if (!(lwo instanceof C30SDKExceptionMessage) &&
    						this.isBEndAddressChange()) {
    					lwo = this.getAddress(C30Address.ADDR_CATEGORY_B_END);
    				}
    				
    	    	}
            	
            }
    		
            if (!(lwo instanceof C30SDKExceptionMessage)) {
            
	            this.setFXServiceAttributes(serviceData);
	
	        	validateAttributes();
	        	
		        Map callResponse = this.queryData("Service", "Create", serviceData, getAccountServerId());
		        service = (HashMap) callResponse.get("Service");
		        populateService();
		        
		        lwo = this;
		        
            }
            
            // check whether serviceability is enabled.  If it is then we make a call to check whether
            // the service which has been created is serviceable at that location.  Unfortunately it is
            // not possible to check for serviceability before the service is created.  Makes no sense!!
            if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {

            	//Integer isServiceabilityEnabled = (Integer) LightweightCache.getCache(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE);
            	Integer isServiceabilityEnabled = (Integer)(this.getFactory().c30SdkFactoryCache.get(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE)).getValue();
            	
            	if (isServiceabilityEnabled.equals(SERVICEABILITY_ENABLED)) {
            		
            		if (!this.isServiceServiceable()) {
                        String error = "The service is not serviceable at the given location.  Either ensure that the\n";
                        error = error + "address is correctly set or change the system parameter SERVICEABILITY_DISABLED to 1.";
                        throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-036"),"INVALID-ATTRIB-036");
            		}
            		
            	}
            	
            }
            

	    }
        
        // this is where we check if they are changing an address.  If an address is being changed
        // and they are using serviceability then we need to make sure that the corresponding
        // address association is created for the new address.
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {

        	if (this.isServiceAddressChange()) {       		
    			lwo = this.getAddress(C30Address.ADDR_CATEGORY_SERVICE);	    			
				if (!(lwo instanceof C30SDKExceptionMessage)) {
					this.addC30SDKObject(createAddressAssoc(C30Address.ADDR_CATEGORY_SERVICE));
				}
        	}
			if (!(lwo instanceof C30SDKExceptionMessage)) {
	        	if (this.isBEndAddressChange()) {       		
					lwo = this.getAddress(C30Address.ADDR_CATEGORY_B_END);
					if (!(lwo instanceof C30SDKExceptionMessage)) {
						this.addC30SDKObject(createAddressAssoc(C30Address.ADDR_CATEGORY_B_END));
					}
	        	}
			}
        	
        }
        
        if (!(lwo instanceof C30SDKExceptionMessage)) {
        	lwo = this;
        }
        return lwo;
    }

    /**
     * Currently this method populates one piece of data.  This needs to include all data when
     * it is completed.  No time right now to do all of them
     * @throws C30SDKInvalidAttributeException thrown if an invalid attribute exists.
     */
    private void populateService() throws C30SDKInvalidAttributeException {
    
        for  ( Iterator it=service.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                 Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                 String key = (String)entry.getKey (  ) ; 
                 if (key.equalsIgnoreCase("KEY"))
                 {    
                   HashMap keyValue = (HashMap)entry.getValue (  ) ; 
                   if (keyValue.containsKey("ViewId")){
                       if (isAttribute("ViewId", C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                           setAttributeValue("ViewId", ((HashMap)service.get("Key")).get("ViewId"), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                       if (isAttribute("ViewId", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                           setAttributeValue("ViewId", ((HashMap)service.get("Key")).get("ViewId"), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
                   }
                 }
                 else{
                    Object value = entry.getValue (  ) ;
                    if (value != null)
                    if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                        setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                    if (key.equalsIgnoreCase("PARENTACCOUNTINTERNALID"))  
                    {   
                        if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                            setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);  
                            
                    }
                    if (value != null)
                        log.debug(  ( String ) key+" " +  value ) ; 
                } 
        }
       
        if (isAttribute(ATTRIB_MEMBER_INST_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttribute(ATTRIB_SERVICE_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)){
            if(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0)
                setAttributeValue(ATTRIB_MEMBER_INST_ID, getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            else   
                setAttributeValue(ATTRIB_MEMBER_INST_ID, new BigInteger(((Integer)getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)).toString()), C30SDKValueConstants.ATTRIB_TYPE_INPUT);           
        }       
        if (isAttribute(ATTRIB_MEMBER_INST_ID2,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttribute(ATTRIB_SERVICE_INTERNAL_ID_RESETS,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))   
           setAttributeValue(ATTRIB_MEMBER_INST_ID2, getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
    }

    /**
     * Method to set the service attribute information.
     * @param serviceData the service map holding the service data which will be passed into the request.
     * @throws C30SDKInvalidAttributeException if an invalid attribute exists.
     */
    private void setFXServiceAttributes(final Map serviceData) throws C30SDKInvalidAttributeException {
        //----------------------------------------------------------------------
        // Set the following key service fields:
        // View Status: Pending
        // Intended View Effective Date: Same as Service Order Desired Date
        // View Create Date: Now
        // Privacy Level: 0
        // Rev Rcv Cost Center
        //----------------------------------------------------------------------
        serviceData.put(ATTRIB_VIEW_STATUS, FX_PENDING_VIEW_STATUS);

        serviceData.put(ATTRIB_VIEW_CREATED_DT, new Date());
        serviceData.put(ATTRIB_CHG_DT, new Date());
        serviceData.put(ATTRIB_EMF_CONFIG_ID, super.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        //----------------------------------------------------------------------
        // Set the FX fields based on LWO attributes that have been configured
        // or explicitly set
        //----------------------------------------------------------------------
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if (attributeName.equals(ATTRIB_ADDRESS1) && attributeValue != null) {
                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_ADDRESS2) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_ADDRESS3) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_ADDRESS4) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_CITY) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_DISPLAY_ADDRESS_ID) && attributeValue != null) {
                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_COUNTRY_CODE) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_EXTERNAL_ADDRESS_ID) && attributeValue != null) {
                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_FX_GEOCODE) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_STATE) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_POSTAL_CODE) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_FRANCHISE_TAX_CODE) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_HOUSE_NUMBER) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_HOUSE_NUMBER_SUFFIX) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_ADDRESS_LOCATION) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_ADDRESS_TYPE_ID) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_EXTENDED_POSTAL_CODE) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_NEAREST_CROSSSTREET) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_POSTFIX_DIRECTIONAL) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_PREFIX_DIRECTIONAL) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_STREET_NAME) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_STREET_SUFFIX) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_UNIT_NO) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_UNIT_TYPE) && attributeValue != null) {
	                serviceData.put(attributeName, attributeValue);
	        } else if (attributeName.equals(ATTRIB_VERTEX_GEOCODE) && attributeValue != null) {
                serviceData.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_SERVICE_ADDRESS1) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_ADDRESS2) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_ADDRESS3) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_CITY) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_COMPANY) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_COUNTRY_CODE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_COUNTY) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_FNAME) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_FRANCHISE_TAX_CODE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_LNAME) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_MINIT) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_PHONE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_PHONE2) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_STATE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_ZIP) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_B_SERVICE_ADDRESS1) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_ADDRESS2) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_ADDRESS3) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_CITY) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_COMPANY) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_COUNTRY_CODE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_COUNTY) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_FNAME) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_FRANCHISE_TAX_CODE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_LNAME) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_MINIT) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_PHONE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_PHONE2) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_STATE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_B_SERVICE_ZIP) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_NAME_GENERATION) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SERVICE_NAME_PRE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_CODEWORD) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_CONVERTED) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_CURRENCY_CODE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_ELIG_KEY1) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_ELIG_KEY2) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_ELIG_KEY3) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_EXRATE_CLASS) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_IS_PREPAID) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_IXC_PROVIDER_ID) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_LEC_PROVIDER_ID) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_NO_BILL) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_NONPUB_NONLIST) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_PRIVACY_LEVEL) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_RATE_CLASS) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_RESTRICTED_PIC) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_REV_RCV_COST_CTR) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SALES_CHANNEL_ID) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SIM_SERIAL_NUMBER) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_SWITCH_ID) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else if (attributeName.equals(ATTRIB_TIMEZONE) && attributeValue != null) {
                    serviceData.put(attributeName, attributeValue);
            } else {
                //------------------------------------------------------
                // check if the attribute is an extended data parameter
                //------------------------------------------------------
                try {
                    if (attributeValue != null) {
                    
                    	//check if the attribute is extended data
                        Integer paramId = new Integer(attributeName);

                        //if we do not already have extended data attributes then create an array
                        if (serviceData.get(ATTRIB_EXTENDED_DATA) == null) {
                            serviceData.put(ATTRIB_EXTENDED_DATA, new Object[1]);

	                        //create the extended data attribute
	                        HashMap extData = new HashMap();
	                        extData.put(ATTRIB_PARAM_ID, paramId);
	                        extData.put(ATTRIB_PARAM_VALUE, attributeValue);

	                        //add it to the array
	                        Object[] newDataParams = C30HashMapUtils.addObjectToArray( (Object[]) serviceData.get(ATTRIB_EXTENDED_DATA), extData);

	                        //put the array into the service data.
	                        serviceData.put(ATTRIB_EXTENDED_DATA, newDataParams);

                        } else {
                            C30HashMapUtils.removeDuplicatesFromArray(serviceData, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

                            Object [] extendedData = (Object[]) serviceData.get(ATTRIB_EXTENDED_DATA);
                            HashMap xdHashMap = new HashMap(((HashMap) extendedData[0]));

                            // set the extended data attributes
                            xdHashMap.put(ATTRIB_PARAM_ID, paramId);
                            xdHashMap.put(ATTRIB_PARAM_VALUE, attributeValue);

                            // add it uniquely to serviceData
                            Object [] newDataParams = C30HashMapUtils.addUniqueHashMapToArray(extendedData, xdHashMap, ATTRIB_PARAM_ID);
                            serviceData.put(ATTRIB_EXTENDED_DATA, newDataParams);
                        }


                    }
                } catch (NumberFormatException e) {
                    // not a valid param id
                }
            }
            
        }
    }
    
   /**
     * method used to determine if an attribute name is an extended data element
     * @param input
     * @return
     */
   private boolean isInteger( String input )  {   
     try{   
        Integer.parseInt( input );   
         return true;  
         }   
     catch( NumberFormatException e ) {   
     return false;  }    
     } 

    
    /**
     * This method is used to create any serviceAddressAssoc objects which might be required for
     * the service.  This method should be called in all cases where serviceability is required
     * and we are changing the service or b-end addresses.
     * @return 
     * @throws C30SDKObjectConnectionException 
     * @throws C30SDKObjectException 
     * @throws C30SDKInvalidAttributeException 
     * @throws C30SDKInvalidConfigurationException 
     * @throws C30SDKInvalidConfigurationException 
     */
    private C30SDKObject createAddressAssoc(Integer category) throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException {
    	
    	C30SDKObject assoc = this.getFactory().createC30SDKObject(C30ServiceAddressAssoc.class, new Object[0]);
    	
    	//set the parent of the association to be the service.    	
    	assoc.setAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, this, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_SERVICE_INTERNAL_ID, this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_ACTIVE_DT, this.getAttributeValue(ATTRIB_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_EMF_CONFIG_ID, this.getAttributeValue(ATTRIB_EMF_CONFIG_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_ASSOCIATION_STATUS, C30ServiceAddressAssoc.ASSOC_STATUS_PENDING, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_ADDRESS_CATEGORY_ID, category, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	
    	if (category.equals(C30Address.ADDR_CATEGORY_SERVICE)) {
    		assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_ADDRESS_ID, this.getAttributeValue(ATTRIB_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	} else if (category.equals(C30Address.ADDR_CATEGORY_B_END)) {
    		assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_ADDRESS_ID, this.getAttributeValue(ATTRIB_B_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	}
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_MEMBER_INST_ID, this.getAttributeValue(ATTRIB_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_ITEM_ACTION_ID, C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	assoc.setAttributeValue(C30ServiceAddressAssoc.ATTRIB_MEMBER_ID, new Integer(1), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	
    	//this.addLightweightObject(assoc);
    	
    	return assoc;
    	
    }
    
    /**
     * Method to get an address which is to be associated with the service.
     * @return
     * @throws C30SDKException 
     */
    private C30SDKObject getAddress(Integer category) throws C30SDKException {
    	
    	C30SDKObject address = this.getFactory().createC30SDKObject(C30Address.class, new Object[0]);
    	
    	if ((this.getAttributeValue(ATTRIB_DISPLAY_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null && category.equals(C30Address.ADDR_CATEGORY_SERVICE))) {
    		
	    	//set the parent of the association to be the service.    	
	    	address.setAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, this, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	
	    	//set all address attributes of the service object into the address object
	    	if (category.equals(C30Address.ADDR_CATEGORY_SERVICE)) {
		    	for (int i = 0; i < SERVICE_ADDRESS_ATTRIBS.length; i++) {
		    		if (this.getAttributeValue(SERVICE_ADDRESS_ATTRIBS[i], C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
		    	        address.setAttributeValue(SERVICE_ADDRESS_ATTRIBS[i], this.getAttributeValue(SERVICE_ADDRESS_ATTRIBS[i], C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		    		}
		    	}
	    	} else if (category.equals(C30Address.ADDR_CATEGORY_B_END)) {
		    	for (int i = 0; i < B_END_ADDRESS_ATTRIBS.length; i++) {
		    		if (this.getAttributeValue(B_END_ADDRESS_ATTRIBS[i], C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
		    	        address.setAttributeValue(B_END_ADDRESS_ATTRIBS[i].substring(1), this.getAttributeValue(B_END_ADDRESS_ATTRIBS[i], C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		    		}
		    	}
	    	}
	        
	    	if (this.getAttributeValue(ATTRIB_ADDRESS_LOCATION, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	        	address.setAttributeValue(ATTRIB_ADDRESS_LOCATION, this.getAttributeValue(ATTRIB_ADDRESS_LOCATION, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    	} else {
	        	address.setAttributeValue(ATTRIB_ADDRESS_LOCATION, C30Address.ADDR_LOCATION_MASTER, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    	}
	    	if (this.getAttributeValue(ATTRIB_ADDRESS_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	        	address.setAttributeValue(ATTRIB_ADDRESS_TYPE_ID, this.getAttributeValue(ATTRIB_ADDRESS_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    	} else {
	            address.setAttributeValue(C30Address.ATTRIB_ADDRESS_TYPE_ID, C30Address.ADDR_TYPE_NON_FREE_FORM, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    	}
	    	
	        address.setAttributeValue(C30Address.ATTRIB_IS_ACTIVE, new Integer(1), C30SDKValueConstants.ATTRIB_TYPE_INPUT);  	
	    	
	    	address = address.process();
	    	
	    	if (!(address instanceof C30SDKExceptionMessage)) {
	    		populateServiceAddress(address, category);
	    	}
	    	
    	} else {
    		if (category.equals(C30Address.ADDR_CATEGORY_SERVICE)) {
    			this.setAttributeValue(ATTRIB_ADDRESS_ID, this.getAttributeValue(ATTRIB_DISPLAY_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    		} else if (category.equals(C30Address.ADDR_CATEGORY_B_END)) {
    			this.setAttributeValue(ATTRIB_B_ADDRESS_ID, this.getAttributeValue(ATTRIB_DISPLAY_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    		} 
    	}
    	
        return address;
    }
    
    /**
     * Method to populate the service with the returned address information.
     * @param address the returned address information
     * @throws C30SDKInvalidAttributeException
     */
    private void populateServiceAddress(C30SDKObject address, Integer category) throws C30SDKInvalidAttributeException {
    	
    	if (category.equals(C30Address.ADDR_CATEGORY_SERVICE)) {
	        this.setAttributeValue(ATTRIB_ADDRESS_ID, address.getAttributeValue(ATTRIB_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_ADDRESS1, address.getAttributeValue(ATTRIB_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_ADDRESS2, address.getAttributeValue(ATTRIB_ADDRESS2, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_ADDRESS3, address.getAttributeValue(ATTRIB_ADDRESS3, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_ADDRESS4, address.getAttributeValue(ATTRIB_ADDRESS4, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_ADDRESS_LOCATION, address.getAttributeValue(ATTRIB_ADDRESS_LOCATION, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_ADDRESS_TYPE_ID, address.getAttributeValue(ATTRIB_ADDRESS_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_CITY, address.getAttributeValue(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_COUNTRY_CODE, address.getAttributeValue(ATTRIB_COUNTRY_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_COUNTY, address.getAttributeValue(ATTRIB_COUNTY, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_EXTENDED_POSTAL_CODE, address.getAttributeValue(ATTRIB_EXTENDED_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_EXTERNAL_ADDRESS_ID, address.getAttributeValue(ATTRIB_EXTERNAL_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_FRANCHISE_TAX_CODE, address.getAttributeValue(ATTRIB_FRANCHISE_TAX_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_FX_GEOCODE, address.getAttributeValue(ATTRIB_FX_GEOCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_HOUSE_NUMBER, address.getAttributeValue(ATTRIB_HOUSE_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_HOUSE_NUMBER_SUFFIX, address.getAttributeValue(ATTRIB_HOUSE_NUMBER_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_KEY_LINE_CODE, address.getAttributeValue(ATTRIB_KEY_LINE_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_NEAREST_CROSSSTREET, address.getAttributeValue(ATTRIB_NEAREST_CROSSSTREET, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_OPTIONAL_ENDORSEMENT_LINE, address.getAttributeValue(ATTRIB_OPTIONAL_ENDORSEMENT_LINE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_POSTAL_CODE, address.getAttributeValue(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_POSTFIX_DIRECTIONAL, address.getAttributeValue(ATTRIB_POSTFIX_DIRECTIONAL, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE, address.getAttributeValue(ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_POSTNET_BARCODE, address.getAttributeValue(ATTRIB_POSTNET_BARCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_PREFIX_DIRECTIONAL, address.getAttributeValue(ATTRIB_PREFIX_DIRECTIONAL, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_STATE, address.getAttributeValue(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_STREET_NAME, address.getAttributeValue(ATTRIB_STREET_NAME, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_STREET_SUFFIX, address.getAttributeValue(ATTRIB_STREET_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_UNIT_NO, address.getAttributeValue(ATTRIB_UNIT_NO, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_UNIT_TYPE, address.getAttributeValue(ATTRIB_UNIT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_VERTEX_GEOCODE, address.getAttributeValue(ATTRIB_VERTEX_GEOCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	} else if (category.equals(C30Address.ADDR_CATEGORY_B_END)) {
	        this.setAttributeValue(ATTRIB_B_ADDRESS_ID, address.getAttributeValue(ATTRIB_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_ADDRESS1, address.getAttributeValue(ATTRIB_ADDRESS1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_ADDRESS2, address.getAttributeValue(ATTRIB_ADDRESS2, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_ADDRESS3, address.getAttributeValue(ATTRIB_ADDRESS3, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_ADDRESS4, address.getAttributeValue(ATTRIB_ADDRESS4, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_ADDRESS_LOCATION, address.getAttributeValue(ATTRIB_ADDRESS_LOCATION, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_ADDRESS_TYPE_ID, address.getAttributeValue(ATTRIB_ADDRESS_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_CITY, address.getAttributeValue(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_COUNTRY_CODE, address.getAttributeValue(ATTRIB_COUNTRY_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_COUNTY, address.getAttributeValue(ATTRIB_COUNTY, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_EXTENDED_POSTAL_CODE, address.getAttributeValue(ATTRIB_EXTENDED_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_EXTERNAL_ADDRESS_ID, address.getAttributeValue(ATTRIB_EXTERNAL_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_FRANCHISE_TAX_CODE, address.getAttributeValue(ATTRIB_FRANCHISE_TAX_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_FX_GEOCODE, address.getAttributeValue(ATTRIB_FX_GEOCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_HOUSE_NUMBER, address.getAttributeValue(ATTRIB_HOUSE_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_HOUSE_NUMBER_SUFFIX, address.getAttributeValue(ATTRIB_HOUSE_NUMBER_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_KEY_LINE_CODE, address.getAttributeValue(ATTRIB_KEY_LINE_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_NEAREST_CROSSSTREET, address.getAttributeValue(ATTRIB_NEAREST_CROSSSTREET, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_OPTIONAL_ENDORSEMENT_LINE, address.getAttributeValue(ATTRIB_OPTIONAL_ENDORSEMENT_LINE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_POSTAL_CODE, address.getAttributeValue(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_POSTFIX_DIRECTIONAL, address.getAttributeValue(ATTRIB_POSTFIX_DIRECTIONAL, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_POSTNET_ADDRESS_BLOCK_BARCODE, address.getAttributeValue(ATTRIB_POSTNET_ADDRESS_BLOCK_BARCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_POSTNET_BARCODE, address.getAttributeValue(ATTRIB_POSTNET_BARCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_PREFIX_DIRECTIONAL, address.getAttributeValue(ATTRIB_PREFIX_DIRECTIONAL, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_STATE, address.getAttributeValue(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_STREET_NAME, address.getAttributeValue(ATTRIB_STREET_NAME, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_STREET_SUFFIX, address.getAttributeValue(ATTRIB_STREET_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_UNIT_NO, address.getAttributeValue(ATTRIB_UNIT_NO, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_UNIT_TYPE, address.getAttributeValue(ATTRIB_UNIT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	        this.setAttributeValue(ATTRIB_B_VERTEX_GEOCODE, address.getAttributeValue(ATTRIB_VERTEX_GEOCODE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	}
    }
    
    /**
     * Method which determines whether an address is being changed.  This method is only used
     * for FX2.0 in conjunction with serviceability so that a proper address move occurs.
     * @return true = yes/false = no
     * @throws C30SDKInvalidAttributeException 
     */
    private boolean isServiceAddressChange() throws C30SDKInvalidAttributeException {
    	boolean isAddressChange = false;
		
    	for (int i = 0; i < SERVICE_ADDRESS_ATTRIBS.length; i++) {
    		if (this.getAttribute(SERVICE_ADDRESS_ATTRIBS[i], C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() != null) {
    			isAddressChange = true;
    			break;
    		}
    	}
		return isAddressChange;
    }

    private boolean isBEndAddressChange() throws C30SDKInvalidAttributeException {
    	boolean isAddressChange = false;
		
    	for (int i = 0; i < B_END_ADDRESS_ATTRIBS.length; i++) {
    		if (this.getAttribute(B_END_ADDRESS_ATTRIBS[i], C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() != null) {
    			isAddressChange = true;
    			break;
    		}
    	}
		return isAddressChange;
    }

    /**
     * Method that checks whether service is available at the requested location.
     * @return true = service is available, false = service is not available.
     * @throws C30SDKInvalidAttributeException 
     * @throws C30SDKObjectConnectionException 
     * @throws C30SDKObjectConnectionException 
     * @throws C30SDKKenanFxCoreException 
     */
    private final boolean isServiceServiceable() throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKKenanFxCoreException {
    	boolean isServiceable = false;
        Map callResponse;
    	
        //create hashmap shell
    	Map serviceabilityDataList = new HashMap();

    	//add the address id to be used in the check
    	if (this.getAttributeValue(ATTRIB_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) != null) {
    		serviceabilityDataList.put(ATTRIB_ADDRESS_ID, this.getAttributeValue(ATTRIB_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
    	} else if (this.getAttributeValue(ATTRIB_B_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) != null) {
    		serviceabilityDataList.put(ATTRIB_ADDRESS_ID, this.getAttributeValue(ATTRIB_B_ADDRESS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
    	}

    	//add the services to be checked against.
        List serviceList = new ArrayList();
    	serviceabilityDataList.put("ServiceList", serviceList);
    	
    	Map service = new HashMap();
    	Map key = new HashMap();
    	service.put("Key", key);
    	
    	key.put(ATTRIB_SERVICE_INTERNAL_ID, this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
    	key.put(ATTRIB_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
    	
        serviceList.add(service);
    	
		callResponse = this.queryData("ServiceAddressServiceServiceabilityValidate", serviceabilityDataList, getAccountServerId());
        
        //get the count
        int count = ((Object[]) callResponse.get("ServiceList")).length;
        if (count == 0) {
        	isServiceable = true;
        }
        
    	return isServiceable;
    }
    
    /**
     * LW Services do not currently support child order items.  This is a placeholder
     * for potential future expansion of the LWO framework.
     * @return any child order items associated to the service.
     * @throws C30SDKException 
     */
    protected final C30SDKObject createChildOrderItems() throws C30SDKException { 
    	C30SDKObject lwo = null;
    	
    	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
    		lwo = this.getC30SDKObject(i);
    		lwo = lwo.process();
    		
    		if (lwo instanceof C30SDKExceptionMessage) {
    			break;
    		}
    		
    	}
    	return lwo; 
    }

    /**
     * Method which gets any data to be cached for further use by the object.
     * @throws C30SDKCacheException if there was an error while trying to populate the service cache.
     */
    protected void loadCache() throws C30SDKCacheException {
    	log.debug("Starting C30Service.loadCache");
    	 
    	// check whether an external ID should be automatically generated.  If it is
    	// the returned value is the external_id_type of the external ID to create.
    	if ((this.getFactory().c30SdkFactoryCache.get(SERVICE_IS_EXT_ID_GENERATED_CACHE)).getValue()==null) {

	    	try {
	            //C30ExternalCallDef call = new C30ExternalCallDef("ps_get_is_ext_id_generated");
				C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_is_ext_id_gen");

                //------------------------------------------
                // set up the stored procedure to be called
                //------------------------------------------
                call.addParam(new C30CustomParamDef("type", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
                call.addParam(new C30CustomParamDef("external_id_type", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
                //call.addParam(new C30CustomParamDef("ps_get_is_ext_id_generated_cv", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
				//call.addParam(new C30CustomParamDef("c30_sdk_get_is_ext_id_generated_cv", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));

	            //--------------------------------
	            // set the input parameter values
	            //--------------------------------
	            ArrayList paramValues = new ArrayList();
	            paramValues.add(new Integer(1));

	            //---------------------------------------
	            // make the call and process the results
	            //---------------------------------------
	            //C30KenanMWDataSource dataSource = this.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES, C30SDKObject.ADMIN_SERVER_ID);
                    /*C30KenanMWDataSource dataSource = this.getFactory().getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
                    dataSource.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES);

	            if (dataSource.getValueAt(0,0) != null) {
	            	//LightweightCache.setCache(SERVICE_IS_EXT_ID_GENERATED_CACHE, new Integer(dataSource.getValueAt(0,0).toString()));
	            	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(SERVICE_IS_EXT_ID_GENERATED_CACHE, new Integer(dataSource.getValueAt(0,0).toString()));
					this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
					
	            } else {
	            	//LightweightCache.setCache(SERVICE_IS_EXT_ID_GENERATED_CACHE, new Integer(-1));
	            	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(SERVICE_IS_EXT_ID_GENERATED_CACHE, new Integer(-1));
					this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
					
	            }
*/
	        } catch (Exception e) {
	                log.error(e);
	    		String error = "An error occurred while trying to load the service cache (SERVICE_IS_EXT_ID_GENERATED_CACHE).";
	        	throw new C30SDKCacheException(exceptionResourceBundle.getString("LW-CACHE-003"),"LW-CACHE-003");
	        }

    	}
    	
    	// check whether serviceability is turned on.
        log.debug("checking if serviceability is on ...");
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        
        	if ((this.getFactory().c30SdkFactoryCache.get(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE)).getValue()==null) {
        	
	            //create hashmap shell
	            Map key = new HashMap();
	            Map systemParameterFilter = new HashMap();
	            systemParameterFilter.put("Key", key);
	
	        	systemParameterFilter.put("Fetch", new Boolean(true));
	            
	        	HashMap attrFilter = new HashMap();
	        	attrFilter.put("Equal", SERVICEABILITY_MODULE);
	        	key.put(ATTRIB_MODULE, attrFilter);
	
	        	attrFilter = new HashMap();
	        	attrFilter.put("Equal", SERVICEABILITY_DISABLED_STR);
	        	key.put(ATTRIB_PARAMETER_NAME, attrFilter);
	
	        	try {
	        		Map callResponse = this.queryData("SystemParameter", "Find", systemParameterFilter, getAccountServerId());
	                
	        		//get the count
	                int count = ( (Integer) callResponse.get("Count")).intValue();
	
	                if (count == 0) {
		            	//LightweightCache.setCache(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE, SERVICEABILITY_DISABLED);
		            	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE, SERVICEABILITY_DISABLED);
						this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
	                } else {
	                    Map sysParam = (HashMap) ((Object[]) callResponse.get("SystemParameterList"))[0];
		            	//LightweightCache.setCache(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE, new Integer(sysParam.get(ATTRIB_INT_VALUE).toString()));
		            	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(SERVICE_IS_SERVICEABILITY_ENABLED_CACHE, new Integer(sysParam.get(ATTRIB_INT_VALUE).toString()));
						this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
	                }
	        	} catch (Exception e) {
		    		String error = "An error occurred while trying to load the service cache (SERVICE_IS_SERVICEABILITY_ENABLED_CACHE).";
		        	throw new C30SDKCacheException(exceptionResourceBundle.getString("LW-CACHE-004"),"LW-CACHE-004");
	        	}
        	}
        }    	
    	log.debug("Finished C30Service.loadCache");
    }

}
