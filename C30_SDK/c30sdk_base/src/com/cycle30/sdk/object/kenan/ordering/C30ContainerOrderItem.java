/*
 * C30ContainerOrderItem.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Lightweight Container Order Item object.
 * Does not encapsulate an FX Object, but helps bundle LW Order items into "business bundles"
 * @author Joe Morales
 */
public class C30ContainerOrderItem extends C30OrderItem { 

    
    /**
     * Creates a new instance of C30OrderItem.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
	public C30ContainerOrderItem(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always 
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework 
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
	public C30ContainerOrderItem(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30ContainerOrderItem.class);
    }
    
    /**
     * Crates child order items for the LW Container Order Item.  LW Containers
     * are used only for the purpose of creating child order items.  Any type 
     * of order item can be configured as a child order item for the LW Container.
     * @return C30SDKObject the returned child order items.
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected final C30SDKObject createChildOrderItems() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {
        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {        
            C30OrderItem lwoi = (C30OrderItem) this.getC30SDKObject(i);
            lwoi.setAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwoi.setNameValuePairs(this.getNameValuePairMap());
            lwoi.createOrderItemInDB(); 
        }
        return this;
    }
    
    /**
     * This method allows a user to set the service attributes.  Service attributes
     * should only be set in a change or a connect order. All other order types
     * are logical sub orders and do no support service field initialization.
     * For this reason this method handled connect/change scenarios. 
     */
    @Override
	protected C30SDKObject createFXObject() { return this; } 
    
    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
