/*
 * C30Deposit.java
 *
 * Created on February 5, 2009, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.financial;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.csgsystems.aruba.connection.BSDMSessionContext;
import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * The C30SDK deposit is a pre-configured template for a Kenan/FX deposit.
 * 
 * @author Tom Ansley
 */
public class C30Deposit extends C30SDKObject implements Cloneable {

	private static Logger log = Logger.getLogger(C30Deposit.class);

	/**
	 * Creates a new instance of C30Deposit.
	 * 
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK deposit
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Deposit(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30Deposit.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * 
	 * @param factory Factory used to create C30Deposit objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Deposit(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30Deposit.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX deposit.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */    
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {
	    
		log.debug("C30Deposit: BEGIN initializeAttributes");
		C30SDKAttribute attribute = null;

		//=============================
		// INPUT ATTRIBUTES
		//=============================
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CHG_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    	
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE, Integer.class);
		attribute.setRequired(true);
		attribute.setValue(1);
                attribute.setExternalName(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);    	

		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DATE_RETURNED, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    	

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT, BigInteger.class);
		attribute.setRequired(true);
		attribute.setExternalName(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DEPOSIT_TYPE, Integer.class);
		attribute.setRequired(true);
		attribute.setExternalName(C30SDKAttributeConstants.ATTRIB_DEPOSIT_TYPE);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
	        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CHG_WHO, String.class);
	        attribute.setRequired(true);
	        attribute.setExternalName(C30SDKAttributeConstants.ATTRIB_CHG_WHO);
	        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_INCOME_TAX_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_INTEREST_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_DATE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    	
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PAY_METHOD, Integer.class);
		attribute.setRequired(true);
	        attribute.setExternalName(C30SDKAttributeConstants.ATTRIB_PAY_METHOD);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REASON_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REFUND_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVER_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		//=============================
		// OUTPUT ATTRIBUTES
		//=============================        
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CHG_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);    	
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);    	
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DATE_RETURNED, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);    	
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DEPOSIT_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_INCOME_TAX_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_INTEREST_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_CODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_DATE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);    	
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PAY_METHOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REASON_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REFUND_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		if ( this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {    	
		}
		if ( this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
		}
		log.debug("C30Deposit: END initializeAttributes");
	}
	
	

	/**
	 * Creates a copy of the C30Deposit object.
	 * 
	 * @return A copy of the C30Deposit object
	 */
	@Override
	public Object clone() {
		C30Deposit copy = (C30Deposit) super.clone();        
		return copy;        
	}
	

	/**
	 * Method to process the deposit.
	 * 
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() {
	    
		C30SDKObject sdkObject = null;

		try {

                    super.setAttributesFromNameValuePairs();
	                
		    //get the associated account
		    if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) == null) {
			log.debug("Found an associated account!");
			sdkObject = getAccount();
		    }

		    //if we got an account rather than an exception
		    if (sdkObject instanceof C30Account) {
			log.debug("Found a valid account!  Now create deposit!");
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, sdkObject, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			createDeposit();
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			sdkObject = this;
		    }

		} catch (C30SDKException e) {
		    sdkObject = this.createExceptionMessage(e);
		}

		return sdkObject;
	}

	
	
	/**
	 * Method to get the account associated with this account note.
	 * 
	 * @return a C30Account object, or a C30SDKExceptionMessage if an error occurred.
	 * @throws C30SDKException 
	 */
	private C30SDKObject getAccount() throws C30SDKException {
	    
		log.debug("Starting C30Deposit.getAccount");

		C30SDKObject account = null;

		//try get account from object children
		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
		    if (this.getC30SDKObject(i) instanceof C30Account) {
			account = this.getC30SDKObject(i);
			log.debug("Found a valid account!");
		    }
		}

		//if no account in children then get account from middleware
		if (account == null) {
			account = this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);
			account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			log.debug("Getting the account ...");
			account = account.process();
		}

		//if the processed account did not produce an exception.
		if (!(account instanceof C30SDKExceptionMessage)) {
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			// set customer server id cache
			// C30SDKCache.setCache(C30SDKObject.ACCOUNT_SERVER_ID_CACHE, account.getAttributeValue(ATTRIB_SERVER_ID));
		}
		log.debug("Finished C30Deposit.getAccount");
		return account;
	}
	
	

	/**
	 * Creates the Kenan/FX deposit using API-TS calls.
	 * 
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void createDeposit() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

		//---------------------------------
		// set and validate all attributes
		//---------------------------------
		setAttributesFromNameValuePairs();
		validateAttributes();
		Map deposit = setDepositAttributes();

		//----------------------------------------
		// Create the deposit request and set the
		// deposit and the effective date into it
		//----------------------------------------
		Map depositRequest = new HashMap();
		depositRequest.put("Deposit", deposit);
		
		//----------------------------------------
		// Temporarily set context operator name
		// to the 'ChgWho' so db will be updated
		// with actual username.
		//----------------------------------------
	        BSDMSessionContext context =  this.getFactory().getContext();
	        String operator = context.getOperatorName();
	        String userId = (String)this.getNameValuePairs().get("ChgWho");
	        context.setOperatorName(userId);

		//-----------------------------------------------
		// create the deposit and then get the response.
		//-----------------------------------------------
		Map callResponse = null;
		callResponse = this.queryData("DepositCreate", depositRequest, getAccountServerId());            

		deposit = (HashMap) callResponse.get("Deposit");

		populateDepositOutput(deposit);
		
		// Restore context operator name
		context.setOperatorName(operator);

	}

	
	/**
	 * Method to ensure all attributes that are visible to the calling application are populated.  This 
	 * method is usually called just before the object returns objects using the process() method.
	 * 
	 * @param deposit the deposit information returned from middleware that should be used to populate the C30SDK object.
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration while populating the object.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while populating the object.
	 * @throws C30SDKObjectException if there was a problem initializing the object while populating the object.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes while populating the object.
	 */
	private void populateDepositOutput(final Map deposit) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {

		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, deposit.get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CHG_DATE, deposit.get(C30SDKAttributeConstants.ATTRIB_CHG_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CHG_WHO, deposit.get(C30SDKAttributeConstants.ATTRIB_CHG_WHO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE, deposit.get(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED, deposit.get(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DATE_RETURNED, deposit.get(C30SDKAttributeConstants.ATTRIB_DATE_RETURNED), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT, deposit.get(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DEPOSIT_TYPE, deposit.get(C30SDKAttributeConstants.ATTRIB_DEPOSIT_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_INCOME_TAX_AMOUNT, deposit.get(C30SDKAttributeConstants.ATTRIB_INCOME_TAX_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_INTEREST_AMOUNT, deposit.get(C30SDKAttributeConstants.ATTRIB_INTEREST_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_CODE, deposit.get(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_DATE, deposit.get(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID, deposit.get(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PAY_METHOD, deposit.get(C30SDKAttributeConstants.ATTRIB_PAY_METHOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_REASON_CODE, deposit.get(C30SDKAttributeConstants.ATTRIB_REASON_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_REFUND_TYPE, deposit.get(C30SDKAttributeConstants.ATTRIB_REFUND_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRACKING_ID, ((HashMap) deposit.get("Key")).get(C30SDKAttributeConstants.ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV, ((HashMap) deposit.get("Key")).get(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

	}

	
	
	/**
	 * Validates that all the required attributes have been set.
	 * 
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException { 

		super.validateAttributes();

		//check to ensure either the deposit amount is greater than zero.
		if (((BigInteger) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(BigInteger.ZERO)) {

		    throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-019"),"INVALID-ATTRIB-019");
		}

		//check to ensure either the account internal id is set or the external id and type.
		if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null
			&& this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null) {
		    throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-020"),"INVALID-ATTRIB-020");
		}

	}
	
	

	/**
	 * Initilize attributes for the Kenan FX deposit that is about to be created through the API TS.
	 * 
	 * @return Map the deposit hashmap that will be used when making the call to API-TS.
	 * @throws C30SDKInvalidAttributeException exception thrown if there was an error setting any attributes.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidConfigurationException 
	 */
	private Map setDepositAttributes() throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException {
		Map deposit = new HashMap();
		//-----------------------------------------------
		// set the deposit attributes
		//-----------------------------------------------
		Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
		while (iterator.hasNext()) {
			String attributeName = (String) iterator.next();
			Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

			if (attributeValue != null) {
				if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_CHG_DATE)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_CHG_DATE, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_CHG_WHO)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_CHG_WHO, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_DATE_RETURNED)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_DATE_RETURNED, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_DEPOSIT_AMOUNT, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_DEPOSIT_TYPE)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_DEPOSIT_TYPE, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_INCOME_TAX_AMOUNT)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_INCOME_TAX_AMOUNT, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_INTEREST_AMOUNT)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_INTEREST_AMOUNT, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_CODE)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_CODE, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_DATE)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_MANUAL_CCAUTH_DATE, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_PAY_METHOD)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_PAY_METHOD, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_REASON_CODE)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_REASON_CODE, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_REFUND_TYPE)) {
					deposit.put(C30SDKAttributeConstants.ATTRIB_REFUND_TYPE, attributeValue);
				}
			}
		}

		deposit.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, ((C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)).getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID));

		//set the date of the transaction if not set manually
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CHG_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
			deposit.put(C30SDKAttributeConstants.ATTRIB_CHG_DATE, new Date());
		}
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
			deposit.put(C30SDKAttributeConstants.ATTRIB_DATE_RECEIVED, new Date());
		}

		return deposit;
	}
	
	

	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}

}
