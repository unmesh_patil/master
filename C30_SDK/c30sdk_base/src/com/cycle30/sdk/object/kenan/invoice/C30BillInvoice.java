/*
 * C30BillInvoice.java
 *
 * Created on March 10, 2009, 11:39 PM
 * Created by Tom Ansley
 */

package com.cycle30.sdk.object.kenan.invoice;




import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.cache.C30SDKCacheableWrapper;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30KenanMWDataSource;
import com.cycle30.sdk.datasource.C30TableDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30StringUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX bill invoice record.
 * @author re
 */
public class C30BillInvoice extends C30SDKObject {

    private static Logger log = Logger.getLogger(C30BillInvoice.class);
    public static final String ATTRIB_FIND = "Find";
    public static final String ATTRIB_ACCOUNT = "Account";
    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID = "AccountExternalId";
    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE = "AccountExternalIdType";
    public static final String ATTRIB_ACCOUNT_INTERNAL_ID = "AccountInternalId";
    public static final String ATTRIB_ACCOUNT_SERVER_ID = "AccountServerId";
    public static final String ATTRIB_ITEM_ACTION_ID = "ItemActionId";
    
    public static final String ATTRIB_ACCOUNT_STATUS = "AccountStatus";
    public static final String ATTRIB_ARCH_FLAG = "ArchFlag";
    public static final String ATTRIB_BACKOUT_STATUS = "BackoutStatus";
    public static final String ATTRIB_BILL_DISP_METH = "BillDispMethod";
    public static final String ATTRIB_BILL_HOLD_CODE = "BillHoldCode";
    public static final String ATTRIB_BILL_INVOICE_DETAIL_AMOUNT = "BillInvoiceDetailAmount";
    public static final String ATTRIB_BILL_INVOICE_DETAIL_TYPE = "BillInvoiceDetailType";
    public static final String ATTRIB_BILL_INVOICE_DETAIL_DESCRIPTION_CODE = "BillInvoiceDetailDescriptionCode";
    public static final String ATTRIB_BILL_ORDER_NUMBER = "BillOrderNumber";
    public static final String ATTRIB_BILL_PERIOD = "BillPeriod";
    public static final String ATTRIB_BILL_REF_NO = "BillRefNo";
    public static final String ATTRIB_BILL_REF_RESETS = "BillRefResets";
    public static final String ATTRIB_BILL_SEQUENCE_NUM = "BillSequenceNum";
    public static final String ATTRIB_COLLECTION_HISTORY = "CollectionHistory";
    public static final String ATTRIB_CONVERTED = "Converted";
    public static final String ATTRIB_COPY_TYPE = "CopyType";
    public static final String ATTRIB_CURRENCY_CODE = "CurrencyCode";
    public static final String ATTRIB_DISPATCH_COUNT = "DispatchCount";
    public static final String ATTRIB_DISPATCH_DATE = "DispatchDate";
    public static final String ATTRIB_END_OFFSET = "EndOffset";
    public static final String ATTRIB_FILE_NAME = "FileName";
    public static final String ATTRIB_FORMAT_ERROR_CODE = "FormatErrorCode";
    public static final String ATTRIB_FORMAT_STATUS = "FormatStatus";
    public static final String ATTRIB_FROM_DATE = "FromDate";
    public static final String ATTRIB_IMAGE_DONE = "ImageDone";
    public static final String ATTRIB_IMAGE_REQ = "ImageReq";
    public static final String ATTRIB_INCLUDE_ADJ = "IncludeAdj";
    public static final String ATTRIB_INCLUDE_BMF = "IncludeBmf";
    public static final String ATTRIB_INCLUDE_NRC = "IncludeNrc";
    public static final String ATTRIB_INCLUDE_RC = "IncludeRc";
    public static final String ATTRIB_INCLUDE_USAGE = "IncludeUsage";
    public static final String ATTRIB_INTERIM_BILL_FLAG = "InterimBillFlag";
   
    public static final String ATTRIB_JNL_EARNED_THRU_DT = "JnlEarnedThruDt";
    public static final String ATTRIB_JNL_STATUS = "JnlStatus";
    public static final String ATTRIB_LANGUAGE_CODE = "LanguageCode";
    public static final String ATTRIB_NEXT_TO_DATE = "NextToDate";
    public static final String ATTRIB_ORIG_BILL_REFNO = "OrigBillRefno";
    public static final String ATTRIB_ORIG_BILL_REF_RESETS = "OrigBillRefResets";
    public static final String ATTRIB_PAGE_COUNT = "PageCount";
    public static final String ATTRIB_PAY_METHOD = "PayMethod";
    public static final String ATTRIB_PAYMENT_DUE_DATE = "PaymentDueDate";
    public static final String ATTRIB_PREP_DATE = "PrepDate";
    public static final String ATTRIB_PREP_ERROR_CODE = "PrepErrorCode";
    public static final String ATTRIB_PREP_STATUS = "PrepStatus";
    public static final String ATTRIB_PREP_TASK = "PrepTask";
    public static final String ATTRIB_PREV_BALANCE_REFNO = "PrevBalanceRefno";
    public static final String ATTRIB_PREV_BALANCE_REF_RESETS = "PrevBalanceRefResets";
    public static final String ATTRIB_PREV_BILL_REFNO = "PrevBillRefno";
    public static final String ATTRIB_PREV_BILL_REF_RESETS = "PrevBillRefResets";
    public static final String ATTRIB_PREV_CUTOFF_DATE = "PrevCutoffDate";
    public static final String ATTRIB_PREV_PPDD = "PrevPpdd";
    public static final String ATTRIB_PROCESS_NUM = "ProcessNum";
    public static final String ATTRIB_SPECIAL_CODE = "SpecialCode";
    public static final String ATTRIB_START_OFFSET = "StartOffset";
    public static final String ATTRIB_STATEMENT_DATE = "StatementDate";
    public static final String ATTRIB_TAX_DATE = "TaxDate";
    public static final String ATTRIB_TAX_JOURNAL_STATUS = "TaxJournalStatus";
    public static final String ATTRIB_TEST_FLAG = "TestFlag";
    public static final String ATTRIB_TO_DATE = "ToDate";
    public static final String ATTRIB_WARM_BILL_FLAG = "WarmBillFlag";
    public static final String ATTRIB_ZIP = "Zip";
    
    public static final Integer ITEM_ACTION_ID_CREATE = new Integer(1);
    public static final Integer ITEM_ACTION_ID_FIND = new Integer(2);
    
    public static final Integer BILL_INVOICE_DETAIL_TYPE_SUMMARY = new Integer(1);

    private static final String MAX_BILL_REF_NO = "MAX_REFNO";
    private static final String MIN_BILL_REF_NO = "MIN_REFNO";

    private static final String MAX_BILL_REF_NO_CACHE = "MAX_BILL_REF_NO_CACHE";
    private static final String MIN_BILL_REF_NO_CACHE = "MIN_BILL_REF_NO_CACHE";
    
    private Map billInvoice = null;
    
    public static final String BILL_REFNO_INVOICE_CALL = "ps_get_bill_invoice";
    public static final String ACCOUNT_INVOICE_CALL = "ps_get_acct_invoice";

    
    /**
     * Creates a new instance of C30OrderItem.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30BillInvoice(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always 
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework 
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30BillInvoice(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30BillInvoice.class);
    }
    
    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the component.
     */
    protected final void initializeAttributes() throws C30SDKInvalidAttributeException {
        super.setClassFieldConfiguration();
    
    }
        
    /**
     * Method to populate this object with the returned results from the stored procedure call.
     * @param dataSource the returned results from the stored procedure call.
     * @throws C30SDKException 
     */
    private void populateNewBillInvoice(final C30KenanMWDataSource dataSource) throws C30SDKException {
    	
    	// if there is a row to populate from.
    	if (dataSource.getRowCount() > 0) {
    	   
    	    this.setAttributeValue(ATTRIB_BILL_INVOICE_DETAIL_TYPE, this.getAttributeValue(ATTRIB_BILL_INVOICE_DETAIL_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            
    		this.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, dataSource.getValueAt(0,0), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_BILL_REF_NO, dataSource.getValueAt(0,1), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_BILL_REF_RESETS, dataSource.getValueAt(0,2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_BILL_SEQUENCE_NUM, dataSource.getValueAt(0,3), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PROCESS_NUM, dataSource.getValueAt(0,4), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREV_BILL_REFNO, dataSource.getValueAt(0,5), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREV_BILL_REF_RESETS, dataSource.getValueAt(0,6), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREV_BALANCE_REFNO, dataSource.getValueAt(0,7), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREV_BALANCE_REF_RESETS, dataSource.getValueAt(0,8), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_ZIP, dataSource.getValueAt(0,9), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_FROM_DATE, dataSource.getValueAt(0,10), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_TO_DATE, dataSource.getValueAt(0,11), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_NEXT_TO_DATE, dataSource.getValueAt(0,12), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREP_DATE, dataSource.getValueAt(0,13), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_TAX_DATE, dataSource.getValueAt(0,14), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_STATEMENT_DATE, dataSource.getValueAt(0,15), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PAYMENT_DUE_DATE, dataSource.getValueAt(0,16), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREV_PPDD, dataSource.getValueAt(0,17), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREV_CUTOFF_DATE, dataSource.getValueAt(0,18), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_BILL_PERIOD, dataSource.getValueAt(0,19), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_CURRENCY_CODE, dataSource.getValueAt(0,20), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PAY_METHOD, dataSource.getValueAt(0,21), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_BILL_DISP_METH, dataSource.getValueAt(0,22), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_ACCOUNT_STATUS, dataSource.getValueAt(0,23), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_IMAGE_REQ, dataSource.getValueAt(0,24), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_IMAGE_DONE, dataSource.getValueAt(0,25), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_SPECIAL_CODE, dataSource.getValueAt(0,26), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREP_TASK, dataSource.getValueAt(0,27), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PREP_STATUS, dataSource.getValueAt(0,28), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_FORMAT_STATUS, dataSource.getValueAt(0,29), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_DISPATCH_COUNT, dataSource.getValueAt(0,30), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_PAGE_COUNT, dataSource.getValueAt(0,31), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_BACKOUT_STATUS, dataSource.getValueAt(0,32), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_COLLECTION_HISTORY, dataSource.getValueAt(0,33), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_JNL_EARNED_THRU_DT, dataSource.getValueAt(0,34), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_JNL_STATUS, dataSource.getValueAt(0,35), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_TEST_FLAG, dataSource.getValueAt(0,36), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT); 
            this.setAttributeValue(ATTRIB_ARCH_FLAG, dataSource.getValueAt(0,37), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_INTERIM_BILL_FLAG, dataSource.getValueAt(0,38), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_INCLUDE_NRC, dataSource.getValueAt(0,39), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_INCLUDE_RC, dataSource.getValueAt(0,40), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_INCLUDE_ADJ, dataSource.getValueAt(0,41), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_INCLUDE_USAGE, dataSource.getValueAt(0,42), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_INCLUDE_BMF, dataSource.getValueAt(0,43), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_CONVERTED, dataSource.getValueAt(0,44), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_WARM_BILL_FLAG, dataSource.getValueAt(0,45), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    			       	
    		// if we have a type code for the row then we have detail 
            // records which we need to create and populate. 
            if (dataSource.getValueAt(0,53) != null) {
            
            	C30SDKObject billInvoiceDetail;
            	
		    	for (int i = 0; i < dataSource.getRowCount(); i++) {
		    		billInvoiceDetail = this.getFactory().createC30SDKObject(C30BillInvoiceDetail.class, new Object[0]);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_BILL_INVOICE, this, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_FROM_DATE, dataSource.getValueAt(0,46), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TO_DATE, dataSource.getValueAt(0,47), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TEST_FLAG, dataSource.getValueAt(i,48), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT); 
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_ARCH_FLAG, dataSource.getValueAt(i,49), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_BILL_INVOICE_ROW, dataSource.getValueAt(i,50), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_PACKAGE_ID, dataSource.getValueAt(i,51), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_COMPONENT_ID, dataSource.getValueAt(i,52), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TYPE_CODE, dataSource.getValueAt(i,53), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_SUBTYPE_CODE, dataSource.getValueAt(i,54), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TRACKING_ID, dataSource.getValueAt(i,55), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TRACKING_ID_SERV, dataSource.getValueAt(i,56), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_PREP_SEQUENCE, dataSource.getValueAt(i,57), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_PRORATE_CODE, dataSource.getValueAt(i,58), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_BILLING_LEVEL, dataSource.getValueAt(i,59), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_BILLING_CATEGORY, dataSource.getValueAt(i,60), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_AMOUNT, dataSource.getValueAt(i,61), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_RATED_AMOUNT, dataSource.getValueAt(i,62), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_SECONDARY_AMOUNT, dataSource.getValueAt(i,63), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_RATE_CURRENCY_CODE, dataSource.getValueAt(i,64), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_UNITS, dataSource.getValueAt(i,65), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TAX, dataSource.getValueAt(i,66), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TAX_RATE, dataSource.getValueAt(i,67), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TAX_PKG_INST_ID, dataSource.getValueAt(i,68), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_DISCOUNT, dataSource.getValueAt(i,69), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_TRANS_DATE, dataSource.getValueAt(i,70), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_DESCRIPTION_CODE, dataSource.getValueAt(i,71), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_RATE_TYPE, dataSource.getValueAt(i,72), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_SUBSCR_NO, dataSource.getValueAt(i,73), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_SUBSCR_NO_RESETS, dataSource.getValueAt(i,74), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_EQUIP_STATUS, dataSource.getValueAt(i,75), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_FEDERAL_TAX, dataSource.getValueAt(i,76), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_STATE_TAX, dataSource.getValueAt(i,77), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_COUNTY_TAX, dataSource.getValueAt(i,78), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_CITY_TAX, dataSource.getValueAt(i,79), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_OTHER_TAX, dataSource.getValueAt(i,80), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_REV_RCV_COST_CTR, dataSource.getValueAt(i,81), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_AGGR_USAGE_ID, dataSource.getValueAt(i,82), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_OPEN_ITEM_ID, dataSource.getValueAt(i,83), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_AMOUNT_REDUCTION, dataSource.getValueAt(i,84), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_AMOUNT_REDUCTION_ID, dataSource.getValueAt(i,85), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_ZONE_CLASS, dataSource.getValueAt(i,86), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_BILL_CLASS, dataSource.getValueAt(i,87), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.setAttributeValue(C30BillInvoiceDetail.ATTRIB_INCLUSIVE_TAX, dataSource.getValueAt(i,88), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		            billInvoiceDetail.process();
		            
		            this.addC30SDKObject(billInvoiceDetail);
		    	}
	    	}
    	}
    }

    // populates bill_ref_no and bill_ref_resets attributes
    private final void getNextBillRefNoAndResets() throws C30SDKObjectException
    {
        Integer billRefNo = new Integer(0);
        Integer billRefResets = new Integer(0);
        try {
            C30ExternalCallDef call = new C30ExternalCallDef("bip_get_bill_refno_master");

            //------------------------------------------
            // set up the stored procedure to be called
            //------------------------------------------
            call.addParam(new C30CustomParamDef("reserve_block", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("max_bill_ref_no", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("min_bill_ref_no", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("start_bill_ref_no", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("end_bill_ref_no", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("bill_ref_resets", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));

            //--------------------------------
            // set the input parameter values
            //--------------------------------
            ArrayList paramValues = new ArrayList();
            paramValues.add(new Integer(1)); // reserve only one bill_ref_no
            paramValues.add((Integer)(this.getFactory().c30SdkFactoryCache.get(MAX_BILL_REF_NO_CACHE)).getValue());
            paramValues.add((Integer)(this.getFactory().c30SdkFactoryCache.get(MIN_BILL_REF_NO_CACHE)).getValue());

            //---------------------------------------
            // make the call and process the results
            //---------------------------------------
            //C30KenanMWDataSource dataSource = this.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES, C30SDKObject.ADMIN_SERVER_ID);
            //C30KenanMWDataSource dataSource = this.getFactory().getMwDataSource(C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);

            /*dataSource.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES);

            if (dataSource.getValueAt(0,0) != null) {
                billRefNo = new Integer(dataSource.getValueAt(0, 0).toString());
                billRefResets = new Integer(dataSource.getValueAt(0, 2).toString());
                log.debug("Setting attributes BillRefNo/Resets: " + billRefNo + "/" + billRefResets);
                this.setAttributeValue(ATTRIB_BILL_REF_NO, billRefNo, C30SDKValueConstants.ATTRIB_TYPE_INPUT );
                this.setAttributeValue(ATTRIB_BILL_REF_RESETS, billRefResets, C30SDKValueConstants.ATTRIB_TYPE_INPUT );
            } else {
                log.debug("Could not get next bill_ref_no !");
                throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-009"),"LW-OBJ-009");
            }
*/        } catch (Exception e) {
                log.error(e);
                
                throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-009"),e,"LW-OBJ-009");
        }

    }
    /**
     * Method to get component elements for this component.  This method is used to get all INSTANCE ID's 
     * of the children.  This method is used when disconnecting the component and its children.
     * @return C30SDKObject the returned bill invoice that was created.  If any detail records were
     * created then these would also be returned as children of the bill invoice.
     * @throws C30SDKException 
     * @throws Exception 
     */
    private void createBillInvoice() throws C30SDKException {
    	log.debug("In createBillInvoice");
    	C30SDKObject lwo = null;
    	
    	//first get the account
    	//lwo = getAccount();

        getNextBillRefNoAndResets();
    	
    	//if (!(lwo instanceof C30SDKExceptionMessage)) {
        	
    		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_create_bill_invoice");
			//C30ExternalCallDef call = new C30ExternalCallDef("ps_create_bill_invoice");
	        try {
	
	            //------------------------------------------
	            // set up the stored procedure to be called
	            //------------------------------------------
                    call.addParam(new C30CustomParamDef("bill_ref_no", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
                    call.addParam(new C30CustomParamDef("bill_ref_resets", C30CustomParamDef.INT, 3, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("account_no", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("arch_flag", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("backout_status", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("bill_hold_code", C30CustomParamDef.INT, 3, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("bill_order_number", C30CustomParamDef.STRING, 75, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("converted", C30CustomParamDef.INT, 3, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("copy_type", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("dispatch_count", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("dispatch_date", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("end_offset", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("file_name", C30CustomParamDef.STRING, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("format_error_code", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("format_status", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("from_date", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("image_done", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("image_req", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("include_adj", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("include_bmf", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("include_nrc", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("include_rc", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("include_usage", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("interim_bill_flag", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("jnl_earned_thru_dt", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("jnl_status", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("language_code", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("next_to_date", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("orig_bill_ref_resets", C30CustomParamDef.INT, 3, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("orig_bill_refno", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("page_count", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("payment_due_date", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("prep_date", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("prep_error_code", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("prep_status", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("prep_task", C30CustomParamDef.STRING, 16, C30CustomParamDef.INPUT));            
	            call.addParam(new C30CustomParamDef("prev_ppdd", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("process_num", C30CustomParamDef.STRING, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("special_code", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("start_offset", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("statement_date", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("tax_date", C30CustomParamDef.DATE, 30, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("tax_journal_status", C30CustomParamDef.INT, 3, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("test_flag", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("warm_bill_flag", C30CustomParamDef.INT, 1, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("bill_invoice_type", C30CustomParamDef.INT, 3, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("amount", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.INPUT));
	            call.addParam(new C30CustomParamDef("description_code", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));  
	            call.addParam(new C30CustomParamDef("ACCOUNT_NO", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILL_REF_NO", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILL_REF_RESETS", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILL_SEQUENCE_NUM", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PROCESS_NUM", C30CustomParamDef.STRING, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREV_BILL_REFNO", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREV_BILL_REF_RESETS", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREV_BALANCE_REFNO", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREV_BALANCE_REF_RESETS", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("ZIP", C30CustomParamDef.STRING, 16, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("FROM_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TO_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("NEXT_TO_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREP_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TAX_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("STATEMENT_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PAYMENT_DUE_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREV_PPDD", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREV_CUTOFF_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILL_PERIOD", C30CustomParamDef.STRING, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("CURRENCY_CODE", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PAY_METHOD", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILL_DISP_METH", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("ACCOUNT_STATUS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("IMAGE_REQ", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("IMAGE_DONE", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("SPECIAL_CODE", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREP_TASK", C30CustomParamDef.STRING, 16, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREP_STATUS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("FORMAT_STATUS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("DISPATCH_COUNT", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PAGE_COUNT", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BACKOUT_STATUS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("COLLECTION_HISTORY", C30CustomParamDef.STRING, 12, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("JNL_EARNED_THRU_DT", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("JNL_STATUS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TEST_FLAG", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT)); 
	            call.addParam(new C30CustomParamDef("ARCH_FLAG", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("INTERIM_BILL_FLAG", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("INCLUDE_NRC", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("INCLUDE_RC", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("INCLUDE_ADJ", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("INCLUDE_USAGE", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("INCLUDE_BMF", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("CONVERTED", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("WARM_BILL_FLAG", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("DET_FROM_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("DET_TO_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("DET_TEST_FLAG", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT)); 
	            call.addParam(new C30CustomParamDef("DET_ARCH_FLAG", C30CustomParamDef.INT, 1, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILL_INVOICE_ROW", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PACKAGE_ID", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("COMPONENT_ID", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TYPE_CODE", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("SUBTYPE_CODE", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TRACKING_ID", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TRACKING_ID_SERV", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PREP_SEQUENCE", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("PRORATE_CODE", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILLING_LEVEL", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILLING_CATEGORY", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("AMOUNT", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("RATED_AMOUNT", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("SECONDARY_AMOUNT", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("RATE_CURRENCY_CODE", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("UNITS", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TAX", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TAX_RATE", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TAX_PKG_INST_ID", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("DISCOUNT", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("TRANS_DATE", C30CustomParamDef.DATE, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("DESCRIPTION_CODE", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("RATE_TYPE", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("SUBSCR_NO", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("SUBSCR_NO_RESETS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("EQUIP_STATUS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("FEDERAL_TAX", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("STATE_TAX", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("COUNTY_TAX", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("CITY_TAX", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("OTHER_TAX", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("REV_RCV_COST_CTR", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("AGGR_USAGE_ID", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("OPEN_ITEM_ID", C30CustomParamDef.NUMERIC, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("AMOUNT_REDUCTION", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("AMOUNT_REDUCTION_ID", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("ZONE_CLASS", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("BILL_CLASS", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
	            call.addParam(new C30CustomParamDef("INCLUSIVE_TAX", C30CustomParamDef.NUMERIC, 18, C30CustomParamDef.OUTPUT));
	            
	        } catch (Exception e) {
	            String error = "There was an error while trying to create the bill invoice stored procedure call.\n";
	    		error = error + "Call Name = " + call.getCallName();
	            throw new C30SDKInvalidAttributeException(error);
	        }
	
	        //--------------------------------
	        // set the input parameter values
	        //--------------------------------
	        ArrayList paramValues = new ArrayList();

                paramValues.add(this.getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                paramValues.add(this.getAttributeValue(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));


	        paramValues.add(this.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_ARCH_FLAG, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_BACKOUT_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        paramValues.add(this.getAttributeValue(ATTRIB_BILL_HOLD_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        paramValues.add(this.getAttributeValue(ATTRIB_BILL_ORDER_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        paramValues.add(this.getAttributeValue(ATTRIB_CONVERTED, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_COPY_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_DISPATCH_COUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_DISPATCH_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_END_OFFSET, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_FILE_NAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_FORMAT_ERROR_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_FORMAT_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_FROM_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_IMAGE_DONE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_IMAGE_REQ, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_INCLUDE_ADJ, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_INCLUDE_BMF, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_INCLUDE_NRC, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_INCLUDE_RC, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_INCLUDE_USAGE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_INTERIM_BILL_FLAG, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_JNL_EARNED_THRU_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_JNL_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_LANGUAGE_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_NEXT_TO_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_ORIG_BILL_REFNO, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_ORIG_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PAGE_COUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PAYMENT_DUE_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PREP_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PREP_ERROR_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PREP_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PREP_TASK, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PREV_PPDD, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_PROCESS_NUM, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_SPECIAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_START_OFFSET, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_STATEMENT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_TAX_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_TAX_JOURNAL_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_TEST_FLAG, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_WARM_BILL_FLAG, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_BILL_INVOICE_DETAIL_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_BILL_INVOICE_DETAIL_AMOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	       	paramValues.add(this.getAttributeValue(ATTRIB_BILL_INVOICE_DETAIL_DESCRIPTION_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        
	        //---------------------------------------
	        // make the call and process the results
	        //---------------------------------------
	        //C30KenanMWDataSource dataSource = this.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES, getAccountServerId());
	
	        //populateNewBillInvoice(dataSource);
                
                // allocate space to structure to indicate success.
	        this.billInvoice = new HashMap();
    	//}
    	
    	//return lwo;
    }
    
    
    /**
     * Method to find the Bill Invoice record for a specified Bill_Ref_No using the Kenan API-TS call.
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void findBillInvoice()
        throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException
    {
            HashMap invoiceFilter = new HashMap();
            invoiceFilter.put("Fetch", Boolean.TRUE);
            int startIdx = 0;
            if(getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null && getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
            {
                Map key = new HashMap();
                invoiceFilter.put("Key", key);
                HashMap accountExternalIdFilter = new HashMap();
                accountExternalIdFilter.put("Equal", getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                key.put(ATTRIB_ACCOUNT_INTERNAL_ID, accountExternalIdFilter);
                HashMap attrFilter = new HashMap();
                attrFilter.put("Equal", getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                key.put(ATTRIB_BILL_REF_NO, attrFilter);
                attrFilter = new HashMap();
                attrFilter.put("Equal", getAttributeValue(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                key.put(ATTRIB_BILL_REF_RESETS, attrFilter);
            } else
           {        
            HashMap attrFilter = new HashMap();
            attrFilter.put("Equal", getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
            invoiceFilter.put(ATTRIB_ACCOUNT_INTERNAL_ID, attrFilter);
            startIdx = 1;
           }
       Map callResponse = queryData("Invoice", "Find", invoiceFilter,  getAccountServerId());
       int count = ((Integer)callResponse.get("Count")).intValue();

        if(count == 0)
        {
            setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            return;
        }
       C30BillInvoice cloneLwo = (C30BillInvoice)super.clone();
       for (int i=startIdx; i < count; i++)
       {
                
             HashMap invoice = (HashMap)((Object[])(Object[])callResponse.get("InvoiceList"))[i];   
                
             if (i == startIdx){
               this.billInvoice = invoice;
               this.populateBillInvoice();
               this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             }
             else
             {
                 C30BillInvoice lwo = (C30BillInvoice)cloneLwo.clone();
                 lwo.billInvoice = invoice;
                 lwo.populateBillInvoice();
                 // add to our list of peer objects
                 lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                 super.addC30SDKObject(lwo);
                 
             } 

         }
         
        }
    
    
    /**
     * Method to populate the output attributes from the bill invoice data returned from 
     * the Kenan API-TS calls or the sql stored procedure call result stored in the billInvoice object.
     * @throws C30SDKInvalidAttributeException
     */
       private void populateBillInvoice()
             throws C30SDKInvalidAttributeException
         {
             for  ( Iterator it=billInvoice.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                      Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                      String key = (String)entry.getKey (  ) ; 
                      if (key.equalsIgnoreCase("KEY"))
                      {    
                        HashMap keyValue = (HashMap)entry.getValue (  ) ; 
                        if (keyValue.containsKey(ATTRIB_ACCOUNT_INTERNAL_ID)){
                            if (isAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, ((HashMap)billInvoice.get("Key")).get(ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                            if (isAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                                setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, ((HashMap)billInvoice.get("Key")).get(ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
                        }
                         if (keyValue.containsKey(ATTRIB_BILL_REF_NO)){
                              if (isAttribute(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                  setAttributeValue(ATTRIB_BILL_REF_NO, ((HashMap)billInvoice.get("Key")).get(ATTRIB_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                              if (isAttribute(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                                  setAttributeValue(ATTRIB_BILL_REF_NO, ((HashMap)billInvoice.get("Key")).get(ATTRIB_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
                          }
                          if (keyValue.containsKey(ATTRIB_BILL_REF_RESETS)){
                               if (isAttribute(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                   setAttributeValue(ATTRIB_BILL_REF_RESETS, ((HashMap)billInvoice.get("Key")).get(ATTRIB_BILL_REF_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                               if (isAttribute(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                                   setAttributeValue(ATTRIB_BILL_REF_RESETS, ((HashMap)billInvoice.get("Key")).get(ATTRIB_BILL_REF_RESETS), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
                           }
                      }
                      else
                      {
                         Object value = entry.getValue (  ) ;
                         if (value != null)
                         {
                             if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)){
                                 setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                 log.debug(  ( String ) key+" " +  value +"   SET") ; 
                                 if (key.equalsIgnoreCase("TaxDate"))
                                     setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                             }
                             else
                               if (value != null)
                                 log.debug(  ( String ) key+" " +  value ) ; 
                             
                         }
                      }
             }
    }
         
    
    /**
     * Method to find Kenan FX 1.0 Bill Invoices using C30JDBCDataSource invoking StoredProcedure in the Customer database.
     * FX 1.0 is a Single Account Server Database so all catalog, admin and customer data is in a single database.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidConfigurationException
     */
    private void findK1BillInvoice()
        throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException
    {
        log.debug("In findK1BillInvoice");
        
            boolean searchByBillRefNo = false;
            String procCall;
            if (getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null){
                searchByBillRefNo = true;
                procCall = BILL_REFNO_INVOICE_CALL;
            }
            else
                procCall = ACCOUNT_INVOICE_CALL;

            C30ExternalCallDef call = new C30ExternalCallDef(procCall);
        try
        {
                C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId());   
                // set input parameter values
                    if (searchByBillRefNo)
                    {
                        call.addParam(new C30CustomParamDef(ATTRIB_BILL_REF_NO, 1, 10, 1));
                        call.addParam(new C30CustomParamDef(ATTRIB_BILL_REF_RESETS, 1, 3, 1));
                    }
                    else
                           call.addParam(new C30CustomParamDef("ACCOUNT_NO", 1, 10, 1));

                  // set output parameter values
                  call.addParam(new C30CustomParamDef("ACCOUNT_NO", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("BILL_REF_NO", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("BILL_REF_RESETS", 1, 3, 2));
                  call.addParam(new C30CustomParamDef("BILL_SEQUENCE_NUM", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("PROCESS_NUM", 2, 1, 2));
                  call.addParam(new C30CustomParamDef("PREV_BILL_REFNO", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("PREV_BILL_REF_RESETS", 1, 3, 2));
                  call.addParam(new C30CustomParamDef("PREV_BALANCE_REFNO", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("PREV_BALANCE_REF_RESETS", 1, 3, 2));
                  call.addParam(new C30CustomParamDef("ZIP", 2, 16, 2));
                  call.addParam(new C30CustomParamDef("FROM_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("TO_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("NEXT_TO_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("PREP_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("TAX_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("STATEMENT_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("PAYMENT_DUE_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("PREV_PPDD", 3, 2));
                  call.addParam(new C30CustomParamDef("PREV_CUTOFF_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("BILL_PERIOD", 2, 3, 2));
                  call.addParam(new C30CustomParamDef("CURRENCY_CODE", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("PAY_METHOD", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("BILL_DISP_METH", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("TAX_JOURNAL_STATUS", 1, 3, 2));
                  call.addParam(new C30CustomParamDef("ACCOUNT_STATUS", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("IMAGE_REQ", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("IMAGE_DONE", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("SPECIAL_CODE", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("PREP_TASK", 2, 16, 2));
                  call.addParam(new C30CustomParamDef("PREP_STATUS", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("FORMAT_STATUS", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("DISPATCH_COUNT", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("DISPATCH_DATE", 3, 2));
                  call.addParam(new C30CustomParamDef("FILE_NAME", 2, 30, 2));
                  call.addParam(new C30CustomParamDef("START_OFFSET", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("END_OFFSET", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("PAGE_COUNT", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("BILL_HOLD_CODE", 1, 3, 2));
                  call.addParam(new C30CustomParamDef("PREP_ERROR_CODE", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("FORMAT_ERROR_CODE", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("BACKOUT_STATUS", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("COPY_TYPE", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("ORIG_BILL_REFNO", 9, 10, 2));
                  call.addParam(new C30CustomParamDef("ORIG_BILL_REF_RESETS", 1, 3, 2));
                  call.addParam(new C30CustomParamDef("COLLECTION_HISTORY", 2, 12, 2));
                  call.addParam(new C30CustomParamDef("JNL_EARNED_THRU_DT", 3, 2));
                  call.addParam(new C30CustomParamDef("JNL_STATUS", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("TEST_FLAG", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("ARCH_FLAG", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("INTERIM_BILL_FLAG", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("INCLUDE_NRC", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("INCLUDE_RC", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("INCLUDE_ADJ", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("INCLUDE_USAGE", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("INCLUDE_BMF", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("LANGUAGE_CODE", 1, 6, 2));
                  call.addParam(new C30CustomParamDef("CONVERTED", 1, 3, 2));
                  call.addParam(new C30CustomParamDef("WARM_BILL_FLAG", 1, 1, 2));
                  call.addParam(new C30CustomParamDef("BILL_ORDER_NUMBER", 2, 75, 2));
                  
                  
                ArrayList paramValues = new ArrayList();
                if (searchByBillRefNo)
                {
                    paramValues.add(getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                    paramValues.add(getAttributeValue(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                }
                else
                    paramValues.add(getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                    
                dataSource.queryData(call, paramValues, 2);
                log.debug("Total row count = " + dataSource.getRowCount());
                int rowCount = dataSource.getRowCount();
                if(rowCount > 0)
                {
                    Map resultSet  = dataSource.getResultMap(); 
                
                    C30BillInvoice cloneLwo = (C30BillInvoice)super.clone();
                         for (int i=0; i < rowCount; i++)
                         {
                                
                             Hashtable invoice = (Hashtable)resultSet.get(i);

                             if (i == 0){
                               this.billInvoice = invoice;
                               //this.populateK1BillInvoice(invoice);
                               this.populateK1BillInvoice();
                               this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                             }
                             else
                             {
                                 C30BillInvoice lwo = (C30BillInvoice)cloneLwo.clone();
                                 lwo.billInvoice = invoice;
                                 //lwo.populateK1BillInvoice(invoice);
                                  lwo.populateK1BillInvoice();
                                 // add to our list of peer objects
                                 lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                                 super.addC30SDKObject(lwo);
                                 
                             }  

                         }

                }
                  
                }
                catch(Exception e)
                {
                    throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-028"),"INVALID-ATTRIB-028");
                }
                
    }

    /**
    * Method to populate FX 1.0 Invoices attributes
    * @throws C30SDKInvalidAttributeException
    */
    private void populateK1BillInvoice()
          throws C30SDKInvalidAttributeException
    {
       
         for  ( Iterator it=billInvoice.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                  Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                  String key = (String)entry.getKey (  ) ; 
                 Object value = billInvoice.get(key) ;
                 key = C30StringUtils.SqlToKenanName(key);
                
                 if (value != null)
                 {
                     if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                     {
                         setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                         
                         log.debug(  ( String ) key+" " +  value +"   SET") ;                            
                     }
                    else
                       if (value != null){
                         log.debug(  ( String ) key+" " +  value ) ; 
                       }
                            
                 }
          }
     }
    
    
    /**
     * Method to validate that all the required dataAttributes have been set.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
     * @throws C30SDKKenanFxCoreException 
     */
    protected final void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        String acctExternalId = null;
        Integer acctExternalIdType = null;
        Integer acctInternalId = null;
        Integer accountServerId = null;
        if  (getAttributeValue("ItemActionId", C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ITEM_ACTION_ID_FIND) ||
             (isAttribute(ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && (Boolean)getAttributeValue(ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT))) {
            
            if (isAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
             acctExternalId = (String)getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            if (isAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
              acctExternalIdType = (Integer)getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            if (isAttribute(ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
               acctInternalId = (Integer)getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
            
            
             if (isAttribute(ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
                accountServerId = (Integer)getAttributeValue(ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
             
                       
            if (((acctExternalId == null || acctExternalIdType == null) && acctInternalId == null)  && accountServerId == null ) 
            {
            	throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-029"),"INVALID-ATTRIB-029");
            }
            
        }
        else  // this is a create request
            super.validateAttributes();
    }
    
	/**
	 * Method to process the bill invoice object.
	 * @return the populated bill invoice object containing the newly created bill invoice information.
	 * @throws C30SDKException 
	 */
	public C30SDKObject process() throws C30SDKException {
    	C30SDKObject lwo = null;
    	try {
                log.debug("Validating attributes ...");
    		validateAttributes();
    	    if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
               isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
    	        getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
            {
                if (super.canServerIdCacheBeSetLocally())
                     super.setAccountServerIdCache();
                 else{
                     if (isAttribute(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)  &&
                         (C30SDKObject)getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null){
                      C30SDKObject account = getAccount();
                         if(account instanceof C30SDKExceptionMessage)
                           return account;
                     }
                }
                super.setAccountServerId();	
            }     
        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ITEM_ACTION_ID_CREATE)) {
        		createBillInvoice();
        	}
    	    if((this.isAttribute("ItemActionId", C30SDKValueConstants.ATTRIB_TYPE_INPUT) && this.getAttributeValue("ItemActionId", C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ITEM_ACTION_ID_FIND)) ||
    	        (isAttribute(ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && (Boolean)this.getAttributeValue(ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) )
            {
    	       
  	        if(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0)
    	            findK1BillInvoice();
    	        else
    	            findBillInvoice();
    	          
    	    }
          if (this.billInvoice != null)
	       	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	 lwo = this;
           // }
    	} catch (C30SDKException e) {
   			lwo = this.createExceptionMessage(e);
		}              
 
    	return lwo;
	}


    /**
     * Method to get the account associated with the bill invoice.  It is a requirement that the
     * account number be made available as an attribute.  This means we can ensure that the number
     * is correct before attempting to create the bill invoice.
     * @return the returned account which is to be associated with the bill invoice.
     * @throws C30SDKException 
     */
    private C30SDKObject getAccount() throws C30SDKException {
    	log.debug("Starting C30AccountExternalId.getAccount");
    	
    	C30SDKObject account = null;
    	
	// if the account external ID has a parent (which must be an account) then get and 
    	// set it into the object.  This saves us from making a DB call to get the account.
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
        
		account = (C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.setAttributeValue(ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
		} else if (this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) == null) {
        	
	    	//try get account from object children
	    	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
	    		if (this.getC30SDKObject(i) instanceof C30Account) {
	    			account = this.getC30SDKObject(i);
	    		}
	    	}
	    	
	    	//if no account in children then get account from middleware
	    	if (account == null) {
		    	account = this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);
                        
	    	    if (this.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)
	    	        account.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	    	    if (this.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)           
	    	            account.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	    	    if (this.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)           
	    	        account.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

                    account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		    account = account.process();
	    	}
	    	
		}
	    	
        //if the processed account did not produce an exception then set the account ID's into this object.
        if (!(account instanceof C30SDKExceptionMessage)) {
         	this.setAttributeValue(ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                setAccountLevelAttributes(account);
        }
    	log.debug("Finished C30AccountExternalId.getAccount");
    	return account;
    }
    
    /**
     *      
     * Method which sets all account data to be usee by the object.
     * @param account
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectException
     */
     private void setAccountLevelAttributes(C30SDKObject account)
         throws C30SDKInvalidAttributeException, C30SDKObjectException
     {
     
         if(account instanceof C30Account)
            if (account != null)
            {
                 if (isAttribute(ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null)
                     setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                 if (isAttribute(ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) 
                     setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                 if (isAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))     
                     setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, account.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                 if (isAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))       
                     setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, account.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            }
     }

     /**
     * Method which gets any data to be cached for further use by the object.
     * @throws C30SDKCacheException
     */
    protected void loadCache() throws C30SDKCacheException {
    	log.debug("Starting C30BillInvoice.loadCache");

         // Find BIP MAX_REFNO system paraemter
        log.debug("Getting MAX_REFNO ...");
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	if ((this.getFactory().c30SdkFactoryCache.get(MAX_BILL_REF_NO_CACHE)).getValue()==null) {
	            try {
                        Map key = new HashMap();
                        Map systemParameterFilter = new HashMap();
                        systemParameterFilter.put("Key", key);
                        systemParameterFilter.put("Fetch", new Boolean(true));
                        HashMap attrFilter = new HashMap();
                        attrFilter.put("Equal", "BIP");
                        key.put("Module", attrFilter);

                        attrFilter = new HashMap();
                        attrFilter.put("Equal", "MAX_REFNO");
                        key.put("ParameterName", attrFilter);


	        	Map callResponse = this.queryData("SystemParameter", "Find", systemParameterFilter, getAccountServerId());

	        	//get the count
	                int count = ( (Integer) callResponse.get("Count")).intValue();

	                if (count == 0) {
		            	//C30SDKCache.setCache(MAX_BILL_REF_NO_CACHE,  new Integer("999999999"));
		            	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(MAX_BILL_REF_NO_CACHE,  new Integer("999999999"));
						this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
						
	                } else {
	                    Map sysParam = (HashMap) ((Object[]) callResponse.get("SystemParameterList"))[0];
		            	//C30SDKCache.setCache(MAX_BILL_REF_NO_CACHE, new Integer(sysParam.get("IntValue").toString()));
		            	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(MAX_BILL_REF_NO_CACHE, new Integer(sysParam.get("IntValue").toString()));
						this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
						
	                }
                        //log.debug("MAX_BILL_REF_NO_CACHE set to " + C30SDKCache.getCache(MAX_BILL_REF_NO_CACHE));
                    } catch (Exception e) {
                            log.error(e);
		    		String error = "An error occurred while trying to load the service cache (MAX_BILL_REF_NO_CACHE).";
		        	throw new C30SDKCacheException(error, e);
                    }
                }

                 if ((this.getFactory().c30SdkFactoryCache.get(MIN_BILL_REF_NO_CACHE)).getValue()==null) {
	            try {
                        Map key = new HashMap();
                        Map systemParameterFilter = new HashMap();
                        systemParameterFilter.put("Key", key);
                        systemParameterFilter.put("Fetch", new Boolean(true));
                        HashMap attrFilter = new HashMap();
                        attrFilter.put("Equal", "BIP");
                        key.put("Module", attrFilter);

                        attrFilter = new HashMap();
                        attrFilter.put("Equal", "MIN_REFNO");
                        key.put("ParameterName", attrFilter);


	        	Map callResponse = this.queryData("SystemParameter", "Find", systemParameterFilter, getAccountServerId());

	        	//get the count
	                int count = ( (Integer) callResponse.get("Count")).intValue();
                       
	                if (count == 0) {
	                	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(MIN_BILL_REF_NO_CACHE, new Integer(1));
	    				this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
		            	//C30SDKCache.setCache(MIN_BILL_REF_NO_CACHE,  new Integer(1));
	                } else {
	                    Map sysParam = (HashMap) ((Object[]) callResponse.get("SystemParameterList"))[0];
		            	//C30SDKCache.setCache(MIN_BILL_REF_NO_CACHE, new Integer(sysParam.get("IntValue").toString()));
		            	C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(MIN_BILL_REF_NO_CACHE, new Integer(sysParam.get("IntValue").toString()));
	    				this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
		            	
	                }
                        //log.debug("MIN_BILL_REF_NO_CACHE set to " + C30SDKCache.getCache(MIN_BILL_REF_NO_CACHE));
                    } catch (Exception e) {
                        String error = "An error occurred while trying to load the service cache (MIN_BILL_REF_NO_CACHE).";
                        throw new C30SDKCacheException(exceptionResourceBundle.getString("LW-CACHE-001"),"LW-CACHE-001");
                    }
                 }
        }
         log.debug("Ending C30BillInvoice.loadCache");
    }
}
