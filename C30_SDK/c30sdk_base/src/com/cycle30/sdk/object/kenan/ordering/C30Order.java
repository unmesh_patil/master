/*
 * C30Order.java
 *
 * Created on December 7, 2006, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKCollection;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30HashMapUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * A lightweight order is a pre-configured template for a Kenan/FX order.  Similar to
 * how a package is defined in the Kenan database as a package_id and is instantiated
 * to an account producing a package_inst_id, the pre-configured order is defined in
 * the Kenan database as a lightweight_order_id and the order is instantiated producing
 * an order_id.
 * <br><br>
 * The configuration of the lightweight order defines the service orders and the service
 * order items that will be generated for the dynamic order that is created.  The
 * lightweight order consists of one or more pre-configured service orders (lightweight
 * service orders), which contain one or more pre-configured order items (lightweight
 * order items).
 *
 * @author John Reeves
 */
public class C30Order extends C30SDKObject implements Cloneable {

    /** Logging. */
    private static Logger log = Logger.getLogger(C30Order.class);

    public static final String ATTRIB_ACCOUNT = "Account";
    public static final String ATTRIB_ACCOUNT_INTERNAL_ID = "AccountInternalId";
    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID = "AccountExternalId";
    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE = "AccountExternalIdType";
    public static final String ATTRIB_BATCH_ID = "BatchId";
    public static final String ATTRIB_CANCEL_ON_ERROR = "CancelOnError";
    public static final String ATTRIB_COMMIT_ORDER = "CommitOrder";
    public static final String ATTRIB_COMPLETE_DT = "CompleteDt";
    public static final String ATTRIB_CONTACT_COMPANY = "ContactCompany";
    public static final String ATTRIB_CONTACT_EMAIL = "ContactEmail";
    public static final String ATTRIB_CONTACT_FNAME = "ContactFname";
    public static final String ATTRIB_CONTACT_LNAME = "ContactLname";
    public static final String ATTRIB_CONTACT_PHONE1 = "ContactPhone1";
    public static final String ATTRIB_CONTACT_PHONE2 = "ContactPhone2";
    public static final String ATTRIB_CONTACT_TITLE = "ContactTitle";
    public static final String ATTRIB_CREATE_DT = "CreateDt";
    public static final String ATTRIB_CREATE_WHO = "CreateWho";
    public static final String ATTRIB_DESCRIPTION = "Description";
    public static final String ATTRIB_EXTENDED_DATA = "ExtendedData";
    public static final String ATTRIB_GENERATE_WORKFLOW = "GenerateWorkflow";
    public static final String ATTRIB_INCREMENTAL_ORDER_COMMIT = "IncrementalOrderCommit";
    public static final String ATTRIB_IS_COMMITTED = "IsCommitted";
    public static final String ATTRIB_IS_LOCKED = "IsLocked";
    public static final String ATTRIB_LOCK_DT = "LockDt";
    public static final String ATTRIB_LOCK_WHO = "LockWho";
    public static final String ATTRIB_ORDER_NUMBER = "OrderNumber";
    public static final String ATTRIB_ORDER_ID = "OrderId";
    public static final String ATTRIB_ORDER_STATUS_ID = "OrderStatusId";
    public static final String ATTRIB_PARAM_ID = "ParamId";
    public static final String ATTRIB_PARAM_VALUE = "ParamValue";
    public static final String ATTRIB_SALES_CHANNEL_ID = "SalesChannelId";

    public static final Integer ORD_STATUS_IN_PROGRESS = new Integer(10);
    public static final Integer ORD_STATUS_COMPLETED = new Integer(80);
    public static final Integer ORD_STATUS_CANCELLED = new Integer(90);
    public static final Integer ORD_STATUS_COMMITTED = new Integer(20);

    public static final Integer ORD_INSERT_SINGLE = new Integer(0);
    public static final Integer ORD_INSERT_MULTI = new Integer(1);

    /**
     * Creates a new instance of C30Order.
     * @param factory Factory used to create C30ServiceOrder objects
     * @param objectType Type of lightweight order
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Order(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /** 
     * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
     * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
     * the framework to create objects.  It is also used by the BRE front end.
     * @param factory Factory used to create C30ServiceOrder objects
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Order(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30Order.class);
    }
    
    /**
     * Initializes the attributes of the Kenan/FX order.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the order item.
     */
    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {
        
    	//===================
        // Data Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_COMPANY, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_EMAIL, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_FNAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_LNAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_PHONE1, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_PHONE2, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_TITLE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_GENERATE_WORKFLOW, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_STATUS_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_DESCRIPTION, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.addAttribute(new C30SDKAttribute(ATTRIB_BATCH_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        //===================
        // System Attributes
        //===================
        C30SDKAttribute attribute = new C30SDKAttribute(ATTRIB_CANCEL_ON_ERROR, Boolean.class);
        attribute.setValue(Boolean.TRUE);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(ATTRIB_IS_COMMITTED, Boolean.class);
        attribute.setValue(Boolean.FALSE);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(ATTRIB_COMMIT_ORDER, Boolean.class);
        attribute.setValue(Boolean.TRUE);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(ATTRIB_INCREMENTAL_ORDER_COMMIT, Boolean.class);
        attribute.setValue(Boolean.FALSE);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //===================
        // Output Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPLETE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_COMPANY, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_EMAIL, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_FNAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_LNAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_PHONE1, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_PHONE2, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONTACT_TITLE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_GENERATE_WORKFLOW, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_LOCKED, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_LOCK_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_LOCK_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_STATUS_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_DESCRIPTION, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);        
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.addAttribute(new C30SDKAttribute(ATTRIB_BATCH_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
        super.loadExtendedDataAttributes("ORD_ORDER", new Integer("0"));
    }

    /**
     * Creates a copy of the C30Order object.
     * @return A copy of the C30Order object
     */
    @Override
	public Object clone() {
        C30Order copy = (C30Order) super.clone();
        return copy;
    }

    /**
     * Processes the order. This method does NOT commit the order.  Use commitOrder() to commit the order.
     * @return the order, if it is successfully processed.
     * @throws C30SDKInvalidAttributeException 
     */
    @Override
	public final C30SDKObject process() throws C30SDKInvalidAttributeException {
    	
        String error = "creating lightweight order:\n\tLW Order Type: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    	C30SDKObject lwo = null;
        
        // ******  added jcw: LWO Enhancement
        C30SDKObject accountLwo = null;
        // ******  end added jcw: LWO Enhancement
        
    	Map order = null;
    	Map orderData = new HashMap();
        C30PackageInstanceList packageInstanceList = new C30PackageInstanceList();

        try {
	        
        	//if the order is null then create the order.
        	if (this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
	            this.setAttributesFromNameValuePairs();
	            this.validateAttributes();
	            //************ added jcw LWO Enhancement
	            if (isAttributeValueSet("Account", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
	              lwo = (C30SDKObject)getAttributeValue("Account", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            else   
	            {
	            lwo = getAssociatedAccount();  // original line
	            if (lwo != null)
	                this.setAttributeValue("Account",lwo, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            }
	            //**********  end LWO Enhancement
	            
	            //we might have an error
	            if (lwo instanceof C30Account) {
	                // ******  added jcw: LWO Enhancement
	                accountLwo = lwo;
	                // ******  end added jcw: LWO Enhancement
                        log.debug("Initializing attributes of FXOrder");
		            this.setFXOrderAttributes(orderData);
                    log.debug("DONE Initializing attributes of FXOrder");
		            Map callResponse = this.queryData("Order", "Create", orderData, getAccountServerId());
			        order = (HashMap) callResponse.get("Order");
			        
			        //remove duplicate extended data - this is a hack to fix a core API-TS bug
			        C30HashMapUtils.removeDuplicatesFromArray(order, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

			        populateOrder(order);
			        
			        error = error + "\n\tOrder ID: " + this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	            }
	        }
        	
        	if (!(lwo instanceof C30SDKExceptionMessage)) {
	            //------------------------------------------
	            // loop through all the lightweight service 
	        	// orders and create the service orders
	            //------------------------------------------
	
        		//------------------------------------
        		//ACCOUNT LEVEL service orders first.
        		//------------------------------------
        		C30SDKCollection serviceOrders = this.getServiceOrderByLevel(Boolean.FALSE);
		        C30SDKObject lwso = null;
	            for (int i = 0; i < serviceOrders.getC30SDKObjectCount(); i++) {
	                lwso = serviceOrders.getC30SDKObject(i);
	                lwso.setNameValuePairs(this.getNameValuePairMap());
	                
	                //set the package link
	                lwso.setAttributeValue(C30ServiceOrder.ATTRIB_PACKAGE_INSTANCE_LIST, packageInstanceList, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	                
	                //set the account into the service order and set the accounts internal ID.
	                C30SDKObject account = (C30SDKObject) this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	                lwso.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                	lwso.setAttributeValue(C30AccountServiceOrder.ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	
	                lwso = lwso.process();
	                
	                //if the processed service order produced an exception then break;
	                if (lwso instanceof C30SDKExceptionMessage) {
	                	lwo = lwso;
	                	break;
	                }
	                
	            }
        		
        		//-------------------------------------
        		//SERVICE LEVEL service orders second.
        		//-------------------------------------
	            if (!(lwo instanceof C30SDKExceptionMessage)) {
		            serviceOrders = this.getServiceOrderByLevel(Boolean.TRUE);
		            for (int i = 0; i < serviceOrders.getC30SDKObjectCount(); i++) {
		                lwso = serviceOrders.getC30SDKObject(i);
		                lwso.setNameValuePairs(this.getNameValuePairMap());
		                
		                //set the package link
		                lwso.setAttributeValue(C30ServiceOrder.ATTRIB_PACKAGE_INSTANCE_LIST, packageInstanceList, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		                
		                C30SDKObject account = (C30SDKObject) this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		                lwso.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		
		                lwso = lwso.process();
		                
		                //if the processed service order produced an exception then break;
		                if (lwso instanceof C30SDKExceptionMessage) {
		                	lwo = lwso;
		                	break;
		                }
		                
		            }
	            }
        	}
        	
            //if we do not have an exception then commit order
            if (!(lwo instanceof C30SDKExceptionMessage)) {
            
	            //if we are going to commit the order
	            if ( ((Boolean) this.getAttributeValue(ATTRIB_COMMIT_ORDER, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {
	                if ( ((Boolean) this.getAttributeValue(ATTRIB_INCREMENTAL_ORDER_COMMIT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {
	                	incrementalCommitOrder();
	                } else {
	                	commitOrder();
	                }
	            }
	            
	    		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            lwo = this;
            
	        //if we do have an exception then we may need to cancel the order.
            } else {
	            
            	if ( ((Boolean) this.getAttributeValue(ATTRIB_CANCEL_ON_ERROR, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(Boolean.TRUE) 
            			&& this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	            	cancelOrder();
	            }

	            if (this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	            	C30SDKObject account = (C30SDKObject) this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            	error = error + "\n\tAccount Internal ID: " + account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	            	error = error + "\n\tAccount External ID: " + account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) + "\n" ;
	            }
        		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "while " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            
    	    // jcw added: LWO enhancements
    	    if(!(lwo instanceof C30SDKExceptionMessage)){
    	    
    	     C30SDKCollection nonServiceOrders = getNonServiceOrders();
    	     C30SDKObject lwnso = null;
    	      int i = 0;
    	     do
    	     {
    	         if(i >= nonServiceOrders.getC30SDKObjectCount())
    	             break;
    	         lwnso = nonServiceOrders.getC30SDKObject(i);
    	         lwnso.setNameValuePairs(getNameValuePairMap());
    	         //C30SDKObject account = (C30SDKObject)getAttributeValue("Account", Constants.ATTRIB_TYPE_INPUT);
    	         lwnso.setAttributeValue("AccountInternalId", accountLwo.getAttributeValue("AccountInternalId", C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	         if (lwnso.isAttribute("Account", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
    	            lwnso.setAttributeValue("Account", accountLwo, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	         lwnso = lwnso.process();
    	         // this is really not an exception for the entire process - need to indicate that there is a related issue
    	         if(lwnso instanceof C30SDKExceptionMessage)
    	         {
    	             //lwo = lwso;
    	             break;
    	         }
    	         i++;
    	     } while(true);
    	    
    	    }// end jcw added: LWO enhancements 
    	    
            
    	} catch (Exception e) {
    		
    	        log.error(e);
    	        
    		//cancel the order
    		try {
    			
	            if ( ((Boolean) this.getAttributeValue(ATTRIB_CANCEL_ON_ERROR, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(Boolean.TRUE)
	            		&& this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	            	cancelOrder();
	            }
            
	    		//create exception to be passed back to user
	            if (this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	            	error = error + " (Order ID: " + this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ")\n" ;
	            }
	            if (this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	            	C30SDKObject account = (C30SDKObject) this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            	error = error + " (Account Internal ID: " + account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) + ")\n" ;
	            	error = error + " (Account External ID: " + account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) + ")\n" ;
	            }
	            
	            C30SDKException er = new C30SDKException("Error " + error, e);
	    		lwo = this.createExceptionMessage(er);

        	} catch (Exception loce) {
        	        log.error(loce);
        		loce.initCause(e);
        		lwo = this.createExceptionMessage(loce);
        	}

        }

        return lwo;
    }

    /**
     * Method to cancel an order.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while cancelling the order
     * @throws C30SDKInvalidAttributeException 
     * @throws C30SDKKenanFxCoreException 
     */
    private void cancelOrder() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        Map key = new HashMap();
        key.put(ATTRIB_ORDER_ID, this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        Map orderData = new HashMap();
        orderData.put("Key", key);

        this.queryData("Order", "Lock", orderData, getAccountServerId());
        Map callResponse = this.queryData("Order", "Cancel", orderData, getAccountServerId());
        Map order = (HashMap) callResponse.get("Order");
        populateOrder(order);
    }

    /**
     * Commits the Kenan/FX order using API-TS calls.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while committing the order
     * @throws C30SDKObjectException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void commitOrder() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        if (((Boolean) this.getAttributeValue(ATTRIB_IS_COMMITTED, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {
            String error = "Lightweight order can only be committed once";
            throw new C30SDKObjectException(error);
        }

        log.info("Bulk committing order: " + this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        if (this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {

	        //create hashmap shell
            Map key = new HashMap();
	        key.put(ATTRIB_ORDER_ID, this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        Map orderData = new HashMap();
	        orderData.put("Key", key);

	        Map callResponse = this.queryData("Order", "Commit", orderData, getAccountServerId());
	        Map order = (HashMap) callResponse.get("Order");
	        
	        populateOrder(order);
	        this.setAttributeValue(ATTRIB_IS_COMMITTED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        } else {
            String error = "Order must be created before it can be committed";
            throw new C30SDKObjectException(error);
        }
    }

   /**
     * Commits the Kenan/FX order using API-TS calls.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while committing the order
     * @throws C30SDKObjectException if there was a problem while committing the order
 * @throws C30SDKKenanFxCoreException 
     */
    private void incrementalCommitOrder() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
         if (((Boolean) this.getAttributeValue(ATTRIB_IS_COMMITTED, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {
            String error = "Lightweight order can only be committed once";
            throw new C30SDKObjectException(error);
        }

        log.info("Incrementally committing order: " + this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        if (this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {

	        Map orderData = new HashMap();
	        Map key = new HashMap();
	        key.put(ATTRIB_ORDER_ID, this.getAttributeValue(ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        orderData.put("Key", key);

	        //Update the order status to committed.
	        orderData.put(ATTRIB_ORDER_STATUS_ID, ORD_STATUS_COMMITTED);

	        Map callResponse = this.queryData("Order", "Update", orderData, getAccountServerId());
	        Map order = (HashMap) ((Object[]) callResponse.get("OrderList"))[0];
	        
	        //remove duplicate extended data - this is a hack to fix a core API-TS bug
	        C30HashMapUtils.removeDuplicatesFromArray(order, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);
	        
	        populateOrder(order);
	        
	        this.setAttributeValue(ATTRIB_IS_COMMITTED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

            C30ServiceOrder lwso = null;
            for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
                lwso = (C30ServiceOrder) this.getC30SDKObject(i);
                lwso.commitServiceOrder();
            }


        } else {
            String error = "Order must be created before it can be committed";
            throw new C30SDKObjectException(error);
        }
    }

    /**
     * Method to populate the object using the hashmap returned from middleware.
     * @throws C30SDKInvalidAttributeException if there is a problem populating an attribute.
     */
    private void populateOrder(final Map order) throws C30SDKInvalidAttributeException {
        this.setAttributeValue(ATTRIB_ORDER_ID, ((HashMap) order.get("Key")).get(ATTRIB_ORDER_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_ORDER_STATUS_ID, order.get(ATTRIB_ORDER_STATUS_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        this.setAttributeValue(ATTRIB_ORDER_ID, ((HashMap) order.get("Key")).get(ATTRIB_ORDER_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, order.get(ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, ((C30SDKObject) this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, ((C30SDKObject) this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMPLETE_DT, order.get(ATTRIB_COMPLETE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTACT_COMPANY, order.get(ATTRIB_CONTACT_COMPANY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTACT_EMAIL, order.get(ATTRIB_CONTACT_EMAIL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTACT_FNAME, order.get(ATTRIB_CONTACT_FNAME), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTACT_LNAME, order.get(ATTRIB_CONTACT_LNAME), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTACT_PHONE1, order.get(ATTRIB_CONTACT_PHONE1), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTACT_PHONE2, order.get(ATTRIB_CONTACT_PHONE2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONTACT_TITLE, order.get(ATTRIB_CONTACT_TITLE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CREATE_DT, order.get(ATTRIB_CREATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CREATE_WHO, order.get(ATTRIB_CREATE_WHO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_GENERATE_WORKFLOW, order.get(ATTRIB_GENERATE_WORKFLOW), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_IS_LOCKED, order.get(ATTRIB_IS_LOCKED), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_LOCK_DT, order.get(ATTRIB_LOCK_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_LOCK_WHO, order.get(ATTRIB_LOCK_WHO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ORDER_STATUS_ID, order.get(ATTRIB_ORDER_STATUS_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ORDER_NUMBER, order.get(ATTRIB_ORDER_NUMBER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_SALES_CHANNEL_ID, order.get(ATTRIB_SALES_CHANNEL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.setAttributeValue(ATTRIB_BATCH_ID, order.get(ATTRIB_BATCH_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
    }

    /**
     * Initilize attirbutes for the Kenan FX order that is about to
     * be created through the API TS.
     * @param order Kenan/FX service order object
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes whileinserting the order item.
     */
    private void setFXOrderAttributes(final Map order) throws C30SDKInvalidAttributeException {

        //------------------------------------------------------------------------
        // Set the following Order attributes:
        //  - OrderStatusId = IN_PROGRESS (not available as an LW Attribute)
        //  - IsLocked = FALSE (not available as an LW Attribute)
        //  - GenerateWorkflow = TRUE (Available as LW attribute - but defaulted)
        //------------------------------------------------------------------------
        order.put(ATTRIB_ORDER_STATUS_ID, ORD_STATUS_IN_PROGRESS);
        order.put(ATTRIB_IS_LOCKED, Boolean.FALSE);

        if (!super.isAttributeValueSet(ATTRIB_GENERATE_WORKFLOW, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
            order.put(ATTRIB_GENERATE_WORKFLOW, Boolean.TRUE);
        }

        //------------------------------------
        // set the remaining order attributes
        //------------------------------------
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = "";
        Object attributeValue = null;
        while (iterator.hasNext()) {
            attributeName = (String) iterator.next();
            attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if (attributeValue != null) {
                if (attributeName.equals(ATTRIB_ACCOUNT_INTERNAL_ID)) {
                    order.put(ATTRIB_ACCOUNT_INTERNAL_ID, attributeValue);
                } else if (attributeName.equals(ATTRIB_CONTACT_COMPANY)) {
                    order.put(ATTRIB_CONTACT_COMPANY, attributeValue);
                } else if (attributeName.equals(ATTRIB_CONTACT_EMAIL)) {
                    order.put(ATTRIB_CONTACT_EMAIL, attributeValue);
                } else if (attributeName.equals(ATTRIB_CONTACT_FNAME)) {
                    order.put(ATTRIB_CONTACT_FNAME, attributeValue);
                } else if (attributeName.equals(ATTRIB_CONTACT_LNAME)) {
                    order.put(ATTRIB_CONTACT_LNAME, attributeValue);
                } else if (attributeName.equals(ATTRIB_CONTACT_PHONE1)) {
                    order.put(ATTRIB_CONTACT_PHONE1, attributeValue);
                } else if (attributeName.equals(ATTRIB_CONTACT_PHONE2)) {
                    order.put(ATTRIB_CONTACT_PHONE2, attributeValue);
                } else if (attributeName.equals(ATTRIB_CONTACT_TITLE)) {
                    order.put(ATTRIB_CONTACT_TITLE, attributeValue);
                } else if (attributeName.equals(ATTRIB_CREATE_WHO)) {
                    order.put(ATTRIB_CREATE_WHO, attributeValue);
                } else if (attributeName.equals(ATTRIB_GENERATE_WORKFLOW)) {
                    order.put(ATTRIB_GENERATE_WORKFLOW, attributeValue);
                } else if (attributeName.equals(ATTRIB_ORDER_NUMBER)) {
                    order.put(ATTRIB_ORDER_NUMBER, attributeValue);
                } else if (attributeName.equals(ATTRIB_SALES_CHANNEL_ID)) {
                    order.put(ATTRIB_SALES_CHANNEL_ID, attributeValue);
                } else if (attributeName.equals(ATTRIB_SALES_CHANNEL_ID)) {
                    order.put(ATTRIB_BATCH_ID, attributeValue);
                } else {
                    //------------------------------------------------------
                    // check if the attribute is an extended data parameter
                    //------------------------------------------------------
                    try {
                        if (attributeValue != null) {
                        	
                        	//check if the attribute is extended data
                            Integer paramId = new Integer(attributeName);
                            
                            //if we do not already have extended data attributes then create an array
                            if (order.get(ATTRIB_EXTENDED_DATA) == null) {
                                order.put(ATTRIB_EXTENDED_DATA, new Object[1]);
                            
    	                        //create the extended data attribute
    	                        HashMap extData = new HashMap();
    	                        extData.put(ATTRIB_PARAM_ID, paramId);
    	                        extData.put(ATTRIB_PARAM_VALUE, attributeValue);
    	                        
    	                        //add it to the array
    	                        Object[] newDataParams = C30HashMapUtils.addObjectToArray( (Object[]) order.get(ATTRIB_EXTENDED_DATA), extData);

    	                        //put the array into the order data.
    	                        order.put(ATTRIB_EXTENDED_DATA, newDataParams);
                            
                            } else {
                                C30HashMapUtils.removeDuplicatesFromArray(order, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

                                Object [] extendedData = (Object[]) order.get(ATTRIB_EXTENDED_DATA);
                                HashMap xdHashMap = new HashMap(((HashMap) extendedData[0]));
      
                                // set the extended data attributes
                                xdHashMap.put(ATTRIB_PARAM_ID, paramId);
                                xdHashMap.put(ATTRIB_PARAM_VALUE, attributeValue);

                                // add it uniquely to order
                                Object [] newDataParams = C30HashMapUtils.addUniqueHashMapToArray(extendedData, xdHashMap, ATTRIB_PARAM_ID);
                                order.put(ATTRIB_EXTENDED_DATA, newDataParams);
                            }

                        }                                       
                    } catch (NumberFormatException e) {
                        // not a valid param id
                    }
                }
            }
        }
    }

    /**
     * Method which gets the service orders of a particular service level.  This is a convenience method
     * since we need to always process the account level service orders first.
     * @param level true = service level, false = account level.
     * @return a collection of service orders.
     * @throws C30SDKInvalidConfigurationException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     */
    private C30SDKCollection getServiceOrderByLevel(Boolean level) throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException {
        C30SDKCollection serviceOrders = (C30SDKCollection) this.getFactory().createC30SDKObject(C30SDKCollection.class, new Object[0]);
        C30SDKObject serviceOrder = null;
        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
        	serviceOrder = this.getC30SDKObject(i);
        	if (serviceOrder.getAttributeValue(C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
        			serviceOrder.getAttributeValue(C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(level)) {
        		serviceOrders.addC30SDKObject(serviceOrder);
        	}
        }
        
        return serviceOrders;
    }
    
    
    /**
     * Method which gets the associated lwo requests that are not service-level service orders
     * @return a collection of service orders.
     * @throws C30SDKInvalidConfigurationException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     */
    private C30SDKCollection getNonServiceOrders()
        throws C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException
    {
        C30SDKCollection nonServiceOrders = (C30SDKCollection)getFactory().createC30SDKObject(com.cycle30.sdk.core.framework.C30SDKCollection.class, new Object[0]);
        C30SDKObject nonServiceOrder = null;
        for(int i = 0; i < getC30SDKObjectCount(); i++)
        {
            nonServiceOrder = getC30SDKObject(i);
            if(nonServiceOrder.getAttributeValue("IsServiceLevel", C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null || nonServiceOrder.getAttributeValue("IsServiceLevel", C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(false))
                nonServiceOrders.addC30SDKObject(nonServiceOrder);
        }

        return nonServiceOrders;
    }

    
    /**
     * Sets the internal account for the Kenan FX Order  based on the external id.  LWO allows client to specify either internal
     * or external ids.  The API TS requires internal id (account_no).  This method supports this functionality
     * @return C30SDKObject the returned account that was found.
     * @throws C30SDKException 
     */
    private C30SDKObject getAssociatedAccount() throws C30SDKException {
    	C30SDKObject account = null;
    	if (!this.isAttributeValueSet(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
            if (this.isAttributeValueSet(ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)
            		&& this.isAttributeValueSet(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {

            	account = this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);

                account.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account = account.process();
                if (account instanceof C30Account) {
                	this.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                	this.setAttributeValue(ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                }
            } else {
                String errorMsg = "The account internal id or external id and external id type must be set in order to create a Kenan/FX order";
                throw new C30SDKObjectException(errorMsg);
            }
        }
        return account;
    }
    
    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
