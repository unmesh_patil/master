/*
 * C30Payment.java
 *
 * Created on July 30, 2008, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.financial;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Random;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * The C30SDK payment is a pre-configured template for a Kenan/FX payment.
 * @author Tom Ansley
 */
public class C30Payment extends C30SDKObject implements Cloneable {

	private C30SDKObject account = null;
	protected Map payment = null;
	
	private static final String NOTE_USER_ID="Client";

	private static Logger log = Logger.getLogger(C30Payment.class);

	/**
	 * Creates a new instance of C30Payment.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Payment(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Payment(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30Payment.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Payment object.
	 * @return A copy of the C30Payment object
	 */
	public Object clone() {
		C30Payment copy = (C30Payment) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the payment.
	 * @return C30SDKObject 
	 * @throws C30SDKInvalidAttributeException 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	public C30SDKObject process() throws C30SDKInvalidAttributeException {
	    
		C30SDKObject sdkObject = null;
		
		try {
			setAttributesFromNameValuePairs();
			super.prevalidate();
			

			log.debug("Working on the Payment Object.....");
			
			//If Only one customer server present then directly make find call on to that customer server.
			if (C30SDKValueConstants.C30_KENAN_TOTAL_CUST_SERVERS.intValue() == 1) {
				if((isAttribute(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
						getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)){
					log.debug(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
					
					if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_PAYMENT_FIND))
						findPayments();
					if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_PAYMENT_CREATE))
						createPayment();
					if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_PAYMENT_CREATE_WITH_ACCT_NOTES))
						createPaymentWithAccountNotes();
					if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_PAYMENT_DELETE))
						deletePayment();
					if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_PAYMENT_REVERSAL))
						reversePayment();
				}

				// non-null payment structure indicates success create or find transaction
				if (this.payment != null) {
					this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				
				sdkObject = this;
				
			}
			else {
				// check to see if we need to explicitly get an account
				if (super.canServerIdCacheBeSetLocally())
					super.setAccountServerIdCache();
				else{
					if (this.account == null){
						account = getAccount();
						if(account instanceof C30SDKExceptionMessage)
							return account;
					} 
				}
				super.setAccountServerId();

				if  (isFindTransaction()) 
					findPayments();
				else     
					createPayment();

				// non-null payment structure indicates success create or find transaction
				if (this.payment != null)
					this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				sdkObject = this;

			}

		} 
		catch (ClassNotFoundException e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-051"),e,"INVALID-ATTRIB-051");
		}
		catch (NullPointerException e) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("LW-OBJ-023"),e,"LW-OBJ-023");
		}
		catch(MissingResourceException e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("LW-OBJ-023"),e,"LW-OBJ-023");
		}
		catch (Exception e) {
		    sdkObject = this.createExceptionMessage((C30SDKException)e);
		} 

		return sdkObject;
	}


	/**
	 * 
	 * @return boolean
	 * @throws C30SDKInvalidAttributeException
	 */
	private boolean isFindTransaction()throws C30SDKInvalidAttributeException  {   boolean retval = false; 
	if  ((this.isAttribute(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
			this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
			this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND) ||
			(  this.isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
					(Boolean)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT))   )) 
		return true;

	return retval;       

	}
	
	
	
	/**
	 * 
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKInvalidConfigurationException
	 * @throws C30SDKKenanFxCoreException
	 */
	private void findPayments()
	       throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException
	{

		HashMap paymentFilter = new HashMap();
		Map attrFilter = new HashMap();
		Map key = new HashMap();
		paymentFilter.put("Key", key);
		paymentFilter.put("Fetch", Boolean.TRUE);
		attrFilter = new HashMap();
		attrFilter.put("IsNull", Boolean.TRUE);
		paymentFilter.put("InactiveDt", attrFilter);
		attrFilter = new HashMap();
		attrFilter.put("Equal", getAttributeValue( C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		paymentFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, attrFilter);
		if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)    {
			attrFilter = new HashMap();
			attrFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			paymentFilter.put(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, attrFilter);
		}
		if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)    {
			attrFilter = new HashMap();
			attrFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			paymentFilter.put(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, attrFilter);
		}

		if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)    {
			attrFilter = new HashMap();
			attrFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			paymentFilter.put(C30SDKAttributeConstants.ATTRIB_TRANS_TYPE, attrFilter);
		}

		Map paymentRequest = new HashMap();

		paymentRequest.put("Payment", paymentFilter);
		Map callResponse = null;
		callResponse = queryData("PaymentFind", paymentRequest, getAccountServerId());
		int count = ((Integer)callResponse.get("Count")).intValue();
		if(count == 0)
		{
			setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			return;
		}
		log.debug("count of services: "+count);
		C30Payment cloneSdkObj = (C30Payment)super.clone();
		for (int i=0; i < count; i++)
		{
			HashMap aPayment = (HashMap)((Object[])(Object[])callResponse.get("PaymentList"))[i];               

			if (i == 0){
				this.payment = aPayment;
				this.populatePaymentOutput();
				this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			}
			else
			{
				C30Payment lwo = (C30Payment)cloneSdkObj.clone();
				lwo.payment = aPayment;
				lwo.populatePaymentOutput();
				// add to our list of peer objects
				lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				super.addC30SDKObject(lwo);
			}  
		}
	}
	
	

	/**
	 * 
	 * @return
	 * @throws C30SDKException
	 */
	private C30SDKObject getAccount() throws C30SDKException {
		log.debug("Starting C30Payment.getAccount");
		C30SDKObject theAccount = null;
		//try get account from object children
		for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
			if (this.getC30SDKObject(i) instanceof C30Account) {
				theAccount = this.getC30SDKObject(i);
			}
		}

		//if no account in children then get account from middleware
		if (theAccount == null) {
			theAccount = this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);
			theAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			theAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			theAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			theAccount = theAccount.process();
		}

		//if the processed account produced an exception.
		if (!(theAccount instanceof C30SDKExceptionMessage)) {
			this.setAccountLevelAttributes(theAccount);
		}
		log.debug("Finished C30Payment.getAccount");
		return theAccount;
	}



	/**
	 *      
	 * Method which sets all account data to be usee by the object.
	 * @param account
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKObjectException
	 */
	private void setAccountLevelAttributes(C30SDKObject theAccount)
	throws C30SDKInvalidAttributeException, C30SDKObjectException
	{

		if(theAccount instanceof C30Account)
			if (theAccount != null)
			{
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null)
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) 
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))     
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))       
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, theAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}
	}


	/**
	 * Creates the Kenan/FX payment using API-TS calls.
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void createPayment() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

		//---------------------------------
		// set input attributes into request object
		//---------------------------------
		Map aPayment = setPaymentAttributes();
		//----------------------------------------
		// Create the payment request and set the
		// payment and the effective date into it
		//----------------------------------------
		Map paymentRequest = new HashMap();
		paymentRequest.put("Payment", aPayment);

		//-----------------------------------------------
		// create the payment and then get the response.
		//-----------------------------------------------
		Map callResponse = null;
		callResponse = this.queryData("PaymentCreate", paymentRequest, getAccountServerId());            

		aPayment = (HashMap) callResponse.get("Payment");
		this.payment = aPayment;
		populatePaymentOutput();

	}
	
	

	/**
	 * Creates the Kenan/FX payment using API-TS calls.
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void createPaymentWithAccountNotes() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

		log.debug("Creating the payment with AccountNotes...");
		//---------------------------------
		// set input attributes into request object
		//---------------------------------
		Map aPayment = setPaymentAttributes();
		String freenotes = (String)aPayment.get("Note");
		//----------------------------------------
		// Create the payment request and set the
		// payment and the effective date into it
		//----------------------------------------
		Map paymentRequest = new HashMap();
		paymentRequest.put("Payment", aPayment);

		//-----------------------------------------------
		// create the payment and then get the response.
		//-----------------------------------------------
		Map callResponse = null;
		callResponse = this.queryData("PaymentCreate", paymentRequest, getAccountServerId());            

		aPayment = (HashMap) callResponse.get("Payment");
		aPayment.put("ExtCorrelationId",(String)((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get("ExtCorrelationId")).getValue());
		this.payment = aPayment;
		String noteText=(String)aPayment.get("Annotation");

		log.debug("FXVersion : "+((Double)this.getFactory().getAttributeValue("FXVersion")).toString());
		//Commenting the note creation because product doesn't support it.
		/*if (((Double)this.getFactory().getAPIVersion()).toString().equalsIgnoreCase(C30VersioningUtils.API_VER_2_0))
		{
			Integer accountno = (Integer)aPayment.get("AccountInternalId");
			createAccountNotes(noteText,accountno);
			if (freenotes != null)
			{
				createAccountNotes(freenotes,accountno);
			}			
		}
*/
		populatePaymentOutput();
	}

	
	/**
	 * 
	 * @return random note id
	 */
	public static String getRandomNoteId() {
		String dateformat = "yyMMddHHmmss";
		SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
		String d = sdf.format(new Date())+getRandomNumber(1000);
		System.out.print("NoteId date=" +d);
		//String ranNum = Double.parseDouble(d);
		return d;
	}
	
	
	
	private static Random randomGenNum = null;
	/**
	 * 
	 * @param range
	 * @return random int value
	 */
	public static int getRandomNumber(int range) {
		if (randomGenNum == null)
			randomGenNum = new Random();
		return randomGenNum.nextInt(range);	
	}
	
	
	/**
	 * 
	 * @param noteText
	 * @param AccountNo
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKObjectException
	 * @throws C30SDKInvalidConfigurationException
	 */
	private void createAccountNotes(String noteText, Integer AccountNo) throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException {

		// Populate all the objects from the GenericEnumerations using Generuc_enumeration_Ref and values.
		// Using Enumeration Find call.

		log.debug("creating Account Notes...");
		/*		C30SDKObject anAccountNote = null;


    if(anAccountNote == null)
    {
    	anAccountNote = this.getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30AccountNote.class, new Object[0]);
    	anAccountNote.setUser(this.getUser(), this.getUserContextofLWObject());
        if (AccountNo != null)
        	anAccountNote.setAttributeValue(Attributes.ATTRIB_ACCOUNT_INTERNAL_ID,AccountNo, Constants.ATTRIB_TYPE_INPUT);

    	anAccountNote.setAttributeValue(Attributes.ATTRIB_ACCT_NOTE_CHG_WHO,this.getUser().getUserId(), Constants.ATTRIB_TYPE_INPUT);
    	anAccountNote.setAttributeValue(Attributes.ATTRIB_ACCT_NOTE_NOTE_TEXT,noteText, Constants.ATTRIB_TYPE_INPUT);
    	anAccountNote.setAttributeValue(Attributes.ATTRIB_ACCT_NOTE_NOTE_SOURCE,1, Constants.ATTRIB_TYPE_INPUT);

    	anAccountNote = anAccountNote.process();
    }

		 */
		try
		{
			C30JDBCDataSource dataSource = C30SDKDataSourceUtils.createJDBCDataSource(C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);

			C30ExternalCallDef call = new C30ExternalCallDef("c30_Insert_Notes_sp");
			//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_field_filter");

			//call.addParam(new C30CustomParamDef("account_no", 2, 2000, 2));
			call.addParam(new C30CustomParamDef("v_note_id", 2, 2000, 1));
			call.addParam(new C30CustomParamDef("v_parent_code", 1, 2000, 1));
			call.addParam(new C30CustomParamDef("v_parent_id1", 1, 2000, 1));
			call.addParam(new C30CustomParamDef("v_parent_id2", 1, 2000, 1));
			call.addParam(new C30CustomParamDef("v_chg_who", 2, 2000, 1));
			call.addParam(new C30CustomParamDef("v_note_text", 2, 2000, 1));
			call.addParam(new C30CustomParamDef("v_type_id", 1, 2000, 1));
			call.addParam(new C30CustomParamDef("v_reason_id", 1, 2000, 1));
			call.addParam(new C30CustomParamDef("v_note_source", 1, 2000, 1));


			ArrayList paramValues = new ArrayList();
			//--------------------------------
			// set the input parameter values
			//--------------------------------

			paramValues.add(getRandomNoteId());
			paramValues.add(1);
			paramValues.add(AccountNo);
			paramValues.add(0);
			paramValues.add(NOTE_USER_ID);
			paramValues.add(noteText);
			paramValues.add(43);
			paramValues.add(7);
			paramValues.add(1);



			dataSource.queryData(call, paramValues, 2);


		}
		catch(Exception e)
		{
			log.error(e);
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-050"),e,"INVALID-ATTRIB-050");
		}

	}



	/**
	 * Creates the Kenan/FX payment using API-TS calls.
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void deletePayment() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

		//---------------------------------
		// set input attributes into request object
		//---------------------------------
		Map aPayment = setPaymentAttributes();
		//----------------------------------------
		// Create the payment request and set the
		// payment and the effective date into it
		//----------------------------------------
		Map paymentRequest = new HashMap();
		paymentRequest.put("Payment", aPayment);

		//-----------------------------------------------
		// Find the payment and then check wether to delete it or not
		//-----------------------------------------------
		Map callResponse = null;
		callResponse = this.queryData("PaymentGet", paymentRequest, getAccountServerId());            

		aPayment = (HashMap) callResponse.get("Payment");
		paymentRequest.put("Payment", aPayment);
		String inputExternalId = (String) ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get("AccountExternalId")).getValue();
		String externalId = (String)aPayment.get("AccountExternalId");
		
		if (!externalId.equalsIgnoreCase(inputExternalId))  {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-053"),"INVALID-ATTRIB-053");
		}
		
		//Check if it is currently a Billed OR UnBilled Payment status.
		Integer billRef     = (Integer)aPayment.get("BillRefNo");
		Integer origBillRef = (Integer)aPayment.get("OrigBillRefNo");
		Integer sourceType  = (Integer)aPayment.get("SourceType");
		
		/* 
                    Ensure that any debit payment made against invoice 0 without
                    any reference to a prior bmf credit (i.e. orig_bill_ref_no is NULL)
                    has been done using a valid refund source. These valid source type values for refunds
                    are as follows:
                    0 --   LBX
                    2 --   EFT
                    3 --   CPM
                    4 --   REFUND
                    This is to ensure that debit payments are not made against the
                    suspense account outside of core refund functionality.
		*/
		
		// Do payment reversal if already billed/invoiced
		if (billRef.intValue() !=0 ) {
                    Integer transType =  (Integer) ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get("TransType")).getValue();
                    ((Map)paymentRequest.get("Payment")).put("TransType",transType);
                    callResponse = this.queryData("PaymentReverse", paymentRequest, getAccountServerId());
		}

                // Do payment delete if original payment has billref != 0 (billed)
		else if (origBillRef !=0) {
			    
                    log.info("Deleting/Void an invoiced with payment info");
                    callResponse = this.queryData("PaymentDelete", paymentRequest, getAccountServerId());
				
		} 

                // Manual payments require special consideration due to API limitations/issues. 
		else if(sourceType ==1 && origBillRef==0) {    
		    
		    refundPayment(aPayment);
                } 		


	    this.payment = aPayment;
	    populatePaymentOutput();

	}
	
	
	
	
	/**
	 * Refund payment by directly calling core Kenan stored procedures (due to API-TS
	 * limitations) 
	 * 
	 * @param aPayment
	 */
        private void refundPayment(Map aPayment) throws C30SDKInvalidAttributeException {
            
            log.info("Performing a Refund Instead of PaymentDelete as the Payment is against suspense account.");
            log.info("Cannot delete the payment because of technical constraints in Kenan.");
         
             HashMap refundMap = new HashMap();
             refundMap.put("OrigSubmitterName",((String)aPayment.get("TransSubmitter")).trim().toLowerCase());
             refundMap.put("LastReviewedName", ((String)aPayment.get("TransSubmitter")).trim().toLowerCase());
         
             refundMap.put("AccountExternalId", (String)aPayment.get("AccountExternalId"));
             refundMap.put("AccountExternalIdType", (Integer)aPayment.get("AccountExternalIdType"));
             refundMap.put("AccountInternalId", (Integer)aPayment.get("AccountInternalId"));
             refundMap.put("RequestDate", new Date());
             refundMap.put("ActiveDate", new Date());
             refundMap.put("Amount", (BigInteger)aPayment.get("TransAmount"));
             refundMap.put("RefundType", (Integer)1);       

         
             try {
                 
                 C30JDBCDataSource dataSource = C30SDKDataSourceUtils.createJDBCDataSource(C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);

                 C30ExternalCallDef call = new C30ExternalCallDef("api_csr_refund_insert");

                 call.addParam(new C30CustomParamDef("v_account_no", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_orig_submitter_name", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_amount", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_status", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_lname", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_fname", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_company", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_address1", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_address2", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_address3", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_city", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_state", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_zip", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_country_code", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_acct_seg_flag", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_refund_reason_code", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_refund_type", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_open_item_id", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_alt_contact_id", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_alt_currency_code", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_request_date", 3, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_xa_mode", 1, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_supervisor_name", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("v_last_reviewed_name", 2, 2000, 1));
                 call.addParam(new C30CustomParamDef("api_csr_refund_insert_cv", 2, 2000, 2));
         

                 //--------------------------------
                 // set the input parameter values
                 //--------------------------------
                 ArrayList paramValues = new ArrayList();
                 
                 paramValues.add((Integer)aPayment.get("AccountInternalId"));
                 paramValues.add(((String)aPayment.get("TransSubmitter")).trim().toLowerCase());
                 paramValues.add((BigInteger)aPayment.get("TransAmount"));
                 paramValues.add(0);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(1);
                 paramValues.add(1);
                 paramValues.add(1);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);
                 paramValues.add(null);

                 Integer refundTrackingId =0;
                 Integer refundTrackingIdServ =0;
                 ResultSet rs = dataSource.queryDataWithRS(call, paramValues, 2);
                 
                 if (rs!= null)   {
                     rs.next();
                     refundTrackingId = (Integer) rs.getInt(1);
                     refundTrackingIdServ = (Integer) rs.getInt(2);
                     log.info("Refund Tracking Id : " + refundTrackingId);
                 }
                 
                 //Performing the refund approval.
                 log.info("Performing a Refund Approval");
                 refundMap.put("LastReviewedName", ((String)aPayment.get("TransSubmitter")).trim().toLowerCase());
                 
                 refundMap.put("AccountExternalId", (String)aPayment.get("AccountExternalId"));
                 refundMap.put("AccountExternalIdType", (Integer)aPayment.get("AccountExternalIdType"));
                 refundMap.put("AccountInternalId", (Integer)aPayment.get("AccountInternalId"));
                 refundMap.put("Amount", (BigInteger)aPayment.get("TransAmount"));
                 //refundMap.put("RefundType", (Integer)1);
                 
                 HashMap key = new HashMap();
                 key.put("TrackingId",refundTrackingId );
                 key.put("TrackingIdServ",refundTrackingIdServ );
                 refundMap.put("Key", key);
                 
                 dataSource = C30SDKDataSourceUtils.createJDBCDataSource(C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);

                 C30ExternalCallDef callRefundApprove = new C30ExternalCallDef("api_refund_approve");;

                 callRefundApprove.addParam(new C30CustomParamDef("v_account_no", 1, 2000, 1));
                 callRefundApprove.addParam(new C30CustomParamDef("v_user", 2, 2000, 1));
                 callRefundApprove.addParam(new C30CustomParamDef("v_supervisor", 2, 2000, 1));
                 callRefundApprove.addParam(new C30CustomParamDef("v_amount", 1, 2000, 1));
                 callRefundApprove.addParam(new C30CustomParamDef("v_refund_track_id", 1, 2000, 1));
                 callRefundApprove.addParam(new C30CustomParamDef("v_refund_track_id_serv", 1, 2000, 1));
                 callRefundApprove.addParam(new C30CustomParamDef("v_xa_mode", 1, 2000, 1));

                 
                 ArrayList paramValues1 = new ArrayList();
                 
                 //--------------------------------
                 // set the input parameter values
                 //--------------------------------

                 paramValues1.add((Integer)aPayment.get("AccountInternalId"));
                 paramValues1.add(((String)aPayment.get("TransSubmitter")).trim().toLowerCase());
                 paramValues1.add(((String)aPayment.get("TransSubmitter")).trim().toLowerCase());
                 paramValues1.add((BigInteger)aPayment.get("TransAmount"));
                 paramValues1.add(refundTrackingId);
                 paramValues1.add(refundTrackingIdServ);
                 paramValues1.add(1);
         
                 dataSource.executeCallableStatement(callRefundApprove, paramValues1, 2);
                 
             } 
             
             catch(Exception e){
                 log.error(e);
                 throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-058"),e,"INVALID-ATTRIB-058");
             } 

	}
        
        
	
	
	/**
	 * Creates the Kenan/FX payment using API-TS calls.
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void reversePayment() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {

		//---------------------------------
		// set input attributes into request object
		//---------------------------------
		Map aPayment = setPaymentAttributes();
		//----------------------------------------
		// Create the payment request and set the
		// payment and the effective date into it
		//----------------------------------------
		Map paymentRequest = new HashMap();
		paymentRequest.put("Payment", aPayment);

		//-----------------------------------------------
		// create the payment and then get the response.
		//-----------------------------------------------
		Map callResponse = null;
		callResponse = this.queryData("PaymentReverse", paymentRequest, getAccountServerId());            

		aPayment = (HashMap) callResponse.get("Payment");
		this.payment = aPayment;
		populatePaymentOutput();

	}
	
	
	/**
	 * Method to ensure all attributes that are visible to the calling application are populated.  This 
	 * method is usually called just before the object returns objects using the process() method.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */
	private void populatePaymentOutput() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException {


		log.debug("Populating the payment output object");
		for  ( Iterator it=this.payment.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
			Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
			String key = (String)entry.getKey (  ) ; 
			Object value = this.payment.get(key) ;
			//key = C30StringUtils.SqlToKenanName(key);

			if (key.equalsIgnoreCase("KEY"))  {    
				HashMap keyValue = (HashMap)entry.getValue (  ) ; 
				setAttributeValue("TrackingId", keyValue.get("TrackingId"), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				setAttributeValue("TrackingIdServ", keyValue.get("TrackingIdServ"), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}

			if (value != null) {
				if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) {
				    setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				    log.debug(  ( String ) key+" " +  value +"   SET") ;                            
				}
				else if (value != null) {
					 log.debug(  ( String ) key+" " +  value ) ; 
				}

			}
		}
		/*
        this.setAttributeValue(ATTRIB_TRACKING_ID, ((Integer)((HashMap) payment.get("Key")).get(ATTRIB_TRACKING_ID)).toString(), Constants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_TRACKING_ID_SERV, ((Integer)((HashMap) payment.get("Key")).get(ATTRIB_TRACKING_ID_SERV)).toString(), Constants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_TRANS_DATE, ((Date) payment.get(ATTRIB_TRANS_DATE)).toString(), Constants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_TRANS_TYPE, ((Integer) payment.get(ATTRIB_TRANS_TYPE)).toString(), Constants.ATTRIB_TYPE_INPUT);

        this.setAttributeValue(Constants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(Constants.ATTRIB_ACCOUNT_EXTERNAL_ID, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(Constants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(Constants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(Constants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(Constants.ATTRIB_ACCOUNT_INTERNAL_ID, Constants.ATTRIB_TYPE_OUTPUT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ACTION_CODE, payment.get(ATTRIB_ACTION_CODE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ANNOTATION, payment.get(ATTRIB_ANNOTATION), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ARCH_FLAG, payment.get(ATTRIB_ARCH_FLAG), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BATCH_ID, payment.get(ATTRIB_BATCH_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BATCH_ID_SERV, payment.get(ATTRIB_BATCH_ID_SERV), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BILL_ORDER_NUMBER, payment.get(ATTRIB_BILL_ORDER_NUMBER), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BILL_REF_NO, payment.get(ATTRIB_BILL_REF_NO), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_BILL_REF_RESETS, payment.get(ATTRIB_BILL_REF_RESETS), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CHG_DATE, payment.get(ATTRIB_CHG_DATE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CURRENCY_CODE, payment.get(ATTRIB_CURRENCY_CODE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_DISCOUNT_ID, payment.get(ATTRIB_DISCOUNT_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_DISTRIBUTED_AMOUNT, payment.get(ATTRIB_DISTRIBUTED_AMOUNT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_DISTRIBUTED_GL_AMOUNT, payment.get(ATTRIB_DISTRIBUTED_GL_AMOUNT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_EXTERNAL_AMOUNT, payment.get(ATTRIB_EXTERNAL_AMOUNT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_EXTERNAL_CURRENCY, payment.get(ATTRIB_EXTERNAL_CURRENCY), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_FILE_ID, payment.get(ATTRIB_FILE_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_GL_AMOUNT, payment.get(ATTRIB_GL_AMOUNT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MANUAL_CCAUTH_CODE, payment.get(ATTRIB_MANUAL_CCAUTH_CODE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MANUAL_CCAUTH_DATE, payment.get(ATTRIB_MANUAL_CCAUTH_DATE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MICR_BANK_ID, payment.get(ATTRIB_MICR_BANK_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MICR_CHECK_NUM, payment.get(ATTRIB_MICR_CHECK_NUM), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MICR_DBA_NUM, payment.get(ATTRIB_MICR_DBA_NUM), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_NO_BILL, payment.get(ATTRIB_NO_BILL), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_OPEN_ITEM_ID, payment.get(ATTRIB_OPEN_ITEM_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ORIG_BILL_REF_NO, payment.get(ATTRIB_ORIG_BILL_REF_NO), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ORIG_BILL_REF_RESETS, payment.get(ATTRIB_ORIG_BILL_REF_RESETS), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ORIG_TRACKING_ID, payment.get(ATTRIB_ORIG_TRACKING_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ORIG_TRACKING_ID_SERV, payment.get(ATTRIB_ORIG_TRACKING_ID_SERV), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_POST_DATE, payment.get(ATTRIB_POST_DATE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_RESPONSE_CODE, payment.get(ATTRIB_RESPONSE_CODE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SOURCE_ID, payment.get(ATTRIB_SOURCE_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SOURCE_ID_SERV, payment.get(ATTRIB_SOURCE_ID_SERV), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SOURCE_TYPE, payment.get(ATTRIB_SOURCE_TYPE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TAX_TYPE_CODE, payment.get(ATTRIB_TAX_TYPE_CODE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRACKING_ID, this.getAttributeValue(ATTRIB_TRACKING_ID, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRACKING_ID_SERV, this.getAttributeValue(ATTRIB_TRACKING_ID_SERV, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_AMOUNT, payment.get(ATTRIB_TRANS_AMOUNT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_CATEGORY, payment.get(ATTRIB_TRANS_CATEGORY), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_DATE, this.getAttributeValue(ATTRIB_TRANS_DATE, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_SOURCE, payment.get(ATTRIB_TRANS_SOURCE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_SUBMITTER, payment.get(ATTRIB_TRANS_SUBMITTER), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_TYPE, this.getAttributeValue(ATTRIB_TRANS_TYPE, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_TYPE_CREDIT, this.getAttributeValue(ATTRIB_TRANS_TYPE_CREDIT, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_TRANS_TYPE_DEBIT, this.getAttributeValue(ATTRIB_TRANS_TYPE_DEBIT, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CLEAR_ACCOUNT_BALANCE, this.getAttributeValue(ATTRIB_CLEAR_ACCOUNT_BALANCE, Constants.ATTRIB_TYPE_INPUT), Constants.ATTRIB_TYPE_OUTPUT);

        if ( this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {    	
        	this.setAttributeValue(ATTRIB_CCARD_ACCOUNT, payment.get(ATTRIB_CCARD_ACCOUNT), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CCARD_CARRIER, payment.get(ATTRIB_CCARD_CARRIER), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CCARD_EXPIRE, payment.get(ATTRIB_CCARD_EXPIRE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CCARD_ID_SERV, payment.get(ATTRIB_CCARD_ID_SERV), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CCARD_OWNR_NAME, payment.get(ATTRIB_CCARD_OWNR_NAME), Constants.ATTRIB_TYPE_OUTPUT);
        }
        if ( this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	this.setAttributeValue(ATTRIB_ALT_BANK_ACC_NUM, payment.get(ATTRIB_ALT_BANK_ACC_NUM), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_AVS_ADDRESS_ID, payment.get(ATTRIB_AVS_ADDRESS_ID), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_AVS_RESPONSE_CODE, payment.get(ATTRIB_AVS_RESPONSE_CODE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_BANK_ACCOUNT_TYPE, payment.get(ATTRIB_BANK_ACCOUNT_TYPE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_BANK_AGENCY_CODE, payment.get(ATTRIB_BANK_AGENCY_CODE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_BANK_AGENCY_NAME, payment.get(ATTRIB_BANK_AGENCY_NAME), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_BANK_CODE, payment.get(ATTRIB_BANK_CODE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_BILL_COMPANY_TAX_ID, payment.get(ATTRIB_BILL_COMPANY_TAX_ID), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_BRANCH_CODE, payment.get(ATTRIB_BRANCH_CODE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_BRANCH_NAME, payment.get(ATTRIB_BRANCH_NAME), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CARD_ACCOUNT, payment.get(ATTRIB_CARD_ACCOUNT), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CARD_CARRIER, payment.get(ATTRIB_CARD_CARRIER), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CARD_EXPIRE, payment.get(ATTRIB_CARD_EXPIRE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CLEARING_HOUSE_ID, payment.get(ATTRIB_CLEARING_HOUSE_ID), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CR_NOTE_BILL_REF_NO, payment.get(ATTRIB_CR_NOTE_BILL_REF_NO), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CR_NOTE_BILL_REF_RESETS, payment.get(ATTRIB_CR_NOTE_BILL_REF_RESETS), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BANK_ACC_NUM, payment.get(ATTRIB_CUST_BANK_ACC_NUM), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BANK_ACCT_TYPE, payment.get(ATTRIB_CUST_BANK_ACCT_TYPE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BANK_SORT_CODE, payment.get(ATTRIB_CUST_BANK_SORT_CODE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BILL_ADDRESS, payment.get(ATTRIB_CUST_BILL_ADDRESS), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BILL_CITY, payment.get(ATTRIB_CUST_BILL_CITY), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BILL_COUNTRY_CODE, payment.get(ATTRIB_CUST_BILL_COUNTRY_CODE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BILL_STATE, payment.get(ATTRIB_CUST_BILL_STATE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_COMPANY_NAME, payment.get(ATTRIB_CUST_COMPANY_NAME), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_BILL_ZIP, payment.get(ATTRIB_CUST_BILL_ZIP), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_EMAIL, payment.get(ATTRIB_CUST_EMAIL), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CUST_PHONE, payment.get(ATTRIB_CUST_PHONE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_CYCLICAL_BILL_USED, payment.get(ATTRIB_CYCLICAL_BILL_USED), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_DRIVER_LICENSE_NUM, payment.get(ATTRIB_DRIVER_LICENSE_NUM), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_DRIVER_LICENSE_STATE, payment.get(ATTRIB_DRIVER_LICENSE_STATE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_EPG_TRANSACTION_ID, payment.get(ATTRIB_EPG_TRANSACTION_ID), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_INACTIVE_DATE, payment.get(ATTRIB_INACTIVE_DATE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_IS_CURRENT, payment.get(ATTRIB_IS_CURRENT), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_IS_DEFAULT, payment.get(ATTRIB_IS_DEFAULT), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_IS_TEMPORARY, payment.get(ATTRIB_IS_TEMPORARY), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_NEW_CUST_BANK_SORT_CODE, payment.get(ATTRIB_NEW_CUST_BANK_SORT_CODE), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_NON_REALTIME_ONLY, payment.get(ATTRIB_NON_REALTIME_ONLY), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_OWNR_FNAME, payment.get(ATTRIB_OWNR_FNAME), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_OWNR_LNAME, payment.get(ATTRIB_OWNR_LNAME), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_PAYMENT_PROFILE_ID, payment.get(ATTRIB_PAYMENT_PROFILE_ID), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_PAY_METHOD, payment.get(ATTRIB_PAY_METHOD), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_REALTIME_INDICATOR, payment.get(ATTRIB_REALTIME_INDICATOR), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_STATUS, payment.get(ATTRIB_STATUS), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_TRANS_FLAG, payment.get(ATTRIB_TRANS_FLAG), Constants.ATTRIB_TYPE_OUTPUT);
        }
		 */
	}

	
	
	/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */    
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException { 

		String acctExternalId = null;
		Integer acctExternalIdType = null;
		Integer acctInternalId = null;
		Integer accountServerId = null;

		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctExternalId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctExternalIdType = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			acctInternalId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 


		if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
			accountServerId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 


		if (((acctExternalId == null || acctExternalIdType == null) && acctInternalId == null)  && accountServerId == null ) {

			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-020"),"INVALID-ATTRIB-020");
		}

		// validate values for create payment or clear balance transaction    
		if  (! isFindTransaction()) {
			super.validateAttributes();

			// for create transactions must have Account External id and Account Ext Id Type
			if (acctExternalId == null || acctExternalIdType == null){
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-020"),"INVALID-ATTRIB-020");
			}

			//check to ensure either the payment amount is set or the payment is set to clear the account balance.
			if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_AMOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null
					&& !((Boolean) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_CLEAR_ACCOUNT_BALANCE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-021"),"INVALID-ATTRIB-021");
			}
			if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_AMOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() != null &&
					((BigInteger) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_AMOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(BigInteger.ZERO)) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-022"),"INVALID-ATTRIB-022");
			}


		}
		else  {// is a Find transaction, check billRefNo and Resets dependencies
			if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null &&
					this.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() != null ){

				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-023"),"INVALID-ATTRIB-023");
			}
			if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() != null &&
					this.getAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null ){
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-023"),"INVALID-ATTRIB-023");
			}
		}

	}

	
	
	/**
	 * Initilize attributes for the Kenan FX payment that is about to be created through the API TS.
	 * @return Map The newly created payments hashmap that will be used to make the API-TS call.
	 * @throws C30SDKInvalidAttributeException exception thrown if there was an error setting any attributes.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidConfigurationException 
	 */
	private Map setPaymentAttributes() throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException {
		Map aPayment = new HashMap();
		String checkNumber="";
		String extTransactionId="";
		String extCorrelationId="";
		String locationId="";
		String agentId="";
		String tenderType="";


		//-----------------------------------------------
		// set the payment attributes
		//-----------------------------------------------
		Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
		while (iterator.hasNext()) {
			String attributeName = (String) iterator.next();
			Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

			if (attributeValue != null) {
				log.debug("Attribute Name: "+attributeName);
				if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_NO)) {
					aPayment.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_RESETS)) {
					aPayment.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID)) {
					aPayment.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE)) {
					aPayment.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_TRANS_AMOUNT)) {
					aPayment.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_TRANS_DATE)) {
					aPayment.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ANNOTATION)) {
					aPayment.put(attributeName, attributeValue);
					
					
				}else if (attributeName.equalsIgnoreCase("TenderType")) {
					log.debug("Tender Type supplied" + attributeValue);
					tenderType=((String)attributeValue).toUpperCase();
					if(this.getFactory().getPaymentObjectConfig().get(tenderType) == null)
					{
						throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-048"),"INVALID-ATTRIB-048");
					}else
					{
						aPayment.put("TransType",new Integer((String)this.getFactory().getPaymentObjectConfig().get(tenderType)));
					}

				}else if (attributeName.equals("MicrCheckNum")) {
					aPayment.put(attributeName, attributeValue);
					checkNumber=(String)attributeValue;
				}else if (attributeName.equals("ExtTransactionId")) {
					aPayment.put(attributeName, attributeValue);
					extTransactionId=(String)attributeValue;
				}else if (attributeName.equals("ExtCorrelationId")) {
					aPayment.put(attributeName, attributeValue);
					extCorrelationId=(String)attributeValue;
				}else if (attributeName.equals("LocationId")) {
					aPayment.put(attributeName, attributeValue);
					locationId=(String)attributeValue;
				}else if (attributeName.equals("AgentId")) {
					aPayment.put(attributeName, attributeValue);
					agentId=(String)attributeValue;
				}else if (attributeName.equals("Note")) {
					aPayment.put(attributeName, attributeValue);
				}
			}
		}
		String annotation  = getAnnotation(tenderType, checkNumber, extTransactionId, extCorrelationId, locationId, agentId);
		aPayment.put("Annotation", annotation);
		log.debug("Setting other attributes.");
		//set the date of the transaction if not set manually
		if (((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_TRANS_DATE)).getValue() == null) {
			aPayment.put(C30SDKAttributeConstants.ATTRIB_TRANS_DATE, new Date());
		}

		/*		//if we are clearing the account balance 
		if ( this.isAttribute(ATTRIB_CLEAR_ACCOUNT_BALANCE, Constants.ATTRIB_TYPE_INPUT) &&
				this.getAttributeValue(ATTRIB_CLEAR_ACCOUNT_BALANCE, Constants.ATTRIB_TYPE_INPUT) != null &&
				((Boolean) this.getAttributeValue(ATTRIB_CLEAR_ACCOUNT_BALANCE, Constants.ATTRIB_TYPE_INPUT)).booleanValue() ) {   
			//get the account balance
			log.debug("Getting account balance ...");
			BigInteger balance = getAccountBalance();
			this.setAttributeValue(ATTRIB_TRANS_AMOUNT, balance.toString(), Constants.ATTRIB_TYPE_INPUT);
			aPayment.put(ATTRIB_TRANS_AMOUNT, balance);

		} 
		 */
		aPayment.put(C30SDKAttributeConstants.ATTRIB_TRANS_AMOUNT, ((BigInteger) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_AMOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)));
		// if balance is positive then use credit payment type, 
		/*	if ( ((BigInteger) this.getAttributeValue(ATTRIB_TRANS_AMOUNT, Constants.ATTRIB_TYPE_INPUT)).compareTo(BigInteger.ZERO) > 0) {
			if (this.getAttributeValue(ATTRIB_TRANS_TYPE_CREDIT, Constants.ATTRIB_TYPE_INPUT) == null) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-024"),"INVALID-ATTRIB-024");
			}
			//aPayment.put(ATTRIB_TRANS_TYPE, this.getAttributeValue(ATTRIB_TRANS_TYPE_CREDIT, Constants.ATTRIB_TYPE_INPUT));

			// otherwise use debit payment type and set amount to positive.
		} else if ( ((BigInteger) this.getAttributeValue(ATTRIB_TRANS_AMOUNT, Constants.ATTRIB_TYPE_INPUT)).compareTo(BigInteger.ZERO) <= 0) {
			if (this.getAttributeValue(ATTRIB_TRANS_TYPE_DEBIT, Constants.ATTRIB_TYPE_INPUT) == null) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-025"),"INVALID-ATTRIB-025");
			}
			aPayment.put(ATTRIB_TRANS_AMOUNT, ((BigInteger) this.getAttributeValue(ATTRIB_TRANS_AMOUNT, Constants.ATTRIB_TYPE_INPUT)).negate());
		//	aPayment.put(ATTRIB_TRANS_TYPE, this.getAttributeValue(ATTRIB_TRANS_TYPE_DEBIT, Constants.ATTRIB_TYPE_INPUT));

		}
		 */	
		
		
		if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_PAYMENT_DELETE))  {
			//Make sure you check the Tracking  Id and Tracking id Serv for this.
			Map deletePayment = new HashMap();
			Map keydata = new HashMap();
			
			if (getAttributeValue("TrackingId", C30SDKValueConstants.ATTRIB_TYPE_INPUT)==null) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-049"),"INVALID-ATTRIB-049");
			}
			else {
				keydata.put("TrackingId",(Integer)getAttributeValue("TrackingId", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			}
			if (getAttributeValue("TrackingIdServ", C30SDKValueConstants.ATTRIB_TYPE_INPUT)==null) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-049"),"INVALID-ATTRIB-049");
			}
			else {
				keydata.put("TrackingIdServ",(Integer)getAttributeValue("TrackingIdServ", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			}
			
			deletePayment.put("Key", keydata);
			return deletePayment;
			
		}
		
		if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_PAYMENT_REVERSAL)) {
			//Make sure you check the Tracking  Id and Tracking id Serv for this.
			Map keydata = new HashMap();
			if (getAttributeValue("TrackingId", C30SDKValueConstants.ATTRIB_TYPE_INPUT)==null) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-049"),"INVALID-ATTRIB-049");
			}
			else {
				keydata.put("TrackingId",(Integer)getAttributeValue("TrackingId", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			}
			if (getAttributeValue("TrackingIdServ", C30SDKValueConstants.ATTRIB_TYPE_INPUT)==null) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-049"),"INVALID-ATTRIB-049");
			}
			else {
				keydata.put("TrackingIdServ",(Integer)getAttributeValue("TrackingIdServ", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			}
			
			aPayment.put("Key", keydata);

		}
		return aPayment;
	}

	
	/**
	 * 
	 * @param tenderType
	 * @param checkNumber
	 * @param extTransactionId
	 * @param extCorrelationId
	 * @param locationId
	 * @param agentId
	 * @return
	 */
	private String getAnnotation(String tenderType, String checkNumber,
			String extTransactionId, String extCorrelationId,
			String locationId, String agentId) {
		// TODO Auto-generated method stub

		String annotation = "Payment; ";
		if (extTransactionId != null ) annotation=annotation+"Trans Num: "+extTransactionId+"; ";
		if (locationId != null ) annotation=annotation+"Store: "+locationId+";Deposit Date: "+new Date()+"; ";
		if (tenderType != null ) annotation=annotation+"Tender: "+tenderType+"; ";
		if (checkNumber != null ) annotation=annotation+"CheckNumber: "+checkNumber+"; ";
		if (agentId != null ) annotation=annotation+"Agent Id: "+agentId+"; ";
		if (extCorrelationId != null ) annotation=annotation+"Reference number: "+extCorrelationId;

		return annotation;
	}

	/**
	 * Internal method to get the outstanding account balance.  This method is 
	 * used when the account is to have their entire balance cleared.
	 * @return the account balance
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKKenanFxCoreException 
	 */
	private BigInteger getAccountBalance() throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKKenanFxCoreException {

		//Create a HashMap to contain the account filter info
		HashMap accountMap = new HashMap();

		//create the key hashmap
		Map key = new HashMap();
		accountMap.put("Key", key);

		//put the ID into the key
		key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

		//make the call
		Map callResponse = this.queryData("Account", "BalanceSummary", accountMap, getAccountServerId());

		return  (BigInteger) callResponse.get("SumBalance");

	}

	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	protected void loadCache() {

	}

}
