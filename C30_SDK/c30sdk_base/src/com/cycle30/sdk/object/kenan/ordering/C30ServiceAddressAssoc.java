/*
 * C30Service.java
 *
 * Created on December 8, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Wrapper class encapsulating a Kenan/FX Service Address Association object and its corresponding order item.
 * @author Tom Ansley
 */
public class C30ServiceAddressAssoc extends C30OrderItem {

    private static Logger log = Logger.getLogger(C30ServiceAddressAssoc.class);

    public static final String ATTRIB_ACCOUNT_INTERNAL_ID = "AccountInternalId";
    public static final String ATTRIB_ACTIVE_DT = "ActiveDt";
    public static final String ATTRIB_ADDRESS_CATEGORY_ID = "AddressCategoryId";
    public static final String ATTRIB_ADDRESS_ID = "AddressId";
    public static final String ATTRIB_ASSOCIATION_STATUS = "AssociationStatus";
    public static final String ATTRIB_EMF_CONFIG_ID = "EmfConfigId";
    public static final String ATTRIB_INACTIVE_DT = "InactiveDt";
    public static final String ATTRIB_SERVICE_ADDRESS_ASSOC_ID = "ServiceAddressAssocId";
    public static final String ATTRIB_SERVICE_INTERNAL_ID = "ServiceInternalId";
    public static final String ATTRIB_SERVICE_INTERNAL_ID_RESETS = "ServiceInternalIdResets";
    
    public static final Integer EXCEPTION_INVALID_ITEM_ACTION_ID = new Integer(21000);
    public static final Integer EXCEPTION_NO_ACTIVE_SERVICE_FOUND = new Integer(21001);
    public static final Integer EXCEPTION_MULTIPLE_ACTIVE_SERVICE_FOUND = new Integer(21002);
    public static final Integer EXCEPTION_INVALID_ASSOCIATION_STATUS = new Integer(21003);
    
    public static final Integer ASSOC_STATUS_PENDING = new Integer(1);
    public static final Integer ASSOC_STATUS_CURRENT = new Integer(2);
    public static final Integer ASSOC_STATUS_OLD = new Integer(3);
    public static final Integer ASSOC_STATUS_CANCELLED = new Integer(4);
    
    public static final Integer ADDR_CATEGORY_SERVICE = new Integer(1);
    public static final Integer ADDR_CATEGORY_B_END = new Integer(2);
    public static final Integer ADDR_CATEGORY_SHIPPING = new Integer(3);
    
    /**
     * Creates a new instance of C30ServiceAddressAssoc.  This order item and object is only available through
     * the framework.  It is used in conjunction with the C30Service object when moving services.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ServiceAddressAssoc(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * Creates a new instance of C30ServiceAddressAssoc.  This order item and object is only available through
     * the framework.  It is used in conjunction with the C30Service object when moving services.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ServiceAddressAssoc(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30Service.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Address Association Order Item.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is invalid.
     */
    @Override
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException{

    	super.initializeAttributes();

        //=================
        // Input Attributes
        //=================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_CATEGORY_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ASSOCIATION_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EMF_CONFIG_ID,Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_ADDRESS_ASSOC_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //===================
        // Output Attributes
        //===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_CATEGORY_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ADDRESS_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ASSOCIATION_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EMF_CONFIG_ID,Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_ADDRESS_ASSOC_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        C30SDKAttribute attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_ADDRESS_ASSOC_MEMBER_TYPE);
    }

    /**
     * Validates the order item.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is invalid.
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected final void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        
    	super.validateAttributes();
        
    	if (!((Integer) super.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID) &&
            !((Integer) super.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(C30OrderItem.ORD_ITEM_CHANGE_ITEM_ACTION_ID)){

            String error = "Invalid item action id: (" + (super.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) +
                    ").  Item action id 10 or 20 must be used for a Service Address Association";
            throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-037"),"INVALID-ATTRIB-037");
        }
    	
    	if (!((Integer) this.getAttributeValue(ATTRIB_ASSOCIATION_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(ASSOC_STATUS_PENDING) &&
                !((Integer) this.getAttributeValue(ATTRIB_ASSOCIATION_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(ASSOC_STATUS_CURRENT) &&
                !((Integer) this.getAttributeValue(ATTRIB_ASSOCIATION_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(ASSOC_STATUS_OLD) &&
                !((Integer) this.getAttributeValue(ATTRIB_ASSOCIATION_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).equals(ASSOC_STATUS_CANCELLED)){

                String error = "Invalid association status: (" + (this.getAttributeValue(ATTRIB_ASSOCIATION_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) +
                        ").  Association status must be 1 (Pending), 2 (Current), 3 (Old), 4 (Cancelled) for a Service Address Association";
                throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-038"),"INVALID-ATTRIB-038");
            }
        	
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    @Override
	public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);
    }

    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Integer)
     */
    @Override
	public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
    	super.setAttributeValue(attributeName, attributeValue, type);

    	//if we are setting the association status
    	if (attributeName.equals(ATTRIB_ITEM_ACTION_ID)) {
    		if (attributeValue.toString().equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID.toString())) {
    			initializeServiceAddressAssocCreateAttributes();
    		}
    	}
    }

    /**
     * Method which sets specific attributes of the address association being created.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is incorrectly set.
     */
    private void initializeServiceAddressAssocCreateAttributes() throws C30SDKInvalidAttributeException {
    }
    
    /** (non-Javadoc).
     * @return C30SDKObject the returned service that was processed.
     * @throws C30SDKKenanFxCoreException 
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#process()
     */
    @Override
	public final C30SDKObject process() throws C30SDKException {
        C30SDKObject lwo = null;

        String error = "Error processing service address association:\n\tLW Order Item Type: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        validateAttributes();
        
        try {
    		lwo = super.process();
    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
    		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}

    	return lwo;

    }

    /**
     * Method to create the service itself.
     * @throws C30SDKObjectConnectionException if there was a connection error while making middleware requests.
     * @throws C30SDKInvalidAttributeException if an invalid attribute exists.
     * @throws C30SDKObjectException thrown if the service could not be created.
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected final C30SDKObject createFXObject() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	C30SDKObject lwo = null;
    	
    	Map assoc = new HashMap();
    	
        // Set the overrideable attributes and create the association
        this.setFXServiceAddressAssocAttributes(assoc);

        Map callResponse = this.queryData("ServiceAddressAssoc", "Create", assoc, getAccountServerId());
        assoc = (HashMap) callResponse.get("ServiceAddressAssoc");
        populateServiceAddressAssociation(assoc);
        
        lwo = this;
        
        return lwo;
    }

    /**
     * Method which takes the returned hashmap from the API-TS call and populates the lightweight object.
     * @throws C30SDKInvalidAttributeException thrown if an invalid attribute exists.
     */
    private void populateServiceAddressAssociation(Map assoc) throws C30SDKInvalidAttributeException {
    	this.setAttributeValue(ATTRIB_SERVICE_ADDRESS_ASSOC_ID, ((HashMap) assoc.get("Key")).get(ATTRIB_SERVICE_ADDRESS_ASSOC_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SERVICE_ADDRESS_ASSOC_ID, ((HashMap) assoc.get("Key")).get(ATTRIB_SERVICE_ADDRESS_ASSOC_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) assoc.get("Key")).get(ATTRIB_SERVICE_ADDRESS_ASSOC_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) assoc.get("Key")).get(ATTRIB_SERVICE_ADDRESS_ASSOC_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, assoc.get(ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ACTIVE_DT, assoc.get(ATTRIB_ACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ADDRESS_CATEGORY_ID, assoc.get(ATTRIB_ADDRESS_CATEGORY_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ADDRESS_ID, assoc.get(ATTRIB_ADDRESS_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ASSOCIATION_STATUS, assoc.get(ATTRIB_ASSOCIATION_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_EMF_CONFIG_ID,assoc.get(ATTRIB_EMF_CONFIG_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_INACTIVE_DT, assoc.get(ATTRIB_INACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SERVICE_ADDRESS_ASSOC_ID, assoc.get(ATTRIB_SERVICE_ADDRESS_ASSOC_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, assoc.get(ATTRIB_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, assoc.get(ATTRIB_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_MEMBER_ID, assoc.get(ATTRIB_ADDRESS_CATEGORY_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, new Integer(0), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_INST_ID, assoc.get(ATTRIB_ADDRESS_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    }

    /**
     * Method to set the association attribute information.
     * @param assocData the map holding the association data which will be passed into the request.
     * @throws C30SDKInvalidAttributeException if an invalid attribute exists.
     */
    private void setFXServiceAddressAssocAttributes(final Map assocData) throws C30SDKInvalidAttributeException {

    	assocData.put(ATTRIB_SERVICE_INTERNAL_ID, ((C30Service) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        assocData.put(ATTRIB_SERVICE_INTERNAL_ID_RESETS, ((C30Service) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        if (this.getAttributeValue(ATTRIB_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
            assocData.put(ATTRIB_ACTIVE_DT, this.getAttributeValue(ATTRIB_ACTIVE_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        } else {
            assocData.put(ATTRIB_ACTIVE_DT, new Date());
        }

        assocData.put(ATTRIB_EMF_CONFIG_ID, this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        assocData.put(ATTRIB_ASSOCIATION_STATUS, ASSOC_STATUS_PENDING);

        //----------------------------------------------------------------------
        // Set the FX fields based on LWO attributes that have been configured
        // or explicitly set
        //----------------------------------------------------------------------
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if (!attributeName.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT) &&
            		!attributeName.equals(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE) &&
            		!attributeName.equals(C30SDKAttributeConstants.ATTRIB_PERSIST_REQUEST_DATE) &&
            		attributeValue != null) {
            	assocData.put(attributeName, attributeValue);
            }
        }
    }

    /**
     * LW Services do not currently support child order items.  This is a placeholder
     * for potential future expansion of the LWO framework.
     * @return any child order items associated to the service.
     */
    @Override
	protected final C30SDKObject createChildOrderItems() { return null; }

    /**
     * Method which gets any data to be cached for further use by the object.
     * @throws C30SDKCacheException if there was an error while trying to populate the service cache.
     */
    @Override
	protected void loadCache() throws C30SDKCacheException {
    	log.debug("Starting C30ServiceAddressAssoc.loadCache");
    	log.debug("Finished C30ServiceAddressAssoc.loadCache");
    }

}

