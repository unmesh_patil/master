/*
 * C30ServiceExternalId.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX External Id object and its corresponding order item..
 * @author Joe Morales
 */
public class C30ServiceExternalId extends C30OrderItem {

    private static Logger log = Logger.getLogger(C30ServiceExternalId.class);

    public static final String ATTRIB_ACTIVE_DATE = "ActiveDate";
    public static final String ATTRIB_CREATE_DATE = "CreateDate";
    public static final String ATTRIB_INACTIVE_DATE = "InactiveDate";
    public static final String ATTRIB_INTENDED_VIEW_EFFECTIVE_DT = "IntendedViewEffectiveDt";
    public static final String ATTRIB_IS_CURRENT = "IsCurrent";
    public static final String ATTRIB_IS_FROM_INVENTORY = "IsFromInventory";
    public static final String ATTRIB_PREV_VIEW_ID = "PrevViewId";
    public static final String ATTRIB_SERVICE_EXTERNAL_ID = "ServiceExternalId";
    public static final String ATTRIB_SERVICE_EXTERNAL_ID_TYPE = "ServiceExternalIdType";
    public static final String ATTRIB_SERVICE_INTERNAL_ID = "ServiceInternalId";
    public static final String ATTRIB_SERVICE_INTERNAL_ID_RESETS = "ServiceInternalIdResets";
    public static final String ATTRIB_VIEW_CREATED_DT = "ViewCreatedDt";
    public static final String ATTRIB_VIEW_EFFECTIVE_DT = "ViewEffectiveDt";
    public static final String ATTRIB_VIEW_STATUS = "ViewStatus";

    public static final String ATTRIB_SEARCH_ACTIVE_DATE = "SearchActiveDate";
    public static final String ATTRIB_SEARCH_CREATE_DATE = "SearchCreateDate";
    public static final String ATTRIB_SEARCH_INACTIVE_DATE = "SearchInactiveDate";
    public static final String ATTRIB_SEARCH_INTENDED_VIEW_EFFECTIVE_DT = "SearchIntendedViewEffectiveDt";
    public static final String ATTRIB_SEARCH_IS_CURRENT = "SearchIsCurrent";
    public static final String ATTRIB_SEARCH_IS_FROM_INVENTORY = "SearchIsFromInventory";
    public static final String ATTRIB_SEARCH_PREV_VIEW_ID = "SearchPrevViewId";
    public static final String ATTRIB_SEARCH_SERVICE_EXTERNAL_ID = "SearchServiceExternalId";
    public static final String ATTRIB_SEARCH_SERVICE_EXTERNAL_ID_TYPE = "SearchServiceExternalIdType";
    public static final String ATTRIB_SEARCH_SERVICE_INTERNAL_ID = "SearchServiceInternalId";
    public static final String ATTRIB_SEARCH_SERVICE_INTERNAL_ID_RESETS = "SearchServiceInternalIdResets";
    public static final String ATTRIB_SEARCH_VIEW_CREATED_DT = "SearchViewCreatedDt";
    public static final String ATTRIB_SEARCH_VIEW_EFFECTIVE_DT = "SearchViewEffectiveDt";
    public static final String ATTRIB_SEARCH_VIEW_STATUS = "SearchViewStatus";
    public static final String ATTRIB_SEARCH_VIEW_ID = "SearchViewId";

    /**
     * Creates a new instance of C30CustomerContract.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ServiceExternalId(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ServiceExternalId(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30ServiceExternalId.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     */
    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException  {

    	super.initializeAttributes();

        this.addAttribute(new C30SDKAttribute(ATTRIB_ACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_CURRENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_FROM_INVENTORY, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_CREATED_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    	// Search Attributes
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CREATE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_INACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_INTENDED_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_IS_CURRENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_IS_FROM_INVENTORY, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PREV_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SERVICE_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SERVICE_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_CREATED_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKAttribute attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_SERVICE_EXT_ID_MEMBER_TYPE);
    }


    /**
     * Creates the Kenan FX customer id equip map view record using API TS.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#createFXObject()
     */
    @Override
	protected C30SDKObject createFXObject() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	C30SDKObject lwo = null;
    	
    	//Map to hold all External ID data.
        Map ciemData = new HashMap();

        //----------------------------------------------------------------------
        // Set the following key service fields:
        // View Status: Pending
        // View Effective Date: Now
        // View Create Date: Now
        // Service External Id Type: based on member_id of the parent order item
        //----------------------------------------------------------------------
        ciemData.put(ATTRIB_VIEW_STATUS, C30OrderItem.FX_PENDING_VIEW_STATUS);
        ciemData.put(ATTRIB_VIEW_CREATED_DT, new Date());
        ciemData.put(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        ciemData.put(ATTRIB_IS_CURRENT, new Boolean("true"));

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            ciemData.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            ciemData.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        ciemData.put(ATTRIB_SERVICE_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        ciemData.put(ATTRIB_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));

        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {
        	setConnectActionValues(ciemData);
        } else if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
            setDisconnectActionValues(ciemData);
        }

        Map callResponse = this.queryData("CustomerIdEquipMap", "Create", ciemData, getAccountServerId());
        Map ciem = (HashMap) callResponse.get("CustomerIdEquipMap");
        populateCIEM(ciem);

        lwo = this;
        
        return lwo;
    }

    private void populateCIEM(final Map ciem) throws C30SDKInvalidAttributeException {

        this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) ciem.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_ACTIVE_DATE, ciem.get(ATTRIB_ACTIVE_DATE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_CREATE_DATE, ciem.get(ATTRIB_CREATE_DATE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.setAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, ciem.get(ATTRIB_SERVICE_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, ciem.get(ATTRIB_SERVICE_EXTERNAL_ID_TYPE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_INACTIVE_DATE, ciem.get(ATTRIB_INACTIVE_DATE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, ciem.get(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_IS_CURRENT, ciem.get(ATTRIB_IS_CURRENT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_IS_FROM_INVENTORY, ciem.get(ATTRIB_IS_FROM_INVENTORY), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_PREV_VIEW_ID, ciem.get(ATTRIB_PREV_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, ciem.get(ATTRIB_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, ciem.get(ATTRIB_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_VIEW_CREATED_DT, ciem.get(ATTRIB_VIEW_CREATED_DT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_VIEW_EFFECTIVE_DT, ciem.get(ATTRIB_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_VIEW_STATUS, ciem.get(ATTRIB_VIEW_STATUS), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        this.setAttributeValue(ATTRIB_MEMBER_ID, ciem.get(ATTRIB_SERVICE_EXTERNAL_ID_TYPE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, ciem.get(ATTRIB_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, ciem.get(ATTRIB_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, new BigInteger(ciem.get(ATTRIB_SERVICE_INTERNAL_ID).toString()), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
    }

    /**
     * Method which validates the definition of the order item.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        
    	super.validateAttributes();
        
    	if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID) &&
            !this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)){

            String error = "Invalid item action id: (" + this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                    ").  Item action id 10 or 30 must be used for an External Id";
            throw new C30SDKInvalidAttributeException(error);
        }
    }

    /**
     * Method which gets an external ID based on criteria already provided.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void findExternalId() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
    	log.debug("Inside C30ServiceExternalId.findExternalId");

        //Create a HashMap to contain the CIEM filter's info
    	HashMap ciemFilter = new HashMap();
        Map key = new HashMap();
        ciemFilter.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	ciemFilter.put("Fetch", new Boolean(true));

        //-----------------------------
        // Set the External ID Type that is specified for the order item
        //-----------------------------
    	HashMap attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	ciemFilter.put(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, attrFilter);

        //-----------------------------
        // Only get the current view
        //-----------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", new Integer(2));
    	ciemFilter.put(ATTRIB_VIEW_STATUS, attrFilter);

        //-------------------------------------------
        // Only get services that are still active
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("IsNull", Boolean.TRUE);
    	ciemFilter.put(ATTRIB_INACTIVE_DATE, attrFilter);

        //-------------------------------------------
        // add the service internal id to the filter
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	ciemFilter.put(ATTRIB_SERVICE_INTERNAL_ID, attrFilter);

        //------------------------------------------------
        // add the service internal resets to the filter
        //------------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	ciemFilter.put(ATTRIB_SERVICE_INTERNAL_ID_RESETS, attrFilter);

        //-------------------------------------------
        // add is_current flag to the filter.
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", Boolean.TRUE);
    	ciemFilter.put(ATTRIB_IS_CURRENT, attrFilter);

        //--------------------------------------------
        // Construct Filter elements from SEARCH
        // attributes that have been set or defaulted
        //--------------------------------------------
        Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeSearchValue = null;
        while (iter.hasNext()) {
            attributeName = (String) iter.next();

            if (attributeName.indexOf("Search") != -1) {
	            attributeSearchValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	            if (attributeSearchValue != null) {

                    attrFilter = new HashMap();
                    attrFilter.put("Equal",attributeSearchValue);

	            	//if we are searching on the key
	            	if (attributeName.equals(ATTRIB_SEARCH_VIEW_ID)) {
	                    key.put(attributeName.replaceFirst("Search", ""), attrFilter);
	                    ciemFilter.put("Key", key);
	            	//if we are not searching on the key.
		            } else {
	                    ciemFilter.put(attributeName.replaceFirst("Search", ""), attrFilter);
		            }

	            }
            }
        }

        Map callResponse = this.queryData("CustomerIdEquipMap", "Find", ciemFilter, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
            throw new C30SDKObjectException("No external id found for subscr no = " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                                ", subscr no resets = " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        } else if (count > 1) {
            throw new C30SDKObjectException("Multiple external ids found for subscr_no = " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                                ", subscr no resets = " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        Map ciem = (HashMap) ((Object[]) callResponse.get("CustomerIdEquipMapList"))[0];
        populateCIEM(ciem);

    	log.debug("Finished C30ServiceExternalId.findExternalId");
    }

    /**
     * Method which sets all the attributes when a connect is performed.
     * @param ciemData the External ID data map that is being populated with the attributes.
     * @throws C30SDKInvalidAttributeException
     */
    private void setConnectActionValues(final Map ciemData) throws C30SDKInvalidAttributeException {
        //-------------------------------------------------------------------
        // The following values are not overrideable.  They are specially set
        // based on account and order attributes
        //-------------------------------------------------------------------
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	ciemData.put(ATTRIB_ACTIVE_DATE, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	ciemData.put(ATTRIB_ACTIVE_DATE, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        }
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeValue = null;
        while (iterator.hasNext()) {
            attributeName = (String) iterator.next();
            attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if(attributeName.equals(ATTRIB_SERVICE_EXTERNAL_ID) && attributeValue != null) {
            	ciemData.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_IS_FROM_INVENTORY) && attributeValue != null) {
            	ciemData.put(attributeName, attributeValue);
            }
        }
    }

    /**
     * Method which sets all the attributes when a disconnect is performed.
     * @param ciemData the External ID data map that is being populated with the attributes.
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void setDisconnectActionValues(final Map ciemData) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	this.findExternalId();

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	ciemData.put(ATTRIB_INACTIVE_DATE, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	ciemData.put(ATTRIB_INACTIVE_DATE, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        ciemData.put(ATTRIB_ACTIVE_DATE, this.getAttributeValue(ATTRIB_ACTIVE_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        ciemData.put(ATTRIB_PREV_VIEW_ID, this.getAttributeValue(ATTRIB_VIEW_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        ciemData.put(ATTRIB_SERVICE_EXTERNAL_ID, this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    }

    @Override
	protected C30SDKObject createChildOrderItems() { return null; }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {

    }

}
