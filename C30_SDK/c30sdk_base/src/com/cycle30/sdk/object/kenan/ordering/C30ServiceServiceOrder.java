/*
 * C30ServiceServiceOrder.java
 *
 * Created on December 7, 2006, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * The lightweight service order is a pre-configured template for a Kenan/FX service
 * order and is associated with a lightweight order.  The lightweight service order
 * will be defined to contain one or more pre-configured order items (lightweight
 * order items).
 * @author Joe Morales
 */
public class C30ServiceServiceOrder extends C30ServiceOrder implements Cloneable {

    private static Logger log = Logger.getLogger(C30ServiceServiceOrder.class);

    public static final String ATTRIB_SERVICE_EXTERNAL_ID = "ServiceExternalId";
    public static final String ATTRIB_SERVICE_EXTERNAL_ID_TYPE = "ServiceExternalIdType";
    public static final String ATTRIB_SERVICE_INTERNAL_ID = "ServiceInternalId";
    public static final String ATTRIB_SERVICE_INTERNAL_ID_RESETS = "ServiceInternalIdResets";
    protected static final String ATTRIB_SERVICE = "Service";
    private static final String ATTRIB_EMF_CONFIG_ID = "EmfConfigId";

    /** Lengths of string based service order attributes. */
    private static final int STR_LEN_SERVICE_EXTERNAL_ID = 144;

    /** Kenan service move in service order type value.  */
    public static final Integer SERV_ORD_TYPE_SERVICE_MOVE_OUT = new Integer(54);
    /** Kenan service move out service order type value.  */
    public static final Integer SERV_ORD_TYPE_SERVICE_MOVE_IN = new Integer(56);
    /** Kenan service disconnect service order type value.  */
    public static final Integer SERV_ORD_TYPE_SERVICE_DISCONNECT = new Integer(50);
    /** Kenan service resume service order type value.  */
    public static final Integer SERV_ORD_TYPE_RESUME = new Integer(70);
    /** Kenan service suspend service order type value.  */
    public static final Integer SERV_ORD_TYPE_SUSPEND = new Integer(80);
    /** Kenan service transfer disconnect service order type value.  */
    public static final Integer SERV_ORD_TYPE_TRANSFER_DISCONNECT = new Integer(100);

    /** Kenan service instance active status. */
    public static final Integer SERVICE_STATUS_ACTIVE = new Integer(1);
    /** Kenan service instance disconnected status. */
    public static final Integer SERVICE_STATUS_DISCONNECTED = new Integer(2);
    /** Kenan service instance suspended status. */
    public static final Integer SERVICE_STATUS_SUSPENDED = new Integer(3);
    /** Kenan service instance transferred out status. */
    public static final Integer SERVICE_STATUS_TRANSFERRED_OUT = new Integer(4);
    /** Kenan service instance transferred in status. */
    public static final Integer SERVICE_STATUS_TRANSFERRED_IN = new Integer(5);

    /**
     * Creates a new instance of C30ServiceServiceOrder.
     * @param factory the factory which is the parent of this object.
     * @param objectType Type of lightweight service service order being constructed
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ServiceServiceOrder(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /** 
     * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
     * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
     * the framework to create objects.  It is also used by the BRE front end.
     * @param factory Factory used to create C30ServiceOrder objects
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ServiceServiceOrder(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30ServiceServiceOrder.class);
    }
    
    /**
     * Initializes the attributes of the Kenan/FX service order.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the service.
     */
    @Override
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException {
    	
        super.initializeAttributes();
        
    	//=================
    	// DATA ATTRIBUTES
    	//=================
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID, String.class, STR_LEN_SERVICE_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRANSFER_FROM_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	this.addAttribute(new C30SDKAttribute(ATTRIB_EMF_CONFIG_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
    	//===================
    	// OUTPUT ATTRIBUTES
    	//===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	this.addAttribute(new C30SDKAttribute(ATTRIB_EMF_CONFIG_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
        this.setAttributeValue(ATTRIB_IS_SERVICE_LEVEL, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    }

    /** 
     * Method to populate the service order.
     * @param serviceOrder the service order information being used to populate the object.
     * @throws C30SDKInvalidAttributeException if an attribute was in an invalid state during initialization
     * @see com.cycle30.sdk.object.kenan.ordering.C30ServiceOrder#populateServiceOrder(java.util.Map)
     */
    @Override
	protected void populateServiceOrder(final Map serviceOrder) throws C30SDKInvalidAttributeException {
        
    	this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, serviceOrder.get(ATTRIB_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, serviceOrder.get(ATTRIB_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	this.setAttributeValue(ATTRIB_EMF_CONFIG_ID, serviceOrder.get(ATTRIB_EMF_CONFIG_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
        super.populateServiceOrder(serviceOrder);
    }
    
    /**
     * This method overrides the regular method because checks need to be made for whether the service
     * order type ID is being set.  If the service order type is a package disconnect then we need to
     * ensure that the package instance ID and serv are set as well.
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    @Override
	public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);
    	
    	if (name.equals(this.getAttribute(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
        	
    		if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_TRANSFER_DISCONNECT)) {
        		this.getAttribute(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
        		this.getAttribute(ATTRIB_TRANSFER_FROM_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
        	}
    		
    	}
    }
        
    /**
	 * Method to process the usage adjustment.
	 * @return C30SDKObject 
     */
    @Override
	public C30SDKObject process() {
        
    	C30SDKObject lwo = null;
    	try {
    		lwo = getOrCreateService();
    		//if the processed service order did not produce an exception.
            if (!(lwo instanceof C30SDKExceptionMessage)) {
    		
	    		lwo = this.createServiceOrder();
	    		
	    		//if the processed service order did not produce an exception.
	            if (!(lwo instanceof C30SDKExceptionMessage)) {
	            	lwo.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	            }
	            
            }

    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}

    	return lwo;
    }
    
    /**
     * Method to create the service that the service order is associated with.
     * @return C30SDKObject the returned service that was just created.
     * @throws C30SDKException 
     */
    private C30SDKObject createService() throws C30SDKException {
        
    	C30SDKObject service = null;
    	//----------------------------------------------------------------------
        // Method used for connect service orders, the sequence of events must be:
        // 1.) Create Service View row
        // 2.) Create Service order Row
        // 3.) Create Order Items.
        // This is a nasty circular dependency and requires a special create
        // option in the C30Service object.  A partial LW order item
        // must be created to get the subscr no for the service order.
        //----------------------------------------------------------------------

        for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
            C30SDKObject lwo = this.getC30SDKObject(i);

            // Find the service instance Order Item and create service view row
            // so the subscr_no/resets can be set for the service order.
            if (lwo.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_MEMBER_TYPE)) {
                lwo.setNameValuePairs(this.getNameValuePairMap());
                
                Date desiredDate = null;
                if (this.getAttributeValue(ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
                	desiredDate = (Date) this.getAttributeValue(ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                } else if (this.getAttributeValue(ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
                	desiredDate = (Date) this.getAttributeValue(ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                } else {
                	desiredDate = new Date();
                }
                lwo.setAttributeValue(C30Service.ATTRIB_SERVICE_ACTIVE_DT, desiredDate, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                lwo.setAttributeValue(C30Service.ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, desiredDate, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                
                //get the account internal ID from the associated account atached to the order.
                C30SDKObject account = (C30SDKObject) this.getAttributeValue(C30Order.class, C30Order.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                lwo.setAttributeValue(C30OrderItem.ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, account.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                
                service = ((C30Service) lwo).connectFXService();
                if (service instanceof C30SDKExceptionMessage) {
                	break;
                } else {
	                this.setAttributeValue(ATTRIB_SERVICE, lwo, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	                this.setAttributeValue(ATTRIB_SERVICE, lwo, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	                service = lwo;
                }
                
            }
        }
        
        return service;
    }

    /**
     * Method to get the service that the service order is associated with.
     * @return C30SDKObject the returned service that was found.
     * @throws C30SDKException 
     */
    private C30SDKObject getService() throws C30SDKException {

        //---------------------------------------------------------------------
        // Validation: Either the external_id or internal_id is set for the
        // LW Service Order. Kenan Service object is obtained via lightweighservice
        // so that further service level validations can be performed.
        //---------------------------------------------------------------------
        C30SDKObject service = this.getFactory().createC30SDKObject(C30Service.class, new Object[0]);

        this.setAttributeValue(ATTRIB_SERVICE, service, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_SERVICE, service, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (!this.isAttributeValueSet(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) ||
            !this.isAttributeValueSet(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
            if (this.isAttributeValueSet(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
                this.isAttributeValueSet(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
                //--------------------------------------------------------------
                // Find the Kenan service object using the external id.  This
                // object will be used for service level validation
                //--------------------------------------------------------------
            	service.setAttributeValue(C30OrderItem.ATTRIB_ITEM_ACTION_ID, C30OrderItem.ORD_ITEM_FIND_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            	service.setAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            	service.setAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, this.getAttributeValue(ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                service = service.process();
            } else {
                String error = "Must set either external id/type or internal id/resets for the lightweight Service Order";
                throw new C30SDKObjectException(error);
            }
        } else {
            //-------------------------------------------------------------------------------
            // If internal id is populated, Find Kenan service object using the internal id.
            //-------------------------------------------------------------------------------
        	service.setAttributeValue(C30OrderItem.ATTRIB_ITEM_ACTION_ID, C30OrderItem.ORD_ITEM_FIND_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	service.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	service.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            service = service.process();
       }
        
        if (service instanceof C30Service) {
            this.setAttributeValue(ATTRIB_SERVICE, service, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            this.setAttributeValue(ATTRIB_SERVICE, service, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        } 
        
        return service;
    }

    /**
     * Sets the internal id/resets for the Kenan FX Service Instance based on
     * either the external id or the internal id provided for the LWO client.
     * @return C30SDKObject the returned service.
     * @throws C30SDKException 
     */
    private C30SDKObject getOrCreateService() throws C30SDKException {
    	log.debug("Starting C30ServiceServiceOrder.getOrCreateService");
    	C30SDKObject service = null;
        //----------------------------------------------------------------------
        // In order to properly set the service instance attributes, the service
        // object must be created or retrieved.  If it is a connect service order
        // the service row must be created. If it is a change or disconenct
        // service order, it must be retrieved.
        //----------------------------------------------------------------------
        if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_CONNECT)) {
            service = this.createService();
        } else {
            service = this.getService();
        }

        //we may have an exception
        if (service instanceof C30Service) {
	        //----------------------------------------------------------------------
	        // Set the Kenan service attributes
	        //----------------------------------------------------------------------
	        this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, ((C30SDKObject) this.getAttributeValue(ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        this.setAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, ((C30SDKObject) this.getAttributeValue(ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        
	        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
	        	this.setAttributeValue(ATTRIB_EMF_CONFIG_ID, ((Integer) ((C30SDKObject) this.getAttributeValue(ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30Service.ATTRIB_EMF_CONFIG_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)).toString(), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        }
	        
	        //----------------------------------------------------------------------
	        // Validate the service account against order account and make sure they
	        // are the same.  The validation MUST take place here because we do not
	        // know this information until we get the service.
	        //----------------------------------------------------------------------
	        C30SDKObject account = (C30SDKObject) this.getAttributeValue(C30Order.class, C30Order.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        if (!account.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).equals(((C30SDKObject) this.getAttributeValue(ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30OrderItem.ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))){
	            String error = "The account associated to the order (internal account no: " + this.getAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
	                       ") does not match the account for the service instance (internal account no: " +
	                       ((C30SDKObject) this.getAttributeValue(ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30OrderItem.ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) + ")";
	            throw new C30SDKInvalidAttributeException(error);
	        }


        }
    	log.debug("Finished C30ServiceServiceOrder.getOrCreateService");
    	return service;
    }

    /**
     * Validates that all the required attributes have been set.
     * @throws C30SDKInvalidAttributeException if there was a problem validating the attributes of the service order.
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected final void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        super.validateAttributes();

        //-----------------------------------------------------------
        // make sure the service order type id is currently supported
        // by the LWO Framework for Service Level Service Orders
        //-----------------------------------------------------------
        if (!this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceOrder.SERV_ORD_TYPE_CONNECT) &&
                !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceOrder.SERV_ORD_TYPE_CHANGE) &&
                !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SERVICE_DISCONNECT) &&
                   !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_RESUME) &&
                   !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SUSPEND) &&
                   !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_TRANSFER_DISCONNECT)&&
                   !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SERVICE_MOVE_OUT)&&
                   !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SERVICE_MOVE_IN)) {
            String error = "The service order type id '" + this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString() +
                           "' is not currently supported by the lightweight ordering framework" +
                           " for service instance level service orders";
            throw new C30SDKInvalidAttributeException(error);
        }

        //-----------------------------------------------------------------------
        // if this is a transfer order, make sure the transfer to account is set
        //-----------------------------------------------------------------------
        if(this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_TRANSFER_DISCONNECT)) {
            if (!this.isAttributeValueSet(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
                String error = "The " + ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID +
                               " attribute must be set for Transfer orders";
                throw new C30SDKInvalidAttributeException(error);
            }
        }
        
        //------------------------------------------------------------------------------------------
        // if this is a service move order, make sure the account move request service order exists
        //------------------------------------------------------------------------------------------
        if(this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SERVICE_MOVE_IN)) {
            
        	C30SDKObject order = (C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	boolean hasRequestMoveSO = false;
        	for (int i = 0; i < order.getC30SDKObjectCount(); i++) {
        		if (order.getC30SDKObject(i) instanceof C30AccountServiceOrder) {
        			if (order.getC30SDKObject(i).getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30AccountServiceOrder.SERV_ORD_TYPE_ACCOUNT_MOVE_REQUEST)) {
        				hasRequestMoveSO = true;
        				break;
        			}
        		}
        	}
        	
        	if (!hasRequestMoveSO) {
                String error = "The service order of type " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                	" does not have a corresponding account level service order of type 45 (Request Move).";
                throw new C30SDKInvalidAttributeException(error);
        	}
        }
        

        //----------------------------------------------------------------------
        // Validate that the service order type is valid for the service status.
        // For example, you should not be able to create a suspend service order
        // for a service that is already suspended.
        //---------------------------------------------------------------------
        Integer serviceStatus = (Integer) ((C30SDKObject) this.getAttributeValue(ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30Service.ATTRIB_STATUS_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SUSPEND) && serviceStatus.equals(SERVICE_STATUS_TRANSFERRED_OUT)) {
            String error = "Can not create a suspend service order (service order type: " + this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString() +
                    ") for a service that has been transferred to another account (service status: " + serviceStatus.toString() + ")";
            throw new C30SDKInvalidAttributeException(error);
        } else if(this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SUSPEND) && !serviceStatus.equals(SERVICE_STATUS_ACTIVE)&& !serviceStatus.equals(SERVICE_STATUS_TRANSFERRED_IN)) {
            String error = "Error: Only active servcies can be suspended (service status:" + serviceStatus.toString() + ")";
            throw new C30SDKInvalidAttributeException(error);
        } else if(this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_RESUME) && !serviceStatus.equals(SERVICE_STATUS_SUSPENDED)) {
            String error = "Error: Service must be suspended in order to create a resume service order.";
            throw new C30SDKInvalidAttributeException(error);
        }

    }

        /**
     * Initilize attirbutes for the Kenan FX service order that is about to
     * be created through the API TS.
     * @param serviceOrder Kenan/FX service order object
     * @throws C30SDKInvalidAttributeException if there was a problem setting the attributes of the service order.
     */
    @Override
	protected final void setServiceOrderAttributes(final Map serviceOrder) throws C30SDKInvalidAttributeException {

        //---------------------------------------------------------------------
        // IsServiceLevel = TRUE - for LW Service level Service orders.
        //---------------------------------------------------------------
        serviceOrder.put(C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL,Boolean.TRUE);

        //----------------------------------------------------------------------
        // If this is a move then we need to get the RelatedServiceOrderId. This
        // ID is the Account Move Request Service Order ID that is created with
        // this service order.
        //----------------------------------------------------------------------
        if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceServiceOrder.SERV_ORD_TYPE_SERVICE_MOVE_IN) ||
        		this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceServiceOrder.SERV_ORD_TYPE_SERVICE_MOVE_OUT)) {
        	C30Order order = (C30Order) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	for (int i = 0; i < order.getC30SDKObjectCount(); i++) {
        		if (order.getC30SDKObject(i) instanceof C30AccountServiceOrder) {
        			if (order.getC30SDKObject(i).getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30AccountServiceOrder.SERV_ORD_TYPE_ACCOUNT_MOVE_REQUEST)) {
        				this.setAttributeValue(ATTRIB_RELATED_SERVICE_ORDER_ID, order.getC30SDKObject(i).getAttributeValue(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        			}
        		}
        	}
        }
        
        //--------------------------------------------
        // set the remaining service order attributes
        //--------------------------------------------
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if (attributeValue != null) {
                if (attributeName.equals(ATTRIB_SERVICE_INTERNAL_ID)) {
                    serviceOrder.put(ATTRIB_SERVICE_INTERNAL_ID, attributeValue);
                } else if (attributeName.equals(ATTRIB_SERVICE_INTERNAL_ID_RESETS)) {
                    serviceOrder.put(ATTRIB_SERVICE_INTERNAL_ID_RESETS, attributeValue);
                } else if (attributeName.equals(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID)) {
                    serviceOrder.put(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID, attributeValue);
                } else if (attributeName.equals(ATTRIB_EMF_CONFIG_ID)) {
                    serviceOrder.put(ATTRIB_EMF_CONFIG_ID, attributeValue);
                }
            }
        }
        super.setServiceOrderAttributes(serviceOrder);
    }
    
    /**
     * Method to add a child object.
     * @param object the child object being added.
     */
    @Override
	public void addC30SDKObject(final C30SDKObject object){
		super.addC30SDKObject(object);
    	
		//after the parent/child relationship has been made check whether the service order is actually
		//a move.  If it is then we need to initialize the service attributes.  The reason this is messy
		//and done here is because we do not know if the service item is a move on initialization until
		//the parent/child relationship has been established.
		try {
			if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_SERVICE_MOVE_IN) &&
					object instanceof C30Service) {
				((C30Service) object).initializeServiceMoveAttributes();
			}
		} catch (C30SDKInvalidAttributeException e) {
		    log.error(e);
		}
    }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
