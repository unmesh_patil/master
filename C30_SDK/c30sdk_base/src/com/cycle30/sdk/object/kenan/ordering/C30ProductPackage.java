/*
 * C30ProductPackage.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX Product Package object and its corresponding order item.
 * @author Joe Morales
 */
public class C30ProductPackage extends C30OrderItem {

    private static Logger log = Logger.getLogger(C30ProductPackage.class);

    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID = "AccountExternalId";
    public static final String ATTRIB_ACTIVE_DT = "ActiveDt";
    public static final String ATTRIB_CONNECT_REASON = "ConnectReason";
    public static final String ATTRIB_HIERARCHY_ID = "HierarchyId";
    public static final String ATTRIB_INACTIVE_DT = "InactiveDt";
    public static final String ATTRIB_PACKAGE_INST_ID = "PackageInstId";
    public static final String ATTRIB_PACKAGE_INST_ID_SERV = "PackageInstIdServ";
    public static final String ATTRIB_PACKAGE_ID = "PackageId";
    public static final String ATTRIB_PACKAGE_STATUS = "PackageStatus";

    public static final String ATTRIB_SEARCH_ACCOUNT_EXTERNAL_ID = "SearchAccountExternalId";
    public static final String ATTRIB_SEARCH_ACTIVE_DT = "SearchActiveDt";
    public static final String ATTRIB_SEARCH_CONNECT_REASON = "SearchConnectReason";
    public static final String ATTRIB_SEARCH_HIERARCHY_ID = "SearchHierarchyId";
    public static final String ATTRIB_SEARCH_INACTIVE_DT = "SearchInactiveDt";
    public static final String ATTRIB_SEARCH_PACKAGE_INST_ID = "SearchPackageInstId";
    public static final String ATTRIB_SEARCH_PACKAGE_INST_ID_SERV = "SearchPackageInstIdServ";
    public static final String ATTRIB_SEARCH_PACKAGE_ID = "SearchPackageId";
    public static final String ATTRIB_SEARCH_PACKAGE_STATUS = "SearchPackageStatus";

    /** Package Value Constants. */
    public static final Integer PRODUCT_PKG_INACTIVE_STATUS = new Integer(0);
    public static final Integer PRODUCT_PKG_ACTIVE_STATUS = new Integer(1);
    public static final Integer PRODUCT_PKG_DISCONNECTED_STATUS = new Integer(2);
    
    /** Service Search Attributes */
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID = "ParentServiceInternalId";
    public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS = "ParentServiceInternalIdResets";
    public static final String ATTRIB_COMPONENT_STATUS = "ComponentStatus";
    
    private C30SDKObject account = null;
    private static Integer startRecord = 1;
    private static Integer returnSetSize = 50;

    /**
     * Creates a new instance of C30ProductPackage.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ProductPackage(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30ProductPackage(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30ProductPackage.class);
    }

    /**
     * Validates the item action id of the order item .
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException{
        
    	super.validateAttributes();
        
    	if ((!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)) &&
            (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID))) {

            String error = "Invalid item action id: (" + this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                    ").  Item action id 10 OR 30 must be used for a Product Package";
            throw new C30SDKInvalidAttributeException(error);
        }
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     */
    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException  {

    	super.initializeAttributes();

    	//=================
    	// Data Attributes
    	//=================
        // added jcw
    	this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) 
             this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        // end added jcw
        
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HIERARCHY_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKAttribute attribute = new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID, Integer.class);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(ATTRIB_PACKAGE_ID, Integer.class);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID_SERV, Integer.class);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        attribute.setOverrideable(false);
        attribute.setValue(C30OrderItem.ORD_ITEM_PACKAGE_MEMBER_TYPE);

    	//===================
    	// Search Attributes
    	//===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_HIERARCHY_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PACKAGE_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PACKAGE_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PACKAGE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PACKAGE_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

    	//===================
    	// Output Attributes
    	//===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CONNECT_REASON, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_HIERARCHY_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_INACTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        //=====================
        // Search Attributes
        //=====================
         this.addSearchAttributes();


    }

    /**
     * Create the product package using the Kenan FX API TS objects.
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected C30SDKObject createFXObject() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	C30SDKObject lwo = null;
    	
        //create hashmap shell
        Map key = new HashMap();
        Map productPackageData = new HashMap();
        productPackageData.put("Key", key);

        productPackageData.put(ATTRIB_PACKAGE_ID, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        productPackageData.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        productPackageData.put(ATTRIB_PACKAGE_STATUS, PRODUCT_PKG_INACTIVE_STATUS);
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            productPackageData.put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            productPackageData.put(ATTRIB_ACTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }

        setFXProductPackageAttributes(productPackageData);

        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {
	        Map callResponse = this.queryData("ProductPackage", "Create", productPackageData, getAccountServerId());
	        Map productPackage = (HashMap) callResponse.get("ProductPackage");
	        populateProductPackage(productPackage);
        } else if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
            this.findPackage();
        }
        
        lwo = this;
        
        return lwo;
    }

    @Override
	public C30SDKObject process() throws C30SDKInvalidAttributeException {
    	log.debug("Starting C30ProductPackage.process()");

    	//in case there is an error.
        String error = "creating lightweight order item:\n\tLW Order Item Type: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKObject lwo = null;
    	try {
    	    if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND)) {
                setAttributesFromNameValuePairs();
                super.validateFindAttributes();
                super.clearOrderAttributes();
                 if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
                    isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
                     getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
                 {

                        // check to see if we need to explicitly get an account
                        if (super.canServerIdCacheBeSetLocally())
                             super.setAccountServerIdCache();
                         else{
                                 if (this.account == null){
                                     account = getAssociatedAccount();
                                     if(account instanceof C30SDKExceptionMessage)
                                       return account;
                                 } 
                         }
                        super.setAccountServerId();
                }
                
                findPackageFor();
                setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                lwo = this;
            }
            else
            {

    		lwo = super.process();

    		//since we have just created a package we need to add the package information to the package instnace list.
    		if (!(lwo instanceof C30SDKExceptionMessage)) {
    			C30PackageInstanceList packageInstances = (C30PackageInstanceList) this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    			packageInstances.addLightweightPackageInstance(
    					(Integer) this.getAttributeValue(ATTRIB_PACKAGE_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT),
    					(Integer) this.getAttributeValue(ATTRIB_PACKAGE_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT),
    					(Integer) this.getAttributeValue(ATTRIB_PACKAGE_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT),
    					(Integer) this.getAttributeValue(ATTRIB_PACKAGE_LINK_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    		}
            }
    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
    		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "Error " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}
    	log.debug("Finished C30ProductPackage.process()");
        return lwo;
    }

    private  void addSearchAttributes()
    throws  C30SDKInvalidAttributeException
    {
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
         
        C30SDKAttribute attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, java.lang.Integer.class);
        attribute.setValue(C30ProductPackage.startRecord);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, java.lang.Integer.class);
        attribute.setValue(C30ProductPackage.returnSetSize);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_COUNT, java.lang.Integer.class);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_TOTAL_RETURNED, java.lang.Integer.class);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, java.lang.Integer.class);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        attribute =  new C30SDKAttribute(C30SDKValueConstants.ATTRIB_LAST_RECORD_RETURNED_NUMBER, java.lang.Integer.class);
        addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) 
             if  (! isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT)){
                 attribute =  new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, java.lang.Integer.class);
                 addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                 addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
             }


    }
    
    private C30SDKObject getAssociatedAccount()
        throws C30SDKException
    {
        C30SDKObject account = null;
        // add the attributes here - not previously part of the order-service_order processing only for the find 
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, java.lang.Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT, com.cycle30.sdk.core.framework.C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if(!isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            if(isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
            {
                account = getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30Account.class, new Object[0]);
                account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account = account.process();
                if(account instanceof C30Account)
                {
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                    if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) 
                         if  (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                             setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                             
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                }
            } else
            {
                String errorMsg = "The account internal id or external id and external id type must be set in order to create or fetch a Kenan/FX object";
                throw new C30SDKObjectException(errorMsg);
            }
        return account;
    }


    private void populateFindMap(Map productPackage)
        throws C30SDKInvalidAttributeException
    {
    // jcw added: LWO Enhancements
        for  ( Iterator it=productPackage.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                 Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                 String key = (String)entry.getKey (  ) ;     
                    Object value = entry.getValue (  ) ;
                    if (value != null)
                    if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                        setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                    else
                       if (key.equalsIgnoreCase(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID) )
                           if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                             setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                       
                    if (value != null)
                        log.debug(  key+" " +  value ) ; 
                
        }
        
        log.debug("Done populating account response!");
    }


    private void populateProductPackage(final Map productPackage) throws C30SDKInvalidAttributeException {

        this.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, productPackage.get(ATTRIB_ACCOUNT_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ACTIVE_DT, productPackage.get(ATTRIB_ACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CONNECT_REASON, productPackage.get(ATTRIB_CONNECT_REASON), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_HIERARCHY_ID, productPackage.get(ATTRIB_HIERARCHY_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_INACTIVE_DT, productPackage.get(ATTRIB_INACTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_STATUS, productPackage.get(ATTRIB_PACKAGE_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, productPackage.get(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_ID, productPackage.get(ATTRIB_PACKAGE_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_INST_ID, ((HashMap) productPackage.get("Key")).get(ATTRIB_PACKAGE_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_PACKAGE_INST_ID_SERV, ((HashMap) productPackage.get("Key")).get(ATTRIB_PACKAGE_INST_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        this.setAttributeValue(ATTRIB_VIEW_ID, new BigInteger("0"), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_ID, productPackage.get(ATTRIB_PACKAGE_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, ((HashMap) productPackage.get("Key")).get(ATTRIB_PACKAGE_INST_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, ((HashMap) productPackage.get("Key")).get(ATTRIB_PACKAGE_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, new BigInteger(((HashMap) productPackage.get("Key")).get(ATTRIB_PACKAGE_INST_ID).toString()), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
    }

    private void setFXProductPackageAttributes(final Map productPackage) throws C30SDKInvalidAttributeException {
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if(attributeName.equals(ATTRIB_CONNECT_REASON) && attributeValue != null) {
                productPackage.put(attributeName, attributeValue);
            } else if(attributeName.equals(ATTRIB_PACKAGE_ID) && attributeValue != null) {
                productPackage.put(attributeName, attributeValue);
            }
        }
    }

    private void findPackageFor() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {


        int lastRecordReturned = 0;
           // set values for record retrieval
        if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                 getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
                startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             
        if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                    getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
                 returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

       // set the value is sent in
        if  (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                    getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null &&
                    isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
            setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            
                   
        //Create a HashMap to contain the Package filter's info
        HashMap filters = new HashMap();
        HashMap key = new HashMap();
      //  filters.put("Key", key);

        //Set the Fetch flag at the root level,
        //so the call will return all fields for found objects
        filters.put("Fetch", new Boolean(true));
        
        
        //-----------------------------------------
        // Only get packages that are still active
        //-----------------------------------------
        HashMap attrFilter = new HashMap();
        attrFilter.put("IsNull", Boolean.TRUE);
        filters.put(ATTRIB_INACTIVE_DT, attrFilter);
        
        /*
        attrFilter = new HashMap();
        attrFilter.put("Equal", new Integer(10000));
        filters.put("BlockSize", attrFilter);
        */
        
        filters.put("PackageInstIdSortDirection", new Boolean(true));    
         attrFilter = new HashMap();
         attrFilter.put("Equal", new Integer(1));
         filters.put("PackageInstIdSortOrder", attrFilter);


        attrFilter = new HashMap();
        if (this.getAttributeValue(ATTRIB_SEARCH_PACKAGE_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
                attrFilter.put("Equal", PRODUCT_PKG_ACTIVE_STATUS);
        } else {
                attrFilter.put("Equal", this.getAttributeValue(ATTRIB_SEARCH_PACKAGE_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        filters.put(ATTRIB_COMPONENT_STATUS, attrFilter);

        //-------------------------------------------
        // Only get packages owned by the parent account.
        //-------------------------------------------
        attrFilter = new HashMap();
        attrFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        filters.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, attrFilter);
     
        //--------------------------------------------
        // Construct Filter elements from SEARCH
        // attributes that have been set or defaulted
        //--------------------------------------------
        if  (isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
             getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ){
           attrFilter = new HashMap();
           attrFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
           filters.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, attrFilter);                 
        }
        if  (isAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
            getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ){
             attrFilter = new HashMap();
             attrFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
             filters.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, attrFilter);                 
         }
        if  (isAttribute(ATTRIB_PACKAGE_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
            getAttributeValue(ATTRIB_PACKAGE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ){
             attrFilter = new HashMap();
             attrFilter.put("Equal", this.getAttributeValue(ATTRIB_PACKAGE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
             filters.put(ATTRIB_PACKAGE_ID, attrFilter);                 
         }
        if  (isAttribute(ATTRIB_PACKAGE_INST_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
            getAttributeValue(ATTRIB_PACKAGE_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ){
             attrFilter = new HashMap();
             attrFilter.put("Equal", this.getAttributeValue(ATTRIB_PACKAGE_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
             filters.put(ATTRIB_PACKAGE_INST_ID, attrFilter);                 
         }
        if  (isAttribute(ATTRIB_PACKAGE_INST_ID_SERV,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
            getAttributeValue(ATTRIB_PACKAGE_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null ){
             attrFilter = new HashMap();
             attrFilter.put("Equal", this.getAttributeValue(ATTRIB_PACKAGE_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
             filters.put(ATTRIB_PACKAGE_INST_ID_SERV, attrFilter);                 
         }  
       
        log.debug(filters.toString());
        //ComponentFindCount
        Map callResponse = this.queryData("Component", "Find", filters, getAccountServerId());
      //  Map callResponse = this.queryData("Component", "FindCount", filters, getAccountServerId());
        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if(count == 0)
        {
          setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
          return;
        }
        
     
        Map productPackage = (HashMap) ((Object[]) callResponse.get("ComponentList"))[0];
        
        int recordsProcessed = 0;
        Integer lastPackageInstId = -1;
        if (startRecord == 1)
        {
            populateFindMap(productPackage);
            lastPackageInstId = (Integer)productPackage.get(ATTRIB_PACKAGE_INST_ID);
            if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            lastRecordReturned = 1;
            recordsProcessed++;
        }

        
        // at this point we have more than a single account to load
         C30ProductPackage cloneLwo = (C30ProductPackage)super.clone();
         Integer aPackageInstId = -1;
         if (startRecord > 1)
           startRecord--;
          for (int i=startRecord; i < count; i++)
          {
              if (recordsProcessed >= returnSetSize )
                break;
             // productPackage = (HashMap)((Object[])(Object[])callResponse.get("ProductPackageList"))[i]; 
              productPackage = (HashMap)((Object[])callResponse.get("ComponentList"))[i]; 
              aPackageInstId  = (Integer)productPackage.get(ATTRIB_PACKAGE_INST_ID);
              if (aPackageInstId.equals(lastPackageInstId))
                 continue;
              lastPackageInstId = aPackageInstId;
              // is this the first account we are processing?
              if (recordsProcessed == 0){
                  populateFindMap(productPackage);
                if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                      this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

                this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
              }
              else
              {
                  C30ProductPackage lwo = (C30ProductPackage)cloneLwo.clone();
                  lwo.populateFindMap(productPackage);
                 // add to our list of peer objects
                  if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                        lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                  lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                  super.addC30SDKObject(lwo);
                  }
              recordsProcessed++;
              lastRecordReturned =  i + 1;
          }
        super.setProcessingAggregateTotalsAttributes(this, count, lastRecordReturned, recordsProcessed );
        
        
    }


    private void findPackage() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	//Create a HashMap to contain the Package filter's info
    	HashMap filters = new HashMap();
    	HashMap key = new HashMap();
    	filters.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	filters.put("Fetch", new Boolean(true));

        //-----------------------------------------
        // Only get packages that are still active
        //-----------------------------------------
    	HashMap attrFilter = new HashMap();
    	attrFilter.put("IsNull", Boolean.TRUE);
    	filters.put(ATTRIB_INACTIVE_DT, attrFilter);

    	attrFilter = new HashMap();
    	if (this.getAttributeValue(ATTRIB_SEARCH_PACKAGE_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
    		attrFilter.put("Equal", PRODUCT_PKG_ACTIVE_STATUS);
    	} else {
    		attrFilter.put("Equal", this.getAttributeValue(ATTRIB_SEARCH_PACKAGE_STATUS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	}
    	filters.put(ATTRIB_PACKAGE_STATUS, attrFilter);

        //-------------------------------------------
        // Only get packages owned by the parent account.
        //-------------------------------------------
    	attrFilter = new HashMap();
    	attrFilter.put("Equal", this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	filters.put(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, attrFilter);

        //--------------------------------------------
        // Construct Filter elements from SEARCH
        // attributes that have been set or defaulted
        //--------------------------------------------
        Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeSearchValue = null;
        while (iter.hasNext()) {
            attributeName = (String) iter.next();

            if (attributeName.indexOf("Search") != -1) {
	            attributeSearchValue = this.getAttributeValue(attributeName, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	            if (attributeSearchValue != null) {

                    attrFilter = new HashMap();
                    attrFilter.put("Equal",attributeSearchValue);

	            	//if we are searching on the key
	            	if (attributeName.equals(ATTRIB_SEARCH_PACKAGE_INST_ID) || attributeName.equals(ATTRIB_SEARCH_PACKAGE_INST_ID_SERV)) {
	                    key.put(attributeName.replaceFirst("Search", ""), attrFilter);
	                    filters.put("Key", key);
	            	//if we are not searching on the key.
		            } else {
	                    filters.put(attributeName.replaceFirst("Search", ""), attrFilter);
		            }

	            }
            }
        }

	    Map callResponse = this.queryData("ProductPackage", "Find", filters, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
            throw new C30SDKObjectException("No active packages match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                                " for account_no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        } else if (count > 1) {
            throw new C30SDKObjectException("Multiple packages match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                                " for account_no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        Map productPackage = (HashMap) ((Object[]) callResponse.get("ProductPackageList"))[0];
        populateProductPackage(productPackage);
    }

    /**
     * Product packages do not currently support child order items.  This is a placeholder
     * for potential future expansion of the LWO framework.
     * @return C30SDKObject the returned child order items.
     */
    @Override
	protected final C30SDKObject createChildOrderItems() { return null; }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {

    }

}
