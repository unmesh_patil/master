/*
 * C30OrderItem.java
 *
 * Created on December 7, 2006, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;



import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * The lightweight order item is a pre-configured template for a Kenan/FX order item
 * and is associated with a lightweight service order.  Kenan order items consist of
 * an action (i.e. add, change, disconnect) and a Kenan object (i.e. product, external_id,
 * etc).
 *
 * @author John Reeves
 */
public abstract class C30OrderItem extends C30SDKObject {

    private static Logger log = Logger.getLogger(C30OrderItem.class);

    public static final String ATTRIB_CREATE_DT = "CreateDt";
    public static final String ATTRIB_INCLUDE_ORDER_ITEM = "IncludeOrderItem";
    public static final String ATTRIB_IS_CANCELLED = "IsCancelled";
    public static final String ATTRIB_ITEM_ACTION_ID = "ItemActionId";
    public static final String ATTRIB_ITEM_ID = "ItemId";
    public static final String ATTRIB_ITEM_TYPE_ID = "ItemTypeId";
    public static final String ATTRIB_MEMBER_ID = "MemberId";
    public static final String ATTRIB_MEMBER_INST_ID = "MemberInstId";
    public static final String ATTRIB_MEMBER_INST_ID2 = "MemberInstId2";
    public static final String ATTRIB_MEMBER_TYPE = "MemberType";
    public static final String ATTRIB_ORDER_ID = "OrderId";
    public static final String ATTRIB_ORDER_STATUS_ID = "OrderStatusId";
    public static final String ATTRIB_PACKAGE_INSTANCE_LIST = "PackageInstanceList";
    public static final String ATTRIB_PACKAGE_LINK_ID = "PackageLinkId";
    public static final String ATTRIB_PARENT_ACCOUNT_INTERNAL_ID = "ParentAccountInternalId";
    public static final String ATTRIB_REVISION_FLAG = "RevisionFlag";
    protected static final String ATTRIB_SERVICE_ORDER_ID = "ServiceOrderId";
    public static final String ATTRIB_SKIP_EXT_ID_VALIDATION = "SkipExtIdValidation";
    public static final String ATTRIB_VIEW_ID = "ViewId";
    public static final String ATTRIB_VIEW_ID2 = "ViewId2";
    public static final String ATTRIB_WORKFLOW_ID = "WorkflowId";
    public static final String ATTRIB_WORKFLOW_START_DT = "WorkflowStartDt";

    protected static final String ATTRIB_DEPENDENCY_DEPENDENCY_TYPE = "DependencyType";
    protected static final String ATTRIB_DEPENDENCY_ENTITY_TYPE_ID = "EntityTypeId";
    protected static final String ATTRIB_DEPENDENCY_DEPENDENT_ENTITY_TYPE_ID = "DependentEntityTypeId";
    protected static final String ATTRIB_DEPENDENCY_ENTITY_PK = "EntityPk";
    protected static final String ATTRIB_DEPENDENCY_DEPENDENT_ENTITY_PK = "DependentEntityPk";
    
    /** Order Item member types. **/
    public static final Integer ORD_ITEM_PRODUCT_MEMBER_TYPE = new Integer(10);
    public static final Integer ORD_ITEM_NRC_MEMBER_TYPE = new Integer(20);
    public static final Integer ORD_ITEM_CONTRACT_MEMBER_TYPE = new Integer(30);
    public static final Integer ORD_ITEM_COMPONENT_MEMBER_TYPE = new Integer(40);
    public static final Integer ORD_ITEM_PACKAGE_MEMBER_TYPE = new Integer(50);
    public static final Integer ORD_ITEM_SERVICE_EXT_ID_MEMBER_TYPE = new Integer(60);
    public static final Integer ORD_ITEM_INVENTORY_MEMBER_TYPE = new Integer(70);
    public static final Integer ORD_ITEM_ADDRESS_ASSOC_MEMBER_TYPE = new Integer(78);
    public static final Integer ORD_ITEM_SERVICE_MEMBER_TYPE = new Integer(80);

    /** LWO special member types. **/
    public static final Integer ORD_ITEM_CONTAINER_MEMBER_TYPE = new Integer(-100);
    public static final Integer ORD_ITEM_PRODUCT_OVERRIDE_MEMBER_TYPE = new Integer(-10);
    public static final Integer ORD_ITEM_SERVICE_ORDER_NOTE_MEMBER_TYPE = new Integer(-20);

    /** Integer Values for Item Action Ids. **/
    public static final Integer ORD_ITEM_CONNECT_ITEM_ACTION_ID = new Integer(10);
    public static final Integer ORD_ITEM_CHANGE_ITEM_ACTION_ID = new Integer(20);
    public static final Integer ORD_ITEM_DISCONNECT_ITEM_ACTION_ID = new Integer(30);
    public static final Integer ORD_ITEM_FIND_ITEM_ACTION_ID = new Integer(-10);

    /** Order Item Levels. **/
    public static final Boolean ORD_ITEM_SERVICE_LEVEL = Boolean.TRUE;
    public static final Boolean ORD_ITEM_ACCOUNT_LEVEL = Boolean.FALSE;

   /** Pending View status for Kenan FX APIs. */
    public static final Integer FX_PENDING_VIEW_STATUS = new Integer(1);

    /**
     * Creates a new instance of C30OrderItem.
     * @param factory
     * @param objectType
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    protected C30OrderItem(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * Creates a new instance of C30OrderItem.
     * @param factory
     * @param objectClass
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    protected C30OrderItem(final C30SDKFrameworkFactory factory, final Class objectClass) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectClass);
    }

    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {
     
      getFactory().setClassFieldConfiguration(this,"com.cycle30.sdk.object.kenan.ordering.C30OrderItem");
      
    }

    /**
     * removes the attributes for this objects.  Used when order-based objects are used in 
     * a data retrieval mode.
     * @throws C30SDKInvalidAttributeException
     */
    protected void clearOrderAttributes()
        throws C30SDKInvalidAttributeException
    {
        getFactory().removeClassFieldConfiguration(this,"com.cycle30.sdk.object.kenan.ordering.C30OrderItem");
    }

    /**
     * Creates a copy of the C30OrderItem object.
     * @return A copy of the C30OrderItem object
     */
    @Override
	public Object clone() {
        return super.clone();
    }

    @Override
	public C30SDKObject process() throws  C30SDKException {
    	log.debug("Starting C30OrderItem.process()");

    	//in case there is an error.
        String error = "creating lightweight order item:\n\tLW Order Item Type: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKObject lwo = null;
    	try {

    		// make sure we want to include this order item. It could be
    		// the case that sometimes we do not want to add the order item.
    		if (((Boolean) this.getAttributeValue(ATTRIB_INCLUDE_ORDER_ITEM, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {

	    		this.setAttributeValue(ATTRIB_PARENT_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30Order.class, C30Order.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

	    	    if (this.getAttributeValue(ATTRIB_ITEM_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null){

	                this.setAttributesFromNameValuePairs();
	                this.validateAttributes();
	                lwo = this.createFXObject();
	                
	                if (!(lwo instanceof C30SDKExceptionMessage)) {
		                this.createOrderItemInDB();
		                lwo = this.createChildOrderItems();
	                }

	            }
    		}

            //if we do not have an exception
            if (!(lwo instanceof C30SDKExceptionMessage)) {
            	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            	lwo = this;

            //if we have an exception.
            } else {
        		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "while " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }

    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
    		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "Error " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}
    	log.debug("Finished C30OrderItem.process()");
        return lwo;
    }

    /**
     * Method used to create order item in the database. This is used to create order
     * items for sub-order items where the view rows have already been created.
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    protected void createOrderItemInDB() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        //create order item with attributes
        Map orderItemData = new HashMap();
        
        setOrderItemAttributes(orderItemData);

        Map callResponse = this.queryData("Item", "Create", orderItemData, getAccountServerId());
        Map orderItem = (HashMap) callResponse.get("Item");
        populateOrderItemOutput(orderItem);
    }

    /**
     * Initilize attirbutes for the Kenan FX order item that is about to be created through API TS.
     * @param orderItemData the map holding the order item attributes
     * @throws C30SDKInvalidAttributeException if an attribute was in an invalid state during service order creation
     */
    protected void setOrderItemAttributes(final Map orderItemData) throws C30SDKInvalidAttributeException {
        log.debug("setOrderItemAttributes.");
         //---------------------------------------------------------------
         // Set required Attributes:
         //----------------------------------------------------------------------    
        
        orderItemData.put(ATTRIB_MEMBER_TYPE,this.getAttributeValue(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        orderItemData.put(ATTRIB_ITEM_ACTION_ID,this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        orderItemData.put(ATTRIB_MEMBER_ID,this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        orderItemData.put(ATTRIB_VIEW_ID,this.getAttributeValue(ATTRIB_VIEW_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        orderItemData.put(ATTRIB_MEMBER_INST_ID,this.getAttributeValue(ATTRIB_MEMBER_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        orderItemData.put(ATTRIB_MEMBER_INST_ID2,this.getAttributeValue(ATTRIB_MEMBER_INST_ID2, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

 
        //-----------------------------------------
        // set the remaining order item attributes
        //-----------------------------------------
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
	        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
	        while (iterator.hasNext()) {
	            String attributeName = (String) iterator.next();
	            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();
	
	            if (attributeValue != null) {
	                if (attributeName.equals(ATTRIB_ORDER_STATUS_ID)) {
	                    orderItemData.put(ATTRIB_ORDER_STATUS_ID, attributeValue);
	                } else if (attributeName.equals(ATTRIB_WORKFLOW_START_DT)) {
	                    orderItemData.put(ATTRIB_WORKFLOW_START_DT, attributeValue);
	                } else if (attributeName.equals(ATTRIB_SKIP_EXT_ID_VALIDATION)) {
	                    orderItemData.put(ATTRIB_SKIP_EXT_ID_VALIDATION, attributeValue);
	                } else if (attributeName.equals(ATTRIB_REVISION_FLAG)) {
	                    orderItemData.put(ATTRIB_REVISION_FLAG, attributeValue);
	                }
	            }
	        }
        }
        
        // get the service order ID.  Remember that it could come from a parent service
        // order or it could be input because we are creating a stand-alone order item.
        if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
        	orderItemData.put(ATTRIB_SERVICE_ORDER_ID, this.getAttributeValue(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        } else {
        	orderItemData.put(ATTRIB_SERVICE_ORDER_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }
        
    } 

    /**
     * Method which populates the order item with the currently available order item hashmap.
     * @throws C30SDKInvalidAttributeException if there is a problem setting the attribute values.
     */
    protected void populateOrderItemOutput(final Map orderItem) throws C30SDKInvalidAttributeException {
    
       
       
        for  ( Iterator it=orderItem.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                 Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                 String key = (String)entry.getKey (  ) ; 
                 if (key.equalsIgnoreCase("KEY"))
                 {    
                   HashMap keyValue = (HashMap)entry.getValue (  ) ; 
                   if (keyValue.containsKey(ATTRIB_ITEM_ID)){
                       if (isAttribute(ATTRIB_ITEM_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                           setAttributeValue(ATTRIB_ITEM_ID, ((HashMap)orderItem.get("Key")).get(ATTRIB_ITEM_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        }
                 }
                 else
                 {
                    Object value = entry.getValue (  ) ;
                    if (value != null)
                    if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                        setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                    
                    if (value != null)
                        log.debug(  key+" " +  value ) ; 
                } 
        }
       
       
        /*
        this.setAttributeValue(ATTRIB_ITEM_ID, ((HashMap) orderItem.get("Key")).get(ATTRIB_ITEM_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_CREATE_DT, orderItem.get(ATTRIB_CREATE_DT), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_IS_CANCELLED, orderItem.get(ATTRIB_IS_CANCELLED), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ITEM_ACTION_ID, orderItem.get(ATTRIB_ITEM_ACTION_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ITEM_TYPE_ID, orderItem.get(ATTRIB_ITEM_TYPE_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MEMBER_ID, orderItem.get(ATTRIB_MEMBER_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MEMBER_INST_ID, orderItem.get(ATTRIB_MEMBER_INST_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, orderItem.get(ATTRIB_MEMBER_INST_ID2), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_MEMBER_TYPE, orderItem.get(ATTRIB_MEMBER_TYPE), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_SERVICE_ORDER_ID, orderItem.get(ATTRIB_SERVICE_ORDER_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_VIEW_ID, orderItem.get(ATTRIB_VIEW_ID), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_VIEW_ID2, orderItem.get(ATTRIB_VIEW_ID2), Constants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_WORKFLOW_ID, orderItem.get(ATTRIB_WORKFLOW_ID), Constants.ATTRIB_TYPE_OUTPUT);

    	if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	this.setAttributeValue(ATTRIB_ORDER_STATUS_ID, orderItem.get(ATTRIB_ORDER_STATUS_ID), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_WORKFLOW_START_DT, orderItem.get(ATTRIB_WORKFLOW_START_DT), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_SKIP_EXT_ID_VALIDATION, orderItem.get(ATTRIB_SKIP_EXT_ID_VALIDATION), Constants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_REVISION_FLAG, orderItem.get(ATTRIB_REVISION_FLAG), Constants.ATTRIB_TYPE_OUTPUT);
        }
        */
    }

    /**
     * Method to get the type of the object being serialized.  This method simply creates an XML style 
     * representation of this type.
     * @return a quasi-xml based string which describes the object.
     * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
     */
    @Override
	public String getSerializeType() throws C30SDKInvalidAttributeException {

    	return "\"OrderItem\" SubType=\"" + this.getAttributeValue(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + "\"" 
        	+ " ItemActionId=\"" + this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + "\""
        	+ " MemberId=\"" + this.getAttributeValue(ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + "\"";
    	
    }

    /**
     * Abstract method that allows polymorphic creation of each orderable
     * item view row.  this is to be defined in the derived class such as
     * Service, CIEM etc. It returns an order item object with the corresponding
     * member inst id, member inst id 2 and view id that are necessary for
     * order item creation
     * @throws C30SDKKenanFxCoreException 
     * @throws C30SDKException 
     */
    protected abstract C30SDKObject createFXObject() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException, C30SDKException;

    /**
     * Abstract method that creates all child order items for a given lw order
     * item type.
     * @return C30SDKObject the returned child order items.
     * @throws C30SDKObjectException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidConfigurationException
     * @throws C30SDKException 
     */
    protected abstract C30SDKObject createChildOrderItems() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKException;

}
