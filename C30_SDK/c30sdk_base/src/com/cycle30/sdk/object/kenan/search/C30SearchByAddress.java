/*
 * C30SDKAddressSearch.java
 *
 * Created on June 8, 2011, 8:00 AM
 *
 */

package com.cycle30.sdk.object.kenan.search;


import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.object.kenan.ordering.C30Service;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;

/**
 * Wrapper class encapsulating a Kenan/FX Address object and its corresponding order item.
 * @author Tom Ansley
 */
public class C30SearchByAddress extends C30CrossObjectSearch {

    private static Logger log = Logger.getLogger(C30SearchByAddress.class);

    public static final String ATTRIB_CITY = "City";
    public static final String ATTRIB_COUNTY = "County";
    public static final String ATTRIB_EXTENDED_POSTAL_CODE = "ExtendedPostalCode";
     public static final String ATTRIB_HOUSE_NUMBER = "HouseNumber";
    public static final String ATTRIB_HOUSE_NUMBER_SUFFIX = "HouseNumberSuffix";
    public static final String ATTRIB_POSTAL_CODE = "PostalCode";
    public static final String ATTRIB_POSTFIX_DIRECTIONAL = "PostfixDirectional";
     public static final String ATTRIB_PREFIX_DIRECTIONAL = "PrefixDirectional";
    public static final String ATTRIB_STATE = "State";
    public static final String ATTRIB_STREET_NAME = "StreetName";
    public static final String ATTRIB_STREET_SUFFIX = "StreetSuffix";
    public static final String ATTRIB_UNIT_NO = "UnitNo";
    public static final String ATTRIB_UNIT_TYPE = "UnitType";
    
    public static final String GET_ADDRESS_OBJECTS_SP_CALL = "c30_sdk_get_address_objects";  
	//public static final String GET_ADDRESS_OBJECTS_SP_CALL = "ps_get_address_objects";  
    
    public static String address1 = "";
    public static String address2 = "";
    public static String city = "";
    public static String zip = "";
    public static String state = "";
    public static String county = "";

    /**
     * Creates a new instance of C30ServiceAddressAssoc.  This order item and object is only available through
     * the framework.  It is used in conjunction with the C30Service object when moving services.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SearchByAddress(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * Creates a new instance of C30ServiceAddressAssoc.  This order item and object is only available through
     * the framework.  It is used in conjunction with the C30Service object when moving services.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30SearchByAddress(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30SearchByAddress.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Address Association Order Item.
     * @throws C30SDKInvalidAttributeException thrown if an attribute is invalid.
     */
    @Override
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException{
        super.initializeAttributes();
        getFactory().setClassFieldConfiguration(this,"com.cycle30.sdk.object.kenan.search.C30SearchByAddress");
        
    }
    
    /** (non-Javadoc).
     * @return C30SDKObject the returned service that was processed.
     * @throws C30SDKObjectException 
     * @throws C30SDKObjectConnectionException 
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#process()
     */
    @Override
	public final C30SDKObject conductSearch() throws C30SDKInvalidAttributeException {
        C30SDKObject lwo = null;

        try {
               // validate what we are searching for
                this.validateSearchAttributes();
               
               // construct the address search strings from the input parameters
               this.buildSearchString();
               // do the search and return the lwo object containing all the results
               lwo =  this.getAddressSearchObjects(this.getAttributeValue(ATTRIB_SEARCH_FOR, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString().toUpperCase());
    		
            if (lwo == null)  {
                    setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    return this;
            }
                
    	} catch (Exception e) {	
            C30SDKException er = new C30SDKException(exceptionResourceBundle.getString("LW-OBJ-021"),e,"LW-OBJ-021");
    		lwo = this.createExceptionMessage(er);
        }
    	
    	return lwo;

    }
    
    
    private void buildSearchString() throws C30SDKInvalidAttributeException{
        // set address1 
         if  (isAttribute(ATTRIB_HOUSE_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                getAttributeValue(ATTRIB_HOUSE_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
              address1 = ((String)getAttributeValue(ATTRIB_HOUSE_NUMBER, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase()+" ";
              
        if  (isAttribute(ATTRIB_HOUSE_NUMBER_SUFFIX,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
               getAttributeValue(ATTRIB_HOUSE_NUMBER_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
             address1 = address1 +((String)getAttributeValue(ATTRIB_HOUSE_NUMBER_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase()+" ";

        if  (isAttribute(ATTRIB_PREFIX_DIRECTIONAL,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
               getAttributeValue(ATTRIB_PREFIX_DIRECTIONAL, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
            address1 = address1 +((String)getAttributeValue(ATTRIB_PREFIX_DIRECTIONAL, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase()+" ";

        if  (isAttribute(ATTRIB_STREET_NAME,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
               getAttributeValue(ATTRIB_STREET_NAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
            address1 = address1 +((String)getAttributeValue(ATTRIB_STREET_NAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase()+" ";

        if  (isAttribute(ATTRIB_STREET_SUFFIX,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
               getAttributeValue(ATTRIB_STREET_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
            address1 = address1 +((String)getAttributeValue(ATTRIB_STREET_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase()+" ";
  
        if  (isAttribute(ATTRIB_POSTFIX_DIRECTIONAL,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
               getAttributeValue(ATTRIB_POSTFIX_DIRECTIONAL, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
            address1 = address1 +((String)getAttributeValue(ATTRIB_STREET_SUFFIX, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase();
        address1 = address1.trim();   
             
       // set address2      
        if  (isAttribute(ATTRIB_UNIT_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
               getAttributeValue(ATTRIB_UNIT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
             address2 =  ((String)getAttributeValue(ATTRIB_UNIT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase()+" ";
        if  (isAttribute(ATTRIB_UNIT_NO,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
              getAttributeValue(ATTRIB_UNIT_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
            address2 = address2 + ((String)getAttributeValue(ATTRIB_UNIT_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).toUpperCase();
       
        address2 = address2.trim();  
        
    }
    
    
    private C30SDKObject getAddressSearchObjects(String objectSearchType)
        throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException
    {
        log.debug("In getAddressSearchObjects");
        
            C30SDKObject lwo = null;
            String procCall;
            procCall = GET_ADDRESS_OBJECTS_SP_CALL;

            C30ExternalCallDef call = new C30ExternalCallDef(procCall);
            C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId());   
        try
        {
               // C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId());   
                // set input parameter values
                  call.addParam(new C30CustomParamDef("OBJECT_TYPE", 2, 10, 1));
                  call.addParam(new C30CustomParamDef("ADDRESS1", 2, 10, 1));
                  call.addParam(new C30CustomParamDef("ADDRESS2", 2, 10, 1));
                  call.addParam(new C30CustomParamDef("CITY", 2, 10, 1));
                  call.addParam(new C30CustomParamDef("COUNTY", 2, 10, 1));
                  call.addParam(new C30CustomParamDef("STATE", 2, 10, 1));
                  call.addParam(new C30CustomParamDef("ZIP", 2, 10, 1));  
                // set output parameters  
                
                 call.addParam(new C30CustomParamDef("ACCOUNT_NO", 1, 10, 2));
                 
                 if (objectSearchType.equalsIgnoreCase("SERVICE")){
                  call.addParam(new C30CustomParamDef("SERVICE_NO", 2, 18, 2));
                 }
                 
                ArrayList paramValues = new ArrayList();

                paramValues.add(objectSearchType);
                paramValues.add(address1);    
                paramValues.add(address2);    
                if  (isAttribute(ATTRIB_CITY,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                           getAttributeValue(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
                   paramValues.add(getAttributeValue(ATTRIB_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT)); 
                else 
                    paramValues.add("");
                    
                if  (isAttribute(ATTRIB_COUNTY,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                               getAttributeValue(ATTRIB_COUNTY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
                       paramValues.add(getAttributeValue(ATTRIB_COUNTY, C30SDKValueConstants.ATTRIB_TYPE_INPUT)); 
                else 
                       paramValues.add("");
                       
                if  (isAttribute(ATTRIB_STATE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                                   getAttributeValue(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
                           paramValues.add(getAttributeValue(ATTRIB_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)); 
                else 
                           paramValues.add("");   
                           
                if  (isAttribute(ATTRIB_POSTAL_CODE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                                       getAttributeValue(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
                               paramValues.add(getAttributeValue(ATTRIB_POSTAL_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)); 
                else 
                               paramValues.add("");   
                               
                dataSource.queryData(call, paramValues, 2);
                log.debug("Total row count = " + dataSource.getRowCount()); 
                
                if(dataSource.getRowCount() > 0)
                {
                
                    if (objectSearchType.equalsIgnoreCase("ACCOUNT")) 
                        lwo = processAccounts(dataSource);
                   
                    if (objectSearchType.equalsIgnoreCase("SERVICE")) 
                        lwo = processServices(dataSource);     
                    
                }
                
                  
                }
                catch(Exception e)
                {
                    String error = e.getMessage();
                    throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-022"),"LW-OBJ-022");
                }
                finally {
                try 
                  {
                     dataSource.closeConnection();
                  } catch (Exception ex){
                    log.error("Exception: "+ex.getMessage());
                  }
                }
         return lwo;       
    }
    
    
    private C30SDKObject processAccounts(C30JDBCDataSource dataSource) throws Exception {
        C30SDKObject account = null;
        C30SDKObject cloneLwo = null;
         
         int count = dataSource.getRowCount();
         int lastRecordReturned = 0;
         
         if(count > 0)
         {
             account = getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30Account.class, new Object[0]);
             //Constants.ATTRIB_ITEM_ACTION_ID, Constants.ITEM_ACTION_ID_FIND
             account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_ALL, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             cloneLwo =  (C30Account)account.clone();
         }
         int recordsProcessed = 0;
        
             
              if (startRecord > 0)
                startRecord--;
               for (int i=startRecord; i < count; i++)
               {
                   String accountInternalId = "";
                   String accountServerId = "";
                   String accountNo = "";
                   if (dataSource.getValueAt(i, 0) != null)
                      accountNo = dataSource.getValueAt(i, 0).toString();
                   else
                      continue;
                   // if K2, split the return value into accountInternalId and accountServerId
                   if (accountNo.indexOf(".") > -1){
                       accountInternalId = accountNo.substring(0, accountNo.indexOf("."));
                       accountServerId =  accountNo.substring(accountNo.indexOf(".")+1, accountNo.length());                  
                   }
                   else
                       accountInternalId = accountNo;
                   if (recordsProcessed >= returnSetSize )
                     break;
                     
                   // is this the first account we are processing?
                   if (recordsProcessed == 0){
                        account = retrieveAccount(account,accountInternalId, accountServerId);
                        if (account instanceof C30SDKExceptionMessage) 
                           return account;
                        if  (account.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                             account.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,i + 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                       
                    }
                   else
                   {
                       C30SDKObject lwo =  retrieveAccount((C30Account)cloneLwo.clone(),accountInternalId, accountServerId);
                       if (lwo instanceof C30SDKExceptionMessage) 
                          return lwo;
                      // add to our list of peer objects
                       if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                             lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                     
                       account.addC30SDKObject(lwo);
                   }
                   recordsProcessed++;
                   lastRecordReturned = i +1;
               }
         
        super.setProcessingAggregateTotalsAttributes(account, count, lastRecordReturned, recordsProcessed );
        return account;
    }
    

   /**
     * This method retrieves a specific account by AccountInternalId
     * @param account
     * @param accountInternalId
     * @param accountServerId
     * @return
 * @throws C30SDKException 
     */
    private C30SDKObject retrieveAccount(C30SDKObject account,String accountInternalId, String accountServerId)
        throws C30SDKException
    {
        account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        if (accountServerId.trim().length() > 0)
            if (account.isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, accountServerId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        account = account.process();
        return account;
    }

    private C30SDKObject processServices(C30JDBCDataSource dataSource) throws Exception {
        C30SDKObject service = null;
        C30SDKObject cloneLwo = null;
         
         int count = dataSource.getRowCount();
         int lastRecordReturned = 0;
         
         if(count > 0)
         {
             service = getFactory().createC30SDKObject(C30Service.class, new Object[0]);
             service.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             cloneLwo =  (C30Service)service.clone();
         }
         int recordsProcessed = 0;
        
             
              if (startRecord > 0)
                startRecord--;
               for (int i=startRecord; i < count; i++)
               {
                   String accountInternalId = "";
                   String serviceInternalId = "";
                   String serviceInternalIdResets = "";
                   String serviceId = "";
                   if (recordsProcessed >= returnSetSize )
                     break;
                   
                   if (dataSource.getValueAt(i, 0) != null){
                       accountInternalId = dataSource.getValueAt(i, 0).toString();
                       serviceId = dataSource.getValueAt(i, 1).toString();
                   }
                   else
                      continue;

                   // split the return value into SubscrNo and SubscrNoResets
                   serviceInternalId = serviceId.substring(0, serviceId.indexOf("."));
                   serviceInternalIdResets =  serviceId.substring(serviceId.indexOf(".")+1, serviceId.length());
                     
                   // is this the first account we are processing?
                   if (recordsProcessed == 0){
                        service = retrieveService(service,serviceInternalId, serviceInternalIdResets,accountInternalId);
                        if  (service.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                             service.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,i + 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                        if (service instanceof C30SDKExceptionMessage) 
                           return service;
                    }
                   else
                   {
                       C30SDKObject lwo =  retrieveService((C30Service)cloneLwo.clone(),serviceInternalId, serviceInternalIdResets,accountInternalId);
                      // add to our list of peer objects
                       if (lwo instanceof C30SDKExceptionMessage) 
                          return lwo;
                       if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                             lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                      
                       service.addC30SDKObject(lwo);
                   }
                   recordsProcessed++;
                   lastRecordReturned = i +1;
 
               }
         
        super.setProcessingAggregateTotalsAttributes(service, count, lastRecordReturned, recordsProcessed );
        return service;
    }
    

    /**
     * This method retrieves a specific service by serviceExternalId, serviceExternalIdType and accountInternalId
     * @param service
     * @param accountInternalId
     * @param accountServerId
     * @return
     * @throws C30SDKException 
     */
    private C30SDKObject retrieveService(C30SDKObject service,String serviceInternalId, String serviceInternalIdResets, String accountInternalId)
        throws C30SDKException
    {
    
        service.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, serviceInternalId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        service.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, serviceInternalIdResets, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        service.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalId, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        service = service.process();
        return service;
    }
 
        /**
         * Validates that all the required dataAttributes have been set.
         * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
         */
        protected final void validateSearchAttributes() throws C30SDKInvalidAttributeException {
           String searchObjectType = this.getAttributeValue(ATTRIB_SEARCH_FOR, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString().toUpperCase();
           if (!searchObjectType.equalsIgnoreCase("SERVICE") && !searchObjectType.equalsIgnoreCase("ACCOUNT")){
                    String error = ATTRIB_SEARCH_FOR+ " should be either SERVICE or ACCOUNT to conduct this search";
                throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-040"),"INVALID-ATTRIB-040");
            }
             
        }

    /**
     * Method which gets any data to be cached for further use by the object.
     * @throws C30SDKCacheException if there was an error while trying to populate the service cache.
     */
    @Override
	protected void loadCache() throws C30SDKCacheException {}

}