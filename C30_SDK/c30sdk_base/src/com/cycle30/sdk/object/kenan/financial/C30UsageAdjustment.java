/*
 * C30UsageAdjustment.java
 *
 * Created on July 25, 2008, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.financial;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30KenanMWDataSource;
import com.cycle30.sdk.datasource.C30TableDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.ordering.C30Service;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * 	The C30SDK usage adjustment allows for the adjusting of billed and unbilled usage as well
 * 	as usage in MIU.
 * 
 * @author Tom Ansley
 */
public class C30UsageAdjustment extends C30SDKObject implements Cloneable {
    
    /** Holds the usage information used to make the actual usage adjustment call. */
    private Map usage;
    /** Holds the usage and adjustment information used to make the actual usage adjustment call. */
    private Map usageAdjustment;
    /** The response from the usage adjustment. */
    private Map adjustment;
    /** The type of adjustment being used. This information is retrieved using the usage */
    private Map adjustmentType;
    /** Holds the invoices which are retrieved in order to get bill numbers.  This speeds up usage finds massively. */
    private Object[] invoiceList;
    private Integer usageType = null;
    
    public static final Integer BILLED_USAGE = new Integer(1);
    public static final Integer UNBILLED_USAGE = new Integer(2);
    public static final Integer MIU_USAGE = new Integer(3);
    
    private static Logger log = Logger.getLogger(C30UsageAdjustment.class);
    
    /**
     * Creates a new instance of C30UsageAdjustment.
     * @param factory the factory which is the parent of this object.
     * @param objectType Type of C30SDK usage adjustment
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30UsageAdjustment(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /**
     * Initializes the base attributes that can be set for any Kenan/FX usage adjustment.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
     */
    protected void initializeAttributes() throws C30SDKInvalidAttributeException {

    	//=============================
    	// DATA ATTRIBUTES
    	//=============================
    	
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_EFFECTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_TARGET, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID2, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        C30SDKAttribute attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID, Integer.class);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV, Integer.class);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_TYPE, Integer.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FRAUD_INDICATOR, Integer.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REQUEST_STATUS, Integer.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ADJ_REASON_CODE, Integer.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ANNOTATION_ADJUSTMENT, String.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT, BigInteger.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, String.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, Integer.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MIU_ANNOTATION, String.class);
        attribute.setValue("Usage updated through LWO process");
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_MIU_ADJUSTED, Boolean.class);
        attribute.setValue("false");
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE, C30SDKObject.class);
        attribute.setOverrideable(false);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_DT, Date.class);
        attribute.setFormat("yyyy-MM-dd HH:mm:ss");
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {    	
            attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_ADJUSTMENT, Integer.class);
            attribute.setRequired(true);
            this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_ADJUSTMENT, BigInteger.class);
            attribute.setRequired(true);
            this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
    	//=============================
    	// OUTPUT ATTRIBUTES
    	//=============================
        
        //Output Attributes For Adjustment
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ADJ_REASON_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ANNOTATION_ADJUSTMENT, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CHG_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CITY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COMB_TAX_RATE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COUNTY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CURRENT_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DISCOUNT_AMT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_EFFECTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FEDERAL_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FRAUD_INDICATOR, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_MIU_ADJUSTED, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_LAST_REVIEWED_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_INVOICE_ROW, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID2, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_PROVIDER_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_SPLIT_ROW_NUM, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_SUBMITTER_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_SUBTYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_TRANS_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_OTHER_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PENDING_FLAG, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_ADJUSTMENT, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATED_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REQUEST_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REVIEW_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_STATE_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SUPERVISOR_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TAX_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TAX_PKG_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TOTAL_AMT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_COUNTER, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANSACT_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USAGE_TYPE_ADJUSTED, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        //Output Attributes For Billed/Unbilled Usage
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ADD_IMPLIED_DECIMAL, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCESS_REGION_ORIGIN, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCESS_REGION_TARGET, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_AGGR_USAGE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_AMOUNT_REDUCTION, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_AMOUNT_REDUCTION_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_AMOUNT_USAGE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ANNOTATION_USAGE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ARCH_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BASE_AMT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILL_AGGR_LEVEL, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILL_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILLING_UNITS_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CCARD_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CCARD_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CDR_DATA_PARTITION_KEY, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CDR_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CELL_ID_ORIGIN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COMP_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COMPONENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CONSOLIDATE_USAGE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CORRIDOR_PLAN_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COUNTRY_CODE_ORIGIN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COUNTRY_CODE_TARGET, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_CUSTOMER_TAG, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DERIVE_DISTANCE_UNITS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DERIVE_JURISDICTION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_CODE_USAGE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DISTANCE_UNITS_INDICATOR, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DURATION_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_EQUIP_CLASS_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_EXT_TRACKING_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FILE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FILE_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FOREIGN_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_FREE_USG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_GUIDE_TO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_GUIDE_TO_PROVIDER, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_PRERATED, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_JURISDICTION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_KEEP_RUNNING_TOTAL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID2, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_MSG_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NETWORK_DELAY_AFTER, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NETWORK_DELAY_BEFORE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NO_BILL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NUM_RECORDS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIG_TYPE_ID_USG, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ORIGIN_COUNTRY_DIAL_CODE_REQ, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_USAGE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PROVIDER_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_CLASS_ORIGIN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_CLASS_TARGET, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_CATEGORY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_ID_ORIGIN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_ID_TARGET, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_TARGET, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_TAX_CODE_TYPE_ORIGIN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_POINT_TAX_CODE_TYPE_TARGET, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATABLE_UNIT_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATE_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATE_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATE_CURRENCY_LOCATION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATE_MINIMUM_DURATION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATE_PERIOD, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATE_PERIOD_ROUNDING, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATED_UNITS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RATING_METHOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RAW_UNITS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RAW_UNITS_ROUNDED, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_RAW_UNITS_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REV_RCV_COST_CTR, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ROUNDING_METHOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SECOND_UNITS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SEQNUM_RATE_USAGE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SEQNUM_RATE_USAGE_OVERRIDES, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SPLIT_ROW_NUM, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TAX_CODE_ORIGIN_REQ, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TAX_CODE_TARGET_REQ, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TAX_LOCATION_USG, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TAX_PKG_COUNT, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_THIRD_UNITS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TIMEZONE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TYPE_ID_USG, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_UNITS_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_UNITS_INDICATOR, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_UNROUNDED_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_BILL_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_COMPONENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_DEFAULT_RATE_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_EQUIP_TYPE_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_JURISDICTION, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_PROVIDER_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_CLASS_OF_SERVICE_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_DISTANCE_BAND_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_EQUIP_CLASS_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_POINT_CLASS_ORIGIN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_POINT_CLASS_TARGET, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_RATE_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_RATE_PERIOD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USE_ZONE_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_USG_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_VH_MINOR_THRESHOLD, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ZONE_CLASS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        //Output Attributes For Adjustment Type
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ALLOW_INTERIM_BILL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILLING_CATEGORY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILLING_LEVEL, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_CODE_ADJUSTMENT_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_GROUP_ADJUSTMENT_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_TEXT_ADJUSTMENT_TYPE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_ADJUSTABLE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_DISCONNECT_CREDIT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_DISPLAYED_ON_BILL, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_JOURNALABLE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_LATE_FEE_EXEMPT_ADJUSTMENT_TYPE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_MODIFIABLE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_NEGATIVE_BILL_ADJ, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_REFINANCE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_VIEWABLE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TAX_ON_INVOICE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_CATEGORY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_TRANS_SIGN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }
  
    /**
     * Creates a copy of the C30UsageAdjustment object.
     * @return A copy of the C30UsageAdjustment object
     */
    public Object clone() {
        C30UsageAdjustment copy = (C30UsageAdjustment) super.clone();        
        return copy;        
    }

    /**
	 * Method to process the usage adjustment.
	 * @return C30SDKObject 
     */
    public C30SDKObject process(){
        
    	C30SDKObject lwo = null;
    	try {
    		lwo = createUsageAdjustment();
    		
    		//make sure an error was not received.
    		if (lwo instanceof C30Service) {
	    		populateResponse();
	    		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    		lwo = this;
    		}
    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}  
              
    	return lwo;
    }
    
    /**
     * @return
     * @throws C30SDKException 
     */
    private C30SDKObject getService() throws C30SDKException {
    	log.debug("Starting C30UsageAdjustment.getService");

    	C30SDKObject lwo = null;
    	
    	//try get service from object children
    	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
    		if (this.getC30SDKObject(i) instanceof C30Service) {
    			lwo = this.getC30SDKObject(i);
    		}
    	}
    	
    	//if no service in children then get account from middleware
    	if (lwo == null) {
	    	lwo = this.getFactory().createC30SDKObject(C30Service.class, new Object[0]);
        	lwo.setAttributeValue(C30Service.ATTRIB_ITEM_ACTION_ID, C30Service.ORD_ITEM_FIND_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    	lwo.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    	lwo.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		    this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE, lwo, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	    	lwo = lwo.process();
    	}
    	log.debug("Finished C30UsageAdjustment.getService");
    	return lwo;
    }
    
    /**
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void getServiceInvoices() throws C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
    	
    	//Create a HashMap that holds the filter info
    	HashMap invoiceFilter = new HashMap();
        Map key = new HashMap();
        invoiceFilter.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found object
    	invoiceFilter.put("Fetch", Boolean.TRUE);

    	C30SDKObject service = (C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	
		Map attributeFilter = new HashMap();
		attributeFilter.put("Equal", service.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
		invoiceFilter.put("AccountInternalId", attributeFilter);

		attributeFilter = new HashMap();
		attributeFilter.put("GreaterThanEqual", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		invoiceFilter.put("StatementDate", attributeFilter);
		
	    Map callResponse = this.queryData("Invoice", "Find", invoiceFilter, getAccountServerId());

   	    invoiceList = (Object[]) callResponse.get("InvoiceList");
        
    }
    
    /**
     * Method to get the usage that needs to be adjusted.  The method first tries to find billed usage.  If none is
     * found then it tries to search for unbilled usage.  Currently, if more than one piece of usage is found then
     * an error is thrown.  This will hopefully change in the future as it is possible to work with adjusting more
     * than one piece of usage at a time.
     * @return the service associated with the usage
     * @throws Exception 
     */
    private C30SDKObject findUsage() throws Exception {
    	
    	C30SDKObject lwo = null;
    	//----------------------------------------------------------
    	// In order to make the usage searches as quick as possible 
    	// we need to get the service of each piece of usage.  The 
    	// usage indices use subscr_no, subscr_no_resets.
    	//----------------------------------------------------------
        try {
        	lwo = getService();
        
        	//check an exception was not thrown
        	if (lwo instanceof C30Service) {
		    	usage = null;
		    	
		    	//----------------
		    	// UNBILLED USAGE
		    	//----------------
		    	usage = getUnbilledUsage();
		    	
		    	//----------------
		    	// BILLED USAGE
		    	//----------------
		    	if (usage == null) {
		            getServiceInvoices();
		    		usage = getBilledUsage();
		    	}
	
		    	//----------------
		    	// MIU USAGE
		    	//----------------
		    	if (usage == null) {
		    		adjustMIUUsage();
		    	}
		    	
		    	//-----------------------------------------------
		    	// If no usage exists as billed, unbilled or MIU
		    	//-----------------------------------------------
		    	if (usage == null && !((Boolean) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_MIU_ADJUSTED, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {
		    		String error = "Neither billed, unbilled or MIU usage was found with the given search criteria.\n";
		    		if (this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID) != null && ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID)).getValue() != null) {
		    			error = error + "Service External ID: " + ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID)).getValue() + "\n";
		    		}
		    		if (this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE) != null && ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE)).getValue() != null) {
		    			error = error + "Service External ID Type: " + ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE)).getValue() + "\n";
		    		}
		    		if (this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN) != null && ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN)).getValue() != null) {
		    			error = error + "Point Origin: " + ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN)).getValue() + "\n";
		    		}
		    		if (this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_POINT_TARGET) != null && ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_POINT_TARGET)).getValue() != null) {
		    			error = error + "Point Target: " + ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_POINT_TARGET)).getValue() + "\n";
		    		}
		            throw new C30SDKObjectException(exceptionResourceBundle.getString("CONN-023"),"CONN-023");
		    	}
        	}
    	
	    //if we cannot find a service then an exception gets thrown.  But, we could 
	    //still have usage in MIU.  If this is the case then go look in MIU before
	    //doing anything else.
        } catch (C30SDKObjectException e) {
        	if (e.getExceptionCode().equals("CONN-017")) {
        		adjustMIUUsage();
        		
        		if (!((Boolean) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_MIU_ADJUSTED, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue()) {
        			throw e;
        		}
        	} else {
        		throw e;
        	}
        }
        return lwo;
    }
    
    /**
     * Method to adjust MIU records which were found with the given criteria.
     * @throws Exception 
     */
    private void adjustMIUUsage() throws Exception {
    	//C30ExternalCallDef call = new C30ExternalCallDef("ps_adjust_miu_usage");
		C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_adjust_miu_usage");

    	try {

            //------------------------------------------
            // set up the stored procedure to be called
            //------------------------------------------
            call.addParam(new C30CustomParamDef("external_id", C30CustomParamDef.STRING, 10, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("external_id_type", C30CustomParamDef.INT, 6, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("amount", C30CustomParamDef.STRING, 18, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("trans_dt", C30CustomParamDef.STRING, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("point_origin", C30CustomParamDef.STRING, 10, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("point_target", C30CustomParamDef.STRING, 10, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("annotation", C30CustomParamDef.STRING, 400, C30CustomParamDef.INPUT));
            call.addParam(new C30CustomParamDef("msg_id", C30CustomParamDef.INT, 10, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("msg_id2", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
            call.addParam(new C30CustomParamDef("msg_id_serv", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));

        } catch (Exception e) {
            throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-026"),"INVALID-ATTRIB-026");
        }

        //--------------------------------
        // set the input parameter values
        //--------------------------------
        ArrayList paramValues = new ArrayList();

        paramValues.add(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	    paramValues.add(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        paramValues.add(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		java.text.SimpleDateFormat date = new java.text.SimpleDateFormat("yyyyMMdd HH:mm:ss");
        paramValues.add(date.format(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_DT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)));

        if(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	        paramValues.add(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		} else {
			paramValues.add("");
		}
		if(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_TARGET, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	        paramValues.add(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_TARGET, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		} else {
			paramValues.add("");
		}

		if(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_MIU_ANNOTATION, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
	        paramValues.add(this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_MIU_ANNOTATION, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		} else {
			paramValues.add("");
		}

		//---------------------------------------
        // make the call and process the results
        //---------------------------------------
        //C30KenanMWDataSource dataSource = this.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES, ADMIN_SERVER_ID);
		/*C30KenanMWDataSource dataSource = this.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES, C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
        
        if (dataSource.getRowCount() > 0) {
	    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID, dataSource.getStringAt(0, 0), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID2, dataSource.getStringAt(0, 1), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID_SERV, dataSource.getStringAt(0, 2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
	    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_MIU_ADJUSTED, "true", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	usageType = MIU_USAGE;
        }
*/
    }
    
    /**
     * Method to get billed usage based on given attributes.
     * @return a hashmap containing the billed usage information.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
     * @throws C30SDKObjectException (10002) if more than one piece of usage was found
     * @throws C30SDKKenanFxCoreException 
     */
    private Map getBilledUsage() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
    	
    	HashMap attributeFilter;
    	String attributeName;
    	Object attributeValue;
    	HashMap usageFilter;
    	Map key;
        //-----------------------------------------------------
        // Now that we have the invoices that this usage could 
        // be a part of we can start looking for the usage.
        //-----------------------------------------------------
        for (int i = 0; i < invoiceList.length; i++) {
        
	        //Create a HashMap that holds the filter info
	    	usageFilter = new HashMap();
	        key = new HashMap();
	        usageFilter.put("Key", key);
	
	    	//Set the Fetch flag at the root level,
	    	//so the call will return all fields for found object
	    	usageFilter.put("Fetch", Boolean.TRUE);
	
	    	//----------------------------------------------------------------------
	    	// This is where we add the bill no and resets that we retrieved above.
	    	//----------------------------------------------------------------------
    		attributeFilter = new HashMap();
			attributeFilter.put("Equal", ((HashMap) ((HashMap) invoiceList[i]).get("Key")).get(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO));
			key.put(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, attributeFilter);
    		
    		attributeFilter = new HashMap();
			attributeFilter.put("Equal", ((HashMap) ((HashMap) invoiceList[i]).get("Key")).get(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS));
			key.put(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, attributeFilter);
    		
    		//----------------------------------------------------------------------
	    	// Filter on C30SDK Attributes.
	    	//----------------------------------------------------------------------
	    	Iterator iter = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
	    	while (iter.hasNext()) {
	    		attributeName = (String) iter.next();
	    		attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();
	    		attributeFilter = new HashMap();
	  
	    		if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID) && attributeValue != null) {
	    			attributeFilter.put("Equal", attributeValue);
	    			usageFilter.put(attributeName, attributeFilter);
	    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE) && attributeValue != null) {
	    			attributeFilter.put("Equal", attributeValue);
	    			usageFilter.put(attributeName, attributeFilter);
	    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_TRANS_DT) && attributeValue != null) {
					attributeFilter.put("Equal", attributeValue);
	    			usageFilter.put(attributeName, attributeFilter);
	    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN) && attributeValue != null) {
	    			attributeFilter.put("Equal", attributeValue);
	    			usageFilter.put(attributeName, attributeFilter);
	    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_POINT_TARGET) && attributeValue != null) {
	    			attributeFilter.put("Equal", attributeValue);
	    			usageFilter.put(attributeName, attributeFilter);
	    		}
	    	}
	
		    Map callResponse = this.queryData("BilledUsage", "Find", usageFilter, getAccountServerId());
	
	        //get the count
	        int count = ( (Integer) callResponse.get("Count")).intValue();
	
	        if (count == 1) {
	    	    usage = (HashMap) ((Object[]) callResponse.get("BilledUsageList"))[0];
	    	    populateUsage();
	    	    usageType = BILLED_USAGE;
	        } else if (count > 1) {
	    		
	            usageType = BILLED_USAGE;
	            throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-005"),"LW-OBJ-005");
	        } 
	        
        }
	    return usage;
    }
    
    /**
     * Method to get unbilled usage based on given attributes.
     * @return a hashmap containing the unbilled usage information.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
     * @throws C30SDKObjectException (10003) if more than one piece of usage was found
     * @throws C30SDKKenanFxCoreException 
     */
    private Map getUnbilledUsage() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
    	
    	//Create a HashMap that holds the filter info
    	HashMap attributeFilter;
    	HashMap usageFilter = new HashMap();
        Map key = new HashMap();
        usageFilter.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found object
    	usageFilter.put("Fetch", Boolean.TRUE);

    	//----------------------------------------------------------------------
    	// This is where we add the bill no and resets that we retrieved above.
    	//----------------------------------------------------------------------
    	C30SDKObject service = (C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	
		attributeFilter = new HashMap();
		attributeFilter.put("Equal", service.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
		usageFilter.put(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, attributeFilter);
		
		attributeFilter = new HashMap();
		attributeFilter.put("Equal", service.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
		usageFilter.put(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, attributeFilter);
		
    	//----------------------------------------------------------------------
    	// Filter on C30SDK Attributes.
    	//----------------------------------------------------------------------
    	Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
    	String attributeName;
    	Object attributeValue;

    	while (iterator.hasNext()) {
    		attributeName = (String) iterator.next();
    		attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();
    		attributeFilter = new HashMap();
  
    		if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID) && attributeValue != null) {
    			attributeFilter.put("Equal", attributeValue);
    			usageFilter.put(attributeName, attributeFilter);
    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE) && attributeValue != null) {
    			attributeFilter.put("Equal", attributeValue);
    			usageFilter.put(attributeName, attributeFilter);
    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_TRANS_DT) && attributeValue != null) {
    			attributeFilter.put("Equal", attributeValue);
    			usageFilter.put(attributeName, attributeFilter);
    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN) && attributeValue != null) {
    			attributeFilter.put("Equal", attributeValue);
    			usageFilter.put(attributeName, attributeFilter);
    		} else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_POINT_TARGET) && attributeValue != null) {
    			attributeFilter.put("Equal", attributeValue);
    			usageFilter.put(attributeName, attributeFilter);
    		}
    	}

	    //Map callResponse = this.queryData("UnbilledUsage", "Find", usageFilter, CATALOG_SERVER_ID);
	    Map callResponse = this.queryData("UnbilledUsage", "Find", usageFilter, C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 1) {
    	    usage = (HashMap) ((Object[]) callResponse.get("UnbilledUsageList"))[0];
    	    populateUsage();
    	    usageType = UNBILLED_USAGE;
        } else if (count > 1) {
    		usageType = UNBILLED_USAGE;
            throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-006"),"LW-OBJ-006");
        } 

	    return usage;
    }
    
    /**
     * @param adjustment
     * @throws C30SDKInvalidAttributeException
     */
    private void populateUsageAdjustment(final Map adjustment) throws C30SDKInvalidAttributeException {
    	
        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRACKING_ID, ((HashMap) adjustment.get("Key")).get(C30SDKAttributeConstants.ATTRIB_TRACKING_ID).toString(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV, ((HashMap) adjustment.get("Key")).get(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_SERV).toString(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ADJ_REASON_CODE, adjustment.get(C30SDKAttributeConstants.ATTRIB_ADJ_REASON_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ANNOTATION_ADJUSTMENT, adjustment.get(C30SDKAttributeConstants.ATTRIB_ANNOTATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, adjustment.get(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, adjustment.get(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CHG_DATE, adjustment.get(C30SDKAttributeConstants.ATTRIB_CHG_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CHG_WHO, adjustment.get(C30SDKAttributeConstants.ATTRIB_CHG_WHO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CITY_TAX, adjustment.get(C30SDKAttributeConstants.ATTRIB_CITY_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_COMB_TAX_RATE, adjustment.get(C30SDKAttributeConstants.ATTRIB_COMB_TAX_RATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_COUNTY_TAX, adjustment.get(C30SDKAttributeConstants.ATTRIB_COUNTY_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE, adjustment.get(C30SDKAttributeConstants.ATTRIB_CURRENCY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CURRENT_FLAG, adjustment.get(C30SDKAttributeConstants.ATTRIB_CURRENT_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DISCOUNT_AMT, adjustment.get(C30SDKAttributeConstants.ATTRIB_DISCOUNT_AMT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_EFFECTIVE_DATE, adjustment.get(C30SDKAttributeConstants.ATTRIB_EFFECTIVE_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FEDERAL_TAX, adjustment.get(C30SDKAttributeConstants.ATTRIB_FEDERAL_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FRAUD_INDICATOR, adjustment.get(C30SDKAttributeConstants.ATTRIB_FRAUD_INDICATOR), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_LAST_REVIEWED_NAME, adjustment.get(C30SDKAttributeConstants.ATTRIB_LAST_REVIEWED_NAME), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_INVOICE_ROW, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_INVOICE_ROW), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_NO, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_RESETS, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_BILL_REF_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID2, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID_SERV, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_MSG_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_PROVIDER_ID, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_PROVIDER_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_SPLIT_ROW_NUM, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_SPLIT_ROW_NUM), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_SUBMITTER_NAME, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_SUBMITTER_NAME), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_SUBTYPE, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_SUBTYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_TRANS_CODE, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_TRANS_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_TYPE, adjustment.get(C30SDKAttributeConstants.ATTRIB_ORIG_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_OTHER_TAX, adjustment.get(C30SDKAttributeConstants.ATTRIB_OTHER_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PENDING_FLAG, adjustment.get(C30SDKAttributeConstants.ATTRIB_PENDING_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_ADJUSTMENT, adjustment.get(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_TYPE, adjustment.get(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATED_FLAG, adjustment.get(C30SDKAttributeConstants.ATTRIB_RATED_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_REQUEST_STATUS, adjustment.get(C30SDKAttributeConstants.ATTRIB_REQUEST_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_REVIEW_DATE, adjustment.get(C30SDKAttributeConstants.ATTRIB_REVIEW_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_STATE_TAX, adjustment.get(C30SDKAttributeConstants.ATTRIB_STATE_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SUPERVISOR_NAME, adjustment.get(C30SDKAttributeConstants.ATTRIB_SUPERVISOR_NAME), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TAX_CLASS, adjustment.get(C30SDKAttributeConstants.ATTRIB_TAX_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TAX_PKG_INST_ID, adjustment.get(C30SDKAttributeConstants.ATTRIB_TAX_PKG_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TOTAL_AMT, adjustment.get(C30SDKAttributeConstants.ATTRIB_TOTAL_AMT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_COUNTER, adjustment.get(C30SDKAttributeConstants.ATTRIB_TRACKING_ID_COUNTER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_CODE, adjustment.get(C30SDKAttributeConstants.ATTRIB_TRANS_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANSACT_DATE, adjustment.get(C30SDKAttributeConstants.ATTRIB_TRANSACT_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }
    
    /**
     * @throws C30SDKInvalidAttributeException
     */
    private void populateUsage() throws C30SDKInvalidAttributeException {
    	
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID, ((HashMap) usage.get("Key")).get(C30SDKAttributeConstants.ATTRIB_MSG_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID2, ((HashMap) usage.get("Key")).get(C30SDKAttributeConstants.ATTRIB_MSG_ID2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_MSG_ID_SERV, ((HashMap) usage.get("Key")).get(C30SDKAttributeConstants.ATTRIB_MSG_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, usage.get(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, usage.get(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SPLIT_ROW_NUM, ((HashMap) usage.get("Key")).get(C30SDKAttributeConstants.ATTRIB_SPLIT_ROW_NUM), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ELEMENT_ID, usage.get(C30SDKAttributeConstants.ATTRIB_ELEMENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_UNITS_INDICATOR, usage.get(C30SDKAttributeConstants.ATTRIB_UNITS_INDICATOR), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, usage.get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID, usage.get(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_RATE_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FILE_ID, usage.get(C30SDKAttributeConstants.ATTRIB_FILE_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_GUIDE_TO, usage.get(C30SDKAttributeConstants.ATTRIB_GUIDE_TO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_COMP_STATUS, usage.get(C30SDKAttributeConstants.ATTRIB_COMP_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID, usage.get(C30SDKAttributeConstants.ATTRIB_OPEN_ITEM_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RAW_UNITS, usage.get(C30SDKAttributeConstants.ATTRIB_RAW_UNITS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USG_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_USG_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_DEFAULT_RATE_TYPE, usage.get(C30SDKAttributeConstants.ATTRIB_USE_DEFAULT_RATE_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATABLE_UNIT_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_RATABLE_UNIT_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CDR_STATUS, usage.get(C30SDKAttributeConstants.ATTRIB_CDR_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_ID_TARGET, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_ID_TARGET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CUSTOMER_TAG, usage.get(C30SDKAttributeConstants.ATTRIB_CUSTOMER_TAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_JURISDICTION, usage.get(C30SDKAttributeConstants.ATTRIB_USE_JURISDICTION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_JURISDICTION, usage.get(C30SDKAttributeConstants.ATTRIB_JURISDICTION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_REDUCTION_ID, usage.get(C30SDKAttributeConstants.ATTRIB_AMOUNT_REDUCTION_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_EXT_TRACKING_ID, usage.get(C30SDKAttributeConstants.ATTRIB_EXT_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_TAX_CODE_TYPE_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_TAX_CODE_TYPE_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CCARD_ID, usage.get(C30SDKAttributeConstants.ATTRIB_CCARD_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_EQUIP_CLASS_CODE, usage.get(C30SDKAttributeConstants.ATTRIB_EQUIP_CLASS_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_BILL_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_USE_BILL_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_NO_BILL, usage.get(C30SDKAttributeConstants.ATTRIB_NO_BILL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_BILL_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_DT, usage.get(C30SDKAttributeConstants.ATTRIB_RATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_GEOCODE, usage.get(C30SDKAttributeConstants.ATTRIB_GEOCODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CDR_DATA_PARTITION_KEY, usage.get(C30SDKAttributeConstants.ATTRIB_CDR_DATA_PARTITION_KEY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_USAGE, usage.get(C30SDKAttributeConstants.ATTRIB_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TAX_LOCATION_USG, usage.get(C30SDKAttributeConstants.ATTRIB_TAX_LOCATION_USG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_GUIDE_TO_PROVIDER, usage.get(C30SDKAttributeConstants.ATTRIB_GUIDE_TO_PROVIDER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ANNOTATION_USAGE, usage.get(C30SDKAttributeConstants.ATTRIB_ANNOTATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_AGGR_USAGE_ID, usage.get(C30SDKAttributeConstants.ATTRIB_AGGR_USAGE_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TAX_PKG_COUNT, usage.get(C30SDKAttributeConstants.ATTRIB_TAX_PKG_COUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_CLASS_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_CLASS_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_PROVIDER_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_USE_PROVIDER_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_REV_RCV_COST_CTR, usage.get(C30SDKAttributeConstants.ATTRIB_REV_RCV_COST_CTR), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_COUNTRY_CODE_TARGET, usage.get(C30SDKAttributeConstants.ATTRIB_COUNTRY_CODE_TARGET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ADD_IMPLIED_DECIMAL, usage.get(C30SDKAttributeConstants.ATTRIB_ADD_IMPLIED_DECIMAL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_NETWORK_DELAY_BEFORE, usage.get(C30SDKAttributeConstants.ATTRIB_NETWORK_DELAY_BEFORE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_CODE_USAGE, usage.get(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CELL_ID_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_CELL_ID_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PRERATED, usage.get(C30SDKAttributeConstants.ATTRIB_IS_PRERATED), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TAX_CODE_TARGET_REQ, usage.get(C30SDKAttributeConstants.ATTRIB_TAX_CODE_TARGET_REQ), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_ELEMENT_ID, usage.get(C30SDKAttributeConstants.ATTRIB_USE_ELEMENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RAW_UNITS_TYPE, usage.get(C30SDKAttributeConstants.ATTRIB_RAW_UNITS_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CORRIDOR_PLAN_ID, usage.get(C30SDKAttributeConstants.ATTRIB_CORRIDOR_PLAN_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FREE_USG, usage.get(C30SDKAttributeConstants.ATTRIB_FREE_USG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, usage.get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_ID_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_ID_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_CURRENCY_CODE, usage.get(C30SDKAttributeConstants.ATTRIB_RATE_CURRENCY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DERIVE_JURISDICTION, usage.get(C30SDKAttributeConstants.ATTRIB_DERIVE_JURISDICTION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SEQNUM_RATE_USAGE_OVERRIDES, usage.get(C30SDKAttributeConstants.ATTRIB_SEQNUM_RATE_USAGE_OVERRIDES), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_USAGE, usage.get(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_COMPONENT_ID, usage.get(C30SDKAttributeConstants.ATTRIB_USE_COMPONENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, usage.get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_DT, usage.get(C30SDKAttributeConstants.ATTRIB_TRANS_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_COMPONENT_ID, usage.get(C30SDKAttributeConstants.ATTRIB_COMPONENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CCARD_ID_SERV, usage.get(C30SDKAttributeConstants.ATTRIB_CCARD_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_COUNTRY_CODE_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_COUNTRY_CODE_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_EQUIP_TYPE_CODE, usage.get(C30SDKAttributeConstants.ATTRIB_USE_EQUIP_TYPE_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_UNROUNDED_AMOUNT, usage.get(C30SDKAttributeConstants.ATTRIB_UNROUNDED_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DERIVE_DISTANCE_UNITS, usage.get(C30SDKAttributeConstants.ATTRIB_DERIVE_DISTANCE_UNITS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_ZONE_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_USE_ZONE_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_POINT_CLASS_TARGET, usage.get(C30SDKAttributeConstants.ATTRIB_USE_POINT_CLASS_TARGET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_CLASS_OF_SERVICE_CODE, usage.get(C30SDKAttributeConstants.ATTRIB_USE_CLASS_OF_SERVICE_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_KEEP_RUNNING_TOTAL, usage.get(C30SDKAttributeConstants.ATTRIB_KEEP_RUNNING_TOTAL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DURATION_FLAG, usage.get(C30SDKAttributeConstants.ATTRIB_DURATION_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PROVIDER_ID, usage.get(C30SDKAttributeConstants.ATTRIB_PROVIDER_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TAX_CODE_ORIGIN_REQ, usage.get(C30SDKAttributeConstants.ATTRIB_TAX_CODE_ORIGIN_REQ), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_EQUIP_CLASS_CODE, usage.get(C30SDKAttributeConstants.ATTRIB_USE_EQUIP_CLASS_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FOREIGN_AMOUNT, usage.get(C30SDKAttributeConstants.ATTRIB_FOREIGN_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_PERIOD_ROUNDING, usage.get(C30SDKAttributeConstants.ATTRIB_RATE_PERIOD_ROUNDING), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_TARGET, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_TARGET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_EXTERNAL_ID, usage.get(C30SDKAttributeConstants.ATTRIB_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_CATEGORY, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_CATEGORY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_CONSOLIDATE_USAGE, usage.get(C30SDKAttributeConstants.ATTRIB_CONSOLIDATE_USAGE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS, usage.get(C30SDKAttributeConstants.ATTRIB_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DISTANCE_UNITS_INDICATOR, usage.get(C30SDKAttributeConstants.ATTRIB_DISTANCE_UNITS_INDICATOR), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_UNITS_CURRENCY_CODE, usage.get(C30SDKAttributeConstants.ATTRIB_UNITS_CURRENCY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCESS_REGION_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_ACCESS_REGION_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_PERIOD, usage.get(C30SDKAttributeConstants.ATTRIB_RATE_PERIOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BASE_AMT, usage.get(C30SDKAttributeConstants.ATTRIB_BASE_AMT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIG_TYPE_ID_USG, usage.get(C30SDKAttributeConstants.ATTRIB_ORIG_TYPE_ID_USG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TIMEZONE, usage.get(C30SDKAttributeConstants.ATTRIB_TIMEZONE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_ID, usage.get(C30SDKAttributeConstants.ATTRIB_TRANS_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_POINT_CLASS_ORIGIN, usage.get(C30SDKAttributeConstants.ATTRIB_USE_POINT_CLASS_ORIGIN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ARCH_FLAG, usage.get(C30SDKAttributeConstants.ATTRIB_ARCH_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCESS_REGION_TARGET, usage.get(C30SDKAttributeConstants.ATTRIB_ACCESS_REGION_TARGET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_AGGR_LEVEL, usage.get(C30SDKAttributeConstants.ATTRIB_BILL_AGGR_LEVEL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_NUM_RECORDS, usage.get(C30SDKAttributeConstants.ATTRIB_NUM_RECORDS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_CLASS_TARGET, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_CLASS_TARGET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_POINT_TAX_CODE_TYPE_TARGET, usage.get(C30SDKAttributeConstants.ATTRIB_POINT_TAX_CODE_TYPE_TARGET), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_RATE_PERIOD, usage.get(C30SDKAttributeConstants.ATTRIB_USE_RATE_PERIOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, usage.get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SECOND_UNITS, usage.get(C30SDKAttributeConstants.ATTRIB_SECOND_UNITS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_RATE_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_USE_RATE_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SEQNUM_RATE_USAGE, usage.get(C30SDKAttributeConstants.ATTRIB_SEQNUM_RATE_USAGE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_AMOUNT_REDUCTION, usage.get(C30SDKAttributeConstants.ATTRIB_AMOUNT_REDUCTION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_THIRD_UNITS, usage.get(C30SDKAttributeConstants.ATTRIB_THIRD_UNITS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_VH_MINOR_THRESHOLD, usage.get(C30SDKAttributeConstants.ATTRIB_VH_MINOR_THRESHOLD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_CURRENCY_LOCATION, usage.get(C30SDKAttributeConstants.ATTRIB_RATE_CURRENCY_LOCATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RAW_UNITS_ROUNDED, usage.get(C30SDKAttributeConstants.ATTRIB_RAW_UNITS_ROUNDED), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ZONE_CLASS, usage.get(C30SDKAttributeConstants.ATTRIB_ZONE_CLASS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ROUNDING_METHOD, usage.get(C30SDKAttributeConstants.ATTRIB_ROUNDING_METHOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILLING_UNITS_TYPE, usage.get(C30SDKAttributeConstants.ATTRIB_BILLING_UNITS_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_FILE_ID_SERV, usage.get(C30SDKAttributeConstants.ATTRIB_FILE_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ORIGIN_COUNTRY_DIAL_CODE_REQ, usage.get(C30SDKAttributeConstants.ATTRIB_ORIGIN_COUNTRY_DIAL_CODE_REQ), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USE_DISTANCE_BAND_ID, usage.get(C30SDKAttributeConstants.ATTRIB_USE_DISTANCE_BAND_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TYPE_ID_USG, usage.get(C30SDKAttributeConstants.ATTRIB_TYPE_ID_USG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_NETWORK_DELAY_AFTER, usage.get(C30SDKAttributeConstants.ATTRIB_NETWORK_DELAY_AFTER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_MINIMUM_DURATION, usage.get(C30SDKAttributeConstants.ATTRIB_RATE_MINIMUM_DURATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, usage.get(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATING_METHOD, usage.get(C30SDKAttributeConstants.ATTRIB_RATING_METHOD), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_RATED_UNITS, usage.get(C30SDKAttributeConstants.ATTRIB_RATED_UNITS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_EXTERNAL_ID_TYPE, usage.get(C30SDKAttributeConstants.ATTRIB_EXTERNAL_ID_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }
    
    /**
     * @throws C30SDKInvalidAttributeException
     */
    private void populateResponse() throws C30SDKInvalidAttributeException {
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_USAGE_TYPE_ADJUSTED, usageType, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_MIU_ADJUSTED, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_MIU_ADJUSTED, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }
    
    /**
     * Creates the Kenan/FX usage adjustment using API-TS calls.  This method ensures that either the billed or unbilled usage is adjusted.
     * The type of usage does not need to be specified by the application.  If no usage is found then an error is thrown.
     * @return C30SDKObject the returned usage adjustment
     * @throws Exception 
     */
    private C30SDKObject createUsageAdjustment() throws Exception {
    	
    	C30SDKObject lwo = null;
    	
    	//-------------------------------
    	// find the usage to be adjusted
    	//-------------------------------
    	setAttributesFromNameValuePairs();
    	
        validateAttributes();
        lwo = findUsage();

        if (lwo instanceof C30Service) {
        
	        //-----------------------------------------------------
	        // set all attributes into the usage to be adjusted if 
	        // the usage has not been determined to be in MIU
	        //-----------------------------------------------------
	        if (!usageType.equals(MIU_USAGE)) {
	        	setUsageAdjustmentAttributes();
	        
		        postPopulateUsageValidate();
		
		        //-------------------------------------------------------
		        // adjust the usage and get the newly created adjustment
		        //-------------------------------------------------------
		        Map callResponse = null;
		        if (usageType.equals(BILLED_USAGE)) {
		        	callResponse = this.queryData("BilledUsageAdjust", usageAdjustment, getAccountServerId());
		        } else {
		        	callResponse = this.queryData("UnbilledUsageAdjust", usageAdjustment, getAccountServerId());
		        }
		        
		        adjustment = (HashMap) callResponse.get("Adjustment");
		    	
		        populateUsageAdjustment(adjustment);
	        }
	        
        }
        return lwo;
    }
    
    /**
     * Method to get the maximum amount that an adjustment can be made for on a piece of usage.  This
     * value also has the tax calculated for that piece of usage as well.
     * @return the maximum amount that can be adjusted.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
     * @throws C30SDKKenanFxCoreException 
     */
    private BigInteger getAdjustmentMaximumAmount() throws C30SDKObjectConnectionException, C30SDKKenanFxCoreException {
        Map callResponse = null;
    	HashMap usageAmountGet = new HashMap();
    	BigInteger amount = null;
        if (usageType.equals(BILLED_USAGE)) {
        	usageAmountGet.put("BilledUsage", usage);
        	callResponse = this.queryData("BilledUsageAmountGet", usageAmountGet, getAccountServerId());
        	amount = (BigInteger) ((HashMap) callResponse.get("BilledUsageAmount")).get("AdjustmentEligibleAmount");
        } else {
        	usageAmountGet.put("UnbilledUsage", usage);
        	callResponse = this.queryData("UnbilledUsageAmountGet", usageAmountGet, getAccountServerId());
        	amount = (BigInteger) ((HashMap) callResponse.get("UnbilledUsageAmount")).get("AdjustmentEligibleAmount");
        }

        return amount;
        
    }

    /**
     * Method to get the type of adjustment that should be used for the adjustment.  Once the adjustment 
     * has been found this object is populated with all the adjustment type information.
     * @return a hashmap containing the adjustment type information.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
     * @throws C30SDKObjectException (10004) if more than one adjustment type was found
     * @throws C30SDKKenanFxCoreException 
     */
    private Map getAdjustmentType() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
    	
    	//Create a HashMap that holds the filter info
    	HashMap adjTypeFilter = new HashMap();

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found object
    	adjTypeFilter.put("Fetch", Boolean.TRUE);

    	//-------------------------------------------------------------------
    	// Filter on C30SDK Attributes.  Filters that will never change
    	// 1. Target type is always usage.
    	// 2. Adjustment is always credit.
    	// 3. Trans target ID is open item ID of usage.
    	// 4. Is Disconnect credit is false.
    	// 5. Language code is set.
    	//-------------------------------------------------------------------

    	HashMap attributeFilter = new HashMap();
		attributeFilter.put("Equal", new Integer(3));
		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_TYPE, attributeFilter);

    	attributeFilter = new HashMap();
		attributeFilter.put("Equal", new Integer(-1));
		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_TRANS_SIGN, attributeFilter);

    	attributeFilter = new HashMap();
		attributeFilter.put("Equal", usage.get(C30SDKAttributeConstants.ATTRIB_TRANS_ID_USG));
		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_ID, attributeFilter);

    	attributeFilter = new HashMap();
		attributeFilter.put("Equal", C30SDKValueConstants.GLOBAL_LANGUAGE_CODE);
		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_LANGUAGE_CODE, attributeFilter);

        if (usageType.equals(BILLED_USAGE)) {
        	attributeFilter = new HashMap();
    		attributeFilter.put("Equal", new Integer(1));
    		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_TRANS_CATEGORY, attributeFilter);

        	attributeFilter = new HashMap();
    		attributeFilter.put("Equal", new Integer(2));
    		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_BILLING_CATEGORY, attributeFilter);
        } else {
        	attributeFilter = new HashMap();
    		attributeFilter.put("Equal", new Integer(5));
    		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_TRANS_CATEGORY, attributeFilter);

        	attributeFilter = new HashMap();
    		attributeFilter.put("Equal", new Integer(1));
    		adjTypeFilter.put(C30SDKAttributeConstants.ATTRIB_BILLING_CATEGORY, attributeFilter);
        }
        
	    Map callResponse = this.queryData("AdjustmentType", "Find", adjTypeFilter, getAccountServerId());

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 1) {
    	    adjustmentType = (HashMap) ((Object[]) callResponse.get("AdjustmentTypeList"))[0];
        } else {
    		String error = "More than one adjustment type was found for the usage being adjusted.\n";
    		error = error + "TransTargetType = 3 (Usage Charge)\n";
    		error = error + "TransSign = -1 (credit)\n";
    		error = error + "TransTargetId = " + usage.get(C30SDKAttributeConstants.ATTRIB_TRANS_ID_USG) + "\n";
            if (usageType.equals(BILLED_USAGE)) {
        		error = error + "TransCategory = 1(prerated)\n";
        		error = error + "BillingCategory = 2(apply to past changes)\n";
            } else {
        		error = error + "TransCategory = 5 (prebilled)\n";
        		error = error + "BillingCategory = 1(apply to current changes)\n";
            }
            throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-007"),"LW-OBJ-007");		
            
        } 
        
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_CATEGORY, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_TRANS_CATEGORY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_ID, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_CODE_ADJUSTMENT_TYPE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_TEXT_ADJUSTMENT_TYPE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_TEXT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_LATE_FEE_EXEMPT_ADJUSTMENT_TYPE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_LATE_FEE_EXEMPT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILLING_CATEGORY, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_BILLING_CATEGORY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_DISCONNECT_CREDIT, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_DISCONNECT_CREDIT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_GROUP_ADJUSTMENT_TYPE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_DESCRIPTION_GROUP), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_TYPE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_TRANS_TARGET_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TRANS_SIGN, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_TRANS_SIGN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_ADJUSTABLE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_ADJUSTABLE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_TAX_ON_INVOICE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_TAX_ON_INVOICE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_JOURNALABLE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_JOURNALABLE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_REFINANCE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_REFINANCE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_NEGATIVE_BILL_ADJ, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_NEGATIVE_BILL_ADJ), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_VIEWABLE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_VIEWABLE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ALLOW_INTERIM_BILL, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_ALLOW_INTERIM_BILL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_DISPLAYED_ON_BILL, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_DISPLAYED_ON_BILL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_BILLING_LEVEL, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_BILLING_LEVEL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_MODIFIABLE, adjustmentType.get(C30SDKAttributeConstants.ATTRIB_IS_MODIFIABLE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
	    return adjustmentType;
    }
    
    /**
     * Validates that all the required attributes have been set.
     * @throws C30SDKInvalidAttributeException (1000) if any attributes are not correctly validated.
     * @throws C30SDKKenanFxCoreException 
     */    
    protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException { 
        
        super.validateAttributes();

        String error = "In order to process a usage adjustment the following attributes must be set:\n";
		boolean isValid = true;
    	if (!this.isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
			error = error + "(Service External ID) ";
			isValid = false;
		}
    	if (!this.isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_SERVICE_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
			error = error + "(Service External ID Type)";
			isValid = false;
		}
		
		if (!isValid) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-027"),"INVALID-ATTRIB-027");
		}
    	
    }
    
    /**
     * This is the validation that is performed after the usage has been retrieved and the adjustment has been created
     * The reason two validations are required is because we need to perform two actions in order for the adjustment to
     * take place. i.e. getting the usage and creating the adjustment.  So, we need to perform another validation after
     * the usage has been retrieved to ensure that the parameters from the user are valid.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
     * @throws C30SDKObjectException (10004) new adjustment plus previous adjustment exceeds the amount of original charge
     * @throws C30SDKKenanFxCoreException 
     */
    private void postPopulateUsageValidate() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKKenanFxCoreException {
    	BigInteger amount = getAdjustmentMaximumAmount();
    	if ( ((BigInteger) usageAdjustment.get(C30SDKAttributeConstants.ATTRIB_AMOUNT)).compareTo(amount) > 0) {
    		String error = "New adjustment plus previous adjustments exceeds the amount of original charge. \n"
    					  + "Maximum Adjustment Amount: " + amount + "\n"
    					  + "Requested Adjustment Amount: " + usageAdjustment.get(C30SDKAttributeConstants.ATTRIB_AMOUNT);
            throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-008"),"LW-OBJ-008");
    	}
    }
    
    /**
     * Initilize attributes for the Kenan FX usage adjustment that is about to be created through the API TS.
     * @throws C30SDKInvalidAttributeException exception thrown if there was an error setting any attributes.
     * @throws C30SDKKenanFxCoreException 
     */
    private void setUsageAdjustmentAttributes() throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKKenanFxCoreException {
        usageAdjustment = new HashMap();
        //-----------------------------------------------
        // set the usage adjustment attributes
        //-----------------------------------------------
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if (attributeValue != null) {
                if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ANNOTATION_ADJUSTMENT)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_ANNOTATION, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ADJ_REASON_CODE)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_ADJ_REASON_CODE, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_REQUEST_STATUS)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_REQUEST_STATUS, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_FRAUD_INDICATOR)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_FRAUD_INDICATOR, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_TYPE)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_TYPE, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS_ADJUSTMENT)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_PRIMARY_UNITS, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_AMOUNT_ADJUSTMENT)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_AMOUNT, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_BILL_REF_NO, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_BILL_REF_RESETS, attributeValue);
                } else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_EFFECTIVE_DATE)) {
                    usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_EFFECTIVE_DATE, attributeValue);
                }
            }
        }
        
        //---------------------------------------
        // get the adjustment type automatically
        //---------------------------------------
        getAdjustmentType();
        usageAdjustment.put(C30SDKAttributeConstants.ATTRIB_TRANS_CODE, ((HashMap) adjustmentType.get("Key")).get(C30SDKAttributeConstants.ATTRIB_TRANS_CODE));
        
        //------------------------------------------------------
        // set the retrieved piece of usage into the adjustment
        //------------------------------------------------------
        if (usageType.equals(BILLED_USAGE)) {
        	usageAdjustment.put("BilledUsage", usage);
        } else {
        	usageAdjustment.put("UnbilledUsage", usage);
        }
    }
    
    /**
     * Method which gets any data to be cached for further use by the object.
     */
    protected void loadCache() {
    	
    }

}
