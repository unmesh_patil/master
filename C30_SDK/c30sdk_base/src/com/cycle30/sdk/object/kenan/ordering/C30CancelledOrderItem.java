/*
 * C30CancelledOrderItem.java
 *
 * Created on May 19, 2006, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Wrapper class encapsulating a Kenan/FX order item that has been cancelled. This 
 * C30SDK order item is ONLY USED INSIDE CUSTOMER CENTER.  Do not use for any 
 * other LWO requirements.
 * 
 * @author Tom Ansley
 */
public class C30CancelledOrderItem extends C30OrderItem {

    /** Logging. */
    private static Logger log = Logger.getLogger(C30OrderItem.class);

   /** Pending View status for Kenan FX APIs. */
    public static final Integer FX_PENDING_VIEW_STATUS = new Integer(1);

    /**
     * Creates a new instance of C30OrderItem.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30CancelledOrderItem(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always 
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework 
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30CancelledOrderItem(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30CancelledOrderItem.class);
    }
    
    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#initializeAttributes()
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes
     */
    @Override
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException {
    	super.initializeAttributes();
        
    }

    /**
     * This C30SDK order item is ONLY USED INSIDE CUSTOMER CENTER.  Do not use for any other LWO requirements.
     * Creates a copy of the C30OrderItem object.
     * @return A copy of the C30OrderItem object
     */
    @Override
	public final Object clone() {
        return super.clone();
    }

    @Override
	public C30SDKObject process() { 
    	log.debug("Inside C30CancelledOrderItem.process");
    	C30SDKObject lwo = null;
    	try {
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			lwo = this;
		} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}
    	log.debug("Finished C30CancelledOrderItem.process");
    	return lwo;
    }
    
    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#createFXObject()
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     */
    @Override
	protected C30SDKObject createFXObject() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException { return null; }
    
    /** 
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#createChildOrderItems()
     * @return the list of C30SDK child order items.
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     */
    @Override
	protected final C30SDKObject createChildOrderItems() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException { return this; }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
