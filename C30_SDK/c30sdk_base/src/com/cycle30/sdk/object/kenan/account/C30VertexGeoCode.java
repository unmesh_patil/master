package com.cycle30.sdk.object.kenan.account;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKSqlConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * C30SDK C30VertexGeoCode 
 * @author Ranjith Nelluri
 */
public class C30VertexGeoCode extends C30SDKObject implements Cloneable {

	private C30SDKObject account = null;
	protected Map balance = null;

	//List of Valid State full names accepted by Vertex.
	private static final Map<String, String> usStateAndTerritories;
	static {
		usStateAndTerritories = new HashMap<String, String>();

		usStateAndTerritories.put("AL","ALABAMA");
		usStateAndTerritories.put("AK","ALASKA");
		usStateAndTerritories.put("AS","AMERICAN SAMOA");
		usStateAndTerritories.put("AZ","ARIZONA");
		usStateAndTerritories.put("AR","ARKANSAS");
		usStateAndTerritories.put("CA","CALIFORNIA");
		usStateAndTerritories.put("CO","COLORADO");
		usStateAndTerritories.put("CT","CONNECTICUT");
		usStateAndTerritories.put("DE","DELAWARE");
		usStateAndTerritories.put("DC","DISTRICT OF COLUMBIA");
		usStateAndTerritories.put("FL","FLORIDA");
		usStateAndTerritories.put("GA","GEORGIA");
		usStateAndTerritories.put("HI","HAWAII");
		usStateAndTerritories.put("ID","IDAHO");
		usStateAndTerritories.put("IL","ILLINOIS");
		usStateAndTerritories.put("IN","INDIANA");
		usStateAndTerritories.put("IA","IOWA");
		usStateAndTerritories.put("KS","KANSAS");
		usStateAndTerritories.put("KY","KENTUCKY");
		usStateAndTerritories.put("LA","LOUISIANA");
		usStateAndTerritories.put("ME","MAINE");
		usStateAndTerritories.put("MD","MARYLAND");
		usStateAndTerritories.put("MA","MASSACHUSETTS");
		usStateAndTerritories.put("MI","MICHIGAN");
		usStateAndTerritories.put("MN","MINNESOTA");
		usStateAndTerritories.put("MS","MISSISSIPPI");
		usStateAndTerritories.put("MO","MISSOURI");
		usStateAndTerritories.put("MT","MONTANA");
		usStateAndTerritories.put("NE","NEBRASKA");
		usStateAndTerritories.put("NV","NEVADA");
		usStateAndTerritories.put("NH","NEW HAMPSHIRE");
		usStateAndTerritories.put("NJ","NEW JERSEY");
		usStateAndTerritories.put("NM","NEW MEXICO");
		usStateAndTerritories.put("NY","NEW YORK");
		usStateAndTerritories.put("NC","NORTH CAROLINA");
		usStateAndTerritories.put("ND","NORTH DAKOTA");
		usStateAndTerritories.put("OH","OHIO");
		usStateAndTerritories.put("OK","OKLAHOMA");
		usStateAndTerritories.put("OR","OREGON");
		usStateAndTerritories.put("PA","PENNSYLVANIA");
		usStateAndTerritories.put("RI","RHODE ISLAND");
		usStateAndTerritories.put("SC","SOUTH CAROLINA");
		usStateAndTerritories.put("SD","SOUTH DAKOTA");
		usStateAndTerritories.put("TN","TENNESSEE");
		usStateAndTerritories.put("TX","TEXAS");
		usStateAndTerritories.put("UT","UTAH");
		usStateAndTerritories.put("VT","VERMONT");
		usStateAndTerritories.put("VA","VIRGINIA");
		usStateAndTerritories.put("WA","WASHINGTON");
		usStateAndTerritories.put("WV","WEST VIRGINIA");
		usStateAndTerritories.put("WI","WISCONSIN");
		usStateAndTerritories.put("WY","WYOMING");

		usStateAndTerritories.put("FM","F.S. OF MICRONESIA");
		usStateAndTerritories.put("VI","VIRGIN ISLANDS");
		usStateAndTerritories.put("PR","PUERTO RICO");
		usStateAndTerritories.put("PW","PALAU");
		usStateAndTerritories.put("MP","N. MARIANA ISLANDS");
		usStateAndTerritories.put("GU","GUAM");
		usStateAndTerritories.put("MH","MARSHALL ISLANDS");
	}

	
	private static Logger log = Logger.getLogger(C30VertexGeoCode.class);

	/**
	 * Creates a new instance of C30Order.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK payment
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30VertexGeoCode(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/** 
	 * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30ServiceOrder objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30VertexGeoCode(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30VertexGeoCode.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX payment.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	@Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

		setClassFieldConfiguration();

	}

	/**
	 * Creates a copy of the C30Order object.
	 * @return A copy of the C30Order object
	 */
	@Override
	public Object clone() {
		C30VertexGeoCode copy = (C30VertexGeoCode) super.clone();        
		return copy;        
	}

	/**
	 * Method to process the C30Order.
	 * @return C30SDKObject 
	 * @throws C30SDKInvalidAttributeException 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	@Override
	public C30SDKObject process() throws C30SDKInvalidAttributeException {
		validateAttributes();

		C30SDKObject lwo = null;
		try {

			//Find and populate GeoCode
			lwo = findGeoCode();


			//if we got an Geocode rather than an exception
			if (lwo instanceof C30VertexGeoCode) {
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwo = this;
			}

		} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}
	/**
	 * Sets the value of an attribute.
	 * @param attributeName the attribute name who's value is to be set.
	 * @param attributeValue  the value the attribute is to be set to.
	 * @param type the type of attribute being processed.
	 * @throws InvalidAttributeException if there was a problem initializing the attributes of the service order.
	 */
	public final void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
		super.setAttributeValue(attributeName, attributeValue, type);

		initializeaGeoCodeAttributes();		
	}

	/**
	 * Find and returns the GeoCodeList for the given StateCity and Zip Code
	 * @return
	 * @throws C30SDKInvalidAttributeException 
	 * @throws InvalidAttributeException 
	 * @throws LightweightKenanFxException 
	 * @throws LightweightObjectConnectionException 
	 */
	private C30SDKObject findGeoCode() throws C30SDKInvalidAttributeException
	{
		//Reading the input data
		String state =(String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_GEOCODE_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
		String zip =(String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_GEOCODE_ZIP, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String city =(String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_GEOCODE_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer country_code =(Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_GEOCODE_COUNTRYCODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer franchise_code =(Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_GEOCODE_FRANCHISECODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String geoCode= null;
		String countyName= null;
		String apiVersion = null;

		//Making call to C30Vertex Database for the GeoCode Find.
		String query = C30SDKSqlConstants.FIND_GEO_CODE_FROM_LOCGEO;

		//Update Zip and Postal code with correct values
		//if (country_code.equals(Constants.COUNTRY_USA)) //i.e.,840
		state = getVertexUSStateValue(state);
		zip = getVertexUSPostalCode(zip);

		
		C30JDBCDataSource dataSource = null;
		try
		{
			dataSource = C30SDKDataSourceUtils.getDataSourceFromSDKPool();   


			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setString(1,city.toUpperCase());
			ps.setString(2,zip);
			ps.setString(3,zip);
			ps.setString(4,state);

			ResultSet resultSet  = ps.executeQuery();

			if(resultSet.next())
			{
				geoCode= resultSet.getString("GEOCODE");
				countyName= resultSet.getString("COUNTYNAME");
			}
			ps.close();
			C30SDKDataSourceUtils.freeConnectionFromSDKPool(dataSource.getConnection());
		}
		catch(Exception e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("C30SDK-GEO-001"),e,"C30SDK-GEO-001");
		}
		

		HashMap geoCodeMap = new HashMap();
		geoCodeMap.put("Geocode", geoCode);
		if (geoCode == null )
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("C30SDK-GEO-002"),"C30SDK-GEO-002");
		geoCodeMap.put("County", countyName);
		populateGeoCode(geoCodeMap);

		// Get the SDK API Version
		apiVersion = this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SDKAPI_VERSION, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString();

		//Set the SDK API version 
		this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_SDKAPI_VERSION,apiVersion,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);			

		return this;
	}


	private void populateGeoCode(Map geoCode) throws C30SDKInvalidAttributeException
	{
		//populate the response into the LWO Object.
		for  ( Iterator it=geoCode.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   
		{  
			Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
			String key = (String)entry.getKey (  ) ;
			if (key.equalsIgnoreCase("TotalCount")){
				setAttributeValue("TotalCount",geoCode.get("TotalCount"),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);				
			}
			if (key.equalsIgnoreCase("Geocode")){
				setAttributeValue("GeoCode",geoCode.get("Geocode"),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);				
			}
			if (key.equalsIgnoreCase("County")){
				setAttributeValue("County",geoCode.get("County"),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);				
			}
		}

	}

	private void initializeaGeoCodeAttributes() throws C30SDKInvalidAttributeException {
		// TODO Auto-generated method stub
		C30SDKAttribute attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_GEOCODE_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setRequired(true);
		attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_GEOCODE_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setRequired(true);
		attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_GEOCODE_ZIP, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setRequired(true);
	}

	/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 */    
	@Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException { 

	}


	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	@Override
	protected void loadCache() {

	}




	/** This method is to return the state string value that can be sent to Vertex Database.
	 *  If the input is AK then the return value is ALASKA
	 *  If the input is Alaska then the return value is ALASKA.
	 *  
	 *  Currently this is only for the US states.
	 * @param inputUSState
	 * @return
	 */
	private String getVertexUSStateValue(String inputUSState)
	{

		String retValue = "";
		if (inputUSState.length() != 2)
			retValue= inputUSState.toUpperCase();
		else
		{
			retValue= usStateAndTerritories.get(inputUSState.toUpperCase());
		}
		
		return retValue;
	}
	
	/** We can accept both 5 digit and 9 digit postal codes. But vertex only need 5digit postal codes.
	 * 
	 * @param inputUSState
	 * @return
	 * @throws C30SDKInvalidAttributeException 
	 */
	private String getVertexUSPostalCode(String inputUSPostalCode) throws C30SDKInvalidAttributeException
	{

		String retValue = "";
		//Validate Postal Code
		if (!validatePostalCode(inputUSPostalCode))
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-509"),"INVALID-ATTRIB-509");
		
		if (inputUSPostalCode.length() == 5)
			retValue= inputUSPostalCode.toUpperCase();
		else
		{
			retValue= inputUSPostalCode.substring(0, 5);
		}
		
		return retValue;
	}
	
	/** This method is for validating the Postal Code based on the RegEx.
	 * 
	 * @param inputPostalCode
	 * @return
	 */
	private boolean validatePostalCode(String inputPostalCode)
	{
		String postalRegex1 = "^\\d{5}(-\\d{4})?$";
		String postalRegex2 = "^\\d{9}";
		
		return Pattern.matches(postalRegex1, inputPostalCode) ||  Pattern.matches(postalRegex2, inputPostalCode);
	}

}