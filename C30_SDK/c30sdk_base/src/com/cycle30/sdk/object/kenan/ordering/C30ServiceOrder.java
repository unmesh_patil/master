/*
 * C30ServiceOrder.java
 *
 * Created on December 7, 2006, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30HashMapUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * The lightweight service order is an abstract class which is used for building other
 * service order types like the C30AccountServiceOrder and C30ServiceServiceOrder.
 * @author Tom Ansley
 */
public abstract class C30ServiceOrder extends C30SDKObject implements Cloneable {
    
	/** Initialize string values for all attributes that can be set by a client **/    
    public static final String ATTRIB_ANNOTATION = "Annotation";
    public static final String ATTRIB_ACCOUNT_INTERNAL_ID = "AccountInternalId";
    public static final String ATTRIB_COMPLETE_DT = "CompleteDt";
    public static final String ATTRIB_CREATE_DT = "CreateDt";
    public static final String ATTRIB_CREATE_WHO = "CreateWho";
    public static final String ATTRIB_CUST_DESIRED_DATE = "CustDesiredDate";
    public static final String ATTRIB_EXCEPTION_ID = "ExceptionId";
    public static final String ATTRIB_EXPEDITE_FLAG = "ExpediteFlag";
    public static final String ATTRIB_EXTENDED_DATA = "ExtendedData";
    public static final String ATTRIB_GENERATE_WORKFLOW = "GenerateWorkflow";
    public static final String ATTRIB_IS_EXPANDED = "IsExpanded";
    public static final String ATTRIB_IS_SERVICE_LEVEL = "IsServiceLevel";
    public static final String ATTRIB_IS_SUBORDER = "IsSuborder";
    public static final String ATTRIB_ITEM_TYPE_ID = "ItemTypeId";
    public static final String ATTRIB_NESTED_DISCONNECT = "NestedDisconnect";
    public static final String ATTRIB_NEW_BALANCE_ACCOUNT_INTERNAL_ID = "NewBalanceAccountInternalId";
    public static final String ATTRIB_NEW_OPEN_ITEM_ID = "NewOpenItemId";
    public static final String ATTRIB_NEW_POSTPAID_ACCOUNT_INTERNAL_ID = "NewPostpaidAccountInternalId";
    public static final String ATTRIB_NO_CANCEL = "NoCancel";
    public static final String ATTRIB_NO_REVISION = "NoRevision";
    public static final String ATTRIB_OPEN_ITEM_ID = "OpenItemId";
    public static final String ATTRIB_ORDER_ID = "OrderId";
    public static final String ATTRIB_ORDER_STATUS_ID = "OrderStatusId";
    public static final String ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID = "OriginalServiceInternalId";
    public static final String ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID_RESETS = "OriginalServiceInternalIdResets";
    public static final String ATTRIB_PACKAGE_INSTANCE_LIST = "PackageInstanceList";
    public static final String ATTRIB_PARAM_ID = "ParamId";
    public static final String ATTRIB_PARAM_VALUE = "ParamValue";
    public static final String ATTRIB_PIVOT_DATE = "PivotDate";
    public static final String ATTRIB_QUEUE_ENTRY_ID = "QueueEntryId";
    public static final String ATTRIB_REDIRECT_CHARGES_BACK = "RedirectChargesBack";
    public static final String ATTRIB_REDIRECT_CHARGES_TO_BALANCE = "RedirectChargesToBalance";
    public static final String ATTRIB_RELATED_SERVICE_ORDER_ID = "RelatedServiceOrderId";
    public static final String ATTRIB_SERVICE_ORDER_ID = "ServiceOrderId";
    public static final String ATTRIB_SERVICE_ORDER_TYPE_ID = "ServiceOrderTypeId";
    public static final String ATTRIB_STATUS_REASON_ID = "StatusReasonId";
    public static final String ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID = "TransferToAccountInternalId";
    public static final String ATTRIB_TRANSFER_FROM_ACCOUNT_INTERNAL_ID = "TransferFromAccountInternalId";
    public static final String ATTRIB_UPDATE_ALTERNATE_ADDRESS = "UpdateAlternateAddress";
    public static final String ATTRIB_UPDATE_BILLING_ADDRESS = "UpdateBillingAddress";
    public static final String ATTRIB_WAIVE_INSTALLMENT_NRC = "WaiveInstallmentNrc";
    public static final String ATTRIB_WAIVE_REFINANCE_NRC = "WaiveRefinanceNrc";
    public static final String ATTRIB_WAIVE_TERMINATION_OBLIGATION = "WaiveTerminationObligation";
    public static final String ATTRIB_WAIVE_UNBILLED_NRC = "WaiveUnbilledNrc";
    public static final String ATTRIB_WAIVE_UNMET_OBLIGATION = "WaiveUnmetObligation";
    public static final String ATTRIB_WORKFLOW_EST_COMPLETE_DT = "WorkflowEstCompleteDt";
    public static final String ATTRIB_WORKFLOW_START_DT = "WorkflowStartDt";
    public static final String ATTRIB_WORKFLOW_ID = "WorkflowId";
    public static final String ATTRIB_WP_JOB_ID = "WpJobId";
    public static final String ATTRIB_WP_PROCESS_ID = "WpProcessId";

    /** Status indicating that the service order is in progress. */
    public static final Integer SERV_ORD_STATUS_IN_PROGRESS = new Integer(10);
    
    /** Level independent Service Order Type value strings. */
    public static final Integer SERV_ORD_TYPE_CONNECT = new Integer(10);
    /** Level independent Service Order Type value strings. */
    public static final Integer SERV_ORD_TYPE_CHANGE = new Integer(20);
    
    /** Service Order Level 1 = Account. */
    public static final Integer SERV_ORD_ACCOUNT_LEVEL = new Integer(1);
    /** Service Order Level 2 = Service. */
    public static final Integer SERV_ORD_SERVICE_LEVEL = new Integer(2);
     
    private static Logger log = Logger.getLogger(C30ServiceOrder.class);
    
    /**
     * Creates a new instance of C30ServiceOrder.
     * @param factory the factory which is the parent of this object.
     * @param objectType Type of lightweight service order being constructed
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    protected C30ServiceOrder(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /** 
     * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
     * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
     * the framework to create objects.  It is also used by the BRE front end.
     * @param factory Factory used to create C30ServiceOrder objects
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    protected C30ServiceOrder(final C30SDKFrameworkFactory factory, final Class objectClass) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectClass);
    }
    
    /**
     * Initializes the base attributes that can be set for any Kenan/FX service order.
     * @throws C30SDKInvalidAttributeException if an attribute was in an invalid state during initialization
     */
    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{

    	//==================
    	// INPUT ATTRIBUTES
    	//==================
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ANNOTATION, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPLETE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EXCEPTION_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_GENERATE_WORKFLOW, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_EXPANDED, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_SERVICE_LEVEL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ITEM_TYPE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEW_BALANCE_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEW_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEW_POSTPAID_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_STATUS_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INSTANCE_LIST, C30PackageInstanceList.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_QUEUE_ENTRY_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_RELATED_SERVICE_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_STATUS_REASON_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_INSTALLMENT_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_REFINANCE_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_TERMINATION_OBLIGATION, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_UNBILLED_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_UNMET_OBLIGATION, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WORKFLOW_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKAttribute attribute = new C30SDKAttribute(ATTRIB_SERVICE_ORDER_TYPE_ID, Integer.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	this.addAttribute(new C30SDKAttribute(ATTRIB_CUST_DESIRED_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	this.addAttribute(new C30SDKAttribute(ATTRIB_EXPEDITE_FLAG, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_NESTED_DISCONNECT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_NO_CANCEL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_NO_REVISION, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_PIVOT_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_REDIRECT_CHARGES_BACK, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_REDIRECT_CHARGES_TO_BALANCE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_UPDATE_ALTERNATE_ADDRESS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_UPDATE_BILLING_ADDRESS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WORKFLOW_EST_COMPLETE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WORKFLOW_START_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WP_JOB_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WP_PROCESS_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
    	//===================
    	// OUTPUT ATTRIBUTES
    	//===================
    	this.addAttribute(new C30SDKAttribute(ATTRIB_SERVICE_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ANNOTATION, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_COMPLETE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_EXCEPTION_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_GENERATE_WORKFLOW, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_EXPANDED, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_IS_SERVICE_LEVEL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ITEM_TYPE_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEW_BALANCE_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEW_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_NEW_POSTPAID_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_STATUS_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_QUEUE_ENTRY_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_RELATED_SERVICE_ORDER_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_TRANSFER_FROM_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_INSTALLMENT_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_REFINANCE_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_TERMINATION_OBLIGATION, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_UNBILLED_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(ATTRIB_WAIVE_UNMET_OBLIGATION, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	this.addAttribute(new C30SDKAttribute(ATTRIB_CUST_DESIRED_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	this.addAttribute(new C30SDKAttribute(ATTRIB_EXPEDITE_FLAG, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_IS_SUBORDER, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_NESTED_DISCONNECT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_NO_CANCEL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_NO_REVISION, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_PIVOT_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_REDIRECT_CHARGES_BACK, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_REDIRECT_CHARGES_TO_BALANCE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_UPDATE_ALTERNATE_ADDRESS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_UPDATE_BILLING_ADDRESS, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WORKFLOW_EST_COMPLETE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WORKFLOW_START_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WP_JOB_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.addAttribute(new C30SDKAttribute(ATTRIB_WP_PROCESS_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
    }
  
    /**
     * Creates a copy of the C30ServiceOrder object.
     * @return A copy of the C30ServiceOrder object
     */
    @Override
	public Object clone() {
        C30ServiceOrder copy = (C30ServiceOrder) super.clone();        
        return copy;        
    }
    
    /**
     * (non-Javadoc).
     * @see com.cycle30.sdk.object.kenan.ordering.C30OrderItem#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Integer)
     */
    @Override
	public void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
    	super.setAttributeValue(attributeName, attributeValue, type);
    	if (attributeName.equals(ATTRIB_SERVICE_ORDER_TYPE_ID)) {
    		super.loadExtendedDataAttributes("ORD_SERVICE_ORDER", (Integer) this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	}
    }

    /**
     * This method overrides the regular method because checks need to be made for whether the service
     * order type ID is being set.  If the service order type is a package disconnect then we need to
     * ensure that the package instance ID and serv are set as well.
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    @Override
	public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);
    	
    	if (name.equals(this.getAttribute(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
    		super.loadExtendedDataAttributes("ORD_SERVICE_ORDER", (Integer) this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	}
    }
        
    /**
     * Method to populate the service order.
     * @param serviceOrder the service order information used to populate this object
     * @throws C30SDKInvalidAttributeException if an attribute was in an invalid state during object population.
     */
    protected void populateServiceOrder(final Map serviceOrder) throws C30SDKInvalidAttributeException {
        this.setAttributeValue(ATTRIB_SERVICE_ORDER_ID, ((HashMap) serviceOrder.get("Key")).get(ATTRIB_SERVICE_ORDER_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, serviceOrder.get(ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_SERVICE_ORDER_ID, ((HashMap) serviceOrder.get("Key")).get(ATTRIB_SERVICE_ORDER_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, serviceOrder.get(ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ANNOTATION, serviceOrder.get(ATTRIB_ANNOTATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_COMPLETE_DT, serviceOrder.get(ATTRIB_COMPLETE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_CREATE_DT, serviceOrder.get(ATTRIB_CREATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_EXCEPTION_ID, serviceOrder.get(ATTRIB_EXCEPTION_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_GENERATE_WORKFLOW, serviceOrder.get(ATTRIB_GENERATE_WORKFLOW), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_IS_EXPANDED, serviceOrder.get(ATTRIB_IS_EXPANDED), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_IS_SERVICE_LEVEL, serviceOrder.get(ATTRIB_IS_SERVICE_LEVEL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ITEM_TYPE_ID, serviceOrder.get(ATTRIB_ITEM_TYPE_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_NEW_BALANCE_ACCOUNT_INTERNAL_ID, serviceOrder.get(ATTRIB_NEW_BALANCE_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_NEW_OPEN_ITEM_ID, serviceOrder.get(ATTRIB_NEW_OPEN_ITEM_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_NEW_POSTPAID_ACCOUNT_INTERNAL_ID, serviceOrder.get(ATTRIB_NEW_POSTPAID_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_OPEN_ITEM_ID, serviceOrder.get(ATTRIB_OPEN_ITEM_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ORDER_ID, serviceOrder.get(ATTRIB_ORDER_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ORDER_STATUS_ID, serviceOrder.get(ATTRIB_ORDER_STATUS_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID, serviceOrder.get(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID_RESETS, serviceOrder.get(ATTRIB_ORIGINAL_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_QUEUE_ENTRY_ID, serviceOrder.get(ATTRIB_QUEUE_ENTRY_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_RELATED_SERVICE_ORDER_ID, serviceOrder.get(ATTRIB_RELATED_SERVICE_ORDER_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID, serviceOrder.get(ATTRIB_TRANSFER_TO_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_TRANSFER_FROM_ACCOUNT_INTERNAL_ID, serviceOrder.get(ATTRIB_TRANSFER_FROM_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_WAIVE_INSTALLMENT_NRC, serviceOrder.get(ATTRIB_WAIVE_INSTALLMENT_NRC), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_WAIVE_REFINANCE_NRC, serviceOrder.get(ATTRIB_WAIVE_REFINANCE_NRC), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_WAIVE_TERMINATION_OBLIGATION, serviceOrder.get(ATTRIB_WAIVE_TERMINATION_OBLIGATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_WAIVE_UNBILLED_NRC, serviceOrder.get(ATTRIB_WAIVE_UNBILLED_NRC), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(ATTRIB_WAIVE_UNMET_OBLIGATION, serviceOrder.get(ATTRIB_WAIVE_UNMET_OBLIGATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            this.setAttributeValue(ATTRIB_CUST_DESIRED_DATE, serviceOrder.get(ATTRIB_CUST_DESIRED_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            this.setAttributeValue(ATTRIB_CUST_DESIRED_DATE, serviceOrder.get(ATTRIB_CUST_DESIRED_DATE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {        	
        	this.setAttributeValue(ATTRIB_EXPEDITE_FLAG, serviceOrder.get(ATTRIB_EXPEDITE_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_IS_SUBORDER, serviceOrder.get(ATTRIB_IS_SUBORDER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_NESTED_DISCONNECT, serviceOrder.get(ATTRIB_NESTED_DISCONNECT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_NO_CANCEL, serviceOrder.get(ATTRIB_NO_CANCEL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_NO_REVISION, serviceOrder.get(ATTRIB_NO_REVISION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_PIVOT_DATE, serviceOrder.get(ATTRIB_PIVOT_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_PIVOT_DATE, serviceOrder.get(ATTRIB_PIVOT_DATE), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.setAttributeValue(ATTRIB_REDIRECT_CHARGES_BACK, serviceOrder.get(ATTRIB_REDIRECT_CHARGES_BACK), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_REDIRECT_CHARGES_TO_BALANCE, serviceOrder.get(ATTRIB_REDIRECT_CHARGES_TO_BALANCE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_UPDATE_ALTERNATE_ADDRESS, serviceOrder.get(ATTRIB_UPDATE_ALTERNATE_ADDRESS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_UPDATE_BILLING_ADDRESS, serviceOrder.get(ATTRIB_UPDATE_BILLING_ADDRESS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_WORKFLOW_EST_COMPLETE_DT, serviceOrder.get(ATTRIB_WORKFLOW_EST_COMPLETE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_WORKFLOW_START_DT, serviceOrder.get(ATTRIB_WORKFLOW_START_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_WP_JOB_ID, serviceOrder.get(ATTRIB_WP_JOB_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(ATTRIB_WP_PROCESS_ID, serviceOrder.get(ATTRIB_WP_PROCESS_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
    }
    
    /**
     * Commits the Kenan/FX service order using API-TS calls.
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKObjectException
     * @throws C30SDKInvalidAttributeException if an attribute was in an invalid state during commit.
     * @throws C30SDKKenanFxCoreException 
     */
    protected final void commitServiceOrder() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        
        if (this.getAttribute(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {

            log.info("Incrementally committing service order: " + this.getAttributeValue(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        
	        Map serviceOrderData = new HashMap();
            Map key = new HashMap();
	        key.put(ATTRIB_SERVICE_ORDER_ID, this.getAttributeValue(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
	        serviceOrderData.put("Key", key);
	        
	        Map callResponse = this.queryData("ServiceOrder", "Commit", serviceOrderData, getAccountServerId());
	        serviceOrderData = (HashMap) ((Object[]) callResponse.get("ServiceOrderList"))[0];
	        this.populateServiceOrder(serviceOrderData);

        } else {
            String error = "Service order must be created before it can be committed";
            throw new C30SDKObjectException(error);
        }
    }
    
    /**
     * Creates the Kenan/FX service order using API-TS calls.  It also creates all associated order items.
     * @return C30SDKObject the returned service order that was created.
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKInvalidConfigurationException
     * @throws C30SDKKenanFxCoreException 
     */
    protected final C30SDKObject createServiceOrder() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {
       
        //in case there is an error.
        String error = "creating lightweight service order:\n\tLW Service Order Type: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKObject lwo = null;
    	
    	//-------------------------------------------------
    	// if we have not yet created the service order in 
        // the database then do a service order create.
    	//-------------------------------------------------
        if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null){
            
            Map serviceOrder = new HashMap();
            
            this.setAttributesFromNameValuePairs();
            setServiceOrderAttributes(serviceOrder);
            validateAttributes();           

            Map callResponse = this.queryData("ServiceOrder", "Create", serviceOrder, getAccountServerId());
	        serviceOrder = (HashMap) callResponse.get("ServiceOrder");
	        
	        //remove duplicate extended data - this is a hack to fix a core API-TS bug
	        C30HashMapUtils.removeDuplicatesFromArray(serviceOrder, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

	        this.populateServiceOrder(serviceOrder);
	        
	        error = error + "\n\tService Order ID: " + this.getAttributeValue(ATTRIB_SERVICE_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }

        try {
        	
            //----------------------------------------------------------------------------
            // after the service order has been created, loop through all the lightweight 
        	// order items and create them ensuring to do the package order items first.
            //----------------------------------------------------------------------------
        	C30SDKObject lwoi = null;
            //Ensure package order items are created first.
            for (int i = 0; i < this.getC30SDKObjectCount(); i++) {        
                lwoi = this.getC30SDKObject(i);
                if (lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_PACKAGE_MEMBER_TYPE)) {
                    lwoi.setNameValuePairs(this.getNameValuePairMap());
                    lwoi.setAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    //lwoi.validateAttributes(); IF I AM NOT MISTAKEN THIS IS DONE IN THE lwoi.process() METHOD
                    lwoi = lwoi.process();
                    //if the processed service order produced an exception.
                    if (lwoi instanceof C30SDKExceptionMessage) {
                    	break;
                    }
                    
                }
            }
            
            //check that we do not have an exception.
            if (!(lwoi instanceof C30SDKExceptionMessage)) {
            	
	            // After packages have been created create the other types.
	            // This is necessary to ensure package link has the proper dependencies.
	            for (int i = 0; i < this.getC30SDKObjectCount(); i++) {        
	                lwoi = this.getC30SDKObject(i);
	                if (!lwoi.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_PACKAGE_MEMBER_TYPE)) {
	                    lwoi.setNameValuePairs(this.getNameValuePairMap());
	                    lwoi.setAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, this.getAttributeValue(ATTRIB_PACKAGE_INSTANCE_LIST, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	                    //lwoi.validateAttributes(); IF I AM NOT MISTAKEN THIS IS DONE IN THE lwoi.process() METHOD
	                    lwoi = lwoi.process();
	                    //if the processed service order produced an exception.
	                    if (lwoi instanceof C30SDKExceptionMessage) {
	                    	break;
	                    }
	                }
	            }
	            
            }
            
            if (lwoi instanceof C30SDKExceptionMessage) {
            	lwo = lwoi;
        		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "while " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            
        } catch(Exception e) {
			lwo = this.createExceptionMessage(e);
    		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "Error " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        if (!(lwo instanceof C30SDKExceptionMessage)) {
        	lwo = this;
        }
        return lwo;
    }

    /**
     * Validates that all the required attributes have been set.
     * @throws C30SDKInvalidAttributeException if an attribute was in an invalid state during validation
     * @throws C30SDKKenanFxCoreException 
     */    
    @Override
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException { 
        
        super.validateAttributes();

        //---------------------------------------------------------------------------------
        // Validate the order is not cancelled or complete before adding the Service Order
        //---------------------------------------------------------------------------------
    	Integer orderStatusId = (Integer) this.getAttributeValue(C30Order.class, C30Order.ATTRIB_ORDER_STATUS_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        if (orderStatusId.equals(C30Order.ORD_STATUS_CANCELLED)) {   
            String error = "Error: Can not add lightweight service order to order. The order is canceled.";
            throw new C30SDKInvalidAttributeException(error);
        } else if (orderStatusId.equals(C30Order.ORD_STATUS_COMPLETED)) {
            String error = "Error: Can not add lightweight service order to order.  The order is already complete.";
            throw new C30SDKInvalidAttributeException(error);
        }
        
    }
    
    /**
     * Method to get the type of the object being serialized.  This method simply creates an XML style 
     * representation of this type.
     * @return a quasi-xml based string which describes the object.
     * @throws C30SDKInvalidAttributeException if an attribute is invalid during processing
     */
    @Override
	public String getSerializeType() throws C30SDKInvalidAttributeException {
    	
    	return "\"ServiceOrder\" SubType=\"" + this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + "\"";
    	
    }

    /**
     * Initilize attirbutes for the Kenan FX service order that is about to 
     * be created through the API TS.
     * @param serviceOrder Kenan/FX service order object
     * @throws C30SDKInvalidAttributeException if an attribute was in an invalid state during service order creation
     */
    protected void setServiceOrderAttributes(final Map serviceOrder) throws C30SDKInvalidAttributeException {
        
        //---------------------------------------------------------------
        // Set Non-configurable Attributes:
        // Service Order type ID
        // Order Status: In Progress for All New Orders
        // Order Id = Order Id being Created
        // Account Internal ID - Account Id for Order being created. 
        //----------------------------------------------------------------------
        serviceOrder.put(C30ServiceOrder.ATTRIB_SERVICE_ORDER_TYPE_ID, this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        serviceOrder.put(C30Order.ATTRIB_ORDER_STATUS_ID, SERV_ORD_STATUS_IN_PROGRESS);
        serviceOrder.put(C30Order.ATTRIB_ORDER_ID, this.getAttributeValue(C30Order.class, C30Order.ATTRIB_ORDER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        serviceOrder.put(C30Order.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30Order.class, C30Order.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));

 
        //----------------------------------------------------------------------
        // Default Cust desired Date to Now if it is not explicitly set. 
        //----------------------------------------------------------------------
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
	
	        if (!super.isAttributeValueSet(ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
	            serviceOrder.put(ATTRIB_CUST_DESIRED_DATE, new Date());
	        }
	        
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
	        if (!super.isAttributeValueSet(ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) {
	            serviceOrder.put(ATTRIB_PIVOT_DATE, new Date());
	        }
        }
        
        //--------------------------------------------
        // set the remaining service order attributes
        //--------------------------------------------
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if (attributeValue != null) {
                if (attributeName.equals(ATTRIB_ANNOTATION)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_CREATE_WHO)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_CUST_DESIRED_DATE)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_PIVOT_DATE)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_STATUS_REASON_ID)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WAIVE_INSTALLMENT_NRC)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WAIVE_REFINANCE_NRC)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WAIVE_TERMINATION_OBLIGATION)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WAIVE_UNBILLED_NRC)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WAIVE_UNMET_OBLIGATION)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_RELATED_SERVICE_ORDER_ID)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_EXPEDITE_FLAG)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_GENERATE_WORKFLOW)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_NO_CANCEL)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_NO_REVISION)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_REDIRECT_CHARGES_BACK)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_REDIRECT_CHARGES_TO_BALANCE)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_UPDATE_ALTERNATE_ADDRESS)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_UPDATE_BILLING_ADDRESS)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WORKFLOW_EST_COMPLETE_DT)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WORKFLOW_START_DT)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WP_JOB_ID)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else if (attributeName.equals(ATTRIB_WP_PROCESS_ID)) {
                    serviceOrder.put(attributeName, attributeValue);
                } else {
                    //------------------------------------------------------
                    // check if the attribute is an extended data parameter
                    //------------------------------------------------------
                    try {
                        if (attributeValue != null) {
                        	
                        	//check if the attribute is extended data
                            Integer paramId = new Integer(attributeName);
                            
                            //if we do not already have extended data attributes then create an array
                            if (serviceOrder.get(ATTRIB_EXTENDED_DATA) == null) {
                                serviceOrder.put(ATTRIB_EXTENDED_DATA, new Object[1]);
                            
    	                        //create the extended data attribute
    	                        HashMap extData = new HashMap();
    	                        extData.put(ATTRIB_PARAM_ID, paramId);
    	                        extData.put(ATTRIB_PARAM_VALUE, attributeValue);
    	                        
    	                        //add it to the array
    	                        Object[] newDataParams = C30HashMapUtils.addObjectToArray( (Object[]) serviceOrder.get(ATTRIB_EXTENDED_DATA), extData);
    	                        
    	                        //put the array into the serviceOrder data.
    	                        serviceOrder.put(ATTRIB_EXTENDED_DATA, newDataParams);
                            
                            } else {
                                C30HashMapUtils.removeDuplicatesFromArray(serviceOrder, ATTRIB_EXTENDED_DATA, ATTRIB_PARAM_ID);

                                Object [] extendedData = (Object[]) serviceOrder.get(ATTRIB_EXTENDED_DATA);
                                HashMap xdHashMap = new HashMap(((HashMap) extendedData[0]));
      
                                // set the extended data attributes
                                xdHashMap.put(ATTRIB_PARAM_ID, paramId);
                                xdHashMap.put(ATTRIB_PARAM_VALUE, attributeValue);

                                // add it uniquely to serviceOrder
                                Object [] newDataParams = C30HashMapUtils.addUniqueHashMapToArray(extendedData, xdHashMap, ATTRIB_PARAM_ID);
                                serviceOrder.put(ATTRIB_EXTENDED_DATA, newDataParams);
                            }

                        }                                       
                    } catch (NumberFormatException e) {
                        // not a valid param id
                    }
                }
            }
        }
        
    } 
 
}
