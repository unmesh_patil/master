/*
 * C30PackageInstanceList.java
 *
 * Created on June 1, 2007, 11:36 AM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.util.ArrayList;

/**
 * A List of Package Instances that have been provisioned for the Lightweight
 * Order.  This list helps link LW Components to the correct LW Packages package
 * inst id at the time of order creation.
 * @author Joe Morales
 */
public class C30PackageInstanceList {

	/** Array holding the package instance list. */
    private ArrayList packageInstanceList;

    /** Creates a new instance of C30PackageInstanceList. */
    public C30PackageInstanceList() {
        packageInstanceList = new ArrayList();
    }

   /**
    * Adds a new lightweight Package Instance to the list.
    * @param packageInstId the instance ID of the package being added to the list.
    * @param packageInstIdServ the instance ID serv of the package being added to the list.
    * @param packageId the package ID of the package being added to the list.
    * @param packageLinkId the package link ID of the package being added to the list.
    */
    public final void addLightweightPackageInstance(final Integer packageInstId, final Integer packageInstIdServ, final Integer packageId, final Integer packageLinkId) {
        C30PackageInstance newInstance = new C30PackageInstance(packageInstId, packageInstIdServ, packageId, packageLinkId);
        packageInstanceList.add(newInstance);
    }

    /**
     * Method to return the package instance ID of the package that has the same link ID.
     * @param lightweightPackageLink the package link ID
     * @return the package ID
     */
    public final Integer getLightweightPackageInstId(final Integer lightweightPackageLink){
        for (int i = 0; i < this.packageInstanceList.size(); i++) {
            C30PackageInstance packageInst = (C30PackageInstance) this.packageInstanceList.get(i);
            if (packageInst.getPackageLinkId().equals(lightweightPackageLink)) {
                return packageInst.getPackageInstId();
            }
        }
        return null;
    }

    /**
     * Method to return the package instance ID serv of the package that has the same link ID.
     * @param lightweightPackageLink the package link ID
     * @return the package ID
     */
    public final Integer getLightweightPackageInstIdServ(final Integer lightweightPackageLink){
        for (int i = 0; i < this.packageInstanceList.size(); i++) {
            C30PackageInstance packageInst = (C30PackageInstance) this.packageInstanceList.get(i);
            if (packageInst.getPackageLinkId().equals(lightweightPackageLink)) {
                return packageInst.getPackageInstIdServ();
            }
        }
        return null;
    }

    /**
     * Method to return the package ID of the package that has the same link ID.
     * @param lightweightPackageLink the package link ID
     * @return the package ID
     */
    public final Integer getLightweightPackageId(final Integer lightweightPackageLink){
        for (int i = 0; i < this.packageInstanceList.size(); i++) {
            C30PackageInstance packageInst = (C30PackageInstance) this.packageInstanceList.get(i);
            if (packageInst.getPackageLinkId().equals(lightweightPackageLink)) {
                return packageInst.getPackageId();
            }
        }
        return null;
    }

}
