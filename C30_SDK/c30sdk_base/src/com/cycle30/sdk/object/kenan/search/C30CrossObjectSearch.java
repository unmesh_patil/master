package com.cycle30.sdk.object.kenan.search;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * The C30SDK CrossObjectSearch is a pre-configured template for conducting
 * searchs for C30SDK objects that have critical associations to attributes that are 
 * not found in the objects themselves, in otherwords, searches that yeild results only
 * when conducted against other objects or attributes.
 * This is basically a JDBC, non-API-TS approach to establishing the objects that must be 
 * returned.
 *
 * @author Jack Wood
 */
public abstract class C30CrossObjectSearch extends C30SDKObject {

    private static Logger log = Logger.getLogger(C30CrossObjectSearch.class);
    public static final String ATTRIB_SEARCH_FOR = "SearchFor";
    protected static Integer startRecord = 1;
    protected static Integer returnSetSize = 50;

    /**
     * Creates a new instance of C30CrossObjectSearch.
     * @param factory
     * @param objectType
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    protected C30CrossObjectSearch(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * Creates a new instance of C30CrossObjectSearch.
     * @param factory
     * @param objectClass
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    protected C30CrossObjectSearch(final C30SDKFrameworkFactory factory, final Class objectClass) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectClass);
    }

    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {
     
      getFactory().setClassFieldConfiguration(this,"com.cycle30.sdk.object.kenan.search.C30CrossObjectSearch");
      
    }


    /**
     * Creates a copy of the C30CrossObjectSearch object.
     * @return A copy of the C30CrossObjectSearch object
     */
    @Override
	public Object clone() {
        return super.clone();
    }

    @Override
	public C30SDKObject process() throws C30SDKInvalidAttributeException {
    	log.debug("Starting C30CrossObjectSearch.process()");

    	//in case there is an error.
        String error = "creating C30CrossObjectSearch:\n\tLW Search Type: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        C30SDKObject lwo = null;
    	try {

    		        this.setAttributesFromNameValuePairs();
	                this.validateAttributes();
                        this.setSearchScope();
	                lwo = this.conductSearch();
	                
            //if we do not have an exception
            if ((lwo instanceof C30SDKExceptionMessage)) {
        	lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "while " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }

    	} catch (C30SDKException e) {
			lwo = this.createExceptionMessage(e);
    		lwo.setAttributeValue(C30SDKExceptionMessage.ATTRIB_MSG_TEXT, "Error " + error, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}
    	log.debug("Finished C30CrossObjectSearch.process()");
        return lwo;
    }

 
    /**
     * Abstract method that conducts the search and returns results in the form of a LightweighObject
     * item type.
     * @return C30SDKObject the returned search object.
     * @throws C30SDKObjectException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidConfigurationException
     */
    protected abstract C30SDKObject conductSearch() throws C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException;

    /**
     * Validates that all the required dataAttributes have been set.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
     * @throws C30SDKKenanFxCoreException 
     */
    @Override
	protected final void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        if (this.getAttributeValue(ATTRIB_SEARCH_FOR, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) { 
                String error = "A specified Search Object Type is required to execute a Complex Object Search";
            throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-039"),"INVALID-ATTRIB-039");
        }
        super.validateAttributes();
    }
    
    /**
     * Sets the scope of the search, starting record and record set return size.
     * @throws C30SDKInvalidAttributeException if there was a problem accessing the dataAttributes of the object.
     */
    protected final void setSearchScope() throws C30SDKInvalidAttributeException {
        if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
               startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            
          if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
                   getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
                returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
         
    }

}
