/*
 * C30AccountServiceOrder.java
 *
 * Created on December 7, 2006, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.util.Map;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;


/**
 * The C30SDK service order is a pre-configured template for a Kenan/FX service
 * order and is associated with a C30SDK order.  The C30SDK service order
 * will be defined to contain one or more pre-configured order items (C30SDK
 * order items).  This class is the account level service order and extends the base
 * service order class.
 * @author Joe Morales
 */
public class C30AccountServiceOrder extends C30ServiceOrder implements Cloneable {

    /**String value of attributeName to set the WaiveUnmetObligation C30SDK Service Order attribute.*/
    public static final String ATTRIB_PACKAGE_INST_ID = "PackageInstId";
    public static final String ATTRIB_PACKAGE_INST_ID_SERV = "PackageInstIdServ";
    public static final String ATTRIB_ACCOUNT_EXTERNAL_ID = "AccountExternalId";
    protected static final String ATTRIB_ACCOUNT = "Account";
    
    /** Account specific service order type value strings. */
    public static final Integer SERV_ORD_TYPE_ACCOUNT_DISCONNECT = new Integer(40);
    /** Account specific service order type value strings. */
    public static final Integer SERV_ORD_TYPE_ACCOUNT_MOVE_REQUEST = new Integer(45);
    /** Account specific service order type value strings. */
    public static final Integer SERV_ORD_TYPE_PACKAGE_DISCONNECT = new Integer(65);

    /**
     * Creates a new instance of C30AccountServiceOrder.
     * @param factory the factory which is the parent of this object.
     * @param objectType Type of C30SDK account service order being constructed
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30AccountServiceOrder(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /** 
     * Creates a new instance of C30SDKObject.  This constructor is used for objects that are not derived from a configuration in the
     * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
     * the framework to create objects.  It is also used by the BRE front end.
     * @param factory Factory used to create C30ServiceOrder objects
     * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
     * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30AccountServiceOrder(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30AccountServiceOrder.class);
    }
    
    /**
     * Initializes the base attributes that can be set for any Kenan/FX service order.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the service order.
     */
    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException {
        
    	//=================
    	// DATA ATTRIBUTES
    	//=================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    	
    	//===================
    	// OUTPUT ATTRIBUTES
    	//===================
        this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(ATTRIB_PACKAGE_INST_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        
    	super.initializeAttributes();
        this.setAttributeValue(ATTRIB_IS_SERVICE_LEVEL, Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    }

    /**
     * This method overrides the regular method because checks need to be made for whether the service
     * order type ID is being set.  If the service order type is a package disconnect then we need to
     * ensure that the package instance ID and serv are set as well.
     * @see com.cycle30.sdk.core.framework.C30SDKObject#setNameValuePair(java.lang.String, java.lang.Object)
     */
    @Override
	public void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
    	super.setNameValuePair(name, value);
    	
    	if (name.equals(this.getAttribute(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
        	
    		if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(SERV_ORD_TYPE_PACKAGE_DISCONNECT)) {
        		this.getAttribute(ATTRIB_PACKAGE_INST_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
        		this.getAttribute(ATTRIB_PACKAGE_INST_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
        	}
    		
    	}
    }
        
    /**
     * Validates that all the required attributes have been set.
     * @throws C30SDKKenanFxCoreException 
     * @throws C30SDKInvalidAttributeException 
     * @throws C30SDKInvalidAttributeException if there was a problem validating the attributes of the service order.
     */
    @Override
	protected final void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

        super.validateAttributes();

        //-----------------------------------------------------------
        // make sure the service order type id is currently supported
        // by the LWO Framework for Account Level Service Orders
        //-----------------------------------------------------------
        if (!this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceOrder.SERV_ORD_TYPE_CONNECT) &&
                !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30ServiceOrder.SERV_ORD_TYPE_CHANGE) &&
                !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30AccountServiceOrder.SERV_ORD_TYPE_ACCOUNT_MOVE_REQUEST) &&
                !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30AccountServiceOrder.SERV_ORD_TYPE_ACCOUNT_DISCONNECT) &&
                !this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30AccountServiceOrder.SERV_ORD_TYPE_PACKAGE_DISCONNECT)) {
            String error = "The service order type id '" + this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                           "' is not currently supported by the C30SDK ordering framework for account level service orders";
            throw new C30SDKInvalidAttributeException(error);
        }
    }

    /**
     * Initilize attirbutes for the Kenan FX service order that is about to
     * be created through the API TS.
     * @param serviceOrder Kenan/FX service order object
     * @throws C30SDKInvalidAttributeException if there was a problem setting the attributes of the service order.
     */
    @Override
	protected final void setServiceOrderAttributes(final Map serviceOrder) throws C30SDKInvalidAttributeException {

        //---------------------------------------------------------------
        // Set the following Key Attributes which are not modifiable via the
        // LWO interface for client programs
        //
        // IsServiceLevel = FALSE - for LW Account level Service orders.
        //---------------------------------------------------------------
        serviceOrder.put(C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL,Boolean.FALSE);
        //if we are doing a disconnect then make sure the package inst ID and SERV are in the hashmap.
        if (this.getAttributeValue(ATTRIB_SERVICE_ORDER_TYPE_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30AccountServiceOrder.SERV_ORD_TYPE_PACKAGE_DISCONNECT)) {
            serviceOrder.put(ATTRIB_PACKAGE_INST_ID, ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(ATTRIB_PACKAGE_INST_ID)).getValue());
            serviceOrder.put(ATTRIB_PACKAGE_INST_ID_SERV, ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(ATTRIB_PACKAGE_INST_ID_SERV)).getValue());
        }
        
        //get the account and set the external id.
        if (this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {
        	this.setAttributeValue(ATTRIB_ACCOUNT_EXTERNAL_ID, ((C30SDKObject) this.getAttributeValue(ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }

        super.setServiceOrderAttributes(serviceOrder);

    }

    /**
	 * Method to process the usage adjustment.
	 * @return C30SDKObject 
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
     */
    @Override
	public C30SDKObject process(){
        
    	C30SDKObject lwo = null;
    	try {
    		lwo = this.createServiceOrder();
            
    		//if the processed service order produced an exception.
            if (!(lwo instanceof C30SDKExceptionMessage)) {
            	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            }
            
    	} catch (Exception e) {
			lwo = this.createExceptionMessage(e);
		}
    	return lwo;
    }
    
    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }

}
