/*
 * C30AccountExternalId.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.account;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Wrapper class encapsulating a Kenan/FX account external Id object and its corresponding order item..
 * @author Joe Morales
 */
public class C30AccountExternalId extends C30SDKObject { 
    
    private static Logger log = Logger.getLogger(C30AccountExternalId.class);
    

    /**
     * Creates a new instance of C30AccountExternalId.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30AccountExternalId(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }
    
    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always 
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework 
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30AccountExternalId(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30AccountExternalId.class);
    }
    
    /**
     * Initializes the attributes of the Kenan/FX account external ID.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     */
    @Override
	protected void initializeAttributes() throws C30SDKInvalidAttributeException  {   
        
    	//=============================
    	// INPUT ATTRIBUTES
    	//=============================
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        C30SDKAttribute attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID, String.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class);
        attribute.setRequired(true);
        this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_INACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_CURRENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

    	//=============================
    	// OUTPUT ATTRIBUTES
    	//=============================
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKObject.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    	this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_ACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_INACTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.addAttribute(new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_IS_CURRENT, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

    }
    
    /**
     * Initializes the attributes of the Kenan/FX account external ID.
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 * @return C30SDKObject the populated external ID, or a C30SDKExceptionMessage if there was an error.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     */
	@Override
	public C30SDKObject process() throws C30SDKInvalidAttributeException {
        C30SDKObject lwo = null;
    	try {

    		lwo = getAccount();
            
            //if we got an account rather than an exception
            if (lwo instanceof C30Account) {
            	createAccountExternalId();
            	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            	lwo = this;
            }
            
            } catch (Exception e) {
                            lwo = this.createExceptionMessage(e);
            }
         
		return lwo;
	}
    
    /**
     * Creates the Kenan FX account external id using API TS. 
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during ID creation.
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during ID creation.
     * @throws C30SDKObjectException if there was a problem initializing the object during ID creation.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during ID creation.
     * @throws C30SDKKenanFxCoreException 
     */
    private void createAccountExternalId() throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {   
	        
    	//Map to hold all external ID data.
        Map accountIdData = new HashMap();
        Map key = new HashMap();
        accountIdData.put("Key", key);
        
       	setCreateAttributeValues(accountIdData);
       	
       	validateAttributes();
    
        Map callResponse = this.queryData("AccountId", "Create", accountIdData, getAccountServerId());
        Map accountExternalId = (HashMap) callResponse.get("AccountId");
        populateAccountExternalId(accountExternalId);

    } 
    
    /**
     * Internal method to set the output attributes of the external ID given the returned information from middleware.
     * @param accountExternalId the map holding the returned external ID attributes.
     * @throws C30SDKInvalidAttributeException if there was a problem setting the attributes of the object.
     */
    private void populateAccountExternalId(final Map accountExternalId) throws C30SDKInvalidAttributeException {
    	
    	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID, ((Map) accountExternalId.get("Key")).get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID_TYPE, ((Map) accountExternalId.get("Key")).get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, ((Map) accountExternalId.get("Key")).get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACTIVE_DATE, ((Map) accountExternalId.get("Key")).get(C30SDKAttributeConstants.ATTRIB_ACTIVE_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_INACTIVE_DATE, accountExternalId.get(C30SDKAttributeConstants.ATTRIB_INACTIVE_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_CURRENT, accountExternalId.get(C30SDKAttributeConstants.ATTRIB_IS_CURRENT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    }
    
    /**
     * Method which sets all the attributes when a connect is performed.
     * @param accountIdData the map holding the information to be used for creating the external ID.
     * @throws C30SDKInvalidAttributeException if there was a problem setting the attributes of the object.
     */
    private void setCreateAttributeValues(final Map accountIdData) throws C30SDKInvalidAttributeException {
        //-------------------------------------------------------------------
        // The following values are not overrideable.  They are specially set
        // based on account and order attributes
        //-------------------------------------------------------------------        
        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        String attributeName = null;
        Object attributeValue = null;
        while (iterator.hasNext()) {
            attributeName = (String) iterator.next();
            attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACTIVE_DATE) && attributeValue != null) {
            	((Map) accountIdData.get("Key")).put(attributeName, attributeValue);
            } else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_INACTIVE_DATE) && attributeValue != null) {
            	accountIdData.put(attributeName, attributeValue);
            } else if(attributeName.equals(C30SDKAttributeConstants.ATTRIB_IS_CURRENT) && attributeValue != null) {
            	accountIdData.put(attributeName, attributeValue);
            }
        }
        
        //set active date to today if there isn't one already set.
        if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACTIVE_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
        	((Map) accountIdData.get("Key")).put(C30SDKAttributeConstants.ATTRIB_ACTIVE_DATE, new Date());
        }
        
        if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_CURRENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {
        	accountIdData.put(C30SDKAttributeConstants.ATTRIB_IS_CURRENT, new Boolean("true"));        
        }
        
        ((Map) accountIdData.get("Key")).put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, ((C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)).getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        ((Map) accountIdData.get("Key")).put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        ((Map) accountIdData.get("Key")).put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_NEW_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        
    }
    
    /**
     * Method to get the account associated with this external ID.
     * @return a C30Account object, or a C30SDKExceptionMessage if an error occurred.
     * @throws C30SDKException 
     */
    private C30SDKObject getAccount() throws C30SDKException {
    	log.debug("Starting C30AccountExternalId.getAccount");
    	
    	C30SDKObject account = null;
    	
		// if the account external ID has an account parent then get and set it 
    	// into the object.  This saves us from making a DB call to get the account.
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null
				&& this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) instanceof C30Account) {
        
			account = (C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        	
		
		} else if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) == null) {
        	
	    	//try get account from object children
	    	for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
	    		if (this.getC30SDKObject(i) instanceof C30Account) {
	    			account = this.getC30SDKObject(i);
	    		}
	    	}
	    	
	    	//if no account in children then get account from middleware
	    	if (account == null) {
		    	account = this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);
		    	account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		    	account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		    	account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		        account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		    	account = account.process();
	    	}
	    	
		}
	    	
        //if the processed account did not produce an exception then set the account ID's into this object.
        if (!(account instanceof C30SDKExceptionMessage)) {
        	this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
    	log.debug("Finished C30AccountExternalId.getAccount");
    	return account;
    }
    
    /**
     * Method which gets any data to be cached for further use by the object.
     */
    @Override
	protected void loadCache() {
    	
    }
}
