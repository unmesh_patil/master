/*
 * C30PackageInstance.java
 *
 * Created on June 1, 2007, 11:36 AM
 *
 */
package com.cycle30.sdk.object.kenan.ordering;

/**
 * A small object representing packages instance mappings.  This is used within
 * a LightweightPackageInstList to help map package objects that span multiple
 * LW Service Orders.  The Package Inst id must be shared across the different SOs.
 * This particular object merely stores the mapping of package inst id of
 * packages provisioned through a LW Order.
 * @author Joe Morales
 */
public class C30PackageInstance {

    private Integer packageInstId;
    private Integer packageInstIdServ;
    private Integer packageId;
    private Integer packageLinkId;

    /** Creates a new instance of C30SDKAttribute.
    * @param pkgInstId the instance ID of the package being created.
    * @param pkgInstIdServ the instance ID serv of the package being created.
    * @param pkgId the package ID of the package being created.
    * @param pkgLinkId the package link ID of the package being created.
    */
    public C30PackageInstance(final Integer pkgInstId, final Integer pkgInstIdServ, final Integer pkgId, final Integer pkgLinkId) {
        this.packageInstId = pkgInstId;
        this.packageInstIdServ = pkgInstIdServ;
        this.packageId = pkgId;
        this.packageLinkId = pkgLinkId;
    }

    /**
     * Gets the class representing the type of the attribute.
     * @return packageInstId
     */
    public final Integer getPackageInstId() {
        return this.packageInstId;
    }

    /**
     * Gets the class representing the type of the attribute.
     * @return packageInstId
     */
    public final Integer getPackageInstIdServ() {
        return this.packageInstIdServ;
    }

    /**
     * Gets the value Package Id.
     * @return packageId
     */
    public final Integer getPackageId() {
        return this.packageId;
    }

    /**
     * Gets the value Package Link Id.
     * @return packageLinkId
     */
    public final Integer getPackageLinkId() {
        return this.packageLinkId;
    }

}
