package com.cycle30.sdk.object.kenan.account;

/*
 * C30Account.java
 *
 * Created on December 9, 2006, 9:38 AM
 *
 */


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.cache.C30SDKCacheableWrapper;
import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.datasource.C30SDKDataSourceUtils;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKSqlConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30HashMapUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX Account object.
 * @author Tom Ansley
 */
public class C30Account extends C30SDKObject {

	private static Logger log = Logger.getLogger(C30Account.class);

	private static Integer startRecord = 0;
	private static Integer returnSetSize = 50;

	/**
	 * Creates a new instance of C30Account.
	 * @param factory Factory used to create C30Account objects
	 * @param objectType Type of C30SDK object
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 * @throws C30SDKInvalidConfigurationException if there is invalid configuration which causes this account to fail during initialization.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Account(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/**
	 * Creates a new instance of C30Account.
	 * @param factory Factory used to create C30Account objects
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware while creating the object
	 * @throws C30SDKObjectException if there was a problem initializing the object
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object.
	 * @throws C30SDKInvalidConfigurationException if there is invalid configuration which causes this account to fail during initialization.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30Account(final C30SDKFrameworkFactory factory) throws C30SDKInvalidAttributeException, C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30Account.class);
	}

	/**
	 * Method to initialize the account attributes.
	 * @exception C30SDKInvalidAttributeException if there was an error while creating and initializing the account attributes.
	 */
	protected final void initializeAttributes() throws C30SDKInvalidAttributeException {
		log.debug("Starting C30Account.initializeAttributes().");
		setClassFieldConfiguration();
		log.debug("Loading extendedDataAttributes.");
		this.loadAccountExtendedDataAttributes(C30SDKAttributeConstants.EXT_DATA_BASE_TABLE, new Integer("0"));
		log.debug("Finished C30Account.initializeAttributes().");

	}


	/**
	 * MEDIUM LEVEL DATA INPUT
	 * Set a name/value pair that can be used to initialize lw object attribute values.
	 * @param name the name of the name/value pair being set
	 * @param value the value of the name/value pair being set.
	 * @throws C30SDKInvalidAttributeException if there was a problem adding the name/value pair.
	 */
	public final void setNameValuePair(final String name, final Object value) throws C30SDKInvalidAttributeException {
		super.setNameValuePair(name, value);

		//if we are setting the action ID
		if (name.equals(this.getAttribute(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getExternalName())) {
			if (value.toString().equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_CREATE.toString())) {
				initializeAccountCreateAttributes();
			} else if (value.toString().equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND.toString())) {
				initializeAccountFindAttributes();
			}
		}

		//if we are setting ParentExternalId or ParentExternalIdType
		if (name.equals(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID) || name.equals(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID_TYPE)) {
			this.getAttribute(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
			this.getAttribute(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
		}

	}


	/**
	 * Sets the value of an attribute.
	 * @param attributeName the attribute name who's value is to be set.
	 * @param attributeValue  the value the attribute is to be set to.
	 * @param type the type of attribute being processed.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the service order.
	 */
	public final void setAttributeValue(final String attributeName, final Object attributeValue, final Integer type) throws C30SDKInvalidAttributeException {
		super.setAttributeValue(attributeName, attributeValue, type);

		//if we are setting the action ID
		if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID)) {
			if (attributeValue.toString().equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_CREATE.toString())) {
				initializeAccountCreateAttributes();
			} else if (attributeValue.toString().equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND.toString())) {
				initializeAccountFindAttributes();
			}
		}
		if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_FIND))
			if (attributeValue.toString().equalsIgnoreCase("TRUE")) 
				initializeAccountFindAttributes();

		//if we are setting ParentExternalId or ParentExternalIdType
		if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID) || attributeName.equals(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID_TYPE)) {
			this.getAttribute(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
			this.getAttribute(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).setRequired(true);
		}

	}


	/**
	 * Method to initialize attributes when an account is being created.
	 * @throws C30SDKInvalidAttributeException if there is an attribute error while initializing them
	 */
	private void initializeAccountCreateAttributes() throws C30SDKInvalidAttributeException {
		C30SDKAttribute attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_BILLING_SERVICE_CENTER_ID, Integer.class);
		attribute.setRequired(true);
		attribute.setIsInternal(false);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_REMIT_SERVICE_CENTER_ID, Integer.class);
		attribute.setRequired(true);
		attribute.setIsInternal(false);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_INQUIRY_SERVICE_CENTER_ID, Integer.class);
		attribute.setRequired(true);
		attribute.setIsInternal(false);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_COLLECTION_SERVICE_CENTER_ID, Integer.class);
		attribute.setRequired(true);
		attribute.setIsInternal(false);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_PRINT_SERVICE_CENTER_ID, Integer.class);
		attribute.setRequired(true);
		attribute.setIsInternal(false);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = new C30SDKAttribute(C30SDKAttributeConstants.ATTRIB_SERVER_CATEGORY, Integer.class);
		attribute.setRequired(true);
		attribute.setIsInternal(false);
		this.addAttribute(attribute, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setOverrideable(false);
	}




	/**
	 * Method to initialize attributes when finding an account.
	 */
	private void initializeAccountFindAttributes() throws C30SDKInvalidAttributeException {
		C30SDKAttribute attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setRequired(false);
		attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setRequired(false);
		attribute = this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setOverrideable(true);
	}

	/**
	 * Finds the Kenan/FX acount based on external id and external id type.
	 * @throws C30SDKException 
	 */
	private void findAccount() throws C30SDKException {
		int lastRecordReturned = 0;
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_ALL)){
			this.setAttributesFromNameValuePairs();
			// set values for record retrieval
			if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			{
				startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				startRecord--;
			}
			if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
				returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}


		//If Only one customer server present then directly make find call on to that customer server.
		if (C30SDKValueConstants.C30_KENAN_TOTAL_CUST_SERVERS.intValue() == 1)
		{

			//Create a HashMap to contain the account filter info
			HashMap accountFilter = new HashMap();
			accountFilter.put("Fetch", Boolean.TRUE);

			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) {

				Map key = new HashMap();
				accountFilter.put("Key", key);

				HashMap accountInternalIdFilter = new HashMap();
				accountInternalIdFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalIdFilter);

			} else if ((this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null
					&& this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null) ||
					this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT)  != null  ||
					this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT)  != null )
			{

				if ((this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null
						&& this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
				{
					//-------------------------------------------
					// add the account external id to the filter
					//-------------------------------------------
					HashMap accountExternalIdFilter = new HashMap();
					accountExternalIdFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
					accountFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, accountExternalIdFilter);

					//------------------------------------------------
					// add the account external id type to the filter
					//------------------------------------------------
					HashMap accountExternalIdTypeFilter = new HashMap();
					accountExternalIdTypeFilter.put("Equal",  this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
					accountFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, accountExternalIdTypeFilter);
				}
				//------------------------------------------------
				// add the account BillFname to the filter
				//------------------------------------------------
				if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
				{
					HashMap accountBillFNameFilter = new HashMap();
					accountBillFNameFilter.put("Like",  this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
					accountFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, accountBillFNameFilter);
				}
				//------------------------------------------------
				// add the account BillLname to the filter
				//------------------------------------------------
				if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
				{
					HashMap accountBillLNameFilter = new HashMap();
					accountBillLNameFilter.put("Like",  this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
					accountFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, accountBillLNameFilter);
				}

			} else {

				if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND)){
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-012"),"INVALID-ATTRIB-012");
				}
				// we are here searching for all 
			}  

			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_ALL)){
				this.setAccountMapAttributes(accountFilter,"FIND");
				log.debug(accountFilter.toString());
				HashMap inactiveDateFilter = new HashMap();
				inactiveDateFilter.put("IsNull", Boolean.TRUE);
				accountFilter.put("InactiveDate", inactiveDateFilter);

				HashMap attributeFilter = new HashMap();
				attributeFilter.put("GreaterThanEqual", new java.util.Date());
				accountFilter.put("ActiveDate", attributeFilter);

			}

			Map callResponse;
			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_ALL))
				callResponse = this.queryData("Account", "Find", accountFilter, C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);
			else    
				callResponse = this.queryData("Account", "FindWithExtendedData", accountFilter, C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);

			//get the count to ensure we found an account.
			int count = ( (Integer) callResponse.get("Count")).intValue();

			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND)){
				if (count == 0) {
					String error = "No account found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
					+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),"INVALID-ATTRIB-013");
				} else if (count > 1) {
					String error = "Multiple accounts found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
					+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					//throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-014"),"INVALID-ATTRIB-014");
				}
			}
			// don't go any further if searching for all and none are returned.
			if(count == 0)
			{
				setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				return;
			}
			//Dealing with the first  Account in the result set.
			Map account = (HashMap) ((Object[]) callResponse.get("AccountList"))[0];
			int recordsProcessed = 0;
			if (startRecord == 1){
				populateAccount(account);
				if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
					setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				lastRecordReturned = 1;
				recordsProcessed++;
			}
			// when retrieving single record in a find request - it is possibly for a related operation: guaranteed to be a single account record
			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND))
			{
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) && getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)!= null)
				{
					Integer serverId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					//C30SDKCache.setCache("ACCOUNT_SERVER_ID_CACHE", serverId);
					C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>("ACCOUNT_SERVER_ID_CACHE", serverId);
					this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
				}
				else 
				{
					Integer acctInternalId = (Integer)((HashMap)account.get("Key")).get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID);
					Integer acctSegId = (Integer)account.get(C30SDKAttributeConstants.ATTRIB_ACCT_SEG_ID);

					if(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) != 0)
						getCustomerServerId_DB(acctInternalId, acctSegId);
				}
				//return;
			}

			// at this point we have more than a single account to load
			C30Account cloneLwo = (C30Account)super.clone();
			if (startRecord > 1)
				startRecord--;

			for (int i=startRecord; i < count; i++)
			{
				if (recordsProcessed >= returnSetSize )
					break;
				HashMap anAccount = (HashMap)((Object[])(Object[])callResponse.get("AccountList"))[i]; 

				// is this the first account we are processing?
				if (recordsProcessed == 0){
					populateAccount(anAccount);
					if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

					this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				else
				{
					C30Account lwo = (C30Account)cloneLwo.clone();
					lwo.populateAccount(anAccount);
					// add to our list of peer objects
					if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					super.addC30SDKObject(lwo);
				}
				recordsProcessed++;
				lastRecordReturned =  i + 1;
			}
			if (count > 1 ) super.setProcessingAggregateTotalsAttributes(this, count, lastRecordReturned, recordsProcessed );
		}
		else
		{

			// If multiple customer server present then first make the AccountLocateFind and then 
			// Make the AccountFindWithExtendedData on all of the customer servers.

			// Create the accountLocate filter incase of multiple customer servers
			HashMap accountLocateFilter = new HashMap();

			//Set the Fetch flag at the root level,
			//so the call will return all fields for found objects
			accountLocateFilter.put("Fetch", Boolean.TRUE);

		}
	}
	
	
	/**
	 * Finds the Kenan/FX acount based on external id and external id type.
	 * @throws C30SDKException, Exception 
	 */

	private void findAccountWithBalanceSummary() throws C30SDKException, Exception {
		int lastRecordReturned = 0;
		int newTotalCount =0;
		int newLastRecordReturned=0;
		HashMap writeoffBalanceList = new HashMap();


		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_WITH_BALANCE_SUMMARY)){
			this.setAttributesFromNameValuePairs();
			super.prevalidate();
			// set values for record retrieval
			if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			{
				startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				//The default value is 1, but we use it to 0 for the index purpose
				if(startRecord<1)
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-057"),"INVALID-ATTRIB-057");
				startRecord--;
			}
			if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
				returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}

		String externalId = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer externalIdType = (Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer accountInternalId = (Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billFname = (String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billLname = (String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billCompany = (String)this.getAttributeValue("BillCompany", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String phone =(String)this.getAttributeValue("CustPhone1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String zip  =(String)this.getAttributeValue("BillZip", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String city  =(String)this.getAttributeValue("BillCity", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billAddress1 = (String)this.getAttributeValue("BillAddress1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String state = (String)this.getAttributeValue("BillState", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Boolean includeInactiveAccount = (Boolean)this.getAttributeValue("IncludeInactiveAccounts", C30SDKValueConstants.ATTRIB_TYPE_INPUT);


		//If Only one customer server present then directly make find call on to that customer server.
		if (C30SDKValueConstants.C30_KENAN_TOTAL_CUST_SERVERS.intValue() == 1)
		{

			//Create a HashMap to contain the account filter info, if only AccountInternalId specified
			HashMap accountFilter = new HashMap();
			ArrayList<Integer> listneedtobeSearched = new ArrayList();
			//If accountInternal Id is provided then use it directly to make the call.
			if (accountInternalId != null) {
				accountFilter.put("Fetch", Boolean.TRUE);
				Map key = new HashMap();
				accountFilter.put("Key", key);

				HashMap accountInternalIdFilter = new HashMap();
				accountInternalIdFilter.put("Equal", accountInternalId);
				key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalIdFilter);
				newTotalCount=1;
			}
			//If accountExternalId is specified
			else if ((externalId != null && externalIdType != null)) 
			{

				accountFilter.put("Fetch", Boolean.TRUE);

				//-------------------------------------------
				// add the account external id to the filter
				//-------------------------------------------
				HashMap accountExternalIdFilter = new HashMap();
				accountExternalIdFilter.put("Equal", externalId);
				accountFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, accountExternalIdFilter);
				//listneedtobeSearched.add(new Integer(externalId));
				//------------------------------------------------
				// add the account external id type to the filter
				//------------------------------------------------
				HashMap accountExternalIdTypeFilter = new HashMap();
				accountExternalIdTypeFilter.put("Equal",  externalIdType);
				accountFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, accountExternalIdTypeFilter);
				newTotalCount=1;
			}//If only FirstName or Last Name provided then
			else
			{
				// Here we get the list of customers that satisfies the search criteria base on
				// all other search criteira.


				//This holds the accountList that satisfies the billaddress and other details.
				ArrayList accuntList = getAccountListfromSearch(billFname, billLname, billCompany, phone, zip,city, state, billAddress1,includeInactiveAccount,externalIdType);

				int i=0;

				for (i=startRecord; i<startRecord+returnSetSize; i++)
				{
					if (i >= accuntList.size()) break;
					listneedtobeSearched.add((Integer)accuntList.get(i));

				}
				newTotalCount = accuntList.size();
				if(listneedtobeSearched.size() == 0)
				{
					setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					String error = "No account found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
					+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),"INVALID-ATTRIB-013");

				}

				newLastRecordReturned = i;
				accountFilter.put("Fetch", Boolean.TRUE);
				Map key = new HashMap();
				accountFilter.put("Key", key);

				//We use infilter for the list of accountInternalIds.
				HashMap accountInternalIdFilter = new HashMap();
				accountInternalIdFilter.put("In", listneedtobeSearched);
				key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalIdFilter);				

			/*	if (!includeInactiveAccount)
				{
					HashMap inactiveDateFilter = new HashMap();
					inactiveDateFilter.put("IsNull", Boolean.TRUE);
					accountFilter.put("InactiveDate", inactiveDateFilter);
				}

				HashMap attributeFilter = new HashMap();
				attributeFilter.put("LessThanEqual", new java.util.Date());
				accountFilter.put("ActiveDate", attributeFilter);*/
			}

			if (listneedtobeSearched.size() > 0) writeoffBalanceList = getWriteOffBalance(listneedtobeSearched);

			int writeoffbalance=0;
			Map callResponse;
			callResponse = this.queryData("Account", "FindWithExtendedData", accountFilter, C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);

			//get the count to ensure we found an account.
			//int count = ( (Integer) callResponse.get("Count")).intValue();
			int count = listneedtobeSearched.size();
			// don't go any further if searching for all and none are returned.
			if(count == 0)
			{
				setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				String error = "No account found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
				+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),"INVALID-ATTRIB-013");

			}

			HashMap accountBalanceMap = new HashMap();
			HashMap key = new HashMap();
			Map accountBalanceResponse =new HashMap();
			int recordsProcessed = 0;
			//Creating/Reading the first account for the Cloning ...
			Map account = (HashMap) ((Object[]) callResponse.get("AccountList"))[0];
			accountBalanceMap.put("Key", account.get("Key"));
			accountBalanceMap.put("Fetch", Boolean.TRUE);

			HashMap fetchTrue = new HashMap();
			fetchTrue.put("Fetch", Boolean.TRUE);
			accountBalanceMap.put("PastDueDays1To30",fetchTrue);
			accountBalanceMap.put("PastDueDays31To60",fetchTrue);
			accountBalanceMap.put("PastDueDays61To90",fetchTrue);
			accountBalanceMap.put("PastDueDays91To120",fetchTrue);
			accountBalanceMap.put("PastDueDaysGt120",fetchTrue);

			HashMap PastDueDays1To30filter = new HashMap();
			PastDueDays1To30filter.put("Fetch", Boolean.TRUE);
			accountBalanceMap.put("PastDueDays1To30",PastDueDays1To30filter);

			key = (HashMap) account.get("Key");

			//Finding the Writeoff Balance amount.
			if (writeoffBalanceList.get(key.get("AccountInternalId")) != null)
				writeoffbalance = ((Integer)writeoffBalanceList.get(key.get("AccountInternalId"))).intValue();
			else
			{
				if(listneedtobeSearched.size()==0)
					writeoffbalance = getWriteOffBalance((Integer)key.get("AccountInternalId"));
				else
					writeoffbalance =0;
			}
			//Finding Account Balance Summary
			accountBalanceResponse = this.queryData("Account", "BalanceSummary", accountBalanceMap, getAccountServerId());
			accountBalanceResponse.put("WriteoffBalance",writeoffbalance);
			if (accountBalanceResponse.get("PastDueDays1To30") == null ) accountBalanceResponse.put("PastDueDays1To30",0);
			if (accountBalanceResponse.get("PastDueDays31To60") == null ) accountBalanceResponse.put("PastDueDays31To60",0);
			if (accountBalanceResponse.get("PastDueDays61To90") == null ) accountBalanceResponse.put("PastDueDays61To90",0);
			if (accountBalanceResponse.get("PastDueDays91To120") == null ) accountBalanceResponse.put("PastDueDays91To120",0);
			if (accountBalanceResponse.get("PastDueDaysGt120") == null ) accountBalanceResponse.put("PastDueDaysGt120",0);
			accountBalanceResponse.put("CurrentInvoiceBalance", accountBalanceResponse.get("SumBalance"));
			accountBalanceResponse.remove("SumBalance");
			accountBalanceResponse.remove("CurrencyCode");
			
			account.put("Balances", accountBalanceResponse);
			account.put("TotalCount", new Integer(newTotalCount));

			if (startRecord == 1){
				populateAccount(account);
				if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
					setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				lastRecordReturned = 1;
				recordsProcessed++;
			}

			populateAccount(account);
			// at this point we have more than a single account to load
			C30Account cloneLwo = (C30Account)super.clone();

			for (int i=0; i < count; i++)
			{

				//create the key hashmap
				HashMap anAccount = (HashMap)((Object[])(Object[])callResponse.get("AccountList"))[i]; 
				accountBalanceMap.put("Key", anAccount.get("Key"));
				accountBalanceMap.put("Fetch", Boolean.TRUE);

				fetchTrue = new HashMap();
				fetchTrue.put("Fetch", Boolean.TRUE);
				accountBalanceMap.put("PastDueDays1To30",fetchTrue);
				accountBalanceMap.put("PastDueDays31To60",fetchTrue);
				accountBalanceMap.put("PastDueDays61To90",fetchTrue);
				accountBalanceMap.put("PastDueDays91To120",fetchTrue);
				accountBalanceMap.put("PastDueDaysGt120",fetchTrue);



				key = (HashMap) anAccount.get("Key");

				// is this the first account we are processing?
				if (recordsProcessed == 0){
					populateAccount(anAccount);
					if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

					this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				else
				{
					C30Account lwo = (C30Account)cloneLwo.clone();
					// add to our list of peer objects
					if (writeoffBalanceList.get(key.get("AccountInternalId")) != null)
						writeoffbalance = ((Integer)writeoffBalanceList.get(key.get("AccountInternalId"))).intValue();
					else
					{
						if(listneedtobeSearched.size()==0)
							writeoffbalance = getWriteOffBalance((Integer)key.get("AccountInternalId"));
						else
							writeoffbalance =0;
					}

					//make the call
					accountBalanceResponse = this.queryData("Account", "BalanceSummary", accountBalanceMap, getAccountServerId());
					accountBalanceResponse.put("WriteoffBalance",writeoffbalance);
					if (accountBalanceResponse.get("PastDueDays1To30") == null ) accountBalanceResponse.put("PastDueDays1To30",0);
					if (accountBalanceResponse.get("PastDueDays31To60") == null ) accountBalanceResponse.put("PastDueDays31To60",0);
					if (accountBalanceResponse.get("PastDueDays61To90") == null ) accountBalanceResponse.put("PastDueDays61To90",0);
					if (accountBalanceResponse.get("PastDueDays91To120") == null ) accountBalanceResponse.put("PastDueDays91To120",0);
					if (accountBalanceResponse.get("PastDueDaysGt120") == null ) accountBalanceResponse.put("PastDueDaysGt120",0);
					accountBalanceResponse.put("CurrentInvoiceBalance", accountBalanceResponse.get("SumBalance"));
					accountBalanceResponse.remove("SumBalance");
					accountBalanceResponse.remove("CurrencyCode");
					
					anAccount.put("Balances", accountBalanceResponse);
					anAccount.put("TotalCount", new Integer(newTotalCount));
					lwo.populateAccount(anAccount);


					if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

					super.addC30SDKObject(lwo);
				}
				recordsProcessed++;
				lastRecordReturned =  i + 1;
			}
			//if (count > 1 ) super.setProcessingAggregateTotalsAttributes(this, newTotalCount, newLastRecordReturned, recordsProcessed );
		}
		else
		{

			// If multiple customer server present then first make the AccountLocateFind and then 
			// Make the AccountFindWithExtendedData on all of the customer servers.

			// Create the accountLocate filter incase of multiple customer servers
			HashMap accountLocateFilter = new HashMap();

			//Set the Fetch flag at the root level,
			//so the call will return all fields for found objects
			accountLocateFilter.put("Fetch", Boolean.TRUE);

		}
	}



	

	/**
	 * Finds the Kenan/FX acount based on external id and external id type.
	 * @throws C30SDKException, Exception 
	 */

	private void findAccountWithBalanceSummaryJdbc() throws C30SDKException, Exception {
		int lastRecordReturned = 0;
		int newTotalCount =0;
		int newLastRecordReturned=0;
		HashMap writeoffBalanceList = new HashMap();


		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_WITH_BALANCE_SUMMARY)){
			this.setAttributesFromNameValuePairs();
			super.prevalidate();
			// set values for record retrieval
			if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			{
				startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				//The default value is 1, but we use it to 0 for the index purpose
				if(startRecord<1)
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-057"),"INVALID-ATTRIB-057");
				startRecord--;
			}
			if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
					getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
				returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		}

		String externalId = (String) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer externalIdType = (Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Integer accountInternalId = (Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billFname = (String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billLname = (String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billCompany = (String)this.getAttributeValue("BillCompany", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String phone =(String)this.getAttributeValue("CustPhone1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String zip  =(String)this.getAttributeValue("BillZip", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String city  =(String)this.getAttributeValue("BillCity", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String billAddress1 = (String)this.getAttributeValue("BillAddress1", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		String state = (String)this.getAttributeValue("BillState", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		Boolean includeInactiveAccount = (Boolean)this.getAttributeValue("IncludeInactiveAccounts", C30SDKValueConstants.ATTRIB_TYPE_INPUT);


		//If Only one customer server present then directly make find call on to that customer server.
		if (C30SDKValueConstants.C30_KENAN_TOTAL_CUST_SERVERS.intValue() == 1)
		{

			//Create a HashMap to contain the account filter info, if only AccountInternalId specified
			HashMap accountFilter = new HashMap();
			ArrayList<Integer> listneedtobeSearched = new ArrayList();
			//If accountInternal Id is provided then use it directly to make the call.
			if (accountInternalId != null) {
				//If the internal Id is supplied please use it as is.
				listneedtobeSearched.add(accountInternalId);
				newTotalCount=1;
			}
			//If accountExternalId is specified
			else if ((externalId != null && externalIdType != null)) 
			{
				//If account externalId is given, use this to find the AccountInternalid
				// Use JDBC to get the AccountInternalId
				C30JDBCDataSource dataSource =this.getFactory().getJDBCDataSource(C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);
				String extendedDataQuery = C30SDKSqlConstants.QUERY_GET_EXTENDEDDATA_PARAM_LIST;
				PreparedStatement ps = dataSource.getPreparedStatement(extendedDataQuery);
				ps.setString(1,(String)externalId);
				ps.setInt(2,(Integer)externalIdType);
				ResultSet resultSet  = ps.executeQuery();
				resultSet.next();
				listneedtobeSearched.add(new Integer(resultSet.getInt("AccountInternalId")));
				resultSet.close();
				
				newTotalCount=1;
			}//If only FirstName or Last Name provided then
			else
			{
				// Here we get the list of customers that satisfies the search criteria base on
				// all other search criteira.


				//This holds the accountList that satisfies the billaddress and other details.
				ArrayList accuntList = getAccountListfromSearch(billFname, billLname, billCompany, phone, zip,city, state, billAddress1,includeInactiveAccount,externalIdType);

				int i=0;

				for (i=startRecord; i<startRecord+returnSetSize; i++)
				{
					if (i >= accuntList.size()) break;
					listneedtobeSearched.add((Integer)accuntList.get(i));

				}
				newTotalCount = accuntList.size();
				if(listneedtobeSearched.size() == 0)
				{
					setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					String error = "No account found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
					+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),"INVALID-ATTRIB-013");

				}

				newLastRecordReturned = i;
				
				//Use JDBC Call to Get all the Account Data, instead of AccountFindWithExtendedData.
				accountFilter.put("Fetch", Boolean.TRUE);
				Map key = new HashMap();
				accountFilter.put("Key", key);

				//We use infilter for the list of accountInternalIds.
				HashMap accountInternalIdFilter = new HashMap();
				accountInternalIdFilter.put("In", listneedtobeSearched);
				key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalIdFilter);				

			/*	if (!includeInactiveAccount)
				{
					HashMap inactiveDateFilter = new HashMap();
					inactiveDateFilter.put("IsNull", Boolean.TRUE);
					accountFilter.put("InactiveDate", inactiveDateFilter);
				}

				HashMap attributeFilter = new HashMap();
				attributeFilter.put("LessThanEqual", new java.util.Date());
				accountFilter.put("ActiveDate", attributeFilter);*/
			}

			if (listneedtobeSearched.size() > 0) writeoffBalanceList = getWriteOffBalance(listneedtobeSearched);

			int writeoffbalance=0;
			//Map callResponse = this.queryData("Account", "FindWithExtendedData", accountFilter, C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);
			Map[] callResponse = findAccountWithExtendedDataJdbcCall(listneedtobeSearched, C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);

			//get the count to ensure we found an account.
			int count = listneedtobeSearched.size();
			// don't go any further if searching for all and none are returned.
			if(count == 0)
			{
				setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				String error = "No account found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
				+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),"INVALID-ATTRIB-013");

			}

			HashMap accountBalanceMap = new HashMap();
			HashMap key = new HashMap();
			Map accountBalanceResponse =new HashMap();
			int recordsProcessed = 0;
			//Creating/Reading the first account for the Cloning ...
			Map account = (HashMap) (callResponse[0]);
			accountBalanceMap.put("Key", account.get("Key"));
			accountBalanceMap.put("Fetch", Boolean.TRUE);

			HashMap fetchTrue = new HashMap();
			fetchTrue.put("Fetch", Boolean.TRUE);
			accountBalanceMap.put("PastDueDays1To30",fetchTrue);
			accountBalanceMap.put("PastDueDays31To60",fetchTrue);
			accountBalanceMap.put("PastDueDays61To90",fetchTrue);
			accountBalanceMap.put("PastDueDays91To120",fetchTrue);
			accountBalanceMap.put("PastDueDaysGt120",fetchTrue);

			HashMap PastDueDays1To30filter = new HashMap();
			PastDueDays1To30filter.put("Fetch", Boolean.TRUE);
			accountBalanceMap.put("PastDueDays1To30",PastDueDays1To30filter);

			key = (HashMap) account.get("Key");

			//Finding the Writeoff Balance amount.
			if (writeoffBalanceList.get(key.get("AccountInternalId")) != null)
				writeoffbalance = ((Integer)writeoffBalanceList.get(key.get("AccountInternalId"))).intValue();
			else
			{
				if(listneedtobeSearched.size()==0)
					writeoffbalance = getWriteOffBalance((Integer)key.get("AccountInternalId"));
				else
					writeoffbalance =0;
			}
			//Finding Account Balance Summary
			
			//accountBalanceResponse = this.queryData("Account", "BalanceSummary", accountBalanceMap, getAccountServerId());
			//Get AccountBalance Summary
			accountBalanceResponse.put("WriteoffBalance",writeoffbalance);
			if (accountBalanceResponse.get("PastDueDays1To30") == null ) accountBalanceResponse.put("PastDueDays1To30",0);
			if (accountBalanceResponse.get("PastDueDays31To60") == null ) accountBalanceResponse.put("PastDueDays31To60",0);
			if (accountBalanceResponse.get("PastDueDays61To90") == null ) accountBalanceResponse.put("PastDueDays61To90",0);
			if (accountBalanceResponse.get("PastDueDays91To120") == null ) accountBalanceResponse.put("PastDueDays91To120",0);
			if (accountBalanceResponse.get("PastDueDaysGt120") == null ) accountBalanceResponse.put("PastDueDaysGt120",0);
			accountBalanceResponse.put("CurrentInvoiceBalance", accountBalanceResponse.get("SumBalance"));
			accountBalanceResponse.remove("SumBalance");
			accountBalanceResponse.remove("CurrencyCode");
			
			account.put("Balances", accountBalanceResponse);
			account.put("TotalCount", new Integer(newTotalCount));

			if (startRecord == 1){
				populateAccount(account);
				if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
					setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				lastRecordReturned = 1;
				recordsProcessed++;
			}

			populateAccount(account);
			// at this point we have more than a single account to load
			C30Account cloneLwo = (C30Account)super.clone();

			for (int i=0; i < count; i++)
			{

				//create the key hashmap
				HashMap anAccount = (HashMap)callResponse[i]; 
				accountBalanceMap.put("Key", anAccount.get("Key"));
				accountBalanceMap.put("Fetch", Boolean.TRUE);

				fetchTrue = new HashMap();
				fetchTrue.put("Fetch", Boolean.TRUE);
				accountBalanceMap.put("PastDueDays1To30",fetchTrue);
				accountBalanceMap.put("PastDueDays31To60",fetchTrue);
				accountBalanceMap.put("PastDueDays61To90",fetchTrue);
				accountBalanceMap.put("PastDueDays91To120",fetchTrue);
				accountBalanceMap.put("PastDueDaysGt120",fetchTrue);



				key = (HashMap) anAccount.get("Key");

				// is this the first account we are processing?
				if (recordsProcessed == 0){
					populateAccount(anAccount);
					if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

					this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				else
				{
					C30Account lwo = (C30Account)cloneLwo.clone();
					// add to our list of peer objects
					if (writeoffBalanceList.get(key.get("AccountInternalId")) != null)
						writeoffbalance = ((Integer)writeoffBalanceList.get(key.get("AccountInternalId"))).intValue();
					else
					{
						if(listneedtobeSearched.size()==0)
							writeoffbalance = getWriteOffBalance((Integer)key.get("AccountInternalId"));
						else
							writeoffbalance =0;
					}

					//make the call
					accountBalanceResponse = this.queryData("Account", "BalanceSummary", accountBalanceMap, getAccountServerId());
					accountBalanceResponse.put("WriteoffBalance",writeoffbalance);
					if (accountBalanceResponse.get("PastDueDays1To30") == null ) accountBalanceResponse.put("PastDueDays1To30",0);
					if (accountBalanceResponse.get("PastDueDays31To60") == null ) accountBalanceResponse.put("PastDueDays31To60",0);
					if (accountBalanceResponse.get("PastDueDays61To90") == null ) accountBalanceResponse.put("PastDueDays61To90",0);
					if (accountBalanceResponse.get("PastDueDays91To120") == null ) accountBalanceResponse.put("PastDueDays91To120",0);
					if (accountBalanceResponse.get("PastDueDaysGt120") == null ) accountBalanceResponse.put("PastDueDaysGt120",0);
					accountBalanceResponse.put("CurrentInvoiceBalance", accountBalanceResponse.get("SumBalance"));
					accountBalanceResponse.remove("SumBalance");
					accountBalanceResponse.remove("CurrencyCode");
					
					anAccount.put("Balances", accountBalanceResponse);
					anAccount.put("TotalCount", new Integer(newTotalCount));
					lwo.populateAccount(anAccount);


					if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

					super.addC30SDKObject(lwo);
				}
				recordsProcessed++;
				lastRecordReturned =  i + 1;
			}
			//if (count > 1 ) super.setProcessingAggregateTotalsAttributes(this, newTotalCount, newLastRecordReturned, recordsProcessed );
		}
		else
		{

			// If multiple customer server present then first make the AccountLocateFind and then 
			// Make the AccountFindWithExtendedData on all of the customer servers.

			// Create the accountLocate filter incase of multiple customer servers
			HashMap accountLocateFilter = new HashMap();

			//Set the Fetch flag at the root level,
			//so the call will return all fields for found objects
			accountLocateFilter.put("Fetch", Boolean.TRUE);

		}
	}



	private Map[] findAccountWithExtendedDataJdbcCall(ArrayList<Integer> listneedtobeSearched,Integer c30_kenan_default_cust_server_id) throws C30SDKException 
	{
		C30JDBCDataSource dataSource =this.getFactory().getJDBCDataSource(c30_kenan_default_cust_server_id);
		int totalCount = listneedtobeSearched.size();
		HashMap[] accountList;
		try
		{
			String query="";
			if (listneedtobeSearched.size() ==1 )
			{
				query=C30SDKSqlConstants.QUERY_FIND_ACCOUNT_DATA_USING_ACCOUNT_LIST;
			}
			else
			{
				for (int i=0;i<listneedtobeSearched.size();i++)
				{
					if (i==0) query=query+"(?,";
					else
						if (i==listneedtobeSearched.size()-1)
							query=query+"?) ";
						else
							query=query+"?,";
				}
			}


			PreparedStatement ps = dataSource.getPreparedStatement(query);
			for(int i=0;i<listneedtobeSearched.size();i++)
			{
				ps.setInt(i+1,listneedtobeSearched.get(i).intValue());
			}

			ResultSet resultSet  = ps.executeQuery();
			accountList = new HashMap[totalCount];
			while (resultSet.next()) 
			{
				HashMap account = new HashMap();
				account.put("BillFname", resultSet.getString("BillFname"));

			}
			resultSet.close();
		}
		catch(Exception e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-018"),"INVALID-ATTRIB-018");
		}
		finally {
			try 
			{
				dataSource.closeConnection();
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}
		
		return accountList;
	}

	private ArrayList getAccountListfromSearch(String billFname,
			String billLname, String billCompany, String phone, String zip,
			String city, String state, String billAddress1, boolean includeInactivefilter, int accountExternalIdType) throws C30SDKException,Exception {
		ArrayList<Integer> AccountList = new ArrayList();

		// If only firtname or LastName is given then do a AccountLocateFind....
		if ((billFname !=null || billLname != null  ) && billCompany == null && phone ==null && state==null && city==null && billAddress1==null)
		{
			
			HashMap accountLocateFilter = new HashMap();

			log.debug("Getting account list from the search...");
			//Set the Fetch flag at the root level,
			//so the call will return all fields for found objects
			accountLocateFilter.put("Fetch", Boolean.TRUE);
			HashMap fnamelikeFilter = new HashMap();
			HashMap lnamelikeFilter = new HashMap();
			HashMap externalIdTypeFilter = new HashMap();
			if (billFname !=null)
			{
				fnamelikeFilter.put("Like",billFname);
				accountLocateFilter.put("BillFnameFind", fnamelikeFilter);
			}
			if (billLname !=null)
			{
				lnamelikeFilter.put("Like",billLname);
				accountLocateFilter.put("BillLnameFind", lnamelikeFilter);
			}
			if (new Integer(accountExternalIdType) != null)
			{
				externalIdTypeFilter.put("Equal",new Integer(accountExternalIdType));
				accountLocateFilter.put("AccountExternalIdType", externalIdTypeFilter);
				//if (!((Double)this.getFactory().getAPIVersion()).toString().equalsIgnoreCase(C30VersioningUtils.API_VER_1_0))
				
			}
			HashMap inactiveDateFilter = new HashMap();
			
			if (!includeInactivefilter)
			{
				inactiveDateFilter.put("IsNull", Boolean.TRUE);
				//if (!((Double)this.getFactory().getAPIVersion()).toString().equalsIgnoreCase(C30VersioningUtils.API_VER_1_0))
				accountLocateFilter.put("InactiveDate", inactiveDateFilter);
				accountLocateFilter.put("DateInactive", inactiveDateFilter);

			}


			
			Map callResponse;
			callResponse = this.queryData("AccountLocate", "Find", accountLocateFilter, C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);
			int count =0;
			//get the count to ensure we found an account.
			if (callResponse != null)
				count= ( (Integer) callResponse.get("Count")).intValue();
			// don't go any further if searching for all and none are returned.
			if(count == 0)
			{
				setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				String error = "No account found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
				+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),"INVALID-ATTRIB-013");

			}
			for(int i=0;i<count;i++)
			{
				AccountList.add((Integer) ((HashMap) ((HashMap) ((Object[]) callResponse.get("AccountLocateList"))[i]).get("Key")).get("AccountInternalId"));
			}
			
		}
		else
		{
			
				C30JDBCDataSource dataSource = C30SDKDataSourceUtils.createJDBCDataSource(C30SDKValueConstants.C30_KENAN_DEFAULT_CUST_SERVER_ID);

				C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_search_acct_by_addr");
				//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_lw_field_filter");

				call.addParam(new C30CustomParamDef("account_no", 2, 2000, 2));
				call.addParam(new C30CustomParamDef("V_FIRSTNAME", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("V_LASTNAME", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("V_COMPANY", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("V_PHONE", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("V_ADDRESSLINE1", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("V_CITY", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("V_ZIP", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("V_STATE", 2, 2000, 1));
				call.addParam(new C30CustomParamDef("v_searchinactiveaccts", 2,2000, 1));

				ArrayList paramValues = new ArrayList();
				//--------------------------------
				// set the input parameter values
				//--------------------------------
				paramValues.add(billFname);
				paramValues.add(billLname);
				paramValues.add(billCompany);
				paramValues.add(phone);
				paramValues.add(billAddress1);
				paramValues.add(city);
				paramValues.add(zip);
				paramValues.add(state);
				if (includeInactivefilter)paramValues.add("TRUE");
				else paramValues.add("FALSE");



				dataSource.queryData(call, paramValues, 2);
				log.debug("Total row count = " + dataSource.getRowCount());
				if(dataSource.getRowCount() > 0)
				{          
					for(int i = 0; i < dataSource.getRowCount(); i++)
					{
						if (dataSource.getValueAt(i, 0) != null)
							AccountList.add(new Integer(((BigDecimal)dataSource.getValueAt(i, 0)).intValue()));
					}
				}

		}			
		return AccountList;
	}

	/**
	 * Method to find Kenan FX 1.0 Bill Invoices using C30JDBCDataSource invoking StoredProcedure in the Customer database.
	 * FX 1.0 is a Single Account Server Database so all catalog, admin and customer data is in a single database.
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidConfigurationException
	 */
	private int getWriteOffBalance(Integer accountInternalId)
	throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException
	{
		log.debug("In getWriteOffBalance");
		int writeOffBalance = 0;

		String query = C30SDKSqlConstants.QUERY_WRITEOFF_BALANCE;

		C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId()); 
		try
		{

			PreparedStatement ps = dataSource.getPreparedStatement(query);
			ps.setInt(1,(Integer)accountInternalId);
			ResultSet resultSet  = ps.executeQuery();
			if (resultSet.next()) {
				writeOffBalance = resultSet.getInt("TRAN_AMT"); 

			}

		}
		catch(Exception e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-018"),"INVALID-ATTRIB-018");
		}
		finally {
			try 
			{
				dataSource.closeConnection();
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}
		return writeOffBalance;

	}

	/**
	 * Method to find Kenan FX 1.0 Bill Invoices using C30JDBCDataSource invoking StoredProcedure in the Customer database.
	 * FX 1.0 is a Single Account Server Database so all catalog, admin and customer data is in a single database.
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidConfigurationException
	 */
	private HashMap getWriteOffBalance(ArrayList<Integer> accountInternalIdList)
	throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException
	{
		log.debug("In getWriteOffBalance");
		int writeOffBalance = 0;
		HashMap result = new HashMap();
		String query = C30SDKSqlConstants.QUERY_WRITEOFF_BALANCE_IN_LIST;

		C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId()); 
		try
		{

			if (accountInternalIdList.size() ==1 )
			{
				query=C30SDKSqlConstants.QUERY_WRITEOFF_BALANCE;
			}
			else
			{
				for (int i=0;i<accountInternalIdList.size();i++)
				{
					if (i==0) query=query+"(?,";
					else
						if (i==accountInternalIdList.size()-1)
							query=query+"?) group by account_no";
						else
							query=query+"?,";
				}
			}


			PreparedStatement ps = dataSource.getPreparedStatement(query);
			for(int i=0;i<accountInternalIdList.size();i++)
			{
				ps.setInt(i+1,accountInternalIdList.get(i).intValue());
			}

			ResultSet resultSet  = ps.executeQuery();
			while (resultSet.next()) {
				writeOffBalance = resultSet.getInt("TRAN_AMT");
				int accountId = resultSet.getInt("account_no");
				result.put(accountId, writeOffBalance);
			}

		}
		catch(Exception e)
		{
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-018"),"INVALID-ATTRIB-018");
		}
		finally {
			try 
			{
				dataSource.closeConnection();
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}
		return result;

	}

	private final void getCustomerServerId_DB(Integer acctInternalId, Integer acctSegId) throws C30SDKObjectException
	{
		Integer serverId = new Integer(0);
		try {
			C30ExternalCallDef call = new C30ExternalCallDef("c30_sdk_get_acct_server_id");
			//C30ExternalCallDef call = new C30ExternalCallDef("ps_get_account_server_id");

			//------------------------------------------
			// set up the stored procedure to be called
			//------------------------------------------
			call.addParam(new C30CustomParamDef("account_no", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			call.addParam(new C30CustomParamDef("acct_seg_id", C30CustomParamDef.INT, 10, C30CustomParamDef.INPUT));
			call.addParam(new C30CustomParamDef("server_id", C30CustomParamDef.INT, 3, C30CustomParamDef.OUTPUT));
			//call.addParam(new C30CustomParamDef("ps_get_is_ext_id_generated_cv", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));
			//call.addParam(new C30CustomParamDef("c30_sdk_get_is_ext_id_generated_cv", C30CustomParamDef.INT, 6, C30CustomParamDef.OUTPUT));

			//--------------------------------
			// set the input parameter values
			//--------------------------------
			ArrayList paramValues = new ArrayList();
			paramValues.add(new Integer(acctInternalId.toString()));
			paramValues.add(new Integer(acctSegId.toString()));

			//---------------------------------------
			// make the call and process the results
			//---------------------------------------
			//C30KenanMWDataSource dataSource = this.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES, C30SDKObject.ADMIN_SERVER_ID);
			//C30KenanMWDataSource dataSource = this.getFactory().getMwDataSource(C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);

			//dataSource.queryData(call, paramValues, C30TableDataSource.PARAM_NAMES);

			/*if (dataSource.getValueAt(0,0) != null) {
				serverId = new Integer(dataSource.getValueAt(0, 0).toString());
				log.debug("Customer Server Id: " + serverId);
				//C30SDKCache.setCache(ACCOUNT_SERVER_ID_CACHE, serverId);
				C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>("ACCOUNT_SERVER_ID_CACHE", serverId);
				this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
			} else {
				log.debug("Customer Server Id not found for account " + acctInternalId + " !");
				throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-002"),"LW-OBJ-002");
			}*/
		} catch (Exception e) {
			log.error(e);
			throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-002"),"LW-OBJ-002");
		}
	}

	/**
	 * Validates that all the required dataAttributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing the dataAttributes of the object.
	 * @throws C30SDKKenanFxCoreException 
	 */
	protected final void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_COMPANY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null 
				&& this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_FNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null 
				&& this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_LNAME, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {

			String error = "Either BillCompany, BillFname or BillLname is required for an account to be created";
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-015"),"INVALID-ATTRIB-015");
		}
		super.validateAttributes();
	}

	/**
	 * Creates an account based on the given attributes.
	 * @throws C30SDKException 
	 */
	private void createAccount() throws C30SDKException {

		this.setAttributesFromNameValuePairs();
		this.validateAttributes();

		//Create a HashMap to contain the account info
		Map accountCreate = new HashMap();
		Map account = new HashMap();

		//put account attributes into hashmap
		this.setAccountMapAttributes(account,"CREATE");
		accountCreate.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT, account);

		//add the service center ID's
		//this is done here because its not actually done in the account hashmap but the account create hashmap.
		accountCreate.put(C30SDKAttributeConstants.ATTRIB_SERVER_CATEGORY, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_SERVER_CATEGORY, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		accountCreate.put(C30SDKAttributeConstants.ATTRIB_BILLING_SERVICE_CENTER_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILLING_SERVICE_CENTER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		accountCreate.put(C30SDKAttributeConstants.ATTRIB_REMIT_SERVICE_CENTER_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_REMIT_SERVICE_CENTER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		accountCreate.put(C30SDKAttributeConstants.ATTRIB_INQUIRY_SERVICE_CENTER_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_INQUIRY_SERVICE_CENTER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		accountCreate.put(C30SDKAttributeConstants.ATTRIB_COLLECTION_SERVICE_CENTER_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_COLLECTION_SERVICE_CENTER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		accountCreate.put(C30SDKAttributeConstants.ATTRIB_PRINT_SERVICE_CENTER_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PRINT_SERVICE_CENTER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

		//make account create call.
		Map callResponse = null;
		try {
			callResponse = this.queryData("AccountCreate", accountCreate, C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);
		} catch (C30SDKObjectConnectionException e) {

			// if this error occurred then we want to try and get the geocode and try again
			// because there is a geocoding issue in Kenan where GeoCodeComponentsFind sometimes 
			// gets a goeocode where AccountCreate cannot find it.
			if (e.getCause().getMessage().indexOf("ck_GEOCODE_NOT_FOUND") != -1) {

				//create the geocode component find hashmap
				Map geocodeComponentsFind = new HashMap();
				geocodeComponentsFind.put(C30SDKAttributeConstants.ATTRIB_GEOCODE_CITY, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				geocodeComponentsFind.put(C30SDKAttributeConstants.ATTRIB_GEOCODE_COUNTY, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_COUNTY, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				geocodeComponentsFind.put(C30SDKAttributeConstants.ATTRIB_GEOCODE_STATE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

				//make the geocode request
				callResponse = this.queryData("GeocodeComponentsFind", geocodeComponentsFind, C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);

				int count = ( (Integer) callResponse.get("Count")).intValue();

				if (count < 1) {
					String error = " ("
						+ this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", "
						+ this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_COUNTY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", "
						+ this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ")";
					throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-003")+error,"LW-OBJ-003");
				}

				//get the first geocode in the list.
				Map geocode = (HashMap) ((Object[]) callResponse.get("GeocodeComponentsList"))[0];

				//add the geocode to the account.
				account.put(C30SDKAttributeConstants.ATTRIB_BILL_GEOCODE, geocode.get(C30SDKAttributeConstants.ATTRIB_GEOCODE_GEOCODE));

				//make the account create call again.
				callResponse = this.queryData("AccountCreate", accountCreate, C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);
			} else {
				throw e;
			}
		}

		account = (HashMap) callResponse.get("Account");
		Integer serverId = new Integer (callResponse.get("AccountServerId").toString());
		//C30SDKCache.setCache(ACCOUNT_SERVER_ID_CACHE, serverId);
		C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>("ACCOUNT_SERVER_ID_CACHE", serverId);
		this.getFactory().c30SdkFactoryCache.set(cacheWrapper);
		//remove duplicate extended data - this is a hack to fix a core API-TS bug
		C30HashMapUtils.removeDuplicatesFromArray(account, C30SDKAttributeConstants.ATTRIB_EXTENDED_DATA, C30SDKAttributeConstants.ATTRIB_PARAM_ID);

		populateAccount(account);
	}


	//  jcw added: to update account
	protected void updateAccount()
	throws C30SDKException
	{
		this.setAttributesFromNameValuePairs();

		Map request = new HashMap();
		Map account = new HashMap();
		this.setAccountMapAttributes(account,"UPDATE");

		if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
		{


			Map key = new HashMap();
			key.clear();
			key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			account.put("Key", key);


		} 

		request.put("Account", account);
		Map callResponse = null;
		try
		{
			callResponse = queryData("AccountUpdate", request,getAccountServerId());
		}
		catch(C30SDKObjectConnectionException e)
		{
			if(e.getCause().getMessage().indexOf("ck_GEOCODE_NOT_FOUND") != -1)
			{
				Map geocodeComponentsFind = new HashMap();
				geocodeComponentsFind.put("City", getAttributeValue("BillCity", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				geocodeComponentsFind.put("County", getAttributeValue("BillCounty", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				geocodeComponentsFind.put("State", getAttributeValue("BillState", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				callResponse = queryData("GeocodeComponentsFind", geocodeComponentsFind, C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
				int count = ((Integer)callResponse.get("Count")).intValue();
				if(count < 1)
				{
					String error = " ("
						+ this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_CITY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", "
						+ this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_COUNTY, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ", "
						+ this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_BILL_STATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) + ")";
					throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-003")+error,"LW-OBJ-003");
				}
				Map geocode = (HashMap)((Object[])(Object[])callResponse.get("GeocodeComponentsList"))[0];
				account.put("BillGeocode", geocode.get("Geocode"));
				callResponse = queryData("AccountUpdate", request, C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);
			} else
			{
				throw e;
			}
		}
		account = (HashMap)callResponse.get("Account");
		C30HashMapUtils.removeDuplicatesFromArray(account, "ExtendedData", "ParamId");
		populateAccount(account);
	}


	/**
	 *  Initilize attirbutes for the Kenan FX service order that is about to 
	 * be created or updated through the API TS.
	 * @param account
	 * @param isUpdate
	 * @throws C30SDKException 
	 */
	private void setAccountMapAttributes(Map account, String actionInd)
	throws C30SDKException
	{
		//if we are creating a hierarchy we may need to go out and get the parent INTERNAL ID.
		//check if the parent account external ID has been set and make sure the parent ID has not already been set.
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null
				&& this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PARENT_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null) {

			C30SDKObject parentAccount = this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);

			parentAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			parentAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_PARENT_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			parentAccount = parentAccount.process();
			if (parentAccount instanceof C30Account) {
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_PARENT_ID, parentAccount.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			} else {
				throw new C30SDKObjectException(exceptionResourceBundle.getString("LW-OBJ-004"),(Throwable) ((C30SDKExceptionMessage) parentAccount).getAttributeValue(C30SDKExceptionMessage.ATTRIB_EXCEPTION),"LW-OBJ-004");

			}

		}


		Iterator mapIiter;
		if (!actionInd.equalsIgnoreCase("CREATE")) 
			mapIiter  = updatedInputAttributes.entrySet().iterator();   
		else 
			mapIiter  = getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).entrySet().iterator();  


		while (mapIiter.hasNext()) 
		{ 
			Map.Entry classEntry = (Map.Entry)mapIiter.next();      
			String attributeName = (String)classEntry.getKey();
			C30SDKAttribute lwAttribute = (C30SDKAttribute)classEntry.getValue();
			Object attributeValue = lwAttribute.getValue();
			if (lwAttribute.isInternal())
				continue;
			// filter out internal use attributes
			if (attributeName.equalsIgnoreCase("ObjectType") || attributeName.equalsIgnoreCase("PersistRequestDate") ||
					attributeName.equalsIgnoreCase("IsProcessed") || attributeName.equalsIgnoreCase("ItemActionId"))
				continue;

			try
			{


				// don't set key values dependent on type of operation to be performed
				if (actionInd.equalsIgnoreCase("CREATE")){ 
					if (attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_SERVER_CATEGORY) || 
							attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_BILLING_SERVICE_CENTER_ID) ||
							attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_REMIT_SERVICE_CENTER_ID) ||
							attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_INQUIRY_SERVICE_CENTER_ID) || 
							attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_COLLECTION_SERVICE_CENTER_ID) ||
							attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_PRINT_SERVICE_CENTER_ID)) 
						continue;
				}
				else  // ignore main account identifiers if this is an update 
					if (actionInd.equalsIgnoreCase("UPDATE"))
						if (attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID) || 
								attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE) ||
								attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID)  ) 
							continue;



				if(attributeValue != null)
				{
					// check to see if the name is a number, if so it is an extended data parameter
					if (!isInteger(attributeName)){
						log.debug("Setting account field: "+attributeName+", "+attributeValue);
						if (actionInd.equalsIgnoreCase("FIND"))
						{
							if (attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID))
							{
								Map key = new HashMap();
								account.put("Key", key);

								HashMap accountInternalIdFilter = new HashMap();
								accountInternalIdFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
								key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalIdFilter);
							}
							else // all other non-key values 
							{
								HashMap attributeFilter = new HashMap(); 
								if (attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_BILL_LNAME) || attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_BILL_ADDRESS_1) || attributeName.equalsIgnoreCase(C30SDKAttributeConstants.ATTRIB_BILL_COMPANY) )
									attributeFilter.put("Like", attributeValue);
								else
									attributeFilter.put("Equal", attributeValue);
								account.put(attributeName, attributeFilter);
							}
						}
						else  // is a create or update mapping
							account.put(attributeName, attributeValue);
					}
					else
					{
						// don't search by extended data
						if (actionInd.equalsIgnoreCase("FIND"))
							continue;

						Integer paramId = new Integer(attributeName);
						if(account.get("ExtendedData") == null)
						{
							account.put("ExtendedData", ((Object) (new Object[1])));
							HashMap extData = new HashMap();
							extData.put("ParamId", paramId);
							extData.put("ParamValue", attributeValue);
							Object newDataParams[] = C30HashMapUtils.addObjectToArray((Object[])(Object[])account.get("ExtendedData"), extData);
							account.put("ExtendedData", ((Object) (newDataParams)));
						} else
						{
							C30HashMapUtils.removeDuplicatesFromArray(account, "ExtendedData", "ParamId");
							Object extendedData[] = (Object[])(Object[])account.get("ExtendedData");
							HashMap xdHashMap = new HashMap((HashMap)extendedData[0]);
							xdHashMap.put("ParamId", paramId);
							xdHashMap.put("ParamValue", attributeValue);
							Object newDataParams[] = C30HashMapUtils.addUniqueHashMapToArray(extendedData, xdHashMap, "ParamId");
							account.put("ExtendedData", ((Object) (newDataParams)));
						}
					}
				}
			}
			catch(NumberFormatException e) { }
		} 
	}

	/**
	 * method used to detect Extended data element in output 
	 * @param input
	 * @return
	 */
	private boolean isInteger( String input )  {   
		try{   
			Integer.parseInt( input );   
			return true;  }   
		catch( NumberFormatException e ) {   
			return false;  }    } 



	/**
	 * Method to populate the C30SDK object after an account hashmap has been retrieved.
	 * @param account the returned account hashmap used to populate the C30SDK object
	 * @throws C30SDKInvalidAttributeException if there was an error setting the account attributes.
	 */

	private void populateAccount(Map account)
	throws C30SDKInvalidAttributeException
	{
		// jcw added: LWO Enhancements
		for  ( Iterator it=account.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
			Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
			String key = (String)entry.getKey (  ) ; 
			if (key.equalsIgnoreCase("KEY"))
			{    
				HashMap keyValue = (HashMap)entry.getValue (  ) ; 
				if (keyValue.containsKey(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID)){
					if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, ((HashMap)account.get("Key")).get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
						setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, ((HashMap)account.get("Key")).get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
				}
			}
			else
				if (key.equalsIgnoreCase("SERVERID")){
					if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)){
						Object value = entry.getValue (  ) ;
						if (value != null)
							setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					}
				}
				else 
					if (key.equalsIgnoreCase("EXTENDEDDATA")){
						setAttributeValue("ExtendedDataList",account.get("ExtendedData"),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
						Object extenjdedData[] = (Object[])(Object[])account.get("ExtendedData");
						for (int j = 0; j < extenjdedData.length; j++)
						{ 
							HashMap extData = (HashMap)extenjdedData[j] ; 
							Integer AttrKey = (Integer)extData.get("ParamId");
							Object AttrValue = (Object)extData.get("ParamValue");
							String AttrName = (String)extData.get("ParamName");
							if (isAttribute(AttrKey.toString(), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
								setAttributeValue(AttrKey.toString(), AttrValue, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

							log.debug(AttrKey+": " +  AttrName) ; 
						}
					}
					else
					{
						Object value = entry.getValue (  ) ;
						if (value != null)
							if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
								setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

						if (value != null)
							log.debug(  ( String ) key+" " +  value ) ; 
					} 
			if (key.equalsIgnoreCase("Balances")){
				setAttributeValue("Balances",account.get("Balances"),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);				
			}
			if (key.equalsIgnoreCase("TotalCount")){
				setAttributeValue("TotalCount",account.get("TotalCount"),C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);				
			}

		}

		log.debug("Done populating account response!");
	}


	/**
	 * @return the result of processing the C30SDK object
	 * @throws C30SDKInvalidAttributeException if there was an error while processing the objects attributes.
	 */
	public final C30SDKObject process() throws C30SDKInvalidAttributeException {
		C30SDKObject lwo = null;
		C30SDKObject linkedLwo = null;

		try {    
			C30SDKValueConstants.C30_KENAN_TOTAL_CUST_SERVERS = this.getFactory().getTotalCustomerServers();

			// check to see if we are going to get back more records that the single target Account and also asking for 
			// bundled transaction.
			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_ALL) &&
					this.getC30SDKObjectCount() > 0)
			{
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-016"),"INVALID-ATTRIB-016");
			}


			if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND)||
					this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_ALL)) {
				log.debug("Finding account ...");
				findAccount();

			} else if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_CREATE)) {
				log.debug("Creating account ...");
				createAccount();
				//log.debug("Done creating account! Context Server: " + this.getFactory().getContextServerId());
			}else if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_WITH_BALANCE_SUMMARY)) {
				log.debug("Find Account with Balances ...");
				findAccountWithBalanceSummary();
				//log.debug("Done Finding  account! Context Server: " + this.getFactory().getContextServerId());
			}
			///////////////////////////////////////////////////////////////

			// jcw added : LWO enhancement to update account 
			if(getAttributeValue("ItemActionId", C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_UPDATE))
			{
				log.debug("Updating account ...");
				if (isAttribute("Account", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
				{
					C30Account account = (C30Account)getAttributeValue("Account", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					account.clone(this);
				}
				else
				{
					findAccount();
				}
				updateAccount();
				setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				log.debug("Done updating account! Context Server: " );
			}


			// process all child objects.  Children could include things 
			// like account external ID's that need to be created.
			if (!this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_ALL)
					&& !this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND_WITH_BALANCE_SUMMARY) )   
			{ 
				for (int i = 0; i < this.getC30SDKObjectCount(); i++) {
					// *******  added jcw: LWO Enhancement

					if (getC30SDKObject(i).isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
							!getC30SDKObject(i).isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) 
						getC30SDKObject(i).setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, this, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

					getC30SDKObject(i).setNameValuePair(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));    

					if (getC30SDKObject(i).isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
							!getC30SDKObject(i).isAttributeValueSet(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) 
						getC30SDKObject(i).setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);  
					linkedLwo = getC30SDKObject(i).process();

					if(linkedLwo instanceof C30SDKExceptionMessage)
					{
						removeC30SDKObject(i);
						addC30SDKObject(i,linkedLwo);
						break;
					}

				}
			}
			if(!(lwo instanceof C30SDKExceptionMessage))
			{
				setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				lwo = this;
			}            


			///////////////////////////////////////////////////////////////// 



		} catch (Exception e) {
		    log.error(e);
		    lwo = this.createExceptionMessage(e);
			
		}

		return lwo;
	}

	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	protected void loadCache() {

	}

}
