/*
 * C30BillInvoice.java
 *
 * Created on March 10, 2009, 11:39 PM
 * Created by Tom Ansley
 */

package com.cycle30.sdk.object.kenan.invoice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30StringUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX bill invoice detail row.
 * @author Tom Ansley
 */
public class C30BillInvoiceDetail extends C30SDKObject {

        private static Logger log = Logger.getLogger(C30BillInvoiceDetail.class);

    //***  jcw added: Lwo Enhancement
     
         private Map billInvoiceDetail = null;
         public static final String BILL_REFNO_INVOICE_DETAIL_CALL = "ps_get_bill_invoice_detail";
         public static final String ACCOUNT_INVOICE_DETAIL_CALL = "ps_get_acct_invoice_detail";
     
         public static final String ATTRIB_BILL_REF_NO = "BillRefNo";
        public static final String ATTRIB_BILL_REF_RESETS = "BillRefResets";

        public static final String ATTRIB_AGGR_USAGE_ID = "AggrUsageId";
        public static final String ATTRIB_AMOUNT = "Amount";
        public static final String ATTRIB_AMOUNT_REDUCTION = "AmountReduction";
        public static final String ATTRIB_AMOUNT_REDUCTION_ID = "AmountReductionId";
        public static final String ATTRIB_ARCH_FLAG = "ArchFlag";
        public static final String ATTRIB_BILLING_CATEGORY = "BillingCategory";
        public static final String ATTRIB_BILLING_LEVEL = "BillingLevel";
        public static final String ATTRIB_BILL_CLASS = "BillClass";
        public static final String ATTRIB_BILL_INVOICE = "BillInvoice";
        public static final String ATTRIB_BILL_INVOICE_ROW = "BillInvoiceRow";
        public static final String ATTRIB_CITY_TAX = "CityTax";
        public static final String ATTRIB_COMPONENT_ID = "ComponentId";
        public static final String ATTRIB_COUNTY_TAX = "CountyTax";
        public static final String ATTRIB_DESCRIPTION_CODE = "DescriptionCode";
        public static final String ATTRIB_DISCOUNT = "Discount";
        public static final String ATTRIB_EQUIP_STATUS = "EquipStatus";
        public static final String ATTRIB_FEDERAL_TAX = "FederalTax";
        public static final String ATTRIB_FROM_DATE = "FromDate";
        public static final String ATTRIB_INCLUSIVE_TAX = "InclusiveTax";
        public static final String ATTRIB_OPEN_ITEM_ID = "OpenItemId";
        public static final String ATTRIB_OTHER_TAX = "OtherTax";
        public static final String ATTRIB_PACKAGE_ID = "PackageId";
        public static final String ATTRIB_PREP_SEQUENCE = "PrepSequence";
        public static final String ATTRIB_PRORATE_CODE = "ProrateCode";
        public static final String ATTRIB_RATED_AMOUNT = "RatedAmount";
        public static final String ATTRIB_RATE_CURRENCY_CODE = "RateCurrencyCode";
        public static final String ATTRIB_RATE_TYPE = "RateType";
        public static final String ATTRIB_REV_RCV_COST_CTR = "RevRcvCostCtr";
        public static final String ATTRIB_SECONDARY_AMOUNT = "SecondaryAmount";
        public static final String ATTRIB_STATE_TAX = "StateTax";
        public static final String ATTRIB_SUBSCR_NO = "SubscrNo";
        public static final String ATTRIB_SUBSCR_NO_RESETS = "SubscrNoResets";
        public static final String ATTRIB_SUBTYPE_CODE = "SubtypeCode";
        public static final String ATTRIB_TAX = "Tax";
        public static final String ATTRIB_TAX_PKG_INST_ID = "TaxPkgInstId";
        public static final String ATTRIB_TAX_RATE = "TaxRate";
        public static final String ATTRIB_TEST_FLAG = "TestFlag";
        public static final String ATTRIB_TO_DATE = "ToDate";
        public static final String ATTRIB_TRACKING_ID = "TrackingId";
        public static final String ATTRIB_TRACKING_ID_SERV = "TrackingIdServ";
        public static final String ATTRIB_TRANS_DATE = "TransDate";
        public static final String ATTRIB_TYPE_CODE = "TypeCode";
        public static final String ATTRIB_UNITS = "Units";
        public static final String ATTRIB_ZONE_CLASS = "ZoneClass";


    /**
     * Creates a new instance of C30OrderItem.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30BillInvoiceDetail(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30BillInvoiceDetail(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30BillInvoiceDetail.class);
    }



    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the component.
     */
    protected final void initializeAttributes() throws C30SDKInvalidAttributeException {
             setClassFieldConfiguration();
         }


    /**
     * Method is only used when a find action is requested
     * no validation used for creating a bill invoice detail row
     * @throws C30SDKInvalidAttributeException
     */
    protected final void validateAttributes()
        throws  C30SDKInvalidAttributeException
    {
    
        String acctExternalId = null;
        Integer acctExternalIdType = null;
        Integer acctInternalId = null;
        Integer accountServerId = null;
       
        if  ((isAttribute(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND) )||
             (isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && (Boolean)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT))) {
            
            if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
             acctExternalId = (String)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
              acctExternalIdType = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
               acctInternalId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
            
            
             if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) )
                accountServerId = (Integer)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_SERVER_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT); 
             
                      
            if ((acctExternalId == null || acctExternalIdType == null) && ( acctInternalId == null  && accountServerId == null )) 
            {
                throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-029"),"INVALID-ATTRIB-029");
            }
            if (getAttributeValue(ATTRIB_BILL_REF_NO,C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null)
            {
                throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-029"),"INVALID-ATTRIB-029");
                    
            }
             
        }
        
    }


    /**
     * @return C30SDKObject the bill invoice detail record itself.
     * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
     * @exception C30SDKInvalidAttributeException if there was an exception while processing the detail record.
     */
    public C30SDKObject process()
        throws C30SDKInvalidAttributeException
    {
        C30SDKObject lwo = null;
        
     try{
        if((isAttribute(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.ITEM_ACTION_ID_FIND)||
            (isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && (Boolean)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT))) )
        { 
          
               validateAttributes(); 
               if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
                    isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
                    getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))                    
               {
                         // check to see if we need to explicitly get an account
                       if (super.canServerIdCacheBeSetLocally())
                           setAccountServerIdCache();
                       else{
                               if ((C30SDKObject)getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null){
                                C30SDKObject account = getAccount();
                                   if(account instanceof C30SDKExceptionMessage)
                                     return account;
                               }
                           
                       }
                        setAccountServerId(); 
               }
               if(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0)
                   findK1BillInvoiceDetails();
               else
                   findBillInvoiceDetails();
              
               if(this.billInvoiceDetail != null)
                   setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
               lwo = this;
         
         }
        else{
        ///////// ***********************   end jcw added: Lwo Enhancement
            log.debug("Starting C30BillInvoiceDetail.process()");
            setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            lwo = this;
        }
        }
        catch(Exception e)
        {
            lwo = createExceptionMessage(e);
        }       
 
        return lwo;
    }

    /**
     * Method to get the account associated with the bill invoice.  It is a requirement that the
     * account number be made available as an attribute.  This means we can ensure that the number
     * is correct before attempting to create the bill invoice.
     * @return the returned account which is to be associated with the bill invoice.
     * @throws C30SDKException 
     */
    private C30SDKObject getAccount()
        throws C30SDKException
    {
        log.debug("Starting C30BillInvoice.getAccount");
        C30SDKObject account = null;
        if(getAttributeValue("ObjectParent", C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
        {
            account = (C30SDKObject)getAttributeValue("ObjectParent", C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        } else
        if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT) == null)
        {
            for(int i = 0; i < getC30SDKObjectCount(); i++)
                if(getC30SDKObject(i) instanceof C30Account)
                    account = getC30SDKObject(i);

            if(account == null)
            {
                account = getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30Account.class, new Object[0]);   
                if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)
                    account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)           
                        account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)           
                    account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                account = account.process();
            }
        }
        if(!(account instanceof C30SDKExceptionMessage))
        {
            setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, account, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            setAccountLevelAttributes(account);
        }
        log.debug("Finished C30BillInvoiceDetail.getAccount");
        return account;
    }


    /**
     *      
     * Method which sets all account data to be usee by the object.
     * @param account
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKObjectException
     */
     private void setAccountLevelAttributes(C30SDKObject account)
         throws C30SDKInvalidAttributeException, C30SDKObjectException
     {
     
         if(account instanceof C30Account)
            if (account != null)
            {
                if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null)
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) 
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))     
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))       
                    setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            }
     }



 
    /**
     * jcw added: Lwo Enhancements
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private void findBillInvoiceDetails()
        throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException
    {
            HashMap invoiceFilter = new HashMap();
            invoiceFilter.put("Fetch", Boolean.TRUE);
            int startIdx = 0;
           
                Map key = new HashMap();
                invoiceFilter.put("Key", key);
                HashMap accountExternalIdFilter = new HashMap();
                accountExternalIdFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                key.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountExternalIdFilter);
                HashMap attrFilter = new HashMap();
                attrFilter.put("Equal", getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                key.put(ATTRIB_BILL_REF_NO, attrFilter);
                attrFilter = new HashMap();
                attrFilter.put("Equal", getAttributeValue(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
                key.put(ATTRIB_BILL_REF_RESETS, attrFilter);
            
       Map callResponse = queryData("InvoiceDetail", "Find", invoiceFilter,  getAccountServerId());
       int count = ((Integer)callResponse.get("Count")).intValue();

        if(count == 0)
        {
            setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            return;
        }
        C30BillInvoiceDetail cloneLwo = (C30BillInvoiceDetail)super.clone();
         for (int i=startIdx; i < count; i++)
         {
                
             HashMap invoiceDetail = (HashMap)((Object[])(Object[])callResponse.get("InvoiceDetailList"))[i];   
                
             if (i == startIdx){
               this.billInvoiceDetail = invoiceDetail;
               this.populateBillInvoiceDetail();
               this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
             }
             else
             {
                 C30BillInvoiceDetail lwo = (C30BillInvoiceDetail)cloneLwo.clone();
                 lwo.billInvoiceDetail = invoiceDetail;
                 lwo.populateBillInvoiceDetail();
                 // add to our list of peer objects
                 lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                 super.addC30SDKObject(lwo);
                 
             }  

         }
         
        }
    
    /**
    * jcw added: Lwo Enhancement
    * this method uses the new defined Attributes approach to loading the lwo object
    * @throws C30SDKInvalidAttributeException
    */
    private void populateBillInvoiceDetail()
     throws C30SDKInvalidAttributeException
    {
                       for  ( Iterator it=billInvoiceDetail.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
                                Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
                                String key = (String)entry.getKey (  ) ; 
                                if (key.equalsIgnoreCase("KEY"))
                                {    
                                  HashMap keyValue = (HashMap)entry.getValue (  ) ; 
                                  if (keyValue.containsKey(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID)){
                                      if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                          setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, ((HashMap)billInvoiceDetail.get("Key")).get("AccountInternalId"), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                      if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                                          setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, ((HashMap)billInvoiceDetail.get("Key")).get("AccountInternalId"), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
                                  }
                                   if (keyValue.containsKey(ATTRIB_BILL_REF_NO)){
                                        if (isAttribute(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                            setAttributeValue(ATTRIB_BILL_REF_NO, ((HashMap)billInvoiceDetail.get("Key")).get(ATTRIB_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                        if (isAttribute(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                                            setAttributeValue(ATTRIB_BILL_REF_NO, ((HashMap)billInvoiceDetail.get("Key")).get(ATTRIB_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
                                    }
                                    if (keyValue.containsKey(ATTRIB_BILL_REF_RESETS)){
                                         if (isAttribute(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                                             setAttributeValue(ATTRIB_BILL_REF_RESETS, ((HashMap)billInvoiceDetail.get("Key")).get("BillRefResets"), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                         if (isAttribute(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT))
                                             setAttributeValue(ATTRIB_BILL_REF_RESETS, ((HashMap)billInvoiceDetail.get("Key")).get("BillRefResets"), C30SDKValueConstants.ATTRIB_TYPE_INPUT);    
                                     }
                                }
                                else
                                {
                                   Object value = entry.getValue (  ) ;
                                   if (value != null)
                                   {
                                       if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)){
                                           setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                                           log.debug(  ( String ) key+" " +  value +"   SET") ; 
                                          
                                       }
                                       else
                                         if (value != null)
                                           log.debug(  ( String ) key+" " +  value ) ; 
                                       
                                   }
                                }
                       }
    }
                  
    
    
    private void findK1BillInvoiceDetails()
    throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException
    {
    log.debug("In getK1BillInvoiceDetails");
    boolean searchByBillRefNo = false;
    String procCall;
    if (getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null){
       searchByBillRefNo = true;
       procCall = BILL_REFNO_INVOICE_DETAIL_CALL;
    }
    else
       procCall = ACCOUNT_INVOICE_DETAIL_CALL;
       
    C30ExternalCallDef call = new C30ExternalCallDef(procCall);
      try
     {
           C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId());     
    
           if (searchByBillRefNo)
           {
               call.addParam(new C30CustomParamDef("bill_ref_no", 1, 10, 1));
               call.addParam(new C30CustomParamDef("bill_ref_resets", 1, 3, 1));
           }
           else
                  call.addParam(new C30CustomParamDef("ACCOUNT_NO", 1, 10, 1));
                  
           call.addParam(new C30CustomParamDef("ACCOUNT_NO", 9, 10, 2));
           call.addParam(new C30CustomParamDef("BILL_REF_NO", 9, 10, 2));
           call.addParam(new C30CustomParamDef("BILL_REF_RESETS", 1, 3, 2));
           call.addParam(new C30CustomParamDef("BILL_SEQUENCE_NUM", 9, 10, 2));
           call.addParam(new C30CustomParamDef("PROCESS_NUM", 2, 1, 2));
           call.addParam(new C30CustomParamDef("PREV_BILL_REFNO", 9, 10, 2));
           call.addParam(new C30CustomParamDef("PREV_BILL_REF_RESETS", 1, 3, 2));
           call.addParam(new C30CustomParamDef("PREV_BALANCE_REFNO", 9, 10, 2));
           call.addParam(new C30CustomParamDef("PREV_BALANCE_REF_RESETS", 1, 3, 2));
           call.addParam(new C30CustomParamDef("ZIP", 2, 16, 2));
           call.addParam(new C30CustomParamDef("FROM_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("TO_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("NEXT_TO_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("PREP_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("TAX_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("STATEMENT_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("PAYMENT_DUE_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("PREV_PPDD", 3, 2));
           call.addParam(new C30CustomParamDef("PREV_CUTOFF_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("BILL_PERIOD", 2, 3, 2));
           call.addParam(new C30CustomParamDef("CURRENCY_CODE", 1, 6, 2));
           call.addParam(new C30CustomParamDef("PAY_METHOD", 1, 6, 2));
           call.addParam(new C30CustomParamDef("BILL_DISP_METH", 1, 6, 2));
           call.addParam(new C30CustomParamDef("ACCOUNT_STATUS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("IMAGE_REQ", 1, 1, 2));
           call.addParam(new C30CustomParamDef("IMAGE_DONE", 1, 1, 2));
           call.addParam(new C30CustomParamDef("SPECIAL_CODE", 9, 10, 2));
           call.addParam(new C30CustomParamDef("PREP_TASK", 2, 16, 2));
           call.addParam(new C30CustomParamDef("PREP_STATUS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("FORMAT_STATUS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("DISPATCH_COUNT", 1, 6, 2));
           call.addParam(new C30CustomParamDef("PAGE_COUNT", 1, 6, 2));
           call.addParam(new C30CustomParamDef("BACKOUT_STATUS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("COLLECTION_HISTORY", 2, 12, 2));
           call.addParam(new C30CustomParamDef("JNL_EARNED_THRU_DT", 3, 2));
           call.addParam(new C30CustomParamDef("JNL_STATUS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("TEST_FLAG", 1, 1, 2));
           call.addParam(new C30CustomParamDef("ARCH_FLAG", 1, 1, 2));
           call.addParam(new C30CustomParamDef("INTERIM_BILL_FLAG", 1, 1, 2));
           call.addParam(new C30CustomParamDef("INCLUDE_NRC", 1, 1, 2));
           call.addParam(new C30CustomParamDef("INCLUDE_RC", 1, 1, 2));
           call.addParam(new C30CustomParamDef("INCLUDE_ADJ", 1, 1, 2));
           call.addParam(new C30CustomParamDef("INCLUDE_USAGE", 1, 1, 2));
           call.addParam(new C30CustomParamDef("INCLUDE_BMF", 1, 1, 2));
           call.addParam(new C30CustomParamDef("CONVERTED", 1, 3, 2));
           call.addParam(new C30CustomParamDef("WARM_BILL_FLAG", 1, 1, 2));
           call.addParam(new C30CustomParamDef("DET_FROM_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("DET_TO_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("DET_TEST_FLAG", 1, 1, 2));
           call.addParam(new C30CustomParamDef("DET_ARCH_FLAG", 1, 1, 2));
           call.addParam(new C30CustomParamDef("BILL_INVOICE_ROW", 9, 10, 2));
           call.addParam(new C30CustomParamDef("PACKAGE_ID", 9, 10, 2));
           call.addParam(new C30CustomParamDef("COMPONENT_ID", 9, 10, 2));
           call.addParam(new C30CustomParamDef("TYPE_CODE", 1, 6, 2));
           call.addParam(new C30CustomParamDef("SUBTYPE_CODE", 9, 10, 2));
           call.addParam(new C30CustomParamDef("TRACKING_ID", 9, 10, 2));
           call.addParam(new C30CustomParamDef("TRACKING_ID_SERV", 1, 3, 2));
           call.addParam(new C30CustomParamDef("PREP_SEQUENCE", 1, 3, 2));
           call.addParam(new C30CustomParamDef("PRORATE_CODE", 9, 10, 2));
           call.addParam(new C30CustomParamDef("BILLING_LEVEL", 1, 6, 2));
           call.addParam(new C30CustomParamDef("BILLING_CATEGORY", 1, 6, 2));
           call.addParam(new C30CustomParamDef("AMOUNT", 9, 18, 2));
           call.addParam(new C30CustomParamDef("RATED_AMOUNT", 9, 18, 2));
           call.addParam(new C30CustomParamDef("SECONDARY_AMOUNT", 9, 18, 2));
           call.addParam(new C30CustomParamDef("RATE_CURRENCY_CODE", 1, 6, 2));
           call.addParam(new C30CustomParamDef("UNITS", 9, 10, 2));
           call.addParam(new C30CustomParamDef("TAX", 9, 18, 2));
           call.addParam(new C30CustomParamDef("TAX_RATE", 9, 10, 2));
           call.addParam(new C30CustomParamDef("TAX_PKG_INST_ID", 9, 10, 2));
           call.addParam(new C30CustomParamDef("DISCOUNT", 9, 18, 2));
           call.addParam(new C30CustomParamDef("TRANS_DATE", 3, 2));
           call.addParam(new C30CustomParamDef("DESCRIPTION_CODE", 9, 10, 2));
           call.addParam(new C30CustomParamDef("RATE_TYPE", 9, 10, 2));
           call.addParam(new C30CustomParamDef("SUBSCR_NO", 9, 10, 2));
           call.addParam(new C30CustomParamDef("SUBSCR_NO_RESETS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("EQUIP_STATUS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("FEDERAL_TAX", 9, 18, 2));
           call.addParam(new C30CustomParamDef("STATE_TAX", 9, 18, 2));
           call.addParam(new C30CustomParamDef("COUNTY_TAX", 9, 18, 2));
           call.addParam(new C30CustomParamDef("CITY_TAX", 9, 18, 2));
           call.addParam(new C30CustomParamDef("OTHER_TAX", 9, 18, 2));
           call.addParam(new C30CustomParamDef("REV_RCV_COST_CTR", 1, 6, 2));
           call.addParam(new C30CustomParamDef("AGGR_USAGE_ID", 9, 10, 2));
           call.addParam(new C30CustomParamDef("OPEN_ITEM_ID", 9, 10, 2));
           call.addParam(new C30CustomParamDef("AMOUNT_REDUCTION", 9, 18, 2));
           call.addParam(new C30CustomParamDef("AMOUNT_REDUCTION_ID", 1, 10, 2));
           call.addParam(new C30CustomParamDef("ZONE_CLASS", 1, 3, 2));
           call.addParam(new C30CustomParamDef("BILL_CLASS", 1, 6, 2));
           call.addParam(new C30CustomParamDef("INCLUSIVE_TAX", 9, 18, 2));
         
       ArrayList paramValues = new ArrayList();
      if (searchByBillRefNo)
      {
           paramValues.add(getAttributeValue(ATTRIB_BILL_REF_NO, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
           paramValues.add(getAttributeValue(ATTRIB_BILL_REF_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
      }
      else
           paramValues.add(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
           
       dataSource.queryData(call, paramValues, 2);
       log.debug("Total row count = " + dataSource.getRowCount());
       int rowCount = dataSource.getRowCount();
       if(rowCount == 0)
       {
           setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
           return;
       }

       Map resultSet  = dataSource.getResultMap(); 
    
       C30BillInvoiceDetail cloneLwo = (C30BillInvoiceDetail)super.clone();
       for (int i=0; i < rowCount; i++)
       {      
                    Hashtable invoiceDetail = (Hashtable)resultSet.get(i);

                    if (i == 0){
                      this.billInvoiceDetail = invoiceDetail;
                      this.populateK1BillInvoiceDetail();
                      this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                    }
                    else
                    {
                        C30BillInvoiceDetail lwo = (C30BillInvoiceDetail)cloneLwo.clone();
                        lwo.billInvoiceDetail = invoiceDetail;
                        lwo.populateK1BillInvoiceDetail();
                        // add to our list of peer objects
                        lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
                        super.addC30SDKObject(lwo);
                    }  
       }
 
       }
       catch(Exception e)
       {
           
           throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-028"),"INVALID-ATTRIB-028");
       }
    
    
    }
    
    
    /**
    * jcw added: Lwo Enhancement
    * this method uses the new defined Attributes approach to loading the lwo object
    * @throws C30SDKInvalidAttributeException
    */
   // private void populateK1BillInvoiceDetail(Hashtable invoiceDetail)
    private void populateK1BillInvoiceDetail()
     throws C30SDKInvalidAttributeException
    {
        
    /*             
    Enumeration it=invoiceDetail.keys();
    while (  it.hasMoreElements() )
    {
            String key = (String)it.nextElement (  ) ;  
     */
     for  ( Iterator it=billInvoiceDetail.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
              Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
              String key = (String)entry.getKey (  ) ; 
     
            Object value = billInvoiceDetail.get(key) ;
            key = C30StringUtils.SqlToKenanName(key);
            
            if (value != null)
            {
                if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
                {
                    setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
                   
                    log.debug(  ( String ) key+" " +  value +"   SET") ;                            
                }
               else
                  if (value != null){
                    log.debug(  ( String ) key+" " +  value ) ; 
                   
                  }
                       
            }
     }
    }
    


    /**
     * Method which gets any data to be cached for further use by the object.
     */
    protected void loadCache() {

    }

}
