/*
 * C30Nrc.java
 *
 * Created on May 17, 2006, 11:39 PM
 *
 */

package com.cycle30.sdk.object.kenan.ordering;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.account.C30Account;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * Wrapper class encapsulating a Kenan/FX NRC object and its corresponding order item.
 * @author Joe Morales
 */
public class C30Nrc extends C30OrderItem {

    private static Logger log = Logger.getLogger(C30Nrc.class);

    public static final String ATTRIB_ACCOUNT_INTERNAL_ID = "AccountInternalId";
    public static final String ATTRIB_ANNOTATION = "Annotation";
    public static final String ATTRIB_ANNOTATION2 = "Annotation2";
    public static final String ATTRIB_ARCH_FLAG = "ArchFlag";
	public static final String ATTRIB_BILL_ORDER_NUMBER = "BillOrderNumber";
	public static final String ATTRIB_BILL_REF_NO = "BillRefNo";
	public static final String ATTRIB_BILL_REF_RESETS = "BillRefResets";
	public static final String ATTRIB_BILLING_ACCOUNT_INTERNAL_ID = "BillingAccountInternalId";
    public static final String ATTRIB_CCARD_ID = "CcardId";
    public static final String ATTRIB_CCARD_ID_SERV = "CcardIdServ";
    public static final String ATTRIB_CHG_WHO = "ChgWho";
	public static final String ATTRIB_CHG_DT = "ChgDt";
	public static final String ATTRIB_CREATE_DT = "CreateDt";
    public static final String ATTRIB_CITY_TAX = "CityTax";
    public static final String ATTRIB_CONTRACT_ASSOCIATION_TYPE = "ContractAssociationType";
    public static final String ATTRIB_CONTRACT_TRACKING_ID = "ContractTrackingId";
    public static final String ATTRIB_CONTRACT_TRACKING_ID_SERV = "ContractTrackingIdServ";
    public static final String ATTRIB_COUNTY_TAX = "CountyTax";
    public static final String ATTRIB_CURRENCY_CODE = "CurrencyCode";
    public static final String ATTRIB_CURRENT_INSTALLMENT = "CurrentInstallment";
    public static final String ATTRIB_CUSTOMER_ORDER_NUMBER = "CustomerOrderNumber";
    public static final String ATTRIB_DATE_NRC_JOURNALABLE = "DateNrcJournalable";
    public static final String ATTRIB_EFFECTIVE_DATE = "EffectiveDate";
    public static final String ATTRIB_ELEMENT_ID = "ElementId";
    public static final String ATTRIB_FEDERAL_TAX = "FederalTax";
    public static final String ATTRIB_FIRST_INSTALLMENT_AMOUNT = "FirstInstallmentAmount";
    private static final String ATTRIB_INACTIVE_DATE = "InactiveDate";
    public static final String ATTRIB_INTENDED_VIEW_EFFECTIVE_DT = "IntendedViewEffectiveDt";
    public static final String ATTRIB_IS_VIEWABLE = "IsViewable";
    public static final String ATTRIB_LAST_REVIEWED_BY = "LastReviewedBy";
    public static final String ATTRIB_NO_BILL = "NoBill";
    public static final String ATTRIB_NRC_CATEGORY = "NrcCategory";
    public static final String ATTRIB_OPEN_ITEM_ID = "OpenItemId";
    public static final String ATTRIB_ORDER_NUMBER = "OrderNumber";
    public static final String ATTRIB_OTHER_TAX = "OtherTax";
    public static final String ATTRIB_PARENT_TRACKING_ID = "ParentTrackingId";
    public static final String ATTRIB_PARENT_TRACKING_ID_SERV = "ParentTrackingIdserv";
	public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID = "ParentServiceInternalId";
	public static final String ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS = "ParentServiceInternalIdResets";
	public static final String ATTRIB_PREV_VIEW_ID = "PrevViewId";
	public static final String ATTRIB_PROFILE_ID = "ProfileId";
    public static final String ATTRIB_RATE = "Rate";
    private static final String ATTRIB_RATE_CLASS = "RateClass";
    public static final String ATTRIB_RATE_DT = "RateDt";
    public static final String ATTRIB_REQUEST_STATUS = "RequestStatus";
    public static final String ATTRIB_REVIEW_DT = "ReviewDt";
    public static final String ATTRIB_SALES_CHANNEL_ID = "SalesChannelId";
    public static final String ATTRIB_SALES_ID = "SalesId";
    public static final String ATTRIB_SHIP_FROM_GEOCODE = "ShipFromGeocode";
    public static final String ATTRIB_SHIP_TO_GEOCODE = "ShipToGeocode";
    public static final String ATTRIB_STATE_TAX = "StateTax";
    public static final String ATTRIB_SUPERVISOR_NAME = "SupervisorName";
	public static final String ATTRIB_TRACKING_ID = "TrackingId";
	public static final String ATTRIB_TRACKING_ID_SERV = "TrackingIdServ";
	public static final String ATTRIB_TRANSACT_DATE = "TransactDate";
    public static final String ATTRIB_TAX_PKG_INST_ID= "TaxPkgInstId";
    public static final String ATTRIB_TAX_TYPE_CODE = "TaxTypeCode";
    public static final String ATTRIB_TOTAL_INSTALLMENTS = "TotalInstallments";
    public static final String ATTRIB_TRANS_SIGN = "TransSign";
    public static final String ATTRIB_TYPE_ID_NRC = "TypeIdNrc";
	public static final String ATTRIB_VIEW_CREATED_DT = "ViewCreatedDt";
	public static final String ATTRIB_VIEW_EFFECTIVE_DT = "ViewEffectiveDt";
	public static final String ATTRIB_VIEW_STATUS = "ViewStatus";

	/** Search Attribute **/
    public static final String ATTRIB_SEARCH_ANNOTATION = "SearchAnnotation";
    public static final String ATTRIB_SEARCH_ANNOTATION2 = "SearchAnnotation2";
    public static final String ATTRIB_SEARCH_ARCH_FLAG = "SearchArchFlag";
	public static final String ATTRIB_SEARCH_BILL_ORDER_NUMBER = "SearchBillOrderNumber";
	public static final String ATTRIB_SEARCH_BILL_REF_NO = "SearchBillRefNo";
	public static final String ATTRIB_SEARCH_BILL_REF_RESETS = "SearchBillRefResets";
	public static final String ATTRIB_SEARCH_BILLING_ACCOUNT_INTERNAL_ID = "SearchBillingAccountInternalId";
    public static final String ATTRIB_SEARCH_CCARD_ID = "SearchCcardId";
    public static final String ATTRIB_SEARCH_CCARD_ID_SERV = "SearchCcardIdServ";
    public static final String ATTRIB_SEARCH_CHG_WHO = "SearchChgWho";
	public static final String ATTRIB_SEARCH_CHG_DT = "SearchChgDt";
	public static final String ATTRIB_SEARCH_CREATE_DT = "SearchCreateDt";
    public static final String ATTRIB_SEARCH_CITY_TAX = "SearchCityTax";
    public static final String ATTRIB_SEARCH_CONTRACT_ASSOCIATION_TYPE = "SearchContractAssociationType";
    public static final String ATTRIB_SEARCH_CONTRACT_TRACKING_ID = "SearchContractTrackingId";
    public static final String ATTRIB_SEARCH_CONTRACT_TRACKING_ID_SERV = "SearchContractTrackingIdServ";
    public static final String ATTRIB_SEARCH_COUNTY_TAX = "SearchCountyTax";
    public static final String ATTRIB_SEARCH_CURRENCY_CODE = "SearchCurrencyCode";
    public static final String ATTRIB_SEARCH_CURRENT_INSTALLMENT = "SearchCurrentInstallment";
    public static final String ATTRIB_SEARCH_CUSTOMER_ORDER_NUMBER = "SearchCustomerOrderNumber";
    public static final String ATTRIB_SEARCH_DATE_NRC_JOURNALABLE = "SearchDateNrcJournalable";
    public static final String ATTRIB_SEARCH_EFFECTIVE_DATE = "SearchEffectiveDate";
    public static final String ATTRIB_SEARCH_ELEMENT_ID = "SearchElementId";
    public static final String ATTRIB_SEARCH_FEDERAL_TAX = "SearchFederalTax";
    public static final String ATTRIB_SEARCH_FIRST_INSTALLMENT_AMOUNT = "SearchFirstInstallmentAmount";
	public static final String ATTRIB_SEARCH_INTENDED_VIEW_EFFECTIVE_DT = "SearchIntendedViewEffectiveDt";
	public static final String ATTRIB_SEARCH_IS_VIEWABLE = "SearchIsViewable";
    public static final String ATTRIB_SEARCH_LAST_REVIEWED_BY = "SearchLastReviewedBy";
    public static final String ATTRIB_SEARCH_NO_BILL = "SearchNoBill";
    public static final String ATTRIB_SEARCH_NRC_CATEGORY = "SearchNrcCategory";
    public static final String ATTRIB_SEARCH_OPEN_ITEM_ID = "SearchOpenItemId";
    public static final String ATTRIB_SEARCH_ORDER_NUMBER = "SearchOrderNumber";
    public static final String ATTRIB_SEARCH_OTHER_TAX = "SearchOtherTax";
    public static final String ATTRIB_SEARCH_PARENT_TRACKING_ID = "SearchParentTrackingId";
    public static final String ATTRIB_SEARCH_PARENT_TRACKING_ID_SERV = "SearchParentTrackingIdserv";
	public static final String ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID = "SearchParentServiceInternalId";
	public static final String ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID_RESETS = "SearchParentServiceInternalIdResets";
	public static final String ATTRIB_SEARCH_PREV_VIEW_ID = "SearchPrevViewId";
	public static final String ATTRIB_SEARCH_PROFILE_ID = "SearchProfileId";
    public static final String ATTRIB_SEARCH_RATE = "SearchRate";
    private static final String ATTRIB_SEARCH_RATE_DT = "SearchRateDt";
    public static final String ATTRIB_SEARCH_REQUEST_STATUS = "SearchRequestStatus";
    public static final String ATTRIB_SEARCH_REVIEW_DT = "SearchReviewDt";
    public static final String ATTRIB_SEARCH_SALES_CHANNEL_ID = "SearchSalesChannelId";
    public static final String ATTRIB_SEARCH_SALES_ID = "SearchSalesId";
    public static final String ATTRIB_SEARCH_SHIP_FROM_GEOCODE = "SearchShipFromGeocode";
    public static final String ATTRIB_SEARCH_SHIP_TO_GEOCODE = "SearchShipToGeocode";
    public static final String ATTRIB_SEARCH_STATE_TAX = "SearchStateTax";
    public static final String ATTRIB_SEARCH_SUPERVISOR_NAME = "SearchSupervisorName";
	public static final String ATTRIB_SEARCH_TRACKING_ID = "SearchTrackingId";
	public static final String ATTRIB_SEARCH_TRACKING_ID_SERV = "SearchTrackingIdServ";
	public static final String ATTRIB_SEARCH_TRANSACT_DATE = "SearchTransactDate";
    public static final String ATTRIB_SEARCH_TAX_PKG_INST_ID= "SearchTaxPkgInstId";
    public static final String ATTRIB_SEARCH_TAX_TYPE_CODE = "SearchTaxTypeCode";
    public static final String ATTRIB_SEARCH_TOTAL_INSTALLMENTS = "SearchTotalInstallments";
    public static final String ATTRIB_SEARCH_TRANS_SIGN = "SearchTransSign";
    private static final String ATTRIB_SEARCH_TYPE_ID_NRC = "SearchTypeIdNrc";
	public static final String ATTRIB_SEARCH_VIEW_CREATED_DT = "SearchViewCreatedDt";
	public static final String ATTRIB_SEARCH_VIEW_EFFECTIVE_DT = "SearchViewEffectiveDt";
	public static final String ATTRIB_SEARCH_VIEW_STATUS = "SearchViewStatus";

    //private Map nrc;
    private Map rateNrc;
    private Map nrcTransDescr;

    /**
     * Creates a new instance of C30OrderItem.
     * @param factory the factory associated with the current session.
     * @param objectType the object type being created.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Nrc(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, objectType);
    }

    /**
     * This constructor is used for objects that are not derived from a configuration in the database.  It is not always
     * true that objects must be derived from the database.  This kind of constructor is often used throughout the framework
     * to create objects.  It is also used by the BRE front end.
     * @param factory the factory associated with the current session.
     * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during initialization
     * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during initialization
     * @throws C30SDKObjectException if there was a problem initializing the object during initialization
     * @throws C30SDKInvalidAttributeException if there was a problem initializing the attributes of the object during initialization
     * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
     */
    public C30Nrc(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
        super(factory, C30Nrc.class);
    }

    /**
     * Initializes the attributes of the Kenan/FX Service Order Item.
     */
    protected void initializeAttributes() throws C30SDKInvalidAttributeException {

    	super.initializeAttributes();

        //===================
        // Data Attributes
        //===================
		this.addAttribute(new C30SDKAttribute(ATTRIB_ANNOTATION, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ANNOTATION2, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ARCH_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_REF_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_REF_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CHG_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CITY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_ASSOCIATION_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_COUNTY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CURRENT_INSTALLMENT, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CUSTOMER_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_DATE_NRC_JOURNALABLE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_EFFECTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_FEDERAL_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_FIRST_INSTALLMENT_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_LAST_REVIEWED_BY, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_NO_BILL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_NRC_CATEGORY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_OTHER_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_RATE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_RATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_REQUEST_STATUS, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_REVIEW_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SHIP_FROM_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SHIP_TO_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_STATE_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SUPERVISOR_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TAX_PKG_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TAX_TYPE_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TOTAL_INSTALLMENTS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRANS_SIGN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRANSACT_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TYPE_ID_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_CREATED_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
    		this.addAttribute(new C30SDKAttribute(ATTRIB_CCARD_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		this.addAttribute(new C30SDKAttribute(ATTRIB_CCARD_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
    		this.addAttribute(new C30SDKAttribute(ATTRIB_IS_VIEWABLE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		this.addAttribute(new C30SDKAttribute(ATTRIB_PROFILE_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }

        //===================
        // Search Attributes
        //===================
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ANNOTATION, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ANNOTATION2, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ARCH_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILL_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILL_REF_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILL_REF_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_BILLING_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CHG_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CITY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONTRACT_ASSOCIATION_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONTRACT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CONTRACT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_COUNTY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CURRENT_INSTALLMENT, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CUSTOMER_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_DATE_NRC_JOURNALABLE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_EFFECTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_FEDERAL_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_FIRST_INSTALLMENT_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_INTENDED_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_LAST_REVIEWED_BY, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_NO_BILL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_NRC_CATEGORY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_OTHER_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PARENT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PREV_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_RATE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_RATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_REQUEST_STATUS, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_REVIEW_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SALES_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SHIP_FROM_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SHIP_TO_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_STATE_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_SUPERVISOR_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TAX_PKG_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TAX_TYPE_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TOTAL_INSTALLMENTS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRANS_SIGN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TRANSACT_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_TYPE_ID_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_CREATED_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_VIEW_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
    		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CCARD_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_CCARD_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
    		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_IS_VIEWABLE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		this.addAttribute(new C30SDKAttribute(ATTRIB_SEARCH_PROFILE_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        }
        
        //===================
        // Output Attributes
        //===================
		this.addAttribute(new C30SDKAttribute(ATTRIB_ANNOTATION, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ANNOTATION2, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ARCH_FLAG, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_REF_NO, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILL_REF_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CHG_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CHG_WHO, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CITY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_ASSOCIATION_TYPE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CONTRACT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_COUNTY_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CREATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CURRENCY_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CURRENT_INSTALLMENT, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_CUSTOMER_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_DATE_NRC_JOURNALABLE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_EFFECTIVE_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ELEMENT_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_FEDERAL_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_FIRST_INSTALLMENT_AMOUNT, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_LAST_REVIEWED_BY, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_NO_BILL, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_NRC_CATEGORY, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_OPEN_ITEM_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_ORDER_NUMBER, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_OTHER_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PARENT_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_PREV_VIEW_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_RATE, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_RATE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_REQUEST_STATUS, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_REVIEW_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_CHANNEL_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SALES_ID, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SHIP_FROM_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SHIP_TO_GEOCODE, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_STATE_TAX, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_SUPERVISOR_NAME, String.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TAX_PKG_INST_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TAX_TYPE_CODE, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TOTAL_INSTALLMENTS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRACKING_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRANS_SIGN, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TRANSACT_DATE, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_TYPE_ID_NRC, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_CREATED_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_EFFECTIVE_DT, Date.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.addAttribute(new C30SDKAttribute(ATTRIB_VIEW_STATUS, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
    		this.addAttribute(new C30SDKAttribute(ATTRIB_CCARD_ID, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    		this.addAttribute(new C30SDKAttribute(ATTRIB_CCARD_ID_SERV, Integer.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
    		this.addAttribute(new C30SDKAttribute(ATTRIB_IS_VIEWABLE, Boolean.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    		this.addAttribute(new C30SDKAttribute(ATTRIB_PROFILE_ID, BigInteger.class), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
		C30SDKAttribute attribute = this.getAttribute(ATTRIB_MEMBER_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
		attribute.setOverrideable(false);
		attribute.setValue(C30OrderItem.ORD_ITEM_NRC_MEMBER_TYPE);
    }

    /* (non-Javadoc)
     * @see com.cycle30.sdk.core.framework.C30SDKObject#validateAttributes()
     */
    protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
        
    	super.validateAttributes();

        if (!this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_CONNECT_ITEM_ACTION_ID)){

            String error = "Invalid item action id: (" + this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) +
                    ").  Item action id 10 must be used for an NRC";
            throw new C30SDKInvalidAttributeException(error);
        }
    }

    /**
     * This method allows a user to set the service attributes.  Service attributes
     * should only be set in a change or a connect order. All other order types
     * are logical sub orders and do no support service field initialization.
     * For this reason this method handled connect/change scenarios.
     * @throws C30SDKException 
     */
    protected C30SDKObject createFXObject() throws C30SDKException {

    	C30SDKObject lwo = null;
    	
        //create hashmap shell
        Map key = new HashMap();
        Map nrcData = new HashMap();
        nrcData.put("Key", key);

        //----------------------------------------------------------------------
        // Set the following key service fields:
        // View Status: Pending
        // View Effective Date: Now
        // View Create Date: Now
        // Service External Id Type: based on member_id of the parent order item
        //----------------------------------------------------------------------
        nrcData.put(ATTRIB_VIEW_STATUS, C30OrderItem.FX_PENDING_VIEW_STATUS);
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
        	nrcData.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
        	nrcData.put(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        }
        nrcData.put(ATTRIB_VIEW_CREATED_DT, new Date());
        nrcData.put(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        nrcData.put(ATTRIB_TYPE_ID_NRC, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

        //----------------------------------------------------------------------
        // If the Nrc is service level, set the subscr_no/resets
        // otherwise set the account no
        //----------------------------------------------------------------------
        if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
        	nrcData.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        	nrcData.put(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
        }

        if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_CONNECT_ITEM_ACTION_ID)) {

        	getNRCRate();
        	getNrcTransDescr();

            this.setConnectActionValues(nrcData);
        } else if (this.getAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(ORD_ITEM_DISCONNECT_ITEM_ACTION_ID)) {
            setDisconnectActionValues(nrcData);
        }

        Map callResponse = this.queryData("Nrc", "Create", nrcData, getAccountServerId());
        Map nrc = (HashMap) callResponse.get("Nrc");
        populateNrc(nrc);
        
        lwo = this;
        
        return lwo;

    }

    /**
     * Method which takes a returned HashMap and populates this object with its values.
     * @param nrc the map holding data to populate this object with.
     * @throws C30SDKInvalidAttributeException
     */
    private void populateNrc(final Map nrc) throws C30SDKInvalidAttributeException {

		this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) nrc.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		this.setAttributeValue(ATTRIB_VIEW_ID, ((HashMap) nrc.get("Key")).get(ATTRIB_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_ANNOTATION, nrc.get(ATTRIB_ANNOTATION), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_ANNOTATION2, nrc.get(ATTRIB_ANNOTATION2), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_ARCH_FLAG, nrc.get(ATTRIB_ARCH_FLAG), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_BILL_ORDER_NUMBER, nrc.get(ATTRIB_BILL_ORDER_NUMBER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_BILL_REF_NO, nrc.get(ATTRIB_BILL_REF_NO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_BILL_REF_RESETS, nrc.get(ATTRIB_BILL_REF_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID, nrc.get(ATTRIB_BILLING_ACCOUNT_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CHG_DT, nrc.get(ATTRIB_CHG_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CHG_WHO, nrc.get(ATTRIB_CHG_WHO), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CITY_TAX, nrc.get(ATTRIB_CITY_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CONTRACT_ASSOCIATION_TYPE, nrc.get(ATTRIB_CONTRACT_ASSOCIATION_TYPE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CONTRACT_TRACKING_ID, nrc.get(ATTRIB_CONTRACT_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CONTRACT_TRACKING_ID_SERV, nrc.get(ATTRIB_CONTRACT_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_COUNTY_TAX, nrc.get(ATTRIB_COUNTY_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CREATE_DT, nrc.get(ATTRIB_CREATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CURRENCY_CODE, nrc.get(ATTRIB_CURRENCY_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CURRENT_INSTALLMENT, nrc.get(ATTRIB_CURRENT_INSTALLMENT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_CUSTOMER_ORDER_NUMBER, nrc.get(ATTRIB_CUSTOMER_ORDER_NUMBER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_DATE_NRC_JOURNALABLE, nrc.get(ATTRIB_DATE_NRC_JOURNALABLE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_EFFECTIVE_DATE, nrc.get(ATTRIB_EFFECTIVE_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_ELEMENT_ID, nrc.get(ATTRIB_ELEMENT_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_FEDERAL_TAX, nrc.get(ATTRIB_FEDERAL_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_FIRST_INSTALLMENT_AMOUNT, nrc.get(ATTRIB_FIRST_INSTALLMENT_AMOUNT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT, nrc.get(ATTRIB_INTENDED_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_LAST_REVIEWED_BY, nrc.get(ATTRIB_LAST_REVIEWED_BY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_NO_BILL, nrc.get(ATTRIB_NO_BILL), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_NRC_CATEGORY, nrc.get(ATTRIB_NRC_CATEGORY), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_OPEN_ITEM_ID, nrc.get(ATTRIB_OPEN_ITEM_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_ORDER_NUMBER, nrc.get(ATTRIB_ORDER_NUMBER), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_OTHER_TAX, nrc.get(ATTRIB_OTHER_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID, nrc.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS, nrc.get(ATTRIB_PARENT_SERVICE_INTERNAL_ID_RESETS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_PARENT_TRACKING_ID, nrc.get(ATTRIB_PARENT_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_PARENT_TRACKING_ID_SERV, nrc.get(ATTRIB_PARENT_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_PREV_VIEW_ID, nrc.get(ATTRIB_PREV_VIEW_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_RATE, nrc.get(ATTRIB_RATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_RATE_DT, nrc.get(ATTRIB_RATE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_REQUEST_STATUS, nrc.get(ATTRIB_REQUEST_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_REVIEW_DT, nrc.get(ATTRIB_REVIEW_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_SALES_CHANNEL_ID, nrc.get(ATTRIB_SALES_CHANNEL_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_SALES_ID, nrc.get(ATTRIB_SALES_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_SHIP_FROM_GEOCODE, nrc.get(ATTRIB_SHIP_FROM_GEOCODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_SHIP_TO_GEOCODE, nrc.get(ATTRIB_SHIP_TO_GEOCODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_STATE_TAX, nrc.get(ATTRIB_STATE_TAX), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_SUPERVISOR_NAME, nrc.get(ATTRIB_SUPERVISOR_NAME), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TAX_PKG_INST_ID, nrc.get(ATTRIB_TAX_PKG_INST_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TAX_TYPE_CODE, nrc.get(ATTRIB_TAX_TYPE_CODE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TOTAL_INSTALLMENTS, nrc.get(ATTRIB_TOTAL_INSTALLMENTS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TRACKING_ID, nrc.get(ATTRIB_TRACKING_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TRACKING_ID_SERV, nrc.get(ATTRIB_TRACKING_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TRANS_SIGN, nrc.get(ATTRIB_TRANS_SIGN), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TRANSACT_DATE, nrc.get(ATTRIB_TRANSACT_DATE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_TYPE_ID_NRC, nrc.get(ATTRIB_TYPE_ID_NRC), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_VIEW_CREATED_DT, nrc.get(ATTRIB_VIEW_CREATED_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_VIEW_EFFECTIVE_DT, nrc.get(ATTRIB_VIEW_EFFECTIVE_DT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		this.setAttributeValue(ATTRIB_VIEW_STATUS, nrc.get(ATTRIB_VIEW_STATUS), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

		this.setAttributeValue(ATTRIB_MEMBER_ID, this.getAttributeValue(ATTRIB_TYPE_ID_NRC, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        this.setAttributeValue(ATTRIB_MEMBER_INST_ID2, this.getAttributeValue(ATTRIB_TRACKING_ID_SERV, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, this.getAttributeValue(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		this.setAttributeValue(ATTRIB_CCARD_ID, nrc.get(ATTRIB_CCARD_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    		this.setAttributeValue(ATTRIB_CCARD_ID_SERV, nrc.get(ATTRIB_CCARD_ID_SERV), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            this.setAttributeValue(ATTRIB_MEMBER_INST_ID, new BigInteger(this.getAttributeValue(ATTRIB_TRACKING_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).toString()), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
    		this.setAttributeValue(ATTRIB_IS_VIEWABLE, nrc.get(ATTRIB_IS_VIEWABLE), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
    		this.setAttributeValue(ATTRIB_PROFILE_ID, nrc.get(ATTRIB_PROFILE_ID), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
        
    }

    /**
     * @param newNrc
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     */
    private void setConnectActionValues(final Map newNrc) throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException {

    	//-------------------------------------------------------------------
        // The following values are not overrideable.  They are specially set
        // based on account and order attributes
        //-------------------------------------------------------------------
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0) {
            newNrc.put(ATTRIB_EFFECTIVE_DATE, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
            newNrc.put(ATTRIB_RATE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_CUST_DESIRED_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        }
        if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
            newNrc.put(ATTRIB_EFFECTIVE_DATE, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
            newNrc.put(ATTRIB_RATE_DT, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_PIVOT_DATE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT));
        }

        Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

            if(attributeName.equals(ATTRIB_CURRENCY_CODE) && attributeValue != null) {
                newNrc.put(ATTRIB_CURRENCY_CODE, (Integer) attributeValue);
            } else if(attributeName.equals(ATTRIB_RATE) && attributeValue != null) {
                newNrc.put(ATTRIB_RATE, (BigInteger) attributeValue);
            } else if(attributeName.equals(ATTRIB_TOTAL_INSTALLMENTS) && attributeValue != null) {
                newNrc.put(ATTRIB_TOTAL_INSTALLMENTS, (Integer) attributeValue);
            }
        }

        //---------------------------------------------
        // Set required values that have not been set.
        //---------------------------------------------
        if (newNrc.get(C30Nrc.ATTRIB_RATE) == null) {
            newNrc.put(ATTRIB_RATE, (BigInteger) rateNrc.get(ATTRIB_RATE));
        }
        if (newNrc.get(C30Nrc.ATTRIB_CURRENCY_CODE) == null) {
            newNrc.put(ATTRIB_CURRENCY_CODE, (Integer) ((HashMap) rateNrc.get("Key")).get(ATTRIB_CURRENCY_CODE));
        }
        if (newNrc.get(C30Nrc.ATTRIB_TOTAL_INSTALLMENTS) == null) {
            newNrc.put(ATTRIB_TOTAL_INSTALLMENTS, (Integer) nrcTransDescr.get("MaxInstallments"));
        }

    }

    /**
     * @param newNrc
     */
    private void setDisconnectActionValues(final Map newNrc) {
        this.findNrcs();
        //newCiem.setActiveDate(this.ciem.getActiveDate());
        //newCiem.setPrevViewId(this.ciem.getViewId());
        //newCiem.setServiceExternalId(this.ciem.getServiceExternalId());
        //newCiem.setInactiveDate(super.getCustDesiredDate());
    }

    /**
     * @return
     * @throws C30SDKException 
     */
    private Map getNRCRate() throws C30SDKException {

    	//Create a HashMap to contain the RateNrc filter's info
    	HashMap rateNrcFilter = new HashMap();
        Map key = new HashMap();
        rateNrcFilter.put("Key", key);

    	//Set the Fetch flag at the root level,
    	//so the call will return all fields for found objects
    	rateNrcFilter.put("Fetch", Boolean.TRUE);

        //--------------------------------------
        // Only get rates that are still active
        //--------------------------------------
    	HashMap inactiveDateFilter = new HashMap();
    	inactiveDateFilter.put("IsNull", Boolean.TRUE);
    	rateNrcFilter.put(ATTRIB_INACTIVE_DATE, inactiveDateFilter);

        // Set Account Filter
    	HashMap typeIdNrcFilter = new HashMap();
    	typeIdNrcFilter.put("Equal", this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
    	key.put(ATTRIB_TYPE_ID_NRC, typeIdNrcFilter);

        //--------------------
        // get the rate class
        //--------------------
    	HashMap rateClassFilter = new HashMap();
    	rateClassFilter.put("Equal", getRateNrcRateClass());
    	key.put(ATTRIB_RATE_CLASS, rateClassFilter);

        Map callResponse = this.queryData("RateNrc", "Find", rateNrcFilter, C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);

        //get the count
        int count = ( (Integer) callResponse.get("Count")).intValue();

        if (count == 0) {
            String error = "No active NRC rates match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

            if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {
                error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)
                      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            }
            throw new C30SDKObjectException(error);
        } else if (count > 1) {

            String error = "Multiple active NRC rates match LW Order item: " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            error = error + " for account no: " + this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

            if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL))  {
                error += ", subscr_no: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)
                      + ", subscr_no_resets: " + this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
            }
            throw new C30SDKObjectException(error);
        }

        rateNrc = (HashMap) ((Object[]) callResponse.get("RateNrcList"))[0];

    	return rateNrc;
    }

    /**
     * Method to get the charge type associated with the NRC.
     * @return Map returns the charge type associated with the NRC.
     * @throws C30SDKObjectException
     * @throws C30SDKObjectConnectionException
     * @throws C30SDKInvalidAttributeException
     * @throws C30SDKKenanFxCoreException 
     */
    private Map getNrcTransDescr() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

    	//Create a HashMap to contain the account filter info
    	HashMap nrcTransDescrFilter = new HashMap();

        Map key = new HashMap();
        nrcTransDescrFilter.put("Key", key);

        key.put(C30SDKAttributeConstants.ATTRIB_LANGUAGE_CODE, C30SDKValueConstants.GLOBAL_LANGUAGE_CODE);
        key.put(ATTRIB_TYPE_ID_NRC, this.getAttributeValue(C30OrderItem.ATTRIB_MEMBER_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));

	    Map callResponse = this.queryData("NrcTransDescr", "Get", nrcTransDescrFilter, C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);

	    nrcTransDescr = (HashMap) callResponse.get("NrcTransDescr");

        return nrcTransDescr;
    }

    /**
     * @return Integer returns the Rate NRC's rate class.
     * @throws C30SDKException 
     */
    private Integer getRateNrcRateClass() throws C30SDKException {
        log.debug("Starting C30Nrc.getRateNrcRateClass");
    	Integer rateClass = null;

        //---------------------------------------------------------------------------
        // this is a SERVICE level order item so get the rate class from the service
        //---------------------------------------------------------------------------
        if (this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_IS_SERVICE_LEVEL, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30OrderItem.ORD_ITEM_SERVICE_LEVEL)) {

        	//get the account from the service order
        	C30Service service = (C30Service) this.getAttributeValue(C30ServiceOrder.class, C30ServiceServiceOrder.ATTRIB_SERVICE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        	//if the service is null, which it could be because we are only creating an order item, then get it from middleware.
        	if (service == null) {
        		service = (C30Service) this.getFactory().createC30SDKObject(C30Service.class, new Object[0]);
            	service.setAttributeValue(C30Service.ATTRIB_ITEM_ACTION_ID, C30Service.ORD_ITEM_FIND_ITEM_ACTION_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            	service.setAttributeValue(C30Service.ATTRIB_SERVICE_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
            	service.setAttributeValue(C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, this.getAttributeValue(C30ServiceOrder.class, C30Service.ATTRIB_SERVICE_INTERNAL_ID_RESETS, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
        		service.process();
        	}
        	rateClass = (Integer) service.getAttributeValue(C30Service.ATTRIB_RATE_CLASS, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        //----------------------------------------------------------------------------
        // this is an ACCOUNT level order item so get the rate class from the account
        //----------------------------------------------------------------------------
        } else {

        	//get the account from the service order
        	C30SDKObject account = (C30SDKObject) this.getAttributeValue(C30ServiceOrder.class, C30AccountServiceOrder.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

        	//if the account is null, which it could be because we are only creating an order item, then get it from middleware.
        	if (account == null) {
	        	account = (C30Account) this.getFactory().createC30SDKObject(C30Account.class, new Object[0]);
	        	account.setAttributeValue(ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        	account.setAttributeValue(ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30ServiceOrder.class, C30ServiceOrder.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
	        	account = account.process();
        	}

        	if (account instanceof C30SDKExceptionMessage) {
        		try {
					throw (Throwable) ((C30SDKExceptionMessage) account).getAttributeValue(C30SDKExceptionMessage.ATTRIB_EXCEPTION, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				} catch (Throwable e) {
					log.error(e);
				}
        	}

        	rateClass = (Integer) account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_RATE_CLASS_DEFAULT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
        }
    	return rateClass;
    }

    /**
     * LW NRCs do not currently support child order items.  This is a placeholder
     * for potential future expansion of the LWO framework.
     * @return C30SDKObject the returned child order items.
     */
    protected C30SDKObject createChildOrderItems() { return null; }

    /**
     * Method which gets any data to be cached for further use by the object.
     */
    protected void loadCache() {

    }

    private void findNrcs() {
        /*//-----------------------------------------------
        // create the filter and fetch the entire object
        //-----------------------------------------------

        NrcObjectBaseFilter nrcFilter = new NrcObjectBaseFilter();
        nrcFilter.setFetch(true);

        //-----------------------------
        // Set the Type Id NRC that is specified for the order item
        //-----------------------------
        IntegerFilter[] typeIdNrcFilter = new IntegerFilter[1];
        typeIdNrcFilter[0] = new IntegerEquals(super.getMemberId());
        nrcFilter.setTypeIdNrcFilter(typeIdNrcFilter);

        //-----------------------------
        // Only get the current view
        //-----------------------------
        IntegerFilter[] viewStatusFilter = new IntegerFilter[1];
        viewStatusFilter[0] = new IntegerEquals(new Integer(2));
        nrcFilter.setViewStatusFilter(viewStatusFilter);

        //-------------------------------------------
        // Only get services that are still active - i.e. only unbilled ones
        //-------------------------------------------
        IntegerFilter[] billRefNoFilter = new IntegerFilter[1];
        billRefNoFilter[0] = new IntegerEquals(new Integer(0));
        nrcFilter.setBillRefNoFilter(billRefNoFilter);

        //-------------------------------------------
        // add the service internal id to the filter
        //-------------------------------------------
        IntegerFilter[] serviceInternalIdFilter = new IntegerFilter[1];
        serviceInternalIdFilter[0] = new IntegerEquals(super.getServiceInternalId());
        nrcFilter.setParentServiceInternalIdFilter(serviceInternalIdFilter);

        //------------------------------------------------
        // add the service internal resets to the filter
        //------------------------------------------------
        IntegerFilter[] serviceInternalIdResetsFilter = new IntegerFilter[1];
        serviceInternalIdResetsFilter[0] = new IntegerEquals(super.getServiceInternalIdResets());
        nrcFilter.setParentServiceInternalIdResetsFilter(serviceInternalIdResetsFilter);

        //------------------------------------------------------------
        // Construct Filter elements from order item search attributes
        // that have been set.
        //------------------------------------------------------------
        Iterator iterator = this.dataAttributes.keySet().iterator();
        while (iterator.hasNext()) {
            String attributeName = (String) iterator.next();
            Object attributeSearchValue = ((C30SDKAttribute) super.attributes.get(attributeName)).getSearchValue();

            if (attributeSearchValue != null) {
                if(attributeName.equals(NRC_CURRENCY_CODE)) {
                    IntegerFilter[] currencyCodeFilter = new IntegerFilter[1];
                    currencyCodeFilter[0] = new IntegerEquals((Integer) attributeSearchValue);
                    nrcFilter.setCurrencyCodeFilter(currencyCodeFilter);
                } else if (attributeName.equals(NRC_RATE)) {
                    BigIntegerFilter[] rateFilter = new BigIntegerFilter[1];
                    rateFilter[0] = new BigIntegerEquals((BigInteger) attributeSearchValue);
                    nrcFilter.setRateFilter(rateFilter);
                } else if (attributeName.equals(NRC_TOTAL_INSTALLMENTS)) {
                    IntegerFilter[] totalInstallmentsFilter = new IntegerFilter[1];
                    totalInstallmentsFilter[0] = new IntegerEquals((Integer) attributeSearchValue);
                    nrcFilter.setTotalInstallmentsFilter(totalInstallmentsFilter);
                }
            }
        }

        //-----------------------------------
        // find all matching NRC records
        //-----------------------------------
        NrcObjectBaseDataList nrcObjectDataList = this.nrcBean.find(context, nrcFilter, Boolean.TRUE);
        NrcObjectBaseData[] nrcObjectData = nrcObjectDataList.getArray();

        if (nrcObjectData.length == 0) {
            throw new Exception("No NRCs found for subscr no = " + super.getServiceInternalId() +
                                ", subscr no resets = " + super.getServiceInternalIdResets());
        } else if (nrcObjectData.length > 1 && super.getSearchBehavior().equals(super.ORD_ITEM_SEARCH_BEHAVIOR_ONE)) {
            throw new Exception ("Multiple NRCs found for subscr_no = " + super.getServiceInternalId() +
                                ", subscr no resets = " + super.getServiceInternalIdResets() +
                                ", search behavior = " + super.ORD_ITEM_SEARCH_BEHAVIOR_ONE);
        }
        nrcObjectData[0].getKey();
        this.nrc = nrcBean.get(context, nrcObjectData[0].getKey());*/
    }

}
