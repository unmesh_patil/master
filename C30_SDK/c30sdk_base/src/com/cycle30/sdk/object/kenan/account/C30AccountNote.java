/*
 * C30AccountNote.java
 *
 * Created on February 5, 2009, 9:25 AM
 *
 */

package com.cycle30.sdk.object.kenan.account;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cycle30.sdk.core.attribute.C30SDKAttribute;
import com.cycle30.sdk.core.framework.C30SDKFrameworkFactory;
import com.cycle30.sdk.core.framework.C30SDKObject;
import com.cycle30.sdk.core.message.C30SDKExceptionMessage;
import com.cycle30.sdk.datasource.C30JDBCDataSource;
import com.cycle30.sdk.exception.C30SDKInvalidAttributeException;
import com.cycle30.sdk.exception.C30SDKInvalidConfigurationException;
import com.cycle30.sdk.exception.C30SDKCacheException;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.exception.C30SDKObjectException;
import com.cycle30.sdk.object.C30SDKAttributeConstants;
import com.cycle30.sdk.object.C30SDKValueConstants;
import com.cycle30.sdk.object.kenan.ordering.C30Service;
import com.cycle30.sdk.util.C30CustomParamDef;
import com.cycle30.sdk.util.C30ExternalCallDef;
import com.cycle30.sdk.util.C30StringUtils;
import com.cycle30.sdk.util.C30VersioningUtils;

/**
 * The C30SDK accountNote is a pre-configured template for a Kenan/FX accountNote.
 * @author Tom Ansley, Ranjith Kumar Nelluri
 */
public class C30AccountNote extends C30SDKObject implements Cloneable {

	/** Initialize string values for all attributes that can be set by a client **/
	private static Integer startRecord = 1;
	private static Integer returnSetSize = 50;        

	public Map note;
	private static Logger log = Logger.getLogger(C30AccountNote.class);

	/**
	 * Creates a new instance of C30AccountNote.
	 * @param factory the factory which is the parent of this object.
	 * @param objectType Type of C30SDK deposit
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30AccountNote(final C30SDKFrameworkFactory factory, final String objectType) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, objectType);
	}

	/**
	 * Creates a new instance of C30AccountNote.  This constructor is used for objects that are not derived from a configuration in the
	 * database.  It is not always true that objects must be derived from the database.  This kind of constructor is often used throughout
	 * the framework to create objects.  It is also used by the BRE front end.
	 * @param factory Factory used to create C30AccountNote objects
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during object instantiation.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during object instantiation.
	 * @throws C30SDKObjectException if there was a problem initializing the object during object instantiation.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during object instantiation.
	 * @throws C30SDKCacheException if any exception is thrown during the loading of cache.
	 */
	public C30AccountNote(final C30SDKFrameworkFactory factory) throws C30SDKObjectConnectionException, C30SDKObjectException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKCacheException {
		super(factory, C30AccountNote.class);
	}

	/**
	 * Initializes the base attributes that can be set for any Kenan/FX deposit.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing an attribute.
	 */
	protected void initializeAttributes() throws C30SDKInvalidAttributeException{
		super.setClassFieldConfiguration();
	}

	/**
	 * Creates a copy of the C30AccountNote object.
	 * @return A copy of the C30AccountNote object
	 */
	public Object clone() {
		C30AccountNote copy = (C30AccountNote) super.clone();
		return copy;
	}


	/**
	 * Method to process the deposit.
	 * @return C30SDKObject
	 * @see com.cycle30.sdk.core.framework.C30SDKObject#process()
	 */
	public C30SDKObject process() {
		C30SDKObject lwo = null;

		try {

			setAttributesFromNameValuePairs();

			//-------------------------
			// validate all attributes
			//-------------------------
			validateAttributes();

			if (C30SDKValueConstants.C30_KENAN_TOTAL_CUST_SERVERS.intValue() == 1)
			{
				if (!isAttribute("Find",C30SDKValueConstants.ATTRIB_TYPE_INPUT) || !(Boolean)getAttributeValue("Find", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
				{
					if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null && ((Boolean)getAttributeValue("EquipExternalIdFind", C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue())
						findService();

					if(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) != 0)         
						createAccountNote();
					else
						createK1AccountNote();
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					lwo = this;
				}
			}
			else
			{
				if(!(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) == 0 && 
						isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) &&
						getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null))
				{

					if (super.canServerIdCacheBeSetLocally()){
						setAccountServerIdCache();
					}
					else {   // need to get the account object to set account level data
						lwo = getAccount();
						if(lwo instanceof C30SDKExceptionMessage)
							return lwo;
					}

					setAccountServerId(); 
				}
				if (isAttribute("Find",C30SDKValueConstants.ATTRIB_TYPE_INPUT))
				{
					if((Boolean)getAttributeValue("Find", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
					{
						if(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) != 0)
							findAccountNotes();  // this method uses the CIT Records
						//  findK2AccountNotes();  // this method uses a stored procedure call to get all notes 
						else
							findK1AccountNotes();
					}
					lwo = this;
				}
				if (!isAttribute("Find",C30SDKValueConstants.ATTRIB_TYPE_INPUT) || !(Boolean)getAttributeValue("Find", C30SDKValueConstants.ATTRIB_TYPE_INPUT))
				{
					if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null && ((Boolean)getAttributeValue("EquipExternalIdFind", C30SDKValueConstants.ATTRIB_TYPE_INPUT)).booleanValue())
						findService();

					if(getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_1_0) != 0)         
						createAccountNote();
					else
						createK1AccountNote();
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_IS_PROCESSED, Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					lwo = this;
				}
			}
		} catch (Exception e) {
		    log.error(e);
		    lwo = this.createExceptionMessage(e);
		}

		return lwo;
	}



	/**
	 * Method to get the account associated with this account note.
	 * @return a C30Account object, or a C30SDKExceptionMessage if an error occurred.
	 * @throws C30SDKException 
	 */
	private C30SDKObject getAccount()
	throws C30SDKException
	{
		log.debug("Starting C30AccountNote.getAccount");
		C30SDKObject anAccount = null;

		if ((this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
				&& ((C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT)) instanceof C30Account) {
			anAccount = (C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_OBJECT_PARENT, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, anAccount, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		}

		if (anAccount == null)
			for(int i = 0; i < getC30SDKObjectCount(); i++)
				if(getC30SDKObject(i) instanceof C30Account)
					anAccount = getC30SDKObject(i);

		if(anAccount == null)
		{
			anAccount = getFactory().createC30SDKObject(com.cycle30.sdk.object.kenan.account.C30Account.class, new Object[0]);
			//anAccount.setUser(this.getUser(), this.getUserContextofLWObject());
			if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)
				anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)           
				anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			if (getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT)!= null)           
				anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);

			anAccount.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ITEM_ACTION_ID, C30SDKValueConstants.ACCOUNT_ITEM_ACTION_ID_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			anAccount = anAccount.process();
		}
		if(!(anAccount instanceof C30SDKExceptionMessage)){
			setAccountLevelAttributes(anAccount);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, anAccount, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
		}
		log.debug("Finished C30AccountNote.getAccount");
		return anAccount;
	}


	// added jcw: Lwo Enhancements
	private boolean setAccountLevelAttributes(C30SDKObject account)
	throws C30SDKInvalidAttributeException, C30SDKObjectException
	{
		boolean accountLevelSet = false;
		if(account instanceof C30Account)
			if (account != null)
			{
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_INPUT) == null)
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)) 
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))     
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				if (isAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))       
					setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, account.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				accountLevelSet = true;    
			}
		return accountLevelSet;
	}



	/**
	 * Method to get the service associated with this account note.  This method is purely to ensure
	 * data integrity before the note is attempted to be created.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during the service retrieval.
	 * @throws C30SDKObjectException if there was a problem initializing the object during the service retrieval.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during the service retrieval.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void findService() throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

		//Create a HashMap to contain the CIEM filter's info
		HashMap ciemFilter = new HashMap();

		//Set the Fetch flag at the root level,
		//so the call will return all fields for found objects
		ciemFilter.put("Fetch", new Boolean(true));

		//-----------------------------
		// Only get the current view
		//-----------------------------
		HashMap attrFilter = new HashMap();
		attrFilter.put("Equal", new Integer(2));
		ciemFilter.put(C30Service.ATTRIB_VIEW_STATUS, attrFilter);

		//-------------------------------------------
		// Only get services that are still active
		//-------------------------------------------
		attrFilter = new HashMap();
		attrFilter.put("IsNull", Boolean.TRUE);
		ciemFilter.put(C30Service.ATTRIB_INACTIVE_DATE, attrFilter);

		//-----------------------------
		// Set the External ID that is specified
		//-----------------------------
		attrFilter = new HashMap();
		attrFilter.put("Equal", this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		ciemFilter.put(C30Service.ATTRIB_SERVICE_EXTERNAL_ID, attrFilter);

		//-------------------------------------------
		// add is_current flag to the filter.
		//-------------------------------------------
		attrFilter = new HashMap();
		attrFilter.put("Equal", Boolean.TRUE);
		ciemFilter.put(C30Service.ATTRIB_IS_CURRENT, attrFilter);

		//Map callResponse = this.queryData("Service", "ExternalFind", ciemFilter, CATALOG_SERVER_ID);
		Map callResponse = this.queryData("Service", "ExternalFind", ciemFilter, C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);

		//get the count to determine whether a service exists.
		int count = ( (Integer) callResponse.get("Count")).intValue();

		if (count == 0) {
			String error = "No active service found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			throw new C30SDKObjectException(exceptionResourceBundle.getString("CONN-017"),"CONN-017");
		}

		HashMap service = (HashMap) ((Object[]) callResponse.get("ServiceList"))[0];

		//if the account external ID does not equal the services account external ID then throw an exception
		if (!service.get(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID).equals( ((C30SDKObject) this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT)).getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID) )) {
			throw new C30SDKObjectException(exceptionResourceBundle.getString("CONN-018"),"CONN-018");
		}

	}

	/**
	 * Creates the Kenan/FX 1.0 account note using API-TS calls.
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void createK1AccountNote()
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException
	{
		Map accountNote = setAccountNoteAttributes();
		Map accountNoteRequest = new HashMap();
		accountNoteRequest.put("Note", accountNote);
		Map callResponse = null;
		callResponse = queryData("NoteCreate", accountNoteRequest, getAccountServerId());
		accountNote = (HashMap)callResponse.get("Note");
		populateAccountNoteOutput(accountNote);
	}



	/**
	 * Creates the Kenan/FX Enhanced Note using API-TS calls to first use existing CIT Record or create one
	 * for the account if a CIT record not found.
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void createAccountNote()
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException
	{
		//---------------------------------
		// set and validate all attributes
		//---------------------------------
		Map accountNote = setAccountNoteAttributes();
		boolean citIdFound = false;
		Map citFindResponse = findCitRecord();
		if (citFindResponse != null){
			// need to populate the request with cit_id as parent_id1
			int count = ((Integer)citFindResponse.get("Count")).intValue();
			if(count > 0){
				HashMap aCitEntry = (HashMap)((Object[])(Object[])citFindResponse.get("CitInteractionList"))[0];  
				HashMap citKey = (HashMap)aCitEntry.get("Key");
				log.debug(" cit Id: "+citKey.get("CitId"));

				BigInteger citId =   new BigInteger(citKey.get("CitId").toString());
				accountNote.put("ParentId1", citId); 
				citIdFound = true;
			} 
		}
		if (!citIdFound){
			BigInteger citId = this.createCitRecord();
			accountNote.put("ParentId1", citId); 
		}

		accountNote.put("ParentCode",C30SDKValueConstants.NOTE_PARENT_CODE);
		accountNote.put("NoteSource",C30SDKValueConstants.NOTE_SOURCE);
		accountNote.put("NoteTemplateId",C30SDKValueConstants.NOTE_TEMPLATE_ID);

		Map accountNoteRequest = new HashMap();
		accountNoteRequest.put("EnhancedNote", accountNote);
		Map callResponse = null;
		//-----------------------------------------------
		// create the accountNote and then get the response.
		//-----------------------------------------------
		callResponse = queryData("EnhancedNoteCreate", accountNoteRequest, getAccountServerId());
		accountNote = (HashMap)callResponse.get("EnhancedNote");
		populateAccountNoteOutput(accountNote);
	}


	/**
	 * Finds the Kenan/FX CIT record using API-TS calls 
	 * @throws C30SDKInvalidConfigurationException if the object has an invalid configuration during processing
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private Map findCitRecord()
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException
	{

		Map callResponse = null;
		HashMap citFilter = new HashMap();
		Map attrFilter = new HashMap();
		Map key = new HashMap();
		citFilter.put("Key", key);
		citFilter.put("Fetch", Boolean.TRUE);

		attrFilter = new HashMap();
		String accountInternalId = getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).toString();
		attrFilter.put("Equal", new BigInteger(accountInternalId));
		citFilter.put("ParentId1", attrFilter);

		attrFilter = new HashMap();
		attrFilter.put("Equal", getAttributeValue( "ParentCode", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		citFilter.put("ParentCode", attrFilter);

		Map citRequest = new HashMap();

		citRequest.put("CitInteraction", citFilter);

		callResponse = queryData("CitInteractionFind", citRequest, getAccountServerId());

		return callResponse;
	}

	/**
	 *   Creates the CIT record using the Kenan API-TS calls.
	 * @return
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKInvalidConfigurationException
	 * @throws C30SDKKenanFxCoreException 
	 */
	private BigInteger createCitRecord()
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException

	{

		BigInteger citId = null;
		Map citRecord = new HashMap();
		citRecord.put("ParentCode",C30SDKValueConstants.PARENT_CODE);
		String accountInternalId = getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT).toString();
		BigInteger parentId1 = new BigInteger(accountInternalId);
		citRecord.put("ParentId1", parentId1);
		citRecord.put("CitImportanceId",C30SDKValueConstants.CIT_IMPORTANCE_ID_MEDIUM);
		citRecord.put("CitChannelId",C30SDKValueConstants.CIT_CHANNEL_ID_DEFAULT);
		citRecord.put("IsPermanent",new Boolean(true));
		citRecord.put("CitReasonId",C30SDKValueConstants.CIT_REASON_ID_DEFAULT);

		Map citRecordRequest = new HashMap();
		citRecordRequest.put("CitInteraction", citRecord);
		Map callResponse = null;
		callResponse = queryData("CitInteractionCreate", citRecordRequest, getAccountServerId());

		HashMap citResponse = (HashMap)callResponse.get("CitInteraction");
		HashMap citKey = (HashMap)citResponse.get("Key");
		log.debug(" cit Id: "+citKey.get("CitId"));
		citId = (BigInteger)citKey.get("CitId");
		return citId;

	}

	/**
	 *  Finds the the Enhanced Notes using the CIT records for the account using the Kenan API-TS calls.
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKInvalidConfigurationException
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void findAccountNotes()
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException
	{
		int lastRecordReturned = 0; 
		int recordsProcessed = 0;
		int totalRecordsFound = 0;
		// set values for record retrieval
		if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
				getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
				getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		HashMap citFilter = new HashMap();
		Map attrFilter = new HashMap();
		Map key = new HashMap();
		citFilter.put("Key", key);
		citFilter.put("Fetch", Boolean.TRUE);

		attrFilter = new HashMap(); 
		attrFilter.put("Equal", new BigInteger(getAttributeValue( C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).toString()));
		citFilter.put("ParentId1", attrFilter);

		attrFilter = new HashMap();
		attrFilter.put("Equal", getAttributeValue( "ParentCode", C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		citFilter.put("ParentCode", attrFilter);

		Map citRequest = new HashMap();

		citRequest.put("CitInteraction", citFilter);
		Map callResponse = null;
		callResponse = queryData("CitInteractionFind", citRequest, getAccountServerId());
		int count = ((Integer)callResponse.get("Count")).intValue();
		if(count == 0)
		{
			setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			return;
		}
		log.debug("count of cit entries: "+count);
		C30AccountNote cloneLwo = (C30AccountNote)super.clone();
		// look at ALL notes, meaning all cit interactions for this account
		for (int i=0; i < count; i++)
		{

			if (recordsProcessed >= returnSetSize )
				break;

			HashMap aCitEntry = (HashMap)((Object[])(Object[])callResponse.get("CitInteractionList"))[i];  
			HashMap citKey = (HashMap)aCitEntry.get("Key");
			log.debug(" cit Id: "+citKey.get("CitId")+ " at element: "+i);



			HashMap noteFilter = new HashMap();
			Map noteAttrFilter = new HashMap();
			Map noteKey = new HashMap();
			noteFilter.put("Key", noteKey);
			noteFilter.put("Fetch", Boolean.TRUE);

			noteAttrFilter = new HashMap();
			noteAttrFilter.put("Equal", new BigInteger(citKey.get("CitId").toString()));
			noteFilter.put("ParentId1", noteAttrFilter);

			Map noteRequest = new HashMap();

			noteRequest.put("EnhancedNote", noteFilter);
			Map callNoteResponse = null;
			callNoteResponse = queryData("EnhancedNoteFind", noteRequest, getAccountServerId());
			int noteCount = ((Integer)callNoteResponse.get("Count")).intValue();
			if (noteCount == 0) 
				continue;
			totalRecordsFound = totalRecordsFound+noteCount; 
			if (startRecord > totalRecordsFound)
				continue;

			int beginningIndx = 0;
			HashMap accountNote;
			if (recordsProcessed == 0){
				beginningIndx = noteCount - (totalRecordsFound - startRecord) - 1;   
				accountNote = (HashMap)((Object[])(Object[])callNoteResponse.get("EnhancedNoteList"))[beginningIndx];  
				this.populateAccountNoteOutput(accountNote);
				if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
					setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, startRecord, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				lastRecordReturned = startRecord;
				recordsProcessed++;
				beginningIndx++;
			}

			log.debug("count of note entries: "+noteCount);

			for (int j=beginningIndx; j < noteCount; j++)
			{
				if (recordsProcessed >= returnSetSize )
					break;

				accountNote = (HashMap)((Object[])(Object[])callNoteResponse.get("EnhancedNoteList"))[j];  

				if (recordsProcessed == 0){
					this.note = accountNote;
					this.populateAccountNoteOutput(accountNote);
					if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, lastRecordReturned+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);               

					this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				}
				else
				{
					C30AccountNote anlwo = (C30AccountNote)cloneLwo.clone();
					anlwo.note = accountNote;
					// set values to pass back in result
					anlwo.populateAccountNoteOutput(accountNote);
					if  (anlwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						anlwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, lastRecordReturned+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					// add to our list of peer objects
					super.addC30SDKObject(anlwo);

				}  
				recordsProcessed++;
				lastRecordReturned++;        


			}// end inner for loop j  

			super.setProcessingAggregateTotalsAttributes(this, totalRecordsFound, lastRecordReturned, recordsProcessed ); 

		}

	}

	/**
	 * the method uses a stored procedure call to gather all notes associated to the account.  Some notes are stored associated to 
	 * CIT records and other are stored directly in the notes table.  This makes it difficult to tell how many notes in all there are 
	 * via the API-TS calls.
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKInvalidConfigurationException
	 */
	private void findK2AccountNotes()
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException
	{

		int lastRecordReturned = 0;
		int beginningIndx = 0;
		// set values for record retrieval
		if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
				getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
				getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		C30JDBCDataSource dataSource = this.getFactory().getJDBCDataSource(getAccountServerId());   
		C30ExternalCallDef call = new C30ExternalCallDef(C30SDKValueConstants.GET_NOTE_OBJECTS_SP_CALL);   

		try {

			call.addParam(new C30CustomParamDef("ACCOUNT_NO", 1, 10, 1));
			ArrayList paramValues = new ArrayList();
			paramValues.add(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));  
			call.addParam(new C30CustomParamDef("NOTE_ID", 9, 18, 2));
			call.addParam(new C30CustomParamDef("PARENT_CODE", 1, 3, 2));
			call.addParam(new C30CustomParamDef("PARENT_ID1", 9, 18, 2));
			call.addParam(new C30CustomParamDef("PARENT_ID2", 1, 6, 2));
			call.addParam(new C30CustomParamDef("LEGACY_NOTE_CODE", 2, 4, 2));
			call.addParam(new C30CustomParamDef("NOTE_SOURCE", 1, 3, 2));
			call.addParam(new C30CustomParamDef("IS_PERMANENT", 1, 1, 2));
			call.addParam(new C30CustomParamDef("CHG_WHO", 2, 32, 2));
			call.addParam(new C30CustomParamDef("CHG_DT", 3, 2));
			call.addParam(new C30CustomParamDef("NOTE_TEXT", 2, 3999, 2));
			call.addParam(new C30CustomParamDef("NOTE_TEMPLATE_ID", 1, 6, 2));
			call.addParam(new C30CustomParamDef("NOTE_TYPE_ID", 1, 6, 2));
			call.addParam(new C30CustomParamDef("NOTE_REASON_ID", 2, 6, 2));

			dataSource.queryData(call, paramValues, 2);

			int count = dataSource.getRowCount();
			if(count == 0)
			{
				setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				return;
			}
			log.debug("Total row count = " + count);
			int recordsProcessed = 0;
			if (startRecord > 1)
				beginningIndx = startRecord - 1;
			Map resultSet  = dataSource.getResultMap(); 

			C30AccountNote cloneLwo = (C30AccountNote)super.clone();
			for (int i=beginningIndx; i < count; i++)
			{
				if (recordsProcessed >= returnSetSize )
					break;                      
				Map accountNote = (Hashtable)resultSet.get(i);

				if (recordsProcessed == 0){
					this.note = accountNote;
					this.populateK2Note(accountNote);
					this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);                         
				}
				else
				{
					C30AccountNote lwo = (C30AccountNote)cloneLwo.clone();                  
					lwo.populateK2Note(accountNote);
					// add to our list of peer objects
					lwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					if  (lwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						lwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
					super.addC30SDKObject(lwo);               
				}          
				recordsProcessed++;
				lastRecordReturned =  i + 1;                  
			}// end for loop i

			super.setProcessingAggregateTotalsAttributes(this, count, lastRecordReturned, recordsProcessed );                

		}
		catch(Exception e)
		{

			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-019"),e,"CONN-019");
		}finally {
			try 
			{
				dataSource.closeConnection();
			} catch (Exception ex){
				log.error("Exception: "+ex.getMessage());
			}
		}
	}

	private void populateK2Note( Map accountNote)
	throws C30SDKInvalidAttributeException
	{

		for  ( Iterator it=accountNote.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
			Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
			String key = (String)entry.getKey (  ) ; 
			Object value = accountNote.get(key) ;
			key = C30StringUtils.SqlToKenanName(key);

			if (value != null)
			{
				if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
				{
					setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

					log.debug(  ( String ) key+" " +  value +"   SET") ;                            
				}
				else
					if (value != null){
						log.debug(  ( String ) key+" " +  value ) ; 
					}

			}
		}
	}


	/**
	 *  Finds the Account Notes using the Kenan FX 1.0 API-TS calls.
	 * @throws C30SDKObjectException
	 * @throws C30SDKObjectConnectionException
	 * @throws C30SDKInvalidAttributeException
	 * @throws C30SDKInvalidConfigurationException
	 * @throws C30SDKKenanFxCoreException 
	 */
	private void findK1AccountNotes()
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException
	{
		int lastRecordReturned = 0; 
		// set values for record retrieval
		if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
				getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			startRecord = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_START_RECORD, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && 
				getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null )
			returnSetSize = (Integer)getAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RETURN_SIZE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);

		HashMap noteFilter = new HashMap();
		noteFilter.put("Fetch", Boolean.TRUE);
		if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
		{                    
			HashMap accountInternalIdFilter = new HashMap();
			accountInternalIdFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
			noteFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalIdFilter);         
		} else
			if(getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null || getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
			{
				HashMap accountExternalIdFilter = new HashMap();
				accountExternalIdFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				noteFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, accountExternalIdFilter);
				HashMap accountExternalIdTypeFilter = new HashMap();
				accountExternalIdTypeFilter.put("Equal", getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
				noteFilter.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, accountExternalIdTypeFilter);
			}


		Map noteRequest = new HashMap();    
		noteRequest.put("Note", noteFilter);
		Map callNoteResponse = null;
		callNoteResponse = queryData("NoteFind", noteRequest, getAccountServerId());
		int count = ((Integer)callNoteResponse.get("Count")).intValue();
		log.debug("count of note entries: "+count);
		if(count == 0)
		{
			setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			return;
		}
		HashMap accountNote = (HashMap)((Object[])(Object[])callNoteResponse.get("NoteList"))[0];  
		int recordsProcessed = 0;
		if (startRecord == 1){
			this.populateAccountNoteOutput(accountNote);
			if  (isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
				setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, 1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			lastRecordReturned = 1;
			recordsProcessed++;
		}

		C30AccountNote cloneLwo = (C30AccountNote)super.clone(); 
		if (startRecord > 1)
			startRecord--;
		for (int i=startRecord; i < count; i++)
		{
			if (recordsProcessed >= returnSetSize )
				break;
			accountNote = (HashMap)((Object[])(Object[])callNoteResponse.get("NoteList"))[i];  

			log.debug(accountNote.toString());   
			// is this the first account we are processing?
			if (recordsProcessed == 0){
				this.note = accountNote;
				this.populateAccountNoteOutput(accountNote);
				this.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				if  (this.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
					this.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);               
			}
			else
			{
				C30AccountNote anlwo = (C30AccountNote)cloneLwo.clone();
				anlwo.note = accountNote;
				anlwo.populateAccountNoteOutput(accountNote);
				if  (anlwo.isAttribute(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER,C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
					anlwo.setAttributeValue(C30SDKValueConstants.ATTRIB_SEARCH_RECORD_NUMBER, i+1, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				anlwo.setAttributeValue("IsProcessed", Boolean.TRUE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
				super.addC30SDKObject(anlwo);                    
			}  

			recordsProcessed++;
			lastRecordReturned =  i + 1;        

		}// end inner for loop j  

		super.setProcessingAggregateTotalsAttributes(this, count, lastRecordReturned, recordsProcessed ); 
	}


	/**
	 * Method to ensure all attributes that are visible to the calling application are populated.  This
	 * method is usually called just before the object returns objects using the process() method.
	 * @param accountNote the map containing the returned note information.
	 * @throws C30SDKInvalidConfigurationException if there was an invalid configuration during the note population.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during the note population.
	 * @throws C30SDKObjectException if there was a problem initializing the object during the note population.
	 * @throws C30SDKInvalidAttributeException if there was a problem initializing attributes during the note population.
	 */    
	private void populateAccountNoteOutput(Map accountNote)
	throws C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidAttributeException, C30SDKInvalidConfigurationException
	{

		if (accountNote != null)
		{
			// jcw added: LWO Enhancements
			for  ( Iterator it=accountNote.entrySet (  ) .iterator (  ) ; it.hasNext (  ) ;  )   {  
				Map.Entry entry =  ( Map.Entry ) it.next (  ) ; 
				String key = (String)entry.getKey (  ) ; 

				Object value = entry.getValue (  ) ;
				if (value != null)
					if (isAttribute(key, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT))
						setAttributeValue(key, value, C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
				System.out.println("Key: :"+key);
				if (value != null)
					log.debug(  ( String ) key+" " +  value ) ; 
				// } 
			}
		}
		else
		{
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_DATE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_DATE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_WHO, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_WHO, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_COMMENTS, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_COMMENTS, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_CODE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_TEXT, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_TEXT, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_PERMANENT_FLAG, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_PERMANENT_FLAG, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);

			if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
				this.setAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_SOURCE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_SOURCE, C30SDKValueConstants.ATTRIB_TYPE_INPUT), C30SDKValueConstants.ATTRIB_TYPE_OUTPUT);
			}

		}
	}


	/**
	 * Validates that all the required attributes have been set.
	 * @throws C30SDKInvalidAttributeException if there was an issue with the attributes of the object during processing.
	 * @throws C30SDKKenanFxCoreException 
	 * @throws C30SDKObjectConnectionException
	 */
	protected void validateAttributes() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {

		super.validateAttributes();
		// jcw added: LWO Enhancement
		if (!isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) ||
				(isAttribute(C30SDKAttributeConstants.ATTRIB_FIND,C30SDKValueConstants.ATTRIB_TYPE_INPUT) && !(Boolean)getAttributeValue(C30SDKAttributeConstants.ATTRIB_FIND, C30SDKValueConstants.ATTRIB_TYPE_INPUT)))
			// end jcw added.      

			//check the note CODE is valid
			if (!isNoteCodeValid()) {

				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("CONN-020"),"CONN-020");
			}

		//check the note SOURCE is valid
		if (this.getFactory().getAPIVersion().compareTo(C30VersioningUtils.API_VERSION_2_0) == 0) {
			if (!this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_SOURCE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.NOTE_SOURCE_SYSTEM_GENERATED)
					&& !this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_SOURCE, C30SDKValueConstants.ATTRIB_TYPE_INPUT).equals(C30SDKValueConstants.NOTE_SOURCE_MANUAL)) {
				throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("CONN-021"),"CONN-021");
			}
		}

		//check to ensure either the account internal id is set or the external id and type.
		if (this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null
				&& this.getAttribute(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT).getValue() == null) {
			throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("CONN-022"),"CONN-022");
		}

	}

	/**
	 * Private method to determine whether the note code supplied is valid.
	 * @return true = valid, false = invalid.
	 * @throws C30SDKInvalidAttributeException if there was a problem retrieving note attributes during note code validation.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private boolean isNoteCodeValid() throws C30SDKInvalidAttributeException, C30SDKKenanFxCoreException {
		boolean isValid = true;

		Map noteCode = new HashMap();

		Map key = new HashMap();
		key.put(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_LANGUAGE_CODE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_LANGUAGE_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		key.put(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_API_CODE, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_CODE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		noteCode.put("Key", key);

		//we have to throw an exception here because instead of the
		//API-TS call passing back an empty set it throws an exception.
		try {
			//this.queryData("NoteCode", "Get", noteCode, ADMIN_SERVER_ID);
			this.queryData("NoteCode", "Get", noteCode, C30SDKValueConstants.C30_KENAN_ADMIN_SERVER_ID);
		} catch (C30SDKObjectConnectionException e) {
			isValid = false;
		}

		return isValid;
	}

	/**
	 * Initilize attributes for the Kenan FX accountNote that is about to be created through the API TS.
	 * @return Map the map that will be used to create the account note using API-TS.
	 * @throws C30SDKInvalidAttributeException exception thrown if there was an error setting any attributes.
	 * @throws C30SDKObjectConnectionException if there was a problem connecting to middleware during processing
	 * @throws C30SDKObjectException if there was a problem initializing the object during processing
	 * @throws C30SDKInvalidConfigurationException if there was a configuration issue while setting the note attributes.
	 * @throws C30SDKKenanFxCoreException 
	 */
	private Map setAccountNoteAttributes() throws C30SDKInvalidAttributeException, C30SDKObjectException, C30SDKObjectConnectionException, C30SDKInvalidConfigurationException, C30SDKKenanFxCoreException {
		Map accountNote = new HashMap();
		//-----------------------------------------------
		// set the accountNote attributes
		//-----------------------------------------------
		Iterator iterator = this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).keySet().iterator();
		while (iterator.hasNext()) {
			String attributeName = (String) iterator.next();
			Object attributeValue = ((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(attributeName)).getValue();

			if (attributeValue != null) {
				if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_DATE)) {
					accountNote.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_WHO)) {
					accountNote.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_COMMENTS)) {
					accountNote.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_EQUIP_EXTERNAL_ID)) {
					accountNote.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_TEXT)) {
					accountNote.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_PERMANENT_FLAG)) {
					accountNote.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_CODE)) {
					accountNote.put(attributeName, attributeValue);
				} else if (attributeName.equals(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_NOTE_SOURCE)) {
					accountNote.put(attributeName, attributeValue);
				}
			}
		}

		String accountExternalId="";
		Integer accountExternalIdType=new Integer(4);
		Integer accountInternalId=new Integer(0);
		//set all account attributes
		//If only account ExternalId and ExternalIdType is given for K1, then get the accountInternalId.

		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
		{
			accountExternalId=(String)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			accountNote.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		}

		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)    
		{
			accountExternalIdType=(Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			accountNote.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE,  this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT));
		}
		if (this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) != null)
		{
			accountInternalId=(Integer)this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
			accountNote.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalId);
		}
		else
		{
			if(!accountExternalId.equalsIgnoreCase("")&& ((Double)this.getFactory().getAPIVersion()).toString().equalsIgnoreCase(C30VersioningUtils.API_VER_1_0))
			{
				HashMap accountLocateFilter = new HashMap();

				//Set the Fetch flag at the root level,
				//so the call will return all fields for found objects
				accountLocateFilter.put("Fetch", Boolean.TRUE);
				HashMap externalIdTypeFilter = new HashMap();
				HashMap externalIdFilter = new HashMap();
				externalIdTypeFilter.put("Equal",new Integer(accountExternalIdType));
				externalIdFilter.put("Equal",accountExternalId);
				accountLocateFilter.put("AccountExternalIdType", externalIdTypeFilter);
				accountLocateFilter.put("AccountExternalId", externalIdFilter);

				HashMap inactiveDateFilter = new HashMap();
				Map callResponse;
				callResponse = this.queryData("AccountLocate", "Find", accountLocateFilter, C30SDKValueConstants.C30_KENAN_CATALOG_SERVER_ID);

				//get the count to ensure we found an account.
				int count = ( (Integer) callResponse.get("Count")).intValue();
				// don't go any further if searching for all and none are returned.
				if(count == 0)
				{
					setAttributeValue("IsProcessed", Boolean.FALSE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					String error = "No account found with external id = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID, C30SDKValueConstants.ATTRIB_TYPE_INPUT) 
					+ ", external id type = " + this.getAttributeValue(C30SDKAttributeConstants.ATTRIB_ACCOUNT_EXTERNAL_ID_TYPE, C30SDKValueConstants.ATTRIB_TYPE_INPUT);
					throw new C30SDKInvalidAttributeException(exceptionResourceBundle.getString("INVALID-ATTRIB-013"),"INVALID-ATTRIB-013");

				}
				for(int i=0;i<count;i++)
				{
					accountInternalId=(Integer) ((HashMap) ((HashMap) ((Object[]) callResponse.get("AccountLocateList"))[i]).get("Key")).get("AccountInternalId");
				}
				accountNote.put(C30SDKAttributeConstants.ATTRIB_ACCOUNT_INTERNAL_ID, accountInternalId);
			}
		}


		//set the date of the transaction if not set manually
		if (((C30SDKAttribute) this.getAttributes(C30SDKValueConstants.ATTRIB_TYPE_INPUT).get(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_DATE)).getValue() == null) {
			accountNote.put(C30SDKAttributeConstants.ATTRIB_ACCT_NOTE_CHG_DATE, new Date());
		}

		return accountNote;
	}

	/**
	 * Method which gets any data to be cached for further use by the object.
	 */
	protected void loadCache() {

	}
}
