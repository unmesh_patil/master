package com.cycle30.sdk.cache;


/**
 * Implementation of the CachePolicy
 *
 *@author     rnelluri
 */
public class C30SDKStaleCachePolicyImpl implements C30SDKCachePolicy {

    /**
     * Purges the cache.
     *
     *@param  cache  The cache to be purged.
     */
    public void purge(C30SDKCache cache) {
        cache.purgeStale();
    }
}

