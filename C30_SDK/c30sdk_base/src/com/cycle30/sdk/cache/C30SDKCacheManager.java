package com.cycle30.sdk.cache;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cycle30.sdk.config.C30SDKPropertyFileManager;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Provides management of multiple caches and performs periodic sweeps
 * through the caches to purge stale entries.
 *
 *@author     rnelluri
 */
public class C30SDKCacheManager implements Runnable {
    
    private static final String DO_SWEEP = "c30sdkcache-manager.dosweep";
    private static final String SLEEP_TIME = "c30sdkcache-manager.sleep-time";
    
    private static Logger log = Logger.getLogger(C30SDKCacheManager.class);
    
    // The one-and-only instance of the CacheManager.
    private static C30SDKCacheManager instance = null;

    /**
     * The set of caches supported in the manager.
     */
    protected Set<C30SDKCacheEntry> caches = null;
    /**
     * The number of milliseconds to sleep between cache sweeps
     */
    protected long sleepTime = 100000; // Modified for shorter sleep time.
    /**
     * Flag indicating that the run-thread for the manager should stop. True
     * indicates that the run thread should die
     */
    protected boolean die = false;
     


    /**
     * Returns the one-and-only instance of the CacheManager.
     *
     *@return    The one-and-only instance of the CacheManager.
     */
    public static C30SDKCacheManager getInstance() {
        if (instance == null) {
            if( log != null ) { log.debug("Creating new instance of CacheManager"); }
            instance = new C30SDKCacheManager();
        }

        return instance;
    }


    /**
     * Creates a new cache to be managed.
     *
     *@return       Reference to the newly created cache.
     */
    public <K, V extends C30SDKCacheable<K>> C30SDKCache<K, V> createCache() {
        return createCache(null);
    }
    
    /**
     * Creates a new cache to be managed.
     *
     * @param  properties The parameters to be passed to the cache. 
     *@return       Reference to the newly created cache.
     */
    public <K, V extends C30SDKCacheable<K>> C30SDKCache<K, V> createCache(Properties properties) {
        if( log != null ) { log.debug("Creating new cache"); }
        C30SDKCache<K, V> cache = new C30SDKCache<K, V>(properties);
        caches.add(new C30SDKCacheEntry(cache, new C30SDKStaleCachePolicyImpl()));
        
        return cache;
    }
    
    /**
     * Stops the cache sweep thread. The thread will stop after the last
     * thread sweep.
     */
    public synchronized void stopCacheSweep() {
        die = true;
    }


    /**
     * Constructs a new Cache manager.
     */
    protected C30SDKCacheManager() {
        boolean startSweep = true;
        try {
        	C30SDKPropertyFileManager rb = C30SDKPropertyFileManager.getInstance();
        	rb.addFromClassPath(C30SDKValueConstants.CACHE_PROPERTIES_FILE_NAME);
            startSweep = rb.getBoolean(DO_SWEEP);
            sleepTime = rb.getLong(SLEEP_TIME);
        } catch (java.util.MissingResourceException ex) {
            if( log != null ) { log.debug("CacheManagerSettings were not found; using defaults"); }
        }
        catch(Exception e)
        {
           log.error(e);
        }
        caches = new HashSet<C30SDKCacheEntry>();
        if (startSweep) {
            Thread t = new Thread(this);
            t.setPriority(Thread.MIN_PRIORITY);
            t.start();
        }
    }


    /**
     * Main thread which sweeps through the registered caches and 
     * purgest them based on the policy set up for the cache.
     */
    public void run() {
        try {
            while (!die) {
                Thread.sleep(sleepTime);
                if( log != null ) { log.debug("Starting purge sweep through caches"); }
                Iterator it = caches.iterator();
                while (it.hasNext()) {
                	C30SDKCacheEntry entry = (C30SDKCacheEntry) it.next();
                    entry.purge();
                }
                if( log != null ) { log.debug("Completed sweep"); }
            }
        } catch (Exception ex) {
            log.error(ex.getStackTrace());
        }
    }


  

    /**
     * Manages a cache entry with it's policy.
     *
     *@author     rnelluri
     */
    class C30SDKCacheEntry {
        /**
         * The cache being managed.
         */
        protected C30SDKCache cache = null;
        /**
         * The policy associated with the cache.
         */
        protected C30SDKCachePolicy policy = null;


        /**
         * Constructs a new CacheEntry
         *
         *@param  cache   The Cache to be managed.
         *@param  policy  The policy to be applied to the cache at purge time.
         */
        public C30SDKCacheEntry(C30SDKCache cache, C30SDKCachePolicy policy) {
            this.cache = cache;
            this.policy = policy;
        }


        /**
         * Purges the cache based on the policy set up for the cache.
         */
        public void purge() {
            if( log != null ) { log.debug("Purging cache"); }
            if (policy != null) {
                policy.purge(cache);
            } else {
                cache.getCache().clear();
            }
        }

    }
}

