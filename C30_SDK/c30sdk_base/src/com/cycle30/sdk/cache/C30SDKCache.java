package com.cycle30.sdk.cache;

import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;



/**
 * Implementation of the caching that supports purging and statling in the framework and 
 * @author rnelluri
 *
 */
public class C30SDKCache<K, V extends C30SDKCacheable<K>> {
    
	private static Logger log = Logger.getLogger(C30SDKCache.class);
    public static final String CACHE_CAPACITY = "c30sdkcache.capacity";
    public static final String CACHE_LRU_PURGE_NUMBER = "c30sdkcache.lru-purge-number";

    
    /**
     * A map of the cached objects. The key value is the key of the cacheable
     * object and the value is a cacheable object.
     */
    protected C30SDKCacheMap<K, V> cache = null;
    // Object used for synchronization locking (mutex object)
    private Object lock = null;
    /**
     * Number of least-recently-used objects to purge from the cache
     */
    protected int lruPurgeNumber = 5;
    /**
     * The maximum size of the cache. The maximum number of cached objects
     */
    protected int capacity = 100;
    /**
     * The load factor for hashing on the map
     */
    protected float loadFactor = 1;


    /**
     * Constructs a new cache with the default parameters.
     */
    public C30SDKCache() {
        this(null);
    }
    
    /**
     * Constructs a new cache with the specified parameters
     *
     * @param  properties    Parameters for the cache settings
     */
    public C30SDKCache(Properties properties) {
        if (properties != null) {
            capacity = Integer.parseInt(properties.getProperty(CACHE_CAPACITY, Integer.toString(capacity)));
            lruPurgeNumber = Integer.parseInt(properties.getProperty(CACHE_LRU_PURGE_NUMBER, Integer.toString(lruPurgeNumber)));
        }
        if( log != null ) { log.debug("Cache() capacity = " + capacity + " loadFactor = " + loadFactor + " lruPurgeNumber = " + lruPurgeNumber); }
        cache = new C30SDKCacheMap<K, V>(capacity + 1, loadFactor, lruPurgeNumber);
        lock = new Object();
    }


    /**
     * Retrieves a value from the cache with the specified key value.
     *
     *@param  key  The key of the item in the cache.
     *@return      The cached object. May be null if not present or the item has aged
     *             off the cache.
     */
    public V get(K key) {
        V value = null;

        synchronized (lock) {
            value = cache.get(key);
        }

        return value;
    }


    /**
     * Adds a cacheable object to the cache
     *
     *@param  obj  Reference to the cacheable object.
     */
    public void set(V obj) {
        synchronized (lock) {
            cache.put(obj.getKey(), obj);
        }
    }


    /**
     * Retrieves a Map of the cached objects. Typically used for purging purposes.
     *
     *@return    Reference to a Map of the cached objects.
     */
    public Map<K, V> getCache() {
        return cache;
    }


    /**
     * Purges any stale objects from the cache.
     */
    public void purgeStale() {
        synchronized (lock) {
            if( log != null ) { log.debug("Purging stale objects"); }
            ArrayList<Object> staleObjects = new ArrayList<Object>(capacity);
            C30SDKCacheable isStaleObject = null;
            Object objectKey = null;

            // Collect up all the stale objects...
            for( Map.Entry me : cache.entrySet() ) {
                objectKey = me.getKey();
                isStaleObject = (C30SDKCacheable) me.getValue();

                if (isStaleObject.isStale()) {
                    staleObjects.add(objectKey);
                }
            }

            // Now, remove all the stale objects...
            for( Object staleObj : staleObjects ) {
                cache.remove(staleObj);
            }

            if( log != null ) { log.debug("Number of stale objects purged = " + staleObjects.size()); }
            staleObjects = null;
        }
    }
    
    /**
     * Removes all cached entries.
     *
     */
    public void reset() {
        if (cache != null) {
            synchronized (lock) {
                cache.clear();
            }
        }
    }

    /**
     * Removes the specified object from the cache map
     *
     * @param  key  Key of the object to remove from the cache.
     * @return      The object removed from the cache. May be null if object is not present.
     */
    public Object remove(Object key) {
        Object cachedObject = null;
        
        synchronized (lock) {
            cache.remove(key);
        }
        return cachedObject;
    }
}

