package com.cycle30.sdk.cache;


import java.util.HashMap;
import java.util.LinkedList;

import org.apache.log4j.Logger;

/**
 * Implements a fixed-size map capable of managing which of the entries is
 * least-recently-used.
 *
 *@author    rnelluri
 */
public class C30SDKCacheMap<K,V> extends HashMap<K,V> {
    private static final long serialVersionUID = 7521327757098604610L;
    private static Logger log = Logger.getLogger(C30SDKCacheMap.class);
    /**
     * The maximum capacity of the map. The number of entries in the map cannot
     * exceed this number.
     */
    protected int capacity = 100;
    /**
     * The number of least-recently-used members of the cache to purge when more
     * room is needed in the cache.
     */
    protected int lruPurgeNumber = 5;
    /**
     * A list of the least recently used elements in the cache.
     */
    protected LinkedList<Object> lruList = null;


    /**
     * Constructs a new CacheMap with the specified parameters
     *
     *@param  capacity        The maximum number of elements allowed in the cache
     *@param  loadFactor      The load factor (used for hashing) of the hash
     *@param  lruPurgeNumber  The number of elements to purge from the cache when more space
     *                        is needed.
     */
    public C30SDKCacheMap(int capacity, float loadFactor, int lruPurgeNumber) {
        super(capacity + 1, loadFactor);

        this.lruPurgeNumber = lruPurgeNumber;
        this.capacity = capacity;

        lruList = new LinkedList<Object>();
    }


    /**
     * Returns the object with the specified key from the cache map
     *
     *@param  key  The key of the object to return from the cache map
     *@return      Reference to the cached object. May be null if not present.
     */
    @Override
	public V get(Object key) {
        V value = null;

        value = super.get(key);

        if (value != null) {
            /*
             *  Make sure the guy we retrieved is now the
             *  most recently used. Puts the key at the
             *  end of the list; that makes it less likely
             *  that it will be bumped off the list...
             */
            lruList.remove(key);
            lruList.addLast(key);
        }

        return value;
    }


    /**
     * Puts a new object into the cache. If this new object will push the cache
     * cache over the maximum size, this method purges the least recently used
     * elements.
     *
     *@param  key    Description of the Parameter
     *@param  value  Description of the Parameter
     */
    @Override
	public V put(K key, V value) {
        V oldObject = null;
        
        if (size() + 1 > capacity) {
            purgeLeastRecentlyUsed();
        }

        oldObject = super.put(key, value);
        lruList.add(key);
        
        return oldObject;
    }


    /**
     * Removes the specified object from the cache map
     *
     *@param  key  Key of the object to remove from the cache.
     *@return      The object removed from the cache. May be null if object is not present.
     */
    @Override
	public V remove(Object key) {
        V removed = null;

        removed = super.remove(key);
        if (removed != null) {
            lruList.remove(key);
        }

        return removed;
    }


    /**
     * Removes the cached object at the specified index of the least-recently-used list.
     *
     *@param  index  The index at which the object should be removed.
     */
    public void remove(int index) {
        remove(lruList.get(index));
    }


    /**
     * Purges the least recently used objects from the cache. The number of elements
     * removed is specified by lruPurgeNumber. 
     */
    protected void purgeLeastRecentlyUsed() {
        if( log != null ) { log.debug("Purging least recently used cache entries"); }
        for (int i = 0; i < lruPurgeNumber; i++) {
            super.remove(lruList.removeFirst());
        }
    }
    
    
    /* (non-Javadoc)
     * @see java.util.Map#clear()
     */
    @Override
	public void clear() {
        super.clear();
        
        lruList.clear();
    }

}

