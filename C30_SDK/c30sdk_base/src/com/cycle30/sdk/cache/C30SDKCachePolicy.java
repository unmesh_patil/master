package com.cycle30.sdk.cache;


/**
 * Policy interface for cache purging
 *
 *@author     rnelluri
 */
public interface C30SDKCachePolicy {
    /**
     * Purges the specified cache
     *
     *@param  cache  The Cache to be purged.
     */
    void purge(C30SDKCache cache);
}

