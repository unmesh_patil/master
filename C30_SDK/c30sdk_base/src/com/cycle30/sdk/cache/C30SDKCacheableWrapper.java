package com.cycle30.sdk.cache;



/**
 * A generic wrapper to allow any object with a key to be used as a cacheable object.
 *
 *@author     rnelluri
 */
public class C30SDKCacheableWrapper<K, V> implements C30SDKCacheable<K> {

    /**
     * The key value
     */
    protected K key = null;
    /**
     * The value to be cached.
     */
    protected V value = null;
    /**
     * The time (milliseconds) the object was created.
     */
    protected long creationTime = 0;
    /**
     * The maximum number of milliseconds that the object is allowed to stay in the cache.
     */
    protected long staleTime = 7200000;


    /**
     * Constructs a new cacheable wrapper with the specified key/value pair
     *
     *@param  key    The key for the cacheable object.
     *@param  value  The value to be cached.
     */
    public C30SDKCacheableWrapper(K key, V value) {
        creationTime = System.currentTimeMillis();
        this.key = key;
        this.value = value;
    }
    
    /**
     * Sets the stale time for this cacheable object
     *
     * @param   staleTime  The number of milliseconds to have elapsed since creation of
     *                     the object before it is considered stale.
     */
    public void setStaleTime(long staleTime) {
        this.staleTime = staleTime;
    }


    /**
     * Returns the key of the cacheable object.
     *
     *@return    The key of the object.
     */
    public final K getKey() {
        return key;
    }


    /**
     * Returns the object to be cached.
     *
     *@return    Reference to the cached object.
     */
    public final V getValue() {
        return value;
    }


    /**
     * Returns true if the cached value is stale.
     *
     *@return    True if the object is stale; false otherwise.
     */
    public final boolean isStale() {
        return (System.currentTimeMillis() - creationTime) > staleTime;
    }
}

