package com.cycle30.sdk.cache;

/**
 * Interface to be implemented by objects that can be cached.
 *
 *@author     rnelluri
 */
public interface C30SDKCacheable<K> {

    /**
     * Returns the key value for the cacheable object.
     *
     *@return    The value of the key for the cacheable object. Cannot be null.
     */
    K getKey();


    /**
     * Returns a flag indicating if this cacheable object is stale (needs to be refreshed).
     *
     *@return    True if the object is stale; false otherwise.
     */
    boolean isStale();

}

