package com.cycle30.sdk.exception;
/** This Class is Responsible for the C30SDK Kenan Middlware/ SecurityServer Exceptions.
 * These are connections are mainly for the Middleware and Database
 * All the Error-Codes related to this are stored in c30KenanErrorText_*.properties
 * This is developed as part of the C30SDK Development Framework.
 * @author Ranjith Kumar Nelluri
 *
 */
public class C30SDKKenanFxCoreException extends C30SDKException {
	
	private static final long serialVersionUID = -1776631507072582049L;

	/**
	 * Constructor which sets a message into the exception.
	 * @param message the message being set.
	 */
	public C30SDKKenanFxCoreException(final String message) {
		super(message);
	}
	
	/**
	 * Constructor which sets a message and exception code into the exception.
	 * @param message the message being set.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKKenanFxCoreException(final String message, final String exceptionCode) {
		super(message, exceptionCode);
	}
	
	/**
	 * Constructor which sets a message and a thrown exception into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 */
	public C30SDKKenanFxCoreException(final String message, final Throwable exception) {
		super(message, exception);
	}

	/**
	 * Constructor which sets a message, a thrown exception and an exception code into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKKenanFxCoreException(final String message, final Throwable exception, final String exceptionCode) {
		super(message, exception, exceptionCode);
	}

	/**
	 * Constructor which sets a message, a thrown exception and an exception code into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 * @param shortdescr Short Descirption of the Exception.
	 * @param exceptionCode the exception code being set.
	 */

	public C30SDKKenanFxCoreException(final String message, final String shortdescr, final Throwable exception, final String exceptionCode)
	{
		super(message, shortdescr, exception, exceptionCode);
	}
	/**
	 * 
	 * @param message
	 * @param AdditionalErrorText
	 * @param exceptionCode
	 */
	public C30SDKKenanFxCoreException(final String message, final String AdditionalErrorText, final String exceptionCode) {
		super(message, AdditionalErrorText, exceptionCode);
	}
	
	/**
	 * Constructor which sets a message, a thrown exception and an exception code into the exception.
	 * @param message the message being set.
	 * @param exception the exception itself which is being set into the exception.
	 * @param exceptionCode the exception code being set.
	 */
	public C30SDKKenanFxCoreException(final String message, final Throwable exception,final String AdditionalErrorText, final String exceptionCode) {
		super(message, exception, AdditionalErrorText,exceptionCode);
	}
}
