package com.cycle30.sdk.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.csgsystems.aruba.connection.ServiceException;
import com.csgsystems.bali.connection.ApiMappings;
import com.csgsystems.fx.security.util.FxException;
import com.cycle30.sdk.cache.C30SDKCache;
import com.cycle30.sdk.cache.C30SDKCacheManager;
import com.cycle30.sdk.cache.C30SDKCacheableWrapper;
import com.cycle30.sdk.datasource.C30KenanMWDataSource;
import com.cycle30.sdk.exception.C30SDKException;
import com.cycle30.sdk.exception.C30SDKKenanFxCoreException;
import com.cycle30.sdk.exception.C30SDKObjectConnectionException;
import com.cycle30.sdk.object.C30SDKValueConstants;

/**
 * Provides management of multiple caches and performs periodic sweeps
 * through the caches to purge stale entries of the Kenan Configuration.
 * This loads all the Admin objects listed in the 
 * C30SDKKenanConfig.Properties
 *
 *@author     rnelluri
 */
public class C30KenanConfigManager
{

	// The one-and-only instance of the CacheManager.
	private static C30KenanConfigManager instance = null;
	private static Logger log = Logger.getLogger(C30KenanConfigManager.class);
	public C30SDKPropertyFileManager exceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	public C30SDKPropertyFileManager KenanExceptionResourceBundle = C30SDKPropertyFileManager.getInstance();
	boolean firstReconnetAttempt= true;
	public C30SDKCache <String, C30SDKCacheableWrapper<String, Object>> configCache ;
	//Stores the list of Kenan Objects that need to be cached.




	/**
	 * Returns the one-and-only instance of the C30KenanConfigManager.
	 *
	 *@return    The one-and-only instance of the C30KenanConfigManager.
	 * @throws C30SDKException 
	 */
	public static C30KenanConfigManager getInstance(C30KenanMWDataSource catalogMWDataSource) throws C30SDKException 
	{
		if (instance == null) 
		{
			if( log != null ) 
			{ 
				log.debug("Creating new instance of C30KenanConfigManager"); 
			}
			instance = new C30KenanConfigManager(catalogMWDataSource);

		}

		return instance;
	}

	/**
	 * Constructs a new C30KenanConfigManager
	 * @throws C30SDKException 
	 */
	protected C30KenanConfigManager(C30KenanMWDataSource catalogMWDataSource) throws C30SDKException 
	{

		C30SDKCacheManager cacheManager=C30SDKCacheManager.getInstance();
		configCache= cacheManager.createCache();
		//cacheManager.createCache();

		boolean startSweep = true;
		Properties listofKenanObjects=loadListofKenanObjects();
		Iterator itSrcProps = listofKenanObjects.entrySet().iterator();
		while (itSrcProps.hasNext()) 
		{
			Map.Entry me = (Map.Entry) itSrcProps.next();
			String objectMethod = (String) me.getKey();
			String objectName = (String) me.getValue();
			log.debug("Entries in the Property File ");
			log.debug(objectMethod + ":"+objectName);
			//HashMap cachedObject = (HashMap) retrieveKenanObject(catalogMWDataSource, objectMethod, objectName);
		}
		
	}

	//Removing references to Kenan APITS/ Tuxedo.
	/*
	 * Method used to retrieve the Kenan Object from Kenan System.
	 */
	/*private Map retrieveKenanObject(C30KenanMWDataSource catalogMWDataSource, String objectMethod, String objectName) throws C30SDKException  
	{
		// Make Kenan MW call to retrieve data.
		//
		Map callResponse = new HashMap();
		HashMap dataFilter = new HashMap();
		dataFilter.put("Fetch", new Boolean(true));

		Map request = new HashMap();
		request.put(objectName, dataFilter);
		try
		{
			callResponse = catalogMWDataSource.queryData(ApiMappings.getCallName(objectMethod), request);
			C30SDKCacheableWrapper<String, Object> cacheWrapper = new C30SDKCacheableWrapper<String, Object>(objectMethod,callResponse);
			configCache.set(cacheWrapper);			
		}
		catch (ServiceException e) 
		{
			String error="";
			if (e.item instanceof HashMap) {
				error = error + ((HashMap) e.item).get("BSD_APPNAME");
				error = error + "-"+((HashMap) e.item).get("BSD_TEXTID");
			} else {
				error = error + e.item + "\n";
			}
			if (e.getErrorCategory().equalsIgnoreCase("BakcEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}
			else if (e.getErrorCategory().equalsIgnoreCase("FrontEnd"))
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorName()),e,e.getErrorName());	
			}
			else
			{
				throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(error.trim()),e.name ,e,error.trim());
			}


		} catch (FxException e) {
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(e.getErrorCode().trim()) ,e,e.getErrorCode().trim());
		} catch (IOException e) {
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		} catch (terrapin.tuxedo.TuxError e) {
			String message = e.getMessage();
			String code[]=message.split("-");
			throw new C30SDKKenanFxCoreException(KenanExceptionResourceBundle.getString(code[0].trim()) ,e,code[0].trim());
		}
		catch (Exception e)
		{
			throw new C30SDKObjectConnectionException(exceptionResourceBundle.getString("CONN-013") ,e,"CONN-013");
		}
		return callResponse;
	} */

	/*
	 * This method is used to load the KenanObjects from the list of
	 * 
	 */
	private Properties loadListofKenanObjects() throws C30SDKException{
		log.debug("Loading all the Kenan Objects defined in the C30SDKKenanConfig.properties");
		C30SDKPropertyFileManager rb = C30SDKPropertyFileManager.getInstance();
		Properties kenanConfigListProp = null;
		try 
		{			
			kenanConfigListProp = rb.addFromFileAndReturnProperties(C30SDKValueConstants.KENAN_CONFIG_LIST_FILE);
		}
		catch (java.util.MissingResourceException ex) 
		{
			if( log != null ) 
			{ 
				log.debug("Kenan Config List File is not found");
				throw new C30SDKException(rb.getString("LW-KENAN-001"),ex, "LW-KENAN-001");
			}
		}
		catch(Exception e)
		{
			log.error(e);
			throw new C30SDKException(rb.getString("LW-KENAN-002"),e, "LW-KENAN-002");
		}

		return kenanConfigListProp;
	}

	/*
	 * Get the cache of the Configuration.
	 */
	public C30SDKCache<String, C30SDKCacheableWrapper<String, Object>> getConfigCache() {
		return configCache;
	}


}
