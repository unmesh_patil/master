package com.cycle30.sdk.config;

import java.io.File;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

/**
 * Provides centralized  control
 * over the configuration/Properties for C30SDK Framework. 
 *
 *@author     rnelluri
 */
public class C30SDKPropertyFileManager extends Properties 
{

    private static final long serialVersionUID = -5439024782805715936L;
    
    // The one-and-only instance of the ConfigurationAccess interface
    private static C30SDKPropertyFileManager propertyFileManager = null;
    // True if checking for duplicates when loading multiple sets of
    // configuration properties.
    private boolean checkingForDuplicates = false;
    
    private static Logger log = Logger.getLogger(C30SDKPropertyFileManager.class);

    /**
     *  Returns the one-and-only instance of the C30SDKConfigurationManager.
     *  First loads the ServerConfiguration manager in DomainFwk. If the class does
     *  not exist or has problem loading that class, uses this configuration manager.
     *  
     *@return    Reference to the implementation of the ConfigurationAccess interface
     */
    
    public static C30SDKPropertyFileManager getInstance() {
        if (propertyFileManager == null) {
            try {
                //Class serverManagerClass = Class.forName("com.cycle30.sdk.config.properties.C30SDKPropertyFileManager");
                //serverManagerClass.newInstance();
            	propertyFileManager = new C30SDKPropertyFileManager();
            } catch (Exception e) {
                log.warn("Can not load com.cycle30.sdk.config.properties.C30SDKPropertyFileManager class");
                
                propertyFileManager = new C30SDKPropertyFileManager();
            }
        }

        return propertyFileManager;
    }

  
    /**
     *  Sets a flag for whether or not to check for duplicates when
     *  adding resources to the configuration manager. To have any
     *  useful effect, this flag must be set prior to loading multiple
     *  sets of resources.
     *
     *@param  checkingForDuplicates  True if it is desired to check for
     *                               duplicates.
     */
    public void setCheckingForDuplicates(boolean checkingForDuplicates) {
        log.info("Setting the checkingForDuplicates flag to " + checkingForDuplicates);
        this.checkingForDuplicates = checkingForDuplicates;
    }


    /**
     *  Returns the flag indicating if the resource manager is checking
     *  for duplicates in the loaded resources.
     *
     *@return    True if the configuration manager is checking for duplicates
     */
    public boolean isCheckingForDuplicates() {
        return checkingForDuplicates;
    }


    /**
     *  Opens the specified resource from the class path and loads it as a
     *  Java properties file and adds the loaded properties to the current set.
     *
     *@param  resourceName                       The name of the resource to load
     *@exception  java.io.IOException            Thrown if an error reading the properties
     *@exception  java.io.FileNotFoundException  Thrown if the resource is not found.
     *@exception  java.lang.IllegalArgumentException Thrown if the checking duplicates 
     *  flag is set and there is at least one key in the incoming properties that 
     *  matches an existing key.
     */
    public void addFromClassPath(String resourceName) throws java.io.IOException, java.io.FileNotFoundException {
    	log.info("Adding configuration resource " + resourceName + " from class path");
        java.io.InputStream is = this.getClass().getResourceAsStream(resourceName);

        if (is != null) {
            addFromStream(is);
        } else {
        	log.error("Couldn't find " + resourceName + " on the CLASSPATH", null);
            throw new java.io.FileNotFoundException("Couldn't find " + resourceName + " on the CLASSPATH");
        }
    }
    
    /**
     *  Opens the specified resource from the class path and loads it as a
     *  Java properties file and adds the loaded properties to the current set.
     *
     *@param  resourceName                       The name of the resource to load
     *@exception  java.io.IOException            Thrown if an error reading the properties
     *@exception  java.io.FileNotFoundException  Thrown if the resource is not found.
     *@exception  java.lang.IllegalArgumentException Thrown if the checking duplicates 
     *  flag is set and there is at least one key in the incoming properties that 
     *  matches an existing key.
     */
    public void addFromClassPathAsaResourceBundle(String resourceName) throws java.io.IOException, java.io.FileNotFoundException {
    	
    	log.info("Adding configuration resource " + resourceName + " from class path");
    	resourceName = "/"+resourceName+"_"+Locale.getDefault().getLanguage()+".properties";
        java.io.InputStream is = this.getClass().getResourceAsStream(resourceName);

        if (is != null) {
            addFromStream(is);
        } else {
        	log.error("Couldn't find " + resourceName + " on the CLASSPATH", null);
            throw new java.io.FileNotFoundException("Couldn't find " + resourceName + " on the CLASSPATH");
        }
    }


    /**
     *  Opens the specified file and loads it as a Java properties file and
     *  adds the loaded properties to the current set.
     *
     *@param  configFilePath                     The path of the properties file to load
     *@exception  java.io.IOException            Thrown if there was an error reading the properties file.
     *@exception  java.io.FileNotFoundException  Thrown if the file didn't exist
     *@exception  java.lang.IllegalArgumentException Thrown if the checking duplicates 
     *  flag is set and there is at least one key in the incoming properties that 
     *  matches an existing key.
     */
    public void addFromFile(java.io.File configFilePath) throws java.io.IOException, java.io.FileNotFoundException {
    	log.info("Adding configuration resource " + configFilePath + " from a file");
        java.io.InputStream is = null;
        try {
            is = new java.io.FileInputStream(configFilePath);
        } catch (java.io.FileNotFoundException ex) {
        	log.error("Error getting resource", ex);
            throw ex;
        }

        addFromStream(is);
    }

    /**
     *  Opens the specified file and loads it as a Java properties file and
     *  return the property object
     *
     *@param  resourceName                     The path of the properties file to load
     *@exception  java.io.IOException            Thrown if there was an error reading the properties file.
     *@exception  java.io.FileNotFoundException  Thrown if the file didn't exist
     *@exception  java.lang.IllegalArgumentException Thrown if the checking duplicates 
     *  flag is set and there is at least one key in the incoming properties that 
     *  matches an existing key.
     */
    public Properties addFromFileAndReturnProperties (String resourceName) throws java.io.IOException, java.io.FileNotFoundException {
    	log.info("Adding configuration resource " + resourceName + " from class path");
        java.io.InputStream is = this.getClass().getResourceAsStream(resourceName);

        Properties newProperties = new Properties();
        newProperties.load(is);
        return newProperties;
    }
    /**
     *  Adds an additional set of properties from the specified input stream.
     *  The input stream must contain a file formatted as a standar Java
     *  properties file.
     *
     *@param  inputStream              Input stream containing the properties to add
     *@exception  java.io.IOException  Thrown if an error reading the input stream
     *@exception  java.lang.IllegalArgumentException Thrown if the checking duplicates 
     *  flag is set and there is at least one key in the incoming properties that 
     *  matches an existing key.
     */
    public void addFromStream(java.io.InputStream inputStream) throws java.io.IOException {
        Properties newProperties = new Properties();
        newProperties.load(inputStream);

        addFromProperties(newProperties);
    }


    /**
     *  Adds the properties to the current configuration.
     *
     *@param  properties  The properties to be added to the configuration
     *@exception  java.lang.IllegalArgumentException Thrown if the checking duplicates 
     *  flag is set and there is at least one key in the incoming properties that 
     *  matches an existing key.
     */
    public void addFromProperties(Properties properties) {
        if (checkingForDuplicates) {
            Enumeration enumKeys = properties.propertyNames();
            while (enumKeys.hasMoreElements()) {
                String key = (String) enumKeys.nextElement();
                if (containsKey(key)) {
                	log.warn("Properties file contains duplicate key: " + key, null);
                    throw new IllegalArgumentException("Properties file contains duplicate key: " + key);
                }
            }
        }

        putAll(properties);
    }


    /**
     *  Returns the value of the specified property as a string.
     *
     *@param  key  The key value of the property
     *@return      The value of the property as a string. Returns null if key not found.
     */
    public String getString(String key) {
        if (! containsKey(key)) {
        	
        		log.info("Key '" + key + "' does not exist");
        }
        return getProperty(key);
    }


    /**
     *  Returns the value of the specified property as an integer.
     *
     *@param  key  The key value of the property
     *@return      The integer value of the property.
     *@exception   java.langIllegalArgumentException  thrown if the key was not found.
     */
    public int getInteger(String key) {
        int retVal = -1;

        String value = getString(key);
        if (value != null) {
            retVal = Integer.parseInt(value);
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }

        return retVal;
    }


    /**
     *  Returns the value of the specified property as a float.
     *
     *@param  key  The key value of the property
     *@return      The floating point value of the property
     *@exception   java.langIllegalArgumentException  thrown if the key was not found.
     */
    public float getFloat(String key) {
        float retVal = -1.0f;

        String value = getString(key);
        if (value != null) {
            retVal = Float.parseFloat(value);
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }

        return retVal;
    }


    /**
     *  Returns the value of the specified property as a double
     *
     *@param  key  The key value of the property
     *@return      The value of the property as a double
     *@exception   java.langIllegalArgumentException  thrown if the key was not found.
     */
    public double getDouble(String key) {
        double retVal = -1.0;

        String value = getString(key);
        if (value != null) {
            retVal = Double.parseDouble(value);
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }

        return retVal;
    }


    /**
     *  Returns the value of the specified property as a long
     *
     *@param  key  The key value of the property
     *@return      The long value of the property
     *@exception   java.langIllegalArgumentException  thrown if the key was not found.
     */
    public long getLong(String key) {
        long retVal = -1;

        String value = getString(key);
        if (value != null) {
            retVal = Long.parseLong(value);
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }

        return retVal;
    }


    /**
     *  Returns the value of the specified property as a short
     *
     *@param  key  The key value of the property
     *@return      The short value of the property
     *@exception   java.langIllegalArgumentException  thrown if the key was not found.
     */
    public short getShort(String key) {
        short retVal = -1;

        String value = getString(key);
        if (value != null) {
            retVal = Short.parseShort(value);
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }

        return retVal;
    }


    /**
     *  Returns the value of the specified property as a File object
     *
     *@param  key  The key value of the property
     *@return      The File object that represents the property value
     *@exception   java.langIllegalArgumentException  thrown if the key was not found.
     */
    public java.io.File getFile(String key) {
        java.io.File retVal = null;

        String value = getString(key);
        if (value != null) {
            retVal = new java.io.File(value);
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }

        return retVal;
    }


    /**
     *  Returns the value of the specified property as a boolean.
     *
     *@param  key  The key value of the property
     *@return      The boolean value of the property. If the property doesn't exist
     *  false is returned.
     */
    public boolean getBoolean(String key) {
        boolean boolVal = false;
        
        String value = getString(key);
        if (value != null) {
            boolVal = Boolean.valueOf(value).booleanValue();
        }
        
        return boolVal;
    }


    /**
     *  Returns the value of the specified property as a URL object
     *
     *@param  key                                 The key value of the property
     *@return                                     The URL object representing the value
     *@exception  java.net.MalformedURLException  Thrown if the URL is malformed.
     *@exception   java.langIllegalArgumentException  thrown if the key was not found.
     */
    public java.net.URL getURL(String key) throws java.net.MalformedURLException {
        java.net.URL retVal = null;

        String value = getString(key);
        if (value != null) {
            retVal = new java.net.URL(value);
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }

        return retVal;
    }


    /** 
     *  Returns a runtime class for the specified key
     *
     *@param  key              The key value of the property
     *@return  The runtime class of the configured key
     *@exception ClassNotFoundException thrown if unable to find runtime class
     */
    public Class getRuntimeClass(String key) throws ClassNotFoundException {
        Class runtimeClass = null;
        
        String value = getString(key);
        if (value != null && value.length() > 0) {
            runtimeClass = Class.forName(value, true, getClass().getClassLoader());
        } else {
            throw new IllegalArgumentException("Invalid key specified: " + key);
        }
        
        return runtimeClass;
    }
    
    
    /**
     *  Returns a java.util.Locale for the specified key
     *
     *@param  key             The key value of the property
     *@return  The java.util.Locale instance for the property; null if not found
     */
    public java.util.Locale getLocale(String key) {
        Locale locale = null;
        
        String value = getString(key);
        if (value != null && value.length() > 0) {
            StringTokenizer st = new StringTokenizer(value, "_");
            String languageCode = st.nextToken();
            String countryCode = null;
            String variant = null;
            if (st.hasMoreTokens()) {
                countryCode = st.nextToken();
            }
            if (st.hasMoreTokens()) {
                variant = st.nextToken();
            }
            if (languageCode != null && countryCode == null && variant == null) {
                locale = new Locale(languageCode);
            } else if (languageCode != null && countryCode != null && variant == null) {
                locale = new Locale(languageCode, countryCode);
            } else if (languageCode != null && countryCode != null && variant != null) {
                locale = new Locale(languageCode, countryCode, variant);
            }
        }
        
        return locale;
    }
    
    
    /**
     *  Constructs a new C30SDKConfigurationManager
     */
    private C30SDKPropertyFileManager() {
    	log.info("Creating new Property File Manager");
        
    }
    



    protected static File getPersonalizationPath(String relativeUri) {
        return new File(System.getProperty("user.home"), relativeUri);
    }
    
    

 
 
    /**
     * Overridden to check the personalization properties first.
     * 
     * @see java.util.Properties#getProperty(java.lang.String, java.lang.String)
     */
    @Override
	public String getProperty(String key, String defaultValue) {
        String   value = super.getProperty(key);

      
            if (value == null) {
                value = defaultValue;
            }
        
        return value;
    }

    /**
     * Overridden to check the personalization properties first.
     * 
     * @see java.util.Properties#getProperty(java.lang.String)
     */
    @Override
	public String getProperty(String key) {
        String value =    super.getProperty(key);
        
        
        return value;
    }
    
    

    

/*
    public Properties getPrefixedProperties(String keyPrefix) {
        Properties prefixedProperties = new Properties();
        
        if (keyPrefix != null && keyPrefix.length() > 0) {
            
             * First, we scan the application properties (in the 'this'
             * reference, then we scan the personalization properties. This will
             * cause the personalization properties to override the application
             * properties (if this actually occurs).
             
            addPrefixedProperties(keyPrefix, prefixedProperties, this);
            addPrefixedProperties(keyPrefix, prefixedProperties, cacheConfigProperties);
        }
        
        return prefixedProperties;
    }*/
    
    
    /**
     * Helper method to find properties in the source properties which have keys
     * that start with the specified prefix and add those properties to the
     * prefixed properties.
     * 
     * @param keyPrefix the prefix of the keys to look for
     * @param prefixedProperties the properties in which to place the matching
     * properties.
     * @param sourceProperties the source set of properties to scan for matches.
     */
    private void addPrefixedProperties(String keyPrefix, Properties prefixedProperties, Properties sourceProperties) {
        if (sourceProperties == null) {
            return;
        }
        
        Iterator itSrcProps = sourceProperties.entrySet().iterator();
        while (itSrcProps.hasNext()) {
            Map.Entry me = (Map.Entry) itSrcProps.next();
            String key = (String) me.getKey();
            String value = (String) me.getValue();
            if (key.startsWith(keyPrefix)) {
                prefixedProperties.setProperty(key, value);
            }
        }
    }

  
}


