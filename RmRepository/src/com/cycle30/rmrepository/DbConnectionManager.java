package com.cycle30.rmrepository;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate.encryptor.HibernatePBEEncryptorRegistry;

public class DbConnectionManager {

	private final static String decryptionAlgorithm = "PBEWithMD5AndDES";
	private static String decryptionPassword = "";

	private static final String PROPERTIES_FILE_NAME = "com.cycle30.rmrepository.repository";
    private static final Logger log = Logger.getLogger(DbConnectionManager.class);
    private static final ResourceBundle properties = ResourceBundle.getBundle(PROPERTIES_FILE_NAME, Locale.US);
    

    //-----------------------------------------------------------------------------
    /** Return a db connection.
     * @param password
     * @return Connection instance
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection getDbConnection() 
                                      throws ClassNotFoundException, SQLException, Exception {
        
        // Get full URI:port/qualified/path from thresholding.properties file
        String url  = properties.getString("repository.db.url");
        String user = properties.getString("repository.db.username");
        String pwd  = properties.getString("repository.db.password");
        
        Connection conn = null;
        Class.forName("oracle.jdbc.driver.OracleDriver");
        
        String password = decryptPassword(pwd);
        
        conn = DriverManager.getConnection (url, user, password);
        
        return conn;

    }
    
    //----------------------------------------------------------------------------
    /* Decrypt db password stored in Advanced Encryption Standard (AES) format.
     * 
     */
    private static String decryptPassword(String pwd) throws Exception {
        
		String decryptedPwd = "";
		// decrypt the password.
		try {

			if (decryptionPassword.trim().length() == 0){
				String decryptionUserKey = properties.getString("db.encryptkey");  
				decryptionPassword = System.getenv(decryptionUserKey);
			}
			StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
			strongEncryptor.setAlgorithm(decryptionAlgorithm);
			strongEncryptor.setPassword(decryptionPassword);
			HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
			registry.registerPBEStringEncryptor("configurationHibernateEncryptor", strongEncryptor);
			decryptedPwd = strongEncryptor.decrypt(pwd);

		} 
		catch (Exception e) {
			throw new Exception ("Error decrypting password: ", e);
		}

        return decryptedPwd;

    }

}
