package com.cycle30.rmrepository;

import com.cycle30.rmrepository.RepositoryUpdaterInf;
import com.cycle30.rmrepository.ArborUpdater;


/** class to instantiate the appropriate repository updater class. */
public class UpdaterFactory {

    
    public static RepositoryUpdaterInf getUpdater(String deployType) throws Exception {
                
        if (deployType.equalsIgnoreCase("ARBOR")) {
           return new ArborUpdater();
        }
        
//        if (deployType.equalsIgnoreCase("CITRIX")) {
//            return new CitrixUpdater();
//        }
        
        throw new Exception("Unrecognized deploy type: " + deployType);
        
    }
}
