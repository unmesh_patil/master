package com.cycle30.rmrepository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.StringTokenizer;

import com.cycle30.rmrepository.RepositoryUpdaterInf;
import com.cycle30.rmrepository.data.ArborDeployDetail;
import com.cycle30.rmrepository.data.ReleaseSummary;


//------------------------------------------------------------------------------
/** Repository updater class for Arbor deploys.  This class opens the 
 * 
 *  */
public class ArborUpdater implements RepositoryUpdaterInf  {

    private String logName;
    private String currentLogSection;
    private ReleaseSummary releaseSummary;
    private ArborDeployDetail deployDetail;
    private Connection dbConn;
    
	private boolean failureFlag = false;	//used to indicate explicit failure when it occurs
    
    private final static int DEPLOY_SUCCESS = 3;
    private final static int DEPLOY_FAILURE = 4;
    
    private final static String[] LOG_SECTIONS = { "Arbor", "WorkPoint", "database", "API", "batch" };
    
    private final static String[] ALLOWABLE_SQL_ERRORS = { "00942", /*'table doesn't exist error */ };
        
    private final static String DEPLOY_DETAIL_SQL =
            "insert into deployment " +
            "  (status_id, deployer, datetime, reason, revision_id, environment_id) " +
            "  values (?, ?, to_date(?, 'MON DD HH24:MI:SS YYYY'), ?, ?, ?)";
    
    private File inputFile;
    private BufferedReader logFile;
        

    //--------------------------------------------------------------------------
    /**
     *  @param conn Connection
     *  @param logFileName 
     */
    public void updateRepository(Connection conn, 
                                  String logFileName,
                                  ReleaseSummary releaseSumm)               
                                       throws FileNotFoundException, IOException, Exception
    {      
        String textLine = null;
        logName = logFileName;
        
        this.releaseSummary = releaseSumm;
        this.dbConn = conn;
        
        logFile = (BufferedReader)openLogFile();
        
        // Get deployer name, start/end time, etc from log header area
        parseLogHeader(logFile);

        // populate an Arbor detail object.
        deployDetail = new ArborDeployDetail();
        deployDetail.setRevisionId(releaseSummary.getRevisionId());
        deployDetail.setDatetime(releaseSummary.getEndTime());
        deployDetail.setDeployer(releaseSummary.getUserid());
        deployDetail.setEnvironmentId(releaseSummary.getDeployEnvId());
        deployDetail.setReason("");		// initialize to nothing
        deployDetail.setStatusId(DEPLOY_FAILURE);	// default "failure" status
        
        // For each log file line, see if it needs to be added to the repository.
        // Do this section-by-section, since the deploy process and log reflects
        // different deployment details for scripts, ADT, J2EE, etc.
        try {
            
           while ( (textLine = logFile.readLine()) != null) {
                parseSectionDetails(textLine);
           }
           
           logFile.close();
           
           insertArborDetail(deployDetail);

           return;           
        }
        catch (IOException ioe) {
            throw ioe;
        }

    }
    
    
    

    //--------------------------------------------------------------------------
    /** Open the log file
     * 
     */
    public Reader openLogFile() throws FileNotFoundException {
        
        inputFile = new File(logName);
        FileReader fr = new FileReader(inputFile);
        BufferedReader br = new BufferedReader(fr);
        
        return br;
    }
    
   


    
    //--------------------------------------------------------------------------
    /** Parse the deploy start/end date and userid from the top of the log
     * 
     * @param logFile
     * @throws Exception 
     */
    private void parseLogHeader(BufferedReader logFile) throws Exception {
        
        String textLine = null;
 
        try {
            
            while ( (textLine = logFile.readLine()) != null) {
                
                // get userid
                if (textLine.indexOf('@') != -1) {
                    String userid = parseUserid(textLine);
                    releaseSummary.setUserid(userid.trim());
                }
                
                // get start date/time
                if (textLine.indexOf("Start time:") != -1) {
                    String startTime = parseDate(textLine);
                    releaseSummary.setStartTime(startTime);
                }
                
                // get end date/time, then exit.
                if (textLine.indexOf("End time:") != -1) {
                    String endTime = parseDate(textLine);
                    releaseSummary.setEndTime(endTime);
                    return;
                }
                
            }
            
            // if makes it this far, means EOF and that something's missing in the log
            throw new Exception ("Unexpected EOF while parsing log file for userid, start/end time");
        }
        
        catch (IOException ioe) {
            throw ioe;
        }
    }
    
    

    //--------------------------------------------------------------------------
    /**
     * @param logFile
     */
    private String parseUserid(String textLine) {
        
        // get userid from the email address.  the text line will look like:
        // "Deployer: xyz@gci.com".  The following obviously only works if @gci.com
        // is in every email address supplied.
        
        String temp1 = textLine.replaceAll("Deployer:", "");
        String temp2 = temp1.replaceAll("@gci.com", "");
        
        return temp2;
        
        
    }
    
    
    //--------------------------------------------------------------------------
    /**
     * @param logFile
     */
    private String parseDate(String textLine) {
        
        // get date from the text line, which will look like:
        // "Start time: Mon Jan 12 10:33:13 NAST 2009"
        //    - or -
        //"End time: Mon Jan 12 10:37:12 NAST 2009"
        
        String temp1 = textLine.replaceAll("Start time: ", "");
        String temp2 = temp1.replaceAll("End time: ", "");
      
        // Tokenize the result string to get the month, day-of-month, 
        // time and year.  
        StringBuffer theDate = new StringBuffer();
        StringTokenizer st = new StringTokenizer(temp2, " ");
        
        int i=0;
        while (st.hasMoreTokens()) {
            i++;
            String token = st.nextToken();
            if (i==2 || i==3 || i==4 || i==6) {
                theDate.append(token + " ");
            }
        }
        return theDate.toString();
        
    }
    
    

    //--------------------------------------------------------------------------
    /** Parse each individual section and add pertinent details to the repository.
     *  The sections are: "Arbor", "ADT", "XVM", "database", "J2EE", "MULE", "batch"
     *  
     * @param textLine
     */
    private void parseSectionDetails (String textLine) throws Exception {
        
        // return if new section in the log file detected.
        if (isNewLogSection(textLine)) {
            return;
        }
        
        // return if no recognizable section has been detected yet.
        if (currentLogSection == null) {
            return;
        }
        
        if (currentLogSection.equalsIgnoreCase("Arbor")) {
            parseArborScriptSection(textLine);
        }
        
        if (currentLogSection.equalsIgnoreCase("batch")) {
            parseBatchSection(textLine);
        }

       if (currentLogSection.equalsIgnoreCase("WorkPoint")) {
            parseWorkPointSection(textLine);
        }
        
        if (currentLogSection.equalsIgnoreCase("database")) {
            parseDatabaseSection(textLine);
        }
        
        if (currentLogSection.equalsIgnoreCase("API")) {
            parseApiSection(textLine);
        }

    }
    
    
    
    
    //--------------------------------------------------------------------------
    /** Parse/process the "Arbor" section of the log file
     *  
     * @param textLine
     */
    private void parseArborScriptSection(String textLine) throws Exception {

        // Need to retrofit this "parser" to do the following:
        //  1) Check for more errors
        //  2) Update reason with something more useful
    	
    	if (textLine.indexOf("BUILD FAILED") != -1) {
    		failureFlag = true;		// indicate explicit failure
    		deployDetail.setStatusId(DEPLOY_FAILURE);
    		deployDetail.setReason("arbor deploy failed for some reason");
    	}
    	if (textLine.indexOf("BUILD SUCCESSFUL") != -1) {
    		if (failureFlag) {
    			deployDetail.setReason(deployDetail.getReason()
    					+ "\narbor deploy partially successful");
    		}
    		else {
    			deployDetail.setStatusId(DEPLOY_SUCCESS);
    		}
    	}
    }
    
    
    //--------------------------------------------------------------------------
    /** Parse/process the "batch" section of the log file
     *  
     * @param textLine
     */
    private void parseBatchSection(String textLine) throws Exception {

        // Need to retrofit this "parser" to do the following:
        //  1) Check for more errors
        //  2) Update reason with something more useful
    	
    	if (textLine.indexOf("BUILD FAILED") != -1) {
    		failureFlag = true;		// indicate explicit failure
    		deployDetail.setStatusId(DEPLOY_FAILURE);
    		deployDetail.setReason("batch deploy failed for some reason");
    	}
    	if (textLine.indexOf("BUILD SUCCESSFUL") != -1) {
    		if (failureFlag) {
    			deployDetail.setReason(deployDetail.getReason()
    					+ "\nbatch deploy partially successful");
    		}
    		else {
    			deployDetail.setStatusId(DEPLOY_SUCCESS);
    		}
    	}
    }
    
    
    //--------------------------------------------------------------------------
    /** Parse/process the "WorkPoint" section of the log file
     *  
     * @param textLine
     */
    private void parseWorkPointSection (String textLine) throws SQLException {
        
        // Need to retrofit this "parser" to do the following:
        //  1) Check for more errors
        //  2) Update reason with something more useful
    	
    	if (textLine.indexOf("BUILD FAILED") != -1) {
    		failureFlag = true;		// indicate explicit failure
    		deployDetail.setStatusId(DEPLOY_FAILURE);
    		deployDetail.setReason("workpoint deploy failed for some reason");
    	}
    	if (textLine.indexOf("BUILD SUCCESSFUL") != -1) {
    		if (failureFlag) {
    			deployDetail.setReason(deployDetail.getReason()
    					+ "\nworkpoint deploy partially successful");
    		}
    		else {
    			deployDetail.setStatusId(DEPLOY_SUCCESS);
    		}
    	}
    }
    
    
    //--------------------------------------------------------------------------
    /** Parse/process the "database" section of the log file.
     * 
     * @param textLine
     */
    private void parseDatabaseSection (String textLine) throws SQLException, Exception {

        // look for SQL source-control artifacts.  
        if (textLine.indexOf("Executing") != -1)
        {
            
            // Need to retrofit these "parsers" to do the following:
            //  1) Check for errors
            //  2) Set deployDetail.status to failed (4)
            //  3) Update reason with something useful


            // get SQL file name from the full string.
            String artifact = getArtifactName(textLine);
            
            // strip off ' for zzzzz on KFXzzz' suffix
            int idx = artifact.indexOf(" for");
            if (idx != -1) {
               artifact = artifact.substring(0, idx);
            }
//            deployDetail.setArtificatName(artifact);
            
            // get artifact path
            String path = getArtifactPath(textLine);
            
            // strip out the dist path.
            idx = path.indexOf("dist/database");
            if (idx != -1) {
                path = path.substring(idx);
            }
//            deployDetail.setScmPath(path);
            

            
            // Check if any errors occurred prior to the SQL commit.  Set a marker 
            // in the file so can return to current position.  Set it to the size of
            // the file...which in theory shouldn't be a problem since the log files
            // typically aren't very big, and casting the long down to an int (2G)  
            // also should never be an issue.
            long fileSize = inputFile.length();
            logFile.mark((int)fileSize);
//            deployDetail.setSuccess( getSqlSuccess() );
            logFile.reset();
            
            if (!failureFlag) {
            	deployDetail.setStatusId(DEPLOY_SUCCESS);
            }

        }
    }

    
    
    //--------------------------------------------------------------------------
    /** Check for any Oracle errors prior to a commit
     *  
     * @param logFile
     */
    private boolean getSqlSuccess() throws IOException, Exception {
    	
        // spin through the log file looking for errors prior to the 'commit' 
        // acknowledgement for the SQL script.  Ignore any 'false-positives', like
        // 
        try {
            String textLine = "";
            
            while ( (textLine = logFile.readLine()) != null) {

                // if hit another 'Executing' log entry, return as this means
                // script results being interrogated is complete.
                if (textLine.indexOf("Executing") != -1) {
                   return true;
                }
                
                int idx = textLine.indexOf("ORA-");
                if (idx != -1) {
                    String errorCode = textLine.substring(idx+4, idx+9);
                    if (isAllowableSqlError(errorCode)) {
                        continue;
                    }
                    else {
                        return false;
                    }
                }
                
            }  // end while()
            
            // big problem if EOF... 
            logFile.close();
            throw new Exception ("Unexpected EOF detect while parsing SQL actions in log file!");
            
         }
        
         catch (IOException ioe) {
             throw ioe;   
         }

    }

    
    
    //--------------------------------------------------------------------------
    /** 
     * @param textLine
     */
    private boolean isAllowableSqlError (String errorCode) {

        for (int i=0; i<ALLOWABLE_SQL_ERRORS.length; i++) {
            if (errorCode.equals(ALLOWABLE_SQL_ERRORS[i])) {
                return true;
            }
        }
            
        return false;        

    }
    
    
    
    //--------------------------------------------------------------------------
    /** Parse/process the "API" section of the log file
     *  
     * @param textLine
     */
    private void parseApiSection (String textLine) throws SQLException {

        // Need to retrofit this "parser" to do the following:
        //  1) Check for more errors
        //  2) Update reason with something more useful
    	
    	if (textLine.indexOf("BUILD FAILED") != -1) {
    		failureFlag = true;		// indicate explicit failure
    		deployDetail.setStatusId(DEPLOY_FAILURE);
    		deployDetail.setReason("workpoint deploy failed for some reason");
    	}
    	if (textLine.indexOf("BUILD SUCCESSFUL") != -1) {
    		if (failureFlag) {
    			deployDetail.setReason(deployDetail.getReason()
    					+ "\nworkpoint deploy partially successful");
    		}
    		else {
    			deployDetail.setStatusId(DEPLOY_SUCCESS);
    		}
    	}
    }

    
    //--------------------------------------------------------------------------
    /** Get artifact name from a path/artifact string
     *  
     * @param textLine
     */
    private String getArtifactName(String textLine) {
        
        // get index of the last '/' char by working backwards from end of string
        int j=0;
        for (int i=textLine.length() - 1; i>0; i--) {
            j=i;
            if (textLine.charAt(i) == '/') {
                break;
            }
        }
        
        String artifact = textLine.substring(j+1);
        
        return artifact;

    }
    
    
    //--------------------------------------------------------------------------
    /** Get artifact path from a path/artifact string
     *  
     * @param textLine
     */
    private String getArtifactPath(String textLine) {


        // get index of the last '/' char by working backwards from end of string
        int j=0;
        for (int i=textLine.length() - 1; i>0; i--) {
            j=i;
            if (textLine.charAt(i) == '/') {
                break;
            }
        }
        
        // Get index of first actual char in path, since may be in the form of
        // "U arbor/dist/..." or "Executing /opt/app..."  (in case of SQL script).
        int startIdx = textLine.indexOf(" ");
        
        String path = textLine.substring(startIdx+1, j);
        
        return path;

    }
    
    
    //--------------------------------------------------------------------------
    /**
     *  Get the log file section name after "Executing" is found in a log entry
     * 
     * @param logText
     */
    private boolean isNewLogSection(String logText) {
        
        if (logText.indexOf("Executing ") == -1) {
            return false;
        }
        
        // Look for "Executing xyz" section string.
        for (int i=0; i<LOG_SECTIONS.length; i++) {   
            String sectionMatch = "Executing " + LOG_SECTIONS[i];
                
            if (logText.indexOf(sectionMatch) != -1) {
                currentLogSection = LOG_SECTIONS[i];
                System.out.println("Processing section: " + currentLogSection);
                return true;
            }    
        } 
        
        return false;

    }
    
    
    
    //--------------------------------------------------------------------------
    /**
     *  Insert an Arbor deploy detail into repository database
     * 
     * @param deployDetail 
     * @throws SQLException 
     */
    private void insertArborDetail(ArborDeployDetail deployDetail) throws SQLException {
     
        try {            
            PreparedStatement ps = null;
            ps = dbConn.prepareStatement(DEPLOY_DETAIL_SQL);
            ps.setInt(1, deployDetail.getStatusId());
            ps.setString(2, deployDetail.getDeployer());
            ps.setString(3, deployDetail.getDatetime());
            ps.setString(4, deployDetail.getReason());
            ps.setInt(5, deployDetail.getRevisionId());
            ps.setInt(6, deployDetail.getEnvironmentId());
    
//            deployDetail.print();
            ps.execute();
     
        }
        catch (SQLException se) {
            throw se;
        }
    }
    
    
    
}
