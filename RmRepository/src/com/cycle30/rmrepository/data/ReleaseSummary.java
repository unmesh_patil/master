package com.cycle30.rmrepository.data;

/**
 * @author DWreggit
 *
 */
public class ReleaseSummary {

    private int revisionNumber;			// SVN revision #
    private int revisionId;				// Tracking App's internal revision.revision_id
    private String userid;				// user id
    private String deploySystem;		// system (i.e., K1, K2, M1)
    private int    deploySystemId;		// Tracking App's internal SystemId (for deploySystem)
	private String deployEnv;			// environment (i.e., DEV, CFG, UAT, PRD)
	private int    deployEnvId;			// Tracking App's internal environmentId (for deployEnv)
    private String deployType;			// Type of deploy (arbor, jboss, etc)?????
    private String startTime;			// start time (from log)
    private String endTime;				// end time (from log)
    
    
    public void print() {
    	System.out.println("revisionNumber = " + revisionNumber + " (revId = " + revisionId + ")");
    	System.out.println("userid" + userid);
    	System.out.println("deploySystem" + deploySystem + " (" + deploySystemId + ")");
    	System.out.println("deployEnv" + deployEnv + " (" + deployEnvId + ")");
    	System.out.println("deployType" + deployType);
    	System.out.println("startTime" + startTime);
    	System.out.println("endTime" + endTime);
    }
    
    /**
     * @return the revNumber
     */
    public int getRevisionNumber() {
        return revisionNumber;
    }
    /**
     * @param revNumber the revNumber to set
     */
    public void setRevisionNumber(int revisionNumber) {
        this.revisionNumber = revisionNumber;
    }
    /**
     * @return the revId
     */
    public int getRevisionId() {
        return revisionId;
    }
    /**
     * @param revNumber the revNumber to set
     */
    public void setRevisionId(int revisionId) {
        this.revisionId = revisionId;
    }
    /**
     * @return the userid
     */
    public String getUserid() {
        return userid;
    }
    /**
     * @param userid the userid to set
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
    /**
     * @return the system
     */
    public String getDeploySystem() {
		return deploySystem;
	}
	/**
	 * @param system the system to set
	 */
	public void setDeploySystem(String deploySystem) {
		this.deploySystem = deploySystem;
	}
	/**
	 * @return the deploySystemId
	 */
	public int getDeploySystemId() {
		return deploySystemId;
	}

	/**
	 * @param deploySystemId the deploySystemId to set
	 */
	public void setDeploySystemId(int deploySystemId) {
		this.deploySystemId = deploySystemId;
	}

	/**
     * @return the deployEnv
     */
    public String getDeployEnv() {
        return deployEnv;
    }
    /**
     * @param deployEnv the deployEnv to set
     */
    public void setDeployEnv(String deployEnv) {
        this.deployEnv = deployEnv;
    }
	/**
	 * @return the deployEnvId
	 */
	public int getDeployEnvId() {
		return deployEnvId;
	}

	/**
	 * @param deployEnvId the deployEnvId to set
	 */
	public void setDeployEnvId(int deployEnvId) {
		this.deployEnvId = deployEnvId;
	}  
	
	/**
     * @return the deployType
     */
    public String getDeployType() {
        return deployType;
    }
    /**
     * @param deployType the deployType to set
     */
    public void setDeployType(String deployType) {
        this.deployType = deployType;
    }
    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }
    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }
    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    
}


