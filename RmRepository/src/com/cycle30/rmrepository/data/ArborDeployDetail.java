package com.cycle30.rmrepository.data;


// class representing a deployment detail data item
public class ArborDeployDetail {

	int revisionId;
	int environmentId;
	int statusId;
	String deployer;
	String datetime;
	String reason;
	

	public void print() {
    	System.out.println("revisionId = " + revisionId);
    	System.out.println("environmentId = " + environmentId);
    	System.out.println("statusId = " + statusId);
    	System.out.println("deployer = " + deployer);
    	System.out.println("datetime = " + datetime);
    	System.out.println("reason = " + reason);
    }
    
    
    /**
	 * @return the revisionNumber
	 */
	public int getRevisionId() {
		return revisionId;
	}

	/**
	 * @param revisionNumber the revisionNumber to set
	 */
	public void setRevisionId(int revisionId) {
		this.revisionId = revisionId;
	}

	/**
	 * @return the environmentId
	 */
	public int getEnvironmentId() {
		return environmentId;
	}

	/**
	 * @param environmentId the environmentId to set
	 */
	public void setEnvironmentId(int environmentId) {
		this.environmentId = environmentId;
	}

	/**
	 * @return the statusId
	 */
	public int getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the deployer
	 */
	public String getDeployer() {
		return deployer;
	}

	/**
	 * @param deployer the deployer to set
	 */
	public void setDeployer(String deployer) {
		this.deployer = deployer;
	}

	/**
	 * @return the datetime
	 */
	public String getDatetime() {
		return datetime;
	}

	/**
	 * @param datetime the datetime to set
	 */
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

}
