package com.cycle30.rmrepository;

import java.io.FileNotFoundException;
import java.io.Reader;
import java.sql.Connection;
import com.cycle30.rmrepository.data.ReleaseSummary;

// Interface representing behavior for an RM repository updater
public interface RepositoryUpdaterInf {

    public Reader openLogFile() throws FileNotFoundException;
    public void updateRepository(Connection conn, String logFileName, ReleaseSummary rs) throws Exception;
}
