package com.cycle30.rmrepository;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cycle30.rmrepository.UpdaterFactory;
import com.cycle30.rmrepository.RepositoryUpdaterInf;
import com.cycle30.rmrepository.data.ReleaseSummary;


/** Driver for performing CM/RM repository updates. */
public class RepositoryUpdateDriver {


    private ReleaseSummary releaseSummary = new ReleaseSummary();
    private Connection conn;
    
    private String logFile;
    
    private final static String ARBOR_RELEASE_SUMMARY_UPDATE_SQL = 
        "update release_summary " +
        "set deploy_start=to_date(?, 'MON DD HH24:MI:SS YYYY'), " +
        "    deploy_end=to_date(?, 'MON DD HH24:MI:SS YYYY'), " +
        "    deployer_name=?, " +
        "    deploy_env=?, " +
        "    release_cr=?, " +
        "    release_number=?, " +
        "    deploy_status=null " +
        "where build_tag=?";
    
    private final static String CITRIX_RELEASE_SUMMARY_UPDATE_SQL = 
        "update release_summary " +
        "set deploy_start=to_date(?, 'MM/DD/YYYY HH24:MI:SS'), " +
        "    deploy_end=to_date(?, 'MM/DD/YYYY HH24:MI:SS'), " +
        "    deployer_name=?, " +
        "    deploy_env=?, " +
        "    release_cr=?, " +
        "    release_number=?, " +
        "    deploy_status=null " +
        "where build_tag=?";
    
  
    private final static String VALIDATE_SYSTEM =
    	"select system_id " +
    	"from system_ref " +
    	"where system_name=?";
    
    private final static String VALIDATE_ENVIRONMENT =
    	"select environment_id " +
    	"from environment_ref " +
    	"where system_id=? and " +
    	"      environment_name=?";
    
    private final static String VALIDATE_REVISION =
    	"select revision_id " +
    	"from revision " +
    	"where revision_number = ?";
    
    //---------------------------------------------------------------------------
    /**
     *  main entry point (invoked from external script)
     */
    public static void main (String args[]) {
        
        //System.out.println("Updating CM/RM repository information...");
        
        RepositoryUpdateDriver rud = new RepositoryUpdateDriver();
        rud.updateRepository(args);
        
        //System.out.println("Repository updates complete.");

    }

    

    //---------------------------------------------------------------------------
    /**
     * @param args
     */
    private void updateRepository (String[] args) {
        
        // Get arguments passed from script
        parseArguments(args);
        
        try { 
        	
            // Get a db connection for reuse. Die if there's an error
            conn = DbConnectionManager.getDbConnection();
            
            // Validate arguments
        	validateArguments(conn);
            
           // Get correct updater instance based on the deploy type (eg. Arbor, Citrix)
           RepositoryUpdaterInf rui = UpdaterFactory.getUpdater(releaseSummary.getDeployType());        
        
           // Do updates to repository
           rui.updateRepository(conn, logFile, releaseSummary);
           
           // Update the release summary with info from the deploy
//           updateReleaseSummary();
           
            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        finally {
            if (conn != null) {
                try {
                  conn.close();
                } catch (Exception e) {
                    System.out.println("Fatal database error occurred!");
                    e.printStackTrace();
                    System.exit(1);
                }
            }
            
        }
        
    }
    
    
    
    //---------------------------------------------------------------------------
    /**
     * @param String args[]
     */
    private void parseArguments(String args[]) {
        
        /*args required:  1-revision #
                          3-system (K1, K2, M1)
                          3-deploy environment (DEV, CFG, etc)
                          4-deploy type (arbor)
                          5-logfile name */
        
        // Fail if not enough args supplied
        if (args.length != 5) {
            System.out.println("Usage: java -jar <jarname> com.cycle30.rmrepository.RepositoryUpdateDriver " +
                                       "rev# system environment deploytype logname");
            System.exit(1);
        }
        
        // keep items needed for updating summary information in an object that 
        // can be easily updated/passed around.
        
        releaseSummary.setRevisionNumber(Integer.parseInt(args[0]));
        releaseSummary.setDeploySystem(args[1].toUpperCase());
        releaseSummary.setDeployEnv(args[2].toUpperCase());
        releaseSummary.setDeployType(args[3].toUpperCase());
        
        logFile = args[4];
        
    }
    

    //---------------------------------------------------------------------------
    /**
     * @param Connection conn
     */
    private void validateArguments(Connection conn) throws SQLException {
    	
    	try {
    		PreparedStatement ps = null;
    		ResultSet rs = null;
    		
    		releaseSummary.setDeployEnvId(-1);
    		releaseSummary.setDeploySystemId(-1);
    		releaseSummary.setRevisionId(-1);
    		
    		// validate system
    		ps = conn.prepareStatement(VALIDATE_SYSTEM);
    		
    		ps.setString(1, releaseSummary.getDeploySystem());
    		
    		ps.execute();
    		rs = ps.getResultSet();
    		if (rs != null) {
    			rs.next();
    			releaseSummary.setDeploySystemId(rs.getInt(1));		// system_id
    		}
    		if (releaseSummary.getDeploySystemId() == -1) {
    			System.err.println("Unable to get systemId for " + releaseSummary.getDeploySystem());
    		}
    		rs.close();
    		ps.close();
    		
    		// validate environment
    		ps = conn.prepareStatement(VALIDATE_ENVIRONMENT);
    		
    		ps.setInt(1, releaseSummary.getDeploySystemId());
    		ps.setString(2, releaseSummary.getDeployEnv());
    		
    		ps.execute();
    		rs = ps.getResultSet();
    		if (rs != null) {
    			rs.next();
    			releaseSummary.setDeployEnvId(rs.getInt(1));	// environment_id
    		}
    		if (releaseSummary.getDeployEnvId() == -1) {
    			System.err.println("Unable to get environmentId for " + releaseSummary.getDeploySystem() + " + " + releaseSummary.getDeployEnv());
    		}
    		rs.close();
    		ps.close();
    		
    		// validate revision number
    		ps = conn.prepareStatement(VALIDATE_REVISION);
    		
    		ps.setInt(1, releaseSummary.getRevisionNumber());
    		
    		ps.execute();
    		rs = ps.getResultSet();
    		if (rs != null) {
    			rs.next();
    			releaseSummary.setRevisionId(rs.getInt(1));	// revision.revision_id
    		}
    		if (releaseSummary.getRevisionId() == -1) {
    			System.err.println("Unable to get revisionId for " + releaseSummary.getRevisionNumber());
    		}
    		rs.close();
    		ps.close();
    	}
        catch (SQLException se) {
            throw se;
        }
    }
  
    
    //---------------------------------------------------------------------------
    /**  Update release summary entry with arguments passed via the deploy script,
     *   and info gathered from parsing the log.
     */
    private void updateReleaseSummary() throws SQLException {
        
        try {
            
            PreparedStatement ps = null;
            
            // use different update SQL based on deploy type (ARBOR or CITRIX), 
            // since the log files have different date formats.
            if (releaseSummary.getDeployType().equalsIgnoreCase("ARBOR")) {
                ps = conn.prepareStatement(ARBOR_RELEASE_SUMMARY_UPDATE_SQL);
            }
            else {
                ps = conn.prepareStatement(CITRIX_RELEASE_SUMMARY_UPDATE_SQL);
            }

            ps.setString(1, releaseSummary.getStartTime());
            ps.setString(2, releaseSummary.getEndTime());
            ps.setString(3, releaseSummary.getUserid());
            ps.setString(4, releaseSummary.getDeployEnv());

            ps.setInt(7, releaseSummary.getRevisionNumber());
            
            ps.execute();
     
        }
        catch (SQLException se) {
            throw se;
        }
    }


}
