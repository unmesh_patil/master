package com.cycle30.rmrepository;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;




//  Validate that release info exists, based on a build tag supplied by the
//  calling application

public class ReleaseInfoValidator {
    
    private static final String RELEASE_SUMMARY_SQL = 
        "select create_date from release_summary where build_tag = ? ";
    
    
    private static final String INSERT_SQL = 
        "insert into release_summary values (?, sysdate, '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, null, '', '', '', '', '')";
    
    
    
    //--------------------------------------------------------------------------
    /**
     * @param args
     */
    public static void main(String[] args) {
        
        if (args.length == 0) {
            System.out.println("ReleaseInfoValidator: no build tag supplied!");
            System.exit(1);
        }
        
        String buildTag = args[0];
        validateReleaseInfo(buildTag);
        
    }
    
    
    
    //--------------------------------------------------------------------------
    /**
     * @param buildTag
     * @return
     */
    private static void validateReleaseInfo(String buildTag) {
        
        Connection conn = null;
            
        try {
            
           conn = DbConnectionManager.getDbConnection();

           PreparedStatement ps = null;
           ps = conn.prepareStatement(RELEASE_SUMMARY_SQL);
           ps.setString(1, buildTag);

           ResultSet resultSet  = ps.executeQuery();
           
           // If a result exists (found a row), exit with success status
           while (resultSet.next()) {
               System.exit(0);
           }
           
           // not found - create a new one.
           ps = conn.prepareStatement(INSERT_SQL);
           ps.setString(1, buildTag);
           ps.execute();
           System.exit(2);
           
           
        } catch (Exception e) {
            System.out.println("Fatal database error occurred!");
            e.printStackTrace();
            System.exit(1);
        }
        
        finally {
            if (conn != null) {
                try {
                  conn.close();
                } catch (Exception e) {
                    System.out.println("Fatal database error occurred!");
                    e.printStackTrace();
                    System.exit(1);
                }
            }
            
        }

    }

}
