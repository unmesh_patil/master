#!/usr/bin/perl

use strict;

#use Term::ReadKey;
#use File::Basename;

#unbuffered output
$| = 1;

my $svn_basedir="trunk"; # subdir of SVN repository to use
my $svnroot="https://subversion.assembla.com/svn/c30billing/awn";
my $svnsubdir;
my $svn_user;
my $svn_password;
my $revision;
my $release_tag="trunk";  # subdir of SVN repository to use
my $basedir; # user directory
my $delete_flag;
my ($revision_id);
my $cm_db_name="griddb";
my $cm_db_user="c30cmrm";
my $cm_db_pwd="h43Urc0nfg";
my $db_cmd;
my $cm_db_cmd_file="/tmp/build.rev.sql.command";
my $cm_db_out_file="/tmp/build.rev.sql.output";
my $releaselistfile="/tmp/build.rev.tmp.filelist";

#
# Execute the SQL script
# inputs: schema name
#         password
#         database SID
#         SQL command(s)
#
sub runscript {
  my ($user, $password, $database, $cmd) = @_;
  my $program = "sqlplus";
  my $arg1 = "-silent";

  open FILE, ">", $cm_db_cmd_file or die $!;
  print FILE "$cmd\n";
  print FILE "exit\n";
  close FILE;

  system("$program $arg1 $user/$password\@$database \@$cm_db_cmd_file 1>$cm_db_out_file");
}

#
# Check for and process files for a revision for a given system_id
# inputs: system_name, AWN, MOBIUS, PIVOT
#
sub processfiles {
  my ($system_name) = @_;

  if (-z $releaselistfile) {
    print("No files in this revision for system_name = $system_name\n");
    return;
  }

  $db_cmd = "select system_id from system_ref where system_name = '$system_name';";
  runscript($cm_db_user, "$cm_db_pwd", $cm_db_name, "$db_cmd");

  my $system_id;
  open(SYSID, "$cm_db_out_file") || die "Could not open $cm_db_out_file";
  my $sysIdText;
  while (<SYSID>) {
    my $line = $_;
    chomp($line);
    $sysIdText += $line;
  }
  close(SYSID);
  ($system_id) = $sysIdText =~ m/(\d+)$/;
  print ("Found system_id = $system_id\n");

  if ($system_id == 0) {
    print "Unable to find system_id for system_name = $system_name.\n";
    return;
  }

  print ("Loading files for system_name = $system_name\n");

  open(FILELIST, "$releaselistfile") || die "Could not open $releaselistfile";
  system("sort -o $releaselistfile $releaselistfile");

  #Create revision record if not already done for this revision
  if (!defined($revision_id)) {
    #create revision record
    print("Insert revision record for $revision\n");
    $db_cmd = "insert into revision (system_id, revision_number, outage_required, active) values ($system_id, $revision, 'N', 'Y');";
    runscript($cm_db_user, "$cm_db_pwd", $cm_db_name, "$db_cmd");

    #extract revision_id that was just created
    $db_cmd = "select revision_id from revision where revision_number = $revision and system_id = $system_id;";
#    print("extracting new revision_id: $db_cmd\n");
    runscript($cm_db_user, "$cm_db_pwd", $cm_db_name, "$db_cmd");

    open(REVID, "$cm_db_out_file") || die "Could not open $cm_db_out_file";
    my $revText;
    while (<REVID>) {
      my $line = $_;
      chomp($line);
      $revText += $line;
    }
    close(REVID);

    ($revision_id) = $revText =~ m/(\d+)$/;
  }

  print "revision_id = :$revision_id:\n";
  while (<FILELIST>) {
    my $svnline = $_;
    my $svnpath;
    chomp($svnline );
    my($action, $filebase, $filename) = $svnline =~ m/(^[AMD]).*,(.*\/)(.*)$/;
    print($action . ", " . $filebase . ", " . $filename . "\n");
    open FILE, ">", $cm_db_cmd_file or die $!;
    $db_cmd = "insert into component (revision_id, path, name, action) values ($revision_id, '$filebase', '$filename', '$action');";
    runscript($cm_db_user, "$cm_db_pwd", $cm_db_name, "$db_cmd");
  }
  close(FILELIST);

  system("rm -f $cm_db_cmd_file");
  system("rm -f $cm_db_out_file");
  system("rm -f $releaselistfile");
}


#
#MAIN EXECUTION
#
if ($#ARGV < 0) {
  print "Usage: $0 <revision> [-t <release_tag>] [-u <svn user>] [-p <svn pwd>] [-d]\n";
  exit 1;
}

#extract db password
#this is only a test and never worked 
#system("`echo '\#K2\@cycl3eod0tc0m$' | gpg -q --passphrase-fd 0 --no-tty -d $ENV.ARBORDIR/.arborpw.gpg`");


$revision = $ARGV[0];
$svn_basedir = "trunk";
print "pre-loading revision: $revision\n";

my $x = 1;
while ($x <= $#ARGV) {
  if ($ARGV[$x] eq "-t") {
    $x = $x + 1;
    $release_tag = $ARGV[$x];
    $svn_basedir = "branches/$release_tag";
    print "Checking branch: $release_tag\n";
  }
  elsif ($ARGV[$x] eq "-u") {
    $x = $x + 1;
    $svn_user = $ARGV[$x];
    print "Using svn username from command line\n";
  }
  elsif ($ARGV[$x] eq "-p") {
    $x = $x + 1;
    $svn_password = $ARGV[$x];
    print "Using svn password from command line\n";
  }
  elsif ($ARGV[$x] eq "-d") {
    $delete_flag = "yes";
    print "Delete flag enabled\n";
  }
  else {
    print "Usage: $0 <revision> [-t <release_tag>] [-d]\n";
    exit 1;
  }
  $x = $x + 1;
}

if (!defined($revision)) {
  print STDERR "Revision # is required.  Exiting.\n";
  die "No revision number";
}

if (defined($delete_flag)) {
  $db_cmd = "delete revision where revision_number = $revision;";
  runscript($cm_db_user, "$cm_db_pwd", $cm_db_name, "$db_cmd");

  print "Revision $revision deleted.\n";
  exit 0;
}

if (!defined($svn_user)) {
#get SVN username and password
  print "Enter SVN username: ";
  $svn_user = <STDIN>;
  chomp $svn_user ;
}

if (!defined($svn_password)) {
  print "Enter SVN password: ";
  system("stty -echo");
  $svn_password = <STDIN>;
  system("stty echo");
  print "\n";
  chomp $svn_password;
}

#check if this revision has already been loaded
print("Checking for existing revision entry...\n");
$db_cmd = "select revision_id from revision where revision_number = $revision;";
runscript($cm_db_user, "$cm_db_pwd", $cm_db_name, "$db_cmd");

my $rev_check = `grep -c 'REVISION_ID' $cm_db_out_file`;
if ($rev_check > 0) {
  print "Revision already loaded.  Please delete before reloading.\n";
  exit 0;
}

my $shortrev = $revision - 1;

system("rm -f $releaselistfile");    #remove just to be sure it's gone before we begin

print("Using svnroot=$svnroot\n");

$svnsubdir="kenan/all/$svn_basedir";
print("Fetching files from $svnsubdir....\n");
system("svn diff --username=$svn_user --password=$svn_password --summarize -r $shortrev:$revision $svnroot/$svnsubdir | sed -e \"s/^MM*[ \t]*/M,/\" | sed -e \"s/^ [AM][ \t]*/A,/\" | sed -e \"s/^A[ \t]*/A,/\" | sed -e \"s/^D[ \t]*/D,/\" | sed -e \"s|$svnroot/||\" >> $releaselistfile");

$svnsubdir="kenan/m2m/$svn_basedir";
print("Fetching files from $svnsubdir....\n");
system("svn diff --username=$svn_user --password=$svn_password --summarize -r $shortrev:$revision $svnroot/$svnsubdir | sed -e \"s/^MM*[ \t]*/M,/\" | sed -e \"s/^ [AM][ \t]*/A,/\" | sed -e \"s/^A[ \t]*/A,/\" | sed -e \"s/^D[ \t]*/D,/\" | sed -e \"s|$svnroot/||\" >> $releaselistfile");

$svnsubdir="integration/dist/$svn_basedir";
print("Fetching files from $svnsubdir....\n");
system("svn diff --username=$svn_user --password=$svn_password --summarize -r $shortrev:$revision $svnroot/$svnsubdir | sed -e \"s/^MM*[ \t]*/M,/\" | sed -e \"s/^ [AM][ \t]*/A,/\" | sed -e \"s/^A[ \t]*/A,/\" | sed -e \"s/^D[ \t]*/D,/\" | sed -e \"s|$svnroot/||\" >> $releaselistfile");

processfiles("AWN");

print "done!\n";

