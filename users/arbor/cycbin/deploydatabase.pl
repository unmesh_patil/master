#!/usr/bin/perl -w

use strict;

use File::Basename;
use File::Path qw(mkpath);

my @pwent; # pwent entry for the user.
my $db_sid;  # database SID
my $showusage;
my $svn_basedir="trunk"; # subdir of SVN repository to use
my $svnroot="https://subversion.assembla.com/svn/c30billing/awn";
my $svn_user;
my $svn_password;
my $tmpdir="deployc30"; # place where dist files are checked out
my $distdir="database"; # distribution directory for db files.
my $basedir; # user directory
my $dbtype; # db type (i.e., arbor, portal, networx)
my @users; # users, set from directory of dist files.
my $user; # iteration variable.
my $revision = ""; # svn revision number, set from argv[1]
my $release_tag; # Release tag, set from argv[2]
my $logfile; # Optional logfile.

my %passwords = ();

# Make sure no one's playing silly games.
$ENV{"IFS"} = " ";

#my $alldb = { "oracle"   =>  { dev   => "M2MDEV",
#                            cfg   => "C3BCFG",
#                            dem   => "C3BDEM",
#                            tst   => "C3BTST",
#                            uat   => "C3B2UAT",
#                            prd   => "C3BPRD",
#                          },
#};


sub runscript {
   my ($user, $password, $database, $script, $log) = @_;
   my $pid;
   my $program = "sqlplus";
   my $arg1 = "-silent";

   print "Calling $script with $user/********\@$database\n";

   pipe(READFROMPARENT, WRITETOCHILD);
   pipe(READFROMCHILD, WRITETOPARENT);
   if ($pid = fork) {
       # parent
       close(WRITETOPARENT);
       close(READFROMPARENT);

       print WRITETOCHILD "$user/$password\@$database\n";
       print WRITETOCHILD "set feedback on\n";
       print WRITETOCHILD "spool $log\n" if defined $log;
       print WRITETOCHILD "PROMPT Executing $script for $user on $database\n";
       print WRITETOCHILD "\@$script\n";
       print WRITETOCHILD "PROMPT Finished executing $script\n";
       print WRITETOCHILD "spool off\n" if defined $log;
       print WRITETOCHILD "exit\n";
       
       $SIG{CHLD} = sub { 1 while ( waitpid(-1, 0100)) > 0 };
       close(WRITETOCHILD);
   } else {
       die "cannot fork: $!" unless defined $pid;
       #child 
       close(WRITETOCHILD);
       close(READFROMCHILD);
       open(STDOUT, ">&=WRITETOPARENT") or die "couldn't redirect STDOUT: $!";
       open(STDIN, ">&=READFROMPARENT") or die "couldn't redirect STDIN: $!";
       exec($program, $arg1);
   }

   while (<READFROMCHILD>) {
       print $_;
   }
   close(READFROMCHILD);
}

sub verifypassword {
    my ($user, $password, $database) = @_;
    my $name;
    my $type;
    my $created;
    my $modified;
    my @output;
    my $pid;
    my $valid = 1;
    my $program = "sqlplus";
    my $arg1 = "-silent";

    pipe(READFROMPARENT, WRITETOCHILD);
    pipe(READFROMCHILD, WRITETOPARENT);
    if ($pid = fork) {
	# parent
        close(WRITETOPARENT);
        close(READFROMPARENT);

        print WRITETOCHILD "$user/$password\@$database\n";
        print WRITETOCHILD "SET FEEDBACK OFF\n";
        print WRITETOCHILD "SET LINESIZE 78\n";
        print WRITETOCHILD "SET PAGESIZE 0\n";

        print WRITETOCHILD "select 0 from dual;\n";
        print WRITETOCHILD "exit\n";

        $SIG{CHLD} = sub { 1 while ( waitpid(-1, 0100)) > 0 };
        close(WRITETOCHILD);
    } else {
        die "cannot fork: $!" unless defined $pid;
        #child 
        close(WRITETOCHILD);
        close(READFROMCHILD);
        open(STDOUT, ">&=WRITETOPARENT") or die "couldn't redirect STDOUT: $!";
        open(STDIN, ">&=READFROMPARENT") or die "couldn't redirect STDIN: $!";
        exec($program, $arg1);
    }

    while (<READFROMCHILD>) {
        if (index($_, "ORA-01017") > -1) {
            $valid = 0;
            last;
        }
    }

    close(READFROMCHILD);
   
    return $valid;
}


sub getinvalidobjects {
    my ($user, $password, $database, $invalid) = @_;
    my $name;
    my $type;
    my $created;
    my $modified;
    my @output;
    my $pid;
    my $program = "sqlplus";
    my $arg1 = "-silent";

    pipe(READFROMPARENT, WRITETOCHILD);
    pipe(READFROMCHILD, WRITETOPARENT);
    if ($pid = fork) {
	# parent
	close(WRITETOPARENT);
	close(READFROMPARENT);

	print WRITETOCHILD "$user/$password\@$database\n";
        print WRITETOCHILD "SET FEEDBACK OFF\n";
        print WRITETOCHILD "SET LINESIZE 78\n";
        print WRITETOCHILD "SET PAGESIZE 0\n";
        print WRITETOCHILD "COLUMN object_name HEADING 'Object' TRU FORMAT A40\n";
        print WRITETOCHILD "COLUMN object_type HEADING 'Type' TRU FORMAT A10\n";
        print WRITETOCHILD "COLUMN created HEADING 'Created'\n";
        print WRITETOCHILD "COLUMN last_ddl_time HEADING 'Modified'\n";

	print WRITETOCHILD "select object_name, object_type, created, last_ddl_time " .
		      "from user_objects where status <> 'VALID';\n";
	print WRITETOCHILD "exit\n";

	$SIG{CHLD} = sub { 1 while ( waitpid(-1, 0100)) > 0 };
	close(WRITETOCHILD);
    } else {
	die "cannot fork: $!" unless defined $pid;
	#child 
	close(WRITETOCHILD);
	close(READFROMCHILD);
	open(STDOUT, ">&=WRITETOPARENT") or die "couldn't redirect STDOUT: $!";
	open(STDIN, ">&=READFROMPARENT") or die "couldn't redirect STDIN: $!";
	exec($program, $arg1);
    }

    while (<READFROMCHILD>) {
	($name, $type, $created, $modified) = split(/\s+/);
	if ( defined $name && defined $type && defined $created && defined $modified) {
	    $invalid->{$type}{$name} = "Created on $created, Modified on $modified";
	}
    }

    close(READFROMCHILD);
   
    return $invalid;
}

#
#MAIN EXECUTION
#
my $x;
if($#ARGV lt "2") {
  $showusage = 1;
} else {
   for ($x = 0; $x < $#ARGV; $x += 2) {
    SWITCH: {
      if ($ARGV[$x] eq "-r" ) {
        $revision = $ARGV[$x + 1];
        if ($svn_basedir eq "") {
          $svn_basedir = "trunk";
        }
        print "Deploying database release revision: $revision\n";
        last SWITCH;
      }
      elsif ($ARGV[$x] eq "-t") {
        $release_tag = $ARGV[$x + 1];
        $svn_basedir = "branches/$release_tag";
        print "Deploying database release tag: $release_tag\n";
        last SWITCH;
      }
      elsif ($ARGV[$x] eq "-l") {
        $logfile = $ARGV[$x + 1];
        print "Using logfile $logfile\n";
        last SWITCH;
      } 
      elsif ($ARGV[$x] eq "-u") {
        $svn_user = $ARGV[$x + 1];
        print "Using login ID of $svn_user\n";
        last SWITCH;
      }
      elsif ($ARGV[$x] eq "-p") {
        $svn_password = $ARGV[$x + 1];
        print "Password from command line\n";
        last SWITCH;
      }
      else {
        $showusage=1;
      }
    }
  }
}

if(defined $showusage) {
  print "Usage: $0 -r <revision> -u <username> -p <password> [-t <release_tag>] [-l <logfile>]\n";
  exit 1;
}


@pwent=getpwuid($>);
$basedir=$pwent[7];
$db_sid = $ENV{'ORACLE_SID'};
print("Using ORACLE_SID=$db_sid\n");


if (!defined $db_sid) {
   print STDERR "No database is defined.\n";
   print STDERR "Please define a %db_sid entry specifying the location of the Kenan db.\n";
   die "No DB defined";
}

if(!defined $svn_user) {
#  $svn_user=$ENV{'SVN_USER'};
}
if(!defined $svn_password) {
#  $svn_password=$ENV{'SVN_PASSWORD'};
}


#cleanup old tmpdir
system("rm -fr $basedir/$tmpdir");

#create clean tmpdir
mkpath("$basedir/$tmpdir/dist/$distdir");
chdir("$basedir/$tmpdir") || die "Could not chdir to $basedir/$tmpdir\n";

my $releaselistfile="/tmp/contents.database";
my $shortrev = $revision - 1;
#print("shortrev=$shortrev\n\n");

system("rm -f $releaselistfile");

#
#Extract database files from "all"
#
my $svntoplevel = "all";
my $svnmasterpath = "$svnroot/kenan/$svntoplevel/$svn_basedir";

print("Fetching list from $svntoplevel/$svn_basedir/core/oracle\n");
system("svn diff --username=$svn_user --password=$svn_password --summarize -r $shortrev:$revision $svnmasterpath/core/database | sed -e \"s/^[AM][AM]*[ \t]*//\" | sed -e \"s/^ [AM][ \t]*//\" | sed -e \"/^D[ \t]*/ d\" | sed -e \"s|$svnmasterpath/core/database/||\" > $releaselistfile");

open(FILELIST, "$releaselistfile") || die "Could not open $releaselistfile";
system("sort -o $releaselistfile $releaselistfile");

while (<FILELIST>) {
#   Check out SVN files.
    print("Exporting $_");

    my $svnpath = $_;
    chomp($svnpath);

    my($filename, $filebase) = fileparse("$basedir/$tmpdir/dist/database/$svnpath");
#    print("Dir: " . $filebase . "\nFile: " . $filename . "\n");
    mkpath($filebase);

    system("svn -q export --username=$svn_user --password=$svn_password -r $revision $svnmasterpath/core/database/$svnpath\@$revision $basedir/$tmpdir/dist/$distdir/$svnpath");
}

#
#Extract database files from "m2m"
#
$svntoplevel = "m2m";
$svnmasterpath = "$svnroot/kenan/$svntoplevel/$svn_basedir";

print("Fetching list from $svntoplevel/$svn_basedir/core/oracle\n");
system("svn diff --username=$svn_user --password=$svn_password --summarize -r $shortrev:$revision $svnmasterpath/core/database | sed -e \"s/^[AM][AM]*[ \t]*//\" | sed -e \"s/^ [AM][ \t]*//\" | sed -e \"/^D[ \t]*/ d\" | sed -e \"s|$svnmasterpath/core/database/||\" > $releaselistfile");

open(FILELIST, "$releaselistfile") || die "Could not open $releaselistfile";
system("sort -o $releaselistfile $releaselistfile");

while (<FILELIST>) {
#   Check out SVN files.
    print("Exporting $_");

    my $svnpath = $_;
    chomp($svnpath);

    my($filename, $filebase) = fileparse("$basedir/$tmpdir/dist/database/$svnpath");
#    print("Dir: " . $filebase . "\nFile: " . $filename . "\n");
    mkpath($filebase);

    system("svn -q export --username=$svn_user --password=$svn_password -r $revision $svnmasterpath/core/database/$svnpath\@$revision $basedir/$tmpdir/dist/$distdir/$svnpath");
}

#
#Extract database files from "integration/dist"
#

$svntoplevel = "dist";
$svnmasterpath = "$svnroot/integration/$svntoplevel/$svn_basedir";

print("Fetching list from $svntoplevel/$svn_basedir/oracle\n");
system("svn diff --username=$svn_user --password=$svn_password --summarize -r $shortrev:$revision $svnmasterpath/database | sed -e \"s/^[AM][AM]*[ \t]*//\" | sed -e \"s/^ [AM][ \t]*//\" | sed -e \"/^D[ \t]*/ d\" | sed -e \"s|$svnmasterpath/database/||\" > $releaselistfile");

open(FILELIST, "$releaselistfile") || die "Could not open $releaselistfile";
system("sort -o $releaselistfile $releaselistfile");

while (<FILELIST>) {
#   Check out SVN files.
    print("Exporting $_");

    my $svnpath = $_;
    chomp($svnpath);

    my($filename, $filebase) = fileparse("$basedir/$tmpdir/dist/database/$svnpath");
#    print("Dir: " . $filebase . "\nFile: " . $filename . "\n");
    mkpath($filebase);

    system("svn -q export --username=$svn_user --password=$svn_password -r $revision $svnmasterpath/database/$svnpath\@$revision $basedir/$tmpdir/dist/$distdir/$svnpath");
}

my $databasedir="$basedir/$tmpdir/dist/$distdir";
opendir(DIST, "$databasedir") || die "Could not open $databasedir";

#get db type (currently this is only "database")
while ($dbtype = readdir(DIST)) {
    next if $dbtype =~ /^\.$/;
    next if $dbtype =~ /^\.\.$/;
    print "Gathering passwords for $dbtype database\n";
    opendir(DBDIST, "$databasedir/$dbtype") || die "Could not open $databasedir/$dbtype";

#get schemas passwords
    while ($user = readdir(DBDIST)) {
        my ($localpassword, $localpassword2, $same, $valid);
        next if $user =~ /^\.$/;
        next if $user =~ /^\.\.$/;

        $same = 0;
        $valid = 0;
        while (!$same && !$valid) {
            print "Enter password for $user: ";
            `stty -echo`;
            $localpassword=<STDIN>;
            `stty echo`;
            chomp $localpassword;
            print "\n";

            print "Re-enter password for $user: ";
            `stty -echo`;
            $localpassword2=<STDIN>;
            `stty echo`;
            chomp $localpassword2;
            print "\n";

            if ($localpassword eq $localpassword2) {
                $same = 1;
                $passwords{$dbtype}{$user}=$localpassword;
                # quick check to verify password
                $valid = verifypassword($user, $passwords{$dbtype}->{$user}, $db_sid);
                if($valid eq "0") {
                    print "Password for $user is invalid.  Please try again.\n";
                    $same = 0;
                }
            }
        }
    }
    closedir(DBDIST);
}
closedir(DIST);

my %startinginvalid = ();
my %endinginvalid = ();

opendir(DIST, "$databasedir") || die "Could not open $databasedir";
while ($dbtype = readdir(DIST)) {
    next if $dbtype =~ /^\.$/;
    next if $dbtype =~ /^\.\.$/;

	my %files = ();
	
# set and cleanup logs
# the undefined logs are used if a database isn't defined for the environment.

    my $dblog = "/tmp/output.undefined.$revision";
    if (defined $db_sid) {
        my $dblog = "/tmp/output.$db_sid.$revision";
    }

    system("rm -f $dblog");

# scan through all database types (arbor, portal,etc)
    opendir(DBDIST, "$databasedir/$dbtype") || die "Could not open $databasedir/$dbtype";
	my $user;
	
    while ($user = readdir(DBDIST)) {
        my $filename;
	
        next if $user =~ /^\.$/;
        next if $user =~ /^\.\.$/;

		my $startinginvalid_ref = getinvalidobjects($user, $passwords{$dbtype}->{$user}, $db_sid, \%startinginvalid);
		%startinginvalid = %{$startinginvalid_ref};

# get all files to deploy
        opendir(DBUSER, "$databasedir/$dbtype/$user") || die "Could not open $databasedir/$dbtype/$user";
        while ($filename = readdir(DBUSER)) {
            next if $filename =~ /^\.$/;
            next if $filename =~ /^\.\.$/;

            push @{$files{$user}}, "$filename";
        }
    }
    closedir(DBDIST);
		
# deploy db files
    foreach my $fileuser (sort keys %files) {
		system(qq[echo "Deploying to $db_sid database with $fileuser schema\n" >> $logfile]) if defined $logfile;

		foreach my $filename (sort @{$files{$fileuser}}) {
#			print("\tFILE: $filename\n");
			runscript($fileuser, $passwords{$dbtype}->{$fileuser}, $db_sid, "$databasedir/$dbtype/$fileuser/$filename", "$dblog");
#                	system("cat $dblog >> $logfile") if defined $logfile;
		}
		# recompile any invalid objects
		system("recompile_invalid_sql.sh $fileuser $passwords{$dbtype}->{$fileuser} $db_sid");
           		my $endinginvalid_ref = getinvalidobjects($fileuser, $passwords{$dbtype}->{$fileuser}, $db_sid, \%endinginvalid);
		%endinginvalid = %{$endinginvalid_ref};
	}
		
	my $name;
	my $type;

	if (defined $logfile) {
		open(LOGFILE, ">>$logfile") || warn "Could not open logfile";
	} else {
		open(LOGFILE, ">&=STDOUT") || warn "Could not reopen log as stdout";
	}

	print LOGFILE "Analyzing $db_sid database\n";
	if (keys %endinginvalid == 0) {
		print LOGFILE "All objects valid.\n\n";
	}
	else {
		foreach $type (keys %endinginvalid) {
			print LOGFILE "The following " . lc($type). "s were made invalid:\n";
			foreach $name (keys %{$endinginvalid{$type}}) {
				if (! exists $startinginvalid{$type}{$name}) {
					print LOGFILE "\t$name: $endinginvalid{$type}{$name}\n";
				}
			}
		}

		foreach $type (keys %endinginvalid) {
			print LOGFILE "The following " . lc($type). "s were already invalid:\n";
			foreach $name (keys %{$endinginvalid{$type}}) {
				if (exists $startinginvalid{$type}{$name}) {
					print LOGFILE "\t$name\n";
				}
			}
		}
	}

    closedir(DBUSER);
    if (defined $logfile) {
        close(LOGFILE);
    }
}
closedir(DIST);

system("rm -rf $databasedir");
