#!/bin/sh

NoConfirmPresent=$([ "${*#*-noconfirm}" != "$*" ] && echo "true")

#echo "param count = $#"
RESET_OPTIONS=false
if [ \( "X$NoConfirmPresent" = "Xtrue" -a $# -gt 2 \) -o \( "X$NoConfirmPresent" != "Xtrue" -a $# -gt 1 \) ]; then
	echo "Reseting all options to false"
	RESET_OPTIONS=true
fi

. /opt/app/kenanfx2/`whoami`/cycbin/env.sh $RESET_OPTIONS

#
# uses spaces for the environment settings because they are displayed in
# the unix console and tabs won't look good.
#

cat << DONE
Default Environment Settings:
mode        Mode                    $MODE
mw          Middleware              $START_MW
wl          WebLogic Server         $START_WL
secsvr      Security Server         $START_SECSVR
secui       Security UI             $START_SECUI
api         Mule API                $START_MULE_API
cu          Mule Client Updater     $START_MULE_CU
ba          BatchAgent              $START_BATCHAGENT
logs        Clean Log Files         $CLEAN_LOGS
DONE

for arg in $*; do
    case $arg in
	-start)
	    echo "Start mode"
            if [ "X${MODE_OPT}" = "Xtrue" ]; then
                echo "Mode option already specified as $MODE"
                exit;
            else
		MODE_OPT=true
		MODE=start
            fi
	    ;;
	-restart)
	    echo "Restart mode"
            if [ "X${MODE_OPT}" = "Xtrue" ]; then
                echo "Mode option already specified as $MODE"
                exit;
            else
		MODE_OPT=true
		MODE=restart
            fi
	    ;;
	-stop)
	    echo "Stop mode"
            if [ "X${MODE_OPT}" = "Xtrue" ]; then
                echo "Mode option already specified as $MODE"
                exit;
            else
		MODE_OPT=true
		MODE=stop
            fi
	    ;;
	-status)
	    echo "Status mode"
            if [ "X${MODE_OPT}" = "Xtrue" ]; then
                echo "Mode option already specified as $MODE"
                exit;
            else
		MODE_OPT=true
		MODE=status
            fi
	    ;;
	-mw)
	    echo "Explicitly enabling middleware"
            if [ "X${MW_OPT}" = "Xtrue" ]; then
                echo "Middleware option already specified as $START_MW"
                exit;
            else
		MW_OPT=true
		START_MW=true
		MFW_OPT=true
		START_MULE_WFM=true
		MULE_WFM_OVERRIDE=true
            fi
	    ;;
	-wl)
	    echo "Explicitly enabling WebLogic"
            if [ "X${WL_OPT}" = "Xtrue" ]; then
                echo "WebLogic option already specified as $START_WL"
                exit;
            else
		WL_OPT=true
		START_WL=true
            fi
	    ;;
	-secsvr)
	    echo "Explicitly enabling security server"
            if [ "X${SECSVR_OPT}" = "Xtrue" ]; then
                echo "Security Server option already specified as $START_SECSVR"
                exit;
            else
		SECSVR_OPT=true
		START_SECSVR=true
            fi
	    ;;
	-secui)
	    echo "Explicitly enabling security server UI"
            if [ "X${SECUI_OPT}" = "Xtrue" ]; then
                echo "Security UI option already specified as $START_SECUI"
                exit;
            else
		SECUI_OPT=true
		START_SECUI=true
            fi
	    ;;
	-api)
          echo "Explicitly enabling Mule API"
          if [ "X${MULE_API_OPT}" = "Xtrue" ]; then
              echo "Mule API option already specified as $START_MULE_API"
              exit;
          else
              MULE_API_OPT=true
              START_MULE_API=true
          fi
	    ;;
	-cu)
          echo "Explicitly enabling Mule Client Updater" 
          if [ "X${MULE_CU_OPT}" = "Xtrue" ]; then
              echo "Mule client updater option already specified as $START_MULE_CU"
              exit;
          else
              MULE_CU_OPT=true
              START_MULE_CU=true
          fi
	    ;;
	-ba)
          echo "Explicitly enabling BatchAgent"
          if [ "X${BATCHAGENT_OPT}" = "Xtrue" ]; then
              echo "BatchAgent option already specified as $START_BATCHAGENT"
              exit;
          else
              BATCHAGENT_OPT=true
              START_BATCHAGENT=true
          fi
	    ;;
	-logs)
	    echo "Explicitly enabling log removal"
            if [ "X${LOGS_OPT}" = "Xtrue" ]; then
                echo "Logs option already specified as $CLEAN_LOGS"
                exit;
            else
		LOGS_OPT=true
		CLEAN_LOGS=true
            fi
	    ;;
    -noconfirm)
        echo "Disabling action confirmation"
        CONFIRM=false
        ;;
       *)
	    echo "Unknown argument $arg."
	    exit 1
	    ;;
    esac
done

if [ "X$MODE" = "Xundefined" ]; then
    echo "Unable to determine mode (start|stop|restart|status) for script"
    exit
fi

#
# uses spaces for the environment settings because they are displayed in
# the unix console and tabs won't look good.
#

if [ $# -gt 1 ]; then
cat << DONE
Current Settings:
mode        Mode                    $MODE
mw          Middleware              $START_MW
wl          WebLogic Server         $START_WL
secsvr      Security Server         $START_SECSVR
secui       Security UI             $START_SECUI
api         Mule API                $START_MULE_API
cu          Mule Client Updater     $START_MULE_CU
ba          BatchAgent              $START_BATCHAGENT
logs        Clean Log Files         $CLEAN_LOGS
DONE
fi
