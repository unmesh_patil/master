#!/bin/sh

#RESET_OPTIONS can be set to true from envconf.sh (only used from envctl.sh)
RESET_OPTIONS=$1

#users
ARBOR_USER=arbor
BATCH_USER=batch
CRBILLING_USER=crbilling
MULEAPI_USER=muleapi

#servers
ARBOR_SERVER=`hostname`.cycle30.com
BATCH_SERVER=`hostname`.cycle30.com
CRBILLING_SERVER=`hostname`.cycle30.com
MULEAPI_SERVER=`hostname`.cycle30.com

#other inits
WAIT_DURATION=120
SEC_HOME=/opt/app/kenanfx2/`whoami`/FXSecServer-5.0.20080305

MODE=undefined
CLEAN_LOGS=false
START_MW=true
START_WL=true
START_SECSVR=true
START_SECUI=true
START_MULE_API=true
START_MULE_CU=true
START_BATCHAGENT=true

case "X${ENVIRONMENT}" in
    Xdev)
      OWNER_EMAIL=""
	;;
    Xcfg)
      CLEAN_LOGS=true
      OWNER_EMAIL="dave.wreggit@cycle30.com"
      ;;
    Xdem)
      OWNER_EMAIL="Jason.Brunelle@cycle30.com"
      ;;
    Xtst)
      OWNER_EMAIL=""
      ;;
    Xuat)
      OWNER_EMAIL=""
      ;;
    Xprf)
      OWNER_EMAIL=""
      ;;
    Xprd)
      OWNER_EMAIL="Doug.Franklin@cycle30.com"
	;;
    *)
	  echo "Unable to determine environment.  Exiting."
	  exit 1
	;;
esac

#
#reset all options to "false" if there's more than one parameter on the command line
#as determined by the calling program (envconf.sh).   This allows each program to be
#explicitly enabled while all others are turned disabled
#
if [ "X$RESET_OPTIONS" = "Xtrue" ]; then
	CLEAN_LOGS=false
	START_MW=false
	START_WL=false
	START_SECSVR=false
	START_SECUI=false
	START_MULE_API=false
	START_MULE_CU=false
	START_BATCHAGENT=false
fi

