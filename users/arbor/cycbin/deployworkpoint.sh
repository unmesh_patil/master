#!/bin/bash

today=`date +%Y%m%0e.%H%M`;

# Check for the right number of arguments.
if [ "$#" -lt "3" ]; then
  echo "Usage: $0 <revision> <userid> <password> [<branchtag>]"
  exit 1
else
  if [ $# = 3 ]; then
    REVISION=$1;
    SVN_USER=$2;
    SVN_PASSWORD=$3;
    BRANCH="trunk";
  else
    if [ $# = 4 ]; then
      REVISION=$1;
      SVN_USER=$2;
      SVN_PASSWORD=$3;
      BRANCH=branches/$4;
    else
      echo "Usage: $0 <revision> <userid> <password> [<branchtag>]"
    fi
  fi
fi

# directory setup for testing
basedir=$HOME

OLD_DIR=`pwd`

SHORTREV=`expr $REVISION - 1`;
BASE_DEST=dist
BASE_TRUNK=dist/$BRANCH
PROJECT=Weblogic
DEST=$BASE_DEST/$PROJECT;

DEPLOY_DIR=$HOME/deployc30

REVISION_RANGE=$SHORTREV:$1

#echo REVISION_RANGE=$REVISION_RANGE
#echo DEPLOY_DIR="$DEPLOY_DIR"
#echo BASE_DEST="$BASE_DEST"
#echo DEST="$DEST"
#exit


BASE_URL=http://subversion.assembla.com/svn/c30billing/awn/integration

svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $REVISION_RANGE $BASE_URL/$BASE_TRUNK/$PROJECT | sed -e "s/^MM*[ \t]*//" | sed -e "s/^ [AM][ \t]*//" | sed -e "s/^A[ \t]*//" | sed -e "/^D[ \t]*/ d" | sed -e "s|$BASE_URL/$BASE_TRUNK/$PROJECT/||" > tmp.file
svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $REVISION_RANGE $BASE_URL/$BASE_TRUNK/$PROJECT | sed -e "/^MM*[ \t]*/ d" | sed -e "/^ [AM][ \t]*/ d" | sed -e "/^A[ \t]*/ d" | sed -e "s/^D[ \t]*//" | sed -e "s|$BASE_URL/$BASE_TRUNK/$PROJECT/||" > tmpd.file
 
if [ -e $DEPLOY_DIR/$DEST ]
   then
     rm -rf $DEPLOY_DIR/$DEST
fi

while read line
do
#  echo line=$line
#  echo dirname=`dirname $line`

  echo Deleting $line from $HOME/bea/Weblogic/user_projects/domains
  rm -f $HOME/bea/Weblogic/user_projects/domains/$line
done < tmpd.file
rm -f tmpd.file
#echo Done reading delete list

while read line
do
  HAS_REVS=true
#  echo Checking out $line to $DEPLOY_DIR/$DEST/$line
  mkdir -p $DEPLOY_DIR/$DEST/`dirname $line`
  svn export --username=$SVN_USER --password=$SVN_PASSWORD -r $REVISION $BASE_URL/$BASE_TRUNK/$PROJECT/$line $DEPLOY_DIR/$DEST/$line
done < tmp.file
rm -f tmp.file

if [ "X${HAS_REVS}" = "Xtrue" ] ; then
	svn export --force --username=$SVN_USER --password=$SVN_PASSWORD $BASE_URL/$BASE_TRUNK/buildscripts/workpoint $DEPLOY_DIR/$DEST

	tolower() { echo $1 | tr "[:upper:]" "[:lower:]"; }
	DEPLOY_ENVIRONMENT=$(tolower ${SYSTEM_NAME}${SYSTEM_TYPE}${ENVIRONMENT})

	if [ -d  "$DEPLOY_DIR/$DEST/workpoint_site" ]; then
		cd $DEPLOY_DIR/$DEST
		ant -DENVIRONMENT=$DEPLOY_ENVIRONMENT copy
	fi
	
	### Turn off weblogic if WorkPoint/* files exist in release ###
	if [ -d "$DEPLOY_DIR/$DEST/WorkPoint" ]; then
#		echo "**************************"
#		echo "**  Stopping WebLogic  ***"
#		echo "**************************"
#		cd ~/bea/Weblogic/user_projects/domains/WorkPoint/bin
#		./stopWebLogic.sh
		cd $DEPLOY_DIR/$DEST
		ant -DENVIRONMENT=$DEPLOY_ENVIRONMENT wldeploy
#
#		echo "**************************"
#		echo "**  Starting WebLogic  ***"
#		echo "**************************"
#		cd ~/bea/Weblogic/user_projects/domains/WorkPoint
#		./startWebLogic.sh
		cd ~
	fi
	
fi

find ~/bea/Weblogic/user_projects/domains/WorkPoint -name "*.sh" | xargs chmod ug+x
find ~/workpoint_site/install_scripts -name "*.sh" | xargs chmod ug+x

echo "Done"

cd $OLD_DIR
rm -rf $DEPLOY_DIR
