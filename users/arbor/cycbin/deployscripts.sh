#!/bin/bash

if [ $# -lt 3 ]; then
    echo "Usage: $0 <revision> <userid> <password> [<branch>]"
    exit 1
else
  if [ $# -eq 3 ]; then
    REVISION=$1
    SVN_USER=$2
    SVN_PASSWORD=$3
    SVN_BASE=trunk
  else
# Revision and branch tag is given
    if [ $# -eq 4 ]; then
      REVISION=$1
      SVN_USER=$2
      SVN_PASSWORD=$3
      RELEASE_TAG=$4
      SVN_BASE=branches/$4
    fi
  fi
fi

ARBORDIR=/opt/app/kenanfx2/`whoami`

#echo ENVIRONMENT=$ENVIRONMENT
#echo SVN_BASE=$SVN_BASE

#Get scripts from CVS

DATE=`date +%Y%m%d`
LOGFILE=/tmp/deploy.$DATE.$$.log
DISTPROJ=arbor/scripts
export ROOT=https://subversion.assembla.com/svn/c30billing/awn

ORIGDIR=/opt/app/kenanfx2/`whoami`
DISTDIR=$ORIGDIR/deployc30
DEST=arbor
SHORTREV=`expr $REVISION - 1`
RELEASELISTFILE=/tmp/tmps.$REVISION.file

if [ ! -d $DISTDIR ]; then 
  mkdir $DISTDIR
else
  rm -rf $DISTDIR
  mkdir -p $DISTDIR/dist/$DISTPROJ
fi
cd $DISTDIR
if [ -a dist ]; then
  rm -rf dist
fi

#echo Getting list of files to deploy
# use "| sed -e "/^D[ \t]*.*/d" | " to remove deleted files from the list
svn diff --username=$SVN_USER --password=$SVN_PASSWORD --summarize -r $SHORTREV:$REVISION $ROOT/integration/dist/$SVN_BASE/arbor | sed -e "s/^MM*[ \t]*//" | sed -e "s/^ [AM][ \t]*//" | sed -e "s/^A[ \t]*//" | sed -e "s|$ROOT/integration/dist/$SVN_BASE/arbor/||" > $RELEASELISTFILE

HAS_FILES=

#Backup files and export to local staging area
for f in $( cat $RELEASELISTFILE ); do
  HAS_FILES="true"
  BASEFILE=$(basename $f)
  BASEDIR=$(dirname $f)
#  echo BASEDIR=$BASEDIR, BASEFILE=$BASEFILE
  echo Creating directory ${DISTDIR}/${DEST}/${BASEDIR}
  mkdir -p ${DISTDIR}/${DEST}/${BASEDIR}
  if [ -f $ORIGDIR/$BASEDIR/$BASEFILE ]; then
#    echo " "
#    echo "Backing up existing $BASEFILE"
    echo Moving $ORIGDIR/$BASEDIR/$BASEFILE $ORIGDIR/$BASEDIR/$BASEFILE.$DATE
    mv $ORIGDIR/$BASEDIR/$BASEFILE $ORIGDIR/$BASEDIR/$BASEFILE.$DATE
  fi
  echo "Exporting $f"
  svn -q export --username=$SVN_USER --password=$SVN_PASSWORD -r$REVISION $ROOT/integration/dist/$SVN_BASE/arbor/$f $DISTDIR/$DEST/$f
done

if [ "X$HAS_FILES" = "Xtrue" ] ; then
  echo Exporting $ROOT/integration/dist/$SVN_BASE/buildscripts/scripts to 
  echo          $DISTDIR/$DEST

  svn export --force --username=$SVN_USER --password=$SVN_PASSWORD $ROOT/integration/dist/$SVN_BASE/buildscripts/scripts $DISTDIR/$DEST

  cd $ORIGDIR

  ORIGDIR=`pwd`
  cd $DISTDIR/$DEST

  tolower() { echo $1 | tr "[:upper:]" "[:lower:]"; }
  DEPLOY_ENVIRONMENT=$(tolower ${SYSTEM_NAME}${SYSTEM_TYPE}${ENVIRONMENT})

#  echo commandline= ant -DENVIRONMENT=$DEPLOY_ENVIRONMENT master
  ant -DENVIRONMENT=$DEPLOY_ENVIRONMENT master
else
  echo NO FILES FOUND TO BE DEPLOYED
fi

#Cleanup SVN sandbox and release file
rm -f $RELEASELISTFILE
rm -fr $DISTDIR/$DISTPROJ

echo " "
