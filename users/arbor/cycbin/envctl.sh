#!/bin/sh

#enable/disable mail notification (set to false to disable)
SENDEMAIL=true

CONFIRM=true

START_TIME=`date`

USERDIR=/opt/app/kenanfx2/`whoami`

. $USERDIR/cycbin/envconf.sh

if [ "X${MODE}" != "Xstatus" -a "X${CONFIRM}" = "Xtrue" ]; then
  echo -n "Are you sure you want to take this action (Y/N)? "
  read VERACTION
  if [[ $VERACTION != [Yy] ]]; then
    echo "Action not confirmed.  Exiting."
    exit 0
  fi
fi

echo Begin ${MODE} of \'${ENVIRONMENT}\' at $START_TIME

ARBORDIR=/opt/app/kenanfx2/${ARBOR_USER}
cd $ARBORDIR/appdir/log

if [ "X${MODE}" = "Xstop" -o "X${MODE}" = "Xrestart" ]; then

    if [ "X${START_BATCHAGENT}" = "Xtrue" ]; then
        echo Shutting down BatchAgent on ${BATCH_SERVER}
        GETPID=`ps -ef | grep batch | grep -v grep | grep BatchFlowAgent | cut -c10-14`
        CHECK=`echo $GETPID`
        if [ "X${CHECK}" != "X" ] ; then
            ssh ${BATCH_USER}@${BATCH_SERVER} "ps -ef | grep batch | grep -v grep | grep BatchFlowAgent | cut -c10-14 | xargs kill -9"
        fi
    fi

    if [ "X${START_MULE_CU}" = "Xtrue" ]; then
        echo Shutting down API client-updater on ${MULEAPI_SERVER}
        ssh ${MULEAPI_USER}@${MULEAPI_SERVER} "cd client-updater; ./bin/clientupdctl.sh stop"
    fi

    if [ "X${START_MULE_API}" = "Xtrue" ]; then
        # shutdown Mule API Server
        echo Shutting down API on ${MULEAPI_SERVER}
        ssh ${MULEAPI_USER}@${MULEAPI_SERVER} "./bin/fxwebctl.sh stop"
    fi
	
    if [ "X${START_SECUI}" = "Xtrue" ]; then
	  if [ -d $SEC_HOME/ui ] ; then
	    # shut down security server UI
	    echo Shutting down security server UI on ${ARBOR_SERVER}
	    (cd $SEC_HOME/ui/jakarta-tomcat-5.5.9/bin; ./shutdown.sh)
	    sleep 15
	  else
	    echo *** Security Server UI not installed ***
	  fi
    else
        echo Skipping security server shutdown UI on ${ARBOR_SERVER}.
    fi

    if [ "X${START_SECSVR}" = "Xtrue" ]; then
	  #shut down security server
	  echo Shutting down security server on ${ARBOR_SERVER}
	  (cd $SEC_HOME/server; ./s.stop)
    else
      echo Skipping security server shutdown on ${ARBOR_SERVER}.
    fi

    if [ "X${START_MW}" = "Xtrue" ]; then
	  # shutdown middleware
	  echo Shutting down middleware on ${ARBOR_SERVER}
	  FX_Control stop ALL
	  GETPID=`ps -ef | grep BBL | grep -v grep | grep kenanfx2 | cut -c10-14`
	  CHECK=`echo $GETPID`
	  if [ "X${CHECK}" != "X" ] ; then
            echo Killing Middleware services
# exclude LTP which is not currently controlled by envctl.sh (djw, 5/23/2011)
            ps -ef | grep arbor | grep -v grep | grep kenanfx2 | grep -v LTP | cut -c10-14 | xargs kill -9
            ps -ef | grep arbor | grep -v grep | grep WSH | cut -c10-14 | xargs kill -9
# give kill the time to finish the job before running cleanup
            sleep 3
	      echo Running cleanup script
            (cd $HOME/scripts; cleanIPC.sh)
        fi
    else
        echo Skipping middleware shutdown on ${ARBOR_SERVER}.
    fi
	
    if [ "X${START_WL}" = "Xtrue" ]; then
	  # shut down WebLogic
	  echo Shutting down WebLogic on ${ARBOR_SERVER}
	  (cd $HOME/bea/Weblogic/user_projects/domains/WorkPoint; ./stopWebLogic.sh)
	  GETDONE=`grep "Disconnected from weblogic server: WorkPointServer" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
	  ERRORCONNECT=`grep "There is no server running" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
	  TimeoutIncrement=10
	  TimeoutMax=24
      TimeoutCounter=$TimeoutMax
	  while [ "X$GETDONE" == "X" -a "X$ERRORCONNECT" == "X" -a "$TimeoutCounter" -gt "0" ]
	  do
	      echo Waiting for WebLogic to shutdown ${TimeoutCounter}...
	      sleep $TimeoutIncrement
          TimeoutCounter=$(($TimeoutCounter - 1))
 	      GETDONE=`grep "Disconnected from weblogic server: WorkPointServer" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
	      ERRORCONNECT=`grep "There is no server running" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
	  done
	  if [ "X$GETDONE" = "X" -a "X$ERRORCONNECT" = "X" ]; then
	      GETPID=`ps -ef | grep Weblogic | grep java | grep -v shutdown | grep -v grep | awk '{ print $2 ; }'`
	      GETDONE="Tired of waiting for Weblogic to shutdown..."
	      ERRORCONNECT="Killing Weblogic"
	      kill -9 $GETPID
	  fi
	  echo $GETDONE
	  echo $ERRORCONNECT
    else
	  echo Skipping WebLogic on ${ARBOR_SERVER}.
    fi

    if [ "X${CLEAN_LOGS}" = "Xtrue" ]; then
      echo Cleaning up log files on ${ARBOR_SERVER}
      find /opt/app/kenanfx2/${ARBOR_USER}/appdir/log -type f -print -exec rm -f {} \; 
    else
      echo Skipping cleaning up log files on ${ARBOR_SERVER}.
    fi
fi

#
#Startup
#
if [ "X${MODE}" = "Xstart" -o "X${MODE}" = "Xrestart" ]; then
    #start weblogic
    if [ "X${START_WL}" = "Xtrue" ]; then
	  # start up WebLogic
	  echo Starting up WebLogic on ${ARBOR_SERVER}
	  (cd $HOME/bea/Weblogic/user_projects/domains/WorkPoint; ./startWebLogic.sh)
	  echo Waiting for Weblogic to complete starting
	  export GETSTARTED=`grep "Server state changed to RUNNING" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
	  export GETFAILED=`grep "Server state changed to FAILED" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
	  while [ "X$GETSTARTED" == "X" -a "X$GETFAILED" == "X" ]
	  do
	    echo Waiting for WebLogic to complete startup...
	    sleep 15
	    export GETSTARTED=`grep "Server state changed to RUNNING" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
        export GETFAILED=`grep "Server state changed to FAILED" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
	  done
	  echo $GETFAILED
	  echo $GETSTARTED
	  
	  if [ "X$GETFAILED" != "X" ]; then
	    echo ************************************
	    echo * WorkPoint Server Failed to Start *
	    echo ************************************
	  else
            TimeoutInc=5
            TimeoutMax=12
            TimeoutCounter=$TimeoutMax
            export GETSTARTED=`grep "AlertMonitor: Connect attempt" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
            export ISDONE=`grep "AlertMonitor: Becoming master" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
            while [ "X$GETSTARTED" == "X" -a "X$ISDONE" == "X" -a "$TimeoutCounter" -gt "0" ]
            do
              echo Waiting for WebLogic to complete phase 2 startup ${TimeoutCounter}...
              sleep $TimeoutInc
              TimeoutCounter=$(($TimeoutCounter - 1))
              export GETSTARTED=`grep "AlertMonitor: Connect attempt" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
              export ISDONE=`grep "AlertMonitor: Becoming master" $HOME/bea/Weblogic/user_projects/domains/WorkPoint/nohup.out`
            done

            if [ "X$ISDONE" != "X" ]; then
              echo "**************************************************"
              echo "* SUCCESS: WorkPoint Server started successfully *"
              echo "**************************************************"
            elif [ "X$GETSTARTED" != "X" ]; then
              echo "****************************************************"
              echo "* SUCCESS: Weblogic is attempting to become master *"
              echo "****************************************************"
            else
              echo "**************************************************************"
              echo "* FAIL: WorkPoint Server Failed to start correctly (phase 2) *"
              echo "**************************************************************"
            fi
          fi
    else
      echo Skipping WebLogic startup on ${ARBOR_SERVER}.
    fi

    # start middleware
    if [ "X${START_MW}" = "Xtrue" ]; then
#        echo Rebuilding middleware tuxconfig on ${ARBOR_SERVER}
#		cat /opt/app/kenanfx2/${ARBOR_USER}/appdir/KenanFX.ubb | \
#           sed s/@HOST@/`uname -n`/g  | tmloadcf -y -
#
#		rm -f /opt/app/kenanfx/${ARBOR_USER}/appdir/TLOG
#		tmadmin << EOTMADMINCMD
#crdl -z /opt/app/kenanfx/${ARBOR_USER}/appdir/TLOG
#crlog -m FX
#quit
#EOTMADMINCMD
	  echo Starting up middleware on ${ARBOR_SERVER}
	  FX_Control start ALL
	  #verify WorkPoint middleware is running
	  GETPID=`ps -ef | grep wkp_ | grep -v grep | awk '{ print $2 ; }'`
	  CHECK=`echo $GETPID`
	  if [ "X${CHECK}" = "X" ] ; then
		echo Starting WorkPoint middleware services
		FX_Control start WKP
	  fi
	  # Pause a few seconds to allow middleware to start completely to avoid potential errors 
	  # with BatchRefresh startup
	  sleep 3
    else
      echo Skipping middleware startup on ${ARBOR_SERVER}.
    fi

    if [ "X${START_SECSVR}" = "Xtrue" ]; then
		# start security server
		echo Starting up security server in $SEC_HOME on ${ARBOR_SERVER}
		(cd $SEC_HOME/server; ./s.start)
    else
		echo Skipping security server startup on ${ARBOR_SERVER}.
    fi
	
    if [ "X${START_SECUI}" = "Xtrue" ]; then
	# shut down security server UI
	  if [ -d $SEC_HOME/ui ] ; then
		echo Starting up security server UI on ${ARBOR_SERVER}
		(cd $SEC_HOME/ui/jakarta-tomcat-5.5.9/bin; ./startup.sh)
	  else
	    echo *** Security Server UI not installed ***
	  fi
    else
		echo Skipping security server UI startup on ${ARBOR_SERVER}.
    fi

    if [ "X${START_MULE_API}" = "Xtrue" ]; then
        echo Starting API on ${MULEAPI_SERVER}
        ssh ${MULEAPI_USER}@${MULEAPI_SERVER} "./bin/fxwebctl.sh start"
        TimeoutIncrement=5
        TimeoutMax=12
        TimeoutCounter=$TimeoutMax
        export GETSTARTED=`grep "Started app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
        export GETFAILED=`grep "Failed to deploy app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
        while [ "X$GETSTARTED" == "X" -a "X$GETFAILED" == "X" -a "$TimeoutCounter" -gt "0" ]
        do
          echo Waiting for Api to complete startup ${TimeoutCounter}...
          sleep $TimeoutIncrement
          TimeoutCounter=$(($TimeoutCounter - 1))
          export GETSTARTED=`grep "Started app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
          export GETFAILED=`grep "Failed to deploy app 'WebEnablement'" $HOME/../muleapi/logs/WebEnablement.log`
        done
        if [ "X$GETSTARTED" != "X" ]; then
          echo $GETSTARTED
        elif [ "X$GETFAILED" != "X" ]; then
          echo "**************************************"
          echo "* Mule API failed to start correctly *"
          echo "**************************************"
        else
          echo "Unable to determine Mule API startup success within the alloted time."
          echo "Continuing envctl...."
        fi
    fi

    if [ "X${START_MULE_CU}" = "Xtrue" ]; then
        echo Starting Client Updater on ${MULEAPI_SERVER}

	#use ssh -f -n to force ssh to the background so that client-updater process won't hang the
	#startup script while using ssh.  Only required because client-updater doesn't use a java shell
	#to fork the real process.
	#Also, two-part command required because the clientupdctl.sh currently kills any process containing
	#"client-updater" and so it kills itself if run as "./client-updater/bin/clientupdctl.sh start"
		
        ssh ${MULEAPI_USER}@${MULEAPI_SERVER} -f -n "cd client-updater; ./bin/clientupdctl.sh start"
		
    #ToDo:  monitor log to determine start success/failure
	#may need to sleep 5 to allow logs to be created because of the ssh -f
    fi

    if [ "X${START_BATCHAGENT}" = "Xtrue" ]; then
        echo Starting BatchAgent on ${BATCH_SERVER}
        ssh ${BATCH_USER}@${BATCH_SERVER} -f -n "bin/batchAgent_check.sh"
    fi
fi

#
# System STATUS
#
if [ "X${MODE}" = "Xstatus" ]; then
    SENDEMAIL=false

# set all status to unknown
    STATUS_MW=-
    STATUS_WL=-
    STATUS_SECSVR=-
    STATUS_SECUI=-
    STATUS_MULE_API=-
    STATUS_MULE_CU=-
    STATUS_BATCHAGENT=-

#get mw statusi
# This only checks one of many processes started by "FX_Control start ALL"
    GETPID=`ps -ef | grep bp_Catalog | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_MW=up
    else
        STATUS_MW=down
    fi

#get weblogic status
    GETPID=`ps -ef | grep Weblogic | grep java | grep -v shutdown | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_WL=up
    else
        STATUS_WL=down
    fi
    GETPID=`ps -ef | grep Weblogic | grep java | grep shutdown | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_WL="shutting down?"
    fi

#get SECSVR status
    GETPID=`ps -ef | grep SecurityServer | grep java | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_SECSVR=up
    else
        STATUS_SECSVR=down
    fi

#get SECUI status
    GETPID=`ps -ef | grep FXSecServer-5.0.20080305/ui | grep java | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_SECUI=up
    else
        STATUS_SECUI=down
    fi

#get Mule API status
    GETPID=`ps -ef | grep muleapi | grep java | grep MuleContainerBootstrap | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_MULE_API=up
    else
        STATUS_MULE_API=down
    fi

#get Client Updater status
    GETPID=`ps -ef | grep java | grep client-updater | grep -v grep | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_MULE_CU=up
    else
        STATUS_MULE_CU=down
    fi
	
#get BatchAgent status
    GETPID=`ps -ef | grep batch | grep -v grep | grep BatchFlowAgent | awk '{ print $2 ; }'`
    if [ "X${GETPID}" != "X" ] ; then
        STATUS_BATCHAGENT=up
    else
        STATUS_BATCHAGENT=down
    fi

#
# uses spaces for the environment settings because they are displayed in
# the unix console and tabs won't look good.
#

cat << DONE
Module status:
Abbr        Module                  $MODE
mw          Middleware              $STATUS_MW
wl          WebLogic Server         $STATUS_WL
secsvr      Security Server         $STATUS_SECSVR
secui       Security UI             $STATUS_SECUI
api         Mule API                $STATUS_MULE_API
cu          Mule Client Updater     $STATUS_MULE_CU
ba          BatchAgent              $STATUS_BATCHAGENT

DONE

fi

FINISH_TIME=`date`
echo Finished ${MODE} of ${ENVIRONMENT} at $FINISH_TIME

#
# uses tabs for the environment settings because they are displayed in
# email and spaces won't work as well.  The irregular tabs are required for
# the email to look correct.
#

if [ "X$SENDEMAIL" = "Xtrue" ]; then
/usr/sbin/sendmail -oi -t << EOMAIL
From: ${ARBOR_USER}@${ARBOR_SERVER}
To: Cycle30-C3BEnvironmentTeam@gci.com
Subject: A ${MODE} of $SYSTEM_NAME "${SYSTEM_TYPE}${ENVIRONMENT}" environment complete
CC: $OWNER_EMAIL

A ${MODE} of the $SYSTEM_NAME "${SYSTEM_TYPE}${ENVIRONMENT}" environment is complete.

Timing:
    Start time:  $START_TIME 
    Finish time: $FINISH_TIME

Current Settings:
	mode		Mode				$MODE
	mw		Middleware			$START_MW
	wl		WebLogic Server		$START_WL
	secsvr		Security Server 		$START_SECSVR
	secui		Security UI			$START_SECUI
	api		Mule API			$START_MULE_API
	cu		Mule Client Updater		$START_MULE_API
	ba		BatchAgent			$START_BATCHAGENT
	logs		Clean Log Files			$CLEAN_LOGS

Called by: 
    $0 $*

Command options:  start, stop, restart, status

EOMAIL
fi
