# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

. $HOME/../cyc.setenv
. $HOME/cyc.bashrc
. $HOME/../sec.bashrc
